//========================================================================================
//  
//  $File: $
//  
//  Owner: RD
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "AFWJSID.h"
#include "CAlert.h"
//#include "libjson.h"
//#include "JSONNode.h"
//#include "JSONOptions.h"
//#include "curl.h"
//#include <stdio.h>
#include "IAppFramework.h"
#include "structureCache.h"
#include "ConfigCache.h"

#define CA(X)  CAlert::InformationAlert(X)
/** AFWJSDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup apjs9_appframework
*/


class AFWJSDialogController : CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		AFWJSDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~AFWJSDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(AFWJSDialogController, kAFWJSDialogControllerImpl)


//struct AppMemoryStruct 
//{
//	char *memory;
//	size_t size;
//};
//
//struct data {
//  char trace_ascii; /* 1 or 0 */ 
//};
//
//static
//void dump(const char *text,
//          FILE *stream, unsigned char *ptr, size_t size,
//          char nohex)
//{
//  size_t i;
//  size_t c;
// 
//  unsigned int width=0x10;
// 
//  if(nohex)
//    /* without the hex output, we can fit more on screen */ 
//    width = 0x40;
// 
//  fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
//          text, (long)size, (long)size);
// 
//  for(i=0; i<size; i+= width) {
// 
//    fprintf(stream, "%4.4lx: ", (long)i);
// 
//    if(!nohex) {
//      /* hex not disabled, show it */ 
//      for(c = 0; c < width; c++)
//        if(i+c < size)
//          fprintf(stream, "%02x ", ptr[i+c]);
//        else
//          fputs("   ", stream);
//    }
// 
//    for(c = 0; (c < width) && (i+c < size); c++) {
//      /* check for 0D0A; if found, skip past and start a new line of output */ 
//      if (nohex && (i+c+1 < size) && ptr[i+c]==0x0D && ptr[i+c+1]==0x0A) {
//        i+=(c+2-width);
//        break;
//      }
//      fprintf(stream, "%c",
//              (ptr[i+c]>=0x20) && (ptr[i+c]<0x80)?ptr[i+c]:'.');
//      /* check again for 0D0A, to avoid an extra \n if it's at width */ 
//      if (nohex && (i+c+2 < size) && ptr[i+c+1]==0x0D && ptr[i+c+2]==0x0A) {
//        i+=(c+3-width);
//        break;
//      }
//    }
//    fputc('\n', stream); /* newline */ 
//  }
//  fflush(stream);
//}
// 
//static
//int my_trace(CURL *handle, curl_infotype type,
//             char *data, size_t size,
//             void *userp)
//{
//  struct data *config = (struct data *)userp;
//  const char *text;
//  (void)handle; /* prevent compiler warning */ 
//  FILE *headerfile;
//	  headerfile = fopen("D:\\RD\\Apsiva\\json\\ApsivaTestData\\libjsontestIDResult.txt", "a");
//
//  switch (type) {
//  case CURLINFO_TEXT:
//    fprintf(stderr, "== Info: %s", data);
//	text = "=> CURLINFO_TEXT : ";
//	 dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
//	//CA(data);
//  default: /* in case a new one is introduced to shock us */ 
//    return 0;
// 
//  case CURLINFO_HEADER_OUT:
//    text = "=> Send header";
//    break;
//  case CURLINFO_DATA_OUT:
//    text = "=> Send data";
//    break;
//  case CURLINFO_SSL_DATA_OUT:
//    text = "=> Send SSL data";
//    break;
//  case CURLINFO_HEADER_IN:
//    text = "<= Recv header";
//    break;
//  case CURLINFO_DATA_IN:
//    text = "<= Recv data";
//    break;
//  case CURLINFO_SSL_DATA_IN:
//    text = "<= Recv SSL data";
//    break;
//  }
// 
// 
//  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
//
//  fclose(headerfile);
//  return 0;
//}
//
//static	 size_t
//AppWriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
//{
//	//CA("Inside AppWriteMemoryCallback");
//  size_t realsize = size * nmemb;
//  struct AppMemoryStruct *mem = (struct AppMemoryStruct *)userp;
// 
//  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
//  if (mem->memory == NULL) {
//    /* out of memory! */ 
//    //CA("not enough memory (realloc returned NULL)\n");
//    exit(EXIT_FAILURE);
//  }
// 
//  memcpy(&(mem->memory[mem->size]), contents, realsize);
//  mem->size += realsize;
//  mem->memory[mem->size] = 0;
// 
//  return realsize;
//}


/* ApplyFields
*/
void AFWJSDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.

	//PMString sessionId("");

	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	CAlert::InformationAlert("ptrIAppFramework is nil");
	//	return;

	//}


	//
	///*LoginInfoValue  loginInfoValueObj = ptrIAppFramework->getLoginInfoProperties();

	//ptrIAppFramework->LogDebug(loginInfoValueObj.getCmurl());
	//ptrIAppFramework->LogDebug(loginInfoValueObj.getUsername());
	//ptrIAppFramework->LogDebug(loginInfoValueObj.getPassword());
	//ptrIAppFramework->LogDebug(loginInfoValueObj.getClientno());
	//ptrIAppFramework->LogDebug(loginInfoValueObj.getAssetserverpath());

	//if(loginInfoValueObj.getCmurl() != "")
	//{
	//	bool16 result=ptrIAppFramework->callLogin(loginInfoValueObj.getCmurl(), loginInfoValueObj.getUsername(), loginInfoValueObj.getPassword(), loginInfoValueObj.getApikey(), loginInfoValueObj.getClientno(), sessionId );
	//	if(result)
	//	{
	//		CA(sessionId);
	//		

	//		ptrIAppFramework->setAssetServerPath(loginInfoValueObj.getAssetserverpath());
	//		ptrIAppFramework->StructureCache_clearInstance();
	//		ptrIAppFramework->EventCache_clearInstance();
	//		ptrIAppFramework->EventCache_clearCurrentSectionData();
	//		ptrIAppFramework->clearAllStaticObjects();
	//	}

	//}*/
	///*bool16 result=ptrIAppFramework->callLogin("https://prd04.apsiva.net/cf-cm", "webapi2@apsiva.com", "wa7744$$", "1dd80ee107d3ad9593c857cc6d14d3a3","244", sessionId );
	//if(result)
	//	CA(sessionId);*/


	////VectorLoginInfoValue newVectorServerInfoPtr = new VectorServerInfoValue;
	//LoginInfoValue  loginInfoValueObj;
	//loginInfoValueObj.setEnvName("FirstEnv");
	//loginInfoValueObj.setCmurl("https://prd04.apsiva.net/cf-cm4");
	//loginInfoValueObj.setUsername("webapi2@apsiva.com");
	//loginInfoValueObj.setPassword("wa7744$$");
	////loginInfoValueObj.setApikey("1dd80ee107d3ad9593c857cc6d14d3a3");
	////loginInfoValueObj.setClientno("244");

	////newVectorServerInfoPtr->push_back(loginInfoValueObj);

	//bool16 isCreated = ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(loginInfoValueObj, 1, 244);

	//LoginInfoValue  loginInfoValueObj1;
	//loginInfoValueObj1.setEnvName("SecondEnv");
	//loginInfoValueObj1.setCmurl("https://prd04.apsiva.net/cf-cm2");
	//loginInfoValueObj1.setUsername("webapi2@apsiva.com");
	//loginInfoValueObj1.setPassword("wa7744$$");
	////loginInfoValueObj1.setApikey("1dd80ee107d3ad9593c857cc6d14d3a3");
	////loginInfoValueObj1.setClientno("245");

	//isCreated = ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(loginInfoValueObj1, 1, 245);

	//if(isCreated)
	//{
	//	VectorLoginInfoPtr vectorServerInfoPtr =  ptrIAppFramework->getLoginInfoProperties();
	//	if(vectorServerInfoPtr != NULL)
	//	{
	//		PMString asd("size: ");
	//		asd.AppendNumber((int32)vectorServerInfoPtr->size());
	//		CA(asd);
	//	}
	//}
}

/* ValidateFields
*/
WidgetID AFWJSDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.


	return result;
}

/* ApplyFields
*/
void AFWJSDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.

	PMString logoutResult("");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return;

	}

	/*StructureCache* structureCacheObj = StructureCache::getInstance();
	if(structureCacheObj->loadStructureCache())		
	{
		CA("Got loadStructureCache");
		
	}
	else
		CA("loadStructureCache Failed");*/

	//ConfigCache* configCacheObj = ConfigCache::getInstance();
	//if(configCacheObj->loadConfigCache())		
	//{
		//CA("Got loadConfigCache");
		
	//}
	//else
	//	CA("loadConfigCache Failed");

	/*PMString alltreeJson;
	bool16 result2=ptrIAppFramework->callAllTreeJson(alltreeJson);
	if(result2)
		CA(alltreeJson);
*/

	// htps://prd02.apsiva.net/cf-cm/json/244/login.json?username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3
	/*bool16 result1=ptrIAppFramework->callLogout(logoutResult);
	if(result1)
		CA(logoutResult);*/
}
