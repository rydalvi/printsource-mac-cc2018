/*
//	File:	ClientOptions.cpp
//
//	Date:	17-Oct-2003
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "CPMUnknown.h"

// Project includes:
#include "IClientOptions.h"
//#include "OptionsUtils.h"
//#include "PubBrowseDlgCreator.h"
//#include "PathBrowseDlgCreator.h"

#include "HelperInterface.h"
#include "AFWJSID.h"
#include "OptionsValue.h"
//#include "OptionsStaticData.h"
#include "CAlert.h"
#include "ITextControlData.h"

#include "IAppFramework.h"
#include "LoginInfoValue.h"
#include "ClientInfoValue.h"
//#include "IPanelMgr.h"
//#include "IWindow.h"
//#include "PaletteRefUtils.h"
//#include "ICategoryBrowser.h"
//#include "IWidgetParent.h"
//
//#include "ISpecialChar.h"

#define CA(X) CAlert::InformationAlert(X)

bool16 doRefreshFlag = kFalse;

class ClientOptions : public CPMUnknown<IClientOptions>
{
	public:
		/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ClientOptions(IPMUnknown*  boss);
		/** 
			Destructor.
		*/
		virtual ~ClientOptions();		
		virtual int32 getDefaultLocale(PMString&);
		virtual int32 getDefPublication(PMString&);
		virtual PMString getImageDownloadPath(void);
		virtual PMString getDocumentDownloadPath(void);
		virtual int32 getDefPublicationDlg(PMString&, bool16);
		virtual void setdoRefreshFlag(bool16);
		
		virtual bool8 checkforFolderExists(PMString&);
		PMString getWhiteBoardMediaPath();

		virtual void setLocale(PMString&, int32&);
		virtual void setPublication(PMString&, int32&);
		virtual void setSection(PMString& ,int32& ) ;

		//virtual void setDefaultFLow(int32 );
		virtual void set_DisplayPartnerImages(int32 );
		virtual void set_DisplayPickListImages(int32 );

		void setAll(PMString& publicationName, int32& publicationId ,PMString& languageName,
					int32& languageId ,PMString& sectionName, int32& sectionId);


	private:
		OptionsValue& getClientOptions(void);
		OptionsValue OptionsObj;
		//int32 checkforPubExistance(PMString& ,int32&); 
		////bool8 checkforFolderExists(PMString&);
		//int32 checkforLocaleExistance(PMString& lName,int32& languageID);
		

};

CREATE_PMINTERFACE(ClientOptions, kClientOptionsImpl)

/* ClientOptions Constructor
*/
ClientOptions::ClientOptions(IPMUnknown* boss): 
	CPMUnknown<IClientOptions>(boss)
{
	
}

/* ClientOptions Destructor
*/
ClientOptions::~ClientOptions()
{
	// Add code to delete extra private data, if any.
}

OptionsValue& ClientOptions::getClientOptions(void)
{
	OptionsValue optnsVal;
	OptionsObj = optnsVal;
	do{
		//ClientOptionsManip cops;		
		/*bool8 isFileExists=cops.IsClientOptionsFileExiats();
		if(isFileExists==FALSE){
			PMString filePath = cops.GetOptionsFilePath();
			if(cops.CreateOptionsFile(filePath)==FALSE){
				CAlert::InformationAlert("Failed to Create File");
				break;
			}
		}*/
		//OptionsObj = cops.ReadClientOptions();
	}while(0);
	return OptionsObj;
}

int32 ClientOptions::getDefPublication(PMString& pubName)
{	
	//CA("Inside ClientOptions::getDefPublication");
	/*int32 val = -1;
	int32 pubID= -1;
	OptionsObj = getClientOptions();*/
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return -1;
	}

	return ptrIAppFramework->getCatalogId();

	//int32 proID = (OptionsObj.getProjectID()).GetAsNumber();//added by mane

	//if(OptionsObj.getPublicationName().NumUTF16TextChars() > 0 && checkforPubExistance(OptionsObj.getPublicationName(),proID) == -1)
	//{
	//	//CA("OptionsObj.getPublicationName().NumUTF16TextChars() >0");
	//	PMString str("");
	//	str = OptionsObj.getPublicationName();
	//	if(str.NumUTF16TextChars() > 0 ){
	//		// if publication deleted from database
	//		PMString message("The current catalog \"");
	//		message += str;
	//		message += "\" does not exist. Please select another catalog.  ";
	//		PMString ok_btn("OK");			
	//		CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
	//		OptionsObj.setPublicationName(PMString(""));
	//		OptionsObj.setPublicationComment(PMString(""));
	//		ClientOptionsManip cops;
	//		cops.WriteClientOptions(OptionsObj);	
	//	}		
	//}
	//if(OptionsObj.getPublicationName().NumUTF16TextChars() == 0)
	//{
	//	//CA("OptionsObj.getPublicationName().NumUTF16TextChars() == 0");
	//	// tracking which button pressed
	//	PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
	//	// for showing stand alone publication browse dialog
	//	/*PersistData::setPublicationDialogType(2);
	//	PubBrowseDlgCreator pubDlagObj(this);
	//	pubDlagObj.CreateDialog();*/

	//	//------------

	//	bool16 isUserLoggedIn=ptrIAppFramework->LOGINMngr_getLoginStatus();
	//	if(!isUserLoggedIn)
	//		return val;
	//	//CA("Inside selected class");
	//	InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
	//	if(iApplication==nil)
	//	{
	//		//CA("iApplication==nil");
	//		return val;
	//	}
	//	InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
	//	if(iPanelMgr == nil)
	//	{
	//		//CA("iPanelMgr == nil");
	//		return val;
	//	}
	//	UID paletteUID = kInvalidUID;
	//	int32 TemplateTop	=0;	
	//	int32 TemplateLeft	=0;		
	//	int32 TemplateRight	=0;	
	//	int32 TemplateBottom =0;	

	//	const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

	//	IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
	//	if(pnlControlView == NULL)
	//	{
	//		//CA("pnlControlView is NULL");
	//		return val;
	//	}

	//	InterfacePtr<IWidgetParent> panelWidgetParent(pnlControlView, UseDefaultIID());
	//	if(panelWidgetParent == NULL)
	//	{
	//		//CA("panelWidgetParent is NULL");
	//		return val;
	//	}
	//	InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
	//	if(palette == NULL)
	//	{
	//		//CA("palette is NULL");
	//		return val;
	//	}
	//	InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
	//	PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
	//	if(palRef.IsValid())
	//		//CA("validPaletRef");
	//	//----------------New CS3 Changes--------------------//
	//	bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
	//	
	//	if(palette)						//if(palette)
	//	{
	//		//CA("palette");
	//		palette->AddRef();
	//		GSysRect PalleteBounds = palette->GetFrameBBox();
	//		//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
	//		TemplateTop		= PalleteBounds.top -30;
	//		TemplateLeft	= PalleteBounds.left;
	//		TemplateRight	= PalleteBounds.right;
	//		TemplateBottom	= PalleteBounds.bottom;
	//	
	//		InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
	//		if(!CatalogBrowserPtr)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
	//			return val;
	//		}
	//		CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight ,1 ,-1);
	//	}

	////	OptionsObj = getClientOptions();
	////	pubID = checkforPubExistance(pubName,proID);//added by mane

	//}
	//pubName = OptionsObj.getPublicationName();
	//pubID = checkforPubExistance(OptionsObj.getPublicationName(),proID);//added by mane

	////CA("pubName  :  "+pubName);

	//ClientOptionsManip cops;
	//cops.SetCatalogIDInAppFramework(pubID);
	///*if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
	//	return -1;*/
	////CA("Exit from ClientOptions::getDefPublication");
	//return pubID;
}

int32 ClientOptions::getDefaultLocale(PMString& localeName)
{	
	//CA("ClientOptions::getDefaultLocale");
	/*int32 localeID= -1;
	OptionsObj = getClientOptions();
	int32 val = -1;*/
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return -1;
	}

	return ptrIAppFramework->getLocaleId();

	//PMString languageIDStr = OptionsObj.getLocaleID();
	//int32 languageID = languageIDStr.GetAsNumber();
	//if(OptionsObj.getDefaultLocale().NumUTF16TextChars() >0 && checkforLocaleExistance(OptionsObj.getDefaultLocale(),languageID) == -1){
	//	//CA("OptionsObj.getDefaultLocale().NumUTF16TextChars() >0 && checkforLocaleExistance(OptionsObj.getDefaultLocale()) == -1");
	//	PMString str("");
	//	str = OptionsObj.getDefaultLocale();
	//	if(str.NumUTF16TextChars() > 0 ){
	//		// if publication deleted from database
	//		PMString message("The current Locale \"");
	//		message += str;
	//		message += "\" does not exist. Please select another Locale.  ";
	//		PMString ok_btn("OK");			
	//		CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
	//		OptionsObj.setDefaultLocale(PMString(""));
	//		ClientOptionsManip cops;
	//		cops.WriteClientOptions(OptionsObj);			
	//	}		
	//}
	//if(OptionsObj.getDefaultLocale().NumUTF16TextChars() == 0)
	//{
	//	//CA("OptionsObj.getDefaultLocale().NumUTF16TextChars() == 0");
	//	// tracking which button pressed
	//	PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
	//	// for showing stand alone publication browse dialog
	//	/*PersistData::setPublicationDialogType(2);
	//	PubBrowseDlgCreator pubDlagObj(this);
	//	pubDlagObj.CreateDialog();*/
	//	
	//	//-------------
	//	bool16 isUserLoggedIn=ptrIAppFramework->LOGINMngr_getLoginStatus();
	//	if(!isUserLoggedIn)			
	//		return val;
	//	//CA("Inside selected class");
	//	InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
	//	if(iApplication==nil)
	//	{
	//		//CA("iApplication==nil");
	//		return val;
	//	}
	//	InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
	//	if(iPanelMgr == nil)
	//	{
	//		//CA("iPanelMgr == nil");
	//		return val;
	//	}
	//	UID paletteUID = kInvalidUID;
	//	int32 TemplateTop	=0;	
	//	int32 TemplateLeft	=0;		
	//	int32 TemplateRight	=0;	
	//	int32 TemplateBottom =0;	

	//	const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

	//	IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
	//	if(pnlControlView == NULL)
	//	{
	//		//CA("pnlControlView is NULL");
	//		return val;
	//	}

	//	InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
	//	if(panelWidgetParent == NULL)
	//	{
	//		//CA("panelWidgetParent is NULL");
	//		return val;
	//	}
	//	InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
	//	if(palette == NULL)
	//	{
	//		//CA("palette is NULL");
	//		return val;
	//	}
	//	InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
	//	PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
	//	//if(palRef.IsValid())
	//		//CA("validPaletRef");
	//	//----------------New CS3 Changes--------------------//
	//	bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
	//	
	//	if(palette)						//if(palette)
	//	{
	//		//CA("palette----1");
	//		palette->AddRef();
	//		GSysRect PalleteBounds = palette->GetFrameBBox();
	//		//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
	//		TemplateTop		= PalleteBounds.top -30;
	//		TemplateLeft	= PalleteBounds.left;
	//		TemplateRight	= PalleteBounds.right;
	//		TemplateBottom	= PalleteBounds.bottom;
	//
	//		InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
	//		if(!CatalogBrowserPtr)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
	//			return val;
	//		}
	//		CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 ,-1);
	//	}

	////	OptionsObj = getClientOptions();		
	////	localeID = checkforLocaleExistance(localeName,languageID);
	//}

	//
	//localeName = OptionsObj.getDefaultLocale();
	//localeID = checkforLocaleExistance(OptionsObj.getDefaultLocale(),languageID);	
	//ClientOptionsManip cops;
	//cops.SetLocaleIDInAppFramework(localeID);
	///*if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
	//	return -1;*/
	////CA("Exit from ClientOptions::getDefaultLocale");

	//return localeID;
	
}



PMString ClientOptions::getImageDownloadPath(void)
{	//CA("ClientOptions::getImageDownloadPath");
	//OptionsObj = getClientOptions();
	//bool8 isDirExists = checkforFolderExists(OptionsObj.getImagePath());	
	////CA(OptionsObj.getImagePath());
	//if(!isDirExists || OptionsObj.getImagePath().NumUTF16TextChars()==0 || OptionsObj.getImagePath() == "")
	//{ 
	//	//CA("OptionsObj.getImagePath()="+OptionsObj.getImagePath());
	//	PersistData::setPathBrowseDialogType(1);
	//	PathBrowseDlgCreator pathDlgObj(this);
	//	pathDlgObj.CreateDialog();

	//	OptionsObj = getClientOptions();
	//}
	//return OptionsObj.getImagePath();
	//return ("C:\\Apsiva");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return "";
	}

		VectorLoginInfoPtr result=ptrIAppFramework->getLoginInfoProperties();
		if(result==NULL)
		{
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			return "";
		}			
		if(result->size()==0){
			//CAlert::InformationAlert("getCurrentServerInfo::Available Servers File show zero entries.");
			return "";
		}
		VectorLoginInfoValue::iterator it;
		PMString envName = ptrIAppFramework->getSelectedEnvName();
		PMString clientId1("");
		clientId1.AppendNumber(ptrIAppFramework->getClientID());

		for(it = result->begin(); it != result->end(); it++)
		{
			LoginInfoValue currentServerInfo=*it;

			if(currentServerInfo.getEnvName().Compare(kTrue , envName) == 0)
			{
				ClientInfoValue clinetInfoObj = currentServerInfo.getClientInfoByClientNo(clientId1);
				ptrIAppFramework->setAssetServerPath(clinetInfoObj.getAssetserverpath());

			}
		}
		ptrIAppFramework->LogDebug(ptrIAppFramework->getAssetServerPath());
	return ptrIAppFramework->getAssetServerPath();
}

PMString ClientOptions::getDocumentDownloadPath(void)
{
	/*OptionsObj = getClientOptions();
	bool8 isDirExists = checkforFolderExists(OptionsObj.getIndesignDocPath());
	if(!isDirExists || OptionsObj.getIndesignDocPath().NumUTF16TextChars()==0 || OptionsObj.getIndesignDocPath() == "")
	{
		PersistData::setPathBrowseDialogType(2);
		PathBrowseDlgCreator pathDlgObj(this);
		pathDlgObj.CreateDialog();
		OptionsObj = getClientOptions();
	}
	return OptionsObj.getIndesignDocPath();*/
	//return ("C:\\Apsiva");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return "";
	}
	return ptrIAppFramework->getAssetServerPath();
}

//int32 ClientOptions::checkforPubExistance(PMString& pName,int32& proID)
//{
//	////CA("ClientOptions::checkforPubExistance");
//	//ClientOptionsManip cops;
//	//int32 pubID;
//	//pubID = cops.checkforPublicationExistance(pName, proID);//added by mane
//	//PersistData::setPubId(pubID); //added by mane
//	////CA("Premal1");	
//	//return pubID;
//	return proID;
//}

//int32 ClientOptions::checkforLocaleExistance(PMString& lName, int32& languageID)
//{
//	/*int32 localeID=-1;
//	ClientOptionsManip cops;
//	localeID = cops.checkforLocaleExistance(lName,languageID);
//	return localeID;*/
//	return languageID;
//}

bool8 ClientOptions::checkforFolderExists(PMString& fpath)
{
	/*bool8 isFolderExists=FALSE;
	ClientOptionsManip cops;
	isFolderExists = cops.checkforFolderExistance(fpath);
	return isFolderExists;*/
	return true;
}

int32 ClientOptions::getDefPublicationDlg(PMString& pubName, bool16 showDialog)
{//CA("ClientOptions::getDefPublicationDlg");
	//int32 pubID= -1;
	//OptionsObj = getClientOptions();	
	//int32 proID = OptionsObj.getProjectID().GetAsNumber();//added by mane

	//if(OptionsObj.getPublicationName().NumUTF16TextChars() >0 && checkforPubExistance(OptionsObj.getPublicationName(),proID) == -1)//argument(proID)added by mane
	//{
	//	PMString str("");
	//	str = OptionsObj.getPublicationName();
	//	if(str.NumUTF16TextChars() > 0 ){
	//		// if publication deleted from database
	//		PMString message("The current catalog \"");
	//		message += str;
	//		message += "\" does not exist. Please select another catalog.  ";
	//		PMString ok_btn("OK");			
	//		CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
	//		OptionsObj.setPublicationName(PMString(""));
	//		OptionsObj.setPublicationComment(PMString(""));
	//		ClientOptionsManip cops;
	//		cops.WriteClientOptions(OptionsObj);			
	//	}		
	//}
	//if(showDialog)
	//{
	//	// tracking which button pressed
	//	PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
	//	// for showing stand alone publication browse dialog
	//	PersistData::setPublicationDialogType(2);
	//	PubBrowseDlgCreator pubDlagObj(this);
	//	pubDlagObj.CreateDialog();
	//	OptionsObj = getClientOptions();		
	//	pubID = checkforPubExistance(pubName,proID);
	//}
	//pubName = OptionsObj.getPublicationName();
	//proID = OptionsObj.getProjectID().GetAsNumber();//added by mane
	//pubID = checkforPubExistance(OptionsObj.getPublicationName(), proID);//added by mane

	//ClientOptionsManip cops;
	//cops.SetCatalogIDInAppFramework(pubID);
	//if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
	//	return -1;
	////CA("exit from showDialog");
	//return pubID;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return -1;
	}

	return ptrIAppFramework->getCatalogId();
}


void ClientOptions::setdoRefreshFlag(bool16 Flag)
{
	doRefreshFlag = Flag;	
}

void ClientOptions::setLocale(PMString& localeName,int32& localeId)
{
	//CA("ClientOptions::setLocale");

	//PersistData::setLocaleName(localeName);
	//
	//ClientOptionsManip cop;
	//int32 LocaleID= cop.checkforLocaleExistance(localeName,localeId);
	//
	////localeId.AppendNumber(LocaleID);
	//
	//PersistData::setLocaleID(LocaleID);
	//
	//int32 localeID = PersistData::getLocaleID();

	//PMString languageIdStr("");
	//languageIdStr.AppendNumber(localeId);
	//
	//OptionsValue optnsObj = cop.ReadClientOptions();
	//optnsObj.setDefaultLocale(localeName);
	//optnsObj.setLocaleID(languageIdStr);
	//cop.WriteClientOptions(optnsObj);

	////CA("localeId : "+ localeId);

	//IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
	//if(panelControlData==nil){
	//	//CAlert::InformationAlert("General panel control data nil");
	//	return;
	//}
	//IControlView* iControlView = panelControlData->FindWidget(kLocaleStaticWidgetID);
	//if(iControlView == nil)
	//{
	//	CAlert::InformationAlert("Fail to find publication static text widget");
	//	return;
	//}	

	//InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	//if (textControlData == nil)
	//{
	//	CAlert::InformationAlert("ITextControlData nil");	
	//	return;
	//}

	//textControlData->SetString(localeName);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		//return -1;
	}

	ptrIAppFramework->setLocaleId(localeId);

}

void ClientOptions::setPublication(PMString& pubName,int32& pubId)
{
	//CA("ClientOptions::setPublication");
	
	//PMString pubNewName = "";
	////pubNewName = pubName ;

	//bool16 isOneSource = kFalse;	
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is nil.");
	//	return;
	//}

	//if(ptrIAppFramework->get_isONEsourceMode())
	//	isOneSource = kTrue;
	//else
	//	isOneSource = kFalse;


	//ClientOptionsManip cop;
	//int32 publicationID= cop.checkforPublicationExistance(pubName,pubId);
	//
	////localeId.AppendNumber(LocaleID);
	//
	//PersistData::setPubId(pubId);
	//PersistData::setPubName(pubName);

	////int32 publicationID = PersistData::getPubId();

	//PMString pubIdStr("");
	//pubIdStr.AppendNumber(publicationID);

	//OptionsValue optnsObj = cop.ReadClientOptions();
	//optnsObj.setPublicationName(pubName);
	//optnsObj.setProjectID(pubIdStr);

	//PMString text = "";
	//if(isOneSource)
	//{
	//	text = "true";
	//	optnsObj.setIsOneSource(text);
	//}
	//else
	//{
	//	text = "false";
	//	optnsObj.setIsOneSource(text);
	//}

	//cop.WriteClientOptions(optnsObj);	
	//cop.SetCatalogIDInAppFramework(publicationID);

	////CA("localeId : "+ localeId);
	////CA("PubName : " + pubName);	
	////---------
	//PMString strID = optnsObj.getLocaleID();
	//int32 lang_ID = strID.GetAsNumber();
	//if(isOneSource)
	//{
	//	VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
	//	if(vectClsValuePtr == nil){
	//		ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
	//		return ;
	//	}
	//	VectorClassInfoValue::iterator itr;
	//	itr = vectClsValuePtr->begin();
	//	
	//	pubNewName = itr->getClassification_name();
	//}
	//else
	//{

	//	CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
	//								
	//	int32 pubRootID = pubModelRootValue.getRootID();
	//	
	//	CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
	//	PMString CatalogName = "";

	//	PMString TempBuffer = pubModelValue.getName();
	//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//	if(iConverter)
	//	{
	//		CatalogName=iConverter->translateString(TempBuffer);
	//	}
	//	else
	//		CatalogName = TempBuffer;

	//	pubName = CatalogName;
	//}

	//
	//IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
	//if(panelControlData==nil){
	//	//CAlert::InformationAlert("General panel control data nil");
	//	return;
	//}
	//IControlView* iControlView = panelControlData->FindWidget(kPublicationStaticWidgetID);
	//if(iControlView == nil)
	//{
	//	//CAlert::InformationAlert("Fail to find publication static text widget");
	//	return;
	//}	

	//InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	//if (textControlData == nil)
	//{
	//	//CAlert::InformationAlert("ITextControlData nil");	
	//	return;
	//}
	//if(isOneSource)
	//	textControlData->SetString(pubNewName);
	//else
	//	textControlData->SetString(pubName);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		//return -1;
	}

	ptrIAppFramework->setCatalogId(pubId);
	

}


PMString ClientOptions::getWhiteBoardMediaPath()
{
	//CA("ClientOptions::getWhiteBoardMediaPath");
	/*PMString WBMediaPath("");

	WBMediaPath = this->getImageDownloadPath();	
	if(WBMediaPath!="")
	{
		const char *imageP=  (WBMediaPath.GetUTF8String());
		if(imageP[std::strlen(imageP)-1]!='\\' || imageP[std::strlen(imageP)-1]!=':')
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
		WBMediaPath.Append("InDesign");
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
		WBMediaPath.Append("WhiteBoard");
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
	}

	return WBMediaPath;*/
	//return ("C:\\Apsiva");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return "";
	}
	return ptrIAppFramework->getAssetServerPath();
}

void ClientOptions::setSection(PMString& secName,int32& secId)
{
	//CA("ClientOptions::setSection");
	
	/*ClientOptionsManip cop;

	PMString secIdStr("");
	secIdStr.AppendNumber(secId);

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setSectionName(secName);
	optnsObj.setSectionID(secIdStr);

	cop.WriteClientOptions(optnsObj);*/

}
//-----17-09-09------
void ClientOptions::set_DisplayPartnerImages(int32 partnerImgs)
{
	//CA("ClientOptions::set_DisplayPartnerImages");
	/*ClientOptionsManip cop;

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDisplayPartnerImgs(partnerImgs);
	
	cop.WriteClientOptions(optnsObj);*/

}

void ClientOptions::set_DisplayPickListImages(int32 pickListImgs)
{
	//CA("ClientOptions::set_DisplayPickListImages");
	/*ClientOptionsManip cop;

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDisplayPickListImgs(pickListImgs);
	
	cop.WriteClientOptions(optnsObj);*/

}




//void ClientOptions::setAll(PMString& pubName, int32& pubId ,PMString& localeName,
//	int32& localeId ,PMString& sectionName, int32& sectionId)
//{
//	ClientOptionsManip cop;
//	OptionsValue optnsObj = cop.ReadClientOptions();
//
//	PMString publicationIdStr("");
//	publicationIdStr.AppendNumber(pubId);
//
//	PMString languageIdStr("");
//	languageIdStr.AppendNumber(localeId);
//
//	PMString sectionIdStr("");
//	sectionIdStr.AppendNumber(sectionId);
//
//
//	//CA("ClientOptions::setPublication");
//	
//	PMString pubNewName = "";
//	//pubNewName = pubName ;
//
//	bool16 isOneSource = kFalse;	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return;
//	}
//
//	if(ptrIAppFramework->get_isONEsourceMode())
//		isOneSource = kTrue;
//	else
//		isOneSource = kFalse;
//
//
//	//ClientOptionsManip cop;
//	int32 publicationID= cop.checkforPublicationExistance(pubName,pubId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setPubId(pubId);
//	PersistData::setPubName(pubName);
//
//	//int32 publicationID = PersistData::getPubId();
//
//	PMString pubIdStr("");
//	pubIdStr.AppendNumber(publicationID);
//
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	optnsObj.setPublicationName(pubName);
//	optnsObj.setProjectID(pubIdStr);
//
//	PMString text = "";
//	if(isOneSource)
//	{
//		text = "true";
//		optnsObj.setIsOneSource(text);
//	}
//	else
//	{
//		text = "false";
//		optnsObj.setIsOneSource(text);
//	}
//
//	//cop.WriteClientOptions(optnsObj);	
//	cop.SetCatalogIDInAppFramework(publicationID);
//
//	//CA("localeId : "+ localeId);
//	//CA("PubName : " + pubName);	
//	//---------
//	PMString strID = optnsObj.getLocaleID();
//	int32 lang_ID = strID.GetAsNumber();
//	if(isOneSource)
//	{
//		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
//		if(vectClsValuePtr == nil){
//			ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
//			return ;
//		}
//		VectorClassInfoValue::iterator itr;
//		itr = vectClsValuePtr->begin();
//		
//		pubNewName = itr->getClassification_name();
//	}
//	else
//	{
//
//		CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
//									
//		int32 pubRootID = pubModelRootValue.getRootID();
//		
//		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
//		PMString CatalogName = "";
//
//		PMString TempBuffer = pubModelValue.getName();
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(iConverter)
//		{
//			CatalogName=iConverter->translateString(TempBuffer);
//		}
//		else
//			CatalogName = TempBuffer;
//
//		pubName = CatalogName;
//	}
//
//	
//	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
//	if(panelControlData==nil){
//		//CAlert::InformationAlert("General panel control data nil");
//		return;
//	}
//	IControlView* iControlView = panelControlData->FindWidget(kPublicationStaticWidgetID);
//	if(iControlView == nil)
//	{
//		//CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
//	if (textControlData == nil)
//	{
//		//CAlert::InformationAlert("ITextControlData nil");	
//		return;
//	}
//	if(isOneSource)
//		textControlData->SetString(pubNewName);
//	else
//		textControlData->SetString(pubName);
//
//
//
//
//
//
//
//
//	//CA("ClientOptions::setLocale");
//
//	PersistData::setLocaleName(localeName);
//	
//	//ClientOptionsManip cop;
//	int32 LocaleID= cop.checkforLocaleExistance(localeName,localeId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setLocaleID(LocaleID);
//	
//	//int32 localeID = PersistData::getLocaleID();
//
//	//PMString languageIdStr("");
//	//languageIdStr.AppendNumber(localeId);
//	
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	//optnsObj.setDefaultLocale(localeName);
//	//optnsObj.setLocaleID(languageIdStr);
//	//cop.WriteClientOptions(optnsObj);
//
//	//CA("localeId : "+ localeId);
//
//	//IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
//	//if(panelControlData==nil){
//	//	//CAlert::InformationAlert("General panel control data nil");
//	//	return;
//	//}
//	IControlView* iControlView1 = panelControlData->FindWidget(kLocaleStaticWidgetID);
//	if(iControlView1 == nil)
//	{
//		CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData1(iControlView1,UseDefaultIID());
//	if (textControlData1 == nil)
//	{
//		CAlert::InformationAlert("ITextControlData1 nil");	
//		return;
//	}
//
//	textControlData1->SetString(localeName);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//	
//	//optnsObj.setPublicationName(pubName);
//	//optnsObj.setProjectID(publicationIdStr);
//	
//	optnsObj.setDefaultLocale(localeName);
//	optnsObj.setLocaleID(languageIdStr);
//	
//	optnsObj.setSectionName(sectionName);
//	optnsObj.setSectionID(sectionIdStr);
//
//	cop.WriteClientOptions(optnsObj);
//
//
//}




void ClientOptions::setAll(PMString& pubName,int32& pubId,PMString& localeName,int32& localeId,
						   PMString& secName,int32& secId){

//PersistData::setLocaleName(localeName);
//	
//	ClientOptionsManip cop;
//	int32 LocaleID= cop.checkforLocaleExistance(localeName,localeId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setLocaleID(LocaleID);
//	
//	int32 localeID = PersistData::getLocaleID();
//
//	PMString languageIdStr("");
//	languageIdStr.AppendNumber(localeId);
//	
//	OptionsValue optnsObj = cop.ReadClientOptions();
//	optnsObj.setDefaultLocale(localeName);
//	optnsObj.setLocaleID(languageIdStr);
//	//cop.WriteClientOptions(optnsObj);
//
//	//CA("localeId : "+ localeId);
//
//	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
//	if(panelControlData==nil){
//		//CAlert::InformationAlert("General panel control data nil");
//		return;
//	}
//	IControlView* iControlView = panelControlData->FindWidget(kLocaleStaticWidgetID);
//	if(iControlView == nil)
//	{
//		CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
//	if (textControlData == nil)
//	{
//		CAlert::InformationAlert("ITextControlData nil");	
//		return;
//	}
//
//	textControlData->SetString(localeName);
//
//	PMString pubNewName = "";
//	//pubNewName = pubName ;
//
//	bool16 isOneSource = kFalse;	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return;
//	}
//
//	if(ptrIAppFramework->get_isONEsourceMode())
//		isOneSource = kTrue;
//	else
//		isOneSource = kFalse;
//
//
//	//ClientOptionsManip cop;
//	int32 publicationID= cop.checkforPublicationExistance(pubName,pubId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setPubId(pubId);
//	PersistData::setPubName(pubName);
//
//	//int32 publicationID = PersistData::getPubId();
//
//	PMString pubIdStr("");
//	pubIdStr.AppendNumber(publicationID);
//
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	optnsObj.setPublicationName(pubName);
//	optnsObj.setProjectID(pubIdStr);
//
//	PMString text = "";
//	if(isOneSource)
//	{
//		text = "true";
//		optnsObj.setIsOneSource(text);
//	}
//	else
//	{
//		text = "false";
//		optnsObj.setIsOneSource(text);
//	}
//
//	//cop.WriteClientOptions(optnsObj);	
//	cop.SetCatalogIDInAppFramework(publicationID);
//
//	//CA("localeId : "+ localeId);
//	//CA("PubName : " + pubName);	
//	//---------
//	PMString strID = optnsObj.getLocaleID();
//	int32 lang_ID = strID.GetAsNumber();
//	if(isOneSource)
//	{
//		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
//		if(vectClsValuePtr == nil){
//			ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
//			return ;
//		}
//		VectorClassInfoValue::iterator itr;
//		itr = vectClsValuePtr->begin();
//		
//		pubNewName = itr->getClassification_name();
//	}
//	else
//	{
//
//		CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
//									
//		int32 pubRootID = pubModelRootValue.getRootID();
//		
//		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
//		PMString CatalogName = "";
//
//		PMString TempBuffer = pubModelValue.getName();
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(iConverter)
//		{
//			CatalogName=iConverter->translateString(TempBuffer);
//		}
//		else
//			CatalogName = TempBuffer;
//
//		pubName = CatalogName;
//	}
//
//	
//	IPanelControlData *panelControlData1 = PersistData::getGeneralPanelControlData();
//	if(panelControlData1==nil){
//		//CAlert::InformationAlert("General panel control data nil");
//		return;
//	}
//
//
//	IControlView* iControlView1 = panelControlData1->FindWidget(kPublicationStaticWidgetID);
//	if(iControlView1 == nil)
//	{
//		//CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData1(iControlView1,UseDefaultIID());
//	if (textControlData1 == nil)
//	{
//		//CAlert::InformationAlert("ITextControlData nil");	
//		return;
//	}
//	if(isOneSource)
//		textControlData1->SetString(pubNewName);
//	else
//		textControlData1->SetString(pubName);
//
//	//ClientOptionsManip cop;
//
//	PMString secIdStr("");
//	secIdStr.AppendNumber(secId);
//
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	optnsObj.setSectionName(secName);
//	optnsObj.setSectionID(secIdStr);
//
//	cop.WriteClientOptions(optnsObj);

}
