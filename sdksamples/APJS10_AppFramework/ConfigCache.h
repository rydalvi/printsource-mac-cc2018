#ifndef __CONFIGCACHE__
#define __CONFIGCACHE__


#include "AFWJSID.h"
#include "vector"
#include "map"
#include <set>
#include "list"
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include "IAppFramework.h"
#include "ConfigValue.h"

using namespace std;

class ConfigCache
{

private: 
		static ConfigCache* ccObjPtr;
		static bool ccInstanceFlag ;
		vector<ConfigValue> ccVectorObj;
		bool16 isConfigCacheLoaded;
		map< PMString, ConfigValue> configCacheMap;

		ConfigCache() 
		{
			// private constructor
		}
		
		

public:

	static ConfigCache* getInstance(); // SingleTone Implementation.
	void clearInstace();

	~ConfigCache() 
	{
		ccInstanceFlag = false;
		isConfigCacheLoaded =  kFalse;
	}

	bool16 loadConfigCache();
	void loadConfigCacheMap(ConfigValue & configValueObj);
	ConfigValue getConfigCacheByConfigName(PMString configName);	
	double getintConfigValue1ByConfigName(PMString configName);
	PMString getPMStringConfigValue1ByConfigName(PMString configName);

};

#endif