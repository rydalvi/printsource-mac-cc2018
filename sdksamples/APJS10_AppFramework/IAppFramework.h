#ifndef __IAppFramework__
#define __IAppFramework__


#include "AFWJSID.h"
#include "StructureCacheValue.h"
#include "EventCacheValue.h"
#include "ClassificationValue.h"
#include "PubModel.h"
#include "ElementModel.h"
#include "TypeModel.h"
#include "Attribute.h"
#include "LanguageModel.h"
#include "ConfigValue.h"
#include "PbObjectValue.h"
#include "ItemModel.h"
#include "ObjectValue.h"
#include "ItemTableValue.h"
#include "AdvanceTableScreenValue.h"
#include "GetSectionData.h"
#include "LoginInfoValue.h"
#include "PicklistGroup.h"
#include "PicklistField.h"
#include "ClientModel.h"
#include "PartnerCacheValue.h"
#include "IndexReference.h"
#include "PricingModule.h"
#include "WorkflowTask.h"
#include "FilterValue.h"
#include "AttributeGroup.h"

//#include "ServerInfoValue.h"
//#include "VendorInfoValue.h"
//#include "UserInfoValue.h"
//#include "CLanguageModel.h"
//#include "PublicationValue.h"
//#include "ObjectTableValue.h"
//

//
//
//#include "TypeGroupModel.h"
//#include "CAttributeModel.h"

//#include "AssetValue.h"
//#include "MilestoneModel.h"
//#include "ObjectMilestoneValue.h"
//#include "RoleInfoValue.h"
//#include "RoleModel.h"
//#include "UserModel.h"
//
		
//#include "ItemTableValue.h"
//#include "MedCustomTableScreenValue.h"
//#include "PossibleValueModel.h"
//#include "CMedProductGroup.h"
//#include "AP_pub_comment.h"
//#include "CStencilValue.h"
//#include "CPubCommentModel.h"
//#include "Value/PFElementValue.h"
//#include "Value/TypeValue.h"
//#include "Value/AttributeValue.h"
//#include "Value/ClassificationValue.h"
//#include "Value/ProductValue.h"
//#include "Value/PublicationValue.h"
//#include "Value/ObjectValue.h"
//#include "Value/PbObjectValue.h"
//#include "Value/ObjectTableValue.h"
//#include "Value/MilestoneValue.h"
//#include "Value/ObjectMilestoneValue.h"
////New Addition added by Yogesh on 22.2.05 at 7.11 pm
//#include "Value/RoleInfoValue.h"
//#include "Value/UserInfoValue.h"
//ended by Yogesh on 7.11 pm

//#include "AdvanceTableScreenValue.h"
//#include "GetSectionData.h"
//#include "PubObjectValue.h"
//
//#include "ProofModel.h"
//#include "ProofPagesModel.h"
//#include "ClientModel.h"
//
//#include "CMMYTable.h"
//#include "ProofingReviewValue.h"
//
//#include "ProofRevisionValue.h"
//
//#include "ManufacturerValue.h"
//#include "SupplierValue.h"
//#include "VersionUpdateValue.h"
//
//#include "OdTemplateValue.h"
//
//#include "OdClientStyleValue.h"
//#include "OdStyleRuleValue.h"
//#include "OdStyleTemplateValue.h"
//#include "OdStyleGroupValue.h"
//#include "OdStyleValue.h"
//#include "AttributeClientCodeModel.h"

#include "vector"
#include "map"
#include <set>
#include "list"

using namespace std;

typedef enum {Debug,Info,Error,Fatal} TypeOfError ;
typedef vector<CClassificationValue> VectorClassInfoValue, *VectorClassInfoPtr;
typedef vector<CPubModel> VectorPubModel, *VectorPubModelPtr;
typedef vector<CElementModel> VectorElementInfoValue, *VectorElementInfoPtr;
typedef vector<CTypeModel> VectorTypeInfoValue , *VectorTypeInfoPtr;
typedef vector<Attribute> VectorAttributeInfoValue, *VectorAttributeInfoPtr;
typedef vector<CLanguageModel>  VectorLanguageModel,*VectorLanguageModelPtr;
typedef vector<CPbObjectValue> VectorPbObjectValue, *VectorPbObjectValuePtr;
typedef vector<CAssetValue> VectorAssetValue, *VectorAssetValuePtr;
typedef vector<CObjectValue> VectorObjectInfoValue, *VectorObjectInfoPtr;
typedef vector<CItemModel> VectorItemModelValue, *VectorItemModelPtr;
typedef map<double,double> *ItemProductCountMapPtr;
typedef vector<CPbObjectValue> VectorPubObjectValue, *VectorPubObjectValuePtr;
typedef vector<CItemTableValue> VectorScreenTableInfoValue, *VectorScreenTableInfoPtr;
//typedef vector<CItemTableValue> VectorScreenItemTableInfoValue, *VectorScreenItemTableInfoPtr;
typedef vector<double> VectorLongIntValue, *VectorLongIntPtr;
typedef vector<CAdvanceTableScreenValue> VectorAdvanceTableScreenValue, *VectorAdvanceTableScreenValuePtr;

typedef list<PMString> PMStringList, *PMStringListPtr;

typedef map<double,Item> ItemMap, *ItemMapPtr;
typedef vector<PMString> PMStringVec, *PMStringVecPtr;
typedef vector<PMString> returnintvec, *returnintvecptr;//refresh content

typedef vector<double>ItemIDN;
typedef vector<double>AttributeIDN;
typedef vector<PMString>ValueN;

typedef multimap<AttributeIDN,ValueN> Attribute_value, *Attribute_valueptr ;//refresh content
typedef multimap<ItemIDN ,Attribute_valueptr> MapInMap, *MapInMapptr;
typedef vector<double>SectionIDlist,*SectionIDlistptr;

typedef vector<double>ElementID,*ElementIDptr;

typedef vector<PicklistGroup> VectorPicklistGroupInfoValue, *VectorPicklistGroupValuePtr;
typedef vector<LoginInfoValue> VectorLoginInfoValue, *VectorLoginInfoPtr;
typedef vector<CClientModel> VectorClientModel, *VectorClientModelPtr;
typedef vector<PricingModule> VectorPricingModule, *VectorPricingModulePtr;
typedef vector<WorkflowTask> VectorWorkflowTask, *VectorWorkflowTaskPtr;
typedef vector<AttributeGroup> VectorAttributeGroup, *VectorAttributeGroupPtr;



//typedef vector<int32> iitemidlist, *iitemidlistptr;
//typedef vector<int32> iitemgroupattributelist, *iitemgroupattributelistptr;
//typedef vector<int32> iitemgrouplist, *iitemgrouplistptr;
//typedef vector<int32> iitemattributelist, *iitemattributelistptr;
//typedef vector<int32> mixattrubuteids,*mixattrubuteidsptr;
//
//typedef vector<CServerInfoValue> VectorServerInfoValue, *VectorServerInfoPtr;
//typedef vector<CVendorInfoValue> VectorVendorInfoValue, *VectorVendorInfoPtr;
//typedef vector<CPublicationValue>  VectorPublicationValue, *VectorPublicationPtr;
//
//
//
//
//typedef vector<CObjectTableValue> VectorScreenTableInfoValue, *VectorScreenTableInfoPtr;
//
//
//
//typedef vector<CStencilValue> VectorODTemplateValue, *VectorODTemplateValuePtr;
//
//typedef vector<vector<CPbObjectValue>* > VectorPubObjectValuePointer,*VectorPubObjectValuePPtr;
//
//typedef vector<CPbObjectValue> VectorPubObjectMilestoneValue, *VectorPubObjectMilestoneValuePtr;
//typedef vector<vector<CPbObjectValue>* > VectorPubObjectMilestoneValuePointer,*VectorPubObjectMilestoneValuePPtr;
//typedef vector<CRoleInfoValue> VectorRoleInfoValue, *VectorRoleInfoValuePtr;
//typedef vector<CUserInfoValue> VectorUserInfoValue, *VectorUserInfoValuePtr;
//typedef vector<CMilestoneModel> VectorMilestoneModel, *VectorMilestonePtr;
//typedef vector<CRoleModel> VectorRoleModel , *VectorRoleModelPtr;
//typedef vector<CUserModel> VectorUserModel, *VectorUserModelPtr;
//typedef vector<CClassificationValue> VectorClassInfoValue, *VectorClassInfoPtr;
//
//

//
//
//typedef vector<CPossibleValueModel> VectorPossibleValueModel,*VectorPossibeValueModelPtr;
//typedef vector<CMedProductGroup> VectorMedProductGroupValue, *VectorProductGroupValuePtr;
//typedef enum {Debug,Info,Error,Fatal} TypeOfError ;
//typedef vector<APpubComment> VectorAPpubCommentValue, *VectorAPpubCommentValuePtr;
//
//typedef vector<CStencilValue> VectorCStencilValue, *VectorCStencilValuePtr;
//typedef vector<CPubObjectValue> VectorCPubObjectValue, *VectorCPubObjectValuePtr;
//typedef vector<ProofPagesModel> VectorProofPagesModel, *VectorProofPagesModelPtr;
//
//typedef vector<ProofingReviewValue> VectorProofingReviewValue, *VectorProofingReviewValuePtr;
//typedef vector<CManufacturerValue> VectorManufacturerValue, *VectorManufacturerValuePtr;
//typedef vector<CSupplierValue> VectorSupplierValue, *VectorSupplierValuePtr;
//
//typedef vector<PMString> * ProofStatusAction;
//typedef map<int32,int32> *ItemProductCountMapPtr;
//typedef set<int32> SetIntValue, *SetIntValuePtr;
//
//typedef vector<OdTemplateValue> OdTemplateValueVec, *pOdTemplateValueVec;
//typedef multimap<int32,OdTemplateValue> OdTemplateValueMap, * OdTemplateValueMapPtr;
//
//typedef vector<OdStyleTemplateValue> OdStyleTemplateValueVec, *OdStyleTemplateValueVecPtr;
//typedef vector<OdStyleValue> OdStyleValueVec, *OdStyleValueVecPtr;
//typedef list<PMString> PMStringList, *PMStringListPtr;
//
//typedef map<int32,Item> ItemMap, *ItemMapPtr;
//typedef vector<PMString> PMStringVec, *PMStringVecPtr;
//typedef vector<PMString> returnintvec, *returnintvecptr;//refresh content
//
//typedef vector<int32>ItemIDN;
//typedef vector<int32>AttributeIDN;
//typedef vector<PMString>ValueN;
//
//typedef multimap<AttributeIDN,ValueN> Attribute_value, *Attribute_valueptr ;//refresh content
//typedef multimap<ItemIDN ,Attribute_valueptr> MapInMap, *MapInMapptr;
//typedef vector<int32>SectionIDlist,*SectionIDlistptr;
//
//typedef vector<int32>ElementID,*ElementIDptr;
//
//typedef map<int32,AttributeClientCodeModel> CroosClientAttributeMap, *CroosClientAttributeMapPtr;


class IAppFramework : public IPMUnknown
{
public:

	enum { kDefaultIID = IID_IAPPFRAMEWORK };


	virtual bool16 callService(PMString url, PMString &returndata)=0; 
	virtual bool16 callLogin(PMString ServerURL, PMString username, PMString Password,/* PMString apikey,*/ PMString clientNo, PMString &sessionId)=0;
	virtual bool16 getLoginStatus()=0;
	virtual bool16 callLogout(PMString &logoutResult)=0;
	virtual bool16 callStructureJson(StructureCacheValue &returndata)=0;
	virtual bool16 callAllTreeJson( EventCacheValue &returndata)=0;
	virtual bool16 callConfigJson(vector<ConfigValue> &returndata)=0;
	virtual bool16 callSectionResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata, PMString itemAttributeId="")=0;
	virtual bool16 callSectionDetailResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds,  bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds = "")=0;
	virtual bool16 callItemGroupDetailJson(double itemGroupId, double sectionId, double langId, CObjectValue &returndata ,  PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds )=0;
	virtual bool16 callItemDetailJson(double itemId, double sectionId, double langId, CItemModel &returndata, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds)=0;
	virtual bool16 callPartnerJson(PartnerCacheValue * partnerCacheValuePtr)=0;

	virtual void LogDebug(PMString err_msg) = 0;
    virtual void LogInfo(PMString err_msg) = 0;
    virtual void LogError(PMString err_msg) = 0;
    virtual void LogFatal(PMString err_msg) = 0;			
	virtual void Log(TypeOfError etype,PMString errorString) = 0; //Cs4
	virtual bool16 GetFileStatus() = 0;
    virtual void SetFileStatus(bool16 status) = 0;
    virtual PMString GetFilePath() = 0;
    virtual void SetFilePath() = 0;
    virtual PMString GetTimeStamp() = 0;
	virtual void getUnixPath(PMString& thisstring) = 0;
	virtual bool IsWinPath(PMString thisstring) = 0;
	virtual void setLoggerFileFlag(int32 setflag) = 0;
 
	virtual VectorClassInfoPtr ClassificationTree_getRoot(double LanguageID)=0;
	virtual VectorClassInfoPtr ClassificationTree_getAllChildren(double ClassID, double LanguageID)=0;
	virtual VectorPubModelPtr ProjectCache_getAllProjects(double)=0;
	//virtual int32 ProjectCache_getChildCount(int32 PubId, int32 LanguageID)=0;
	virtual VectorPubModelPtr ProjectCache_getAllChildren(double PubId, double LanguageID)=0;

	
	virtual bool16 get_isONEsourceMode()=0;
	virtual void set_isONEsourceMode(bool16)=0;
	virtual VectorElementInfoPtr StructureCache_getItemGroupCopyAttributesByLanguageId(double languageId)=0;
	virtual VectorTypeInfoPtr StructureCache_getItemGroupImageAttributes()=0;
	virtual bool16 StructureCache_isElementMPV(double elementID)=0;
	virtual VectorTypeInfoPtr StructureCache_getListTableTypes()=0;
	virtual VectorAttributeInfoPtr StructureCache_getItemAttributesForClassAndParents(double ClassID , double LanguageID)=0;
	virtual VectorTypeInfoPtr StructureCache_getItemImagesForClassAndParents(double ClassID)=0;
	virtual VectorElementInfoPtr StructureCache_getSectionCopyAttributesByLanguageId(double languageId)=0;
	virtual VectorTypeInfoPtr StructureCache_getSectionImageAttributes()=0;
	virtual VectorLanguageModelPtr StructureCache_getAllLanguages() = 0;
	virtual void setCatalogId(double catalog_id)=0;
	virtual void setLocaleId(double locale_id)=0;
	virtual double getCatalogId()=0;
	virtual double getLocaleId()=0;
	virtual void StructureCache_clearInstance()=0;
	virtual void EventCache_clearInstance()=0;
	virtual bool16 CONFIGCACHE_getPVImageFileName(int32 index) = 0;

	virtual bool16 ConfigCache_getDisplayItemComponents()=0;
	virtual bool16 ConfigCache_getDisplayItemXref()=0;
	virtual bool16 ConfigCache_getDisplayItemAccessory()=0;
	virtual int32 getPM_Project_Levels() = 0 ;

	virtual	CPubModel getpubModelByPubID(double PubID,double languageID) = 0;
	virtual CClassificationValue StructureCache_getCClassificationValueByClassId(double classId, double LanguageID) = 0;
	virtual VectorPbObjectValuePtr getProductsAndItemsForSection(double curSecId, double langId , PMString itemAttributeId ="" ,bool16 isChildDataRequire = kFalse)=0;
	virtual VectorPbObjectValuePtr getProductsAndItemsForSectionInDetail(double curSecId, double langId , PMString itemGroupIds, PMString itemIds ,  PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds, bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds ="")=0;

	virtual double ConfigCache_getintConfigValue1ByConfigName(PMString configName) =0;
	virtual PMString ConfigCache_getPMStringConfigValue1ByConfigName(PMString configName) =0;
		
	virtual VectorPubObjectValuePtr getItemsForSubSection(double subSecID, double LanguageID) =0;
	virtual VectorPubObjectValuePtr getProductsForSubSection(double subSecID, double LanguageID) =0;

	virtual bool16 EventCache_setCurrentSectionData( double sectionId, double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds, bool16 isCategorySpecificResultCall,  bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds="")=0;
	virtual void EventCache_clearCurrentSectionData()=0;

	virtual void EventCache_setCurrentObjectData(double sectionId, double ObjectId, int32 isProduct, double langId, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds) =0;
	virtual void EventCache_clearCurrentObjectData()=0;

	virtual VectorScreenTableInfoPtr GETProjectProduct_getAllScreenTablesBySectionidObjectid(double section_id, double object_id, double langId, bool16 isCustomTable = kFalse)=0;

	virtual void clearAllStaticObjects()=0;
	virtual PMString GETItem_GetItemAttributeDetailsByLanguageId(double ItemID,double AttributeID,double LanguageID, double sectionId, bool16 RefreshFlag)=0;
	virtual CObjectValue GETProduct_getObjectElementValue(double objectID, double sectionId, double LanguageId =1) =0;
	virtual PMString GETProduct_getAttributeValueByLanguageID(double ObjectID,double AttributeID,double LanguageID, double sectionId, bool16 IsFlag)=0;

	virtual VectorScreenTableInfoPtr GETProjectProduct_getItemTablesByPubObjectId(double itemId, double sectionId, double langId) =0;

	virtual double ConfigCache_getPubItemSalePriceTableType()=0;
	virtual double ConfigCache_getPubItemRegularPriceTableType()=0;

	virtual PMString ATTRIBUTECache_getItemAttributeName(double AttributeID,double languageID) =0;
	virtual bool16 ElementCache_GetElementModelByElementID(double elementID,CElementModel & cElementModelObj, double languageId = 1) = 0;

	virtual bool16 ConfigCache_getSprayTableStructurePerItem() = 0;
	virtual VectorLongIntPtr GETProjectProduct_getAllItemIDsFromTables(double ObjectID, double sectionId) =0;

	virtual PMString PUBModel_getAttributeValueByLanguageID(double PubID,double AttributeID,double LanguageID,bool16 Isflag)=0;

	virtual VectorAssetValuePtr GETAssets_GetAssetByParentAndType(double objectId, double sectionId, int32 isProduct, double typeId = -1) = 0; 

	virtual CPbObjectValue getPbObjectValueBySectionIdObjectId(double sectionId, double ObjectId, int32 isProduct, double langId) =0;  // ObjectId can be ItemGroupId or ItemId

	virtual PMString StructureCache_getListNameByTypeId(double typeId)=0;

	virtual PMString StructureCache_TYPECACHE_getTypeNameById(double typeId)=0;
	
	virtual double CONFIGCACHE_getElementIDForItemGroupDescription() = 0;

	virtual double CONFIGCACHE_getItemDescriptionAttribute() = 0;

	virtual VectorLoginInfoPtr getLoginInfoProperties() = 0;

	virtual PMString getAssetServerPath()=0;
	virtual void setAssetServerPath(PMString str)=0;

	virtual void clearConfigCache()=0;

	virtual VectorAssetValuePtr GETAssets_GetPVMPVAssetByParentIdAndAttributeId(double ParentId,double AttributeId, double sectionId, double LangaugeId, int32 objectType, double pvGroupId, int32 PVImageindex)=0;

	virtual bool16 LOGINMngr_updateOrCreateNewServerProperties(LoginInfoValue oServerInfo, int updateflag, double clientID)=0;
	virtual bool16 LOGINMngr_deleteUser(char* envName, double ClientId)=0;
	virtual void LOGINMngr_setServerInfoValue(LoginInfoValue ServerConfigValue) = 0;

	virtual bool16 callLoginFirstStep(PMString serverURL, PMString username, PMString password, vector<CClientModel> * vectClientModels , PMString & errorMsg)=0;
	virtual bool16 LOGINMngr_logoutCurrentUser()=0;

	virtual void setClientID(double clientid) = 0;
	virtual double getClientID() = 0;

	virtual int32 getAssetServerOption()=0;
	virtual void setAssetServerOption(int32 Option)=0;

	virtual void setMissingFlag(bool16 status)=0;
	virtual	bool16 getMissingFlag()=0;

	virtual void setSelectedEnvName(PMString name)=0;
	virtual	PMString getSelectedEnvName()=0;

	virtual CAssetValue getPartnerAssetValueByPartnerIdImageTypeId(double objectId, double sectionId, double languageId, int32 imageTypeId, int32 whichTab)=0;

	virtual bool16 getIndexTerms(set<PMString> resultIds, vector<IndexReference> &returndata , PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes) = 0;
	virtual bool16 callIndexTermJson(set<PMString> resultIds, vector<IndexReference> &returndata , PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes) = 0;

	virtual Attribute ATTRIBUTECache_getItemAttributeForIndex(int32 option, double languageID) = 0;
	
	virtual bool16 callCategorySpecificResultJson( double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds)=0;

	virtual VectorPbObjectValuePtr getProductsAndItemsForCategorySpecificResult( double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds )=0;

	virtual bool16 getFieldSwapValues(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId) = 0;
	virtual bool16 callFieldSwapValuesJson(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId) = 0;
	virtual bool16 getRefreshData(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIds, PMString itemGroupListIds ="", PMString itemListIds ="") = 0;
	virtual bool16 callRefreshDataJson(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIds, PMString itemGroupListIds="", PMString itemListIds="") = 0;

	virtual VectorAttributeInfoPtr StructureCache_getAllItemAttributes(double LanguageID)=0;

	virtual PicklistGroup StructureCache_getPickListGroups(double pickListGroupId)=0;
	virtual double getSectionIdByLevelAndEventId(int32 sectionLevel, double sectionId, double LangId)=0;

	virtual double  getDollerOff(double regularPrice, double eventPrice) =0;
	virtual double  getPercentageOff(double regularPrice, double eventPrice)=0;

	virtual bool16 StructureCache_isAttributeMPV(double attributeId)=0;

	virtual bool16 callReverseUpdateJson(int32 index, double objectId, double fieldId, double langId, double sectionId, PMString value)=0;

	virtual double StructureCache_getPickListGroupIdByElementMPVId(double elementID)=0;
	virtual double StructureCache_getPickListGroupIdByAttributeMPVId(double attributeId)=0;
	virtual Attribute StructureCache_getItemAttributeModelByAttributeId(double AttributeId)=0;

	virtual VectorAdvanceTableScreenValuePtr getHybridTableData(double section_id , double object_id , double language_id, int32 isProduct, bool16 isSectionLevelHybridTable=0) = 0;

	virtual bool16 CONFIGCACHE_getShowCustomerInIndesign() = 0;
	virtual double CONFIGCACHE_getCustomerPVTypeId(int32 isCustomer) = 0;
	virtual PMString CONFIGCACHE_getCustomerAssetTypes(int32 isCustomer) = 0;
	virtual double getSelectedCustPVID(int32 isCustomer) = 0;
	virtual void setSelectedCustPVID(double pvID,int32 isCustomer) = 0;
    
    virtual bool16 apiCallService(PMString url, PMString &returndata)=0;
    virtual bool16 callFilterService(vector<FilterValue> &returndata)=0;
    virtual bool16 callFilterResultService(PMString filterId, int32 limit, int32 offset, vector<CItemModel> &returndata)=0;
    virtual bool16 callFilterResultCountService(PMString filterId, int32 &returndata)=0;

	virtual bool16 callGetPricingModule(vector<PricingModule> &returndata)=0;
    virtual bool16 callGetWorkflowTasks(vector<WorkflowTask> &returndata)=0;
    
    virtual bool16 callGetAttributeGroups(  vector<AttributeGroup> &returndata)=0;
    virtual VectorAttributeGroupPtr StructureCache_getItemAttributeGroups()=0;
    virtual vector<double> StructureCache_getItemAttributeListforAttributeGroupId(double attributeGroupId, bool16 isRefreshCall= kFalse)=0;
    virtual vector<double> StructureCache_getItemAttributeListforAttributeGroupKey(PMString &attributeGroupKey, bool16 isRefreshCall= kFalse)=0;
    virtual bool16 callGetAttributesByAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, AttributeGroup &returndata)=0;
    virtual bool16 callGetAttributesByItemIdAndAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, double itemId,vector<double> &returndata)=0;
    
    virtual vector<double> getItemAttributeListforAttributeGroupIdAndItemId(double sectionId, double itemId, double attributeGroupId, double languageId)=0;
    virtual vector<double> StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(PMString &attributeGroupKey, double itemId)=0;
    
    virtual VectorAssetValuePtr GETAssets_GetAssetByParentAndAssetId(double objectId, double sectionId, int32 isProduct, double assetId)=0;
    
    virtual PMString StructureCache_getItemAttributeGroupNameByAttributeGroupId(double attributeGroupId)=0;
    virtual bool16 callGetAttributesV3(vector<Attribute> &itemFields)=0;
    

//	virtual bool16 SetURL()=0;
//	virtual bool16 SetLocalMode()=0;
//	virtual int UTILITYMngr_dispErrorMessage(void)=0;
//	virtual void setCatalog_ID(int32 catalog_id)=0;
//	virtual void setLocale_ID(int32 locale_id)=0;
//	virtual int32 getCatalog_ID()=0;
//	virtual int32 getLocale_ID()=0;
//	virtual void setSelectedConnMode(int mode)=0;
//	virtual int getSelectedConnMode()=0;
//	virtual bool16 LOGINMngr_getLoginStatus()=0;
//	virtual bool16 LOGINMngr_isValidServerURL(PMString)=0;
//	virtual bool16 LOGINMngr_isValidServer(PMString serverName, int port)=0;
//
//	virtual VectorServerInfoPtr LOGINMngr_getAvailableServers(const char* fullPath,const char* fileName, int32 ClientID)=0; //Cs4 /*ConstCString*/
//	virtual bool16 LOGINMngr_updateOrCreateNewServerProperties(CServerInfoValue oServerInfo, int updateflag, char* fullPath, char* fileName,int32 clientID)=0;
//	virtual bool16 LOGINMngr_deleteUser(char* envName, char* fullPath, char* fileName, int32 ClientId)=0;
//	virtual bool16 LOGINMngr_loginUser(PMString szUserName,PMString szUserPassword,PMString clientName,VectorClientModelPtr &vectorClientModelPtr)=0;
//	virtual CUserInfoValue LOGINMngr_getUserProfile()=0;
//	virtual bool16 LOGINMngr_logoutCurrentUser()=0;
//	virtual void LOGINMngr_setLoginStatus(bool16)=0;
//	virtual VectorVendorInfoPtr LOGINMngr_getVendorVersions(void)=0;
//	virtual bool16 LOGINMngr_setDBServer(CServerInfoValue serverValue, char* fullPath, char* fileName)=0;
//
//	virtual int isDirectory(const char* path)=0; //CS4*******
//	// added by vaibhav 24 Jan
//	virtual VectorPubModelPtr PUBMgr_getAllProjects(int32)=0;
//	virtual VectorLanguageModelPtr LNGCache_getAllLanguages() = 0;
//	virtual PMString SPCLCharMngr_getSpecialCharByCode(char* code) = 0 ;
//	virtual bool16 TYPECACHE_clearInstance(void)= 0;
//	virtual VectorElementInfoPtr ElementCache_getCopyAttributesByIndex(int32 index,int32 LanguageID)= 0;  // index = 1 : PF copy attr, index = 2:PG copy attr, index = 3:PR copy attr.
//	virtual VectorTypeInfoPtr ElementCache_getImageAttributesByIndex(int32 index)=0; // index = 1 : PF Image attr, index = 2:PG Image attr, index = 3:PR Image attr.
//	virtual VectorTypeInfoPtr ElementCache_getProductTableTypes()=0;
//	virtual VectorElementInfoPtr ElementCache_getProjectSecSubSecAttributesByIndex(int32 index, int32 Language_ID)=0;  // index = 1: Project attributes, index = 2: Section Attributes, index= 3: SubSection Attributes.
//	virtual VectorTypeInfoPtr AttributeCache_getItemImages()=0; 
//	virtual VectorAttributeInfoPtr AttributeCache_getItemAttributes(int32 LanguageID)=0;
//	virtual bool16 ELEMENTCACHE_clearInstance(void)= 0;
//	virtual bool16 ATTRIBUTECACHE_clearInstance(void)= 0;	
//	virtual	VectorScreenTableInfoPtr GETProjectProduct_getAllScreenTablesBySectionidObjectid(int32 section_id, int32 object_id, bool16 isCustomTable = kFalse)=0;
//	virtual int32 getPM_Project_Levels() = 0 ;
//	virtual int32 getPM_Product_Levels() =0;
//	//virtual CPubModel* PROJECTCache_getPublicationValueByID(int32 PubID) = 0;
//	virtual VectorAssetValuePtr GETAssets_GetAssetByParentAndType(int32 ObjectId,int32 ObjectTypeId, int32 typeId) = 0; 
//	virtual int32 StaticAttributeIDs_CheckPublicationID(int32 AttributeID)=0;
//	virtual bool16 GETAsset_downLoadItemAsset(int32 assetID,PMString targetLocalPath)=0;
//	virtual bool16 GETAsset_downLoadProductAsset(int32 assetID,PMString targetLocalPath)=0;
//	virtual CObjectValue GETProduct_getObjectElementValue(int32 objectID, int32 LanguageId =1 )=0;
//	virtual PMString GETProduct_getAttributeValueByLanguageID(int32 ObjectID,int32 AttributeID,int32 LanguageID,bool16 IsFlag)=0;
//	virtual PMString PUBModel_getAttributeValueByLanguageID(int32 PubID,int32 AttributeID,int32 LanguageID,bool16 Isflag)=0;
//	virtual PMString ATTRIBUTECache_getItemAttributeName(int32 AttributeID,int32 languageID)=0;
//	virtual VectorLongIntPtr GETProduct_getAllItemIDsFromTables(int32 ObjectID)=0;
//	virtual VectorPubModelPtr getAllSectionsByPubIdAndLanguageId(int32 publicationId , int32 languageId) = 0 ; 
//	virtual VectorPubModelPtr getAllSubsectionsBySectionIdAndLanguageId(int32 sectionId ,  int32 languageId) = 0 ;
//	virtual int32 TYPEMngr_getObjectTypeID(char* typeStr) = 0 ;
//	virtual VectorPubObjectValuePPtr PBObjMngr_getProductsForSubSection(int32, int32)=0;
//	virtual int32 getNameNoPosition(const char * ptrstr)=0;
//	
//	virtual CMilestoneModel *MilestoneCache_getDesignerMilestone() = 0 ;
//	virtual CObjectMilestoneValue *findCObjectMilestoneByObjectIdMilestoneId(int32 ObjectId,int32 MilestoneId)=0;
//	virtual	bool16 insertObjectMilestone(int32 ObjectId,int32 MilestoneId) = 0 ;	
//	virtual	bool16 updateObjectMilestone(CObjectMilestoneValue val) = 0 ;
//	virtual	PMString GetStaticFieldStringFromInterface(char* InterfaceClassPath, char* FieldName)=0;
//	virtual	int32 TYPECACHE_getTypeByCode(char* typeStr)=0;
//	virtual PMString GETItem_GetItemAttributeDetailsByLanguageId(int32 ItemID,int32 AttributeID,int32 LanguageID, bool16 RefreshFlag)=0;
//	virtual bool16 GETProduct_setObjectElementValue(int32 ObjectId,int32 elementId,PMString EntierStory, int32 LanguageID, int32 SectionID)=0;
//	virtual VectorPubObjectMilestoneValuePPtr GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(int32 subsecId , char * mstoneIds , bool8 flag) = 0 ;
//	virtual VectorPubObjectMilestoneValuePPtr GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(int32 subsecId,char * mstoneIds , bool8 flag , int32 roleID , int32 userID) = 0 ;
//	virtual VectorRoleModelPtr RoleCache_getAllRoles() = 0 ;
//	virtual VectorUserInfoValuePtr UserCache_getAllUsersByRole(int32 roleId)=0;
//	virtual VectorMilestonePtr MilestoneCache_getMilestonesByRetrieveAccess(int32 roleId) = 0 ;
//	virtual VectorUserModelPtr UserCache_getAllUsersByRoleId(int32 roleId) = 0;
//	virtual VectorPubObjectMilestoneValuePPtr GetProjectProductAction_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(int32 subsecId,char* mstoneIds,bool8 new_product,bool8 add_to_spread,bool8 update_spread,bool8 update_copy,bool8 update_art,bool8 update_item_tables,bool8 delete_from_spread) = 0 ;
//	virtual bool16 MILESTONECACHE_getRoleMilestoneWriteAccess(int32 roleId , int32 milestoneId) = 0 ;
//	virtual bool16 IsProductDeleted(int32 SectionID, int32 ProductID)=0;
//	virtual bool16 CONFIGCACHE_getPM_DisplayWorkflow() = 0 ;
//	virtual void UTILITYMngr_getPreviewURL(bool16 IsOneSource ,int32 IsProduct, int32 ObjectId,int32 Pb_Object_Id)= 0 ;
//	virtual PMString UTILITYMngr_getConfig_ServerPRINTHelpURL()=0;
//	virtual bool16 ElementCache_isElementMPV(int32 elementID)=0;
//	virtual CItemModel GETItem_GetItemAttributeDetailsByLanguageId(int32 itemID,int32 languageID)=0;
//	virtual PMString GETProduct_getObjectAndObjectValuesByObjectId(int32 objectID)=0;
//	virtual bool16 LANGUAGECACHE_clearInstance(void)=0;
//	virtual bool16 PROJECTCACHE_clearInstance(void)=0;
//	virtual PMString ClientActionGetAssets_getItemAssetFolder(int32 assetId)= 0;
//	virtual PMString ClientActionGetAssets_getProductAssetFolder(int32 assetId)= 0;
//	virtual bool16 POSSIBLEVALUECACHE_clearInstance(void)=0;
//	virtual PMString SPCLCharMngr_getSpclCharacterByIndesignCode(char* code) = 0;
//	virtual bool16 ElementCache_GetElementModelByElementID(int32 elementID,CElementModel & cElementModelObj, int32 languageId = 1) = 0;
//	virtual VectorClassInfoPtr ClassificationTree_getRoot(int32 LanguageID)=0;
//	virtual VectorClassInfoPtr ClassificationTree_getAllChildren(int32 ClassID, int32 LanguageID)=0;
//
///////////////////////////// Added By Yatin ////////////////////////////////////////////
//	virtual VectorPubModelPtr ProjectCache_getAllProjects(int32)=0;
//	virtual int32 ProjectCache_getChildCount(int32 PubId, int32 LanguageID)=0;
//	virtual VectorPubModelPtr ProjectCache_getAllChildren(int32 PubId, int32 LanguageID)=0;
//	virtual VectorPubObjectValuePtr GetProjectProducts_getProductsBySubSection(int32 subSecID, int32 LanguageID)= 0;
//	virtual VectorPubObjectValuePtr GetProjectProducts_getItemsForSubSection(int32 subSecID, int32 LanguageID)= 0;
//	virtual VectorPubObjectValuePtr GetProjectProducts_getProductsAndItemsForSubSection(int32 subSecID, int32 LanguageID)= 0;
//
//	/////////////////added on 1Aug///////////////////////////////////////////////////
//	virtual VectorTypeInfoPtr AttributeCache_getItemTableTypes()=0;
//	virtual VectorScreenItemTableInfoPtr GETProjectProduct_getItemTablesByPubObjectId(int32 object_id)=0;
//
//////////////////////////// Added By Vijay on 16/8/2006/////////////////////////////////
//	virtual VectorAssetValuePtr GETAssets_GetAssetByParentAndType(int32 parentId,int32 parentTypeId) = 0; 
//	virtual VectorAssetValuePtr	GETAssets_GetAssetByParentAndTypeAndMVPId(int32 ObjectId,int32 ObjectTypeId, int32 typeId,int32 MPVId)=0;
//
//	virtual PMString TYPECACHE_getTypeNameById(int32 typeId)=0;
//	virtual VectorLongIntPtr GETProjectProduct_getAllItemIDsFromTables(int32 ObjectID, int32 SectionID)=0;
//	virtual VectorAttributeInfoPtr AttributeCache_getItemAttributesForClassAndParents(int32 ClassID , int32 LanguageID)=0;
//	virtual VectorTypeInfoPtr AttributeCache_getItemImagesForClassAndParents(int32 ClassID)=0;
//
//	//added on 22Sept..EventPrice addition.
//	virtual	int32 getPubItemPriceTableType() = 0;
//	virtual PMString getPriceAttributePrefix() = 0;
//	virtual int32 getRegularPriceAttributeId() = 0;
//	virtual PMString getPriceSuffix(int32 attributeID) = 0;
//	//ended on 22Sept..EventPrice addition.
//
//	virtual bool16 get_isONEsourceMode()=0;
//	virtual void set_isONEsourceMode(bool16)=0;
//	//Added on 26/09
//	virtual VectorObjectInfoPtr GetONEsourceObjects_getONEsourceProductsByClassId(int32 classId,int32 LanguageID)=0;
//	virtual VectorItemModelPtr GetONEsourceObjects_getONEsourceItemsByClassId(int32 classId,int32 LanguageID)=0;
//	
//	//added on 29Sept..EventPrice addition.
//	virtual int32 getPercentageDifference(double regularPrice,double eventPrice)=0;
//	virtual int32 getDollerDifference(double regularPrice ,double eventPrice)=0;
//	//ended on 29Sept..EventPrice addition.
//
//	//Added on 9/10/06 For ONEsource(Master).
//	virtual VectorScreenTableInfoPtr GetProduct_getObjectTableByObjectId(int32 object_id)=0;
//	virtual VectorElementInfoPtr getCategoryCopyAttributesForPrint(int32 Language_ID)=0;
//	virtual int32 getTreeLevelsIncludingMaster()=0;
//
//	//Added on 10/10/06 For ONEsource(Master).
//	virtual PMString getCategoryCopyAttributeValue(int32 objectId,int32 levelNo, int32 attrId,int32 LanguageID, int32 isProduct)=0;
//	virtual CMedCustomTableScreenValue* GetONEsourceObjects_getMed_CustomTableScreenValue(int32 ObjectId)=0;
//	//Added on 3/11/06 By Dattatray For ONEsource 
//	virtual bool16 createZipFile(vector <PMString> files,PMString path,PMString outputFile)=0;
//
//	virtual VectorScreenTableInfoPtr GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(int32 item_id, int32 Language_id, bool16 isKitTable)=0;
//
//	//20Nov..
//	virtual	VectorPossibeValueModelPtr GETProduct_getMPVAttributeValueByLanguageID(int32 ObjectID,int32 AttributeID,int32 LanguageID,bool16 IsFlag)=0;
//
//	virtual VectorTypeInfoPtr TypeCache_getEventPriceTypeList()=0;
//
//	//18/12/06
//	virtual int32 getEventPriceAttributeId(int32 typeId) = 0;
//	//Added on 19/12/2006 By Dattatray
//	virtual bool16 ConfigCache_getDisplayItemKits()=0;
//	virtual bool16 ConfigCache_getDisplayItemComponents()=0;
//	virtual bool16 ConfigCache_getDisplayItemCustomTable()=0;
//	virtual bool16 ConfigCache_getSprayTableStructurePerItem()=0;
//	//End on 19/12/2006
//	virtual void GETCommon_refreshClientCache()=0;
//	//Added on 10/01/07
//	virtual int32 ConfigCache_getProductGroupAttributeId()=0;
//	//Added on 18/01/07 
//	virtual VectorProductGroupValuePtr GetONEsourceObjects_getProductGroupList(vector<int32> objectIds)=0;
//	//Added on 19/01/07
//	virtual bool16 GetItem_updateItemAttributeValue(int32 itemId,int32 attributeId,PMString value)=0;
//	//following methods are added for loggerfile on 2-2-2007 =====from here
//	virtual void LogDebug(PMString err_msg) = 0;
//    virtual void LogInfo(PMString err_msg) = 0;
//    virtual void LogError(PMString err_msg) = 0;
//    virtual void LogFatal(PMString err_msg) = 0;			
//	virtual void Log(TypeOfError etype,const char* errorString) = 0; //Cs4
//	virtual bool16 GetFileStatus() = 0;
//    virtual void SetFileStatus(bool16 status) = 0;
//    virtual char* GetFilePath() = 0;
//    virtual void SetFilePath() = 0;
//    virtual PMString GetTimeStamp() = 0;
//	virtual void getUnixPath(PMString& thisstring) = 0;
//	virtual bool IsWinPath(PMString thisstring) = 0;
//	virtual void setLoggerFileFlag(int32 setflag) = 0;
//	//=============================================2-2-2007========upto here
//	
//	//13/01/07 event price new addition 
//	virtual int32 ConfigCache_getPubItemSalePriceTableType() = 0;
//	virtual int32 ConfigCache_getPubItemRegularPriceTableType() = 0;
//
//	virtual PMString GETItem_GetIndividualItemAttributeDetailsByLanguageId(int32 ItemID,int32 AttributeID,int32 LanguageID)=0;
//	virtual int32 getAssetServerOption()=0;
//	virtual void setAssetServerOption(int32 Option)=0;
//	
//	virtual bool16 isJDBCEnable() = 0;
//	
//	//8-march
//	virtual PMString getLoginErrorString()=0;
//	virtual void setLoginErrorString(PMString lStatus)=0;
//	virtual void appendLoginErrorString(PMString lStatus)=0;
//	//8-march
//
//	virtual void setMissingFlag(bool16 status)=0;
//	virtual	bool16 getMissingFlag()=0;
//
//	virtual bool16 ClientActionGetAssets_setUploadFiletoDatabase(char* FileName, char* LocalFilePath, char * subDirectory)=0;
//
//	virtual bool16 loadAllCaches()=0;
//
//	virtual bool16 AttributeCache_isAttributeMPV(int32 elementID)= 0;
//	virtual void UpdateStarFlagforPbObject(int32 sectionId, int32 ObjectId, bool flag)=0;
//
//	virtual bool16 ConfigCache_getDisplayItemXref()=0;
//	virtual bool16 ConfigCache_getDisplayItemAccessory()=0;
//
//	virtual void UpdateNewFlagforPbObject(int32 sectionId, int32 objectId, bool flag)=0;
//	virtual CPbObjectValue* GetProjectProducts_findByObjectIdPubId(int32 subSecID, int32 objectID )=0;
//
//	virtual VectorScreenTableInfoPtr GETItem_getXRefScreenTableByItemIdLanguageId(int32 item_id, int32 Language_id )=0;
//	virtual VectorScreenTableInfoPtr GETItem_getAccessoryScreenTableByItemIdLanguageId(int32 item_id, int32 Language_id )=0;
//
//	virtual PMString GETItem_GetItemXrefAttributeDetailsByLanguageId(int32 XrefItemID,int32 AttributeID,int32 LanguageID)=0;
//	
//	//From here Added
//			//8-march
//	//5-april
//	virtual VectorTypeInfoPtr TypeCache_getTypesByGroupCode(PMString groupCode)=0;
//	//5-april
////vijay 10/4/07
//	virtual	int32 getStageBySectionId(int32 sectionId) =0;
//	virtual	VectorAPpubCommentValuePtr getPubCommentsBySectionId(int32 sectionId)=0;
////18-april
//	virtual PMString getCatalogPlanningTemplateFolderPath()=0;
////18-april
//
////	virtual bool16 ClientActionGetAssets_setUploadFiletoDatabase(char* FileName, char* LocalFilePath, char * subDirectory)=0;
//
////14-may chetan
//	virtual bool16 GetProjectProducts_updateWhiteBoardStageIdForSection(int32 sectionId, int32 whiteboardStageId)=0;
////14-may
//	//Up To Here..
//
//	virtual VectorAdvanceTableScreenValuePtr getHybridTableData(int32 sectionId , int32 ProductOrItemId , int32 LanguageId , bool16 isSectionLevelHybridTable)=0;
//	
//	virtual VectorAssetValuePtr GETAssets_getThumbnailAssetByParentId(int32 parentId)=0; 
//	virtual PMString getCommentByPublicationID(int32 PubID)=0;
//	virtual VectorAssetValuePtr GETAssets_getAssetByAssetId(int32 AssetId)=0; 
//
//	virtual VectorAPpubCommentValuePtr getPubCommentsBySectionIdStageId(int32 sectionId, int32 stageId)=0;
//
//	virtual int32 getPubComments_insertStencilsValue(CStencilValue &cStencilValueObj)=0;
//	virtual VectorPubObjectValuePtr GetProjectProducts_getHybridTablesForSubSection(int32 subSecID, int32 LanguageID)=0;
//    virtual VectorTypeInfoPtr AttributeCache_getHybridTableTypesForClassAndParents(int32 ClassID) = 0;
//	
//	virtual CUserInfoValue UserCache_getUserValueByUserId(int32 userId)=0;
//
//	virtual bool16 GetPubComments_updatePubComment(int32 pubCommentId, int32 commentStatus, PMString responseComment, int32 updateUserId)=0;
//	virtual VectorTypeInfoPtr TypeCache_getWBCommentTypeList()=0;
//	virtual void clearAllStaticObjects() = 0;
//	
//	virtual int32 PubCommentModel_insertAPpubComment(vector <CPubCommentModel>& objAPpubComment)=0;
//
//	virtual CPubModel getpubModelByPubID(int32 PubID,int32 languageID)=0;
//	virtual CPubCommentModel GetPubComments_GetPubCommentModel(int32 pubCommentId) =0;
//	virtual bool16 GetProjectProducts_updatePublicationProofed(int32 sectionId)=0;
//
//	
//	///  Methods added to handle rand, Manufacturer, Supplier & Publication Logo Images.... By Rahul.
//	virtual PMString GetAssets_getAssetFolderForBMSandPubLogo(int32 parentId, int32 imageTypeId) =0 ;
//	virtual PMString GetAssets_getItemBMSAssetFileNamebyParentIdTypeId(int32 parentId, int32 imageTypeId)=0;
//	virtual bool16 GETAsset_downloadItemBMSAsset(int32 itemId, int32 imageTypeId, PMString targetLocalPath)=0;
//	virtual PMString GetAssets_getPubLogoAssetFileNamebyParentIdTypeId(int32 parentId, int32 imageTypeId)=0;
//	virtual bool16 GETAsset_downloadPubLogoAsset(int32 itemId, int32 imageTypeId, PMString targetLocalPath)=0;
//	virtual PMString GetAssets_getProductBMSAssetFileNamebyParentIdTypeId(int32 parentId, int32 imageTypeId)=0;
//	virtual bool16 GETAsset_downloadProductBMSAsset(int32 itemId, int32 imageTypeId, PMString targetLocalPath)=0;
//
//	virtual void getObjectInfo(int32 ObjectId, int32 ObjectTypeId, int32 SetionId, /*int32 PublicationId,*/
//		bool16 IsOneSource, bool16 iscopy, bool16 isAsset, bool16 isDbTable, bool16 isHyTable,
//		vector <int32> attributeIds, vector <int32> assetIds , vector <int32> dbTypeIds,
//		vector <int32> hyTypeIds,int32 isProductItemOrHybrid) = 0;
//
//	virtual CAttributeModel ATTRIBUTECache_getItemAttributeForIndex(int32 option, int32 languageID)=0;   //Chetan
//	virtual VectorAttributeInfoPtr GetSearch_GetSearchableAttributes(int32 Language_ID)=0;
//	virtual VectorItemModelPtr GetSearch_getSearchResult(int32 attributeID, PMString value, bool16 exactMatch, PMString searchBy, bool16 matchCase, int32 searchClassId, bool16 includeChild, PMString frmCrDate, PMString toCrDate, PMString frmUpDate, PMString toUpDate, PMString searchNonCat, int32 LanguageID)=0;
//	virtual VectorObjectInfoPtr GetSearch_getProductSearch(PMString productName,PMString productNumber, int32 LanguageID)=0;
//	virtual bool16 GetPubComments_DeletePubComment(int32 pubCommentId)=0;
//	virtual bool16 GetProjectProducts_callPickup(int32 srcSecId,int32 trgSecId,int32 objectId)=0;
//	
//	virtual VectorTypeInfoPtr TypeCache_getPubTypeList() = 0;
//	virtual int32 GetProject_CreatePublication(int32 pubIdNew, int32 pubId, PMString pubName, PMString pubNumber,PMString comments,int32 pubTypeId,int32 pageCount) = 0;
//	virtual int32 GetProject_getPublicationID() = 0;
//	virtual bool16 GetProject_updatePublication(CPubModel &model) = 0;
//
//	virtual VectorCStencilValuePtr getPubComments_getStencilsBySectionId(int32 sectionId)= 0;
//	virtual int32 getPubComments_UpdateStencilsValue(CStencilValue &cStencilValueObj)= 0;
//	virtual void getPubComments_UpdateMasterPageNameForSectionStencil(int32 sectionId, PMString MasterPageName)= 0;
//
//	virtual PMString GetProject_validateCatalogCreation(int32 OldpubId, PMString pubName, PMString pubNumber)=0;
//	virtual PMString getPubSecSubsecCopyAttributeValue(int32 objectId,int32 attrId,int32 LanguageID)=0;
//
//	virtual bool16 CONFIGCACHE_getIndesignSbShowPcAttribute()=0;
//	virtual PMString ClientActionGetAssets_getCategoryAssetFolder(int32 assetId, int32 ImageType)=0;
//	virtual bool16 GETAsset_downLoadCatagoryAsset(int32 assetID,PMString targetLocalPath , int32 imageType)=0;
//	virtual int32 GetONEsourceObjects_getCategoryClassIDByObjectIdLevelNo(int32 objectId,int32 levelNo, int32 isProduct)=0;
//
//	virtual PMString getCodeToRename(PMString stringToRename)=0;
//	virtual PMString LABELCACHE_getLabel(PMString code, int32 language_Id)=0;
//	
//	virtual VectorAssetValuePtr GETAssets_GetPVMPVAssetByParentIdAndAttributeId(int32 ParentId,int32 AttributeId, int32 LangaugeId,int32 objectType,int32 PVImageindex = 1) = 0;
//	virtual bool16 GETAsset_downLoadPVMPVAsset(PMString subDirPath ,PMString  fileName,PMString targetLocalPath) = 0;
//
//	virtual VectorAttributeInfoPtr AttributeCache_getAttributeByParentIdLanguageId(int32 AttributeID , int32 LanguageID) = 0;
//	virtual bool16 ElementCache_GetElementModelForLanguageIdByElementId(int32 elementID, int32 languageId, CElementModel & cElementModelObj,int32 isProduct=1) = 0;
//	virtual VectorTypeInfoPtr GetItemOrProductBMSImageTypes(bool16 isProduct) = 0;
//
//	virtual bool16 CONFIGCACHE_getShowCustomerInIndesign() = 0;
//	virtual int32 CONFIGCACHE_getCustomerPVTypeId(int32 isCustomer) = 0;
//	virtual PMString CONFIGCACHE_getCustomerAssetTypes(int32 isCustomer) = 0;
//	virtual VectorPossibeValueModelPtr POSSIBELVALUECACHE_getAllPossibleValuesByTypeId(int32 PVTypeId) = 0;
//	virtual int32 getSelectedCustPVID(int32 isCustomer) = 0;
//	virtual void setSelectedCustPVID(int32 pvID,int32 isCustomer) = 0;
//
//	virtual bool16 CONFIGCACHE_getShowWhirlPool()=0;
//	virtual VectorAssetValuePtr GetMPVAssetByBaseModelNumberForWP_EventItemSwatchImage(int32 sectionId, int32 itemId, int32 PVimageIndex = 1)=0;
//	
//	virtual VectorAssetValuePtr GETAssets_GetPVAssetsList(PMString pvTypeID,int32 itemID,int32 PVImageindex = 1) = 0;
//	virtual bool16 CONFIGCACHE_getPVImageFileName(int32 index) = 0;
//	virtual void GetSectionData_getDataForPubOrCat(const GetSectionData& getSectionDataObj) = 0;
//	virtual void GetSectionData_clearSectionDataCache() = 0;
//	virtual bool16 CLASSIFICATIONTREE_clearInstance(void) = 0;
//	
//	
//	
//	virtual void setClientID(int32 clientid) = 0;
//	virtual int32 getClientID() = 0;
//
//	virtual VectorCPubObjectValuePtr GetEventObject_getObjectValueByEventAttributeAndObjectId(int32 objectId,int32 attibuteId,int32 eventId) = 0;
//	virtual VectorCPubObjectValuePtr GetEventObject_getObjectValueByEventAndObjectId(int32 objectId,int32 eventId) = 0;
//	virtual VectorScreenItemTableInfoPtr GETONEsourceObjects_getItemTablesByItenId(int32 object_id) =0;
//	
//	virtual bool16 CONFIGCACHE_getCmShowEventNumber()=0;
//	virtual bool16 getConfigCM_DISPLAY_PUB_TYPES_GROUP_TREE() = 0;
//	
//	
//
//	virtual int32 GetProof_generateProofId() = 0;
//	virtual	int32 GetProof_generateProofPageId(int32 numOfPages) = 0;
//	virtual bool16 GetProof_insertProofAndProofPages(ProofModel& , vector<ProofPagesModel>&)=0;
//	virtual VectorAPpubCommentValuePtr GetPubComments_getPubCommentsByProofPageIdUserId(int32 proofPageId, int32 userId)=0;
//
//	virtual bool16 GetProof_getProofAndProofPagesByProofId(int32 proofId, ProofModel&, vector<ProofPagesModel>&) = 0;
//	virtual VectorUserModelPtr GetProof_getAllUsersByProofId(int32 proofId) = 0;
//	virtual PMString GETItem_GetItemMMYDetailsByLanguageId(int32 ItemID,int32 AttributeID,int32 LanguageID )=0;
//	virtual bool16 ATTRIBUTECache_isItemAttributeEventSpecific(int32 AttributeID) = 0;
//	virtual VectorElementInfoPtr ElementCache_getEventSectionAttributesByLangaugeId(int32 LanguageID,int32 isSecEventAttribute) = 0;
//
//	virtual void getMultipleSectionData(vec_GetMultipleSectionData& vecGetMultipleSectionData, GetSectionData& getSectionDataObj) = 0;
//	virtual void switchSectionData(int32 sectionData) = 0;
//	virtual void multipleSectionDataClearInstance() = 0;
//
//	virtual CMMYTable* getMMYTableByItemIds(vector<int32>* vec_itemIdsPtr) = 0;
//	virtual PMString MakeModelCache_getMakeValueByMakeID(int32 makeID) = 0;
//	virtual PMString MakeModelCache_getModelValueByModelID(int32 modelID) = 0;
//	virtual PMString GetMakeModel_getYearByItemIdModelId(int32 ItemID,int32 modelID) = 0;
//	virtual VectorAttributeInfoPtr AttributeCache_getAllItemAttributes() = 0;
//	virtual VectorProofingReviewValuePtr GetProof_getAllReviews() = 0;
//	virtual bool16 GetProof_saveProof(int32 proofId,int32 roleId,PMString reviewIds,PMString userId,PMString dates,PMString otherNotes) = 0;
//
//	virtual bool16 GetProof_insertProofRevision(ProofRevisionValue& proofRevisionValue) = 0;
//
//	virtual VectorManufacturerValuePtr SYSTEMATTRIBUTEVALUECACHE_getAllManufacturers() = 0;
//	virtual VectorSupplierValuePtr SYSTEMATTRIBUTEVALUECACHE_getAllSuppliers() = 0;
//	
//	virtual ProofStatusAction ProofStatusCache_getProofStatusAndNextAction(int32 statusId) = 0;
//
//	virtual bool16 GetProof_updateProofStatusId(int32 proofId,int32 statusId) = 0;
//
//	virtual VectorLongIntPtr CONFIGCACHE_getIndesignProofBuyerUserList() = 0;
//
//	virtual int32 CONFIGCACHE_getBuyerRoleId() = 0;
//	virtual int32 CONFIGCACHE_getElementIDForItemGroupDescription() = 0;
//	virtual VectorAttributeInfoPtr AttributeCache_getItemAttributesForClass(int32 ClassID , int32 LanguageID) = 0;
//	
//	virtual PMString get_currentBuildVersion() = 0;  //*** Added For VErsioning.
//	virtual void GetVersionUpdate_getVersionUpdateValue( VersionUpdateValue* versionValue)=0;
//	virtual ItemProductCountMapPtr GETPROJECTPRODUCTS_getItemProductCountForSection(int32 isOnsource) = 0;
//	virtual VectorLongIntPtr CONFIGCACHE_getEventPriceAttributes() = 0;
//	virtual ItemMapPtr GetProjectProducts_getAllLeadingItemIds(int32 secID) = 0;
//
//	virtual int32 getOdStyleTemplate_insertOdTemplateValue(OdTemplateValue &OdTemplateValueObj) = 0;
//	virtual int32 getOdStyleTemplate_generateStyleGroupId() = 0;
//	virtual int32 getOdStyleTemplate_generateStyleId() = 0;
//	virtual int32 getOdStyleTemplate_generateStyleRuleId() = 0;
//	virtual bool16 getOdStyleTemplate_insertOdStyleGroupValue( OdStyleGroupValue&   odStyleGroupValue) = 0;
//	virtual bool16 getOdStyleTemplate_insertOdStyleValue( OdStyleValue&    odStyleValue  ) = 0;
//	virtual OdTemplateValueMapPtr getOdStyleTemplate_getAllTemplate() = 0;
//	virtual VectorLongIntPtr getOdStyleTemplate_getAllStyleGroupId() = 0;
//	virtual VectorLongIntPtr getOdStyleTemplate_getAllStyleId() = 0;
//	virtual bool16 getOdStyleTemplate_insertOdStyleTemplateValue(OdStyleTemplateValueVecPtr OdStyleTemplateValueptr) = 0;
//	virtual bool16 getOdStyleTemplate_insertOdStyleRuleValue(OdStyleRuleValue &OdStyleRuleValueObj) = 0;
//
//	virtual OdStyleValueVecPtr getOdStyleTemplate_getStyleValueByStyleId(int32 styleId) = 0; 
//	virtual OdStyleRuleValue* getOdStyleTemplate_getStyleRuleValueByStyleId(int32 styleId) = 0;
//
//	virtual OdStyleValueVecPtr getOdStyleTemplate_getAllStyleNos() = 0;
//	virtual PMStringListPtr getOdStyleTemplate_getAllStyleGroupNos() = 0;
//
//	virtual pOdTemplateValueVec getOdStyleTemplate_getAllTemplatesByStyleId(int32 styleId,OdStyleTemplateValueVecPtr& OdStyleTemplateValueptr)= 0;
//
//	virtual void clearStaticLanguageModelVector() = 0;
//
//	virtual int32 ProjectCache_getLevelForSection()=0;
//
//	virtual VectorPubModelPtr ProjectCache_getParentsToRootPubIdListForIndesign(int32 pubId)=0;
//
//	virtual PMStringVecPtr CONFIGCACHE_getByConfigName(PMString ConfigName) = 0;
//
//	virtual bool16 GetConfig_getFunctionAccessByFunctionCode(PMString functionCode) = 0;
//	virtual VectorPubModelPtr GetProject_getAllSectionsByPublicationIdAndLevel(int32 publicationId , int32 level , int32 languageId) = 0;
//	virtual VectorLongIntPtr GetProjectProducts_getTableIDsByObjectIdItemIdStringAndSeqOrderstring(int32 objectId, VectorLongIntPtr vecOfItemIdsPtr) = 0;
//	
//	//virtual CroosClientAttributeMapPtr getAllCrossClientAttributeByClientId(int32 client_ID , map<int32,AttributeClientCodeModel>& AttributeClientMapByCode_Id ) = 0;
//
//	//added by avinash
//	virtual PMStringVecPtr GetProjectProducts_getListTableNameByLanguageId(int32 root_id, int32 object_id, int32 table_id, int32 lang_id, bool flag) = 0;
//	//till here
//
//    virtual MapInMapptr GetItem_getItemAndItemGroupByIdListAndFieldIds(iitemidlist, iitemattributelist, iitemgrouplist,iitemgroupattributelist)=0;//By Amarjit patil
//	virtual bool16 CheckItemInDB_getItemDetails(int32 itemID,int32 languageID)= 0;//By amarjit patil
//	virtual bool16 CheckItemInSection_getAllitemIdsBySectionIds(int32 sectionId, int32 itemId)=0;//By amarjit patil
//	//virtual bool16 CheckItemGroupInDB_getObjectAndObjectValuesByObjectId(int32 itemID)= 0;//By amarjit patil
//	//virtual bool16 GetItemsGroupsFromDB_FindByObjectIdPubId(int32 sectionId, int32 itemId)=0;//By amarjit patil
//
//	virtual ElementIDptr FindItemGroupInDB_getAllDeletedItemGroupByIds(vector<int32>itemID)=0;
//	virtual ElementIDptr FindParentItemInDB_findAllItemsWithDeletedParents(vector<int32>itemIdList)=0;
//	virtual ElementIDptr FindParentItemInSection_getPubItemWithDeletedParents(int32 itemid,int32 sectionId)=0;
//	virtual ElementIDptr FindParentItemGroupInDB_getAllItemGroupItemsWithDeletedParents(vector<int32>Itemgroup)=0;
//	virtual ElementIDptr FindParentItemGroupInSection_getItemGroupPubItemsWithDeletedParents(int32 itemgroupId,int32 sectionId)=0;
//	virtual bool16 checkimageinDB_getAssetByParentAndType(int32 ItemorItemgroupId,int32 ItemorItemgroipTypeId,int32 ImageTypeId )=0;
//	//virtual PMStringVecPtr GetProjectProducts_getListTableNameByLanguageId(int32 root_id, int32 object_id, int32 table_id, int32 lang_id, bool flag) = 0;
//	virtual PMString GetProduct_getItemByTableIDName(int32 tableID)=0;
//	virtual MapInMapptr GetItem_getItemAndItemGroupByIdListAndFieldIdsNew(iitemidlist, iitemattributelist, iitemgrouplist,iitemgroupattributelist)=0;
//	virtual MapInMapptr GetItem_getItemAndItemGroupByIdListAndFieldIdsNewforCustomTable(iitemidlist, iitemattributelist, iitemgrouplist,iitemgroupattributelist,int32 langID)=0;
//	virtual VectorODTemplateValuePtr GETODTemlate_FindByObjectIdAndObjectTypeId(int32 parentId,int32 parentTypeId)=0;
//	virtual void userTest()=0;
//	//added by avinash
//	virtual void UpdateFlagsforPbObject(int32 sectionId, int32 ObjectId, bool16 flag, int32 whichDesignerAction)=0;
//	virtual  void UpdateDeleteFlagforPbObject(int32 sectionId, int32 objectId, bool flag)=0;
//	virtual void XYZ(int32 sectionId) = 0;
//	//till here
//
//	virtual void setSelectedDivPVIDArray(int32 arr[]) = 0;
//	virtual int32 *getSelectedDivPVIDArray() = 0;
	
};

#endif
