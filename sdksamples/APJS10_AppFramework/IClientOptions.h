
#ifndef __IClientOptions__
#define __IClientOptions__

#include "IPMUnknown.h"
#include "AFWJSID.h"

class IClientOptions : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_ICLIENTOPTIONS };
	
		virtual int32 getDefaultLocale(PMString&)=0;
		virtual int32 getDefPublication(PMString&)=0;
		virtual PMString getImageDownloadPath(void)=0;
		virtual PMString getDocumentDownloadPath(void)=0;
		virtual int32 getDefPublicationDlg(PMString&, bool16)=0;
		virtual void setdoRefreshFlag(bool16)=0;

		virtual PMString getWhiteBoardMediaPath()=0;
		virtual bool8 checkforFolderExists(PMString&) = 0;

		virtual void setLocale(PMString&, int32&)=0;
		virtual void setPublication(PMString&, int32&)=0;
		virtual void setSection(PMString& ,int32&)=0;
		
		//virtual void setDefaultFLow(int32 );
		virtual void set_DisplayPartnerImages(int32 )=0;
		virtual void set_DisplayPickListImages(int32 )=0;
};

#endif


/*
InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
if(cop!=nil){
	// call IClientOptions methods
	PMString imagePath = getImageDownloadPath();
}

*/