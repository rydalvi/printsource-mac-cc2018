#include "VCPlugInHeaders.h"
#include "PartnerCache.h"
#include "CAlert.h"
#include "IAppFramework.h"

#define CA(x) CAlert::InformationAlert(x)


PartnerCache* PartnerCache::pcObjPtr = NULL;
bool PartnerCache::pcInstanceFlag = false;


PartnerCache* PartnerCache::getInstance() // SingleTone Implementation.
{
	//CA("Inside PartnerCache::getInstance");
	if(!pcInstanceFlag)
	{
		//CA("Creating Instance");
		pcObjPtr = new PartnerCache();
		pcInstanceFlag = true;
		pcObjPtr->loadPartnerCache();
		return pcObjPtr;
	}
	else
	{
		if(!pcObjPtr->isPartnerCacheLoaded)
		{
			pcObjPtr->loadPartnerCache();
		}
		return pcObjPtr;
	}
}

void PartnerCache::clearInstace()
{
	isPartnerCacheLoaded = kFalse;
	pcInstanceFlag = kFalse;
	if(pcValuePtr != NULL)
	{
		delete pcValuePtr;
		pcValuePtr = NULL;
	}
}


bool16 PartnerCache::loadPartnerCache()
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return kFalse;
	}
	
	/*if(pcValuePtr != NULL)
	{
		delete pcValuePtr;
		pcValuePtr = NULL;
	}*/
	pcValuePtr = new PartnerCacheValue();
	bool16 result=ptrIAppFramework->callPartnerJson(pcValuePtr);
	if(result)
	{
		isPartnerCacheLoaded = kTrue;
		
	}
	else
	{
		isPartnerCacheLoaded = kFalse;
		return kFalse;
	}
	return kTrue;
}

PMString PartnerCache::getPartnerNameByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
{
	PMString resultValue("");
	if(pcValuePtr != NULL)
	{
		resultValue = pcValuePtr->getPartnerNameByPartnerIdPartnerType(partnerId, partnerType);
	}

	return resultValue;
}

	
CAssetValue PartnerCache::getPartnerAssetValueByPartnerIdImageTypeId(double partnerId, int32 imageTypeId)
{
	CAssetValue resultValue;
	if(pcValuePtr != NULL)
	{
		resultValue = pcValuePtr->getPartnerAssetValueByPartnerIdImageTypeId(partnerId, imageTypeId);
	}
	return resultValue;
}
	

PMString PartnerCache::getPartnerDescription1ByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
{
	PMString resultValue("");
	if(pcValuePtr != NULL)
	{
		resultValue = pcValuePtr->getPartnerDescription1ByPartnerIdPartnerType(partnerId, partnerType);
	}

	return resultValue;
}

PMString PartnerCache::getPartnerDescription2ByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
{
	PMString resultValue("");
	if(pcValuePtr != NULL)
	{
		resultValue = pcValuePtr->getPartnerDescription2ByPartnerIdPartnerType(partnerId, partnerType);
	}

	return resultValue;
}