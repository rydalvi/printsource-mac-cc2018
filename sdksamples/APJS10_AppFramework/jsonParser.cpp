#include "VCPlugInHeaders.h"
#include "jsonParser.h"
#include "CAlert.h"
#include <iostream>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "IAppFramework.h"
#include <string>


#define CA(x) log(x)
#define CA_NUM(a,b) {PMString str(a);str.Append( " : ");str.AppendNumber(b);CA(str);}


void log(const char * logtext)
{
	//const char* path = "D:\\RD\\Apsiva\\json\\ApsivaTestData\\libjsontestResult.txt";
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}
/*
	ptrIAppFramework->SetFilePath();
	PMString fileFullPathstr = ptrIAppFramework->GetFilePath();
	//headerfile = fopen(ptrIAppFramework->GetFilePath(), "a");
	string tempString(fileFullPathstr.GetPlatformString());
	char* fileFullPath = const_cast<char *> (tempString.c_str());

	ofstream myfile;
    myfile.open (fileFullPath, ios::out | ios::app );
	myfile <<logtext;
	myfile << "\n";
    myfile.close();
 */
    PMString err_msg(logtext);
    ptrIAppFramework->LogError(err_msg);
}

bool16 jsonParser::parseLogin(PMString& inputJson, PMString &sessionId, PMString &apiKey, PMString &pimVersion)
{
	bool16 result = kFalse;
	bool a = false;
	//a = /*json_is_valid*/is_valid(inputJson.GetUTF8String()/*"{\"sessionId\":\"6FB7FFD982CB9EF4D87BAAD2A2B4AD34.worker1\"}"*/);
	/*if(!a)
	{
		CA("Invalid Json");
		return result;
	}	
	else*/
	{
		//CA( "Valid Json");

	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
		//CA("Inside Main 3 \n" );
	    try
		{
		   if(current == NULL)
		   {
			   log("current == NULL");
			   return result;
		   }
		   //ParseJSON(*current);
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				// get the node name and value as a PMString
				std::string node_name = (*i) -> name();
				//CA(node_name.c_str());
				if(node_name.compare("sessionId") == 0)
				{
					std::string node_value = (*i) -> as_string();
					//CA(node_value.c_str());
					sessionId.Append(node_value.c_str());
					result = kTrue;
				}
                if(node_name.compare("api_token") == 0)
                {
                    std::string node_value = (*i) -> as_string();
                    //CA(node_value.c_str());
                    apiKey.Append(node_value.c_str());
                    result = kTrue;
                }
                if(node_name.compare("pimVersion") == 0)
                {
                    std::string node_value = (*i) -> as_string();
                    //CA(node_value.c_str());
                    pimVersion.Append(node_value.c_str());
                    result = kTrue;
                }
				else if(node_name.compare("error") == 0)
				{
					//std::string node_value = (*i) -> as_string();
					//CA(node_value.c_str());
					//sessionId.Append(node_value.c_str());
					result = kFalse;
				}

				//// get the node name and value as a PMString
				//char * node_name = json_name(*i);
				////CA(node_name.c_str());
				//if(strcmp(node_name, "sessionId") == 0)
				//{
				//	char* node_value = json_as_string(*i);
				//	//CA(node_value.c_str());
				//	sessionId.Append(node_value);
				//	json_free(node_value);
				//	result = kTrue;
				//}
				//else if(strcmp(node_name,"error") == 0)
				//{
				//	//char* node_value = json_as_string(*i);
				//	//log(node_value);
				//	//CA(node_value.c_str());					
				//	//json_free(node_value);
				//	result = kFalse;
				//}
				//json_free(node_name);

				++i;
			}
			result = kTrue;
		}catch (...){
				log("parseLogin false");
				return result;
		}
		json_delete(current);

	}
		/*json_free_all();
		json_delete_all();*/
		return result;
}

bool16 jsonParser::parseLogout(PMString inputJson, PMString &logoutStatus)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		log("Invalid Json");
		return result;
	}	
	else
	{
		//CA( "Valid Json");

	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
		//CA("Inside Main 3 \n" );
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }
		   //ParseJSON(*current);
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				// get the node name and value as a PMString
				std::string node_name = (*i) -> name();
				//CA(node_name.c_str());
				if(node_name.compare("logout") == 0)
				{
					std::string node_value = (*i) -> as_string();
					//CA(node_value.c_str());
					logoutStatus.Append(node_value.c_str());
					result = kTrue;
				}
 
				++i;
			}
			result = kTrue;
		}catch (...){
				CA("false");
				return result;
		}
        json_delete(current);
	}
		return result;
}

bool16 jsonParser::parseStructure(PMString inputJson, StructureCacheValue &structureTreeObj)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("Invalid Json");
		return result;
	}	
	else
	{
		//CA( "Valid Json");

	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
		//CA("Inside Main 3 \n" );
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }
		   //ParseJSON(*current);
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{
			
					//CA("Got an Array or Object");
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("structure") == 0)
					{
						//CA("got First Structure");
						ParseStructureChildernJSON(**i, structureTreeObj);

						//CA("ClassificationName : " + structureTreeObj.getClassificationName() );
						//CA_NUM("ItemGroupFieldSize " , (int32) structureTreeObj.getItemGroupFields().size());
						
						//setParentIdInStructureObjects(structureTreeObj, -1);
					}

				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			result = kTrue;
		}catch (...){
				CA("false");
				return result;
		}
        json_delete(current);
	}
		return result;
}

bool16 jsonParser::parseAllTree(PMString inputJson, EventCacheValue &eventTreeObj)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("Invalid Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("events") == 0)
					{
						//CA("got First events");
						//ParseEventChildernJSON(**i, eventTreeObj);
						//CA("ClassificationName : " + structureTreeObj.getClassificationName() );
						//CA_NUM("ItemGroupFieldSize " , (int32) structureTreeObj.getItemGroupFields().size());
						vector<EventCacheValue> vectorEventCacheValue;
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							EventCacheValue ECChildernValue;
							ParseEventChildernJSON(((*i)->at(arrayCounter)), ECChildernValue);
							vectorEventCacheValue.push_back(ECChildernValue);
						}

						eventTreeObj.setChildren(vectorEventCacheValue);
					}
				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			result = kTrue;
		}catch (...){
				CA("Exception caught in parseAllTree");
				return result;
		}
        json_delete(current);
	}
		return result;
}


void jsonParser::ParseStructureChildernJSON(JSONNode & nodeObj, StructureCacheValue &structureTreeObj)
{
	JSONNode::json_iterator i = nodeObj.begin();
	double parentClassId = -1;
    while (i != nodeObj.end()){
		//log("Inside ParseStructureChildernJSON");
		 // get the node name and value as a PMString
	  try
	  {
        std::string node_name = (*i) -> name();
		//log(node_name.c_str());
        // recursively call ourselves to dig deeper into the tree
		if ((*i)->type() == JSON_ARRAY)
		{	

			if(node_name.compare("children") == 0)
			{	
				vector<StructureCacheValue> vectorStrCacheValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					StructureCacheValue SCChildernValue;
					ParseStructureChildernJSON(((*i)->at(arrayCounter)), SCChildernValue);
					vectorStrCacheValue.push_back(SCChildernValue);
				}

				structureTreeObj.setChildren(vectorStrCacheValue);
			}
			else if(node_name.compare("itemFields")== 0)
			{
				//CA("Got Item Fields");
				vector<Attribute> vectoritemFieldValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					Attribute itemFieldValue;
					ParseAttributeJSON(((*i)->at(arrayCounter)), itemFieldValue);
					vectoritemFieldValue.push_back(itemFieldValue);
				}
				structureTreeObj.setItemFields(vectoritemFieldValue);
			}
			else if(node_name.compare("itemGroupFields")== 0)
			{
				//CA("Item Group Fields");
				//CA_NUM("ItemGroupFieldSize " , (int32)(*i)->size());
				vector<CElementModel> vectoritemGroupFieldValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CElementModel itemGroupFieldValue;
					ParseElementJSON(((*i)->at(arrayCounter)), itemGroupFieldValue);
					vectoritemGroupFieldValue.push_back(itemGroupFieldValue);
				}
				structureTreeObj.setItemGroupFields(vectoritemGroupFieldValue);
			}
			else if(node_name.compare("itemAssetTypes")== 0)
			{
				vector<CTypeModel> vectoritemAssetTypeValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CTypeModel itemAssetTypeValue;
					ParseTypeJSON(((*i)->at(arrayCounter)), itemAssetTypeValue);
					vectoritemAssetTypeValue.push_back(itemAssetTypeValue);
				}
				structureTreeObj.setItemAssetTypes(vectoritemAssetTypeValue);
			}
			else if(node_name.compare("itemGroupAssetTypes")== 0)
			{
				vector<CTypeModel> vectoritemGroupAssetTypesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CTypeModel itemGroupAssetTypeValue;
					ParseTypeJSON(((*i)->at(arrayCounter)), itemGroupAssetTypeValue);
					vectoritemGroupAssetTypesValue.push_back(itemGroupAssetTypeValue);
				}
				structureTreeObj.setItemGroupAssetTypes(vectoritemGroupAssetTypesValue);
			}
			else if(node_name.compare("listTypes")== 0)
			{
				vector<CTypeModel> vectorlistTypesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CTypeModel listTypeValue;
					ParseTypeJSON(((*i)->at(arrayCounter)), listTypeValue);
					vectorlistTypesValue.push_back(listTypeValue);
				}
				structureTreeObj.setListTypes(vectorlistTypesValue);
			}
			else if(node_name.compare("sectionAssetTypes")== 0)
			{
				vector<CTypeModel> vectorSectionAssetTypesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CTypeModel sectionAssetTypesValue;
					ParseTypeJSON(((*i)->at(arrayCounter)), sectionAssetTypesValue);
					vectorSectionAssetTypesValue.push_back(sectionAssetTypesValue);
				}
				structureTreeObj.setSectionAssetTypes(vectorSectionAssetTypesValue);
			}
			else if(node_name.compare("sectionFields")== 0)
			{				
				vector<CElementModel> vectorSectionFieldsValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CElementModel sectionFieldsValue;
					ParseElementJSON(((*i)->at(arrayCounter)), sectionFieldsValue);
					vectorSectionFieldsValue.push_back(sectionFieldsValue);
				}
				structureTreeObj.setSectionFields(vectorSectionFieldsValue);
			}
			else if(node_name.compare("languages")== 0)
			{				
				vector<CLanguageModel> vectorlanguageValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CLanguageModel languageValue;
					ParseLanguageJSON(((*i)->at(arrayCounter)), languageValue);
					vectorlanguageValue.push_back(languageValue);
				}
				structureTreeObj.setLanguages(vectorlanguageValue);
			}
			else if(node_name.compare("statuses")== 0)
			{				
				vector<StageValue> vectorStatuseValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					StageValue statuseValue;
					ParseStatusJSON(((*i)->at(arrayCounter)), statuseValue);
					vectorStatuseValue.push_back(statuseValue);
				}
				structureTreeObj.setStatuses(vectorStatuseValue);
			}
			else if(node_name.compare("values")== 0)
			{				
				vector<ItemGroupValue> vectorObjectValuesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					ItemGroupValue objectValuesValue;
					ParseItemGroupValueJSON(((*i)->at(arrayCounter)), objectValuesValue);
					vectorObjectValuesValue.push_back(objectValuesValue);
				}
				structureTreeObj.setObjectValues(vectorObjectValuesValue);
			}
			else if(node_name.compare("picklistGroups")== 0)
			{				
				vector<PicklistGroup> vectorPicklistGroupsValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					PicklistGroup picklistGroupsValue;
					ParsePicklistGroupValueJSON(((*i)->at(arrayCounter)), picklistGroupsValue);
					vectorPicklistGroupsValue.push_back(picklistGroupsValue);
				}
				structureTreeObj.setPicklistGroups(vectorPicklistGroupsValue);
			}

		}
		 if( (*i) -> type() == JSON_NODE){
			//log("Got an JSON_NODE ");
			//log(((string)(*i)->name()).c_str());
		 }
       
		else if(node_name.compare("categoryId")== 0)
		{
			//CA("categoryId");
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log("categoryId");
			//log(((*i) -> as_string()).c_str());
			structureTreeObj.setClassificationId(node_value.GetAsDouble());
			//CA_NUM("categoryId",node_value.GetAsNumber());
		}

		else if(node_name.compare("name")== 0)
		{
			//CA("name");
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log("name");
			//log(((*i) -> as_string()).c_str());
			structureTreeObj.setClassificationName(node_value);
			//CA(node_value);
		}

		else if(node_name.compare("classNo")== 0)
		{
			//CA("name");
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log("classNo");
			//log(((*i) -> as_string()).c_str());
			structureTreeObj.setClassNo(node_value);
			//CA(node_value);
		}

		/*else if(node_name.compare("keywords") == 0)
		{
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str());
			log(((*i) -> as_string()).c_str());
			structureTreeObj.setKeywords(node_value);
		}

		else if(node_name.compare("primaryContentManager")== 0)
		{
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str());
			log(((*i) -> as_string()).c_str());
			structureTreeObj.setPrimaryContentManager(node_value.GetAsNumber());
		}*/

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseStructureChildernJSON");
	  }
 
        ++i;
    }
	
}

void jsonParser::ParseAttributeJSON(JSONNode & nodeObj, Attribute &itemFieldValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end()){		

     try
	 {
        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);		
		//log(((*i) -> as_string()).c_str());

        if(node_name.compare("dataType")== 0)
		{	
			itemFieldValue.setDataType(node_value);			
		}
		else if(node_name.compare("languageId")== 0)
		{
			itemFieldValue.setLanguageId(node_value.GetAsDouble());
		}
		else if(node_name.compare("dataTypeId")== 0)
		{
			itemFieldValue.setDataTypeId(node_value.GetAsDouble());
		}
		else if(node_name.compare("name")== 0)
		{	
			itemFieldValue.setDisplayName(node_value);			
		}
		else if(node_name.compare("fieldId")== 0)
		{	
			itemFieldValue.setAttributeId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("sysName")== 0) // tableCol changed to sysName
		{	
			itemFieldValue.setTableCol(node_value);			
		}
		else if(node_name.compare("mpvSeparator")== 0)
		{	
			itemFieldValue.setMpvSeparator(node_value);			
		}
		else if(node_name.compare("mpvPrefix")== 0)
		{	
			itemFieldValue.setMpvPrefix(node_value);			
		}		
		else if(node_name.compare("mpvSuffix")== 0)
		{	
			itemFieldValue.setMpvSuffix(node_value);			
		}
		else if(node_name.compare("picklistGroupId")== 0)  //"pvTypeId"
		{	
			itemFieldValue.setPicklistGroupId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("uniqueness") == 0)
		{
			itemFieldValue.setUniqueness(node_value.GetAsNumber());
		}		
		else if(node_name.compare("catalogId") == 0)
		{
			itemFieldValue.setCatalogId(node_value.GetAsDouble());
		}
		else if(node_name.compare("decMaxLength")== 0)
		{	
			itemFieldValue.setDecMaxLength(node_value.GetAsNumber());			
		}
		else if(node_name.compare("decMinLength")== 0)
		{	
			itemFieldValue.setDecMinLength(node_value.GetAsNumber());			
		}
		else if(node_name.compare("domainId")== 0)
		{	
			itemFieldValue.setDomainId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("editLines")== 0)
		{	
			itemFieldValue.setEditLines(node_value.GetAsNumber());			
		}
		else if(node_name.compare("emptyValueDisplay")== 0)
		{	
			itemFieldValue.setEmptyValueDisplay(node_value);			
		}
		else if(node_name.compare("attributeNo")== 0)
		{	
			itemFieldValue.setNumber(node_value);			
		}
		else if(node_name.compare("maxLength")== 0)
		{	
			itemFieldValue.setMaxLength(node_value.GetAsNumber());			
		}
		else if(node_name.compare("minLength")== 0)
		{	
			itemFieldValue.setMinLength(node_value.GetAsNumber());			
		}		
		else if(node_name.compare("parentAttributeId")== 0)
		{	
			itemFieldValue.setParentAttributeId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("pubItem")== 0)
		{	
			itemFieldValue.setPubItem(node_value.GetAsNumber());			
		}
		else if(node_name.compare("required")== 0)
		{	
			itemFieldValue.setRequired(node_value.GetAsNumber());			
		}		
		else if(node_name.compare("uniquenessCaseSensitive")== 0) 
		{	
			itemFieldValue.setUniquenessCaseSensitive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("uniquenessCondensed")== 0) 
		{	
			itemFieldValue.setUniquenessCondensed(node_value.GetAsNumber());			
		}
		else if(node_name.compare("unitId")== 0) 
		{	
			itemFieldValue.setUnitId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("allowNewEntry")== 0) 
		{	
			itemFieldValue.setAllowNewEntry(node_value.GetAsNumber());			
		}
        else if(node_name.compare("dataType")== 0)
        {
            itemFieldValue.setDataType(node_value);
        }
        else if(node_name.compare("eventSpecific")== 0)
		{
			if(node_value.GetAsNumber() == 1)
                itemFieldValue.setIsEventSpecific(kTrue);
            else
                itemFieldValue.setIsEventSpecific(kFalse);
		}
		else if(node_name.compare("details")== 0)
		{
			//CA("Got Item Fields");
			vector<Details> vectordetailsValue;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				Details detailsValue;
				ParseDetailsJSON(((*i)->at(arrayCounter)), detailsValue);
				vectordetailsValue.push_back(detailsValue);
			}
			itemFieldValue.setNameList(vectordetailsValue);
		}

	 }catch(...)
	  {
		  log("exception caught in jsonParser::ParseAttributeJSON");
	  }
 
        ++i;
    }

}

void jsonParser::ParseElementJSON(JSONNode & nodeObj, CElementModel &itemGroupFieldValue)
{
	//log("Inside ParseElementJSON");
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		

	  try
	  {
        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("name")== 0)
		{				
			itemGroupFieldValue.setDisplayName(node_value);			
		}
		else if(node_name.compare("cdActive")== 0)
		{
			itemGroupFieldValue.setCdActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("printActive")== 0)
		{	
			itemGroupFieldValue.setPrintActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("webActive")== 0)
		{	
			itemGroupFieldValue.setWebActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("languageId")== 0)
		{	
			itemGroupFieldValue.setLanguageId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("fieldId")== 0)
		{	
			itemGroupFieldValue.setElementId(node_value.GetAsDouble());			
		}		
		else if(node_name.compare("dataType")== 0)
		{	
			itemGroupFieldValue.setDataTypeString(node_value);			
		}
		else if(node_name.compare("dataTypeId")== 0)
		{	
			itemGroupFieldValue.setDataTypeId(node_value.GetAsDouble());			
		}	
		
		else if(node_name.compare("mpvPrefix")== 0)
		{	
			itemGroupFieldValue.setMpvPrefix(node_value);			
		}
		else if(node_name.compare("mpvSeparator")== 0)
		{	
			itemGroupFieldValue.setMpvSeparator(node_value);			
		}
		else if(node_name.compare("mpvSuffix")== 0)
		{	
			itemGroupFieldValue.setMpvSuffix(node_value);			
		}
		else if(node_name.compare("picklistGroupId")== 0) //"pvTypeId"
		{	
			itemGroupFieldValue.setPicklistGroupId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("editLines")== 0)
		{	
			itemGroupFieldValue.setEditLines(node_value.GetAsNumber());			
		}
		else if(node_name.compare("emptyValueDisplay")== 0)
		{	
			itemGroupFieldValue.setEmptyValueDisplay(node_value);			
		}
		else if(node_name.compare("parentElementId")== 0)
		{	
			itemGroupFieldValue.setParentElementId(node_value.GetAsDouble());			
		}
		
		else if(node_name.compare("typeId")== 0)
		{	
			itemGroupFieldValue.setTypeId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("uniqueness")== 0)
		{	
			itemGroupFieldValue.setUniqueness(node_value.GetAsNumber());			
		}
		else if(node_name.compare("sysName")== 0)
		{	
			itemGroupFieldValue.setSysName(node_value);			
		}
	 	else if(node_name.compare("details")== 0)
		{
			//CA("Got Item Fields");
			vector<Details> vectordetailsValue;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				Details detailsValue;
				ParseDetailsJSON(((*i)->at(arrayCounter)), detailsValue);
				vectordetailsValue.push_back(detailsValue);
			}
			itemGroupFieldValue.setNameList(vectordetailsValue);
		}
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseElementJSON");
	  }

		++i;
    }

}

void jsonParser::ParseTypeJSON(JSONNode & nodeObj, CTypeModel &itemAssetTypeValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("typeId")== 0)
		{				
			itemAssetTypeValue.setTypeId(node_value.GetAsDouble());			
		}
		/*else if(node_name.compare("typeGroup")== 0)
		{				
			itemAssetTypeValue.setTypeId(node_value);			
		}*/
		else if(node_name.compare("name")== 0)
		{				
			itemAssetTypeValue.setName(node_value);			
		}
		else if(node_name.compare("code")== 0)
		{				
			itemAssetTypeValue.setCode(node_value);			
		}
		else if(node_name.compare("description")== 0)
		{				
			itemAssetTypeValue.setDescription(node_value);			
		}
		else if(node_name.compare("cdActive")== 0)
		{
			itemAssetTypeValue.setCdActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("printActive")== 0)
		{
			itemAssetTypeValue.setPrintActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("webActive")== 0)
		{
			itemAssetTypeValue.setWebActive(node_value.GetAsNumber());			
		}
		else if(node_name.compare("thumbnail")== 0)
		{
			itemAssetTypeValue.setThumbnail(node_value.GetAsNumber());			
		}
		else if(node_name.compare("flag1")== 0)
		{
			itemAssetTypeValue.setFlag1(node_value.GetAsNumber());			
		}
		else if(node_name.compare("flag2")== 0)
		{
			itemAssetTypeValue.setFlag2(node_value.GetAsNumber());			
		}
		else if(node_name.compare("url")== 0)
		{
			itemAssetTypeValue.setUrl(node_value);	
			//log("url");
		}
		else if(node_name.compare("active")== 0)
		{
			itemAssetTypeValue.setActive(node_value.GetAsNumber());			
		}
		
      }catch(...)
	  {
		  log("exception caught in jsonParser::ParseTypeJSON");
	  }
		++i;
    }
}


void jsonParser::ParseLanguageJSON(JSONNode & nodeObj, CLanguageModel &languageValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("languageId")== 0)
		{				
			languageValue.setLanguageID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("abbreviation")== 0)
		{				
			languageValue.setAbbreviation(node_value);			
		}
		else if(node_name.compare("defaultLanguage")== 0)
		{				
			languageValue.setdefaultLanguage(node_value.GetAsDouble());			
		}
		else if(node_name.compare("name")== 0)
		{				
			languageValue.setLanguageName(node_value);			
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseLanguageJSON");
	  }
		++i;
    }
}

void jsonParser::ParseStatusJSON(JSONNode & nodeObj, StageValue &statuseValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("statusId")== 0)
		{				
			statuseValue.setStageId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("name")== 0)
		{				
			statuseValue.setName(node_value);			
		}
		else if(node_name.compare("initialStage")== 0)
		{				
			statuseValue.setInitialStage(node_value.GetAsDouble());			
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseStatusJSON");
	  }
		++i;
    }
}

void jsonParser::ParseItemGroupValueJSON(JSONNode & nodeObj, ItemGroupValue &objectValuesValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("value")== 0)
		{				
			if(node_value == "null")
				node_value = "";
			objectValuesValue.setObjectValue(node_value);			
		}
		else if(node_name.compare("fieldName")== 0)
		{				
			objectValuesValue.setFieldName(node_value);			
		}
		else if(node_name.compare("fieldId")== 0)
		{				
			objectValuesValue.setElementId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("picklistValueIdsString")== 0)
		{				
			objectValuesValue.setPicklistValueIdsString(node_value);			
		}
		/*else if(node_name.compare("name")== 0)
		{				
			objectValuesValue.setName(node_value);			
		}*/

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseItemGroupValueJSON");
	  }
		++i;
    }

}

void jsonParser::ParsePicklistGroupValueJSON(JSONNode & nodeObj, PicklistGroup &picklistGroupsValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {
		std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		if ((*i)->type() == JSON_ARRAY)
		{
			if(node_name.compare("values") == 0)
			{	
				vector<PicklistValue> vectorPicklistValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					PicklistValue picklistValue;
					ParsePicklistValueJSON(((*i)->at(arrayCounter)), picklistValue);
					vectorPicklistValue.push_back(picklistValue);
				}
				picklistGroupsValue.setPicklistValues(vectorPicklistValue);
			}
		}
		else
		{
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log(((*i) -> as_string()).c_str());

			if(node_name.compare("name")== 0)
			{				
				picklistGroupsValue.setName(node_value);			
			}
			else if(node_name.compare("code")== 0)
			{				
				picklistGroupsValue.setCode(node_value);			
			}
			else if(node_name.compare("typeId")== 0)
			{				
				picklistGroupsValue.setTypeId(node_value.GetAsDouble());			
			}
			else if(node_name.compare("description")== 0)
			{				
				picklistGroupsValue.setDescription(node_value);			
			}
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParsePicklistGroupValueJSON");
	  }
		++i;
    }
}

void jsonParser::ParsePicklistValueJSON(JSONNode & nodeObj, PicklistValue &picklistValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("valueId")== 0)
		{				
			picklistValue.setValueId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("abbreviation")== 0)
		{				
			picklistValue.setAbbreviation(node_value);			
		}
		else if(node_name.compare("description")== 0)
		{				
			picklistValue.setDescription(node_value);			
		}
		else if(node_name.compare("value")== 0)
		{				
			picklistValue.setPossibleValue(node_value);			
		}
		else if(node_name.compare("imageFileName1")== 0)
		{				
			picklistValue.setImageFileName1(node_value);			
		}
		else if(node_name.compare("imageFileName2")== 0)
		{				
			picklistValue.setImageFileName2(node_value);			
		}
		else if(node_name.compare("imageFileName3")== 0)
		{				
			picklistValue.setImageFileName3(node_value);			
		}
		else if(node_name.compare("imageFileName4")== 0)
		{				
			picklistValue.setImageFileName4(node_value);			
		}
		else if(node_name.compare("imageFileName5")== 0)
		{				
			picklistValue.setImageFileName5(node_value);			
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParsePicklistValueJSON");
	  }
		++i;
    }

}


void jsonParser::ParseEventChildernJSON(JSONNode & nodeObj, EventCacheValue &eventTreeObj)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end()){
		//log("Inside ParseEventChildernJSON");
		 // get the node name and value as a PMString
	  try
	  {
        std::string node_name = (*i) -> name();
		//log(node_name.c_str());
        // recursively call ourselves to dig deeper into the tree
		if ((*i)->type() == JSON_ARRAY)
		{	

			if(node_name.compare("children") == 0)
			{	//CA("children");
				vector<EventCacheValue> vectorEventCacheValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					EventCacheValue ECChildernValue;
					ParseEventChildernJSON(((*i)->at(arrayCounter)), ECChildernValue);
					vectorEventCacheValue.push_back(ECChildernValue);
				}

				eventTreeObj.setChildren(vectorEventCacheValue);
			}
			
			else if(node_name.compare("assets")== 0)
			{
				vector<CAssetValue> vectorAssetValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CAssetValue assetValue;
					ParseAssetsJSON(((*i)->at(arrayCounter)), assetValue);
					vectorAssetValue.push_back(assetValue);
				}
				eventTreeObj.setAssets(vectorAssetValue);
			}			
			else if(node_name.compare("values")== 0)
			{				
				vector<ItemGroupValue> vectorObjectValuesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					ItemGroupValue objectValuesValue;
					ParseItemGroupValueJSON(((*i)->at(arrayCounter)), objectValuesValue);
					vectorObjectValuesValue.push_back(objectValuesValue);
				}
				eventTreeObj.setObjectValues(vectorObjectValuesValue);
			}			

		}
		else if( (*i) -> type() == JSON_NODE)
		{
			//log("Got an JSON_NODE ");
			////CA("Got an JSON_NODE ");
			//log(((string)(*i)->name()).c_str());

			//vector<EventCacheValue> vectorEventCacheValue;
			//	
			//EventCacheValue ECChildernValue;
			//ParseEventChildernJSON(**i, ECChildernValue);
			//vectorEventCacheValue.push_back(ECChildernValue);
			//	

			//eventTreeObj.setChildren(vectorEventCacheValue);
		 }
       
		else if(node_name.compare("name")== 0)
		{
			//CA("name");
			PMString node_value("");
			

			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::/*kUnknownEncoding*/kUnknownEncoding );
			
			/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				CAlert::InformationAlert("ptrIAppFramework is nil");
				return ;
			}
			ptrIAppFramework->LogDebug(node_value);*/

			//log("name");
			//log(((*i) -> as_string()).c_str());
			eventTreeObj.setName(node_value);
			//CA(node_value);
		}
		else if(node_name.compare("webActive")== 0)
		{
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log(((*i) -> as_string()).c_str());
			eventTreeObj.setWebActive(node_value.GetAsNumber());
		}
		else if(node_name.compare("eventType")== 0)
		{
			//CA("name");
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
			//log("classNo");
			//log(((*i) -> as_string()).c_str());
			eventTreeObj.setEventType(node_value);
			//CA(node_value);
		}
		else if(node_name.compare("eventId")== 0)
		{			
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);			
			//log(((*i) -> as_string()).c_str());
			eventTreeObj.setEventId(node_value.GetAsDouble());
			//CA_NUM("categoryId",node_value.GetAsNumber());
		}
		else if(node_name.compare("pubTypeId")== 0)
		{			
			PMString node_value("");
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);			
			//log(((*i) -> as_string()).c_str());
			eventTreeObj.setPubTypeId(node_value.GetAsDouble());
			//CA_NUM("categoryId",node_value.GetAsDouble());
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseEventChildernJSON");
	  }
         ++i;
    }	
}

void jsonParser::ParseAssetsJSON(JSONNode & nodeObj, CAssetValue &assetValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("assetId")== 0)
		{				
			assetValue.setAsset_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("parentTypeId")== 0)
		{				
			assetValue.setParent_type_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("typeId")== 0)
		{				
			assetValue.setType_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("description")== 0)
		{				
			assetValue.setDescription(node_value);			
		}
		else if(node_name.compare("url")== 0)
		{				
			assetValue.setUrl(node_value);			
		}
		else if(node_name.compare("seqOrder")== 0)
		{				
			assetValue.setSeqOrder(node_value.GetAsNumber());			
		}	
		else if(node_name.compare("cdActive")== 0)
		{				
			assetValue.setCd_active(node_value.GetAsNumber());			
		}
		//else if(node_name.compare("printActive")== 0)
		//{
		//	assetValue.setPrint_active(node_value.GetAsNumber());
		//}
		else if(node_name.compare("webActive")== 0)
		{				
			assetValue.setWeb_active(node_value.GetAsNumber());			
		}
		else if(node_name.compare("missing")== 0)
		{				
			assetValue.setMissing(node_value.GetAsNumber());			
		}
		else if(node_name.compare("thumbnail")== 0)
		{				
			assetValue.setThumbnail(node_value.GetAsNumber());			
		}
		else if(node_name.compare("flag1")== 0)
		{				
			assetValue.setFlag1(node_value.GetAsNumber());			
		}
		else if(node_name.compare("flag2")== 0)
		{				
			assetValue.setFlag2(node_value.GetAsNumber());			
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseAssetsJSON");
	  }
		++i;
    }

}

bool16 jsonParser::parseSysConfig(PMString inputJson, vector<ConfigValue> &configVectorObj)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		log("Invalid parseSysConfig Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("sysConfigs") == 0)
					{												
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							ConfigValue configValue;
							ParseConfigJSON(((*i)->at(arrayCounter)), configValue);
							configVectorObj.push_back(configValue);
						}						
					}
				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			result = kTrue;
		}catch (...){
				CA("Exception caught in parseSysConfig");
				return result;
		}
        json_delete(current);
	}
		return result;
}

void jsonParser::ParseConfigJSON(JSONNode & nodeObj, ConfigValue &configValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("configId")== 0)
		{				
			configValue.setConfigId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("clientId")== 0)
		{				
			configValue.setClientId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("configName")== 0)
		{				
			configValue.setConfigName(node_value);			
			//log(((*i) -> as_string()).c_str());
		}
		else if(node_name.compare("configValue1")== 0)
		{				
			configValue.setConfigValue1(node_value);			
		}
		else if(node_name.compare("configValue2")== 0)
		{				
			configValue.setConfigValue2(node_value);			
		}
		else if(node_name.compare("configValue3")== 0)
		{				
			configValue.setConfigValue3(node_value);			
		}
		else if(node_name.compare("active")== 0)
		{				
			configValue.setActive(node_value.GetAsNumber());			
		}		

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }
}

//void jsonParser::setParentIdInStructureObjects(StructureCacheValue & structureTreeObj, int32 parentId)
//{
//	structureTreeObj.setParentId(parentId);
//	if(structureTreeObj.getChildren().size() > 0)
//	{
//		for(int count =0; count < structureTreeObj.getChildren().size() ; count++)
//		{
//			setParentIdInStructureObjects(structureTreeObj.getChildren().at(count), structureTreeObj.getCategoryId() );
//		}
//	}
//}

bool16 jsonParser::parseSectionResult(PMString inputJson, vector<CPbObjectValue> &pbObjVectorObj, double sectionId, PMString attributeIdStr)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("Invalid parseSectionResult Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("results") == 0)
					{												
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							CPbObjectValue pbObjValue;
							ParseSectionResultJSON(((*i)->at(arrayCounter)), pbObjValue, sectionId , attributeIdStr);
							pbObjVectorObj.push_back(pbObjValue);
						}						
					}
				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			result = kTrue;
		}catch (...){
				CA("Exception caught in parseSectionResult");
				return result;
		}
        json_delete(current);
	}
		return result;
}


void jsonParser::ParseSectionResultJSON(JSONNode & nodeObj, CPbObjectValue &pbObjValue, double sectionId, PMString attributeIdStr)
{
	JSONNode::json_iterator i = nodeObj.begin();
	int32 isProduct = -1; // isProduct = 1 for Product , = 0 for Item , = 2 for Hybrid Table

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

	CObjectValue itemGroupObj;
	double itemGroupDescriptionAttrId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ITEM_GROUP_DESCRIPTION_ATTRIBUTE");

	CItemModel itemModelObj;	
	double itemDescriptionAttrId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ITEM_DESCRIPTION_ATTRIBUTE");

	PMString Description("");

	while( (i != nodeObj.end()))
	{
		try
		 {
			std::string node_name = (*i) -> name();
			if(node_name.compare("type")== 0)
			{				
				PMString node_value("");
				node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
				
				if(node_value.Compare(kFalse, "itemGroup")== 0)
				{
					isProduct = 1;
				}
				else if(node_value.Compare(kFalse, "item")== 0)
				{
					isProduct = 0; 
				}
			}
		}
		catch(...)
		{
		}
		++i;
	}

	i = nodeObj.begin();

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if( isProduct == 1)
		{
			
			if(node_name.compare("number")== 0)
			{				
				itemGroupObj.setNumber(node_value);			
			}
			else if(node_name.compare("vendorId")== 0)
			{				
				itemGroupObj.setVendor_id(node_value.GetAsDouble());			
			}
			else if(node_name.compare("categoryId")== 0)
			{				
				itemGroupObj.setclass_id(node_value.GetAsDouble());				
			}
			else if(node_name.compare("groupId")== 0)
			{				
				itemGroupObj.setObject_id(node_value.GetAsDouble());	
				pbObjValue.setObject_id(node_value.GetAsDouble());	
			}
			else if(node_name.compare("comments")== 0)
			{				
				itemGroupObj.setComment(node_value);			
			}
			else if(node_name.compare("brandId")== 0)
			{				
				itemGroupObj.setBrandId(node_value.GetAsDouble());			
			}
			else if(node_name.compare("manufacturerId")== 0)
			{				
				itemGroupObj.setManufacturerId(node_value.GetAsDouble());			
			}
			else if(node_name.compare("allListsItemCount")== 0)
			{				
				itemGroupObj.setChildCount(node_value.GetAsNumber());			
			}
			else if(node_name.compare("sectionResultId")== 0)
			{				
				pbObjValue.setPub_object_id(node_value.GetAsDouble());		
				itemGroupObj.setSectionResultId(node_value.GetAsDouble());
			}
			else if(node_name.compare("typeId")== 0)
			{				
				pbObjValue.setobject_type_id(node_value.GetAsDouble());		
				itemGroupObj.setObject_type_id(node_value.GetAsDouble());	
			}
			else if(node_name.compare("star")== 0)
			{			
				if(node_value == ("true") )
				{
					pbObjValue.setStarredFlag1(kTrue);	
					itemGroupObj.setIsStared(1);
				}
				else
				{
					pbObjValue.setStarredFlag1(kFalse);	
					itemGroupObj.setIsStared(0);
				}
			}
			else if(node_name.compare("newProduct")== 0)
			{				
				if(node_value == ("true"))
				{
					pbObjValue.setNew_product(kTrue);
					itemGroupObj.setIsNewProduct(1);
				}
				else
				{
					pbObjValue.setNew_product(kFalse);	
					itemGroupObj.setIsNewProduct(0);
				}
			}
			else if(node_name.compare("assets")== 0)
			{
				vector<CAssetValue> vectorAssetValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CAssetValue assetValue;
					ParseAssetsJSON(((*i)->at(arrayCounter)), assetValue);
					vectorAssetValue.push_back(assetValue);
				}
				itemGroupObj.setAssets(vectorAssetValue);
			}			
			else if(node_name.compare("values")== 0)
			{				
				vector<ItemGroupValue> vectorObjectValuesValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					ItemGroupValue objectValuesValue;
					ParseItemGroupValueJSON(((*i)->at(arrayCounter)), objectValuesValue);
					vectorObjectValuesValue.push_back(objectValuesValue);

					if(objectValuesValue.getElementId() == itemGroupDescriptionAttrId)
					{
						Description = (objectValuesValue.getObjectValue());
						itemGroupObj.setName(objectValuesValue.getObjectValue());
					}
				}
				itemGroupObj.setObjectValues(vectorObjectValuesValue);
			}
			else if(node_name.compare("lists")== 0)
			{				
				vector<CItemTableValue> vectorObjectLists;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CItemTableValue objectTableValue;
					ParseTableValueJSON(((*i)->at(arrayCounter)), objectTableValue);
					vectorObjectLists.push_back(objectTableValue);
				}
				itemGroupObj.setLists(vectorObjectLists);
				itemGroupObj.setChildCount((*i)->size());
			}
			else if(node_name.compare("tables")== 0)
			{				
				vector<CAdvanceTableScreenValue> vectorObjectAdvTables;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CAdvanceTableScreenValue objectAdvTableValue;
					ParseAdvanceTableValueJSON(((*i)->at(arrayCounter)), objectAdvTableValue);
					vectorObjectAdvTables.push_back(objectAdvTableValue);
				}
				itemGroupObj.setAdvanceTables(vectorObjectAdvTables);
			}

			itemGroupObj.setParent_id(sectionId);
			
		}
		else if( isProduct == 0)
		{
			
			if(node_name.compare("number")== 0)
			{				
				itemModelObj.setItemNo(node_value);			
			}
			else if(node_name.compare("itemId")== 0)
			{				
				itemModelObj.setItemID(node_value.GetAsDouble());	
				pbObjValue.setObject_id(node_value.GetAsDouble());
			}
			else if(node_name.compare("categoryId")== 0)
			{				
				itemModelObj.setClassID(node_value.GetAsDouble());			
			}
			else if(node_name.compare("statusId")== 0)
			{				
				itemModelObj.setStageId(node_value.GetAsDouble());			
			}
			else if(node_name.compare("brandId")== 0)
			{				
				itemModelObj.setBrandID(node_value.GetAsDouble());			
			}
			else if(node_name.compare("manufacturerId")== 0)
			{				
				itemModelObj.setManufacturerID(node_value.GetAsDouble());			
			}
			else if(node_name.compare("vendorId")== 0)
			{				
				itemModelObj.setSupplierID(node_value.GetAsDouble());			
			}
			else if(node_name.compare("allListsItemCount")== 0)
			{				
				itemModelObj.setChildItemCount(node_value.GetAsNumber());			
			}
			else if(node_name.compare("assets")== 0)
			{
				vector<CAssetValue> vectorAssetValue;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CAssetValue assetValue;
					ParseAssetsJSON(((*i)->at(arrayCounter)), assetValue);
					vectorAssetValue.push_back(assetValue);
				}
				itemModelObj.setAssetList(vectorAssetValue);
			}			
			else if(node_name.compare("values")== 0)
			{				
				vector<ItemValue> vectorItemValues;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					ItemValue itemValueObj;
					ParseItemValueJSON(((*i)->at(arrayCounter)), itemValueObj);
					vectorItemValues.push_back(itemValueObj);
										
					if(itemValueObj.getFieldId() == itemDescriptionAttrId)
					{
						Description = (itemValueObj.getFieldValue());
						itemModelObj.setItemDesc(Description);
					}
                    
                    if(attributeIdStr!= "" && itemValueObj.getFieldId() == attributeIdStr.GetAsDouble())
                    {
                        itemModelObj.setAttributeValueStr(itemValueObj.getFieldValue());
                    }
				}
				itemModelObj.setValues(vectorItemValues);
			}
			else if(node_name.compare("lists")== 0)
			{				
				vector<CItemTableValue> vectorObjectLists;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CItemTableValue objectTableValue;
					ParseTableValueJSON(((*i)->at(arrayCounter)), objectTableValue);
					vectorObjectLists.push_back(objectTableValue);
				}
				itemModelObj.setLists(vectorObjectLists);
				itemModelObj.setChildItemCount((*i)->size());
			}
			else if(node_name.compare("tables")== 0)
			{				
				vector<CAdvanceTableScreenValue> vectorObjectAdvTables;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					CAdvanceTableScreenValue objectAdvTableValue;
					ParseAdvanceTableValueJSON(((*i)->at(arrayCounter)), objectAdvTableValue);
					vectorObjectAdvTables.push_back(objectAdvTableValue);
				}
				itemModelObj.setAdvanceTables(vectorObjectAdvTables);
			}
            else if(node_name.compare("attributeGroups")== 0)
            {
                vector<AttributeGroup> vectorObjectAttributeGroups;
                ParseAttributeGroupValueJSONForSectionResult( **i, vectorObjectAttributeGroups);
                itemModelObj.setAttributeGroups(vectorObjectAttributeGroups);
            }
			else if(node_name.compare("sectionResultId")== 0)
			{				
				pbObjValue.setPub_object_id(node_value.GetAsDouble());	
				itemModelObj.setSectionResultId(node_value.GetAsDouble());
			}	
			else if(node_name.compare("typeId")== 0)
			{				
				pbObjValue.setobject_type_id(node_value.GetAsDouble());	
				itemModelObj.setParent_type_id(node_value.GetAsDouble());	
			}
			else if(node_name.compare("star")== 0)
			{			
				if(node_value == ("true") )
				{
					pbObjValue.setStarredFlag1(kTrue);	
					itemModelObj.setIsStared(1);
				}
				else
				{
					pbObjValue.setStarredFlag1(kFalse);	
					itemModelObj.setIsStared(0);
				}
			}
			else if(node_name.compare("newProduct")== 0)
			{				
				if(node_value == ("true"))
				{
					pbObjValue.setNew_product(kTrue);
					itemModelObj.setIsNewProduct(1);
				}
				else
				{
					pbObjValue.setNew_product(kFalse);	
					itemModelObj.setIsNewProduct(0);
				}
			}
			itemModelObj.setParentId(sectionId);
			
		}				

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }

	pbObjValue.setObjectValue(itemGroupObj);
	pbObjValue.setItemModel(itemModelObj);
	pbObjValue.setisProduct(isProduct);

	if(isProduct == 1)
	{
		PMString pubName = itemGroupObj.get_number();
		if(Description != "")
			pubName.Append(" : " + Description);
		pbObjValue.setName(pubName);
	}
	else if(isProduct == 0)
	{
		PMString pubName = itemModelObj.getItemNo();
		if(Description != "")
			pubName.Append(" : " + Description);
		pbObjValue.setName(pubName);		
	}
}

// Function to Parse List inside ItemGroup or Item. (Both the List are of same type so common function for both ItemGroup and Item)
void jsonParser::ParseTableValueJSON(JSONNode & nodeObj, CItemTableValue &objTableValue)
{
	JSONNode::json_iterator i = nodeObj.begin();

	 while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);

		if(node_name.compare("name")== 0)
		{				
			objTableValue.setName(node_value);			
		}
		else if(node_name.compare("typeId")== 0)
		{				
			objTableValue.setTableTypeID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("listId")== 0)
		{				
			objTableValue.setTableID(node_value.GetAsDouble());				
		}
		if(node_name.compare("description")== 0)
		{				
			objTableValue.setDescription(node_value);			
		}
		if(node_name.compare("stencilName")== 0)
		{				
			objTableValue.setstencil_name(node_value);			
		}
		else if(node_name.compare("columns")== 0)
		{
			vector<Column> vectorColValue;
			vector<double> vecttableHeader;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				Column colValue;				
				ParseTableColumnJSON(((*i)->at(arrayCounter)), colValue);
				vectorColValue.push_back(colValue);
				vecttableHeader.push_back(colValue.getFieldId());
			}
			objTableValue.setColumn(vectorColValue);
			objTableValue.setTableHeader(vecttableHeader);
		}
		else if(node_name.compare("rows")== 0)
		{
			vector<vector<PMString> > tableData;
			vector<double> itemIds;
            vector<ChildItemModel> childItems;
            
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				vector<PMString> rowValue;	
				double rowItemid;
                ChildItemModel childItem;
				ParseTableRowJSON(((*i)->at(arrayCounter)), rowValue, rowItemid);
                ParseTableRowJSONForChildItem(((*i)->at(arrayCounter)), childItem);
				tableData.push_back(rowValue);
				itemIds.push_back(rowItemid);
                childItems.push_back(childItem);
			}
			objTableValue.setTableData(tableData);
			objTableValue.setItemIds(itemIds);
            objTableValue.setChildItems(childItems);
		}
		else if(node_name.compare("customColumns")== 0)
		{
			vector<Column> vectorColValue;
			vector<double> vecttableHeader;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				Column colValue;				
				ParseTableColumnJSON(((*i)->at(arrayCounter)), colValue);
				vectorColValue.push_back(colValue);
				vecttableHeader.push_back(colValue.getFieldId());
			}
			objTableValue.setCustomColumn(vectorColValue);
			objTableValue.setCustomTableHeader(vecttableHeader);
		}
		else if(node_name.compare("customRows")== 0)
		{
			vector<vector<PMString> > tableData;
			vector<double> itemIds;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				vector<PMString> rowValue;	
				double rowItemid;
				ParseTableRowJSON(((*i)->at(arrayCounter)), rowValue, rowItemid);
				tableData.push_back(rowValue);
				itemIds.push_back(rowItemid);
			}
			objTableValue.setCustomTableData(tableData);
			objTableValue.setCustomItemIds(itemIds);
		}
		if(node_name.compare("notes")== 0)
		{	
			//log("notes");
			vector<PMString> notesValue;				
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				PMString value("");
				value.SetCString((((*i)->at(arrayCounter)).as_string()).c_str(), PMString::kUnknownEncoding);
				if(value == "null")
					value = "";
				notesValue.push_back(value);
				//log("Notes");
				//log(value.GetUTF8String());
			}
			objTableValue.setNotesList(notesValue);			
		}



	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }
}

void jsonParser::ParseTableColumnJSON(JSONNode & nodeObj, Column &colValue)
{
	JSONNode::json_iterator i = nodeObj.begin();

	 while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);

		if(node_name.compare("fieldName")== 0)
		{				
			colValue.setFieldName(node_value);			
		}
		else if(node_name.compare("fieldId")== 0)
		{				
			colValue.setFieldId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("index")== 0)
		{				
			colValue.setIndex(node_value.GetAsNumber());				
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }
}

void jsonParser::ParseTableRowJSON(JSONNode & nodeObj, vector<PMString> &rowValue, double &rowitemId)
{
	JSONNode::json_iterator i = nodeObj.begin();
	while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);

		if(node_name.compare("values")== 0)
		{				
			vector<PMString> stringValueArray;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				PMString value("");
				value.SetCString((((*i)->at(arrayCounter)).as_string()).c_str(), PMString::kUnknownEncoding);
				if(value == "null")
					value = "";

				rowValue.push_back(value);
				//log("Row Value: ");
				//log(value.GetUTF8String());
			}
		}

		else if(node_name.compare("itemId")== 0)
		{					
			rowitemId = node_value.GetAsDouble();
		}

	  }catch(...)
		{
			log("exception caught in jsonParser::ParseConfigJSON");
		}
		++i;
	}

}

// Function to parse an Array of Strings
void jsonParser::ParseStringArrayJSON(JSONNode & nodeObj, vector<PMString> &stringArrayValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
	while (i != nodeObj.end())
	{		
      try
	  {

        //std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		if( ((*i) != NULL) )
		{
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
		}
		if(node_value == "null")
			node_value = "";
		stringArrayValue.push_back(node_value);

	  }catch(...)
	    {
			log("exception caught in jsonParser::ParseStringArrayJSON");
		}
	  ++i;
	}
}

void jsonParser::ParseStringJSON(JSONNode & nodeObj, PMString &stringValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
	//while (i != nodeObj.end())
	{		
      try
	  {

        //std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		if( ((*i) != NULL) )
		{
			node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
		}
		if(node_value == "null")
			node_value = "";
		stringValue = node_value;
		//log("Inside ParseStringJSON");
		//log(stringValue.GetUTF8String());

	  }catch(...)
	    {
			log("exception caught in jsonParser::ParseStringJSON");
		}
	  ++i;
	}
}

void jsonParser::ParseItemValueJSON(JSONNode & nodeObj, ItemValue &itemValueObj)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("value")== 0)
		{				
			if(node_value == "null")
				node_value = "";
			itemValueObj.setFieldValue(node_value);			
		}
		else if(node_name.compare("fieldName")== 0)
		{				
			itemValueObj.setFieldName(node_value);			
		}
		else if(node_name.compare("fieldId")== 0)
		{				
			itemValueObj.setFieldId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("picklistValueIdsString")== 0)
		{				
			itemValueObj.setPicklistValueIdsString(node_value);			
		}

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseItemGroupValueJSON");
	  }
		++i;
    }
}

bool16 jsonParser::parseItemGroupDetails(PMString inputJson, CObjectValue &itemGroupObjectValue)
{
	//itemGroup
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("Invalid parseItemGroupDetails Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   log("current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();					
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("itemGroup") == 0)
					{	
						ParseItemGroupDetailsJson(**i, itemGroupObjectValue);												
					}
				}				
				++i;
			}
			result = kTrue;
		}catch (...){
				log("Exception caught in parseItemGroupDetails");
				return result;
		}
        json_delete(current);
	}
		return result;
}

bool16 jsonParser:: parseItemDetails(PMString inputJson, CItemModel &itemObjectValue)
{
	//itemGroup
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		log("Invalid parseItemGroupDetails Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   CA("current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();					
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("item") == 0)
					{	
						ParseItemDetailsJson(**i, itemObjectValue);												
					}
				}				
				++i;
			}
			result = kTrue;

		}catch (...){
				log("Exception caught in parseItemGroupDetails");
				return result;
		}
        json_delete(current);
	}
		return result;
}


void jsonParser::ParseItemGroupDetailsJson(JSONNode & nodeObj, CObjectValue &itemGroupObjectValue)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}
	
    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());
				
		if(node_name.compare("number")== 0)
		{				
			itemGroupObjectValue.setNumber(node_value);			
		}
		else if(node_name.compare("vendorId")== 0)
		{				
			itemGroupObjectValue.setVendor_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("categoryId")== 0)
		{				
			itemGroupObjectValue.setclass_id(node_value.GetAsDouble());				
		}
		else if(node_name.compare("groupId")== 0)
		{				
			itemGroupObjectValue.setObject_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("comments")== 0)
		{				
			itemGroupObjectValue.setComment(node_value);			
		}
		else if(node_name.compare("brandId")== 0)
		{				
			itemGroupObjectValue.setBrandId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("manufacturerId")== 0)
		{				
			itemGroupObjectValue.setManufacturerId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("allListsItemCount")== 0)
		{				
			itemGroupObjectValue.setChildCount(node_value.GetAsNumber());			
		}
		else if(node_name.compare("sectionResultId")== 0)
		{				
			itemGroupObjectValue.setSectionResultId(node_value.GetAsDouble());			
		}

		else if(node_name.compare("assets")== 0)
		{
			vector<CAssetValue> vectorAssetValue;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				CAssetValue assetValue;
				ParseAssetsJSON(((*i)->at(arrayCounter)), assetValue);
				vectorAssetValue.push_back(assetValue);
			}
			itemGroupObjectValue.setAssets(vectorAssetValue);
		}			
		else if(node_name.compare("values")== 0)
		{				
			vector<ItemGroupValue> vectorObjectValuesValue;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				ItemGroupValue objectValuesValue;
				ParseItemGroupValueJSON(((*i)->at(arrayCounter)), objectValuesValue);
				vectorObjectValuesValue.push_back(objectValuesValue);

				/*if(objectValuesValue.getElementId() == itemGroupDescriptionAttrId)
				{
					Description = (objectValuesValue.getObjectValue());
					itemGroupObjectValue.setName(objectValuesValue.getObjectValue());
				}*/
			}
			itemGroupObjectValue.setObjectValues(vectorObjectValuesValue);
		}
		else if(node_name.compare("lists")== 0)
		{				
			vector<CItemTableValue> vectorObjectLists;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				CItemTableValue objectTableValue;
				ParseTableValueJSON(((*i)->at(arrayCounter)), objectTableValue);
				vectorObjectLists.push_back(objectTableValue);
			}
			itemGroupObjectValue.setLists(vectorObjectLists);
			itemGroupObjectValue.setChildCount((*i)->size());
		}		
		else if(node_name.compare("typeId")== 0)
		{				
			itemGroupObjectValue.setObject_type_id(node_value.GetAsDouble());	
		}
		else if(node_name.compare("star")== 0)
		{			
			if(node_value == ("true") )
			{				
				itemGroupObjectValue.setIsStared(1);
			}
			else
			{				
				itemGroupObjectValue.setIsStared(0);
			}
		}
		else if(node_name.compare("newProduct")== 0)
		{				
			if(node_value == ("true"))
			{				
				itemGroupObjectValue.setIsNewProduct(1);
			}
			else
			{			
				itemGroupObjectValue.setIsNewProduct(0);
			}
		}
							

	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }


}
void jsonParser::ParseItemDetailsJson(JSONNode & nodeObj, CItemModel &itemObjectValue)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("number")== 0)
		{				
			itemObjectValue.setItemNo(node_value);			
		}
		else if(node_name.compare("itemId")== 0)
		{				
			itemObjectValue.setItemID(node_value.GetAsDouble());	
			//pbObjValue.setObject_id(node_value.GetAsDouble());
		}
		else if(node_name.compare("categoryId")== 0)
		{				
			itemObjectValue.setClassID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("statusId")== 0)
		{				
			itemObjectValue.setStageId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("brandId")== 0)
		{				
			itemObjectValue.setBrandID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("manufacturerId")== 0)
		{				
			itemObjectValue.setManufacturerID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("supplierId")== 0)
		{				
			itemObjectValue.setSupplierID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("allListsItemCount")== 0)
		{				
			itemObjectValue.setChildItemCount(node_value.GetAsNumber());			
		}
		else if(node_name.compare("assets")== 0)
		{
			vector<CAssetValue> vectorAssetValue;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				CAssetValue assetValue;
				ParseAssetsJSON(((*i)->at(arrayCounter)), assetValue);
				vectorAssetValue.push_back(assetValue);
			}
			itemObjectValue.setAssetList(vectorAssetValue);
		}			
		else if(node_name.compare("values")== 0)
		{				
			vector<ItemValue> vectorItemValues;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				ItemValue itemValueObj;
				ParseItemValueJSON(((*i)->at(arrayCounter)), itemValueObj);
				vectorItemValues.push_back(itemValueObj);
										
				/*if(itemValueObj.getFieldId() == itemDescriptionAttrId)
				{
					Description = (itemValueObj.getFieldValue());
					itemModelObj.setItemDesc(Description);
				}*/
			}
			itemObjectValue.setValues(vectorItemValues);
		}
		else if(node_name.compare("lists")== 0)
		{				
			vector<CItemTableValue> vectorObjectLists;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				CItemTableValue objectTableValue;
				ParseTableValueJSON(((*i)->at(arrayCounter)), objectTableValue);
				vectorObjectLists.push_back(objectTableValue);
			}
			itemObjectValue.setLists(vectorObjectLists);
			itemObjectValue.setChildItemCount((*i)->size());
		}
		else if(node_name.compare("sectionResultId")== 0)
		{				
			itemObjectValue.setSectionResultId(node_value.GetAsDouble());			
		}		
		else if(node_name.compare("typeId")== 0)
		{				
			itemObjectValue.setParent_type_id(node_value.GetAsDouble());	
		}
		else if(node_name.compare("star")== 0)
		{			
			if(node_value == ("true") )
			{				
				itemObjectValue.setIsStared(1);
			}
			else
			{				
				itemObjectValue.setIsStared(0);
			}
		}
		else if(node_name.compare("newProduct")== 0)
		{				
			if(node_value == ("true"))
			{				
				itemObjectValue.setIsNewProduct(1);
			}
			else
			{			
				itemObjectValue.setIsNewProduct(0);
			}
		}
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }

}


bool16 jsonParser::parseLoginProperties(PMString inputJson, VectorLoginInfoPtr vectloginInfoObject)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		log("Invalid parseLoginProperties Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   log("current == NULL");
			   return result;
		   }


		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					if(node_name.compare("environments") == 0)
					{												
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							LoginInfoValue loginInfoObject;
							ParseLoginInfoJson(((*i)->at(arrayCounter)), loginInfoObject);
							vectloginInfoObject->push_back(loginInfoObject);
						}						
					}
				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			result = kTrue;
		   

		}catch (...){
			log("false");
			return result;
		}
        json_delete(current);
	}
	return result;
}


void jsonParser::ParseLoginInfoJson(JSONNode & nodeObj, LoginInfoValue &loginInfoObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("cmurl")== 0)
		{				
			loginInfoObject.setCmurl(node_value);			
		}
		else if(node_name.compare("envName")== 0)
		{				
			loginInfoObject.setEnvName(node_value);				
		}
		else if(node_name.compare("username")== 0)
		{				
			loginInfoObject.setUsername(node_value);				
		}
		else if(node_name.compare("password")== 0)
		{				
			loginInfoObject.setPassword(node_value);			
		}
		else if(node_name.compare("dbVendorName")== 0)
		{				
			loginInfoObject.setVendorName(node_value);			
		}
		else if(node_name.compare("dbVersion")== 0)
		{				
			loginInfoObject.setVersion(node_value);			
		}
		else if(node_name.compare("dbServerName")== 0)
		{				
			loginInfoObject.setServerName(node_value);			
		}
		else if(node_name.compare("dbServerPort")== 0)
		{				
			loginInfoObject.setServerPort(node_value.GetAsNumber());			
		}
		else if(node_name.compare("dbSchema")== 0)
		{				
			loginInfoObject.setSchema(node_value);			
		}
		else if(node_name.compare("dbUserName")== 0)
		{				
			loginInfoObject.setDbUserName(node_value);			
		}
		else if(node_name.compare("dbPassword")== 0)
		{				
			loginInfoObject.setDbPassword(node_value);			
		}
						
		else if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
		{					
			std::string node_name = (*i) -> name();
			//CA(((PMString)(*i)->name()).c_str());
			//log(((string)(*i)->name()).c_str());
			if(node_name.compare("clientlist") == 0)
			{											
				vector<ClientInfoValue> vectClientInfo;
				for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
				{
					ClientInfoValue clientInfoObject;
					ParseClientInfoJson(((*i)->at(arrayCounter)), clientInfoObject);
					vectClientInfo.push_back(clientInfoObject);
				}				
				loginInfoObject.setClientList(vectClientInfo);
			}
			
		}		
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseLoginInfoJson");
	  }
		++i;
    }

}

void jsonParser::ParseClientInfoJson(JSONNode & nodeObj, ClientInfoValue &clientInfoObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
		//log(((*i) -> as_string()).c_str());

		if(node_name.compare("clientno")== 0)
		{				
			clientInfoObject.setClientno(node_value);			
		}
		else if(node_name.compare("assetserverpath")== 0)
		{				
			clientInfoObject.setAssetserverpath(node_value);			
		}		
		else if(node_name.compare("documentPath")== 0)
		{				
			clientInfoObject.setDocumentPath(node_value);			
		}
		else if(node_name.compare("language")== 0)
		{				
			clientInfoObject.setLanguage(node_value);			
		}
		else if(node_name.compare("project")== 0)
		{				
			clientInfoObject.setProject(node_value);			
		}
		else if(node_name.compare("isActive")== 0)
		{				
			if(node_value == "true")
				clientInfoObject.setIsActive(kTrue);	
			else
				clientInfoObject.setIsActive(kFalse);	
		}
		else if(node_name.compare("isItemTablesasTabbedText")== 0)
		{				
			clientInfoObject.setisItemTablesasTabbedText(node_value.GetAsNumber());			
		}
		else if(node_name.compare("isAddTableHeaders")== 0)
		{				
			clientInfoObject.setisAddTableHeaders(node_value.GetAsNumber());			
		}
		else if(node_name.compare("assetStatus")== 0)
		{				
			clientInfoObject.setAssetStatus(node_value.GetAsNumber());			
		}
		else if(node_name.compare("logLevel")== 0)
		{				
			clientInfoObject.setLogLevel(node_value.GetAsNumber());			
		}
		else if(node_name.compare("missingFlag")== 0)
		{				
			clientInfoObject.setMissingFlag(node_value.GetAsNumber());			
		}
		else if(node_name.compare("sectionID")== 0)
		{				
			clientInfoObject.setSectionID(node_value);			
		}
		else if(node_name.compare("isONESource")== 0)
		{				
			clientInfoObject.setIsONESource(node_value);			
		}
		else if(node_name.compare("defaultFlow")== 0)
		{				
			clientInfoObject.setdefaultFlow(node_value.GetAsNumber());	
		}
		else if(node_name.compare("displayPartnerImages")== 0)
		{				
			clientInfoObject.setDisplayPartnerImages(node_value.GetAsNumber());	
		}
		else if(node_name.compare("displayPickListImages")== 0)
		{				
			clientInfoObject.setDisplayPickListImages(node_value.GetAsNumber());	
		}
		else if(node_name.compare("isShowObjectCount")== 0)
		{				
			clientInfoObject.setShowObjectCountFlag(node_value.GetAsNumber());	
		}
		else if(node_name.compare("byPassForSingleSelectionSpray")== 0)
		{				
			clientInfoObject.setByPassForSingleSelectionSprayFlag(node_value.GetAsNumber());	
		}
		else if(node_name.compare("horizontalSpacing")== 0)
		{				
			clientInfoObject.setHorizontalSpacing(node_value.GetAsNumber());	
		}
		else if(node_name.compare("verticalSpacing")== 0)
		{				
			clientInfoObject.setVerticalSpacing(node_value.GetAsNumber());	
		}
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseClientInfoJson");
	  }
		++i;
    }

}


bool16 jsonParser::writeLoginProperties(PMString FilePath, vector<LoginInfoValue>* vectloginInfoObject)
{
	
	bool16 result = kFalse;
	std::string text;

	string tempString(FilePath.GetPlatformString());
	char * filepathstr = const_cast<char *> (tempString.c_str());// GetFilePath();

	FILE *file = fopen( filepathstr, "w" );
	if ( !file )
      return result;
    fclose( file );

    ofstream outFile ;
    outFile.open( filepathstr , ios::app);
    if(!outFile)
    {
        //CA("Unable to open Log File");
        return result;
    }
    
	JSONNode* n =  new JSONNode(JSON_NODE);
	
	JSONNode* c = new JSONNode(JSON_ARRAY);
	c->set_name("environments");

	if(vectloginInfoObject != NULL && vectloginInfoObject->size() > 0)
	{
		for(int count =0; count < vectloginInfoObject->size(); count++ )
		{
			LoginInfoValue lnObjVal = vectloginInfoObject->at(count);
			
			JSONNode* envnode= new JSONNode(JSON_NODE);

			envnode->push_back(new JSONNode("envName", lnObjVal.getEnvName().GetPlatformString()));
			envnode->push_back(new JSONNode("cmurl", lnObjVal.getCmurl().GetPlatformString()));
			envnode->push_back(new JSONNode("username", lnObjVal.getUsername().GetPlatformString()));
			envnode->push_back(new JSONNode("password", lnObjVal.getPassword().GetPlatformString()));
			envnode->push_back(new JSONNode("dbVendorName", lnObjVal.getVendorName().GetPlatformString()));
			envnode->push_back(new JSONNode("dbVersion", lnObjVal.getVersion().GetPlatformString()));
			envnode->push_back(new JSONNode("dbServerName", lnObjVal.getServerName().GetPlatformString()));
			envnode->push_back(new JSONNode("dbServerPort", convert_to_string(lnObjVal.getServerPort())));
			envnode->push_back(new JSONNode("dbSchema", lnObjVal.getSchema().GetPlatformString()));
			envnode->push_back(new JSONNode("dbUserName", lnObjVal.getDbUserName().GetPlatformString()));
			envnode->push_back(new JSONNode("dbPassword", lnObjVal.getDbPassword().GetPlatformString()));
			
			if(lnObjVal.getClientList().size() > 0)
			{
				JSONNode* cl = new JSONNode(JSON_ARRAY);
				cl->set_name("clientlist");

				for(int i=0; i < lnObjVal.getClientList().size(); i++)
				{
					ClientInfoValue clientInfoObj = lnObjVal.getClientList().at(i);

					JSONNode* clientnode= new JSONNode(JSON_NODE);

					clientnode->push_back(new JSONNode("clientno", clientInfoObj.getClientno().GetPlatformString()));
					clientnode->push_back(new JSONNode("assetserverpath", clientInfoObj.getAssetserverpath().GetPlatformString()));				

					clientnode->push_back(new JSONNode("documentPath", clientInfoObj.getDocumentPath().GetPlatformString()));
					clientnode->push_back(new JSONNode("language", clientInfoObj.getLanguage().GetPlatformString()));
					clientnode->push_back(new JSONNode("project", clientInfoObj.getProject().GetPlatformString()));
					clientnode->push_back(new JSONNode("isActive", (bool)clientInfoObj.getIsActive()));
					clientnode->push_back(new JSONNode("isItemTablesasTabbedText", convert_to_string(clientInfoObj.getisItemTablesasTabbedText())));
					clientnode->push_back(new JSONNode("isAddTableHeaders",convert_to_string(clientInfoObj.getisAddTableHeaders())));
					clientnode->push_back(new JSONNode("assetStatus", convert_to_string(clientInfoObj.getAssetStatus())));
					clientnode->push_back(new JSONNode("logLevel", convert_to_string(clientInfoObj.getLogLevel())));
					clientnode->push_back(new JSONNode("missingFlag", convert_to_string(clientInfoObj.getMissingFlag())));
					clientnode->push_back(new JSONNode("sectionID", clientInfoObj.getSectionID().GetPlatformString()));
					clientnode->push_back(new JSONNode("isONESource", clientInfoObj.getIsONESource().GetPlatformString()));
					clientnode->push_back(new JSONNode("defaultFlow", convert_to_string(clientInfoObj.getdefaultFlow())));
					clientnode->push_back(new JSONNode("displayPartnerImages", convert_to_string(clientInfoObj.getDisplayPartnerImages())));
					clientnode->push_back(new JSONNode("displayPickListImages", convert_to_string(clientInfoObj.getDisplayPickListImages())));
					clientnode->push_back(new JSONNode("isShowObjectCount", convert_to_string(clientInfoObj.getShowObjectCountFlag())));
					clientnode->push_back(new JSONNode("byPassForSingleSelectionSpray", convert_to_string(clientInfoObj.getByPassForSingleSelectionSprayFlag())));
					clientnode->push_back(new JSONNode("horizontalSpacing", convert_to_string(clientInfoObj.getHorizontalSpacing())));
					clientnode->push_back(new JSONNode("verticalSpacing", convert_to_string(clientInfoObj.getVerticalSpacing())));

					cl->push_back(clientnode);
				}
				envnode->push_back(cl);
				
			}
			c->push_back(envnode);
		}
	}
	
	n->push_back(c);
	std::string jc = (std::string)n->write_formatted();
    const char * strPtr = (char *)jc.c_str();
	//log(jc.c_str());
	//fprintf(file, strPtr);
	//fclose( file );
    outFile<<jc<<endl;
	outFile.close();
    
	delete n;
	//fclose( file );
	result = kTrue;
	return result;
}


bool16 jsonParser::parseClientValues(PMString inputJson, vector<CClientModel> * vectClientModel)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("parseClientValues Invalid Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   CA("parseClientValues current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				std::string node_name1 = (*i) -> name();
				//CA(node_name.c_str());
				if(node_name1.compare("error") == 0)
				{
					result = kFalse;
				}
				else if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();					
					if(node_name.compare("clients") == 0)
					{
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							CClientModel clientModelObj;
							ParseClientModelJson(((*i)->at(arrayCounter)), clientModelObj);
							vectClientModel->push_back(clientModelObj);
							
						}			
						result = kTrue;
					}					
				}		

				++i;
			}
			
		}catch (...){
				CA("Exception caught in parseClientValues");
				return result;
		}
        json_delete(current);
	}
	return result;
}


void jsonParser::ParseClientModelJson(JSONNode & nodeObj, CClientModel &clientModelObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("clientId")== 0)
		{				
			clientModelObject.setClient_id(node_value.GetAsDouble());			
		}
		else if(node_name.compare("name")== 0)
		{				
			clientModelObject.setName(node_value);				
		}
		else if(node_name.compare("addressId")== 0)
		{				
			clientModelObject.setAddress_id(node_value.GetAsDouble());				
		}
		else if(node_name.compare("cmContext")== 0)
		{				
			clientModelObject.setCm_context(node_value);			
		}
		else if(node_name.compare("assetsContext")== 0)
		{				
			clientModelObject.setAssets_context(node_value);			
		}
		else if(node_name.compare("websourceUrl")== 0)
		{				
			clientModelObject.setWebsource_url(node_value);			
		}		
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseClientModelJson");
	  }
		++i;
    }

}


bool16 jsonParser::parsePartners(PMString inputJson, PartnerCacheValue &partnerCacheValueObj)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		log("parsePartners Invalid Json");
		return result;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());		
	    try
		{
		   if(current == NULL)
		   {
			   log("parsePartners current == NULL");
			   return kFalse;
		   }		   
		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				std::string node_name1 = (*i) -> name();
				//CA(node_name.c_str());
				if(node_name1.compare("error") == 0)
				{
					result = kFalse;
				}
				else if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{					
					std::string node_name = (*i) -> name();					
					if(node_name.compare("brands") == 0)
					{
						vector<PartnerValue> brands;
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							PartnerValue partnerValueObj;
							ParsePartnerValueJson(((*i)->at(arrayCounter)), partnerValueObj);
							brands.push_back(partnerValueObj);							
						}
						partnerCacheValueObj.setBrands(brands);
						result = kTrue;
					}	
					else if(node_name.compare("manufacturers") == 0)
					{												
						vector<PartnerValue> manufacturers;
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							PartnerValue partnerValueObj;
							ParsePartnerValueJson(((*i)->at(arrayCounter)), partnerValueObj);
							manufacturers.push_back(partnerValueObj);							
						}
						partnerCacheValueObj.setManufacturers(manufacturers);
						result = kTrue;						
					}
					else if(node_name.compare("vendors") == 0)
					{												
						vector<PartnerValue> vendors;
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							PartnerValue partnerValueObj;
							ParsePartnerValueJson(((*i)->at(arrayCounter)), partnerValueObj);
							vendors.push_back(partnerValueObj);							
						}
						partnerCacheValueObj.setVendors(vendors);
						result = kTrue;						
					}
				}		

				++i;
			}
			
		}catch (...){
				log("Exception caught in parsePartners");
				return result;
		}
        json_delete(current);
	}
	return result;
}

void jsonParser::ParsePartnerValueJson(JSONNode & nodeObj, PartnerValue &partnerValueObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("id")== 0)
		{				
			partnerValueObject.setId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("name")== 0)
		{				
			partnerValueObject.setName(node_value);				
		}
		else if(node_name.compare("displayPartner")== 0)
		{				
			partnerValueObject.setDisplayPartner(node_value.GetAsNumber());				
		}
		else if(node_name.compare("code")== 0)
		{				
			partnerValueObject.setCode(node_value);			
		}
		else if(node_name.compare("description1")== 0)
		{				
			partnerValueObject.setDescription1(node_value);			
		}
		else if(node_name.compare("description2")== 0)
		{				
			partnerValueObject.setDescription2(node_value);			
		}
		else if(node_name.compare("number")== 0)
		{				
			partnerValueObject.setNumber(node_value);			
		}
		else if(node_name.compare("imageFilename1")== 0)
		{				
			partnerValueObject.setImageFilename1(node_value);			
		}
		else if(node_name.compare("imageFilename2")== 0)
		{				
			partnerValueObject.setImageFilename2(node_value);			
		}
		else if(node_name.compare("imageFilename3")== 0)
		{				
			partnerValueObject.setImageFilename3(node_value);			
		}
		else if(node_name.compare("imageFilename4")== 0)
		{				
			partnerValueObject.setImageFilename4(node_value);			
		}
		else if(node_name.compare("imageFilename5")== 0)
		{				
			partnerValueObject.setImageFilename5(node_value);			
		}
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParsePartnerValueJson");
	  }
		++i;
    }

}


bool16 jsonParser::parseIndexReferences(PMString inputJson, vector<IndexReference>& indexReferenceVectorObj)
{
	bool16 result = kFalse;
	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		CA("Invalid parseIndexReferences Json");
		return result;
	}	
	else
	{
		JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());	
		try
		{
			if(current == NULL)
			{
				CA("current == NULL");
				return kFalse;
			}

			JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE) 
				{
					std::string node_name = (*i) -> name();
					if(node_name.compare("indexTerms") == 0 || node_name.compare("fieldSwapValues") == 0 || node_name.compare("refreshDataValues") == 0)
					{												
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							IndexReference indexReference;
							ParseIndexReferenceJSON(((*i)->at(arrayCounter)), indexReference);
							indexReferenceVectorObj.push_back(indexReference);
						}						
					}
				}

				++i;
			}

			result = kTrue;
		} 
		catch (...){
			CA("Exception caught in parseIndexReferences");
			return result;
		}
        json_delete(current);

	}

	return result;
 
}

void jsonParser::ParseIndexReferenceJSON(JSONNode & nodeObj, IndexReference &indexReference)
{
	JSONNode::json_iterator i = nodeObj.begin();
	PMString nullString("null");
    while (i != nodeObj.end())
	{		
      try
	  {
        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("sectionId")== 0)
		{				
			indexReference.setSectionID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("parentTypeId")== 0)
		{				
			indexReference.setParentTypeID(node_value.GetAsDouble());			
		}
		else if(node_name.compare("parentId")== 0)
		{				
			indexReference.setParentID(node_value.GetAsDouble());
		}
		else if(node_name.compare("childId")== 0)
		{				
			indexReference.setChildID(node_value.GetAsDouble());		
		}
		else if(node_name.compare("indexTerms")== 0)
		{				
			vector<PMString> indexTerms;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				PMString indexTerm("");
				indexTerm.SetCString((((*i)->at(arrayCounter)).as_string()).c_str(), PMString::kUnknownEncoding);

				if (indexTerm != nullString)
					indexTerms.push_back(indexTerm);
				else
					CA("null string");
			}
			indexReference.setIndexTerms(indexTerms);			
		}
		else if (node_name.compare("newValue") == 0)
		{
			indexReference.setNewValue(node_value);
		}
		else if (node_name.compare("oldFieldId") == 0)
		{
			indexReference.setOldFieldID(node_value.GetAsDouble());
		}
		else if (node_name.compare("newFieldId") == 0)
		{
			indexReference.setNewFieldID(node_value.GetAsDouble());
		}
		else if (node_name.compare("itemFlag") == 0)
		{
			if(node_value.Compare(kFalse, "false") == 0)
				indexReference.setIsItem(kFalse);
			else 
				indexReference.setIsItem(kTrue);
		}
		else if (node_name.compare("itemGroupFlag") == 0)
		{
			if(node_value.Compare(kFalse, "false") == 0)
				indexReference.setIsItemGroup(kFalse);
			else 
				indexReference.setIsItemGroup(kTrue);
		}
		else if (node_name.compare("sectionFlag") == 0)
		{
			if(node_value.Compare(kFalse, "false") == 0)
				indexReference.setIsSection(kFalse);
			else 
				indexReference.setIsSection(kTrue);
		}		
		else if (node_name.compare("listId") == 0)
		{
			indexReference.setTableId(node_value.GetAsDouble());
		}
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParseConfigJSON");
	  }
		++i;
    }
}


bool16 jsonParser::checkSessionTimeoutJson(PMString inputJson)
{
	bool16 isSessionTimeout = kFalse;

	bool a = json_is_valid(inputJson.GetPlatformString().c_str());
	if(!a)
	{
		//CA("Invalid Json");
		return isSessionTimeout;
	}	
	else
	{
	    JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
	    try
		{
		   if(current == NULL)
		   {
			  // CA("current == NULL");
			   return kFalse;
		   }

		   JSONNode::json_iterator i = current->begin();
			while (i != current->end())
			{				
				if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
				{			
					//CA("Got an Array or Object");
					std::string node_name = (*i) -> name();
					//CA(((PMString)(*i)->name()).c_str());
					//log(((string)(*i)->name()).c_str());
					vector<PMString> errorMessage;
					if(node_name.compare("error")== 0)
					{				
						vector<PMString> stringValueArray;
						for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
						{
							PMString value("");
							value.SetCString((((*i)->at(arrayCounter)).as_string()).c_str(), PMString::kUnknownEncoding);
							if(value == "null")
								value = "";
							//CA(value);
							errorMessage.push_back(value);
						}

						if(errorMessage.size() > 0)
						{
							if(errorMessage.at(0).Compare(kTrue , "session_timeout") == 0)
							{
								isSessionTimeout = kTrue;
							}
						}
					}
				}
				//log(((string)(*i)->name()).c_str());
				++i;
			}
			
		}catch (...){
				//CA("false");
				return isSessionTimeout;
		}
        json_delete(current);
	}	
	return isSessionTimeout;
}

string jsonParser::convert_to_string(int32 a)
{
    PMString str("");
    str.AppendNumber(a);
    return str.GetPlatformString();
    
}


void jsonParser::ParseAdvanceTableValueJSON(JSONNode & nodeObj, CAdvanceTableScreenValue &advanceTableValueObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("tableId")== 0)
		{				
			advanceTableValueObject.setTableId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("name")== 0)
		{				
			advanceTableValueObject.setTableName(node_value);				
		}
		else if(node_name.compare("headerRowCount")== 0)
		{				
			advanceTableValueObject.setHeaderRowCount(node_value.GetAsNumber());				
		}
		else if(node_name.compare("bodyRowCount")== 0)
		{				
			advanceTableValueObject.setBodyRowCount(node_value.GetAsNumber());				
		}
		else if(node_name.compare("columnCount")== 0)
		{				
			advanceTableValueObject.setColumnCount(node_value.GetAsNumber());				
		}
		else if(node_name.compare("tableTypeId")== 0)
		{				
			advanceTableValueObject.setTableTypeId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("languageId")== 0)
		{				
			advanceTableValueObject.setLanguageId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("sectionResultId")== 0)
		{				
			advanceTableValueObject.setSectionResultId(node_value.GetAsDouble());				
		}
        else if(node_name.compare("note1")== 0)
		{
			advanceTableValueObject.setNote1(node_value);
		}
        else if(node_name.compare("note2")== 0)
		{
			advanceTableValueObject.setNote2(node_value);
		}
        else if(node_name.compare("note3")== 0)
		{
			advanceTableValueObject.setNote3(node_value);
		}
        else if(node_name.compare("note4")== 0)
		{
			advanceTableValueObject.setNote4(node_value);
		}
        else if(node_name.compare("note5")== 0)
		{
			advanceTableValueObject.setNote5(node_value);
		}
		else if(node_name.compare("cells")== 0)
		{				
			vector<CAdvanceTableCellValue> vectorObjectAdvTableCells;
			for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
			{
				CAdvanceTableCellValue objectAdvTableCellValue;
				ParseAdvanceTableCellValueJSON(((*i)->at(arrayCounter)), objectAdvTableCellValue);
				vectorObjectAdvTableCells.push_back(objectAdvTableCellValue);
			}
			advanceTableValueObject.setCells(vectorObjectAdvTableCells);
		}
		
			
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParsePartnerValueJson");
	  }
		++i;
    }
}

void jsonParser::ParseAdvanceTableCellValueJSON(JSONNode & nodeObj, CAdvanceTableCellValue &advanceTableCellValueObject)
{
	JSONNode::json_iterator i = nodeObj.begin();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

    while (i != nodeObj.end())
	{		
      try
	  {

        std::string node_name = (*i) -> name();
	    //log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
        //log(((*i) -> as_string()).c_str());

		if(node_name.compare("rowId")== 0)
		{				
			advanceTableCellValueObject.setRowId(node_value.GetAsDouble());			
		}
		else if(node_name.compare("colId")== 0)
		{				
			advanceTableCellValueObject.setColId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("rowLength")== 0)
		{				
			advanceTableCellValueObject.setRowLength(node_value.GetAsNumber());				
		}
		else if(node_name.compare("colLength")== 0)
		{				
			advanceTableCellValueObject.setColLength(node_value.GetAsNumber());				
		}
		else if(node_name.compare("fieldId")== 0)
		{				
			advanceTableCellValueObject.setFieldId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("itemId")== 0)
		{				
			advanceTableCellValueObject.setItemId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("assetTypeId")== 0)
		{				
			advanceTableCellValueObject.setAssetTypeId(node_value.GetAsDouble());				
		}
		else if(node_name.compare("value")== 0)
		{				
			advanceTableCellValueObject.setValue(node_value);				
		}
        else if(node_name.compare("cellId")== 0)
		{
			advanceTableCellValueObject.setCellId(node_value.GetAsDouble());
		}
					
	  }catch(...)
	  {
		  log("exception caught in jsonParser::ParsePartnerValueJson");
	  }
		++i;
    }

}



void jsonParser::ParseDetailsJSON(JSONNode & nodeObj, Details &detailValue)
{
	JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end()){		

     try
	 {
        std::string node_name = (*i) -> name();
		//log(node_name.c_str());

		PMString node_value("");
		node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);		
		//log(((*i) -> as_string()).c_str());

        if(node_name.compare("fieldName")== 0)
		{	
			detailValue.setFieldName(node_value);			
		}
		else if(node_name.compare("languageId")== 0)
		{
			detailValue.setLanguageId(node_value.GetAsDouble());
		}
		else if(node_name.compare("value")== 0)
		{	
			detailValue.setValue(node_value);			
		}
		

	 }catch(...)
	  {
		  log("exception caught in jsonParser::ParseDetailsJSON");
	  }
 
        ++i;
    }
}

// Filter sprayer
bool16 jsonParser::parseFilterValue(PMString inputJson, vector<FilterValue> &filterObject)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseSysConfig Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    //CA(((PMString)(*i)->name()).c_str());
                    //log(((string)(*i)->name()).c_str());
                    if(node_name.compare("filters") == 0)
                    {
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            FilterValue filterValue;
                            ParseFilterJSON(((*i)->at(arrayCounter)), filterValue);
                            filterObject.push_back(filterValue);
                        }
                    }
                }
                //log(((string)(*i)->name()).c_str());
                ++i;
            }
            result = kTrue;
        }catch (...){
            log("Exception caught in parseSysConfig");
            return result;
        }
        json_delete(current);
    }
    return result;
}

void jsonParser::ParseFilterJSON(JSONNode & nodeObj, FilterValue &filterValue)
{
    JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
    {
        try
        {
            
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("id")== 0)
            {
                filterValue.setFilterId(node_value);
            }
            else if(node_name.compare("name")== 0)
            {
                filterValue.setFilterName(node_value);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseConfigJSON");
        }
        ++i;
    }
}

bool16 jsonParser::parseFilterResultValue(PMString inputJson, vector<CItemModel> &filterResultObject)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseSysConfig Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    //CA(((PMString)(*i)->name()).c_str());
                    //log(((string)(*i)->name()).c_str());
                    if(node_name.compare("results") == 0)
                    {
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            CItemModel filterResultValue;
                            ParseFilterResultJSON(((*i)->at(arrayCounter)), filterResultValue);
                            filterResultObject.push_back(filterResultValue);
                        }
                    }
                }
                //log(((string)(*i)->name()).c_str());
                ++i;
            }
            result = kTrue;
        }catch (...){
            CA("Exception caught in parseSysConfig");
            return result;
        }
        json_delete(current);
    }
    return result;
}

void jsonParser::ParseFilterResultJSON(JSONNode & nodeObj, CItemModel &filterResultValue)
{
    JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
    {
        try
        {
            
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("resultId")== 0)
            {
                filterResultValue.setItemID(node_value.GetAsDouble());
            }
            else if(node_name.compare("masterCategoryId")== 0)
            {
                filterResultValue.setSectionResultId(node_value.GetAsDouble());
            }
            else if(node_name.compare("supplierId")== 0)
            {
                filterResultValue.setSupplierID(node_value.GetAsDouble());
            }
            else if(node_name.compare("statusId")== 0)
            {
                filterResultValue.setStageId(node_value.GetAsDouble());
            }
            else if(node_name.compare("values")== 0)
            {
                vector<ItemValue> vectorItemValues;
                for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                {
                    ItemValue itemValueObj;
                    ParseItemValueJSON(((*i)->at(arrayCounter)), itemValueObj);
                    vectorItemValues.push_back(itemValueObj);
                }
                filterResultValue.setValues(vectorItemValues);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseConfigJSON");
        }
        ++i;
    }

}

bool16 jsonParser::parseFilterResultCountValue(PMString inputJson, int32 &filterResultCount)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseSysConfig Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                std::string node_name = (*i) -> name();
//                CA(((PMString)(*i)->name()).c_str());
                //log(((string)(*i)->name()).c_str());
                if(node_name.compare("count") == 0)
                {
                    PMString node_value("");
                    node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
                    filterResultCount = node_value.GetAsNumber();
                    
                }
                //log(((string)(*i)->name()).c_str());
                ++i;
            }
            result = kTrue;
        }catch (...){
            CA("Exception caught in parseSysConfig");
            return result;
        }
        json_delete(current);
    }
    return result;

}

bool16 jsonParser::parsePricingModuleList(PMString inputJson, vector<PricingModule>& pricingModuleVectorObj)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parsePricingModuleList Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    if(node_name.compare("pricingModule") == 0 )
                    {
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            PricingModule pricingModule;
                            
                            ParsePricingModuleJSON(((*i)->at(arrayCounter)), pricingModule);
                            
                            pricingModuleVectorObj.push_back(pricingModule);
                        }						
                    }
                }
                
                ++i;
            }
            
            result = kTrue;
        } 
        catch (...){
            CA("Exception caught in parseIndexReferences");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
    
}


void jsonParser::ParsePricingModuleJSON(JSONNode & nodeObj, PricingModule &pricingModule)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("pricingModuleId")== 0)
            {
                pricingModule.setPricingModuleId(node_value.GetAsDouble());
            }
            else if(node_name.compare("module")== 0)
            {
                pricingModule.setModule(node_value);
                
                //CA("PricingModule " + node_value);
            }
            else if(node_name.compare("description")== 0)
            {
                pricingModule.setDescription(node_value);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParsePricingModuleSON");
        }
        ++i;
    }
}

bool16 jsonParser::parseWorkflowTaskList(PMString inputJson, vector<WorkflowTask>& workflowTaskVectorObj)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parsePricingModuleList Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    if(node_name.compare("tasks") == 0 )
                    {
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            WorkflowTask workflowTask;
                            ParseWorkflowTaskJSON(((*i)->at(arrayCounter)), workflowTask);
                            workflowTaskVectorObj.push_back(workflowTask);
                        }
                    }
                }
                
                ++i;
            }
            
            result = kTrue;
        }
        catch (...){
            CA("Exception caught in parseIndexReferences");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
    
}


void jsonParser::ParseWorkflowTaskJSON(JSONNode & nodeObj, WorkflowTask &workflowTask)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("clientId")== 0)
            {
                workflowTask.setClientId(node_value.GetAsDouble());
            }
            else if(node_name.compare("taskId")== 0)
            {
                workflowTask.setTaskId(node_value.GetAsDouble());
            }
            else if(node_name.compare("objectId")== 0)
            {
                workflowTask.setObjectId(node_value.GetAsDouble());
            }
            else if(node_name.compare("currentStepId")== 0)
            {
                workflowTask.setClientId(node_value.GetAsDouble());
            }
            else if(node_name.compare("taskName")== 0)
            {
                workflowTask.setTaskName(node_value);
            }
            else if(node_name.compare("dueDate")== 0)
            {
                workflowTask.setDueDate(node_value.GetAsDouble());
            }
            else if(node_name.compare("taskStatus")== 0)
            {
                workflowTask.setTaskStatus(node_value);
            }
            else if(node_name.compare("emailNotification")== 0)
            {
                if(node_value == ("true") )
                {
                    workflowTask.setEmailNotification(kTrue);
                }
                else
                {				
                    workflowTask.setEmailNotification(kFalse);
                }
            }
            else if(node_name.compare("attributeGroupId")== 0)
            {
                workflowTask.setAttributeGroupId(node_value.GetAsDouble());
            }
            else if(node_name.compare("userId")== 0)
            {
                workflowTask.setUserId(node_value.GetAsDouble());
            }
            else if(node_name.compare("roleId")== 0)
            {
                workflowTask.setRoleId(node_value.GetAsDouble());
            }
            else if(node_name.compare("languageId")== 0)
            {
                workflowTask.setLanguageId(node_value.GetAsDouble());
            }
            else if(node_name.compare("assignorId")== 0)
            {
                workflowTask.setAssignorId(node_value.GetAsDouble());
            }
            else if(node_name.compare("assignorName")== 0)
            {
                workflowTask.setAssignorName(node_value);
            }
            else if(node_name.compare("workflowId")== 0)
            {
                workflowTask.setWorkflowId(node_value.GetAsDouble());
            }
            else if(node_name.compare("workflowName")== 0)
            {
                workflowTask.setWorkflowName(node_value);
            }
            else if(node_name.compare("objectType")== 0)
            {
                workflowTask.setObjectType(node_value);
            }
            if(node_name.compare("item") == 0)
            {
                WorkflowItem workflowItemObj;
                ParseWorkflowItemJson(**i, workflowItemObj);
                workflowTask.setWorkflowItem(workflowItemObj);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParsePricingModuleSON");
        }
        ++i;
    }
}

void jsonParser::ParseWorkflowItemJson(JSONNode & nodeObj, WorkflowItem &workflowItem)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("updateDate")== 0)
            {
                workflowItem.setUpdateDate(node_value.GetAsDouble());
            }
            else if(node_name.compare("categoryId")== 0)
            {
                workflowItem.setUpdateDate(node_value.GetAsDouble());
            }
            else if(node_name.compare("seqOrder")== 0)
            {
                workflowItem.setSeqOrder(node_value.GetAsNumber());
            }
            else if(node_name.compare("allListsItemCount")== 0)
            {
                workflowItem.setAllListsItemCount(node_value.GetAsNumber());
            }
            else if(node_name.compare("masterCategoryId")== 0)
            {
                workflowItem.setMasterCategoryId(node_value.GetAsDouble());
            }
            else if(node_name.compare("typeId")== 0)
            {
                workflowItem.setTypeId(node_value.GetAsDouble());
            }
            else if(node_name.compare("listsCount")== 0)
            {
                workflowItem.setListsCount(node_value.GetAsNumber());
            }
            else if(node_name.compare("advTablesCount")== 0)
            {
                workflowItem.setAdvTablesCount(node_value.GetAsNumber());
            }
            else if(node_name.compare("id")== 0)
            {
                workflowItem.setItemId(node_value.GetAsDouble());
            }
            else if(node_name.compare("number")== 0)
            {
                workflowItem.setNumber(node_value);
            }
            else if(node_name.compare("type")== 0)
            {
                workflowItem.setType(node_value);
            }
            else if(node_name.compare("statusId")== 0)
            {
                workflowItem.setStatusId(node_value.GetAsNumber());
            }
            else if(node_name.compare("values") == 0 )
            {
                vector<WorkflowItemValue> workflowTaskVectorObj;
                for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                {
                    WorkflowItemValue workflowItemValue;
                    
                    ParseWorkflowItemValueJSON(((*i)->at(arrayCounter)), workflowItemValue);
                    workflowTaskVectorObj.push_back(workflowItemValue);
                }
                workflowItem.setValues(workflowTaskVectorObj);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseWorkflowItemJson");
        }
        ++i;
    }
}

void jsonParser::ParseWorkflowItemValueJSON(JSONNode & nodeObj, WorkflowItemValue &workflowItemValue)
{
    JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end()){
        
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("fieldId")== 0)
            {
               workflowItemValue.setFieldId(node_value.GetAsDouble());
            }
            else if(node_name.compare("languageId")== 0)
            {
                workflowItemValue.setLanguageId(node_value.GetAsDouble());
            }
            else if(node_name.compare("fieldKey")== 0)
            {	
                workflowItemValue.setFieldKey(node_value);
            }
            else if(node_name.compare("fieldName")== 0)
            {
                workflowItemValue.setFieldName(node_value);
            }
            else if(node_name.compare("value")== 0)
            {
                workflowItemValue.setValue(node_value);
            }
            
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseWorkflowTaskJSON");
        }
        
        ++i;
    }
}



bool16 jsonParser::parseAttributeGoupsJson(PMString inputJson, vector<AttributeGroup> &attributeGroups)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseAttributeGoupsJson Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    if(node_name.compare("attributeGroups") == 0 )
                    {
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            AttributeGroup attributeGroup;
                            ParseAttributeGoupJSON(((*i)->at(arrayCounter)), attributeGroup);
                            attributeGroups.push_back(attributeGroup);
                        }
                    }
                }
                
                ++i;
            }
            
            result = kTrue;
        }
        catch (...){
            CA("Exception caught in parseAttributeGoupsJson");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
    
}

void jsonParser::ParseAttributeGoupJSON(JSONNode & nodeObj, AttributeGroup &attributeGroup)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("id")== 0)
            {
                attributeGroup.setAttributeGroupId(node_value.GetAsDouble());
            }
            else if(node_name.compare("seqOrder")== 0)
            {
                attributeGroup.setSeqOrder(node_value.GetAsNumber());
            }
            else if(node_name.compare("key")== 0)
            {
                attributeGroup.setKey(node_value);
            }
            else if(node_name.compare("localization") == 0)
            {
                PMString name("");
                ParseLocalizationJson(**i, name);
                attributeGroup.setName(name);
            }
            else if(node_name.compare("name") == 0)
            {
                attributeGroup.setName(node_value);
            }
            else if(node_name.compare("attributes") == 0 )
            {
                vector<double> attributeIds;
                for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                {
                    double attributeId;
                    
                    ParseAttributeIdListJSON(((*i)->at(arrayCounter)), attributeId);
                    attributeIds.push_back(attributeId);
                }
                attributeGroup.setAttributeIds(attributeIds);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseAttributeGoupJSON");
        }
        ++i;
    }
}


void jsonParser::ParseLocalizationJson(JSONNode & nodeObj, PMString &name)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("en_US") == 0)
            {
                ParseLanguageSpecificDataJson(**i, name);
            }

            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseLocalizationJson");
        }
        ++i;
    }
}

void jsonParser::ParseLanguageSpecificDataJson(JSONNode & nodeObj, PMString &name)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("name") == 0)
            {
                name = node_value;
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseLanguageSpecificDataJson");
        }
        ++i;
    }
}

void jsonParser::ParseAttributeIdListJSON(JSONNode & nodeObj, double &attributeId)
{
    JSONNode::json_iterator i = nodeObj.begin();
    PMString nullString("null");
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("id") == 0)
            {
                attributeId = node_value.GetAsDouble();
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseAttributeIdListJSON");
        }
        ++i;
    }
}

bool16 jsonParser::parseAttributeGoupJson(PMString inputJson, AttributeGroup &attributeGroup)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseAttributeGoupsJson Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ( (*i) -> type() == JSON_NODE)
                {
                    ParseAttributeGoupJSON((**i), attributeGroup);
                    break;
                }
                ++i;
            }
            
            result = kTrue;
        }
        catch (...){
            CA("Exception caught in parseAttributeGoupJson");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
}

bool16 jsonParser::parseAttributesOfGroupJson(PMString inputJson, vector<double> &attributeIds)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseAttributeGoupsJson Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    if(node_name.compare("attributes") == 0 )
                    {
                        vector<double> localAttributeIds;
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            double attributeId;
                            
                            ParseAttributeIdListJSON(((*i)->at(arrayCounter)), attributeId);
                            localAttributeIds.push_back(attributeId);
                        }
                        attributeIds = localAttributeIds;
                    }
                }
                
                ++i;
            }
            
            result = kTrue;
        }
        catch (...){
            CA("Exception caught in parseAttributesOfGroupJson");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
    
}

void jsonParser::ParseAttributeGroupValueJSONForSectionResult(JSONNode & nodeObj, vector<AttributeGroup> &attributeGroups)
{
    //CA(" inside ParseAttributeGroupValueJSONForSectionResult ");
    JSONNode::json_iterator i = nodeObj.begin();
    double parentClassId = -1;
    
    try
    {
        AttributeGroup attributeGroupObj;
        PMString node_name("");
        node_name.SetCString(((*i) -> name()).c_str(), PMString::kUnknownEncoding);
        attributeGroupObj.setAttributeGroupId( node_name.GetAsDouble());
        //CA("node_name: " + node_name );
        if ((*i)->type() == JSON_ARRAY)
        {
            vector<double> vectorAttributeIds;
            for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
            {
                PMString node_value1("");
                node_value1.SetCString((((*i)->at(arrayCounter)).as_string()).c_str(), PMString::kUnknownEncoding);
                //CA("node_value1: " + node_value1 );
                vectorAttributeIds.push_back(node_value1.GetAsDouble());
            }
            attributeGroupObj.setAttributeIds(vectorAttributeIds);
        }
        attributeGroups.push_back(attributeGroupObj);
    }catch(...)
    {
        log("exception caught in jsonParser::ParseAttributeGroupValueJSONForSectionResult");
    }
}

bool16 jsonParser::parseAttributesV3Json(PMString inputJson, vector<Attribute> &attributes)
{
    bool16 result = kFalse;
    bool a = json_is_valid(inputJson.GetPlatformString().c_str());
    if(!a)
    {
        CA("Invalid parseAttributeV3Json Json");
        return result;
    }
    else
    {
        JSONNode * current = (JSONNode *)json_parse(inputJson.GetPlatformString().c_str());
        try
        {
            if(current == NULL)
            {
                CA("current == NULL");
                return kFalse;
            }
            
            JSONNode::json_iterator i = current->begin();
            while (i != current->end())
            {
                if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE)
                {
                    std::string node_name = (*i) -> name();
                    if(node_name.compare("attributes") == 0 )
                    {
                        vector<Attribute> localAttribute;
                        for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                        {
                            Attribute attributeObj;
                            
                            parseAttributeV3Json(((*i)->at(arrayCounter)), attributeObj);
                            attributes.push_back(attributeObj);
                        }
                        
                    }
                }
                
                ++i;
            }
            
            result = kTrue;
        }
        catch (...){
            CA("Exception caught in parseAttributesV3Json");
            return result;
        }
        json_delete(current);
        
    }
    
    return result;
    
}

void jsonParser::parseAttributeV3Json(JSONNode & nodeObj, Attribute &itemFieldValue)
{
    JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end()){
        
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            //log(((*i) -> as_string()).c_str());
            
            if(node_name.compare("dataType")== 0)
            {
                itemFieldValue.setDataType(node_value);
            }
            else if(node_name.compare("number")== 0)
            {
                itemFieldValue.setNumber(node_value);
            }
            else if(node_name.compare("languageId")== 0)
            {
                itemFieldValue.setLanguageId(node_value.GetAsDouble());
            }
            else if(node_name.compare("dataTypeId")== 0)
            {
                itemFieldValue.setDataTypeId(node_value.GetAsDouble());
            }
            else if(node_name.compare("id")== 0)
            {
                itemFieldValue.setAttributeId(node_value.GetAsDouble());
            }
            else if(node_name.compare("picklistId")== 0)  //"pvTypeId"
            {
                itemFieldValue.setPicklistGroupId(node_value.GetAsDouble());
            }
            else if(node_name.compare("localization") == 0)
            {
                PMString name("");
                ParseLocalizationJson(**i, name);
                itemFieldValue.setDisplayName(name);
            }
            else if(node_name.compare("name") == 0)
            {
                itemFieldValue.setName(node_value);
            }
            else if(node_name.compare("editLines")== 0)
            {
                itemFieldValue.setEditLines(node_value.GetAsNumber());
            }

            
            /*
            
            else if(node_name.compare("sysName")== 0) // tableCol changed to sysName
            {
                itemFieldValue.setTableCol(node_value);
            }
            else if(node_name.compare("mpvSeparator")== 0)
            {
                itemFieldValue.setMpvSeparator(node_value);
            }
            else if(node_name.compare("mpvPrefix")== 0)
            {
                itemFieldValue.setMpvPrefix(node_value);
            }
            else if(node_name.compare("mpvSuffix")== 0)
            {
                itemFieldValue.setMpvSuffix(node_value);
            }
            else if(node_name.compare("uniqueness") == 0)
            {
                itemFieldValue.setUniqueness(node_value.GetAsNumber());
            }
            else if(node_name.compare("catalogId") == 0)
            {
                itemFieldValue.setCatalogId(node_value.GetAsDouble());
            }
            else if(node_name.compare("decMaxLength")== 0)
            {
                itemFieldValue.setDecMaxLength(node_value.GetAsNumber());
            }
            else if(node_name.compare("decMinLength")== 0)
            {
                itemFieldValue.setDecMinLength(node_value.GetAsNumber());
            }
            else if(node_name.compare("domainId")== 0)
            {
                itemFieldValue.setDomainId(node_value.GetAsDouble());
            }
            else if(node_name.compare("emptyValueDisplay")== 0)
            {
                itemFieldValue.setEmptyValueDisplay(node_value);
            }
            
            else if(node_name.compare("maxLength")== 0)
            {
                itemFieldValue.setMaxLength(node_value.GetAsNumber());
            }
            else if(node_name.compare("minLength")== 0)
            {
                itemFieldValue.setMinLength(node_value.GetAsNumber());
            }
            else if(node_name.compare("parentAttributeId")== 0)
            {
                itemFieldValue.setParentAttributeId(node_value.GetAsDouble());
            }
            else if(node_name.compare("pubItem")== 0)
            {
                itemFieldValue.setPubItem(node_value.GetAsNumber());
            }
            else if(node_name.compare("required")== 0)
            {
                itemFieldValue.setRequired(node_value.GetAsNumber());
            }
            else if(node_name.compare("uniquenessCaseSensitive")== 0)
            {
                itemFieldValue.setUniquenessCaseSensitive(node_value.GetAsNumber());
            }
            else if(node_name.compare("uniquenessCondensed")== 0)
            {
                itemFieldValue.setUniquenessCondensed(node_value.GetAsNumber());
            }
            else if(node_name.compare("unitId")== 0) 
            {	
                itemFieldValue.setUnitId(node_value.GetAsDouble());			
            }
            else if(node_name.compare("allowNewEntry")== 0) 
            {	
                itemFieldValue.setAllowNewEntry(node_value.GetAsNumber());			
            }
            else if(node_name.compare("eventSpecific")== 0)
            {
                if(node_value.GetAsNumber() == 1)
                    itemFieldValue.setIsEventSpecific(kTrue);
                else
                    itemFieldValue.setIsEventSpecific(kFalse);
            }
            else if(node_name.compare("details")== 0)
            {
                //CA("Got Item Fields");
                vector<Details> vectordetailsValue;
                for(int arrayCounter =0; arrayCounter < (*i)->size(); arrayCounter++)
                {
                    Details detailsValue;
                    ParseDetailsJSON(((*i)->at(arrayCounter)), detailsValue);
                    vectordetailsValue.push_back(detailsValue);
                }
                itemFieldValue.setNameList(vectordetailsValue);
            }
            */
            
        }catch(...)
        {
            log("exception caught in jsonParser::parseAttributeV3Json");
        }
        
        ++i;
    }
    
}


void jsonParser::ParseTableRowJSONForChildItem(JSONNode & nodeObj, ChildItemModel &childItem)
{
    JSONNode::json_iterator i = nodeObj.begin();
    while (i != nodeObj.end())
    {
        try
        {
            std::string node_name = (*i) -> name();
            //log(node_name.c_str());
            
            PMString node_value("");
            node_value.SetCString(((*i) -> as_string()).c_str(), PMString::kUnknownEncoding);
            
            if(node_name.compare("itemId")== 0)
            {					
                childItem.setItemID ( node_value.GetAsDouble());
            }
            else if(node_name.compare("categoryId")== 0)
            {
                childItem.setClassID( node_value.GetAsDouble());
            }
            else if(node_name.compare("itemNumber")== 0)
            {
                childItem.setItemNo( node_value);
            }
            else if(node_name.compare("itemName")== 0)
            {
                childItem.setItemDesc( node_value);
            }
            
        }catch(...)
        {
            log("exception caught in jsonParser::ParseTableRowJSONForChildItem");
        }
        ++i;
    }
    
}
