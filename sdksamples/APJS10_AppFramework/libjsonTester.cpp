#include "VCPlugInHeaders.h"
#include <iostream>
#include <string>
#include <ctime>
#include <stdio.h>
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include <iostream>
#include <fstream>
#include "curl.h"
//#include "Winsock.h"
#include <stdlib.h>

using namespace std;

  struct MemoryStruct {
	  char *memory;
	  size_t size;
	};

static std::string
readInputTestFile( const char *path )
{
	std::string text;
   FILE *file = fopen( path, "rb" );
   if ( !file )
      return text ;
   fseek( file, 0, SEEK_END );
   long size = ftell( file );
   fseek( file, 0, SEEK_SET );
   
   char *buffer = new char[size+1];
   buffer[size] = 0;
   if ( fread( buffer, 1, size, file ) == (unsigned long)size )
      text = buffer;
   fclose( file );
   delete[] buffer;
   return text;
}

static void log(const char * logtext)
{
	//const char* path = "D:\\RD\\Apsiva\\json\\ApsivaTestData\\libjsontestResult.txt";
	 ofstream myfile;
    myfile.open ("D:\\RD\\Apsiva\\json\\ApsivaTestData\\libjsontestResult.txt", ios::out | ios::app );
	myfile <<logtext;
	myfile << "\n";
    myfile.close();
}

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory == NULL) {
    /* out of memory! */ 
    printf("not enough memory (realloc returned NULL)\n");
    exit(EXIT_FAILURE);
  }
 
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
 
  return realsize;
}

string performJsonWebservicecall(void);
string performJsonWebservicecall(void)
{
	log("Inside performJsonWebservicecall");
  CURL *curl;
  CURLcode res;
  /* Minimalistic http request */
  //const char *request = "GET / HTTPS/1.0\r\nHost: prd02.apsiva.net/cf-cm/json/244/login.json?username=webapi2%40apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3\r\n\r\n";
  //curl_socket_t sockfd; /* socket */
  //long sockextr;
  //size_t iolen;
  //curl_off_t nread;

  /*
	$data = array("name" => "Hagrid", "age" => "36");                                                                    
	$data_string = json_encode($data);                                                                                   
 
	$ch = curl_init('http://api.local/rest/users');                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);                                                                                                                   
 
	$result = curl_exec($ch);
  */

  //log(request);

  struct MemoryStruct chunk1;
 
  chunk1.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */ 
  chunk1.size = 0;    /* no data at this point */ 

  struct curl_slist *chunk = NULL;

  string postArguments = "244/login.json&username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3"; 
  char buffer[20];
  sprintf(buffer, "%d",  postArguments.size());
  

  chunk = curl_slist_append(chunk, "Content-Type: application/json");
  chunk = curl_slist_append(chunk, buffer);


  curl_global_init(CURL_GLOBAL_ALL);


  curl = curl_easy_init();
  log("after curl_easy_init");
  if(curl) {
	log("Inside curl");
    curl_easy_setopt(curl, CURLOPT_URL, "https://dev08.apsiva.net/cf-cm/master/structure.json;jessionid=D12B0325A94E711D36C1A614756F98F9.worker1");
	// curl_easy_setopt(curl, CURLOPT_URL, "https://dev08.apsiva.net/cf-cm/json/244/login.json?username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3");
	log("After curl_easy_setopt CURLOPT_URL");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	//log("After curl_easy_setopt CURLOPT_SSL_VERIFYPEER");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	//curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST"); 
	//curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, 1L);
	log("After curl_easy_setopt CURLOPT_SSL_VERIFYHOST");
	//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "&JSESSIONID=A16AE4C11E03CDD639CC842DD6EDCABF.worker1");
		
		//"244/login.json&username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3");  //&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3
    /* Do not do the transfer - only connect to host */
    //curl_easy_setopt(curl, CURLOPT_CONNECT_ONLY, 1L);
	char error[1024];
	 curl_easy_setopt ( curl, CURLOPT_ERRORBUFFER, error ); 
	 
	 /* send all data to this function  */ 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
 
	  /* we pass our 'chunk' struct to the callback function */ 
	  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk1);
	  curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	   /* some servers don't like requests that are made without a user-agent
     field, so we provide one */
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	//curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 20);

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
	log("After curl_easy_setopt CURLOPT_HTTPHEADER");
    res = curl_easy_perform(curl);
	log("After curl_easy_perform");

    if(CURLE_OK != res)
    {
		log("CURLE_OK != res");
      log( strerror(res));
	  log(error);
	    if(chunk1.memory)
		  {
			log(chunk1.memory);
			free(chunk1.memory);

		  }
      return string("ERROR");
    }

	log("CURLE_OK == res");

	//printf("%lu bytes retrieved\n", (long)chunk1.size);
	log(chunk1.memory);
  if(chunk1.memory)
  {
    log(chunk1.memory);
    free(chunk1.memory);

  }
 //   /* Extract the socket from the curl handle - we'll need it for waiting.
 //    * Note that this API takes a pointer to a 'long' while we use
 //    * curl_socket_t for sockets otherwise.
 //    */
 //   res = curl_easy_getinfo(curl, CURLINFO_LASTSOCKET, &sockextr);

 //   if(CURLE_OK != res)
 //   {
	//	log("curl_easy_getinfo CURLE_OK != res");
 //     log( curl_easy_strerror(res));
 //     return string("ERROR1");
 //   }

	//log("curl_easy_getinfo CURLE_OK == res");
 //   sockfd = sockextr;

 //   /* wait for the socket to become ready for sending */
 //   if(!wait_on_socket(sockfd, 0, 60000L))
 //   {
 //     log("Error: timeout.\n");
 //     return string("ERROR2");
 //   }

 //   log("Sending request.");
 //   /* Send the request. Real applications should check the iolen
 //    * to see if all the request has been sent */
 //   res = curl_easy_send(curl, request, strlen(request), &iolen);

 //   if(CURLE_OK != res)
 //   {
	//	log("curl_easy_send CURLE_OK != res ");
 //     log( curl_easy_strerror(res));
 //     return string("ERROR2");
 //   }
 //   log("Reading response.");

 //   /* read the response */
 //   for(;;)
 //   {
 //     char buf[1024];
	//  log("Inside For loop");

 //     wait_on_socket(sockfd, 1, 60000L);
 //     res = curl_easy_recv(curl, buf, 1024, &iolen);

 //     if(CURLE_OK != res){

	//	  log("Error5");
 //       break;

	//  }

 //     nread = (curl_off_t)iolen;

 //     log("Received %" CURL_FORMAT_CURL_OFF_T " bytes.\n" );

	//  log("\n\n");
	//  log(buf);
	//  


 //   }

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
 
  return "Success";
}



void ParseJSON( JSONNode & n){
     JSONNode::json_iterator i = n.begin();
    while (i != n.end()){
		log("Inside ParseJson");
        // recursively call ourselves to dig deeper into the tree
        if ((*i)->type() == JSON_ARRAY || (*i) -> type() == JSON_NODE){
			
			log("Got an Array or Object");
			log(((string)(*i)->name()).c_str());
            ParseJSON(**i);
        }
 
        // get the node name and value as a string
        std::string node_name = (*i) -> name();
		log(node_name.c_str());
		std::string node_value = (*i) -> as_string();
		log(node_value.c_str());
 
        ++i;
    }
}


int main () {
   // JSONNODE * node;

	//performJsonWebservicecall();

	string path = "D:\\RD\\Apsiva\\json\\ApsivaTestData\\tree.json";
	
	printf("Inside Main");
   std::string input = readInputTestFile( path.c_str() );
   if ( input.empty() )
   {
      log( "Failed to read input or empty input: %s\n" );
	  log( path.c_str());
      return 3;
   }
	log("Inside Main 2 \n" );
	log(input.c_str());
	log("Inside Main 2.6 \n" );
   //json_string buffer((input));
	bool a = json_is_valid(input.c_str());
	if(a)
	log( "Valid Json");
	else
		log("Invalid Json");
   JSONNode * current = (JSONNode *)json_parse(input.c_str());
	log("Inside Main 3 \n" );
   try {
	   if(current == NULL)
	   {
		   log("current == NULL");
		   return 1;
	   }

		ParseJSON(*current);

				/*printf("Inside Main 4 \n" );
				wchar_t * str =json_as_string(json_get(current, JSON_TEXT("categoryId")));		
				printf("Inside Main 5 \n" );
				printf((char*)str);
				printf("Inside Main 6 \n" );*/

	   // for(JSONNode::json_iterator it = current->begin(); it != current->end(); ++it){
				//log("Inside Main 4 \n" );
				//char * str =json_name((*it)); //json_as_string(JSON_TEXT(*it)->name());		
				//log("Inside Main 5 \n" );
				//if(str != NULL){
				//	log((char*)str);
				//log("Inside Main 6 \n" );
				//}
				//JSONNode * str1 = (JSONNode *)json_as_array((*it));
				//if(str1 != NULL){
				//	log("Inside Main 7 \n" );
				//	if(str1->type() == JSON_NULL)
				//		log("Node is of type JSON_NULL");
				//	else if(str1->type() == JSON_STRING)
				//	{
				//		log("Node is of type JSON_STRING");
				//	}
				//	else if(str1->type() == JSON_NUMBER)
				//	{
				//		log("Node is of type JSON_NUMBER");
				//	}
				//	else if(str1->type() == JSON_BOOL)
				//	{
				//		log("Node is of type JSON_BOOL");
				//	}
				//	else if(str1->type() == JSON_ARRAY)
				//	{
				//		log("Node is of type JSON_ARRAY");

				//		char buffer[20];
				//		sprintf(buffer, "%d",  str1->size());
				//		log(buffer);
				//		for(int arrayCounter =0; arrayCounter < str1->size(); arrayCounter++)
				//		{
				//			JSONNode  arrayObject = str1->at(arrayCounter);
				//			log("Inside Array");

				//			if(arrayObject.type() == JSON_NODE)
				//			{
				//				for(int nodeChildCount =0 ; nodeChildCount < arrayObject.size(); nodeChildCount++ )
				//				{
				//					log("Child Object");
				//					JSONNode chidObj = arrayObject.at(nodeChildCount);
				//					json_string objName = chidObj.name();
				//					json_string objValue = chidObj.as_string();							
				//					log(objName.c_str());
				//					log(objValue.c_str());
				//				}
				//			}

				//			json_string objName = arrayObject.name();
				//			json_string objValue = arrayObject.as_string();							
				//			log(objName.c_str());
				//			log(objValue.c_str());

				//		}
				//		
				//	}
				//	else if(str1->type() == JSON_NODE)
				//	{
				//		log("Node is of type JSON_NODE");
				//	}
				//	
				////printf((char*)str1);
				//}

			 //}
				
				
	} catch (...){
				log("false");
			}


 //string mystr = makeBigFormatted();
 
   /**
	   node = json_parse(mystr.c_str());
	   for (unsigned int j = 0; j < IT_COUNT; ++j){
		  JSONNODE * meh = json_at(node, j * 2);
		  json_as_float(json_get(meh, "name"));
		  char * str = json_as_string(json_get(meh, "another"));
		  json_free(str);
		  
		  meh = json_at(node, j * 2 + 1);
		  json_as_int(json_at(meh, 0));
		  json_as_int(json_at(meh, 1));
		  json_as_bool(json_at(meh, 2));
		  json_as_bool(json_at(meh, 3));
		  json_as_int(json_at(meh, 4));
	   }
	   json_delete(node);
    }
    cout << "Reading:             " << clock() - start << endl;
   */
    
    return 0;
}
