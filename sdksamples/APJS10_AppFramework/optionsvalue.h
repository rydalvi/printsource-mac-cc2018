
#ifndef __OptionsValue__
#define __OptionsValue__

#include"PMString.h"

class OptionsValue
{	
	public:
		OptionsValue();
		OptionsValue(char*,char*,char*, char*, char*, char*, int32, int32,int32 ,char*,char*,char*,int32,int32,int32,int32,int32,int32,int32);//21-feb//nitin
		OptionsValue(PMString,PMString,PMString,PMString,PMString, PMString, int32, int32,int32 , PMString,PMString,PMString,int32,int32,int32,int32,int32,int32,int32);//21-feb	//nitin
		OptionsValue(OptionsValue&);
		~OptionsValue();
		void operator=(OptionsValue&);
		PMString& getDefaultLocale(void);   // Added for Locale
		PMString& getPublicationName(void);		
		PMString& getImagePath(void);
		PMString& getIndesignDocPath(void);
		PMString& getLocaleID(void);
		PMString& getProjectID(void);
//21-feb
		int32 getAssetStatus(void);
		int32 getLogLevel(void);
		int32 getMissingFlag(void);//nitin
//21-feb
		void setDefaultLocale(PMString& );
		void setPublicationName(PMString& );
		void setImagePath(PMString&);
		void setIndesignDocPath(PMString&);
		void setLocaleID(PMString&);
		void setProjectID(PMString&);
//21-feb
		void setAssetStatus(int32);
		void setLogLevel(int32);
		//added by nitin
		void setMissingFlag(int32);//nitin
//21-feb

//06-sep
		PMString& getPublicationComment(void);
		void setPublicationComment(PMString& );
//06-sep

		double getclientID();
		void setclientID(double);

		double getclass_ID(void); //added by mane
		void setclass_ID(double);

		PMString& getSectionName(void);
		void setSectionName(PMString& );

		PMString& getSectionID(void);
		void setSectionID(PMString& );

		PMString& getIsOneSource(void);
		void setIsOneSource(PMString& );

		int32 getDefault_Flow(void);
		void setDefault_Flow(int32);

		int32 getDisplayPartnerImgs(void);
		void setDisplayPartnerImgs(int32);

		int32 getDisplayPickListImgs(void);
		void setDisplayPickListImgs(int32);

		int32 getShowObjCountFlag(void);
		void setShowObjCountFlag(int32);

		int32 getByPassForSingleSelectionSprayFlag(void);
		void setByPassForSingleSelectionSprayFlag(int32);

		int32 getHorizontalSpacing(void);
		void setHorizontalSpacing(int32);

		int32 getVerticalSpacing(void);
		void setVerticalSpacing(int32);

	private:		
		//t32 pubID;
		PMString LocaleName;
		PMString PublicationName;
		PMString ImagePath;
		PMString DocPath;
		PMString LocaleID;
		PMString ProjectID;
//21-feb
		int32 assetStatus;
		int32 logLevel;
		int32 missingFlag;//nitin
//21-feb
//06-sep
		PMString PublicationComment;
//06-sep
		double clientID;
		double classID; //aded by mane

		PMString SectionName;
		PMString SectionID;
		PMString isOneSource;

		int32 default_Flow;		// 1 = Horizontal Flow , 0 = Vertical Flow.
		int32 displayPartnerImgs;
		int32 displayPickListImgs;
		int32 showObjCount;

		int32 byPassForSingleSelectionSpray;
		int32 horizontalSpacing;
		int32 verticalSpacing;
};


#endif