#include "VCPlugInHeaders.h"
#include "ISpecialChar.h"
#include "IAppFramework.h"
#include "CTUnicodeTranslator.h"
//#include "CommonFunctions.h"
#include "TextChar.h"
#include "IEncodingUtils.h"
#include <string>
class IPMStream;
#include "IWorkspace.h"
#include "IUIDData.h"
#include "IBoolData.h"
#include "ICommand.h"
#include "IDocument.h"
#include "IDatabase.h"
#include "ILayoutUIUtils.h" //Cs4
#include "CmdUtils.h"
#include "TextID.h"
#include "CAlert.h"
#include "PlatformChar.h"

#define CA(X) CAlert::InformationAlert(X)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

class SpecialChar : public CPMUnknown<ISpecialChar>
{
public :
	using ISpecialChar ::translateString;
	SpecialChar(IPMUnknown*  boss);
	~SpecialChar();
	PMString translateString(const PMString&);
	PMString translateToDatabaseCode(const PMString& );
	PMString handleAmpersandCase(PMString &);
	void ChangeQutationMarkONOFFState(bool16);
	PMString translateStringNew(const PMString& code, VectorHtmlTrackerPtr vectHtmlValue);
	//TagList translateStringForAdvanceTableCell(const CellData &,PMString &);

	PMString getNumber(char *code);
	int convertToHex(char *strNum);
	int convertIndesignCodeIntoHexCode(char* hexNum);
	int getBinary(int tempNum, char *val);
};

CREATE_PMINTERFACE(SpecialChar, kSpecialCharImpl)

SpecialChar::SpecialChar(IPMUnknown* boss) : CPMUnknown<ISpecialChar>(boss)
{}

SpecialChar::~SpecialChar(){}


//PMString SpecialChar::translateString(const PMString& code)
//{
//	//PMString codeVal("");	
//	//const char* inpCode=code.GrabCString();
//	//char* txt= NULL;
//	//const char *index=inpCode;
//	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	//if(ptrIAppFramework == nil)
//	//	return codeVal;
//
//	//int32 length=std::strlen(inpCode);
//
//	//while((*index)!='\0')
//	//{
//	//	txt=std::strstr(index, "<<APS");		
//	//	while(index!=txt && index[0]!='\0')
//	//	{
//	//		if(index[0]=='\n')
//	//		{
//	//			index++;
//	//			continue;
//	//		}
//	//		codeVal.Append(index[0]);
//	//		index++;
//	//	}
//	//	if(txt)
//	//	{
//	//		do
//	//		{
//	//			PMString hexRep=getNumber(txt);
//	//			if(!hexRep.NumUTF16TextChars())
//	//				break;
//	//			//CA(hexRep);
//	//			PMString indesignCode(""); 
//	//	
//	//			indesignCode=ptrIAppFramework->SPCLCharMngr_getSpecialCharByCode(hexRep.GrabCString());
//	//			if(indesignCode.NumUTF16TextChars())
//	//			{
//	//				//CA(indesignCode);
//	//				int hexVal=convertIndesignCodeIntoHexCode(indesignCode.GrabCString());
//	//				if(hexVal==-1)
//	//				{
//	//					//CA("hexVal==-1");
//	//					codeVal.Append(" ");
//	//					break;
//	//				}
//	//				const textchar characterCode=hexVal;
//	//				//char buf; 
//	//				//::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
//	//				//codeVal.Append(buf);
//	//				codeVal.AppendW(&characterCode);
//	//				CA(codeVal);
//	//			}
//	//			else
//	//			{  
//	//				//CA("indesignCode.NumUTF16TextChars() == 0");
//	//				codeVal.Append(" ");
//	//			}
//	//		}while (kFalse);
//	//		
//	//		while(txt[0]!='\0')
//	//		{
//	//			if(txt[0]==' ' || txt[0]=='\n' || txt[0]=='.' || txt[0]==',' || (txt[0]=='>' && txt[1]=='>'))
//	//				break;
//	//			txt++;
//	//		}
//	//		if((txt[0]=='>' && txt[1]=='>'))
//	//			txt+=2;
//
//	//		index=txt;
//	//	}
//	//}
//	
//	PMString inpCode = code;
//	//CA(inpCode);
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//		return code;
//
//	PMString KeyString("<<APS");	
//    CharCounter Counter = -1;
//	
//	do
//	{		
//		Counter = inpCode.IndexOfString("<<APS");
//		//PMString ASD("Counter  : ");
//		//ASD.AppendNumber(Counter);
//		//CA(ASD);
//        if(Counter != -1)
//		{	
//			PMString *TempString = NULL;
//			int32 ToatalLength = inpCode.NumUTF16TextChars();
//
//			PMString ZeroLengthString("");	
//			PMString BlankString("");
//
//			if(Counter > 0)
//			TempString = inpCode.SubWString(0, Counter);	
//			if(Counter == 0)
//				TempString = &BlankString;
//
//			PMString *ApsivaString = inpCode.SubWString(Counter, 10);
//
//			//CA("ApsivaString : " + *ApsivaString);
//
//			PMString *RemaningString = NULL;
//			if(Counter + 10 < ToatalLength)
//			{
//				RemaningString = inpCode.SubWString(Counter+10);
//				//CA("RemaningString : " + *RemaningString);
//			}
//
//			PMString hexRep=getNumber((*ApsivaString).GrabCString());
//			if(!hexRep.NumUTF16TextChars())
//				break;
//
//			//CA(hexRep);
//			PMString indesignCode(""); 
//
//			//CA(" 157 ");
//			indesignCode=ptrIAppFramework->SPCLCharMngr_getSpecialCharByCode(hexRep.GrabCString());
//			//CA("158");
//			//CA("indesignCode : "+indesignCode);
//			if(indesignCode.NumUTF16TextChars()!= 0)
//			{
//				//CA("indesignCode.NumUTF16TextChars()!= 0 : "+ indesignCode);
//				int hexVal=convertIndesignCodeIntoHexCode(indesignCode.GrabCString());
//				if(hexVal==-1)
//				{
//					//CA("hexVal==-1");
//					const textchar characterCode=0;	
//					if(Counter == 0)
//					{
//						ZeroLengthString.AppendW(&characterCode);						
//						TempString= &ZeroLengthString;
//					}
//					else
//					(*TempString).AppendW(&characterCode);
//					break;
//				}
//				const textchar characterCode=hexVal;	
//				if(Counter == 0)
//				{
//					ZeroLengthString.AppendW(&characterCode);					
//					TempString= &ZeroLengthString;
//				}
//				else
//					(*TempString).AppendW(&characterCode);
//
//				if(RemaningString != NULL)
//				{
//					(*TempString).Append((*RemaningString));
//					//CA("After Appending RemaningString : " + (*TempString));
//
//				}
//				inpCode = (*TempString);	
//				//CA("inpCode : " +inpCode);
//			}
//            else
//			{  			
//				//(*TempString).Append("");
//				if(RemaningString != NULL)
//				(*TempString).Append(*RemaningString);
//				inpCode = (*TempString);
//			}
//		}
//
//	}while(Counter != -1);
//
//	//CA(inpCode);
//	return inpCode;
//}

// New Version of translateString used for 27514 which can take multiple Indesign code for one APS code.
PMString SpecialChar::translateString(const PMString& code)
{	
	//CA("Old One");
	PMString inpCode = code;
	//CA(inpCode);
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return code;

	//PMString KeyString("<<APS");	
 //   	CharCounter Counter = -1;
	//
	//do
	//{		
	//	Counter = inpCode.IndexOfString("<<APS");
	//	//PMString ASD("Counter  : ");
	//	//ASD.AppendNumber(Counter);
	//	//CA(ASD);
 //      	if(Counter != -1)
	//	{	
	//		PMString *TempString = NULL;
	//		bool16 TempStringFlagToDelete = kFalse;///////////////////param
	//		int32 ToatalLength = inpCode.NumUTF16TextChars();

	//		PMString ZeroLengthString("");	
	//		PMString BlankString("");
	//		PMString NewBlankString("");

	//		if(Counter > 0)
	//		{
	//			TempString = inpCode.Substring(0, Counter); //cs4
	//			TempStringFlagToDelete = kTrue;
	//		}
	//		if(Counter == 0)
	//			TempString = &BlankString;

	//		PMString *ApsivaString = inpCode.Substring/*SubWString*/(Counter, 10); //Cs4

	//		//CA("ApsivaString : " + *ApsivaString);

	//		PMString *RemaningString = NULL;
	//		if(Counter + 10 < ToatalLength)
	//		{
	//			RemaningString = inpCode.Substring/*SubWString*/(Counter+10);//Cs4
	//			//CA("RemaningString : " + *RemaningString);
	//		}

	//		PMString hexRep=getNumber(const_cast<char*>((*ApsivaString).GrabCString()/*GetPlatformString().c_str()*/)); //Cs4
	//		if(!hexRep.NumUTF16TextChars())
	//			break;
	//		
	//		if(ApsivaString)///////////////////////param
	//			delete ApsivaString;

	//		//CA(hexRep);
	//		PMString indesignCode(""); 

	//		//CA(" ...157 ");
	//		indesignCode=ptrIAppFramework->SPCLCharMngr_getSpecialCharByCode(const_cast<char*>(hexRep.GrabCString()/*GetPlatformString().c_str()*/)); //Cs4
	//		//CA(indesignCode);

	//		
	//		//CA(".158");
	//		indesignCode.Append(",");
	//		//CA("indesignCode : "+indesignCode);
	//		
	//		CharCounter Counter1 = -1;
	//		CharCounter CurrPosition = 0;
	//		do
	//		{
	//			Counter1 = indesignCode.IndexOfString(",");
	//			if(Counter1 != -1)
	//			{
	//				//CA("Counter1 valid");
	//				if(Counter1 <2)
	//					break;
	//				
	//				//PMString ZXC("Counter1 : ");
	//				//ZXC.AppendNumber(Counter1);
	//				//CA(ZXC);
	//				
	//				PMString *CurrIndesignCode = indesignCode.Substring(0, Counter1 );
	//				//CA("CurrIndesignCode : "+ *CurrIndesignCode);
	//				PMString CURRENTcode = *CurrIndesignCode;		
	//		
	//				if(CURRENTcode.NumUTF16TextChars()!= 0)
	//				{
	//					//CA("indesignCode.NumUTF16TextChars()!= 0 : "+ indesignCode);
	//					if(CURRENTcode.NumUTF16TextChars() ==6)
	//					{
	//						PMString* tempHex =  CURRENTcode.Substring(2);
	//						//CA(*tempHex);
	//						PMString newTemp = ("<");
	//						newTemp.Append(*tempHex);
	//						newTemp.Append(">");
	//						//CA(newTemp);
	//						bool16 Flag = newTemp.ParseForEmbeddedCharacters();
	//						if(Counter == 0)
	//						{
	//							ZeroLengthString.Append(newTemp);	
	//							//CA("ZeroLengthString : "+ ZeroLengthString);						
	//							TempString= &ZeroLengthString;
	//						}
	//						else
	//						{							
	//							(*TempString).Append(newTemp);
	//							//(*TempString).AppendW(&characterCode);
	//						}
	//						if(tempHex)
	//							delete tempHex;
	//						//CA(newTemp);
	//					}
	//					else
	//					{
	//						if(Counter == 0)
	//						{
	//							ZeroLengthString.Append("");	
	//							//CA("ZeroLengthString : "+ ZeroLengthString);						
	//							TempString= &ZeroLengthString;
	//						}
	//						else
	//						{								
	//							(*TempString).Append("");
	//							//(*TempString).AppendW(&characterCode);
	//						}							
	//					}
	//				}
	//				else
	//				{  	
	//					//CA("Never Here");		
	//					//(*TempString).Append("");
	//					if(RemaningString != NULL)
	//					(*TempString).Append(*RemaningString);
	//					inpCode = (*TempString);
	//				}						
	//					
	//				indesignCode.Remove(0, Counter1+1);
	//				
	//				if(CurrIndesignCode)
	//					delete CurrIndesignCode;
	//				//CA(indesignCode);
	//			}
	//		}while(Counter1 != -1);
	//			
	//				
	//		//CA("For Remaining String");
	//		if(RemaningString != NULL)
	//		{
	//			(*TempString).Append((*RemaningString));
	//			//CA("After Appending RemaningString : " + (*TempString));

	//		}
	//		inpCode = (*TempString);

	//		if(RemaningString)
	//			delete RemaningString;
	//		if(TempStringFlagToDelete)
	//			if(TempString)
	//				delete TempString;
	//		//CA("inpCode : " +inpCode);				
	//	}	

	//}while(Counter != -1);

	//CA(inpCode);
	return inpCode;
}






//PMString SpecialChar::translateToDatabaseCode(const PMString& code)
//{
//	//CA("SpecialChar::translateToDatabaseCode");
//
//	const char* data=code.GrabCString();
//	char* txt= NULL;
//	PMString ResultString("");
//	const char *index=data;
//			
//	int32 length=std::strlen(data);
//
//	int32 i=0;
//	char *HexString=NULL ;
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//		return ResultString;
//
//
//	while(index[i])
//	{
//		PMString ABC("");
//		ABC.Append(index[i]);
//		UTF32TextChar temp = Utils<IEncodingUtils>()->StringToCharacterCode(ABC, IEncodingUtils::kCharInput);
//	
//		textchar customchar = temp.GetValue();
//		switch (customchar)
//		{
//		case kTextChar_Ampersand:
//			{
//				HexString= "0x0026";				
//				//ResultString.Append("<<APS100>>");
//				break;
//			}
//
//		case kTextChar_Bullet:
//			{
//				HexString= "0x2022";				
//				//ResultString.Append("<<APS101>>");
//				break;
//			}
//
//		case kTextChar_CopyrightSign:
//			{
//				HexString= "0x00A9";
//				ResultString.Append("<<APS102>>");
//				break;
//			}
//
//		case kTextChar_RegisteredSign:
//			{
//				HexString= "0x00AE";
//				ResultString.Append("<<APS103>>");
//				break;
//			}
//
//		case kTextChar_TrademarkSign:
//			{
//				HexString= "0x2122";
//				ResultString.Append("<<APS104>>");
//				break;
//			}
//
//		case kTextChar_PlusSign:
//			{
//				HexString= "0x002B";
//				ResultString.Append("<<APS105>>");
//				break;
//			}
//			
//		case kTextChar_Dagger:
//			{
//				HexString= "0x2020";
//				ResultString.Append("<<APS106>>");
//				break;
//			}
//		
//		case kTextChar_DoubleDagger:
//			{
//				HexString= "0x2021";
//				ResultString.Append("<<APS107>>");
//				break;
//			}
//
//		case kTextChar_EmDash:
//			{
//				HexString= "0x2014";
//				ResultString.Append("<<APS108>>");
//				break;
//			}
//
//		case kTextChar_DegreeSign:
//			{
//				HexString= "0x00B0";
//				ResultString.Append("<<APS110>>");
//				break;
//			}
//
//		case kTextChar_MathDelta:
//			{
//				HexString= "0x2206";
//				ResultString.Append("<<APS115>>");
//				break;
//			}
//
//		case kTextChar_CentSign:
//			{
//				HexString= "0x00A2";
//				ResultString.Append("<<APS116>>");
//				break;
//			}
//
//		case kTextChar_PoundSign:
//			{
//				HexString= "0x00A3";
//				ResultString.Append("<<APS117>>");
//				break;
//			}
//
//		case kTextChar_YenSign:
//			{
//				HexString= "0x00A5";
//				ResultString.Append("<<APS118>>");
//				break;
//			}
//
//		case kTextChar_Infinity:
//			{
//				HexString= "0x221E";
//				ResultString.Append("<<APS119>>");
//				break;
//			}
//
//		case kTextChar_LessEqual:
//			{
//				HexString= "0x2264";
//				ResultString.Append("<<APS120>>");
//				break;
//			}
//
//		case kTextChar_GreaterEqual:
//			{
//				HexString= "0x2265";
//				ResultString.Append("<<APS121>>");
//				break;
//			}
//
//		case kTextChar_Summation:
//			{
//				HexString= "0x2211";
//				ResultString.Append("<<APS122>>");
//				break;
//			}
//
//		case kTextChar_CR:
//			{
//				HexString= "0x000D";
//				ResultString.Append("<<APS126>>");
//				break;
//			}
//
//		case kTextChar_LeftDblAngQuote:
//			{
//				HexString= "0x00AB";
//				ResultString.Append("<<APS128>>");
//				break;
//			}
//
//		case kTextChar_LeftDoubleQuotationMark:
//			{
//				HexString= "0x201C";
//				ResultString.Append("<<APS129>>");
//				break;
//			}
//
//		case kTextChar_RightDoubleQuotationMark:
//			{
//				HexString= "0x201D";
//				ResultString.Append("<<APS130>>");
//				break;
//			}
//
//		case kTextChar_LF:
//			{
//				HexString= "0x000A";
//				ResultString.Append("<<APS131>>");
//				break;
//			}
//
//		default:
//			{	HexString= "";
//				ResultString.Append(index[i]);
//				break;
//			}
//		}
//
//		ResultString.Clear();
//	
//		PMString indesignCode(""); 				
//		indesignCode=ptrIAppFramework->SPCLCharMngr_getSpclCharacterByIndesignCode(HexString);
//		CA(indesignCode);
//		if(indesignCode.NumUTF16TextChars())
//		{
//			ResultString.Append("<<APS" + indesignCode + ">>");
//			CA(ResultString);
//		}
//		else
//		{
//			ResultString.Append(index[i]);
//		}
//					
//		i++;
//	}
//	//CA(ResultString);
//	//CA(" leaving special char ");
//	return ResultString;
//}


// New Unicode Version /////
PMString SpecialChar::translateToDatabaseCode(const PMString& code)
{
	//CA("SpecialChar::translateToDatabaseCode : " + code);	
	PMString ResultString("");			
	PMString inpCode= code;
	//int32 ToatalCharCount =inpCode.NumUTF16TextChars();	

	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return ResultString;

	//for(int32 i=0; i<ToatalCharCount; i++)
	//{   		
	//	PMString ABC("");
	//	PMString *TempString = inpCode.Substring/*SubWString*/(i, 1); //CS4
 //       ABC = (*TempString);

	//	if(TempString)            ///////////////////////param
	//		delete TempString;
	//	//CA("ABC : "+ ABC);
	//	UTF32TextChar temp = Utils<IEncodingUtils>()->StringToCharacterCode(ABC, IEncodingUtils::kCharInput);
	//	PMString HexString("");	
	//
	//	textchar customchar = temp.GetValue();
	//	switch (customchar)
	//	{
	//	case kTextChar_Ampersand:
	//		{
	//			HexString= "0x0026";				
	//			//ResultString.Append("<<APS100>>");
	//			break;
	//		}

	//	case kTextChar_Bullet:
	//		{
	//			HexString= "0x2022";				
	//			//ResultString.Append("<<APS101>>");
	//			break;
	//		}

	//	case kTextChar_CopyrightSign:
	//		{
	//			HexString= "0x00A9";
	//			//ResultString.Append("<<APS102>>");
	//			break;
	//		}

	//	case kTextChar_RegisteredSign:
	//		{
	//			HexString= "0x00AE";
	//			//ResultString.Append("<<APS103>>");
	//			break;
	//		}

	//	case kTextChar_TrademarkSign:
	//		{
	//			HexString= "0x2122";
	//			//ResultString.Append("<<APS104>>");
	//			break;
	//		}

	//	case kTextChar_PlusSign:
	//		{
	//			HexString= "0x002B";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}
	//		
	//	case kTextChar_Dagger:
	//		{
	//			HexString= "0x2020";
	//			//ResultString.Append("<<APS106>>");
	//			break;
	//		}
	//	
	//	case kTextChar_DoubleDagger:
	//		{
	//			HexString= "0x2021";
	//			//ResultString.Append("<<APS107>>");
	//			break;
	//		}

	//	case kTextChar_EmDash:
	//		{
	//			HexString= "0x2014";
	//			//ResultString.Append("<<APS108>>");
	//			break;
	//		}

	//	case kTextChar_DegreeSign:
	//		{
	//			HexString= "0x00B0";
	//			//ResultString.Append("<<APS110>>");
	//			break;
	//		}

	//	case kTextChar_MathDelta:
	//		{
	//			HexString= "0x2206";
	//			//ResultString.Append("<<APS115>>");
	//			break;
	//		}

	//	case kTextChar_CentSign:
	//		{
	//			HexString= "0x00A2";
	//			//ResultString.Append("<<APS116>>");
	//			break;
	//		}

	//	case kTextChar_PoundSign:
	//		{
	//			HexString= "0x00A3";
	//			//ResultString.Append("<<APS117>>");
	//			break;
	//		}

	//	case kTextChar_YenSign:
	//		{
	//			HexString= "0x00A5";
	//			//ResultString.Append("<<APS118>>");
	//			break;
	//		}

	//	case kTextChar_Infinity:
	//		{
	//			HexString= "0x221E";
	//			//ResultString.Append("<<APS119>>");
	//			break;
	//		}

	//	case kTextChar_LessEqual:
	//		{
	//			HexString= "0x2264";
	//			//ResultString.Append("<<APS120>>");
	//			break;
	//		}

	//	case kTextChar_GreaterEqual:
	//		{
	//			HexString= "0x2265";
	//			//ResultString.Append("<<APS121>>");
	//			break;
	//		}

	//	case kTextChar_Summation:
	//		{
	//			HexString= "0x2211";
	//			//ResultString.Append("<<APS122>>");
	//			break;
	//		}

	//	case kTextChar_CR:
	//		{
	//			HexString= "0x000D";
	//			//ResultString.Append("<<APS126>>");
	//			break;
	//		}

	//	case kTextChar_LeftDblAngQuote:
	//		{
	//			HexString= "0x00AB";
	//			//ResultString.Append("<<APS128>>");
	//			break;
	//		}

	//	case kTextChar_LeftDoubleQuotationMark:
	//		{
	//			HexString= "0x201C";
	//			//ResultString.Append("<<APS129>>");
	//			break;
	//		}

	//	case kTextChar_RightDoubleQuotationMark:
	//		{
	//			HexString= "0x201D";
	//			//ResultString.Append("<<APS130>>");
	//			break;
	//		}

	//	case kTextChar_LF:
	//		{
	//			HexString= "0x000A";
	//			//ResultString.Append("<<APS131>>");
	//			break;
	//		}

	//	case kTextChar_RightAlignedTab:
	//		{
	//			HexString= "0x0008";
	//			//ResultString.Append("<<APS131>>");
	//			break;
	//		}

	//	case 0x00B1:
	//		{
	//			HexString= "0x00B1";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case kTextChar_Solidus:
	//		{
	//			HexString= "0x002F";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case 0x03BC:  // Mu
	//		{
	//			HexString= "0x03BC";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   kTextChar_GreekPi:  // Pi
	//		{
	//			HexString= "0x03C0";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   kTextChar_GreekOmega:  // Omega
	//		{
	//			HexString= "0x03A9";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}			
	//
	//	case   0x00BC:  // One-fourth
	//		{
	//			HexString= "0x00BC";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00BD:  // One-half
	//		{
	//			HexString= "0x00BD";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00BE:  // Three-fourth
	//		{
	//			HexString= "0x00BE";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00C4:  // German A
	//		{
	//			HexString= "0x00C4";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00FC:  // German small u
	//		{
	//			HexString= "0x00FC";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00DC:  // German Capital u
	//		{
	//			HexString= "0x00DC";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	case   0x00E4:  // German Small A
	//		{
	//			HexString= "0x00E4";
	//			//ResultString.Append("<<APS105>>");
	//			break;
	//		}

	//	default:
	//		{	HexString= "";
	//			ResultString.Append(ABC);
	//			break;
	//		}
	//	}
	//
	//	//CA("HexString : "+ HexString);
	//	PMString indesignCode(""); 	
	//	//CA("623");
	//	//ptrIAppFramework->LogInfo("AP7_SpecialChar::SpecialChar::translateToDatabaseCode : HexString = " + HexString);
	//	if(HexString != "")
	//	{
	//		indesignCode=ptrIAppFramework->SPCLCharMngr_getSpclCharacterByIndesignCode(const_cast<char*>(HexString.GrabCString()/*GetPlatformString().c_str()*/));//CS4
	//			
	//		//CA("indesignCode : " + indesignCode);
	//		if(indesignCode.NumUTF16TextChars())
	//		{
	//			ResultString.Append("<<APS" + indesignCode + ">>");			
	//		}
	//		else
	//		{
	//			if(customchar == kTextChar_RightAlignedTab)
	//				ResultString.Append(kTextChar_Tab);
	//			else
	//				ResultString.Append(ABC);			
	//		}
	//	}
	//	else
	//		ptrIAppFramework->LogDebug("AP7_SpecialChar::SpecialChar::translateToDatabaseCode : HexString is blank for code = " + code );
	//}
	//CA("ResultString Output String : " + ResultString);
	//CA(" leaving special char ");
	ResultString = inpCode; //Apsiva 9 stub
	return ResultString;
}

PMString SpecialChar :: handleAmpersandCase(PMString & stringToDisplay)
{
		/*const char* data=stringToDisplay.GrabCString();
		int32 i=0,j=0;
		char index[1000] = "";
		PMString stringToInsert("");
		
		while(data[i])
		{
			if(data[i]!='&')
			{
				index[j] = data[i];
				i++;
				j++;
				continue;
			}
			else
			{
				index[j] = '&';
				index[++j] = '&';
				j++;
				i++;
			}
		}
		index[j]='\0';
		stringToInsert.Append(index);*/
		
		PMString ResultString("");			
		PMString inpCode= stringToDisplay;
		int32 ToatalCharCount =inpCode.NumUTF16TextChars();	
		PMString Ampersand("&");

		for(int32 i=0; i<ToatalCharCount; i++)
		{   		
			PMString ABC("");
			PMString *TempString = inpCode.Substring(i, 1); //cs4
			ABC = (*TempString);
			if(TempString)       /////////////////////////////param
				delete TempString;

			if(ABC.Compare(kTrue, Ampersand )==0)
			{	//CA("Compare true");
				ResultString.Append(ABC);
				ResultString.Append(ABC);
			}
			else
			{
				ResultString.Append(ABC);
			}
		}
		//CA("ResultString After Ampersand : " + ResultString);
		return ResultString;
}

void SpecialChar::ChangeQutationMarkONOFFState(bool16 State)
{
	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	IDataBase* db = ::GetDataBase(doc); 
	if (db == nil)
		return ;

	InterfacePtr<IWorkspace> workspace(doc->GetDocWorkSpace(), UseDefaultIID());

	InterfacePtr<ICommand> attributeCmd(CmdUtils::CreateCommand(kUseTypographersQuotesCmdBoss));
	InterfacePtr<IUIDData> useAttributeUidData(attributeCmd, IID_IUIDDATA);
	useAttributeUidData->Set(workspace);
	InterfacePtr<IBoolData> useAttributeBoolData(attributeCmd, IID_IBOOLDATA);
	useAttributeBoolData->Set(State);
	CmdUtils::ProcessCommand(attributeCmd);
}


PMString SpecialChar::translateStringNew(const PMString& code, VectorHtmlTrackerPtr vectHtmlPtr)
{	
	//CA("New One");
	PMString inpCode = code;
	//CA(inpCode);
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return code;

	//PMString KeyString("<<APS");	
 //   	CharCounter Counter = -1;
	//
	//do
	//{		
	//	Counter = inpCode.IndexOfString("<<APS");
	//	/*PMString ASD("Counter  : ");
	//	ASD.AppendNumber(Counter);
	//	CA(ASD);*/
 //      	if(Counter != -1)
	//	{	
	//		PMString *TempString = NULL;
	//		bool16 TempStringFlagToDelete = kFalse;
	//		int32 ToatalLength = inpCode.NumUTF16TextChars();

	//		PMString ZeroLengthString("");	
	//		PMString BlankString("");
	//		PMString NewBlankString("");

	//		if(Counter > 0)
	//		{
	//			TempString = inpCode.Substring(0, Counter); //cs4
	//			TempStringFlagToDelete = kTrue;
	//		}
	//		if(Counter == 0)
	//			TempString = &BlankString;

	//		PMString *ApsivaString = inpCode.Substring/*SubWString*/(Counter, 10);//Cs4
	//		
	//		//CA("TempString : " + *TempString);
	//		//CA("ApsivaString : " + *ApsivaString);
	//		PMString *RemaningString = NULL;
	//		if(Counter + 10 < ToatalLength)
	//		{
	//			RemaningString = inpCode.Substring/*SubWString*/(Counter+10); //Cs4
	//			//CA("RemaningString : " + *RemaningString);
	//		}

	//		PMString hexRep=getNumber(const_cast<char*> ((*ApsivaString).GrabCString()/*GetPlatformString().c_str()*/));//Cs4
	//		if(!hexRep.NumUTF16TextChars())
	//			break;
	//		if(ApsivaString)         //////////////////////param
	//			delete ApsivaString;

	//		//CA(hexRep);
	//		PMString indesignCode(""); 

	//		//CA(" ...157 ");
	//		indesignCode=ptrIAppFramework->SPCLCharMngr_getSpecialCharByCode(const_cast<char*>(hexRep.GrabCString()/*GetPlatformString().c_str()*/));//Cs4
	//		//CA(indesignCode);

	//		if((indesignCode == "1001") || (indesignCode == "1002") || (indesignCode == "1003")|| (indesignCode == "1004"))
	//		{
	//			if(vectHtmlPtr->size() == 0)
	//			{
	//				if(indesignCode == "1001" )
	//				{							
	//					HtmlTracker htmlTrackerObj;
	//					htmlTrackerObj.style = 1;
	//					htmlTrackerObj.startIndex = Counter;
	//					htmlTrackerObj.flag = kFalse;
	//					vectHtmlPtr->push_back(htmlTrackerObj);
	//				}
	//				else if(indesignCode == "1003")
	//				{   //CA("2");
	//					HtmlTracker htmlTrackerObj;
	//					htmlTrackerObj.style = 2;
	//					htmlTrackerObj.startIndex = Counter;
	//					htmlTrackerObj.flag = kFalse;
	//					vectHtmlPtr->push_back(htmlTrackerObj);
	//				}
	//			}
	//			else
	//			{
	//				if(indesignCode == "1001" )
	//				{
	//					//CA("3");
	//					HtmlTracker htmlTrackerObj;
	//					htmlTrackerObj.style = 1;
	//					htmlTrackerObj.startIndex = Counter;
	//					vectHtmlPtr->push_back(htmlTrackerObj);
	//				}
	//				else if(indesignCode == "1003")
	//				{
	//					//CA("4");
	//					HtmlTracker htmlTrackerObj;
	//					htmlTrackerObj.style = 2;
	//					htmlTrackerObj.startIndex = Counter;
	//					vectHtmlPtr->push_back(htmlTrackerObj);
	//				}
	//				else if(indesignCode == "1002" )
	//				{   						
	//					VectorHtmlTrackerValue::iterator it1;
	//					for(it1 = vectHtmlPtr->begin(); it1!=vectHtmlPtr->end(); it1++)
	//					{ 							
	//						if(it1->flag == kTrue)
	//							continue;
	//						
	//						if(it1->style != 1)
	//							continue;
	//						/*
	//						PMString ASD("5ooooooooooo Counter  : ");
	//						ASD.AppendNumber(Counter);
	//						ASD.Append(" it1->StartIndex : ");
	//						ASD.AppendNumber(it1->startIndex);
	//						CA(ASD);*/
	//						it1->EndIndex = Counter;
	//						it1->flag = kTrue;
	//						break;
	//					}
	//				}
	//				else if(indesignCode == "1004" )
	//				{	//CA("6");
	//					VectorHtmlTrackerValue::iterator it1;
	//					for(it1 = vectHtmlPtr->begin(); it1!=vectHtmlPtr->end(); it1++)
	//					{
	//						if(it1->flag == kTrue)
	//							continue;

	//						if(it1->style != 2)
	//							continue;

	//						it1->EndIndex = Counter;
	//						it1->flag = kTrue;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		//CA(".158");
	//		indesignCode.Append(",");
	//		//CA("indesignCode : "+indesignCode);
	//		
	//		CharCounter Counter1 = -1;
	//		CharCounter CurrPosition = 0;
	//		do
	//		{
	//			Counter1 = indesignCode.IndexOfString(",");
	//			if(Counter1 != -1)
	//			{
	//				//CA("Counter1 valid");
	//				if(Counter1 <2)
	//				break;
	//				
	//				/*PMString ZXC("Counter1 : ");
	//				ZXC.AppendNumber(Counter1);
	//				CA(ZXC);*/
	//				
	//				PMString *CurrIndesignCode = indesignCode.Substring(0, Counter1 );
	//				//CA("CurrIndesignCode : "+ *CurrIndesignCode);
	//				PMString CURRENTcode = *CurrIndesignCode;		
	//		
	//				if(CURRENTcode.NumUTF16TextChars()!= 0)
	//				{
	//					//CA("indesignCode.NumUTF16TextChars()!= 0 : "+ indesignCode);
	//					if(CURRENTcode.NumUTF16TextChars() ==6)
	//					{
	//						PMString* tempHex =  CURRENTcode.Substring(2);
	//						//CA(*tempHex);
	//						PMString newTemp = ("<");
	//						newTemp.Append(*tempHex);
	//						newTemp.Append(">");
	//						//CA(newTemp);
	//						bool16 Flag = newTemp.ParseForEmbeddedCharacters();
	//						if(Counter == 0)
	//						{
	//							ZeroLengthString.Append(newTemp);	
	//						//	CA("ZeroLengthString : "+ ZeroLengthString);						
	//							TempString= &ZeroLengthString;
	//						}
	//						else
	//						{							
	//							(*TempString).Append(newTemp);
	//							//(*TempString).AppendW(&characterCode);
	//						}

	//						if(tempHex)
	//							delete tempHex;
	//						//CA(newTemp);
	//						//CA("TempString = " + *TempString);
	//					}
	//					else
	//					{
	//						if(Counter == 0)
	//						{
	//							ZeroLengthString.Append("");	
	//						//	CA("ZeroLengthString : "+ ZeroLengthString);						
	//							TempString= &ZeroLengthString;
	//						}
	//						else
	//						{								
	//							(*TempString).Append("");
	//							//(*TempString).AppendW(&characterCode);
	//						}						
	//					}
	//				}
	//				else
	//				{  	
	//					//CA("Never Here");		
	//					//(*TempString).Append("");
	//					if(RemaningString != NULL)
	//					(*TempString).Append(*RemaningString);
	//					inpCode = (*TempString);
	//				}				
	//					
	//				indesignCode.Remove(0, Counter1+1);
	//				
	//				if(CurrIndesignCode)            
	//				{
	//					delete CurrIndesignCode;
	//				}
	//				//CA(indesignCode);
	//			}
	//		}while(Counter1 != -1);
	//			
	//				
	//		//CA("For Remaining String");
	//		if(RemaningString != NULL)
	//		{
	//			(*TempString).Append((*RemaningString));
	//			//CA("After Appending RemaningString : " + (*TempString));
	//		}
	//		inpCode = (*TempString);

	//		if(RemaningString)              ///////////////////param
	//		{
	//			delete RemaningString;
	//		}
	//		//CA("inpCode : " +inpCode);				
	//		if(TempStringFlagToDelete)            ///////////////////param
	//		{
	//			if(TempString)	
	//				delete TempString;
	//		}
	//	}
	//}while(Counter != -1);

	//CA(inpCode);
	return inpCode;
}

//void ToggleBold(InterfacePtr<ITextModel> textModel, PMString dataString)
//{
//
//
//
//}

//following function is applicable for following strings....
//"name: <APS,I,30024383,11000005> with  <APS,I,30024383,11000005>"
//"<APS,I,30024383,11000005> with  <APS,I,30024383,11000005> asde"
//" asdf "
//"";
//<APS,IA,1197,30024383> 
//<APS,PA,1197,30024383> 
// <APS,PV,itemId,AttributeId,PVId+activeIndicator> 
//Taglist and outString are out parameters..
//for above strings output will be ....
//outString = "name XYZ with PQR" and Taglist size will be 2
//outString = "XYZ with PQR asde" and taglist size will be 2
//outString = " asdf " and taglist size will be 0
//outString = "" and taglist size will be 0

//TagList SpecialChar::translateStringForAdvanceTableCell(const CellData & cellData ,PMString & outString)
//{
	//CA("SpecialChar::translateStringForAdvanceTableCell");
//	TagList tList;
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//			break;
//
//		vector<PMString >::iterator it;
//
//		vector<PMString > vec_Data;
//		vec_Data = cellData.cellContent;
//	
//		int32 vec_DataSize=static_cast<int32>(vec_Data.size());
//		if(vec_Data.size() == 0)
//		{
//			//CA("vec_Data.size() == 0");
//			outString="";
//			break;
//		}
//	
//		int32 cellDataCounter = 1;		
//
//		it = vec_Data.begin();
//		
//		PMString stringToTranslate = translateString(*it);
//		//CA("stringToTranslate	:	"+stringToTranslate);	    
//		PMString inpCode = stringToTranslate;
//		PMString KeyString("<APS");	
//    	CharCounter Counter = -1;
//		bool16 flag = kFalse;
//		int32 imageCount = 0;
//		int32 TotalLength = stringToTranslate.NumUTF16TextChars();;
//		do
//		{
//			Counter = inpCode.IndexOfString("<APS");
//
//			if(Counter != -1)
//			{
//				flag = kTrue;
//
//				PMString *TempString = NULL;
//				bool16 TempStringFlagToDelete = kFalse;//////////////////////params
//				int32 ToatalLength = inpCode.NumUTF16TextChars();
//
//				PMString BlankString("");
//
//				if(Counter > 0)
//				{
//					TempString = inpCode.Substring(0, Counter); //cs4
//					TempStringFlagToDelete = kTrue;
//				}
//				if(Counter == 0)
//					TempString = &BlankString;
//
//				outString.Append(*TempString);
//					
//				if(TempStringFlagToDelete)           ///////////////////param
//				{
//					if(TempString)
//						delete TempString;
//				}
//				CharCounter Counter1 = -1;
//				Counter1 = inpCode.IndexOfCharacter('>',Counter);
//
//				PMString *ApsivaString = inpCode.Substring/*SubWString*/(Counter, (Counter1 - Counter + 1)); //Cs4
//				//CA("ApsivaString : " + *ApsivaString); //<APS,I,30024383,11000005>
//				
//				int32 len = (*ApsivaString).NumUTF16TextChars();
//				/*PMString strLen("");
//				strLen.AppendNumber(len);
//				CA("strLen = " + strLen);*/
//
//				TagStruct tagInfo;
//				PlatformChar ch = (*ApsivaString).GetChar(5); 
//				
//				PMString *strParentID = NULL;
//				PMString *strID = NULL;
//
//				bool16 assetFound = kFalse;
//
//				if(ch == 'I')
//				{
//					//CA("ch == I");
//					PlatformChar ch1 = (*ApsivaString).GetChar(6);
//					if(ch1 == 'A')			
//						assetFound = kTrue;	
//					else
//						assetFound = kFalse;
//
//					tagInfo.whichTab = 4;	
//					
//					CharCounter Counter2 = -1;
//					if(assetFound)			
//					{
//						Counter2 = (*ApsivaString).IndexOfCharacter(',',8);
//						strParentID = (*ApsivaString).Substring/*SubWString*/(8, Counter2 - 8 );	//Cs4
//						tagInfo.imgFlag = 1;
//					//	tagInfo.parentId = (*strParentID).GetAsNumber();
//						strID = (*ApsivaString)./*SubWString*/Substring(Counter2 +1 , len -2 - Counter2);//Cs4
//						//CA("strID = " + *strID);
//						int32 assetID   = (*strID).GetAsNumber();
//						//PMString	d("assetId	=	"); 
//						//d.AppendNumber(assetID);
//						//CA(d);	
//						ptrIAppFramework->clearAllStaticObjects();
//						VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getAssetByAssetId(assetID);
//						if(AssetValuePtrObj == NULL)
//						{
//							ptrIAppFramework->LogInfo("AP7_SpecialChar::SpecialChar::translateStringForAdvanceTableCell::AssetValuePtrObj == NULL");	
//							//CA("Show Item images : AssetValuePtrObj == NULL");
//							break ;
//						}
//						if(AssetValuePtrObj->size() ==0)
//						{
//							ptrIAppFramework->LogInfo("AP7_SpecialChar::SpecialChar::translateStringForAdvanceTableCell::AssetValuePtrObj->size() ==0");	
//							//CA("AssetValuePtrObj->size() ==0");
//							break ;			
//						}
//						
//						VectorAssetValue::iterator it;
//						CAssetValue objCAssetvalue;
//						for(it = AssetValuePtrObj->begin() ; it!=AssetValuePtrObj->end() ; it++)
//						{
//							objCAssetvalue = *it;
//							tagInfo.parentId = objCAssetvalue.getParent_id();  
//							tagInfo.parentTypeID = objCAssetvalue.getParent_type_id();
//							tagInfo.typeId = objCAssetvalue.getType_id();
//						
//						/*	PMString a;
//							a.Append("parentId	:	");
//							a.AppendNumber(tagInfo.parentId);
//							a.Append("\r");
//							a.Append("parentTypeID	:	");
//							a.AppendNumber(tagInfo.parentTypeID);
//							a.Append("\r");
//							a.Append("typeId	:	");
//							a.AppendNumber(tagInfo.typeId);
//							CA(a);*/							
//						}
//						int32 lenOFoutString = outString.NumUTF16TextChars();
//						tagInfo.startIndex = imageCount + lenOFoutString /*- 1*/;
//						imageCount++;
//					}
//					else
//					{
//						//CA("Not An Asset");
//						if(ch1 == 'C')
//						{
//							Counter2 = (*ApsivaString).IndexOfCharacter(',',8);
//							strParentID = (*ApsivaString)./*SubWString*/Substring(8, Counter2 - 8 );//CS4
//							/*tagInfo.typeId = (*strParentID).GetAsNumber();*/
//							tagInfo.childId = (*strParentID).GetAsNumber();
//							strID = (*ApsivaString)./*SubWString*/Substring(Counter2 +1 , len -2 - Counter2);//Cs4
//							tagInfo.elementId = (*strID).GetAsNumber();
//							tagInfo.imgFlag = 0;
//							tagInfo.childTag = 1;
//						}
//						else
//						{
//							//CA("ApsivaString  = " + *ApsivaString);
//							Counter2 = (*ApsivaString).IndexOfCharacter(',',7);
//							strParentID = (*ApsivaString)./*SubWString*/Substring(7, Counter2 - 7 );//Cs4
//							/*tagInfo.typeId = (*strParentID).GetAsNumber();*/
//							tagInfo.childId = (*strParentID).GetAsNumber();
//							strID = (*ApsivaString)./*SubWString*/Substring(Counter2 +1 , len -2 - Counter2);//Cs4
//							//CA("strID = " + *strID);
//							tagInfo.elementId = (*strID).GetAsNumber();
//							tagInfo.imgFlag = 0;
//							tagInfo.childTag = 1;
//							//CA("strParentID = " + *strParentID);
//						}
//						
//						int32 lenOFoutString = outString.NumUTF16TextChars();
//						tagInfo.startIndex = lenOFoutString /*- 1*/;
//
//					}
//					//int32 lenOFoutString = outString.NumUTF16TextChars();
//					//tagInfo.startIndex = lenOFoutString /*- 1*/;
//
//					PMString tempString("");
//					if(cellDataCounter	< vec_Data.size())
//					{
//						it++;
//						tempString = translateString(*it);
//						//CA("tempString	:	"+tempString);
//						cellDataCounter++;
//					}
//					
//					outString.Append(tempString);
////CA("outString	:	"+outString);					
//					int32 lenOFoutString = -1;
//					lenOFoutString = outString.NumUTF16TextChars();
//					
//					if(assetFound == kFalse)
//						tagInfo.endIndex = lenOFoutString  - tagInfo.startIndex /*- 1*/;
//					else
//						tagInfo.endIndex = 	tagInfo.startIndex + 1;
//
//					tList.push_back(tagInfo);
//				}
//				if(ch == 'P')
//				{
//					PlatformChar ch1 = (*ApsivaString).GetChar(6);
//					bool16 isPVImage = kFalse;
//					if(ch1 == 'A')			
//						assetFound = kTrue;	
//					else if(ch1 == 'V')
//						isPVImage = kTrue;
//					else
//						assetFound = kFalse;
//
//					tagInfo.whichTab = 3;
//					
//					CharCounter Counter2 = -1;
//					
//					if(assetFound)			
//					{
//						Counter2 = (*ApsivaString).IndexOfCharacter(',',8);
//						strParentID = (*ApsivaString)./*SubWString*/Substring(8, Counter2 - 8 );	//Cs4
//						tagInfo.imgFlag = 1;
//						strID = (*ApsivaString)./*SubWString*/Substring(Counter2 +1 , len -2 - Counter2);//Cs4
//						int32 assetID   = (*strID).GetAsNumber();
//
//						ptrIAppFramework->clearAllStaticObjects();
//
//						VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getAssetByAssetId(assetID);
//						if(AssetValuePtrObj == NULL)
//						{
//							//CA("Show Product images : AssetValuePtrObj == NULL");
//							break ;
//						}
//						if(AssetValuePtrObj->size() == 0)
//						{
//							//CA("AssetValuePtrObj->size() == 0");
//							break ;			
//						}
//						
//						VectorAssetValue::iterator it;
//						CAssetValue objCAssetvalue;
//						for(it = AssetValuePtrObj->begin() ; it!=AssetValuePtrObj->end() ; it++)
//						{
//							objCAssetvalue = *it;
//							tagInfo.parentId = objCAssetvalue.getParent_id();  
//							tagInfo.parentTypeID = objCAssetvalue.getParent_type_id();
//							tagInfo.typeId = objCAssetvalue.getType_id();
//						}
//						
//						int32 lenOFoutString = outString.NumUTF16TextChars();
//						tagInfo.startIndex = imageCount + lenOFoutString /*- 1*/;
//						
//						imageCount++;
//					}
//					else if(isPVImage)
//					{
//						//CA("isPVImage");
//						Counter2 = (*ApsivaString).IndexOfCharacter(',',8);
//						strID = (*ApsivaString).Substring(8, Counter2 - 8 );	
//						int32 itemID   = (*strID).GetAsNumber();
//										
//						CharCounter Counter3 = (*ApsivaString).IndexOfCharacter(',',Counter2+1);
//						strID = (*ApsivaString).Substring(Counter2+1, Counter3 -Counter2 );	
//						int32 attributrID   = (*strID).GetAsNumber();
//											
//						strParentID = (*ApsivaString).Substring(Counter3 +1 , len -2 - Counter3);
//						int32 templen = strParentID->NumUTF16TextChars();
//						int32 PVID = strParentID->Substring(0,templen-1)->GetAsNumber();
//						int32 PVImageindex = strParentID->Substring(templen-1,templen)->GetAsNumber();
//							
//						//PMString s("tempPVID	:	");
//						//s.Append(tempPVID);
//						//s.Append("\nitemID	:	");
//						//s.AppendNumber(itemID);
//						//s.Append("\nattributrID	:	");
//						//s.AppendNumber(attributrID);
//						//s.Append("\nPVImageindex	:	");
//						//s.AppendNumber(PVImageindex);
//						//CA(s);
//
//						ptrIAppFramework->clearAllStaticObjects();
//						
//						PMString tempPVID("");
//						tempPVID.AppendNumber(PVID);
//						VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVAssetsList(tempPVID,itemID,PVImageindex);
//
//						if(AssetValuePtrObj == NULL)
//						{
//							//CA("AssetValuePtrObj == NULL");
//							break ;
//						}
//						if(AssetValuePtrObj->size() == 0)
//						{
//							//CA("AssetValuePtrObj->size() == 0");
//							break ;			
//						}
//												
//						tagInfo.parentId = AssetValuePtrObj->at(0).getParent_id();  
//						tagInfo.parentTypeID = AssetValuePtrObj->at(0).getParent_type_id();
//						PMString tempTypeID("");
//						tempTypeID.AppendNumber(AssetValuePtrObj->at(0).getType_id());
//						//tempTypeID.AppendNumber(PVImageindex);
//						tagInfo.typeId = tempTypeID.GetAsNumber();
//						tagInfo.elementId = attributrID;
//						tagInfo.imageIndex = PVImageindex;
//						int32 lenOFoutString = outString.NumUTF16TextChars();
//						tagInfo.startIndex = imageCount + lenOFoutString ;
//						tagInfo.imgFlag = 1;
//						tagInfo.whichTab = 4;
//						//tagInfo.rowno = PVImageindex;
//						imageCount++;
//						//CA("outOF isPVImage");
//					}
//					else
//					{
//						Counter2 = (*ApsivaString).IndexOfCharacter(',',7);
//						strParentID = (*ApsivaString)./*SubWString*/Substring(7, Counter2 - 7 );//Cs4
//						//CA("strParentID = " + *strParentID);
//						tagInfo.typeId = (*strParentID).GetAsNumber();
//						
//						strID = (*ApsivaString)./*SubWString*/Substring(Counter2 +1 , len -2 - Counter2);//Cs4
//						//CA("strID = " + *strID);
//						tagInfo.elementId = (*strID).GetAsNumber();
//						tagInfo.imgFlag = 0;
//
//						int32 lenOFoutString = outString.NumUTF16TextChars();
//						tagInfo.startIndex = lenOFoutString /*- 1*/;
//
//					}
//					//int32 lenOFoutString = outString.NumUTF16TextChars();
//					//tagInfo.startIndex = lenOFoutString /*- 1*/;
//					
//					
//					//outString.Append(*it);
//					PMString tempString("");
//					if(cellDataCounter	<	vec_Data.size())
//					{
//						it++;
//						tempString = translateString(*it);
//						cellDataCounter++;											
//					}
//					
//					outString.Append(tempString);
//				
//					int32 lenOFoutString = -1;
//					
//					lenOFoutString = outString.NumUTF16TextChars();
//					
//					if(assetFound == kFalse && tagInfo.elementId < 1)
//						tagInfo.endIndex = lenOFoutString  - tagInfo.startIndex /*- 1*/;
//					else
//						tagInfo.endIndex = 	tagInfo.startIndex + 1;
//				
//					tList.push_back(tagInfo);
//				}
//
//				if(Counter1 == ToatalLength-1)
//					break;
//
//				PMString *RemaningString = NULL;
//				bool16 RemaningStringFlagToDelete = kFalse;
//				if(Counter1 < ToatalLength)
//				{					
//					RemaningString = inpCode.Substring(Counter1+1); //cs4
//					RemaningStringFlagToDelete = kTrue;
//					//CA("RemaningString : " + *RemaningString);					
//				}
//
//				PMString key1(">");
//				if(!((*RemaningString).Contains(key1,0)))
//				{
//
//					if((*RemaningString).NumUTF16TextChars()>0)
//					{
//						outString.Append(*RemaningString);
//						//CA("LastString  = " + *RemaningString);
//					}
//					break;						
//				}
//				else
//				{
//					inpCode = *RemaningString;
//					
//				}
//				if(RemaningStringFlagToDelete)
//					if(RemaningString)
//						delete RemaningString;
//
//				if(ApsivaString)              
//					delete ApsivaString;
//
//				if(strParentID)
//					delete strParentID;
//				
//				if(strID)
//					delete strID;
//				
//			}            
//		}while(Counter != -1);
//			
//		if(TotalLength == 0)
//		{
//			//CA("TotalLength == 0");
//			outString.Append(stringToTranslate);
//			break;
//		}
//		if(TotalLength > Counter && flag == kFalse)
//		{
//			//CA("TotalLength > Counter = " + tempString);
//			outString.Append(inpCode);
//		}
//
//	}while(kFalse);
//	return tList;
//}




PMString SpecialChar::getNumber(char *code)
{
	char *start=NULL;
	char *iter=NULL;
	PMString retVal;

	start=std::strstr(code, "<<APS");
	if(!start)
	{
		return retVal;
	}
	start=start+5;
	if(!start)
	{
		return retVal;
	}

	int flag=0;

	for(iter=start; iter != NULL; iter++)
	{
		if(iter[0]=='>')
		{
			flag=1;
			break;
		}
		if(iter[0]<'0' || iter[0]>'9')
		{
			return retVal;
		}
		retVal.Append(iter[0]);
	}
	if(!flag)
		retVal.Clear();
	return retVal;
}



int SpecialChar::convertToHex(char *strNum)
{
	int i=0, tempNum, finalNum=0;
	int length=(int)std::strlen(strNum);
	char alpha;
	
	char binaryRep[255];
	char ret[5];

	std::strcpy(binaryRep, "");

	for(i=2; i<length; i++)
	{
		if(strNum[i]>='0' &&  strNum[i]<='9')
			tempNum=strNum[i]-'0';
		else
		{
			alpha=strNum[i];
			alpha=std::toupper(alpha);
			tempNum=alpha-'A'+10;
			if(tempNum>15 || tempNum<10)
				return -1;
		}
		if(!getBinary(tempNum, ret))
			return -1;
		std::strcat(binaryRep, ret);
	}

	tempNum=1;

	for(i=(int)std::strlen(binaryRep)-1;i>=0; i--)
	{
		finalNum=finalNum+(binaryRep[i]-'0')*tempNum;
		tempNum*=2;
	}
	return finalNum;
}

int SpecialChar::convertIndesignCodeIntoHexCode(char* hexNum)
{
	int hexVal=0;
	hexVal = this->convertToHex(hexNum);
	if(hexVal!=-1)
		return hexVal;
	return -1;
}

int SpecialChar::getBinary(int tempNum, char *val)
{
	char tmpVal[5];
	int count=3;
	int temp=tempNum;
	std::strcpy(tmpVal, "0000");
	while(tempNum>0 && count>=-1)
	{
		temp=tempNum%2;
		tempNum=tempNum/2;
		tmpVal[count]='0'+temp;
		count--;
	}
	std::strcpy(val, tmpVal);
	if(tempNum>0)
		return 0;
	return 1;
}

