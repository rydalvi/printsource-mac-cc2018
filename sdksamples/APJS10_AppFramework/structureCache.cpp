#include "VCPlugInHeaders.h"
#include "structureCache.h"
#include "CAlert.h"
#include "IAppFramework.h"

#define CA(x) CAlert::InformationAlert(x)


StructureCache* StructureCache::scObjPtr = NULL;
bool StructureCache::instanceFlag = false;
//StructureCacheValue* StructureCache::scValueObjPtr = NULL;

StructureCache* StructureCache::getInstance() // Singletone Implementation.
{
	//CA("Inside StructureCache::getInstance");
	if(!instanceFlag)
	{
		//CA("Creating Instance");
		scObjPtr = new StructureCache();
		instanceFlag = true;
		scObjPtr->loadStructureCache();
		return scObjPtr;
	}
	else
	{
		if(!scObjPtr->isClassTreeLoaded)
		{
			scObjPtr->loadStructureCache();
		}
		return scObjPtr;
	}
}

void StructureCache::clearInstace()
{
	isClassTreeLoaded = kFalse;
	structureCacheMap.clear();	
	itemCoreAttributeMap.clear();
	itemParametricAttributeMap.clear();
	AllItemAttributes.clear();
}

void StructureCache::myMethod()
{
	//CA("Inside MyMethod");
}

bool16 StructureCache::loadStructureCache()
{
	//StructureCache::scValueObjPtr = new StructureCacheValue();
	//CA("Inside loadStructureCache ");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return kFalse;

	}

	//PMString structureJson;

	bool16 result=ptrIAppFramework->callStructureJson(scValueObj);
	if(result)
	{		
			//CA(scValueObj.getClassificationName()); 
		isClassTreeLoaded = kTrue;
        
        vector<AttributeGroup> attributeGroups;
        bool16 resultAttributeGroup = ptrIAppFramework->callGetAttributeGroups(attributeGroups);
        if(resultAttributeGroup == kTrue)
        {
            scValueObj.setAttributeGroups(attributeGroups);
        }
        
		loadStructureCacheMap(scValueObj, -1);

	}
	else
	{
		isClassTreeLoaded = kFalse;
		return kFalse;
	}
	return kTrue;
}

void StructureCache::loadStructureCacheMap(StructureCacheValue & scChildObj, double parentId)
{
	scChildObj.setParentId(parentId);
	structureCacheMap.insert(map<double ,StructureCacheValue > ::value_type(scChildObj.getCategoryId(),scChildObj));

	if(parentId == -1)
	{
		if(scChildObj.getItemFields().size() > 0)
		{
			for(int count =0; count < scChildObj.getItemFields().size(); count ++ )
			{
				Attribute attrObj = scChildObj.getItemFields().at(count);
				itemCoreAttributeMap.insert(map<double ,Attribute > ::value_type(attrObj.getAttributeId(), attrObj));
				AllItemAttributes.push_back(attrObj);
			}
		}
	}
	else 
	{
		if(scChildObj.getItemFields().size() > 0)
		{
			for(int count =0; count < scChildObj.getItemFields().size(); count ++ )
			{
				Attribute attrObj = scChildObj.getItemFields().at(count);
				itemParametricAttributeMap.insert(map<double ,Attribute > ::value_type(attrObj.getAttributeId(), attrObj));
				AllItemAttributes.push_back(attrObj);
			}
		}
	}

	for(int childCount =0; childCount < scChildObj.getChildren().size(); childCount++ )
	{
		loadStructureCacheMap(scChildObj.getChildren()[childCount], scChildObj.getCategoryId());
		
	}
	
}

VectorClassInfoPtr StructureCache::ClassificationTree_getRoot(double LanguageID)
{
	VectorClassInfoPtr vectorClassificationValue = NULL;
	if(!isClassTreeLoaded)
	{
		loadStructureCache();
	}
	
	if(isClassTreeLoaded)
	{

		vectorClassificationValue = new VectorClassInfoValue;
		CClassificationValue ObjClassValue;

		ObjClassValue.setClassification_name(scValueObj.getClassificationName());
		ObjClassValue.setClass_id(scValueObj.getCategoryId());
		ObjClassValue.setParent_id(-2);		
		ObjClassValue.setChildCount((int32)(scValueObj.getChildren().size()));	
		ObjClassValue.setClass_level(1);
		vectorClassificationValue->push_back(ObjClassValue);
		classificationRootId = scValueObj.getCategoryId();  // Saving Classification Root Id in private membor of structureCache for quick referance.
	}

	return vectorClassificationValue;
}



VectorClassInfoPtr StructureCache::ClassificationTree_getAllChildren(double ClassID, double LanguageID)
{
	VectorClassInfoPtr vectorClassificationValue = NULL;
	if(!isClassTreeLoaded)
	{
		loadStructureCache();
	}
	
	if(isClassTreeLoaded)
	{

		StructureCacheValue scValueObj = getStructureCacheValueByClassId(ClassID);

		if(scValueObj.getChildren().size() > 0)
		{
			vectorClassificationValue = new VectorClassInfoValue;

			for(int count =0; count < scValueObj.getChildren().size(); count++)
			{
				StructureCacheValue scChildValueObj = scValueObj.getChildren().at(count);
				CClassificationValue ObjClassValue;
				ObjClassValue.setClassification_name(scChildValueObj.getClassificationName());
				ObjClassValue.setClass_id(scChildValueObj.getCategoryId());
				ObjClassValue.setParent_id(scValueObj.getCategoryId());		
				ObjClassValue.setChildCount((int32)(scChildValueObj.getChildren().size()));	
				ObjClassValue.setClass_level(3);
				vectorClassificationValue->push_back(ObjClassValue);
			}
		}				
	}

	return vectorClassificationValue;
}

StructureCacheValue StructureCache::getStructureCacheValueByClassId(double ClassID)
{
	StructureCacheValue scChidObj;
	map< double, StructureCacheValue>::iterator it;
	int iskeyInMap = (int)structureCacheMap.count(ClassID);
	if(iskeyInMap == 1)
	{
		it = structureCacheMap.find(ClassID);
		scChidObj = it->second;
	}
	return scChidObj;
}

VectorElementInfoPtr StructureCache::StructureCache_getItemGroupCopyAttributesByLanguageId(double languageId)
{
	VectorElementInfoPtr result = NULL;
	if(scValueObj.getItemGroupFields().size() > 0)
	{
		result = new VectorElementInfoValue;
		for(int count=0; count < scValueObj.getItemGroupFields().size(); count++ )
		{
			if(  scValueObj.getItemGroupFields().at(count).getPrintActive())
			{
				result->push_back(scValueObj.getItemGroupFields().at(count));
			}
		}	

	}
	return result;
}

VectorTypeInfoPtr StructureCache::StructureCache_getItemGroupImageAttributes()
{
	VectorTypeInfoPtr result = NULL;
	if(scValueObj.getItemGroupAssetTypes().size() > 0)
	{
		result = new VectorTypeInfoValue;
		for(int count=0; count < scValueObj.getItemGroupAssetTypes().size(); count++ )
		{
			if(  scValueObj.getItemGroupAssetTypes().at(count).getPrintActive())
			{
				result->push_back(scValueObj.getItemGroupAssetTypes().at(count));
			}
		}	
	}
	return result;
}

bool16 StructureCache::StructureCache_isElementMPV(double elementID)
{
	bool16 result = kFalse;
	if(scValueObj.getItemGroupFields().size() > 0)
	{
		
		for(int count=0; count < scValueObj.getItemGroupFields().size(); count++ )
		{
			if(  scValueObj.getItemGroupFields().at(count).getElementId() == elementID)
			{
				if(scValueObj.getItemGroupFields().at(count).getPicklistGroupId() > 0)
				{
					result = kTrue;
				}
				break;
			}
		}
	}
	return result;
}

double StructureCache::StructureCache_getPickListGroupIdByElementMPVId(double elementID)
{
	double pickListGroupId = -1;
	if(scValueObj.getItemGroupFields().size() > 0)
	{
		
		for(int count=0; count < scValueObj.getItemGroupFields().size(); count++ )
		{
			if(  scValueObj.getItemGroupFields().at(count).getElementId() == elementID)
			{
				if(scValueObj.getItemGroupFields().at(count).getPicklistGroupId() > 0)
				{
					pickListGroupId = scValueObj.getItemGroupFields().at(count).getPicklistGroupId();					
				}
				break;
			}
		}
	}
	return pickListGroupId;
}

VectorTypeInfoPtr StructureCache::StructureCache_getListTableTypes()
{
	VectorTypeInfoPtr result = NULL;
	if(scValueObj.getListTypes().size() > 0)
	{
		result = new VectorTypeInfoValue;
		for(int count=0; count < scValueObj.getListTypes().size(); count++ )
		{
			//if(isWebListTableType(scValueObj.getListTypes().at(count).getTypeId()))
			//	continue;
			if(  scValueObj.getListTypes().at(count).getPrintActive())
			{
				result->push_back(scValueObj.getListTypes().at(count));
			}
		}
	}
	return result;
}



VectorAttributeInfoPtr StructureCache::StructureCache_getItemAttributesForClassAndParents(double ClassID , double LanguageID)
{
	StructureCacheValue scChidObj;
	VectorAttributeInfoPtr result = NULL;
	result = new VectorAttributeInfoValue;
	map< double, StructureCacheValue>::iterator it;

	vector<double> classIdList = getAllParentClassIds(ClassID);
	/*PMString ASD("Parent Class List: ");
	ASD.AppendNumber((int32)classIdList.size());
	ASD.Append(" : ");*/
	
	
	for(int32 c= (int32)classIdList.size()-1; c >= 0 ; c--)
	{
		/*ASD.AppendNumber((int32)classIdList.at(c));
		ASD.Append(" , ");
		CA(ASD);*/

		if(classIdList.at(c) == -1)
			continue;

		double ClassificationID = classIdList.at(c);
		int iskeyInMap = (int)structureCacheMap.count(ClassificationID);
		if(iskeyInMap == 1)
		{
			it = structureCacheMap.find(ClassificationID);
			StructureCacheValue scChidObj;
			scChidObj = it->second;

			if(scChidObj.getItemFields().size() > 0)
			{		
				
				for(int count=0; count < scChidObj.getItemFields().size(); count++ )
				{				
					bool16 isParametric = kFalse;
					if(ClassificationID != classificationRootId) 
					{
						isParametric = kTrue;
					}

					Attribute itemAttrObject = scChidObj.getItemFields().at(count);
					itemAttrObject.setisParametric(isParametric);
					result->push_back(itemAttrObject);				
				}
			}
		}

	}
	
	return result;
}


VectorTypeInfoPtr StructureCache::StructureCache_getItemImagesForClassAndParents(double ClassID)
{
	StructureCacheValue scChidObj;
	VectorTypeInfoPtr result = NULL;
	map< double, StructureCacheValue>::iterator it;
	
	vector<double> classIdList = getAllParentClassIds(ClassID);
	result = new VectorTypeInfoValue;

	for(int32 c= (int32)classIdList.size()-1; c >= 0 ; c--)
	{
		
		if(classIdList.at(c) == -1)
			continue;

		int iskeyInMap = (int)structureCacheMap.count(classIdList.at(c));
		if(iskeyInMap == 1)
		{
			it = structureCacheMap.find(classIdList.at(c));
			scChidObj = it->second;

			if(scChidObj.getItemAssetTypes().size() > 0)
			{		
				
				for(int count=0; count < scChidObj.getItemAssetTypes().size(); count++ )
				{	
					if( scValueObj.getItemAssetTypes().at(count).getPrintActive())
					{
						result->push_back(scChidObj.getItemAssetTypes().at(count));		
					}
				}
			}
		}
	}
	return result;
}

VectorElementInfoPtr StructureCache::StructureCache_getSectionCopyAttributesByLanguageId(double languageId)
{
	VectorElementInfoPtr result = NULL;
	if(scValueObj.getSectionFields().size() > 0)
	{
		result = new VectorElementInfoValue;
		for(int count=0; count < scValueObj.getSectionFields().size(); count++ )
		{
			if(  scValueObj.getSectionFields().at(count).getPrintActive())
			{
				result->push_back(scValueObj.getSectionFields().at(count));
			}
		}	

	}
	return result;
}

VectorTypeInfoPtr StructureCache::StructureCache_getSectionImageAttributes()
{
	VectorTypeInfoPtr result = NULL;
	if(scValueObj.getSectionAssetTypes().size() > 0)
	{
		result = new VectorTypeInfoValue;
		for(int count=0; count < scValueObj.getSectionAssetTypes().size(); count++ )
		{
			if(  scValueObj.getSectionAssetTypes().at(count).getPrintActive())
			{
				result->push_back(scValueObj.getSectionAssetTypes().at(count));
			}
		}	
	}
	return result;
}

VectorLanguageModelPtr StructureCache::StructureCache_getAllLanguages()
{
	VectorLanguageModelPtr result = NULL;

	if(scValueObj.getLanguages().size() > 0)
	{
		result = new VectorLanguageModel;
		for(int count=0; count < scValueObj.getLanguages().size(); count++ )
		{
			result->push_back(scValueObj.getLanguages().at(count));
		}
	}
	return result;
}

vector<double> StructureCache::getAllParentClassIds(double classId)
{
	vector<double> parentIdList;
	parentIdList.push_back(classId);
	map< double, StructureCacheValue>::iterator it;
	//int iskeyInMap = (int)structureCacheMap.count(classId);
	int gotRoot = 0;
	do
	{
		int iskeyInMap = (int)structureCacheMap.count(classId);
		if(iskeyInMap == 1)
		{
			it = structureCacheMap.find(classId);
			StructureCacheValue scChidObj;
			scChidObj = it->second;
			double parentId = scChidObj.getParentId();
			parentIdList.push_back(parentId);
			if(parentId == -1)
			{
				gotRoot = 0;
			}
			else
			{
				gotRoot = 1;
			}
			classId = parentId;
		}

	}while(gotRoot)	;

	return parentIdList;
}

CClassificationValue StructureCache::StructureCache_getCClassificationValueByClassId(double classId, double LanguageID)
{
	CClassificationValue ObjClassValue;
	if(!isClassTreeLoaded)
	{
		loadStructureCache();
	}
	
	if(isClassTreeLoaded)
	{

		StructureCacheValue scValueObj = getStructureCacheValueByClassId(classId);
		
		ObjClassValue.setClassification_name(scValueObj.getClassificationName());
		ObjClassValue.setClass_id(scValueObj.getCategoryId());
		//ObjClassValue.setParent_id(scValueObj.getCategoryId());		
		ObjClassValue.setChildCount((int32)(scValueObj.getChildren().size()));	
		//ObjClassValue.setClass_level(3);						
	}

	return ObjClassValue;
}

double StructureCache::StructureCache_getUniqueItemGroupCopyElementId()
{
	double UniqueItemGroupCopyElementId = -1;

	if(scValueObj.getItemGroupFields().size() > 0)
	{		
		for(int count=0; count < scValueObj.getItemGroupFields().size(); count++ )
		{
			if(  scValueObj.getItemGroupFields().at(count).getPrintActive() && scValueObj.getItemGroupFields().at(count).getUniqueness() != 0 )
			{
				UniqueItemGroupCopyElementId = scValueObj.getItemGroupFields().at(count).getElementId();
				break;
			}
		}	

	}

	return UniqueItemGroupCopyElementId;
}

double StructureCache::StructureCache_getUniqueItemCopyAttributeId()
{
	double UniqueItemCopyAttributeId = -1;

	if(scValueObj.getItemFields().size() > 0)
	{		
		for(int count=0; count < scValueObj.getItemFields().size(); count++ )
		{
			if( scValueObj.getItemFields().at(count).getUniqueness() != 0 )
			{
				UniqueItemCopyAttributeId = scValueObj.getItemFields().at(count).getAttributeId();
				break;
			}
		}	
	}
	return UniqueItemCopyAttributeId;
}

Attribute StructureCache::StructureCache_getItemAttributeModelByAttributeId(double AttributeId)
{

	map< double, Attribute>::iterator it;
	Attribute attrObj;
	int iskeyInCoreMap = (int)itemCoreAttributeMap.count(AttributeId);		
	if(iskeyInCoreMap == 1)
	{
		it = itemCoreAttributeMap.find(AttributeId);			
		return it->second;			
	}
	int iskeyInParaMap = (int)itemParametricAttributeMap.count(AttributeId);
	if(iskeyInParaMap == 1)
	{
		it = itemParametricAttributeMap.find(AttributeId);			
		return it->second;	
	}
	return attrObj;
}

PMString StructureCache::StructureCache_getListNameByTypeId(double typeId)
{
	PMString result("");
	VectorTypeInfoPtr vectTypeInfoPtr = this->StructureCache_getListTableTypes();
	if(vectTypeInfoPtr != NULL && vectTypeInfoPtr->size() > 0)
	{
		VectorTypeInfoValue::iterator it1;

		for(it1=vectTypeInfoPtr->begin();it1!=vectTypeInfoPtr->end();it1++)
		{	
			if(typeId == it1->getTypeId())
			{
				result =  it1->getName();
				break;
			}
		}
	}
	return result; 
}

PMString StructureCache::StructureCache_TYPECACHE_getTypeNameById(double typeId)
{
	PMString result("");
	bool flag = false;
	if(scValueObj.getItemGroupAssetTypes().size() > 0)
	{
		VectorTypeInfoValue vectTypeInfo =(scValueObj.getItemGroupAssetTypes());
		if( vectTypeInfo.size() > 0)
		{
			

			for(int i=0; i < vectTypeInfo.size(); i++ )
			{	
				if(typeId == vectTypeInfo.at(i).getTypeId())
				{
					result =  vectTypeInfo.at(i).getName();
					flag = true;
					break;
				}
			}
		}
	}
	if(!flag && scValueObj.getItemAssetTypes().size() > 0)
	{
		VectorTypeInfoValue vectTypeInfo =(scValueObj.getItemAssetTypes());
		if( vectTypeInfo.size() > 0)
		{
			for(int i=0; i < vectTypeInfo.size(); i++ )
			{	
				if(typeId == vectTypeInfo.at(i).getTypeId())
				{
					result =  vectTypeInfo.at(i).getName();
					flag = true;
					break;
				}
			}
		}
	}

	return result; 
}


PicklistGroup StructureCache::StructureCache_getPickListGroups(double picklistGroupId)
{
	PicklistGroup result;
	if(scValueObj.getPicklistGroups().size() > 0)
	{		
		for(int count=0; count < scValueObj.getPicklistGroups().size(); count++ )
		{
			if(scValueObj.getPicklistGroups().at(count).getTypeId() == picklistGroupId)
			{
				result = scValueObj.getPicklistGroups().at(count);
				break;
			}
		}
	}
	return result;
}

// 0- No, 1- Brand, 2- Manufacturer, 3- Supplier/vendor 
int32 StructureCache::isItemAttributeOfBMS(double AttributeId)
{
	int32 result = 0;

	if(scValueObj.getItemFields().size() > 0)
	{		
		for(int count=0; count < scValueObj.getItemFields().size(); count++ )
		{
			if( (scValueObj.getItemFields().at(count).getAttributeId() == AttributeId) && (scValueObj.getItemFields().at(count).getTableCol() != "") )
			{
				if(scValueObj.getItemFields().at(count).getTableCol() == "BRAND_ID")
				{
					result =1;
					break;
				}
				else if(scValueObj.getItemFields().at(count).getTableCol() == "MANUFACTURER_ID")
				{
					result =2;
					break;
				}
				else if(scValueObj.getItemFields().at(count).getTableCol() == "SUPPLIER_ID")
				{
					result =3;
					break;
				}
			}
		}	
	}
	return result;
}


// 0- No, 1- Brand, 2- Manufacturer, 3- Supplier/vendor 
int32 StructureCache::isProductElementOfBMS(double ElementId)
{
	int32 result = 0;

	if(scValueObj.getItemGroupFields().size() > 0)
	{		
		for(int count=0; count < scValueObj.getItemGroupFields().size(); count++ )
		{
		if( (scValueObj.getItemGroupFields().at(count).getElementId() == ElementId) && (scValueObj.getItemGroupFields().at(count).getSysName() != "") )
			{
				if(scValueObj.getItemGroupFields().at(count).getSysName() == "BRAND_ID")
				{
					result =1;
					break;
				}
				else if(scValueObj.getItemGroupFields().at(count).getSysName() == "MANUFACTURER_ID")
				{
					result =2;
					break;
				}
				else if(scValueObj.getItemGroupFields().at(count).getSysName() == "SUPPLIER_ID")
				{
					result =3;
					break;
				}
			}
		}	
	}
	return result;
}

Attribute StructureCache::getBMSAttributeForItem(int32 option) // 1= Brand, 2= Manufacturer, 3= Supplier
{
	Attribute result;
	if(scValueObj.getItemFields().size() > 0)
	{		
		for(int count=0; count < scValueObj.getItemFields().size(); count++ )
		{
			if( (scValueObj.getItemFields().at(count).getTableCol() != "") )
			{
				if((option == 1) && (scValueObj.getItemFields().at(count).getTableCol() == "BRAND_ID") )
				{
					result = scValueObj.getItemFields().at(count);
					break;
				}
				else if((option == 2) && (scValueObj.getItemFields().at(count).getTableCol() == "MANUFACTURER_ID"))
				{
					result = scValueObj.getItemFields().at(count);
					break;
				}
				else if((option == 3) && (scValueObj.getItemFields().at(count).getTableCol() == "SUPPLIER_ID"))
				{
					result = scValueObj.getItemFields().at(count);
					break;
				}
			}
		}	
	}
	return result;

}


bool16 StructureCache::isWebListTableType(double tableType)
{
	bool16 result = kFalse;

	if(scValueObj.getListTypes().size() > 0)
	{
		
		for(int count=0; count < scValueObj.getListTypes().size(); count++ )
		{
			if((scValueObj.getListTypes().at(count).getTypeId() == tableType) && (scValueObj.getListTypes().at(count).getName().Compare(kFalse, "weblist") == 0))
			{
				result = kTrue;
				break;
			}
		}
	}
	
	return result;

}

VectorAttributeInfoPtr StructureCache::StructureCache_getAllItemAttributes(double LanguageID)
{
	VectorAttributeInfoPtr allItemAttributeListPtr = NULL;
	if(AllItemAttributes.size() > 0)
	{
		allItemAttributeListPtr =  new VectorAttributeInfoValue();
		*allItemAttributeListPtr = AllItemAttributes;
	}

	return allItemAttributeListPtr;
}

bool16 StructureCache::StructureCache_isAttributeMPV(double attributeId)
{
	bool16 result = kFalse;

	Attribute attObj = this->StructureCache_getItemAttributeModelByAttributeId(attributeId);
	if(attObj.getPicklistGroupId() > 0)
	{
		result =kTrue;
	}
	return result;
}

double StructureCache::StructureCache_getPickListGroupIdByAttributeMPVId(double attributeId)
{
	double pickListGroupId = -1;
	Attribute attObj = this->StructureCache_getItemAttributeModelByAttributeId(attributeId);
	if(attObj.getPicklistGroupId() > 0)
	{
		pickListGroupId =attObj.getPicklistGroupId();
	}	
	return pickListGroupId;
}

VectorAttributeGroupPtr StructureCache::StructureCache_getItemAttributeGroups()
{
    VectorAttributeGroupPtr result = NULL;
    if(scValueObj.getAttributeGroups().size() > 0)
    {
        result = new VectorAttributeGroup;
        for(int count=0; count < scValueObj.getAttributeGroups().size(); count++ )
        {
            
            result->push_back(scValueObj.getAttributeGroups().at(count));
        
        }	
    }
    return result;
}

vector<double> StructureCache::StructureCache_getItemAttributeListforAttributeGroupId(double attributeGroupId)
{
    vector<double> attributeList;
    if(scValueObj.getAttributeGroups().size() > 0)
    {
        
        for(int count=0; count < scValueObj.getAttributeGroups().size(); count++ )
        {
            if(attributeGroupId == scValueObj.getAttributeGroups().at(count).getAttributeGroupId())
            {
                attributeList = scValueObj.getAttributeGroups().at(count).getAttributeIds();
                break;
            }
        }
    }
    return attributeList;
}

vector<double> StructureCache::StructureCache_getItemAttributeListforAttributeGroupKey(PMString &attributeGroupKey)
{
    vector<double> attributeList;
    if(scValueObj.getAttributeGroups().size() > 0)
    {
        
        for(int count=0; count < scValueObj.getAttributeGroups().size(); count++ )
        {
            if(attributeGroupKey.IsEqual(scValueObj.getAttributeGroups().at(count).getKey()))
            {
                attributeList = scValueObj.getAttributeGroups().at(count).getAttributeIds();
                break;
            }
        }
    }
    return attributeList;
}

PMString StructureCache::StructureCache_getItemAttributeGroupNameByAttributeGroupId(double attributeGroupId)
{
    PMString result("");
    if(scValueObj.getAttributeGroups().size() > 0)
    {
        for(int count=0; count < scValueObj.getAttributeGroups().size(); count++ )
        {
            if(attributeGroupId == scValueObj.getAttributeGroups().at(count).getAttributeGroupId())
            {
                result = scValueObj.getAttributeGroups().at(count).getName();
                break;
            }
        }
    }
    return result;
}
