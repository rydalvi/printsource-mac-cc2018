#ifndef __STRUCTURECACHE__
#define __STRUCTURECACHE__


#include "AFWJSID.h"
#include "vector"
#include "map"
#include <set>
#include "list"
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"

#include "StructureCacheValue.h"
#include "ClassificationValue.h"
#include "IAppFramework.h"

using namespace std;

class StructureCache
{

private: 
		static StructureCache* scObjPtr;
		static bool instanceFlag ;
		StructureCacheValue scValueObj;
		bool16 isClassTreeLoaded;
		map< double, StructureCacheValue> structureCacheMap;
		double classificationRootId;

		StructureCache() 
		{
			// private constructor
			isClassTreeLoaded = kFalse;
			classificationRootId = -1;
		}
		
		map<double, Attribute> itemCoreAttributeMap;
		map<double, Attribute> itemParametricAttributeMap;		
		vector<Attribute> AllItemAttributes;
    
			
public:

	static StructureCache* getInstance(); // SingleTone Implementation.
	void clearInstace();

	~StructureCache() 
	{
		instanceFlag = false; 
	}

	void myMethod();
	bool16 loadStructureCache();
	void loadStructureCacheMap(StructureCacheValue & scChildObj , double parentId);

	VectorClassInfoPtr ClassificationTree_getRoot(double LanguageID);
	StructureCacheValue getStructureCacheValueByClassId(double ClassID);
	VectorClassInfoPtr ClassificationTree_getAllChildren(double ClassID, double LanguageID);	
	VectorElementInfoPtr StructureCache_getItemGroupCopyAttributesByLanguageId(double languageId);
	VectorTypeInfoPtr StructureCache_getItemGroupImageAttributes();
	bool16 StructureCache_isElementMPV(double elementID);
	VectorTypeInfoPtr StructureCache_getListTableTypes();
	VectorAttributeInfoPtr StructureCache_getItemAttributesForClassAndParents(double ClassID , double LanguageID);
	VectorTypeInfoPtr StructureCache_getItemImagesForClassAndParents(double ClassID);
	VectorElementInfoPtr StructureCache_getSectionCopyAttributesByLanguageId(double languageId);
	VectorTypeInfoPtr StructureCache_getSectionImageAttributes();
	VectorLanguageModelPtr StructureCache_getAllLanguages();
	vector<double> getAllParentClassIds(double classId);

	CClassificationValue StructureCache_getCClassificationValueByClassId(double classId, double LanguageID);

	double StructureCache_getUniqueItemGroupCopyElementId();
	double StructureCache_getUniqueItemCopyAttributeId();

	Attribute StructureCache_getItemAttributeModelByAttributeId(double AttributeId);
	PMString StructureCache_getListNameByTypeId(double typeId);
	PMString StructureCache_TYPECACHE_getTypeNameById(double typeId);
	PicklistGroup StructureCache_getPickListGroups(double picklistGroupId);

	int32 isItemAttributeOfBMS(double AttributeId); // 0- No, 1- Brand, 2- Manufacturer, 3- Supplier
	int32 isProductElementOfBMS(double ElementId); // 0- No, 1- Brand, 2- Manufacturer, 3- Supplier

	Attribute getBMSAttributeForItem(int32 option); // 1= Brand, 2= Manufacturer, 3= Supplier

	bool16 isWebListTableType(double tableType);

	VectorAttributeInfoPtr StructureCache_getAllItemAttributes(double LanguageID);
	bool16 StructureCache_isAttributeMPV(double attributeId);

	double StructureCache_getPickListGroupIdByElementMPVId(double elementID);
	double StructureCache_getPickListGroupIdByAttributeMPVId(double attributeId);
    
    VectorAttributeGroupPtr StructureCache_getItemAttributeGroups();
    vector<double> StructureCache_getItemAttributeListforAttributeGroupId(double attributeGroupId);
    vector<double> StructureCache_getItemAttributeListforAttributeGroupKey(PMString &attributeGroupKey);
    PMString StructureCache_getItemAttributeGroupNameByAttributeGroupId(double attributeGroupId);
    
};

#endif
