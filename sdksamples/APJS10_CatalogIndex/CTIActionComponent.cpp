//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "CTIID.h"
#include "CTIActionComponent.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "IControlView.h"

#include "IBookManager.h"
#include "IBook.h"
#include "IBookContentMgr.h"

#define CA(X)		CAlert::InformationAlert(X)

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs9_catalogindex

*/

IDialog* Firstdialog = NULL;

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CTIActionComponent, kCTIActionComponentImpl)
bool16 IsTheBookBlank()
{

	bool16 status = kFalse;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			return status;
		}

		InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::bookManager == nil");
			break; 
		}
		IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
		if(currentActiveBook == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::currentActiveBook == nil");
			break;			
		}
		InterfacePtr<IBookContentMgr> iBookContentMgr(currentActiveBook,UseDefaultIID());
		if(iBookContentMgr == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::iBookContentMgr == nil");
			break;
		}

		int32 contentCount = iBookContentMgr->GetContentCount();
		if(contentCount <= 0)
		{
			status = kTrue;
		}
	}while(kFalse);

	return status;
}

/* CTIActionComponent Constructor
*/
CTIActionComponent::CTIActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void CTIActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kCTIAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kCTIDialogActionID:
		{
			this->DoDialog();
			
		}
		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void CTIActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kCTIAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void CTIActionComponent::DoDialog()
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());//Cs4
		ASSERT(application);
		if (application == nil) {	
			ptrIAppFramework->LogDebug("AP46CatalogIndex::CTIActionComponent::DoDialog::application is nil");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIActionComponent::DoDialog::dialogMgr is nil");
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kCTIPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIActionComponent::DoDialog::dialog is nil");
			break;
		}
		Firstdialog =dialog;
		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}


void CTIActionComponent::CloseDialog()
{
if(Firstdialog){
		if(Firstdialog->IsOpen())
		{
			Firstdialog->Close();
		}
	}

}

void CTIActionComponent::UpdateActionStates (IActionStateList* iListPtr)
{	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->getLoginStatus();
	if(result)
	{
		iListPtr->SetNthActionState(0,kEnabledAction);		
	}
	else
	{
		iListPtr->SetNthActionState(0,kDisabled_Unselected);		
	}	
}

//*****A dded Cs4
void  CTIActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->getLoginStatus();
	bool16 isbookblank = IsTheBookBlank();
	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
	{
		ActionID actionID = listToUpdate->GetNthAction(iter);
		if(actionID == kCTIDialogActionID)
		{
			if(result && !isbookblank)
			{
				listToUpdate->SetNthActionState(0,kEnabledAction);		
			}
			else
			{
				listToUpdate->SetNthActionState(0,kDisabled_Unselected);		
			}	
		}
		
	}
}


//  Code generated by DollyXs code generator
