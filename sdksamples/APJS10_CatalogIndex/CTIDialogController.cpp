//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "CTIID.h"
#include "CreateIndex.h"
#include "GenerateIndex.h"
//#include "LayoutUtils.h"  //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "IWorkspace.h"
#include "IDocument.h"
#include "IIndexTopicList.h"
#include "IIndexTopicListList.h"
#include "IndexSectionHeader.h"
#include "IEditTopicEntryCmdData.h"
#include "ICreateTopicEntryCmdData.h"
#include "IndexTopicEntry.h"
#include "IActiveTopicListContext.h"
#include "ICreateTopicListCmdData.h"
// General includes
#include "ILayoutUIUtils.h"
#include "WideString.h"
#include "IPanelControlData.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "IAppFramework.h"
#include "SDKLayoutHelper.h"
#include "AttributeListData.h"
#include "IBookContentMgr.h"
#include "IBookUtils.h"
#include "IBookManager.h"
#include "IBook.h"
#include "IDataLink.h"
#include "ISession.h"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKUtilities.h"
#include "SDKFileHelper.h"
#include "CTIActionComponent.h"
#include "IBoolData.h"
#include "IIntData.h"
#include "IBookPaginateOptions.h"
#include "BookID.h"
#include "IBookContentCmdData.h"
#include "IDocumentCommands.h"
#include "IClientOptions.h"
#include "PMString.h"
#include "ICoreFilename.h"

bool16 isIndexRemovedFromBookAndCreatedForAnotherDoc = kFalse;
extern PMString gStrLangName;


#define CA(X)		CAlert::InformationAlert(X)

IControlView* IndexEntryListComboView = NULL;
vectorAttributeListData NewAttributeList;

vectorAttributeListData NewAttributeList1;
vectorAttributeListData NewAttributeList2;

vectorAttributeListData NewAttributeList11;
vectorAttributeListData NewAttributeList22;
vectorAttributeListData NewAttributeList33;
vectorAttributeListData NewAttributeList44;

double SelectedAttrID= -1;
extern double CurrentClassID;

extern double SelectedlanguageID ;

class BookContentDocInfo
{
public:
	PMString DocumentName;
	IDFile DocFile;
	int32 index;
	UID DocUID;
	//IDocument* documentPtr;
};

typedef vector<BookContentDocInfo> BookContentDocInfoVector;

/** CTIDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.
	
	@ingroup apjs9_catalogindex
*/
class CTIDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CTIDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~CTIDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);

		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
		BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
};

CREATE_PMINTERFACE(CTIDialogController, kCTIDialogControllerImpl)

/* ApplyFields
*/
void CTIDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{//CA(__FUNCTION__);

	CDialogController::InitializeDialogFields(dlgContext);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;


	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Interface for IClientOptions not found.");
		return;
	}

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	

	IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCTINewGroupPanelWidgetID);
	if(firstGroupPanelControlData == nil)
	{		
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::firstGroupPanelControlData == nil");
		return;
	}
	firstGroupPanelControlData->HideView();
	firstGroupPanelControlData->Disable();

	IControlView * GroupPanelControlData = panelControlData->FindWidget(kCTIOldGroupPanelWidgetID);
	if(GroupPanelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::GroupPanelControlData == nil");
		return;
	}
	GroupPanelControlData->ShowView();
	GroupPanelControlData->Enable();


//added by sagar to change in UI
	/*IControlView * ctiClusterPanelControlData = panelControlData->FindWidget(kCTIClusterPanelWidgetID);
	if(ctiClusterPanelControlData == nil)
	{		
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::ctiClusterPanelControlData == nil");
		CA("if(ctiClusterPanelControlData == nil)");
		return;
	}*/
//	ctiClusterPanelControlData->HideView();
//	ctiClusterPanelControlData->Disable();
	//CA("ctiClusterPanelControlData is hidden");
//till here
	//***
	IControlView *	selectlanguageDropDownView = panelControlData->FindWidget(kCTISelectlanguageDropDownWidgetID);
	if(!selectlanguageDropDownView)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::selectlanguageDropDownView == nil");
		return;
	}

	InterfacePtr<IStringListControlData> selectlanguageDropListData(selectlanguageDropDownView,UseDefaultIID());	
	if (selectlanguageDropListData==nil)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::selectlanguageDropListData == nil");
		return;
	}

	selectlanguageDropListData->Clear(kFalse,kFalse);

	InterfacePtr<IDropDownListController> selectlanguageDropListController1(selectlanguageDropDownView, UseDefaultIID());
	if (selectlanguageDropListController1==nil)
	{
       ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::selectlanguageDropListController1 == nil");
		return;
	}
	
	VectorLanguageModelPtr VectorLangNamePtr = ptrIAppFramework->StructureCache_getAllLanguages();
	if(VectorLangNamePtr == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::VectorLangNamePtr == nil");
		return;
	}
	
	VectorLanguageModel::iterator itr;
	int j = 0;
	for(itr = VectorLangNamePtr->begin(); itr != VectorLangNamePtr->end(); itr++)
	{		
		PMString localename(itr->getLangugeName());
        localename.ParseForEmbeddedCharacters();
		selectlanguageDropListData->AddString(localename, j, kFalse, kFalse);
		j++;
	}
	if(gStrLangName == ""){
		selectlanguageDropListController1->Select(0);
		SelectedlanguageID = VectorLangNamePtr->at(0).getLanguageID();
	}
	else
	{		
		int32 listIndx =0;
		for(itr = VectorLangNamePtr->begin(); itr != VectorLangNamePtr->end(); itr++,listIndx++)
		{
			if(gStrLangName.Compare(kTrue,itr->getLangugeName()) == 0){
				selectlanguageDropListController1->Select(listIndx);
				SelectedlanguageID = itr->getLanguageID();				
				break;
			}

		}
			
	}



	// Put code to initialize widget values here.

	
	bool16 IsOneSource = ptrIAppFramework->get_isONEsourceMode();


	/*InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
	if(dialogController == nil)
	{
		CA("dialogController == nil");
		return;
	}*/
	//dialogController->SetTriStateControlData(kCustomRadioButtonWidgetID/*kItemNumberRadioButtonWidgetID*/,kTrue);

	NewAttributeList11.clear();
	NewAttributeList22.clear();
	NewAttributeList33.clear();
	NewAttributeList44.clear();
	

}

/* ValidateFields
*/
WidgetID CTIDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void CTIDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
//CA(__FUNCTION__);
	// TODO add code that gathers widget values and applies them.
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID());//Cs4
	if (bookManager == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::There is not book manager!");
		return;
	}
	int32 bookCount = bookManager->GetBookCount();
	if (bookCount <= 0)
	{
		ptrIAppFramework->LogInfo("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::There is no book open. You must first open a book before running this snippet.");
		return;
	}

	PMString  ActiveBookName;
	bool16 result = Utils<IBookUtils>()->GetActiveBookName(ActiveBookName);


	vector<IDFile> BookFileList;
	BookFileList.clear();
	IBook* book = NULL;

	for (int32 i = 0 ; i < bookCount ; i++)
	{
		book = bookManager->GetNthBook(i);
		if (book == nil)
		{	//go onto the next book...
			continue;
		}
		PMString BookTitalName = book->GetBookTitleName();
		if(BookTitalName == ActiveBookName)
		{	
			InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::This book doesn't have a book content manager!  Something is wrong.");
				break;
			}
			BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
			if(BookContentinfovector== nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::BookContentinfovector is nil");			
				break;
			}

			if(BookContentinfovector->size()<=0)
			{
				ptrIAppFramework->LogInfo("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields:: No document found in Book! ");
				return;
			}

			BookContentDocInfoVector::iterator it1;
			for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
			{	
				IDFile  fileName= it1->DocFile ;
				BookFileList.push_back(fileName);
			}
			if(BookContentinfovector)
			{
				BookContentinfovector->clear();
				delete BookContentinfovector;
			}

		}
	}

	PMString attrName;
	attrName=this->GetTextControlData(kCTIIndexEntryComboBoxWidgetID);

	InterfacePtr<IStringListControlData>strListCtrlData(IndexEntryListComboView,UseDefaultIID());
	if(!strListCtrlData)
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::Strin list control data is null");
		return;
	}
	int32 index=strListCtrlData->GetIndex(attrName);
	if(index == 0)
	{
		CA("Please select the Index Entry");
		return;
	}

	AttributeListData CurrentAttribute = NewAttributeList[index-1];
	SelectedAttrID = CurrentAttribute.id;

	if(BookFileList.size() <=0)
	{
		ptrIAppFramework->LogInfo("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::BookFileList.size <=0");	
		return;
	}

	SnpIndexTopics createIndexObj;

	SDKLayoutHelper sdklhelp;
	PMString IndexFileName("");

	int lastpos = 0;
	for (int i = 0 ; i< ActiveBookName.CharCount();i++)
		if ((ActiveBookName[i] == '.') )
			lastpos = i;
	// At this point lastpos should point to the last delimeter, knock off the rest of the string.
	ActiveBookName.Truncate(ActiveBookName.CharCount()-lastpos);

	IndexFileName.Append(ActiveBookName);
	IndexFileName.Append("_Index.indd");



	bool16 IsIndexFileExist= kFalse;
	for(int32 i=0; i<BookFileList.size(); i++ )
	{
		IDFile flName= BookFileList[i];
		PMString BookFileName = flName.GetFileName();

		if(BookFileName == IndexFileName)
		{
			IsIndexFileExist = kTrue;
			break;
		}
	}

	IDFile flName= book->GetBookFileSpec();
	IDFile IndexFile;
	SDKUtilities sdkUtilityObj;
	PMString FileName("");
	sdkUtilityObj.GetPathName(flName,FileName);

	sdkUtilityObj.RemoveLastElement(FileName);
	FileName.Append("\\");
	FileName.Append(IndexFileName);
	IndexFile = sdkUtilityObj.PMStringToSysFile(&FileName);

	if(!IsIndexFileExist)
	{

		bool16  IsFileExists = FileUtils::DoesFileExist (IndexFile);
		if(IsFileExists)
		{
			InterfacePtr<ICoreFilename> corefn((ICoreFilename*)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));
			if (corefn)
			{
				const IDFile* tempIndexFile = &IndexFile;
				corefn->Initialize(tempIndexFile);
				bool16 isFileOPEN = corefn->IsFileOpen();
				if(isFileOPEN)
				{
					UIDRef CurrIndexDocRef =sdklhelp.OpenDocument(IndexFile);
					sdklhelp.SaveDocumentAs(CurrIndexDocRef, IndexFile);
					isIndexRemovedFromBookAndCreatedForAnotherDoc = kTrue;
				}
			}
		}
		do
		{
			InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
			if (bookContentMgr == nil)
			{
				CA("This book doesn't have a book content manager!  Something is wrong.");
				break;
			}


			if(!isIndexRemovedFromBookAndCreatedForAnotherDoc)
			{
				ErrorCode result1 = kFailure;
				UIDRef documentUIDRef = sdklhelp.CreateDocument();
				if (documentUIDRef == UIDRef::gNull) {
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::UIDRef is NULL");
					return;
				}

			// Open a view onto the document.
			
				result1 = sdklhelp.OpenLayoutWindow(documentUIDRef);
				sdklhelp.SaveDocumentAs(documentUIDRef, IndexFile);
			}
			
			UIDRef bookRef = ::GetUIDRef(book);
			ICommandSequence *piCmdSequence =CmdUtils::BeginCommandSequence();
			piCmdSequence->SetName("Add Document to Book");

			InterfacePtr<ICommand> piCreateContentCmd(
			CmdUtils::CreateCommand(kConstructContentCmdBoss) );
			if (!piCreateContentCmd)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::!piCreateContentCmd");
				break;
			}

			K2Vector<IDFile> docList;
			docList.push_back(IndexFile);

			InterfacePtr<IBookContentCmdData>
			piBookContentCmdData(piCreateContentCmd, IID_IBOOKCONTENTCMDDATA);
			piBookContentCmdData->SetTargetBook(flName);
			piBookContentCmdData->SetContentFile(docList);

			ErrorCode result;
			if ((result = CmdUtils::ProcessCommand(piCreateContentCmd)) !=	kSuccess)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::!kSuccess");
				break;
			}

			// 2. Process kAddDocToBookCmdBoss to add the book content to the book
			const UIDList *list = piCreateContentCmd->GetItemList();
		
			if (!(list && list->Length() > 0))
			{
				ptrIAppFramework->LogInfo("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::!(list && list->Length() > 0)");
				break;
			}


			InterfacePtr<ICommand>
			piAddDocCmd(CmdUtils::CreateCommand(kAddDocToBookCmdBoss));
			if (!piAddDocCmd)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::!piAddDocCmd");			
				break;
			}

			// The position where the doc is to be inserted within the book
			//  Valid values are -1 (beginning of book) to
			//IBookContentMgr::GetContentCount()-1 (end of book)
			int32 destPos = bookContentMgr->GetContentCount()-1;
			UIDList* nonConstList = const_cast<UIDList*>(list);
			InterfacePtr<IBookContentCmdData> piAddDocCmdData(piAddDocCmd,
			IID_IBOOKCONTENTCMDDATA);
			piAddDocCmdData->SetTargetBook(flName);
			piAddDocCmdData->SetContentFile(docList);
			piAddDocCmdData->SetContentList(nonConstList);
			piAddDocCmdData->SetDestPosition(destPos);

			result = CmdUtils::ProcessCommand(piAddDocCmd);

			// 3. Process kSetRepaginationCmdBoss to repaginate the book
			//after adding a new doc
			//  This step is optional; do it only if you want to renumber
			//all pages in other documents
			//  whose position in the book changes. This will modify and
			//save those docs.
			const UIDList *addList = piAddDocCmd->GetItemList();
			if (!(result == kSuccess && addList->Length() > 0))
			{
				ptrIAppFramework->LogInfo("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::result != kSuccess");		
				break;
			}

			InterfacePtr<ICommand>
			piRepaginateCmd(CmdUtils::CreateCommand(kSetRepaginationCmdBoss));
			if (!piRepaginateCmd)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::ApplyDialogFields::!piRepaginateCmd");			
				break;
			}

			// Get repagination options from the book and apply the same to
			//the command
			InterfacePtr<IBookPaginateOptions> piPaginateOptions(book,IID_IBOOKPAGINATEOPTIONS);
			IBookPaginateOptions::Options options =	piPaginateOptions->GetPaginateOptions();
			bool16 bInsert = piPaginateOptions->GetInsertBlankPage();
			bool16 bAutoRepaginate =piPaginateOptions->GetAutoRepaginateFlag();
			bool16 bForceToRepaginate = kTrue;

			InterfacePtr<IBookPaginateOptions>piPaginateCmdData(piRepaginateCmd, IID_IBOOKPAGINATEOPTIONS);
			piPaginateCmdData->SetPaginateOptions(options);
			piPaginateCmdData->SetInsertBlankPage(bInsert);
			piPaginateCmdData->SetAutoRepaginateFlag(bAutoRepaginate);
			piPaginateCmdData->SetCurrentBookUIDRef(bookRef);

			InterfacePtr<IIntData> piPosition(piRepaginateCmd,IID_IINTDATA);
			piPosition->Set(destPos);

			InterfacePtr<IBoolData> piForceRepaginate(piRepaginateCmd,IID_IBOOLDATA);
			piForceRepaginate->Set(bForceToRepaginate);

			result = CmdUtils::ProcessCommand(piRepaginateCmd);
			CmdUtils::EndCommandSequence(piCmdSequence);


		} while (false);
	}
	else
	{
		 UIDRef CurrIndexDocRef =sdklhelp.OpenDocument(IndexFile);
		 ErrorCode err = sdklhelp.OpenLayoutWindow(CurrIndexDocRef);
	}

	InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
	if (bookContentMgr == nil)
	{
		CA("This book doesn't have a book content manager!  Something is wrong.");
		return;
	}
	BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
	if(BookContentinfovector== nil)
		return;

	if(BookContentinfovector->size()<=0)
	{
		CA(" No document found in Book! ");
		return;
	}
	vector<IDFile> BookFileListNew;
	BookFileListNew.clear();
	BookContentDocInfoVector::iterator it2;
	for(it2 = BookContentinfovector->begin(); it2 != BookContentinfovector->end(); it2++)
	{
		IDFile  fileName= it2->DocFile ;
		BookFileListNew.push_back(fileName);
	}


	for(int32 i=0; i<BookFileListNew.size(); i++ )
	{
		IDFile flName= BookFileListNew[i];
		if(BookFileListNew[i] != IndexFile)
		{
			if(i > 0)
				CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

			UIDRef CurrDocRef = sdklhelp.OpenDocument(flName);
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);	

			VectorMarkerInfoPtr vectorMarkerInfoPtr = NULL;
			vectorMarkerInfoPtr = createIndexObj.getDocumentSelectedBoxIds(SelectedAttrID, 3);

			if(vectorMarkerInfoPtr != NULL)
			{
				int32 vectorMarkerInfoSize = static_cast<int32>(vectorMarkerInfoPtr->size());
				for(int32 markerInfoIndex=0; markerInfoIndex<vectorMarkerInfoSize; markerInfoIndex++)
				{	
					ErrorCode result = createIndexObj.CreatePageEntry(vectorMarkerInfoPtr->at(markerInfoIndex)); 
				}

				delete vectorMarkerInfoPtr;
			}
			
			sdklhelp.SaveDocumentAs(CurrDocRef,flName);
			sdklhelp.CloseDocument(CurrDocRef, kTrue);
		}
	}
	isIndexRemovedFromBookAndCreatedForAnotherDoc = kFalse;

	
	if(BookContentinfovector)
	{
		BookContentinfovector->clear();
		delete BookContentinfovector;
	}

}



BookContentDocInfoVector* CTIDialogController::GetBookContentDocInfo(IBookContentMgr* bookContentMgr)
{	//CA(__FUNCTION__);
	bool16 flag=kFalse;
	BookContentDocInfoVector* BookContentDocInfovectorPtr = new BookContentDocInfoVector;
	BookContentDocInfovectorPtr->clear();
	do {
		if (bookContentMgr == nil)
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		

		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			BookContentDocInfo contentDocInfo;
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content
			InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			if (bookLink == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
				break; // out of for loop
			}

			// get the book name and add it to the list
			PMString* baseName = bookLink->GetBaseName();
			ASSERT(baseName && baseName->IsNull() == kFalse);
			
			IDFile CurrFile;
			bool16 IsMissingPluginFlag = kFalse;

			IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

			bool16 Flag1 = kFalse;
			Flag1 =  FileUtils::DoesFileExist(CurrFile);
			if(Flag1 == kFalse)
			{
				continue;
			}
			else
			{				
				contentDocInfo.DocFile = CurrFile;	
				contentDocInfo.DocumentName = (*baseName);
				contentDocInfo.DocUID = contentUID;
				contentDocInfo.index = i;
				BookContentDocInfovectorPtr->push_back(contentDocInfo);
				flag=kTrue;
			}			
		}

	} while (false);
	if(flag)
		return BookContentDocInfovectorPtr;
	else
	{
		delete BookContentDocInfovectorPtr;
		return NULL;
	}
}

//  Code generated by DollyXs code generator
