//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
// Project includes:
#include "CTIID.h"
#include "IStringListControlData.h"
#include "CAlert.h"
#include "IDropDownListController.h"
#include "CDialogController.h"
#include "IAppFramework.h"
#include "IClientOptions.h"
#include "AttributeListData.h"
#include "IBookManager.h"
#include "IBook.h"
#include "IBookUtils.h"
#include "IBookContentMgr.h"
#include "IDataLink.h"
#include "FileUtils.h"
#include "CreateIndex.h"
#include "SDKLayoutHelper.h"
#include "SDKUtilities.h"
#include "ICoreFilename.h"
#include "IBookContentCmdData.h"
#include "IBookPaginateOptions.h"
#include "IIntData.h"
#include "IBoolData.h"
//#include "ITextFrame.h"
#include "IXMLReferenceData.h"
#include "IXMLUtils.h"



#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "IDataBase.h"
#include "IIndexTopicListList.h"
#include "IIndexTopicList.h"
#include "ICommand.h"
#include "UIDList.h"
#include "IDeleteIndexPageEntryCmdData.h"
#include "AcquireModalCursor.h"

#include "IDFile.h"
#include "SDKFileHelper.h"
#include "IDocFileHandler.h"
#include "IDocumentUtils.h"
#include "IGalleySettings.h"
#include <IInterfaceColors.h>
#include "IGalleySettings.h"
#include "IWorkspace.h"
#include "IDocumentList.h"
#include "IIndexTopicListList.h"
#include "IIndexTopicList.h"
#include "IndexTopicEntry.h"
#include "IndexTopicEntryNode.h"
#include "IActiveTopicListContext.h"
#include "ICreateTopicListCmdData.h"
#include "ICreateTopicEntryCmdData.h"
#include "ICreateIndexPageEntryCmdData.h"
#include "IIndexPageEntryCmdData.h"
#include "IRangeData.h"
#include "IInsertIndexMarkCmdData.h"
#include "ISelectionUtils.h"
#include "ITextSelectionSuite.h"
#include "IDeleteIndexPageEntryCmdData.h"
#include "IDeleteTopicEntryCmdData.h"
#include "IndexReference.h"
#include "IPageItemTypeUtils.h"
#include "IXMLReferenceData.h"
#include "IStyleInfo.h"
#include "ITextAttributes.h"
#include "IStyleGroupManager.h"
#include "IBookManager.h"
#include "IBookContentMgr.h"
#include "IBookUtils.h"
#include "IOpenLayoutCmdData.h"
#include "IDocumentCommands.h"
#include "ProgressBar.h"


class BookContentDocInfo
{
public:
	PMString DocumentName;
	IDFile DocFile;
	int32 index;
	UID DocUID;
	//IDocument* documentPtr;
};
typedef vector<BookContentDocInfo> BookContentDocInfoVector;


double SelectedAttrID1= -1;
double SelectedAttrID2= -1;


#define CA(X) CAlert::InformationAlert(X)
double CurrentClassID = -1;
extern vectorAttributeListData NewAttributeList;

extern vectorAttributeListData NewAttributeList1;
extern vectorAttributeListData NewAttributeList2;
extern vectorAttributeListData NewAttributeList11;
extern vectorAttributeListData NewAttributeList22;
extern vectorAttributeListData NewAttributeList33;
extern vectorAttributeListData NewAttributeList44;


PMString gStrLangName = "";
double SelectedlanguageID =-1;
bool16 isIndexRemovedFromBookAndCreatedForAnotherDocnew = kFalse;

static int32 TotalTagsInDocument ; 
static int32 TotalUnusedTagsRemoved;

/** Implements IObserver based on the partial implementation CDialogObserver.



	
	@ingroup apjs9_catalogindex
*/
class CTIDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CTIDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CTIDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);

		BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
//==================================================================
		//Following functions are added by vijay on 27-10-2006===========FROM HERE
		void populatePRDropDownbox();
		void populateItemDropDownbox();
		void populateProjectDropDownbox();
		void populateCatagoryDropDownbox();
//=========================================================UPTO HERE

//==================================================================
		//Following functions are added by Tushar on 16-11-2006===========FROM HERE
		void populatePRDropDownbox1();
		void populateItemDropDownbox1();
		void populateProjectDropDownbox1();
		void populateCatagoryDropDownbox1();
		void populatePRDropDownbox2();
		void populateItemDropDownbox2();
		void populateProjectDropDownbox2();
		void populateCatagoryDropDownbox2();
		
//=========================================================UPTO HERE
		void DeleteAllPageRefIndices(IDocument* iDocument);
		void CollectAllPageRefIndices(IDocument* iDocument,UIDList &pageRefList);
		void DoTopicNode( IIndexTopicList* pIndexTopicList,double sectionId,double parentNodeId,UIDList &pageRefs );
		void DeletePageRef(UIDRef &pageRef);

		void AddKeys(IIDXMLElement* childElement, set<PMString>& resultIds);
		void TagDocument(IIDXMLElement* childElement, IDocument* doc, vector<IndexReference> &indexReferences, UIDRef documentTopicListRef, bool isTable, set<WideString> &childIds, bool shouldTag);

		void DeleteAllTopics(IDocument* iDocument);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CTIDialogObserver, kCTIDialogObserverImpl)

/* AutoAttach
*/
void CTIDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		AttachToWidget(kCTIIndexEntryComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTISelectBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTIIndexEntryComboBox1WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTISelectBox1WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTIIndexEntryComboBox2WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTISelectBox2WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		
		AttachToWidget(kCTISelectlanguageDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCatalogIdexCancelIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCatalogIdexNextIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCTIBackIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCTIStartIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCTICancel1IconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		
		AttachToWidget(kCTILevel1DDBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kCTILevel2DDBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kLevel1DDBoxSIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kLevel2DDBoxSIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kLevel1DDBoxTIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kLevel2DDBoxTIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		
		

		// Attach to other widgets you want to handle dynamically here. 

	} while (kFalse);
}

/* AutoDetach
*/
void CTIDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		DetachFromWidget(kCTIIndexEntryComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTISelectBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTIIndexEntryComboBox1WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTISelectBox1WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTIIndexEntryComboBox2WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTISelectBox2WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		
		DetachFromWidget(kCTISelectlanguageDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCatalogIdexCancelIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCatalogIdexNextIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCTIBackIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCTIStartIconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCTICancel1IconWidgetID, IID_ITRISTATECONTROLDATA,panelControlData);
		
		

		DetachFromWidget(kCTILevel1DDBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kCTILevel2DDBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kLevel1DDBoxSIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kLevel2DDBoxSIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kLevel1DDBoxTIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kLevel2DDBoxTIWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		
		// Detach from other widgets you handle dynamically here.

	} while (kFalse);
}

/* Update
*/
void CTIDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		bool16 customRadioFlag = kFalse;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		break;

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Interface for IClientOptions not found.");
			return;
		}
		/*if(theSelectedWidget == kCTISelectBoxWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{	
			
			bool16 IsOneSource = ptrIAppFramework->get_isONEsourceMode();

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView* SelectComboView = panelControlData->FindWidget(kCTISelectBoxWidgetID);
			if(SelectComboView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectComboView1 is nil");			
				return;
			}
	    
			InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
			if(SelectDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController is nil");
				return;	
			}

			int32 selectedIndex = SelectDropListController->GetSelected();

			IControlView* IndexEntryComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
			if(IndexEntryComboView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryComboView is nil");			
				return;
			}
	
			if( selectedIndex == 1)
			{	
				IndexEntryComboView->Enable();
				if(IsOneSource)
					populateCatagoryDropDownbox();
				
				else
					populateProjectDropDownbox();
			}
			else if( selectedIndex == 2)
			{
				IndexEntryComboView->Enable();
				populatePRDropDownbox();
			}
			else if( selectedIndex == 3)
			{
				IndexEntryComboView->Enable();
				populateItemDropDownbox();
			}
			if( selectedIndex == 0)
			{
						
				InterfacePtr<IDropDownListController> SelectDropListController(IndexEntryComboView, UseDefaultIID());
				if(SelectDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController is nil");					
					return;
				}

				SelectDropListController->Select(0);
				IndexEntryComboView->Disable();

				IControlView* okButtonControlView = panelControlData->FindWidget(kOKButtonWidgetID);
				if(okButtonControlView == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::okButtonControlView is nil");				
					return;
				}

				okButtonControlView->Disable();



			}
		}*/

		/*
		if(theSelectedWidget == kCTISelectBox1WidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{	
			
			bool16 IsOneSource = ptrIAppFramework->get_isONEsourceMode();

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView* SelectComboView = panelControlData->FindWidget(kCTISelectBox1WidgetID);
			if(SelectComboView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectComboView is nil");							
				return;
			}
	    
			InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
			if(SelectDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController is nil");			
				return;	
			}

			int32 selectedIndex = SelectDropListController->GetSelected();
			if( selectedIndex == 1)
			{			
				if(IsOneSource)
					populateCatagoryDropDownbox1();
				
				else
					populateProjectDropDownbox1();
			}
			else if( selectedIndex == 2)
			{
				populatePRDropDownbox1();
			}
			else if( selectedIndex == 3)
			{
				populateItemDropDownbox1();
			}
			
			IControlView* IndexEntryCombo1View = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
			if(IndexEntryCombo1View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo1View is nil");			
				return;
			}

			IControlView* startButtonControlView = panelControlData->FindWidget(kCTIStartIconWidgetID);
			if(startButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::startButtonControlView is nil");			
				return;
			}

			if(selectedIndex == 1 || selectedIndex == 2 || selectedIndex == 3)
			{
				IndexEntryCombo1View->Enable();
			}
			else
			{
				IControlView* IndexEntryCombo1View = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
				if(IndexEntryCombo1View == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo1View is nil");
					return;
				}
				
				InterfacePtr<IDropDownListController> SelectDropListController1(IndexEntryCombo1View, UseDefaultIID());
				if(SelectDropListController1 == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController1 is nil");
					return;
				}

				SelectDropListController1->Select(0);
				IndexEntryCombo1View->Disable();
				startButtonControlView->Disable();
			}
		}
		*/

		/*
		if(theSelectedWidget == kCTISelectBox2WidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{	
			
			bool16 IsOneSource = ptrIAppFramework->get_isONEsourceMode();

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView* SelectComboView = panelControlData->FindWidget(kCTISelectBox2WidgetID);
			if(SelectComboView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectComboView is nil");
				return;
			}
	    
			InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
			if(SelectDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController is nil");			
				return;	
			}

			int32 selectedIndex = SelectDropListController->GetSelected();
			if( selectedIndex == 1)
			{			
				if(IsOneSource)
					populateCatagoryDropDownbox2();
				
				else
					populateProjectDropDownbox2();
			}
			else if( selectedIndex == 2)
			{
				populatePRDropDownbox2();
			}
			else if( selectedIndex == 3)
			{
				populateItemDropDownbox2();
			}


			IControlView* IndexEntryCombo2View = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
			if(IndexEntryCombo2View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo2View is nil");						
				return;
			}
			
			IControlView* startButtonControlView = panelControlData->FindWidget(kCTIStartIconWidgetID);
			if(startButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::startButtonControlView is nil");			
				return;
			}

			if(selectedIndex == 1 || selectedIndex == 2 || selectedIndex == 3)
			{
				IndexEntryCombo2View->Enable();
			}
			else
			{
				IControlView* IndexEntryCombo2View = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
				if(IndexEntryCombo2View == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo2View is nil");							
					return;
				}
				
				InterfacePtr<IDropDownListController> SelectDropListController2(IndexEntryCombo2View, UseDefaultIID());
				if(SelectDropListController2 == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController2 is nil");
					return;
				}

				SelectDropListController2->Select(0);
				IndexEntryCombo2View->Disable();
				startButtonControlView->Disable();
			}
		}
		*/

	/*
		if(theSelectedWidget == kCTIIndexEntryComboBoxWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			
			IControlView* IndexEntryComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
			if(IndexEntryComboView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryComboView is nil");
				return;
			}
			
			InterfacePtr<IDropDownListController> SelectDropListController(IndexEntryComboView, UseDefaultIID());
			if(SelectDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController is nil");			
				return;
			}

			int32 index=SelectDropListController->GetSelected();

			IControlView* okButtonControlView = panelControlData->FindWidget(kOKButtonWidgetID);
			if(okButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::okButtonControlView is nil");
				return;
			}
	
			if(index != 0 )
			{
				okButtonControlView->Enable();
			}
			else
				okButtonControlView->Disable();


		}

		if(theSelectedWidget == kCTIIndexEntryComboBox1WidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			
			IControlView* IndexEntryCombo1View = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
			if(IndexEntryCombo1View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo1View is nil");
				return;
			}
			
			InterfacePtr<IDropDownListController> SelectDropListController1(IndexEntryCombo1View, UseDefaultIID());
			if(SelectDropListController1 == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController1 is nil");
				return;
			}

			int32 index=SelectDropListController1->GetSelected();

			IControlView* IndexEntryCombo2View = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
			if(IndexEntryCombo2View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo2View is nil");
				return;
			}

			InterfacePtr<IDropDownListController> SelectDropListController2(IndexEntryCombo2View, UseDefaultIID());
			if(SelectDropListController2 == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController2 is nil");
				return;
			}

			int32 index1=SelectDropListController2->GetSelected();

			IControlView* startButtonControlView = panelControlData->FindWidget(kCTIStartIconWidgetID);
			if(startButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::startButtonControlView is nil");
				return;
			}
	
			if(index != 0 && index1 != 0 )
			{
				startButtonControlView->Enable();
			}
			else
				startButtonControlView->Disable();
		}

		*/

		/*
		if(theSelectedWidget == kCTIIndexEntryComboBox2WidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			
			IControlView* IndexEntryCombo1View = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
			if(IndexEntryCombo1View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo1View is nil");
				return;
			}
			
			InterfacePtr<IDropDownListController> SelectDropListController1(IndexEntryCombo1View, UseDefaultIID());
			if(SelectDropListController1 == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController1 is nil");
				return;
			}

			int32 index=SelectDropListController1->GetSelected();

			IControlView* IndexEntryCombo2View = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
			if(IndexEntryCombo2View == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IndexEntryCombo2View is nil");
				return;
			}

			InterfacePtr<IDropDownListController> SelectDropListController2(IndexEntryCombo2View, UseDefaultIID());
			if(SelectDropListController2 == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SelectDropListController2 is nil");
				return;
			}

			int32 index1=SelectDropListController2->GetSelected();

			
			
			IControlView* startButtonControlView = panelControlData->FindWidget(kCTIStartIconWidgetID);
			if(startButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::startButtonControlView is nil");
				return;
			}
			
			if(index != 0 && index1 != 0 )
			{
				startButtonControlView->Enable();
			}
			else
				startButtonControlView->Disable();

		}

		*/

		if(theSelectedWidget == kCTIBackIconWidgetID/*kCTIBackButtonWidgetID*/ && theChange==kTrueStateMessage)
		{
			
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			
			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCTINewGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::firstGroupPanelControlData is nil");
				break;
			}
			firstGroupPanelControlData->HideView();
			firstGroupPanelControlData->Disable();

			IControlView * zerothGroupPanelControlData = panelControlData->FindWidget(kCTIOldGroupPanelWidgetID);
			if(zerothGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::zerothGroupPanelControlData == nil");
				break;
			}
			zerothGroupPanelControlData->ShowView();
            zerothGroupPanelControlData->Enable();
			IControlView* plevel1ControlView =panelControlData->FindWidget(kCTILevel1DDBoxWidgetID);
			if (plevel1ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel1ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel1DDListController(plevel1ControlView, UseDefaultIID());
			if (plevel1DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel1DDListController invalid");
				break;
			}
			plevel1DDListController->Select(0);

			IControlView* plevel2ControlView =panelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
			if (plevel2ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel2DDListController(plevel2ControlView, UseDefaultIID());
			if (plevel2DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
				break;
			}
			plevel2DDListController->Select(0);

			//by Amarjit
			IControlView* plevel3ControlView =panelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
			if (plevel3ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel3DDListController(plevel3ControlView, UseDefaultIID());
			if (plevel3DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
				break;
			}
			plevel3DDListController->Select(0);

		}

		if((theSelectedWidget == kCatalogIdexCancelIconWidgetID || theSelectedWidget ==kCancelButton_WidgetID) && theChange==kTrueStateMessage)
		{
			if(theSelectedWidget == kCatalogIdexCancelIconWidgetID)
			CDialogObserver::CloseDialog();		
		}

		if((theSelectedWidget == kCatalogIdexNextIconWidgetID || theSelectedWidget == kOKButtonWidgetID) && theChange==kTrueStateMessage)
		{
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView * zerothGroupPanelControlData = panelControlData->FindWidget(kCTIOldGroupPanelWidgetID);
			if(zerothGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::zerothGroupPanelControlData == nil");
				break;
			}
			zerothGroupPanelControlData->HideView();
			zerothGroupPanelControlData->Disable();


			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCTINewGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->ShowView();
			firstGroupPanelControlData->Enable();

			InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				break;
			}
			Attribute cAtrrModelObj;
			WidgetID selectedRadioWidgetID = dialogController->GetSelectedClusterWidget(kCTIClusterPanelWidgetID); 

			InterfacePtr<IStringListControlData> level1DropListData(dialogController->QueryListControlDataInterface(kCTILevel1DDBoxWidgetID));
			if(level1DropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1DropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level1DropListController(level1DropListData, UseDefaultIID());
			if(level1DropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1DropListController is nil");	
				return;
			}

			level1DropListData->Clear(kFalse, kFalse);
			PMString str3("--Select--");
			str3.SetTranslatable(kFalse);
			level1DropListData->AddString(str3);
		
			InterfacePtr<IStringListControlData> level2DropListData(dialogController->QueryListControlDataInterface(kCTILevel2DDBoxWidgetID));
			if(level2DropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level2DropListController(level2DropListData, UseDefaultIID());
			if(level2DropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListController is nil");	
				return;
			}

			level2DropListData->Clear(kFalse, kFalse);
			level2DropListData->AddString(str3);
				 
			InterfacePtr<IStringListControlData> level3DropListData(dialogController->QueryListControlDataInterface(kCTILevel3DDBoxWidgetID));
			if(level3DropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3DropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level3DropListController(level3DropListData, UseDefaultIID());
			if(level3DropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3DropListController is nil");	
				return;
			}

			level3DropListData->Clear(kFalse, kFalse);
			level3DropListData->AddString(str3);

			InterfacePtr<IStringListControlData> level1SIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxSIWidgetID));
			if(level1SIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level1SIDropListController(level1SIDropListData, UseDefaultIID());
			if(level1SIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListController is nil");	
				return;
			}

			level1SIDropListData->Clear(kFalse, kFalse);
			level1SIDropListData->AddString(str3);
		
			InterfacePtr<IStringListControlData> level2SIDropListData(dialogController->QueryListControlDataInterface(kLevel2DDBoxSIWidgetID));
			if(level2SIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level2SIDropListController(level2SIDropListData, UseDefaultIID());
			if(level2SIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListController is nil");	
				return;
			}

			level2SIDropListData->Clear(kFalse, kFalse);
			level2SIDropListData->AddString(str3);

			InterfacePtr<IStringListControlData> level3SIDropListData(dialogController->QueryListControlDataInterface(kLevel3DDBoxSIWidgetID));
			if(level3SIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3SIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level3SIDropListController(level3SIDropListData, UseDefaultIID());
			if(level3SIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3SIDropListController is nil");	
				return;
			}

			level3SIDropListData->Clear(kFalse, kFalse);
			level3SIDropListData->AddString(str3);

			InterfacePtr<IStringListControlData> level1TIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxTIWidgetID));
			if(level1TIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level1TIDropListController(level1TIDropListData, UseDefaultIID());
			if(level1TIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");	
				return;
			}

			level1TIDropListData->Clear(kFalse, kFalse);
			level1TIDropListData->AddString(str3);

			InterfacePtr<IStringListControlData> level2TIDropListData(dialogController->QueryListControlDataInterface(kLevel2DDBoxTIWidgetID));
			if(level2TIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2TIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level2TIDropListController(level2TIDropListData, UseDefaultIID());
			if(level2TIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2TIDropListData is nil");	
				return;
			}

			level2TIDropListData->Clear(kFalse, kFalse);
			level2TIDropListData->AddString(str3);

			InterfacePtr<IStringListControlData> level3TIDropListData(dialogController->QueryListControlDataInterface(kLevel3DDBoxTIWidgetID));
			if(level3TIDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3TIDropListData is nil");
				return;
			}

			InterfacePtr<IDropDownListController> level3TIDropListController(level3TIDropListData, UseDefaultIID());
			if(level3TIDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level3TIDropListData is nil");	
				return;
			}

			level3TIDropListData->Clear(kFalse, kFalse);
			level3TIDropListData->AddString(str3);

			IControlView * Level2DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
			if(Level2DDBoxWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
				break;
			}
			// By Amarjit patil

			IControlView * Level3DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
			if(Level3DDBoxWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
				break;
			}


			IControlView * Level1DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
			if(Level1DDBoxSIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlData == nil");
				break;
			}
			IControlView * Level2DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxSIWidgetID);
			if(Level2DDBoxSIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
				break;
			}

			//by Amarjit patil

			IControlView * Level3DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxSIWidgetID);
			if(Level3DDBoxSIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlData == nil");
				break;
			}

			IControlView * Level1DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
			if(Level1DDBoxTIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlData == nil");
				break;
			}
			IControlView * Level2DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxTIWidgetID);
			if(Level2DDBoxTIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
				break;
			}

			//By Amarjit patil

			IControlView * Level3DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
			if(Level3DDBoxTIWidgetControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
				break;
			}

			VectorAttributeInfoPtr AttrInfoVectPtr11 = NULL;
			if(CurrentClassID == -1)
			{				
				VectorClassInfoPtr vectClassInfoValuePtr11 = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
				if(vectClassInfoValuePtr11==nil)
				{
					ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::ClassificationTree_getRoot's vectClassInfoValuePtr11 is nil");
					return;
				}
					
				VectorClassInfoValue::iterator it;
				it=vectClassInfoValuePtr11->begin();
				CurrentClassID = it->getClass_id();

				if(vectClassInfoValuePtr11)
					delete vectClassInfoValuePtr11;

			}

			AttrInfoVectPtr11 = ptrIAppFramework->/*AttributeCache_getItemAttributesForClass*/StructureCache_getItemAttributesForClassAndParents(CurrentClassID,SelectedlanguageID);
			if(AttrInfoVectPtr11== NULL)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::StructureCache_getItemAttributesForClassAndParents's AttrInfoVectPtr11 is nil");
				return;
			}

			VectorAttributeInfoValue::iterator it11;

			for(it11=AttrInfoVectPtr11->begin();it11!=AttrInfoVectPtr11->end();it11++)
			{
				//if(it11->getLanguageId() != SelectedlanguageID)
				//	continue;
				
				AttributeListData ElementData;
				PMString codeStr("i");
				PMString displayName("Item : ");
				codeStr.AppendNumber(PMReal(it11->getAttributeId()));
				ElementData.code= codeStr;
				ElementData.id = it11->getAttributeId();
				ElementData.index = 4;
				ElementData.isImageFlag = kFalse;
				displayName.Append(it11->getDisplayName());
				displayName.SetTranslatable(kFalse);
				ElementData.name = displayName;
				ElementData.tableFlag = 0;
				ElementData.typeId = -1/*it->getElement_type_id()*/;

				NewAttributeList11.push_back(ElementData);
				NewAttributeList22.push_back(ElementData);
				NewAttributeList33.push_back(ElementData);
				NewAttributeList44.push_back(ElementData);
				level1DropListData->AddString(ElementData.name);
				level2DropListData->AddString(ElementData.name);
				level3DropListData->AddString(ElementData.name);
				level1SIDropListData->AddString(ElementData.name);
				level2SIDropListData->AddString(ElementData.name);
				level3SIDropListData->AddString(ElementData.name);
				level1TIDropListData->AddString(ElementData.name);
				level2TIDropListData->AddString(ElementData.name);
				level3TIDropListData->AddString(ElementData.name);
			}



			//**** Added for ITEM GROUP 5/04/2010
			VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId(SelectedlanguageID);
			if(eleValObj)
			{
				VectorElementInfoValue::iterator itrr;
				for(itrr=eleValObj->begin();itrr!=eleValObj->end();itrr++)
				{
					//if(itrr->getLanguageId() != SelectedlanguageID)
					//	continue;

					AttributeListData ElementData;
					PMString codeStr("g");
					PMString displayName("ItemGroup : ");
					codeStr.AppendNumber(PMReal(itrr->getElementId()));
					ElementData.code= codeStr;
					ElementData.id = itrr->getElementId();
					ElementData.index = 3;
					ElementData.isImageFlag = kFalse;
					//ElementData.name = itrr->getDisplayName();	
					displayName.Append(itrr->getDisplayName());
					displayName.SetTranslatable(kFalse);
					ElementData.name = displayName;

					ElementData.tableFlag = 0;
					ElementData.typeId = -1;

					NewAttributeList11.push_back(ElementData);
					NewAttributeList22.push_back(ElementData);
					NewAttributeList33.push_back(ElementData);
					NewAttributeList44.push_back(ElementData);

					level1DropListData->AddString(ElementData.name);
					level2DropListData->AddString(ElementData.name);
					level3DropListData->AddString(ElementData.name);
					level1SIDropListData->AddString(ElementData.name);
					level2SIDropListData->AddString(ElementData.name);
					level3SIDropListData->AddString(ElementData.name);
					level1TIDropListData->AddString(ElementData.name);
					level2TIDropListData->AddString(ElementData.name);
					level3TIDropListData->AddString(ElementData.name);
				}
					AttributeListData ElementData1;
					ElementData1.code= "";
					ElementData1.id =-121 ;
					ElementData1.index = 3;
					ElementData1.isImageFlag = kFalse;
					ElementData1.name = "List/Table Name";		
					ElementData1.name.SetTranslatable(kFalse);
					ElementData1.tableFlag = 0;
					ElementData1.typeId = -1;

					NewAttributeList11.push_back(ElementData1);
					NewAttributeList22.push_back(ElementData1);
					NewAttributeList33.push_back(ElementData1);
					NewAttributeList44.push_back(ElementData1);

					level1DropListData->AddString(ElementData1.name);
					level2DropListData->AddString(ElementData1.name);
					level3DropListData->AddString(ElementData1.name);
					level1SIDropListData->AddString(ElementData1.name);
					level2SIDropListData->AddString(ElementData1.name);
					level3SIDropListData->AddString(ElementData1.name);
					level1TIDropListData->AddString(ElementData1.name);
					level2TIDropListData->AddString(ElementData1.name);
					level3TIDropListData->AddString(ElementData1.name);
					
					/*level1DropListData->AddString(ElementData1.name);
					level2DropListData->AddString(ElementData1.name);
					level1SIDropListData->AddString(ElementData1.name);
					level2SIDropListData->AddString(ElementData1.name);
					level1TIDropListData->AddString(ElementData1.name);
					level2TIDropListData->AddString(ElementData1.name);*/
			}			
	

			//*********Added for Event or Publication ***************//  
			//VectorElementInfoPtr elementobj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(SelectedlanguageID);
			//if(elementobj)
			{
				PMString temp4("Event");
				PMString temp5;
				VectorElementInfoValue::iterator iter1;
				//for(iter1=elementobj->begin();iter1!=elementobj->end();iter1++)
				{
					//if(iter1 == elementobj->begin())
					{
						//if(iter1->getLanguageId()!=SelectedlanguageID)
						//	continue;
						temp5 = temp4 + " ";
						temp5.Append("Name");

						AttributeListData EleData;
						PMString codeStr("l1");						
						EleData.code= codeStr;
						EleData.id = 1;
						EleData.index = 5;
						EleData.isImageFlag = kFalse;
						EleData.name = temp5;
						EleData.name.SetTranslatable(kFalse);
						EleData.tableFlag = 0;
						EleData.typeId = -1;
						EleData.catLevel = -1;

						NewAttributeList11.push_back(EleData);
						NewAttributeList22.push_back(EleData);
						NewAttributeList33.push_back(EleData);
						NewAttributeList44.push_back(EleData);

						level1DropListData->AddString(EleData.name);
						level2DropListData->AddString(EleData.name);
						level3DropListData->AddString(EleData.name);
						level1SIDropListData->AddString(EleData.name);
						level2SIDropListData->AddString(EleData.name);
						level3SIDropListData->AddString(EleData.name);
						level1TIDropListData->AddString(EleData.name);
						level2TIDropListData->AddString(EleData.name);
						level3TIDropListData->AddString(EleData.name);
					}
					//if(iter1->getLanguageId()!=SelectedlanguageID)
					//	continue;
					/*temp5 = temp4 + " ";
					temp5.Append(iter1->getDisplayName());

					AttributeListData EleData;
					EleData.code= "";
					EleData.id = iter1->getElementId();
					EleData.index = 5;
					EleData.isImageFlag = kFalse;
					EleData.name = temp5;
					EleData.name.SetTranslatable(kFalse);
					EleData.tableFlag = 0;
					EleData.typeId = -1;
					EleData.catLevel = -1;

					NewAttributeList11.push_back(EleData);
					NewAttributeList22.push_back(EleData);
					NewAttributeList33.push_back(EleData);
					NewAttributeList44.push_back(EleData);

					level1DropListData->AddString(EleData.name);
					level2DropListData->AddString(EleData.name);
					level3DropListData->AddString(EleData.name);
					level1SIDropListData->AddString(EleData.name);
					level2SIDropListData->AddString(EleData.name);
					level3SIDropListData->AddString(EleData.name);
					level1TIDropListData->AddString(EleData.name);
					level2TIDropListData->AddString(EleData.name);
					level3TIDropListData->AddString(EleData.name);*/
				}
			}		


			PMString temp1;
			PMString temp2;
			PMString Level;
			PMString temp3("Section");

			VectorElementInfoValue::iterator iter2;
			int32 NumberOfSectionLevels = 5; //ptrIAppFramework->ProjectCache_getLevelForSection();

			
			eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(SelectedlanguageID);
			if(eleValObj==nil)
			{
				ptrIAppFramework->LogError("AP46_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::StructureCache_getSectionCopyAttributesByLanguageId's eleValObj is nil");
				break;
			}

			for(int32 levelNo = 1; levelNo <= NumberOfSectionLevels; levelNo++)
			{
				temp1.Clear();
				temp2.Clear();							

				temp1.Append(", Level ");
				temp1.AppendNumber(levelNo);
				
				for(iter2=eleValObj->begin();iter2!=eleValObj->end();iter2++)
				{
					if(iter2 == eleValObj->begin())
					{
						temp2 = temp3 + " ";
						temp2.Append("Name");
						temp2.Append(temp1);

						AttributeListData EleData2;
						PMString codeStr("l");
						codeStr.AppendNumber(levelNo + 1);
						EleData2.code= codeStr;
						EleData2.id = levelNo + 1; // 2, 3, 4, 5, 6
						EleData2.index = 5;
						EleData2.isImageFlag = kFalse;
						EleData2.name = temp2;
						EleData2.name.SetTranslatable(kFalse);
						EleData2.tableFlag = 0;
						EleData2.typeId = -1;

						int32 localCatLevel = levelNo;
						++localCatLevel;
						localCatLevel = 0 - localCatLevel;
						EleData2.catLevel = localCatLevel;

						NewAttributeList11.push_back(EleData2);
						NewAttributeList22.push_back(EleData2);
						NewAttributeList33.push_back(EleData2);
						NewAttributeList44.push_back(EleData2);

						level1DropListData->AddString(EleData2.name);
						level2DropListData->AddString(EleData2.name);
						level3DropListData->AddString(EleData2.name);
						level1SIDropListData->AddString(EleData2.name);
						level2SIDropListData->AddString(EleData2.name);
						level3SIDropListData->AddString(EleData2.name);
						level1TIDropListData->AddString(EleData2.name);
						level2TIDropListData->AddString(EleData2.name);
						level3TIDropListData->AddString(EleData2.name);

					}



					if(1/*iter2->getLanguageId()== SelectedlanguageID*/)
					{
						if(iter2->getElementId() == 0)
							continue;

						temp2 = temp3 + " ";
						temp2.Append(iter2->getDisplayName());
						temp2.Append(temp1);

						AttributeListData EleData2;
						PMString codeStr("s");
						codeStr.AppendNumber(PMReal(iter2->getElementId()));
						EleData2.code= codeStr;
						EleData2.id = iter2->getElementId();
						EleData2.index = 5;
						EleData2.isImageFlag = kFalse;
						EleData2.name = temp2;
						EleData2.name.SetTranslatable(kFalse);
						EleData2.tableFlag = 0;
						EleData2.typeId = -1;

						int32 localCatLevel = levelNo;
						++localCatLevel;
						localCatLevel = 0 - localCatLevel;
						EleData2.catLevel = localCatLevel;

						NewAttributeList11.push_back(EleData2);
						NewAttributeList22.push_back(EleData2);
						NewAttributeList33.push_back(EleData2);
						NewAttributeList44.push_back(EleData2);

						level1DropListData->AddString(EleData2.name);
						level2DropListData->AddString(EleData2.name);
						level3DropListData->AddString(EleData2.name);
						level1SIDropListData->AddString(EleData2.name);
						level2SIDropListData->AddString(EleData2.name);
						level3SIDropListData->AddString(EleData2.name);
						level1TIDropListData->AddString(EleData2.name);
						level2TIDropListData->AddString(EleData2.name);
						level3TIDropListData->AddString(EleData2.name);
					}
				}
			}	


			/*temp1.Clear();
			temp2.Clear();
			temp3.Clear();
			Level.Clear();
			temp3.Append("Category");
			
			
			int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();
			VectorElementInfoPtr eleValObj1 = ptrIAppFramework->getCategoryCopyAttributesForPrint(SelectedlanguageID);	 
			if(eleValObj1)
			{
				VectorElementInfoValue::iterator itr4;
				for(int32 levelNo = 1; levelNo <= levels; levelNo++)
				{
					temp1.Clear();
					temp2.Clear();							

					temp1.Append(", Level ");
					temp1.AppendNumber(levelNo);

					for(itr4=eleValObj1->begin();itr4!=eleValObj1->end();itr4++)
					{
						if(itr4->getLanguageId()!=SelectedlanguageID)
							continue;

						temp2 = temp3 + " ";
						temp2.Append(itr4->getDisplayName());
						temp2.Append(temp1);

						AttributeListData EleData4;
						EleData4.code= "";
						EleData4.id = itr4->getElementId();
						EleData4.index = 5;
						EleData4.isImageFlag = kFalse;
						EleData4.name = temp2;
						EleData4.tableFlag = 0;
						EleData4.typeId = -1;
						
						int32 localCatLevel = levelNo;					
						EleData4.catLevel = ++localCatLevel;
						

						NewAttributeList11.push_back(EleData4);
						NewAttributeList22.push_back(EleData4);
						NewAttributeList33.push_back(EleData4);
						NewAttributeList44.push_back(EleData4);

						level1DropListData->AddString(EleData4.name);
						level2DropListData->AddString(EleData4.name);
						level3DropListData->AddString(EleData4.name);
						level1SIDropListData->AddString(EleData4.name);
						level2SIDropListData->AddString(EleData4.name);
						level3SIDropListData->AddString(EleData4.name);
						level1TIDropListData->AddString(EleData4.name);
						level2TIDropListData->AddString(EleData4.name);
						level3TIDropListData->AddString(EleData4.name);

					}
				}
			}*/

			level1DropListController->Select(0);
			level2DropListController->Select(0);
			level3DropListController->Select(0);
			level1SIDropListController->Select(0);
			level2SIDropListController->Select(0);
			level3SIDropListController->Select(0);
			level1TIDropListController->Select(0);
			level2TIDropListController->Select(0);
			level3TIDropListController->Select(0);


			if(selectedRadioWidgetID == kItemNumberRadioButtonWidgetID)
			{
				cAtrrModelObj = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(1, SelectedlanguageID);
			}
			else if(selectedRadioWidgetID == kCategoryNameRadioButtonWidgetID)
			{
				cAtrrModelObj = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(2,SelectedlanguageID );
			}
			else if(selectedRadioWidgetID == kBrandNameRadioButtonWidgetID)
			{
				cAtrrModelObj = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(3, SelectedlanguageID);
			}
			else if(selectedRadioWidgetID == kManufacturerNameRadioButtonWidgetID)
			{
				cAtrrModelObj = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(4, SelectedlanguageID);
			}
			else if(selectedRadioWidgetID == kSupplierNameRadioButtonWidgetID)
			{
				cAtrrModelObj = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(5, SelectedlanguageID);
			}
			else if(selectedRadioWidgetID == kCustomRadioButtonWidgetID)
			{
				customRadioFlag = kTrue;
			}
			/*int32 asd = cAtrrModelObj.getAttributeId();
			int32 i;
			
			for(i = 0;i<NewAttributeList11.size();i++)
			{
				if(asd == NewAttributeList11[i].id)
					break;
			}*/

			IControlView* level1DDControlView = panelControlData->FindWidget(kCTILevel1DDBoxWidgetID);
			if (level1DDControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::level1DDControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel1DDListController(level1DDControlView, UseDefaultIID());
			if (plevel1DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel1DDListController invalid");
				break;
			}
			//if(i == NewAttributeList11.size() || customRadioFlag == kTrue){
				plevel1DDListController->Select(0);

				Level2DDBoxWidgetControlData->Disable();
				Level1DDBoxSIWidgetControlData->Enable();
				Level2DDBoxSIWidgetControlData->Disable();
				Level1DDBoxTIWidgetControlData->Enable();
				Level2DDBoxTIWidgetControlData->Disable();

			/*}
			else{
				plevel1DDListController->Select(i+1);
				
				Level2DDBoxWidgetControlData->Enable();
				Level1DDBoxSIWidgetControlData->Enable();
			}*/

		}
		

		if((theSelectedWidget == kCTICancel1IconWidgetID/*kCTINewCancelButtonWidgetID*/) && (theChange == kTrueStateMessage))
		{
			CDialogObserver::CloseDialog();
			break;
		}

		if(theSelectedWidget == kCTIStartIconWidgetID/*kCTIStartButtonWidgetID*/ && theChange==kTrueStateMessage)
		{
			ptrIAppFramework->LogDebug("AP7_CatalogIndex::Started Creating Index");
			InterfacePtr<IPanelControlData> pPanelControlData(this, UseDefaultIID());
			if (pPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::pPanelControlData is nil");
				break;
			}
			

			IControlView* plevel1ControlView =pPanelControlData->FindWidget(kCTILevel1DDBoxWidgetID);
			if (plevel1ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel1ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel1DDListController(plevel1ControlView, UseDefaultIID());
			if (plevel1DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel1DDListController invalid");
				break;
			}
			int32 index11 =0; int32 index22 = 0; int32 index33 = 0;
			index11 = plevel1DDListController->GetSelected();
			if(index11 == 0)
			{
				CA("Caution .... you are going wrong ");
				break;
			}
			IControlView* plevel2ControlView =pPanelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
			if (plevel2ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel2DDListController(plevel2ControlView, UseDefaultIID());
			if (plevel2DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
				break;
			}

			index22 = plevel2DDListController->GetSelected();

			//By Amarjit

			IControlView* plevel3ControlView =pPanelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
			if (plevel3ControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
				break;
			}
		
			InterfacePtr<IDropDownListController> plevel3DDListController(plevel3ControlView, UseDefaultIID());
			if (plevel3DDListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
				break;
			}

			index33 = plevel3DDListController->GetSelected();

			
			
			AcquireWaitCursor awc;
			awc.Animate();
			int32 indexSILevel1 = 0,indexSILevel2 = 0,indexSILevel3 = 0,indexTILevel1 = 0,indexTILevel2 = 0,indexTILevel3 = 0;
			//if(index11 > 0)
		//	{
				IControlView* SIlevel1DDControlView =pPanelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
				if (plevel2ControlView == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
					break;
				}
			
				InterfacePtr<IDropDownListController> SIlevel1DDListController(SIlevel1DDControlView, UseDefaultIID());
				if (plevel2DDListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
					break;
				}

				indexSILevel1=SIlevel1DDListController->GetSelected();
				if(indexSILevel1 > 0)
				{
					IControlView* SIlevel2DDControlView =pPanelControlData->FindWidget(kLevel2DDBoxSIWidgetID);
					if (plevel2ControlView == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SIlevel2DDControlView invalid");
						break;
					}
				
					InterfacePtr<IDropDownListController>SIlevel2DDListController(SIlevel2DDControlView, UseDefaultIID());
					if (SIlevel2DDListController == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SIlevel2DDListController invalid");
						break;
					}

					indexSILevel2 = SIlevel2DDListController->GetSelected();

					//By amarjit

					IControlView* SIlevel3DDControlView =pPanelControlData->FindWidget(kLevel3DDBoxSIWidgetID);
					if (SIlevel3DDControlView == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SIlevel2DDControlView invalid");
						break;
					}
				
					InterfacePtr<IDropDownListController>SIlevel3DDListController(SIlevel3DDControlView, UseDefaultIID());
					if (SIlevel3DDListController == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::SIlevel2DDListController invalid");
						break;
					}

					indexSILevel3 = SIlevel3DDListController->GetSelected();

				}

				{
					IControlView* TIlevel1DDControlView =pPanelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
					if (plevel2ControlView == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2ControlView invalid");
						break;
					}
				
					InterfacePtr<IDropDownListController> TIlevel1DDListController(TIlevel1DDControlView, UseDefaultIID());
					if (plevel2DDListController == nil)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::plevel2DDListController invalid");
						break;
					}

					indexTILevel1 = TIlevel1DDListController->GetSelected();
					if(indexTILevel1 > 0){
						IControlView* TIlevel2DDControlView =pPanelControlData->FindWidget(kLevel2DDBoxTIWidgetID);
						if (TIlevel2DDControlView == nil)
						{
							ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::TIlevel2DDControlView invalid");
							break;
						}
					
						InterfacePtr<IDropDownListController> TIlevel2DDListController(TIlevel2DDControlView, UseDefaultIID());
						if (TIlevel2DDListController == nil)
						{
							ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::TIlevel2DDListController invalid");
							break;
						}

						indexTILevel2 = TIlevel2DDListController->GetSelected();

						//By Amarjt 
						IControlView* TIlevel3DDControlView =pPanelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
						if (TIlevel3DDControlView == nil)
						{
							ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::TIlevel2DDControlView invalid");
							break;
						}
					
						InterfacePtr<IDropDownListController> TIlevel3DDListController(TIlevel3DDControlView, UseDefaultIID());
						if (TIlevel3DDListController == nil)
						{
							ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::TIlevel2DDListController invalid");
							break;
						}

						indexTILevel3 = TIlevel3DDListController->GetSelected();
					}
				}

		//	}

			PMString PrimaryIndex("");
			PMString SecondaryIndex("");
			PMString TertioryIndex("");

			//index11 ; index22 ; index33 ;

			if(index11 ==0)
				PrimaryIndex.Append(",");
			else if (index11 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[index11-1];
				PrimaryIndex.Append(PageRefAttributeList11.code);
				PrimaryIndex.Append(",");
			}
			if(index22 ==0)
				PrimaryIndex.Append(",");
			else if (index22 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[index22-1];
				PrimaryIndex.Append(PageRefAttributeList11.code);
				PrimaryIndex.Append(",");
			}
			if(index33 ==0)
				PrimaryIndex.Append("");
			else if (index33 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[index33-1];
				PrimaryIndex.Append(PageRefAttributeList11.code);
				
			}

			// indexSILevel1 , indexSILevel2 , indexSILevel3 , 

			if(indexSILevel1 ==0)
				SecondaryIndex.Append(",");
			else if (indexSILevel1 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexSILevel1-1];
				SecondaryIndex.Append(PageRefAttributeList11.code);
				SecondaryIndex.Append(",");
			}
			if(indexSILevel2 ==0)
				SecondaryIndex.Append(",");
			else if (indexSILevel2 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexSILevel2-1];
				SecondaryIndex.Append(PageRefAttributeList11.code);
				SecondaryIndex.Append(",");
			}
			if(indexSILevel3 ==0)
				SecondaryIndex.Append("");
			else if (indexSILevel3 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexSILevel3-1];
				SecondaryIndex.Append(PageRefAttributeList11.code);				
			}

			// indexTILevel1 ,indexTILevel2 ,indexTILevel3 ;

			if(indexTILevel1 ==0)
				TertioryIndex.Append(",");
			else if (indexTILevel1 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexTILevel1-1];
				TertioryIndex.Append(PageRefAttributeList11.code);
				TertioryIndex.Append(",");
			}
			if(indexTILevel2 ==0)
				TertioryIndex.Append(",");
			else if (indexTILevel2 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexTILevel2-1];
				TertioryIndex.Append(PageRefAttributeList11.code);
				TertioryIndex.Append(",");
			}
			if(indexTILevel3 ==0)
				TertioryIndex.Append("");
			else if (indexTILevel3 > 0)
			{
				AttributeListData PageRefAttributeList11;								
				PageRefAttributeList11 = NewAttributeList11[indexTILevel3-1];
				TertioryIndex.Append(PageRefAttributeList11.code);				
			}

			ptrIAppFramework->LogDebug("Primary Index :" + PrimaryIndex);
			ptrIAppFramework->LogDebug("Secondary Index :" + SecondaryIndex);
			ptrIAppFramework->LogDebug("Tertiory Index :" + TertioryIndex);

			InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::There is not book manager!");
				return;
			}
		
			int32 bookCount = bookManager->GetBookCount();
			if (bookCount <= 0)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::There is no book open. You must first open a book before running this snippet.");
				return;
			}



			PMString  ActiveBookName;
			bool16 result = Utils<IBookUtils>()->GetActiveBookName(ActiveBookName);


			vector<IDFile> BookFileList;
			BookFileList.clear();
			IBook* book = NULL;

			for (int32 i = 0 ; i < bookCount ; i++)
			{
				book = bookManager->GetNthBook(i);
				if (book == nil)
				{	
					continue;
				}
				PMString BookTitalName = book->GetBookTitleName();
		
				if(BookTitalName == ActiveBookName)
				{	
					InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
					if (bookContentMgr == nil) 
					{
						CA("This book doesn't have a book content manager!  Something is wrong.");
						break;
					}
					BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
					if(BookContentinfovector== nil)
						break;
		
					if(BookContentinfovector->size()<=0)
					{
						CA(" No document found in Book! ");
						return;
					}
		
					BookContentDocInfoVector::iterator it1;
					for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
					{	
						IDFile  fileName= it1->DocFile ;
						BookFileList.push_back(fileName);
					}

					BookContentinfovector->clear();
					delete BookContentinfovector;
				}
			}
            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 1)");
			


			if(BookFileList.size() <=0)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::BookFileList.size <= 0");
				return;
			}

            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 2)");
			SnpIndexTopics createIndexObj;

			SDKLayoutHelper sdklhelp;
			PMString IndexFileName("");

			int lastpos = 0;
			for (int i = 0 ; i< ActiveBookName.CharCount();i++)
				if ((ActiveBookName[i] == '.') )
					lastpos = i;
			// At this point lastpos should point to the last delimeter, knock off the rest of the string.
			ActiveBookName.Truncate(ActiveBookName.CharCount()-lastpos);

			IndexFileName.Append(ActiveBookName);
			IndexFileName.Append("_Index.indd");

			bool16 IsIndexFileExist= kFalse;
			for(int32 i=0; i<BookFileList.size(); i++ )
			{
				IDFile flName= BookFileList[i];
				PMString BookFileName = flName.GetFileName();

				if(BookFileName == IndexFileName)
				{
					IsIndexFileExist = kTrue;
					break;
				}
			}

            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 3)");
			IDFile flName= book->GetBookFileSpec();
			IDFile IndexFile;
			SDKUtilities sdkUtilityObj;
			PMString FileName("");
			sdkUtilityObj.GetPathName(flName,FileName);

			sdkUtilityObj.RemoveLastElement(FileName);
			FileName.Append("\\");
			FileName.Append(IndexFileName);

            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 4)");
			#ifdef MACINTOSH		
			
				SDKUtilities::convertToMacPath(FileName);
			
			#endif
		
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::FileName = " + FileName);
			IndexFile = sdkUtilityObj.PMStringToSysFile(&FileName);
            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 5)");
			if(!IsIndexFileExist)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::!IsIndexFileExist = ");
				bool16  IsFileExists = FileUtils::DoesFileExist (IndexFile);
				if(IsFileExists)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IsFileExists");
					InterfacePtr<ICoreFilename> corefn((ICoreFilename*)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));
					if (corefn)
					{
						const IDFile* tempIndexFile = &IndexFile;
						corefn->Initialize(tempIndexFile);
						bool16 isFileOPEN = corefn->IsFileOpen();
						if(isFileOPEN)
						{
							UIDRef CurrIndexDocRef =sdklhelp.OpenDocument(IndexFile);
							sdklhelp.SaveDocumentAs(CurrIndexDocRef, IndexFile);
							isIndexRemovedFromBookAndCreatedForAnotherDocnew = kTrue;
						}
					}
					//ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::IsFileExists 2");
				}
				do
				{
					InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
					if (bookContentMgr == nil)
					{
						ptrIAppFramework->LogDebug("This book doesn't have a book content manager!  Something is wrong.");
						break;
					}


					if(!isIndexRemovedFromBookAndCreatedForAnotherDocnew)
					{
						ErrorCode result1 = kFailure;
						UIDRef documentUIDRef = sdklhelp.CreateDocument();
						if (documentUIDRef == UIDRef::gNull) {
							ptrIAppFramework->LogDebug("documentUIDRef == UIDRef::gNull");
							return;
						}

						// Open a view onto the document.
			
						result1 = sdklhelp.OpenLayoutWindow(documentUIDRef);
						sdklhelp.SaveDocumentAs(documentUIDRef, IndexFile);
					}
					
					UIDRef bookRef = ::GetUIDRef(book);
					ICommandSequence *piCmdSequence =CmdUtils::BeginCommandSequence();
					piCmdSequence->SetName("Add Document to Book");

					InterfacePtr<ICommand> piCreateContentCmd(CmdUtils::CreateCommand(kConstructContentCmdBoss));
					if (!piCreateContentCmd)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::!piCreateContentCmd");
						break;
					}

					K2Vector<IDFile> docList;
					docList.push_back(IndexFile);

					InterfacePtr<IBookContentCmdData>
					piBookContentCmdData(piCreateContentCmd, IID_IBOOKCONTENTCMDDATA);
					piBookContentCmdData->SetTargetBook(flName);
					piBookContentCmdData->SetContentFile(docList);

                    //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 6)");
					ErrorCode result;
					if ((result = CmdUtils::ProcessCommand(piCreateContentCmd)) !=	kSuccess)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: piCreateContentCmd !=	kSuccess");
						break;
					}

			// 2. Process kAddDocToBookCmdBoss to add the book content to the book
					const UIDList *list = piCreateContentCmd->GetItemList();
		
					if (!(list && list->Length() > 0))
					{	
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: list && list->Length() > 0)");
						break;
					}

					InterfacePtr<ICommand>
					piAddDocCmd(CmdUtils::CreateCommand(kAddDocToBookCmdBoss));
					if (!piAddDocCmd)
					{
						ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: piAddDocCmd)");
						break;
					}
                    //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 7)");

			// The position where the doc is to be inserted within the book
			//  Valid values are -1 (beginning of book) to
					int32 destPos = bookContentMgr->GetContentCount()-1;
					UIDList* nonConstList = const_cast<UIDList*>(list);
					InterfacePtr<IBookContentCmdData> piAddDocCmdData(piAddDocCmd,IID_IBOOKCONTENTCMDDATA);
					piAddDocCmdData->SetTargetBook(flName);
					piAddDocCmdData->SetContentFile(docList);
					piAddDocCmdData->SetContentList(nonConstList);
					piAddDocCmdData->SetDestPosition(destPos);

					result = CmdUtils::ProcessCommand(piAddDocCmd);

			// 3. Process kSetRepaginationCmdBoss to repaginate the book
			//after adding a new doc
			//  This step is optional; do it only if you want to renumber
			//all pages in other documents
			//  whose position in the book changes. This will modify and
			//save those docs.
					const UIDList *addList = piAddDocCmd->GetItemList();
					if (!(result == kSuccess && addList->Length() > 0))
						break;

                    //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 8)");
					InterfacePtr<ICommand>
					piRepaginateCmd(CmdUtils::CreateCommand(kSetRepaginationCmdBoss));
					if (!piRepaginateCmd)
						break;

                    //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 9)");

			// Get repagination options from the book and apply the same to
			//the command
					InterfacePtr<IBookPaginateOptions> piPaginateOptions(book,IID_IBOOKPAGINATEOPTIONS);
					IBookPaginateOptions::Options options =	piPaginateOptions->GetPaginateOptions();
					bool16 bInsert = piPaginateOptions->GetInsertBlankPage();
					bool16 bAutoRepaginate =piPaginateOptions->GetAutoRepaginateFlag();
					bool16 bForceToRepaginate = kTrue;
					InterfacePtr<IBookPaginateOptions>piPaginateCmdData(piRepaginateCmd, IID_IBOOKPAGINATEOPTIONS);
					piPaginateCmdData->SetPaginateOptions(options);
					piPaginateCmdData->SetInsertBlankPage(bInsert);
					piPaginateCmdData->SetAutoRepaginateFlag(bAutoRepaginate);
					piPaginateCmdData->SetCurrentBookUIDRef(bookRef);

					InterfacePtr<IIntData> piPosition(piRepaginateCmd,IID_IINTDATA);
					piPosition->Set(destPos);

					InterfacePtr<IBoolData> piForceRepaginate(piRepaginateCmd,IID_IBOOLDATA);
					piForceRepaginate->Set(bForceToRepaginate);

					result = CmdUtils::ProcessCommand(piRepaginateCmd);
					CmdUtils::EndCommandSequence(piCmdSequence);
                    //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 10)");

				} while (false);
			}
			else
			{
				UIDRef CurrIndexDocRef =sdklhelp.OpenDocument(IndexFile);
				ErrorCode err = sdklhelp.OpenLayoutWindow(CurrIndexDocRef);
			}
            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 11)");
			InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
			if (bookContentMgr == nil)
			{
				//CA("This book doesn't have a book content manager!  Something is wrong.");
				return;
			}
            //ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 12)");
			BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
			if(BookContentinfovector== nil)
				return;

			if(BookContentinfovector->size()<=0)
			{
				return;
			}
			vector<IDFile> BookFileListNew;
			BookFileListNew.clear();
			BookContentDocInfoVector::iterator it2;
			for(it2 = BookContentinfovector->begin(); it2 != BookContentinfovector->end(); it2++)
			{
				IDFile  fileName= it2->DocFile ;
				BookFileListNew.push_back(fileName);
			}

			BookContentinfovector->clear();
			delete BookContentinfovector;


			//ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update:: step 13)");
			for(int32 i=0; i<BookFileListNew.size(); i++ )
			{
				IDFile flName = BookFileListNew[i];
				if(BookFileListNew[i] != IndexFile)
				{

                    ptrIAppFramework->LogDebug("AP7_CatalogIndex:: For DOC ="+flName.GetFileName());
					if(i > 0)
						CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
					
					UIDRef CurrDocRef = sdklhelp.OpenDocument(flName);
					ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);	

					IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();  
					if(fntDoc==nil)
						continue;
					
					// Jamie's changes started...................................
					IDataBase* docDatabase = ::GetDataBase((IPMUnknown*) fntDoc);
					const IDFile* idFile = docDatabase->GetSysFile();

					IIDXMLElement* rootElem = Utils<IXMLUtils>()->QueryRootElement(docDatabase);

					int32 count1 = rootElem->GetChildCount();
					int32 badTagsCnt = 0;

					set<PMString> uniqueResultIds;

					do
					{
						PMString UnusedFrameRemovalKey("Unused Frame Removal");
						UnusedFrameRemovalKey.SetTranslatable(kFalse);
						PMString UsusedFrameRemovalInProgressKey("Unused frame removal is in progress ...");
						UsusedFrameRemovalInProgressKey.SetTranslatable(kFalse);

						RangeProgressBar progressBar(UnusedFrameRemovalKey, 0, count1, kTrue);
						progressBar.SetTaskText(UsusedFrameRemovalInProgressKey);
				
						for(int32 i = count1 - 1; i >= 0; --i)
						{
							XMLReference xmlRef = rootElem->GetNthChild(i);
							IIDXMLElement* childElem = xmlRef.Instantiate();

							const XMLContentReference& contentRef = childElem->GetContentReference();
							const UIDRef & uidref = contentRef.GetUIDRef();
							if(uidref  == UIDRef::gNull)
							{
								ErrorCode status = Utils<IXMLElementCommands>()->DeleteElement(xmlRef,kTrue);
								badTagsCnt++;
							}
							else
							{
								AddKeys(childElem, uniqueResultIds);  // this will have to move into the new logic
							}

							childElem->Release();

							progressBar.SetPosition(count1 - i);
						}
					} while (kFalse);

					do
					{
						vector<IndexReference> indexReferences;

						PMString indexingDoc("Building Index");
						PMString taskText("");
						indexingDoc.SetTranslatable(kFalse);

						int32 taskCount = rootElem->GetChildCount() + 2;

						RangeProgressBar progressBar2(indexingDoc, 0, taskCount, kTrue);
						taskText.Append("Retrieving index terms from server");
						taskText.SetTranslatable(kFalse);
						progressBar2.SetTaskText(taskText);

						ptrIAppFramework->getIndexTerms(uniqueResultIds, indexReferences, PrimaryIndex, SecondaryIndex, TertioryIndex);
						progressBar2.SetPosition(1);

						TotalTagsInDocument = count1;
						TotalUnusedTagsRemoved = badTagsCnt;

						taskText.Clear();
						taskText.Append("Removing old index terms");
						taskText.SetTranslatable(kFalse);
						progressBar2.SetTaskText(taskText);
						DeleteAllTopics(fntDoc);
						progressBar2.SetPosition(2);

						taskText.Clear();
						taskText.Append("Adding new terms to index");
						taskText.SetTranslatable(kFalse);
						progressBar2.SetTaskText(taskText);

						UIDRef documentTopicListRef;
						InterfacePtr <IIndexTopicListList> pIndexTopicListList(fntDoc->GetDocWorkSpace(), UseDefaultIID());

						int32 topicListCount = pIndexTopicListList->GetNumTopicLists();
						if (topicListCount == 0)
						{
							InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
							InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
							piCreateTopicListCmdData->SetTargetItem(fntDoc->GetDocWorkSpace());
							piCreateTopicListCmdData->SetDoNotifyFlag(kTrue);
							if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
								break;
						}

						// Indesign only supports upto one topic list currently
						UID uid = pIndexTopicListList->GetNthTopicList(0);
						documentTopicListRef = UIDRef(docDatabase, uid);

						set<WideString> childIds;
						for(int32 i=0; i < rootElem->GetChildCount(); i++)
						{
							XMLReference xmlRef = rootElem->GetNthChild(i);
							IIDXMLElement* childElem = xmlRef.Instantiate();
							TagDocument(childElem, fntDoc, indexReferences, documentTopicListRef, false, childIds, false);
							childElem->Release();
							progressBar2.SetPosition(i + 3);
						}


					} while (kFalse);
					
					rootElem->Release();

					sdklhelp.SaveDocumentAs(CurrDocRef,flName);
					sdklhelp.CloseDocument(CurrDocRef, kTrue);
                    CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
				}
			}
			isIndexRemovedFromBookAndCreatedForAnotherDocnew = kFalse;

			CDialogObserver::CloseDialog();
		}
		
		if(theSelectedWidget == kCTISelectlanguageDropDownWidgetID)
		{
			InterfacePtr<IPanelControlData> pPanelControlData(this, UseDefaultIID());
			IControlView *	selectlanguageDropDownView = pPanelControlData->FindWidget(kCTISelectlanguageDropDownWidgetID);
			if(!selectlanguageDropDownView)
			{
				break;
			}

			InterfacePtr<IStringListControlData> selectlanguageDropListData(selectlanguageDropDownView,UseDefaultIID());	
			if (selectlanguageDropListData==nil)
			{
				break;
			}

			InterfacePtr<IDropDownListController> selectlanguageDropListController(selectlanguageDropDownView, UseDefaultIID());
			if (selectlanguageDropListController==nil)
			{
				break;
			}
			int32 index = selectlanguageDropListController->GetSelected();
			gStrLangName = selectlanguageDropListData->GetString(index);
	
			VectorLanguageModelPtr VectorLangNamePtr = ptrIAppFramework->StructureCache_getAllLanguages();
			if(VectorLangNamePtr == NULL)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitilizeDialogFields::VectorLangNamePtr == nil");
				return;
			}
			
			VectorLanguageModel::iterator itr;
			for(itr = VectorLangNamePtr->begin(); itr != VectorLangNamePtr->end(); itr++/*,listIndx++*/)
			{
				if(gStrLangName.Compare(kTrue,itr->getLangugeName()) ==0 ){					
					
					SelectedlanguageID = itr->getLanguageID();					
					break;
				}

			}			
		}
		//by Amarjit 
		else if(theSelectedWidget == kCTILevel2DDBoxWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			IControlView* Level2DDBoxWidgetControlView = panelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
			if(Level2DDBoxWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level2DDBoxWidgetController(Level2DDBoxWidgetControlView, UseDefaultIID());
			if(Level2DDBoxWidgetController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxWidgetController is nil");
				return;
			}

			int32 index2 = Level2DDBoxWidgetController->GetSelected();
			if(index2 == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				
				InterfacePtr<IStringListControlData> level3DropListData(dialogController->QueryListControlDataInterface(kCTILevel3DDBoxWidgetID));
				if(level3DropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level3DropListController(level3DropListData, UseDefaultIID());
				if(level3DropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListController is nil");	
					return;
				}
				level3DropListController->Select(0);

					 
				InterfacePtr<IStringListControlData> level1SIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxSIWidgetID));
				if(level1SIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level1SIDropListController(level1SIDropListData, UseDefaultIID());
				if(level1SIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListController is nil");	
					return;
				}
				level1SIDropListController->Select(0);	

				IControlView * Level3DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
				if(Level3DDBoxWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxWidgetControlData->IsEnabled ())
					Level3DDBoxWidgetControlData->Disable();
				
				IControlView * Level1DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
				if(Level1DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlData == nil");
					break;
				}
				/*if(Level1DDBoxSIWidgetControlData->IsEnabled ())
					Level1DDBoxSIWidgetControlData->Disable();*/
			}
			else
			{
				IControlView * Level3DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
				if(Level3DDBoxWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxWidgetControlData->IsEnabled() == kFalse)
					Level3DDBoxWidgetControlData->Enable();
				
				IControlView * Level1DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
				if(Level1DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level1DDBoxSIWidgetControlData->IsEnabled() == kFalse)
					Level1DDBoxSIWidgetControlData->Enable();
			}
		
		}
		else if(theSelectedWidget == kLevel2DDBoxSIWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("123");
			IControlView* Level2DDBoxSIWidgetControlView = panelControlData->FindWidget(kLevel2DDBoxSIWidgetID);
			if(Level2DDBoxSIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level2DDBoxSIWidgetController(Level2DDBoxSIWidgetControlView, UseDefaultIID());
			if(Level2DDBoxSIWidgetController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlView is nil");
				return;
			}

			int32 index = Level2DDBoxSIWidgetController->GetSelected();

			if(index == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				InterfacePtr<IStringListControlData> level3SIDropListData(dialogController->QueryListControlDataInterface(kLevel3DDBoxSIWidgetID));
				if(level3SIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level3SIDropListController(level3SIDropListData, UseDefaultIID());
				if(level3SIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListController is nil");	
					return;
				}
				level3SIDropListController->Select(0);

				InterfacePtr<IStringListControlData> level1TIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxTIWidgetID));
				if(level1TIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level1TIDropListController(level1TIDropListData, UseDefaultIID());
				if(level1TIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");	
					return;
				}
				level1TIDropListController->Select(0);
				IControlView * Level1DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
				if(Level1DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level1DDBoxTIWidgetControlData->IsEnabled())
					Level1DDBoxTIWidgetControlData->Disable();

				IControlView * Level3DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxSIWidgetID);
				if(Level3DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
					break;
				}
				/*if(Level3DDBoxSIWidgetControlData->IsEnabled())
					Level3DDBoxSIWidgetControlData->Disable();*/

			}
			else
			{
				IControlView * Level3DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxSIWidgetID);
				if(Level3DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxSIWidgetControlData->IsEnabled() == kFalse)
					Level3DDBoxSIWidgetControlData->Enable();
				
				IControlView * Level1DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
				if(Level1DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level1DDBoxTIWidgetControlData->IsEnabled() == kFalse)
					Level1DDBoxTIWidgetControlData->Enable();
			
			}
		
		}
		else if(theSelectedWidget == kLevel2DDBoxTIWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("123");
			IControlView* Level2DDBoxTIWidgetControlView = panelControlData->FindWidget(kLevel2DDBoxTIWidgetID);
			if(Level2DDBoxTIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level2DDBoxSIWidgetController(Level2DDBoxTIWidgetControlView, UseDefaultIID());
			if(Level2DDBoxSIWidgetController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlView is nil");
				return;
			}

			int32 index = Level2DDBoxSIWidgetController->GetSelected();
			if(index == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				InterfacePtr<IStringListControlData> Level3DDBoxTIWidgetData(dialogController->QueryListControlDataInterface(kLevel3DDBoxTIWidgetID));
				if(Level3DDBoxTIWidgetData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDBoxTIWidgetData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> Level3DDListController(Level3DDBoxTIWidgetData, UseDefaultIID());
				if(Level3DDListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDListController is nil");	
					return;
				}
				Level3DDListController->Select(0);

				IControlView * Level3DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
				if(Level3DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxTIWidgetControlData->IsEnabled())
					Level3DDBoxTIWidgetControlData->Disable();

			}
			else
			{
				
				IControlView * Level3DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
				if(Level3DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
					break;
				}
				
				if(Level3DDBoxTIWidgetControlData->IsEnabled() == kFalse)
					Level3DDBoxTIWidgetControlData->Enable();
				
			}
		
		}
		//=============================END==============================================================================
		else if(theSelectedWidget == kCTILevel1DDBoxWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			IControlView* Level1DDBoxWidgetControlView = panelControlData->FindWidget(kCTILevel1DDBoxWidgetID);
			if(Level1DDBoxWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level1DDBoxWidgetController(Level1DDBoxWidgetControlView, UseDefaultIID());
			if(Level1DDBoxWidgetController == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxWidgetController is nil");
				return;
			}

			int32 index = Level1DDBoxWidgetController->GetSelected();

			if(index == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				InterfacePtr<IStringListControlData> level2DropListData(dialogController->QueryListControlDataInterface(kCTILevel2DDBoxWidgetID));
				if(level2DropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level2DropListController(level2DropListData, UseDefaultIID());
				if(level2DropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListController is nil");	
					return;
				}
				level2DropListController->Select(0);

				//By Amarjit patil
				InterfacePtr<IStringListControlData> level3DropListData(dialogController->QueryListControlDataInterface(kCTILevel3DDBoxWidgetID));
				if(level3DropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level3DropListController(level3DropListData, UseDefaultIID());
				if(level3DropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2DropListController is nil");	
					return;
				}
				level3DropListController->Select(0);

					 
				InterfacePtr<IStringListControlData> level1SIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxSIWidgetID));
				if(level1SIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level1SIDropListController(level1SIDropListData, UseDefaultIID());
				if(level1SIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1SIDropListController is nil");	
					return;
				}
				level1SIDropListController->Select(0);			
						
				IControlView * Level2DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
				if(Level2DDBoxWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxWidgetControlData->IsEnabled ())
					Level2DDBoxWidgetControlData->Disable();
				//By Amarjit patil
				IControlView * Level3DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel3DDBoxWidgetID);
				if(Level3DDBoxWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxWidgetControlData->IsEnabled ())
					Level3DDBoxWidgetControlData->Disable();
				
				IControlView * Level1DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
				if(Level1DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level1DDBoxSIWidgetControlData->IsEnabled ())
					Level1DDBoxSIWidgetControlData->Disable();			
			}
			else{
				IControlView * Level2DDBoxWidgetControlData = panelControlData->FindWidget(kCTILevel2DDBoxWidgetID);
				if(Level2DDBoxWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxWidgetControlData->IsEnabled() == kFalse)
					Level2DDBoxWidgetControlData->Enable();

			}
		}
		else if(theSelectedWidget == kLevel1DDBoxSIWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			IControlView* Level1DDBoxSIWidgetControlView = panelControlData->FindWidget(kLevel1DDBoxSIWidgetID);
			if(Level1DDBoxSIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level1DDBoxSIWidgetController(Level1DDBoxSIWidgetControlView, UseDefaultIID());
			if(Level1DDBoxSIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxSIWidgetControlView is nil");
				return;
			}

			int32 index = Level1DDBoxSIWidgetController->GetSelected();

			if(index == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				InterfacePtr<IStringListControlData> level2SIDropListData(dialogController->QueryListControlDataInterface(kLevel2DDBoxSIWidgetID));
				if(level2SIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level2SIDropListController(level2SIDropListData, UseDefaultIID());
				if(level2SIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListController is nil");	
					return;
				}
				level2SIDropListController->Select(0);

				//By Amarjit 

				InterfacePtr<IStringListControlData> level3SIDropListData(dialogController->QueryListControlDataInterface(kLevel3DDBoxSIWidgetID));
				if(level3SIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level3SIDropListController(level3SIDropListData, UseDefaultIID());
				if(level2SIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level2SIDropListController is nil");	
					return;
				}
				level3SIDropListController->Select(0);

				InterfacePtr<IStringListControlData> level1TIDropListData(dialogController->QueryListControlDataInterface(kLevel1DDBoxTIWidgetID));
				if(level1TIDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> level1TIDropListController(level1TIDropListData, UseDefaultIID());
				if(level1TIDropListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::level1TIDropListData is nil");	
					return;
				}
				level1TIDropListController->Select(0);

				IControlView * Level1DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
				if(Level1DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level1DDBoxTIWidgetControlData->IsEnabled())
					Level1DDBoxTIWidgetControlData->Disable();

				IControlView * Level2DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxSIWidgetID);
				if(Level2DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxSIWidgetControlData->IsEnabled())
					Level2DDBoxSIWidgetControlData->Disable();		

				//By Amarjit patil

				IControlView * Level3DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxSIWidgetID);
				if(Level3DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxSIWidgetControlData->IsEnabled())
					Level3DDBoxSIWidgetControlData->Disable();	
			}
			else{
				IControlView * Level2DDBoxSIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxSIWidgetID);
				if(Level2DDBoxSIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxSIWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxSIWidgetControlData->IsEnabled() == kFalse)
					Level2DDBoxSIWidgetControlData->Enable();

			}
		}
		else if(theSelectedWidget == kLevel1DDBoxTIWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			IControlView* Level1DDBoxTIWidgetControlView = panelControlData->FindWidget(kLevel1DDBoxTIWidgetID);
			if(Level1DDBoxTIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlView is nil");
				return;
			}

			InterfacePtr<IDropDownListController> Level1DDBoxSIWidgetController(Level1DDBoxTIWidgetControlView, UseDefaultIID());
			if(Level1DDBoxTIWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level1DDBoxTIWidgetControlView is nil");
				return;
			}

			int32 index = Level1DDBoxSIWidgetController->GetSelected();

			if(index == 0){
				InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
				if(dialogController == nil)
				{
					CA("dialogController == nil");
					break;
				}
				InterfacePtr<IStringListControlData> Level2DDBoxTIWidgetData(dialogController->QueryListControlDataInterface(kLevel2DDBoxTIWidgetID));
				if(Level2DDBoxTIWidgetData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDBoxTIWidgetData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> Level2DDListController(Level2DDBoxTIWidgetData, UseDefaultIID());
				if(Level2DDListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDListController is nil");	
					return;
				}
				Level2DDListController->Select(0);

				//By Amarjit patil

				InterfacePtr<IStringListControlData> Level3DDBoxTIWidgetData(dialogController->QueryListControlDataInterface(kLevel3DDBoxTIWidgetID));
				if(Level3DDBoxTIWidgetData==nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDBoxTIWidgetData is nil");
					return;
				}

				InterfacePtr<IDropDownListController> Level3DDListController(Level3DDBoxTIWidgetData, UseDefaultIID());
				if(Level3DDListController == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogController::InitializeDialogFields::Level2DDListController is nil");	
					return;
				}
				Level3DDListController->Select(0);

				IControlView * Level2DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxTIWidgetID);
				if(Level2DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxTIWidgetControlData->IsEnabled())
					Level2DDBoxTIWidgetControlData->Disable();
				//By Amarjit patil
				IControlView * Level3DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
				if(Level3DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level3DDBoxTIWidgetControlData->IsEnabled())
					Level3DDBoxTIWidgetControlData->Disable();

			}
			else{
				IControlView * Level2DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel2DDBoxTIWidgetID);
				if(Level2DDBoxTIWidgetControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
					break;
				}
				if(Level2DDBoxTIWidgetControlData->IsEnabled() == kFalse)
					Level2DDBoxTIWidgetControlData->Enable();

				////By Amarjit patil

				//IControlView * Level3DDBoxTIWidgetControlData = panelControlData->FindWidget(kLevel3DDBoxTIWidgetID);
				//if(Level3DDBoxTIWidgetControlData == nil)
				//{
				//	ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::Update::Level2DDBoxTIWidgetControlData == nil");
				//	break;
				//}
				//if(Level3DDBoxTIWidgetControlData->IsEnabled() == kFalse)
				//	Level3DDBoxTIWidgetControlData->Enable();
			}

		}
		
}while (false);
}
//  Code generated by DollyXs code generator

void CTIDialogObserver::populatePRDropDownbox()
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return;
	do
	{
		NewAttributeList.clear();
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox::SelectDropListController is nil");		
			return;
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox::SelectDropListData is nil");				
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox::Interface for IClientOptions not found.");
			return;
		}
		
        PMString dummyLangId("");
		double languageID = ptrIClientOptions->getDefaultLocale(dummyLangId);
		
		VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( SelectedlanguageID/*languageID*/);
		if(eleValObj==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox::StructureCache_getItemGroupCopyAttributesByLanguageId's eleValObj is nil");						
			break;
		}
			
		VectorElementInfoValue::iterator it;
				
		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it->getElementId();
			ElementData.index = 3;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = it->getElementId();

			NewAttributeList.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}
		
		SelectDropListController->Select(0);
				
		delete eleValObj;
	}while(kFalse);
}

void CTIDialogObserver::populatePRDropDownbox1()
{
	do
	{
		NewAttributeList1.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox1::SelectComboView is nil");		
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox1::SelectDropListController is nil");
			return;
		}
	

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox1::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox1::Interface for IClientOptions not found.");
			return;
		}
		
		
		VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( SelectedlanguageID/*languageID*/);
		if(eleValObj==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox1::StructureCache_getItemGroupCopyAttributesByLanguageId's eleValObj is nil");
			break;
		}
			
		VectorElementInfoValue::iterator it;
				
		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it->getElementId();
			ElementData.index = 3;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = it->getElementId();

			NewAttributeList1.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}
		delete eleValObj;

		SelectDropListController->Select(0);
	}while(kFalse);
}

void CTIDialogObserver::populatePRDropDownbox2()
{
	do
	{
		NewAttributeList2.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox2::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox2::SelectDropListController is nil");
			return;
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox2::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox2::Interface for IClientOptions not found.");
			return;
		}
		
		VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( SelectedlanguageID/*languageID*/);
		if(eleValObj==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populatePRDropDownbox2::StructureCache_getItemGroupCopyAttributesByLanguageId's eleValObj is nil");
			break;
		}
			
		VectorElementInfoValue::iterator it;
				
		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it->getElementId();
			ElementData.index = 3;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = -1; //it->getElement_type_id();

			NewAttributeList2.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}

		SelectDropListController->Select(0);
		delete eleValObj;
	}while(kFalse);
}

void CTIDialogObserver::populateItemDropDownbox1()
{
	do
	{
		NewAttributeList1.clear();

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);
	
		VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
		if(CurrentClassID == -1)
		{				
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
			if(vectClassInfoValuePtr==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::ClassificationTree_getRoot's vectClassInfoValuePtr is nil");
				break;
			}
				
			VectorClassInfoValue::iterator it;
			it=vectClassInfoValuePtr->begin();
			CurrentClassID = it->getClass_id();
			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::Interface for IClientOptions not found.");
			return;
		}
		
		AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID,SelectedlanguageID/*languageID*/ );
		if(AttrInfoVectPtr== NULL)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox1::StructureCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil");
			break;
		}
					
		VectorAttributeInfoValue::iterator it2;

		for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it2->getAttributeId();
			ElementData.index = 4;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it2->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = -1/*it->getElement_type_id()*/;

			NewAttributeList1.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}
		if(AttrInfoVectPtr)
			delete AttrInfoVectPtr;
		SelectDropListController->Select(0);
	}while(kFalse);
}

void CTIDialogObserver::populateItemDropDownbox2()
{
	do
	{
		NewAttributeList2.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);
	
		VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
		if(CurrentClassID == -1)
		{				
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
			if(vectClassInfoValuePtr==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::ClassificationTree_getRoot's vectClassInfoValuePtr is nil");
				break;
			}
				
			VectorClassInfoValue::iterator it;
			it=vectClassInfoValuePtr->begin();
			CurrentClassID = it->getClass_id();
			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::Interface for IClientOptions not found.");
			return;
		}
		
		AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID,SelectedlanguageID/*languageID*/ );
		if(AttrInfoVectPtr== NULL)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox2::StructureCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil");
			break;
		}
					
		VectorAttributeInfoValue::iterator it2;

		for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it2->getAttributeId();
			ElementData.index = 4;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it2->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = -1/*it->getElement_type_id()*/;

			NewAttributeList2.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}

		if(AttrInfoVectPtr)
			delete AttrInfoVectPtr;
		SelectDropListController->Select(0);
	}while(kFalse);
}


void CTIDialogObserver::populateItemDropDownbox()
{
	do
	{
		NewAttributeList.clear();

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);
	
		VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
		if(CurrentClassID == -1)
		{				
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
			if(vectClassInfoValuePtr==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::ClassificationTree_getRoot's vectClassInfoValuePtr is nil");
				break;
			}
				
			VectorClassInfoValue::iterator it;
			it=vectClassInfoValuePtr->begin();
			CurrentClassID = it->getClass_id();
			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::Interface for IClientOptions not found.");
			return;
		}
		
		AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID,SelectedlanguageID/*languageID*/ );
		if(AttrInfoVectPtr== NULL)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateItemDropDownbox::StructureCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil");
			break;
		}
					
		VectorAttributeInfoValue::iterator it2;

		for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
		{
			AttributeListData ElementData;
			ElementData.code= "";
			ElementData.id = it2->getAttributeId();
			ElementData.index = 4;
			ElementData.isImageFlag = kFalse;
			ElementData.name = it2->getDisplayName();
			ElementData.tableFlag = 0;
			ElementData.typeId = -1/*it->getElement_type_id()*/;

			NewAttributeList.push_back(ElementData);
			SelectDropListData->AddString(ElementData.name);
		}

		if(AttrInfoVectPtr)
			delete AttrInfoVectPtr;
		SelectDropListController->Select(0);
	}while(kFalse);
}


void CTIDialogObserver::populateProjectDropDownbox()
{
	do
	{
		NewAttributeList.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
	
		int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox::Interface for IClientOptions not found.");
			return;
		}
		VectorElementInfoPtr eleValObj;
		
	//	if( PM_Project_Level == 1 )
		{
			 eleValObj = ptrIAppFramework->/*ElementCache_getProjectSecSubSecAttributesByIndex*/StructureCache_getSectionCopyAttributesByLanguageId(/*1,*/SelectedlanguageID/*languageID*/ );
			if(eleValObj==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox::StructureCache_getSectionCopyAttributesByLanguageId's eleValObj is nil");			
				break;
			}
			VectorElementInfoValue::iterator it;

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{			
				AttributeListData ElementData;
				ElementData.code= "";
				ElementData.id = it->getElementId();
				ElementData.index = 5;
				ElementData.isImageFlag = kFalse;
				ElementData.name = it->getDisplayName();
				ElementData.tableFlag = 0;
				ElementData.typeId = -1/*it->getElement_type_id()*/;

				NewAttributeList.push_back(ElementData);
				SelectDropListData->AddString(ElementData.name);
			}
		}
		if(eleValObj)
			delete eleValObj;
		SelectDropListController->Select(0);
		
	}while(kFalse);
}


void CTIDialogObserver::populateProjectDropDownbox1()
{
	do
	{
		NewAttributeList1.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
	
		int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox1::SelectComboView is nil");		
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox1::SelectDropListController is nil");		
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox1::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox1::Interface for IClientOptions not found.");
			return;
		}
		VectorElementInfoPtr eleValObj;
		
	//	if( PM_Project_Level == 1 )
		{
			 eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(/*1,*/SelectedlanguageID/*languageID*/ );
			if(eleValObj==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox1::StructureCache_getSectionCopyAttributesByLanguageId's eleValObj is nil");
				break;
			}
			VectorElementInfoValue::iterator it;

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{			
				AttributeListData ElementData;
				ElementData.code= "";
				ElementData.id = it->getElementId();
				ElementData.index = 5;
				ElementData.isImageFlag = kFalse;
				ElementData.name = it->getDisplayName();
				ElementData.tableFlag = 0;
				ElementData.typeId = -1/*it->getElement_type_id()*/;

				NewAttributeList1.push_back(ElementData);
				SelectDropListData->AddString(ElementData.name);
			}
		}
		if(eleValObj)
			delete eleValObj;
		SelectDropListController->Select(0);
		
	}while(kFalse);
}


void CTIDialogObserver::populateProjectDropDownbox2()
{
	do
	{
		NewAttributeList2.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
	
		int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox2::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox2::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox2::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox2::Interface for IClientOptions not found.");
			return;
		}
		VectorElementInfoPtr eleValObj;
		
	//	if( PM_Project_Level == 1 )
		{
			 eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(/*1,*/SelectedlanguageID/*languageID*/ );
			if(eleValObj==nil)
			{
				ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateProjectDropDownbox2::StructureCache_getSectionCopyAttributesByLanguageId's eleValObj is nil");
				break;
			}
			VectorElementInfoValue::iterator it;

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{			
				AttributeListData ElementData;
				ElementData.code= "";
				ElementData.id = it->getElementId();
				ElementData.index = 5;
				ElementData.isImageFlag = kFalse;
				ElementData.name = it->getDisplayName();
				ElementData.tableFlag = 0;
				ElementData.typeId = -1/*it->getElement_type_id()*/;

				NewAttributeList2.push_back(ElementData);
				SelectDropListData->AddString(ElementData.name);
			}
		}
		if(eleValObj)
			delete eleValObj;
		SelectDropListController->Select(0);
		
	}while(kFalse);
}

void CTIDialogObserver::populateCatagoryDropDownbox()
{
	//do
	//{
	//	NewAttributeList.clear();
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//		break;

	//	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//	IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBoxWidgetID);
	//	if(SelectComboView == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox::SelectComboView is nil");
	//		return;
	//	}
	//    
	//	InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
	//	if(SelectDropListController == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox::SelectDropListController is nil");
	//		return;	
	//	}

	//	InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
	//	if(SelectDropListData==nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox::SelectDropListData is nil");
	//		return;
	//	}

	//	SelectDropListData->Clear(kFalse, kFalse);
	//	PMString str("--Select--");
	//	SelectDropListData->AddString(str);

	//	
	//	
	//	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//	if(ptrIClientOptions==nil)
	//	{
	//		ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox::Interface for IClientOptions not found.");
	//		return;
	//	}
	//	VectorElementInfoPtr eleValObj = ptrIAppFramework->getCategoryCopyAttributesForPrint(SelectedlanguageID/*languageID*/);
	//	if(eleValObj==nil)
	//	{
	//		ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox::getCategoryCopyAttributesForPrint's eleValObj is nil");
	//		break;
	//	}

	//	VectorElementInfoValue::iterator it;

	//	int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();

	//	PMString temp1;
	//	PMString temp2;
	//	PMString Level;
	//	for(int32 j=1 ; j<=levels ;j++ )
	//	{
	//		temp1.Append("-Level ");
	//		Level.Clear();
	//		Level.AppendNumber(j);
	//		temp1.AppendNumber(j);

	//		for(it=eleValObj->begin();it!=eleValObj->end();it++)
	//		{
	//			temp2 = it->getDisplayName();
	//			temp2.Append(temp1);

	//			AttributeListData ElementData;
	//			ElementData.code= "";
	//			ElementData.id = it->getElementId();
	//			ElementData.index = 5;
	//			ElementData.isImageFlag = kFalse;
	//			ElementData.name = temp2;
	//			ElementData.tableFlag = 0;
	//			ElementData.typeId =-1 /*it->getElement_type_id()*/;

	//			NewAttributeList.push_back(ElementData);
	//			SelectDropListData->AddString(ElementData.name);

	//			/*temp2.Clear();
	//			temp2.AppendNumber(it->getElement_type_id());
	//			temp2.Clear();*/
	//		}
	//		temp1.Clear();
	//	}
	//	if(eleValObj)
	//		delete eleValObj;
	//	SelectDropListController->Select(0);
	//}while(kFalse);
}


void CTIDialogObserver::populateCatagoryDropDownbox1()
{
	do
	{
		NewAttributeList1.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox1WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox1::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox1::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox1::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		
		
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox1::Interface for IClientOptions not found.");
			return;
		}
		//VectorElementInfoPtr eleValObj = ptrIAppFramework->getCategoryCopyAttributesForPrint(SelectedlanguageID/*languageID*/);
		//if(eleValObj==nil)
		//{
		//	ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox1::getCategoryCopyAttributesForPrint'S eleValObj is nil");
		//	break;
		//}

		//VectorElementInfoValue::iterator it;

		//int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();

		//PMString temp1;
		//PMString temp2;
		//PMString Level;
		//for(int32 j=1 ; j<=levels ;j++ )
		//{
		//	temp1.Append("-Level ");
		//	Level.Clear();
		//	Level.AppendNumber(j);
		//	temp1.AppendNumber(j);

		//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//	{
		//		temp2 = it->getDisplayName();
		//		temp2.Append(temp1);

		//		AttributeListData ElementData;
		//		ElementData.code= "";
		//		ElementData.id = it->getElementId();
		//		ElementData.index = 5;
		//		ElementData.isImageFlag = kFalse;
		//		ElementData.name = temp2;
		//		ElementData.tableFlag = 0;
		//		ElementData.typeId =-1 /*it->getElement_type_id()*/;

		//		NewAttributeList1.push_back(ElementData);
		//		SelectDropListData->AddString(ElementData.name);

		//		/*temp2.Clear();
		//		temp2.AppendNumber(it->getElementId());
		//		temp2.Clear();*/
		//	}
		//	temp1.Clear();
		//}
		//if(eleValObj)
		//	delete eleValObj;
		SelectDropListController->Select(0);
	}while(kFalse);
}


void CTIDialogObserver::populateCatagoryDropDownbox2()
{
	do
	{
		NewAttributeList2.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IControlView* SelectComboView = panelControlData->FindWidget(kCTIIndexEntryComboBox2WidgetID);
		if(SelectComboView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox2::SelectComboView is nil");
			return;
		}
	    
		InterfacePtr<IDropDownListController> SelectDropListController(SelectComboView, UseDefaultIID());
		if(SelectDropListController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox2::SelectDropListController is nil");
			return;	
		}

		InterfacePtr<IStringListControlData> SelectDropListData(SelectDropListController,UseDefaultIID());
		if(SelectDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox2::SelectDropListData is nil");
			return;
		}

		SelectDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		SelectDropListData->AddString(str);

		
		
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox2::Interface for IClientOptions not found.");
			return;
		}
		//VectorElementInfoPtr eleValObj = ptrIAppFramework->getCategoryCopyAttributesForPrint(SelectedlanguageID/*languageID*/);
		//if(eleValObj==nil)
		//{
		//	ptrIAppFramework->LogError("AP46_CatalogIndex::CTIDialogObserver::populateCatagoryDropDownbox2::getCategoryCopyAttributesForPrint'S eleValObj is nil");
		//	break;
		//}

		//VectorElementInfoValue::iterator it;

		//int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();

		//PMString temp1;
		//PMString temp2;
		//PMString Level;
		//for(int32 j=1 ; j<=levels ;j++ )
		//{
		//	temp1.Append("-Level ");
		//	Level.Clear();
		//	Level.AppendNumber(j);
		//	temp1.AppendNumber(j);

		//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//	{
		//		temp2 = it->getDisplayName();
		//		temp2.Append(temp1);

		//		AttributeListData ElementData;
		//		ElementData.code= "";
		//		ElementData.id = it->getElementId();
		//		ElementData.index = 5;
		//		ElementData.isImageFlag = kFalse;
		//		ElementData.name = temp2;
		//		ElementData.tableFlag = 0;
		//		ElementData.typeId =-1 /*it->getElement_type_id()*/;

		//		NewAttributeList2.push_back(ElementData);
		//		SelectDropListData->AddString(ElementData.name);

		//		/*temp2.Clear();
		//		temp2.AppendNumber(it->getElement_type_id());
		//		temp2.Clear();*/
		//	}
		//	temp1.Clear();
		//}
		//if(eleValObj)
		//	delete eleValObj;
		SelectDropListController->Select(0);
	}while(kFalse);
}


BookContentDocInfoVector* CTIDialogObserver::GetBookContentDocInfo(IBookContentMgr* bookContentMgr)
{
	BookContentDocInfoVector* BookContentDocInfovectorPtr = new BookContentDocInfoVector;
	BookContentDocInfovectorPtr->clear();
	
	do {
		if (bookContentMgr == nil)
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();

		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			BookContentDocInfo contentDocInfo;
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content
			InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			if (bookLink == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
				break; // out of for loop
			}

			// get the book name and add it to the list
			PMString* baseName = bookLink->GetBaseName();
			//ASSERT(baseName && baseName->IsNull() == kFalse);
			
			IDFile CurrFile;
			bool16 IsMissingPluginFlag = kFalse;

			IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

			bool16 Flag1 = kFalse;
			Flag1 =  FileUtils::DoesFileExist(CurrFile);
			if(Flag1 == kFalse)
			{
				//CA("File dOES nOT Exists");
				continue;
			}
			else
			{				
				contentDocInfo.DocFile = CurrFile;	
				contentDocInfo.DocumentName = (*baseName);
				contentDocInfo.DocUID = contentUID;
				contentDocInfo.index = i;
				BookContentDocInfovectorPtr->push_back(contentDocInfo);
			}			
		}

	} while (false);
	return BookContentDocInfovectorPtr;
}
void CTIDialogObserver::DeleteAllPageRefIndices(IDocument* iDocument)
{
	UIDList pageRefList(::GetDataBase(iDocument));
	CollectAllPageRefIndices(iDocument,pageRefList);
	for ( int32 i=0 ; i < pageRefList.Length() ; i++ )
	{
		// Note, the kDeleteIndexPageEntryCmdBoss will delete only one at a time
		UIDRef pageRef(pageRefList.GetRef(i));
		DeletePageRef(pageRef);
	}
}
void CTIDialogObserver::CollectAllPageRefIndices(IDocument* iDocument,UIDList &pageRefList)
{
	do
	{
		IDataBase* db = ::GetDataBase(iDocument);
		InterfacePtr <IIndexTopicListList> pIndexTopicListList(iDocument->GetDocWorkSpace(), UseDefaultIID());
		for ( int32 i=0 ; i < pIndexTopicListList->GetNumTopicLists() ;	i++ )
		{
			UID uid = pIndexTopicListList->GetNthTopicList(i);
			InterfacePtr<IIndexTopicList> pIndexTopicList(db,uid,UseDefaultIID());
			if(pIndexTopicList )
			{
				int32 n = pIndexTopicList->GetNumTopicSections();
				for ( int32 j = 0 ; j < n ; j++ )
				{
					// get the page refs for this topic node
					UID secUID = pIndexTopicList->GetNthTopicSectionUID(j);
					DoTopicNode( pIndexTopicList,j,-1,pageRefList );
				}
			}
			pIndexTopicListList->RemoveTopicList(uid);//Added by Amit on 291209
		}
	}
	while ( false );
}
void CTIDialogObserver::DoTopicNode( IIndexTopicList* pIndexTopicList,double sectionId,double parentNodeId,UIDList &pageRefs )
{
	int32 n = pIndexTopicList->GetNumChildrenOfNode(sectionId ,parentNodeId);
	for ( int32 i=0 ; i < n ; i++ )
	{
		int32 nid = pIndexTopicList->GetNthChildNodeId(sectionId,parentNodeId,i);
		int32 pageEntries = pIndexTopicList->GetNumPageEntriesOfNode(sectionId,nid);
		for ( int32 j=0 ; j < pageEntries ; j++ )
		{
			UIDRef pageRefUID;
			pIndexTopicList->GetNthPageEntryUIDRef(sectionId,nid,j,pageRefUID);
			pageRefs.Append(pageRefUID.GetUID()); // add to list of refs to delete
		}
		// if this node has children, get their refs too
		DoTopicNode( pIndexTopicList,sectionId,nid,pageRefs );
	}
}
void CTIDialogObserver::DeletePageRef(UIDRef &pageRef)
{
	InterfacePtr<ICommand>iDeletePageRefCmd(CmdUtils::CreateCommand(kDeleteIndexPageEntryCmdBoss));
	InterfacePtr<IDeleteIndexPageEntryCmdData>cmdData(iDeletePageRefCmd, UseDefaultIID());
	if (iDeletePageRefCmd && cmdData)
	{
		cmdData->SetDeleteMarkerFlag(kTrue);
		cmdData->SetDoNotifyFlag(kFalse);
		iDeletePageRefCmd->SetItemList(UIDList(pageRef));
		CmdUtils::ProcessCommand(iDeletePageRefCmd);
	}
}



void CTIDialogObserver::AddKeys(IIDXMLElement* xmlElement, set<PMString>& resultIds)
{
	WideString sectionIDValue("");
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	PMString helperKey("");
	WideString underscore("_");
	WideString negativeOne("-1");
	WideString zero("0");
	WideString one("1");
	//WideString itemTypeForAssets("6882"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("6936"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForAssets("2192"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("2246"); // JAMIE this needs to be dynamic!!

	WideString parentID("parentID");
	WideString sectionID("sectionID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");

	if (xmlElement->HasAttribute(sectionID))
	{
		sectionIDValue.Append(xmlElement->GetAttributeValue(sectionID));
		if (sectionIDValue != zero) 
		{
			if (xmlElement->HasAttribute(parentID))
			{
				parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
				//if (parentIDValue != negativeOne)
				{
					if (xmlElement->HasAttribute(parentTypeID))
					{
						parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
						//if (parentTypeIDValue != negativeOne)
						{
							// JAMIE later removed the need to tag images for indexing.  We are only tagging text
							// JAMIE if parentTypeID is the item type ID for assets, convert it to the item type id for values...so we don't get repetitive items in the set
							//if (parentTypeIDValue == itemTypeForAssets)
							//{
							//	parentTypeIDValue.Clear();
							//	parentTypeIDValue.Append(itemTypeForValues);
							//}
							if (xmlElement->HasAttribute(imgFlag)) {
								imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
								if (imgFlagValue != one) {
									if (xmlElement->HasAttribute(childId)) {
										childIdValue.Append(xmlElement->GetAttributeValue(childId));

										helperKey.Append(sectionIDValue);
										helperKey.Append(underscore);
										helperKey.Append(parentTypeIDValue);
										helperKey.Append(underscore);
										helperKey.Append(parentIDValue);
										helperKey.Append(underscore);
										helperKey.Append(childIdValue);
										helperKey.SetTranslatable(kFalse);
										//CA(helperKey);

										resultIds.insert(set<PMString> ::value_type(helperKey));
									}
								}
							}
						}
					}
				}
			}
		}
	}

	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();
		AddKeys(childElement, resultIds);
		childElement->Release();
	}
}


void CTIDialogObserver::TagDocument(IIDXMLElement* xmlElement, IDocument* doc, 
									   vector<IndexReference> &indexReferences, UIDRef documentTopicListRef,
									   bool isTable, set<WideString> &childIds, bool shouldIndex)
{
	WideString sectionIDValue("");
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	PMString helperKey("");
	WideString minusOne("_-1");
	WideString zero("0");
	WideString one("1");
	WideString underscore("_");
	WideString negativeOne("-1");
	//WideString itemTypeForAssets("6882"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("6936"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForAssets("2192"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("2246"); // JAMIE this needs to be dynamic!!

	WideString parentID("parentID");
	WideString sectionID("sectionID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");

	if (xmlElement->HasAttribute(sectionID))
	{
		sectionIDValue.Append(xmlElement->GetAttributeValue(sectionID));
		if (sectionIDValue != zero) 
		{
			if (xmlElement->HasAttribute(parentID))
			{
				parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
				if (parentIDValue != negativeOne)
				{
					if (xmlElement->HasAttribute(parentTypeID))
					{
						parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
						//if (parentTypeIDValue != negativeOne)
						{
							// JAMIE later removed the need to tag images for indexing.  We are only tagging text
							// JAMIE if parentTypeID is the item type ID for assets, convert it to the item type id for values...so we don't get repetitive items in the set
							//if (parentTypeIDValue == itemTypeForAssets)
							//{
							//	parentTypeIDValue.Clear();
							//	parentTypeIDValue.Append(itemTypeForValues);
							//}
							if (xmlElement->HasAttribute(imgFlag)) {
								imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
								if (imgFlagValue != one) 
								{
									if (xmlElement->HasAttribute(childId)) {
										childIdValue.Append(xmlElement->GetAttributeValue(childId));

										for(int32 i=0; i < indexReferences.size(); i++) 
										{
											IndexReference indexReference = indexReferences.at(i);

											PMString irSectionId("");
											PMString irParentTypeId("");
											PMString irParentId("");
											PMString irChildId("");
											irSectionId.AppendNumber(PMReal(indexReference.getSectionID()));
											irParentTypeId.AppendNumber(PMReal(indexReference.getParentTypeID()));
											irParentId.AppendNumber(PMReal(indexReference.getParentID()));
											irChildId.AppendNumber(PMReal(indexReference.getChildID()));

											// if the indexReference is found, add to index
											if (irSectionId == sectionIDValue 
												&& irParentTypeId == parentTypeIDValue
												&& irParentId == parentIDValue
												&& irChildId == childIdValue
												&& shouldIndex)
											{
												//PMString ii("");
												//ii.AppendNumber(i);
												//ii.Append(" of ");
												//int size = static_cast<int>(indexReferences.size());
												//ii.AppendNumber(size);
												//ii.SetTranslatable(kFalse);
												//CA(ii);

												for (int32 l=0; l<3; l++)
												{
													vector<PMString> indexTerms;
													if (l == 0)
														indexTerms = indexReference.getIndexTerms();
													else if (l == 1)
														indexTerms = indexReference.getIndexTerms2();
													else
														indexTerms = indexReference.getIndexTerms3();

													if (indexTerms.size() == 0)
														break;

													IndexTopicEntry	entry;
													entry.GetNumNodes() = (int16) indexTerms.size();
													for (int32 k=0; k < indexTerms.size(); k++)
													{
														entry.GetTopicEntryNode(k).GetLanguage() = kLanguageUserDefault;
														entry.GetTopicEntryNode(k).GetLevel() = k + 1;
														WideString indexTerm = WideString(indexTerms.at(k));
														entry.GetTopicEntryNode(k).GetDisplayString() = indexTerm;
													}
										
													UID sectionUID;
													int32 topicNodeId;

													InterfacePtr<IIndexTopicList> piIndexTopicList(documentTopicListRef, UseDefaultIID());
													if (!piIndexTopicList->FindTopicEntry(entry, &sectionUID, &topicNodeId))
													{
														// Create a Topic Entry with the obtained string
														InterfacePtr<ICommand> piCreateTopicEntryCmd(CmdUtils::CreateCommand(kCreateTopicEntryCmdBoss));
														InterfacePtr<ICreateTopicEntryCmdData> piCreateTopicCmdData(piCreateTopicEntryCmd, UseDefaultIID());

														piCreateTopicCmdData->SetTopicListUIDRef(documentTopicListRef);
														piCreateTopicCmdData->SetTopicEntry(entry);
														if (CmdUtils::ProcessCommand(piCreateTopicEntryCmd) != kSuccess)
															break;

														sectionUID = piCreateTopicCmdData->GetTopicSectionUID();
														topicNodeId = piCreateTopicCmdData->GetTopicNodeId();
													}

													InterfacePtr<ICommand> piCreatePageEntryCmd(CmdUtils::CreateCommand(kCreateIndexPageEntryCmdBoss));
													InterfacePtr<ICreateIndexPageEntryCmdData> piCreatePageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
													InterfacePtr<IIndexPageEntryCmdData> piIndexPageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
													piCreatePageEntryCmdData->SetTopicListUIDRef(documentTopicListRef);
													piCreatePageEntryCmdData->SetTopicSectionUID(sectionUID);
													piCreatePageEntryCmdData->SetTopicNodeId(topicNodeId);
													piIndexPageEntryCmdData->SetPageRangeType(IIndexPageEntryData::kCurrentPage, kInvalidUID, 0);
													piIndexPageEntryCmdData->SetStyleUID(kInvalidUID);

													if (CmdUtils::ProcessCommand(piCreatePageEntryCmd) != kSuccess)
														break;

													UIDRef pageEntryUIDRef(piCreatePageEntryCmd->GetItemList()->GetRef(0));
													if (pageEntryUIDRef.GetUID() == kInvalidUID)
														break;

													TextIndex sIndex=0, eIndex=0;
													Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);

													if (eIndex > sIndex) 
													{
														RangeData rData(sIndex, eIndex);

														const XMLReference& xmlRef = xmlElement->GetXMLReference();
														const UIDRef & uidref = xmlRef.GetUIDRef();
														if(uidref != UIDRef::gNull)
														{
															if (l == 0)
															{
																InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite());
																txtSelSuite->SetTextSelection(uidref, rData, Selection::kDontScrollSelection, nil);
															}

															// Insert a mark in the text and in the owned item strand
															InterfacePtr<ICommand> piInsertIndexMarkCmd(CmdUtils::CreateCommand(kInsertIndexMarkCmdBoss));
															InterfacePtr<IRangeData> piRangeData(piInsertIndexMarkCmd, IID_IRANGEDATA);
															InterfacePtr<IInsertIndexMarkCmdData> piInsertIndexMarkCmdData(piInsertIndexMarkCmd, UseDefaultIID());
															piInsertIndexMarkCmdData->SetTargetItem(uidref);
															piInsertIndexMarkCmdData->SetPageEntryUIDRef(pageEntryUIDRef);

															piRangeData->Set(sIndex, eIndex);
															if (CmdUtils::ProcessCommand(piInsertIndexMarkCmd) != kSuccess)
																break;

															// Update the index entry page numbers
															InterfacePtr<ICommand> piUpdatePageEntryCmd(CmdUtils::CreateCommand(kUpdatePageEntryCmdBoss));
															piUpdatePageEntryCmd->SetItemList(pageEntryUIDRef);

															if (CmdUtils::ProcessCommand(piUpdatePageEntryCmd) != kSuccess)
																break;
														}
													}
												}

												break;	
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	WideString sectionTag("Section_");
	WideString customTabbedTextTag("CustomTabbedText");
	WideString psTableTag("PSTable");
	WideString header("header");

	bool alreadyTagged = false;
	
	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();

		WideString tagName = childElement->GetTagString();
		bool isSectionTag = tagName.Contains(sectionTag);
		bool isCustomTabbedTextTag = tagName == customTabbedTextTag;
		bool isPsTableTag = tagName.Contains(psTableTag);

		if (isCustomTabbedTextTag || isPsTableTag)
			isTable = true;

		if (!alreadyTagged && !isTable && !isSectionTag)
		{
			childIds.clear();
			TagDocument(childElement, doc, indexReferences, documentTopicListRef, isTable, childIds, true);
			alreadyTagged = true;
		}
		else if (isTable && !isSectionTag) 
		{
			bool isHeaderRowTag = false;
			if (childElement->HasAttribute(header))
			{
				WideString headerValue("");
				headerValue.Append(childElement->GetAttributeValue(header));
				if (headerValue == one)
					isHeaderRowTag = true;
			}

			if (isCustomTabbedTextTag || isPsTableTag || !isHeaderRowTag)
			{
				if (childElement->HasAttribute(childId))
				{
					WideString childIdValue("");
					childIdValue.Append(childElement->GetAttributeValue(childId));
					set<WideString>::iterator it;
					it = childIds.find(childIdValue);
					bool isChild = childIdValue != negativeOne;
					if (it == childIds.end()) 
					{
						if (isChild)
							childIds.insert(set<WideString> ::value_type(childIdValue));

						TagDocument(childElement, doc, indexReferences, documentTopicListRef, isTable, childIds, isChild);
						alreadyTagged = true;
					}
				}
				else
				{
					TagDocument(childElement, doc, indexReferences, documentTopicListRef, isTable, childIds, false);
					alreadyTagged = true;
				}
			}
		}
		
		childElement->Release();
	}
}


void CTIDialogObserver::DeleteAllTopics(IDocument* iDocument)
{
	IDataBase* db = ::GetDataBase(iDocument);
	InterfacePtr <IIndexTopicListList> pIndexTopicListList(iDocument->GetDocWorkSpace(), UseDefaultIID());

	// Indesign only supports upto one topic list currently
	UID uid = pIndexTopicListList->GetNthTopicList(0);
	UIDRef uidRef = UIDRef(db, uid);

	InterfacePtr<IIndexTopicList> pIndexTopicList(db,uid,UseDefaultIID());
	if(pIndexTopicList )
	{
		int32 totalTopicSections = pIndexTopicList->GetNumTopicSections();
		for (int32 j=0; j < totalTopicSections; j++)
		{
			int32 totalChildNodes = pIndexTopicList->GetNumChildrenOfNode(j, -1);
			for (int32 k=0; k < totalChildNodes; k++)
			{
				// always pass in 0 because once the first child is deleted, the next one is the first again
				int32 nodeId = pIndexTopicList->GetNthChildNodeId(j, -1, 0);
				InterfacePtr<ICommand>iDeleteTopicCmd(CmdUtils::CreateCommand(kDeleteTopicEntryCmdBoss));
				InterfacePtr<IDeleteTopicEntryCmdData>cmdData(iDeleteTopicCmd, UseDefaultIID());
				cmdData->SetDoNotifyFlag(kFalse);
				cmdData->SetTopicListUIDRef(uidRef);
				cmdData->SetTopicSectionId(j);
				cmdData->SetTopicNodeId(nodeId);
				CmdUtils::ProcessCommand(iDeleteTopicCmd);
			}
		}
	}
}
