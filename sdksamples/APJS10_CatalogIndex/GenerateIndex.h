#ifndef _GENERATEINDEX_H_
#define _GENERATEINDEX_H_

// SnpGenIndex.cpp
// Generate an index
// 

// PCH include
#include "VCPluginHeaders.h"

// Framework includes
//#include "SnpRunnable.h"
//#include "SnipRunLog.h"

// Interface includes
#include "IActiveTopicListContext.h"
#include "IBoolData.h"
#include "ICommand.h"
#include "IDataLinkReference.h"
#include "IDocument.h"
#include "IIndexCmdData.h"
#include "IIndexOptions.h"
#include "IPersistUIDRefData.h"
#include "IStoryList.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "IUIDData.h"

// General includes
#include "IndexingID.h"
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "PreferenceUtils.h"


// Customised ASSERT
// To be used in the "do ... while(false)" loops to break out on error condition



/** This snippet shows how to:
\li Generate an index in a document
*/


// The snippet class
class SnpGenIndex /*: public SnpRunnable*/
{
public:
	// SnippetRunner Framework hooks

	/** Constructor
	*/
	SnpGenIndex();

	/** Destructor
	*/
	virtual			~SnpGenIndex();

	/** Can run if a document is open
	@param	runnableContext		[IN]	See ISnpRunnableContext for documentation.
	@return kTrue if snippet can run, kFalse otherwise
	*/
	//bool16 CanRun(ISnpRunnableContext *runnableContext);

	/** Run the snippet.
	@param	runnableContext		[IN]	See ISnpRunnableContext for documentation.
	@return kSuccess on success, other ErrorCode otherwise.
	*/
	//ErrorCode Run(ISnpRunnableContext *runnableContext);

	/** Checks whether there is already an index story in the document.
	@param		piDoc	[IN] Interface pointer to the document which is to be indexed.
	@return The UIDRef of the text story of the Index if one is present, nil otherwise.
	*/
	UIDRef GetIndexTextStory(IDocument *piDoc);

	/** Creates the index.
	@param		piDoc				[IN] Interface pointer to the document which is to be indexed.
	@param		indexStoryUIDRef	[IN] The UIDRef of the story which contains the previously
	generated index. If there is none, then this parameter is UIDRef(nil, kInvalidUID).
	@return		kSuccess	If the index was successfully created, kFailure otherwise.
	*/ 
	ErrorCode GenerateIndex(IDocument *piDoc, UIDRef indexStoryUIDRef);

};

#endif