//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/public/interfaces/architecture/IPersistUIDRefData.h $
//  
//  Owner: brendan
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __IPersistUIDRefData__
#define __IPersistUIDRefData__

#ifdef ID_DEPRECATED
// please update code to use IPersistUIDData instead of IPersistUIDRefData
#include "IPersistUIDData.h"

class IPersistUIDRefData : public IPersistUIDData
{
};

#endif

#endif
