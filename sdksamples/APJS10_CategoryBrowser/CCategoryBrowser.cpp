#include "VCPlugInHeaders.h"
#include "ICategoryBrowser.h"
#include "CPMUnknown.h"
#include "CTBActionComponent.h"
#include "CTBID.h"
#include "CAlert.h"
//----------
#include "ClassMediator.h"
#include "ITreeViewController.h"
#include "IAppFramework.h"
#include "CTBTreeNodeID.h"
#include "ITreeViewMgr.h"
#include "ITreeViewHierarchyAdapter.h"
#include "ClassTreeModel.h"
#include "ILoginHelper.h"
#include "ClassificationNode.h"
#include "ClassCache.h"

#include "IPanelMgr.h"
#include "IApplication.h"
#include "AcquireModalCursor.h"
#include "CTBSelectionObserver.h"

using namespace std;
#define CA(X) CAlert::InformationAlert(X)

extern  NodeID catNode_id;
extern  NodeID pubNodeID ;
int32 isContentSprayerFlag = 1;		//--- 1 for Content Sprayer , 2 TemplateBuilder , 3 Search .
extern int32 node_count ;

extern double currSelLanguageID ;
int32 isCheckCount = 0; 
extern bool16 isSelectEventCall;

class CCategoryBrowser:public CPMUnknown<ICategoryBrowser>
{	
public:
	CCategoryBrowser(IPMUnknown* );
	~CCategoryBrowser();	
	void OpenCategoryBrowser(int32 Top, int32 Left, int32 Right, int32 Bottom ,int32 isContentFlg , double currSelected_ClassID);
	double GetSelectedClassID(PMString &ClassName);
	void CloseCategoryBrowser();

};

CREATE_PMINTERFACE(CCategoryBrowser,kCTBCategoryBrowserImpl)

CCategoryBrowser::CCategoryBrowser(IPMUnknown* boss):CPMUnknown<ICategoryBrowser>(boss)
{}

CCategoryBrowser::~CCategoryBrowser()
{}

void CCategoryBrowser::OpenCategoryBrowser(int32 Top, int32 Left, int32 Bottom, int32 Right ,int32 isContentFlg , double currSelected_ClassID)
{
	//CA("CCategoryBrowser::OpenCategoryBrowser");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::ptrIAppFramework == nil");
		return ;
	}

	AcquireWaitCursor awc ;
	awc.Animate(); 

	InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		//CA("ptrLogInHelper == nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::ptrLogInHelper == nil");
		return ;
	}
	LoginInfoValue cserverInfoValue;
	bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
	ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();


	PMString strProjectID = clientInfoObj.getProject();
	//PMString strProjectID = ptrIAppFramework->getCatalogId();
	double projectID = ptrIAppFramework->getCatalogId(); //strProjectID.GetAsNumber();
	
	currSelLanguageID = clientInfoObj.getLanguage().GetAsDouble();	//---------
	//currSelLanguageID = ptrIAppFramework->getLocaleId();

									
	isCheckCount =  kFalse; //cserverInfoValue.getShowObjectCountFlag();
	

	InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	if(iApplication==nil)
	{
		//CA("iApplication==nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::OpenCategoryBrowser::DoPallete::iApplication == nil");
		return;
	}
	
	InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); //Amit
	if(iPanelMgr == nil)
	{
		//CA("iPanelMgr==nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::OpenCategoryBrowser::DoPallete::iPanelMgr == nil");				
		return;
	}
	const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;
	bool16 showPanel = iPanelMgr->IsPanelWithMenuIDShown(MyPalleteActionID);
	/*if(showPanel  &&  strProjectID !="" )
	{
		CTBActionComponent actionComponetObj(this);
		actionComponetObj.ClosePallete();
	}*/

	PMString ASD("isContentFlg: ");
	ASD.AppendNumber(isContentFlg);
	ptrIAppFramework->LogError(ASD);

	//CA("CCategoryBrowser::OpenCategoryBrowser 1 ");
	if(isContentSprayerFlag != isContentFlg  &&  showPanel)
	{
		//CA("isContentSprayerFlag != isContentFlg  &&  showPanel");
		CTBActionComponent actionComponetObj(this);
		actionComponetObj.ClosePallete();
	}
	isContentSprayerFlag = isContentFlg ;

	if(isContentSprayerFlag != 1)
		node_count = 0;

	ClassCache clsCache1;
	clsCache1.clearMap();
	//CA("CCategoryBrowser::OpenCategoryBrowser 2 ");
	CTBActionComponent actionComponetObj(this);
	//actionComponetObj.DoPallete(Top, Left, Bottom, Right); 
	actionComponetObj.DoPallete();

	//CA("CCategoryBrowser::OpenCategoryBrowser 3 ");
	PMString str("");
	str.AppendNumber(ClassTreeModel::getRootId());
	
	ptrIAppFramework->LogError("AP7_CategoryBrowser::OpenCategoryBrowser:: ClassTreeModel::getRootId() = " + str);

	
	PMString str1("");
	str1.AppendNumber(clsCache1.getSize());
	ptrIAppFramework->LogError("****** AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::  "  +  str1);
	//CA("CCategoryBrowser::OpenCategoryBrowser 4 ");
	if(clsCache1.getSize() <= 1)
	{
		ptrIAppFramework->LogError("****** AP7_CategoryBrowser::ClassTreeModel:: before loading catagory tree first time ");
		//CA("CCategoryBrowser::OpenCategoryBrowser 5 ");
		CTBSelectionObserver CTBSelectionObserverObj(this);
		CTBSelectionObserverObj.loadPaletteData();

		ptrIAppFramework->LogError("****** AP7_CategoryBrowser::ClassTreeModel:: after loading catagory tree first time ");
	}

	//CA("CCategoryBrowser::OpenCategoryBrowser 6 ");
	InterfacePtr<ITreeViewMgr> treeViewMgr(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
	if(!treeViewMgr) 
	{
		//CA("!treeViewMgr");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::!treeViewMgr");
		return;
	}
	InterfacePtr<ITreeViewController> treeViewCntrl(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
	if(treeViewCntrl==nil) 
	{
		//CA("treeViewCntrl==nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::treeViewCntrl==nil");
		return ;
	}

	//if(/*strProjectID == ""*/ projectID == -1)
	//{	
	//	//CA("strProjectID == ");
	//	ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::strProjectID == NULL ");
	//	return;
	//}
	
	if(ClassMediator::ClassTreeCntrlView)
			ClassMediator::ClassTreeCntrlView->Invalidate();
		
		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	//CA("CCategoryBrowser::OpenCategoryBrowser 7 ");
	//if(isContentSprayerFlag == 2 )
	//{
	//	//CA("isContentSprayerFlag ==== 2");
	//	treeViewMgr->ExpandNode(catNode_id , kTrue);
	//	treeViewMgr->CollapseNode(catNode_id , kTrue);
	//}
	//else 
	//if(isContentSprayerFlag == 1 )
	//{
	//	//CA("isContentSprayerFlag ==== 1");
	//	ptrIAppFramework->LogDebug("isContentSprayerFlag ==== 1");
	//	treeViewMgr->ExpandNode(pubNodeID , kTrue);
	//	treeViewMgr->CollapseNode(pubNodeID , kTrue);
	//}
	//CA("123");
	//else if(isContentSprayerFlag == 3 )
	//{
	//	//CA("isContentSprayerFlag ==== 3");
	//	treeViewMgr->ExpandNode(catNode_id , kTrue);
	//	treeViewMgr->CollapseNode(catNode_id , kTrue);
	//	treeViewMgr->ExpandNode(pubNodeID , kTrue);
	//	treeViewMgr->CollapseNode(pubNodeID , kTrue);
	//}
	//CA("CCategoryBrowser::OpenCategoryBrowser 8 ");
	
	//ClassificationNode classNode;
	//ClassCache clsCache;
	//if(isContentSprayerFlag == 1 || isContentSprayerFlag == 3)
	//{
	//	//CA("isContentSprayerFlag == 1 || isContentSprayerFlag == 3");
	//	PMString strSectionID =	cserverInfoValue.getSectionID();
	//	int32 section_ID = strSectionID.GetAsNumber();
	//	//CA("CCategoryBrowser::OpenCategoryBrowser 9 ");
	//	if(section_ID == -1)
	//	{
	//		bool16 isExist = clsCache.isNodeExist(projectID , classNode);
	//		if(isExist)
	//		{
	//			NodeID node_id = classNode.getNode_ID();
	//			treeViewMgr->ScrollToNode(node_id , ITreeViewMgr::eScrollIntoView);
	//			treeViewCntrl->Select(node_id);
	//		}
	//		else
	//		{
	//			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::section_ID == -1 !!isExist  ::  ");
	//		}
	//		
	//	}
	//	else
	//	{
	//		//CA("CCategoryBrowser::OpenCategoryBrowser 10 ");
	//		bool16 isExist = clsCache.isNodeExist(section_ID , classNode);
	//		if(isExist)
	//		{
	//			NodeID node_id = classNode.getNode_ID();
	//			isSelectEventCall = kTrue;
	//			treeViewMgr->ScrollToNode(node_id , ITreeViewMgr::eScrollIntoView);
	//			treeViewCntrl->Select(node_id);
	//			isSelectEventCall = kFalse;
	//		}
	//		else
	//		{
	//			
	//			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::section_ID != -1!!isExist :  " + strSectionID);


	//				bool16 isExist = clsCache.isNodeExist(projectID , classNode);
	//				if(isExist)
	//				{
	//					//CA("CCategoryBrowser::OpenCategoryBrowser 11 ");
	//					NodeID node_id = classNode.getNode_ID();
	//					treeViewMgr->ScrollToNode(node_id , ITreeViewMgr::eScrollIntoView);

	//					isSelectEventCall = kTrue;
	//					treeViewCntrl->Select(node_id);
	//					isSelectEventCall = kFalse;
	//				}
	//				else
	//				{
	//					ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::section_ID == -1 !!isExist  ::  ");
	//				}
	//		
	//		}

	//	}
	//	
	//}
	//else if(isContentSprayerFlag == 2 )
	//{
	//	//CA("isContentSprayerFlag == 2");
	//	bool16 isExist = clsCache.isNodeExist(currSelected_ClassID , classNode);
	//	if(isExist)
	//	{
	//		NodeID temp_NodeId = classNode.getNode_ID();
	//		treeViewMgr->ScrollToNode(temp_NodeId , ITreeViewMgr::eScrollIntoView);
	//		treeViewCntrl->Select(temp_NodeId);

	//	}
	//	else
	//	{
	//		ptrIAppFramework->LogError("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::isContentSprayerFlag == 2 !!isExist");
	//	}
	//
	//}
		
}


double CCategoryBrowser::GetSelectedClassID(PMString &ClassName)
{
	return 0;
}

void CCategoryBrowser::CloseCategoryBrowser()
{
	CTBActionComponent actionComponetObj(this);
	actionComponetObj.ClosePallete(); 
}