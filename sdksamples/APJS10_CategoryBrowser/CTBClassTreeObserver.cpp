#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "K2Vector.h"
#include "K2Vector.tpp" 
#include "SystemUtils.h"
//#include "CTBID.h"
#include "CTBID.h"
#include "ClassTreeModel.h"
#include "CAlert.h"
#include "ClassificationNode.h"
#include "ClassCache.h"
#include "IAppFrameWork.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "CDialogObserver.h"
#include "ClassMediator.h"
#include "CTBTreeNodeID.h"
#include "ITreeViewController.h"
#include "ITemplatesDialogCloser.h"
#include "IApplication.h"
//commented by nitin
//#include "IPaletteMgr.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "PMString.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "IContentSprayer.h"
#include "IAppFramework.h"
#include "SPID.h"
//
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
//#include "SEAID.h"
//#include "IOneSourceSearch.h"
#include "IClientOptions.h"
#include "StringUtils.h"
#include "ICMMInterface.h"
//
#define CA(X)		CAlert::InformationAlert(X)

double currPublicationID = -1;
PMString currPublicationName = "";
extern double currSelLanguageID;
extern PMString currSelLangName ;
extern int32 isContentSprayerFlag ;
bool16 isSelectEventCall = kFalse;
//extern int32 isCheckCount;
//extern ItemProductCountMapPtr itemProductSectionCountPtr;

//-------------The end user changes the node which is selected in the tree view control. To receive notifications
//about this, attach an observer (IObserver implementation of your own) to the ISubject
//interface of your kTreeViewWidgetBoss subclass, and listen along protocol IID_ITREEVIEWCONTROLLER.

class CTBClassTreeObservser : public CObserver
{
private:
	double selectedID;
public:
	CTBClassTreeObservser(IPMUnknown *boss) : CObserver(boss) {}
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(CTBClassTreeObservser, kCTBClassTreeObserverImpl)

void CTBClassTreeObservser::AutoAttach()
{
	//CAlert::InformationAlert("Inside AutoAttach");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this,  IID_ITREEVIEWCONTROLLER);
	}
}


void CTBClassTreeObservser::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,  IID_ITREEVIEWCONTROLLER);
	}
}

void CTBClassTreeObservser::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	//CA("CTBClassTreeObservser::Update");	
	
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}

	//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update");			
	bool16 isONESource = kFalse ;
	PMString selectedClassName = "";

	switch(theChange.Get())
	{
		case kListSelectionChangedMessage:
		{ 
			//ptrIAppFramework->LogDebug("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update:kListSelectionChangedMessage");		
			//if(isSelectEventCall)
			//{
			//	//CA("isSelectEventCall");
			//	//isSelectEventCall = kFalse;
			//	return;
			//}
			//CA("kListSelectionChangedMessage"); 
			InterfacePtr<ITreeViewController> treeViewCntrl(this, UseDefaultIID());
			if(treeViewCntrl==nil) 
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::treeViewCntrl == nil");			
				break;
			}

			NodeIDList selectedItem;
			
			treeViewCntrl->GetSelectedItems(selectedItem);
			if(selectedItem.size()<=0)
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::selectedItem.size()<=0");			
				break;
			}

			NodeID nid=selectedItem[0];
			TreeNodePtr<CTBTreeNodeID>  uidNodeIDTemp(nid);

			double uid= uidNodeIDTemp->GetNodeId();
			selectedID=uid;                                             

			ClassificationNode clasNode;
			ClassCache clsCache;

			bool16 retval = clsCache.isExist(uid, clasNode);
			if(retval==kTrue)
			{
				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 1");		
				PMString hierarchy("");
				int count=0;
				

				double CurrentClassID = clasNode.getClassId();
				PMString* temp = new PMString[clasNode.getLevel()+ 1];
	
				while(1)
				{
					if(clsCache.isExist(uid, clasNode))
					{
						//CA("clsName  :  "+clasNode.getClassName());
						//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: selected " + clasNode.getClassName());		
						temp[count]=clasNode.getClassName();
						if(clasNode.getParentId()==-2/*1*/)
						{
							//ptrIAppFramework->LogInfo("AP7_CategoryBrowser::CTBClassTreeObserver::Update::clasNode.getParentId()==-1");			
							break;
						}
						count++;
						uid=clasNode.getParentId();
					}
				}

				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 2 ");		
				for(int i=count;i>=0; i--)
				{
					hierarchy.Append(temp[i]);
					if(i>0)
						hierarchy.Append(" > ");
				}

				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 3 ");	
				retval = clsCache.isExist(selectedID, clasNode);
				ClassMediator::clasId = selectedID;
				//classID=selectedID;
				ClassMediator::clasName = clasNode.getClassName();
				//CA("ClassMediator::clasName  :  "+ClassMediator::clasName);

				ClassMediator::hier = hierarchy;

				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 4 ");	
				//name=hierarchy;
				//CA("hierarchy   :  "+hierarchy);
/////////////////////////////////////////////////////////////////////////////
//following code is added by vijay on 12/9/2006
				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
				if(app == NULL)
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::app == nil");			
					break;
				}

				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager()); 
				if(panelMgr == NULL) 
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::panelMgr == nil");			
					break;
				}
				IControlView* myPanel = panelMgr->GetVisiblePanel(kCTBPanelWidgetID);
				if(!myPanel) 
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::myPanel == nil");			
					break;
				}
			
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				IControlView* textView = panelControlData->FindWidget( kCTBMultilineTextWidgetID);
				InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);

				WideString ws;
                hierarchy.ParseForEmbeddedCharacters();
                StringUtils::ConvertUTF8ToWideString(hierarchy.GetUTF8String(), ws);
                PMString asd(ws);
                asd.ParseForEmbeddedCharacters();
				textPtr->SetString(asd.SetTranslatable(kFalse));

				//hierarchy.SetTranslatable(kFalse);
				//textPtr->SetString(hierarchy);

////////////////////////////////////////////////////////////upto here

				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 5 ");	

				if(isContentSprayerFlag == 2)
				{
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: isContentSprayerFlag == 2 ");	
					//CA("isContentSprayerFlag == 2");
					InterfacePtr<ITemplatesDialogCloser> TmpldlgCloser((ITemplatesDialogCloser*) ::CreateObject(kTemplatesDialogCloserBoss,IID_ITEMPLATEDIALOGCLOSER));
					if(TmpldlgCloser != nil)
					{
						//CAlert::InformationAlert("Refreshing Item List");
						if(!clasNode.getIsPUB())
						{
							//CA("Calling refreshItemTreeForClassID");
							//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 6 ");	
							//ptrIAppFramework->set_isONEsourceMode(kTrue);  // Comented now not sure of use.
							TmpldlgCloser->refreshItemTreeForClassID(CurrentClassID);

							//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: 7 ");	
						}
						
					}
				}

				/*ClassMediator::PFID=0;
				ClassMediator::PGID=0;*/
				
				/*InterfacePtr<ITextControlData> newEltext(ClassMediator::ClassPathStringView,UseDefaultIID());
				if (newEltext == nil) { 
					break;
				}
				newEltext->SetString(hierarchy);*/
				

	//			/*InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	//			if(ptrIAppFramework == nil)
	//			{
	//				CAlert::ErrorAlert("Interface for IAppFramework not found.");
	//				return ;
	//			}
	//		
	//			int32 Obj_Type_Id=-1;
	//			
	//			if(PF_LEVELOption == 3)
	//			{
	//				Obj_Type_Id = ptrIAppFramework->TYPE_getTypeIdByCode("PF_LEVEL");
	//			}
	//			else if(PF_LEVELOption == 2)
	//			{
	//				Obj_Type_Id = ptrIAppFramework->TYPE_getTypeIdByCode("PG_LEVEL");
	//					
	//			}
	//			else if(PF_LEVELOption == 1)
	//			{						
	//				break;
	//			}
	//			VectorFamilyInfoPtr vectFamilyInfoValuePtr = 
	//				ptrIAppFramework->FAMMngr_findByClassIdAndObjectTypeId(selectedID, Obj_Type_Id);
	//			if(vectFamilyInfoValuePtr == NULL)
	//			{
	//				ClassMediator::PrFamilyDropDownView->Enable();						
	//				InterfacePtr<IDropDownListController> iPFDropDownListController(ClassMediator::PrFamilyDropDownView, UseDefaultIID());
	//				if (iPFDropDownListController == nil)
	//					break;

	//				InterfacePtr<IStringListControlData> iPFStringListControlData(iPFDropDownListController, UseDefaultIID());
	//				if (iPFStringListControlData == nil)
	//					break;
	//				
	//				iPFStringListControlData->Clear(kFalse, kFalse);
	//				ClassMediator::PrFamilyDropDownView->Disable();
	//				
	//				if(PF_LEVELOption == 3)
	//				{
	//					ClassMediator::PrGroupDropDownView->Enable();
	//					InterfacePtr<IDropDownListController> iPGDropDownListController(ClassMediator::PrGroupDropDownView, UseDefaultIID());
	//					if (iPFDropDownListController == nil)
	//						break;

	//					InterfacePtr<IStringListControlData> iPGStringListControlData(iPGDropDownListController, UseDefaultIID());
	//					if (iPGStringListControlData == nil)
	//						break;
	//					
	//					iPGStringListControlData->Clear(kFalse, kFalse);						
	//					ClassMediator::PrGroupDropDownView->Disable();
	//				}
	//				break;
	//			}
	//			else
	//			{
	//				if(vectFamilyInfoValuePtr->size() <=0)
	//					break;
	//				
	//				PFComboList.clear();
	//				ClassMediator::PrFamilyDropDownView->Enable();
	//				InterfacePtr<IDropDownListController> iPFDropDownListController(ClassMediator::PrFamilyDropDownView, UseDefaultIID());
	//				if (iPFDropDownListController == nil)
	//					break;

	//				InterfacePtr<IStringListControlData> iStringListControlData(iPFDropDownListController, UseDefaultIID());
	//				if (iStringListControlData == nil)
	//					break;

	//				iStringListControlData->Clear(kFalse, kFalse);
	//				PMString str("--Select--");
	//				iStringListControlData->AddString(str);

	//				VectorFamilyInfoValue::iterator it;
	//				for(it=vectFamilyInfoValuePtr->begin(); it!=vectFamilyInfoValuePtr->end(); it++)
	//				{
	//					CObjectValue ObjVal;
	//					ObjVal.setObject_id(it->getObject_id());
	//					ObjVal.setObject_type_id(it->getObject_type_id());
	//					ObjVal.setParent_id(it->getParent_id());
	//					ObjVal.setLevel_no(it->getLevel_no());
	//					ObjVal.setName(it->getName());
	//					PMString PFName=it->getName();							
	//					iStringListControlData->AddString(PFName);
	//					PFComboList.push_back(ObjVal);
	//				}
	//				iPFDropDownListController->Select(0);
	//				if(PF_LEVELOption == 3)
	//				{
	//					ClassMediator::PrGroupDropDownView->Enable();
	//					InterfacePtr<IDropDownListController> iPGDropDownListController(ClassMediator::PrGroupDropDownView, UseDefaultIID());
	//					if (iPFDropDownListController == nil)
	//						break;

	//					InterfacePtr<IStringListControlData> iPGStringListControlData(iPGDropDownListController, UseDefaultIID());
	//					if (iPGStringListControlData == nil)
	//						break;
	//					
	//					iPGStringListControlData->Clear(kFalse, kFalse);						
	//					ClassMediator::PrGroupDropDownView->Disable();
	//				}
	//			}
	//			if(temp)
	//			delete[] temp;*/
	//			break;
				if(temp)
				{
					//delete temp; //og
					delete[] temp;
				}

			}


			//Added on 21/09/2006 to pass ClassID and Name to AP7_ProductFinder where the Class Name will be displayed on multilineTextWidget when global_project_level=4
//// for search 


			InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
			if(app == NULL) 
			{
				ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::app == nil");			
				break ;
			}
			//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
			//if(paletteMgr == NULL) 
			//{
			//	ptrIAppFramework->LogDebug("AP46_CategoryBrowser::CTBClassTreeObserver::Update::paletteMgr == nil");			
			//	break;
			//}
			InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
			if(panelMgr == NULL) 
			{
				ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::panelMgr == nil");			
				break;
			}
			//IControlView* myPanel = panelMgr->GetVisiblePanel(kSEAPanelWidgetID);
			//if(!myPanel) 
			//{
			//	//CA("if(!myPanel) ");			
			//	//return kFalse;
			//}
////// for search ends
//
//			//Added on 21/09/2006 to pass ClassID and Name to AP7_ProductFinder where the Class Name will be displayed on multilineTextWidget when global_project_level=4
			if(isContentSprayerFlag == 1 || isContentSprayerFlag == 3)
			{
				//CA("isContentSprayerFlag == 1 || isContentSprayerFlag == 3");
				//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update: isContentSprayerFlag == 1 || isContentSprayerFlag == 3 ");	
				InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
				if(!ContentSprayerPtr)
				{
					ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
					return ;
				}
				InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
				if(cop == nil)
				{
					//CA("cop == nil");
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
					break;
				}
	//			int32 ID = ClassMediator::clasId;
	//			/*PMString num;
	//			num.AppendNumber(ID);
	//			CA("classId" + num);*/
	//			PMString name("");
				bool16 functionResult;
	//			name = ClassMediator::hier;
	//			//Added by nitin
	//			////22/06/07
	//			CharCounter Counter1 = -1;
	//			Counter1 = name.IndexOfString(">");
	//			if(Counter1 != -1)
	//			{
	//				name.Remove(0, Counter1 + 1 );
	//
	//			}
	//			else
	//				name = "Select Category";
	//
	//			////22/05/07
	//
				PMString name("");
				double ID = ClassMediator::clasId;
				name = ClassMediator::clasName;
				double sectionID = -1;
				PMString sectionName = "";

				currPublicationName = name;
				currPublicationID = ID;

				if(clasNode.getIsPUB() == kTrue)	
				{
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 8");	
					//CA("clasNode.getIsPUB() == kTrue");
					isONESource = kFalse ;
					ptrIAppFramework->set_isONEsourceMode(kFalse);

					if(clasNode.getLevel() > 3)	//-----
					{
						//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 9");
						 sectionID = ClassMediator::clasId;
						 sectionName = clasNode.getClassName();
						
						CPubModel pubModelCurrentValue = ptrIAppFramework->getpubModelByPubID(ClassMediator::clasId,currSelLanguageID);

						CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubModelCurrentValue.getRootID(),currSelLanguageID);

						currPublicationID = pubModelRootValue.getRootID();
						currPublicationName = pubModelRootValue.getName();

						//CA("currPublicationName = " + currPublicationName);
					}
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 10");
				}
				else
				{
					isONESource = kTrue ;
					//ptrIAppFramework->set_isONEsourceMode(kTrue);
						
					ID = clasNode.getClassId();
					name = ClassMediator::clasName;

					currPublicationName = name;
					currPublicationID = ID;

				}


				cop->setPublication(currPublicationName , currPublicationID);
				cop->setLocale(currSelLangName , currSelLanguageID );
				cop->setSection(sectionName,sectionID );
                
                ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::creating cmmInterfacePtr");
                InterfacePtr<ICMMInterface> cmmInterfacePtr((ICMMInterface*)::CreateObject(kCMMInterfaceBoss, IID_ICMMINTERFACE));
                if(!cmmInterfacePtr)
                {
                    ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to cmmInterfacePtr not found");
                }
                else
                {
                    ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Before callning  cmmInterfacePtr->setEventInCMMcd");
                
                    cmmInterfacePtr->setEventInCMM(currPublicationID , currSelLanguageID);
                    ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::After callning  cmmInterfacePtr->setEventInCMMcd");
                }

				if(ContentSprayerPtr != nil)
				{
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 11");
					//CA("ContentSprayerPtr != nil 11111");
					vector<CPubModel>* pCPubModelVec = NULL;
					vector<ClassificationNode>* pClassificationNodeVec = NULL;
					if(clasNode.getParentId() != -3)
					{
						//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 12");
						pClassificationNodeVec = clsCache.getAllChildren(clasNode.getParentId());

						PMString siz("pClassificationNodeVec->size() = ");
							siz.AppendNumber(static_cast<double>(pClassificationNodeVec->size()));
							//CA(siz);

						if(pClassificationNodeVec != NULL && pClassificationNodeVec->size() > 0)
						{
							//CA("pClassificationNodeVec != NULL && pClassificationNodeVec->size() > 0 ");

							pCPubModelVec = new vector<CPubModel>;
							vector<ClassificationNode>::iterator itr = pClassificationNodeVec->begin();
							for(;itr != pClassificationNodeVec->end(); ++itr)
							{
								pCPubModelVec->push_back(itr->getCPubModel());
							}
							
						}
						//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 13");
					}

					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update before setClassNameAndClassID");
					functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name ,isONESource, pCPubModelVec /*,isCheckCount ,itemProductSectionCountPtr*/ );
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update after setClassNameAndClassID");
					if(pCPubModelVec != NULL)				
						delete pCPubModelVec;
					
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 14");
					if(pClassificationNodeVec != NULL)						
						delete pClassificationNodeVec;
					
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 15");
					if(functionResult == kFalse  && isContentSprayerFlag != 3)
					{
						ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 16");
						//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::functionResult == kFalse");			
						break;
					}
					//ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 17");
					//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::   after opening content sprayer");			

				}
				//if(isContentSprayerFlag == 3)/*else*/
				//{
				//	ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 18");
				//	//CA("else isContentSprayerFlag == 3");
				//	InterfacePtr<IOneSourceSearch> ContentSprayerPtr((IOneSourceSearch*)::CreateObject(kSEAOneSourceSearchBoss, IID_IONESOURCESEARCH));
				//	if(!ContentSprayerPtr)
				//	{
				//		//CA("if(!ContentSprayerPtr)");
				//		ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
				//		return ;
				//	}
				//	int32 ID = ClassMediator::clasId;
				//	//PMString name("");
				//	bool16 functionResult;
				//	//name = ClassMediator::hier;
				//	//////22/05/07
				//	//	CharCounter Counter1 = -1;
				//	//	Counter1 = name.IndexOfString(">");
				//	//	if(Counter1 != -1)
				//	//	{
				//	//		name.Remove(0, Counter1 + 1 );

				//	//	}
				//	//	else
				//	//		name = "Select Category";

				//	////22/05/07

				//	ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 19");
				//	if(ContentSprayerPtr != nil)
				//	{
				//		ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 20");
				//		//CA("ContentSprayerPtr != nil");
				//		ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update before setClassNameAndClassID");
				//		functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name);
				//		ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update after setClassNameAndClassID");
				//		if(functionResult == kFalse)
				//		{
				//			ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::functionResult == kFalse");			
				//			break;
				//		}
				//		ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update 21");
				//	}

				//}
                
               
                
			}
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::   111111111111111111111111");
			break;
		}
			
	}

}


