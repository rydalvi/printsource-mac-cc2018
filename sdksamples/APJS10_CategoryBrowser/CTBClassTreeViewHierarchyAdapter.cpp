#include "VCPlugInHeaders.h"
#include "ITreeViewHierarchyAdapter.h"
//#include "IUIDData.h"
#include "CPMUnknown.h"
//#include "CTBID.h"
#include "CTBID.h"
#include "CTBTreeNodeID.h"
#include "ClassTreeModel.h"
#include "IAppFramework.h"
#include "CAlert.h"
#include "ClassificationNode.h"
#include "ClassCache.h"
extern ClassCache clsCache;
#define CA(X) CAlert::InformationAlert(X)
//30/09/06----------------------------------It is an Adapter  in Adapter Pattern ...The plug-in client code�s implementation of ITreeViewHierarchyAdapter-------------------------//
//--------------It  provides the means for navigating through your tree model to TreeView widget-----------------------------------//
//The framework uses the NodeID class as a smart pointer to Uniquely identifying each node. NodeID is a class that holds and deletes pointers to NodeIDClass classes.

class CTBClassTreeViewHierarchyAdapter : 
	public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
	CTBClassTreeViewHierarchyAdapter(IPMUnknown* boss);
	virtual ~CTBClassTreeViewHierarchyAdapter();
	virtual NodeID_rv	GetRootNode() const;
	virtual NodeID_rv	GetParentNode( const NodeID& node ) const;
	virtual int32		GetNumChildren( const NodeID& node ) const;
	virtual NodeID_rv	GetNthChild( const NodeID& node, const int32& nth ) const;
	virtual int32		GetChildIndex( const NodeID& parent, const NodeID& child ) const;
	virtual NodeID_rv	GetGenericNodeID() const;
	virtual bool16  ShouldAddNthChild( const NodeID& node, const int32& nth ) const { return kTrue; }

private:
	ClassTreeModel	fBscTreeModel;
};	

CREATE_PMINTERFACE(CTBClassTreeViewHierarchyAdapter, kCTBClassTreeViewHierarchyAdapterImpl)

CTBClassTreeViewHierarchyAdapter::CTBClassTreeViewHierarchyAdapter(IPMUnknown* boss) : 
	CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
	
}

CTBClassTreeViewHierarchyAdapter::~CTBClassTreeViewHierarchyAdapter()
{

}

//Requests the parent node of a given node. If the given node is a root node, there is no parent for a root node, so return nil.
//Otherwise, depending on the location of the node in your tree model, return its parent.
NodeID_rv	CTBClassTreeViewHierarchyAdapter::GetRootNode() const
{	
	double rootUID = fBscTreeModel.GetRootUID();	
	return CTBTreeNodeID::Create(rootUID);
}

NodeID_rv	CTBClassTreeViewHierarchyAdapter::GetParentNode( const NodeID& node ) const
{
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		//CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return kInvalidNodeID;
	}
	//ptrIAppFramework->LogDebug("CTBClassTreeViewHierarchyAdapter::GetParentNode");
	do 
	{
		TreeNodePtr<CTBTreeNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewHierarchyAdapter::GetParentNode::uidNodeID == nil");		
			break; 
		}


		//int32 uid = uidNodeID->GetNodeId();

		if(/*uid*/ uidNodeID->GetNodeId() == fBscTreeModel.GetRootUID())
		{
			//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewHierarchyAdapter::GetParentNode::uid == fBscTreeModel.GetRootUID");				
			break;
		}
		
		//ASSERT(uid != kInvalidUID);
		if(/*uid*/ uidNodeID->GetNodeId() == 0 )//-1; //kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewHierarchyAdapter::GetParentNode::uid == kInvalidUID");				
			break;
		}
		
		//int32 uidParent = fBscTreeModel.GetParentUID(/*uid*/ uidNodeID->GetNodeId());
		if(/*uidParent*/   fBscTreeModel.GetParentUID(/*uid*/ uidNodeID->GetNodeId()) != -1 /*kInvalidUID*/) 
			return CTBTreeNodeID::Create(/*uidParent*/  fBscTreeModel.GetParentUID(/*uid*/ uidNodeID->GetNodeId()));

	}while(kFalse);
	return kInvalidNodeID;	
}
//Return the number of children given a node that is in your tree model.
int32 CTBClassTreeViewHierarchyAdapter::GetNumChildren( const NodeID& node ) const
{
	int32 retval=0;
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return 0;
		}
		//ptrIAppFramework->LogDebug("CTBClassTreeViewHierarchyAdapter::GetNumChildren");

		//CA("Inside ClassTreeViewHierarchyAdapter   ::  GetNumChildren");
		TreeNodePtr<CTBTreeNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break;
		
		//int32 uid = uidNodeID->GetUID();

		if(uidNodeID->GetNodeId() == 0 )//kInvalidUID) 
			break;

		//---------
		//ClassificationNode classNode;	
		//classNode.setClassId(/*uid*/uidNodeID->GetUID().Get());
		//classNode.setNode_ID(node);
		//clsCache.addNodeID(classNode);
		
		if(/*uid*/uidNodeID->GetNodeId() == fBscTreeModel.GetRootUID()) 
			retval = fBscTreeModel.GetRootCount();
		else 
			retval = fBscTreeModel.GetChildCount(/*uid*/ uidNodeID->GetNodeId());

		//PMString ASD("GetNumChildren : ");
		//ASD.AppendNumber(retval);
		//CA(ASD);
		
	} while(kFalse);
	return retval;
}
//this method passes in the index position of the child node that it is querying.
NodeID_rv	CTBClassTreeViewHierarchyAdapter::GetNthChild( const NodeID& node, const int32& nth) const
{
	/*PMString ASD("ClassTreeViewHierarchyAdapter::GetNthChild : ");
	ASD.AppendNumber(nth);
	CA(ASD);*/

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return kInvalidNodeID;
		}
		//ptrIAppFramework->LogDebug("CTBClassTreeViewHierarchyAdapter::GetNthChild");

	TreeNodePtr<CTBTreeNodeID>	uidNodeID(node);
	if( uidNodeID != nil)
	{	
		double uidChild = 0; //kInvalidUID;

		if(uidNodeID->GetNodeId() == fBscTreeModel.GetRootUID()) 
		{   
			uidChild = fBscTreeModel.GetNthRootChild(nth);
		}
		else 
		{   
			uidChild = fBscTreeModel.GetNthChildUID(uidNodeID->GetNodeId(), nth);			
		}

		if(uidChild != 0 /*kInvalidUID*/){			
			return CTBTreeNodeID::Create(uidChild);
		}
	}
	
	return kInvalidNodeID;	
}

//the index refers to the index position used in the ITreeViewHierarchyAdapter::GetNthChild(), and the range of the index is from 0 to the (number of children-1).
int32 CTBClassTreeViewHierarchyAdapter::GetChildIndex
	(const NodeID& parent, const NodeID& child ) const
{
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return -1;
		}
		//ptrIAppFramework->LogDebug("CTBClassTreeViewHierarchyAdapter::GetChildIndex");

		TreeNodePtr<CTBTreeNodeID>	parentUIDNodeID(parent);
		//ASSERT(parentUIDNodeID);
		if(parentUIDNodeID==nil) 
			break;
		
		TreeNodePtr<CTBTreeNodeID>	childUIDNodeID(child);
		//ASSERT(childUIDNodeID);
		if(childUIDNodeID==nil) 
			break;

		if(parentUIDNodeID->GetNodeId() == 0 /*kInvalidUID*/) 
			break;
		
		if(childUIDNodeID->GetNodeId() == 0 /*kInvalidUID*/) 
			break;

		if(parentUIDNodeID->GetNodeId() == fBscTreeModel.GetRootUID()) 
			return fBscTreeModel.GetIndexForRootChild(childUIDNodeID->GetNodeId());
		else 
			return fBscTreeModel.GetChildIndexFor(parentUIDNodeID->GetNodeId(), childUIDNodeID->GetNodeId());			
	} while(kFalse);
	return (-1);
}


//Return a dummy node that makes a generic node. This method is used primarily for persistence.
//When something is purged and the application framework has to write out a NodeIDClass, it uses the ReadWrite() method on the NodeIDClass. When the application
//framework needs to read it back in, it needs to be able to create instances of that NodeIDClass. It uses ITreeViewHierarchyAdapter::GetGenericNodeID() to create an instance, and then calls
//ReadWrite() on the NodeIDClass to initialize it.

NodeID_rv  CTBClassTreeViewHierarchyAdapter::GetGenericNodeID() const
{
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return CTBTreeNodeID::Create(0 /*kInvalidUID*/);
		}
		//ptrIAppFramework->LogDebug("CTBClassTreeViewHierarchyAdapter::GetGenericNodeID");

	return CTBTreeNodeID::Create(0);//-1; //kInvalidUID);
}

