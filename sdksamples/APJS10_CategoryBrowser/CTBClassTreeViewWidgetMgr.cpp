#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "CTBTreeNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
//#include "CTBID.h"
#include "CTBID.h"
#include "CAlert.h"
#include "ClassTreeModel.h"
#include "IAppFramework.h"
	
//-------
#include "ClassMediator.h"
#include "ClassificationNode.h"
#include "ClassCache.h"
#include "ITreeViewMgr.h"
#include "CTBTreeNodeID.h"
#include "WideString.h"
#include "StringUtils.h"
#include "ILoginHelper.h"

#define CA(Z) CAlert::InformationAlert(Z)
//----------30/09 Here you create the tree view control widgets(widgets for the node in the tree)in your client code and apply node data to the widget-------------------- //
//------There is a partial implementation of ITreeViewWidgetMgr, called CTreeViewWidgetMgr, that takes care of the
//-----widget placement implementation and simplifies the implementation of this interface.

//The framework creates the widget once in ITreeViewWidgetMgr::CreateWidgetForNode(), 
//and then uses it several times by calling ITreeViewWidgetMgr::ApplyNodeIDToWidget(),
//and passing in a different NodeID. In this way, the framework is not continually creating
//and deleting widgets.

int32 node_count  = 0;
NodeID catNode_id;
NodeID pubNodeID ;
extern int32 isContentSprayerFlag ;

class CTBClassTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	CTBClassTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~CTBClassTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;

private:
	PMString getNodeText(const double& uid) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};

CREATE_PMINTERFACE(CTBClassTreeViewWidgetMgr, kCTBClassTreeViewWidgetMgrImpl)

CTBClassTreeViewWidgetMgr::CTBClassTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{
}


//Create the right widget for the NodeID passed in. In this method, create the widget, but do not change the widget data to match the node
IControlView* CTBClassTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{
	//CA("CTBClassTreeViewWidgetMgr::CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kCTBPluginID,//kCTBPluginID, 
							kViewRsrcType, 
							kCTBClassTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	//ASSERT(retval);
	return retval;
}
//Returns a widget id that corresponds to the type of widget given.
WidgetID CTBClassTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{
	return kCTBClassTreePanelNodeWidgetID;
}

//Changing the widget data, such as setting the text of a static text widget on a panel to render the data associated with a node,
bool16 CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{
	//CA("CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget");
	ClassificationNode classNode;	
	ClassCache clsCache;

	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::ptrIAppFramework == nil");
		return kFalse;
	}
	//ptrIAppFramework->LogError("Inside CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget");

	InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		//ASSERT(panelControlData);
		if(panelControlData==nil) {
			
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData == nil");
			return kFalse;
		}

	////IControlView* TreeCtrlView =panelControlData->FindWidget(kCTBClassTreeViewWidgetID);
	////	if(TreeCtrlView==nil)
	////	{
	////		ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::TreeCtrlView == nil");
	////		return kFalse;
	////	}
	////	ClassMediator::ClassTreeCntrlView = TreeCtrlView;
	//////----------
	////InterfacePtr<ITreeViewMgr> treeViewMgr(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
	////if(!treeViewMgr) 
	////{
	////	ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::!treeViewMgr");	
	////	return kFalse;
	////}
	/*InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::ptrLogInHelper is nil");
		return kFalse;
	}*/

	do
	{
		TreeNodePtr<CTBTreeNodeID>  uidNodeIDTemp(node);
		double class_id = uidNodeIDTemp->GetNodeId()/*.Get()*/;

		if(isContentSprayerFlag == 1)
		{
			if(class_id == -3/*node_count == 0*/)
			{
				//CA("if(class_id == -3");
				pubNodeID = node;
			}
		}
		else if(isContentSprayerFlag == 2)
		{
			if(node_count == 0)
			{
				catNode_id = node;
			}
		}
		/*else if(isContentSprayerFlag == 3)
		{
			if(node_count == 0)
				catNode_id = node;
			if(class_id == -3)	
				pubNodeID = node;
		}*/

		/*classNode.setClassId(class_id);
		classNode.setNode_ID(node);
		clsCache.addNodeID(classNode);*/
		node_count++;

		/*InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil) 
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData == nil");
			break;
		}*/
		IControlView*   expanderWidget = panelControlData->FindWidget(kCTBClassTreeNodeExpanderWidgetID);
		//ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::expanderWidget == nil");
			break;
		}
		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		//ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter==nil");
			break;
		}
		ClassTreeModel model;

		if(expanderWidget)
		{
			//CA("expanderWidget");
			//TreeNodePtr<CTBTreeNodeID>  uidNodeIDTemp(node);

			//int result=model.GetNodePathLengthFromRoot(uidNodeIDTemp->GetNodeId());
			
			if(adapter->GetNumChildren(node)<=0)
			{
				/*int32 numOfChild = adapter->GetNumChildren(node);
				PMString abc(" numOfChild : ");
				abc.AppendNumber(numOfChild); 
				CA(abc);*/
				expanderWidget->HideView();	
			}
			else
			{
				//CA("Show Expendar widget");			
				expanderWidget->ShowView();
			}
		}

		//TreeNodePtr<CTBTreeNodeID>  uidNodeID(node);
		//ASSERT(uidNodeID);
		if(/*uidNodeID*/uidNodeIDTemp == nil)
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID==nil");
			break;
		}
		
		IControlView* displayStringView = panelControlData->FindWidget( kCTBClassTreeNodeNameWidgetID );
		//ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView==nil");
			break;
		}

		IControlView* displayPubStringView = panelControlData->FindWidget( kCTBClassTreeNodePUBNameWidgetID );
		//ASSERT(displayPubStringView);
		if(displayPubStringView == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::displayPubStringView==nil");
			break;
		}

		if(model.isPubNode(/*uidNodeID*/uidNodeIDTemp->GetNodeId()))
		{
			//----For Events -----
			//CA("****isPubNode True****");
			displayStringView->HideView();
			displayPubStringView->ShowView();
			
			InterfacePtr<ITextControlData>  textControlData( displayPubStringView, UseDefaultIID() );
			ASSERT(textControlData);
			if(textControlData== nil)
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData==nil");
				break;
			}
			PMString  stringToDisplay( this->getNodeText(/*uidNodeID*/uidNodeIDTemp->GetNodeId()) );



			//stringToDisplay.SetTranslatable( kFalse );
			//ptrIAppFramework->LogDebug("in CB ");
			//ptrIAppFramework->LogDebug(stringToDisplay);

			WideString ws;
			StringUtils::ConvertUTF8ToWideString(stringToDisplay.GetUTF8String () , ws);
			PMString AS(ws);
			AS.SetTranslatable(kFalse);
            AS.ParseForEmbeddedCharacters();
			textControlData->SetString(AS, kTrue, kFalse);
	//CA(stringToDisplay);
			this->indent( node, widget, displayPubStringView );

		}
		else
		{	//----- For ONEsource classes. ---------
			//CA("****isPubNode False****");
			displayStringView->ShowView();
			displayPubStringView->HideView();
			
			InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
			//ASSERT(textControlData);
			if(textControlData== nil)
			{
				//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData==nil");
				break;
			}
			PMString stringToDisplay( this->getNodeText(/*uidNodeID*/uidNodeIDTemp->GetNodeId()) );
			stringToDisplay.SetTranslatable( kFalse );
            stringToDisplay.ParseForEmbeddedCharacters();
			textControlData->SetString(stringToDisplay);
			//CA(stringToDisplay);
			this->indent( node, widget, displayStringView );

		}

	} while(kFalse);
	return kTrue;
}

PMReal CTBClassTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{
	do
	{
		TreeNodePtr<CTBTreeNodeID>  uidNodeID(node);
		//ASSERT(uidNodeID);
		if(uidNodeID == NULL) 
			break;
		
		ClassTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->GetNodeId());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+5.5);
	} while(kFalse);
	return 0.0;
}

PMString CTBClassTreeViewWidgetMgr::getNodeText(const double& uid) const
{	
	ClassTreeModel model;
	return model.ToString(uid);
}

void CTBClassTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}

