//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "CEventHandler.h"
#include "IEvent.h"
//#include "LPTLibraryService.h"
#include "IApplication.h"
//commented by nitin
//#include "IPaletteMgr.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "ILibraryPanelData.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "FileUtils.h"
#include "IControlView.h"
#include "ILibrary.h"
#include "LibraryPanelID.h"
#include "ILibraryPanelData.h"
#include "ILibrary.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"
#include "IPanelControlData.h"
#include "CTBID.h"
#include "CTBActionComponent.h"
#include <IFrameUtils.h>
//#include "IMessageServer.h"

#include "IClientOptions.h"
#include "IAppFramework.h"
#include "ITreeViewController.h"
#include "ClassMediator.h"
#include "UIDNodeID.h"
#include "ClassCache.h"
#include "IContentSprayer.h"
#include "IOneSourceSearch.h"
#include "ITemplatesDialogCloser.h"

#ifdef MACINTOSH
	#include "K2Vector.tpp"
#endif






#define FILENAME			PMString("CTBEventHandler.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


extern int32 currSelLanguageID ;
extern PMString currSelLangName ;
extern int32 currPublicationID;
extern PMString currPublicationName;
extern int32 isContentSprayerFlag ;
extern int32 isCheckCount;
extern ItemProductCountMapPtr itemProductSectionCountPtr;

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class CTBEventHandler : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	CTBEventHandler(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~CTBEventHandler(){}


	/** Left mouse button released.
	@param e [IN] event of interest
	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonDn(IEvent* e);

	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	////////virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
	////////	
	/////////** Window has been deactivated. Traditional response is to
	////////	deactivate the controls in the window.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 Deactivate(IEvent* e) 
	////////{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	////////
	/////////** Application has been suspended. Control is passed to
	////////	another application. 
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	////////
	/////////** Application has been resumed. Control is passed back to the
	////////	application from another application.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
	////////	
	/////////** Mouse has moved outside the sensitive region.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
	////////	 
	/////////** User is holding down the mouse button and dragging the mouse.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
	////////	 
	/////////** Left mouse button (or only mouse button) has been pressed.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/ 
	////////virtual bool16 LButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
	////////	 
	/////////** Right mouse button (or second button) has been pressed.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
	////////	 
	/////////** Middle mouse button of a 3 button mouse has been pressed.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
	////////	
	/////////** Left mouse button released.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
	////////	 
	/////////** Right mouse button released.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
	////////	 
	/////////** Middle mouse button released.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
	////////	 
	/////////** Double click with any button; this is the only event that we're interested in here-
	////////	on this event we load the placegun with an asset if it can be imported.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 ButtonDblClk(IEvent* e);
	/////////** Triple click with any button.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
	////////	 
	/////////** Quadruple click with any button.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
	////////	 
	/////////** Quintuple click with any button.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
	////////	 
	/////////** Event for a particular control. Used only on Windows.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
	////////	
	////////	
	////////// Keyboard Related Events
	////////
	/////////** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
	////////	 
	/////////** Keyboard key down that generates a character.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
	////////	
	/////////** Keyboard key released.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
	////////	 
	////////
	////////// Keyboard Focus Related Functions
	////////
	/////////** Key focus is now passed to the window.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
	////////	
	/////////** Window has lost key focus.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
	////////	
	/////////** Typically called before GiveUpKeyFocus() is called. Return kFalse
	////////	to hold onto the keyboard focus.
	////////	@return kFalse to hold onto the keyboard focus
	////////*/
	////////virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
	////////	 
	/////////** The keyboard is temporarily being taken away. Remember enough state
	////////	to resume where you left off. 
	////////	@return kTrue if you really suspended
	////////	yourself. If you simply gave up the keyboard, return kFalse.
	////////*/
	////////virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
	////////	 
	/////////** The keyboard has been handed back. 
	////////	@return kTrue if you resumed yourself. Otherwise, return kFalse.
	////////*/
	////////virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
	////////	 
	/////////** Determine if this eventhandler can be focus of keyboard event 
	////////	@return kTrue if this eventhandler supports being the focus
	////////	of keyboard event
	////////*/
	////////virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
	////////	 
	/////////** Return kTrue if this event handler wants to get keyboard focus
	////////	while tabbing through widgets. Note: For almost all event handlers
	////////	CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
	////////	If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
	////////	for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
	////////	kFalse then the event handler is skipped.
	////////	@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	////////*/
	////////virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
	////////	 

	/////////** Platform independent menu event
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
	////////	 
	/////////** Window needs to repaint.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
	////////	
	/////////** Method to handle platform specific events
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
	////////	 
	/////////** Call the base system event handler.
	////////	@param e [IN] event of interest
	////////	@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	////////*/
	////////virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
	////////	
	////////	
	/////////** Temporary.
	////////*/
	////////virtual void SetView(IControlView* view)
	////////{  
	////////	InterfacePtr<IEventHandler> 
	////////		delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER); 
	////////	delegate->SetView(view);  
	////////}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( CTBEventHandler, kCTBEventHandlerImpl)

	
CTBEventHandler::CTBEventHandler(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 CTBEventHandler::LButtonDn(IEvent* e)
{
	//CA("Left Click");
	return kFalse;
}


bool16 CTBEventHandler::ButtonDblClk(IEvent* e) 
{
//	//CA("ButtonDblClk");
//	
//	//InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
//	//if (!panelControlData)
//	//	return kFalse;
//
//	//IControlView* treeControlView=panelControlData->FindWidget(kCTBClassTreeViewWidgetID);
//	//if(!treeControlView)
//	//	return kFalse;
//
//	//-------------
//
	bool16 functionResult;
	bool16 isONESource = kFalse ;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
	if(cop == nil)
	{
		//CA("cop == nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
		return kFalse;
	}

	InterfacePtr<ITreeViewController> treeViewCntrl(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
	if(treeViewCntrl==nil) 
	{
		//CA("treeViewCntrl==nil");
		ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBSelectionObserver::Update::treeViewCntrl == nil");			
		return kFalse;
	}

	NodeIDList selectedItem;
	
	treeViewCntrl->GetSelectedItems(selectedItem);
	if(selectedItem.size()<=0)
	{
		//CA("selectedItem.size()<=0");
		ptrIAppFramework->LogInfo("AP7_CategoryBrowser::CTBSelectionObserver::Update::selectedItem.size()<=0");			
		return kFalse;
	}

	NodeID nid=selectedItem[0];
	TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);

	int32 uid= uidNodeIDTemp->GetUID().Get();

	ClassificationNode clasNode;
	ClassCache clsCache;

	bool16 retval = clsCache.isExist(uid, clasNode);
	if(retval == kTrue)
	{
		//CA("retval == kTrue");	
		ClassMediator::clasId = clasNode.getClassId();
		ClassMediator::clasName = clasNode.getClassName();

	}
	
	if(isContentSprayerFlag == 1 || isContentSprayerFlag == 3)
	{
		//CA("isContentSprayerFlag == 1");
		InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
		if(!ContentSprayerPtr)
		{
			ptrIAppFramework->LogDebug("AP46_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
			return kFalse;
		}
		InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
		if(cop == nil)
		{
			//CA("cop == nil");
			ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
			return kFalse;
		}
//			int32 ID = ClassMediator::clasId;
//			/*PMString num;
//			num.AppendNumber(ID);
//			CA("classId" + num);*/
//			PMString name("");
//bool16 functionResult;
//			name = ClassMediator::hier;
//			//Added by nitin
//			////22/06/07
//			CharCounter Counter1 = -1;
//			Counter1 = name.IndexOfString(">");
//			if(Counter1 != -1)
//			{
//				name.Remove(0, Counter1 + 1 );
//
//			}
//			else
//				name = "Select Category";
//
//			////22/05/07
//
		PMString name("");
		int32 ID = ClassMediator::clasId;
		name = ClassMediator::clasName;
		int32 sectionID = -1;
		PMString sectionName = "";

		currPublicationName = name;
		currPublicationID = ID;

		if(clasNode.getIsPUB() == kTrue)	
		{
			//CA("clasNode.getIsPUB() == kTrue");
			isONESource = kFalse ;
			ptrIAppFramework->set_isONEsourceMode(kFalse);

			if(clasNode.getLevel() > 3)	//-----
			{
				 sectionID = ClassMediator::clasId;
				 sectionName = clasNode.getClassName();
				
				CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(ClassMediator::clasId,currSelLanguageID);

				currPublicationID = pubModelRootValue.getRootID();
				currPublicationName = pubModelRootValue.getName();

			}
		}
		else
		{
			isONESource = kTrue ;
			ptrIAppFramework->set_isONEsourceMode(kTrue);
				
			ID = clasNode.getClassId();
			name = ClassMediator::clasName;

			currPublicationName = name;
			currPublicationID = ID;

		}

		//cop->setAll(currPublicationName , currPublicationID, currSelLangName , currSelLanguageID, sectionName,sectionID  );
				
		cop->setPublication(currPublicationName , currPublicationID);
		cop->setLocale(currSelLangName , currSelLanguageID );
		cop->setSection(sectionName,sectionID );

		if(ContentSprayerPtr != nil)
		{
			//CA("ContentSprayerPtr != nil");
			functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name ,isONESource ,NULL,isCheckCount ,itemProductSectionCountPtr );
			if(functionResult == kFalse)
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::functionResult == kFalse");			
				//return kFalse;	//-------
			}
		}
		if(isContentSprayerFlag == 3)/*else*/
		{
			//CA("else");
			InterfacePtr<IOneSourceSearch> ContentSprayerPtr((IOneSourceSearch*)::CreateObject(kSEAOneSourceSearchBoss, IID_IONESOURCESEARCH));
			if(!ContentSprayerPtr)
			{
				//CA("if(!ContentSprayerPtr)");
				ptrIAppFramework->LogDebug("AP46_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
				return kFalse;
			}
			int32 ID = ClassMediator::clasId;
			//PMString name("");
//bool16 functionResult;
			//name = ClassMediator::hier;
			//////22/05/07
			//	CharCounter Counter1 = -1;
			//	Counter1 = name.IndexOfString(">");
			//	if(Counter1 != -1)
			//	{
			//		name.Remove(0, Counter1 + 1 );

			//	}
			//	else
			//		name = "Select Category";

			////22/05/07


			if(ContentSprayerPtr != nil)
			{
				functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name);
				if(functionResult == kFalse)
				{
					ptrIAppFramework->LogError("AP46_CategoryBrowser::CTBClassTreeObserver::Update::functionResult == kFalse");			
					return kFalse;
				}
			}

		}
	}


	//-----------
	LButtonDn(e);

	CTBActionComponent actionComponetObj(this);
	actionComponetObj.ClosePallete(); 

    return kTrue;
}