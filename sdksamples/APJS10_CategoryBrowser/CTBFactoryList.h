//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(CTBActionComponent,				kCTBActionComponentImpl)
REGISTER_PMINTERFACE(CTBClassTreeViewHierarchyAdapter,	kCTBClassTreeViewHierarchyAdapterImpl)
REGISTER_PMINTERFACE(CTBClassTreeObservser,				kCTBClassTreeObserverImpl)
REGISTER_PMINTERFACE(CTBSelectionObserver,				kCTBSelectionObserverImpl)
REGISTER_PMINTERFACE(CTBClassTreeViewWidgetMgr,			kCTBClassTreeViewWidgetMgrImpl)
REGISTER_PMINTERFACE(CCategoryBrowser,					kCTBCategoryBrowserImpl)
//REGISTER_PMINTERFACE(SnipRunControlView_TMP , kSnipCTBRunControlViewImpl )
//REGISTER_PMINTERFACE( CTBEventHandler, kCTBEventHandlerImpl)
REGISTER_PMINTERFACE(CTBLoginEventsHandler,kCTBLoginEventsHandlerImpl)