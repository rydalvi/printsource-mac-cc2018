#ifndef __CTBPlugInEntrypoint_h__
#define __CTBPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class CTBPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fCTBTypeLib;
#endif                    
};

#endif