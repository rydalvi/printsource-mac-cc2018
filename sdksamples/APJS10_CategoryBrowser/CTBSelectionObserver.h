#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "CTBID.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IAppFramework.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
//#include "IClassificationTree.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "IEventHandler.h"

class CTBSelectionObserver : public ActiveSelectionObserver
{
	public:
		CTBSelectionObserver(IPMUnknown *boss);
		virtual ~CTBSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void loadPaletteData();
		
	protected:
		IPanelControlData*	QueryPanelControlData();
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void UpdatePanel();	
};
