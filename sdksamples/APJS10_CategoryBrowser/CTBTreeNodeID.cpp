#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPMStream.h"

// Project includes:
#include "CTBTreeNodeID.h"

/**
	@ingroup paneltreeview
*/

/* Compare
*/
int32 CTBTreeNodeID::Compare(const NodeIDClass* nodeID) const
{
	const CTBTreeNodeID* fileNode = static_cast<const CTBTreeNodeID*>(nodeID);
	//ASSERT(nodeID);
	//return (const_cast<CTBTreeNodeID*>(this)->fPath.Compare(kFalse, fileNode->GetPath()));
	if(fnodeId == fileNode->GetNodeId() )
		return 0;
	else if(fnodeId < fileNode->GetNodeId() )
		return -1;
	else
		return 1;
}


/* Clone
*/
NodeIDClass* CTBTreeNodeID::Clone() const
{
	return new CTBTreeNodeID(fnodeId);
}


/* Read
*/
void CTBTreeNodeID::Read(IPMStream* stream)
{
	//fnodeId.ReadWrite(stream);
	//stream->XferInt32(fnodeId);

}


/* Write
*/
void CTBTreeNodeID::Write(IPMStream* stream) const
{
	//(const_cast<CTBTreeNodeID*>(this)->fnodeId).ReadWrite(stream);
	//stream->XferInt32(fnodeId);
}

//	end, File:	PnlTrvDataNode.cpp
