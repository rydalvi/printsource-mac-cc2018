#ifndef __CLASSCACHE_H__
#define __CLASSCACHE_H__

#include "VCPlugInHeaders.h"
#include "ClassificationNode.h"
#include "map"
#include "vector"

using namespace std;

class ClassCache
{
private:
	static map<double, ClassificationNode>* mpClassCache;
	//static map<int32, ClassificationNode>* mpNodeClassCache;
public:
	ClassCache();
	~ClassCache()
	{		
		
	}

	bool16 isExist(double, ClassificationNode&);
	bool16 isExist(double, int32, ClassificationNode&);
	bool16 add(ClassificationNode&);
	bool16 clearMap(void);

	//bool16 addNodeID(ClassificationNode& );
	//bool16 isNodeExist(int32 , ClassificationNode& );
	//bool16 clearNodeMap(void);

	vector<ClassificationNode>* getAllChildren(double);

	int32 getSize();
};

#endif


