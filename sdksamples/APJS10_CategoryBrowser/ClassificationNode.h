#ifndef __CLASSIFICATIONNODE_H__
#define __CLASSIFICATIONNODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "ClassificationValue.h"
#include "PubModel.h"

#include "NodeID.h"

class ClassificationNode
{
private :
	PMString className;
	double classId;
	double parentId;
	int level;
	int32 seqNumber;
	int32 childCount;
	bool16 isPUB;
	bool16 isEventType;
	CClassificationValue ClassificationValueObj;
	CPubModel	CPubModelObj;
	//-------
	NodeID node_ID ;

public:
	ClassificationNode()
		:className(""), classId(0), parentId(0),  level(0) , seqNumber(0), childCount(0), isPUB(kFalse), isEventType(kFalse) ,ClassificationValueObj(), CPubModelObj(), node_ID(NULL) {}

	void setAll
	(PMString& clasName, double clasId, double parentId, int level,
	 int32 seqNumber, int32 childCount, bool16 isPub ,bool16 isEvntTyp,CClassificationValue ClassValueObj,CPubModel	CPbModelObj , NodeID node_id)
	{
		this->className=clasName;
		this->classId=clasId;
		this->parentId=parentId;
		this->level=level;
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->isPUB=isPub;
		this->isEventType=isEvntTyp;
		this->ClassificationValueObj = ClassValueObj;
		this->CPubModelObj = CPbModelObj;

		//-----
		this->node_ID = node_id;
		
	}

	void setClassificationValue(CClassificationValue ClsValueObj)
	{
		this->ClassificationValueObj=ClsValueObj;
	}

	CClassificationValue getClassificationValue()
	{
		return this->ClassificationValueObj;
	}
	void setCPubModel(CPubModel  CPbModelObj)
	{
		this->CPubModelObj=CPbModelObj;
	}

	CPubModel getCPubModel()
	{
		return this->CPubModelObj;
	}

	void setClassName(PMString& clasName)
	{
		this->className=clasName;
	}

	PMString getClassName()
	{
		return this->className;
	}

	void setClassId(double clasId)
	{
		this->classId=clasId;
	}

	double getClassId()
	{
		return this->classId;
	}

	void setParentId(double parentId) 
	{ 
		this->parentId=parentId; 
	}

	double getParentId(void) 
	{ 
		return this->parentId; 
	}

	void setLevel(int level) 
	{ 
		this->level=level; 
	}

	int getLevel(void) 
	{ 
		return this->level; 
	}

	void setSequence(int32 seqNumber)
	{ 
		this->seqNumber=seqNumber; 
	}

	int32 getSequence(void) 
	{ 
		return this->seqNumber; 
	}

	void setChildCount(int32 childCount) 
	{
		this->childCount=childCount; 
	}

	int32 getChildCount(void) 
	{ 
		return this->childCount; 
	}
	void setIsPUB(bool16 isPub) 
	{
		this->isPUB=isPub; 
	}

	bool16 getIsPUB(void) 
	{ 
		return this->isPUB; 
	}
	void setIsEventType(bool16 isEvntTyp) 
	{
		this->isEventType=isEvntTyp; 
	}

	bool16 getIsEventType(void) 
	{ 
		return this->isEventType; 
	}


	//---------
	void setNode_ID(NodeID nodeid)
	{
		this->node_ID = nodeid;
	}

	NodeID getNode_ID()
	{
		return this->node_ID;
	}

};

#endif