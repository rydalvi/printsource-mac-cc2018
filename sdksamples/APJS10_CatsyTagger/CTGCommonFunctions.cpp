#include "VCPlugInHeaders.h"
#include "K2Vector.tpp"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "SystemUtils.h"
#include "SDKUtilities.h"
#include "IPanelControlData.h"
#include "IApplication.h"
#include "k2smartptr.h"
#include "ILayoutUIUtils.h"
#include "CmdUtils.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "ITextModel.h"
#include "IFrameUtils.h"
#include "IFrameList.h"
#include "IHierarchy.h"
#include "ILayoutTarget.h"
#include "ITransform.h"
#include "ITextFocusManager.h"
#include "IDocument.h"
#include "ISpreadList.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "ISpread.h"
#include "IScrapItem.h"
#include "ITextControlData.h"
#include "IHierarchyCmdData.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "GenericID.h"
#include "CTGCommonFunctions.h"
#include "ISelectionUtils.h"
#include "vector"
#include "k2vector.h"
#include "IGraphicFrameData.h"
#include "IRefPointUIUtils.h"
#include "SelectUtils.h"
#include "ISelectionUtils.h"
#include "IResizeItemsCmdData.h"
#include "IBoundsData.h"
#include "IPageItemUtils.h"
// Interface includes:
#include "IHierarchy.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "IWorkspace.h"
#include "IStyleInfo.h"
#include "IStyleNameTable.h"
#include "ITextAttributes.h"
#include "IBoolData.h"
#include "IIDXMLElement.h"
#include "IXMLreferenceData.h"
#include "IXMLUtils.h"
#include "IXMLTag.h"
#include "IXMLTagList.h"
#include "IXMLStyleToTagMap.h"
#include "IXMLTagToStyleMap.h"
#include "IXMLTagCommands.h"
#include "IXMLElementCommands.h"
#include "IXMLMappingCommands.h"
#include "IXMLAttributeCommands.h"
#include "ILayoutSelectionSuite.h"
#include "TextEditorID.h"

#include "CTGID.h"
#include "MediatorClass.h"
//#include "TPLListboxData.h"
#include "ITextTarget.h"
#include "ITableModel.h"
#include "ITblBscSuite.h"
#include "ISelectionManager.h"
#include "ITableModelList.h"
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITriStateControlData.h"
#include "CTUnicodeTranslator.h"
#include "IMasterSpreadList.h"
#include "IMasterSpread.h"
#include "IMasterSpreadUtils.h"
#include "ITextMiscellanySuite.h"
#include "TextEditorID.h"
#include "ITextSelectionSuite.h"
#include "ITagReader.h"
#include "TGRID.h"
#include "ITextEditSuite.h"
#include "ITableTextContent.h"
#include "ITableSelectionSuite.h"
#include "ITableTextSelection.h"
#include "IActiveContext.h"
#include "ITextStoryThreadDict.h"
#include <UIDRef.h>
#include <IFrameContentFacade.h>
#include "IHierarchy.h"

//#include "IoverlapProperties.h"
#include "IMasterOverrideList.h"
//Testing whether the pageItem lies on the master or not 1Aub
#include "Utils.h"
#include "ILayoutUtils.h"
#include "CAlert.h"
#include "IAppFramework.h"
#include "ILayoutControlData.h"
#include "IPageItemTypeUtils.h"
#include "ITextFrameColumn.h"
//#include "TPLManipulateInline.h"

#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "PublicationNode.h"
#include "TagStruct.h"
#include "WideString.h"

#define DRAGMODE		1
#define CONTENTSMODE	2

#include "CAlert.h"
//#include "IMessageServer.h"
//Alert Macros.////////////
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("CTGCommonFunctions.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////
/*
extern bool16 ISTabbedText;
extern bool16 isAddTableHeader;
extern int32 SelectedRowNo;
extern bool16 isAddListName;

// added by Tushar on 21_12_06 
static double globalParentTypeId = -1; //this variable is added for event price. 
										//this variable is set to -1 normally otherwise set to type id when the stencil is of 'event price type' in tag table function. 

extern bool16 isComponentAttr;
extern bool16 isXRefAttr;
extern bool16 isAccessoryAttr;
extern bool16 isMMYAttr;
extern bool16 isMMYSortAttr;

extern bool16 isAddAsDisplayName;
extern bool16 isAddImageDescription;
extern bool16 deleteIfEmpty;
extern bool16 isHorizontalFlow;
extern bool16 isOutputAsSwatch;

extern bool16 isSpreadBasedLetterKeys;


int32 indexForBaseNumber = 0;

int32 textInsertIndex=0;
extern double table_TypeID ;
 
 */

//namespace 
//{
//	PMString prepareTagName(PMString name)
//	{
//		PMString tagName("");
//		for(int i=0;i<name.NumUTF16TextChars(); i++)
//		{
//			if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
//				continue;
//			if(name.GetChar(i) ==' '){
//				tagName.Append("_");
//			}
//			else tagName.Append(name.GetChar(i));
//		}
//		return tagName;
//	}
//}
//
//PMString keepOnlyAlphaNumeric(PMString name);
//
//PMString keepOnlyAlphaNumeric(PMString name)
//{
//	PMString tagName("");
//
//	for(int i=0;i<name.NumUTF16TextChars(); i++)
//	{
//		bool isAlphaNumeric = false ;
//
//		PlatformChar ch = name.GetChar(i);
//
//		if(ch.IsAlpha() || ch.IsNumber())
//			isAlphaNumeric = true ;
//
//		if(ch.IsSpace())
//		{
//			isAlphaNumeric = true ;
//			ch.Set('_') ;
//		}
//
//		if(ch == '_')
//			isAlphaNumeric = true ;
//	
//		if(isAlphaNumeric) 
//			tagName.Append(ch);
//	}
//
//	return tagName ;
//}

//int16 CTGCommonFunctions::deleteThisBox(UIDRef boxUIDRef)
//{
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework == nil");
//		return 0;
//	}
//	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
//	if(scrap==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::deleteThisBox::scrap==nil");
//		return 0;
//	}
//	InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
//	if(!command)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::deleteThisBox::!command");
//		return 0;
//	}
//	command->SetItemList(UIDList(boxUIDRef));
//	if(CmdUtils::ProcessCommand(command)!=kSuccess)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::deleteThisBox::CmdUtils::ProcessCommand(command)!=kSuccess");
//		return 0;
//	}
//	return 1;
//}

//===============================================================================
//int16 CTGCommonFunctions::convertBoxToTextBox(UIDRef boxUIDRef)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework == nil");
//		return 0;
//	}
//	if(TPLMediatorClass::imageFlag==kFalse)
//	{
//		InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
//		if(!command){
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToText::command is nil");
//			return 0;
//		}
//		command->SetItemList(UIDList(boxUIDRef));
//		if(CmdUtils::ProcessCommand(command)!=kSuccess)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToTextBox::processcommand not success in convertBoxToTextBox Function");		
//			return 0;}
//	}
//	 if(TPLMediatorClass::imageFlag==kTrue)
//	 {
//		 if(isAddAsDisplayName)
//		 {
//			InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
//			if(!command)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToText::command is nil");
//				return 0;
//			}
//			command->SetItemList(UIDList(boxUIDRef));
//			if(CmdUtils::ProcessCommand(command)!=kSuccess)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToTextBox::processcommand not success in convertBoxToTextBox Function");		
//				return 0;
//			 }
//			return 1;
//		 }
//		 if(isAddImageDescription)
//		 {
//			InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
//			if(!command)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToText::command is nil");
//				return 0;
//			}
//			command->SetItemList(UIDList(boxUIDRef));
//			if(CmdUtils::ProcessCommand(command)!=kSuccess)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::convertBoxToTextBox::processcommand not success in convertBoxToTextBox Function");		
//				return 0;
//			}
//			return 1;
//		}
//
//		 ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::converBoxToTextBox:: imageFlag is kTrue");
//		return 0;
//	 }
//	return 1;
//}



//==================================================================================








//===============================================================================
//Method Purpose: Replace the white spaces with '_' 
//PMString CTGCommonFunctions::RemoveWhiteSpace(PMString name)
//{
//	PMString tagName("");
//	for(int i=0;i<name.NumUTF16TextChars();i++)
//	{
//		if(name.GetChar(i) ==' '){
//			tagName.Append("_");
//		}
//		else tagName.Append(name.GetChar(i));
//	}
//	return tagName;
//}


//================================================================================


//================================================================================
//// Overload of  attachAttributes ......... written for User Defind Tables.
//void CTGCommonFunctions::attachAttributes
//(XMLReference* newTag,bool16 IsPrintSourceTag, TPLListboxInfo lstboxInfo,double rowno, double colno)
//{
//	//event price related addition...added by Tushar on 28/12/06
//	if((lstboxInfo.id == -701 || lstboxInfo.id == -702 /*|| lstboxInfo.id == -703 || lstboxInfo.id == -704*/) && (globalParentTypeId == -1))
//	{
//		globalParentTypeId = lstboxInfo.typeId;
//		lstboxInfo.typeId = -1;
//	}
//	//event price related addition
//
//	bool16 flag=IsPrintSourceTag;	
//	
//	PMString attribName("ID");
//	PMString attribVal("");
//	attribVal.AppendNumber(PMReal(lstboxInfo.id));
//	if(flag){
//		attribVal="-1";
//	} 
//	ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "typeId";
//	if(isSpreadBasedLetterKeys)
//	{
//		attribVal.AppendNumber(1);
//	}
//	else
//	{
//		attribVal.AppendNumber(PMReal(lstboxInfo.typeId));
//	}
//	if(flag){
//		//CA("flag True");
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "header";
//	attribVal.AppendNumber(lstboxInfo.header);
//	if(lstboxInfo.tableFlag == 1 && isAddTableHeader)
//		attribVal = "1";
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "isEventField";
//	if(lstboxInfo.isEventField)
//		attribVal.AppendNumber(1);
//	else
//		attribVal.AppendNumber(-1);
//	
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "deleteIfEmpty";
//	if(deleteIfEmpty && lstboxInfo.tableFlag != 1 && lstboxInfo.isImageFlag != kTrue)
//		attribVal.AppendNumber(1);
//	else
//		attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "dataType";
//	attribVal.AppendNumber(lstboxInfo.dataType);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "isAutoResize";
//	if(this->getAutoResizeChkboxState())
//		attribVal.AppendNumber(1);
//	else if(this->getOverflowChkboxState())
//		attribVal.AppendNumber(2);
//	else
//		attribVal.AppendNumber(0);
//	
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "LanguageID";	
//	if(lstboxInfo.id != -1 && lstboxInfo.tableFlag == 1)
//	{
//		attribVal.AppendNumber(PMReal(lstboxInfo.LanguageID));
//	}
//	else
//		attribVal.AppendNumber(PMReal(lstboxInfo.LanguageID));
//
//	if(flag){
//		attribVal="NULL";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "index";
//    attribVal.AppendNumber( lstboxInfo.listBoxType);
//	
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "pbObjectId";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "parentID";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "childId";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "sectionID";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "parentTypeID";
//	attribVal.AppendNumber(globalParentTypeId);  
//
//	globalParentTypeId = -1;
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "isSprayItemPerFrame";
//	attribVal.AppendNumber(lstboxInfo.isSprayItemPerFrame);
//    
//    if(lstboxInfo.isImageFlag == 1 && isOutputAsSwatch && lstboxInfo.id != -1) // only set for PV image
//    {
//        attribVal.Clear();
//        attribVal.AppendNumber(3);
//    }
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "catLevel";
//	if(lstboxInfo.listBoxType == 5) 
//	{			
//		if(lstboxInfo.code == "")		
//			attribVal.AppendNumber(-1);
//		else 
//			attribVal.Append(lstboxInfo.code);
//	}
//	else
//		attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "imgFlag";
//	attribVal.AppendNumber(lstboxInfo.isImageFlag);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "imageIndex";
//	attribVal.AppendNumber(lstboxInfo.imageIndex);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "flowDir";
//	attribVal.AppendNumber(lstboxInfo.flowDir);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "childTag";
//	if(ISTabbedText)
//		attribVal.AppendNumber(1);
//	else
//		attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "tableFlag";
//	attribVal.AppendNumber(lstboxInfo.tableFlag);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "tableType";
//	attribVal.AppendNumber(lstboxInfo.tableType);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "tableId";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "rowno";
//	//CA("Attaching tag to row ");
//	if(isComponentAttr && (lstboxInfo.listBoxType == 4))
//		attribVal.AppendNumber(-901); /// For Component Table Attribute
//	else if(isXRefAttr && (lstboxInfo.listBoxType ==4))
//		attribVal.AppendNumber(-902); /// For XRef Table Attribute
//	else if(isAccessoryAttr && (lstboxInfo.listBoxType ==4))
//		attribVal.AppendNumber(-903); /// For Accessory Table Attribute
//	else if(isMMYAttr && (lstboxInfo.listBoxType ==4))
//		attribVal.AppendNumber(-904); /// For MMY Table Attribute
//	//else if(deleteIfEmpty && lstboxInfo.tableFlag != 1 && lstboxInfo.isImageFlag != kTrue /*&& !(lstboxInfo.id == -701 || lstboxInfo.id == -702 || lstboxInfo.id == -703 || lstboxInfo.id == -704)*/)
//	//	attribVal.AppendNumber(-117); /// For delete if unsprayed or delete if empty
//	else
//		attribVal.AppendNumber(rowno);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "colno";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
//
//	//-----------
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::ptrIAppFramework == nil");
//		return ;
//	}
//
//	IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
//	if(iItemListCntrlView==nil) 
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::iItemListCntrlView == nil");
//		return ;
//	}
//	InterfacePtr<ITriStateControlData> itristatecontroldata(iItemListCntrlView, UseDefaultIID());
//	if(itristatecontroldata==nil) 
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::itristatecontroldata == nil");			
//		return ;
//	}
//
//	if(itristatecontroldata->IsSelected())
//	{
//		
//		IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
//		if(tableListdropdownCtrlView==nil) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::tableListdropdownCtrlView == nil");
//			return ;
//		}
//		
//		InterfacePtr<IDropDownListController> tableListDropDownController(tableListdropdownCtrlView, UseDefaultIID());
//		if (tableListDropDownController == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::tableListDropDownController == nil");				
//			return ;		
//		}
//
//		InterfacePtr<IStringListControlData> tablelistDropDownData(tableListDropDownController, UseDefaultIID());
//		if (tablelistDropDownData == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::attachAttributes::tablelistDropDownData invalid");
//			return ;
//		}
//		int32 index = tableListDropDownController->GetSelected(); 
//		PMString tableName = tablelistDropDownData->GetString(index);
//		if(tableName == "All")
//			table_TypeID = -1;
//
//	}
//	else
//		table_TypeID = -1;
//
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "field1";
//	if(lstboxInfo.tableFlag == 1)	//------
//	{	
//		attribVal.AppendNumber(-1);
//	}
//	else
//		attribVal.AppendNumber(table_TypeID/*-1*/);
//	
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "field2";
//	if(isMMYSortAttr && (lstboxInfo.listBoxType ==4))
//		attribVal.AppendNumber(1);
//	else
//		attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "field3";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "field4";
//	attribVal.AppendNumber(-1);
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//	attribName.Clear();
//	attribVal.Clear();
//	attribName = "field5";
//	attribVal.AppendNumber(PMReal(lstboxInfo.field5));
//	if(flag){
//		attribVal="-1";
//	}
//	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//    
//    attribName.Clear();
//    attribVal.Clear();
//    attribName = "groupKey";
//    attribVal.Append("");
//    
//    if(lstboxInfo.dataType == 6)
//    {
//        attribVal = lstboxInfo.code;
//    }
//    if(flag){
//        attribVal="";
//    }
//    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
//
//}



//================================================================================
// Method Pupose: Use to attach XML tags to graphic frames.
//void CTGCommonFunctions::addTagToGraphicFrame
//(UIDRef curBox,int32 selIndex,int32 selTab)
//{
//	//CA("CTGCommonFunctions::addTagToGraphicFrame");
//	do{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil){
//			//CA("ptrIAppFramework is nil");		
//			break;
//		}
//
//		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
//		if(unknown == nil){
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToGraphicFrame::unknown nil");
//			break;
//		}
//		InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
//		if(doc == nil){
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToGraphicFrame::Document not found...");
//			break;
//		}
//		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//		if (rootElement == nil){
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToGraphicFrame::IIDXMLElement NIL...");
//			break;
//		}
//		TPLListboxData lstboxData;
//		TPLListboxInfo lstboxInfo;
//		lstboxInfo=lstboxData.getData(selTab, selIndex);
//		XMLReference parent = rootElement->GetXMLReference();
//		if(isAddImageDescription)
//		{//CA("if(isAddImageDescription)");
//			if(lstboxInfo.isImageFlag == kTrue )
//			{
//				//lstboxInfo.id = -124;
//				lstboxInfo.dataType = 2;
//			}
//		}
//        if(isOutputAsSwatch)
//		{//CA("if(isOutputAsSwatch)");
//			if(lstboxInfo.isImageFlag == kTrue )
//			{
//				lstboxInfo.isSprayItemPerFrame= 3;
//			}
//		}
//		PMString tagString = this->prepareTagName(lstboxInfo.name);
//		PMString storyTagName =	this->RemoveWhiteSpace(tagString);
//		
//		XMLReference storyXMLRef = this->TagFrameElement(parent, curBox.GetUID(),storyTagName);
//		if(storyXMLRef == kInvalidXMLReference)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToGraphicFrame::Fail to set Graphic Frame Tag ...");
//			break;
//		}
//		
//		// Following stmt actually attaches the tag.
//		this->attachAttributes(&storyXMLRef,selIndex,selTab);
//	}while(kFalse);
//}




//================================================================================
//int16 CTGCommonFunctions::appendTextIntoBox                                    
//(UIDRef boxUIDRef, PMString textToInsert, bool16 prompt)
//{
//	UIDRef ref;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//    
//	UID textFrameUID = kInvalidUID;
//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//	if (graphicFrameDataOne) 
//	{
//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
//	}
//	if(textFrameUID == kInvalidUID)
//	{
//		//ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTextIntoBox::textFrameUID is kInvalidUID");
//		return 0;
//	}
//		
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//
//	if(textFrameUID == kInvalidUID)
//	{	
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTextIntoBox::!layoutSelectionSuite");
//				return 0;
//			}
//		UIDList ItemList(boxUIDRef);
//
//		if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
//			// Clear the selection
//			iSelectionManager->DeselectAll(nil);
//		}
//
//        layoutSelectionSuite->SelectPageItems(ItemList,Selection::kReplace,  Selection::kDontScrollLayoutSelection );
//		this->convertBoxToTextBox(boxUIDRef);
//	
//	}
//
//	int32 start = 0;
//	
//	IActiveContext* ac = GetExecutionContextSession()->GetActiveContext();
//	
//	InterfacePtr<ITextMiscellanySuite>txtSelectionSuite((const IPMUnknown*)ac->GetContextSelection(),UseDefaultIID());
//	if(!txtSelectionSuite)
//	{	
//		appendTextIntoSelectedAndOverlappedBox(boxUIDRef,textToInsert);
//		return 1;
//	}
//
//	txtSelectionSuite->GetCaretPosition(start);
//	textToInsert.SetTranslatable(kFalse);
//
//	const PMString tabElement("\r", PMString::kEncodingASCII);
//
//	if(textToInsert.Compare(kTrue,tabElement)==0)	//Added For All Standard Stencil
//		start = textInsertIndex;
//	
//////////////////////////////////////////////////////////////FROM HERE
////following code added to store carat position into mediator class variable.
//
//	int32 CaratPosition = 0;
//
//	txtSelectionSuite->GetCaretPosition(CaratPosition);
//	TPLMediatorClass::initialCaratPosition = CaratPosition;
//			
///////////////////////////////////////////////////////////////UPTO HERE
//
//
//	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//	if (graphicFrameHierarchy == nil) 
//	{
//		//CA("graphicFrameHierarchy is NULL");
//		return 0;
//	}
//					
//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//	if (!multiColumnItemHierarchy) {
//		//CA("multiColumnItemHierarchy is NULL");
//		return 0;
//	}
//
//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//	if (!multiColumnItemTextFrame) {
//		//CA("multiColumnItemTextFrame is NULL");
//		return 0;
//	}
//	InterfacePtr<IHierarchy>
//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//	if (!frameItemHierarchy) {
//		//CA("frameItemHierarchy is NULL");
//		return 0;
//	}
//
//	InterfacePtr<ITextFrameColumn>
//	frameItemTFC(frameItemHierarchy, UseDefaultIID());
//	if (!frameItemTFC) {
//		//CA("!!!ITextFrameColumn");
//		return 0;
//	}
//
//	InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//	if(!textModel)
//	{
//		//CA("!textModel" );
//		return 0;
//	}
//
//    textToInsert.ParseForEmbeddedCharacters();
//	WideString* myText=new WideString(textToInsert);
//
//		if(TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox == 0)
//		{			
//			if(TPLMediatorClass::overLapingStatus == 0)
//			{		
//				if(TPLMediatorClass::isCaratInsideTable == 1)
//				{	
//					start = 0;
//					textModel->Insert(start,myText);
//				}				
//				else if(TPLMediatorClass::isInsideTable == kFalse)
//				{	
//						PMString hardreturn("\r", PMString::kEncodingASCII);
//						PMString baseNumber("[Base Number]", PMString::kEncodingASCII);
//						if(textToInsert.Compare(kTrue,hardreturn)==0 || (indexForBaseNumber == textInsertIndex/*28*/ && textToInsert.Compare(kTrue,baseNumber)==0))
//						{
//							if(indexForBaseNumber == textInsertIndex)
//							{
//								start = textInsertIndex;
//								TPLMediatorClass::initialCaratPosition = start;
//							}
//							textModel->Insert(start,myText);
//						}
//						else
//						{
//							start = 0;
//							textModel->Insert(start,myText);
//						}
//				}								
//				else
//				{	
//					txtSelectionSuite->GetCaretPosition(start);
//					textModel->Insert(start,myText);
//				}
//			}
//
//			
//			else if(TPLMediatorClass::overLapingStatus == 1 )
//			{			
//				if( TPLMediatorClass::checkForOverlapWithTextFrame == kFalse)
//				{	
//					if(TPLMediatorClass::checkForOverlapWithMasterframe == kTrue)
//					{	
//						start = 0;
//						textModel->Insert(start,myText);
//					}
//					
//					else if(TPLMediatorClass::checkForOverlapWithMasterframe == kFalse)
//					{
//						CA("Please select the frame before Drag.");
//						return 0;
//					}
//				}				
//				else
//				{	
//					start = TPLMediatorClass::initialCaratPosition ;
//					textModel->Insert(start,myText);
//				}
//
//			}
//		}
//
//		else if(TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox == 1)
//		{	
//			start = TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion ;
//			textModel->Insert(start,myText);
//			
//			TPLMediatorClass::initialCaratPosition = start;
//		}
//
//	if(myText)
//		delete myText;
//	return 1;
//}


//================================================================================
//Method Purpose: Embedd one box into other.

//int16 CTGCommonFunctions::embedBoxIntoBox(UIDRef toEmbed, UIDRef bigBox)
//{    
//	int flag=1;
//	PMRect tmpRect;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//	InterfacePtr<IGeometry> iGeometry(bigBox, UseDefaultIID());
//	if(iGeometry==nil)
//		flag=0;
//	else
//		tmpRect = iGeometry->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeometry)); 
//
//	UID textFrameUID = kInvalidUID;
//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(bigBox, UseDefaultIID());
//	if (graphicFrameDataOne) 
//	{
//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
//	}
//	if(textFrameUID == kInvalidUID)
//		this->convertBoxToTextBox(toEmbed);
//	
//	InterfacePtr<IHierarchy> iChild(toEmbed, IID_IHIERARCHY);
//	if(iChild==nil){
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox:: iChild is nil");	
//		return 0;   
//	}
//
//	UIDList itemList(toEmbed);
//   
//	InterfacePtr<ICommand> iRemoveCmd(CmdUtils::CreateCommand(kRemoveFromHierarchyCmdBoss));
//	if(!iRemoveCmd)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::!iRemoveCmd");	
//		return 0;
//	}
//    
//	iRemoveCmd->SetItemList(itemList.GetRef(0));
//	int32 retval1=CmdUtils::ProcessCommand(iRemoveCmd);
//	if(retval1!=kSuccess)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::retval1!=kSuccess");	
//		return 0;
//	}
//
//	/////////////[Adding our page item to new hierarchy]////////////
//	
//	InterfacePtr<ICommand> iMoveCmd(CmdUtils::CreateCommand(kAddToHierarchyCmdBoss));
//	if(!iMoveCmd)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::!iMoveCmd");	
//		return 0;
//	}
//      
//	InterfacePtr<IHierarchyCmdData> iHierarchyCmdData(iMoveCmd, IID_IHIERARCHYCMDDATA);
//	if(!iHierarchyCmdData)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::iHierarchyCmdData is null");	
//		return 0;
//	}
//  
//	iHierarchyCmdData->SetParent(bigBox);
//   
//	K2Vector<int32> children;	
//	children.push_back(0);
//	iHierarchyCmdData->SetIndexInParent(children); 
//	
//	iMoveCmd->SetItemList(UIDList(itemList.GetRef(0)));
//	if(CmdUtils::ProcessCommand(iMoveCmd)!=kSuccess)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::!kSuccess");	
//		return 0;
//	}
//    
//	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
//	if(fntDoc==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::fntDoc is nil");	
//		return -1;
//	}
//	PreDirty(fntDoc);
//
//	if(flag)
//	{
//		
//		InterfacePtr<IGeometry> iGeo(toEmbed, UseDefaultIID());
//		if(iGeo==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedBoxIntoBox::iGeo is nil");		
//			return 1;
//		}
//		iGeo->SetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo), tmpRect);
//	    
//	  }
//	return 1;
//
//}

//================================================================================

//PBPMPoint CTGCommonFunctions::GetActiveReferencePoint()
//{
//	PBPMPoint refPoint(0,0);
//	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//	if(layoutData)
//	{
//        refPoint = Utils<IRefPointUIUtils>()->GetPasteboardReferencePoint(layoutData);
//	
//	}
//	return refPoint;
//}




//================================================================================
//Method Putpose: Get the selected box UIDRef from the current document.
//int16 CTGCommonFunctions::getSelectionFromLayout(UIDRef& selectedBox)
//{
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");	
//		return 0;
//	}
//
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//
//	if(!iSelectionManager)
//	{	
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getSelectionFromLayout::!iSelectionManager");
//		return 0;
//	}
//	
//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//	
//	if(!txtMisSuite)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getSelectionFromLayout::!txtMisSuite");		
//		return 0; 
//	}
//
//	UIDList	selectUIDList;
//	txtMisSuite->GetUidList(selectUIDList);
//	
//	const int32 listLength=selectUIDList.Length();
//	
//	if(listLength==0)
//	{
//		return 0;
//	}
//	
//	selectedBox=selectUIDList.GetRef(0);
//	return 1;
//}

// Method Purpose: depending upon the 'choice' variable inserts 'theContent' text/ embed the box into current mouse pointed text frame.
// choice: if Radio button 'Assign to Current Frame' is selected then just appends the text content in the end of text in text frame
//if Radio button 'Create New Frame' is selected then embeds the new frame in the current mouse pointed frame.

//The text which we want to insert or append into frame is passeed in the second 
//argument(i.e. theContent)


//================================================================================
//void CTGCommonFunctions::insertOrAppend
//(int32 selectedRowIndex, PMString theContent, int32 choice, int32 whichTab)
//{
//
//	indexForBaseNumber=0;	//Added For All Standard TAbles Stencil
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");	
//		return;
//	}
//	
//	TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox = 0;
//	TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = 0;
//
//	bool16 successFlag=kTrue;
//	UIDRef overlapBoxUIDRef;
//	UIDList newList;
//	UIDRef selBoxUIDRef;
//
//	do
//	{
//		// Checking if the frame is tagged
//		bool16 isFrameTagged=kFalse;
//		//isFrameTagged=this->checkIfFrameIsTagged();
//		if(isFrameTagged==kTrue)
//		{
//			this->getSelectionFromLayout(selBoxUIDRef);
//			deleteThisBox(selBoxUIDRef);
//			return;
//		}
//		
//        //Here in following code we are appending square brackets (at begining and at end)
//        //to text which is to be inserted or appended into frame.
//
//		PMString tempStr("[");
//		tempStr.Append(theContent);
//		if(isComponentAttr && (whichTab == 4))
//			tempStr.Append("_Component");
//		else if(isXRefAttr && (whichTab == 4))
//			tempStr.Append("_Xref"); /// For XRef Table Attribute
//		else if(isAccessoryAttr && (whichTab == 4))
//			tempStr.Append("_Accessory"); /// For Accessory Table Attribute		
//		else if(isMMYAttr && (whichTab == 4))
//			tempStr.Append("_MMY"); /// For Accessory Table Attribute		
//		
//		if(isAddAsDisplayName	||	theContent.Compare(kTrue,"All Standard Tables") == 0 )
//		{
//			if(theContent.Compare(kTrue,"All Standard Tables") == 0)
//			{
//				tempStr.Clear();
//				tempStr.Append("[Standard Table");
//			}
//			tempStr.Append("_DsplName");
//		}
//		if(isAddImageDescription)
//			tempStr.Append("_Description");
//
//		tempStr.Append("]");
//		theContent.Clear();
//		theContent.Append(tempStr);
//
//
//
//        //Inside if condition we are calling getSelectionFromLayout we are passing selBoxUIDRef
//        //by reference as a out parameter.This function will return UIDRef of frame which is
//        //currently selected.
//		if(!this->getSelectionFromLayout(selBoxUIDRef))
//		{
//			successFlag=kFalse;
//			break;
//		}
//
//		//Start compound undo
//		//seq=CmdUtils::BeginCommandSequence();		
//		if(TPLMediatorClass::imageFlag!=kTrue)
//		{	
//			this->convertBoxToTextBox(selBoxUIDRef);
//            //following function appends text into the box
//		}
//		
//		//CA("convertBoxToTextBox");
//		if(TPLMediatorClass::imageFlag==kTrue && (isAddAsDisplayName == kTrue || isAddImageDescription == kTrue))
//		{
//			this->convertBoxToTextBox(selBoxUIDRef);
//            //following function appends text into the box
//		}
//		//bool16 collisionFlag=kTrue;
//
////Inside if condition we are calling checkForOverLaps ,if newly draged object is on the
////existing frame then it will return 1, else it will return 0.
////We are passing UIDRef of currently selected frame(selBoxUIDRef--which is newly 
////created frame) and  two out parameters  overlapBoxUIDRef(which is UIDRef reference) 
////and newList(which is UIDList reference). If checkForOverLaps function returns 1 ,
////we get UIDRef of frame which is overlaped by newly created frame. In the third
//// parameter(overlapBoxUIDRef)----------------added by vijay. 	
//		
//		if(!this->checkForOverLaps(selBoxUIDRef, 0, overlapBoxUIDRef, newList))
//		{
//			//CA("checkForOverLaps");	
////Program flow will come inside 'if' block if frame is created for the first
////time(it means it is not overlaped).
////Here we are calling addTagToText function. In this function we attach
////XML Attributes to the added Tag
//			TPLMediatorClass::overLapingStatus = 0;
//			this->appendTextIntoBox(selBoxUIDRef, theContent, kTrue);
//
//			//This is a global function ,It will replace blank spaces with '_'		
//			::prepareTagName(theContent);
//			
//			this->addTagToText(selBoxUIDRef, selectedRowIndex, whichTab,theContent);
//
//			if(theContent.Compare(kTrue,"[Standard Table_DsplName]")==0)	/////	This is to add enter after All Standard Tables_DsplName in case of All Standard Table Stencil in tabbed text form
//			{
//				
//				if(!isAddAsDisplayName)
//				{
//                    //	CA("Match");
//					textInsertIndex=0;
//					textInsertIndex = theContent.CharCount()+2;
//					PMString hardreturn("\r", PMString::kEncodingASCII);
//					this->appendTextIntoBox(selBoxUIDRef, hardreturn, kTrue);
//					textInsertIndex++;
//					indexForBaseNumber = textInsertIndex;// 28; 
//					theContent.Clear();
//					theContent.Append("[Base Number]");					
//					this->appendTextIntoBox(selBoxUIDRef,theContent, kTrue);
//					::prepareTagName(theContent);
//					this->addTagToText(selBoxUIDRef, 0, 4,theContent);
//					indexForBaseNumber=0;
//				}
//			}			
//
//			return;
//		}
//		else
//		{	
//			TPLMediatorClass::overLapingStatus = 1;
//		
//			int result;
//			InterfacePtr<IPMUnknown> unknown(overlapBoxUIDRef ,IID_IUNKNOWN);
//			InterfacePtr<IHierarchy> iChildHier(unknown,UseDefaultIID());
//			if(iChildHier == nil)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::insertOrAppend::iChildHier == nil");
//				return ;
//			}		
//			IDataBase * database = overlapBoxUIDRef.GetDataBase();
//			UID pageUID = Utils<ILayoutUtils>()->GetOwnerPageUID(iChildHier);
//
//			TPLMediatorClass::checkForOverlapWithMasterframe = Utils<ILayoutUtils>()->IsAMaster(pageUID,database);
//
//			if(TPLMediatorClass::checkForOverlapWithMasterframe)
//			{
//				bool16 IsOverlapOnExistingFrame = this->checkForOverlapWithMasterFrameFunction(overlapBoxUIDRef);
//				
//				if(IsOverlapOnExistingFrame == kTrue)
//				{
//					//ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGCommonFunctions::insertOrAppend::IsOverlapOnExistingFrame == kTrue");
//				}
//				else if(IsOverlapOnExistingFrame == kFalse)
//				{
//					TPLMediatorClass::overLapingStatus = 1;
//					result=this->appendTextIntoBox(selBoxUIDRef, theContent, kTrue);
//					if(result==0)
//					{
//						successFlag=kFalse;
//					}
//					else if(result!=-1)
//					{	
//						this->addTagToText(selBoxUIDRef, selectedRowIndex, whichTab,theContent);
//			
//					}
//				}	
//			}
//			else
//			{
//				TPLMediatorClass::overLapingStatus = 1;
//				//In following line of code we are deleting the newly created frame				
//				this->deleteThisBox(selBoxUIDRef);
//				result=this->appendTextIntoBox(overlapBoxUIDRef, theContent, kTrue);
//				if(result==0)
//				{
//					successFlag=kFalse;
//				}
//				else if(result!=-1)
//				{	
//					this->addTagToText(overlapBoxUIDRef, selectedRowIndex, whichTab,theContent);
//		
//				}
//				
//			}
//		}
//		TPLMediatorClass::checkForOverlapWithMasterframe = kFalse;
//
//
//	}while(kFalse);
//
//}





//================================================================================
//int16 CTGCommonFunctions::getBoxDimensions(UIDRef theBox, PMRect& bind)
//{
//	PMRect theArea;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//
//	InterfacePtr<IGeometry> iGeometry(theBox, UseDefaultIID());
//	if(iGeometry==nil)
//	{	
//		//ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getBoxDimensions::iGeometry is nil");
//		return kFalse;
//	}
//
//	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//	bind=theArea;
//	return kTrue;
//}


//================================================================================
//Method Purpose: Checks whether the dragged box is overlapping over the existing box in the document
//int16 CTGCommonFunctions::checkForOverLaps
//(UIDRef draggedItem, int32 spreadNumber, UIDRef& overlappingItem, UIDList& theList)
//{	
//	TPLListboxInfo lstboxInfo;
//	TPLListboxData lstboxData;
//	lstboxInfo=lstboxData.getData(SelectedRowNo, TPLMediatorClass::curSelRowIndex);
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return -1;
//	}
//	//CA("Inside checkForOverLaps");
//	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
//	if(fntDoc==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::fntDoc is nil");	
//		return -1;
//	}	
//	
//	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//	if (layout == nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::layout is nil");
//		return -1;
//	}
//
//	IDataBase* database = ::GetDataBase(fntDoc);
//	if(database==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::database is nil");	
//		return -1;
//	}
//
//	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
//	if (iSpreadList==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::iSpreadList is nil");
//		return -1;
//	}
//
//
//
//	bool16 collisionFlag=kFalse;
//	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
//	{
//		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
//
//		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
//		if(!spread)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::!ispread");		
//			return -1;
//		}
//
//		UIDList allPageItems(database);
//		int numPages=spread->GetNumPages();
//
//		for(int i=0; i<numPages; i++)
//		{					
//			UIDList tempList(database);
//			spread->GetItemsOnPage(i, &tempList, kFalse);
//			allPageItems.Append(tempList);			
//		}
//
//		PMRect theArea, dragItemArea;
//		PMRect smallestItem(0,0,0,0);
//		if(!this->getBoxDimensions(draggedItem, dragItemArea))
//		{	
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::returns kFalse");
//			return 0;
//		}
//
//		PMReal centerX, centerY;
//		centerX=(dragItemArea.Bottom()+dragItemArea.Top())/2;
//		centerY=(dragItemArea.Right()+dragItemArea.Left())/2;
//
//		for(int32 i=0; i<allPageItems.Length(); i++)
//		{			
//						
//			if(allPageItems.GetRef(i)==draggedItem)
//			{
//				continue;
//			}
//
//			if(this->getBoxDimensions(allPageItems.GetRef(i), theArea)<1)
//			{
//				continue;
//			}
//			//This box may have some kids
//			InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
//			if(iChild)
//			{
//				UID kidUID;
//				int numKids=iChild->GetChildCount();
//				
//				for(int j=0; j<numKids-1; j++)
//				{
//					kidUID=iChild->GetChildUID(j);
//					allPageItems.Append(kidUID);
//				}
//			}
//			if(centerX>=theArea.Top() && centerY>=theArea.Left() && centerX<=theArea.Bottom() && centerY<=theArea.Right())
//			{
//				//CA("Colision Found");
//				//if(!collisionFlag)
//				if(ISTabbedText == kFalse && lstboxInfo.tableFlag == 1)
//				{
//					ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::ISTabbedText == kFalse && lstboxInfo.tableFlag == 1");
//					/*collisionFlag = kFalse;
//					break;		*/
//					collisionFlag = kTrue;
//					continue;
//				}
//
//
//				if(TPLMediatorClass::imageFlag == kTrue)
//				{	
//					ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLaps::TPLMediatorClass::imageFlag == kTrue");
//					collisionFlag = kFalse;
//					break;
//				}
//				else
//				{	//CA("Colision Found 111");
//					collisionFlag=kTrue;
//					smallestItem=theArea;
//					overlappingItem=allPageItems.GetRef(i);
//					theList=allPageItems;
//					continue;
//				}
//			}
//		}
//	}
//
//	if(collisionFlag)
//	{
//		return 1;		
//	}
//	
//	return 0;
//}

//int16 CTGCommonFunctions::refreshLstbox(UID boxToSkip)
//{
///* Resetting isDragged flag for all the listboxes entries */
//	for(int a=1; a<=5; a++)
//	{
//		TPLListboxData lstboxData;
//		int32 lstSize=0;
//		lstSize = lstboxData.returnListVectorSize(a);
//		for(int w=0; w<lstSize ; w++)
//		{
//			lstboxData.setIsBoxDragged(a, w, kFalse);
//		}
//	}
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return -1;
//	}
//
///* Getting all the page items on the current document */
//	do
//	{
//		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
//		if(fntDoc==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::fntDoc is nil");		
//			break;
//		}
//
//		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layout == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::layout is nil");		
//			break;
//		}
//
//		IDataBase* database = ::GetDataBase(fntDoc);
//		if(database==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::database is nil");		
//			break;
//		}
//
//		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
//		if (iSpreadList==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstBox::iSpreadList is nil");		
//			break;
//		}
//
//	/* storing all the page items into allPageItems */
//		UIDList allPageItems(database);
//
//	/* Iterating thru all the spreads of the current document */
//		for(int x=0; x<iSpreadList->GetSpreadCount(); x++)
//		{
//			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(x));
//
//			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
//			if(!spread)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::!ispread");			
//				break;
//			}
//
//	/* iterating thru all the pages of the spread under consideration */
//			int numPages=spread->GetNumPages();
//			if(numPages<=0)
//			{
//				break;
//			}
//
//			for(int m=0; m<numPages; m++)
//			{
//				UIDList tempList(database);
//				spread->GetItemsOnPage(m, &tempList, kFalse);
//				allPageItems.Append(tempList);
//			}
//	/* Checking for the child boxes also */
//			for(int i=0; i<allPageItems.Length(); i++)
//			{
//				InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
//				if(iChild)
//				{
//					UID kidUID;
//					int numKids=iChild->GetChildCount();
//					for(int j=0; j<numKids; j++)
//					{
//						kidUID=iChild->GetChildUID(j);
//						allPageItems.Append(kidUID);
//					}
//				}
//			}
//		}
//
//		// For Master Spread getting UIDs
//		InterfacePtr<IMasterSpreadList>	masterSpreadList
//			(database , database ->GetRootUID(), UseDefaultIID());
//		if(masterSpreadList==nil)
//		{
//					
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::masterSpreadList is nil");
//			return -1;
//		}
//
//		for(int masterSpNum=0; masterSpNum< masterSpreadList->GetMasterSpreadCount(); masterSpNum++)
//		{
//			UID masterSpUID=masterSpreadList->GetNthMasterSpreadUID(masterSpNum);
//			InterfacePtr<ISpread> masterSpd(UIDRef(database,masterSpUID),UseDefaultIID());
//			if(!masterSpd)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::!masterSpd");			
//				return -1;
//			}
//
//			UIDList masterPageItems(database);
//
//			for(int32 numPgs=0;numPgs<masterSpd->GetNumPages();numPgs++)
//			{
//				UIDList tempList(database);
//				masterSpd->GetItemsOnPage(numPgs, &tempList, kFalse);
//				allPageItems.Append(tempList);
//			}
//
//			for(int32 i=0; i<allPageItems.Length(); i++)
//			{
//				//This box may have some kids
//				InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
//				if(iChild)
//				{
//					UID kidUID;
//					int numKids=iChild->GetChildCount();
//					
//					for(int j=0; j<numKids-1; j++)
//					{
//						kidUID=iChild->GetChildUID(j);
//						allPageItems.Append(kidUID);
//					}
//				}
//			}
//		}
//
//
//	}while(kFalse);
//
//	if(TPLMediatorClass::PFLstboxCntrlView==nil
//		||TPLMediatorClass::PGLstboxCntrlView==nil
//		||TPLMediatorClass::PRLstboxCntrlView==nil
//		||TPLMediatorClass::ItemLstboxCntrlView==nil
//		||TPLMediatorClass::ProjectLstboxCntrlView==nil)
//			
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::refreshLstbox::****CntrlView == nil");	
//		return -1;
//	}
//
//	for(int i=1;i<=5;i++)
//	{
//		TPLListboxData lstboxData;
//		int32 lstSize=0;
//		lstSize = lstboxData.returnListVectorSize(i);
//
//		for(int y=0; y<lstSize ; y++)
//		{
//			TPLListboxData lstboxData;
//			TPLListboxInfo lstboxInfo;
//			lstboxInfo=lstboxData.getData(i, y);
//
//			switch(i)
//			{
//			case 1:
//				if(lstboxInfo.isDragged)
//					this->updateIcon
//						(TPLMediatorClass::PFLstboxCntrlView, y, kTrue);
//				else
//					this->updateIcon
//						(TPLMediatorClass::PFLstboxCntrlView, y, kFalse);
//				break;
//
//			case 2:
//				if(lstboxInfo.isDragged)
//					this->updateIcon
//						(TPLMediatorClass::PGLstboxCntrlView, y, kTrue);
//				else
//					this->updateIcon
//						(TPLMediatorClass::PGLstboxCntrlView, y, kFalse);
//				break;
//
//			case 3:
//				if(lstboxInfo.isDragged)
//					this->updateIcon
//						(TPLMediatorClass::PRLstboxCntrlView, y, kTrue);
//				else
//					this->updateIcon
//						(TPLMediatorClass::PRLstboxCntrlView, y, kFalse);
//				break;
//
//			case 4:
//				if(lstboxInfo.isDragged)
//					this->updateIcon
//						(TPLMediatorClass::ItemLstboxCntrlView, y, kTrue);
//				else
//					this->updateIcon
//						(TPLMediatorClass::ItemLstboxCntrlView, y, kFalse);
//				break;
//			case 5:
//				if(lstboxInfo.isDragged)
//					this->updateIcon
//						(TPLMediatorClass::ProjectLstboxCntrlView, y, kTrue);
//				else
//					this->updateIcon
//						(TPLMediatorClass::ProjectLstboxCntrlView, y, kFalse);
//				break;
//			}
//		}
//	}
//	return 0;
//}


//================================================================================
//void CTGCommonFunctions::updateIcon
//(IControlView* lstcntrlview, int32 index, bool16 checked)
//{
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil){
//			//CA("ptrIAppFramework is nil");		
//			break;
//		}
//
//		
//		InterfacePtr<IPanelControlData> panelData(lstcntrlview, UseDefaultIID());
//		if (panelData == nil) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::panelData == nil");		
//			break;
//		}
//		
//		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
//		if(cellControlView == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::cellControlView == nil");		
//			break;
//		}
//
//		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
//		if(cellPanelData == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::cellPanelData == nil");		
//			break;
//		}
//		
//		IControlView* nameTextView = cellPanelData->GetWidget(index);
//		if ( nameTextView == nil ) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::nameTextView == nil");		
//			break;
//		}
//
//		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
//		if(cellPanelDataChild == nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::cellPanelDataChild == nil");		
//			break;
//		}
//
//		int toHidden=0;
//		int toShow=0;
//		
//		if(checked)
//		{
//			toHidden=0;
//			toShow=1;
//		}
//		else
//		{
//			toHidden=1;
//			toShow=0;
//		}
//		
//		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
//		if(childHideView==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::childHideView == nil");
//			break;
//		}
//
//		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
//		if(childShowView==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::updateIcon::childShowView == nil");
//			break;
//		}
//
//		childHideView->HideView();
//		childShowView->ShowView();	
//	}while(kFalse);
//}


//================================================================================
//Method Purpose: Creates a table inside a selected text frame
//int16 CTGCommonFunctions::createNewTable(void)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//	int16 flag=1;
//	do
//	{
//        
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{
//			flag=0;
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::createNewTable""!iSelectionMAnager");
//			break;
//		}
//
//
//		InterfacePtr<ITblBscSuite> tblBscSuite(static_cast<ITblBscSuite* >
//			( Utils<ISelectionUtils>()->QuerySuite(ITblBscSuite::kDefaultIID,iSelectionManager))); 
//		if (tblBscSuite == nil)
//		{	
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::createNewTable::tblBscSuite == nil");
//			flag=0;
//			break;
//		}
//		if (tblBscSuite->CanInsertTable() == kFalse)
//		{	
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::createNewTable::tblBscSuite->CanInsertTable() == kFalse");
//			flag=0;
//			break;
//		}
//		int32 numRows = 2;
//		int32 numCols = 2;
//		
//		ErrorCode status = tblBscSuite->InsertTable(numRows, numCols);
//		if(status != kSuccess) 
//		{	
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::createNewTable::status != kSuccess");
//			flag=0;
//			break;
//		}
//
//	} while(kFalse);
//	return flag;
//}


//================================================================================
// Method Purpose: Change the Mode to Content Mode or Drag Mode
//int CTGCommonFunctions::changeMode(int whichMode)
//{
//	int status=kFailure;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//	if(whichMode==DRAGMODE)
//	{
//		InterfacePtr<ICommand> setToolCmd(CmdUtils::CreateCommand(kSetToolCmdBoss));
//		if(!setToolCmd)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!setToolCmd");		
//			return kFailure;
//		}
//
//		InterfacePtr<IToolCmdData> setToolCmdData(setToolCmd, UseDefaultIID());
//		if(!setToolCmdData)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!setToolCmdData");		
//			return kFailure;
//		}
//
//		InterfacePtr<ITool> textTool(this->queryTool(kPointerToolBoss));
//		if(!textTool)
//		{	
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!textTool");		
//			return kFailure;
//		}
//
//		setToolCmdData->Set(textTool, textTool->GetToolType());
//		status=CmdUtils::ProcessCommand(setToolCmd);
//	}
//	else if(whichMode==CONTENTSMODE)
//	{
//		InterfacePtr<ICommand> setToolCmd(CmdUtils::CreateCommand(kSetToolCmdBoss));
//		if(!setToolCmd)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!setToolCmd");		
//			return kFailure;
//		}
//
//		InterfacePtr<IToolCmdData> setToolCmdData(setToolCmd, UseDefaultIID());
//		if(!setToolCmdData)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!setToolCmdData");		
//			return kFailure;
//		}
//
//		InterfacePtr<ITool> textTool(this->queryTool(kIBeamToolBoss));
//		if(!textTool)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::changeMode::!textTool");
//			return kFailure;
//		}
//
//		setToolCmdData->Set(textTool, textTool->GetToolType());
//		status=CmdUtils::ProcessCommand(setToolCmd);
//	}
//	return 1;
//}


//================================================================================
//Method Purpose: returns the pointer to ITool interface
//ITool* CTGCommonFunctions::queryTool(const ClassID& toolClass)	
//{
//	ITool *tool = nil;
//	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
//	ASSERT(app);
//	InterfacePtr<IToolManager> toolMgr(app->QueryToolManager());
//	ASSERT(toolMgr);
//	if(toolMgr)
//	{
//		for(int32 i = 0; i < toolMgr->GetNumTools(); i++)
//		{
//			ToolRecord rec = toolMgr->GetNthTool(i);
//			InterfacePtr<ITool> tool(rec.QueryTool());
//			ASSERT(tool);
//			if(::GetClass(tool) == toolClass)
//			{
//				return rec.QueryTool();
//			}
//		}
//		for(int32 j = 0; j < toolMgr->GetNumSubTools(); j++)
//		{
//			ToolRecord rec = toolMgr->GetNthSubTool(j);
//			InterfacePtr<ITool> tool(rec.QueryTool());
//			ASSERT(tool);
//			if(::GetClass(tool) == toolClass)
//			{
//				return rec.QueryTool();
//			}
//		}
//	}				
//	return tool;
//}


//================================================================================
//Method Purpose: append new table in the given text frame
//int16 CTGCommonFunctions::appendNewTable(UIDRef selBoxUIDRef)
//{
//	int16 flag=1;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//	do
//	{
//		int16 retVal=0;
//
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(selBoxUIDRef, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		if(textFrameUID==kInvalidUID)
//		{	
//			PMString message("The box you are trying to append data is not a text box. Would you like to convert it to a text box? The data in the box may be lost.");
//			PMString okBtn("OK");
//			PMString cancelBtn("Cancel");
//			int answer=1;
//			
//			answer=CAlert::ModalAlert(message, okBtn, cancelBtn, "", 2, CAlert::eQuestionIcon);		
//
//			if(answer==1)
//			{
//				this->convertBoxToTextBox(selBoxUIDRef);
//				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(selBoxUIDRef, UseDefaultIID());
//				if (graphicFrameDataOne) 
//				{
//					textFrameUID = graphicFrameDataOne->GetTextContentUID();
//				}
//				if(textFrameUID == kInvalidUID)
//				{
//					return 0;
//				}
//			}
//			else
//				return -1;//special return value
//		}
//
//			InterfacePtr<IHierarchy> graphicFrameHierarchy(selBoxUIDRef, UseDefaultIID());
//			if (graphicFrameHierarchy == nil) 
//			{
//				//CA("graphicFrameHierarchy is NULL");
//				flag=0;
//				break;
//			}
//							
//			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//			if (!multiColumnItemHierarchy) {
//				//CA("multiColumnItemHierarchy is NULL");
//				flag=0;
//				break;
//			}
//
//			InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//			if (!multiColumnItemTextFrame) {
//				//CA("multiColumnItemTextFrame is NULL");
//				flag=0;
//				break;
//			}
//			InterfacePtr<IHierarchy>
//			frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//			if (!frameItemHierarchy) {
//				//CA("frameItemHierarchy is NULL");
//				flag=0;
//				break;
//			}
//
//			InterfacePtr<ITextFrameColumn>
//			frameItemTFC(frameItemHierarchy, UseDefaultIID());
//			if (!frameItemTFC) {
//				//CA("!!ITextFrameColumn");
//				flag=0;
//				break;
//			}
//
//			InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//			if(!textModel)
//			{
//				//CA("!textModel" );
//				flag=0;
//				break;
//			}
//
//		this->changeMode(CONTENTSMODE);
//
//		TextIndex startIndex = frameItemTFC->TextStart();
//		TextIndex finishIndex =0;
//
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
//			// Clear the selection
//			iSelectionManager->DeselectAll(nil);
//		}
//		
//		if(frameItemTFC->TextSpan() > 0)
//		{
//			TextIndex finishIndex = frameItemTFC->TextSpan()-1;
//
//			RangeData range(startIndex , finishIndex,nil);
//
//			
//			if(iSelectionManager) 
//			{ 
//				InterfacePtr<ITextSelectionSuite> textSelectionSuite(iSelectionManager, UseDefaultIID()); 
//				if (textSelectionSuite) 
//				{ 
//					textSelectionSuite->SetTextSelection( 
//					::GetUIDRef(textModel), 
//					range, 
//					Selection::kScrollIntoView, nil);
//				} 
//			}
//		}
//		else
//		{
//			if(iSelectionManager) 
//			{ 
//				InterfacePtr<ITextSelectionSuite> textSelectionSuite(iSelectionManager, UseDefaultIID()); 
//				if (textSelectionSuite) 
//				{ 
//					textSelectionSuite->SetTextSelection( 
//					::GetUIDRef(textModel), 
//					RangeData(0, RangeData::kLeanForward), 
//					Selection::kScrollIntoView, nil);
//				} 
//			}
//
//		}
//
//		if(!ISTabbedText)
//		{
//			retVal=this->createNewTable();
//			if(!retVal)
//			{
//				ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendNewTable::Error in creating new table");
//				flag=0;
//				break;
//			}
//		}
//
//		this->changeMode(DRAGMODE);
//	
//	}while(kFalse);
//		return flag;
//	
//		
//	
//}


//================================================================================
//Method Purpose: Embeds or appeds the Table inside text frame depending on selected Radio button:
// Append to the current frame/ Create new frame.
//int16 CTGCommonFunctions::embedOrAppendTable(int16 choice,int32 selIndex,int32 selTab)
//{
//	//CA("CTGCommonFunctions::embedOrAppendTable");
//	UIDRef overlapBoxUIDRef;
//	UIDList newList;
//	UIDRef selBoxUIDRef;
//	int16 retVal=0;
//	int16 flag;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return 0;
//	}
//
//	this->addTagToGraphicFrame(selBoxUIDRef,selIndex,selTab);
//	bool16 isFrameTagged=kFalse;
//
//	isFrameTagged=this->checkIfFrameIsTagged();
//	
//	if(isFrameTagged==kTrue)
//	{
//		this->getSelectionFromLayout(selBoxUIDRef);
//		deleteThisBox(selBoxUIDRef);
//		return 0;
//	}
//	retVal=this->getSelectionFromLayout(selBoxUIDRef);
//	if(!retVal){
//		ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::embedOrAppendNewTable::!retVal");	
//		return 0;
//	}
//	
//	this->convertBoxToTextBox(selBoxUIDRef);
//	this->appendNewTable(selBoxUIDRef);
//	retVal=this->checkForOverLaps(selBoxUIDRef, 0, overlapBoxUIDRef, newList);
//	if(!retVal)
//	{
//		this->addTagToGraphicFrame(selBoxUIDRef,selIndex,selTab);
//		return 0;
//
//	}
//	switch(choice)
//	{
//		case 1: // Append Table
//			if(this->appendNewTable(overlapBoxUIDRef)==-1)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::embedOrAppendTable::kFalse");			
//				break;
//			}
//			this->deleteThisBox(selBoxUIDRef);
//			break;
//
//		case 2: // Embed Table
//			if((this->embedBoxIntoBox(selBoxUIDRef,overlapBoxUIDRef))==-1)
//            {
//
//                retVal=this->createNewTable();
//                if(!retVal)
//                {
//                    flag=0;
//                    break;
//                }
//            }
//            
//            retVal=this->createNewTable();
//            if(!retVal)
//            {
//                // 	CA("Error in creating new table");
//                flag=0;
//                break;
//            }
//            break;
//	}
//
//	this->addTagToGraphicFrame(selBoxUIDRef,selIndex,selTab);
//	CTGCommonFunctions::refreshLstbox(kInvalidUID);
//	return 1;
//}


//===================================================================================
//int16 CTGCommonFunctions::getTableUIDOfSelBox(UIDRef &selBoxUIDRef)
//{
///*	do
//	{
//		InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
//		if(layoutData==nil)
//			return 0;
//		InterfacePtr<ISelection> selection(layoutData->QuerySelection());
//		if (selection == nil)
//			return 0;
//		InterfacePtr<ITextTarget> textTarget(static_cast<ITextTarget*>
//			(selection->QueryInterface(ITextTarget::kDefaultIID)));
//		if (textTarget == nil)
//			return 0;
//		InterfacePtr<ITableTarget> tableTarget(textTarget, UseDefaultIID());
//		if(tableTarget == nil) 
//			return 0;
//		InterfacePtr<ITableModel> tableModel(tableTarget->QueryModel());
//		if(tableModel == nil) 
//			return 0;
//		InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel());
//		if(textModel == nil) 
//			return 0;
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==nil) 
//			return 0;
//		int32	tableIndex = tableList->GetModelCount() - 1;
//
//		InterfacePtr<ITableModel> table(tableList->QueryNthModel(tableIndex));
//		if(table == nil) 
//			return 0;
//		const UIDRef tableRef(::GetUIDRef(table));
//		selBoxUIDRef=tableRef;
//	}while(kFalse);*/
//	return 1;
//	
//}


//Method Purpose: Get the status of 'Frame Tag' check box onpallete 

//==================================================================================
//bool16 CTGCommonFunctions::getFrameTagChkboxState()
//{
//	bool16 chkboxState=kFalse;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//	IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
//	if(iChkboxCntrlView==nil) 
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getFrameTagChkBoxState::iChkboxCntrlView is nil");	
//		return chkboxState;	
//	}
//	
//	if(iChkboxCntrlView->IsEnabled()){
//		InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
//		if(itristatecontroldata==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getFrameTagChkboxState::!itristatecontroldata is nil");		
//			return chkboxState;
//		}
//		if(itristatecontroldata->IsSelected()){
//			chkboxState=kTrue;
//		}
//		else{
//			chkboxState=kFalse;
//		}
//	}
//	else{
//		chkboxState=kFalse;
//	}
//	return chkboxState;
//}


//==================================================================================
//Method Purpose: Get the status of 'Auto Resize' check box onpallete 
//bool16 CTGCommonFunctions::getAutoResizeChkboxState()  /// Added for auto resize chkbox
//{
//	bool16 chkboxState=kFalse;
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//	IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAutoResizeWidgetID);
//	if(iChkboxCntrlView==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getAutoResizeChkbosState::iChkboxCntrlView is nil");	
//		return chkboxState;	
//	}
//
//	if(iChkboxCntrlView->IsEnabled()){
//		InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
//		if(itristatecontroldata==nil)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::getAutoResizeChkboxState::itristatecontroldata is nil");		
//			return chkboxState;
//		}
//		if(itristatecontroldata->IsSelected()){
//			chkboxState=kTrue;
//		}
//		else{
//			chkboxState=kFalse;
//		}
//	}
//	else{
//		chkboxState=kFalse;
//	}
//	return chkboxState;
//}



//=================================================================================
//bool16 CTGCommonFunctions::checkIfFrameIsTagged()
//{
//	bool16 isFrameTaggedFlag=kFalse;
//	UIDRef overlapBoxUIDRef;
//	UIDList newList;
//	UIDRef selBoxUIDRef;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//
//	do{
//		if(!this->getSelectionFromLayout(selBoxUIDRef))
//		{
//		break;}
//		if(!this->checkForOverLaps(selBoxUIDRef,0,overlapBoxUIDRef,newList))
//		{
//			break;
//		}
//		
//		InterfacePtr<ITagReader> iTagReaderPtr
//				(static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,ITagReader::kDefaultIID)));
//		if(!iTagReaderPtr)
//			break;
//		TagList tagList=iTagReaderPtr->getFrameTags(overlapBoxUIDRef);
//		if(tagList.size()==0)
//		{
//			// the overlapped box is not our dragged box
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkIfFrameIsTagged::tagList's size is 0");
//			break;
//		}
//		if( tagList[0].elementId==-1 &&
//			tagList[0].parentId==-1 &&
//			tagList[0].parentTypeID==-1 &&
//			tagList[0].imgFlag==-1 &&
//			tagList[0].sectionID==-1 &&
//			tagList[0].typeId==-1 &&
//			tagList[0].whichTab==-1)
//			break;
//		else{
//			
//			  isFrameTaggedFlag=kTrue;
//		}
//		for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
//		{
//			tagList[tagIndex].tagPtr->Release();
//		}
//	}while(kFalse);
//	return isFrameTaggedFlag;
//}






////Method Purpose: Use to tag text inside the Cell of a Table.
//ErrorCode CTGCommonFunctions::TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, TPLListboxInfo& ListboxInfoData, int32 startTextIndex)
// {
//		ErrorCode errCode=kFailure; 
//	 	
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			//CA("ptrIAppFramework is nil");		
//			return errCode;
//		}
//		UIDRef TableUIDRef(::GetUIDRef(tableModel)); 
//		InterfacePtr<ITableTextContent>tblTextContent(tableModel,UseDefaultIID());
//		if(!tblTextContent)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source ITableTextContent in function is null");
//			return kFailure ;
//		}
//
//		InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel());
//		if(!tblTextModel)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source Text Model in fucntion is null");
//			return kFailure;
//		}
//		UIDRef ref2(::GetUIDRef(tblTextModel));
//
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{	
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Selection Manager is Null ");
//				return kFailure;
//		}
//		iSelectionManager->DeselectAll(nil);
//
//		InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));
//		if(!tblSelSuite)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText:: Source Table Selection suite is null");
//			return kFailure;
//		}
//		
//		tblSelSuite->DeselectAll();
//
//		UIDRef tableTextModelUIDRef(::GetUIDRef(tblTextModel));
//	
//		PMString NewTagName;
//		if( ( SelectedRowNo == 4) && (isComponentAttr) )
//			NewTagName = ListboxInfoData.name + "_Component" ;
//		else if(isXRefAttr && (SelectedRowNo == 4))
//			NewTagName = ListboxInfoData.name + "_Xref" ; /// For XRef Table Attribute
//		else if(isAccessoryAttr && (SelectedRowNo == 4))
//			NewTagName = ListboxInfoData.name + "_Accessory" ; /// For Accessory Table Attribute	
//		else if(isMMYAttr && (SelectedRowNo == 4))
//			NewTagName = ListboxInfoData.name + "_MMY" ; /// For Accessory Table Attribute	
//		else
//			NewTagName = ListboxInfoData.name ;
//
//		if(isAddAsDisplayName)
//			NewTagName.Append("_DsplName");
//
//		if(isAddImageDescription)
//			NewTagName.Append("_Description");
//		
//		PMString cellTagName =  NewTagName;
//		
//		int32 endTextIndex;
//		if(ListboxInfoData.isImageFlag == kTrue) // for Image in table Cell
//		{
//			endTextIndex =  startTextIndex + 1 ;
//		}
//		else 
//			endTextIndex =  startTextIndex + cellTagName.NumUTF16TextChars();
//
//		cellTagName = this->prepareTagName( NewTagName);
//
//		errCode=Utils<IXMLElementCommands>()->CreateElement(WideString(cellTagName) ,tableTextModelUIDRef,startTextIndex,endTextIndex,kInvalidXMLReference,&CellTextxmlRef);
//		 return errCode;
// }




//void CTGCommonFunctions::insertOrAppendOnDoubleClick(int32 selectedRowIndex,PMString theContent,int32 whichTab)
//{	
//	bool16 successFlag=kTrue;
//	UIDRef overlapBoxUIDRef;
//	UIDList newList;
//	UIDRef selBoxUIDRef;
//	
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//	if(!iSelectionManager)
//	{	//CA("APJS10_CatsyTagger::CTGCommonFunctions::insertOrAppendOnDoubleClick::!iSelectionManager");
//		return ;
//	}
//
//	InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
//	
//	if(!txtSelectionSuite)
//	{
//		//CA("Please Select a Type Tool");
//	}
//
//	bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(selBoxUIDRef);
//	if(ISTextFrame == kFalse){
//		//CA("ISTextFrame is return false");
//	}
//	do
//	{	
//		// Checking if the frame is tagged
//		bool16 isFrameTagged=kFalse;
//		
//		if(isFrameTagged==kTrue)
//		{
//			//CAlert::ErrorAlert("Cannot Append or Embed into Tagged Frame");
//			this->getSelectionFromLayout(selBoxUIDRef);
//			deleteThisBox(selBoxUIDRef);
//			return;
//		}		
//		PMString tempStr("[");
//		tempStr.Append(theContent);
//		tempStr.Append("]");
//		theContent.Clear();
//		theContent.Append(tempStr);
//		if(TPLMediatorClass::imageFlag!=kTrue)
//		{
//			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//			InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
//            theContent.ParseForEmbeddedCharacters();
//			WideString str(theContent);				
//			textEditSuite->InsertText(str);
//			
//		}
//
//		this->addTagToTextDoubleClickVersion(selBoxUIDRef, selectedRowIndex, TPLMediatorClass::curSelLstbox, theContent );
//        return;
//		
//	}while(kFalse);
//}


///////////////////////////////////////////////////////////////////////////
//Following function is modified to provide Drag-Drop support
// Method Purpose: Adds xml tag to the selected Text  
//void CTGCommonFunctions::addTagToText
//(UIDRef curBox,int32 selIndex,int32 selTab,PMString result)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return ;
//	}
//	
//	do
//	{
//		InterfacePtr<IPMUnknown> unknown(curBox,IID_IUNKNOWN);		
//		
//		int16 isTextFrame=Utils<IFrameUtils>()->IsTextFrame(unknown);
//		if(isTextFrame==0)
//		{
//			//CA("isTextFrame==0...");
//			this->addTagToGraphicFrame(curBox,selIndex,selTab);
//			return;
//		}
//
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		
//		if(textFrameUID==kInvalidUID)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToText::textFrameUID is kInvalidUID");
//			return;
//		}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			return;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			return;
//		}
//
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!!ITextFrameColumn");
//			return;
//		}
//
//		InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
//		if(!txtModel)
//		{
//			//CA("!txtModel" );
//			return;
//		}
//
//		TPLListboxData lstboxData;
//		TPLListboxInfo lstboxInfo;
//		lstboxInfo=lstboxData.getData(selTab, selIndex);
//		
//		IActiveContext* ac = GetExecutionContextSession()->GetActiveContext();
//		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite((const IPMUnknown*)ac->GetContextSelection(),UseDefaultIID());
//		
//		int32 start=0;
//		int32 end;
//
//		if(TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox == 0)
//		{			
//			if(TPLMediatorClass::overLapingStatus == 0)
//			{		
//				if(TPLMediatorClass::isCaratInsideTable == 1)
//				{
//					start = 0;					
//					end=start+result.NumUTF16TextChars() ;	
//				}
//				
//				else if(TPLMediatorClass::isInsideTable == kFalse)
//				{		
//					start = 0;
//					if(indexForBaseNumber == textInsertIndex)
//						start = textInsertIndex;
//					end=start+result.NumUTF16TextChars() ;
//				}
//				else
//				{
//					txtSelectionSuite->GetCaretPosition(start);
//					end=start+result.NumUTF16TextChars() ;
//				}
//			}
//
//			else if(TPLMediatorClass::overLapingStatus == 1 )
//			{				
//				if( TPLMediatorClass::checkForOverlapWithTextFrame == kFalse)
//				{	
//					if(TPLMediatorClass::checkForOverlapWithMasterframe == kTrue)
//					{	
//						start = 0;					
//						end=start+result.NumUTF16TextChars() ;	 
//					}
//				}
//				else 
//				{
//					start = TPLMediatorClass::initialCaratPosition ;
//					end= TPLMediatorClass::initialCaratPosition + result.NumUTF16TextChars() ;
//				}	
//			}
//		}
//
//		else if(TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox == 1)
//		{
//			start = TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion ;
//			end=start+result.NumUTF16TextChars() ;
//
//			TPLMediatorClass::initialCaratPosition = start;
//		}
//
//		// Look for a text selection. Bail if we can't find one.
//		UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
//		if (txtMdlUIDRef.GetDataBase() == nil) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToText::text Mocel to UIDRef nil");
//			break;
//		}
//		
//		// Get the document's root element
//		InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
//		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//
//		if (rootElement == nil)
//		{			
//			// This menu item should only appear when there is a document.
//			// If the XML plug-in is loaded then all documents should have a root element.
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToText::ExpXMLActionComponent::DoTagSelectedText - Nil IIDXMLElement* for document root");
//			break;
//		}
//			
//		// Before tagging the text itself, we have to verify the story itself is tagged.
//		InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
//		
//		if (storyXMLRefData == nil)
//		{
//			// XML plug-in should insure that the story has this interface
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToText::ExpXMLActionComponent::DoTagSelectedText - nil IXMLReferenceData* from kTextStoryBoss");
//			break;
//		}
//		
//		XMLReference storyXMLRef = storyXMLRefData->GetReference();
//
//		/// for attaching XMLtag to the selected range of text.. 
//		//first we need to tag whole story innsdie the Text box
//		// We can add the Text tag below the Frame Story Tag In our case the Story tag name  is 'PRINTsource'
//		if (storyXMLRef == kInvalidXMLReference)   
//		{												
//			
//			
//			XMLReference parent = rootElement->GetXMLReference();
//			PMString storyTagName("PRINTsource");
//			
//            //Here first time attachAttributes() function is called
//			storyXMLRef = TagFrameElement(parent, txtMdlUIDRef.GetUID(), storyTagName);
//		
//			if (storyXMLRef == kInvalidXMLReference)
//			{				
//				
//				// We should have been able to create this element
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToText::ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
//				break;
//			}
//			
//		}	
//		// dont attach tag to the text inside frame .. so foll. code
//		bool16 frameTagChkboxState=kFalse;
//		frameTagChkboxState=this->getFrameTagChkboxState();
//		
//		if(frameTagChkboxState==kFalse)
//		{
//			// Now use the ElementCommands utility directly to tag the text
//			PMString myTextElement = prepareTagName(lstboxInfo.name);				
//			XMLReference* newTag = new XMLReference;				
//						
//			if(myTextElement.Compare(kTrue,"All_Standard_Tables")==0)
//			{
//				myTextElement.Clear();
//				myTextElement.Append("Standard_Table");
//			}
//			
//			::prepareTagName(myTextElement);
//			myTextElement = ::keepOnlyAlphaNumeric(myTextElement);
//
//
//			Utils<IXMLElementCommands>()->CreateElement(WideString(myTextElement), txtMdlUIDRef,/*TPLMediatorClass::initialCaratPosition*/start, end,kInvalidXMLReference, newTag);
//
//			if((selTab == 4) || (selTab == 3) || (selTab == 5))  // changed for Attribute Name spray support
//			{
//				if(isAddTableHeader && lstboxInfo.tableFlag != 1)
//				{    
//					globalParentTypeId = lstboxInfo.typeId;
//					lstboxInfo.header = 1;
//				}
//				if(isAddAsDisplayName)
//				{
//					if(lstboxInfo.isImageFlag == kTrue )
//					{
//						lstboxInfo.header = 1;
//                        if(isHorizontalFlow == kTrue)
//                            lstboxInfo.flowDir = 0;
//                        else if( isHorizontalFlow == kFalse)
//                            lstboxInfo.flowDir = 1;
//					}
//					else if(lstboxInfo.tableFlag == kTrue )
//					{
//						lstboxInfo.dataType = 4;
//					}
//					else
//					{
//						lstboxInfo.header = 1;
//					}
//				}
//
//				if(isAddImageDescription)
//				{
//					if(lstboxInfo.isImageFlag == kTrue )
//					{
//						//lstboxInfo.id = -124;
//						lstboxInfo.dataType = 2;
//                        if(isHorizontalFlow == kTrue)
//                            lstboxInfo.flowDir = 0;
//                        else if( isHorizontalFlow == kFalse)
//                            lstboxInfo.flowDir = 1;
//					}
//					else
//					{
//						lstboxInfo.header = 1;
//					}
//				}
//				if(lstboxInfo.name.Compare(kTrue,"All Standard Tables")==0)
//				{
//					lstboxInfo.id = -121;
//					lstboxInfo.tableFlag = 0;
//				}
//				attachAttributes
//				(newTag,kFalse,lstboxInfo,-1, -1);
//			}
//			else
//            {
//                attachAttributes(newTag,selIndex,selTab);
//            }
//		}
//
//	} while (false);	
//}





//void CTGCommonFunctions::appendTextIntoSelectedAndOverlappedBox
//(UIDRef boxUIDRef,PMString textToInsert)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return ;
//	}
//
//	InterfacePtr<IPMUnknown> unknown(boxUIDRef,IID_IUNKNOWN);
//		
//		int16 isTextFrame=Utils<IFrameUtils>()->IsTextFrame(unknown);
//		if(isTextFrame==0)
//		{
//			//this->addTagToGraphicFrame(curBox,selIndex,selTab);
//			ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGCommonFunctions::appendTextIntoSelectedAndOverlappedBox::isTextFrame == 0");
//			return;
//		}
//		
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		
//		if(textFrameUID==kInvalidUID)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTextIntoSelectedAndOverLappedBox::textFrameUID == kInvalidUID");
//			return;
//		}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			return;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			return;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!!ITextFrameColumn");
//			return;
//		}
//
//		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//		if(!textModel)
//		{
//			//CA("!textModel" );
//			return;
//		}
//
//		TextIndex startIndex = frameItemTFC->TextSpan()-1;  //here we calculate the last available text index in the text frame
//		
//    textToInsert.ParseForEmbeddedCharacters();
//	WideString* myText=new WideString(textToInsert);
//		
//	textModel->Insert(startIndex,myText);
//		
//	TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox = 1;
//	TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = startIndex;
//	
//	if(myText)
//		delete myText;
//}

/////////////////////////////////////////////////////////////////////////

//Following function is called from TPLCustFlavHlpr.cpp (from TPLCustFlavHlpr::ProcessDragDropCommand() ) 
//This function takes only one argument (PMPoint currentPoint) which is the point 
//where we drag-drop(Point where we release mouse click after drag action)
//Inside this function we get UIDRef of text-frame which has blinking cursor
//in it. After that we get left,right,top & bottom corners of that frame.
//Then we check wether  currentPoint(argument of following function)is inside
//the Text-Frame or not & depending upon that we set the flag from Mediator Class
//TPLMediatorClass::checkForOverlapWithTextFrame.
//We use this flag inside if condition in appendTextIntoBox() function.

//void CTGCommonFunctions::checkForOverlapWithTextFrame(PMPoint currentPoint)
//{	
//		UIDRef textSelectionFrame;
//
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			//CA("ptrIAppFramework is nil");		
//			return ;
//		}
//
//		InterfacePtr<ISelectionManager>	iSelectionManager1 (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(!iSelectionManager1)
//		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverLapWithTextFrame::Selection Manager is Null ");
//				return ;
//		}	
//
//		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite1(iSelectionManager1,UseDefaultIID());
//		if(txtSelectionSuite1)
//		{
//			//CA("Text Selection Suite is not null");
//			txtSelectionSuite1->GetFrameUIDRef(textSelectionFrame);
//		}
//		
//	
//		PMRect theArea;
//		if(!this->getBoxDimensions(textSelectionFrame, theArea))
//		{	
//			return ;
//		}
//
//		PMReal left, right ,top ,bottom;
//		
//		left = theArea.Left();
//		right = theArea.Right();
//		top = theArea.Top();
//		bottom = theArea.Bottom();
//
//		if( ((left < currentPoint.X())  &&( currentPoint.X() < right)) && ((top < currentPoint.Y()) &&(currentPoint.Y()< bottom)))
//		{
//			TPLMediatorClass::checkForOverlapWithTextFrame = kTrue;
//			return ;
//		}
//		else
//		{
//			TPLMediatorClass::checkForOverlapWithTextFrame = kFalse;
//			return ;		
//		}
//}

/////////////////////////////////////////////////////////////////////////
//bool16 CTGCommonFunctions::checkForOverlapWithMasterFrameFunction(UIDRef overlapBoxUIDRef)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse ;
//	}
//	UID overlapBoxUID = overlapBoxUIDRef.GetUID();
//	int32 size = TPLMediatorClass::UIDListofMasterPageItems.Length();
//	if(size == 0)
//	{
//		//ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::checkForOverlapWithMasterFrameFunction::size == 0");
//		return kFalse;
//	}
//	
//	for(int i=0; i<size ;i++)
//	{
//		if(TPLMediatorClass::UIDListofMasterPageItems[i] == overlapBoxUID)
//			return kTrue;
//			
//	}
//	
//	TPLMediatorClass::UIDListofMasterPageItems.Append(overlapBoxUID);
//	return kFalse;
//}

////Method Purpose: Get the status of 'Overflow' check box onpallete 
//bool16 CTGCommonFunctions::getOverflowChkboxState()  /// Added for overflow chkbox
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//	bool16 chkboxState=kFalse;
//	IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOverFlowWidgetID);
//	if(iChkboxCntrlView==nil) 
//	{
//		ptrIAppFramework->LogDebug("AP7TemplateBuilder::CTGCommonFunctions::getOverflowChkboxState::ichkboxCntrlView in nil");	
//		return chkboxState;	
//	}
//	if(iChkboxCntrlView->IsEnabled()){
//		InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
//		if(itristatecontroldata==nil) 
//		{
//			ptrIAppFramework->LogDebug("AP7TemplateBuilder::CTGCommonFunctions::getOverfilowChkboxState::itristatecontroldata is nil");		
//			return chkboxState;
//		}
//		
//		if(itristatecontroldata->IsSelected()){
//			chkboxState=kTrue;
//		}
//		else{
//			chkboxState=kFalse;
//		}
//	}
//	else{
//		chkboxState=kFalse;
//	}
//	return chkboxState;
//}


//void CTGCommonFunctions::addTagToPVBoxDoubleClickVersion
//(UIDRef curBox,int32 selIndex,int32 selTab,PMString result)
//{		
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return;
//	}
//	do
//	{
//		InterfacePtr<IPMUnknown> unknown(curBox,IID_IUNKNOWN);
//		
//		int16 isTextFrame=Utils<IFrameUtils>()->IsTextFrame(unknown);
//		if(isTextFrame==0)
//		{		
//			ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::isTextFrame == 0");
//			return;
//		} 
//		
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		if(textFrameUID==kInvalidUID)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::textFrameUID==kInvalidUID");
//			return;
//		}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			return;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			return;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			return;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!!ITextFrameColumn");
//			return;
//		}
//
//		InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
//		if(!txtModel)
//		{
//			//CA("!txtModel" );
//			return;
//		}
//
//		TPLListboxData lstboxData;
//		TPLListboxInfo lstboxInfo;
//		lstboxInfo=lstboxData.getData(selTab, selIndex);
//		
//		IActiveContext* ac =GetExecutionContextSession()->GetActiveContext();
//		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite((const IPMUnknown*)ac->GetContextSelection(),UseDefaultIID());
//			
//		int32 start=0;
//		int32 end =0;		
//		txtSelectionSuite->GetTextSelectionRange(start, end);		
//
//		// Look for a text selection. Bail if we can't find one.
//		UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
//		if (txtMdlUIDRef.GetDataBase() == nil) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::text Mocel to UIDRef nil");
//			break;
//		}
//			
//		// Get the document's root element
//		InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
//		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//		
//		if (rootElement == nil)
//		{			
//			// This menu item should only appear when there is a document.
//			// If the XML plug-in is loaded then all documents should have a root element.
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - Nil IIDXMLElement* for document root");
//			break;
//		}
//			
//		// Before tagging the text itself, we have to verify the story itself is tagged.
//		InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
//		
//		if (storyXMLRefData == nil)
//		{
//			// XML plug-in should insure that the story has this interface
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - nil IXMLReferenceData* from kTextStoryBoss");
//			break;
//		}
//		
//		XMLReference storyXMLRef = storyXMLRefData->GetReference();
//
//		/// for attaching XMLtag to the selected range of text.. 
//		//first we need to tag whole story innsdie the Text box
//		// We can add the Text tag below the Frame Story Tag In our case the Story tag name  is 'PRINTsource'
//		if (storyXMLRef == kInvalidXMLReference)   
//		{												
//			
//			//CAlert::InformationAlert("Story is not tagged");
//			// The story is not tagged, so we have to create an element for it before
//			// tagging the text in the story.
//			XMLReference parent = rootElement->GetXMLReference();
//			
//			PMString storyTagName("PRINTsource");
//			//CA("add tag to text");
//			storyXMLRef = TagFrameElement(parent, txtMdlUIDRef.GetUID(), storyTagName);
//			
//			if (storyXMLRef == kInvalidXMLReference)
//			{						
//				// We should have been able to create this element
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToPVBoxDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
//				break;
//			}
//			
//		}	
//		// dont attach tag to the text inside frame .. so foll. code
//		bool16 frameTagChkboxState=kFalse;
//		frameTagChkboxState=this->getFrameTagChkboxState();
//		
//		if(frameTagChkboxState==kFalse)
//		{		
//			// Now use the ElementCommands utility directly to tag the text						
//			PMString myTextElement = prepareTagName(result);
//			XMLReference* newTag = new XMLReference;		
//			
//			::prepareTagName(myTextElement);
//
//			Utils<IXMLElementCommands>()->CreateElement(WideString(myTextElement), txtMdlUIDRef,start, end,kInvalidXMLReference, newTag);
//			attachAttributes(newTag,selIndex,selTab);	
//		}		
//	} while (false);	
//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//ErrorCode CTGCommonFunctions::CreateTable(const UIDRef& storyRef,
//										const TextIndex at,
//										  const int32 numRows,
//										  const int32 numCols,
//										  const PMReal rowHeight,
//                                          const PMReal colWidth,
//										  const CellType cellType)
//{
//	ErrorCode status = kFailure;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return status;
//	}
//	do {
//		InterfacePtr<ITextModel> textModel(storyRef, UseDefaultIID());
//		ASSERT(textModel);
//		if(textModel == nil) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::CreateTable::textModel == nil");		
//			break;
//		}
//
//		Utils<ITableUtils> tableUtils;
//		if (!tableUtils) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::CreateTable::!tableUtils");
//			break;
//		}
//		tableUtils->InsertTable (textModel, at, 0,
//									 numRows, numCols,
//									 rowHeight, colWidth,
//									 cellType,
//									 ITableUtils::eSetSelectionInFirstCell);
//		status = kSuccess;
//
//	} while (false);
//
//	return status;
//
//} // SnipCreateTable


//bool16 CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag(TPLListboxInfo lstboxInfo,UIDRef ref)
//{
//	TPLListboxInfo lstboxInfotemp = lstboxInfo;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return kFalse;
//	}
//
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//	if(!iSelectionManager)
//	{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::insertTableInsideTableCellAndApplyTag::!iSelectionManager");
//		return kFalse;
//	}
//
//	InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
//	if(!txtSelectionSuite){
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndAppyTag::Please Select a Type Tool");
//	}
//
//	TPLMediatorClass::tableFlag=lstboxInfo.tableFlag;
//
//	int32 start=0;
//	txtSelectionSuite->GetCaretPosition(start);
//	
//	InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
//	if (graphicFrameHierarchy == nil) 
//	{
//		//CA("graphicFrameHierarchy is NULL");
//		return kFalse;
//	}
//					
//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//	if (!multiColumnItemHierarchy) {
//		//CA("multiColumnItemHierarchy is NULL");
//		return kFalse;
//	}
//
//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//	if (!multiColumnItemTextFrame) {
//		//CA("multiColumnItemTextFrame is NULL");
//		return kFalse;
//	}
//	InterfacePtr<IHierarchy>
//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//	if (!frameItemHierarchy) {
//		//CA("frameItemHierarchy is NULL");
//		return kFalse;
//	}
//
//	InterfacePtr<ITextFrameColumn>
//	frameItemTFC(frameItemHierarchy, UseDefaultIID());
//	if (!frameItemTFC) {
//		//CA("!!!ITextFrameColumn");
//		return kFalse;
//	}
//
//	InterfacePtr<ITextModel> txtModel1(frameItemTFC->QueryTextModel());
//	if(!txtModel1)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::Text Model is null");	
//		return kFalse;
//	}
//
//	UIDRef frameref=::GetUIDRef(frameItemTFC);
//
//	UIDRef textStoryUIDRef = ::GetUIDRef(txtModel1);
//	UIDRef ref1=::GetUIDRef(frameItemTFC);
//	
//	TextIndex TablStartIndex1 = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel1,start);
//
//	this->CreateTable(textStoryUIDRef,
//							start,
//							2,
//							2,
//							20,
//							50
//							);
//
//	UIDRef tableRef=( Utils<ITableUtils>()->GetTableModel(txtModel1, start));
//	TPLMediatorClass::IsTableInsideTableCell = kTrue;
//
//	
//	int32 TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel1,0);
//	UIDRef tableRef1=( Utils<ITableUtils>()->GetTableModel(txtModel1, TablStartIndex));
//	if(tableRef1==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::tableRef1 == nil");
//		return kFalse;
//	}
//
//	UIDRef txtMdlUIDRef =::GetUIDRef(txtModel1);
//	InterfacePtr<ITableModel> tableModel(tableRef1, UseDefaultIID());
//	if (tableModel == nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::Err: invalid interface pointer ITableFrame");
//		return kFalse;
//	}
//
//	UIDRef tableModelUIDRef(::GetUIDRef(tableModel));
//	PMString TableTagName = "PSTable";
//	PMString CellTagName="PSCell";
//
//	XMLReference xmlRef;
//	InterfacePtr<ITblBscSuite>tableSuite(iSelectionManager,UseDefaultIID());
//	if(!tableSuite)
//	{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::Table Suite is null");
//		return kFalse;
//	}
//
//	GridArea gridArea;
//	tableSuite->GetCellRange(gridArea);
//	RowRange rRange;
//	rRange=gridArea.GetRows();
//
//	ColRange cRange;
//	cRange=gridArea.GetCols();
//	
//	this->TagTable(tableModelUIDRef,frameref,TableTagName,CellTagName,xmlRef,lstboxInfo);
//
//	InterfacePtr<ITableModelList> tableList(txtModel1, UseDefaultIID());
//	if(tableList==nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::tableList == nil");
//		return kFalse;
//	}
//
//	int32	tableIndex = tableList->GetModelCount() - 1;
//	InterfacePtr<ITableModel> tableModel1(tableList->QueryNthModel(tableIndex));
//	if(tableModel1 == nil) 
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::tableModel1 == nil");
//		return kFalse;
//	}
//
//	InterfacePtr<ITextStoryThreadDict> textStoryThreadDict(tableModel1,UseDefaultIID());
//	if(textStoryThreadDict == nil)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::textStoryThreadDict == nil");
//		return kFalse;
//	}
//	
//	InterfacePtr<ITableModel> innertableModel1(tableList->QueryNthModel(tableIndex));
//	if(!innertableModel1)
//	{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::!innertableModel1");
//		return kFalse;
//	}
//	
//	UIDRef innertableModelUIDRef(::GetUIDRef(innertableModel1));
//	PMString innerTableTagName =this->prepareTagName(lstboxInfo.name);
//	PMString innerTableCellTagName="PSTableCell";
//
//	XMLReference xmlRefinner;
//	ErrorCode err = Utils<IXMLElementCommands>()->CreateTableElement(WideString(innerTableTagName),WideString(innerTableCellTagName),innertableModelUIDRef,&xmlRefinner);
//	if (err != kSuccess)
//	{
//		ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::InsertTableInsideTableCellAndApplyTag::ExpXMLActionComponent::TagFrameElement - CreateTableElement failed");
//		return kFalse;
//	}
//	 
//	this->attachAttributes(&xmlRefinner, kFalse,lstboxInfotemp , rRange.start, cRange.start);
//
//	TPLMediatorClass::IsTableInsideTableCell = kFalse;
//	return kTrue;
//		
//}




/////////////////////////////////////////////////////////////////////////////////upto here
//Method Purpose: Use to Tag given Table 
//ErrorCode CTGCommonFunctions::TagTable(const UIDRef& tableModelUIDRef,UIDRef& BoxRef,
//								const PMString& tableTagName,
//								const PMString& cellTagName,
//								XMLReference& outCreatedXMLReference,
//								TPLListboxInfo& lstboxInfo )
//{
//
//	ErrorCode err = kFailure;
//	static int TableCount = 0;
//	static UIDRef TableUIDREF;
//	static UIDRef boxUIDref;
//	static PMString TableName("");
//	static PMString CellName("");
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");		
//		return err;
//	}
//	
//	do
//	{
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Selection Manager is Null ");
//				return err;
//		}
//
//		InterfacePtr<ITblBscSuite>tableSuite(iSelectionManager,UseDefaultIID());
//		if(!tableSuite)
//		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Table Suite is null");
//			 return err;
//		}
//
//		GridArea gridArea;
//		tableSuite->GetCellRange(gridArea);
//		RowRange rRange;
//		rRange=gridArea.GetRows();
//		ColRange cRange;
//		cRange=gridArea.GetCols();
//		UIDRef ref;
//		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite(iSelectionManager,UseDefaultIID());
//		if(!txtSelectionSuite){
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Text Selection Suite is null");
//		}
//		else
//		{
//			txtSelectionSuite->GetFrameUIDRef(ref);
//			InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
//			if (graphicFrameHierarchy == nil) 
//			{
//				//CA("graphicFrameHierarchy is NULL");
//				return err;
//			}
//							
//			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//			if (!multiColumnItemHierarchy) {
//				//CA("multiColumnItemHierarchy is NULL");
//				return err;
//			}
//				
//			int32 chldCount = graphicFrameHierarchy->GetChildCount();
//
//			InterfacePtr<IMultiColumnTextFrame>
//			frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
//			if (!frameItemTFC) {
//				//CA("!!!ITextFrameColumn");
//				return err;
//			}
//
//			InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//			if(!textModel)
//			{
//				//CA("!textModel");
//				return err;
//			}
//
//			InterfacePtr<ITableModel> tableModel(tableModelUIDRef, UseDefaultIID());
//			if (tableModel == nil)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Err: invalid interface pointer ITableFrame");
//				break;
//			}
//
//			GridAddress grd(rRange.start,cRange.start);
//			GridID id=tableModel->GetGridID(grd);
//
//			/*UIDRef tableModelUIDRef(::GetUIDRef(tableModel));*/
//			UIDRef textRef(::GetUIDRef(textModel));
//			//CA("Before TagStory");
//			err=TagStory(PMString("PRINTsource"),textRef);
//			//CA("After Tag Story");
//			XMLReference xmlRef;
//
//			if(boxUIDref !=BoxRef){
//				TableCount = 0;
//				boxUIDref = BoxRef;
//			}
//			PMString TableTagName("PSTable");
//			PMString CellTagName("PSCell");
//
//			if(TableUIDREF != tableModelUIDRef)
//			{
//				TableTagName.AppendNumber(TableCount);
//				CellTagName.AppendNumber(TableCount);
//				//CA(TableTagName);
//				TableCount++;
//				TableUIDREF = tableModelUIDRef;
//				TableName = TableTagName;
//				CellName = CellTagName;
//
//			}
//			//CA(TableName);
//
//			err=AddTableAndCellElements(tableModelUIDRef, TableName, CellTagName, xmlRef,lstboxInfo.listBoxType);
//
//			//CA("After AddTableAndCellElements ");
//			XMLReference CellTextXmlRef;
//			
//			if(TPLMediatorClass::IsTableInsideTableCell == kFalse)
//				err=TagTableCellText(tableModel,xmlRef, id, gridArea, CellTextXmlRef, lstboxInfo);
//
//			//following if added by Tushar on 5/1/07
//			//if(lstboxInfo.id == -701 || lstboxInfo.id == -702 || lstboxInfo.id == -703 || lstboxInfo.id == -704) 
//			//{
//				globalParentTypeId = lstboxInfo.typeId;
//			//}
//
//			if(lstboxInfo.tableFlag == 0 /*&& lstboxInfo.listBoxType == 4 */&& lstboxInfo.isImageFlag == kFalse)  // changed for Attribute Name spray support
//				{//This is for itemcopy attributes
//					if(isAddTableHeader)
//					{
//						lstboxInfo.typeId = -2;
//					}
//					/*else 
//					{
//						lstboxInfo.typeId = -1;
//					}*/
//				}
//				else if(lstboxInfo.tableFlag == 1 && (lstboxInfo.listBoxType == 3 || lstboxInfo.listBoxType == 4))
//				{//This is for ItemTable tag of either product or item  inside the table cell.
//					if(isAddTableHeader)
//						lstboxInfo.id = -103;
//					else
//						lstboxInfo.id = -104;
//				}
//	/////////////////////////////////////////////////////// upto here ////////////
//	//////////////////////////////////////////////////////////////////////////////
////end added for table header.
//				
//				//following if condion is added by vijay on 9-11-2006
//				if(TPLMediatorClass::IsTableInsideTableCell == kFalse)
//					this->attachAttributes(&CellTextXmlRef, kFalse, lstboxInfo, rRange.start, cRange.start);
//		//restore the original value of typeId which is -1
//				if(isAddTableHeader)
//				{
//					lstboxInfo.typeId = globalParentTypeId;
//				}
//				
//
//		}
//	} while(kFalse);
//	return err;
//}



//void CTGCommonFunctions::appendTableNameAndTable(int32 selectedRowIndex, PMString theContent, int32 whichTab)	// This function is written only for All Standard Table Stencil
//{
//	//CA("Inside CTGCommonFunctions::appendTableNameAndTable ");
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CA("ptrIAppFramework is nil");	
//		return;
//	}
//
//	TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox = 0;
//	TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = 0;
//
//	
//	bool16 successFlag=kTrue;
//	UIDRef overlapBoxUIDRef;
//	UIDList newList;
//	UIDRef selBoxUIDRef;
//
//	//ICommandSequence* seq = nil;
//	do
//	{
//		// Checking if the frame is tagged
//		bool16 isFrameTagged=kFalse;
//		//isFrameTagged=this->checkIfFrameIsTagged();
//		if(isFrameTagged==kTrue)
//		{
//			this->getSelectionFromLayout(selBoxUIDRef);
//			deleteThisBox(selBoxUIDRef);
//			return;
//		}
//		
////Here in following code we are appending square brackets (at begining and at end)
////to text which is to be inserted or appended into frame.
//
//		PMString tempStr("");
//		
//		
//		if(theContent.Compare(kTrue,"All Standard Tables") == 0 )
//		{
//			tempStr.Clear();
//				tempStr.Append("[Standard Table_DsplName]");
//		}
//		theContent.Clear();
//		theContent.Append(tempStr);
//
//////////////////////////////////////////////////////////////////////////////		
////following function call is added by vijay choudhari on 28-4-2006
////This is a global function ,It will replace blank spaces with '_'		
//		
//		
/////////////////////////////////////////////////////////////////////////////
//
////Inside if condition we are calling getSelectionFromLayout we are passing selBoxUIDRef
////by reference as a out parameter.This function will return UIDRef of frame which is 
////currently selected.
//		if(!this->getSelectionFromLayout(selBoxUIDRef))
//		{
//			successFlag=kFalse;
//			break;
//		}
//
//		//Start compound undo
//		//seq=CmdUtils::BeginCommandSequence();		
//		if(TPLMediatorClass::imageFlag!=kTrue)
//		{	
//			this->convertBoxToTextBox(selBoxUIDRef);
////following function appends text into the box			
//		}
//		
////Inside if condition we are calling checkForOverLaps ,if newly draged object is on the
////existing frame then it will return 1, else it will return 0.
////We are passing UIDRef of currently selected frame(selBoxUIDRef--which is newly 
////created frame) and  two out parameters  overlapBoxUIDRef(which is UIDRef reference) 
////and newList(which is UIDList reference). If checkForOverLaps function returns 1 ,
////we get UIDRef of frame which is overlaped by newly created frame. In the third
//// parameter(overlapBoxUIDRef)----------------added by vijay. 	
//		
//		if(!this->checkForOverLaps(selBoxUIDRef, 0, overlapBoxUIDRef, newList))
//		{
//			//CA("After checkForOverLaps");	
////Program flow will come inside 'if' block if frame is created for the first
////time(it means it is not overlaped).
////Here we are calling addTagToText function. In this function we attach
////XML Attributes to the added Tag
//			TPLMediatorClass::overLapingStatus = 0;
//			this->appendTextIntoBox(selBoxUIDRef, theContent, kTrue);
//			::prepareTagName(theContent);
//			this->addTagToText(selBoxUIDRef, selectedRowIndex, whichTab,theContent);
//		}
//		else
//		{
//	
//			TPLMediatorClass::overLapingStatus = 1;
//	
//			int result;
//			InterfacePtr<IPMUnknown> unknown(overlapBoxUIDRef ,IID_IUNKNOWN);
//			InterfacePtr<IHierarchy> iChildHier(unknown,UseDefaultIID());
//			if(iChildHier == nil)
//			{
//				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::iChildHier == nil");
//				return ;
//			}		
//			IDataBase * database = overlapBoxUIDRef.GetDataBase();
//			UID pageUID = Utils<ILayoutUtils>()->GetOwnerPageUID(iChildHier);
//
//			TPLMediatorClass::checkForOverlapWithMasterframe = Utils<ILayoutUtils>()->IsAMaster(pageUID,database);
//
//			if(TPLMediatorClass::checkForOverlapWithMasterframe)
//			{
//				bool16 IsOverlapOnExistingFrame = this->checkForOverlapWithMasterFrameFunction(overlapBoxUIDRef);
//				
//				if(IsOverlapOnExistingFrame == kTrue)
//				{
//					//ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGCommonFunctions::insertOrAppend::IsOverlapOnExistingFrame == kTrue");
//				}
//				else if(IsOverlapOnExistingFrame == kFalse)
//				{
//					TPLMediatorClass::overLapingStatus = 1;
//					result=this->appendTextIntoBox(selBoxUIDRef, theContent, kTrue);
//					if(result==0)
//					{
//						successFlag=kFalse;
//					}
//					else if(result!=-1)
//					{	
//						::prepareTagName(theContent);
//						this->addTagToText(selBoxUIDRef, selectedRowIndex, whichTab,theContent);
//			
//					}
//				}	
//			}
//			else
//			{
//				TPLMediatorClass::overLapingStatus = 1;
//				//In following line of code we are deleting the newly created frame				
//				this->deleteThisBox(selBoxUIDRef);
//				result=this->appendTextIntoBox(overlapBoxUIDRef, theContent, kTrue);
//				if(result==0)
//				{
//					successFlag=kFalse;
//				}
//				else if(result!=-1)
//				{	
//					::prepareTagName(theContent);
//					this->addTagToText(overlapBoxUIDRef, selectedRowIndex, whichTab,theContent);
//		
//				}
//				
//			}
//		}
//////////////////	Appending table after [Standard Table_DsplName] tag
//		textInsertIndex = theContent.CharCount()+2;
//		PMString hardreturn("\r", PMString::kEncodingASCII);
//		this->appendTextIntoBox(selBoxUIDRef, hardreturn, kTrue);
//		::prepareTagName(theContent);
//		TPLMediatorClass::checkForOverlapWithMasterframe = kFalse;
//		
//		int16 retVal=0;
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(selBoxUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!graphicFrameHierarchy");
//			break;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!multiColumnItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!multiColumnItemTextFrame");
//			break;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!frameItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!frameItemTFC");
//			break;
//		}
//
//		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//		if(!textModel)
//		{
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!textModel");
//			break;
//		}
//
//		TextIndex startIndex = textInsertIndex+1;
//		TextIndex finishIndex = textInsertIndex+1;
//
//		RangeData range(finishIndex,finishIndex,nil); 
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(iSelectionManager) 
//		{ 
//			InterfacePtr<ITextSelectionSuite> textSelectionSuite(iSelectionManager, UseDefaultIID()); 
//			if (textSelectionSuite) 
//			{ 
//				textSelectionSuite->SetTextSelection( 
//				::GetUIDRef(textModel), 
//				range, 
//				Selection::kScrollIntoView, nil);
//			} 
//		}
//
//		retVal=this->createNewTable();
//		if(!retVal)
//		{
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::Error in creating new table");
//			break;
//		}
/////	Appened table 		
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==NULL)
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!tableList");
//			break;
//		}
//		int32	tableIndex = tableList->GetModelCount() - 1;
//		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
//		{
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::tableIndex<0");
//			break;
//		}
//
//		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//		if(tableModel == NULL) 
//		{
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::!tableModel");
//			break;
//		}
//
//		UIDRef tableRef(::GetUIDRef(tableModel));
//
/////////////	To add tags to tabel that we added in text frame
//		XMLReference xmlRef;
//		this->AddTableAndCellElements(tableRef,"PSTable0","PSCell",xmlRef,3);
///////////////	End tag to table
//
//		//	Now going to attach tag to PRINTSource
//		InterfacePtr<IDocument> doc(selBoxUIDRef.GetDataBase(), selBoxUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
//		if(doc == nil){
//			//CA("doc is nil");
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::Document not found...");
//			break;
//		}
//		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//		if (rootElement == nil){
//			//CA("rootElement is nil");
//			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::IIDXMLElement NIL...");
//			break;
//		}
//
//		XMLReference root = rootElement->GetXMLReference();
//		//IIDXMLElement * rootTagPtr= root.Instantiate();
//		InterfacePtr<IIDXMLElement>rootTagPtr(root.Instantiate());
//		if(rootTagPtr == NULL)
//		{
//			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGCommonFunctions::appendTableNameAndTable::rootTagPtr == NULL");
//			break;
//		}
//		XMLReference childXMLReference = rootTagPtr->GetNthChild(0);
//////		IIDXMLElement * childReference = childXMLReference.Instantiate();
////
////
////		PMString tagString = this->prepareTagName("Standard Table");
////		PMString storyTagName =	this->RemoveWhiteSpace(tagString);
////		
////
////		XMLReference storyXMLRef = this->TagFrameElement(childXMLReference, tableRef.GetUID(),storyTagName);
////		if(storyXMLRef == kInvalidXMLReference)
////		{
////			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToGraphicFrame::Fail to set Graphic Frame Tag ...");
////			break;
////		}
////		
////		// Following stmt actually attaches the tag.
////		this->attachAttributes(&storyXMLRef,selectedRowIndex,whichTab);
//
//		this->attachAttributes(&childXMLReference,-2,-2);
//
//	}while(kFalse);
//	
//}


void CTGCommonFunctions::addTagToTextDoubleClickVersion(UIDRef curBox, PublicationNode &localPNode ,Attribute &attributeObj,int32 startIndex, int32 endIndex)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA("ptrIAppFramework is nil");
        return ;
    }
    
    do
    {
        InterfacePtr<IPMUnknown> unknown(curBox,IID_IUNKNOWN);
        bool16 isTextFrame = kFalse;
        
        UID textFrameUID = kInvalidUID;
        InterfacePtr<IGraphicFrameData> graphicFrameData(curBox, UseDefaultIID());
        if (graphicFrameData)
        {
            textFrameUID = graphicFrameData->GetTextContentUID();
            if(textFrameUID != kInvalidUID)
            {
                isTextFrame = kTrue;
            }
        }
        
        if(isTextFrame == kFalse)
        {
            ptrIAppFramework->LogDebug("addTagToTextDoubleClickVersion::isTextFrame is false");
            return;
        }
        
        
        if(textFrameUID==kInvalidUID)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToTextDoubleClickVersion::textFrameUID == kInvalidUID");
            return;
        }
        
        InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
        if (graphicFrameHierarchy == nil)
        {
            //CA("graphicFrameHierarchy is NULL");
            return;
        }
        
        int32 chldCount = graphicFrameHierarchy->GetChildCount();
        
        InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
        if (!multiColumnItemHierarchy) {
            //CA("multiColumnItemHierarchy is NULL");
        }
        InterfacePtr<IMultiColumnTextFrame>
        frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
        if (!frameItemTFC) {
            //CA("!!!IMultiColumnTextFrame");
            return;
        }
        
        InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
        if(!txtModel)
        {
            //CA("!txtModel");
            return;
        }
        
        //TPLListboxData lstboxData;
        //TPLListboxInfo lstboxInfo;
        //lstboxInfo=lstboxData.getData(selTab, selIndex);
        
        IActiveContext* ac =GetExecutionContextSession()->GetActiveContext(); //Cs4
        InterfacePtr<ITextMiscellanySuite>txtSelectionSuite((const IPMUnknown*)ac->GetContextSelection(),UseDefaultIID());
        
        //		int32 start=0;
        //		int32 end  =0;
        
        //		txtSelectionSuite->GetCaretPosition(start);
        //
        //		if(!TPLMediatorClass::imageFlag)
        //        {
        //				start = start - result.NumUTF16TextChars();
        //				end=start+result.NumUTF16TextChars();
        //		}
        //
        //		if(TPLMediatorClass::imageFlag)
        //        {
        //			txtSelectionSuite->GetCaretPosition(end);
        //			start = start - 1;
        //		}
        
        // Look for a text selection. Bail if we can't find one.
        UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
        if (txtMdlUIDRef.GetDataBase() == nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToTextDoubleClickVersion::text Mocel to UIDRef nil");
            break;
        }
        
        // Get the document's root element
        InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
        InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
        
        if (rootElement == nil)
        {
            // This menu item should only appear when there is a document.
            // If the XML plug-in is loaded then all documents should have a root element.
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToTextDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - Nil IIDXMLElement* for document root");
            break;
        }
        
        // Before tagging the text itself, we have to verify the story itself is tagged.
        InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
        
        if (storyXMLRefData == nil)
        {
            // XML plug-in should insure that the story has this interface
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToTextDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - nil IXMLReferenceData* from kTextStoryBoss");
            break;
        }
        
        XMLReference storyXMLRef = storyXMLRefData->GetReference();
        
        /// for attaching XMLtag to the selected range of text..
        //first we need to tag whole story innsdie the Text box
        // We can add the Text tag below the Frame Story Tag In our case the Story tag name  is 'PRINTsource'
        if (storyXMLRef == kInvalidXMLReference)
        {
            // The story is not tagged, so we have to create an element for it before
            // tagging the text in the story.
            XMLReference parent = rootElement->GetXMLReference();
            
            PMString storyTagName("PRINTsource");
            storyXMLRef = TagFrameElement(parent, txtMdlUIDRef.GetUID(), storyTagName, localPNode , attributeObj, kTrue );
            
            if (storyXMLRef == kInvalidXMLReference)
            {
                
                // We should have been able to create this element
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::addTagToTextDoubleClickVersion::ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
                break;
            }
            
        }

        // Now use the ElementCommands utility directly to tag the text
        PMString myTextElement = this->prepareTagName(attributeObj.getDisplayName());
        XMLReference* newTag = new XMLReference;
        
        //prepareTagName(myTextElement);
        Utils<IXMLElementCommands>()->CreateElement(WideString(myTextElement) , txtMdlUIDRef, startIndex, endIndex,kInvalidXMLReference, newTag);
        
        attachAttributes(newTag, kFalse, localPNode, attributeObj);
        
        
    } while (false);	
}


// Method Purpose: Use to attach XML tag structure to the Frame (Graphics/Text frame with Table).
XMLReference CTGCommonFunctions::TagFrameElement
(const XMLReference& newElementParent, UID frameUID, const PMString& frameTagName, PublicationNode &localPNode ,Attribute &attributeObj, bool16 isPRINTsourceTag)
{
    
    XMLReference resultXMLRef=kInvalidXMLReference;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA("ptrIAppFramework == nil");
        return resultXMLRef;
    }
    
    do {
        
        // Acquire the IXLMElementCommands interface on the Utils boss, and use its method
        // to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
        //CA("frameTagName::"+frameTagName);
        
        ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef);
        // Verify the results: no errors, valid XMLRef returned, we can instantiate it.
        if (errCode != kSuccess)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - CreateElement failed");
            break;
        }
        if (resultXMLRef == kInvalidXMLReference)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
            break;
        }
        InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
        if (newXMLElement==nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
        }
    }while (false);
    
    //if(isInsideTable)
    //{
    //    ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagFrameElement::isInsideTable==1");
    //    return resultXMLRef;
    //}
    
    
    this->attachAttributes(&resultXMLRef,isPRINTsourceTag, localPNode, attributeObj);
    
    
    return resultXMLRef;
}

// Methos Purpose: adds attributes to XMLelement
void CTGCommonFunctions::attachAttributes
(XMLReference* newTag,bool16 isPRINTSourceTag, PublicationNode &localPNode,Attribute &attribute )
{
    bool16 flag=kFalse;
    flag = isPRINTSourceTag;
    Mediator md;
    
    bool16 isChildTag = kFalse;
    
    if(localPNode.getSectionID() == localPNode.getParentId())
        isChildTag = kFalse;
    else
        isChildTag = kTrue;
    
    PMString attribName("ID");
    PMString attribVal("");
    attribVal.AppendNumber(PMReal(attribute.getAttributeId()));
    if(flag){
        attribVal="-1";
    }
    ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "typeId";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "header";
    attribVal="-1";
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isEventField";
    if(attribute.getIsEventSpecific())
        attribVal.AppendNumber(1);
    else
        attribVal.AppendNumber(-1);
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "deleteIfEmpty";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "dataType";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isAutoResize";
    attribVal="-1";
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "LanguageID";
    attribVal.AppendNumber(PMReal(md.languageID));
    
    if(flag){
        attribVal="NULL";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "index";
    attribVal.AppendNumber(4);
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "pbObjectId";
    attribVal.AppendNumber(PMReal(localPNode.getPBObjectID()));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "parentID";
    if(isChildTag)
    {
        attribVal = "";
        attribVal.AppendNumber(PMReal(localPNode.getParentId()));
    }
    else
        attribVal.AppendNumber(PMReal(localPNode.getPubId()));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "childId";
    attribVal.AppendNumber(-1);
    if(flag){
        attribVal="-1";
    }
    else if(isChildTag)
    {
        attribVal = "";
        attribVal.AppendNumber(PMReal(localPNode.getPubId()));
    }
    
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "sectionID";
    attribVal.AppendNumber(PMReal(localPNode.getSectionID()));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "parentTypeID";
    attribVal.AppendNumber(PMReal(localPNode.getTypeId()));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isSprayItemPerFrame";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "catLevel";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "imgFlag";
    attribVal="0";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "imageIndex";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "flowDir";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "childTag";
    attribVal="-1";
    if(isChildTag)
    {
        attribVal = "1";
    }
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableFlag";
    attribVal="-1001";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableType";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableId";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "rowno";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "colno";
    attribVal="-1";
    if(flag)
    {
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    attribName.Clear();
    attribVal.Clear();
    attribName = "field1";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field2";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field3";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field4";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field5";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "groupKey";
    attribVal.Append("");
    if(flag){
        attribVal="";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
}

//Method Purpose: remove the Special character from the Name.
PMString CTGCommonFunctions::keepOnlyAlphaNumeric(PMString name)
{
    //CA(__FUNCTION__);
    PMString tagName("");
    
    for(int i=0;i<name.NumUTF16TextChars(); i++)
    {
        bool isAlphaNumeric = false ;
        
        PlatformChar ch = name.GetChar(i);
        
        if(ch.IsAlpha() || ch.IsNumber())
            isAlphaNumeric = true ;
        
        if(ch.IsSpace())
        {
            isAlphaNumeric = true ;
            ch.Set('_') ;
        }
        
        if(ch == '_')
            isAlphaNumeric = true ;
        
        if(isAlphaNumeric) 
            tagName.Append(ch);
    }
    
    return tagName ;
}

////////////////////////////////////////////////////////////////////////////////
//Method Purpose: Removes the blank spaces from the name variable and Replaces
//it with '_' as Indesign doesnot allow the XML tag names with blank spaces or Special characters.
// Prepares the Tag name as "PR Name" => "[PR_Name]"
PMString CTGCommonFunctions::prepareTagName(PMString name)
{
    //CA(__FUNCTION__);
    PMString tagName("");
    
    PlatformChar ch = name.GetChar(0);
    
    if(ch.IsNumber())
    {
        PMString temp = name;
        name.Clear();
        name = "A";
        name.Append(temp);
        temp.Clear();
    }
    
    for(int i=0;i<name.NumUTF16TextChars(); i++)
    {
        
        if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
            continue;
        if(name.GetChar(i) ==' '){
            tagName.Append("_");
        }
        else tagName.Append(name.GetChar(i));
    }
    PMString FinalTagName("");
    FinalTagName = this->keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.
    return FinalTagName;
}

//Method Purpose: Use to Tag given Table
ErrorCode CTGCommonFunctions::TagTable(const UIDRef& tableModelUIDRef,
                                UIDRef& BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								PublicationNode &localPNode,
                                Attribute &attributeObj,
                                int32 startIndex,
                                int32 endIndex)
{
	ErrorCode err = kFailure;
    
	static int TableCount = 0;
	static UIDRef TableUIDREF;
	static UIDRef boxUIDref;
	static PMString TableName("");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework is nil");
		return err;
	}

	do
	{
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Selection Manager is Null ");
				return err;
		}

		InterfacePtr<ITblBscSuite>tableSuite(iSelectionManager,UseDefaultIID());
		if(!tableSuite)
		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Table Suite is null");
			 return err;
		}

		GridArea gridArea;
		tableSuite->GetCellRange(gridArea);
		RowRange rRange;
		rRange=gridArea.GetRows();
		ColRange cRange;
		cRange=gridArea.GetCols();

		UIDRef ref;
		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite(iSelectionManager,UseDefaultIID());
		if(!txtSelectionSuite){
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Text Selection Suite is null");
		}
		else
		{
			txtSelectionSuite->GetFrameUIDRef(ref);

			InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
			if (graphicFrameHierarchy == nil)
			{
				//CA("graphicFrameHierarchy is NULL");
				return err;
			}

			int32 chldCount = graphicFrameHierarchy->GetChildCount();

			InterfacePtr<IHierarchy>multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
			if (!multiColumnItemHierarchy) {
				//CA("multiColumnItemHierarchy is NULL");
				return err;
			}

			InterfacePtr<IMultiColumnTextFrame>frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
			if (!frameItemTFC) {
				//CA("!!!IMultiColumnTextFrame");
				return err;
			}

			InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
			if(!textModel)
			{
				//CA("!textModel" );
				return err;
			}

			InterfacePtr<ITableModel> tableModel(tableModelUIDRef, UseDefaultIID());
			if (tableModel == nil)
			{
				ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Err: invalid interface pointer ITableFrame");
				break;
			}

			GridAddress grd(rRange.start,cRange.start);
			GridID id=tableModel->GetGridID(grd);

			UIDRef textRef(::GetUIDRef(textModel));
			err=TagStory(PMString("PRINTsource"),textRef);
			XMLReference xmlRef;

			if(boxUIDref !=BoxRef){
				TableCount = 0;
				boxUIDref = BoxRef;
			}
			PMString TableTagName("PSTable");
			PMString CellTagName("PSCell");

			if(TableUIDREF != tableModelUIDRef)
			{
				TableTagName.AppendNumber(TableCount);
				TableCount++;
				TableUIDREF = tableModelUIDRef;
				TableName = TableTagName;
			}
     
            TagStruct tableTagStruct;
            
            bool16 isChildTag = kFalse;
            if(localPNode.getSectionID() == localPNode.getParentId())
                isChildTag = kFalse;
            else
                isChildTag = kTrue;
            

//            if(isChildTag)
//            {
//                tableTagStruct.parentId = localPNode.getParentId();
//                tableTagStruct.childId = -1;
//                tableTagStruct.childTag = -1;
//            }
//            else
            {
                tableTagStruct.parentId=localPNode.getPubId();
                tableTagStruct.childId = -1;
                tableTagStruct.childTag = -1;
            }
            
            tableTagStruct.pbObjectId=localPNode.getPBObjectID();
            tableTagStruct.sectionID=localPNode.getSectionID();
            tableTagStruct.whichTab =4;
            tableTagStruct.imgFlag= 0;
            tableTagStruct.elementId = -102;

            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Before AddTableAndCellElements");
			err = AddTableAndCellElements(tableModelUIDRef, TableName, CellTagName, xmlRef, tableTagStruct);
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::After AddTableAndCellElements");

			XMLReference CellTextXmlRef;
            
            TagStruct cellTagStruct;
            
//            if(isChildTag)
//            {
//                cellTagStruct.parentId = localPNode.getParentId();
//                cellTagStruct.childId = localPNode.getPubId();
//                cellTagStruct.childTag = 1;
//            }
//            else
            {
                cellTagStruct.parentId = localPNode.getPubId();
                cellTagStruct.childId = -1;
                cellTagStruct.childTag = -1;
            }
                
            
            cellTagStruct.pbObjectId = localPNode.getPBObjectID();
            cellTagStruct.sectionID = localPNode.getSectionID();
            cellTagStruct.whichTab = 4;
            cellTagStruct.imgFlag= 0;
            cellTagStruct.elementId= attributeObj.getAttributeId();
            cellTagStruct.isEventField = attributeObj.getIsEventSpecific();
            cellTagStruct.parentTypeID = localPNode.getTypeId();
            cellTagStruct.imageIndex = -1;
            cellTagStruct.flowDir = -1;
            
            PMString CellTextTagName = attributeObj.getDisplayName();
            if(CellTextTagName == "")
                CellTextTagName = "_";
            
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::Before TagTableCellText" + CellTextTagName);
			err=TagTableCellText(tableModel, xmlRef, id, gridArea, CellTextXmlRef, CellTextTagName, startIndex, endIndex);
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::After TagTableCellText" + CellTextTagName);

			this->attachAttributes(&CellTextXmlRef, kFalse, cellTagStruct);
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTable::After attachAttributes");
		}

	} while(kFalse);
    
    
	return err;
    
}


//Method Purpose: Use to tag the whole story of text frame.
ErrorCode CTGCommonFunctions::TagStory(const PMString& tagName,
									const UIDRef& textModelUIDRef)
{
	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework is nil");
		return err;
	}
	do {
		IDataBase* database = textModelUIDRef.GetDataBase();
		UIDRef documentUIDRef(database, database->GetRootUID());
		if(textModelUIDRef.GetDataBase()==nil)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagStory::TagStory---Invalid uid of UIDRef of text story");
		}
		InterfacePtr<ITextModel> textModel(textModelUIDRef, UseDefaultIID());
		ASSERT(textModel);
		if(!textModel) {
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagStory::Text Model is null in tagging story");
			break;
		}

		InterfacePtr<IIDXMLElement> rootXMLElement(Utils<IXMLUtils>()->QueryRootElement(database));
		ASSERT(rootXMLElement);
		if(!rootXMLElement) {
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagStory::Root xmlElement is null");
			break;
		}
		XMLReference rootXMLReference = rootXMLElement->GetXMLReference();

		int32 indexInParent =  rootXMLElement->GetChildCount();
		if(indexInParent < 0) {
			indexInParent = 0;
		}
		bool16 ISTagExist = kFalse;

		UIDRef tagUIDRef = this->AcquireTag(textModelUIDRef, tagName, ISTagExist);
		if(!ISTagExist)
		{//CA("Before Creation PRINTsource Tag");
			XMLReference PSXmlRef;
			err = Utils<IXMLElementCommands>()->CreateElement(tagUIDRef.GetUID(),
				textModelUIDRef.GetUID(),
				rootXMLReference,
				indexInParent,
				&PSXmlRef
				);

            TagStruct tStruct;
			this->attachAttributes(&PSXmlRef, kTrue, tStruct);
			//CA("After Creating PRINTsource tag");
		}

	} while(kFalse);
	return err;
}


//=================================================================================

//Method Purpose: use to serch for a xmltag with given tag name if tag found
//then returs that tagUIDRef otherwise creates the new tag
UIDRef CTGCommonFunctions::AcquireTag(const UIDRef& boxUIDRef,
									 const PMString& tagName, bool16 &ISTagPresent
									 )
{
	UIDRef retval;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework is nil");
		return retval;
	}

	do {

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne)
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		UIDRef ref;
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::AcquireTag::Selection Manager is Null ");
				return retval;
		}

		InterfacePtr<ITextMiscellanySuite>txtSelectionSuite(iSelectionManager,UseDefaultIID());
		if(!txtSelectionSuite){
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::AcquireTag::Text Selection Suite is null");
			break;
		}
        txtSelectionSuite->GetFrameUIDRef(ref);

		InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
		if (graphicFrameHierarchy == nil)
		{
			//CA("graphicFrameHierarchy is NULL");
			return retval;
		}

		InterfacePtr<IHierarchy>
		multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			return retval;
		}

		InterfacePtr<IMultiColumnTextFrame>
		frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!IMultiColumnTextFrame");
			return retval;
		}

		InterfacePtr<ITextModel> objTxtMdl(frameItemTFC->QueryTextModel());
		if(!objTxtMdl)
		{
			//CA("!objTxtMdl" );
			return retval;
		}

		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);

		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		if (objXMLRefDat==nil)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::AcquireTag::objXMLRefDat==nil");
			break;
		}
		XMLReference xmlRef=objXMLRefDat->GetReference();
		UIDRef refUID=xmlRef.GetUIDRef();
		UID existingTagUID = kInvalidUID;
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement!=nil)
		{
			int elementCount=xmlElement->GetChildCount();
			PMString ORgTagString = xmlElement->GetTagString();

			if(tagName == ORgTagString)
            {
                existingTagUID = xmlElement->GetTagUID();

			}
			else
			{

				for(int i=0; i<elementCount; i++)
				{

					XMLReference elementXMLref=xmlElement->GetNthChild(i);
					InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
					if(childElement==nil)
						continue;

					PMString ChildtagName=childElement->GetTagString();
					if(tagName == ChildtagName)
                    {
						existingTagUID = childElement->GetTagUID();
						break;
					}

				}
			}
		}

		InterfacePtr<IXMLTagList> tagList(Utils<IXMLUtils>()->QueryXMLTagList(txtMdlUIDRef.GetDataBase()));
		ASSERT(tagList);
		if(!tagList) {
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::AcquireTag::!tagList");
			break;
		}

		if(existingTagUID == kInvalidUID)
        {
			UID createdTagUID = kInvalidUID;

			ErrorCode err = Utils<IXMLTagCommands>()->CreateTag (::GetUIDRef(tagList),
															WideString(tagName),
															kInvalidUID,
															&createdTagUID);
			if(err != kFailure){
				//CA("Create Tag Succesful");
			}
			ASSERT(err == kSuccess);
			ASSERT(createdTagUID != kInvalidUID);
			retval = UIDRef(::GetDataBase(tagList), createdTagUID);
			ISTagPresent = kFalse;

		}
        else
        {
			ISTagPresent = kTrue;
			retval = UIDRef(::GetDataBase(tagList), existingTagUID);
		}

	} while(kFalse);

	return retval;
}


//Method Purpose: Use to tag the given Table with tableTagName & CellTagName if the table is not tagged already
ErrorCode CTGCommonFunctions::AddTableAndCellElements(const UIDRef& tableModelUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								TagStruct &tStruct
								)
{
	ErrorCode err = kFailure;
    Mediator md;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework is nil");
		return err;
	}
	do {
		// + preconditions
		IDataBase* database = tableModelUIDRef.GetDataBase();
		UIDRef documentUIDRef1(database, database->GetRootUID());

		bool16 ISTagPresent = kFalse;
		UIDRef tablTagUIDRef = this->AcquireTag(tableModelUIDRef, tableTagName, ISTagPresent);
		bool16 ISTagPresent1 = kFalse;
		UIDRef cellTagUIDRef = this->AcquireTag(tableModelUIDRef, cellTagName, ISTagPresent1);

		if(!ISTagPresent)
		{  //CA("Creating Table Element");
			err = Utils<IXMLElementCommands>()->CreateTableElement ( WideString(tableTagName),
																			 WideString(cellTagName),
																			tableModelUIDRef,
																			&outCreatedXMLReference);

			if(err != kFailure)
			{//////CA("CreateTableElement fails");
                
                tStruct.isTablePresent = kTrue; // ListboxInfoData.tableFlag = 1;
				
				tStruct.languageID= md.languageID;
                tStruct.tableType = 2;
                tStruct.whichTab =4;
                tStruct.imgFlag =0;
                tStruct.imageIndex = -1;
                tStruct.flowDir = -1;
                

				//ListboxInfoData.name = tableTagName;
				//ListboxInfoData.index = 3;
				//ListboxInfoData.isImageFlag =0;
				//ListboxInfoData.listBoxType = listBoxType;

				this->attachAttributes(&outCreatedXMLReference, kFalse, tStruct);
			}

		}
		
	} while(kFalse);
	return err;
}


void CTGCommonFunctions::attachAttributes(XMLReference* newTag,bool16 isPRINTSourceTag, TagStruct &tStruct )
{
    bool16 flag=kFalse;
    flag = isPRINTSourceTag;
    Mediator md;
    
    PMString attribName("ID");
    PMString attribVal("");
    attribVal.AppendNumber(PMReal(tStruct.elementId));
    if(flag){
        attribVal="-1";
    }
    ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "typeId";
    attribVal.AppendNumber(PMReal(tStruct.typeId));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "header";
    attribVal.AppendNumber((tStruct.header));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isEventField";
    if(tStruct.isEventField)
        attribVal.AppendNumber(1);
    else
        attribVal.AppendNumber(-1);
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "deleteIfEmpty";
    attribVal.AppendNumber((tStruct.deleteIfEmpty));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "dataType";
    attribVal.AppendNumber((tStruct.dataType));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isAutoResize";
    attribVal="-1";
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "LanguageID";
    attribVal.AppendNumber(PMReal(md.languageID));
    
    if(flag){
        attribVal="NULL";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "index";
    attribVal.AppendNumber(4);
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "pbObjectId";
    attribVal.AppendNumber(PMReal(tStruct.pbObjectId));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "parentID";
    attribVal.AppendNumber(PMReal(tStruct.parentId));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "childId";
    attribVal.AppendNumber(PMReal(tStruct.childId));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "sectionID";
    attribVal.AppendNumber(PMReal(tStruct.sectionID));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "parentTypeID";
    attribVal.AppendNumber(PMReal(tStruct.parentTypeID));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "isSprayItemPerFrame";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "catLevel";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "imgFlag";
    attribVal.AppendNumber((tStruct.imgFlag));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "imageIndex";
    attribVal.AppendNumber((tStruct.imageIndex));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "flowDir";
    attribVal.AppendNumber((tStruct.flowDir));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "childTag";
    attribVal.AppendNumber((tStruct.childTag));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableFlag";
    if(tStruct.isTablePresent == 1)
        attribVal="1";
    else if(tStruct.isTablePresent == 0)
        attribVal="0";
    else
        attribVal="-1001";
    
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableType";
    attribVal.AppendNumber((tStruct.tableType));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "tableId";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "rowno";
    attribVal.AppendNumber((tStruct.rowno));
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "colno";
    attribVal.AppendNumber((tStruct.colno));
    if(flag)
    {
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal)); //Cs4
    attribName.Clear();
    attribVal.Clear();
    attribName = "field1";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field2";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field3";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field4";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "field5";
    attribVal="-1";
    if(flag){
        attribVal="-1";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "groupKey";
    attribVal.Append("");
    if(flag){
        attribVal="";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
}

//Method Purpose: Use to tag text inside the Cell of a Table.
ErrorCode CTGCommonFunctions::TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, PMString cellTagName, int32 startIndex, int32 endIndex)
 {//CA("new TagTableCellText");
		ErrorCode errCode=kFailure;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework is nil");
			return errCode;
		}
	 	/////////////////////////////////
		UIDRef TableUIDRef(::GetUIDRef(tableModel));
		InterfacePtr<ITableTextContent>tblTextContent(tableModel,UseDefaultIID());
		if(!tblTextContent)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source ITableTextContent in function is null");
			return kFailure ;
		}
		InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel()/*,UseDefaultIID()*/);
		if(!tblTextModel)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source Text Model in fucntion is null");
			return kFailure;
		}
		UIDRef ref2(::GetUIDRef(tblTextModel));

		/////////////////added by premal//////////////////////////

		InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
		if(!tblSelSuite)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source Table Selection suite is null");
			return kFailure;
		}
     
		/*
        tblSelSuite->Select(tableModel,gridArea,tblSelSuite->kAddTo,kTrue);
     
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Selection Manager is Null ");
				return kFailure;
		}
		InterfacePtr<ITblBscSuite>tableSuite(iSelectionManager,UseDefaultIID());
		if(!tableSuite)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Table Suite is null");
			return kFailure;
		}
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2)
		if(!pTextSel)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source IConcrete Selection is null");
			return kFailure;
		}

		InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
		if(!tblTxtSel)
		{
			ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText::Source Table Text Selection is null");
			return kFailure;
		}

		tableSuite->GetCellRange(gridArea);
		RowRange rRange;
		rRange=gridArea.GetRows();
		PMString msg("Rows: ");
		msg.AppendNumber(rRange.start);
		ColRange cRange;
		cRange=gridArea.GetCols();

		GridAddress gadr(rRange.start,cRange.start);
      */
		//tblTxtSel->SelectTextInCell(tableModel,gadr);
		//int32 firstCharIndex,lastCharIndex;
		//firstCharIndex=tblTxtSel->GetIndexOfFirstCharInCell();
		//lastCharIndex=tblTxtSel->GetIndexOfLastCharInCell();
		UIDRef tableTextModelUIDRef(::GetUIDRef(tblTextModel));
    		cellTagName = this->prepareTagName(cellTagName);
        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGCommonFunctions::TagTableCellText:: "+ cellTagName);
     
     
     PMString ASD("startIndex = ");
     ASD.AppendNumber(startIndex);
     ASD.Append(" endIndex = ");
     ASD.AppendNumber(endIndex);
     ptrIAppFramework->LogDebug("TagTableCellText Range: "+ ASD);
     
     errCode=Utils<IXMLElementCommands>()->CreateElement(WideString(cellTagName),tableTextModelUIDRef, startIndex, endIndex, kInvalidXMLReference,&CellTextxmlRef);

     
	 return errCode;
 }


void CTGCommonFunctions::addTagToGraphicFrame(UIDRef curBox, PublicationNode &localPNode, Attribute &attributeObj)
{
    //CA("TPLCommonFunctions::addTagToGraphicFrame");
    do{
        Mediator md;
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil){
            //CA("ptrIAppFramework is nil");
            break;
        }
        
        InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
        if(unknown == nil){
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CTGCommonFunctions::addTagToGraphicFrame::unknown nil");
            break;
        }
        InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
        if(doc == nil){
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CTGCommonFunctions::addTagToGraphicFrame::Document not found...");
            break;
        }
        InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
        if (rootElement == nil){
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CTGCommonFunctions::addTagToGraphicFrame::IIDXMLElement NIL...");
            break;
        }
        
        XMLReference parent = rootElement->GetXMLReference();
        PMString storyTagName = this->prepareTagName(attributeObj.getDisplayName());
        
        XMLReference resultXMLRef=kInvalidXMLReference;
        UID frameUID = curBox.GetUID();
    
        ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement( WideString(storyTagName), frameUID, parent, 0, &resultXMLRef);
        // Verify the results: no errors, valid XMLRef returned, we can instantiate it.
        if (errCode != kSuccess)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - CreateElement failed");
            break;
        }
        if (resultXMLRef == kInvalidXMLReference)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
            break;
        }
        InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
        if (newXMLElement==nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::TagFrameElement::ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
        }
    
        TagStruct imageFrameTag;
        imageFrameTag.elementId = attributeObj.getAttributeId();
        imageFrameTag.typeId = -1;
        imageFrameTag.header = -1;
        imageFrameTag.isEventField = -1;
        imageFrameTag.deleteIfEmpty = -1;
        imageFrameTag.dataType = 1;
        imageFrameTag.isAutoResize = -1;
        imageFrameTag.languageID = md.languageID;
        imageFrameTag.whichTab = 4;
        imageFrameTag.pbObjectId = localPNode.getPBObjectID();
        imageFrameTag.parentId = localPNode.getPubId();
        imageFrameTag.childId = -1;
        imageFrameTag.sectionID = localPNode.getSectionID();
        imageFrameTag.parentTypeID = localPNode.getTypeId();
        imageFrameTag.isSprayItemPerFrame = -1;
        imageFrameTag.catLevel = -1;
        imageFrameTag.imgFlag = 1;
        imageFrameTag.imageIndex = 1;
        imageFrameTag.flowDir = 0;
        imageFrameTag.childTag = -1;
        imageFrameTag.isTablePresent = kFalse;
        
        
        // Following stmt actually attaches the tag.
        this->attachAttributes(&resultXMLRef, kFalse ,imageFrameTag);
    }while(kFalse);
}
