#ifndef __CTGCOMMONFUNCTIONS_H__
#define __CTGCOMMONFUNCTIONS_H__

#include "VCPlugInHeaders.h"
#include "IIDXMLElement.h"
#include "IXMLreferenceData.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "ITableModel.h"
#include "PublicationNode.h"
#include "Attribute.h"
#include "TagStruct.h"

class CTGCommonFunctions
{
public:
	//int16 deleteThisBox(UIDRef);
	//int16 convertBoxToTextBox(UIDRef);
	XMLReference TagFrameElement(const XMLReference& newElementParent, UID frameUID ,const PMString& frameTagName, PublicationNode &localPNode, Attribute &attributeObj, bool16 isPRINTsourceTag);
	PMString prepareTagName(PMString);
	PMString RemoveWhiteSpace(PMString);
    void attachAttributes(XMLReference* newTag,bool16 isPRINTSourceTag, PublicationNode &localPNode,Attribute &attribute);
    
	//void addTagToGraphicFrame(UIDRef,int32,int32);
	//void addTagToText(UIDRef,int32,int32,PMString);
	//int16 appendTextIntoBox(UIDRef,PMString,bool16);
	//int16 embedBoxIntoBox(UIDRef,UIDRef );
	//PBPMPoint GetActiveReferencePoint(void);
	//int16 getSelectionFromLayout(UIDRef&);
	//void insertOrAppend(int32,PMString,int32,int32);
	//int16 getBoxDimensions(UIDRef,PMRect&);
	//int16 checkForOverLaps(UIDRef,int32,UIDRef&,UIDList&);
	//int16 refreshLstbox(UID);
	//void updateIcon(IControlView*,int32,bool16);
	//int16 createNewTable(void);
	//int changeMode(int whichMode);
	//ITool* queryTool(const ClassID& toolClass);
	//int16 appendNewTable(UIDRef);
	//int16 embedOrAppendTable(int16,int32,int32);
	//int16 getTableUIDOfSelBox(UIDRef &selBoxUIDRef);
	//bool16 getFrameTagChkboxState();
	//bool16 checkIfFrameIsTagged();
	//bool16 getAutoResizeChkboxState();
	//bool16 getOverflowChkboxState();

	UIDRef AcquireTag(const UIDRef& documentUIDRef,const PMString& tagName, bool16 &ISTagPresent);
    ErrorCode TagTable(const UIDRef& tableModelUIDRef,
                                           UIDRef& BoxRef,
                                           const PMString& tableTagName,
                                           const PMString& cellTagName,
                                           XMLReference& outCreatedXMLReference,
                                           PublicationNode &localPNode,
                                           Attribute &attributeObj,
                                           int32 startIndex,
                                           int32 endIndex);
	//overloaded TagTable function
	//ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef& BoxRef,
	//							const PMString& tableTagName,
	//							const PMString& cellTagName,
	//							XMLReference& outCreatedXMLReference, TPLListboxInfo&);
	
	
	ErrorCode TagStory(const PMString& tagName,
									const UIDRef& textModelUIDRef);
	//ErrorCode TagCurrentCell(const XMLReference& xmlRef);
    ErrorCode AddTableAndCellElements(const UIDRef& tableModelUIDRef,
                                        const PMString& tableTagName,
                                        const PMString& cellTagName,
                                        XMLReference& outCreatedXMLReference,
                                        TagStruct &tStruct
                                      );
//	ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,
//								XMLReference &parentXMLRef, GridID id,
//								GridArea gridArea, XMLReference &CellTextxmlRef, TPLListboxInfo&, int32 startTextIndex);
	//Overloaded TagTableCellText function
    
	ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef,             PMString cellTagName, int32 startIndex, int32 endIndex);


	//void attachAttributes(XMLReference* newTag,bool16 IsPrintSourceTag, TPLListboxInfo lstboxInfo,double rowno, double colno);

	PMString keepOnlyAlphaNumeric(PMString name);

	//void insertOrAppendOnDoubleClick(int32,PMString,int32);

    void addTagToTextDoubleClickVersion(UIDRef curBox, PublicationNode &localPNode ,Attribute &attributeObj,int32 startIndex, int32 endIndex);

	//void appendTextIntoSelectedAndOverlappedBox(UIDRef,PMString);

	//void checkForOverlapWithTextFrame(PMPoint);

	//bool16 checkForOverlapWithMasterFrameFunction(UIDRef);

	//void addTagToPVBoxDoubleClickVersion(UIDRef curBox,int32 selIndex,int32 selTab,PMString result);

	//ErrorCode CreateTable(const UIDRef& storyRef,
	//									const TextIndex at,
	//									  const int32 numRows,
	//									  const int32 numCols,
	//									  const PMReal rowHeight,
    //                                      const PMReal colWidth,
	//									  const CellType cellType = kTextContentType);
    
	//bool16 InsertTableInsideTableCellAndApplyTag(TPLListboxInfo lstboxInfo,UIDRef ref);

	//void appendTableNameAndTable(int32 selectedRowIndex, PMString theContent, int32 whichTab);
    
    void attachAttributes(XMLReference* newTag,bool16 isPRINTSourceTag, TagStruct &tStruct );
    
    void addTagToGraphicFrame(UIDRef curBox, PublicationNode &localPNode, Attribute &attributeObj);

};




#endif
