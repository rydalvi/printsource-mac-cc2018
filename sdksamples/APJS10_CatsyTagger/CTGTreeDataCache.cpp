#include "VCPlugInHeaders.h"
#include "CTGTreeDataCache.h"
#include "CAlert.h"
#include "vector"

map<int32, PublicationNode>* CTGTreeDataCache::dataCache=NULL;

CTGTreeDataCache::CTGTreeDataCache()
{
	if(dataCache)
		return;
	dataCache=new map<int32, PublicationNode>;
}

bool16 CTGTreeDataCache::getAllIdForLevel(int level, int32& numIds, vector<int32>& idList)
{
	int32 flag=0;
	PublicationNode pNode;
	if(!dataCache)
	{
		dataCache=new map<int32, PublicationNode>;
		return kFalse;
	}
	map<int32, PublicationNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		/*if(pNode.getLevel()==level)
		{
			flag++;
			idList.push_back(pNode.getPubId());
		}*/
	}
	numIds=flag;
	return kTrue;
}

bool16 CTGTreeDataCache::isExist(int32 id, PublicationNode& pNode)
{
	if(!dataCache)
	{
		dataCache=new map<int32, PublicationNode>;
		return kFalse;
	}

	map<int32, PublicationNode>::iterator mapIterator;

	mapIterator=dataCache->find(id);
	if(mapIterator==dataCache->end())
		return kFalse;

	pNode=(*mapIterator).second;
	pNode.setHitCount(pNode.getHitCount()+1);//Increase hit count by 1
	return kTrue;
}

bool16 CTGTreeDataCache::add(PublicationNode& pNodeToAdd)
{
	if(!dataCache)
		dataCache=new map<int32, PublicationNode>;
	map<int32, PublicationNode>::iterator mapIterator;
	PublicationNode firstNode, anyNode;
	int32 removalId=-1;

	if((dataCache->size()+1)<dataCache->max_size())//Cache has some space left
	{
		mapIterator=dataCache->find(pNodeToAdd.getTreeNodeId());
		if(mapIterator==dataCache->end())//Not found. Insert it!!
		{
			dataCache->insert(map<int32, PublicationNode>::value_type(pNodeToAdd.getTreeNodeId(), pNodeToAdd));
			return kTrue;
		}
		//Node exists...Increase the hit count
		firstNode=(*mapIterator).second;
		(*mapIterator).second.setHitCount(firstNode.getHitCount()+1);
		return kTrue;
	}

	CAlert::ErrorAlert("System is low on resources. Please close some applications and proceed.");

	//We do not have any space left...Remove the element which is MOST accessed

	mapIterator=dataCache->begin();
	firstNode=(*mapIterator).second;

	removalId=firstNode.getTreeNodeId();
	
	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		anyNode=(*mapIterator).second;

		if(anyNode.getHitCount()>MAX_HITS_BEFORE_STALE)//Save some iterations
		{
			removalId=anyNode.getTreeNodeId();			
			break;
		}
		if(anyNode.getHitCount()>firstNode.getHitCount())
			removalId=anyNode.getTreeNodeId();			
	}
	dataCache->erase(removalId);
	dataCache->insert(map<int32, PublicationNode>::value_type(pNodeToAdd.getTreeNodeId(), pNodeToAdd));
	return kTrue;
}

bool16 CTGTreeDataCache::clearMap(void)
{
	if(!dataCache)
		return kFalse;
	//CA("deleting dataCache");
	dataCache->erase(dataCache->begin(), dataCache->end());
	dataCache->clear();
	delete dataCache;
	dataCache=NULL;
	return kTrue;
}

bool16 CTGTreeDataCache::isExist(int32 parentId, int32 sequence, PublicationNode& pNode)
{
	int flag=0;
	if(!dataCache)
	{
		dataCache=new map<int32, PublicationNode>;
		return kFalse;
	}
	map<int32, PublicationNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		if(pNode.getTreeParentNodeId()==parentId)
		{
			if(pNode.getSequence()==sequence)
			{
				pNode=(*mapIterator).second;
				(*mapIterator).second.setHitCount(pNode.getHitCount()+1);
				flag=1;
				break;
			}
		}
	}

	if(!flag)
		return kFalse;
	return kTrue;
}
