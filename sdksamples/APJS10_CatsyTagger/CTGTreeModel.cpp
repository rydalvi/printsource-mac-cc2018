#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "CTGID.h"
#include "CTGTreeModel.h"
#include "CAlert.h"
#include "MediatorClass.h"
#include "PublicationNode.h"
#include "CTGTreeDataCache.h"
//#include "GlobalData.h"


#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("CTGTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X);//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

CTGTreeDataCache dc;

int32 CTGTreeModel::root=0;
PMString CTGTreeModel::publicationName;
double CTGTreeModel::classId=0;
//extern TPLDataNodeList ProjectDataNodeList;
extern PublicationNodeList pNodeDataList;

int32 RowCountForTree = -1;
//extern int32 SelectedRowNo;
static int32 NodeCount =0;


CTGTreeModel::CTGTreeModel()
{
	//root=-1;
	//classId=11002;
}

CTGTreeModel::~CTGTreeModel() 
{
}


int32 CTGTreeModel::GetRootUID() const
{	
	if(classId<=0)
		return 0;// kInvalidUID;
	
	PublicationNode pNode1;
	if(dc.isExist(root, pNode1))
	{
		return root;
	}
	
	dc.clearMap();
	
	
	PublicationNodeList CurrentNodeList;
	CurrentNodeList.clear();

    NodeCount =0;
	CurrentNodeList = pNodeDataList;
    
    int32 childCount = 0;
    for(int i =0 ; i<CurrentNodeList.size(); i++)
    {
        if(CurrentNodeList[i].getTreeParentNodeId() == root)
            childCount++;
    }

	RowCountForTree = 0;
	PMString name("Root");
	pNode1.setPubId(root);
	pNode1.setChildCount(childCount);
	/*pNode1.setLevel(0);*/
	pNode1.setParentId(-2);
	pNode1.setPublicationName(name);
	pNode1.setSequence(0);
    pNode1.setTreeNodeId(root);
    pNode1.setTreeParentNodeId(-2);
    
	dc.add(pNode1);
    NodeCount++;
    
    //CA_NUM("Child Count:", pNode1.getChildCount());
	
	int count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode1.setLevel(1);*/
				pNode1 = CurrentNodeList[i];
        
                if(CurrentNodeList[i].getTreeParentNodeId() != root)
                    continue;
        
                pNode1.setTreeNodeId(CurrentNodeList[i].getTreeNodeId());
                pNode1.setTreeParentNodeId(CurrentNodeList[i].getTreeParentNodeId());
                //CA_NUM("NodeCount:", NodeCount);
				pNode1.setParentId(CurrentNodeList[i].getParentId());
				pNode1.setSequence(count);
				count++;
				pNode1.setPubId(CurrentNodeList[i].getPubId());
				PMString ASD(CurrentNodeList[i].getName());
				pNode1.setPublicationName(ASD);
				//CA(ASD);
                //CAlert::InformationAlert(ASD);
				pNode1.setChildCount(CurrentNodeList[i].getChildCount());
                pNode1.setHitCount(CurrentNodeList[i].getHitCount());
				pNode1.setIsProduct(CurrentNodeList[i].getIsProduct());
				pNode1.setDesignerAction(CurrentNodeList[i].getDesignerAction());
				pNode1.setIconCount(CurrentNodeList[i].getIconCount());
				pNode1.setIsONEsource(CurrentNodeList[i].getIsONEsource());
				pNode1.setChildItemCount(CurrentNodeList[i].getChildItemCount());
				pNode1.setPBObjectID(CurrentNodeList[i].getPBObjectID());
				pNode1.setSectionID(CurrentNodeList[i].getSectionID());
				pNode1.setPublicationID(CurrentNodeList[i].getPublicationID());
                NodeCount++;
				
			dc.add(pNode1);
	}
	return root;
}


int32 CTGTreeModel::GetRootCount() const
{
	int32 retval=0;
	PublicationNode pNode1;
	if(dc.isExist(root, pNode1))
	{
		return pNode1.getChildCount();
	}
	return 0;
}

int32 CTGTreeModel::GetChildCount(const int32& uid) const
{
	PublicationNode pNode1;

	if(dc.isExist(uid, pNode1))
	{
		return pNode1.getChildCount();
	}
	return -1;
}

int32 CTGTreeModel::GetParentUID(const int32& uid) const
{
	PublicationNode pNode1;

	if(dc.isExist(uid, pNode1))
	{
		if(pNode1.getTreeParentNodeId()==-2)
			return 0; //kInvalidUID;
		return pNode1.getTreeParentNodeId();
	}
	return 0; //kInvalidUID;
}


int32 CTGTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	PublicationNode childpNode;
    PublicationNode parentpNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, childpNode))
    {
		return childpNode.getSequence();
    }
    else
    {
        if(dc.isExist(parentUID, parentpNode))
        {
            PublicationNodeList CurrentNodeList = pNodeDataList;
            int count=0;
            for(int i =0 ; i<CurrentNodeList.size(); i++)
            { //CAlert::InformationAlert(" 123");
                /*pNode1.setLevel(1);*/
                PublicationNode pNode1 = CurrentNodeList[i];
                
                if(CurrentNodeList[i].getTreeParentNodeId() != parentpNode.getTreeNodeId())
                    continue;
                if(childUID == CurrentNodeList[i].getTreeNodeId())
                    retval = count;
                
                pNode1.setTreeNodeId(CurrentNodeList[i].getTreeNodeId());
                pNode1.setTreeParentNodeId(CurrentNodeList[i].getTreeParentNodeId());
                //CA_NUM("NodeCount:", NodeCount);
                pNode1.setParentId(CurrentNodeList[i].getParentId());
                pNode1.setSequence(count);
                count++;
                pNode1.setPubId(CurrentNodeList[i].getPubId());
                PMString ASD(CurrentNodeList[i].getName());
                pNode1.setPublicationName(ASD);
                //CA(ASD);
                //CAlert::InformationAlert(ASD);
                pNode1.setChildCount(CurrentNodeList[i].getChildCount());
                pNode1.setHitCount(CurrentNodeList[i].getHitCount());
                pNode1.setIsProduct(CurrentNodeList[i].getIsProduct());
                pNode1.setDesignerAction(CurrentNodeList[i].getDesignerAction());
                pNode1.setIconCount(CurrentNodeList[i].getIconCount());
                pNode1.setIsONEsource(CurrentNodeList[i].getIsONEsource());
                pNode1.setChildItemCount(CurrentNodeList[i].getChildItemCount());
                pNode1.setPBObjectID(CurrentNodeList[i].getPBObjectID());
                pNode1.setSectionID(CurrentNodeList[i].getSectionID());
                pNode1.setPublicationID(CurrentNodeList[i].getPublicationID());
                NodeCount++;
                
                dc.add(pNode1);
            }

            
        }
        
    }
	return retval;
}


int32 CTGTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
    PublicationNode childpNode;
    PublicationNode parentpNode;
    int32 retval= 0;
    

	if(dc.isExist(parentUID, index, childpNode))
    {
		return childpNode.getTreeNodeId();
    }
    else
    {
        if(dc.isExist(parentUID, parentpNode))
        {
            int count=0;
            PublicationNodeList CurrentNodeList = pNodeDataList;
            
            for(int i =0 ; i<CurrentNodeList.size(); i++)
            { //CAlert::InformationAlert(" 123");
                /*pNode1.setLevel(1);*/
                PublicationNode pNode1 = CurrentNodeList[i];
                
                if(CurrentNodeList[i].getTreeParentNodeId() != parentpNode.getTreeNodeId())
                    continue;
                
                if(index == count)
                    retval = pNode1.getTreeNodeId();
                
                pNode1.setTreeNodeId(CurrentNodeList[i].getTreeNodeId());
                pNode1.setTreeParentNodeId(CurrentNodeList[i].getTreeParentNodeId());
                //CA_NUM("NodeCount:", NodeCount);
                pNode1.setParentId(CurrentNodeList[i].getParentId());
                pNode1.setSequence(count);
                count++;
                pNode1.setPubId(CurrentNodeList[i].getPubId());
                PMString ASD(CurrentNodeList[i].getName());
                pNode1.setPublicationName(ASD);
                //CA(ASD);
                //CAlert::InformationAlert(ASD);
                pNode1.setChildCount(CurrentNodeList[i].getChildCount());
                pNode1.setHitCount(CurrentNodeList[i].getHitCount());
                pNode1.setIsProduct(CurrentNodeList[i].getIsProduct());
                pNode1.setDesignerAction(CurrentNodeList[i].getDesignerAction());
                pNode1.setIconCount(CurrentNodeList[i].getIconCount());
                pNode1.setIsONEsource(CurrentNodeList[i].getIsONEsource());
                pNode1.setChildItemCount(CurrentNodeList[i].getChildItemCount());
                pNode1.setPBObjectID(CurrentNodeList[i].getPBObjectID());
                pNode1.setSectionID(CurrentNodeList[i].getSectionID());
                pNode1.setPublicationID(CurrentNodeList[i].getPublicationID());
                NodeCount++;
                
                dc.add(pNode1);
            }
            
            
        }
        
    }
    
	return retval; // kInvalidUID;
}


int32 CTGTreeModel::GetNthRootChild(const int32& index) const
{
	PublicationNode pNode1;
	if(dc.isExist(root, index, pNode1))
	{
		return pNode1.getTreeNodeId();
	}
	return 0; //kInvalidUID;
}

int32 CTGTreeModel::GetIndexForRootChild(const int32& uid) const
{
	PublicationNode pNode1;
	if(dc.isExist(uid, pNode1))
		return pNode1.getSequence();
	return -1;
}

PMString CTGTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	PublicationNode pNode1;
	PMString name("");

	if(dc.isExist(uid, pNode1))
	{	
		*Rowno= pNode1.getSequence();
		//*Rowno = pNode1.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode1.getHitCount());
		CA(ASD);*/
		return pNode1.getName();
	}
	return name;
}


int32 CTGTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	PublicationNode pNode1;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode1))
		return (1);
	return -1;
}
