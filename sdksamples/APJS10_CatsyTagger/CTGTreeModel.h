#ifndef __CTGTREEMODEL_H
#define __CTGTREEMODEL_H

class IHierarchy;

/** Helper class to mediate between the tree-view adapter and the document 
	object model. The root of tree is the spreadlist. 

	It's probably better coding practice to factor the model to be independent 
	of the adapter and provide an API like this one here rather than place the 
	burden on the adapter class itself of mapping into the tree-model of 
	interest. It is certainly easier to test.
*/

class CTGTreeModel
{
public:
	CTGTreeModel();
	virtual ~CTGTreeModel();
	/**
		Accessor for the int32 of the root of the tree model. In this
		case we've set the spread-list (on kDocBoss) to be the root of the tree.
	*/
	int32	GetRootUID() const;
	/** Determine how many children the root (spreadlist) has, equal
		to the count of spreads.
		@return number of children that the root node has
	*/
	int32 GetRootCount() const;
	/** Determine the index of the given int32 in the root's kids, i.e. what's the
		index of a spread with given int32?
		@param uid
		@return index of child in list of children of root, (-1) if not child of root
	*/
	int32 GetIndexForRootChild(const int32& uid) const;
	/** Determine the int32 of the child of the root node (spreadlist) by index, i.e.
		answer the question "what's the int32 of the spread at given index?".
		@param index
		@return int32 of the child at given index, kInvalidUID if not child at specified index
		
	*/
	int32 GetNthRootChild(const int32& index) const;
	/**
		Return count of children on node with given int32.
		@param uid data on node of interest
		@return child count for given node
	*/
	int32 GetChildCount(const int32& uid) const;
	/**
		Determine the int32 of the parent of given int32.
		@param uid specifies the item of interest
		@return int32 of parent if it can be found or kInvalidUID otherwise (for instance, if uid refers to the root)
	*/
	int32 GetParentUID(const int32& uid) const;
	/**
		Determine the index of a child with given int32
		@param parentUID gives int32 of the parent
		@param childUID givesn the int32 of child that we're seeking index in parent list of kids for
		@return index in the parent's children of the given child, (-1) if failed to find child
	*/
	int32 GetChildIndexFor(const int32& parentUID, const int32& childUID) const;
	/** Determine the int32 of child of a given parent by index
		@param parentUID
		@param index
		@return int32 of child at given index
	*/
	int32 GetNthChildUID(const int32& parentUID, const int32& index) const;
	/** Convert a int32 into something that can be displayed in a tree-view.
		The debug build shows the class of the boss object associated with this int32,
		and the release build just displays the int32 since the class info is not available.

		@param uid unique identifier for document object
		@return string based representation of the data given	
	*/
	PMString ToString(const int32& uid, int32 *Rowno) const;
	/** Discover how far from the root the node with this int32 as data is. 
		For instance, the child of the root will return 1, the grandchildren 2 and so on.

		@param uid specifies the data at the node of interest
		@return how many paths to hop in tree to get to the root
	*/
	int32 GetNodePathLengthFromRoot(const int32& uid);
	void setRoot(int32 val, PMString& pubName, int32 cid) { root=val; publicationName=pubName; classId=cid; }
    
    void setCurrentSectionId(double curSectionId) {this->CurrentSectionId = curSectionId; }
    double getCurrentSectionId() const { return this->CurrentSectionId;}

private:
	static int32 root;
	static double classId;//Actually the pnode (Item / ItemGroup/ LeadingItem) id
	static PMString publicationName;
    static double CurrentSectionId;
    
};

#endif

