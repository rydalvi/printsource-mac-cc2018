#include "VCPluginHeaders.h"
#include "PMString.h"
#include "SectionData.h"
#include "vector"

using namespace std;

vector<PubData> SectionData::sectionInfoVector;
vector<PubData> SectionData::attributeInfoVector;

void SectionData::setSectionVectorInfo(double id,PMString name,int32 lvl, double rootID, double typeID, PMString comment){
	PubData publdata;
	publdata.setPubId(id);
	publdata.setPubName(name);
	publdata.setPubLvlNo(lvl);
	publdata.setroot_ID(rootID);
	publdata.settype_ID(typeID);
	publdata.setComment(comment);
	sectionInfoVector.push_back(publdata);
}

PubData SectionData::getSectionVectorInfo(int32 index){
	return sectionInfoVector[index];
}

void SectionData::clearSectionVector(void){
	sectionInfoVector.erase(sectionInfoVector.begin(),sectionInfoVector.end());
	//sectionInfoVector.clear();
}

void SectionData::setAttributeVectorInfo(double id,PMString name,int32 lvl, double rootID, double typeID, PMString comment){
	PubData publdata;
	publdata.setPubId(id);
	publdata.setPubName(name);
	publdata.setPubLvlNo(lvl);
	publdata.setroot_ID(rootID);
	publdata.settype_ID(typeID);
	publdata.setComment(comment);
	attributeInfoVector.push_back(publdata);
}

PubData SectionData::getAttributeVectorInfo(int32 index){
	return attributeInfoVector[index];
}

void SectionData::clearAttributeVector(void){
	attributeInfoVector.erase(attributeInfoVector.begin(),attributeInfoVector.end());
	//subSectionInfoVector.clear();
}
