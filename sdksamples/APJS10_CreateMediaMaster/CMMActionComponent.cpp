//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "CMMDialogObserver.h"
// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IActionStateList.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#include "CMMActionComponent.h"
#include "MediatorClass.h"
#include "ExportFileHelper.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "CMMID.h"
#include "AFWJSID.h"
// PrintSource4.5 Interface include
#include "IAppFramework.h"
#include "ITagReader.h"
#include "ISpecialChar.h"
#include "IClientOptions.h"
#include "IDataSprayer.h"



/// Global Pointers
ITagReader* itagReader = NULL;
IAppFramework* ptrIAppFramework = NULL;
ISpecialChar*  iConverter = NULL;
IClientOptions* ptrIClientOptions = NULL;
IDataSprayer* DataSprayerPtr = NULL;
///////////


//macros
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMActionComponent.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

//added on 7Nov..
bool16 IsTheBookBlank();
//ended on 7Nov..

//IDialog* dialog1;


/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs10_createmediamaster

*/
/*
class CMMActionComponent : public CActionComponent
{
public:

// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
 
		CMMActionComponent(IPMUnknown* boss);

//        The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil.

		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		void UpdateActionStates (IActionStateList* iListPtr);
		//static void DoCMMSpraySettingsDialog();
	private:
//		Encapsulates functionality for the about menu item.
		void DoAbout();
		
//		Opens this plug-in's dialog.
		void DoDialog();
		void DoCMMSpraySettingsDialog();



};
*/

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CMMActionComponent, kCMMActionComponentImpl)

/* CMMActionComponent Constructor
*/
CMMActionComponent::CMMActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void CMMActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kCMMAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kCMMDialogActionID:
		case kCMMBookActionID:
		{
			//CA("hi");
			//if(IsTheBookBlank())
			//{
				this->DoDialog();
				//CA("hi 2");
			//}
			//else
			//{
			//	CAlert::InformationAlert("The book is not blank.");
			//}
			//this->DoCMMSpraySettingsDialog();
			break;
		}

		case kWhiteBoardRefreshActionID:
		{
			CMMDialogObserver dialogObserver(widget);
			dialogObserver.refreshWhiteBoardMedia();
			break;
		}
		
		case kCreatePDFsActionID:
		{
			//ExportFileHelper pdfExport;
			//pdfExport.exportEachDocInBookAsPDF();
			break;
		}

		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void CMMActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kCMMAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void CMMActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kCMMPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			/*kSDKDefDialogResourceID*/kCMMSpraySettingsDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		
		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec,  IDialog::kModeless /*kMovableModal*/);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}



//  Code generated by DollyXs code generator

void /*CMMActionComponent:: */DoCMMSpraySettingsDialog()
{
	//CA("DoCMMSpraySettingsDialog 1");
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //CS4
		if (application == nil) 
		{	
			//CA("application == nil");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil) 
		{
			//CA("dialogMgr == nil");
			break;
		}
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kCMMPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSpraySettingsDialogResourceID/*kCMMSpraySettingsDialogResourceID*//*kSDKDefDialogResourceID*/,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);
		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog * dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal,kTrue,kFalse);
		if (dialog == nil) 
		{
			//CA("dialog == nil");
			break;
		}
		// Open the dialog.
		dialog->Open();
	
	} while (false);

}



//*** This Function Not Needed More
void CMMActionComponent::UpdateActionStates (IActionStateList* iListPtr)
{	
	//CA("CMMActionComponent::UpdateActionStates ");
	//do
	//{
	//	if(ptrIAppFramework == NULL)
	//	{  			
	//		IAppFramework* ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//		if(ptrIAppFrameworkOne == nil)
	//			break;
	//		ptrIAppFramework = ptrIAppFrameworkOne;
	//	}

	//	if(iConverter == NULL)
	//	{	
	//		ISpecialChar* iConverterOne(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//		if(!iConverterOne)
	//		{	
	//			break;
	//		}
	//		iConverter = iConverterOne;
	//	}

	//	if(ptrIClientOptions == NULL)
	//	{	
	//		IClientOptions* ptrIClientOptionsOne((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//		if(ptrIClientOptionsOne==nil)
	//		{
	//			CAlert::ErrorAlert("Interface for IClientOptions not found.");
	//			break;
	//		}
	//		ptrIClientOptions = ptrIClientOptionsOne;
	//	}

	//	if(itagReader ==NULL)
	//	{	
	//		ITagReader* itagReaderOne
	//			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//		if(!itagReaderOne)
	//			break;
	//		itagReader = itagReaderOne;
	//	}		

	//	if(DataSprayerPtr == NULL)
	//	{	
	//		IDataSprayer* DataSprayerPtrOne((static_cast<IDataSprayer*>(CreateObject(kDataSprayerBoss, IID_IDataSprayer))));
	//		if(!DataSprayerPtrOne)
	//		{
	//			ptrIAppFramework->LogError("AP46CreateMedia::UpdateActionStates::Pointre to DataSprayerPtr not found");//
	//			break;
	//		}
	//		DataSprayerPtr = DataSprayerPtrOne;
	//	}
	//	
	//}while(0);


	////CA("Inside UpdateActionStates");
	////InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	CA("ptrIAppFramework == nil");
	//	return;
	//}

	//bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();

	//for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	//{
	//	ActionID actionID = iListPtr->GetNthAction(iter);		
	//	
	//	if(actionID == kCMMBookActionID)
	//	{ 	
	//		if(result)
	//			iListPtr->SetNthActionState(iter,kEnabledAction);
	//		else
	//			iListPtr->SetNthActionState(iter,kDisabled_Unselected);
	//	}
	//	if(actionID == kWhiteBoardRefreshActionID)
	//	{
	//		if(result && !IsTheBookBlank())
	//			iListPtr->SetNthActionState(iter,kEnabledAction);
	//		else
	//			iListPtr->SetNthActionState(iter,kDisabled_Unselected);
	//	}
	//	if(actionID == kCreatePDFsActionID)
	//	{
	//		if(result && !IsTheBookBlank())
	//			iListPtr->SetNthActionState(iter,kEnabledAction);
	//		else
	//			iListPtr->SetNthActionState(iter,kDisabled_Unselected);
	//	}
	//}
}


//*** Added For Cs4 
void    CMMActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList *iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{
	do
	{
		if(ptrIAppFramework == NULL)
		{  			
			IAppFramework* ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFrameworkOne == nil)
				break;
			ptrIAppFramework = ptrIAppFrameworkOne;
		}

		if(iConverter == NULL)
		{	
			ISpecialChar* iConverterOne(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverterOne)
			{	
				break;
			}
			iConverter = iConverterOne;
		}

		if(ptrIClientOptions == NULL)
		{	
			IClientOptions* ptrIClientOptionsOne((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptionsOne==nil)
			{
				CAlert::ErrorAlert("Interface for IClientOptions not found.");
				break;
			}
			ptrIClientOptions = ptrIClientOptionsOne;
		}

		if(itagReader ==NULL)
		{	
			ITagReader* itagReaderOne
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReaderOne)
				break;
			itagReader = itagReaderOne;
		}		

		if(DataSprayerPtr == NULL)
		{	
			IDataSprayer* DataSprayerPtrOne((static_cast<IDataSprayer*>(CreateObject(kDataSprayerBoss, IID_IDataSprayer))));
			if(!DataSprayerPtrOne)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::UpdateActionStates::Pointre to DataSprayerPtr not found");//
				break;
			}
			DataSprayerPtr = DataSprayerPtrOne;
		}
		
	}while(0);


	//CA("Inside UpdateActionStates");
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework == nil");
		return;
	}

	bool16 result=ptrIAppFramework->getLoginStatus();

	for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	{
		ActionID actionID = iListPtr->GetNthAction(iter);		
		
		if(actionID == kCMMBookActionID)
		{ 	
			if(result)
				iListPtr->SetNthActionState(iter,kEnabledAction);
			else
				iListPtr->SetNthActionState(iter,kDisabled_Unselected);
		}
		if(actionID == kWhiteBoardRefreshActionID)
		{
			if(result && !IsTheBookBlank())
				iListPtr->SetNthActionState(iter,kEnabledAction);
			else
				iListPtr->SetNthActionState(iter,kDisabled_Unselected);
		}
		if(actionID == kCreatePDFsActionID)
		{
			if(result && !IsTheBookBlank())
				iListPtr->SetNthActionState(iter,kEnabledAction);
			else
				iListPtr->SetNthActionState(iter,kDisabled_Unselected);
		}
	}


}
