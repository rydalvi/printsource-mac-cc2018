//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/wlistboxcomposite/WLBCmpListBoxObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: jbond $
//  
//  $DateTime: 2005/03/16 15:42:00 $
//  
//  $Revision: #2 $
//  
//  $Change: 326153 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "CMMID.h"
//-#include "SDKListBoxHelper.h"
#include "SDKFileHelper.h"

#include "IWidgetParent.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITriStateControlData.h"

#include "IEventUtils.h"
#include "Utils.h"

#include "MediatorClass.h"
#include "CMMActionComponent.h"

//for template file validation
#include "SDKUtilities.h"
#include "FileUtils.h"

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMCmpTemplateFileListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
/**

	Observes the listbox via IID_ILISTCONTROLDATA.

	@ingroup wlistboxcomposite
	
*/
extern 	int no_of_lstboxElements;
//forward declaration
void DoCMMSpraySettingsDialog();
class CMMTemplateFileListBoxObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	CMMTemplateFileListBoxObserver(IPMUnknown *boss);

	/**
		Destructor for WLBCmpListBoxObserver class
		
	*/	
	~CMMTemplateFileListBoxObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		This class is interested in changes along IID_ILISTCONTROLDATA protocol with classID of
		kListSelectionChangedByUserMessage. This message is sent when a user clicks on an element
		in the list-box.

	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	 void updateListBox(bool16 isAttaching);

	 //--------
	 void CheckAllRowsSelectedforSelectAllCheckBox();

	
};

CREATE_PMINTERFACE(CMMTemplateFileListBoxObserver, kCMMTemplateFileListBoxObserverImpl)


CMMTemplateFileListBoxObserver::CMMTemplateFileListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


CMMTemplateFileListBoxObserver::~CMMTemplateFileListBoxObserver()
{
	
}


void CMMTemplateFileListBoxObserver::AutoAttach()
{
	
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}	
	//updateListBox(kTrue);
}


void CMMTemplateFileListBoxObserver::AutoDetach()
{
	
	//updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);
		subject->DetachObserver(this,IID_ITRISTATECONTROLDATA);
	}
}


void CMMTemplateFileListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if(!controlView) 
			{
				CAlert::InformationAlert("controlView == nil");
				return;
			}
		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CA("AP46CreateMedia::Update::ptrIAppFramework == NULL");		
			return;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		///////////////////////////////////////
		if ((
			(theSelectedWidget == kListBoxSelectMasterTemplateRollOverButtonWidgetID)
			||
			(theSelectedWidget == kListBoxSelectStencilTemplateRollOverButtonWidgetID)
			||
			(theSelectedWidget == kListBoxSelectItemStencilRollOverButtonWidgetID)//added by Tushar on 18/01/07
			||
			(theSelectedWidget == kListBoxSelectHybridTableStencilRollOverButtonWidgetID)//Added By Amit on 4/9/07
			||
			(theSelectedWidget == kListBoxApplyMasterUnderneathElementsRollOverButtonWidgetID)
			||
			(theSelectedWidget == kListBoxApplyAllStencilUnderneathElementsRollOverButtonWidgetID)
			||
			(theSelectedWidget == kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID)//added by Tushar on 18/01/07
			||
			(theSelectedWidget == kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID)//added by Tushar on 18/01/07
			||
			(theSelectedWidget == kListBoxApplyItemStencilUnderneathElementsRollOverButtonWidgetID)//added by Tushar on 18/01/07
			||
			(theSelectedWidget == kListBoxApplyHybridTableStencilUnderneathElementsRollOverButtonWidgetID)//added by Amit on 4/09/07)
			||
			(theSelectedWidget == kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID)/*19-jan*/
			||
			(theSelectedWidget == kListBoxSelectSectionStencilRollOverButtonWidgetID)/*19-jan*/
			||
			(theSelectedWidget == kListBoxSectionStencilUnderneathElementsRollOverButtonWidgetID)/*19-jan*/
			)
			&& 
			(protocol == IID_ITRISTATECONTROLDATA) 
			&& 
			(theChange == kTrueStateMessage) ) 
		{
			
			do
			{
			/*
			GSysPoint pt = Utils<IEventUtils>()->GetGlobalMouseLocation();
			PMPoint pmPoint(pt);
			
			IControlView * listBox = MediatorClass ::PublicationTemplateMappingListBox;
			if(listBox == nil)
			{
				CAlert::InformationAlert("listBox == nil");
				break;
			}
			InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			ASSERT_MSG(listCntl != nil, "listCntl nil");
			if(listCntl == nil)
			{
				CAlert::InformationAlert("listCntl == nil");
				break;
			}
			int32 rowIndex= listCntl->FindRowHit(pmPoint);

			if(theSelectedWidget == kListBoxSelectTemplateFileButtonWidgetID)
			{
				SDKFileOpenChooser FileChooseDLG;
				FileChooseDLG.SetTitle("Select the Template file.");
				FileChooseDLG.ShowDialog();
				PMString pathOfFile("");
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				CA(pathOfFile);
			}			
			*/			
			//////////////end/////////////////////////
			InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
			if(findParentOfRollOverIconButtonWidget == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfRollOverIconButtonWidget == nil");
			//	break;
			}
			IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
			if(primaryResourcePanelWidgetUnKnown == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
			if(primaryResourcePanelWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetControlView == nil");
				break;
			}
			InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
			if(primaryResourcePanelWidgetPanelControlData == nil)			
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetPanelControlData == nil");
				break;
			}
//////////25-jan
			MediatorClass :: iPrimaryPanelCntrlDataPtr = primaryResourcePanelWidgetPanelControlData;
//////////25-jan end
			PMString pathOfFile("");
			//find parent of primaryResourcePanelWidgetPanel which is a cellpanelwidget
			InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(primaryResourcePanelWidgetPanelControlData,UseDefaultIID());
			if(findParentOfPrimaryResourcePanelWidget == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfPrimaryResourcePanelWidget == nil");
				break;
			}
			IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
			if(cellPanelWidgetUnKnown == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
			if(cellPanelWidgetPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetPanelControlData == nil");
				break;
			}
			MediatorClass :: iPanelCntrlDataPtr = cellPanelWidgetPanelControlData;/*19-jan*/
			int32 listBoxRowIndex = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);
			//PMString ss;
			//ss.AppendNumber(listBoxRowIndex);
			//CA(ss);
			//PMString indexx;
			//indexx.AppendNumber(listBoxRowIndex);
			//CA(indexx);


			IControlView * listBox = MediatorClass ::PublicationTemplateMappingListBox; //-------

			if(theSelectedWidget == kListBoxSelectMasterTemplateRollOverButtonWidgetID)
			{
	
				SDKFileOpenChooser FileChooseDLG;
				PMString str("Select Master File.");
				str.SetTranslatable(kFalse);
				FileChooseDLG.SetTitle(str);
				
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					
					ptrIAppFramework->LogError("AP46CreateMedia::Update::!FileChooseDLG.IsChosen");
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				pathOfFile.SetTranslatable(kFalse);
				//3Jun
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt" && extension != "indd")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				PMString fileName;
				fileName.SetTranslatable(kFalse);
				FileUtils::GetFileName(pathOfFile,fileName);
				

				/*
				//end 3Jun
				IControlView * textView = primaryResourcePanelWidgetPanelControlData->FindWidget(kListBoxMasterFileNameTextWidgetID);
				if(textView == nil)
				{
					CA("textView == nil");
					break;
				}
				InterfacePtr<ITextControlData> iTextControlData(textView,UseDefaultIID());
				if(iTextControlData == nil)
				{
					CA("iTextControlData == nil");
					break;
				}

				iTextControlData->SetString(fileName);	
				*/

				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 (kInvalidIDs) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxMasterFileNameTextWidgetID,fileName,listBoxRowIndex);				
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].masterFileWithCompletePath=pathOfFile;
			
				//--------
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
				listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
				CheckAllRowsSelectedforSelectAllCheckBox();

//////////////////////////////////////////////////////	Following code is only for Pricebook option
				if(MediatorClass::createMediaRadioOption == 5)
				{
					//CA("MediatorClass::createMediaRadioOption == 5");
					PMString filePath = pathOfFile;/*MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].masterFileWithCompletePath;*/
					filePath.SetTranslatable(kFalse);
					SDKFileHelper fileHelper(filePath);
					if(filePath == "" || fileHelper.IsExisting() == kFalse)
					{
						//CAlert::InformationAlert("FilePath is Invalid");
						CAlert::InformationAlert("Please select a valid master page file and try again.");
						break;
					}
					PMString fileName;
					FileUtils::GetFileName(filePath,fileName);
					listBoxRowIndex += 1;
					while(listBoxRowIndex < no_of_lstboxElements)
					{
						listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxMasterFileNameTextWidgetID,fileName,listBoxRowIndex);				
						MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].masterFileWithCompletePath=filePath;
						//--------
						MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
						listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);

						listBoxRowIndex++;
					}
					CheckAllRowsSelectedforSelectAllCheckBox();
				}
/////////////////////////////////////////////////////////////

			}
			//following if added by Tushar on 18/01/07
			if(theSelectedWidget == kListBoxSelectItemStencilRollOverButtonWidgetID)
			{
	
				SDKFileOpenChooser FileChooseDLG;
				FileChooseDLG.SetTitle("Select Item Template File.");
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				//3Jun
				pathOfFile.SetTranslatable(kFalse);
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt" && extension != "indd")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				PMString fileName;
				FileUtils::GetFileName(pathOfFile,fileName);				
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 (kInvalidIDs) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,fileName,listBoxRowIndex);				
				
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ItemStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;
				
				//--------
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
				listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
				CheckAllRowsSelectedforSelectAllCheckBox();
			}

			if(theSelectedWidget == kListBoxSelectHybridTableStencilRollOverButtonWidgetID)
			{
	
				SDKFileOpenChooser FileChooseDLG;
				FileChooseDLG.SetTitle("Select Hybrid Table Template File.");
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				//3Jun
				pathOfFile.SetTranslatable(kFalse);
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt" && extension != "indd")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				PMString fileName;
				FileUtils::GetFileName(pathOfFile,fileName);				
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 (kInvalidIDs) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxHybridTableFileNameTextWidgetID,fileName,listBoxRowIndex);				
				
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].HybridTableStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;

				//--------
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
				listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
				CheckAllRowsSelectedforSelectAllCheckBox();

			}

			//30Oct...
			if(theSelectedWidget == kListBoxApplyMasterUnderneathElementsRollOverButtonWidgetID)
			{
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 ids(kInvalidID) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				
				PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].masterFileWithCompletePath;

				SDKFileHelper fileHelper(filePath);
				if(filePath == "" || fileHelper.IsExisting() == kFalse)
				{
					//CAlert::InformationAlert("FilePath is Invalid");
					CAlert::InformationAlert("Please select a valid master page file and try again.");
					break;
				}
				PMString fileName;
				FileUtils::GetFileName(filePath,fileName);
				listBoxRowIndex += 1;

				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxMasterFileNameTextWidgetID,fileName,listBoxRowIndex);				
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].masterFileWithCompletePath=filePath;
					//--------
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
					listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);

					listBoxRowIndex++;
				}
				CheckAllRowsSelectedforSelectAllCheckBox();

			}
			//end 30Oct...

			//30Oct..
			if(theSelectedWidget == kListBoxSelectStencilTemplateRollOverButtonWidgetID)
			{
				SDKFileOpenChooser FileChooseDLG;
				PMString str("Select Template File.");
				str.SetTranslatable(kFalse);
				FileChooseDLG.SetTitle(str);
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				pathOfFile.SetTranslatable(kFalse);
				//3Jun
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt" && extension != "indd")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				PMString fileName;
				fileName.SetTranslatable(kFalse);
				FileUtils::GetFileName(pathOfFile,fileName);				
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 (kInvalidIDs) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,fileName,listBoxRowIndex);

				//added for spec sheet
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameForSpecSheetTextWidgetID,fileName,listBoxRowIndex);
				
				/*following lines commented by tushar on 20/01/06
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kTrue;*/

				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ProductStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;

				//--------
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
				listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
				CheckAllRowsSelectedforSelectAllCheckBox();

			}
			//end 30Oct..

			//30Oct...
			if(theSelectedWidget == kListBoxApplyAllStencilUnderneathElementsRollOverButtonWidgetID)
			{
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 ids(kInvalidID) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter

			//	MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;//added by Tushar
			//	subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;//added by Tushar
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				//PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath;//commented by Tushar
				PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ProductStencilFilePath;
				filePath.SetTranslatable(kFalse);
				SDKFileHelper fileHelper(filePath);
				if(filePath == "" || fileHelper.IsExisting() == kFalse)
				{
					//CAlert::InformationAlert("FilePath is Invalid");
					CAlert::InformationAlert("Please select a valid item group template file and try again.");
					break;
				}

				PMString fileName;
				fileName.SetTranslatable(kFalse);
				FileUtils::GetFileName(filePath,fileName);
				listBoxRowIndex += 1;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,fileName,listBoxRowIndex);

					//added for spec sheet
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameForSpecSheetTextWidgetID,fileName,listBoxRowIndex);
					//following lines commented by Tushar
					////MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath=filePath;
					////MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kTrue;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ProductStencilFilePath=filePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;//added by Tushar 0n 20/01/07
					//MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;//added by Tushar
					//--------
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
					listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
					
					listBoxRowIndex++;
				}
				CheckAllRowsSelectedforSelectAllCheckBox();

				//following code added by Tushar on 19/01/07
				
				//MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				//subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;
				//listBoxRowIndex += 1;
				//while(listBoxRowIndex < no_of_lstboxElements)
				//{
				//	//listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,fileName,listBoxRowIndex);				
				//	MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;					
				//	listBoxRowIndex++;
				//}

				//upto here

			}
			//end 30Oct...

/*19-jan*/	else if((theSelectedWidget == kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID)||(theSelectedWidget == kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID))
			{
				
				//PMString indexx;
				//indexx.AppendNumber(index);
				//CA(indexx);
				MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				PMString num;
				num.AppendNumber(MediatorClass :: selectedIndexOfListBoxElement);
			//	CA(num);
				//DoCMMSpraySettingsDialog();
				//CMMActionComponent cmmActn(MediatorClass :: CMMActionComponentIPMUnknown);
				//cmmActn.
				DoCMMSpraySettingsDialog();
			}

			//else if(theSelectedWidget == kInvokeSubSectionOptionsDialogRollOverButtonButton1WidgetID)
			//{
			//	
			//	//PMString indexx;
			//	//indexx.AppendNumber(index);
			//	//CA(indexx);
			//	MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
			//	PMString num;
			//	num.AppendNumber(MediatorClass :: selectedIndexOfListBoxElement);
			////	CA(num);
			//	//DoCMMSpraySettingsDialog();
			//	//CMMActionComponent cmmActn(MediatorClass :: CMMActionComponentIPMUnknown);
			//	//cmmActn.
			//	DoCMMSpraySettingsDialog();
			//}

			//30Oct...
			else if(theSelectedWidget == kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID)
			{
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;
				listBoxRowIndex += 1;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
 /*19-jan*/			listHelper.UpdateSubSectionOptionIcon(cellPanelWidgetPanelControlData,kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID,kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID,listBoxRowIndex);	
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;					
					listBoxRowIndex++;
				}
			}
			//end 30Oct...

			else if(theSelectedWidget == kListBoxApplyItemStencilUnderneathElementsRollOverButtonWidgetID)
			{
				//MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				//subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;
				//listBoxRowIndex += 1;
				//while(listBoxRowIndex < no_of_lstboxElements)
				//{
				//	//listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,fileName,listBoxRowIndex);				
				//	MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;					
				//	listBoxRowIndex++;
				//}

				//MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;//added by Tushar
				//subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;//added by Tushar
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				//PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath;//commented by Tushar on 20/01/07
				PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ItemStencilFilePath;
				filePath.SetTranslatable(kFalse);
				SDKFileHelper fileHelper(filePath);
				if(filePath == "" || fileHelper.IsExisting() == kFalse)
				{
					//CAlert::InformationAlert("FilePath is Invalid");
					CAlert::InformationAlert("Please select a valid item template file and try again.");
					break;
				}

				PMString fileName;
				FileUtils::GetFileName(filePath,fileName);
				listBoxRowIndex += 1;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,fileName,listBoxRowIndex);				
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ItemStencilFilePath=filePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;
					//MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;//added by Tushar
					//--------
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
					listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
					
					listBoxRowIndex++;
				}
				CheckAllRowsSelectedforSelectAllCheckBox();
			}
			else if(theSelectedWidget == kListBoxApplyHybridTableStencilUnderneathElementsRollOverButtonWidgetID)
			{
				//MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				//subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;
				//listBoxRowIndex += 1;
				//while(listBoxRowIndex < no_of_lstboxElements)
				//{
				//	//listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,fileName,listBoxRowIndex);				
				//	MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;					
				//	listBoxRowIndex++;
				//}

				//MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;//added by Tushar
				//subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;//added by Tushar
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				//PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath;//commented by Tushar on 20/01/07
				PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].HybridTableStencilFilePath;
				SDKFileHelper fileHelper(filePath);
				if(filePath == "" || fileHelper.IsExisting() == kFalse)
				{
					//CAlert::InformationAlert("FilePath is Invalid");
					CAlert::InformationAlert("Please select a valid table template file and try again");
					break;
				}

				PMString fileName;
				FileUtils::GetFileName(filePath,fileName);
				listBoxRowIndex += 1;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxHybridTableFileNameTextWidgetID,fileName,listBoxRowIndex);				
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].HybridTableStencilFilePath=filePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;
					//MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;//added by Tushar
					//--------
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
					listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);

					listBoxRowIndex++;
				}
				CheckAllRowsSelectedforSelectAllCheckBox();
			}

			else if(theSelectedWidget == kListBoxSectionStencilUnderneathElementsRollOverButtonWidgetID)
			{
				//CA("kListBoxSectionStencilUnderneathElementsRollOverButtonWidgetID");
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				//PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath;//commented by Tushar on 20/01/07
				PMString filePath = MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].SectionStencilFilePath;
				SDKFileHelper fileHelper(filePath);
				if(filePath == "" || fileHelper.IsExisting() == kFalse)
				{
					//CAlert::InformationAlert("FilePath is Invalid");
					CAlert::InformationAlert("Please select a valid section template file and try again.");
					break;
				}

				PMString fileName;
				FileUtils::GetFileName(filePath,fileName);
				listBoxRowIndex += 1;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxSectionFileNameTextWidgetID,fileName,listBoxRowIndex);				
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].SectionStencilFilePath=filePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;
					//MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].ssss = ssss;//added by Tushar
					
					//--------
					MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
					listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);

					listBoxRowIndex++;
				}
				CheckAllRowsSelectedforSelectAllCheckBox();
			}

			else if(theSelectedWidget == kListBoxSelectSectionStencilRollOverButtonWidgetID)
			{
				//CA("kListBoxSelectSectionStencilRollOverButtonWidgetID");
				SDKFileOpenChooser FileChooseDLG;
				FileChooseDLG.SetTitle("Select Template File.");
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				pathOfFile.SetTranslatable(kFalse);
				//3Jun
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt" && extension != "indd")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				PMString fileName;
				FileUtils::GetFileName(pathOfFile,fileName);				
				//The SDKListBoxHelper listHelper is a dummy object.It solves no purpose.
				//so all the 0 (kInvalidIDs) are passed while constructing object of it.
				//It is created only for using the UpdateStaticTextWidget function and it takes
				//cellPanelControlData as one of its parameter
				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxSectionFileNameTextWidgetID,fileName,listBoxRowIndex);				
				
				/*following lines commented by tushar on 20/01/06
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].AllStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kTrue;*/

				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].SectionStencilFilePath = pathOfFile;
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSingleStencilFileForProductAndItem = kFalse;

				//--------
				MediatorClass::vector_subSectionSprayerListBoxParameters[listBoxRowIndex].isSelected = kTrue;
				listHelper.ToggleCheckUnchekIconOfSelectedRow(listBox , listBoxRowIndex , kTrue);
				CheckAllRowsSelectedforSelectAllCheckBox();
			}
			
			/*
			WidgetID id = clickedwidgetcontrolView->GetWidgetID();
			
			if(id == kCellPanelWidgetID)
			{
				CAlert::InformationAlert("kCellPanelWidgetID");
				
			}
			else if (id == kCMMMTemplateFileListBoxWidgetID)
			{
				CAlert::InformationAlert("kCMMMTemplateFileListBoxWidgetID");
				
			}
			else if (id == kCMMPanelListParentWidgetId)
			{
				CAlert::InformationAlert("kCMMPanelListParentWidgetId");
				
			}
			else 
			{
				CAlert::InformationAlert("default");
				
			}
			*/
			/*
			RsrcID rsrcid = clickedwidgetcontrolView->GetRsrcID();
				PMString rid("RsrcID = ");
				rid.AppendNumber(rsrcid);
				CAlert::InformationAlert(rid);

			PMString num;
			num.AppendNumber(id.Get());
			CAlert::InformationAlert(num);
			
			InterfacePtr<IWidgetParent> iParentWidget(clickedwidgetcontrolView,UseDefaultIID());
			if(iParentWidget == nil)
			{
				CAlert::InformationAlert("iParentWidget == nil");
				break;
			}
			InterfacePtr<IPMUnknown> unknown2(iParentWidget->GetParent());
			if(unknown2 == nil)
			{
				CAlert::InformationAlert("unknown2 == nil");
				break;
			}
			InterfacePtr<IControlView>clickedwidgetcontrolView2(unknown2,UseDefaultIID());
			if(clickedwidgetcontrolView2 == nil)
			{
				CAlert::InformationAlert("clickedwidgetcontrolView2 == nil");
				break;
			}

			 id = clickedwidgetcontrolView2->GetWidgetID();
			
				if(id == kCellPanelWidgetID)
				{
					CAlert::InformationAlert("kCellPanelWidgetID");
					
				}
			else if (id == kCMMMTemplateFileListBoxWidgetID)
				{
					CAlert::InformationAlert("kCMMMTemplateFileListBoxWidgetID");
					
				}
			else if (id == kCMMPanelListParentWidgetId)
				{
					CAlert::InformationAlert("kCMMPanelListParentWidgetId");
					
				}
			else 
				{
					CAlert::InformationAlert("default");
					
				}
			num.Append("\n");
			num.AppendNumber(id.Get());
			CAlert::InformationAlert(num);

			InterfacePtr<IPanelControlData>panel(clickedwidgetcontrolView2,UseDefaultIID());
			if(panel == nil)			
			{
				CAlert::InformationAlert("panel == nil");
				break;
			}
			int32 length = panel->Length();
			PMString len;
			len.AppendNumber(length);
			CAlert::InformationAlert(len);
			*/
			}while(kFalse);
			
		}
		
////					else if((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) {
////					do {	
////						IControlView * listBox = MediatorClass ::PublicationTemplateMappingListBox;
////						if(listBox == nil) 
////						{
////							break;
////						}
////						InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
////						ASSERT_MSG(listCntl != nil, "listCntl nil");
////						if(listCntl == nil) 
////						{
////							break;
////						}
////						//  Get the first item in the selection (should be only one if the list box
////						// has been set up correctly in the framework resource file)
////						K2Vector<int32> multipleSelection ;
////						listCntl->GetSelected(multipleSelection) ;
////						const int kSelectionLength =multipleSelection.size();
////						
////						
////						//CA("In CMMTemplateFileListBoxObserver.cpp...Line 721..");
////
////						int32 indexSelected = -1;
////						//MultipleSelection is removed
////						if (kSelectionLength > 0 )
////						{
////							
////								indexSelected = multipleSelection[0];
////								//PMString indexs;
////								//indexs.AppendNumber(indexSelected);
////								//CA(indexs);
////
////						}
////						listCntl->DeselectAll();
////
////
////						//////////////////MediatorClass :: vector_subSectionSprayerListBoxParameters[indexSelected].isSelected = !(MediatorClass :: vector_subSectionSprayerListBoxParameters[indexSelected].isSelected);
////						//////////////////SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
////						////////////////// Commented By Rahul
////						/////////////////*listHelper.ToggleCheckUnchekIconOfSelectedRow
////						////////////////	(
////						////////////////		listBox,
////						////////////////		indexSelected,
////						////////////////		MediatorClass :: vector_subSectionSprayerListBoxParameters[indexSelected].isSelected
////						////////////////	);*/
////						//////////////////added on 6Jun
////
////
////					/*	if(MediatorClass ::IsCommonMasterForAllPublicationRadioWidgetSelected)
////						{
////							CA("here in IsCommonMasterForAllPublicationRadioWidgetSelected");
////
////							InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(MediatorClass ::selectAllSectionCheckBoxControlView,UseDefaultIID());
////							if(selectAllSectionCheckBoxTriState == nil)
////							{
////								CA("selectAllSectionCheckBoxTriState == nil")
////								break;
////							}
////							MediatorClass ::IsEventSourceInListBox = kTrue;
////							selectAllSectionCheckBoxTriState->Deselect(kTrue);
////							MediatorClass ::IsEventSourceInListBox = kFalse;
////						}*/
////						//ended on 6Jun
////					} while(0);
////
////			//////////////////////// Added by Chetan dogra on 03-October//////////
////			//////////////////////////////////////////////////////////////////////
////			////////////////////////// to select "Select all section" checkbox////
////			////////////////////   when all subSectionSprayerListBoxParameters ///
////			////////////////////////        are selected individually ////////////
////			//////////////////////////////////////////////////////////////////////
////					
////					//CA(" Chetan's check.....");
////					//bool16 flag = kTrue;
////					//for(int index=0;index < no_of_lstboxElements;index++)
////					//{
////					//	if(MediatorClass :: vector_subSectionSprayerListBoxParameters[index].isSelected);
////					//	else flag = kFalse;
////					//}		
////					//if(flag)
////					//{
////					//	InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(MediatorClass ::selectAllSectionCheckBoxControlView,UseDefaultIID());
////					//		if(selectAllSectionCheckBoxTriState == nil)
////					//		{
////					//			CA("selectAllSectionCheckBoxTriState == nil");
////					//			return;//break;
////					//		}
////					//		CA("kSelectAllCheckBoxWidgetID selecting kTrue in chetean's code");
////					////		MediatorClass ::IsEventSourceInListBox = kTrue;
////					//		selectAllSectionCheckBoxTriState->Select(kTrue);
////					////		MediatorClass ::IsEventSourceInListBox = kFalse;
////					//}
////}
}
//////////////////////////////////////////////////  till here ////////
 
void CMMTemplateFileListBoxObserver::updateListBox(bool16 isAttaching)
{
	//CAlert::InformationAlert("2");
	SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
	listHelper.EmptyCurrentListBox();
	// See if we can find the  listbox
	// If we can then populate the listbox in brain-dead way
	if(isAttaching) {
		do {
				const int targetDisplayWidgetId = kListBoxMasterFileNameTextWidgetID;
				const int kNumberItems = 5;
				for(int i=0; i < kNumberItems; i++) {
					//CAlert::InformationAlert("hi");
					PMString name("Filexxxxxxxxxxxxxxxxxxxxxxxxxxxxx : ");
					name.AppendNumber(i+1);
					listHelper.AddElement(name, targetDisplayWidgetId);
				}
		   } while(0);
	}
}

void CMMTemplateFileListBoxObserver::CheckAllRowsSelectedforSelectAllCheckBox()
{
	//CA("CMMTemplateFileListBoxObserver::CheckAllRowsSelectedforSelectAllCheckBox");
	bool16 flag = kTrue;

	for(int index=0;index < no_of_lstboxElements;index++)
	{
		if(MediatorClass :: vector_subSectionSprayerListBoxParameters[index].isSelected);
		else
			flag = kFalse;
	}		
	if(flag)
	{
		InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(MediatorClass ::selectAllSectionCheckBoxControlView,UseDefaultIID());
		if(selectAllSectionCheckBoxTriState == nil)
		{
			CA("selectAllSectionCheckBoxTriState == nil");
			return;
		}
		MediatorClass ::IsEventSourceInListBox = kTrue;
		selectAllSectionCheckBoxTriState->Select(kTrue);
		MediatorClass ::IsEventSourceInListBox = kFalse;
	}

}

