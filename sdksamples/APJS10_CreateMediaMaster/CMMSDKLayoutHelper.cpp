//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/common/SDKLayoutHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IDocument.h"
#include "IDocumentCommands.h"
//#include "IOpenLayoutWinCmdData.h" //Cs3 Depricated
#include "IOpenLayoutCmdData.h"      //Cs4
#include "IDocumentUtils.h"
#include "IDocFileHandler.h"
#include "INewDocCmdData.h"
#include "IDocumentLayer.h"
#include "ILayerList.h"
#include "IWindow.h"
#include "ICreateMCFrameData.h"
#include "IMultiColumnItemData.h"
#include "INewPageItemCmdData.h"
#include "IPlacePIData.h"
//#include "IImportFileCmdData.h"
#include "IPathUtils.h"
#include "ISpread.h"
#include "ISpreadLayer.h"
#include "IGraphicFrameData.h"
#include "ITransform.h"
#include "IGeometry.h"
#include "IHierarchy.h"
#include "IMargins.h"
//#include "ITextFrame.h"  //CS# CHANGE
 #include "ITextFrameColumn.h"
#include "ITextModel.h"
#include "ITOPFrameData.h"
#include "ISpreadList.h"
#include "IBaselineFrameGridData.h"
#include "ILayerUtils.h"

// General includes:
#include "CmdUtils.h"
#include "UIDList.h"
#include "SplineID.h"		// kSplineItemBoss
#include "TransformUtils.h" // InnerToPasteboard
#include "OpenPlaceID.h"
#include "PMPathPoint.h"
#include "Utils.h"
#include "PreferenceUtils.h"

// Project includes.
#include "SDKLayoutHelper.h"

#include "CAlert.h"
#include "URI.h"
#include "IURIUtils.h"
#include "IImportResourceCmdData.h"

//macros
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMSDKLayoutHelper.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


// By default frames created will pickup the default standoff (text wrap)
// that is in operation.
const bool16 kSDKLayoutAllowDefaultStandOff = kTrue;

/* Default constructor
*/

SDKLayoutHelper::SDKLayoutHelper()
{
}

/* Destructor
*/
SDKLayoutHelper::~SDKLayoutHelper()
{

}

/*
*/
UIDRef SDKLayoutHelper::CreateDocument(UIFlags	uiflags,
	const	PMReal& width,
	const	PMReal& height,
	const	int32 numPages,
	const	int32 numPagesPerSpread
)
{
	UIDRef result = UIDRef::gNull;
	do {
		// Create the command.
		InterfacePtr<ICommand> newDocCmd(Utils<IDocumentCommands>()->CreateNewCommand(uiflags));
		ASSERT(newDocCmd);
		if (newDocCmd == nil) {
			break;
		}
		
		// Set the command's parameterised data.
		InterfacePtr<INewDocCmdData> newDocCmdData(newDocCmd, UseDefaultIID());
		ASSERT(newDocCmdData);
		if (newDocCmdData == nil) {
			break;
		}
		newDocCmdData->SetCreateBasicDocument(kFalse); // Override the following defaults.
		PMRect pageSize = PMRect(0.0, 0.0, width, height);
		newDocCmdData->SetPageSizePref(pageSize);
		bool16 bWide = kTrue; // landscape orientation.
		if (width < height)
		{
			// portrait orientation.
			bWide = kFalse;
		}
		newDocCmdData->SetWideOrientation(bWide);
		// Size margin proportional to document width and height.
		PMReal horMargin = width / 20;
		PMReal verMargin = height / 20;
		newDocCmdData->SetMargins(horMargin, verMargin, horMargin, verMargin);
		newDocCmdData->SetNumPages(numPages);
		newDocCmdData->SetPagesPerSpread(numPagesPerSpread);

		// Create the new document.
		if (CmdUtils::ProcessCommand(newDocCmd) != kSuccess) {
			ASSERT_FAIL("IDocumentCommands::CreateNewCommand failed");
			break;
		}

		// Pass the UIDRef of the new document back to the caller.
		const UIDList& newDocCmdUIDList = newDocCmd->GetItemListReference();
		result = newDocCmdUIDList.GetRef(0);
		if (result == UIDRef::gNull) {
			ASSERT_FAIL("newDocCmd returned invalid document UIDRef!");
			break;
		}
	} while (false);
	return result;
}

/*
*/
UIDRef SDKLayoutHelper::OpenDocument(const IDFile& sysFile, UIFlags uiFlags)
{
	//CAlert::InformationAlert("SDKLayoutHelper::OpenDocument");
	UIDRef result = UIDRef::gNull;
	do 
	{
		UIDRef tempRef;
		ErrorCode status = Utils<IDocumentCommands>()->Open(&tempRef,sysFile,uiFlags,IOpenFileCmdData::kOpenDefault,IOpenFileCmdData::kNotUseLockFile, kFalse);
		ASSERT(status == kSuccess);
		if(status != kSuccess){
			break;
		}

		// Grab the UIDRef of the newly opened document and verify it.
		InterfacePtr<IDocument> document(tempRef, UseDefaultIID());
		ASSERT(document);
		if (!document)	{
			// If we could not get an IDocument the postconditions are not met
			break;
		}
		// OK the document is now open but not on view.
		result = tempRef;
	} while(false);
	return result;
}

/*
*/
ErrorCode SDKLayoutHelper::CloseDocument(const UIDRef& documentUIDRef,
			bool16 saveOnClose,
			UIFlags uiFlags, 
			bool8 allowCancel, 
			IDocFileHandler::CloseCmdMode cmdMode)
{
	
	ErrorCode result = kFailure;
	// Older approach
	//Refer http://www.adobeforums.com/cgi-bin/webx/.3bbeba7e/5
	
	do {
		// Get the IDocFileHandler interface.

		// Starting from InDesign 3.0, IDocumentUtils has been added to 
		// the utils boss where it is more logically placed. 
		// The version of IDocumentUtils on the session boss is being deprecated. 
		InterfacePtr<IDocFileHandler> docFileHandler(Utils<IDocumentUtils>()->QueryDocFileHandler(documentUIDRef));
		if (!docFileHandler) {
			break;
		}

		// Save before closing if asked.
		if(saveOnClose && docFileHandler->CanSave(documentUIDRef) ) {
			docFileHandler->Save(documentUIDRef, uiFlags);
			result = ErrorUtils::PMGetGlobalErrorCode();
			ASSERT_MSG(result == kSuccess, "IDocFileHandler::Save failed");
			if (result != kSuccess) {
				break;
			}
		}

		// Close document.
		if (docFileHandler->CanClose(documentUIDRef)) {
			//CA("before close");
			docFileHandler->Close(documentUIDRef, uiFlags, allowCancel, cmdMode);
			//CA("after close");
			result = ErrorUtils::PMGetGlobalErrorCode();
			ASSERT_MSG(result == kSuccess || result == kCancel, "IDocFileHandler::Close failed");
			if (result != kSuccess) {
				break;
			}
		}
	} while(false);
	
	/*
	do
	{
		
		InterfacePtr<ICommand> closeDoc(Utils<IDocumentCommands>()->CreateCloseCommand(documentUIDRef));
		if(closeDoc == nil)
		{
			CAlert::InformationAlert("closeDoc == nil");
		}
		result = CmdUtils::ScheduleCommand(closeDoc);
	}while(kFalse);
	*/
	return result;
}

/*
*/
ErrorCode SDKLayoutHelper::SaveDocumentAs(const UIDRef& documentUIDRef,
										const IDFile& sysFile,
										UIFlags uiFlags)
{
	ErrorCode result = kFailure;

	/* Older approach
	Refer http://www.adobeforums.com/cgi-bin/webx/.3bbeba7e/5
	
	do {
		// Save the document to another file.

		// Starting from InDesign 3.0, IDocumentUtils has been added to 
		// the utils boss where it is more logically placed. 
		// The version of IDocumentUtils on the session boss is being deprecated. 
		InterfacePtr<IDocFileHandler> docFileHandler(Utils<IDocumentUtils>()->QueryDocFileHandler(documentUIDRef));
		if (!docFileHandler) {
			break;
		}
		//Try to do SaveAs
		if(docFileHandler->CanSaveAs(documentUIDRef) ) {
			docFileHandler->SaveAs (documentUIDRef, &sysFile, uiFlags);
			result = ErrorUtils::PMGetGlobalErrorCode();
			ASSERT_MSG(result == kSuccess, "IDocFileHandler::SaveAs failed");
			if (result != kSuccess) {
				break;
			}
		}
	} while(false);
	*/
	do
	{
	InterfacePtr<IDocFileHandler> docFileHandler(Utils<IDocumentUtils>()->QueryDocFileHandler(documentUIDRef));
		if (!docFileHandler) {
			break;
		}
		//Try to do SaveAs
		if(docFileHandler->CanSaveAs(documentUIDRef) ) {
			docFileHandler->SaveAs (documentUIDRef, &sysFile, uiFlags);
			result = ErrorUtils::PMGetGlobalErrorCode();
			ASSERT_MSG(result == kSuccess, "IDocFileHandler::SaveAs failed");
			if (result != kSuccess) {
				break;
			}
		}
	}while(0);
	/*do
	{
		result = Utils<IDocumentCommands>()->SaveAs(documentUIDRef, sysFile);		
	}while(kFalse);*/
	return result;
}

/*
*/
ErrorCode SDKLayoutHelper::OpenLayoutWindow(const UIDRef& documentUIDRef)
{
	ErrorCode result = kFailure;
	do
	{
		// Create the command.
		InterfacePtr<ICommand> openLayoutWinCmd(CmdUtils::CreateCommand(kOpenLayoutCmdBoss/*kOpenLayoutWinCmdBoss*/));//Cs4
		ASSERT(openLayoutWinCmd);
		if (openLayoutWinCmd == nil){
			break;
		}

		// Pass the command the UIDRef of the document.
		openLayoutWinCmd->SetItemList(UIDList(documentUIDRef));
		
		// You could override the command's default options if necessary... IOpenLayoutCmdData
//		InterfacePtr<IOpenLayoutWinCmdData> openLayoutWinCmdData(openLayoutWinCmd, UseDefaultIID()); //Cs3
		InterfacePtr<IOpenLayoutPresentationCmdData> openLayoutWinCmdData(openLayoutWinCmd, UseDefaultIID()); //Cs4
		ASSERT(openLayoutWinCmdData);
		if (openLayoutWinCmdData == nil) {
			break;
		}
		// ...we just take the defaults.

		// Open a window on the document.
		if (CmdUtils::ProcessCommand(openLayoutWinCmd) != kSuccess) {
			ASSERT_FAIL("kOpenLayoutWinCmdBoss failed");
			break;
		}

		// Validate the the newly opened window is returned.
		//Please Uncomment This Code.....`
		//IWindow* window = openLayoutWinCmdData->GetOutputWindow();
		//ASSERT(window);
		//if (window == nil) {
		//	// If we couldn't get an IWindow the postconditions won't be met
		//	break;
		//}

		result = kSuccess;

	} while (false);
	return result;
}

/*
*/
UIDRef  SDKLayoutHelper::GetActiveSpreadLayerRef(const UIDRef& spreadUIDRef, 
												bool16 wantGuideLayer)
{
	UIDRef result = UIDRef::gNull;
	do {
		// Navigate from spread to document.
		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		ASSERT(spread);
		if(!spread) {
			break;
		}
		IDataBase* db = spreadUIDRef.GetDataBase();
		InterfacePtr<IDocument> document(db, db->GetRootUID(), UseDefaultIID());
		ASSERT(document);
		if (document == nil) {
			break;
		}

		// We need to get the active document layer before we can find the corresponding spread layer
		InterfacePtr<IDocumentLayer> documentLayer(Utils<ILayerUtils>()->QueryDocumentActiveLayer(document));
		if (!documentLayer) {
			// If for some reason ILayerUtils doesn't give the active document layer then find a document layer via ILayerList.
			InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST); //no kDefaultIID UseDefaultIID());
			ASSERT(layerList);
			if(!layerList) {
				break;
			}
			/*IDocumentLayer**/ UIDRef layerRef/*layer*/ =layerList->GetNextActiveLayer(int32(0));
			if (layerRef  != UIDRef()) {
				/*layer->AddRef();
				documentLayer.reset(layer);*/  //***Commented
				documentLayer = InterfacePtr<IDocumentLayer>(layerRef, UseDefaultIID()); //**ADDED
			}
		}
		ASSERT(documentLayer);
		if(!documentLayer) {
			break;
		}

		int32 spreadLayerPosition;
		// to navigate to a kSpreadLayerBoss from kSpreadBoss we need the help of kDocumentLayerBoss
		InterfacePtr<ISpreadLayer> contentSpreadLayer(spread->QueryLayer(documentLayer, &spreadLayerPosition, wantGuideLayer));
		ASSERT(contentSpreadLayer);
		result = ::GetUIDRef(contentSpreadLayer);
		
	} while(false);
	return result;
}

/*
*/
UIDRef  SDKLayoutHelper::GetSpreadLayerRef(const UIDRef& spreadUIDRef, 
												int32 documentLayerIndex, 
												bool16 wantGuideLayer)
{
	UIDRef result = UIDRef::gNull;
	do {
		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		ASSERT(spread);
		if(!spread) {
			break;
		}

		// Find the document layer indicated by the given documentLayerIndex. 
		IDataBase* db = spreadUIDRef.GetDataBase();
		InterfacePtr<IDocument> document(db, db->GetRootUID(), UseDefaultIID());
		ASSERT(document);
		if (!document) {
			break;
		}
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST); //UseDefaultIID());
		ASSERT(layerList);
		if(!layerList) {
			break;
		}
		/*IDocumentLayer* documentLayer = layerList->GetLayer(documentLayerIndex);*/ ///**Commented By Sachin Sharma
		InterfacePtr<IDocumentLayer> documentLayer(layerList->QueryLayer(documentLayerIndex));///***Added
		ASSERT(documentLayer);
		if(!documentLayer) {
			break;
		}

		// Use the spread to find the spread layer associated with this document layer.
		int32 spreadLayerIndex;
		InterfacePtr<ISpreadLayer> spreadLayer(spread->QueryLayer(documentLayer, &spreadLayerIndex, wantGuideLayer));
		ASSERT(spreadLayer);
		if (!spreadLayer) {
			break;
		}

		result = ::GetUIDRef(spreadLayer);
		
	} while(false);
	return result;
}

/*
*/
PMRect SDKLayoutHelper::PageToSpread(const UIDRef& pageUIDRef, const PMRect& boundsInPageCoords)	
{
	PMRect result(boundsInPageCoords);
	do {
		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}
		PMRect marginBBox;
		InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
		// Note it's OK if the page does not have margins.
		if (margins) {
			margins->GetMargins(&marginBBox.Left(), &marginBBox.Top(), &marginBBox.Right(), &marginBBox.Bottom());
		}
		result.MoveRel(marginBBox.Left(), marginBBox.Top());
		//::InnerToParent(transform, &result);  //***Commented
		::TransformInnerRectToParent(transform, &result);///*added
	} while (false);
	return result;
}

/*
*/
UIDRef SDKLayoutHelper::PlaceFileInFrame(const IDFile& contentFile, 
							const UIDRef& parentUIDRef, 
							const PMRect& boundsInParentCoords, 
							const UIFlags uiFlags, 
							const bool16 retainFormat, 
							const bool16 convertQuotes, 
							const bool16 applyCJKGrid,
							const UID& uidPreview)
{
	UIDRef result = UIDRef::gNull;
	do {
		InterfacePtr<IHierarchy> hierarchy(parentUIDRef, UseDefaultIID());
		ASSERT(hierarchy);
		if (!hierarchy) {
			break;
		}

		// Set up the co-ordinates for the frame. The caller passes in the
		// co-ordinates in the co-ordinate space of the parent. kImportAndPlaceCmdBoss
		// expects pasteboard co-ordinates so we have to transform the points before passing
		// them to the command.
		InterfacePtr<ITransform> transform(parentUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (transform == nil) {
			break;
		}
		PBPMPoint topLeft = boundsInParentCoords.LeftTop();
		PBPMPoint rightBottom = boundsInParentCoords.RightBottom();
		//::ParentToPasteboard(transform, &topLeft); //**Commented
		::TransformParentPointToPasteboard(transform,&topLeft);///***Added
		
		//::ParentToPasteboard(transform, &rightBottom);  //**Commented 
		::TransformParentPointToPasteboard(transform, &rightBottom); //**Added
		// Fill in the PointList. If you know you are only placing graphics
		// kImportAndPlaceCmdBoss will automatically size the frame to fit the content
		// if you just specify one point, the leftTop co-ordinate. Since we
		// want to be able to place both text and graphics we specify two
		// points that together give the bounding box. We subsequently process 
		// kFitFrameToContentCmdBoss, that way we can have a single method that places 
		// pictures or text.
		PMPointList pointList(2);
		pointList.push_back(topLeft);
		pointList.push_back(rightBottom);

		// Create kImportAndPlaceCmdBoss.
		InterfacePtr<ICommand> importAndPlaceCmd(CmdUtils::CreateCommand(kImportAndPlaceCmdBoss));
		ASSERT(importAndPlaceCmd != nil);
		if (importAndPlaceCmd == nil) {
			break;
		}

		// Set up the command's data interfaces.
		InterfacePtr<IPlacePIData> placePIData(importAndPlaceCmd, UseDefaultIID());
		ASSERT(placePIData != nil);
		if (placePIData == nil) {
			break;
		}
		placePIData->Set(parentUIDRef, &pointList, kFalse/*Don't use place gun contents, use the file.*/);

		//InterfacePtr<IImportFileCmdData> importFileCmdData(importAndPlaceCmd, IID_IIMPORTFILECMDDATA); // no kDefaultIID
		//ASSERT(importFileCmdData != nil);
		//if (importFileCmdData == nil) {
		//	break;
		//}
		//importFileCmdData->Set(parentUIDRef.GetDataBase(), contentFile, uiFlags, retainFormat, convertQuotes, applyCJKGrid, uidPreview);

		//// Process the command.
		//if (CmdUtils::ProcessCommand(importAndPlaceCmd) != kSuccess) {
		//	ASSERT_FAIL("kImportAndPlaceCmdBoss failed");
		//	break;
		//}

		//----- above code Comment and add new code for CS5-------
		#pragma message("LINKREWORK: Temporary, fix this up later!")

		URI tmpURI;
		Utils<IURIUtils>()->IDFileToURI(contentFile, tmpURI);
		InterfacePtr<IImportResourceCmdData> importResourceCmdData(importAndPlaceCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID
		ASSERT(importResourceCmdData != nil);
		if (importResourceCmdData == nil) {
			break;
		}
		importResourceCmdData->Set(parentUIDRef.GetDataBase(), tmpURI, uiFlags, retainFormat, convertQuotes, applyCJKGrid, uidPreview);

		// Process the command.
		if (CmdUtils::ProcessCommand(importAndPlaceCmd) != kSuccess) {
			ASSERT_FAIL("kImportAndPlaceCmdBoss failed");
			break;
		}

		// Get a reference to the created frame to the caller.
		const UIDList& outItemList = importAndPlaceCmd->GetItemListReference();
		ASSERT(outItemList.Length() > 0);
		if (!(outItemList.Length() > 0)) {
			break;
		}
		UIDRef frameUIDRef = importAndPlaceCmd->GetItemListReference().GetRef(0);
		InterfacePtr<IGraphicFrameData> graphicFrameData(frameUIDRef, UseDefaultIID());
		ASSERT(graphicFrameData);
		if (!graphicFrameData) {
			break;
		}

		result = frameUIDRef;

		// Fit the frame to the text content we placed.
		InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
		ASSERT(fitFrameToContentCmd != nil);
		if (fitFrameToContentCmd == nil) {
			break;
		}
		fitFrameToContentCmd->SetItemList(UIDList(result));
		if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
			ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
			break;
		}
	} while(false);

	return result;
}

/*
*/
UIDRef SDKLayoutHelper::CreateRectangleFrame(const UIDRef& parentUIDRef, const PMRect& boundsInParentCoords)	
{
	UIDRef result = UIDRef::gNull;
	PMRect bounds(boundsInParentCoords);
	result = Utils<IPathUtils>()->CreateRectangleSpline(parentUIDRef,
			bounds, 
			INewPageItemCmdData::kGraphicFrameAttributes,
			kSDKLayoutAllowDefaultStandOff,
			/*IGeometry::kInnerCoordinateSpace */Transform::InnerCoordinates());
	//ASSERT(result != UIDRef::gNull);
	// If the above call returned UIDRef::gNull do we consider this a failure?
	// (JZhou)
	return result;
}

/*
*/
UIDRef SDKLayoutHelper::CreateTextFrame(const UIDRef& parentUIDRef,
										const PMRect& boundsInParentCoords,
										int32 numberOfColumns,
										bool16 verticalFrame,
										UIDRef* storyUIDRef)
{
	UIDRef result = UIDRef::gNull;
	do
	{
		// Set up the co-ordinates for the frame. The caller passes in the
		// co-ordinates in the co-ordinate space of the parent. kCreateMultiColumnItemCmdBoss
		// expects pasteboard co-ordinates so we have to transform the points before passing
		// them to the command.
		PMPoint topLeft = boundsInParentCoords.LeftTop();
		PMPoint rightBottom = boundsInParentCoords.RightBottom();
		InterfacePtr<ITransform> transform(parentUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (transform == nil) {
			break;
		}
		//::ParentToPasteboard(transform, &topLeft); //Commented
		::TransformParentPointToPasteboard(transform, &topLeft);///**Added
		
		//::ParentToPasteboard(transform, &rightBottom);///Commented
		::TransformParentPointToPasteboard(transform,&rightBottom);///****Added
		
		PMPointList points(2);
		points.push_back(topLeft);
		points.push_back(rightBottom);

		// Create the command.
		InterfacePtr<ICommand> createTextFrameCmd(CmdUtils::CreateCommand(kCreateMultiColumnItemCmdBoss));
		ASSERT(createTextFrameCmd);
		if (createTextFrameCmd == nil) {
			break;
		}

		// Set up the INewPageItemCmdData data interface.
		InterfacePtr<INewPageItemCmdData> newPageItemCmdData(createTextFrameCmd, UseDefaultIID());
		ASSERT(newPageItemCmdData);
		if (newPageItemCmdData == nil) {
			break;
		}
		// ClassID of frame in which kMultiColumnItemBoss will be created.
		const ClassID graphicFrameClassID = kSplineItemBoss; 
		newPageItemCmdData->Set(parentUIDRef.GetDataBase(), graphicFrameClassID, parentUIDRef.GetUID(), points);

		// Set up ICreateFrameData.
		InterfacePtr<ICreateMCFrameData > createFrameData(createTextFrameCmd, UseDefaultIID());
		ASSERT(createFrameData);
		if (createFrameData == nil) {
			break;
		}
		// We recommend you don't activate the text editor via kCreateMultiColumnItemCmdBoss.
		// Under InDesign 3.0 the default value of ICreateFrameData::SetActivateTextEditor
		// has changed from kTrue to kFalse because many expensive notifications happen if
		// the text editor is activated.
		//createFrameData->SetActivateTextEditor(nil) ;	// nil to not activate text editor
		// Default is that it isn't activated anyway, this call was bogus (irp)
		
		// Specify the frame orientation
		ICreateMCFrameData ::Orientation orientation = ICreateMCFrameData ::kHorizontal;
		if (verticalFrame == kTrue) {
			orientation	= ICreateMCFrameData ::kVertical;
		}
		createFrameData->SetOrientation(orientation);

		// Set up IMultiColumnItemData to override defaults.
		InterfacePtr<IMultiColumnItemData> multiColumnItemData(createTextFrameCmd, UseDefaultIID());
		ASSERT(multiColumnItemData);
		if (!multiColumnItemData) {
			break;
		}
		// 1037710+
		// Under InDesign 4.0 the text frame options such as 
		// as insets are now parameters passed to kCreateMultiColumnItemCmdBoss,
		// the command no longer picks up these defaults from the workspace,
		// instead the client code must direct the command at the values to use.
		InterfacePtr<ITextOptions> textOptions((ITextOptions*)::QueryPreferences(ITextOptions::kDefaultIID, parentUIDRef));
		if (textOptions) {
			multiColumnItemData->SetTextFrameOptionsData(textOptions);
			InterfacePtr<IBaselineFrameGridData> baselineFrameGridData(textOptions, UseDefaultIID());
			if (baselineFrameGridData) {
				multiColumnItemData->SetBaselineFrameGridData(baselineFrameGridData);
			}
		} //1037710+
		if (numberOfColumns > 0) {
			multiColumnItemData->SetNumberOfColumns(numberOfColumns);
		}

		// Process the command.
		if (CmdUtils::ProcessCommand(createTextFrameCmd)!=kSuccess) {
			ASSERT_FAIL("kCreateMultiColumnItemCmdBoss failed");
			break;
		}

		// The command returns a reference to the graphic frame, normally a kSplineItemBoss,
		// with which the underlying kMultiColumnItemBoss and kFrameItemBoss objects are
		// associated.
		const UIDList& outItemList = createTextFrameCmd->GetItemListReference();
		ASSERT(outItemList.Length() > 0);
		if (!(outItemList.Length() > 0)) {
			break;
		}
		result = outItemList.GetRef(0);;
		ASSERT(result != UIDRef::gNull);

		// If the caller requested it by passing in a pointer return a reference
		// to the story associated with the text frame.
		if (storyUIDRef) {
			*storyUIDRef = UIDRef(parentUIDRef.GetDataBase(), createFrameData->GetStory());
		}

	} while(false);

	return result;
}

/*
*/
UIDRef SDKLayoutHelper::CreateRectangleGraphic(const UIDRef& parentUIDRef, const PMRect& boundsInParentCoords)	
{
	UIDRef result = UIDRef::gNull;
	PMRect bounds(boundsInParentCoords);
	result = Utils<IPathUtils>()->CreateRectangleSpline(parentUIDRef,
			bounds, 
			INewPageItemCmdData::kDefaultGraphicAttributes,
			kSDKLayoutAllowDefaultStandOff,
			/*IGeometry::kInnerCoordinateSpace*/Transform::PasteboardCoordinates());
	ASSERT(result != UIDRef::gNull);
	return result;
}

/*
*/
UIDRef SDKLayoutHelper::CreateSplineGraphic(const UIDRef& parentUIDRef, const PMRect& boundsInParentCoords, const PMRect& boundsInInnerCoords, PMPathPointList& pathPointList, bool16 bClosedPath)
{
	UIDRef result  = UIDRef::gNull;

	// Create a transformation matrix that transforms the path to the given boundsInParentCoords.
	PMReal scaleX = boundsInParentCoords.Width()/boundsInInnerCoords.Width();
	PMReal scaleY = boundsInParentCoords.Height()/boundsInInnerCoords.Height();
	PMMatrix transformationMatrix(scaleX, 0, 0, scaleY, boundsInParentCoords.Left(), boundsInParentCoords.Top());
	
	// Transform each point in the path.
	PMPathPointList::iterator iter;
	InterfacePtr<IGeometry> parentGeometry(parentUIDRef, UseDefaultIID());
	for (iter = pathPointList.begin(); iter < pathPointList.end(); iter++) {
		iter->TransformPoints(transformationMatrix);
	}
	
	// Create the spline.
	result = Utils<IPathUtils>()->CreateSpline(parentUIDRef,
		pathPointList, 
		INewPageItemCmdData::kDefaultGraphicAttributes,
		kSDKLayoutAllowDefaultStandOff,
		bClosedPath,
		/*IGeometry::kInnerCoordinateSpace*/Transform::PasteboardCoordinates());
	ASSERT(result != UIDRef::gNull);
	return result;
} 

/*
*/
UIDRef SDKLayoutHelper::GetGraphicFrameRef(const InterfacePtr</*ITextFrame*/ITextFrameColumn>& textFrame)
{
	UIDRef result = UIDRef::gNull;

	do {
		ASSERT(textFrame);
		if (!textFrame) {
			break;
		}

		// Check for a text frame for text on a path.
		InterfacePtr<ITOPFrameData> topFrameData(textFrame, UseDefaultIID());
		if (topFrameData != nil) {
			// This is a text on a path text frame. Refer to the
			// spline that the text on a path is associated with.
			result = UIDRef(::GetDataBase(textFrame), topFrameData->GetMainSplineItemUID());
			break;
		}

		// Check for a regular text frame by going up
		// the hierarchy till we find a parent object
		// that aggregates IGraphicFrameData. This is
		// the graphic frame that contains the text content.
	/*	InterfacePtr<IHierarchy> child(textFrame, UseDefaultIID());
		if (child == nil) {
			break;
		}
		do {
			InterfacePtr<IHierarchy> parent(child->QueryParent());
			if (parent == nil) {
				break;
			}
			InterfacePtr<IGraphicFrameData> graphicFrameData(parent, UseDefaultIID());
			if (graphicFrameData != nil) {
				// We have a regular text frame.
				result = ::GetUIDRef(graphicFrameData);
				break;
			}
			child = parent;
		} while(child != nil);
	*/
        InterfacePtr<IHierarchy> graphicFrameHierarchy(Utils<ITextUtils>()->QuerySplineFromTextFrame(textFrame));
        result = ::GetUIDRef(graphicFrameHierarchy);

	} while(false);

	return result;

}

/*
*/
UIDRef SDKLayoutHelper::GetTextModelRef(const InterfacePtr<IGraphicFrameData>& graphicFrameData)
{
	UIDRef result = UIDRef::gNull;
	do {
		ASSERT(graphicFrameData);
		if (!graphicFrameData) {
			break;
		}
		// IGraphicFrameData references any associated kMultiColumnItemBoss object.
		UID textframeUID = graphicFrameData->GetTextContentUID();
		if (textframeUID == kInvalidUID) {
			break;
		}
		// Conveniently kMultiColumnItemBoss aggregates ITextFrame,as does each column (kFrameItemBoss)   
		InterfacePtr</*ITextFrame*/ITextFrameColumn> textFrame(::GetDataBase(graphicFrameData), textframeUID, UseDefaultIID());
		ASSERT(textFrame);
		if(!textFrame) {
			break;
		}
		UID textModelUID = textFrame->GetTextModelUID();
		ASSERT(textModelUID != kInvalidUID);
		if (textModelUID == kInvalidUID) {
			break;
		}
		result = UIDRef(::GetDataBase(graphicFrameData), textModelUID);
	} while(false);
	return result;
}

//  End, SDKLayoutHelper.cpp
