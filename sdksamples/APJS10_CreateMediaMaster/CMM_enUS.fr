//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_enUS)
{
        k_enUS,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
        	 // ----- Menu strings
                kCMMCompanyKey,					kCMMCompanyValue,
                kCMMAboutMenuKey,				kCMMPluginName "",
                kCMMPluginsMenuKey,				kCMMPluginName "",
				kCMMDialogMenuItemKey,			"Create Media",
	
                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
				kCMMDialogTitleKey,     kCMMPluginName,


		// ----- Misc strings
                kCMMAboutBoxStringKey,			kCMMPluginName " [US], version " kCMMVersion " by " kCMMAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,
                kCMMCreateMediaStringKey,		"Create Media",
                kCMMCreatePDFStringKey,			"Create PDFs",
                kCMMEmptyStringKey,				"",
                kCMMBySpreadsStringKey, "Create Media By Spreads",
                kCMMBySectionsStringKey, "Create Media By Sections",
                kCMMFirstPageStringKey, "First Page:",
                kCMMSpreadTogetherStringKey, "Keep spreads together",
                kCMMCancelStringKey, "Cancel",
                kCMMNextStringKey, "Next>>",
                kCMMSelectPublicationStringKey, "Select Publication:",
                kCMMCreateMediaPublicationStringKey, "  Create Media Publication",
                kCMM1docPerSectionStringKey, "Create One Document Per Section",
                kCMMBlankSpaceStringKey, "  ",
                kCMM1DocPerSpreadStringKey, "Create One Document Per Spread - Beta!",
                kCMMUseThisOptionStringKey, "Use this portion if you want each InDesign document to consist of exactly one spread.",
                kCMM1docperpageStringKey, "Create One Document Per Page",
                kCMMUseThisOptionFor1pageStringKey, "Use this portion if you want each InDesign document to consist one page.",
                kCMMDocPerSectionOnWhiteboardStringKey, "Create One Document Per Section Based on Whiteboard- Beta!",
                kCMMCustDesignedfromWhiteboardStringKey, "Get the custom designed catalog from Whiteboard - Beta!.",
                kCMMCreatePriceBookStringKey, "Create Price Book",
                kCMMSpecSheetsinBulkStringKey, "Use this option if you want to create spec sheets in bulk.",
                kCMMNext2StringKey, "Next",
                kCMMSelectallStringKey, "Select all",
                kCMMFolderpathStringKey, "Folder path",
                kCMMSectionStringKey, "Section",
                kCMMMasterpageStringKey, "Master Page (*.indt)", 
                kCMMTemplateStringKey, "Template (*.indt)",
                kCMMFlowStringKey, "Flow",
                kCMMVerticalPointsStringKey, "Vertical Points: ",
                kCMMHorizontalPointsStringKey, "Horizontal Points: ",
                kCMMSpreadFlowStringKey, " Spread Flow ",
                kCMMHorizontalFlowStringKey, "Horizontal Flow For All Image Spray ",
                kCMMSpraysectionwithoutPageBreakStringKey, "Spray Section Without Page Break",
                //kCMMAddSectionTemplateStringKey, "Add Section Template",
                kCMMAtstartofsectionStringKey, "At The Start Of Section ",
                kCMMSelectMasterFileStringKey, "Select Master File Name",
                kCMMSelectProductFileNameStringKey, "Select Product File Name",
                kCMMSelectExportToPDFStringKey, "Automatically Export to PDF",
                kCMMSelectTemplateFileNameStringKey, "Select Template File Name",	
                
                kCMMSelectMasterStringKey, "Select the master",
                kCMMusethesamemasterStringKey, "Use the same master for all the subsections below",
                kCMMSelectProductTemplateStringKey, "select the product Template",
                
                
                
                
               	
               

		
        }

};

#endif // __ODFRC__

//  Code generated by DollyXs code generator
