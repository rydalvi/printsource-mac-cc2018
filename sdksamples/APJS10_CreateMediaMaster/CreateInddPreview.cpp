
#include "VCPlugInHeaders.h"

// Interface includes:
#include "ICommand.h"
#include "IDataBase.h"
#include "IDocFileHandler.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IInCopyDocUtils.h"
#include "IOpenFileCmdData.h"
#include "IPageList.h"
#include "IPMStream.h"
#include "ISession.h"
#include "IDocumentCommands.h"
#include "CAlert.h"
// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "FeatureSets.h"
#include "LocaleSetting.h"
#include "OpenPlaceID.h"
#include "SDKFileHelper.h"
#include "SnapshotUtils.h"
#include "StreamUtil.h"
#include "UIDRef.h"
#include "CreateInddPreview.h"
#include "FileUtils.h"
#include "SDKUtilities.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "IGeometry.h"
#include "ILayoutUIUtils.h" 
#include "IHierarchy.h"
//#include "PageItemUtils.h" //Cs3 DepriCated
#include "IPageItemUtils.h" //Cs4
#include "IAppFramework.h"
#include "ISpreadList.h"
#include "IMargins.h"
#include "CStencilValue.h"
#include "MediatorClass.h"

#define CA(x) CAlert::InformationAlert(x)
extern int32 CurrentSelectedSubSection;
//--------------------------------------------------------------------------------



 

ErrorCode CreateInddPreview::CreatePagePreview  (  const IDFile &  documentFile,  
  const IOpenFileCmdData::OpenFlags  docOpenFlags,  
  const IDFile &  jpegFile,  
  const PMReal &  xScale ,  
  const PMReal &  yScale ,  
  const PMReal &  desiredRes, 
  PMString outPutFolderpath, PMString subDirectoryPath, PMString toMoveJPG
 )   
  {
			
		//////////////////
			PMString outputFolder;
			outputFolder = outPutFolderpath;
			PMString tempOutputPath(outPutFolderpath);
			tempOutputPath.SetTranslatable(kFalse);
			SDKUtilities sdkutil;
			sdkutil.AppendPathSeparator (tempOutputPath);
		/////////////

     ErrorCode status = kFailure;
     UIDRef docRef(UIDRef::gNull);
     do {

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;
		//=====================
			IDFile tempjpegFile = jpegFile;
		//====================

        status = Utils<IDocumentCommands>()->Open(&docRef, documentFile,
            kSuppressUI, IOpenFileCmdData::kOpenOriginal, IOpenFileCmdData::kUseLockFile, kFalse);
////// Chetan 
		SDKFileHelper fileHelper(documentFile);
		PMString documentPath1(fileHelper.GetPath().GrabCString());
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:document to be export: " + documentPath1);
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:outPutFolderpath : " + outPutFolderpath);
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:subDirectoryPath : " + subDirectoryPath);
////// Chetan 
        if (status != kSuccess) {
            SDKFileHelper fileHelper(documentFile);
            //SNIPLOG("Couldn't open document \"%s\"", fileHelper.GetPath().GrabCString());
            break;
        }

        if (docRef.ExistsInDB() == kFalse) 
        {
            CA("Invalid document!");
            break;
        }
        InterfacePtr<IDocument> doc(docRef, UseDefaultIID());
        if (doc == nil)
        {
            CA("Could not get IDocument");
            break;
        }

		InterfacePtr<ISpreadList> spreadList(doc, IID_ISPREADLIST); 
		int32 imageCount =0;
		for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
		{ 
			
			UIDRef spreadRef(docRef.GetDataBase(), spreadList->GetNthSpreadUID(i)); 
			InterfacePtr<ISpread> spread(spreadRef, UseDefaultIID());
			int32 SpreadPageCount = spread->GetNumPages();
			
			for(int32 p=0; p < SpreadPageCount; p++)
			{

				UID originalPageUID = spread->GetNthPageUID(p);
				UIDRef originalPageUIDRef(docRef.GetDataBase(), originalPageUID);
				outputFolder.Append("\\");   //// Chetan

				PMString jpgPageName("");				
				PMString jpgPageNametemp=SDKUtilities::SysFileToPMString(&jpegFile);			
				FileUtils::GetBaseFileName (jpgPageNametemp,jpgPageName);  
				PMString StencilName("");
				StencilName = jpgPageName;
				jpgPageName.AppendNumber(0);
				jpgPageName.AppendNumber(imageCount);	
				StencilName = jpgPageName;
				
				jpgPageName.Append(".jpg");

			
				if(tempOutputPath.Contains(".jpg"))
				{
					sdkutil.RemoveLastElement(tempOutputPath);
					sdkutil.AppendPathSeparator(tempOutputPath);
				}
		
				tempOutputPath.Append(jpgPageName);
				tempjpegFile.SetString(tempOutputPath);

				outputFolder.Append(jpgPageName);   //// Chetan

				//UIDRef PageUIDRef(docRef.GetDataBase(), pageList->GetNthPageUID(i));
				if (spreadRef.ExistsInDB() == kFalse) 
				{
					CA("Invalid page!");
					break;
				}
			
				// export the snapshot to file
				// NOTE: this instantiates a SnapshotUtils
				status = this->SetPageToSnapshot(/*spreadRef*/originalPageUIDRef, xScale, yScale, desiredRes);
				if (status != kSuccess) 
				{
					CA("ERROR: SetPageToSnapshot failed!");
					break;
				}
		
				// reset the status (for break statements)
				status = kFailure;
		
				// export the snapshot to JPEG
				status = this->ExportImageToJPEG(/*jpegFile*/tempjpegFile);
				if (status != kSuccess) 
				{
					CA("ERROR: ExportImageToJPEG failed!");
					break;
				}
		
				// clean up the snapshot utils instance
				this->CleanUp();
			
				// if we get this far, set the status to success (it should be already...)
				status = kSuccess;

				IDFile destFolder(toMoveJPG);     /////// Chetan
				IDFile srcFile(outputFolder);     /////// Chetan

				//outPutFolderpath = "C:\\Program Files\\Adobe\\Adobe InDesign CS2 Products SDK\\build\\win\\sdkprj";

				if( !(outPutFolderpath == "" || subDirectoryPath == "" || jpgPageName == "" ))
				{
					//bool16 uploadResult = ptrIAppFramework->ClientActionGetAssets_setUploadFiletoDatabase(jpgPageName.GrabCString(), outPutFolderpath.GrabCString(), subDirectoryPath.GrabCString()); //Cs3
					bool16 uploadResult = ptrIAppFramework->ClientActionGetAssets_setUploadFiletoDatabase( const_cast<char*> (jpgPageName.GrabCString()), 
																										   const_cast<char*>(outPutFolderpath.GrabCString()), 
																										   const_cast<char*>(subDirectoryPath.GrabCString())
																										  ); //Cs4
					if(!uploadResult)
					{
						//CA("File Upload Failed for:" + jpgPageName);
						ptrIAppFramework->LogDebug("File Upload Failed for : " + jpgPageName);
					}
					else
					{
						//CA("File Uploaded Successfully :" + jpgPageName);
						ptrIAppFramework->LogDebug("File Uploaded Successfully : " + jpgPageName);

////  Chetan 
						bool16 status1 = FileUtils::MoveFile(srcFile ,destFolder);     
						if(status1 == kFalse)
						{
							//CA("file not moved");
						}
						sdkutil.RemoveLastElement(outputFolder);
////  Chetan 
						PMRect pageBounds;
						PMRect marginBoxBounds;
						bool16 result = this->getMarginBounds(originalPageUIDRef, pageBounds, marginBoxBounds);
						
						PMReal pageWidth = pageBounds.Right() - pageBounds.Left();
						PMReal pageHeight = pageBounds.Bottom() - pageBounds.Top();

						CStencilValue StencilValueObj;
						StencilValueObj.setbottom_margin(ToInt32(marginBoxBounds.Bottom()));
						StencilValueObj.settop_margin(ToInt32(marginBoxBounds.Top()));
						StencilValueObj.setleft_margin(ToInt32(marginBoxBounds.Left()));
						StencilValueObj.setright_margin(ToInt32(marginBoxBounds.Right()));

						StencilValueObj.setstencil_width(ToInt32(pageWidth));
						StencilValueObj.setstencil_height(ToInt32(pageHeight));

						StencilValueObj.setno_of_pages(p);
						StencilValueObj.setstencil_type_id(5); // Proof.
						
						StencilValueObj.setJpegFilename(jpgPageName);
						StencilValueObj.setStencilFilename(StencilName);
						StencilValueObj.setParentFolderPath(subDirectoryPath);
						//StencilValueObj.setstage_id(1);
						//StencilValueObj.setsection_id(CurrentSelectedSubSection);
						StencilValueObj.setsequence_no(imageCount);

						/*PMString ASD("pageWidth : " );
						ASD.AppendNumber(pageWidth);
						ASD.Append("   pageHeight : ");
						ASD.AppendNumber(pageHeight);
						ASD.Append("   numPages : ");
						ASD.AppendNumber(numPages);
						ASD.Append("   marginBoxBounds.Bottom() : ");
						ASD.AppendNumber(marginBoxBounds.Bottom());
						ASD.Append("   marginBoxBounds.Top() : ");
						ASD.AppendNumber(marginBoxBounds.Top());
						ASD.Append("   marginBoxBounds.Left() : ");
						ASD.AppendNumber(marginBoxBounds.Left());
						ASD.Append("   marginBoxBounds.Right() : ");
						ASD.AppendNumber(marginBoxBounds.Right());
						CA(ASD);*/

						int32 MasterRootId = ptrIAppFramework->getPubComments_insertStencilsValue(StencilValueObj);
						imageCount++;

					}

				}

				jpgPageName.Clear();
			}
			
		}
		
     } while (false);
 

     return status;
 }


 


 


ErrorCode CreateInddPreview::ExportImageToJPEG  (  const IDFile &  jpegSysFile  )  
 {
  /* Exports the snapshot to JPEG file. 


Parameters:
 jpegSysFile  IN File to write.  

Returns:
kSuccess on success, kFailure otherwise.*/


     ErrorCode status = kFailure;
     do
     {
         if (fSnapshotUtils == nil) 
         {
             ASSERT_FAIL("fSnapshotUtils is nil!");
             break;
         }
         uint32 mode = kOpenOut|kOpenTrunc;
         OSType fileType = 'JPEG';
         OSType creator = kAnyCreator;
 
         // create a stream to write out
         InterfacePtr<IPMStream> jpegStream(StreamUtil::CreateFileStreamWrite(jpegSysFile, mode, fileType, creator));
         if (jpegStream == nil) 
         {
             ASSERT_FAIL("Could not create a stream to write, so we can't export");
             break;
         }
 
         // export to JPEG
         status = fSnapshotUtils->ExportImageToJPEG(jpegStream);
     
     } while (false);
     return status;
 } 

 


ErrorCode CreateInddPreview::SetPageToSnapshot(UIDRef & itemToSnapshot,const PMReal&  xScale ,const  PMReal&  yScale ,const  PMReal&  desiredRes) 
  {
     ErrorCode status = kFailure;
     do {
         // make sure item is valid
         if (itemToSnapshot.ExistsInDB() == kFalse) 
         {
             ASSERT(itemToSnapshot.ExistsInDB());
             break;
         }
 
         // Clean up first.
         this->CleanUp();
 
         PMReal minRes = desiredRes;
         // create a new snapshot utils instance
         fSnapshotUtils = new SnapshotUtils(itemToSnapshot, 
                                            xScale,     // X Scale
                                            yScale,     // Y Scale
                                            desiredRes,    // desired resolution of resulting snapshot
                                            minRes,    // minimum resolution of resulting snapshot
                                            0,     // Extend the bounds of the area to be drawn by the given amount of bleed
											IShape::kPrinting, //IShape::kDrawFrameEdge,// Drawing flags (kPrinting suppresses drawing of margins and guides)
                                            kTrue,    // kTrue forces images and graphics to draw at full resolution, kFalse to draw proxies
                                            kFalse,    // kTrue to draw grayscale, kFalse to draw RGB
                                            kFalse);    // kTrue to add an alpha channel, kFalse no alpha channel
 
         if (fSnapshotUtils == nil) 
         {
             ASSERT_FAIL("Could not create SnapshotUtils");
             break;
         }
 
         // if we got here, everything is good
         status = kSuccess;
 
     } while (false);
     return status;
 }

//Member Function Documentation
void CreateInddPreview::CleanUp ()  
 /*
   Deletes internal instance of SnapshotUtils. 
*/


  {
     
     if (fSnapshotUtils) 
     {
         // clean up snapshot utils
         delete fSnapshotUtils;
         fSnapshotUtils = nil;
     }
 }



   bool16 CreateInddPreview::getMarginBounds(const UIDRef& pageUIDRef, PMRect& pageBounds, PMRect& marginBoxBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			CA("pageGeometry is invalid");
			break;
		}
		pageBounds = pageGeometry->GetStrokeBoundingBox();

		// ... and the page's margins.
		InterfacePtr<IMargins> pageMargins(pageGeometry, IID_IMARGINS);
		if (pageMargins == nil)
		{
			CA("pageMargins is invalid");
			break;
		}
		PMReal leftMargin,topMargin,rightMargin,bottomMargin;
		pageMargins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);

		//PMString ASD("pageWidth : " );
		///*ASD.AppendNumber(pageWidth);
		//ASD.Append("   pageHeight : ");
		//ASD.AppendNumber(pageHeight);
		//ASD.Append("   numPages : ");
		//ASD.AppendNumber(numPages);*/
		//ASD.Append("   bottomMargin : ");
		//ASD.AppendNumber(bottomMargin);
		//ASD.Append("   topMargin : ");
		//ASD.AppendNumber(topMargin);
		//ASD.Append("  leftMargin : ");
		//ASD.AppendNumber(leftMargin);
		//ASD.Append("   rightMargin : ");
		//ASD.AppendNumber(rightMargin);
		//CA(ASD);

		// Place the item into a frame the size of the page margins
		// with origin at the top left margin. Note that the frame
		// is automatically resized to fit the content if the 
		// content is a graphic. Convert the points into the
		// pasteboard co-ordinate space.
		/*PMPoint leftTop(pageBounds.Left()+leftMargin, pageBounds.Top()+topMargin);
		PMPoint rightBottom(pageBounds.Right() - rightMargin, pageBounds.Bottom() - bottomMargin);
		::InnerToPasteboard(pageGeometry,&leftTop);
		::InnerToPasteboard(pageGeometry,&rightBottom);*/

		marginBoxBounds.Left() = leftMargin;
		marginBoxBounds.Top() = topMargin;
		marginBoxBounds.Right() = rightMargin;
		marginBoxBounds.Bottom() = bottomMargin;	

		result = kTrue;
	}
	while(kFalse);
	return result;
}



ErrorCode CreateInddPreview::CreatePagePreviewForPAGE_BASED_WB  (  const IDFile &  documentFile,  
  const IOpenFileCmdData::OpenFlags  docOpenFlags,  
  const IDFile &  jpegFile,  
  const PMReal &  xScale ,  
  const PMReal &  yScale ,  
  const PMReal &  desiredRes, 
  PMString outPutFolderpath, PMString subDirectoryPath, PMString toMoveJPG
 )   
  {
			
	//////////////////
	PMString outputFolder;
	outputFolder = outPutFolderpath;
	PMString tempOutputPath(outPutFolderpath);
	tempOutputPath.SetTranslatable(kFalse);
	SDKUtilities sdkutil;
	sdkutil.AppendPathSeparator (tempOutputPath);
	/////////////
	int32 sectionVectorSize = static_cast<int32> (MediatorClass ::vector_subSectionSprayerListBoxParameters.size());

     ErrorCode status = kFailure;
     UIDRef docRef(UIDRef::gNull);
     do {

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;
		//=====================
			IDFile tempjpegFile = jpegFile;
		//====================

        status = Utils<IDocumentCommands>()->Open(&docRef, documentFile,
            kSuppressUI, IOpenFileCmdData::kOpenOriginal, IOpenFileCmdData::kUseLockFile, kFalse);
////// Chetan 
		SDKFileHelper fileHelper(documentFile);
		PMString documentPath1(fileHelper.GetPath().GrabCString());
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:document to be export: " + documentPath1);
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:outPutFolderpath : " + outPutFolderpath);
		ptrIAppFramework->LogInfo("AP46_CreateMedia::CreateInddPreview::CreatePagePreview:subDirectoryPath : " + subDirectoryPath);
////// Chetan 
        if (status != kSuccess) {
            SDKFileHelper fileHelper(documentFile);
            //SNIPLOG("Couldn't open document \"%s\"", fileHelper.GetPath().GrabCString());
            break;
        }

        if (docRef.ExistsInDB() == kFalse) 
        {
            CA("Invalid document!");
            break;
        }
        InterfacePtr<IDocument> doc(docRef, UseDefaultIID());
        if (doc == nil)
        {
            CA("Could not get IDocument");
            break;
        }

		InterfacePtr<ISpreadList> spreadList(doc, IID_ISPREADLIST); 
		int32 imageCount =0;
		int32 sectionPerPageCount = 0;
		for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
		{ 
			
			UIDRef spreadRef(docRef.GetDataBase(), spreadList->GetNthSpreadUID(i)); 
			InterfacePtr<ISpread> spread(spreadRef, UseDefaultIID());
			int32 SpreadPageCount = spread->GetNumPages();
			
			for(int32 p=0; p < SpreadPageCount; p++)
			{

				UID originalPageUID = spread->GetNthPageUID(p);
				UIDRef originalPageUIDRef(docRef.GetDataBase(), originalPageUID);

				subSectionSprayerListBoxParameters & subSectionListBoxParams = MediatorClass ::vector_subSectionSprayerListBoxParameters[sectionPerPageCount];

				PMString SectionIdString("");
				SectionIdString.AppendNumber(subSectionListBoxParams.vec_ssd[0].subSectionID);

				PMString outputFolderPerPage(outputFolder);
				outputFolderPerPage.Append("\\");   
				outputFolderPerPage.Append(SectionIdString);
				outputFolderPerPage.Append("\\");   

				PMString jpgPageName("");				
				PMString jpgPageNametemp=SDKUtilities::SysFileToPMString(&jpegFile);		
				

				FileUtils::GetBaseFileName (jpgPageNametemp,jpgPageName);  
				//CA("jpgPageName 1: " + jpgPageName);

				PMString StencilName("");
				jpgPageName = SectionIdString;
				StencilName = jpgPageName;
				jpgPageName.AppendNumber(0);
				jpgPageName.AppendNumber(/*imageCount*/p);	// for left page its 0 for right page its 1
				StencilName = jpgPageName;
				
				jpgPageName.Append(".jpg");

				//CA("jpgPageName 2: " + jpgPageName);			
				if(tempOutputPath.Contains(".jpg"))
				{
					sdkutil.RemoveLastElement(tempOutputPath);
					sdkutil.AppendPathSeparator(tempOutputPath);
				}
		
				tempOutputPath.Append(jpgPageName);
				tempjpegFile.SetString(tempOutputPath);

				//CA("jpgPageName 3: " + jpgPageName);
				//CA("tempjpegFile 1: " + tempOutputPath);

				outputFolderPerPage.Append(jpgPageName);   //// Chetan

				//UIDRef PageUIDRef(docRef.GetDataBase(), pageList->GetNthPageUID(i));
				if (spreadRef.ExistsInDB() == kFalse) 
				{
					CA("Invalid page!");
					break;
				}
			
				// export the snapshot to file
				// NOTE: this instantiates a SnapshotUtils
				status = this->SetPageToSnapshot(/*spreadRef*/originalPageUIDRef, xScale, yScale, desiredRes);
				if (status != kSuccess) 
				{
					CA("ERROR: SetPageToSnapshot failed!");
					break;
				}
		
				// reset the status (for break statements)
				status = kFailure;
		
				// export the snapshot to JPEG
				status = this->ExportImageToJPEG(/*jpegFile*/tempjpegFile);
				if (status != kSuccess) 
				{
					CA("ERROR: ExportImageToJPEG failed!");
					break;
				}
		
				// clean up the snapshot utils instance
				this->CleanUp();
			
				// if we get this far, set the status to success (it should be already...)
				status = kSuccess;

				IDFile destFolder(toMoveJPG);     /////// Chetan
				IDFile srcFile(outputFolder);     /////// Chetan

				//outPutFolderpath = "C:\\Program Files\\Adobe\\Adobe InDesign CS2 Products SDK\\build\\win\\sdkprj";
				PMString CurrentSectionSubDirectoryPath("");
				if(subDirectoryPath != "")
				{
					CurrentSectionSubDirectoryPath = subDirectoryPath;
					CurrentSectionSubDirectoryPath.Append("/");
					CurrentSectionSubDirectoryPath.Append(SectionIdString);
				}

				//CA("outPutFolderpath : " + outPutFolderpath + "\n subDirectoryPath : "  + CurrentSectionSubDirectoryPath + "\n jpgPageName : "+ jpgPageName);

				if( !(outputFolderPerPage == "" || subDirectoryPath == "" || jpgPageName == "" ))
				{
					bool16 uploadResult = ptrIAppFramework->ClientActionGetAssets_setUploadFiletoDatabase( const_cast<char*>(jpgPageName.GrabCString()), 
																										   const_cast<char*>(outPutFolderpath.GrabCString()), 
																										   const_cast<char*>(CurrentSectionSubDirectoryPath.GrabCString())
																										  );
					if(!uploadResult)
					{
						//CA("File Upload Failed for:" + jpgPageName);
						ptrIAppFramework->LogDebug("File Upload Failed for : " + jpgPageName);
					}
					else
					{
						//CA("File Uploaded Successfully :" + jpgPageName);
						ptrIAppFramework->LogDebug("File Uploaded Successfully : " + jpgPageName);

////  Chetan 
						bool16 status1 = FileUtils::MoveFile(srcFile ,destFolder);     
						if(status1 == kFalse)
						{
							//CA("file not moved");
						}
						sdkutil.RemoveLastElement(outputFolder);
////  Chetan 
						PMRect pageBounds;
						PMRect marginBoxBounds;
						bool16 result = this->getMarginBounds(originalPageUIDRef, pageBounds, marginBoxBounds);
						
						PMReal pageWidth = pageBounds.Right() - pageBounds.Left();
						PMReal pageHeight = pageBounds.Bottom() - pageBounds.Top();

						CStencilValue StencilValueObj;
						StencilValueObj.setbottom_margin(ToInt32(marginBoxBounds.Bottom()));
						StencilValueObj.settop_margin(ToInt32(marginBoxBounds.Top()));
						StencilValueObj.setleft_margin(ToInt32(marginBoxBounds.Left()));
						StencilValueObj.setright_margin(ToInt32(marginBoxBounds.Right()));

						StencilValueObj.setstencil_width(ToInt32(pageWidth));
						StencilValueObj.setstencil_height(ToInt32(pageHeight));

						StencilValueObj.setno_of_pages(p);
						StencilValueObj.setstencil_type_id(5); // Proof.
						
						StencilValueObj.setJpegFilename(jpgPageName);
						StencilValueObj.setStencilFilename(StencilName);
						StencilValueObj.setParentFolderPath(subDirectoryPath);
						//StencilValueObj.setstage_id(1);
						//StencilValueObj.setsection_id(/*CurrentSelectedSubSection*/subSectionListBoxParams.vec_ssd[0].subSectionID);
						StencilValueObj.setsequence_no(imageCount);

						/*PMString ASD("pageWidth : " );
						ASD.AppendNumber(pageWidth);
						ASD.Append("   pageHeight : ");
						ASD.AppendNumber(pageHeight);
						ASD.Append("   numPages : ");
						ASD.AppendNumber(numPages);
						ASD.Append("   marginBoxBounds.Bottom() : ");
						ASD.AppendNumber(marginBoxBounds.Bottom());
						ASD.Append("   marginBoxBounds.Top() : ");
						ASD.AppendNumber(marginBoxBounds.Top());
						ASD.Append("   marginBoxBounds.Left() : ");
						ASD.AppendNumber(marginBoxBounds.Left());
						ASD.Append("   marginBoxBounds.Right() : ");
						ASD.AppendNumber(marginBoxBounds.Right());
						CA(ASD);*/

						int32 MasterRootId = ptrIAppFramework->getPubComments_insertStencilsValue(StencilValueObj);
						imageCount++;

					}

				}

				jpgPageName.Clear();				
				sectionPerPageCount++;
			}			
		}		
     } while (false);
     return status;
 }


 

