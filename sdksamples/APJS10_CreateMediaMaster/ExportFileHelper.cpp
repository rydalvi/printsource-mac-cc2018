#include "VCPlugInHeaders.h"

#include "ExportFileHelper.h"
#include "IAppFramework.h"


#include "IBookManager.h"
#include "SDKFileHelper.h"
#include "ProgressBar.h"
#include "FileUtils.h"


#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

void ExportFileHelper::exportEachDocInBookAsPDF()
{

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return ;


	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession()/*gSession*/, UseDefaultIID());//Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
		return ; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
		return ;			
	}

	InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
	if (bookContentMgr == nil) 
	{
		CA("This book doesn't have a book content manager!  Something is wrong.");
		return;
	}
	K2Vector<PMString> bookContentNames = GetBookContentNames(bookContentMgr);
	
	int32 bookContentsCount = bookContentNames.size();
	if (bookContentsCount < 0)
	{
		CA("This book doesn't have any content!");
		return;
	}

	/*PMString docCount("bookContentsCount = ");
	docCount.AppendNumber(bookContentsCount);
	CA(docCount);*/

	PMString bookName = activeBook->GetBookTitleName();
	
	CharCounter lastPeriod = bookName.LastIndexOfWChar(UTF32TextChar('.'));
	int32 len = bookName.WCharLength();
	if (lastPeriod > 0) 
	{
		// period found - truncate last N chars
		bookName.Truncate(len-lastPeriod);
	}

	PMString title("Exporting Book: ");
	PMString temp = ("");
	temp.Append(bookName);
	title += temp;
	RangeProgressBar progressBar(title, 0, bookContentsCount, kTrue);
	progressBar.SetTaskText("Exporting Documents");


	UIDList bookContentsToExport(::GetDataBase(activeBook));
	for(int32 docIndex = 0; docIndex < bookContentsCount; docIndex++)
	{
		// we wants specific book content to output
		UID bookContentUID = bookContentMgr->GetNthContent(docIndex);
		bookContentsToExport.Append(bookContentUID);
		bool16 exportAllDocsInBook = kFalse;

		// create a PDF filename for this document

		PMString docName = bookContentNames[docIndex];
		//CA("docName = " + docName);

		
		CharCounter lastPeriod = docName.LastIndexOfWChar(UTF32TextChar('.'));
		int32 len = docName.WCharLength();
		if (lastPeriod > 0) 
		{
			// period found - truncate last N chars
			docName.Truncate(len-lastPeriod);
		}

		// just append a .pdf at the end
		docName.Append(".pdf");

		PMString fileName;
		FileUtils::GetFileName(docName,fileName);
		PMString tempString("Exporting ");
		tempString += fileName;
		tempString += "...";
		progressBar.SetTaskText(tempString);
		progressBar.SetPosition(docIndex);


		//CA("pdfDocPath = " + docName);
		SDKFileHelper fileHelper(docName);
		IDFile pdfFilename = fileHelper.GetIDFile(); 

		// get PDF export styles
		K2Vector<PMString> pdfExportStyles = GetPDFExportStyles();
		// get current PDF export style index
		int32 defaultPDFExportStyle = 0;/*GetCurrentPDFExportStyle() >= 0;*/
		/*if (defaultPDFExportStyle < 0) 
		{
			CA("NOTE: You haven't opened the PDF Export Style dialog yet! Please open it at least once.");
			defaultPDFExportStyle = 0;
		}*/
	
		// perform the export operation
		ErrorCode status = kCancel; 
		status = ExportBookAsPDF(activeBook, 
							   pdfFilename, 
							   bookContentsToExport, 
							   exportAllDocsInBook);




	
		bookContentsToExport.Clear();
	}
}
/* GetBookContentNames
*/
K2Vector<PMString> ExportFileHelper::GetBookContentNames(IBookContentMgr* bookContentMgr)
{
	K2Vector<PMString> bookContentNames;
	bookContentNames.clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database? "); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content
			InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			if (bookLink == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
				break; // out of for loop
			}

			//// get the book name and add it to the list
			//PMString* baseName = bookLink->GetBaseName();
			//ASSERT(baseName && baseName->empty() == kFalse);
			//bookContentNames.push_back(*baseName);



			PMString* pathName = bookLink->GetFullName();
			bookContentNames.push_back(*pathName);
			//CA("pathName = " + *pathName);
		}

	} while (false);
	return bookContentNames;
}


K2Vector<PMString> ExportFileHelper::GetPDFExportStyles(void)
{
	K2Vector<PMString> styles;
	styles.clear();
	do {
        InterfacePtr<IPDFExptStyleListMgr> styleMgr
			((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
        if (styleMgr == nil)
		{
			ASSERT_FAIL("Failed to load IPDFExptStyleListMgr");
			break;
		}
		int32 numStyles = styleMgr->GetNumStyles();
		for (int32 i = 0 ; i < numStyles ; i++) 
		{
			PMString name;
			ErrorCode status = kSuccess;
			status = styleMgr->GetNthStyleName(i, &name);
			if (status != kSuccess) 
			{
				ASSERT_FAIL(FORMAT_ARGS("Could not get %dth PDF Export style name", i));
				continue;
			}
			else
			{
				styles.push_back(name);
			}
		}
	} while (false);
	return styles;
}


/* ExportBookAsPDF
*/
ErrorCode ExportFileHelper::ExportBookAsPDF(IBook* book, 
                                              IDFile& pdfFileName, 
											  UIDList& bookContentsToExport, 
											  bool16 exportAllDocsInBook,
											  IBookOutputActionCmdData::OutputUIOptions options)
{
	ErrorCode status = kFailure;
	do
	{
		SDKFileHelper fileHelper(pdfFileName);
		if (book == nil ||
			fileHelper.GetPath().empty()) 
		{ 
			CA("One or more preconditions are not met - exiting");
			break; 
		} 
		if (exportAllDocsInBook != kTrue && bookContentsToExport.IsEmpty())
		{
            /*	If you want to export specific contents within a book
				to PDF, make sure you fill out UIDList with kBookContentBoss UIDs 
				that you want to export.
				
				Hint: Use IBookContentManager (kBookBoss), which gives you UIDs for kBookContentBoss. 
				Also, use IBookContent and IDataLink (GetFullName() method is useful)
				in kBookContentBoss. 
				
				If you want to add docs to a book, use the kAddDocToBookCmdBoss command. (BookID.h)
			*/
			CA("You want to export specific documents from the book, but your bookContentsToExport UIDList is empty!");
			break;
		}

		// create the book export action command
		InterfacePtr<ICommand> bookExportCmd(CmdUtils::CreateCommand(kBookExportActionCmdBoss));
		if (bookExportCmd == nil) 
		{ 
			ASSERT(bookExportCmd);
			break; 
		}
		// Get the IBookOutputActionCmdData data interface
		InterfacePtr<IBookOutputActionCmdData>  data(bookExportCmd, IID_IBOOKOUTPUTACTIONCMDDATA);
		if (data == nil) 
		{ 
			break; 
		}

		// get the filename of the book database
		IDFile bookFile = book->GetBookFileSpec();
		// set the book file name, book content list, and the export-all flag
		data->Set(bookFile, &bookContentsToExport, exportAllDocsInBook); 
		// check if the PDF filepath is a valid path
		SDKFileHelper helper(pdfFileName);
		bool16 hasFileName = (helper.GetPath().empty() == kFalse);
		// set the output filename
		data->SetOutputFile(pdfFileName, hasFileName);
		// set the UI options
		data->SetOutputUIOptions(options);

		// process the command
		status = CmdUtils::ProcessCommand(bookExportCmd);

		ASSERT(status == kSuccess);

	} while (false);
	return status;
}

