#ifndef __ExportFileHelper_h__
#define __ExportFileHelper_h__


///export pdf related files
#include "IBookContentMgr.h"
#include "IBook.h"
#include "IDataLink.h"
#include "IBookOutputActionCmdData.h"
#include "IPDFExptStyleListMgr.h"
#include "PreferenceUtils.h"

class ExportFileHelper
{
public:
	void exportEachDocInBookAsPDF();






private:
	K2Vector<PMString> GetBookContentNames(IBookContentMgr* bookContentMgr);
	K2Vector<PMString> GetPDFExportStyles(void);
	ErrorCode ExportBookAsPDF(IBook* book, 
							  IDFile& pdfFileName, 
							  UIDList& bookContentsToExport, 
							  bool16 exportAllDocsInBook = kTrue,
							  IBookOutputActionCmdData::OutputUIOptions options = IBookOutputActionCmdData::kSuppressEverything);
	

};

#endif