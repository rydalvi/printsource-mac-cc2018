//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/SpraySettingsDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"


// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
//#include "CDialogObserver.h"
#include "CAlert.h"
// Project includes:
#include "CMMID.h"
#include <ITriStateControlData.h>
#include <IWidgetParent.h>
#include <ITextControlData.h>
#include <SDKFileHelper.h>
#include <SDKUtilities.h>
#include <IDialogController.h>
#include "MediatorClass.h"		   //19-jan 
//-#include "SDKListBoxHelper.h"      //19-jan
#include "CDialogObserver.h"

#include "SpraySettingDialogController.h"



#define CA(x) CAlert::InformationAlert(x)

/**	Implements IObserver based on the partial implementation CDialogObserver; 
	allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	
	 @ingroup basicdialog
	
*/
class SpraySettingsDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SpraySettingsDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~SpraySettingsDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. 
				Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(SpraySettingsDialogObserver, kSpraySettingsDialogObserverImpl)

/* AutoAttach
*/
void SpraySettingsDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SpraySettingsDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to BasicDialog's info button widget.
		//AttachToWidget(kSpraySettingsIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		// Attach to other widgets you want to handle dynamically here.
		//30Oct..The stencil selection is shifted in the listbox.
		//30Oct..AttachToWidget(kALL1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kItemStencil1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kProductStencil1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kALLRadioButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kItemStencilRadioButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

		//30Oct..InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		//30Oct..dialogController->SetTriStateControlData(kALLRadioButtonWidgetID,kTrue);
		
		AttachToWidget(kspraySectionWithOutPageBreakWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kAddSectionStencilWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kAtTheStartOfSectionWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kAtTheStartOfEachPageWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kAtTheStartOfFirstPageAndSpreadWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCMMOKButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCMMCancelButton_WidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		

	} while (false);
}

/* AutoDetach
*/
void SpraySettingsDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SpraySettingsDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from BasicDialog's info button widget.
		//DetachFromWidget(kSpraySettingsIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		// Detach from other widgets you handle dynamically here.
		//30Oct..DetachFromWidget(kALL1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..DetachFromWidget(kItemStencil1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..DetachFromWidget(kProductStencil1RollOverIconButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..DetachFromWidget(kALLRadioButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..DetachFromWidget(kItemStencilRadioButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kspraySectionWithOutPageBreakWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kAddSectionStencilWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kAtTheStartOfSectionWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kAtTheStartOfEachPageWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kAtTheStartOfFirstPageAndSpreadWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCMMOKButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCMMCancelButton_WidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		
	} while (false);
}

/* Update
*/
void SpraySettingsDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("SpraySettingsDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		//following code is added by vijay choudhari on 9/8/2006///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////From Here

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		
		/*
		//30Oct..
		//The stencil selection is shifted in the listbox.
		IControlView * ItemStencilRealEditBoxControlView = panelControlData->FindWidget(kItemStencilRealEditBoxWidgetID);
		IControlView * ItemStencil1RollOverIconButtonControlView = panelControlData->FindWidget(kItemStencil1RollOverIconButtonWidgetID);
		IControlView * ProductStencilRealEditBoxControlView = panelControlData->FindWidget(kProductStencilRealEditBoxWidgetID);
		IControlView * ProductStencil1RollOverIconButtonControlView = panelControlData->FindWidget(kProductStencil1RollOverIconButtonWidgetID);
		IControlView * ALLRealEditBoxControlView = panelControlData->FindWidget(kALLRealEditBoxWidgetID);
		IControlView * ALL1RollOverIconButton = panelControlData->FindWidget(kALL1RollOverIconButtonWidgetID);

		if(theSelectedWidget == kALLRadioButtonWidgetID && theChange == kTrueStateMessage)
		{
			ALLRealEditBoxControlView->Enable();
			ALL1RollOverIconButton->Enable();
			ItemStencilRealEditBoxControlView->Disable();
			ItemStencil1RollOverIconButtonControlView->Disable();
			ProductStencilRealEditBoxControlView->Disable();
			ProductStencil1RollOverIconButtonControlView->Disable();

		}
		
		if(theSelectedWidget == kItemStencilRadioButtonWidgetID && theChange == kTrueStateMessage)
		{
			ItemStencilRealEditBoxControlView->Enable();
			ItemStencil1RollOverIconButtonControlView->Enable();
			ProductStencilRealEditBoxControlView->Enable();
			ProductStencil1RollOverIconButtonControlView->Enable();
			ALLRealEditBoxControlView->Disable();
			ALL1RollOverIconButton->Disable();
		}
				
		if ( (theSelectedWidget == kALL1RollOverIconButtonWidgetID || theSelectedWidget == kItemStencil1RollOverIconButtonWidgetID || theSelectedWidget == kProductStencil1RollOverIconButtonWidgetID)&& theChange == kTrueStateMessage)
		{			
			do
			{
		
			InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
			if(findParentOfRollOverIconButtonWidget == nil)
			{
				CAlert::InformationAlert("findParentOfRollOverIconButtonWidget == nil");
				break;
			}
			IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
			if(primaryResourcePanelWidgetUnKnown == nil)
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
			if(primaryResourcePanelWidgetControlView == nil)
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetControlView == nil");
				break;
			}
			InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
			if(primaryResourcePanelWidgetPanelControlData == nil)			
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetPanelControlData == nil");
				break;
			}

			PMString pathOfFile("");
			//find parent of primaryResourcePanelWidgetPanel which is a cellpanelwidget
			InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(panelControlData,UseDefaultIID());
			if(findParentOfPrimaryResourcePanelWidget == nil)
			{
				CAlert::InformationAlert("findParentOfPrimaryResourcePanelWidget == nil");
				break;
			}
			IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
			if(cellPanelWidgetUnKnown == nil)
			{
				CAlert::InformationAlert("cellPanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
			if(cellPanelWidgetPanelControlData == nil)
			{
				CAlert::InformationAlert("cellPanelWidgetPanelControlData == nil");
				break;
			}
			int32 index = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);

		//	if(theSelectedWidget == kALL1RollOverIconButtonWidgetID)//kListBoxSelectTemplateFileButtonWidgetID
		//	{
				SDKFileOpenChooser FileChooseDLG;
				FileChooseDLG.SetTitle("Select Template File.");
				FileChooseDLG.ShowDialog();
				if(!FileChooseDLG.IsChosen())
				{ 
					break;
				}
				pathOfFile = FileChooseDLG.GetPath();
				//3Jun
				IDFile templateFile(pathOfFile);
				SDKUtilities sdkutils;
				PMString extension = sdkutils.GetExtension(templateFile);
				if(extension != "indt")
				{
					CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
					break;
				}
				//end 3Jun
				
				
				
				if(theSelectedWidget == kALL1RollOverIconButtonWidgetID)
				{
					InterfacePtr<ITextControlData> iTextControlData(ALLRealEditBoxControlView,UseDefaultIID());
					if(iTextControlData == nil)
					{
						CA("iTextControlData == nil");
						break;
					}

					iTextControlData->SetString(pathOfFile);

				}

				if(theSelectedWidget == kItemStencil1RollOverIconButtonWidgetID)
				{
					InterfacePtr<ITextControlData> iTextControlData(ItemStencilRealEditBoxControlView,UseDefaultIID());
					if(iTextControlData == nil)
					{
						CA("iTextControlData == nil");
						break;
					}

					iTextControlData->SetString(pathOfFile);

				}	


				if(theSelectedWidget == kProductStencil1RollOverIconButtonWidgetID)
				{
					InterfacePtr<ITextControlData> iTextControlData(ProductStencilRealEditBoxControlView,UseDefaultIID());
					if(iTextControlData == nil)
					{
						CA("iTextControlData == nil");
						break;
					}

					iTextControlData->SetString(pathOfFile);

				}	

				//MediatorClass::vector_subSectionSprayerListBoxParameters[index].masterFileWithCompletePath=pathOfFile;

		//	}

			
			}while(kFalse);




		}



		
//////////////////////////////////////////////////////////////////////////////Upto here
///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//
		//if (theSelectedWidget == kSpraySettingsIconSuiteWidgetID && theChange == kTrueStateMessage)
		//{
		//	// About box
		//	CAlert::ModalAlert
		//	(
		//		kSpraySettingsAboutBoxStringKey,		// Alert string
		//		kOKString, 						// OK button
		//		kNullString, 					// No second button
		//		kNullString, 					// No third button
		//		1,								// Set OK button to default
		//		CAlert::eInformationIcon		// Information icon.
		//	);
		//}
		//

		//end 30Oct..
		*/

//19-jan from here
		if((theSelectedWidget == kCMMOKButtonWidgetID || theSelectedWidget == kOKButtonWidgetID) && theChange == kTrueStateMessage)
		{   
			//CA("Ok button in spray setting");
			IPanelControlData* cellPanelWidgetPanelControlData = MediatorClass ::iPanelCntrlDataPtr ;
			int32 listBoxRowIndex = MediatorClass ::selectedIndexOfListBoxElement ;
/*-			SDKListBoxHelper listHelper(nil, 0, 0, 0);
			listHelper.UpdateSubSectionOptionIcon(cellPanelWidgetPanelControlData,kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID,kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID,listBoxRowIndex);	
-*/
 
			IPanelControlData* primaryWidgetPanelControlData = MediatorClass ::iPrimaryPanelCntrlDataPtr ;
			IControlView * ListBoxApplySpraySettingsUnderneathElementsRollOverButtonCntrlView = primaryWidgetPanelControlData->FindWidget(kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID);
			if(ListBoxApplySpraySettingsUnderneathElementsRollOverButtonCntrlView == nil)
			{
				CA("ListBoxApplySpraySettingsUnderneathElementsRollOverButtonCntrlView == nil");
				break;
			}

			ListBoxApplySpraySettingsUnderneathElementsRollOverButtonCntrlView->Enable();
			
			SpraySettingsDialogController spraySettingsDialogControllerObj(this);
			spraySettingsDialogControllerObj.ApplyDialogFields(nil,kInvalidWidgetID);
			
			if(theSelectedWidget == kCMMOKButtonWidgetID)
				CDialogObserver::CloseDialog();
		}

		if((theSelectedWidget == kCMMCancelButton_WidgetID || theSelectedWidget == kCancelButton_WidgetID) && (theChange == kTrueStateMessage))
		{
			//CA("cancel button");
			if(theSelectedWidget == kCMMCancelButton_WidgetID)
				CDialogObserver::CloseDialog();
			break;
		}

	
//New Code added for price book on 27/12/07
		if(theSelectedWidget == kAddSectionStencilWidgetID /*&& theChange == kTrueStateMessage*/)
		{
			//CA("kAddSectionStencilWidgetID is selected");
			
			IControlView * kAddSectionStencilCheckBoxControlView = panelControlData->FindWidget(kAddSectionStencilWidgetID);
			if(kAddSectionStencilCheckBoxControlView == nil)
			{
				CA("kAddSectionStencilCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectAddSectionStencilCheckBoxTriState(kAddSectionStencilCheckBoxControlView,UseDefaultIID());
			if(selectAddSectionStencilCheckBoxTriState == nil)
			{
				CA("selectAddSectionStencilCheckBoxTriState == nil");
				break;
			}

			//ITriStateControlData::TriState addSectionStencilState = dialogController->GetTriStateControlData(kAddSectionStencilWidgetID);

			if(selectAddSectionStencilCheckBoxTriState->IsSelected())
			{
				InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				if(!panelControlData) 
				{
					break;
				}
				IControlView * atTheStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfSectionWidgetID);
				if(atTheStartOfSectionCheckBoxControlView == nil)
				{
					CA("atTheStartOfSectionCheckBoxControlView == nil");
					break;
				}
				atTheStartOfSectionCheckBoxControlView->Enable();
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfSectionCheckBoxTriState(atTheStartOfSectionCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfSectionCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfSectionCheckBoxTriState == nil");
					break;
				}
				selectAtTheStartOfSectionCheckBoxTriState->Select(kTrue);
				IControlView * atTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
				if(atTheStartOfEachPageCheckBoxControlView == nil)
				{
					CA("atTheStartOfEachPageCheckBoxControlView == nil");
					break;
				}
				atTheStartOfEachPageCheckBoxControlView->Enable();

				IControlView * atTheStartOfFirstPageAndSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
				if(atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil)
				{
					CA("atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil");
					break;
				}
				atTheStartOfFirstPageAndSpreadCheckBoxControlView->Enable();
				
			}

			if(selectAddSectionStencilCheckBoxTriState->IsDeselected())
			{
				
				//CA("selectAddSectionStencilCheckBoxTriState is deselected");
				InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				if(!panelControlData) 
				{
					break;
				}
				IControlView * atTheStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfSectionWidgetID);
				if(atTheStartOfSectionCheckBoxControlView == nil)
				{
					CA("atTheStartOfSectionCheckBoxControlView == nil");
					break;
				}
				
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfSectionCheckBoxTriState(atTheStartOfSectionCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfSectionCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfSectionCheckBoxTriState == nil");
					break;
				}

				selectAtTheStartOfSectionCheckBoxTriState->Deselect (kTrue);

				atTheStartOfSectionCheckBoxControlView->Disable();



				IControlView * atTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
				if(atTheStartOfEachPageCheckBoxControlView == nil)
				{
					CA("atTheStartOfEachPageCheckBoxControlView == nil");
					break;
				}

				InterfacePtr<ITriStateControlData>selectAtTheStartOfEachPageCheckBoxTriState(atTheStartOfEachPageCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfEachPageCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfEachPageCheckBoxTriState == nil");
					break;
				}

				if(selectAtTheStartOfEachPageCheckBoxTriState->IsSelected())
				{

					selectAtTheStartOfEachPageCheckBoxTriState->Deselect(kTrue);
					MediatorClass ::AtTheStartOfEachPageState = kFalse;
				}

				atTheStartOfEachPageCheckBoxControlView->Disable();	
			
				IControlView * atTheStartOfFirstPageAndSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
				if(atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil)
				{
					CA("atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil");
					break;
				}

				InterfacePtr<ITriStateControlData>selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState(atTheStartOfFirstPageAndSpreadCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil");
					break;
				}

				if(selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState->IsSelected())
				{

					selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState->Deselect(kTrue);
					MediatorClass ::AtTheStartOfEachPageAndSpreadState = kFalse;
				}
				atTheStartOfFirstPageAndSpreadCheckBoxControlView->Disable();

			}
		}

		if(theSelectedWidget == kAtTheStartOfSectionWidgetID /*&& theChange == kTrueStateMessage*/)
		{
			//CA("kAtTheStartOfSectionWidgetID");
			
			IControlView * kAtTheStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfSectionWidgetID);
			if(kAtTheStartOfSectionCheckBoxControlView == nil)
			{
				CA("kAtTheStartOfSectionCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectkAtTheStartOfSectionCheckBoxTriState(kAtTheStartOfSectionCheckBoxControlView,UseDefaultIID());
			if(selectkAtTheStartOfSectionCheckBoxTriState == nil)
			{
				CA("selectkAtTheStartOfSectionCheckBoxTriState == nil");
				break;
			}

			//ITriStateControlData::TriState addSectionStencilState = dialogController->GetTriStateControlData(kAddSectionStencilWidgetID);

			if(selectkAtTheStartOfSectionCheckBoxTriState->IsSelected())
			{
				//CA("kAtTheStartOfSectionWidgetID is selected");
				MediatorClass ::AtTheStartOfSectionState = kTrue;
			}
			if(selectkAtTheStartOfSectionCheckBoxTriState->IsDeselected())
			{
				//CA("kAtTheStartOfSectionWidgetID is Deselected");
				MediatorClass ::AtTheStartOfSectionState = kFalse;
			}

		}

		if(theSelectedWidget == kAtTheStartOfEachPageWidgetID /*&& theChange == kTrueStateMessage*/)
		{
			//CA("kAtTheStartOfEachPageWidgetID");
			
			IControlView * kAtTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
			if(kAtTheStartOfEachPageCheckBoxControlView == nil)
			{
				CA("kAtTheStartOfEachPageCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectkAtTheStartOfEachPageCheckBoxTriState(kAtTheStartOfEachPageCheckBoxControlView,UseDefaultIID());
			if(selectkAtTheStartOfEachPageCheckBoxTriState == nil)
			{
				CA("selectkAtTheStartOfEachPageCheckBoxTriState == nil");
				break;
			}

			//ITriStateControlData::TriState addSectionStencilState = dialogController->GetTriStateControlData(kAddSectionStencilWidgetID);

			if(selectkAtTheStartOfEachPageCheckBoxTriState->IsSelected())
			{
				//CA("kAtTheStartOfEachPageWidgetID is selected");
				MediatorClass ::AtTheStartOfEachPageState = kTrue;

				
				//To deselect other widget
				IControlView * atTheStartOfFirstPageAndSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
				if(atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil)
				{
					CA("atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil");
					break;
				}
				atTheStartOfFirstPageAndSpreadCheckBoxControlView->Enable();
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState(atTheStartOfFirstPageAndSpreadCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil");
					break;
				}
				if(selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState->IsSelected())
				{
					selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState->Deselect(kTrue);
					MediatorClass ::AtTheStartOfEachPageAndSpreadState = kFalse;
				}
				//..
			}
			if(selectkAtTheStartOfEachPageCheckBoxTriState->IsDeselected())
			{
				//CA("kAtTheStartOfEachPageWidgetID is deselected");
				MediatorClass ::AtTheStartOfEachPageState = kFalse;
			

			//To select  other widget
				IControlView * atTheStartOfFirstPageAndSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
				if(atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil)
				{
					CA("atTheStartOfFirstPageAndSpreadCheckBoxControlView == nil");
					break;
				}
				atTheStartOfFirstPageAndSpreadCheckBoxControlView->Enable();
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState(atTheStartOfFirstPageAndSpreadCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil");
					break;
				}
			}
			//..

		}
		if(theSelectedWidget == kAtTheStartOfFirstPageAndSpreadWidgetID /*&& theChange == kTrueStateMessage*/)
		{
			//CA("kAtTheStartOfFirstPageAndSpreadWidgetID");
			
			IControlView * kAtTheStartOfFirstPageAndSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
			if(kAtTheStartOfFirstPageAndSpreadCheckBoxControlView == nil)
			{
				CA("kAtTheStartOfFirstPageAndSpreadCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectkAtTheStartOfFirstPageAndSpreadCheckBoxTriState(kAtTheStartOfFirstPageAndSpreadCheckBoxControlView,UseDefaultIID());
			if(selectkAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil)
			{
				CA("selectkAtTheStartOfFirstPageAndSpreadCheckBoxTriState == nil");
				break;
			}

			//ITriStateControlData::TriState addSectionStencilState = dialogController->GetTriStateControlData(kAddSectionStencilWidgetID);

			if(selectkAtTheStartOfFirstPageAndSpreadCheckBoxTriState->IsSelected())
			{
				//CA("kAtTheStartOfFirstPageAndSpreadWidgetID is selected");
				MediatorClass ::AtTheStartOfEachPageAndSpreadState = kTrue;

				//To deselect other widget
				IControlView * atTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
				if(atTheStartOfEachPageCheckBoxControlView == nil)
				{
					CA("atTheStartOfEachPageCheckBoxControlView == nil");
					break;
				}
			//	atTheStartOfEachPageCheckBoxControlView->Enable();
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfEachPageCheckBoxTriState(atTheStartOfEachPageCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfEachPageCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfEachPageCheckBoxTriState == nil");
					break;
				}
				if(selectAtTheStartOfEachPageCheckBoxTriState->IsSelected())
				{
					selectAtTheStartOfEachPageCheckBoxTriState->Deselect(kTrue);
					MediatorClass ::AtTheStartOfEachPageState = kFalse;
				}
				//..
			}
			if(selectkAtTheStartOfFirstPageAndSpreadCheckBoxTriState->IsDeselected())
			{
				//CA("kAtTheStartOfFirstPageAndSpreadWidgetID is deselected");
				MediatorClass ::AtTheStartOfEachPageAndSpreadState = kFalse;
				//To deselect other widget
				IControlView * atTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
				if(atTheStartOfEachPageCheckBoxControlView == nil)
				{
					CA("atTheStartOfEachPageCheckBoxControlView == nil");
					break;
				}
				//atTheStartOfEachPageCheckBoxControlView->Enable();
				
				InterfacePtr<ITriStateControlData>selectAtTheStartOfEachPageCheckBoxTriState(atTheStartOfEachPageCheckBoxControlView,UseDefaultIID());
				if(selectAtTheStartOfEachPageCheckBoxTriState == nil)
				{
					CA("selectAtTheStartOfEachPageCheckBoxTriState == nil");
					break;
				}

				if(selectAtTheStartOfEachPageCheckBoxTriState->IsSelected())
				{
					//CA("selectAtTheStartOfEachPageCheckBoxTriState is selected");
					MediatorClass ::AtTheStartOfEachPageAndSpreadState = kTrue;
				}
				
				//..
			}

		}

		if(theSelectedWidget == kspraySectionWithOutPageBreakWidgetID /*&& theChange == kTrueStateMessage*/)
		{
			//CA("kspraySectionWithOutPageBreakWidgetID");
			IControlView * kAspraySectionWithOutPageBreakCheckBoxControlView = panelControlData->FindWidget(kspraySectionWithOutPageBreakWidgetID);
			if(kAspraySectionWithOutPageBreakCheckBoxControlView == nil)
			{
				CA("kAspraySectionWithOutPageBreakCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectkspraySectionWithOutPageBreakCheckBoxTriState(kAspraySectionWithOutPageBreakCheckBoxControlView,UseDefaultIID());
			if(selectkspraySectionWithOutPageBreakCheckBoxTriState == nil)
			{
				//CA("selectkspraySectionWithOutPageBreakCheckBoxTriState == nil");
				break;
			}
				if(selectkspraySectionWithOutPageBreakCheckBoxTriState->IsSelected())
				{
					//CA("selectkspraySectionWithOutPageBreakCheckBoxTriState is selected");
					MediatorClass ::AtSpraySectionWithOutPageBreakState = kTrue;
				}
				if(selectkspraySectionWithOutPageBreakCheckBoxTriState->IsDeselected())
				{
					//CA("selectkspraySectionWithOutPageBreakCheckBoxTriState is deselected");
					MediatorClass ::AtSpraySectionWithOutPageBreakState = kFalse;
				}
		}

	} while (false);
}

// End, BscDialogObserver.cpp.




