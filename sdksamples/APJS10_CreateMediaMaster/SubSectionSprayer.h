#ifndef __SUBSECTIONSPRAYER_H__
#define __SUBSECTIONSPRAYER_H__

#include "VCPluginHeaders.h"
#include "DSFID.h"
#include "IDataSprayer.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "vector"
#include "CSprayStencilInfo.h"

class RangeProgressBar;

using namespace std;


bool16 deleteStartPageOfTheDocument(UIDRef & startPageUIDRef);

/*-
class CSprayStencilInfo
{
public:
	bool16 isCopy;
	bool16 isProductCopy;
	bool16 isSectionCopy;
	bool16 isEventField;

	bool16 isAsset;
	bool16 isProductAsset;
	bool16 isSectionLevelAsset;

	bool16 isDBTable;
	bool16 isProductDBTable;
	bool16 isCustomTablePresent;

	bool16 isHyTable;
	bool16 isProductHyTable;
	bool16 isSectionLevelHyTable;
	
	bool16 isChildTag;
	bool16 isProductChildTag;

	bool16 isBMSAssets;
	bool16 isProductBMSAssets;
	bool16 isSectionLevelBMSAssets;

	bool16 isItemPVMPVAssets;
	bool16 isProductPVMPVAssets;
	bool16 isSectionPVMPVAssets;
	bool16 isPublicationPVMPVAssets;
	bool16 isCatagoryPVMPVAssets;

	bool16 isEventSectionImages;
	bool16 isCategoryImages;

	vector<int32 > AttributeIds;
	vector<int32 > AssetIds;
	vector<int32 > dBTypeIds;
	vector<int32 > HyTypeIds;	

	vector<int32 > itemPVAssetIdList;
	vector<int32 > productPVAssetIdList;
	vector<int32 > sectionPVAssetIdList;
	vector<int32 > publicationPVAssetIdList;
	vector<int32 > catagoryPVAssetIdList;
	vector<int32 > categoryAssetIdList;
	vector<int32 > eventSectionAssetIdList;

	CSprayStencilInfo()
	{
		isCopy = kFalse;
		isProductCopy = kFalse;
		isSectionCopy = kFalse;
		isEventField = kFalse;

		isAsset = kFalse;
		isProductAsset = kFalse;
		isSectionLevelAsset = kFalse;

		isDBTable = kFalse;
		isProductDBTable = kFalse;
		isCustomTablePresent = kFalse;

		isHyTable = kFalse;
		isProductHyTable = kFalse;
		isSectionLevelHyTable = kFalse;
	
		isChildTag = kFalse;
		isProductChildTag = kFalse;

		isBMSAssets = kFalse;
		isProductBMSAssets = kFalse;
		isSectionLevelBMSAssets = kFalse;

		isItemPVMPVAssets = kFalse;
		isProductPVMPVAssets = kFalse;
		isSectionPVMPVAssets = kFalse;
		isPublicationPVMPVAssets = kFalse;
		isCatagoryPVMPVAssets = kFalse;
		isEventSectionImages = kFalse;
		isCategoryImages = kFalse;
	}	
};
typedef vector<CSprayStencilInfo> vectorCSprayStencilInfo;
-*/
 
//class DynFrameStruct
//{
//public:
//	int HorzCnt;
//	int VertCnt;
//	bool16 isLastHorzFrame;
//	PMRect BoxBounds;
//};
//typedef vector<DynFrameStruct> FrameBoundsList;

class BoxBounds
{
public:
	PMReal Top;
	PMReal Bottom;
	PMReal Left;
	PMReal Right;
	UIDRef BoxUIDRef;
	PMReal compressShift;
	PMReal enlargeShift;
	bool16 shiftUP;
	bool16 isUnderImage;
};

typedef vector<BoxBounds> vectorBoxBounds;


class SubSectionSprayer 
{
public:
	//pure virtual function "startSpraying" should be overridden
	SubSectionSprayer()
	{
		sprayedProductIndex = 0; //to start with the second product
	}
	void startSpraying(void);
	void startSprayingSubSection(bool16 &isCancelHit);//startSprayingSubSection(void);

	//void DoDialog();

	PMString selectedSubSection;

//private:
	//subsectionsprayer related
	bool16 getCurrentPage(UIDRef&, UIDRef&);
	bool16 getSelectedBoxIds(UIDList&);
	bool16 getMaxLimitsOfBoxes(const UIDList&, PMRect&, vectorBoxBounds &boxboundlist);
	void getMaxHorizSprayCount(const PMRect&, const PMRect&, int16&);
	void getMaxVertSprayCount(const PMRect&, const PMRect&, int16&);
	bool16 getMarginBounds(const UIDRef&, PMRect&);
	bool16 getPageBounds(const UIDRef&, PMRect&);
	void sprayPage(const UIDRef&, const UIDList&, const PMRect&, const PBPMPoint&, int32&, RangeProgressBar&, bool16);
	//bool16 willBoxesOverlap(const PMRect&, const PMRect&, const int16&, const int16&, PBPMPoint&);
	bool16 getBoxPosition(const PMRect&, const PMRect&, const int16&, const int16&, PBPMPoint&);
	bool16 CopySelectedItems();
	void moveBoxes(const UIDList&, const PBPMPoint&);
	bool16 getAllBoxIds(const UIDList&);
	void sprayFirstProductOfSubSection(const UIDList&);
	void sprayFromSecondProductOfSubSection(const UIDList& , const PMRect& , const PBPMPoint& , int32, RangeProgressBar&,bool16 &);
	bool8 isBoxParent(const UIDRef&, IDataBase*);
	UIDList getParentSelectionsOnly(UIDList&, IDataBase*);
	
	void callSprayForThisBox(UIDRef, TagList&);
	bool16 callIsFrameTagged(const UIDRef&);
	bool16 callSprayForTaggedBox(const UIDRef&);
	
//	void setDataSprayersUIDList(vector<int32>);
//	void setSSIdInPFTreeModel(int32);
//	bool16 getAllIdForLevel(int32, int32&, vector<int32>&);
//	void getAllSetOfIds(int32, vector<int32>&);
	void setPublicationID(double);
 	void setImagePath(PMString);

	vector<double> allPFIdList;
	int32 sprayedProductIndex;
	bool16 getAllIdForLevel(int32& numIds, vector<double>& idList);
	bool16 getMaxLimitsOfBoxesForProductList(vector<PMRect> boxMarginList, PMRect& maxBounds);
	bool16 getBoxPositionForResizableFrame(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, const int16& horizCnt,const int16& vertCnt,  FrameBoundsList ProductBlockList, PBPMPoint& moveToPoints);
	void sprayPageWithResizableFrame(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag);

	void moveAutoResizeBoxAfterSpray(const UIDList& copiedBoxUIDList, vectorBoxBounds vectorCurBoxBoundsBeforeSpray );
	int deleteThisBoxUIDList(UIDList boxUIDList);

	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);


	//9 th May
	void startSprayingSubSectionForSpread(void);

	void sprayPageWithResizableFrameNew(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag , bool16 &allProductSprayed );
	bool16 getBoxPositionForResizableFrameNew(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag);
	bool16 getBoxPositionForResizableFrameNewVerticalFlow(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag);

	ErrorCode AdjustMaxLimitsOfBoxes(const UIDList& boxList, PMRect PagemaxBounds);




	//end 9 th May

	UIDList selectUIDList;
	PMString imagePath;

	//2Nov..to get read from pageCount global variable.
	int32  getPageCount();
	bool16 getLastPageUIDRef(UIDRef& lastPageUIDRef, UIDRef& lastSpreadUIDRef);
	bool16 CopyAndPasteTheSelectedStencilItemsToTheLastPage();
	bool16 ScriptingVersionOfAddNewPage();
	//end 2Nov..
//26-april
	void sprayPageWithResizableFrameNewWhite(const UIDRef& pageUIDRef, const PMRect& origMaxBoxBounds, int32& currentProd, int32& currentSpread, RangeProgressBar& progressBar, PMReal TopX, PMReal TopY);
	void getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage);
	int32 getNoOfPagesfromCurrentSpread();
//26-april
//28-april
	void sprayFromSecondProductOfSubSectionWhite(const UIDList& , const PMRect& , const PBPMPoint& , int32, RangeProgressBar&);
//28-april

	PMPoint PageToSpreadCoOrdinates(const UIDRef& pageUIDRef, const PMPoint& boundsInPageCoords);

	bool16 getStencilInfo(const UIDList& productSelUIDList,CSprayStencilInfo& objCSprayStencilInfo);

	bool16 sprayPageWithResizableFrameForSection(bool16 toggleFlag,PMRect &marginBoxBounds,FrameBoundsList &ProdBlockBoundList,int16 &horizCnt,int16 &vertCnt);		//	Amit	14/1/08

	void startSprayingSubSectionNew(bool16 &toggleFlag,PMRect &marginBoxBounds,bool16 &isCancelHit);

	void startSprayingSubSectionForSpecSheet();

	bool16 getAllBoxIdsForGroupFrames(UIDList& tempFrameList);
    
    int32 checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow);
    bool16 checkIsHorizontalFlowFlagPresent(const UIDList &selectUIDList);
    bool16 addNewPageHere(PMRect& PagemarginBoxBounds);
    ErrorCode CreatePages(const IDocument* iDocument, const UIDRef spreadToAddTo,const int16 numPagesToInsert, const int16 pageToInsertAt, const bool16 allowShuffle);
    
};

#endif
