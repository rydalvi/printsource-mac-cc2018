#ifndef __CPDFConverter_h__
#define __CPDFConverter_h__

#include "HelperInterface.h"
#include "CAlert.h"
#include "PMString.h"

#include "IBoolData.h"
#include "ICommand.h" 
#include "IDataBase.h"
#include "IOutputPages.h"
#include "IPDFExportPrefs.h"
#include "IPDFSecurityPrefs.h"
#include "IPrintContentPrefs.h"
#include "ISysFileData.h"
#include "IUIFlagData.h"

#include "CmdUtils.h"
#include "K2SmartPtr.h"
#include "ProgressBar.h"
#include "SDKUtilities.h"		
#include "UIDList.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "BookID.h" 
#include "DocumentID.h" // for IID_IDOCUMENT, IID_ISYSFILEDATA and IID_IUIFLAGDATA
#include "PDFID.h" // for kPDFExportCmdBoss and IID_IPDFEXPORTPREFS
#include "PrintID.h" // for IID_IPRINTCONTENTPREFS

#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "ErrorUtils.h"
//#include "ExportID.h"
#include "IDocument.h"
#include "IExportProvider.h"
#include "IK2ServiceProvider.h"
#include "IK2ServiceRegistry.h"
#include "IObjectModel.h"
#include "ISession.h"
#include "ISelectionUtils.h"
#include "ITextFocus.h"
#include "ITextSelectionSuite.h"
//#include "LayoutUtils.h" //Cs3 
#include "ILayoutUtils.h"  //Cs4
#include "PersistUtils.h"
#include "SDKUtilities.h"
#include "Utils.h"
#include "IBookUtils.h"
#include "IApplication.h"
#include "IDocumentUtils.h"
#include "IDocumentList.h"



class CPDFConverter
{
public:
	
	ErrorCode OutputPagesToPDF(const PMString& OutputfileName, 
						   const UIDList& pagesUIDList, 
						   const K2::UIFlags uiFlags1 = /*kFullUI*/kSuppressUI);

	//Takes the input parameters as path of Input file and output file and then converts to PDF
	//void ConvertoPDF(const SysFile& Inputfile, const PMString& Outputfile);

	//Takes the input parameters as Pointer to current open document And path of output file
	void ConvertoPDFwithDoc(IDocument* doc, const PMString& Outputfile);

	void ExportPDF(IDocument *frontDoc,IDFile idFile,bool16 first_Doc);

	ErrorCode ExportDocUsingOldSettings(const PMString& OutputfileName, 
						   const UIDList& pagesUIDList, 
						   const K2::UIFlags uiFlags1 = kSuppressUI);



};

#endif