//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CPDFID_h__
#define __CPDFID_h__

#include "SDKDef.h"

// Company:
#define kCPDFCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCPDFCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCPDFPluginName	"Create PDFs"			// Name of this plug-in.
#define kCPDFPrefixNumber	0xABA4800 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCPDFVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCPDFAuthor		"Catsy"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCPDFPrefixNumber above to modify the prefix.)
#define kCPDFPrefix		RezLong(kCPDFPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCPDFStringPrefix	SDK_DEF_STRINGIZE(kCPDFPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCPDFMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCPDFMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCPDFPluginID, kCPDFPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCPDFActionComponentBoss, kCPDFPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCPDFDialogBoss, kCPDFPrefix + 2)
DECLARE_PMID(kClassIDSpace, kCPDFSelectDocsDialogBoss, kCPDFPrefix + 3) //**Added
DECLARE_PMID(kClassIDSpace, kCPDFListBoxWidgetBoss, kCPDFPrefix + 4)//***Added
DECLARE_PMID(kClassIDSpace, kCPDFCustomIconSuiteWidgetBoss, kCPDFPrefix + 5)
DECLARE_PMID(kClassIDSpace, kCPDFIconSuiteWidgetBoss, kCPDFPrefix + 6)//-----ADD By Lalit--------

//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCPDFBoss, kCPDFPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICPDFINTERFACE, kCPDFPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCPDFActionComponentImpl, kCPDFPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCPDFDialogControllerImpl, kCPDFPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kCPDFDialogObserverImpl, kCPDFPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kCPDFSelectDocsDialogControllerImpl, kCPDFPrefix + 3) //***Added
//-------ADDED BY LALIT------
DECLARE_PMID(kImplementationIDSpace, kCPDFSelectDocsDialogObserverImpl, kCPDFPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kCPDFListBoxObserverImpl, kCPDFPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCPDFImpl, kCPDFPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCPDFAboutActionID, kCPDFPrefix + 0)

DECLARE_PMID(kActionIDSpace, kCPDFDialogActionID, kCPDFPrefix + 4)
DECLARE_PMID(kActionIDSpace, kCreatePDFsActionID, kCPDFPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCPDFActionID, kCPDFPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCPDFDialogWidgetID, kCPDFPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kCPDFSelectDocsDialogWidgetID, kCPDFPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kCPDFStaticTextWidgetID, kCPDFPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kCPDFListParentWidgetId, kCPDFPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kCPDFListBoxWidgetID, kCPDFPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kCPDFCustomIconSuiteWidget, kCPDFPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kCheckIconWidgetID, kCPDFPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kPDFNameTextWidgetID, kCPDFPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kCPDFUnCheckIconWidgetID, kCPDFPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kCPDFCheckIconWidgetID, kCPDFPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kSelectAllCheckBoxWidgetID, kCPDFPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgOK1ButtonWidgetID, kCPDFPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCancel1ButtonWidgetID, kCPDFPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kRfhDlgOK2ButtonWidgetID, kCPDFPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kCPDFWidgetID, kCPDFPrefix + 25)


// "About Plug-ins" sub-menu:
#define kCPDFAboutMenuKey			kCPDFStringPrefix "kCPDFAboutMenuKey"
#define kCPDFAboutMenuPath		kSDKDefStandardAboutMenuPath kCPDFCompanyKey

// "Plug-ins" sub-menu:
#define kCPDFPluginsMenuKey 		kCPDFStringPrefix "kCPDFPluginsMenuKey"
#define kCPDFPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCPDFCompanyKey kSDKDefDelimitMenuPath kCPDFPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCPDFAboutBoxStringKey	kCPDFStringPrefix "kCPDFAboutBoxStringKey"
#define kCPDFTargetMenuPath kCPDFPluginsMenuPath

// Menu item positions:

#define kCPDFDialogTitleKey               kCPDFStringPrefix "kCPDFDialogTitleKey"
#define kCPDFSelectDocsDialogTitleKey     kCPDFStringPrefix "kCPDFSelectDocsDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kCPDFDialogMenuItemKey kCPDFStringPrefix "kCPDFDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kCPDFDialogMenuItemPosition	12.0

//Resource ID
#define kSDKDefDialog1ResourceID 900 //**Added	
#define kCPDFListElementRsrcID   901 //**Added
#define kCheckIconID						11000
#define kUncheckIconID						11001

// Initial data format version numbers
#define kCPDFFirstMajorFormatNumber  RezLong(1)
#define kCPDFFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCPDFCurrentMajorFormatNumber kCPDFFirstMajorFormatNumber
#define kCPDFCurrentMinorFormatNumber kCPDFFirstMinorFormatNumber

//PNGR ID
#define kPNGOK1IconRsrcID               2000
#define kPNGOK1IconRollRsrcID           2000
#define kPNGCancel1IconRsrcID           2001
#define kPNGCancel1IconRollRsrcID       2001
#define kPNGOK2IconRsrcID               2002
#define kPNGOK2IconRollRsrcID           2002

#endif // __CPDFID_h__
