#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "CPDFID.h"
#include "SystemUtils.h"
//#include "RefreshData.h"
#include "SDKListBoxHelper.h"
//#include "MediatorClass.h"
#include "IListControlData.h"
#include "IPanelControlData.h"
//Added on 30May by Yogesh
#include "ITriStateControlData.h"
#include "CDialogController.h"
//ended on 30May

#include "CAlert.h"
//#include "IMessageServer.h"

#define CA(x) CAlert::InformationAlert(x)
extern K2Vector<PMString> bookContentNames;		//----ADDED BY LALIT------
extern K2Vector<bool16>  isSelected;
extern IControlView* SelectAllControlView;

//#define FILENAME			PMString("CPDFListBoxObserver.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
////Added By Dattatray on 30/10 
//bool16 IsEventInListBox = kFalse;
//extern RefreshDataList rBookDataList;
//extern RefreshDataList UniqueBookDataList;
//extern int GroupFlag;
class CPDFListBoxObserver : public CObserver
{
public:
	CPDFListBoxObserver(IPMUnknown *boss);
	~CPDFListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(CPDFListBoxObserver, kCPDFListBoxObserverImpl)

CPDFListBoxObserver::CPDFListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

CPDFListBoxObserver::~CPDFListBoxObserver()
{
}

void CPDFListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void CPDFListBoxObserver::AutoDetach()
{
 	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void CPDFListBoxObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
//---------ADDED BY LALIT-----------
	//	IsEventInListBox = kTrue;
	InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
	{	
		
		//CA("CPDFListBoxObserver::Update");
		SDKListBoxHelper listHelper(this, kCPDFPluginID);
		IControlView *listBoxControlView  = pPanelData->FindWidget(kCPDFListBoxWidgetID);

		InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
		int32 currentlySelectedIndex = listCntl->GetClickItem (); 
		
		if(bookContentNames.size()==0){
			//CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
			return;
		}
		
	//	int32 index =0;
		//for(itr=bookContentNames.begin();itr!=bookContentNames.end() && itrIsSelected != isSelected.end();itr++,itrIsSelected++)
		//{
			//if(index == currentlySelectedIndex)
			//{
				//CA("index == currentlySelectedIndex");
				listHelper.CheckUncheckRow(listBoxControlView,currentlySelectedIndex,!isSelected[currentlySelectedIndex]);
				//(*itrIsSelected) = !(*itrIsSelected);
				isSelected[currentlySelectedIndex] = !isSelected[currentlySelectedIndex];

			//}
			//index++;			
		//}

		K2Vector<bool16>::iterator itrIsSelected;		
		itrIsSelected = isSelected.begin();

		for(itrIsSelected = isSelected.begin();itrIsSelected != isSelected.end();  itrIsSelected++ )
		{
			if(*itrIsSelected == kFalse)
			{					
				//CA("*itrIsSelected == kFalse");
				InterfacePtr<ITriStateControlData>selectAllrisetControlData(SelectAllControlView/*selectAllCheckbxControlView*/,UseDefaultIID());
				if(selectAllrisetControlData==nil)
				{
					//CA("selectAllCheckbxControlView==nil");
					break;
				}
				selectAllrisetControlData->Deselect/*Select*/(kFalse,kFalse);						
			}				
		}
		bool16 checkedSelectAll = kTrue;
		for(itrIsSelected = isSelected.begin();itrIsSelected != isSelected.end();  itrIsSelected++ )
		{
			if( *itrIsSelected == kFalse)
			{
				checkedSelectAll = kFalse;
				//CA("*itrIsSelected == kFalse");
				break;						
			}				
		}
		if(checkedSelectAll == kTrue)
		{
			InterfacePtr<ITriStateControlData>selectAllrisetControlData(SelectAllControlView/*selectAllCheckbxControlView*/,UseDefaultIID());
			if(selectAllrisetControlData==nil)
			{
				//CA("selectAllCheckbxControlView==nil");
				return;
			}
			selectAllrisetControlData->Select/*Select*/(kFalse,kFalse);
		}

		//$$$$$$$$$$$$$$

		//IControlView * uncheckboxControlview = pPanelData->FindWidget(/*widgetId*/kCPDFUnCheckIconWidgetID);
		//if(uncheckboxControlview==nil){
		//	CA("checkboxControlview==nil");
		//	return;
		//}		
		//
		//IControlView * checkboxControlview = pPanelData->FindWidget(/*widgetId*/kCPDFCheckIconWidgetID);
		//if(checkboxControlview==nil)
		//{
		//	CA("checkboxControlview1==nil");
		//	return;
		//}		
		//if(checkboxControlview->IsVisible())
		//{
		//	CA("checkboxControlview= >>>IsVisible");
		//	//uncheckboxControlview->Show();
		//	listHelper.CheckUncheckRow(listBoxControlView,currentlySelectedIndex,kFalse);
		//}
		//
		//if(uncheckboxControlview->IsVisible())
		//{
		//	CA("uncheckboxControlview->IsVisible()");
		//	//checkboxControlview->Show();
		//	listHelper.CheckUncheckRow(listBoxControlView,currentlySelectedIndex,kTrue);
		//}
		//************

		//int32 index=0;				
		//for(itr=bookContentNames.begin();itr!=bookContentNames.end();itr++)
		//{
			//CA("Inside for loop");
		/*	WidgetID widgetId=listBoxControlView->GetWidgetID();
			if(widgetId==nil)
			{
				CA("widgetId==nil");
				break;
			}*/
			//IControlView * uncheckboxControlview = pPanelData->FindWidget(/*widgetId*/kCPDFUnCheckIconWidgetID);
			//if(uncheckboxControlview==nil)
			//{
			//	CA("checkboxControlview==nil");
			//	return;
			//}		
			//
			//IControlView * checkboxControlview = pPanelData->FindWidget(/*widgetId*/kCPDFCheckIconWidgetID);
			//if(checkboxControlview==nil)
			//{
			//	CA("checkboxControlview1==nil");
			//	return;
			//}		
			//if(checkboxControlview->IsVisible()){
			//	CA("checkboxControlview= >>>IsVisible");
			//	uncheckboxControlview->Show();
			//	//listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
			//}
			///*else
			//	CA("Is checkboxControlview >>>Not Visibele");*/
			//if(uncheckboxControlview->IsVisible()){

			//	//CA("IsVisible");
			//	checkboxControlview->Show();
			//	//listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
			//}
			/*else
				CA("Is Not Visibele");*/
			
			
			
		//	InterfacePtr<IListBoxController> checkboxCntl(checkboxControlview,IID_ILISTBOXCONTROLLER);

			/*InterfacePtr<ITriStateControlData>checkboxState(checkboxControlview,UseDefaultIID());
			if(checkboxState==nil)
			{
				CA("checkboxState==nil");
				return;
			}*/
		/*	if(checkboxState->IsSelected())
			{
				CA("Selected");
				listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
				checkboxState->Select();
				
			}
			else
			{
				CA("UnSelected");
				listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
				checkboxState->Deselect();
			}*/

		//}
		
				
	}

}
 
