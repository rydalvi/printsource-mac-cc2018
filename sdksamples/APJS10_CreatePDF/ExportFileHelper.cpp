#include "VCPlugInHeaders.h"

#include "ExportFileHelper.h"
#include "CPDFConverter.h"
#include "IAppFramework.h"


#include "IBookManager.h"
#include "SDKFileHelper.h"
#include "SDKLayoutHelper.h"
#include "ProgressBar.h"
#include "FileUtils.h"
//#include "LayoutUIUtils.h" //Cs3
#include "ILayoutUIUtils.h"   //Cs4
#include "SDKUtilities.h"
#include "IDialogMgr.h"
#include "LocaleSetting.h"
#include "CPDFID.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SDKListBoxHelper.h"
#include "IBookContent.h"

K2Vector<PMString> bookContentNames; 
extern K2Vector<bool16>  isSelected; 

#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

void ExportFileHelper::exportEachDocInBookAsPDF()
{

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return ;


	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID());//Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
		return ; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
		return ;			
	}

	InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
	if (bookContentMgr == nil) 
	{
		CA("This book doesn't have a book content manager!  Something is wrong.");
		return;
	}

	bookContentNames.clear(); 
	K2Vector<PMString> bookContentPaths = GetBookContentNames(bookContentMgr,bookContentNames);

	
	int32 bookContentsCount = bookContentPaths.size();
	if (bookContentsCount < 0)
	{
		CA("This book doesn't have any content!");
		return;
	}

///open dialog for selecting folder to save output files
	SDKFolderChooser folderChooser;
	folderChooser.SetTitle("Select Folder");
	folderChooser.ShowDialog();
	if(!folderChooser.IsChosen())
	{	
		//cancelClicked=kTrue;
		return;
	}
	
	PMString outputFolderPath=folderChooser.GetPath();
	//CA(outputFolderPath);
	if(outputFolderPath.IsEmpty() == kTrue)
	{	CA("Please Select folder to store output files.");
		return;
	}
	
//	this->DoDocSelectDialog();  //**** Opening  the SelectDocZ Dialog

	//PMString docCount("bookContentsCount = ");
	//docCount.AppendNumber(bookContentsCount);
	//CA(docCount);

	PMString bookName = activeBook->GetBookTitleName();
	
	CharCounter lastPeriod = bookName.LastIndexOfWChar(UTF32TextChar('.'));
	int32 len = bookName.WCharLength();
	if (lastPeriod > 0) 
	{
		// period found - truncate last N chars
		bookName.Truncate(len-lastPeriod);
	}
//PMString s("isSelected.size()	:	");
//s.AppendNumber(isSelected.size());
//CA(s);
//-	if(isSelected.size()<1)
//-		return;

	K2Vector<bool16>::iterator itr;
//-	itr = isSelected.begin();
	int32 checkedNumber=0;
    
//-	for(checkedNumber=0; itr!=isSelected.end(); itr++)
//-	{
//-		if(*itr==kTrue)
//-			checkedNumber++;
//-	}
//-	itr=isSelected.begin();

	PMString title("Exporting Book: ");
	PMString temp = ("");
	temp.Append(bookName);
	title += temp;
	RangeProgressBar progressBar(title, 0, checkedNumber, kTrue);
	progressBar.SetTaskText("Exporting Documents");


	UIDList bookContentsToExport(::GetDataBase(activeBook));

	//***
	

	SDKUtilities sdkutils;
	bool16 isfirstDocToExport = kTrue;
	int32 doc=0;
	for(int32 docIndex = 0; docIndex < bookContentsCount; docIndex++ /*-,itr++-*/)
	{
		/*-
		if((*itr) == kFalse) 
			continue;
		-*/
		if(docIndex != 0)
		{
			CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
		}
		// we wants specific book content to output
		UID bookContentUID = bookContentMgr->GetNthContent(docIndex);
		bookContentsToExport.Append(bookContentUID);
		bool16 exportAllDocsInBook = kFalse;

		// create a PDF filename for this document

		//PMString docName = bookContentPaths[docIndex];
		//CA("docName = " + docName);
		//PMString temp = docName;

	//removing inddDocName
	//	CharCounter lastPeriod1 = temp.LastIndexOfWChar(UTF32TextChar('\\'));
	//	int32 len1 = temp.WCharLength();
	//	if (lastPeriod1 > 0)
	//	{
	//		// period found - truncate last N chars
	//		temp.Truncate(len1-lastPeriod1);
	//	}
	
	//getting section name
	//	CharCounter lastPeriod2 = temp.LastIndexOfWChar(UTF32TextChar('\\'));
	//	int32 len2 = temp.WCharLength();
	//	PMString* str = NULL;
	//	if (lastPeriod2 > 0)
	//	{
	//		str = temp.Substring(lastPeriod2 + 1 ,len2 - lastPeriod2);
	//		//CA("section name = " + *str);
	//	}
		PMString PDFFolderPath(outputFolderPath);
		sdkutils.AppendPathSeparator(PDFFolderPath);
        
//        CA(PDFFolderPath);
//		PDFFolderPath.Append(*str);
//		sdkutils.AppendPathSeparator(PDFFolderPath);
//		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&PDFFolderPath), kTrue);

		//folderName.Append(docName);
		//docName.Clear();
		//docName.Append(folderName);
		//CA("after adding folder path docName = " + docName);	
		

		//PMString tempString("Exporting ");
		//tempString += fileName;
		//tempString += "...";
		//progressBar.SetTaskText(tempString);
		//progressBar.SetPosition(doc);
		//doc++;

		//CA("pdfDocPath = " + docName);
		//SDKFileHelper fileHelper(docName);
		

		// get PDF export styles
		K2Vector<PMString> pdfExportStyles = GetPDFExportStyles();
		// get current PDF export style index
		int32 defaultPDFExportStyle = 0;/*GetCurrentPDFExportStyle() >= 0;*/
		/*if (defaultPDFExportStyle < 0) 
		{
			CA("NOTE: You haven't opened the PDF Export Style dialog yet! Please open it at least once.");
			defaultPDFExportStyle = 0;
		}*/
	
		// perform the export operation
//ErrorCode status = kCancel; 
//status = ExportBookAsPDF(activeBook, 
//					   pdfFilename, 
//					   bookContentsToExport, 
//					   exportAllDocsInBook);

		PMString inddDocPath = bookContentPaths[docIndex];
        //CA("inddDocPath: " + inddDocPath);

		CPDFConverter pdfConverter;
		//if(docIndex != 0)
		//{			
		//	SDKFileHelper fileHelper1(inddDocPath);
		//	IDFile Inputfile = fileHelper1.GetIDFile();
		//	bool16 HIJ = kFalse;
		//	bool16 HPP = kFalse;
		//	// opens the input file in the Background
		//	IDocument* fntDoc = Utils<IBookUtils>()->OpenDocAtBackground 
		//						(
		//							Inputfile, // in
		//							HIJ,// out
		//							HPP
		//						);
		//	
		//	if(fntDoc==nil)
		//	{
		//		CA("Document not found");
		//		break;
		//	}
		//	//CPDFConverter pdfConverter;
		//	pdfConverter.ConvertoPDFwithDoc(fntDoc,docName);

		//	Utils<IBookUtils>()->ProcessCloseDocCmd(fntDoc);
		//}		
		//else
		{
			//openDoc
			SDKLayoutHelper sdklhelp;
            SDKUtilities asd;
			//IDFile templateIDFile = asd.PMStringToSysFile(&inddDocPath);
			//
            
            IDFile CurrFile;
            bool16 IsMissingPluginFlag = kFalse;
            
            IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
            (
             ::GetDataBase(activeBook),
             bookContentUID,
             CurrFile,
             IsMissingPluginFlag
             );
            
            bool16 Flag1 = kFalse;
            Flag1 =  FileUtils::DoesFileExist(CurrFile);
            if(Flag1 == kFalse)
            {
                CA("File does not exists");
                ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::File does not exists");
                continue;
            }

            PMString docName = CurrFile.GetFileName();
            //CA("docName: "+ docName);
            
            CharCounter lastPeriod = docName.LastIndexOfWChar(UTF32TextChar('.'));
            int32 len = docName.WCharLength();
            if (lastPeriod > 0)
            {
                // period found - truncate last N chars
                docName.Truncate(len-lastPeriod);
            }
            
            // just append a .pdf at the end
            docName.Append(".pdf");
            //CA(docName);
            
            
            PMString tempString("Exporting ");
            tempString += docName;
            tempString += "...";
            progressBar.SetTaskText(tempString);
            progressBar.SetPosition(doc);
            doc++;
            
            PDFFolderPath.Append(docName);
            //CA("PDFFolderPath: " + PDFFolderPath);
            SDKFileHelper fileHelper(PDFFolderPath);
            IDFile pdfFilename = fileHelper.GetIDFile();
            
            UIDRef templateDocUIDRef = sdklhelp.OpenDocument(CurrFile);
			if(templateDocUIDRef == UIDRef ::gNull)
			{
				CA("templateDocUIDRef == UIDRef ::gNull");
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::uidref of templatedoc is invalid");
				continue;
			}						
			
			ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
			if(err == kFailure)
			{
				CA("Error occured while opening the layoutwindow of the template");
				continue;
			}
//
			//IDocument* fntDoc = ::GetFrontDocument(); //Cs3
			IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
			if( fntDoc == nil)
				CA(" fntDoc == nil ");
			
			//CPDFConverter pdfConverter;
			if(isfirstDocToExport == kTrue)
			{
				pdfConverter.ExportPDF(fntDoc,pdfFilename,kTrue);
				isfirstDocToExport = kFalse;
			}
			else
				pdfConverter.ExportPDF(fntDoc,pdfFilename,kFalse);
			//pdfConverter.ConvertoPDFwithDoc(fntDoc,docName);

			//pdfConverter.ConvertoPDFwithDoc(fntDoc,docName);

			//Utils<IBookUtils>()->ProcessCloseDocCmd(fntDoc);  //Cs3

			//Cs4 **************Start For Closing Front Document			
			SDKLayoutHelper layoutHelper;
			layoutHelper.CloseDocument(templateDocUIDRef,kTrue,kSuppressUI);
			//Cs4 End*****************

		}

	
		bookContentsToExport.Clear();
	}
}
/* GetBookContentNames
*/
K2Vector<PMString> ExportFileHelper::GetBookContentNames(IBookContentMgr* bookContentMgr,K2Vector<PMString>& bookContentNames)
{
	K2Vector<PMString> bookContentPaths;
	bookContentPaths.clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				//somehow, we got a bad UID
				//CA("contentUID == kInvalidUID continue ");
				continue; // just goto the next one
			}
			// get the datalink that points to the book content   Depricated Cs3
			//InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			//if (bookLink == nil) 
			//{
			//	CA("IDataLink for book #%d is missing");
			//	//ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
			//	break; // out of for loop
			//}

			////get the book name and add it to the list
			//PMString* baseName = bookLink->GetBaseName();  //***Depricated 
			//ASSERT(baseName && baseName->empty() == kFalse);
				
			//**** Aaded for cs4 New
			InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
			if (bookContent == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IBookContent for book #%d is missing", i));
				//CA("bookContent == nil");
				break; // out of for loop
			}
			PMString baseName = bookContent->GetShortName();
			//****** Testing						
			//PMString asd("ExportFileHelper::GetBookContentNames");
			//asd.Append("\n  bookContentNames = "+(/***/baseName)+ "\n MyBookName = "/*shortName*/);
			//CA(asd);
			//*******

			bookContentNames.push_back(/***/baseName);
			//Get Full Path
			//PMString* pathName = bookLink->GetFullName(); //cs 3 Depricated
			PMString pathName = bookContent->GetLongName();
			bookContentPaths.push_back(/***/pathName);
			//CA("pathName = " + pathName);
		}

	} while (false);
	return bookContentPaths;
}


K2Vector<PMString> ExportFileHelper::GetPDFExportStyles(void)
{
	K2Vector<PMString> styles;
	styles.clear();
	do {
        InterfacePtr<IPDFExptStyleListMgr> styleMgr
			((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
        if (styleMgr == nil)
		{
			ASSERT_FAIL("Failed to load IPDFExptStyleListMgr");
			break;
		}
		int32 numStyles = styleMgr->GetNumStyles();
		for (int32 i = 0 ; i < numStyles ; i++) 
		{
			PMString name;
			ErrorCode status = kSuccess;
			status = styleMgr->GetNthStyleName(i, &name);
			if (status != kSuccess) 
			{
				ASSERT_FAIL(FORMAT_ARGS("Could not get %dth PDF Export style name", i));
				continue;
			}
			else
			{
				styles.push_back(name);
			}
		}
	} while (false);
	return styles;
}


/* ExportBookAsPDF
*/
ErrorCode ExportFileHelper::ExportBookAsPDF(IBook* book, 
                                              IDFile& pdfFileName, 
											  UIDList& bookContentsToExport, 
											  bool16 exportAllDocsInBook,
											  IBookOutputActionCmdData::OutputUIOptions options)
{
	ErrorCode status = kFailure;
	do
	{
		SDKFileHelper fileHelper(pdfFileName);
		if (book == nil ||
			fileHelper.GetPath().empty()) 
		{ 
			CA("One or more preconditions are not met - exiting");
			break; 
		} 
		if (exportAllDocsInBook != kTrue && bookContentsToExport.IsEmpty())
		{
            /*	If you want to export specific contents within a book
				to PDF, make sure you fill out UIDList with kBookContentBoss UIDs 
				that you want to export.
				
				Hint: Use IBookContentManager (kBookBoss), which gives you UIDs for kBookContentBoss. 
				Also, use IBookContent and IDataLink (GetFullName() method is useful)
				in kBookContentBoss. 
				
				If you want to add docs to a book, use the kAddDocToBookCmdBoss command. (BookID.h)
			*/
			CA("You want to export specific documents from the book, but your bookContentsToExport UIDList is empty!");
			break;
		}

		// create the book export action command
		InterfacePtr<ICommand> bookExportCmd(CmdUtils::CreateCommand(kBookExportActionCmdBoss));
		if (bookExportCmd == nil) 
		{ 
			ASSERT(bookExportCmd);
			break; 
		}
		// Get the IBookOutputActionCmdData data interface
		InterfacePtr<IBookOutputActionCmdData>  data(bookExportCmd, IID_IBOOKOUTPUTACTIONCMDDATA);
		if (data == nil) 
		{ 
			break; 
		}

		// get the filename of the book database
		IDFile bookFile = book->GetBookFileSpec();
		// set the book file name, book content list, and the export-all flag
		data->Set(bookFile, &bookContentsToExport, exportAllDocsInBook); 
		// check if the PDF filepath is a valid path
		SDKFileHelper helper(pdfFileName);
		bool16 hasFileName = (helper.GetPath().empty() == kFalse);
		// set the output filename
		data->SetOutputFile(pdfFileName, hasFileName);
		// set the UI options
		data->SetOutputUIOptions(options);

		// process the command
		status = CmdUtils::ProcessCommand(bookExportCmd);

		ASSERT(status == kSuccess);

	} while (false);
	return status;
}


//******Method To Open the DocSelectDialog
void ExportFileHelper::DoDocSelectDialog()
{
	InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	ASSERT(application);
	if (application == nil) {	
		return;
	}
	InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
	ASSERT(dialogMgr);
	if (dialogMgr == nil) {
		return;
	}
	
	PMLocaleId nLocale = LocaleSetting::GetLocale();
	RsrcSpec dialogSpec
	(
		nLocale,					// Locale index from PMLocaleIDs.h. 
		kCPDFPluginID,			// Our Plug-in ID  
		kViewRsrcType,				// This is the kViewRsrcType.
		kSDKDefDialog1ResourceID,	// Resource ID for our dialog.
		kTrue						// Initially visible.
	);

	// CreateNewDialog takes the dialogSpec created above, and also
	// the type of dialog being created (kMovableModal).
	IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
	if (dialog == nil) {
		return;
	}	
	dialog->Open(); 
	dialog->WaitForDialog();	
}