#include "VCPlugInHeaders.h"
#include "IPMStream.h"
#include "CPMUnknown.h"
#include "ISlugData.h"
#include "DSFID.h"
#include "CAlert.h"
#include "SlugStructure.h"

#define CA(X) CAlert::InformationAlert(X)

class IPMStream;

typedef vector<SlugStruct> SlugList;

class SlugData : public CPMUnknown<ISlugData>
{
	public:
		SlugData(IPMUnknown*  boss);
		virtual ~SlugData();
		void SetList(SlugList&, PMString&, PMString&);
		const bool16 GetList(SlugList&, PMString&, PMString&);
		void ReadWrite(IPMStream*  stream, ImplementationID implementation);
	private:
		int32 numElements;
		int32* elementId;
		int32* typeId;
		int32* parentId;
		int16* whichTab;
		int32* reserved1;
		int32* reserved2;
		PMString elementName;
		PMString colName;
		SlugList dataList;

};


CREATE_PERSIST_PMINTERFACE(SlugData, kSlugImpl)

SlugData::SlugData(IPMUnknown* boss) : CPMUnknown<ISlugData>(boss)
{
	//CA("In SlugData const");
	elementId=nil;
	typeId=nil;
	parentId=nil;
	whichTab=nil;
	reserved1=nil;
	reserved2=nil;
	numElements=11111;
}

SlugData::~SlugData()
{
	//CA("In SlugData ~const");
	if(elementId)
		delete [] elementId;
	if(typeId)
		delete [] typeId;
	if(whichTab)
		delete [] whichTab;
	if(reserved1)
		delete [] reserved1;
	if(reserved2)
		delete [] reserved2;
}

const bool16 SlugData::GetList(SlugList& theList, PMString& nameList, PMString& colList)
{
	//CA("In GetList");
	nameList=elementName;
	colList=colName;
	theList=this->dataList;
	return kTrue;
}

void SlugData::SetList(SlugList& theList, PMString& nameList, PMString& colList)
{
	//CA("In SetList");
	this->elementName=nameList;
	this->dataList=theList;
	this->colName=colList;
	Dirty();
	//CA("In Out SetList");
}

void SlugData::ReadWrite(IPMStream*  s, ImplementationID implementation)
{

	//CA("ReadWrite");

	SlugList::iterator it;
	
	if(s->IsReading())
	{
		s->XferInt32(numElements);
		
		elementId =new int32[numElements];
		typeId	=new int32[numElements];
		parentId=new int32[numElements];
		whichTab=new int16[numElements];
		reserved1=new int32[numElements];
		reserved2=new int32[numElements];
		
		s->XferInt32(elementId, numElements);
		s->XferInt32(typeId, numElements);
		s->XferInt32(parentId, numElements);
		s->XferInt16(whichTab, numElements);
		s->XferInt32(reserved1, numElements);
		s->XferInt32(reserved2, numElements);
		elementName.ReadWrite(s);
		colName.ReadWrite(s);

		for(int32 i=0; i<numElements; i++)
		{
			SlugStruct temp;
			temp.elementId=elementId[i];
			temp.parentId=parentId[i];
			temp.reserved1=reserved1[i];
			temp.reserved2=reserved2[i];
			temp.typeId=typeId[i];
			temp.whichTab=whichTab[i];
			dataList.push_back(temp);
		}
	}
	else
	{
		numElements=dataList.size();
		s->XferInt32(numElements);
		int32 i=0;

		elementId=new int32[numElements];
		typeId	=new int32[numElements];
		parentId=new int32[numElements];
		whichTab=new int16[numElements];
		reserved1=new int32[numElements];
		reserved2=new int32[numElements];

		for(it=dataList.begin(); it!=dataList.end(); it++)
		{
			elementId[i]=it->elementId;
			typeId[i]=it->typeId;
			parentId[i]=it->parentId;
			whichTab[i]=it->whichTab;
			reserved1[i]=it->reserved1;
			reserved2[i]=it->reserved2;
			i++;
		}
		s->XferInt32(elementId, numElements);
		s->XferInt32(typeId, numElements);
		s->XferInt32(parentId, numElements);
		s->XferInt16(whichTab, numElements);
		s->XferInt32(reserved1, numElements);
		s->XferInt32(reserved2, numElements);
		elementName.ReadWrite(s);
		colName.ReadWrite(s);
	}
}