#include "VCPlugInHeaders.h"


#include "ITextMiscellanySuite.h"

#include "CmdUtils.h"
#include "UIDList.h"
#include "SelectionASBTemplates.tpp"

class TextMiscellanySuiteASB : public CPMUnknown<ITextMiscellanySuite>
{
	public:
		TextMiscellanySuiteASB(IPMUnknown *iBoss);
	
	/** Destructor. */
	virtual ~TextMiscellanySuiteASB(void);

	virtual bool16 GetCurrentSpecifier(ISpecifier *&);
	virtual bool16 GetUidList(UIDList &);
};
CREATE_PMINTERFACE(TextMiscellanySuiteASB, kTextMiscellanySuiteASBImpl1)

TextMiscellanySuiteASB::TextMiscellanySuiteASB(IPMUnknown* iBoss) :
CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}
TextMiscellanySuiteASB::~TextMiscellanySuiteASB(void)
{
}

//#pragma mark-
bool16 TextMiscellanySuiteASB::GetCurrentSpecifier(ISpecifier * & Spec)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetCurrentSpecifier,Spec), this));
}
bool16 TextMiscellanySuiteASB::GetUidList(UIDList & TempUIDList)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetUidList,TempUIDList), this));
}
