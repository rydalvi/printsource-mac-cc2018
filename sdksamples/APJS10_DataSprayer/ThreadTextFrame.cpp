
#include "VCPluginHeaders.h"


#include "ITextModel.h"
#include "IHierarchy.h"
#include "IDocument.h"


//overset.
#include "IFrameList.h"

//createandthreadtextframe function includes
#include "TransformUtils.h"
#include "IGeometry.h"
#include "ITextColumnSizer.h"
#include "IGraphicFrameData.h"
#include "ITextParcelList.h"
#include "SDKLayoutHelper.h"
#include "IItemLockData.h"

//#include "LayoutUIUtils.h" //Cs3
#include "ILayoutUIUtils.h"  //Cs4
#include "IMargins.h"
#include "IFrameListComposer.h"
#include "IFrameUtils.h"
#include "ISpread.h"
#include "ThreadTextFrame.h"
#include "ITextFrameColumn.h"
#include "IMultiColumnTextFrame.h"
#include "IAppFramework.h"
#include "ITransformFacade.h"
#include "IPageList.h"

//#include "CmdUtils.h"


#include "CAlert.h"

#define CA(x) CAlert::InformationAlert(x)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}


UID ThreadTextFrame :: GetTextContentUID(const UIDRef& graphicFrameUIDRef)
{
	UID result = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameData(graphicFrameUIDRef, UseDefaultIID());
	if (graphicFrameData) {
		result = graphicFrameData->GetTextContentUID();
	}
	return result;
}


bool16 ThreadTextFrame :: CanThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID)
{
	bool16 result = kFalse;
	do {
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;

		UID fromMultiColumnItemUID = GetTextContentUID(UIDRef(database, fromGraphicFrameUID));
		if (fromMultiColumnItemUID == kInvalidUID) {
			break;
		}

		// Check if the story underlying fromGraphicFrameUID is locked.
		InterfacePtr<ITextColumnSizer> fromTextColumnSizer(database, fromMultiColumnItemUID, UseDefaultIID());
		ASSERT(fromTextColumnSizer != NULL);
		if (fromTextColumnSizer == NULL) {
			break;
		}
		
		//InterfacePtr<ITextFrame> fromTextFrame(fromTextColumnSizer, UseDefaultIID());
		InterfacePtr<IMultiColumnTextFrame> fromMCF(fromTextColumnSizer, UseDefaultIID());
		ASSERT(fromMCF != nil);
		if (fromMCF == nil) {
			break;
		}
		InterfacePtr<ITextModel> fromTextModel(fromMCF->QueryTextModel());
		ASSERT(fromTextModel != nil);
		if (fromTextModel == nil) {
			break;
		}
		InterfacePtr<IItemLockData> fromTextLockData(fromTextModel, UseDefaultIID());
		ASSERT(fromTextLockData != NULL);
		if (fromTextLockData == NULL) {
			break;
		}
		if (fromTextLockData) {
			if (fromTextLockData->GetInsertLock() == kTrue || fromTextLockData->GetAttributeLock() == kTrue) {
				// A lock exists so don't link.
				break;
			}
		}

		// Check if the story underlying toGraphicFrameUID is locked.
		UID toMultiColumnItemUID = GetTextContentUID(UIDRef(database, toGraphicFrameUID));
		if (toMultiColumnItemUID == kInvalidUID) {
			break;
		}
		InterfacePtr<ITextColumnSizer> toTextColumnSizer(database, toMultiColumnItemUID, UseDefaultIID());
		ASSERT(toTextColumnSizer != NULL);
		if (toTextColumnSizer == NULL) {
			break;
		}
		/*InterfacePtr<ITextFrame> toTextFrame(toTextColumnSizer, UseDefaultIID());
		ASSERT(toTextFrame != NULL);
		if (toTextFrame == NULL) {
			break;
		}*/

		InterfacePtr<IMultiColumnTextFrame> toTextFrame(toTextColumnSizer, UseDefaultIID());
		ASSERT(toTextFrame != NULL);
		if (toTextFrame == NULL) {
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: CanThreadTextFrames:IMultiColumnTextFrame == nil");
			break;
		}

		InterfacePtr<ITextModel> toTextModel(toTextFrame->QueryTextModel());
		ASSERT(toTextModel != NULL);
		if (toTextModel == NULL) {
			break;
		}
		InterfacePtr<IItemLockData> toTextLockData(toTextModel, UseDefaultIID());
		ASSERT(toTextLockData != NULL);
		if (toTextLockData == NULL) {
			break;
		}
		if (toTextLockData) {
			if (toTextLockData->GetInsertLock() == kTrue || toTextLockData->GetAttributeLock() == kTrue) {
				// A lock exists so don't link.
				break;
			}
		}

		// If we get here there are no text locks on the stories involved in the link operation.

		// If the story underlying toGraphicFrameUID is empty we can link the frame
		// anywhere in the frame list.
		if (toTextModel->TotalLength() == 1) {
			result = kTrue;
			break;
		}

		// The story underlying toGraphicFrameUID is not empty.

		// Check we are appending toGraphicFrameUID onto the end of the frame list underlying fromGraphicFrameUID.
		InterfacePtr<IFrameList> fromFrameList(fromMCF->QueryFrameList());
		ASSERT(fromFrameList);
		if (!fromFrameList) {
			break;
		}
		InterfacePtr<IFrameList> toFrameList(toTextFrame->QueryFrameList());
		ASSERT(toFrameList);
		if (!toFrameList) {
			break;
		}

		//if (fromTextColumnSizer->GetLastFrameIndex() != fromFrameList->GetFrameCount() - 1) //Cs3
		if (!toTextFrame->GetIsLastMCF())  //Cs4
		{
			// Frame being linked from is not the last one in its frame list.
			break;
		}
		//if (toTextColumnSizer->GetFirstFrameIndex() != 0)  //Cs3
		if(!toTextFrame->GetIsFirstMCF() )  //Cs4
		{
			// Frame being linked to is not the first one in its frame list.
			break;
		}

		// If we get here we are linking the last frame in the frame list underlying fromGraphicFrameUID
		// onto the first frame in the frame list underlying fromGraphicFrameUID.
		// We have an end to start link.
		result = kTrue;

	} while(false);
	return result;
}

void ThreadTextFrame :: DoCreateAndThreadTextFrame(UIDRef& fromGraphicFrameUIDRef)
{
	//CA("Inside ThreadTextFrame :: DoCreateAndThreadTextFrame");
	while(kTrue)
	{
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
		
		/*InterfacePtr<IPMUnknown> unknown(fromGraphicFrameUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(fromGraphicFrameUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		if (textFrameUID == kInvalidUID)
		{
			//CA("textFrameUID == kInvalidUID");
			break;
		}
	
		//InterfacePtr<ITextFrame> textFrame(fromGraphicFrameUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		//if (textFrame == NULL)
		//{
		//	//CA("textFrame == NULL");
		//	break;
		//}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(fromGraphicFrameUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: DoCreateAndThreadTextFrame:graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: DoCreateAndThreadTextFrame:multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: DoCreateAndThreadTextFrame:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: DoCreateAndThreadTextFrame:frameItemHierarchy == nil");
			continue;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_DataSprayer::ThreadTextFrame :: DoCreateAndThreadTextFrame:textFrame == nil");
			//CA("!!!ITextFrameColumn");
			continue;
		}

		//bool16 result = 
		//CA_NUM("result :",result);
		//break;

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}

		bool16 result = this->IsTextFrameOverset(textFrame);//IsStoryOverset(textModel);
		if(result)
		{
			//CA("Adding New Page");
			//::AddNewPage();		//Cs3
			Utils<ILayoutUIUtils>()->AddNewPage();	//Cs4
			fromGraphicFrameUIDRef = CreateAndThreadTextFrame(fromGraphicFrameUIDRef);
			
		}
		else 
			break;
	}//end while loop
}

UIDRef ThreadTextFrame :: CreateAndThreadTextFrame(const UIDRef& fromGraphicFrameUIDRef)
{
					
	UIDRef result = UIDRef::gNull;
	// Wrap the commands in a sequence.
	//CmdUtils::SequencePtr seq(kFailure);// CS3 change
	CmdUtils::SequencePtr seq;
	
	do {
		// Create a new text frame.

		UIDRef pageUIDRef;
		UIDRef spreadUIDRef;
		getCurrentPage(pageUIDRef, spreadUIDRef);

		
		PMRect marginBoxBounds;
		getMarginBounds(pageUIDRef,marginBoxBounds);
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == NULL)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		PMRect pagebounds = pageGeometry->GetStrokeBoundingBox();


		// Determine the bounds of the new text frame.
		// Make it the same size as the given frame, positioned alongside at a 10 point offset.
		InterfacePtr<IGeometry> geometry(fromGraphicFrameUIDRef, UseDefaultIID());
		ASSERT(geometry);
		if (!geometry)
		{
			break;
		}
		PMMatrix inner2parent = ::InnerToParentMatrix(geometry);
		PMRect boundsInParentCoords = geometry->GetStrokeBoundingBox(inner2parent);
		
		//boundsInParentCoords.MoveRel(boundsInParentCoords.Width()+ 10.0, 0.0);

		// Parent the new text frame on the same spread layer as the given text frame.
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()/*::QueryFrontLayoutData()*/); //CS4
		if (layoutControlData == NULL)
			break;
		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());		
		if (activeSpreadLayerHierarchy == NULL) 
		{
			break;
		}
		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);
		
		/*InterfacePtr<IHierarchy> hierarchy(fromGraphicFrameUIDRef, UseDefaultIID());
		ASSERT(hierarchy);
		if (!hierarchy) {
			break;
		}
		UIDRef parentUIDRef = UIDRef(fromGraphicFrameUIDRef.GetDataBase(), hierarchy->GetLayerUID());*/
		

		// Get the number of columns in the given text frame.
		InterfacePtr<ITextColumnSizer> frameColumnSizer(fromGraphicFrameUIDRef.GetDataBase(), GetTextContentUID(fromGraphicFrameUIDRef), UseDefaultIID());
		ASSERT(frameColumnSizer);
		if (!frameColumnSizer) {
			break;
		}
		int32 numberOfColumns = frameColumnSizer->GetNumberOfColumns();

		// Determine if the given text frame is vertical.
		InterfacePtr<IMultiColumnTextFrame> mcf(frameColumnSizer, UseDefaultIID());
		if (!mcf) {
			break;
		}
		InterfacePtr<IFrameList> frameList(mcf->QueryFrameList());
		if (!frameList) {
			break;
		}
		InterfacePtr<ITextParcelList> textParcelList(frameList,UseDefaultIID());
		if (!textParcelList) {
			break;
		}
		bool16 isVertical = textParcelList->GetIsVertical();

		// Create the new frame based on the properties of the given text frame determined above.
		SDKLayoutHelper layoutHelper;
		UIDRef toGraphicFrameUIDRef = layoutHelper.CreateTextFrame(parentUIDRef, boundsInParentCoords, numberOfColumns, isVertical);
		if (toGraphicFrameUIDRef == UIDRef::gNull) 
		{
			break;
		}

		InterfacePtr<ITransform> transform(toGraphicFrameUIDRef,UseDefaultIID());
		if(transform == NULL)
		{
			CA("transform == NULL");
			break;
		}
		/*CA_NUM("Pageleft :",pagebounds.Left());
		CA_NUM("Pagetop :",pagebounds.Top());
		CA_NUM("Pageleft :",pagebounds.Right());
		CA_NUM("Pageleft :",pagebounds.Bottom());*/
		//marginBoxBounds.Left(),marginBoxBounds.Top()
		//transform->MoveTo(PMPoint(-576,-300));

		//MovePageItemAbsolute(transform,PBPMPoint(marginBoxBounds.Left(),marginBoxBounds.Top()));
		UIDList moveUIDList(toGraphicFrameUIDRef);
		ErrorCode errorCode;
		Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
		//PBPMPoint referencePoint(PMPoint(0,0));
		PBPMPoint referencePoint(PMPoint(pagebounds.Left(),pagebounds.Top()));
		
		//CA_NUM("Left : ", (marginBoxBounds.Left()- boundsInParentCoords.Left() ));
		//CA_NUM("top :",marginBoxBounds.Top());
		errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateTo((marginBoxBounds.Left()- boundsInParentCoords.Left())/2 ,(marginBoxBounds.Top())+(marginBoxBounds.Bottom() - marginBoxBounds.Top())/2));
		

		// Link the out-port of the given text frame to the in-port of the new text frame.
		if (CanThreadTextFrames(fromGraphicFrameUIDRef.GetDataBase(), fromGraphicFrameUIDRef.GetUID(), toGraphicFrameUIDRef.GetUID()) == kFalse) {
			break;
		}
		if (ThreadTextFrames(fromGraphicFrameUIDRef.GetDataBase(), fromGraphicFrameUIDRef.GetUID(), toGraphicFrameUIDRef.GetUID()) != kSuccess) {
			break;
		}

		// If we get here we have successfully created a new text frame and linked it to the given text frame.
		//seq.SetState(kSuccess);// CS3 change
		result = toGraphicFrameUIDRef;
		//RecomposeTextFrame(textFrame);

	} while(false);

	//CA("returning ");
	return result;
}


ErrorCode ThreadTextFrame :: ThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID)
{
	ErrorCode status = kFailure;
	//CmdUtils::SequencePtr seq(status);// CS3 change
	CmdUtils::SequencePtr seq;

	do {
		UID fromMultiColumnItemUID = GetTextContentUID(UIDRef(database, fromGraphicFrameUID));
		if (fromMultiColumnItemUID== kInvalidUID) {
			break;
		}
		InterfacePtr<ITextColumnSizer> fromTextColumnSizer(database, fromMultiColumnItemUID, UseDefaultIID());
		ASSERT(fromTextColumnSizer != NULL);
		if (fromTextColumnSizer == NULL) {
			break;
		}

		//InterfacePtr<ITextFrame> fromTextFrame(fromTextColumnSizer, UseDefaultIID());
		InterfacePtr<IMultiColumnTextFrame> fromTextFrame(fromTextColumnSizer, UseDefaultIID());
		ASSERT(fromTextFrame != NULL);
		if (fromTextFrame == NULL) {
			break;
		}
		InterfacePtr<ITextModel> fromTextModel(fromTextFrame->QueryTextModel());
		ASSERT(fromTextModel != NULL);
		if (fromTextModel == NULL) {
			break;
		}

		UID toMultiColumnItemUID = GetTextContentUID(UIDRef(database, toGraphicFrameUID));
		if (toMultiColumnItemUID== kInvalidUID) {
			break;
		}
		InterfacePtr<ITextColumnSizer> toTextColumnSizer(database, toMultiColumnItemUID, UseDefaultIID());
		ASSERT(toTextColumnSizer != NULL);
		if (toTextColumnSizer == NULL) {
			break;
		}
		//InterfacePtr<ITextFrame> toTextFrame(toTextColumnSizer, UseDefaultIID());
		InterfacePtr<IMultiColumnTextFrame> toTextFrame(toTextColumnSizer, UseDefaultIID());
		ASSERT(toTextFrame != NULL);
		if (toTextFrame == NULL) {
			CA("toTextFrame != NULL");
			break;
		}
		InterfacePtr<ITextModel> toTextModel(toTextFrame->QueryTextModel());
		ASSERT(toTextModel != NULL);
		if (toTextModel == NULL) {
			break;
		}

		// If there is text content in the story underlying the text frame
		// given by toGraphicFrameUID, append this text to the end of the story
		// underlying the text frame given by parameter fromGraphicFrameUID.
		if (toTextModel->TotalLength() > 1) {
			Utils<ITextUtils> textUtils;
			ASSERT(textUtils);
			if (!textUtils) {
				break;
			}
			InterfacePtr<ICommand> moveStoryRangeCmd(textUtils->QueryMoveStoryFromAllToEndCommand(::GetUIDRef(toTextModel), ::GetUIDRef(fromTextModel)));
			ASSERT(moveStoryRangeCmd);
			if (!moveStoryRangeCmd) {
				break;
			}
			status = CmdUtils::ProcessCommand(moveStoryRangeCmd);
			ASSERT_MSG(status == kSuccess, "ITextUtils::QueryMoveStoryFromAllToEndCommand failed");
			if (status != kSuccess) {
				break;
			}
			if (toTextModel->TotalLength() > 1) {
				ASSERT_FAIL("toTextModel->TotalLength() should be 1, the content should have been merged");
			}
		}

		// Link the text flow across kMultiColumnItemBoss objects.
		// Requires an item list containing pairs of UID that refer to the
		// objects to be linked together in from(out-port)/to(in-port) order.
	    InterfacePtr<ICommand> textLinkCmd(CmdUtils::CreateCommand(kTextLinkCmdBoss));
		ASSERT(textLinkCmd != NULL);
		if (textLinkCmd == NULL) {
			break;
		}
		UIDList itemList(database);
		itemList.Append(fromMultiColumnItemUID);
		itemList.Append(toMultiColumnItemUID);
		// Note that the story underlying the object referenced by toMultiColumnItemUID
		// must be empty. You need to move any text content it has before linking the
		// frames as illustrated by the code above.
		textLinkCmd->SetItemList(itemList);
		status = CmdUtils::ProcessCommand(textLinkCmd);
		ASSERT_MSG(status == kSuccess, "kTextLinkCmdBoss failed");

	} while (false);
	//seq.SetState(status);// CS3 change
	return status;
}

bool16 ThreadTextFrame :: getCurrentPage(UIDRef& pageUIDRef, UIDRef& spreadUIDRef)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()/*::QueryFrontLayoutData()*/); //Cs4
		if (layoutData == NULL)
			break;

		IDocument* document = layoutData->GetDocument();
		if (document == NULL)
			break;

		IDataBase* database = ::GetDataBase(document);
		if(!database)
			break;
		
		/*UID pageUID = layoutData->GetPage();
		if(pageUID == kInvalidUID)
			break;*/
		
		//int32 CurrentPageIndex= pageUidList.size();
		//if(CurrentPageIndex== 0 )  //CurrentPageIndex== PageCount)
		//{
			

			//IGeometry* spreadGeomPtr = layoutData->GetSpread();//CS3 Change
			UIDRef spreadUidRef = layoutData->GetSpreadRef(); 
			if(spreadUidRef == NULL)
				break;

			//InterfacePtr<ISpread> iSpread(spreadGeomPtr, UseDefaultIID());
			InterfacePtr<ISpread> iSpread(spreadUidRef, UseDefaultIID());
			if (iSpread == NULL)
				return kFalse;

			int numPages=iSpread->GetNumPages();

			UID pageUID= iSpread->GetNthPageUID(numPages-1);

			UIDRef pageRef(database, pageUID);
			pageUIDRef = pageRef;

			//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
			InterfacePtr<IHierarchy> hierarchyPtr(spreadUidRef, IID_IHIERARCHY);
			if(hierarchyPtr == NULL)
				break;

			UID spreadUID = hierarchyPtr->GetSpreadUID();
			UIDRef spreadRef(database, spreadUID);
			spreadUIDRef = spreadRef;
			result = kTrue;		
			//break;
		//}
		//else if(CurrentPageIndex!=PageCount)
		//{
			//if(pageUidList[CurrentPageIndex-1]!= pageUID)
			//{
				//UIDRef pageRef(database, pageUID);
				//pageUIDRef = pageRef;
				//IGeometry* spreadGeomPtr = layoutData->GetSpread();
				//if(spreadGeomPtr == NULL)
					//break;

				//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				//if(hierarchyPtr == NULL)
					//break;

				//UID spreadUID = hierarchyPtr->GetSpreadUID();
				//UIDRef spreadRef(database, spreadUID);
				//spreadUIDRef = spreadRef;
				//result = kTrue;				
				//break;		
			//}
			//else
			//{
			//	IGeometry* spreadItem = layoutData->GetSpread();
			//	if(spreadItem == NULL)
				//	return kFalse;

				//InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
				//if (iSpread == NULL)
					//return kFalse;

				//int numPages=iSpread->GetNumPages();
				//int OldPageIndex= iSpread->GetPageIndex(pageUID);
				//if(numPages >= OldPageIndex+1 )
				// pageUID= iSpread->GetNthPageUID(OldPageIndex+1);

				//UIDRef pageRef(database, pageUID);
				//pageUIDRef = pageRef;

				//IGeometry* spreadGeomPtr = layoutData->GetSpread();
				//if(spreadGeomPtr == NULL)
				//	break;

				//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				//if(hierarchyPtr == NULL)
				//	break;

				//UID spreadUID = hierarchyPtr->GetSpreadUID();
				//UIDRef spreadRef(database, spreadUID);
				//spreadUIDRef = spreadRef;
				//result = kTrue;				
				//break;		
			//}
		//}

	}
	while(kFalse);
	return result;

}

bool16 ThreadTextFrame::getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return result;
		}

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
			break;

		IDocument* doc = layoutData->GetDocument();
		if (doc == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
			break;
		}
		InterfacePtr<IPageList> pageList(doc, UseDefaultIID());
		if (pageList == nil)
		{
			ASSERT_FAIL("pageList is invalid");
			break;
		}
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		PMRect pageBounds = pageGeometry->GetStrokeBoundingBox();

		PMReal leftMargin =0.0,topMargin =0.0,rightMargin = 0.0,bottomMargin =0.0;
		// ... and the page's margins.
		/*InterfacePtr<IMargins> pageMargins(pageGeometry, IID_IMARGINS);
		if (pageMargins == nil)
		{
			ASSERT_FAIL("pageMargins is invalid");
			break;
		}*/
		
		/*pageMargins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);*/

		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}
		PMRect marginBBox;
		InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
		// Note it's OK if the page does not have margins.
		if (margins) {
			margins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);
		}

		PageType pageType = pageList->GetPageType(pageUIDRef.GetUID()) ;

		PMPoint leftTop;
		PMPoint rightBottom;

		if(pageType == kLeftPage)
		{
			//CA("pageType == kLeftPage");
			leftTop.X(pageBounds.Left()+rightMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - leftMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
	
		}
		else if(pageType == kRightPage)
		{
			//CA("pageType == kRightPage");
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
		}
		else if(pageType == kUnisexPage)
		{
			//CA("pageType == kUnisexPage");
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);

		}

		// Place the item into a frame the size of the page margins
		// with origin at the top left margin. Note that the frame
		// is automatically resized to fit the content if the 
		// content is a graphic. Convert the points into the
		// pasteboard co-ordinate space.
		/*CAI("leftMargin", leftMargin);
		CAI("topMargin", topMargin );
		CAI("rightMargin", rightMargin );
		CAI("bottomMargin", bottomMargin );*/
		/*PMPoint leftTop(pageBounds.Left()+leftMargin, pageBounds.Top()+topMargin);
		PMPoint rightBottom(pageBounds.Right() - rightMargin, pageBounds.Bottom() - bottomMargin);*/

		/*CAI("PageleftMargin", pageBounds.Left()+leftMargin);
		CAI("PagetopMargin", pageBounds.Top()+topMargin );
		CAI("PagerightMargin", pageBounds.Right() - rightMargin );
		CAI("PagebottomMargin", pageBounds.Bottom() - bottomMargin );*/
		
		///****Commented By Sachin sharma
		/*::InnerToPasteboard(pageGeometry,&leftTop);
		::InnerToPasteboard(pageGeometry,&rightBottom);*/
	
		//****ADded
		::TransformInnerPointToPasteboard(pageGeometry,&leftTop);
		::TransformInnerPointToPasteboard(pageGeometry,&rightBottom);
		
		marginBoxBounds.Left() = leftTop.X();
		marginBoxBounds.Top() = leftTop.Y();
		marginBoxBounds.Right() = rightBottom.X();
		marginBoxBounds.Bottom() = rightBottom.Y();

		result = kTrue;
	}
	while(kFalse);

	return result;
}


//bool16 ThreadTextFrame :: RecomposeTextFrame(const InterfacePtr<ITextFrame> textFrame)
bool16 ThreadTextFrame :: RecomposeTextFrame(const InterfacePtr<ITextFrameColumn> textFrame)
{
	bool16 result = kFalse;
	do
	{

		InterfacePtr<IFrameList> frameList(textFrame->QueryFrameList());
		if (frameList == NULL) 
		{
			CA("invalid frameList");
			break;
		}
		int32 frameCount = frameList->GetFrameCount();

		for(int32 frameIndex = 0;frameIndex < frameCount;frameIndex++)
		{
			InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
			if (frameListComposer == NULL) {
				ASSERT_FAIL("frameListComposer invalid");
				break;
			}
			frameListComposer->RecomposeThru/*RecomposeThruNthFrame*/(frameIndex);	//-----CS5---
		}
		result = kTrue;
	}while(kFalse);

	return result;


}


//bool16 ThreadTextFrame :: IsTextFrameOverset(const InterfacePtr<ITextFrame> textFrame)
bool16 ThreadTextFrame :: IsTextFrameOverset(const InterfacePtr<ITextFrameColumn> textFrameColumn)
{
	bool16 overset = kFalse;
	do {		
		// Check for damage & recompose if necessary
		InterfacePtr<IFrameList> frameList(textFrameColumn->QueryFrameList());
		if (frameList == nil) {
			ASSERT_FAIL("invalid frameList");
			break;
		}		
		int32 firstDamagedFrameIndex = frameList->GetFirstDamagedFrameIndex();
		UID frameUID = ::GetUID(textFrameColumn);
		int32 frameIndex = frameList->GetFrameIndex(frameUID);
		if (frameIndex < 0) {
			ASSERT_FAIL("invalid frameIndex");
			break;
		}
		if (firstDamagedFrameIndex <= frameIndex) {
			// Frame not fully composed, force recomposition.
			InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
			if (frameListComposer == nil) {
				ASSERT_FAIL("frameListComposer invalid");
				break;
			}
			frameListComposer->RecomposeThru/*RecomposeThruNthFrame*/(frameIndex);	//----CS5--
		}

		// Find the TextIndex of the final character in the story.
		InterfacePtr<ITextModel> textModel(textFrameColumn->QueryTextModel());
		if (textModel == nil) {
			ASSERT_FAIL("invalid textModel");
			break;
		}
		TextIndex storyFinalTextIndex = textModel->GetPrimaryStoryThreadSpan() - 1;
		
		// Find the TextIndex of the final character in the frame.
		TextIndex frameFinalTextIndex = 
			textFrameColumn->TextStart() + textFrameColumn->TextSpan() - 1;

		if (frameFinalTextIndex == storyFinalTextIndex) {
			// Frame displays the final character in the story.
			break;
		}

		if (frameFinalTextIndex == storyFinalTextIndex - 1) {
			// Frame does not display the terminating kTextChar_CR.
			// But don't consider this condition as overset.
			break;
		}

		// If we drop through to this point in the code
		// the frame is overset.
		overset = kTrue;

	} while (false); // only do once
	return overset;
}


