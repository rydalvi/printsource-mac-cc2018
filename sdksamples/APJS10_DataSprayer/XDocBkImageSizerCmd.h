#include "VCPlugInHeaders.h"

// Interface includes:
#include "IGeometry.h"
#include "IHierarchy.h"
#include "IInlineData.h"
#include "IRefPointUtils.h"
#include "IStoryList.h"
#include "ITextModel.h"
#include "ITextParcelList.h"
//#include "ITransformCmdUtils.h"

// General includes:
#include "CmdUtils.h"
#include "Command.h"
#include "ErrorUtils.h"
#include "K2Vector.tpp"
#include "OwnedItemDataList.h"

// Project includes
//#include "XDocBkID.h"
#include "DSFID.h"



class XDocBkImageSizerCmd : public Command
{
public:
	/** Constructor.
		@param boss IN interface ptr from boss object on which this interface is aggregated.*/
	XDocBkImageSizerCmd(IPMUnknown* boss);

public:
	/** @see Command::Do
	 */
	virtual void Do();



	/** @see Command::CreateName
	 */
	virtual PMString* CreateName();

	/**	Find all the in-lines in given story
		@param textModelUIDRef IN text model containing the inlines of interest
		@param outList OUT parameter holding the inline boss objects
	 */
	void CollectInlinesInStory(const UIDRef& textModelUIDRef, K2Vector<UIDRef>& outList);

	/**	Resize an inline in given text model (maybe, if it needs to be scaled to fit in column)
		@param textModelUIDRef IN text model in which inline is to be scaled to fit
		@param inlineBossUIDRef IN boss object of type kInlineBoss
	 */
	void ResizeInlineGraphicToFit(const UIDRef& textModelUIDRef, const UIDRef& inlineBossUIDRef);

	/**	Get the width of the column into which this inline is being placed
		@param textModelUIDRef IN text model of interest
		@param uidRef IN inline thing, we're going to query for IInlineData on this to find out where in the story the inline is
		@return PMReal composed width of column
	 */
	PMReal CalcColumnParcelWidth(const UIDRef& textModelUIDRef, const UIDRef& uidRef) const;

	/**	Method to calculate width to scale inline to if required
		@param uidRef IN the image whose width we'll get from its IGeometry
		@return PMReal the current width before scaling this image
	 */
	PMReal CalcInlineGraphicWidth(const UIDRef& uidRef) const;

};
