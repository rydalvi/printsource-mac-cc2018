#include "VCPluginHeaders.h"
#include "TableUtility.h"
#include "IFrameUtils.h"
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ITableModel.h"
#include "IAppFramework.h"
#include "PublicationNode.h"
#include "ITableCommands.h"
#include "ITableSuite.h"
#include "ISpecialChar.h"
#include "ITool.h"
#include "CAlert.h"
#include "IToolCmdData.h"
#include "IConcreteSelection.h"
#include "ITableTextSelection.h"
#include "ITableModel.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "TextEditorID.h"
#include "ITableSelectionSuite.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "IGeometry.h"
#include "ISelectionUtils.h"
#include "SlugStructure.h"
#include "ITextAttributeSuite.h"
#include "ITextSelectionSuite.h"
#include "ITableFrame.h"
#include "ITableLayout.h"
#include "IPasteboardUtils.h"
#include "TransformUtils.h"
#include "ITableSelectionSuite.h"
#include "IFrameContentSuite.h"
#include "textiterator.h"
#include "CTUnicodeTranslator.h"
#include "ILayoutSelectionSuite.h"
#include "IFrameList.h"
#include "IFrameContentUtils.h"
#include "ITblBscSuite.h"
#include "ITableTextContent.h"
#include "IToolCmdData.h"
#include "IToolManager.h"
#include "IToolBoxUtils.h"
#include "IApplication.h"
#include "ITagReader.h"
#include "ITableSuite.h"
#include "ITextStoryThreadDictHier.h"
#include "ITextStoryThreadDict.h"
#include "IRangeData.h"
#include "IUIDData.h"
#include "ITableUtils.h"
#include "TableStyleUtils.h"
#include "IDataSprayer.h"
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "IMultiColumnTextFrame.h"

#include "ICellContent.h"
#include "ICellContentMgr.h"
#include "ITableGeometry.h"
#include "CommonFunctions.h"
#include "ITextStoryThreadDict.h"
#include "ITextStoryThread.h"
//#include "MedCustomTableScreenValue.h"
#include "ThreadTextFrame.h"

#include <vector>
#include "IDCSInfo.h"
#include "IInlineData.h"
#include "IShape.h"
#include "IStandOffItemData.h"
#include "IPathGeometry.h"
#include "FileUtils.h"
#include "IDocument.h"
#include "IItemStrand.h"
#include "SDKLayoutHelper.h"

#include "ITextModelCmds.h"
#include "IConcreteSelection.h"
#include "ITableTextSelection.h"
#include "ITextTarget.h"
#include "IScrapSuite.h"
#include "IClipboardController.h"
#include "ITransformFacade.h"
#include "TableSourceInfoValue.h"
#include "TSTableSourceHelper.h"
#include "ILayoutUIUtils.h"

#include "ITableCopyPasteCmdData.h"
#include "ITextStoryThread.h"

#include <map>
#include "ToolRecord.h"
#include "OMTypes.h"
#include "IXMLUtils.h"
#include "IXMLReferenceData.h"
#include "IXMLAttributeCommands.h"
#include "IXMLTagCommands.h"
#include "PersistUtils.h"
#include "IXMLTagList.h"




inline PMString  numToPMString(int32 num){
PMString numStr;
numStr.AppendNumber(num);
return numStr;
}

#define pNode pNode1

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("TableUtility.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + PMString("\n") + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern double CurrentSectionID;
extern double CurrentPublicationID;
extern double CurrentSubSectionID;
extern PublicationNode pNode;
extern double CurrentObjectTypeID;
bool16 AddHeaderToTable = kTrue;
extern void setTagParent(TagStruct& tStruct, double id, bool16 isTagged=kFalse);
extern int32 global_project_level;
extern vector<double> idList;
bool16 AddListNameToTable = kFalse;


bool16 IsDBTable = kFalse;

vector<double> HeadersizeVector;
extern char AlphabetArray[];
int TableFlagForStandardProductTable = 0;

UIDRef newFrameUIDRef;		//// to add graphics frame in hybrid table

extern bool16 HorizontalFlow;
extern bool16 isInlineImage;

static double attributeId = -1;
static int32 delStart = -1;
static int32 delEnd =-1;
static bool16 shouldDelete = kFalse;
static int32 deleteCount =0;
static vector<RangeData> deleteTextRanges;

CREATE_PMINTERFACE (TableUtility, kTableUtilityImpl)

ErrorCode DeleteText(ITextModel* textModel, const TextIndex position, const int32 length);

TableUtility::TableUtility(IPMUnknown* boss):CPMUnknown<ITableUtility>(boss)
{
	
}

		
bool16 TableUtility::isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef)
{
	bool16 returnValue=kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	do
	{
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::isTablePresent::!textFrameUID");	
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::isTablePresent::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::isTablePresent::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::isTablePresent::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::isTablePresent::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::isTablePresent::!textFrame");
			break;
		}
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::isTablePresent::!textModel");
			break;
		}
		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::isTablePresent::!tableList");
			break;
		}
		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			//CA("tableIndex<0");
			break;
		}

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == NULL) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::isTablePresent::!tableModel");
			break;
		}
		UIDRef tableRef(::GetUIDRef(tableModel));
		tableUIDRef=tableRef;
		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}

void TableUtility::fillDataInTable
(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{
		IsDBTable = kTrue;		//This metod is getting called only for DBTable.
		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
		if(tStruct.isAutoResize == 1)
			IsAutoResize= 1;

		if(tStruct.isAutoResize == 2)
			IsAutoResize= 2;

		if(tStruct.header == 1)
        {
			AddHeaderToTable = kTrue;
            AddListNameToTable = kFalse;
        }
		else if(tStruct.header == 2)
        {
            AddHeaderToTable = kTrue;
            AddListNameToTable = kTrue;
        }
        else if(tStruct.header == 3)
        {
            AddHeaderToTable = kFalse;
            AddListNameToTable = kTrue;
        }
        else
        {
            AddHeaderToTable = kFalse;
            AddListNameToTable = kFalse;
        }

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.

		double sectionid = -1;
		VectorScreenTableInfoPtr tableInfo = NULL;
		
		TableSourceInfoValue *tableSourceInfoValueObj;
		bool16 isCallFromTS = kFalse;
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		if(ptrTableSourceHelper != nil)
		{
			tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
			if(tableSourceInfoValueObj)
			{
				isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
				if(isCallFromTS == kTrue)
				{
					tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
					if(!tableInfo)
						return;
					sectionid = CurrentSubSectionID = CurrentSectionID;
				}				
			}
		}
		
		if(isCallFromTS == kFalse)	///This is for call from content sprayer
		{
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;

			if(pNode.getIsONEsource())
			{
				// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
			}else
			{
				// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
				//CA("Not ONEsource");
				tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, tStruct.languageID, kFalse);
			}
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_DataSprayerModel::TableUtility::fillDataInTable::!tableInfo");
				return;
			}

			if(tableInfo->size()==0){
				//CA("tableInfo->size()==0");
				break;
			}
		
		}

		//It checks the selection present or not........
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			//CA("Slection NULL");
			break;
		}

		if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			iSelectionManager->DeselectAll(nil);
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			//CA("!layoutSelectionSuite");
			return;
		}
		//It gets refernce to persistant objects present in the (database) document as long as document is open.
		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			//CA("My Suit NULL");
			//break;
		}

		PMRect theArea;
		PMReal rel;
		int32 ht;
		int32 wt;

		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
		if(iGeometry==NULL)
		{
			//CA("iGeometry==NULL");
			break;
		}
		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
		rel = theArea.Height();
		ht=::ToInt32(rel);
		rel=theArea.Width();	
		wt=::ToInt32(rel);

		int32 numRows=0;
		int32 numCols=0;

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;

		bool16 typeidFound=kFalse;

		vector<vector<PMString> > vec_tablerows;//-------
		vector<double> vec_tableheaders;
		
		if(isCallFromTS == kFalse) //This is for call from content sprayer
		{
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == tStruct.typeId)
				{   				
					typeidFound=kTrue;
					numRows =static_cast<int32>(oTableValue.getTableData().size()); //cs4
					if(numRows<=0)
					{
						//CA("numRows<=0");
						break;
					}
					numCols = static_cast<int32>(oTableValue.getTableHeader().size());
					if(numCols<=0)
					{
						//CA("numCols<=0");
						break;
					}

					break;
				}
			}
			if(tableInfo)
			{
				tableInfo->clear();
				delete tableInfo;
			}
				
		}
		else	///This is for call from TABLEsource
		{
			tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
			
			if(tableSourceInfoValueObj->getVec_Table_ID().size() > 1)
			{
				for(it = tableInfo->begin(); it != tableInfo->end() ; it++)  
				{
					oTableValue = *it;
					
					for(int32 selectedTableIndex =0; selectedTableIndex <  tableSourceInfoValueObj->getVec_TableType_ID().size(); selectedTableIndex ++ )
					{
						if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex) && tStruct.typeId == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex))
						{
							if(selectedTableIndex == 0)
							{
								vec_tableheaders = oTableValue.getTableHeader();
							}
						
							typeidFound=kTrue;
							vector<vector<PMString> > vec_Test = oTableValue.getTableData();
						
							vector<vector<PMString> >::iterator it1;
						
							vector<PMString> testVector ;
		
							for(it1=vec_Test.begin(); it1!=vec_Test.end(); it1++)
							{
								vector<PMString>::iterator it2;
								for(it2=(*it1).begin();it2!=(*it1).end();it2++)
								{
									testVector.push_back(*it2);
								}
							
								vec_tablerows.push_back(testVector);
								testVector.clear();

							}
						
							numRows = numRows + static_cast<int32>(oTableValue.getTableData().size()); 
							if(selectedTableIndex == 0)
								numCols = numCols + static_cast<int32>(oTableValue.getTableHeader().size());

							break; // break from inner for
						}
					}
				}
			}
			else
			{
				for(it = tableInfo->begin(); it != tableInfo->end() ; it++)  
				{
					oTableValue = *it;
					if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && tStruct.typeId == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0))
					{
						typeidFound=kTrue;
						numRows =static_cast<int32>(oTableValue.getTableData().size()); //cs4
						if(numRows<=0)
						{
							//CA("numRows<=0");
							break;
						}
						numCols = static_cast<int32>(oTableValue.getTableHeader().size());
						if(numCols<=0)
						{
							//CA("numCols<=0");
							break;
						}
						break;
					}
				}
				
			}

		}

		if(!typeidFound){
			//CA("!typeidFound");
			break;	
		}
		
		tStruct.tableId = oTableValue.getTableID();

		bool8 isTranspose = 0; //oTableValue.getTranspose();
		TableFlagForStandardProductTable = 1;  // Since We are spraying Product Table

		if(isTranspose)
		{
			int32 i=0, j=0;
			resizeTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht);
			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
	
			if(AddHeaderToTable){
				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
				i=1; j=0;
			}
            
            if(AddListNameToTable)
            {
				putListNameInTable(tableUIDRef, oTableValue.getName(), isTranspose, tStruct, BoxUIDRef, static_cast<int32>(vec_tableheaders.size()));
                i=i+1; j=0;
            }

			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vec_items = oTableValue.getItemIds();
			
			vector<vector<PMString> >::iterator it1;
			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if((j)>numCols)
						break;
					setTableRowColData(tableUIDRef, (*it2), j, i);	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );
					stencileInfo.elementName = dispname;
					PMString TableColName("");
                    TableColName.Append("ATTRID_");
                    TableColName.AppendNumber(PMReal(vec_tableheaders[j]));

					TableColName = keepOnlyAlphaNumeric(TableColName);

					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					stencileInfo.tableType = tStruct.tableType;
					stencileInfo.typeId = -1;


					if (AddHeaderToTable && AddListNameToTable)
						stencileInfo.childId = vec_items[i - 2];
					else if (AddHeaderToTable && !AddListNameToTable)
						stencileInfo.childId = vec_items[i - 1];
					else if (!AddHeaderToTable && AddListNameToTable)
						stencileInfo.childId = vec_items[i - 1];
					else
						stencileInfo.childId  = vec_items[i];
			
					stencileInfo.childTag = 1;
					stencileInfo.parentId = pNode.getPubId();
					stencileInfo.parentTypeId = pNode.getTypeId();
					stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
					stencileInfo.imgFlag = CurrentSubSectionID; 
					stencileInfo.sectionID = CurrentSubSectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.LanguageID = tStruct.languageID;

					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					stencileInfo.tableId = tStruct.tableId;
					stencileInfo.pbObjectId = pNode.getPBObjectID();
					XMLReference txmlref;
					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";
						//CA("PSTable Not Found : "+ TableTagNameNew);
					}

					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i,tableId );
					j++;
				}
				i++;
				j=0;
			}
		}
		else
		{
			//CA("Inside !isTranspose");
			int32 i=0, j=0;

			//resizeTable gets called if Auto Re-Size is selected..
			resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
			vector<double> vec_items;
			
			if(isCallFromTS == kFalse || (tableSourceInfoValueObj->getVec_Table_ID().size() <= 1) )
				vec_tableheaders = oTableValue.getTableHeader();

			if(AddHeaderToTable){
				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
				i=1; j=0;
			}
            
            if(AddListNameToTable)
            {
				putListNameInTable(tableUIDRef, oTableValue.getName(), isTranspose, tStruct, BoxUIDRef, static_cast<int32>(vec_tableheaders.size()));
                i=i+1; j=0;
            }
            
			vec_items = oTableValue.getItemIds();
			if( (isCallFromTS == kFalse) || (tableSourceInfoValueObj->getVec_Table_ID().size() <= 1))
			{
				vec_tablerows = oTableValue.getTableData();
			}

			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if((j)>numCols) 
					{
						break;
					}

					setTableRowColData(tableUIDRef, (*it2), i, j);
	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );
					stencileInfo.elementName = dispname;
					PMString TableColName("");

                    TableColName.Append("ATTRID_");
                    TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
					TableColName = keepOnlyAlphaNumeric(TableColName);

					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					stencileInfo.typeId = -1;

					if (AddHeaderToTable && AddListNameToTable)
						stencileInfo.childId = vec_items[i-2];
					else if (AddHeaderToTable && !AddListNameToTable)
						stencileInfo.childId = vec_items[i - 1];
					else if (!AddHeaderToTable && AddListNameToTable)
						stencileInfo.childId = vec_items[i - 1];
					else
						stencileInfo.childId = vec_items[i];

					stencileInfo.childTag = 1;
					stencileInfo.tableType = tStruct.tableType;
					stencileInfo.parentId = pNode.getPubId();
					stencileInfo.parentTypeId = pNode.getTypeId();
					stencileInfo.whichTab = 4; // Item Attributes ////for cell tag of product standard Table
					stencileInfo.imgFlag = CurrentSubSectionID; 
					stencileInfo.sectionID = CurrentSubSectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					stencileInfo.tableId = tStruct.tableId;
					stencileInfo.pbObjectId = pNode.getPBObjectID();

					XMLReference txmlref;

					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";
						//CA("PSTable Not Found : "+ TableTagNameNew);
					}
					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j, tableId );
					j++;
				}
				i++;
				j=0;
			}
			i=0;j=0;
			
		}
		
		if(IsAutoResize == 1)
		{
            InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			ASSERT(fitFrameToContentCmd != nil);
			if (fitFrameToContentCmd == nil) {
                return;
			}
			fitFrameToContentCmd->SetItemList(UIDList(BoxUIDRef));
			if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
                ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
                return;
			}
			return ; 
		}

		if(IsAutoResize == 2)
		{
			//CA("Inside overflow");
			ThreadTextFrame ThreadObj;
			ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
		}
		
	}while(kFalse);
	IsDBTable = kFalse;
    AddHeaderToTable = kFalse;
    AddListNameToTable = kFalse;

}

bool16 TableUtility ::FillDataInsideItemTableOfItem
			(
				XMLReference & boxXMLRef,
				UIDRef & tableModelUIDRef

			)
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<IIDXMLElement>boxXMLPtr(boxXMLRef.Instantiate());
		//Collect objectId,sectionid and typeId and languageId
		double objectId = pNode.getPBObjectID();
		double sectionid = -1;
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;

		InterfacePtr<ITableModel> tableModel(tableModelUIDRef,UseDefaultIID());
		if(tableModel == NULL)
		{
			//CA("tableModel == NULL");
			break;
		}
		
		XMLTagAttributeValue xmlTagAttrVal;
		getAllXMLTagAttributeValues(boxXMLRef,xmlTagAttrVal);
		
		//SectionLevelItem has two important IDs.PubId and PBObjectID.
		//PBObjectID is used while spraying its item table.

		PMString parentIDstr("");
		parentIDstr.AppendNumber(PMReal(pNode.getPubId()));
		xmlTagAttrVal.parentID = parentIDstr;

		PMString parentTypeIDstr("");
		parentTypeIDstr.AppendNumber(PMReal(pNode.getTypeId()));
		xmlTagAttrVal.parentTypeID = parentTypeIDstr;
		
		PMString sectionIDstr("");
		sectionIDstr.AppendNumber(PMReal(sectionid));
		xmlTagAttrVal.sectionID =sectionIDstr;

		//IMPORTANT: rowno stores the PBObjectID i.e the itemID.
		PMString rowno("");
		rowno = xmlTagAttrVal.rowno;
		xmlTagAttrVal.rowno =rowno;

		PMString pbObjectId("");
		pbObjectId.AppendNumber(PMReal(pNode.getPBObjectID()));
		xmlTagAttrVal.pbObjectId =pbObjectId;			
		
		double typeId =xmlTagAttrVal.typeId.GetAsDouble();
		double languageId = xmlTagAttrVal.LanguageID.GetAsDouble();
		PMString colnoForTableFlag = xmlTagAttrVal.colno;
		PMString isHeaderFlag = xmlTagAttrVal.header;

		//Add XMLElement to table and add the attributes to the table.
		//Note that the cellTags are also attached while attaching tag to table.
		//TableTag has attributes but cellTag has no attributes.
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		VectorScreenTableInfoPtr tableInfo=NULL;
		TableSourceInfoValue *tableSourceInfoValueObj;
		bool16 isCallFromTS = kFalse;
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		if(ptrTableSourceHelper != nil)
		{
			tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
			if(tableSourceInfoValueObj)
			{
				isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
				if(isCallFromTS == kTrue)
				{
					tableInfo=tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
					if(!tableInfo)
						return kFalse;
				}				
			}
		}
	
		if(isCallFromTS == kFalse)
		{
			tableInfo=ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID,languageId );
			if(!tableInfo)
			{
				break;
			}
		}

		int32 numRows=0;
		int32 numCols=0;

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;
		bool16 typeidFound=kFalse;
	
		if(isCallFromTS == kFalse) //This is for call from content sprayer
		{
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == typeId)
				{   				
					typeidFound=kTrue;				
					break;
				}
			}
			if(tableInfo)
			{
				tableInfo->clear();
				delete tableInfo;
			}
				
		}
		else	//This is for call from TABLEsource
		{

			for(it = tableInfo->begin(); it != tableInfo->end() ; it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0)  &&  oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && typeId == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
				{
					typeidFound=kTrue;
					break;
				}
			}
		}

		if(!typeidFound)
		{
			//CA("typeidFound == kFalse");
			break;
		}
		PMString tableID("");
		tableID.AppendNumber(PMReal(oTableValue.getTableID()));
		xmlTagAttrVal.tableId = tableID;
		setAllXMLTagAttributeValues(boxXMLRef ,xmlTagAttrVal);	

		XMLReference tableXMLRef;
		ErrorCode err = Utils<IXMLElementCommands>()->
						CreateTableElement( WideString("PSTable"), WideString("PSCell"),tableModelUIDRef,&tableXMLRef); //Cs4
		//Add attributes to the newly created table tag
		createAllXMLTagAttributes(tableXMLRef,xmlTagAttrVal);

		numRows = static_cast<int32>(oTableValue.getTableData().size());
		if(numRows<=0)
			break;
		numCols = static_cast<int32>(oTableValue.getTableHeader().size());
		if(numCols<=0)
			break;

		bool8 isTranspose = oTableValue.getTranspose();
		//Note that don't take the literal meaning of row and column
		//it has to be taken w.r.t the isTranspose flag.
		
		if(isTranspose == kFalse)
		{
			//rows are sprayed in rows of the table on document
			if(isHeaderFlag == "1")//add Table Header also 
			{//if(colnoForTableFlag != "-555")
				numRows = numRows + 1;//add for header row
			}
            else if(isHeaderFlag == "2")//add Table Header also + List name
			{
				numRows = numRows + 2;//add for header row + List name
			}
            if(isHeaderFlag == "3")//add List Name
			{
				numRows = numRows + 1;//add for List name
			}
		}
		else
		{
			//rows are sprayed in columns of the table on document
			//colums are sprayed in rows of the table on document
			int32 temp=0;
			temp = numRows;
			numRows = numCols;
			if(isHeaderFlag == "1")
            {
				temp = temp + 1;
            }
            else if(isHeaderFlag == "2")//add Table Header also + List name
			{
				temp = temp + 2;//add for header row + List name
			}
            if(isHeaderFlag == "3")//add List Name
			{
				temp = temp + 1;//add for List name
			}
			numCols = temp;
		}		
		//check the number of rows and colums of the current document table
		RowRange rowR = tableModel->GetTotalRows();
		ColRange colR = tableModel->GetTotalCols();
		int32 numberOfDocTableRows = rowR.count;
		int32 numberOfDocTableCols = colR.count;
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::FillDataInsideItemTableOfItem::!tableCommands");
			break;
		}

		InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
		if(tableGeometry == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::FillDataInsideItemTableOfItem::!tableGeometry");
			break;
		}
        //CA("Inserting col and rows");
		PMReal rowHt = tableGeometry->GetRowHeights(numberOfDocTableRows-1);
		PMReal colWidth = tableGeometry->GetColWidths(numberOfDocTableCols-1);
		if(numberOfDocTableRows != numRows)
		{			
			if(numberOfDocTableRows < numRows)
			{
				//we have to add the additional rows at the end
				int32 difference = numRows-numberOfDocTableRows;
				RowRange rr(numberOfDocTableRows-1,difference);
				ErrorCode result = tableCommands->InsertRows(rr, Tables::eAfter, rowHt);				
			}
			else
			{
				//we have to delete the surplus rows from bottom
				int32 difference = numberOfDocTableRows - numRows;
				RowRange rr(numberOfDocTableRows-difference,difference);
				ErrorCode result = tableCommands->DeleteRows(rr);				
			}
		}
		if(numberOfDocTableCols != numCols)
		{
			if(numberOfDocTableCols < numCols)
			{
				//we have to add the additional cols at the end
				int32 difference = numCols-numberOfDocTableCols ;
				ColRange cr(numberOfDocTableCols-1,difference);
				ErrorCode result = tableCommands->InsertColumns(cr, Tables::eAfter,colWidth);				
			}
			else
			{
				//we have to delete the surplus rows from bottom
				int32 difference = numberOfDocTableCols - numCols ;
				ColRange cr(numberOfDocTableCols-difference-1,difference);
				ErrorCode result = tableCommands->DeleteColumns(cr);				
			}
		}
		//When the control comes to this part..we have ensured that both the document
		//table and the database table have same number of rows and columns.
		//So now delete each tag from the cell and add new tag.
		vector<double>  vec_tableheaders = oTableValue.getTableHeader();		
		vector<vector<PMString> >  vec_tablerows = oTableValue.getTableData();
		vector<double>  vec_items = oTableValue.getItemIds();

		InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
		if(tableTextContent == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::FillDataInsideItemTableOfItem::!tableTextContent");											
			break;
		}
		UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();

		InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel());
		if(textModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::FillDataInsideItemTableOfItem::!textModel");											
			break;
		}

		int32 headerEntity = 0;//headerEntity can be row or column depending on the transpose
		vector<double> :: iterator colIterator;
		
		int32 headerEntity1 = 0;
		int32 headerEntity2 = 0;

		xmlTagAttrVal.tableFlag ="-1001";//indicates that the parent of this tag is an Item and not a product.
        //CA("Going to spray headers");
		if(isHeaderFlag == "1" || isHeaderFlag == "2" )//Add Table Header also isHeaderFlag
		{//if(colnoForTableFlag != "-555")
			
			for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity++)
			{//for loop of Header
				//Table Header tag have typeId = -2..ItemId is not required.
				//ID is taken from the vec_tableheaders elements.
				//Iterate through the first row of the document table.
				GridAddress cellAddr;
				if(isTranspose == kFalse)
				{
					//CA("isTranspose == kFalse");
					cellAddr.Set(0,headerEntity);
				}
				else
					cellAddr.Set(headerEntity,0);
                
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName((*colIterator),languageId);
				PMString textToInsert("");
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//if(!iConverter)
				{
					textToInsert=dispname;					
				}
				//else
				//	textToInsert=iConverter->translateString(dispname);
                
				//Update the Tag and its attribute values.
				PMString tagName("ATTRID_");
				tagName.AppendNumber(PMReal((*colIterator)));
				PMString attrVal;
				xmlTagAttrVal.tagName = tagName;
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*colIterator)));
				xmlTagAttrVal.ID =attrVal;

				attrVal.Clear();
				attrVal.AppendNumber(-1);
				xmlTagAttrVal.typeId=attrVal;

				attrVal.Clear();
				attrVal.AppendNumber(1);
				xmlTagAttrVal.header=attrVal;
				attrVal.Clear();
				attrVal.AppendNumber(PMReal(oTableValue.getTableID()));
				xmlTagAttrVal.tableId = attrVal;

				AddTagToCellText
					(
						cellAddr,
						tableModel,
						textToInsert,
						textModel,
						xmlTagAttrVal
					);
			
			
			}//end for loop of Header
			headerEntity1 = 1;//means Spray the data from the next row or column.
		}
        if(isHeaderFlag == "2" || isHeaderFlag == "3" )//Add Table Header also isHeaderFlag
		{
				GridAddress cellAddr;
				if(isTranspose == kFalse)
				{
					//CA("isTranspose == kFalse");
					cellAddr.Set(headerEntity1,0);
                    RowRange rowRange(headerEntity1, 1);
                    ColRange colRange(0 , numCols );
                    GridArea gridArea(rowRange,colRange);
                    ErrorCode errorCode = tableCommands->MergeCells(gridArea);
				}
				else
                {
					cellAddr.Set(0, headerEntity1);
                
                    RowRange rowRange(0, numCols);
                    ColRange colRange(headerEntity1 , 1 );
                    GridArea gridArea(rowRange,colRange);
                    ErrorCode errorCode = tableCommands->MergeCells(gridArea);
                }
                
				PMString dispname =oTableValue.getName();
				PMString textToInsert("");
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//if(!iConverter)
				{
					textToInsert=dispname;
				}
				//else
				//	textToInsert=iConverter->translateString(dispname);
                
				//Update the Tag and its attribute values.
				PMString tagName("List_Table_Name");
				XMLTagAttributeValue xmlTagAttrValforListName;
                xmlTagAttrValforListName = xmlTagAttrVal;
                
				PMString attrVal;
				xmlTagAttrValforListName.tagName = tagName;
				attrVal.Clear();
				xmlTagAttrValforListName.ID ="-121";
                xmlTagAttrValforListName.index = "4"; //Fot Item Table
                
				attrVal.Clear();
				attrVal.AppendNumber(-1);
				xmlTagAttrValforListName.typeId=attrVal;
				xmlTagAttrValforListName.header="-1";
				attrVal.Clear();
				attrVal.AppendNumber(PMReal(oTableValue.getTableID()));
				xmlTagAttrValforListName.tableId = attrVal;
                xmlTagAttrValforListName.dataType = "4";
                xmlTagAttrValforListName.tableFlag ="0";
                
				AddTagToCellText
                (
                 cellAddr,
                 tableModel,
                 textToInsert,
                 textModel,
                 xmlTagAttrValforListName
                 );

			headerEntity1 = headerEntity1 + 1;//means Spray the data from the next row or column.
		}
	 
		//For each row the ID element(attribute Id) is same but the typeId(ItemID) changes.
		//ID is taken from the vec_tableheaders
	
		//When isTranspose == kFalse
		//	headerEntity1 is row
		//	heaerEntity2 is column
		//When isTranspose == kTrue
		//	headerEntity1 is column
		//	headerEntity2 is row
		vector<double> :: iterator rowIterator;
		for(rowIterator=vec_items.begin(); rowIterator!=vec_items.end(); rowIterator++,headerEntity1++)
		{//start for 2
			//For each column the ID is different but the ItemID is same.
			vector<double>::iterator colIterator;
			headerEntity2 = 0;
			for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity2++)
			{				
				//row data feeding
				//ID is taken from the vec_tableheaders elements.
				//Iterate through the first row of the document table.	
				PMString dataToBeSprayed("");
				PMString strdataToBeSprayed("");
                
                if((*colIterator) == -401 || (*colIterator) == -402 || (*colIterator) == -403){
					strdataToBeSprayed = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId((*rowIterator),(*colIterator), languageId);
				}
				else{
					strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId((*rowIterator),(*colIterator),languageId, CurrentSectionID, kFalse);
				}
				
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//if(!iConverter)
				{
					dataToBeSprayed=(strdataToBeSprayed);					
				}
				//else
				//	dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);

				GridAddress cellAddr;
				if(isTranspose == kFalse)
                    cellAddr.Set(headerEntity1,headerEntity2);				
				else
					cellAddr.Set(headerEntity2,headerEntity1);

				//Update the Tag and its attribute values.
				PMString tagName("ATTRID_");
				tagName.AppendNumber(PMReal((*colIterator)));
				PMString attrVal;
				xmlTagAttrVal.tagName = tagName;
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*colIterator)));
				xmlTagAttrVal.ID =attrVal;
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*rowIterator)));
				xmlTagAttrVal.childId = attrVal;
				
				attrVal.Clear();
				attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
				xmlTagAttrVal.pbObjectId = attrVal;
				attrVal.Clear();
				attrVal.AppendNumber(-1);
				xmlTagAttrVal.header= attrVal;
				attrVal.Clear();
				attrVal.AppendNumber(PMReal(oTableValue.getTableID()));
				xmlTagAttrVal.tableId = attrVal;
				attrVal.Clear();
				attrVal.AppendNumber(1);
				xmlTagAttrVal.childTag = attrVal;


				AddTagToCellText
				(
					cellAddr,
					tableModel,
					dataToBeSprayed,
					textModel,
					xmlTagAttrVal
				);			
			}			
		}//end for 2
//CA("sprayed data");
		result = kTrue;
	}while(kFalse);
	return result;
	
}//end FillDataInsideItemTableOfItem

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////4Aug Item Table Preset Start ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//Do not use this function.
//Use FillDataInsideItemTableOfItem instead
void TableUtility::fillDataInTableForItem
(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	//CA("in fillDataInTableForItem");
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{
		IsDBTable = kTrue;
		int32 IsAutoResize = 0;
		if(tStruct.isAutoResize == 1)
			IsAutoResize= 1;

		if(tStruct.isAutoResize == 2)
			IsAutoResize= 2; 

		AddHeaderToTable = kTrue;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		double objectId = pNode.getPBObjectID(); //Product object
		double sectionid = -1;
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;
        
		VectorScreenTableInfoPtr tableInfo=
				ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, tStruct.languageID);
		if(!tableInfo)
		{
			//CA("tableinfo is NULL");
			return;
		}

		if(tableInfo->size()==0){//CA(" tableInfo->size()==0");
			break;
		}

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("Slection NULL");
			break;
		}
		if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			iSelectionManager->DeselectAll(nil);
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			return;
		}

		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
		
		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			//CA("My Suit NULL");
			//break;
		}
	
		PMRect theArea;
		PMReal rel;
		int32 ht;
		int32 wt;

		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
		if(iGeometry==NULL)
			break;

		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
		rel = theArea.Height();
		ht=::ToInt32(rel);
		rel=theArea.Width();	
		wt=::ToInt32(rel);

		int32 numRows=0;
		int32 numCols=0;

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;

		bool16 typeidFound=kFalse;
		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{
			oTableValue = *it;
			if(oTableValue.getTableTypeID() == tStruct.typeId)
			{   				
				typeidFound=kTrue;				
				break;
			}
		}
		if(tableInfo)
			delete tableInfo;

		if(!typeidFound){
			break;
		}

		numRows =static_cast<int32>(oTableValue.getTableData().size());
		if(numRows<=0)
			break;
		numCols =static_cast<int32>(oTableValue.getTableHeader().size());
		if(numCols<=0)
			break;
		bool8 isTranspose = oTableValue.getTranspose();
		TableFlagForStandardProductTable = 0; // Since we are spraying Item Table

		if(isTranspose)
		{
			int32 i=0, j=0;
			resizeTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht);
		
			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
	
			if(AddHeaderToTable){
				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
			 i=1; j=0;
			}

			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vec_items = oTableValue.getItemIds();
			
			vector<vector<PMString> >::iterator it1;
			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if((j)>numCols)
						break;
					setTableRowColData(tableUIDRef, (*it2), j, i);	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is
					stencileInfo.elementName = dispname;
					PMString TableColName("");
                    TableColName.Append("ATTRID_");
                    TableColName.AppendNumber(PMReal(vec_tableheaders[j]));

					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					stencileInfo.typeId = -1;
					if(AddHeaderToTable)
						stencileInfo.childId  = vec_items[i-1];
					else
						stencileInfo.childId  = vec_items[i];

					stencileInfo.tableType = tStruct.tableType;
					stencileInfo.parentId = pNode.getPBObjectID();
					stencileInfo.parentTypeId = pNode.getTypeId();
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = CurrentSubSectionID; 
					stencileInfo.sectionID = CurrentSubSectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.LanguageID = tStruct.languageID;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;

					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, j, i,tableId );
					j++;
				}
				i++;
				j=0;
			}
		}
		else
		{
			int32 i=0, j=0;
			resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);

			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
			if(AddHeaderToTable){
			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
			i=1; j=0;
			}

			vec_items = oTableValue.getItemIds();
			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if((j)>numCols)
						break;
		
					setTableRowColData(tableUIDRef, (*it2), i, j);
	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is
					stencileInfo.elementName = dispname;

					PMString TableColName("");
                    TableColName.Append("ATTRID_");
                    TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					stencileInfo.typeId = -1;
					if(AddHeaderToTable)
						stencileInfo.childId = vec_items[i-1];
					else
						stencileInfo.childId  = vec_items[i];

					stencileInfo.tableType = tStruct.tableType;
					stencileInfo.parentId = pNode.getPBObjectID();
					stencileInfo.parentTypeId = pNode.getTypeId();
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = CurrentSubSectionID; 
					stencileInfo.sectionID = CurrentSubSectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, i, j, tableId );
				j++;
				}
				i++;
				j=0;
			}
			i=0;j=0;
			
		}

		if(IsAutoResize == 1)
		{
			InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			ASSERT(fitFrameToContentCmd != nil);
			if (fitFrameToContentCmd == nil) {
			return;
			}
			fitFrameToContentCmd->SetItemList(UIDList(BoxUIDRef));
			if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
			ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
			return;
			}

			return ;
		}
		
		if(IsAutoResize == 2)
		{
			//CA("Inside overflow");
			ThreadTextFrame ThreadObj;
			ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
		}
		/*TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
		TabStyleObj.ApplyTableStyle();
		TabStyleObj.setTableStyle();*/
	
	}while(kFalse);
	IsDBTable = kFalse;

}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////4Aug Item Table Preset Start ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose, const int32& Width, const int32& Height )
{
	do
	{
		ErrorCode result;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 ColWidth = 0;
		int32 RowHt =0;

		PMReal ColumnWidth(ColWidth);
		PMReal RowHeight(RowHt);

		int32 presentRows =tableModel->GetTotalRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
			break;

		GridArea gridAreaForLastRow;
		int32 currentRows =tableModel->GetTotalRows().count;
		int32 currentCols = tableModel->GetTotalCols().count;
				 
		gridAreaForLastRow.topRow = currentRows-1;
		gridAreaForLastRow.leftCol = 0;
		gridAreaForLastRow.bottomRow = currentRows;
		gridAreaForLastRow.rightCol = currentCols;			

		if(isTranspose)
		{
			//CA("isTranspose");
			int32 nonewCols = 0;
			nonewCols = numCols;
			if(AddHeaderToTable){
				int32 PresenrtColsBeforeAddHeader = tableModel->GetTotalCols().count;
				result = tableCommands->InsertColumns(ColRange(PresenrtColsBeforeAddHeader-1,1), Tables::eAfter, 0);
				nonewCols = numCols + 1;
			}
			presentRows =tableModel->GetTotalRows().count;
			presentCols = tableModel->GetTotalCols().count;

			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, 0);
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(numRows-1, presentRows-numRows));
            }
			if(presentCols<nonewCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, nonewCols-presentCols), Tables::eAfter, 0);
			}
			else if( (presentCols > nonewCols) && (nonewCols > 0) )
			{
				result = tableCommands->DeleteColumns(ColRange(nonewCols-1, presentCols-nonewCols));
			}
		}
		else
		{
			//CA("!isTranspose");
			int32 numrowsNew =0;
			numrowsNew = numRows;
			
			if(AddHeaderToTable)
			{
                // CA("Add Header");
				int32 PresenrtRowsBrforeAddHeader = tableModel->GetTotalRows().count;
				result = tableCommands->InsertRows(RowRange(PresenrtRowsBrforeAddHeader-1 , 1), Tables::eAfter, 0);
				
				bool16 canCopy = tableModel->CanCopy(gridAreaForLastRow);
				if(canCopy == kFalse)
				{
					//CA("canCopy == kFalse");
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
					break;
				}
				numrowsNew = numRows +1;
			}
            if(AddListNameToTable)
			{
                // CA("Add AddListNameToTable");
				int32 PresenrtRowsBrforeAddHeader = tableModel->GetTotalRows().count;
				result = tableCommands->InsertRows(RowRange(PresenrtRowsBrforeAddHeader-1 , 1), Tables::eAfter, 0);
				
				numrowsNew = numrowsNew +1;
			}
			
			presentRows =tableModel->GetTotalRows().count;
			presentCols = tableModel->GetTotalCols().count;
	
			if(presentRows<numrowsNew)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numrowsNew-presentRows), Tables::eAfter, 0);
				bool16 canCopy = tableModel->CanCopy(gridAreaForLastRow);
				if(canCopy == kFalse)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
					break;
				}
				
				TableMemento * tempPtr = tableModel->Copy(gridAreaForLastRow);
				if(tempPtr == NULL)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
					break;
				}
				if(AddHeaderToTable)
					presentRows--;
                
                if(AddListNameToTable)
					presentRows--;
				
				for(int32 pasteIndex = 0;pasteIndex < numrowsNew-presentRows;pasteIndex++)
				{	//Apsiva9 ??
					//tableModel->Paste(GridAddress((pasteIndex + presentRows),0),ITableModel::eAll,tempPtr);
					//tempPtr = tableModel->Copy(gridAreaForLastRow);
				}
			}
			else if(presentRows>numrowsNew)
			{	
				//CA("Delete Row 1");
				result = tableCommands->DeleteRows(RowRange(numrowsNew-1, presentRows-numrowsNew));
				
			}

			if(presentCols<numCols)
			{
					//CA("InsertColumns	1");
					result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, 0);
			}
			else if( (presentCols > numCols) && (numCols > 0) )
			{
				result = tableCommands->DeleteColumns(ColRange(numCols-1, presentCols-numCols));

			}
		}
		
	}
	while(kFalse);
}

void TableUtility::setTableRowColData
(const UIDRef& tableUIDRef, const PMString& data, const int32& rowIndex, const int32& colIndex)
{
	PMString texttoinsert("");
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
        
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			break;
		}
		int32 numRowsNew =tableModel->GetTotalRows().count;
		int32 numColsNew = tableModel->GetTotalCols().count;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		PMString tempBuffer(data);
		
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//if(!iConverter)
		{	//CA("13");
			texttoinsert = tempBuffer;
			//return;
		}
		//texttoinsert = iConverter->translateString(tempBuffer);
		//WideString* wStr=new WideString(texttoinsert);
        texttoinsert.ParseForEmbeddedCharacters();
		const WideString wStr(texttoinsert);

		//iConverter->ChangeQutationMarkONOFFState(kFalse);
		tableCommands->SetCellText(wStr, GridAddress(rowIndex, colIndex));
		//iConverter->ChangeQutationMarkONOFFState(kTrue);
		
	}
	while(kFalse);
}

void TableUtility::putHeaderDataInTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
	do
	{
		HeadersizeVector.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		for(int i=0;i<vec_tableheaders.size();i++)
		{
			PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );
			InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
			if (tableModel == NULL)
			{
			//	CA("Err: invalid interface pointer ITableFrame");
				break;
			}

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
			//	CA("Err: invalid interface pointer ITableCommands");
				break;
			}
            dispname.ParseForEmbeddedCharacters();
			const WideString wStr(dispname);
			int32 StringLength = dispname.NumUTF16TextChars();
			HeadersizeVector.push_back(StringLength);

			if(isTranspose)
			{
				tableCommands->SetCellText(wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = vec_tableheaders[i];
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );
				stencileInfo.elementName = dispname;

				PMString TableColName("");
                TableColName.Append("ATTRID_");
                TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
                
				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;
				stencileInfo.header = 1;

				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.parentId = pNode.getPubId();
				stencileInfo.parentTypeId = pNode.getTypeId();
				stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
				//stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = CurrentSectionID; 
				stencileInfo.sectionID = CurrentSubSectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				stencileInfo.tableId = tStruct.tableId;
				stencileInfo.pbObjectId = pNode.getPBObjectID();
				XMLReference txmlref;

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

			}
			else
			{
				tableCommands->SetCellText(wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId =vec_tableheaders[i];
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );
				stencileInfo.elementName = dispname;
				PMString TableColName("");
                TableColName.Append("ATTRID_");
                TableColName.AppendNumber(PMReal(vec_tableheaders[i]));

				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID =tStruct.languageID;
				stencileInfo.typeId  = -1;
				stencileInfo.header = 1;
				
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.parentId = pNode.getPubId();
				stencileInfo.parentTypeId = pNode.getTypeId();
				stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
				//stencileInfo.whichTab = tStruct.whichTab; 
				stencileInfo.imgFlag = CurrentSectionID; 
				stencileInfo.sectionID = CurrentSubSectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				stencileInfo.tableId = tStruct.tableId;
				stencileInfo.pbObjectId = pNode.getPBObjectID();
				XMLReference txmlref;
			
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
			}
		}
	}
	while(kFalse);
}

void TableUtility::putListNameInTable
(const UIDRef& tableUIDRef, PMString listName, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, int32 numCols)
{
	do
	{
		HeadersizeVector.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
        
        int i=0;
        if(AddHeaderToTable)
            i++;
        
        
		//for(int i=0;i<vec_tableheaders.size();i++)
		//{
			InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
			if (tableModel == NULL)
			{
                //	CA("Err: invalid interface pointer ITableFrame");
				break;
			}
            
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
                //	CA("Err: invalid interface pointer ITableCommands");
				break;
			}
            listName.ParseForEmbeddedCharacters();
			const WideString wStr(listName);
			int32 StringLength = listName.NumUTF16TextChars();
            
			if(isTranspose)
			{
                //Merge column
                
                RowRange rowRange(0, numCols);
				ColRange colRange(i , 1 );
                GridArea gridArea(rowRange,colRange);
                ErrorCode errorCode = tableCommands->MergeCells(gridArea);
                
				tableCommands->SetCellText(wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -121; // List/Table Name
				stencileInfo.elementName = "List_Table_Name";
                
				stencileInfo.TagName = "List_Table_Name";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;
				stencileInfo.header = -1;
                
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.parentId = pNode.getPubId();
				stencileInfo.parentTypeId = pNode.getTypeId();
				stencileInfo.whichTab = tStruct.whichTab;
				//stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = CurrentSectionID;
				stencileInfo.sectionID = CurrentSubSectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				stencileInfo.tableId = tStruct.tableId;
				stencileInfo.pbObjectId = pNode.getPBObjectID();
				XMLReference txmlref;
                
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
                
			}
			else
			{
                //Merge full row
                
                RowRange rowRange(i, 1);
				ColRange colRange(0 , numCols );
                GridArea gridArea(rowRange,colRange);
                ErrorCode errorCode = tableCommands->MergeCells(gridArea);

                
				tableCommands->SetCellText(wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId =-121; // List/Table Name
				stencileInfo.elementName = "List_Table_Name";
                
				stencileInfo.TagName = "List_Table_Name";
				stencileInfo.LanguageID =tStruct.languageID;
				stencileInfo.typeId  = -1;
				stencileInfo.header = -1;
                stencileInfo.dataType = 4; // table Name
				
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.parentId = pNode.getPubId();
				stencileInfo.parentTypeId = pNode.getTypeId();
				stencileInfo.whichTab = tStruct.whichTab;
				//stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = CurrentSectionID;
				stencileInfo.sectionID = CurrentSubSectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				stencileInfo.tableId = tStruct.tableId;
				stencileInfo.pbObjectId = pNode.getPBObjectID();
                stencileInfo.tableFlag = 1;
				XMLReference txmlref;
                
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );
			}
		//}
	}
	while(kFalse);
}

void TableUtility::SetSlug(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, vector<double> vec_items,bool8 isTranspose)
{
/*	//CA("In show Function");
	do{
					//CA("1");
					//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection());
					//if(!iSelectionManager)
					//{
					//	CA("Source Selection Manager is null");
					//	break;
					//}
					////CA("2");
					//InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
					//if(!pTextSel)
					//{
					//	CA("Source IConcrete Selection is null");
					//	break;
					//}
					////CA("Got Concrete Selection");

					//InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
					//if(!tblTxtSel)
					//{
					//	CA(" Source Table Text Selection is null");
					//	break;
					//}
					//CA("Got TableText selection");

					
					//CA("234");
				InterfacePtr<ITableModel> tblModel(tableUIDRef,UseDefaultIID());
				if(!tblModel)
				{
					CA("tblModel is null");
				}

				//CA("Calling Premal Function");
				//IterateThroughTable(tblModel);
				//CA("After Premal's Function");

				ColRange col = tblModel->GetTotalCols();
				RowRange row = tblModel->GetTotalRows();

				int32 rows=0,cols=0;

				if(isTranspose)
				{
					 rows = col.count;
					 cols = row.count;
				}
				else
				{
					 rows = row.count;
					 cols = col.count;
				}

				PMString AS1;
				//AS1.AppendNumber(rows);
				//AS1.AppendNumber(cols);
				//CA(AS1);
				for(int i=0;i<rows;i++)
				{
					for(int j=0;j<cols;j++)
					{
					InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
					if(!tblSelSuite)
					{
					CA(" Source Table Selection suite is null");
					break;
					}
					//CA("Got Table Selection Suite");
					if(!isTranspose)
					{
						GridArea ga(i,j,i,j);
						tblSelSuite->DeselectAll();
						tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
					}
					else
					{
						GridArea ga(j,i,j,i);
						tblSelSuite->DeselectAll();
						tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
					}

					//GridArea ga(rowIndex,colIndex,rowIndex,colIndex);
					
					//CA("999");
					//tblTxtSel->SelectTextInCell(tblModel,gadr);
					//tblTxtSel->SelectTextInCell(tblModel,GridAddress(rowIndex, colIndex));
					//tblTxtSel->SelectTextInCell(tblModel,GridAddress(i,j));
					//tblTxtSel->SelectTextInCell(tblModel,GridAddress(j,i)); //for Transpose table
/////////////////////////////Adding Slug/////////////////////////////////////
					InterfacePtr<ISlugData> temp1Data(::CreateObject2<ISlugData>(kBscTAAttrBoss));

					SlugList anotherTempList;
					PMString tempName("amit");
					PMString tempCol("Kumar");

					//CA("11");
						
					SlugStruct sT;

					//PMString ami;
					//ami.Append("AttrbId....");
					//ami.AppendNumber(vec_tableheaders[j]);
					//CA(ami);
					sT.elementId =vec_tableheaders[j];
					if(i==0)
					{
						//CA("sT.typeId=-1");
						sT.typeId=-1;
					}
					else
					{	
						//ami.Clear();
						//ami.Append("Itemid....");
						//ami.AppendNumber(vec_items[i-1]);
						//CA(ami);
						sT.typeId=vec_items[i-1];
					}
					sT.whichTab = 1;
					sT.reserved1=0;
					sT.parentId=-1;
					sT.reserved2=0;

					anotherTempList.push_back(sT);

					//count1++;

					temp1Data->SetList(anotherTempList, tempName, tempCol);

					AttributeBossList *attrs = new AttributeBossList;
					//CA("get constructor");
					attrs->ApplyAttribute(temp1Data);

					InterfacePtr<ITextAttributeSuite>textAttributeSuite
					((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
					if (textAttributeSuite != NULL && textAttributeSuite->CanApplyAttributes())
					{
						// apply the attributes on the current text selection.
						//CA("Added structure");
						ErrorCode status = textAttributeSuite->ApplyAttributes(attrs,kCharAttrStrandBoss);//kCharAttrStrandBoss
					}
					else
					{
						CA("text is not selected");
					}
////////////////////////////////END OF ADDING//////////////////////////////////////////
					tblSelSuite->DeselectAll();
					//anotherTempList.clear();
					}
				}
				//for(int i=0;i<rows;i++)
				//{
				//	for(int j=0;j<cols;j++)
				//	{
				//		InterfacePtr<ITextAttributeSuite>textAttributeSuite2
				//((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
				//if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
				//{
				//
				//	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
				//	if(!tblSelSuite)
				//	{
				//	CA(" Source Table Selection suite is null");
				//	break;
				//	}
				//	//CA("Got Table Selection Suite");
				//	GridArea ga(i,j,i,j);
				//	//GridArea ga(rowIndex,colIndex,rowIndex,colIndex);
				//	tblSelSuite->DeselectAll();
				//	tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
				//	CA("999");
				//	//tblTxtSel->SelectTextInCell(tblModel,gadr);
				//	//tblTxtSel->SelectTextInCell(tblModel,GridAddress(rowIndex, colIndex));
				//	tblTxtSel->SelectTextInCell(tblModel,GridAddress(i,j));

				//	PMString AS1;
				//	AS1.AppendNumber(textAttributeSuite2->CountAttributes(kBscTAAttrBoss));
				//	CA(AS1);
				//	AttributeBossList *attrs = new AttributeBossList;
				//	textAttributeSuite2->CopyToAttributeLists(attrs);
				//	if(!attrs)
				//	{
				//		CA("Attrs is Null");
				//	}

				//	InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
				//	if(!temp1Data)
				//	{
				//		CA("temp1Data is NULL...........");
				//	}
				//	InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
				//	if(!temp2Data)
				//	{
				//		CA("temp1Data is NULL...........");
				//	}
				//	if(temp2Data)
				//	{
				//	SlugList anotherTempList;
				//			PMString tempName;
				//			PMString tempCol;
				//			int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
				//			CA(tempName);
				//			CA(tempCol);
				//			PMString AS;
				//			AS.AppendNumber(anotherTempList.size());
				//			CA(AS);
				//			AS.Clear();
				//			AS.AppendNumber(anotherTempList[0].elementId);
				//			CA(AS);
				//			AS.Clear();
				//			AS.AppendNumber(anotherTempList[0].typeId);
				//			CA(AS);
				//	}
				//}


				//	}
				//}

					//CA("000");
				
	}while(kFalse);
*/
}


void TableUtility::Slugreader(const UIDRef& tableUIDRef, const int32& rowIndex, const int32& colIndex,double& Attr_id,double& Item_id)
{
/*	
	CA("TableUtility::Slugreader");
	int32 attr_id=-1;
	do{
					//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection());
					//if(!iSelectionManager)
					//{
					//	CA("Source Selection Manager is null");
					//	break;
					//}
					////CA("2");
					//InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
					//if(!pTextSel)
					//{
					//	CA("Source IConcrete Selection is null");
					//	break;
					//}
					////CA("Got Concrete Selection");

					//InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
					//if(!tblTxtSel)
					//{
					//	CA(" Source Table Text Selection is null");
					//	break;
					//}
					//CA("Got TableText selection");

					InterfacePtr<ITableModel> tblModel(tableUIDRef,UseDefaultIID());
					if(!tblModel)
					{
						CA("tblModel is null");
					}
					//for(int i=0;i<rowIndex;i++)
				//{
					//for(int j=0;j<colIndex;j++)
					//{
						InterfacePtr<ITextAttributeSuite>textAttributeSuite2
				((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
				if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
				{
				
					InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
					if(!tblSelSuite)
					{
					CA(" Source Table Selection suite is null");
					break;
					}
					//CA("Got Table Selection Suite");
					//GridArea ga(i,j,i,j);
					GridArea ga(rowIndex,colIndex,rowIndex,colIndex);
					//tblSelSuite->DeselectAll();
					tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
					CA("999");
					//tblTxtSel->SelectTextInCell(tblModel,gadr);
					//tblTxtSel->SelectTextInCell(tblModel,GridAddress(rowIndex, colIndex));
					//tblTxtSel->SelectTextInCell(tblModel,GridAddress(i,j));


					PMString AS1;
					AS1.AppendNumber(textAttributeSuite2->CountAttributes(kBscTAAttrBoss));
					CA(AS1);
					AttributeBossList *attrs = new AttributeBossList;
					textAttributeSuite2->CopyToAttributeLists(attrs);
					if(!attrs)
					{
						CA("Attrs is Null");
					}

					InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
					if(!temp1Data)
					{
						CA("temp1Data is NULL...........");
					}
					InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
					if(!temp2Data)
					{
						CA("temp2Data is NULL...........");
					}
					if(temp2Data)
					{
					SlugList anotherTempList;
							PMString tempName;
							PMString tempCol;
							int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
							CA(tempName);
							CA(tempCol);
							PMString AS;
							AS.AppendNumber(anotherTempList.size());
							CA(AS);
							AS.Clear();
							AS.AppendNumber(anotherTempList[0].elementId);
							CA(AS);
							AS.Clear();
							AS.AppendNumber(anotherTempList[0].typeId);
							CA(AS);
							Attr_id= anotherTempList[0].elementId;
							Item_id = anotherTempList[0].typeId;
					}
				}


					//}
				//}


	}while(kFalse);
*/
}

PMString TableUtility::TabbedTableData(const UIDRef& tableUIDRef, PublicationNode& pNode, double tableTypeId,double& tableId, TagStruct& tStruct)
{
	PMString result("");
	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
 
		double objectId = pNode.getPubId(); //Product object
		VectorScreenTableInfoPtr tableInfo = NULL;
		
		if(pNode.getIsONEsource())
		{
			// For ONEsource
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
		}else
		{
			//For publication mode is selected
			tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSectionID, objectId, tStruct.languageID, kFalse);
		}

		if(!tableInfo)
		{
			//CA("tableinfo is NULL");
			break;
		}

		if(tableInfo->size()==0){ //CA(" tableInfo->size()==0");
			break;
		}
		int32 numRows=0;
		int32 numCols=0;

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;

		bool16 typeidFound=kFalse;

		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{
			oTableValue = *it;

			if(oTableValue.getTableTypeID() == tableTypeId)
			{
				typeidFound=kTrue;				
				break;
			}
		}

		if(!typeidFound){ 
			break;
		}
		numRows = static_cast<int32>(oTableValue.getTableData().size());
		if(numRows<=0)
			break;
		numCols = static_cast<int32>(oTableValue.getTableHeader().size());
		if(numCols<=0)
			break;
		bool8 isTranspose = oTableValue.getTranspose();

		vector<double> vec_tableheaders;
		vector<double> vec_items;

		vec_tableheaders = oTableValue.getTableHeader();
		vec_items = oTableValue.getItemIds();
	//	isTranspose = kTrue;

		if(isTranspose)
		{
			int32 i=1, j=0;
			bool8 FLAG=kFalse;
			
			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();		

			for(i=0;i<vec_tableheaders.size();i++)
			{
				if(i!=0)
					result.Append("\r");

				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );
				result.Append(dispname);
				result.Append("\t");

				vector<vector<PMString> >::iterator it1;
				for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
				{

					vector<PMString>::iterator it2;
					it2=(*it1).begin();
					it2 = it2+i;
					result.Append((*it2));
					if(it1!=vec_tablerows.end()-1)
						result.Append("\t");
				}
			}

		}
		else
		{
			int32 i=1, j=0;
			bool8 FLAG=kFalse;
			for(i=0;i<vec_tableheaders.size();i++)
			{
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );
				result.Append(dispname);
				if(i<vec_tableheaders.size()-1)
				result.Append("\t");
				else
					result.Append("\r");
				FLAG=kTrue;
			}

			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if((j)>numCols)
						break;

					result.Append((*it2));
					if(it2!=(*it1).end()-1)
						result.Append("\t");
                    j++;
				}
				if(it1!=vec_tablerows.end())
				{
					result.Append("\r");
				}

				i++;
				j=0;
			}
			
        }

    }while(kFalse);

	return result;
}

void TableUtility::AddTexttoTabbedTable(const UIDRef& tableUIDRef,PMString result,vector<double> vec_tableheaders, vector<double> vec_items,bool8 isTranspose)
{
	//bool8 Flagtr = kFalse;

	//InterfacePtr<IPMUnknown> unknown(tableUIDRef, IID_IUNKNOWN);
	//UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	//if (textFrameUID == kInvalidUID)
	//	return;
	//
	//InterfacePtr<ITextFrame> textFrame(tableUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//if (textFrame == NULL)
	//	return;
	//
	//InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	//if (textModel == NULL)
	//	return;
	////			//CA("Tabbed table box");

	//			PMString textToInsert;
	////			/*textToInsert.Append("Amit");
	////			textToInsert.Append("\t");
	////			textToInsert.Append("Kumar");
	////			textToInsert.Append("\r");
	////			textToInsert.Append("Srivastava");
	////			textToInsert.Append("\t");
	////			textToInsert.Append("i m gr8");
	////			int32 tStart=0,tEnd=0;*/

	////			tEnd = 10;
	//			int32 tableId=0;
	//			textToInsert = oTableUtil.TabbedTableData(boxUIDRef, pNode, tStruct.typeId, tableId);
	//			
	//			WideString* myText=new WideString(result);

	//			CA("123");
	//			textModel->Insert(kTrue,0, myText);

	//			InterfacePtr<ITagWriter> tWrite
	//			((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
	//			if(!tWrite)
	//				return;

	//			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);

	//			//CA("456");
	//			//PMString as;
	//			//as.AppendNumber(textModel->GetPrimaryStoryThreadSpan());
	//			//CA(as);

	////			//CA("456");
	////			//PMString as;
	////			//as.AppendNumber(textModel->GetPrimaryStoryThreadSpan());
	////			//CA(as);

	////			/*InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
	////			if(!txtSelSuite)
	////			{
	////			CA("Text Selection Suite is null");
	////			return;
	////			}*/

	////			if(!isTranspose)
	////			{
	////				int32 start=0;
	////				int32 attrIndex=0,itemIndex=0;
	////				for(int j=0;j<result.ByteLength();j++)
	////				{
	////					///CA("111111111111111");
	////						while((result[j]!='\t' && result[j]!='\r') && j<result.ByteLength())
	////						{
	////							//CA("678");
	////							j++;
	////						}
	////					
	////					RangeData rData(start,j);

	////					/*PMString as;
	////					as.AppendNumber(start);
	////					as.AppendNumber(j);
	////					CA(as);*/

	////					InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
	////					if(!txtSelSuite)
	////					{
	////					CA("Text Selection Suite is null");
	////					return;
	////					}

	////					const UIDRef wrt = ::GetUIDRef(textModel);

	////					txtSelSuite->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);
	////					

	////					start=j+1;

	////					if(vec_tableheaders.size()==attrIndex)
	////					{
	////						attrIndex=0;
	////						if(Flagtr == kTrue)
	////							itemIndex++;
	////						Flagtr = kTrue;
	////					}
	////					if(Flagtr == kTrue)
	////					{
	////						//CA("1111");
	////						//tWrite->AddSlugtoText(vec_tableheaders[attrIndex],vec_items[itemIndex]);
	////						attrIndex++;
	////						//itemIndex++;
	////					}
	////					else
	////					{
	////						//CA("2222");
	////						tWrite->AddSlugtoText(vec_tableheaders[attrIndex],-1);
	////						attrIndex++;
	////					}

	////					/*RangeData rData1(0,0);
	////					txtSelSuite->SetTextSelection(wrt,rData1,Selection::kDontScrollSelection ,NULL);
	////					CA("Select");*/

	////					InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->QueryActiveSelection());
	////					if(!iSelectionManager)
	////					{
	////						CA("Source Selection Manager is null");
	////						break;
	////					}
	////				
	////					InterfacePtr<ILayoutSelectionSuite>layoutSelSuite(Utils<ISelectionUtils>()->QueryLayoutSelectionSuite(iSelectionManager));
	////						if(!layoutSelSuite)
	////						{
	////						CA("Layout Selection Suite is null");
	////						//break;
	////						}
	////						layoutSelSuite->DeselectAll();


	////					
	////				}
	////				//TextSlugReader(tableUIDRef);
	////				
	////			}
	////			else
	////			{

	////				int32 start=0;
	////				int32 attrIndex=0,itemIndex=0;
	////				Flagtr = kTrue;
	////				for(int j=0;j<result.ByteLength();j++)
	////				{
	////					///CA("111111111111111");
	////						while((result[j]!='\t' && result[j]!='\r') && j<result.ByteLength())
	////						{
	////							//CA("678");
	////							j++;
	////						}
	////					
	////					RangeData rData(start,j);

	////					/*PMString as;
	////					as.AppendNumber(start);
	////					as.AppendNumber(j);
	////					CA(as);*/

	////					InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
	////					if(!txtSelSuite)
	////					{
	////					CA("Text Selection Suite is null");
	////					return;
	////					}

	////					const UIDRef wrt = ::GetUIDRef(textModel);

	////					txtSelSuite->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);
	////					

	////					start=j+1;

	////					if(vec_items.size()==itemIndex)
	////					{
	////						attrIndex++;
	////						Flagtr= kTrue;
	////						itemIndex=0;
	////						
	////					}
	////					if(Flagtr == kTrue)
	////					{
	////						//CA("1111");
	////						tWrite->AddSlugtoText(vec_tableheaders[attrIndex],-1);
	////						//attrIndex++;
	////						Flagtr= kFalse;
	////						//itemIndex++;
	////					}
	////					else
	////					{
	////						//CA("2222");
	////						tWrite->AddSlugtoText(vec_tableheaders[attrIndex],vec_items[itemIndex]);
	////						itemIndex++;
	////					}


	////					InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->QueryActiveSelection());
	////					if(!iSelectionManager)
	////					{
	////						CA("Source Selection Manager is null");
	////						break;
	////					}
	////				
	////					InterfacePtr<ILayoutSelectionSuite>layoutSelSuite(Utils<ISelectionUtils>()->QueryLayoutSelectionSuite(iSelectionManager));
	////						if(!layoutSelSuite)
	////						{
	////						CA("Layout Selection Suite is null");
	////						//break;
	////						}
	////						layoutSelSuite->DeselectAll();

	////				}

	////			}
}
void  TableUtility::AddSlugtoText(double Attr_id,double Item_id)
{
	/*
					InterfacePtr<ISlugData> temp1Data(::CreateObject2<ISlugData>(kBscTAAttrBoss));
					SlugList anotherTempList;
					PMString tempName("amit");
					PMString tempCol("Kumar");

					SlugStruct sT;

					//PMString as("Attr..");
					as.AppendNumber(Attr_id);
					CA(as);
					as.Clear();
					as.AppendNumber(Item_id);
					CA(as);*/

					//PMString ami;
					//ami.Append("AttrbId....");
					//ami.AppendNumber(vec_tableheaders[j]);
					//CA(ami);
	//				sT.elementId =Attr_id;
					//int32 i=0;
					//if(i==0)
					//{
						//CA("sT.typeId=-1");
						//sT.typeId=-1;
					//}
					//else
					//{	
						//ami.Clear();
						//ami.Append("Itemid....");
						//ami.AppendNumber(vec_items[i-1]);
						//CA(ami);
	/*					sT.typeId=Item_id;
					//}
					sT.whichTab = 1;
					sT.reserved1=0;
					sT.parentId=-1;
					sT.reserved2=0;

					anotherTempList.push_back(sT);

					//count1++;

					temp1Data->SetList(anotherTempList, tempName, tempCol);

					AttributeBossList *attrs = new AttributeBossList;
					//CA("get constructor");
					attrs->ApplyAttribute(temp1Data);

					InterfacePtr<ITextAttributeSuite>textAttributeSuite
					((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
					if (textAttributeSuite != NULL && textAttributeSuite->CanApplyAttributes())
					{
					// apply the attributes on the current text selection.
						//CA("Added structure");
						ErrorCode status = textAttributeSuite->ApplyAttributes(attrs,kCharAttrStrandBoss);//kCharAttrStrandBoss
					}
					else
					{
						CA("text is not selected");
					}
		*/
	}

void TableUtility::IterateThroughTable(InterfacePtr<ITableModel>&tblModel)
{
	//if(!tblModel)
	//{
	//	CA("Table Model for layout is null");
	//	return;
	//}
	//InterfacePtr<ITableLayout>tblLayout(tblModel,UseDefaultIID());
	//if(!tblLayout)
	//{
	//	CA("Table Layout is null");
	//	return;
	//}
	//ITableLayout::frame_iterator beginFrmItr=tblLayout->begin_frame_iterator ();
	//ITableLayout::frame_iterator endFrmItr=tblLayout->end_frame_iterator ();
	//PMString totIteration("Total Iteration: ");
	//int32 totItr=0;
	//InterfacePtr<ITableFrame>tblFrame((*beginFrmItr).QueryFrame(),UseDefaultIID());
	//if(!tblFrame)
	//	CA("Table Frame is null");
	////UIDRef tblFrameRef=::GetUIDRef(tblModel);
	//ITableFrame::const_parcel_iterator begParcelItr=tblFrame-> begin_parcel_iter ();
	//ITableFrame::const_parcel_iterator endParcelItr=tblFrame-> end_parcel_iter ();
	//
	//UIDRef tblFrameRef=tblFrame->GetFrameRef();;
	//while(1)
	//{
	//	if(begParcelItr==endParcelItr)
	//		break;
	//	CellParcelAddr parcelAddr=(*begParcelItr);
	//	UID frameUID=tblLayout->GetParcelFrameUID(parcelAddr.cellAddr,parcelAddr.parcelKey);
	//	if(frameUID==kInvalidUID)
	//		CA("Invalid Cell Frame UID");
	//	/*PMString celAddr("Row and Col: ");
	//	celAddr.AppendNumber(parcelAddr.cellAddr.row);
	//	celAddr.Append("  ");
	//	celAddr.AppendNumber(parcelAddr.cellAddr.col);
	//	CA(celAddr);*/
	//	
	//	/*GridAddress ga(0,0);
	//	ParcelKey pk;
	//	CellParcelAddr parcelAddr1(ga,pk);*/
	//	InterfacePtr<ITableFrame>tableFrame(tblFrameRef.GetDataBase(),frameUID,UseDefaultIID());
	//	if(!tableFrame)
	//	{
	//		CA("TableFrame is null");
	//		return;
	//	}

	//	////////////////////////add your code here/////////////
	//	InterfacePtr<ISlugData> temp(::GetUIDRef(tableFrame),UseDefaultIID());
	//	if(!temp)
	//	{
	//		CA("temp is null");
	//	}

	//	///////////////////////////////////////////////////////
	//	/*if(tableFrame->Contains(parcelAddr1))
	//		CA("Correct Frame");*/
	//	/*InterfacePtr<ITextFrame>textFrame(tableFrame->QueryFrame(),UseDefaultIID());
	//	if(!textFrame)
	//	{
	//		CA("Text Frame is null");
	//		return;
	//	}*/
	//	/*InterfacePtr<ITextModel>txtModel(textFrame->QueryTextModel(),UseDefaultIID());
	//	if(!txtModel)
	//	{
	//		CA("Text Model is null");
	//		return;
	//	}
	//	PMString totLength("Total Length: ");
	//	totLength.AppendNumber(txtModel->TotalLength());
	//	CA(totLength);*/
	//	begParcelItr++;
	//	totItr++;
	//}
	//totIteration.AppendNumber(totItr);
	////CA(totIteration);

}

void TableUtility::NewSlugreader(InterfacePtr<ITableModel> &tblModel, const int32& rowIndex, const int32& colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel)
{
	//CA("TableUtility::NewSlugreader");

					//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection());
					//if(!iSelectionManager)
					//{
					//	CA("Source Selection Manager is null");
					//	return;
					//}
					////CA("2");
					//InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
					//if(!pTextSel)
					//{
					//	CA("Source IConcrete Selection is null");
					//	return;
					//}
					////CA("Got Concrete Selection");

					//InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
					//if(!tblTxtSel)
					//{
					//	CA(" Source Table Text Selection is null");
					//	return;
					//}
					////CA("Got TableText selection");
	do{
	//				
	//			InterfacePtr<ITextAttributeSuite>textAttributeSuite2
	//			((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
	//			if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
	//			{
	//			
	//				InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	//				if(!tblSelSuite)
	//				{
	//				CA(" Source Table Selection suite is null");
	//				break;
	//				}
	//				//CA("Got Table Selection Suite");
	//				//GridArea ga(i,j,i,j);
	//				GridArea ga(rowIndex,colIndex,rowIndex,colIndex);
	//				//tblSelSuite->DeselectAll();
	//				tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
	//				//CA("999");
	//				//tblTxtSel->SelectTextInCell(tblModel,gadr);
	//				tblTxtSel->SelectTextInCell(tblModel,GridAddress(rowIndex, colIndex));
	//				//tblTxtSel->SelectTextInCell(tblModel,GridAddress(i,j));


	//				//PMString AS1;
	//				//AS1.AppendNumber(textAttributeSuite2->CountAttributes(kBscTAAttrBoss));
	//				//CA(AS1);
	//				AttributeBossList *attrs = new AttributeBossList;
	//				textAttributeSuite2->CopyToAttributeLists(attrs);
	//				if(!attrs)
	//				{
	//					CA("Attrs is Null");
	//				}

	//				InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
	//				if(!temp1Data)
	//				{
	//					//CA("temp1Data is NULL...........");
	//				}
	//				InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
	//				if(!temp2Data)
	//				{
	//					//CA("temp2Data is NULL...........");
	//				}
	//				if(temp2Data)
	//				{
	//						SlugList anotherTempList;
	//						PMString tempName;
	//						PMString tempCol;
	//						int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
	//						/*CA(tempName);
	//						CA(tempCol);
	//						PMString AS;
	//						AS.AppendNumber(anotherTempList.size());
	//						CA(AS);
	//						AS.Clear();
	//						AS.AppendNumber(anotherTempList[0].elementId);
	//						CA(AS);
	//						AS.Clear();
	//						AS.AppendNumber(anotherTempList[0].typeId);
	//						CA(AS);*/
	//						Attr_id= anotherTempList[0].elementId;
	//						Item_id = anotherTempList[0].typeId;
	//				}
	//			}


	}while(kFalse);
}
void  TableUtility::TextSlugReader(const UIDRef& tableUIDRef)
{
//	int32 StoryLenght=0;
//
//	//CA("TableUtility::TextSlugReader");
//
//		InterfacePtr<IPMUnknown> unknown(tableUIDRef, IID_IUNKNOWN);
//		UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//
//		if (textFrameUID == kInvalidUID)
//			return;
//
//		InterfacePtr<ITextFrame> textFrame(tableUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		if (textFrame == NULL)
//			return;
//			
//		
//		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//		if (textModel == NULL)
//			return;
//
//
//		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
//		if(!txtSelSuite)
//		{
//		CA("Text Selection Suite is null");
//		return;
//		}
//
//		//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
//		//if(!iSelectionManager)
//		//{
//		//	CA("Source Selection Manager is null");
//		//	return;
//		//}
//		////CA("2");
//		//InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
//		//if(!pTextSel)
//		//{
//		//	CA("Source IConcrete Selection is null");
//		//	return;
//		//}
//
//		//InterfacePtr<ITextSelectionSuite> txtSel(pTextSel,UseDefaultIID());
//		//if(!txtSel)
//		//{
//		//CA("Text Selection Suite is null");
//		//return;
//		//}
//
////CA("5");
//		StoryLenght = textModel->GetPrimaryStoryThreadSpan();
//
//
//		TextIterator begin(textModel, 0);
//		TextIterator end(textModel, StoryLenght);
//
//		int32 j=0,start=0;
//		for (TextIterator iter = begin; iter <= end && j<StoryLenght; iter++,j++)
//		{
//	
//			//const textchar characterCode = (*iter).GetTextChar();
//			UTF16TextChar characterCode;
//			int32 len=1; 
//			(*iter).ToUTF16(&characterCode,&len);
//			char buf;
//			
//			::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
//			
//			if((buf=='\t' || buf=='\r') && j<StoryLenght)
//			{
//				//CA("got the tab");
//				RangeData rData(start,j);
//
//				//PMString as;
//				//as.AppendNumber(start);
//				//as.AppendNumber(j);
//				//CA(as);
//				//CA("8");
//				const UIDRef wrt = ::GetUIDRef(textModel);
//				//CA("9");
//				txtSelSuite->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);
//
//				//txtSel->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);
//
//				//CA("10");
//				start=j+1;
//				InterfacePtr<ITextAttributeSuite>textAttributeSuite2
//				((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
//				if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
//				{
//					AttributeBossList *attrs = new AttributeBossList;
//					textAttributeSuite2->CopyToAttributeLists(attrs);
//					if(!attrs)
//					{
//						CA("Attrs is Null");
//					}
//
//					InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
//					if(!temp1Data)
//					{
//						CA("temp1Data is NULL...........");
//					}
//					InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
//					if(!temp2Data)
//					{
//						CA("temp2Data is NULL...........");
//					}
//					if(temp2Data)
//					{
//						int32 Attr_id=0,Item_id=0;
//							SlugList anotherTempList;
//							PMString tempName;
//							PMString tempCol;
//							int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
//							/*CA(tempName);
//							CA(tempCol);
//							PMString AS;
//							AS.AppendNumber(anotherTempList.size());
//							CA(AS);
//							AS.Clear();
//							AS.AppendNumber(anotherTempList[0].elementId);
//							CA(AS);
//							AS.Clear();
//							AS.AppendNumber(anotherTempList[0].typeId);
//							CA(AS);*/
//							Attr_id= anotherTempList[0].elementId;
//							Item_id = anotherTempList[0].typeId;
//					}
//				}
//			}
//		}
}


bool8  TableUtility::IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id)
{
	//int32 StoryLenght=0;

	//	InterfacePtr<IPMUnknown> unknown(tableUIDRef, IID_IUNKNOWN);
	//	UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);

	//	if (textFrameUID == kInvalidUID)
	//		return kFalse;

	//	InterfacePtr<ITextFrame> textFrame(tableUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//	if (textFrame == NULL)
	//		return kFalse;
	//		
	//	
	//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	//	if (textModel == NULL)
	//		return kFalse;


	//	InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
	//	if(!txtSelSuite)
	//	{
	//	CA("Text Selection Suite is null");
	//	return kFalse;
	//	}

	//	
	//	StoryLenght = textModel->GetPrimaryStoryThreadSpan();


	//	TextIterator begin(textModel, 0);
	//	TextIterator end(textModel, StoryLenght);

	//	int32 j=0,start=0;
	//	for (TextIterator iter = begin; iter <= end && j<StoryLenght; iter++,j++)
	//	{
	//
	//		//const textchar characterCode = (*iter).GetTextChar();
	//		UTF16TextChar characterCode;
	//		int32 len=1; 
	//		(*iter).ToUTF16(&characterCode,&len);
	//		char buf;
	//		
	//		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
	//		
	//		if((buf=='\t' || buf=='\r') && j<StoryLenght)
	//		{
	//			
	//			RangeData rData(start,j);

	//			const UIDRef wrt = ::GetUIDRef(textModel);
	//		
	//			txtSelSuite->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);

	//			start=j+1;
	//			InterfacePtr<ITextAttributeSuite>textAttributeSuite2
	//			((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
	//			if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
	//			{
	//				AttributeBossList *attrs = new AttributeBossList;
	//				textAttributeSuite2->CopyToAttributeLists(attrs);
	//				if(!attrs)
	//				{
	//					CA("Attrs is Null");
	//				}

	//				InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
	//				if(!temp1Data)
	//				{
	//					//CA("temp1Data is NULL...........");
	//				}
	//				InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
	//				if(!temp2Data)
	//				{
	//					//CA("temp2Data is NULL...........");
	//				}
	//				if(temp2Data)
	//				{
	//					
	//						SlugList anotherTempList;
	//						PMString tempName;
	//						PMString tempCol;
	//						int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
	//						if(Attr_id==anotherTempList[0].elementId)
	//							return kTrue;
	//				}
	//			}
	//			
	//		}

	//	}
return kFalse;
}

bool8 TableUtility::UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col)
{
	//int32 StoryLenght=0;

	//	InterfacePtr<IPMUnknown> unknown(tableUIDRef, IID_IUNKNOWN);
	//	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);

	//	if (textFrameUID == kInvalidUID)
	//		return kFalse;

	//	InterfacePtr<ITextFrame> textFrame(tableUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//	if (textFrame == NULL)
	//		return kFalse;
	//		
	//	
	//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	//	if (textModel == NULL)
	//		return kFalse;


	//	InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
	//	if(!txtSelSuite)
	//	{
	//	CA("Text Selection Suite is null");
	//	return kFalse;
	//	}

	//	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	//	if(ptrIAppFramework == NULL)
	//		return kFalse;
	//	
	//	StoryLenght = textModel->GetPrimaryStoryThreadSpan();
	//	//PMString kool;
	//	//kool.AppendNumber(StoryLenght);
	//	//CA(kool);


	//	TextIterator begin(textModel, 0);
	//	TextIterator end(textModel, StoryLenght);

	//	int32 j=0,start=0;
	//	for (TextIterator iter = begin; iter <= end && j<StoryLenght; iter++,j++)
	//	{
	//
	//		//const textchar characterCode = (*iter).GetTextChar();
	//		UTF16TextChar characterCode;
	//		int32 len=1; 
	//		(*iter).ToUTF16(&characterCode,&len);
	//		char buf;
	//		
	//		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
	//		
	//		if((buf=='\t' || buf=='\r') && j<StoryLenght)
	//		{
	//			
	//			RangeData rData(start,j);

	//			const UIDRef wrt = ::GetUIDRef(textModel);
	//		
	//			txtSelSuite->SetTextSelection(wrt,rData,Selection::kDontScrollSelection ,NULL);

	//			//start=j+1;
	//			InterfacePtr<ITextAttributeSuite>textAttributeSuite2
	//			((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
	//			if (textAttributeSuite2 != NULL && textAttributeSuite2->CanApplyAttributes())
	//			{
	//				AttributeBossList *attrs = new AttributeBossList;
	//				textAttributeSuite2->CopyToAttributeLists(attrs);
	//				if(!attrs)
	//				{
	//					CA("Attrs is Null");
	//				}

	//				InterfacePtr<const IPMUnknown> temp1Data(attrs->QueryByClassID(kBscTAAttrBoss,IID_ISLUGDATA));
	//				if(!temp1Data)
	//				{
	//					//CA("temp1Data is NULL...........");
	//				}
	//				InterfacePtr<ISlugData> temp2Data(temp1Data,IID_ISLUGDATA);
	//				if(!temp2Data)
	//				{
	//					//CA("temp2Data is NULL...........");
	//				}
	//				if(temp2Data)
	//				{
	//					
	//						SlugList anotherTempList;
	//						PMString tempName;
	//						PMString tempCol;
	//						int16 attrState2 = temp2Data->GetList(anotherTempList, tempName, tempCol);
	//						/*PMString as;
	//						as.AppendNumber(anotherTempList[0].elementId);
	//						CA(as);
	//						as.Clear();
	//						as.AppendNumber(anotherTempList[0].typeId);
	//						CA(as);*/
	//						if(Attr_id==anotherTempList[0].elementId && anotherTempList[0].typeId!=-1)
	//						{
	//							PMString result = ptrIAppFramework->findProductCore(anotherTempList[0].typeId,Table_col.GrabCString());
	//							int32 length=result.ByteLength();

	//							if(j-start!=result.ByteLength())
	//							{
	//								WideString* myText=new WideString(result);	
	//								ErrorCode Err = textModel->Replace(kTrue,start,j-start/*(j-(start))*/, myText);
	//								//j=result.ByteLength()+start;
	//								StoryLenght = textModel->GetPrimaryStoryThreadSpan();
	//								TextIterator end1(textModel,StoryLenght);
	//								end=end1;
	//								TextIterator begin1(textModel, 0);
	//								iter=begin1;
	//								j=0,start=0;
	//								
	//							}
	//							

	//						}
	//				}
	//				start=j+1;
	//			}
	//			
	//		}

	//	}
return kFalse;
}

ErrorCode TableUtility::ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)
{
	ErrorCode status = kFailure;
	do
	{
		if (commandClass == kInvalidClass)
		{
			ASSERT_FAIL("ProcessSimpleCommand: commandClass is invalid"); break;
		}
		InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(commandClass));
		if (cmd == NULL)
		{
			ASSERT(cmd); break;
		}

		cmd->SetItemList(itemsIn);
		status = CmdUtils::ProcessCommand(cmd);

		if (status == kSuccess)
		{
			const UIDList& local_itemsOut = cmd->GetItemListReference();
			itemsOut = local_itemsOut;
		}
		else
		{
			ASSERT_FAIL("ProcessSimpleCommand: The command failed");
		}
	} while (false);
	return status;
}


ErrorCode TableUtility::TagTable(const UIDRef& tableModelUIDRef,UIDRef BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								SlugStruct& stencilInfoData,
								int32 rowno, int32 colno, double TableID)
{
	ErrorCode err = kFailure;
	static int TableCount = 0;
	static UIDRef TableUIDREF;
	static UIDRef boxUIDref;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		return err;
	}

	do 
	{
		
		RowRange rowRange(rowno, 0);
		ColRange colRange(colno,0);
		GridArea gridArea(rowRange, colRange);
		RowRange rRange;
		rRange=gridArea.GetRows();

		ColRange cRange;
		cRange=gridArea.GetCols();

		InterfacePtr<ITableModel> tableModel(tableModelUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			ptrIAppFramework->LogError("tableModel == NULL");
			break;
		}
		InterfacePtr<ITableTextContent>tblTextContent(tableModel,UseDefaultIID());
		if(tblTextContent == NULL)
		{
			ptrIAppFramework->LogError("tblTextContent == NULL");
			return kFailure ;
		}

		InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel());
		if(tblTextModel == NULL)
		{
			ptrIAppFramework->LogError("tblTextModel == NULL");
			return kFailure;
		}
		UIDRef ref2(::GetUIDRef(tblTextModel));
				
		GridAddress gadr(rowno,colno);
		//script_related_change test code
		InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gadr));
		if(cellContent == NULL)
		{
			//CA("cellContent == NULL");
				return kFailure;
		}
		InterfacePtr<ITextStoryThread> iTextStoryThread(cellContent,UseDefaultIID());
		if(iTextStoryThread == NULL)
		{
			//CA("iTextStoryThread == NULL");
			return kFailure;
		}
		int32 firstCharIndex,lastCharIndex;
		firstCharIndex = iTextStoryThread->GetTextStart();
		lastCharIndex = iTextStoryThread->GetTextEnd();
		
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if(textFrameUID==kInvalidUID)
		{	
			this->convertBoxToTextBox(BoxRef);
			
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxRef, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if(textFrameUID == kInvalidUID)
			{
				//CA("textFrameUID == kInvalidUID");
				break;
			}			
		}
		

		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy");
			ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::TagTable::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy");
			ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::TagTable::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("!multiColumnItemTextFrame");
			ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::TagTable::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("!frameItemHierarchy");
			ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::TagTable::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::TagTable::!textFrame");
			break;
		}
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL){
			//CA("Err: invalid interface pointer ITextModel");
			break;
		}
		
		RangeData range(firstCharIndex,firstCharIndex,RangeData::kLeanBack); 

		
        GridAddress grd(rowno, colno);
        GridID id=tableModel->GetGridID(grd);

        UIDRef textRef(::GetUIDRef(textModel));
        err=TagStory(tableTagName,textRef, BoxRef);

        if(boxUIDref !=BoxRef)
        {
            TableCount = 0;
            boxUIDref = BoxRef;
        }
        PMString CellTagName("PSCell");
        XMLReference xmlRef;
        if(!IsDBTable)
            err=AddTableAndCellElements(tableModelUIDRef, BoxRef, tableTagName, "PSCell", xmlRef, TableID, stencilInfoData);
        else
        {
            err=AddTableAndCellElements(tableModelUIDRef, BoxRef, cellTagName, "PSCell", xmlRef, TableID, stencilInfoData);
        }

        XMLReference CellTextXmlRef;
        err=TagTableCellText(tableModel,xmlRef, id, gridArea, CellTextXmlRef, stencilInfoData);
        this->attachAttributes(&CellTextXmlRef, kFalse, stencilInfoData, rowno, colno);

	} while(kFalse);
	return err;
}



ErrorCode TableUtility::TagStory(const PMString& tagName,
									const UIDRef& textModelUIDRef, const UIDRef& boxRef)
{
	ErrorCode err = kFailure;
	do {
		IDataBase* database = textModelUIDRef.GetDataBase();
		UIDRef documentUIDRef(database, database->GetRootUID());
		if(textModelUIDRef.GetDataBase()==NULL)
		{
			//CA("TagStory---Invalid uid of UIDRef of text story");
		}

		InterfacePtr<ITextModel> textModel(textModelUIDRef, UseDefaultIID());
		ASSERT(textModel);
		if(!textModel) {
			//CA("Text Model is null in tagging story");
			break;
		}
		InterfacePtr<IIDXMLElement> rootXMLElement(Utils<IXMLUtils>()->QueryRootElement(database));
		ASSERT(rootXMLElement);
		if(!rootXMLElement) {
			//CA("Root xmlElement is null");
			break;
		}
		XMLReference rootXMLReference = rootXMLElement->GetXMLReference();
		int32 indexInParent =  rootXMLElement->GetChildCount();
		if(indexInParent < 0) {
			indexInParent = 0;
		} 
		bool16 ISTagExist = kFalse;
		UIDRef tagUIDRef = this->AcquireTag(boxRef, tagName, ISTagExist);
		if(!ISTagExist)
		{
			XMLReference PSXmlRef;
			err = Utils<IXMLElementCommands>()->CreateElement(tagUIDRef.GetUID(), 
				textModelUIDRef.GetUID(),
				rootXMLReference,
				indexInParent,
				&PSXmlRef
				);

			SlugStruct stl; 
			this->attachAttributes(&PSXmlRef, kTrue, stl, -1, -1);
		}
		
	} while(kFalse);
	return err;
}

ErrorCode TableUtility::AddTableAndCellElements(const UIDRef& tableModelUIDRef, const UIDRef& BoxUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								double TableID, 
								SlugStruct& stencilInfoData
								)
{
	ErrorCode err = kFailure;
	
	do {
		IDataBase* database = tableModelUIDRef.GetDataBase();
		UIDRef documentUIDRef1(database, database->GetRootUID());

		bool16 ISTagPresent = kFalse;
		UIDRef tablTagUIDRef = this->AcquireTag(BoxUIDRef, tableTagName, ISTagPresent);
		
		if(!ISTagPresent)
		{
			err = Utils<IXMLElementCommands>()->CreateTableElement ( WideString(tableTagName),
																			 WideString(cellTagName),
																			tableModelUIDRef,
																			&outCreatedXMLReference);
			if(err != kFailure)
			{
				SlugStruct stencileTableData;
				stencileTableData.elementName = tableTagName;
				stencileTableData.parentId = pNode.getPubId();
                stencileTableData.elementId = -1; //stencilInfoData.elementId;
				stencileTableData.whichTab =stencilInfoData.whichTab;

				stencileTableData.imgFlag = CurrentSectionID;
				stencileTableData.isAutoResize = stencilInfoData.isAutoResize;
				stencileTableData.parentTypeId = pNode.getTypeId();
				if(global_project_level == 3)
					stencileTableData.sectionID = CurrentSubSectionID;
				if(global_project_level == 2)
					stencileTableData.sectionID = CurrentSectionID;
				stencileTableData.LanguageID = stencilInfoData.LanguageID;
				stencileTableData.typeId = stencilInfoData.tableTypeId;
				stencileTableData.col_no = stencilInfoData.col_no;

				stencileTableData.dataType = stencilInfoData.dataType;
				stencileTableData.header = stencilInfoData.header;
				stencileTableData.pbObjectId = stencilInfoData.pbObjectId;
				stencileTableData.tableType = stencilInfoData.tableType;
				stencileTableData.tableId = stencilInfoData.tableId;
				stencileTableData.field1 = stencilInfoData.field1;
				stencileTableData.field2 = stencilInfoData.field2;
				stencileTableData.field3 = stencilInfoData.field3;
				stencileTableData.field4 = stencilInfoData.field4;
				stencileTableData.field5 = stencilInfoData.field5;
                stencileTableData.groupKey = stencilInfoData.groupKey;
				this->attachAttributes(&outCreatedXMLReference, kFalse, stencileTableData, -1, stencileTableData.col_no,TableFlagForStandardProductTable);
			}
		
		}
		else

		{
			//CA("Table Tag Already present Getting the outCreatedXMLReference");
			InterfacePtr<ITableModel> tableModel(tableModelUIDRef, UseDefaultIID());
			if(!tableModel) { //CA("!tableModel");
				break;
			}
			InterfacePtr<IXMLReferenceData> xmlReferenceData(tableModelUIDRef, UseDefaultIID());
			// Hmmm... we can get a table target but no tagged table,
			// so let's not assert here
			if(!xmlReferenceData) { //CA("!xmlReferenceData");
				break;
			}
			outCreatedXMLReference = xmlReferenceData->GetReference();
			//CA("return outCreatedXMLReference");

		}
		
	} while(kFalse);
	return err;
}

ErrorCode TableUtility::TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, SlugStruct& stencilInfoData)
 {     
		ErrorCode errCode=kFailure;
        UIDRef TableUIDRef(::GetUIDRef(tableModel));
		InterfacePtr<ITableTextContent>tblTextContent(tableModel,UseDefaultIID());
		if(!tblTextContent)
		{
			//CA("Source ITableTextContent in function is null");
			return kFailure ;
		}
		InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel()/*,UseDefaultIID()*/); //Rahul
		if(!tblTextModel)
		{
			//CA("Source Text Model in fucntion is null");
			return kFailure;
		}
		UIDRef ref2(::GetUIDRef(tblTextModel));

		RowRange rRange;
		rRange=gridArea.GetRows();
		ColRange cRange;
		cRange=gridArea.GetCols();
		
		GridAddress gadr(rRange.start,cRange.start);

		InterfacePtr<ICellContent> iCellContent(tableModel->QueryCellContentBoss(gadr));
		if(iCellContent == NULL)
		{
			CA("iCellContent == NULL");
			return kFailure;
		}
		InterfacePtr<IXMLReferenceData> iCellXMLRefData(iCellContent,UseDefaultIID());
		if(iCellXMLRefData == NULL)
		{
			CA("iCellXMLRefData == NULL");
			return kFailure;
		}
		InterfacePtr<IIDXMLElement> iCellXMLElement(iCellXMLRefData->GetReference().Instantiate());
		if(iCellXMLElement == NULL)
		{
			CA("iCellXMLElement == NULL");
			return kFailure;
		}
		
		InterfacePtr<ITextStoryThread>iTextStoryThread(iCellContent,UseDefaultIID());
		if(iTextStoryThread == NULL)
		{
			CA("iTextStoryThread == NULL");
			return kFailure;
		}
		

		int32 firstCharIndex,lastCharIndex;
		firstCharIndex = iTextStoryThread->GetTextStart();
		lastCharIndex  = iTextStoryThread->GetTextEnd();

		InterfacePtr<ITextModel>cellTextModel(iTextStoryThread->QueryTextModel());
		if(cellTextModel == NULL)
		{
			CA("cellTextModel == NULL");
			return kFailure;
		}
		UIDRef cellTextModelUIDRef(::GetUIDRef(cellTextModel));
		UIDRef tableTextModelUIDRef(::GetUIDRef(tblTextModel));
		errCode = kFailure;
		errCode=Utils<IXMLElementCommands>()->CreateElement(WideString(stencilInfoData.TagName),tableTextModelUIDRef, firstCharIndex, lastCharIndex -1,iCellXMLRefData->GetReference(), &CellTextxmlRef);

	 return errCode;
 }



 void TableUtility::attachAttributes
(XMLReference* newTag, bool16 IsPRINTsourceRootTag, SlugStruct& stencilInfoData, int rowno, int colno,int ProductStandardTableCase)
{
	bool16 flag=IsPRINTsourceRootTag;
	PMString attribName("ID");
	PMString attribVal("");
	attribVal.AppendNumber(PMReal(stencilInfoData.elementId));
	if(flag){
		attribVal="-1";
	} 	
	XMLReference newTag1;
	ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	
	attribName.Clear();
	attribVal.Clear();
	attribName="typeId";
	attribVal.AppendNumber(PMReal(stencilInfoData.typeId));
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName="header";
	attribVal.AppendNumber(stencilInfoData.header);
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="isEventField";
	attribVal.AppendNumber(stencilInfoData.isEventField);
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="deleteIfEmpty";
	attribVal.AppendNumber(stencilInfoData.deleteIfEmpty);
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="dataType";
	attribVal.AppendNumber(stencilInfoData.dataType);
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="isAutoResize";
	attribVal.AppendNumber(stencilInfoData.isAutoResize);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "LanguageID";	
	attribVal.AppendNumber(PMReal(stencilInfoData.LanguageID));
	if(flag){
		attribVal="NULL";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName="index";
	if(ProductStandardTableCase == 0)
	{
		attribVal.AppendNumber(stencilInfoData.whichTab);
		if(flag){
			attribVal="-1";
		}
	}
	if(ProductStandardTableCase == 1) ////this is also used in hybrid table for Product
	{
		attribVal="3";
	}
	if(ProductStandardTableCase == 2) //this is used in hybrid table for Item
	{
		attribVal="4";
	}
	if(ProductStandardTableCase == 3) //this is used in hybrid table for Item
	{
		attribVal="5";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "pbObjectId";	
	attribVal.AppendNumber(PMReal(stencilInfoData.pbObjectId));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="parentID";
	attribVal.AppendNumber(PMReal(stencilInfoData.parentId));
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="childId";
	attribVal.AppendNumber(PMReal(stencilInfoData.childId));
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="sectionID";
	attribVal.AppendNumber(PMReal(stencilInfoData.sectionID));
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="parentTypeID";
	attribVal="";
	attribVal.AppendNumber(PMReal(stencilInfoData.parentTypeId));
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="isSprayItemPerFrame";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.isSprayItemPerFrame);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="catLevel";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.catLevel);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="imgFlag";
	if(stencilInfoData.imgFlag == 1)
		attribVal.AppendNumber(1);
	else
		attribVal.AppendNumber(0);
	if(flag){
		attribVal="-1";
	} 	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="imageIndex";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.imageIndex);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="flowDir";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.flowDir);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="childTag";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.childTag);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="tableFlag";
	if(ProductStandardTableCase == 0)
	{
		attribVal.AppendNumber(0);
		if(flag){
			attribVal="-1";
		}
	}
	if(ProductStandardTableCase == 1 || ProductStandardTableCase == 2 || ProductStandardTableCase == 3)
	{
		attribVal="1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));


	attribName.Clear();
	attribVal.Clear();
	attribName="tableType";
	attribVal="";
	attribVal.AppendNumber(stencilInfoData.tableType);
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName="tableId";
	attribVal="";
	attribVal.AppendNumber(PMReal(stencilInfoData.tableId));
	if(flag){
		attribVal="-1";
	}	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "rowno";
	attribVal.AppendNumber(PMReal(rowno));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "colno";
	attribVal.AppendNumber(PMReal(colno));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field1";
	attribVal.AppendNumber(PMReal(stencilInfoData.field1));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field2";
	attribVal.AppendNumber(PMReal(stencilInfoData.field2));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "field3";
	attribVal.AppendNumber(PMReal(stencilInfoData.field3));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "field4";
	attribVal.AppendNumber(PMReal(stencilInfoData.field4));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "field5";
	attribVal.AppendNumber(PMReal(stencilInfoData.field5));
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "groupKey";
    attribVal.Append(stencilInfoData.groupKey);
    if(flag){
        attribVal="";
    }
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

}

UIDRef TableUtility::AcquireTag(const UIDRef& boxUIDRef, 
									 const PMString& tagName, bool16 &ISTagPresent
									 )
{
	UIDRef retval;
	do {
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		UIDRef ref = boxUIDRef;
	
		InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::AcquireTag::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::AcquireTag::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::AcquireTag::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::AcquireTag::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::AcquireTag::!textFrame");
			break;
		}
				
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == NULL){//CA("objTxtMdl == NULL");
			break;
		}
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
	
		InterfacePtr<IXMLReferenceData> objXMLRefDat((IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA));
		if (objXMLRefDat==NULL){//CA("objXMLRefDat==NULL");
			break;
		}
		XMLReference xmlRef=objXMLRefDat->GetReference();
		UIDRef refUID=xmlRef.GetUIDRef();	
		UID existingTagUID = kInvalidUID;

		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement!=NULL)
		{ 
			int elementCount=xmlElement->GetChildCount();
			PMString ORgTagString = xmlElement->GetTagString();
			
			if(tagName == ORgTagString)
            {
                existingTagUID = xmlElement->GetTagUID();
			}
			else
			{
				for(int i=0; i<elementCount; i++)
				{
					XMLReference elementXMLref=xmlElement->GetNthChild(i);
					InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
					if(childElement==NULL)
						continue;
			
					PMString ChildtagName=childElement->GetTagString();
					if(tagName == ChildtagName)
                    {
						existingTagUID = childElement->GetTagUID();
						break;
					}

				}
			}
		}
        
		InterfacePtr<IXMLTagList> tagList(Utils<IXMLUtils>()->QueryXMLTagList(txtMdlUIDRef.GetDataBase()));
		ASSERT(tagList);
		if(!tagList)
        {
			break;
		}

		if(existingTagUID == kInvalidUID) {
			UID createdTagUID = kInvalidUID;
			ErrorCode err = Utils<IXMLTagCommands>()->CreateTag (::GetUIDRef(tagList), 
															WideString(tagName),
															kInvalidUID, 
															&createdTagUID);
			if(err != kFailure){
				//CA("Create Tag Succesful");
			}
			ASSERT(err == kSuccess);
			ASSERT(createdTagUID != kInvalidUID);
			retval = UIDRef(::GetDataBase(tagList), createdTagUID);
			ISTagPresent = kFalse;
		} else
        {
			ISTagPresent = kTrue;
			retval = UIDRef(::GetDataBase(tagList), existingTagUID);
		}

	} while(kFalse);

	return retval;
}

int16 TableUtility::convertBoxToTextBox(UIDRef boxUIDRef)
{
    InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
    if(!command)
        return 0;
    command->SetItemList(UIDList(boxUIDRef));
    if(CmdUtils::ProcessCommand(command)!=kSuccess)
    {
        //CA("processcommand not success in convertBoxToTextBox Function");
        return 0;
    }
	return 1;
}

int TableUtility::changeMode(int whichMode)
{
	int status=kFailure;
	if(whichMode==DRAGMODE)
	{
		InterfacePtr<ICommand> setToolCmd(CmdUtils::CreateCommand(kSetToolCmdBoss));
		if(!setToolCmd)
			return kFailure;

		InterfacePtr<IToolCmdData> setToolCmdData(setToolCmd, UseDefaultIID());
		if(!setToolCmdData)
			return kFailure;

		InterfacePtr<ITool> textTool(Utils<IToolBoxUtils>()->QueryTool(kPointerToolBoss));
		ASSERT(textTool);
		if (!textTool) 
		{
			return kFailure;
		}
		if (Utils<IToolBoxUtils>()->SetActiveTool(textTool) == kFalse) 
		{
			ASSERT_FAIL("IToolBoxUtils::SetActiveTool failed to change to kIBeamToolBoss");
			return kFailure;
		}
		
		setToolCmdData->Set(textTool, textTool->GetToolType());
		status=CmdUtils::ProcessCommand(setToolCmd);
	}
	else if(whichMode==CONTENTSMODE)
	{
		InterfacePtr<ICommand> setToolCmd(CmdUtils::CreateCommand(kSetToolCmdBoss));
		if(!setToolCmd)
			return kFailure;

		InterfacePtr<IToolCmdData> setToolCmdData(setToolCmd, UseDefaultIID());
		if(!setToolCmdData)
			return kFailure;

		InterfacePtr<ITool> textTool(Utils<IToolBoxUtils>()->QueryTool(kIBeamToolBoss));
		ASSERT(textTool);
		if (!textTool) 
		{
			return kFailure;
		}
		if (Utils<IToolBoxUtils>()->SetActiveTool(textTool) == kFalse) 
		{
			//ASSERT_FAIL("IToolBoxUtils::SetActiveTool failed to change to kIBeamToolBoss");
			return kFailure;
		}

		setToolCmdData->Set(textTool, textTool->GetToolType());
		status=CmdUtils::ProcessCommand(setToolCmd);
	}
	return 1;
}

ErrorCode TableUtility::SprayUserTableAsPerCellTagsForOneItem(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return errcode;
		}

		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: itagReader == NULL");
			return errcode;
		}

		if(!itagReader->GetUpdatedTag(slugInfo))
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: !itagReader->GetUpdatedTag(slugInfo)");
			return errcode;
		}
		 	
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForOneItem::!textFrame");
			break;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: textModel ==NULL");
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: tableList == NULL");
			break;
		}
		int32	tableIndex = tableList->GetModelCount();
		
		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: tableIndex < 0");
			break;
		}
		for(int i = 0; i<tableIndex; i++)
		{
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(i));
			if(tableModel == NULL) 
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem: scripting :: tableModel == NULL");
				break;
			}
			UIDRef tableRef(::GetUIDRef(tableModel));
			
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem: Err: invalid interface pointer ITableCommands");
				break;
			}

			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
			if(contentRef.IsTable()) {
				//CA("Table Content");
			} else if(contentRef.IsTableCell()) {
				//CA("Table Cell");
			} 

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//CA("ContentRef != tableRef");	
				continue;
			}

/////////////====================================////////////////////
			
			int32 OriginalRowNo = tableModel->GetTotalRows().count;
			int32 OriginalColNo = tableModel->GetTotalCols().count;

		VectorScreenTableInfoPtr tableInfo = NULL;


		
		if(pNode.getIsONEsource())
		{
			// For ONEsource
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		}else
		{
			//For publication
			tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSectionID, pNode.getPubId(), slugInfo.languageID, kFalse);
		}
			
		if(!tableInfo)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForOneItem:  tableinfo is NULL");
				break;
			}

			if(tableInfo->size()==0){// CA(" tableInfo->size()==0");
				break;
			}
					
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items, FinalItemIds;

			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				oTableValue = *it;
				
				vec_items = oTableValue.getItemIds();
				if(FinalItemIds.size() == 0)
				{
					FinalItemIds = vec_items;
				}
				else
				{
					for(int32 i=0; i<vec_items.size(); i++)
					{	bool16 Flag = kFalse;
						for(int32 j=0; j<FinalItemIds.size(); j++)
						{
							if(vec_items[i] == FinalItemIds[j])
							{
								Flag = kTrue;
								break;
							}				
						}
						if(!Flag)
							FinalItemIds.push_back(vec_items[i]);
					}

				}
			}

			bool16 IsMoreThan1Item= kFalse;
			int32 TotalNoOfItems = static_cast<double>(FinalItemIds.size());

			if(FinalItemIds.size()>1)
			{ 

				TableFlagForStandardProductTable = 1; // Since we are spraying Product Table

				InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection());
				if(!iSelectionManager)
				{
					//CA("Source Selection Manager is null");
					break;
				}

				if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection))
				{
					// Clear the selection
					iSelectionManager->DeselectAll(nil);
				}

				int32 TotalRowNos = 0;
				for(int32 i=1; i<FinalItemIds.size(); i++)
				{	
					IsMoreThan1Item = kTrue;
					TotalRowNos = OriginalRowNo;
					ErrorCode result = tableCommands->InsertRows(RowRange(TotalRowNos-1, OriginalRowNo), Tables::eAfter, 0);
					if(result)
					{
						
					}
					
					InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
					if(!pTextSel)
					{
						//CA("Source IConcrete Selection is null");
						return kFailure;
					}
					
					InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
					if(!tblTxtSel)
					{
						//CA(" Source Table Text Selection is null");
						return kFailure;
					}
					GridAddress gadr(0,0);
					//tblTxtSel->AddRef();
					tblTxtSel->SelectTextInCell(tableModel,gadr);

					RowRange rRange(0,OriginalRowNo);
					ColRange cRange(0, OriginalColNo);
					GridArea gridArea(rRange, cRange);

					InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));
					if(!tblSelSuite)
					{
						//CA(" Source Table Selection suite is null");
						return kFailure;
					}
                    
                    tblSelSuite->DeselectAll();
					tblSelSuite->Select(tableModel,gridArea,tblSelSuite->kAddTo,kTrue);

					bool16 CanCopy = tableModel->CanCopy(gridArea);
					if(CanCopy)
					{
						TableMemento * tableMomento = tableModel->Copy(gridArea);
						
						RowRange rRange1(TotalRowNos-1,OriginalRowNo);
						ColRange cRange1(0,OriginalColNo);
						GridArea gridArea1(rRange1, cRange1);
						tblSelSuite->DeselectAll();
						tblSelSuite->Select(tableModel,gridArea1,tblSelSuite->kAddTo,kTrue);
			
						GridAddress grd(TotalRowNos,0);
						tableModel->Paste(grd,ITableModel::eAll,tableMomento);
						tblSelSuite->DeselectAll();
						
					}
					if(result)
						TotalRowNos+= OriginalRowNo;
				}
			}


			InterfacePtr<IXMLReferenceData>xmlRefData(contentRef.Instantiate());
			XMLReference xmlRef=xmlRefData->GetReference();
			InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
			int elementCount=xmlElement->GetChildCount();
			PMString ORgTagString = xmlElement->GetTagString();

			{
				int32 ElementCountForSingleItem = elementCount/TotalNoOfItems;
				int32 LastCount =0;
				int32 LastRowCount =0;
				
				for(int32 i3 =0; i3<TotalNoOfItems; i3++)
				{	
					bool16 refreshItemFlag = kTrue;
					for(int i4=0; i4<ElementCountForSingleItem; i4++)
					{	
						XMLReference elementXMLref=xmlElement->GetNthChild(i4+ LastCount);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
						if(childElement==NULL)
							continue;						
						
						PMString ChildtagName=childElement->GetTagString();
						int elementCount1=childElement->GetChildCount();
						for(int i2=0; i2<elementCount1; i2++)
						{
							XMLReference elementXMLref1=childElement->GetNthChild(i2);
							InterfacePtr<IIDXMLElement>childElement1(elementXMLref1.Instantiate());
							if(childElement1==NULL)
								continue;
							PMString ChileEleName = childElement1->GetTagString();
							
							TagStruct tInfo;
							int32 attribCount=childElement1->GetAttributeCount();
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childElement1, &sIndex, &eIndex);
							
							tInfo.startIndex=sIndex;
							tInfo.endIndex=eIndex;
							tInfo.tagPtr=childElement;
							
							for(int j=0; j<attribCount; j++)
							{
								PMString attribName=childElement1->GetAttributeNameAt(j);
								PMString attribVal=childElement1->GetAttributeValue(WideString(attribName));
								itagReader->getCorrespondingTagAttributes(attribName, attribVal, tInfo);
							}
							tInfo.curBoxUIDRef=boxUIDRef;					

							PMString result("");
							if(childElement1->GetAttributeValue(WideString("isEventField")) == WideString("1"))
							{
								//CA("tagInfo.isEventField");
								/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(FinalItemIds[0],tInfo.elementId,CurrentSectionID);
								if(vecPtr != NULL)
									if(vecPtr->size()> 0)
										result = vecPtr->at(0).getObjectValue();*/	

								//CA("itemAttributeValue = " + itemAttributeValue);
							}
							else if(tInfo.elementId == -401 || tInfo.elementId == -402 || tInfo.elementId == -403){
								result = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(FinalItemIds[0],tInfo.elementId, tInfo.languageID);
							}
							else{
								result = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(FinalItemIds[0],tInfo.elementId, tInfo.languageID, CurrentSectionID, refreshItemFlag);
							}
							refreshItemFlag = kFalse;
							if(result == ""){ //CA("result is Empty");
								/*result.Append("N/A");*/
							}
                            result.ParseForEmbeddedCharacters();
							const WideString wStr(result);
							tableCommands->SetCellText(wStr, GridAddress(tInfo.rowno + LastRowCount ,tInfo.colno));
							SlugStruct stencileInfo;
							stencileInfo.elementId =tInfo.elementId;

							PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tInfo.elementId, tInfo.languageID );
							stencileInfo.elementName = dispname;
							PMString TableColName("");

							TableColName.Append("ATTRID_");
							TableColName.AppendNumber(PMReal(tInfo.elementId));
							stencileInfo.TagName = TableColName;
							stencileInfo.typeId  = -1;
							stencileInfo.childId =  FinalItemIds[i3];
							stencileInfo.parentId = pNode.getPubId();
							stencileInfo.parentTypeId = CurrentObjectTypeID;
							stencileInfo.whichTab = tInfo.whichTab;

							stencileInfo.imgFlag = CurrentSectionID; 
							stencileInfo.isAutoResize = tInfo.isAutoResize;
							stencileInfo.LanguageID = tInfo.languageID;
							stencileInfo.tableTypeId = tInfo.typeId;
							XMLReference txmlref;
							this->TagTable(tableRef,boxUIDRef,xmlElement->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, tInfo.rowno + LastRowCount, tInfo.colno, 0 );
						}
					}
					LastCount+= ElementCountForSingleItem;
					LastRowCount+= OriginalRowNo;
				}
			}
		}
	}while(0);

	return errcode;
}

// Method Purpose: Used to Spray User/Custom Tables with only Item Attribute ids, NO Header/DB Tables. Made specially for Lazboy  
//Can Spray tables with multiple items for each item it creates a new Table in the same text frame.
ErrorCode TableUtility::SprayUserTableAsPerCellTagsForMultipleItem(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	bool16 isTableSizeZero = kFalse;
	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return errcode;
		}
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: scripting :: itagReader == NULL");
			return errcode;
		}

		if(!itagReader->GetUpdatedTag(slugInfo))
			return errcode;

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		
		if (textFrameUID == kInvalidUID)
		{//CA("textFrameUID == kInvalidUID");
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!textFrame");
			break;
		}
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: textModel == NULL");
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: tableList == NULL");
			break;
		}
		int32	tableIndex = tableList->GetModelCount()/* - 1*/;

		
		
		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;
		}
				
		for(int i = 0; i<tableIndex; i++)
		{
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(i));
			if(tableModel == NULL) 
			{
				//CA("tableModel == NULL");
				continue;
			}

			UIDRef tableRef(::GetUIDRef(tableModel));
				
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: invalid interface pointer ITableCommands");
				break;
			}

			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
			if(contentRef.IsTable()) {
				//CA("Table Content");
			} else if(contentRef.IsTableCell()) {
				//CA("Table Cell");
			} 

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//CA("ContentRef != tableRef");	
				continue;
			}
			
			int32 OriginalRowNo = tableModel->GetTotalRows().count;
			int32 OriginalColNo = tableModel->GetTotalCols().count;

			PMRect theArea;
			PMReal rel;			
			int32 wt;
			InterfacePtr<IGeometry> iGeometry(boxUIDRef, UseDefaultIID());
			if(iGeometry==NULL)
				break;

			theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
			rel=theArea.Width();	
			wt=::ToInt32(rel);
			int32 colWidth = wt/OriginalColNo;
			int32 rowHeight = 10;

			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;

			
		VectorScreenTableInfoPtr tableInfo = NULL;
		
		if(pNode.getIsONEsource())
		{
			// For ONEsource
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		}else
		{
			//For publication
			tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), slugInfo.languageID, kFalse );
		}
			if(!tableInfo)
			{
				isTableSizeZero = kTrue; 
				//CA("tableinfo is NULL");
				break;
			}

			if(tableInfo->size()==0){ 
				//CA(" tableInfo->size()==0");
				isTableSizeZero = kTrue;
				break;
			}
					
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();

			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{				
				oTableValue = *it;				
				vec_items = oTableValue.getItemIds();
			
				if(FinalItemIds.size() == 0)
				{
					FinalItemIds = vec_items;
				}
				else
				{
					for(int32 i=0; i<vec_items.size(); i++)
					{	bool16 Flag = kFalse;
						for(int32 j=0; j<FinalItemIds.size(); j++)
						{
							if(vec_items[i] == FinalItemIds[j])
							{//CA("Flag = kTrue");
								Flag = kTrue;
								break;
							}				
						}
						if(!Flag)
							FinalItemIds.push_back(vec_items[i]);
					}

				}
			}

			if(tableInfo)
				delete tableInfo;
	
			bool16 IsMoreThan1Item= kFalse;
			int32 TotalNoOfItems =static_cast<double>(FinalItemIds.size());

			if(FinalItemIds.size()>1)
			{ 
				InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection());
				if(!iSelectionManager)
				{
					//CA("Source Selection Manager is null");
					break;
				}			
				if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection))
				{
					// Clear the selection
					iSelectionManager->DeselectAll(nil);
				}
					
				int32 TotalRowNos = 0;				
				IsMoreThan1Item = kTrue;
				TotalRowNos = OriginalRowNo;					
					
				InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
				if(!pTextSel)
				{
					//CA("Source IConcrete Selection is null");
					break;
				}
					
				InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
				if(!tblTxtSel)
				{
					//CA(" Source Table Text Selection is null");
					break;
				}
				GridAddress gadr(0,0);
				tblTxtSel->SelectTextInCell(tableModel,gadr);

				RowRange rRange(0,OriginalRowNo);
				ColRange cRange(0, OriginalColNo);
				GridArea gridArea(rRange, cRange);

				InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));
				if(!tblSelSuite)
				{
					//CA(" Source Table Selection suite is null");
					break;
				}

				tblSelSuite->DeselectAll();				
				tblSelSuite->Select(tableModel,gridArea,tblSelSuite->kAddTo,kTrue);

				UID textFrameUID = kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
				if (graphicFrameDataOne) 
				{
					textFrameUID = graphicFrameDataOne->GetTextContentUID();
				}

				if(textFrameUID==kInvalidUID)
				{
					this->convertBoxToTextBox(boxUIDRef);
					UID textFrameUID = kInvalidUID;
					InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
					if (graphicFrameDataOne) 
					{
						textFrameUID = graphicFrameDataOne->GetTextContentUID();
					}

					if(textFrameUID == kInvalidUID)
					{
						break;
					}			
				}
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA(graphicFrameHierarchy);
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!graphicFrameHierarchy");
					break;
				}
								
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA(multiColumnItemHierarchy);
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!multiColumnItemHierarchy");
					break;
				}

				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					//CA(!multiColumnItemTextFrame);
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!multiColumnItemTextFrame");
					//CA("Its Not MultiColumn");
					break;
				}
				
				InterfacePtr<IHierarchy>
				frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
				if (!frameItemHierarchy) {
					//CA(“!frameItemHierarchy”);
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!frameItemHierarchy");
					break;
				}

				InterfacePtr<ITextFrameColumn>
				textFrame(frameItemHierarchy, UseDefaultIID());
				if (!textFrame) {
					//CA("!!!ITextFrameColumn");
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem::!textFrame");
					break;
				}
				InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
				if (textModel == NULL)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: Err: invalid interface pointer ITextModel");
					break;
				}

				UIDRef SourceUIDRef = ::GetUIDRef(textModel);
				this->changeMode(CONTENTSMODE); //CONTENTSMODE

				int32 EndIndex = textFrame->TextSpan()-1;
                this->CopyTables(SourceUIDRef, EndIndex,(int32) FinalItemIds.size()-1);

			}			
		}

		InterfacePtr<ITableModelList> NewtableList(textModel, UseDefaultIID());
		if(NewtableList==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: NewtableList == NULL");
			break;
		}
		int32	NewtableIndex = NewtableList->GetModelCount();

		TagList NewTagList;
		NewTagList = itagReader->getTagsFromBox(boxUIDRef);

		if(NewTagList.size()>0)
		{
			for(int32 i=0; i< NewTagList.size(); i++)
			{
				IIDXMLElement* XMLElementPtr =  NewTagList[i].tagPtr;
				for(int i = 0; i<NewtableIndex; i++)
				{
					bool16 refreshItemFlag = kTrue; 
					InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(i));
					if(tableModel == NULL) {
						break;
					}
					UIDRef tableRef(::GetUIDRef(tableModel));	
				
					XMLContentReference contentRef = XMLElementPtr->GetContentReference();
					if(contentRef.IsTable()) {
						//CA("Table Content");
					} else if(contentRef.IsTableCell()) {
						//CA("Table Cell");
					} 

					UIDRef ContentRef = contentRef.GetUIDRef();
					if( ContentRef != tableRef)
					{
						//CA("ContentRef != tableRef");	
						continue;
					}
				
					InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
					if(tableCommands==NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayUserTableAsPerCellTagsForMultipleItem: Err: invalid interface pointer ITableCommands");
						break;
					}

					InterfacePtr<IXMLReferenceData>xmlRefData(contentRef.Instantiate());
					XMLReference xmlRef=xmlRefData->GetReference();
					InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
					int elementCount=xmlElement->GetChildCount();
					PMString ORgTagString = xmlElement->GetTagString();
									
					for(int i4=0; i4<elementCount; i4++)
					{	
						XMLReference elementXMLref=xmlElement->GetNthChild(i4);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
						if(childElement==NULL)
							continue;						
						
						PMString ChildtagName=childElement->GetTagString();

						GridAddress SelectedCell;
						XMLContentReference contentRef = childElement->GetContentReference();
						if(contentRef.IsTableCell())
						{							
							SelectedCell = contentRef.GetGridAddress(tableModel); 							
						}

						int elementCount1=childElement->GetChildCount();
				
						for(int i2=0; i2<elementCount1; i2++)
						{						
							XMLReference elementXMLref1=childElement->GetNthChild(i2);
							InterfacePtr<IIDXMLElement>childElement1(elementXMLref1.Instantiate());
							if(childElement1==NULL){
								continue;
							}
							PMString ChileEleName = childElement1->GetTagString();
				
							TagStruct tInfo;
							int32 attribCount=childElement1->GetAttributeCount();
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childElement1, &sIndex, &eIndex);
							
							tInfo.startIndex=sIndex;
							tInfo.endIndex=eIndex;
							tInfo.tagPtr=childElement;
					
							for(int j=0; j<attribCount; j++)
							{	
								PMString attribName=childElement1->GetAttributeNameAt(j);
								PMString attribVal=childElement1->GetAttributeValue(WideString(attribName));
								itagReader->getCorrespondingTagAttributes(attribName, attribVal, tInfo);
							}
							tInfo.curBoxUIDRef=boxUIDRef;
							GridAddress CurrentCell(tInfo.rowno, tInfo.colno) ;

							PMString result("");
							if(!isTableSizeZero)
							{
								if(childElement1->GetAttributeValue(WideString("isEventField")) == WideString("1"))
								{
									//CA("tagInfo.isEventField");
									/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(FinalItemIds[0],tInfo.elementId,CurrentSectionID);
									if(vecPtr != NULL)
										if(vecPtr->size()> 0)
											result = vecPtr->at(0).getObjectValue();*/	

									//CA("itemAttributeValue = " + itemAttributeValue);
								}
								else if(tInfo.elementId == -401 || tInfo.elementId == -402 || tInfo.elementId == -403){
									result = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(FinalItemIds[i],tInfo.elementId, tInfo.languageID);
								}
								else{
									result = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(FinalItemIds[i],tInfo.elementId, tInfo.languageID, CurrentSectionID, refreshItemFlag );
								}
							}
							refreshItemFlag = kFalse;
                            
                            result.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertText(new WideString(result));
							ReplaceText(textModel,sIndex+1,(eIndex) -(sIndex+1),insertText);
							
							SlugStruct stencileInfo;
							stencileInfo.elementId =tInfo.elementId;
							
							PMString dispname("");
							if(!isTableSizeZero)
							dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tInfo.elementId, tInfo.languageID );
							stencileInfo.elementName = dispname;
							PMString TableColName("");

							TableColName.Append("ATTRID_");
							TableColName.AppendNumber(PMReal(tInfo.elementId));
							stencileInfo.TagName = TableColName;
							if(!isTableSizeZero)
							{
								stencileInfo.typeId  = -1;
								stencileInfo.childId = FinalItemIds[i];
								stencileInfo.parentId = pNode.getPubId();
								stencileInfo.parentTypeId = CurrentObjectTypeID;
								stencileInfo.whichTab =tInfo.whichTab;

								stencileInfo.imgFlag = CurrentSectionID; 
								stencileInfo.isAutoResize = tInfo.isAutoResize;
								stencileInfo.LanguageID = tInfo.languageID;		
								if(global_project_level == 3)
									stencileInfo.sectionID = CurrentSubSectionID;
								if(global_project_level == 2)
									stencileInfo.sectionID = CurrentSectionID;
			
							}
							else
							{
								stencileInfo.typeId  = -1;
								stencileInfo.childId = -1;
								stencileInfo.parentId = pNode.getPubId();
								stencileInfo.parentTypeId = CurrentObjectTypeID;
								stencileInfo.whichTab = tInfo.whichTab;
								stencileInfo.imgFlag = 0;  
								stencileInfo.isAutoResize = tInfo.isAutoResize;
								stencileInfo.LanguageID = tInfo.languageID;							
								if(global_project_level == 3)
									stencileInfo.sectionID = CurrentSubSectionID;
								if(global_project_level == 2)
									stencileInfo.sectionID = CurrentSectionID;
							}
							XMLReference  xmlTagReference = childElement1->GetXMLReference();

							PMString attrVal;
							attrVal.AppendNumber(PMReal(stencileInfo.typeId));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("typeId"),WideString(attrVal));

							attrVal.Clear();
							attrVal.AppendNumber(PMReal(stencileInfo.childId));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("childId"),WideString(attrVal));

							attrVal.Clear();
							attrVal.AppendNumber(PMReal(stencileInfo.parentId));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentID"),WideString(attrVal));
							
							attrVal.Clear();
							attrVal.AppendNumber(PMReal(stencileInfo.parentTypeId));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentTypeID"),WideString(attrVal));
							
							attrVal.Clear();
							attrVal.AppendNumber(PMReal(stencileInfo.sectionID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("sectionID"),WideString(attrVal));
						}
					}	

					XMLReference  xmlTagReference = xmlElement->GetXMLReference();
					PMString attrVal;	
					int32 index;
					if(pNode.getIsProduct() == 1)
						index = 3;
					if(pNode.getIsProduct() == 0)
						index = 4;

					attrVal.Clear();
					attrVal.AppendNumber(index);
					Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("index"),WideString(attrVal));

					attrVal.Clear();
					attrVal.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentID"),WideString(attrVal));
					
					attrVal.Clear();
					attrVal.AppendNumber(PMReal(pNode.getTypeId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentTypeID"),WideString(attrVal));
					
					attrVal.Clear();
					attrVal.AppendNumber(PMReal(CurrentSectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("sectionID"),WideString(attrVal));
				}
			}
		}

		for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
		{
			NewTagList[tagIndex].tagPtr->Release();
		}
	}while(0);

	return errcode;
}


void TableUtility::SetDBTableStatus(bool16 Status)
{
	IsDBTable = Status;
}

ErrorCode TableUtility::CopyTables(const UIDRef& sourceStory, int32 StartIndexForNewTable, int32 repeteCopy)	
{
	ErrorCode status = kFailure;
	do {
		// Collect the ranges of text in the source story 
		// on which each embedded table is anchored.
		Text::StoryRangeList tableAnchorRanges;
		InterfacePtr<ITextModel> textModel(sourceStory, UseDefaultIID());
		ASSERT(textModel);
		if (textModel == NULL) {
			ASSERT_FAIL("invalid textModel");
			break;
		}
		InterfacePtr<ITextStoryThreadDictHier> textStoryDictHier(textModel, UseDefaultIID());
		ASSERT(textStoryDictHier);
		if(textStoryDictHier == NULL) {
			break;
		}
		UID nextUID = sourceStory.GetUID();
		while(nextUID != kInvalidUID)
        {
			InterfacePtr<ITextStoryThreadDict> textStoryThreadDict(sourceStory.GetDataBase(), nextUID, UseDefaultIID());
			ASSERT(textStoryThreadDict);
			if(textStoryThreadDict == NULL) {
				break;
			}
			InterfacePtr<ITableModel> tableModel(textStoryThreadDict, UseDefaultIID());
			if (tableModel) {
				// This is a table.
				bool16 wasAnchored;
				Text::StoryRange anchorRange = textStoryThreadDict->GetAnchorTextRange(&wasAnchored);
				if (wasAnchored && anchorRange.End() < textModel->GetPrimaryStoryThreadSpan()) {
					// This table is anchored in the story so store
					// the range of text on which it is anchored for
					// the subsequent copy operation.
					tableAnchorRanges.push_back(anchorRange);
				}
			}
			// Get the next text story thread dictionary in the collection.
			nextUID = textStoryDictHier->NextUID(nextUID);	
		}

		if (tableAnchorRanges.size() == 0) {
			// Nothing to copy.
			status = kSuccess;
			break;
		}

		// Copy tables embedded in the soure story by copying the range
		// of text on which each table is anchored.
		CmdUtils::SequencePtr sequence;		
		int32 IndexDiff = StartIndexForNewTable;

		for(int32 i=0; i< repeteCopy; i++)
		{
			Text::StoryRangeList::iterator iter = tableAnchorRanges.end();
			while (--iter >= tableAnchorRanges.begin()) {
				
					status = this->ProcessCopyStoryRangeCmd
					(
					sourceStory,
					iter->GetStart(NULL),
					iter->GetEnd() - iter->GetStart(NULL),
					sourceStory,
					StartIndexForNewTable, // Insert the copied stuff at the start of the destination story.
					0 // insert rather than replace
					);
				if (status != kSuccess) {
					//CA("CopyStoryRangeCmd failed");
					break;
				}
			}			
			StartIndexForNewTable+=IndexDiff;		
		}
	} while (false);

	return status;
}


ErrorCode TableUtility::ProcessCopyStoryRangeCmd(
			const UIDRef& sourceStory, 
			TextIndex sourceStart, 
			int32 sourceLength, 
			const UIDRef& destStory,
			TextIndex destStart, 
			int32 destLength
		)
{
	ErrorCode status = kFailure;
	do {
		// Validate the range of text to be copied from the source story.
		InterfacePtr<ITextModel> sourceTextModel(sourceStory, UseDefaultIID());
		if (sourceTextModel == NULL) {
			break;
		}
		int32 sourceStoryLength = sourceTextModel->TotalLength();
		if (sourceStart > sourceStoryLength - 1) {
			break;
		}
		if (sourceLength < 1) {
			break;
		}
		TextIndex sourceEnd = sourceStart + sourceLength;
		if (sourceEnd > sourceStoryLength - 1) {
			break;
		}

		// Validate the range of text to be replaced in the destination story.
		InterfacePtr<ITextModel> destTextModel(destStory, UseDefaultIID());
		if (destTextModel == NULL) {
			break;
		}
		int32 destStoryLength = destTextModel->TotalLength();
		if (destStart> destStoryLength - 1) {
			break;
		}
		if (destLength < 0) {
			break;
		}
		TextIndex destEnd = destStart + destLength;
		if (destEnd > destStoryLength - 1) {
			break;
		}

		// Create kCopyStoryRangeCmdBoss.
		InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
		if (copyStoryRangeCmd == NULL) {
			break;
		}

		// Refer the command to the source story and range to be copied.
		InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceUIDData == NULL) {
			break;
		}
		sourceUIDData->Set(sourceStory);
		InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceRangeData == NULL) {
			break;
		}
		sourceRangeData->Set(sourceStart, sourceEnd);

		// Refer the command to the destination story and the range to be replaced.
		UIDList itemList(destStory);
		copyStoryRangeCmd->SetItemList(itemList);
		InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
		destRangeData->Set(destStart, destEnd);

		// Process CopyStoryRangeCmd
		status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
	} while(false);
	return status;
}


ErrorCode TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	//CA("TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable");
	/*
	The data will be sprayed inside table in following manner
	Original Stencil
		 
	    Element0   blank    <-------
								    |-->Take this as rowPattern to repeat
		blank	   Element1 <-------

	Now take this as the repeating row pattern.
	Suppose the selected product has 2 items.Then the sprayed table will look like this.
	(without header)

	Element0/Item0	blank			<-----
										  |---->First repeated row Pattern(Item is constant for a pattern)
	blank			Element1/Item0  <-----
	
	Element0/Item1  blank			<-----
									 	  |---->Second repeated row Pattern(Item is constant for a pattern)
	blank			Element1/Item1  <-----

	repeatation of the rowPattern = number of items present for the product.(In above case its 2)
	So for spraying, three nested loops are implemented.
	First loop will iterate through the rowPattern
	Second loop will iterate through each row in the rowPattern
	Third loop will iterate through each column


	You will come to know the elementid by ID attribute of the CellTextXMLElement.
	But for item id you have to use application framework method. 
	*/

	//The TagStruct slugInfo in this case stores the TagStruct data attached to the table.
	//So if the textframe contains mulitple PRINTsource tagged table then we have to check the 
	//TagStruct data of each table with the slugInfo.

	bool16 ItemGroupSprayForItemHeader = kFalse;  //added for Mcguire Bearing Client
													//previously it was sprayiing all items with Item header row. 

	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;	
	do
	{	
		VectorScreenTableInfoPtr tableInfo = NULL;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		if (textFrameUID == kInvalidUID)
		{
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!textFrame");
			break;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			break;
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
				
		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;
		}
			
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == NULL)
				continue;//continues the for..tableIndex

			bool16 HeaderRowPresent = kFalse;

			RowRange rowRange(0,0);
			rowRange = tableModel->GetHeaderRows();
			int32 headerStart = rowRange.start;
			int32 headerCount = rowRange.count;

			if(headerCount != 0)
			{
				//CA("HeaderPresent");
				HeaderRowPresent = kTrue;
			}
				
			UIDRef tableRef(::GetUIDRef(tableModel)); 
		
			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
			XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();

			UIDRef ContentRef = contentRef.GetUIDRef();
			if(ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}			
			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;

			int32 totalNumberOfRowsBeforeRowInsertion = 0;
			if(HeaderRowPresent)
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
			else
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
			
			GridArea gridAreaForEntireTable;
			if(HeaderRowPresent)
			{
				gridAreaForEntireTable.topRow = headerCount;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
			}
			else
			{
				gridAreaForEntireTable.topRow = 0;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
			}
			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
			if(tableGeometry == NULL)
			{
				//CA("tableGeometry == NULL");
				break;
			}

			PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1);
			
			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;

		/*
			////////////////////////////////////
			collect the CellTags from the first row.
			The structure of the TableXMLElementTag is as below
			TableXMLElementTag|(TableXMLElementTag has all printsource attributes.)
								|
								|
								--CellXMLElementTag|(CellXMLElementTag doesn't have any printsource attributes.)
												   |
												   |
												    --TextXMLElementTag  (TextXMLElementTag has all the printsource attributes.)
			We are basically interested in the TextXMLElementTag.
			////////////////////////////////////////
		*/
			bool16 tableHeaderPresent = kFalse;	
			bool16 isProductItemPresent = kFalse;
			//Change the attributes of the tableTag.
			//ID..The older ID was -1 which is same as elementID of Product_Number.
			//Change it to someother value so that product_number and table id doesn't conflict.
			//we are already using ID = -101 for item_table_in_tabbed_text format.
			//so ID = -102
			
			int32 field_1 = -1;
			PMString attributeVal("");

			TableSourceInfoValue *tableSourceInfoValueObj;
			bool16 isCallFromTS = kFalse;
			InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
			if(ptrTableSourceHelper != nil)
			{
				tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
				if(tableSourceInfoValueObj)
				{
					isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
					if(isCallFromTS)
					{
						tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
						if(!tableInfo)
							break;
					}
				}
			}
			
			if(isCallFromTS )
			{
				attributeVal.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(0)));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef,WideString("tableId"),WideString(attributeVal));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef,WideString("ID"),WideString("-102"));
			}
			else
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("ID"),WideString("-102"));

			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentID"),WideString(attributeVal));
			attributeVal.Clear();
			
			attributeVal.Append(WideString("-1"));
			if(isCallFromTS)
			{
				attributeVal.Clear();
				attributeVal.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_TableType_ID().at(0)));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef,WideString("typeId"),WideString(attributeVal));
				
			}
			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentTypeID"),WideString(attributeVal));
			attributeVal.Clear();
			
			attributeVal.AppendNumber(PMReal(sectionid));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("sectionID"),WideString(attributeVal));
			attributeVal.Clear();
			
			//rowno of Table tag will now have the number of rows in repetating pattern excluding Header rows.
			attributeVal.AppendNumber(PMReal(totalNumberOfRowsBeforeRowInsertion));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("rowno"),WideString(attributeVal));
			attributeVal.Clear();

			attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("pbObjectId"),WideString(attributeVal));
			attributeVal.Clear();

			if(pNode.getIsProduct() == 1)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("index"),WideString("3"));
			}
			if(pNode.getIsProduct() == 0)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("index"),WideString("4"));
			}

			for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
			{//start for tagIndex = 0
				XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
				//This is a tag attached to the cell.
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				//Get the text tag attached to the text inside the cell.
				//We are providing only one texttag inside cell.
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					continue;
				}
				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
				{
					XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
					InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
					//The cell may be blank. i.e doesn't have any text tag inside it.
					if(cellTextTagPtr == NULL)
					{
						continue;
					}
					PMString  header_Str    =  cellTextTagPtr->GetAttributeValue(WideString("header"));
					PMString  childTag_Str  =  cellTextTagPtr->GetAttributeValue(WideString("childTag"));

					ItemGroupSprayForItemHeader = kFalse;
					if(header_Str == "1" && childTag_Str == "-1")	//---Condition for Mcguire Bearing Client --
						ItemGroupSprayForItemHeader = kTrue;				

					//Now check the typeId attribute of the cellTextTag.
					if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1")
						//The below line added on 11July...the serial blast day.
						//for spraying the whole item preset table inside table cell.
						|| cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103"))&& HeaderRowPresent == kFalse) 
					{
						tableHeaderPresent = kTrue;
					}
					
					//Ensure that the tag is of item  (i.e index =4) and not itemTable (i.e tableFlag =1).
					if(cellTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag")) != WideString("1"))
					{
						//CA("isProductItemPresent = kTrue");
						isProductItemPresent = kTrue;
					}

					PMString attributeVal("");
					attributeVal.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attributeVal));
					attributeVal.Clear();

					PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
					double ID = id.GetAsDouble();
								
					if((ID != -701) && (ID != -702) && (ID != -703) && (ID != -704))
					{
						attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentTypeID"),WideString(attributeVal));
						attributeVal.Clear();
					}

					attributeVal.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("sectionID"),WideString(attributeVal));
					attributeVal.Clear();

					attributeVal.AppendNumber(slugInfo.tableType);
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableType"),WideString(attributeVal));				
					attributeVal.Clear();

					if(isCallFromTS )
					{
						attributeVal.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(0)));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableId"),WideString(attributeVal));
					}
					attributeVal.Clear();
					attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("pbObjectId"),WideString(attributeVal));
					attributeVal.Clear();

					PMString field1	= cellTextTagPtr->GetAttributeValue(WideString("field1"));
					field_1 = field1.GetAsNumber();

				}
			}//end for tagIndex = 0 

			if(isProductItemPresent == kFalse)
			{
				//CA("isProductItemPresent == kFalse");
				for(int32 localTagIndex = 0;localTagIndex < slugInfo.tagPtr->GetChildCount();localTagIndex++)
				{//start for tagIndex = 0
					XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(localTagIndex);
					//This is a tag attached to the cell.
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}

					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
					{
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == NULL)
						{
							continue;
						}
						
						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strImageFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
						double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();
						double typeId = strTypeID.GetAsDouble();
						PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						int32 header = strHeader.GetAsNumber();

						PMString dispName("");
						if(strIndex == "4")//This is ItemTable..
						{
							makeTheTagImpotent(cellTextXMLElementPtr);
						}
						else if(strElementID == "-103")
						{
							do
							{
								VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
								if(typeValObj==NULL)
									break;

								VectorTypeInfoValue::iterator it1;

								bool16 typeIDFound = kFalse;
								for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
								{	
									if(typeId == it1->getTypeId())
									{
										typeIDFound = kTrue;
										break;
									}			
								}
								if(typeIDFound)
									dispName = it1->getName();

								if(typeValObj)
									delete typeValObj;

							}while(kFalse);
						}
						else if(tableFlag == "0" && header ==1 )
						{
							CElementModel  cElementModelObj;
							bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
							if(result)
								dispName = cElementModelObj.getDisplayName();
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getTypeId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("typeId"),WideString(attrVal));																	
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableFlag"),WideString("-13"));
						}
						else if(strElementID == "-104")
						{//This is ProductItemTable data
							if(strIndex == "4")//This is ItemTable..
							{
								makeTheTagImpotent(cellTextXMLElementPtr);
							}
							else
							{
								//Product ItemTable
								PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
								TagStruct tagInfo;												
								tagInfo.whichTab = strIndex.GetAsNumber();
								tagInfo.parentId = pNode.getPubId();

								tagInfo.isTablePresent = tableFlag.GetAsNumber();
								tagInfo.typeId = typeId;
								tagInfo.sectionID = sectionid;								
								GetItemTableInTabbedTextForm(tagInfo,dispName);
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("ID"),WideString("-104"));
							}
						}
						else if(strImageFlag == "1")
						{
							XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
							UIDRef ref = cntentREF.GetUIDRef();
							if(ref == UIDRef::gNull)
							{
								//CA("ref == UIDRef::gNull");
								continue;
							}
																						
							InterfacePtr<ITagReader> itagReader
							((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
							if(!itagReader)
								continue ;
							
							TagList tList=itagReader->getTagsFromBox(ref);
							TagStruct tagInfoo;
							int numTags=static_cast<int>(tList.size());

							if(numTags<0)
							{
								continue;
							}

							tagInfoo=tList[0];

							IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
							if(!iDataSprayer)
							{	
								//CA("!iDtaSprayer");
								continue;
							}
							//CA("calling Fill image in a box");
							if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
								|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
								|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
								|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
							{
								iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, tagInfoo.parentId);
							}
							else if(tagInfoo.elementId > 0)
							{
								isInlineImage = kTrue;
								iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo, tagInfoo.parentId,kTrue);
								iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
							}
							else
							{
								iDataSprayer->fillImageInBox(ref ,tagInfoo, tagInfoo.parentId);
							}

							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
							continue;
						}
						else if(tableFlag == "0")
						{
							//CA("product here");							
							dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID, languageID, CurrentSectionID, kTrue);
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getTypeId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("typeId"),WideString(attrVal));
							attrVal.Clear();
							attrVal.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attrVal));
						}
						
						int32 delStart = -1, delEnd = -1;
						bool16 shouldDelete = kFalse;
						if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
						{//if(tagInfo.rowno == -117)
								delStart = tagStartPos-1;
							if(dispName == "")							
								shouldDelete = kTrue;
						}
						if(shouldDelete == kTrue)
						{
							int32 tagStartPos1 = -1;
							int32 tagEndPos1 = -1;

							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

							TextIterator begin(textModel , tagEndPos + 1);
							TextIterator end(textModel , tagEndPos1 - 1);
							int32 addthis = 0;
							bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter <= end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									enterFound = kTrue;
									break;
								}
									
							}
							if(enterFound)
							{
								//CA("enterFound == kTrue");
								delEnd = tagEndPos + 1 + addthis;
							}
							else
							{
								//CA("enterFound == kFalse");
								delEnd = tagEndPos1;
							}

							DeleteText( textModel,   delStart, delEnd- delStart + 1);
							CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

							continue;
						}

						PMString textToInsert("");
						//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						//if(!iConverter)
						{
							textToInsert=dispName;					
						}
						//else
						//	textToInsert=iConverter->translateString(dispName);

						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
                        textToInsert.ParseForEmbeddedCharacters();
						/*K2*/boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);

					}     			
				}//end for tagIndex = 0
				
				InterfacePtr<IDataSprayer> iDataSprayer((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!iDataSprayer)
				{	
					//CA("!iDtaSprayer");
					continue;
				}

				UIDRef tempUIDRef = boxUIDRef;
				iDataSprayer->fitInlineImageInBoxInsideTableCell(tempUIDRef);
				

				attributeVal.Clear();
				attributeVal.AppendNumber(field_1);
				XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();

				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("field1"),WideString(attributeVal));

				continue;
			}//end of product if
			if(pNode.getIsProduct() == 1)
			{
				if(isCallFromTS)
				{
					CItemTableValue oTableValue;
					vector<double> vec_items;
					FinalItemIds.clear();
					
					bool16 tableFound = kFalse;
					VectorScreenTableInfoValue::iterator it;
					for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
					{				
						oTableValue = *it;		

						if(oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
						{
							vec_items = oTableValue.getItemIds();
							FinalItemIds = vec_items;
							tableFound = kTrue;
							
							break;
						}
					}
									
					if(tableFound == kFalse)
						return kFalse;
				}
				else
				{
					//CA("Content Sprayer");
					VectorScreenTableInfoPtr tableInfo = NULL;
					if(pNode.getIsONEsource())
					{
						// For ONEsource
						//CA("ONE SOURCE");
						//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
					}else
					{	
						// for publication
						double languageID = ptrIAppFramework->getLocaleId();
						tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), languageID,  kTrue);
					}
					if(!tableInfo)
					{
						break;
					}
					if(tableInfo->size()==0)
					{ 
						makeTagInsideCellImpotent(slugInfo.tagPtr);
						break;
					}
							
					CItemTableValue oTableValue;
					VectorScreenTableInfoValue::iterator it;

					bool16 typeidFound=kFalse;
					vector<double> vec_items;
					FinalItemIds.clear();

					int32 Event_List_Val = -1;
					bool16 isEvent_Val_Check = kFalse;
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{				
						oTableValue = *it;				
						vec_items = oTableValue.getItemIds();

						isEvent_Val_Check = kFalse;
									
 						if(field_1 == -1)
						{
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
								{	
									bool16 Flag = kFalse;
									for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
									{
										if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
								}

							}
						}
						else
						{
							if(field_1  !=  oTableValue.getTableTypeID())
								continue;

							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
								{	
									bool16 Flag = kFalse;
									for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
									{
										if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
								}

							}

						}
					}

					if(tableInfo)
					{
						tableInfo->clear();
						delete tableInfo;
						
					}
				}
			}
				
			attributeVal.Clear();
			attributeVal.AppendNumber(field_1);

			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("field1"),WideString(attributeVal));

			int32 TotalNoOfItems =static_cast<int32>(FinalItemIds.size());
			if(TotalNoOfItems <= 0)
			{
				break;
			}

			GridArea bodyArea = tableModel->GetBodyArea();
			int32 bodyCellCount = 0;
			ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
			ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
			while (iterTable1 != endTable1)
			{
				bodyCellCount++;
				++iterTable1;
			}
            
			int32 rowPatternRepeatCount =static_cast<int32>(FinalItemIds.size());			
			//int32 rowsToBeStillAdded = TotalNoOfItems - totalNumberOfRowsBeforeRowInsertion;
			//we have to deal with 2 cases
			//case 1: when tableHeaders are absent
			//then the total number of rows i.e rowCount = FinalItemIds.size()-1
			//case 2 : when tableHeaders are present
			//then the total number of rows i.e rowCount = FinalItemsIds.size()
			if(tableHeaderPresent)
			{
				rowPatternRepeatCount = rowPatternRepeatCount + 1;
			}
            
			int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
				break;
			}

			if(!ItemGroupSprayForItemHeader)	//-----This Condition only for Mcguire Bearing Client 
			{
                if(HeaderRowPresent)
			{
				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
				}
				if(rowPatternRepeatCount > 1)
				{
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
						break;
					}
					
					TableMemento* tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
						break;
					}
					
					for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						tableModel->Paste(GridAddress((pasteIndex * totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr, kTrue);
						if(pasteIndex < rowPatternRepeatCount - 1)
						{
							tempPtr = tableModel->Copy(gridAreaForEntireTable); //og
						}
					}
				}
			}
			else 
			{
				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);
				}

				if(rowPatternRepeatCount > 1)
				{
					
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
						break;
					}
					TableMemento* tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
						break;
					}
				
					for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{		
						tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr); // og
						tempPtr = tableModel->Copy(gridAreaForEntireTable);
					}
				}

			}
            }
			//Note the tags are numbered continuously.and not in rows and columns fashion.
			//its our responsibility to find the exact row and exact column of the tag.
	
			int32 nonHeaderRowPatternIndex = 0;

			if(HeaderRowPresent)
			{
				GridArea headerArea = tableModel->GetHeaderArea();
				ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
				ITableModel::const_iterator endTable(tableModel->end(headerArea));
				while (iterTable != endTable)
				{
					GridAddress gridAddress = (*iterTable);         
					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
					if(cellContent == nil) 
					{
						//CA("cellContent == nil");
						break;
					}
					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
					if(!cellXMLReferenceData) {
						//CA("!cellXMLReferenceData");
						break;
					}
					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
					++iterTable;
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							continue;
						}
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								continue;
							}

							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
							PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
							PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
							PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString strdataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
	
							PMString strHeaderVal =   cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
							PMString strChildTag =    cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
							PMString strChildId =    cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
	
							double childId = strChildId.GetAsDouble();
							double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
							int32 childTag = strChildTag.GetAsNumber();

							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							PMString dispName("");
							////if normal tag present in header row...
							////then we are spraying the value related to the first item.
							double itemID = FinalItemIds[0];
							
							if((strdataType == "4" && isCallFromTS) || (elementID == -121 && isCallFromTS))
							{
								//CA("Going to spray table Name");
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
								for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
								{				
									oTableValue = *it;
									if(isCallFromTS)
									{
										if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
										{
											strParentTypeID.clear();
											strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(strParentTypeID));
											
											strTypeId.Clear();
											strTypeId.AppendNumber(PMReal(oTableValue.getTableID()));
											cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(strTypeId));

											dispName = oTableValue.getName();
											break;
										}
									}									
								}

							}
							else if(strTypeId == "-1")
							{
								double parentTypeID = strParentTypeID.GetAsDouble();

								//CA("#################normal tag present in header row###############");
								if(tableFlag == "1")
								{
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")
									{//Product ItemTable
										PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
										TagStruct tagInfo;												
										tagInfo.whichTab = index.GetAsNumber();
										if(index == "3")
											tagInfo.parentId = pNode.getPubId();

										tagInfo.isTablePresent = tableFlag.GetAsNumber();
										tagInfo.typeId = typeID.GetAsDouble();
										tagInfo.sectionID = sectionid;
										
											
										GetItemTableInTabbedTextForm(tagInfo,dispName);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));
									}

									else if(index == "4")
									{//This is the Section level ItemTableTag and we have selected the product
										makeTheTagImpotent(cellTextXMLElementPtr);
									}
									
								}
								else if(imgFlag == "1")
								{							
									XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
									UIDRef ref = cntentREF.GetUIDRef();
									if(ref == UIDRef::gNull)
									{
										//CA("ref == UIDRef::gNull");
										continue;
									}

									InterfacePtr<ITagReader> itagReader
									((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
									if(!itagReader)
										continue ;
									
									TagList tList=itagReader->getTagsFromBox(ref);
									TagStruct tagInfoo;
									int numTags=static_cast<int>(tList.size());

									if(numTags<0)
									{
										continue;
									}

									tagInfoo=tList[0];

									tagInfoo.parentId = pNode.getPubId();
									tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
									tagInfoo.sectionID=CurrentSubSectionID;

									IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
									if(!iDataSprayer)
									{	
										//CA("!iDtaSprayer");
										continue;
									}
									
									if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
										|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
										|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
										|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
									{
										iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
									}
									else if(tagInfoo.elementId > 0)
									{
										isInlineImage = kTrue;
										iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo, itemID,kTrue);
										iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
									}
									else
									{
										iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
									}

									PMString attrVal;
									attrVal.AppendNumber(PMReal(itemID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									
									for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
									{
										tList[tagIndex].tagPtr->Release();
									}
									
									continue;
								}
								else if(tableFlag == "0")
								{
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									
									if(index =="4")
									{//Item
										//added on 22Sept..EventPrice addition.
										if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
										{
											TagStruct tagInfo;
											tagInfo.elementId = elementID;
											tagInfo.tagPtr = cellTextXMLElementPtr;
											tagInfo.typeId = itemID;
											tagInfo.parentTypeID = parentTypeID;
											
											tagInfo.childId = childId;
											tagInfo.childTag = childTag;
											handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
										}
										//ended on 22Sept..EventPrice addition.
										else if(elementID == -803)  // For 'Letter Key'
										{											
											dispName = "a" ;	// For First item
										}			
										else
										{
											if(strHeaderVal == "1")
											{
												dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
											}
											else
											{
												dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
											}
										}

										if (elementID == -827)  // For 'Number Key'
										{
											dispName = "1" ;	// For First item
										}
										
										PMString attrVal;
										attrVal.AppendNumber(PMReal(itemID));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
									}
									else if( index == "3")
									{//Product
										if((strdataType == "4" ) || (strdataType == "5" ) )
										{
											VectorScreenTableInfoPtr table_Info = NULL;
											if(pNode.getIsONEsource())
											{
												// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
												//table_Info =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
											}else
											{
												// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
												table_Info= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId() , languageID, kTrue); // getting only customtable data

											}
											if(!table_Info)
											{
												ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
												return kFalse;
											}

											if(table_Info->size()==0){
												ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
												delete table_Info;
												break;
											}
											CItemTableValue oTableValue;
											VectorScreenTableInfoValue::iterator it;
											for(it = table_Info->begin() ; it != table_Info->end() ; it++)
											{				
												oTableValue = *it;
												PMString strTableId;
												if(elementID == -121)
												{
													strTableId.Clear();
													strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));
													dispName = oTableValue.getName();

													strParentTypeID.clear();
														strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
														Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("typeId"),WideString(strParentTypeID));
													break;
												}
												else if(strdataType == "5" ) 
												{
													if(elementID == -983)//---List Description
													{
														dispName = oTableValue.getDescription();	
														strTableId.Clear();
														strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
														Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));

														strParentTypeID.clear();
														strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
														Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("typeId"),WideString(strParentTypeID));
													}
													else if(elementID == -982)//---Stencil Name
													{
														dispName = oTableValue.getstencil_name();
														strTableId.Clear();
														strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
														Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));

														strParentTypeID.clear();
														strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
														Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("typeId"),WideString(strParentTypeID));
													}
													break;
													
												}
											}
										}
										else
										{
											dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID, CurrentSectionID, kFalse);
											PMString attrVal;
											attrVal.AppendNumber(PMReal(pNode.getTypeId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
											attrVal.Clear();

											attrVal.AppendNumber(PMReal(pNode.getPubId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
										}
									}
								}
								
							}
							else if(imgFlag == "1")
							{
								XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
								UIDRef ref = cntentREF.GetUIDRef();
								if(ref == UIDRef::gNull)
								{
									int32 StartPos1 = -1;
									int32 EndPos1 = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									

									InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
										textModel->QueryStrand(kOwnedItemStrandBoss, 
										IItemStrand::kDefaultIID)));
									if (itemStrand == nil) {
										continue;
									}
									//CA("going for Owened items");
									OwnedItemDataList ownedList;
									itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
									int32 count = ownedList.size();
									if(count > 0)
									{
										UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
										if(newRef == UIDRef::gNull)
										{
											continue;
										}
										ref = newRef;
									}else
										continue;
								}
								

								InterfacePtr<ITagReader> itagReader
								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
								if(!itagReader)
									continue ;
								
								TagList tList=itagReader->getTagsFromBox(ref);
								TagStruct tagInfoo;
								int numTags=static_cast<int>(tList.size());

								if(numTags<0)
								{
									continue;
								}

								tagInfoo=tList[0];

								tagInfoo.parentId = pNode.getPubId();
								tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
								tagInfoo.sectionID=CurrentSubSectionID;

								IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
								if(!iDataSprayer)
								{	
									//CA("!iDtaSprayer");
									continue;
								}
								
								if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
									|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
									|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
									|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
								{
									iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
								}
								else if(tagInfoo.elementId > 0)
								{
									isInlineImage = kTrue;
									iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo, itemID,kTrue);
									iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
								}
								else
								{
									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
								}
				
								
								PMString attrVal;
								attrVal.AppendNumber(PMReal(itemID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
								
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
								continue;
							}
							else
							{
								if(tableFlag == "1")
								{					
									//CA("inside cell table is present");
                                    //added on 11July...the serial blast day
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")
									{
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-103"));										
										do
										{
											PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));

											double typeId = strTypeID.GetAsDouble();
											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
											if(typeValObj==NULL)
											{
												ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
												break;
											}

											VectorTypeInfoValue::iterator it1;

											bool16 typeIDFound = kFalse;
											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
											{	
												if(typeId == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
											}
											if(typeIDFound)
												dispName = it1->getName();

											if(typeValObj)
												delete typeValObj;
										}while(kFalse);
												
									}
									//This is a special case.The tag is of product but the Item is
									//selected.So make this tag unproductive for refresh.(Impotent)
									else if(index == "4")
									{
										makeTheTagImpotent(cellTextXMLElementPtr);
									}
											
											
								}
								else
								{
                                    //CA("tableFlag != 1");
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")
									{//For product
										CElementModel  cElementModelObj;
										bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
										if(result)
											dispName = cElementModelObj.getDisplayName();
										PMString attrVal;
										attrVal.AppendNumber(PMReal(pNode.getTypeId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
																			
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableFlag"),WideString("-13"));
									}
									else if(index == "4")
									{
										if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
										{
											PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
											double parentTypeID = strParentTypeID.GetAsDouble();
											
											/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
											if(TypeInfoVectorPtr != NULL)
											{
												VectorTypeInfoValue::iterator it3;
												int32 Type_id = -1;
												PMString temp = "";
												PMString name = "";
												for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
												{
													Type_id = it3->getTypeId();
													if(parentTypeID == Type_id)
													{
														temp = it3->getName();
														if(elementID == -701)
														{
															dispName = temp;
														}
														else if(elementID == -702)
														{
															dispName = temp + " Suffix";
														}
													}														
												}													
											}
											if(TypeInfoVectorPtr)
												delete TypeInfoVectorPtr;*/
										}
										else if(elementID == -703)
										{
											dispName = "$Off";
										}
										else if(elementID == -704)
										{
											dispName = "%Off";
										}
										else
											dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

										//We have assumed that there is atleast one stencil with HeaderAttirbute
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("1"));
									}
									
								}
							}
                            //added to attach the tag values to ListandtableName
							PMString rowValue("");
							rowValue.AppendNumber(PMReal(gridAddress.row));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));

							PMString colValue("");
							colValue.AppendNumber(PMReal(gridAddress.col));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));

                            //till here

							int32 delStart = -1, delEnd = -1;
							bool16 shouldDelete = kFalse;
							if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
							{//if(tagInfo.rowno == -117)
                                delStart = tagStartPos-1;
								if(dispName == "")							
									shouldDelete = kTrue;
							}
							if(shouldDelete == kTrue )
							{
								//CA("Going to delete 1 ");
						
								int32 tagStartPos1 = -1;
								int32 tagEndPos1 = -1;

								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

								TextIterator begin(textModel , tagEndPos + 1);
								TextIterator end(textModel , tagEndPos1 - 1);
								int32 addthis = 0;
								bool16 enterFound = kFalse;
								for(TextIterator iter = begin ;iter <= end ; iter++)
								{
									addthis++;
									if(*iter == kTextChar_CR)
									{
										enterFound = kTrue;
										break;
									}
										
								}
								if(enterFound)
								{
									//CA("enterFound == kTrue");
									delEnd = tagEndPos + 1 + addthis;
								}
								else
								{
									//CA("enterFound == kFalse");
									delEnd = tagEndPos1;
								}

								DeleteText( textModel,   delStart, delEnd- delStart + 1 );
								CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
								continue;
							}

						
							
							
							
							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//	textToInsert=iConverter->translateString(dispName);

							//CA("textToInsert = " + textToInsert);
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
						}
				}//for..iterate through each row of the headerRowPattern
			
				nonHeaderRowPatternIndex = 1;
				rowPatternRepeatCount++;

			}
			
				if(tableHeaderPresent)
				{//if 2 tableHeaderPresent 
				//First rowPattern  now is of header element.
				//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
					for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
					{//for..iterate through each row of the repeatedRowPattern
						for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
						{//for..iterate through each column
                            	int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
								XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
								//Also note that, cell may be blank i.e doesn't contain any textTag.
								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
								{
									continue;
								}
								
								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
								{
									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
									//Check if the cell is blank.
									if(cellTextXMLElementPtr == NULL)
									{
									continue;
									}

									//Get all the elementID and languageID from the cellTextXMLElement
									//Note ITagReader plugin methods are not used.
									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
									//check whether the tag specifies ProductCopyAttributes.
									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
									PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));

									double elementID = strElementID.GetAsDouble();
									double languageID = strLanguageID.GetAsDouble();

									int32 tagStartPos = -1;
									int32 tagEndPos = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
									tagStartPos = tagStartPos + 1;
									tagEndPos = tagEndPos -1;

									PMString dispName(""); 
									if(tableFlag == "1")
									{					
										//CA("inside cell table is present");
                                        //added on 11July...the serial blast day
										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										if(index== "3")
										{
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-103"));										
											do
											{
												PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
												double typeId = strTypeID.GetAsDouble();
												VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
												if(typeValObj==NULL)
												{
													ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
													break;
												}

												VectorTypeInfoValue::iterator it1;

												bool16 typeIDFound = kFalse;
												for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
												{	
													if(typeId == it1->getTypeId())
													{
														typeIDFound = kTrue;
														break;
													}			
												}
												if(typeIDFound)
													dispName = it1->getName();

												if(typeValObj)
													delete typeValObj;
											}while(kFalse);
											
										}
										//This is a special case.The tag is of product but the Item is
                                        //selected.So make this tag unproductive for refresh.(Impotent)
										else if(index == "4")
										{
											makeTheTagImpotent(cellTextXMLElementPtr);
										}
									}
									else
									{							
										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										if(index== "3")
										{//For product
											CElementModel  cElementModelObj;
											bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
											if(result)
												dispName = cElementModelObj.getDisplayName();
											PMString attrVal;
											attrVal.AppendNumber(PMReal(pNode.getTypeId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
																				
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableFlag"),WideString("-13"));
										}
										else if(index == "4")
										{
											//following code added by Tushar on 27/12/06
											if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
											{
												PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
												double parentTypeID = strParentTypeID.GetAsDouble();
												
												/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
												if(TypeInfoVectorPtr != NULL)
												{
													VectorTypeInfoValue::iterator it3;
													int32 Type_id = -1;
													PMString temp = "";
													PMString name = "";
													for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
													{
														Type_id = it3->getTypeId();
														if(parentTypeID == Type_id)
														{
															temp = it3->getName();
															if(elementID == -701)
															{
																dispName = temp;
															}
															else if(elementID == -702)
															{
																dispName = temp + " Suffix";
															}
														}														
													}													
												}
												if(TypeInfoVectorPtr)
													delete TypeInfoVectorPtr;*/
											}
											else if(elementID == -703)
											{
												dispName = "$Off";
											}
											else if(elementID == -704)
											{
												dispName = "%Off";
											}
											else
												dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

											//Modification of XMLTagAttributes.
											//modify the XMLTagAttribute "typeId" value to -2.
											//We have assumed that there is atleast one stencil with HeaderAttirbute
											//true .So assign for all the stencil in the header row , typeId == -2
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("1"));
											//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
										}
										
									}
									
									
									int32 delStart = -1, delEnd = -1;
									bool16 shouldDelete = kFalse;
									if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
									{//if(tagInfo.rowno == -117)
                                        delStart = tagStartPos-1;
										if(dispName == "")							
											shouldDelete = kTrue;
									}
									if(shouldDelete == kTrue )
									{
										//CA("Going to delete 1 ");
								
										int32 tagStartPos1 = -1;
										int32 tagEndPos1 = -1;

										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

										TextIterator begin(textModel , tagEndPos + 1);
										TextIterator end(textModel , tagEndPos1 - 1);
										int32 addthis = 0;
										bool16 enterFound = kFalse;
										for(TextIterator iter = begin ;iter <= end ; iter++)
										{
											addthis++;
											if(*iter == kTextChar_CR)
											{
												enterFound = kTrue;
												break;
											}
												
										}
										if(enterFound)
										{
											//CA("enterFound == kTrue");
											delEnd = tagEndPos + 1 + addthis;
										}
										else
										{
											//CA("enterFound == kFalse");
											delEnd = tagEndPos1;
										}

										DeleteText( textModel,   delStart, delEnd- delStart  + 1);
										CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
										continue;
									}

						
									
									PMString textToInsert("");
									//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
									//if(!iConverter)
									{
										textToInsert=dispName;					
									}
									//else
									//	textToInsert=iConverter->translateString(dispName);

									//CA("textToInsert = " + textToInsert);
									//Spray the Header data .We don't have to change the tag attributes
									//as we have done it while copy and paste.
                                    textToInsert.ParseForEmbeddedCharacters();
									boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
									ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
								}
                       		}//end for..iterate through each column.
					}//end for..iterate through each row of repeatedRowPattern.
					//All the subsequent rows will now spray the ItemValue.
					nonHeaderRowPatternIndex = 1;					
				}//end if 2 tableHeaderPresent
								
							GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);

							int32 ColIndex = -1;
							GridArea bodyArea_new = tableModel->GetBodyArea();
							
							ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
							ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));

							while (iterTable != endTable)
							{
								GridAddress gridAddress = (*iterTable); 
								if(tableHeaderPresent)
									if(gridAddress <= headerRowGridAddress){
										iterTable++;							
										continue;
									}

								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
								if(cellContent == nil) 
								{
									break;
								}
								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
								if(!cellXMLReferenceData) {
									break;
								}
								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
								++iterTable;

								ColIndex++;
								int32 itemIDIndex = ColIndex/bodyCellCount;
								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
								
								//Note that, cell may be blank i.e doesn't contain any textTag.
								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
								{
									continue;
								}
								
								// Checking for number of tag element inside cell
								int32 cellChildCount = cellXMLElementPtr->GetChildCount();
								for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount;tagIndex1++)
								{
									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());							
									//Check if the cell is blank.
									if(cellTextXMLElementPtr == NULL)
									{
										continue;
									}
									//Get all the elementID and languageID from the cellTextXMLElement
									//Note ITagReader plugin methods are not used.
									
									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
									//check whether the tag specifies ProductCopyAttributes.
									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
									PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
									PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
									PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
									PMString strChildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
									PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

									int32 header = strHeader.GetAsNumber();	
									double childId = strChildId.GetAsDouble();
									int32 childTag = strChildTag.GetAsNumber();

									double elementID = strElementID.GetAsDouble();
									double languageID = strLanguageID.GetAsDouble();
									double itemID = FinalItemIds[itemIDIndex];
									double parentTypeID = strParentTypeID.GetAsDouble();

									int32 tagStartPos = -1;
									int32 tagEndPos = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
									tagStartPos = tagStartPos + 1;
									tagEndPos = tagEndPos -1;

									PMString dispName("");
									if(tableFlag == "1")
									{
										//The following code will spray the table preset inside the table cell.
										//for copy ID = -104
										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										if(index== "3")// || index == "4")
										{//Product ItemTable
											PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
											TagStruct tagInfo;												
											tagInfo.whichTab = index.GetAsNumber();
											if(index == "3")
												tagInfo.parentId = pNode.getPubId();
											/*else if(index == "4")
												tagInfo.parentId = pNode.getPBObjectID();*/

											tagInfo.isTablePresent = tableFlag.GetAsNumber();
											tagInfo.typeId = typeID.GetAsDouble();
											tagInfo.sectionID = sectionid;
											
												
											GetItemTableInTabbedTextForm(tagInfo,dispName);
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));
										}
										else if(index == "4")
										{//This is the Section level ItemTableTag and we have selected the product
										//for spraying
											makeTheTagImpotent(cellTextXMLElementPtr);
										}
										
									}

									
									else if(imgFlag == "1")
									{
										XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
										UIDRef ref = cntentREF.GetUIDRef();
										if(ref == UIDRef::gNull)
										{
											//CA("ref == UIDRef::gNull");
											continue;
										}

										InterfacePtr<ITagReader> itagReader
										((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
										if(!itagReader)
											continue ;
										
										TagList tList=itagReader->getTagsFromBox(ref);
										TagStruct tagInfoo;
										int numTags=static_cast<int>(tList.size());

										if(numTags<0)
										{
											continue;
										}

										tagInfoo=tList[0];
										
										if(tagInfoo.whichTab != 4)
										{
											tagInfoo.parentId = pNode.getPubId();
											tagInfoo.parentTypeID = strParentTypeID.GetAsDouble();
										}
										else if(tagInfoo.whichTab == 4)
										{
											//CA("tagInfoo.whichTab == 4");
											tagInfoo.parentId = itemID;
											tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");	
										}
										tagInfoo.sectionID=CurrentSubSectionID;

										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
										if(!iDataSprayer)
										{	
											//CA("!iDtaSprayer");
											continue;
										}
										
										if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
											|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
											|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
											|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
										{
											iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
										}
										else if(tagInfoo.elementId > 0)
										{
											isInlineImage = kTrue;
											iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo, itemID,kTrue);
											iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);					
										}
										else
										{
											iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
										}

										
										
										PMString attrVal;
										attrVal.AppendNumber(PMReal(itemID));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
										
										for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
										{
											tList[tagIndex].tagPtr->Release();
										}
										continue;
									}
									else if(tableFlag == "0")
									{
										//CA("tableFlag = 0");
										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										if(index =="4")
										{//Item
											//added on 22Sept..EventPrice addition.												
											/*if(strElementID == "-701")
											{
												PMString  attributeIDfromNotes = "-1";
												bool8 isNotesValid = getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
												cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
												elementID = attributeIDfromNotes.GetAsNumber();
											}*/
											if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
											{
												TagStruct tagInfo;
												tagInfo.elementId = elementID;
												tagInfo.tagPtr = cellTextXMLElementPtr;
												tagInfo.typeId = itemID;
												tagInfo.parentTypeID = parentTypeID;//added by Tushar on 27/12/06
												tagInfo.childId = childId;
												tagInfo.childTag = childTag;
												handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
											}//ended on 22Sept..EventPrice addition.
											else if(elementID == -803)  // For 'Letter Key'
											{
												dispName.Clear();
												int wholeNo =  itemIDIndex / 26;
												int remainder = itemIDIndex % 26;								

												if(wholeNo == 0)
												{// will print 'a' or 'b'... 'z'  upto 26 item ids
													dispName.Append(AlphabetArray[remainder]);
												}
												else  if(wholeNo <= 26)
												{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
													dispName.Append(AlphabetArray[wholeNo-1]);	
													dispName.Append(AlphabetArray[remainder]);	
												}
												else if(wholeNo <= 52)
												{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
													dispName.Append(AlphabetArray[0]);
													dispName.Append(AlphabetArray[wholeNo -26 -1]);	
													dispName.Append(AlphabetArray[remainder]);										
												}
											}
											else
											{	

												dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
											}
											if(elementID == -827) //******Number Keys
											{
												dispName.Clear();
												dispName.Append(itemIDIndex+1);
												
											}
											
											PMString attrVal;
											attrVal.AppendNumber(PMReal(itemID));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));

											attrVal.clear();
											attrVal.Append("1");
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childTag"),WideString(attrVal));
											attrVal.Clear();

										}
										else if( index == "3")
										{//Product
											
											dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(), elementID, languageID, CurrentSectionID, kTrue);
											PMString attrVal;
											attrVal.AppendNumber(PMReal(pNode.getTypeId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
											attrVal.Clear();

											attrVal.AppendNumber(PMReal(pNode.getPubId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("parentID"),WideString(attrVal));
										}
									}
                                    //added to attach the tag Values to child elements
									PMString rowValue("");
									rowValue.AppendNumber(PMReal(gridAddress.row));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));

									PMString colValue("");
									colValue.AppendNumber(PMReal(gridAddress.col));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));
                                    //till here
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("-1"));
									int32 delStart = -1, delEnd = -1;
									bool16 shouldDelete = kFalse;
									if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
									{//if(tagInfo.rowno == -117)
                                        delStart = tagStartPos-1;

										if(dispName == "")							
											shouldDelete = kTrue;
									}
									if(shouldDelete == kTrue )
									{
								
										int32 tagStartPos1 = -1;
										int32 tagEndPos1 = -1;
										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);
										TextIterator begin(textModel , tagEndPos + 1);
										TextIterator end(textModel , tagEndPos1 - 1);
										int32 addthis = 0;
										bool16 enterFound = kFalse;
										for(TextIterator iter = begin ;iter <= end ; iter++)
										{
											addthis++;
											if(*iter == kTextChar_CR)
											{
												enterFound = kTrue;
												break;
											}
												
										}
										if(enterFound)
										{
											//CA("enterFound == kTrue");
											delEnd = tagEndPos + 1 + addthis;
										}
										else
										{
											//CA("enterFound == kFalse");
											delEnd = tagEndPos1;
										}

										DeleteText( textModel,   delStart, delEnd- delStart  + 1);
										CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
										continue;
									}

									

									PMString textToInsert("");
									//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
									//if(!iConverter)
									{
										textToInsert=dispName;					
									}
									//else
									//	textToInsert=iConverter->translateString(dispName);
								
								
							
									//Spray the Header data .We don't have to change the tag attributes
									//as we have done it while copy and paste.
                                    textToInsert.ParseForEmbeddedCharacters();
									boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
									
									ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
								}//end for..iterate through each cell each text tag
								
							}//end for..iterate through each column

			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{	
				//CA("!iDtaSprayer");
				continue;
			}
			UIDRef tempUIDRef = boxUIDRef;				
			DataSprayerPtr->fitInlineImageInBoxInsideTableCell(tempUIDRef);
					
	   }//end for..tableIndex
	   errcode = kTrue;
	}while(0);

	return errcode;
}


ErrorCode TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	//CA("TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable");
	
	deleteCount =0;
	deleteTextRanges.clear();

	/*
	The data will be sprayed inside table in following manner
	Original Stencil
		 
	    Element0   blank    <-------
								    |-->Take this as rowPattern to repeat
		blank	   Element1 <-------

	Now take this as the repeating row pattern.
	Suppose the selected product has 2 items.Then the sprayed table will look like this.
	(without header)

	Element0/Item0	blank			<-----
										  |---->First repeated row Pattern(Item is constant for a pattern)
	blank			Element1/Item0  <-----
	
	Element0/Item1  blank			<-----
									 	  |---->Second repeated row Pattern(Item is constant for a pattern)
	blank			Element1/Item1  <-----

	repeatation of the rowPattern = number of items present for the product.(In above case its 2)
	So for spraying, three nested loops are implemented.
	First loop will iterate through the rowPattern
	Second loop will iterate through each row in the rowPattern
	Third loop will iterate through each column

	You will come to know the elementid by ID attribute of the CellTextXMLElement.
	But for item id you have to use application framework method. 
	*/

	//The TagStruct slugInfo in this case stores the TagStruct data attached to the table.
	//So if the textframe contains mulitple PRINTsource tagged table then we have to check the 
	//TagStruct data of each table with the slugInfo.
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;	
	double languageId = -1;

	XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		if (textFrameUID == kInvalidUID)
		{
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!textFrame");
			break;
		}
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			break;
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::totalNumberOfTablesInsideTextFrame < 0");																						
			break;
		}

		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == NULL)
				continue;//continues the for..tableIndex

			bool16 HeaderRowPresent = kFalse;

			RowRange rowRange(0,0);
			rowRange = tableModel->GetHeaderRows();
			int32 headerStart = rowRange.start;
			int32 headerCount = rowRange.count;
			
			PMString HeaderStart = "";
			HeaderStart.AppendNumber(headerStart);
			PMString HeaderCount = "";
			HeaderCount.AppendNumber(headerCount);

			if(headerCount != 0)
			{
				//CA("HeaderPresent");
				HeaderRowPresent = kTrue;
			}
			
			
			UIDRef tableRef(::GetUIDRef(tableModel));
			languageId = slugInfo.languageID; 
			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();			

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}	
			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
					
			int32 totalNumberOfRowsBeforeRowInsertion = 0;
			if(HeaderRowPresent)
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
			else
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;

			GridArea gridAreaForEntireTable;
			if(HeaderRowPresent)
			{
				gridAreaForEntireTable.topRow = headerCount;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
			}
			else
			{
				gridAreaForEntireTable.topRow = 0;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
			}

			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
			if(tableGeometry == NULL)
			{
				//CA("tableGeometry == NULL");
				break;
			}
			PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 			
			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;
			
		/*
			////////////////////////////////////
			collect the CellTags from the first row.
			The structure of the TableXMLElementTag is as below
			  TableXMLElementTag|(TableXMLElementTag has all printsource attributes.)
								|
								|
								--CellXMLElementTag|(CellXMLElementTag doesn't have any printsource attributes.)
												   |
												   |
												    --TextXMLElementTag  (TextXMLElementTag has all the printsource attributes.)
			We are basically interested in the TextXMLElementTag.
			////////////////////////////////////////
		*/
			bool16 tableHeaderPresent = kFalse;
			bool16 isSectionLevelItemItemPresent = kFalse;
			int32 isComponentAttributePresent = 0;
			vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
												  //values inside it for spraying 'Quantity' & 'Availability' attributes.
			vector<vector<PMString> > Accessory_vec_tablerows; //To store Accessory's 'tableData'
			//Change the attributes of the tableTag.
			//ID..The older ID was -1 which is same as elementID of Product_Number.
			//Change it to someother value so that product_number and table id doesn't conflict.
			//we are already using ID = -101 for item_table_in_tabbed_text format.
			//so ID = -102
		
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("ID"),WideString("-102"));//--------------

			//parentID
			PMString attributeVal("");
			attributeVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentID"),WideString(attributeVal));
			attributeVal.Clear();
			//parentTypeID
			attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentTypeID"),WideString(attributeVal));
			attributeVal.Clear();
			//sectionID
			attributeVal.AppendNumber(PMReal(sectionid));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("sectionID"),WideString(attributeVal));
			

			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("pbObjectId"),WideString(attributeVal));

			//rowno of Table tag will now have the number of rows in repetating pattern excluding Header rows.
			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(totalNumberOfRowsBeforeRowInsertion));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("rowno"),WideString(attributeVal));
			attributeVal.Clear();

			//--------------
			VectorScreenTableInfoPtr tableInfo=NULL;
			TableSourceInfoValue *tableSourceInfoValueObj;
			bool16 isCallFromTS = kFalse;
			InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
			if(ptrTableSourceHelper != nil)
			{
				tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
				if(tableSourceInfoValueObj)
				{
					isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
					if(isCallFromTS)
					{
						tableInfo=tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
						if(!tableInfo)
							break;
					}
				}
			}
			attributeVal.Clear();
			if(isCallFromTS )
			{
				attributeVal.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(0)));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("tableId"),WideString(attributeVal));
			}
			
			attributeVal.Clear();
			if(pNode.getIsProduct() == 1)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("index"),WideString("3"));
			}
			if(pNode.getIsProduct() == 0)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("index"),WideString("4"));
			}
			
			int32 field_1 = -1;		
			for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
			{//start for tagIndex = 0
				XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
				//This is a tag attached to the cell.
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				//Get the text tag attached to the text inside the cell.
				//We are providing only one texttag inside cell.
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					continue;
				}
				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
				{
					XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
					InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
					//The cell may be blank. i.e doesn't have any text tag inside it.
					if(cellTextTagPtr == NULL)
					{
						continue;
					}

					PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
					field_1 = fld_1.GetAsNumber();

					//Now check the typeId attribute of the cellTextTag.
					if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1")
						//The below line added on 11July...the serial blast day.
						//for spraying the whole item preset table inside table cell.
						|| cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103")) && HeaderRowPresent == kFalse)
					{
						//CA("header present");
						tableHeaderPresent = kTrue;
					}
					
					if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
					{
						isSectionLevelItemItemPresent = kTrue;
						if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-901") )
							isComponentAttributePresent = 1;
						if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-902") )
							isComponentAttributePresent = 2;
						if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-903") )
							isComponentAttributePresent = 3;
					}
					
					//change the other necessary attributes of the the cellTextTag
					//parentID
					PMString attributeVal("");
					attributeVal.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attributeVal));
					attributeVal.Clear();

					PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
					double ID = id.GetAsDouble();
											
					//parentTypeID
					if(ID != -701 && ID != -702 && ID != -703 && ID != -704)
					{
						attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentTypeID"),WideString(attributeVal));
						attributeVal.Clear();
					}
					attributeVal.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("sectionID"),WideString(attributeVal));
					
					attributeVal.Clear();
					attributeVal.AppendNumber(PMReal(slugInfo.tableType));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableType"),WideString(attributeVal));
					
					attributeVal.Clear();
					attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("pbObjectId"),WideString(attributeVal));

				}
			}//end for tagIndex = 0 


			if(isSectionLevelItemItemPresent == kFalse)
			{
				for(int32 localTagIndex = 0;localTagIndex < slugInfo.tagPtr->GetChildCount();localTagIndex++)
				{//for..iterate through each column								
					XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(localTagIndex);
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					
					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
					{						
						continue;
					}
					int32 cellChildCount = cellXMLElementPtr->GetChildCount();

					PMString child("cellChildCount = ");
					child.AppendNumber(cellChildCount);
					//CA(child);

					for(int32 tagIndex2 = 0;tagIndex2 < cellChildCount;tagIndex2++)
					{

						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex2);
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == NULL)
						{
							//CA("cellTextXMLElementPtr5 == NULL");
							continue;
						}
					
						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
						PMString strimageFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
						PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						PMString strChildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

						int32 header = strHeader.GetAsNumber();	
						double childId = strChildId.GetAsDouble();
						int32 childTag = strChildTag.GetAsNumber();
						double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();
						double typeId = strTypeID.GetAsDouble();
						double parentTypeID = strParentTypeID.GetAsDouble();

						PMString strheaderFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						
						PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
						int32 isEventFieldVal = isEventFieldStr.GetAsNumber();

						PMString dispName("");
						if(cellTextXMLElementPtr->GetAttributeValue(WideString("index")) == WideString("3"))
						{
							//CA("inside");
							makeTheTagImpotent(cellTextXMLElementPtr);
						}
						else if(strElementID == "-103" )
						{//CA("strElementID == -103");
							//This is ItemTable Header
							do
							{
								VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
								if(typeValObj==NULL)
								{	//CA("typeValObj==NULL");
									break;
								}

								VectorTypeInfoValue::iterator it1;

								bool16 typeIDFound = kFalse;
								for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
								{	
									if(typeId == it1->getTypeId())
									{
										typeIDFound = kTrue;
										break;
									}			
								}
								if(typeIDFound)
									dispName = it1->getName();
								
								if(typeValObj)
									delete typeValObj;					
							}while(kFalse);
						}//end of tag if
						else if(tableFlag == "0" && header == 1)
						{
							if(elementID == -701 || elementID == -702)
							{
								PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
								double parentTypeID = strParentTypeID.GetAsDouble();
								
								/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
								if(TypeInfoVectorPtr != NULL)
								{
									VectorTypeInfoValue::iterator it3;
									int32 Type_id = -1;
									PMString temp = "";
									PMString name = "";
									for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									{
										Type_id = it3->getTypeId();
										if(parentTypeID == Type_id)
										{
											temp = it3->getName();
											if(elementID == -701)
											{
												dispName = temp;
											}
											else if(elementID == -702)
											{
												dispName = temp + " Suffix";
											}
										}
									}
								}
								
								if(TypeInfoVectorPtr)
									delete TypeInfoVectorPtr;*/

							}
							else if(elementID == -703)
							{
								dispName = "$Off";
							}
							else if(elementID == -704)
							{
								dispName = "%Off";
							}
							//upto here added by Tushar on 6/1/07
							//dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID,kTrue);
							//cellTextXMLElementPtr->SetAttributeValue("typeId","-2");
							//added on 22Sept..EventPrice addition.
							/*if(elementID == -701)
								dispName.SetCString("Event Price");
							else if(elementID == -702)
								dispName.SetCString("Suffix");
							else if(elementID == -703)
								dispName.SetCString("$Off");
							else if(elementID == -704)
								dispName.SetCString("%Off");*/
							//ended on 22Sept..EventPrice addition.
							else
								dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
							
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("parentID"),WideString(attrVal));
							attrVal.Clear();

							attrVal.AppendNumber(PMReal(pNode.getTypeId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("parentTypeId"),WideString(attrVal));
							attrVal.Clear();
										
						}
						else if(strElementID == "-104")
						{
							//CA("strElementID == -104");
							//This is ProductItemTable data
							//Product ItemTable
							PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							TagStruct tagInfo;												
							tagInfo.whichTab = strIndex.GetAsNumber();
							//This is just an adjustment.
							tagInfo.parentId = pNode.getPBObjectID();
							tagInfo.isTablePresent = tableFlag.GetAsNumber();
							tagInfo.typeId = typeId;
							tagInfo.sectionID = sectionid;								
							GetItemTableInTabbedTextForm(tagInfo,dispName);
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("ID"),WideString("-104"));

							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("pbObjectId"),WideString(attrVal));
						}							
						
						else if(strimageFlag == "1")
						{	
							XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
							UIDRef ref = cntentREF.GetUIDRef();
							if(ref == UIDRef::gNull)
							{
								ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::strimageFlag == 1: ref == UIDRef::gNull");
								continue;
							}
																	
							InterfacePtr<ITagReader> itagReader
							((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
							if(!itagReader)
								continue ;

							TagList tList=itagReader->getTagsFromBox(ref);
							TagStruct tagInfoo;
							int numTags=static_cast<int>(tList.size());
							if(numTags<0)
							{
								continue;
							}

							tagInfoo=tList[0];

							tagInfoo.parentId=pNode.getPubId();
							tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
							tagInfoo.sectionID=CurrentSubSectionID;

							IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
							if(!iDataSprayer)
							{	
								//CA("!iDtaSprayer");
								continue;
							}
							if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
								|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
								|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
								|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
							{
								iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, tagInfoo.parentId);
							}
							else if(tagInfoo.elementId > 0)
							{
								isInlineImage = kTrue;
								iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo, pNode.getPubId(),kTrue);			
								iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
							}
							else
							{
								iDataSprayer->sprayItemImage(ref,tagInfoo,pNode ,pNode.getPubId());
							}
							
							
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
							continue;
						}
						else if(tableFlag == "0")
						{
							//CA("tableFlag == 0");
							if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
							{
								TagStruct tagInfo;
								tagInfo.elementId = elementID;
								tagInfo.tagPtr = cellTextXMLElementPtr;
								tagInfo.typeId = pNode.getPubId();
								tagInfo.parentTypeID = parentTypeID;
								tagInfo.childId = childId;
								tagInfo.childTag = childTag;
								tagInfo.header = header;
								handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
							}
							//ended on 22Sept..EventPrice addition.	
							else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
							{
								/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),elementID,CurrentSubSectionID);
								if(vecPtr != NULL){
									if(vecPtr->size()> 0){
										dispName = vecPtr->at(0).getObjectValue();	
									}
								}*/
							}
							else if(elementID == -401 || elementID == -402 || elementID == -403) //----For Make, Mode, Year.
							{
								dispName = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(pNode.getPubId(),elementID, languageID);
							}
							else
							{
								dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID, CurrentSectionID,kFalse);
							}
							
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("childId"),WideString(attrVal));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("parentID"),WideString(attrVal));
							attrVal.Clear();
						}
						
						int32 delStart = -1, delEnd = -1;
						bool16 shouldDelete = kFalse;
						tagStartPos = -1;
						tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;
					
						if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
						{//if(tagInfo.rowno == -117)
								delStart = tagStartPos-1;
							delEnd = tagEndPos;
							if(dispName == "")						
								shouldDelete = kTrue;
						}

						TextIndex position = 0;
						TextIndex length = 0;

						if(shouldDelete == kTrue )
						{
							int32 tagStartPos1 = -1;
							int32 tagEndPos1 = -1;

							if(deleteCount >0)
								CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
						
							deleteCount++;

							XMLReference perentXMLRef = cellTextXMLElementPtr->GetParent();
							InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
							if(!perentXMLElement)
								continue;
							Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);

							{
								TextIterator begin1(textModel , delStart);
								TextIterator end1(textModel , tagStartPos1);
								int32 removethis = 0;
								bool16 prevEnterFound = kFalse;
								for(TextIterator iter = begin1 ;iter > end1 ; iter--)
								{
									
									if(*iter == kTextChar_CR)
									{
										prevEnterFound = kTrue;
										break;
									}
									removethis++;
										
								}
								if(prevEnterFound)
								{
									//CA("prevEnterFound == kTrue 2222");
									//delStart = delStart - removethis + 1;
								}
								else
								{
									//CA("prevEnterFound == kFalse 2222");
									//delStart = tagStartPos1 ;
								}
							}

							TextIterator begin(textModel , delStart);
							TextIterator end(textModel , tagEndPos1);
							int32 addthis = 0;
							bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter < end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									enterFound = kTrue;
									break;
								}

									
							}
							if(enterFound)
							{
								//CA("enterFound == kTrue 1111");
								//delEnd = delStart/*tagEndPos*/ + addthis;
							}
							else
							{
								//CA("enterFound == kFalse 1111");
								//delEnd = tagEndPos1 ;//-1
							}
                            ///find enter char before current tag
							bool16 needToAddInVec = kTrue;
							
							if(needToAddInVec == kTrue){
								
								RangeData rangeData(delStart,delEnd+1);
								deleteTextRanges.push_back(rangeData);

								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("deleteIfEmpty"), WideString("2"));
								continue;
							}

						}

						
						PMString textToInsert("");
						//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						//if(!iConverter)
						{
							textToInsert=dispName;					
						}
						//else
						//	textToInsert=iConverter->translateString(dispName);

						//CA("textToInsert = "+ textToInsert);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
					}
				}//end for..iterate through each row of repeatedRowPattern.

				
				
				int32 vec_size =static_cast<int32>(deleteTextRanges.size());
				if(vec_size > 0)
				{
					for(int32 i = 0 ; i < vec_size; i++)
					{
						if(i >0)
								CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

						if(textModel != NULL)
						{
							InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
							if (textModelCmds != NULL) {

								InterfacePtr<ICommand> deteteCmd(textModelCmds->DeleteCmd(deleteTextRanges[vec_size-i-1], kTrue));
								if (deteteCmd != NULL) {

									PMString temp("deleted  = ");
									temp.AppendNumber(i);
									temp.Append(" , delStart =  ");
									RangeData::Lean l = RangeData::kLeanForward;
									temp.AppendNumber(deleteTextRanges[vec_size-i-1].Start(&l));
									temp.Append(" , delEnd =  ");
									temp.AppendNumber(deleteTextRanges[vec_size-i-1].End());

									ErrorCode err = CmdUtils::ProcessCommand(deteteCmd);
									if(err == kFailure)
									{
										//CA("not deleted ................................");
										//CA(temp);
									}
									else
									{
										//CA("deleted");
										//CA(temp);
									}
								}
							}
						}
					}


					for(int32 index = slugInfo.tagPtr->GetChildCount() - 1 ; index >= 0 ; index--)
					{
						XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(index);
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
												
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)												
							continue;
						
						int32 cellChildCount = cellXMLElementPtr->GetChildCount();

						for(int32 cellChildIndex = cellChildCount - 1; cellChildIndex >= 0; cellChildCount = cellXMLElementPtr->GetChildCount() , cellChildIndex--)
						{
							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(cellChildIndex);
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
							if(cellTextXMLElementPtr == NULL)
							{
								//CA("cellTextXMLElementPtr5 == NULL");
								continue;
							}						
						
							if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("2"))
							{
								if(cellChildIndex == cellChildCount - 1)//if its a last tag inside a cell
								{
									int32 lastTagStartPos = -1;
									int32 lastTagEndPos = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr, &lastTagStartPos, &lastTagEndPos);
								
									XMLReference perentXMLRef = cellTextXMLElementPtr->GetParent();
									InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
									if(!perentXMLElement)
										continue;

									int32 CellTagStartPos = -1;
									int32 CellTagEndPos = -1; 

									Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&CellTagStartPos,&CellTagEndPos);

									delStart = lastTagStartPos; 
									{
										TextIterator begin1(textModel , delStart);
										TextIterator end1(textModel , CellTagStartPos);
										
										int32 removethis = 0;
										bool16 prevEnterFound = kFalse;
										for(TextIterator iter = begin1 ;iter > end1 ; iter--)
										{
											
											if(*iter == kTextChar_CR)
											{
												prevEnterFound = kTrue;
												break;
											}
											removethis++;
												
										}
										if(prevEnterFound)
										{
											delStart = delStart - removethis + 1;
										}
										else
										{
											//CA("prevEnterFound == kFalse 2222");
											//delStart = tagStartPos1 ;
										}
									}

									delEnd = lastTagEndPos;
									{
										TextIterator begin(textModel , delStart);
										TextIterator end(textModel , CellTagEndPos);
										
										int32 addthis = 0;
										bool16 nextEnterFound = kFalse;
										for(TextIterator iter = begin ;iter < end ; iter++)
										{
											addthis++;
											if(*iter == kTextChar_CR)
											{
												nextEnterFound = kTrue;
												break;
											}

												
										}
										if(nextEnterFound)
										{
											//CA("enterFound == kTrue 1111");
											delEnd = delEnd + addthis;
										}
										else
										{
											//CA("enterFound == kFalse 1111");
											delEnd = CellTagEndPos ;//-1
										}
									}
									
									if(cellChildCount > 1)
									{
										int32 tempChildIndex = cellChildIndex;
										while(tempChildIndex < cellChildCount )
										{
											XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tempChildIndex -1);
											InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
											if(cellTextXMLElementPtr == NULL)
											{
												//CA("cellTextXMLElementPtr5 == NULL");
												break;
											}						
										
											int32 tagStartPos = -1;
											int32 tagEndPos = -1; 

											Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);

											if(tagStartPos > delStart && tagEndPos < delEnd && cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("2"))
											{
												--cellChildIndex;
												--tempChildIndex;
												continue;
											}
											else
											{
												delStart = tagEndPos + 2;
												break;
											}
											
										}

									}

									int32 st,e;
									
									if(cellChildCount == 1)
									{
										st = delStart;
										e = delEnd;
									}
									else
									{
										st = delStart - 1;
										e = delEnd;
									}
									RangeData rangeData(st,e);
									if(textModel != NULL)
									{
										InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
										if (textModelCmds != NULL) {

											InterfacePtr<ICommand> deteteCmd(textModelCmds->DeleteCmd(rangeData, kTrue));
											if (deteteCmd != NULL) {

												ErrorCode err = CmdUtils::ProcessCommand(deteteCmd);
												if(err == kFailure)
												{
													//CA("not deleted ................................");
													//CA(temp);
												}
												else
												{
													//CA("deleted");
													//CA(temp);
												}												
											}
										}
									}
								}
								else									
								{
									int32 CurrentTagStartPos = -1;
									int32 CurrentTagEndPos = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr, &CurrentTagStartPos, &CurrentTagEndPos);
								
									XMLReference perentXMLRef = cellTextXMLElementPtr->GetParent();
									InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
									if(!perentXMLElement)
										continue;

									int32 CellTagStartPos = -1;
									int32 CellTagEndPos = -1; 

									Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&CellTagStartPos,&CellTagEndPos);

									delStart = CurrentTagStartPos; 
									{
										TextIterator begin1(textModel , delStart);
										TextIterator end1(textModel , CellTagStartPos);
										
										int32 removethis = 0;
										bool16 prevEnterFound = kFalse;
										for(TextIterator iter = begin1 ;iter > end1 ; iter--)
										{
											
											if(*iter == kTextChar_CR)
											{
												prevEnterFound = kTrue;
												break;
											}
											removethis++;
												
										}
										if(prevEnterFound)
										{
											//CA("prevEnterFound == kTrue 2222");
											delStart = delStart - removethis + 1;
										}
										else
										{
											//CA("prevEnterFound == kFalse 2222");
											//delStart = tagStartPos1 ;
										}
									}

									delEnd = CurrentTagEndPos + 1;
									
									if(cellChildCount > 1)
									{
										int32 tempChildIndex = cellChildIndex;
										while(tempChildIndex < cellChildCount && tempChildIndex > 0)
										{
											XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tempChildIndex -1);
											InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
											if(cellTextXMLElementPtr == NULL)
											{
												//CA("cellTextXMLElementPtr5 == NULL");
												break;
											}						
										
											int32 tagStartPos = -1;
											int32 tagEndPos = -1; 

											Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);

											if(tagStartPos < delStart && tagEndPos < delStart && cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("2"))
											{
												delStart = tagStartPos + 1;
												--cellChildIndex;
												--tempChildIndex;
												continue;
											}
											else
											{
												delStart = tagEndPos + 1;
												break;
											}
											
										}

									}

									int32 st,e;
									
									if(cellChildCount == 1)
									{
										st = delStart;
										e = delEnd;
									}
									else
									{
										st = delStart - 1;
										e = delEnd;
									}
									RangeData rangeData(st,e);
									if(textModel != NULL)
									{
										InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
										if (textModelCmds != NULL) {

											InterfacePtr<ICommand> deteteCmd(textModelCmds->DeleteCmd(rangeData, kTrue));
											if (deteteCmd != NULL) {
												
												ErrorCode err = CmdUtils::ProcessCommand(deteteCmd);
												if(err == kFailure)
												{
													//CA("not deleted ................................");
													//CA(temp);
												}
												else
												{
													//CA("deleted");
													//CA(temp);
												}												
											}
										}
									}
								}
							}
						}
					}
				}
				
				InterfacePtr<IDataSprayer> iDataSprayer((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!iDataSprayer)
				{	
					//CA("!iDtaSprayer");
					continue;
				}
				UIDRef tempUIDRef = boxUIDRef;				
				iDataSprayer->fitInlineImageInBoxInsideTableCell(tempUIDRef);

				attributeVal.Clear();
				attributeVal.AppendNumber(PMReal(field_1));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("field1"),WideString(attributeVal));				
				continue;
			}
		
				
			if(isComponentAttributePresent == 0)//Normal case
			{
				if(isCallFromTS )
				{
					CItemTableValue oTableValue;
					vector<double> vec_items;
					FinalItemIds.clear();
					
					bool16 tableFound = kFalse;
					VectorScreenTableInfoValue::iterator it;
					if(!tableInfo)
					{
						makeTagInsideCellImpotent(slugInfo.tagPtr); 
						break;
					}
					
					if(tableInfo->size()==0)
					{ 
						makeTagInsideCellImpotent(slugInfo.tagPtr); 
						break;
					}
					for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
					{				
						oTableValue = *it;		

						if(oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
						{
							vec_items = oTableValue.getItemIds();
							FinalItemIds = vec_items;
							tableFound = kTrue;
							break;
						}
					}									
					if(tableFound == kFalse)
						return kFalse;
				}
				else
				{
					VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, slugInfo.languageID );
					if(!tableInfo)
					{
						//CA("tableinfo is NULL");
						makeTagInsideCellImpotent(slugInfo.tagPtr);
						break;
					}
					
					if(tableInfo->size()==0)
					{
						//CA("tableinfo size==0");
						makeTagInsideCellImpotent(slugInfo.tagPtr);
						break;
					}
					CItemTableValue oTableValue;
					VectorScreenTableInfoValue::iterator it;

					bool16 typeidFound=kFalse;
					vector<double> vec_items;
					FinalItemIds.clear();

					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						//for tabelInfo start
						oTableValue = *it;				
						vec_items = oTableValue.getItemIds();
					
						if(field_1 == -1)
						{
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}
						else
						{
							if(field_1 != oTableValue.getTableTypeID()){
								//CA("field_1 != oTableValue.getTableTypeID()");
								continue;
							}
							//CA("got the table");
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}

					}//for tabelInfo end
					
					if(tableInfo)
						delete tableInfo;
				}
			}
			//else  // For component table item attributes.
			//{
			//	//CA("isComponentTableAttributePresent == kTrue");
			//	do
			//	{
			//		CItemTableValue oTableValue;
			//		VectorScreenTableInfoValue::iterator it  ;	

			//		VectorScreenTableInfoPtr KittableInfo = NULL;
			//		VectorScreenTableInfoPtr AccessoryTableInfo = NULL;

			//		if(isComponentAttributePresent == 1)// we are spraying component table.
			//		{
			//			bool16 isKitTable = kFalse; 
			//			KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(pNode.getPubId(), languageId, isKitTable); 
			//			if(!KittableInfo)
			//			{
			//				//CA("KittableInfo is NULL");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
			//				break;
			//			}

			//			if(KittableInfo->size()==0){
			//				//CA(" KittableInfo->size()==0");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
			//				break;
			//			}
			//			it = KittableInfo->begin();
			//		}
			//		else if(isComponentAttributePresent == 2)// we are spraying XRef table.
			//		{
			//			KittableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(pNode.getPubId(), languageId); 
			//			if(!KittableInfo)
			//			{
			//				//CA("AccessoryTableInfo is NULL");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr); 
			//				break;
			//			}

			//			if(KittableInfo->size()==0){
			//				//CA(" AccessoryTableInfo->size()==0");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr);
			//				break;
			//			}
			//			it = KittableInfo->begin();
			//		}
			//		else if(isComponentAttributePresent == 3)// we are spraying Accessory table.
			//		{
			//			AccessoryTableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(pNode.getPubId(), languageId); 
			//			if(!AccessoryTableInfo)
			//			{
			//				//CA("AccessoryTableInfo is NULL");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
			//				break;
			//			}

			//			if(AccessoryTableInfo->size()==0){
			//				//CA(" AccessoryTableInfo->size()==0");
			//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
			//				break;
			//			}
			//			it = AccessoryTableInfo->begin();
			//		}
		

			//		bool16 typeidFound=kFalse;
			//		vector<int32> vec_items;
			//		FinalItemIds.clear();

			//		{//for tabelInfo start				
			//			oTableValue = *it;				
			//			vec_items = oTableValue.getItemIds();
			//					
			//			if(FinalItemIds.size() == 0)
			//			{
			//				FinalItemIds = vec_items;
			//			}
			//			else
			//			{
			//				for(int32 i=0; i<vec_items.size(); i++)
			//				{	bool16 Flag = kFalse;
			//					for(int32 j=0; j<FinalItemIds.size(); j++)
			//					{
			//						if(vec_items[i] == FinalItemIds[j])
			//						{
			//							Flag = kTrue;
			//							break;
			//						}				
			//					}
			//					if(!Flag)
			//						FinalItemIds.push_back(vec_items[i]);
			//				}
			//			}
			//		}//for tabelInfo end
			//		
			//		if((isComponentAttributePresent == 1) || (isComponentAttributePresent == 2))
			//			Kit_vec_tablerows = oTableValue.getTableData();
			//		else if(isComponentAttributePresent == 3)
			//			Accessory_vec_tablerows = oTableValue.getTableData();

			//		if(KittableInfo)
			//			delete KittableInfo;
			//		if(AccessoryTableInfo)
			//			delete AccessoryTableInfo;
			//	}while(0);

			//}
			
			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(field_1));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("field1"),WideString(attributeVal));

			int32 TotalNoOfItems =static_cast<int32>(FinalItemIds.size());
			if(TotalNoOfItems <= 0)
			{
				//CA("TotalNoOfItem <= 0");
				makeTagInsideCellImpotent(slugInfo.tagPtr);
				break;
			}

			GridArea bodyArea = tableModel->GetBodyArea();
			int32 bodyCellCount = 0;
			ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
			ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
			while (iterTable1 != endTable1)
			{
				bodyCellCount++;
				++iterTable1;
			}
						
			int32 rowPatternRepeatCount =static_cast<int32>(FinalItemIds.size());			
			//int32 rowsToBeStillAdded = TotalNoOfItems - totalNumberOfRowsBeforeRowInsertion;
			//we have to deal with 2 cases
			//case 1: when tableHeaders are absent
			//then the total number of rows i.e rowCount = FinalItemIds.size()-1
			//case 2 : when tableHeaders are present
			//then the total number of rows i.e rowCount = FinalItemsIds.size()
			if(tableHeaderPresent)
			{  	
				//CA("tableHeaderPresent");
				//rowsToBeStillAdded = rowsToBeStillAdded + 1;
				rowPatternRepeatCount = rowPatternRepeatCount + 1;
			}
			int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				break;
			}
			
			if(HeaderRowPresent)
			{
				//CA("HeaderRowPresent");
				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
				}

			if(rowPatternRepeatCount > 1) //this if is just to make sure that Paste gets call after copy
				{
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
						break;
					}
					
					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
						break;
					}
					
					for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						tableModel->Paste(GridAddress((pasteIndex * totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);

						if(pasteIndex < rowPatternRepeatCount - 1)
						{
							tempPtr = tableModel->Copy(gridAreaForEntireTable); //og
						}
					}
				}
			}
			else
			{
				//CA("HeaderRowPresent==kFalse");
				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);

				}
				
			
			   if(rowPatternRepeatCount > 1)
			   {
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						//CA("canCopy == kFalse");
						break;
					}
					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable) == NULL");
						break;
					}
				
					for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
						tempPtr = tableModel->Copy(gridAreaForEntireTable);
					}
				}
			}
			//return kSuccess;
			// int32 rowCount;
			//Note the tags are numbered continuously.and not in rows and columns fashion.
			//its our responsibility to find the exact row and exact column of the tag.
				
			int32 nonHeaderRowPatternIndex = 0;
			
			if(HeaderRowPresent)
			{
				GridArea headerArea = tableModel->GetHeaderArea();
				ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
				ITableModel::const_iterator endTable(tableModel->end(headerArea));
				while (iterTable != endTable)
				{
					GridAddress gridAddress = (*iterTable);         
					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
					if(cellContent == nil) 
					{
						//CA("cellContent == nil");
						break;
					}
					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
					if(!cellXMLReferenceData) {
						//CA("!cellXMLReferenceData");
						break;
					}
					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
					++iterTable;

						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							continue;
						}
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								continue;
							}

							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
							PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
							PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));///11/05/07
							PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString strDataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
							PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
							PMString strChildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
							PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

							int32 header = strHeader.GetAsNumber();	
							double childId = strChildId.GetAsDouble();
							int32 childTag = strChildTag.GetAsNumber();	
							double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
													
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							PMString dispName("");
							////if normal tag present in header row...
							////then we are spraying the value related to the first item.
							double itemID = FinalItemIds[0];

							if((strDataType == "4" || ((strDataType == "5" )  )) && isCallFromTS)	//-------------
							{
								//CA("Going to spray table Name");
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
								for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
								{				
									oTableValue = *it;		
									if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
									{
										if(elementID == -121)
										{
											dispName = oTableValue.getName();
										}
										else if(elementID == -983) //---List Description
										{
											dispName =oTableValue.getDescription();
										}
										else if( elementID == -982)//---Stencil Name
										{
											dispName = oTableValue.getstencil_name();
										}
										else
										{
											dispName = oTableValue.getName();
											attributeVal.Clear();
											attributeVal.AppendNumber(PMReal(oTableValue.getTableTypeID()));
											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attributeVal));
										}

										attributeVal.Clear();
										attributeVal.AppendNumber(PMReal(oTableValue.getTableID()));
										cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
										break;
									}
								}

							}
							else if((strDataType == "4" ) || ( (strDataType == "5" )  ) && !isCallFromTS )
							{
								//CA("strdataType == 4 && elementID == -121");
								VectorScreenTableInfoPtr table_Info = NULL;

									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
								table_Info= ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, slugInfo.languageID ); // getting only customtable data

								if(!table_Info)
								{
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::::!tableInfo");
									return kFalse;
								}

								if(table_Info->size()==0){
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
									delete table_Info;
									break;
								}
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
								for(it = table_Info->begin() ; it != table_Info->end() ; it++)
								{				
									oTableValue = *it;
									PMString strTableId;
									if(elementID == -121)
									{
										strTableId.Clear();
										strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));
										dispName = oTableValue.getName();
										break;
									}
									else if(strDataType == "5") 
									{
										if(elementID == -983)//---List Description
										{
										//	CA("tagInfo.elementId == -983");
											dispName = oTableValue.getDescription();	
											strTableId.Clear();
											strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));
										}
										else if(elementID == -982)//---Stencil Name
										{
										//	CA("tagInfo.elementId == -982");
											dispName = oTableValue.getstencil_name();
											strTableId.Clear();
											strTableId.AppendNumber(PMReal(oTableValue.getTableID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTableId));
										}
										break;
													
									}
								}
							}
							else if(strTypeId == "-1")
							{
								double parentTypeID = strParentTypeID.GetAsDouble();

								//CA("#################normal tag present in header row###############");
								if(tableFlag == "1")
								{
									//The following code will spray the table preset inside the table cell.
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")// || index == "4")
									{//Product ItemTable
										PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
										TagStruct tagInfo;												
										tagInfo.whichTab = index.GetAsNumber();
										if(index == "3")
											tagInfo.parentId = pNode.getPubId();
										/*else if(index == "4")
											tagInfo.parentId = pNode.getPBObjectID();*/

										tagInfo.isTablePresent = tableFlag.GetAsNumber();
										tagInfo.typeId = typeID.GetAsDouble();
										tagInfo.sectionID = sectionid;
																					
										GetItemTableInTabbedTextForm(tagInfo,dispName);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));
									}
									else if(index == "4")
									{//This is the Section level ItemTableTag and we have selected the product
									//for spraying
										makeTheTagImpotent(cellTextXMLElementPtr);										
									}
									
								}

									
								else if(imgFlag == "1")
								{							
									XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
									UIDRef ref = cntentREF.GetUIDRef();
									if(ref == UIDRef::gNull)
									{
										//CA("ref == UIDRef::gNull");
										continue;
									}

									InterfacePtr<ITagReader> itagReader
									((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
									if(!itagReader)
										continue ;
									
									TagList tList=itagReader->getTagsFromBox(ref);
									TagStruct tagInfoo;
									int numTags=static_cast<int>(tList.size());

									if(numTags<0)
									{
										continue;
									}

									tagInfoo=tList[0];

									tagInfoo.parentId = pNode.getPubId();
									tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
									tagInfoo.sectionID=CurrentSubSectionID;

									IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
									if(!iDataSprayer)
									{	
										//CA("!iDtaSprayer");
										continue;
									}

									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
									
									PMString attrVal;
									attrVal.AppendNumber(PMReal(itemID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
										
									for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
									{
										tList[tagIndex].tagPtr->Release();
									}
									continue;
								}
								else if(tableFlag == "0")
								{
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index =="4")
									{//Item
										if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) != WideString("1"))
											itemID = pNode.getPubId();

										if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
										{
											TagStruct tagInfo;
											tagInfo.elementId = elementID;
											tagInfo.tagPtr = cellTextXMLElementPtr;
											tagInfo.typeId = itemID;
											tagInfo.parentTypeID = parentTypeID;
											tagInfo.childId = childId;
											tagInfo.childTag = childTag;
											tagInfo.header = header;
											handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
										}
										else if(elementID == -803)  // For 'Letter Key'
										{
											dispName = "a" ;			// For First Item										
										}
										else if((isComponentAttributePresent == 1) && (elementID == -805) )  // For 'Quantity'
										{
											dispName = (Kit_vec_tablerows[0][0]);
										}
										else if((isComponentAttributePresent == 1) && (elementID == -806) )  // For 'Availability'
										{
											dispName = (Kit_vec_tablerows[0][1]);			
										}
										else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
										{
											dispName = (Kit_vec_tablerows[0][0]);			
										}
										else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
										{
											dispName = (Kit_vec_tablerows[0][1]);			
										}
										else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
										{
											dispName = (Kit_vec_tablerows[0][2]);			
										}
										else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
										{
											dispName = (Kit_vec_tablerows[0][3]);			
										}
										else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
										{
											dispName = (Kit_vec_tablerows[0][4]);			
										}
										else if((isComponentAttributePresent == 3) && (elementID == -812) )  // For 'Quantity'
										{
											dispName = (Accessory_vec_tablerows[0][0]);
										}
										else if((isComponentAttributePresent == 3) && (elementID == -813) )  // For 'Required'
										{
											dispName = (Accessory_vec_tablerows[0][1]);
										}
										else if((isComponentAttributePresent == 3) && (elementID == -814) )  // For 'Comments'
										{
											dispName = (Accessory_vec_tablerows[0][2]);
										}
										else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[0][4]) == "N"))
										{
											dispName = ""; //ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
										}
										else if(elementID == -827)  // For 'Number Key'
										{
											dispName = "1" ;
										}
										else
										{
											dispName =  ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);										
											//dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);  // getting single item attributye data at a time.
										}
										
										PMString attrVal;
										attrVal.AppendNumber(PMReal(itemID));
										if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) == WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) == WideString("-101"))
										{
											//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	 										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
										}
										else
										{
											PMString attrVal("");
											attrVal.AppendNumber(PMReal(pNode.getPubId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
										}
									}
									else if( index == "3")
									{//Product
										
										dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID, CurrentSectionID, kTrue);
										PMString attrVal;
										attrVal.AppendNumber(PMReal(pNode.getTypeId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
										attrVal.Clear();

										attrVal.AppendNumber(PMReal(pNode.getPubId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									}
								}								
							}
							else if(imgFlag == "1")
							{							
								XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
								UIDRef ref = cntentREF.GetUIDRef();
								if(ref == UIDRef::gNull)
								{
									//CA("ref == UIDRef::gNull");
									continue;
								}

								InterfacePtr<ITagReader> itagReader
								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
								if(!itagReader)
									continue ;
								
								TagList tList=itagReader->getTagsFromBox(ref);
								TagStruct tagInfoo;
								int numTags=static_cast<int>(tList.size());

								if(numTags<0)
								{
									continue;
								}

								tagInfoo=tList[0];

								tagInfoo.parentId = pNode.getPubId();
								tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
								tagInfoo.sectionID=CurrentSubSectionID;

								IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
								if(!iDataSprayer)
								{	
									CA("!iDtaSprayer");
									continue;
								}

								iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
								
								PMString attrVal;
								attrVal.AppendNumber(PMReal(itemID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
								
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
								
								continue;
							}
							else
							{							
								if(tableFlag == "1")
								{
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")
									{
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-103"));										
										do
										{
											PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
		
											double typeId = strTypeID.GetAsDouble();
											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
											if(typeValObj==NULL)
											{
												ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
												break;
											}

											VectorTypeInfoValue::iterator it1;

											bool16 typeIDFound = kFalse;
											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
											{	
												if(typeId == it1->getTypeId())
													{
														typeIDFound = kTrue;
														break;
													}			
											}
											if(typeIDFound)
												dispName = it1->getName();

											if(typeValObj)
												delete typeValObj;
										}while(kFalse);
												
									}
									//This is a special case.The tag is of product but the Item is
									//selected.So make this tag unproductive for refresh.(Impotent)
									else if(index == "4")
									{
                                        makeTheTagImpotent(cellTextXMLElementPtr);
									}
								}
								else
								{
						
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")
									{//For product
										CElementModel  cElementModelObj;
										bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
										if(result)
											dispName = cElementModelObj.getDisplayName();
										PMString attrVal;
										attrVal.AppendNumber(PMReal(pNode.getTypeId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
																			
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableFlag"),WideString("-13"));
									}
									else if(index == "4")
									{
										if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
										{
											PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
											double parentTypeID = strParentTypeID.GetAsDouble();
											
											/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
											if(TypeInfoVectorPtr != NULL)
											{
												VectorTypeInfoValue::iterator it3;
												int32 Type_id = -1;
												PMString temp = "";
												PMString name = "";
												for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
												{
													Type_id = it3->getTypeId();
													if(parentTypeID == Type_id)
													{
														temp = it3->getName();
														if(elementID == -701)
														{
															dispName = temp;
														}
														else if(elementID == -702)
														{
															dispName = temp + " Suffix";
														}
													}														
												}													
											}
											if(TypeInfoVectorPtr)
												delete TypeInfoVectorPtr;*/
										}
										else if(elementID == -703)
										{
											dispName = "$Off";
										}
										else if(elementID == -704)
										{
											dispName = "%Off";
										}
										else if((elementID == -805) )  // For 'Quantity'
										{
											//CA("Quantity Header");
											dispName = "Quantity";
										}
										else if((elementID == -806) )  // For 'Availability'
										{
											dispName = "Availability";			
										}
										else if(elementID == -807)
										{									
											dispName = "Cross-reference Type";
										}
										else if(elementID == -808)
										{									
											dispName = "Rating";
										}
										else if(elementID == -809)
										{									
											dispName = "Alternate";
										}
										else if(elementID == -810)
										{									
											dispName = "Comments";
										}
										else if(elementID == -811)
										{									
											dispName = "";
										}
										else if((elementID == -812) )  // For 'Quantity'
										{
											//CA("Quantity Header");
											dispName = "Quantity";
										}
										else if((elementID == -813) )  // For 'Required'
										{
											//CA("Required Header");
											dispName = "Required";
										}
										else if((elementID == -814) )  // For 'Comments'
										{
											//CA("Comments Header");
											dispName = "Comments";
										}

										else
											dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

										//Modification of XMLTagAttributes.
										//modify the XMLTagAttribute "typeId" value to -2.
										//We have assumed that there is atleast one stencil with HeaderAttirbute
										//true .So assign for all the stencil in the header row , typeId == -2
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("1"));
									}									
								}
							}

							int32 delStart = -1, delEnd = -1;
							bool16 shouldDelete = kFalse;
							if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
							{//if(tagInfo.rowno == -117)
                                delStart = tagStartPos-1;
								if(dispName == "")							
									shouldDelete = kTrue;
							}
							if(shouldDelete == kTrue )
							{
								int32 tagStartPos1 = -1;
								int32 tagEndPos1 = -1;
								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

								TextIterator begin(textModel , tagEndPos + 1);
								TextIterator end(textModel , tagEndPos1 - 1);
								int32 addthis = 0;
								bool16 enterFound = kFalse;
								for(TextIterator iter = begin ;iter <= end ; iter++)
								{
									addthis++;
									if(*iter == kTextChar_CR)
									{
										enterFound = kTrue;
										break;
									}
										
								}
								if(enterFound)
								{
									//CA("enterFound == kTrue");
									delEnd = tagEndPos + 1 + addthis;
								}
								else
								{
									//CA("enterFound == kFalse");
									delEnd = tagEndPos1;
								}
								DeleteText( textModel,   delStart, delEnd- delStart  + 1);

								continue;
							}

							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//	textToInsert=iConverter->translateString(dispName);

							//CA("textToInsert = " + textToInsert);
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
						}
				}//for..iterate through each row of the headerRowPattern
			
				nonHeaderRowPatternIndex = 1;
				rowPatternRepeatCount++;

			}

				
			if(tableHeaderPresent)
			{
			//First rowPattern  now is of header element.
			//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
				for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
				{//for..iterate through each row of the repeatedRowPattern
                
					for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
					{//for..iterate through each column
						int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
						XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							continue;
						}
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								continue;
							}

							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
							
							double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
							
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							PMString dispName(""); 
							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							if(index == "3")
							{
								makeTheTagImpotent(cellTextXMLElementPtr);
							}					
							else if(tableFlag == "1")
							{	
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("ID"),WideString("-103"));										
								do
								{
									PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
									double typeId = strTypeID.GetAsDouble();
									VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
									if(typeValObj==NULL)
									{
										ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
										break;
									}

									VectorTypeInfoValue::iterator it1;

									bool16 typeIDFound = kFalse;
									for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
									{	
										   if(typeId == it1->getTypeId())
											{
												typeIDFound = kTrue;
												break;
											}			
									}
									if(typeIDFound)
										dispName = it1->getName();

									if(typeValObj)
										delete typeValObj;
								}while(kFalse);
								
							}
							else
							{
								//CA("tableFlag	!= 1");
								if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
								{
									PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
									double parentTypeID = strParentTypeID.GetAsDouble();
									
									/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
									if(TypeInfoVectorPtr != NULL)
									{
										VectorTypeInfoValue::iterator it3;
										int32 Type_id = -1;
										PMString temp = "";
										PMString name = "";
										for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
										{
											Type_id = it3->getTypeId();
											if(parentTypeID == Type_id)
											{
												temp = it3->getName();
												if(elementID == -701)
												{
													dispName = temp;
												}
												else if(elementID == -702)
												{
													dispName = temp + " Suffix";
												}
											}
										}
									}

									if(TypeInfoVectorPtr)
										delete TypeInfoVectorPtr;*/
								}
								else if(elementID == -703)
								{
									dispName = "$Off";
								}
								else if(elementID == -704)
								{
									dispName = "%Off";
								}	
								else if((elementID == -805) )  // For 'Quantity'
								{
									//CA("Quantity Header");
									dispName = "Quantity";
								}
								else if((elementID == -806) )  // For 'Availability'
								{
									dispName = "Availability";			
								}
								else if(elementID == -807)
								{									
									dispName = "Cross-reference Type";
								}
								else if(elementID == -808)
								{									
									dispName = "Rating";
								}
								else if(elementID == -809)
								{									
									dispName = "Alternate";
								}
								else if(elementID == -810)
								{									
									dispName = "Comments";
								}
								else if(elementID == -811)
								{									
									dispName = "";
								}
								else if((elementID == -812) )  // For 'Quantity'
								{
									//CA("Quantity Header");
									dispName = "Quantity";
								}
								else if((elementID == -813) )  // For 'Required'
								{
									//CA("Required Header");
									dispName = "Required";
								}
								else if((elementID == -814) )  // For 'Comments'
								{
									//CA("Comments Header");
									dispName = "Comments";
								}
								else
								{
									//CA("else");
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
									//CA("dispName	:	"+dispName);

								}
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("header"),WideString("1"));
							}
							
							int32 delStart = -1, delEnd = -1;
							bool16 shouldDelete = kFalse;
							if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
							{//if(tagInfo.rowno == -117)
                                delStart = tagStartPos-1;
								if(dispName == "")							
									shouldDelete = kTrue;
							}
							if(shouldDelete == kTrue )
							{
								int32 tagStartPos1 = -1;
								int32 tagEndPos1 = -1;

								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

								TextIterator begin(textModel , tagEndPos + 1);
								TextIterator end(textModel , tagEndPos1 - 1);
								int32 addthis = 0;
								bool16 enterFound = kFalse;
								for(TextIterator iter = begin ;iter <= end ; iter++)
								{
									addthis++;
									if(*iter == kTextChar_CR)
									{
										enterFound = kTrue;
										break;
									}
										
								}
								if(enterFound)
								{
									//CA("enterFound == kTrue");
									delEnd = tagEndPos + 1 + addthis;
								}
								else
								{
									//CA("enterFound == kFalse");
									delEnd = tagEndPos1;
								}

								DeleteText( textModel,   delStart, delEnd- delStart  + 1);

								continue;
							}


							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//	textToInsert=iConverter->translateString(dispName);
							
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
						}
					}//end for..iterate through each column.
				}//end for..iterate through each row of repeatedRowPattern.
				//All the subsequent rows will now spray the ItemValue.
				nonHeaderRowPatternIndex = 1;					
			}//end if 2 tableHeaderPresent

					GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);

					int32 ColIndex = -1;
					GridArea bodyArea_new = tableModel->GetBodyArea();
					
					ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
					ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
					while (iterTable != endTable)
					{
						GridAddress gridAddress = (*iterTable); 
						if(tableHeaderPresent)
							if(gridAddress <= headerRowGridAddress){
								iterTable++;							
								continue;
							}
						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
						if(cellContent == nil) 
						{
							break;
						}
						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
						if(!cellXMLReferenceData) {
							break;
						}
						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
						++iterTable;

						ColIndex++;
						int32 itemIDIndex = ColIndex/bodyCellCount;

						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());										
						//Note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							//CA("continue");
							continue;
						}
			
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//so always take the 0th childTag of the Cell.
						int32 cellChildCount = cellXMLElementPtr->GetChildCount();
						for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount ;tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{	
								continue;
							}
							
							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							

							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
							PMString imageFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
							PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
							PMString strChildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
							PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
							int32 header = strHeader.GetAsNumber();	
							double childId = strChildId.GetAsDouble();
							int32 childTag = strChildTag.GetAsNumber();

							double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
							double itemID = FinalItemIds[itemIDIndex];
							double parentTypeID = strParentTypeID.GetAsDouble();
																		 
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;
						
							int32 field_val = -1;
							PMString field_valStr = cellTextXMLElementPtr->GetAttributeValue(WideString("field1"));
							field_val = field_valStr.GetAsNumber();
							PMString typeIdStr =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							double typeID = typeIdStr.GetAsDouble();

							PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
							int32 isEventFieldVal = isEventFieldStr.GetAsNumber();

							PMString dispName("");
							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							if(index == "3")
							{
								makeTheTagImpotent(cellTextXMLElementPtr);
							}
							else if(tableFlag == "1")
							{
								//The following code will spray the table preset inside the table cell.
								if(index == "4")
								{//Item ItemTable
									if(typeID != -1)
									{
										if(isCallFromTS == kTrue)
										{
											CItemTableValue	oTableVal;
											VectorScreenTableInfoValue::iterator iter;
											if(!tableInfo)
											{
												//CA("!tableInfo");
												break;
											}
											if(tableInfo->size() == 0)
											{
												//CA("!tableInfo->size() == 0");
												break;
											}
											for(iter = tableInfo->begin() ; iter != tableInfo->end() ; iter++)
											{				
												oTableVal = *iter;
												if(oTableVal.getTableTypeID() != typeID)
													continue;
											}
										}
										else
										{
											//CA("isCallFromTS == kFalse");
										}
										
									}
									else
									{
										PMString valDataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
										if(valDataType == "4")	
										{
											PMString str_ParentTypeID("");
									
											if(pNode.getIsProduct() == 0)
											{
												CItemTableValue oTableValue;
												if(isCallFromTS == kTrue)
												{
													if(tableInfo == NULL)
														break;
													VectorScreenTableInfoValue::iterator it;
													for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
													{				
														oTableValue = *it;		
														if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(tableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(tableIndex))
														{
															str_ParentTypeID.clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(str_ParentTypeID));
															
															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("tableId"),WideString(str_ParentTypeID));

															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(slugInfo.tableType);
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef,WideString("tableType"),WideString(str_ParentTypeID));
															dispName = oTableValue.getName();
															break;
														}
													}
												}
											}
											
										}
										else
										{
											XMLContentReference xmlCntnRef = cellTextXMLElementPtr->GetContentReference();
											InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());

											bool16 isTablePresentInsideCell = xmlCntnRef.IsTable();
											/////following functionallity is not yet completed 
											//for section level item having items which contains their own item table. 
											if(isTablePresentInsideCell)
											{
												//CA("isTablePresentInsideCell = kTrue");
												//
												//InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
												//if(innerTableModel == NULL)
												//{
												//	CA("innerTableModel == NULL");
												//	continue;
												//}
												//UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
								
												//XMLTagAttributeValue xmlTagAttrVal;
												////collect all the attributes from the tag.
												////typeId,LanguageID are important
												//getAllXMLTagAttributeValues(cellTextXMLElementRef,xmlTagAttrVal);	

												////CA("xmlTagAttrVal.typeId : " + xmlTagAttrVal.typeId);

												///*if(xmlTagAttrVal.typeId.GetAsNumber() == -111)
												//{
												//	fillDataInCMedCustomTableInsideTable
												//	(
												//		cellTextXMLElementRef,
												//		innerTableModelUIDRef,
												//		pNode

												//	);
												//}
												//else
												//{*/
												//	FillDataTableForItemOrProduct
												//	(
												//		cellTextXMLElementRef,
												//		innerTableModelUIDRef,
												//		kFalse
												//	);
												///*}*/

												//UIDList itemList(slugInfo.curBoxUIDRef);
												//UIDList processedItems;
												//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
												//ErrorCode status =  ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);

												//tableflag++;
												//continue;
											}

											PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
											TagStruct tagInfo;												
											tagInfo.whichTab = index.GetAsNumber();
											tagInfo.parentId = pNode.getPBObjectID();

											tagInfo.isTablePresent = tableFlag.GetAsNumber();
											tagInfo.typeId = typeID.GetAsDouble();
											tagInfo.sectionID = sectionid;
											
												
											GetItemTableInTabbedTextForm(tagInfo,dispName);
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));

											PMString attrVal;
											attrVal.Clear();
											attrVal.AppendNumber(PMReal(pNode.getPubId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
											attrVal.Clear();

											attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("pbObjectId"),WideString(attrVal));
										}
									}
								}

							}
							else if(imageFlag == "1")
							{
								XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
								UIDRef ref = cntentREF.GetUIDRef();
								if(ref == UIDRef::gNull)
								{
									int32 StartPos1 = -1;
									int32 EndPos1 = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									

									InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
										textModel->QueryStrand(kOwnedItemStrandBoss, 
										IItemStrand::kDefaultIID)));
									if (itemStrand == nil) {
										//CA("invalid itemStrand");
										continue;
									}
									OwnedItemDataList ownedList;
									itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
									int32 count = ownedList.size();

									if(count > 0)
									{
										UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
										if(newRef == UIDRef::gNull)
										{
											//CA("ref == UIDRef::gNull....2");
											continue;
										}
										ref = newRef;
									}else
										continue;
								}

								InterfacePtr<ITagReader> itagReader
								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
								if(!itagReader)
									continue ;
								
								TagList tList=itagReader->getTagsFromBox(ref);
								TagStruct tagInfoo;
								int numTags=static_cast<int>(tList.size());

								if(numTags<=0)
								{
									continue;
								}

								tagInfoo=tList[0];

								tagInfoo.parentId = itemID;//pNode.getPubId();
								tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
								tagInfoo.sectionID=CurrentSubSectionID;

								IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
								if(!iDataSprayer)
								{	
									//CA("!iDtaSprayer");
									continue;
								}

								if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
								|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
								|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
								|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
								{
									iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
								}
								else if(tagInfoo.elementId > 0)
								{
									isInlineImage = kTrue;
									iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo,itemID,kTrue);
									iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
														
								}
								else
								{
									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
								}

								
							
								PMString attrVal;
								attrVal.AppendNumber(PMReal(itemID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellXMLElementRef, WideString("parentID"),WideString(attrVal));
								attrVal.Clear();
								
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
								continue;
							}
							else if(tableFlag == "0")
							{
								PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
								PMString colNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
								PMString ischildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
								
								if(index =="4" && ischildTag == "1")
								{
									if(field_val != -1)
									{
										CItemTableValue	oTableVal;
										VectorScreenTableInfoValue::iterator iter;
										for(iter = tableInfo->begin() ; iter != tableInfo->end() ; iter++)
										{				
											oTableVal = *iter;
											if(oTableVal.getTableTypeID() != field_val)
												continue;
										
											if(oTableVal.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) )
											{
                                                dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
												break;
											}
										}
									}
									else
									{
										//if(index =="4" && colNo == "-101")	
										//CA("index == 4 && colNo == -101");
										//ChildItem
										if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
										{
											TagStruct tagInfo;
											tagInfo.elementId = elementID;
											tagInfo.tagPtr = cellTextXMLElementPtr;
											tagInfo.typeId = itemID;
											tagInfo.parentTypeID = parentTypeID;
											tagInfo.childId = childId;
											tagInfo.childTag = childTag;
											int32 header = strHeader.GetAsNumber();

											handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
										}

										else if(elementID == -803)  // For 'Letter Key'
										{
											dispName.Clear();
											int wholeNo =  itemIDIndex / 26;
											int remainder = itemIDIndex % 26;								

											if(wholeNo == 0)
											{// will print 'a' or 'b'... 'z'  upto 26 item ids
												dispName.Append(AlphabetArray[remainder]);
											}
											else  if(wholeNo <= 26)
											{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
												dispName.Append(AlphabetArray[wholeNo-1]);	
												dispName.Append(AlphabetArray[remainder]);	
											}
											else if(wholeNo <= 52)
											{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
												dispName.Append(AlphabetArray[0]);
												dispName.Append(AlphabetArray[wholeNo -26 -1]);	
												dispName.Append(AlphabetArray[remainder]);										
											}
										}
										//else if((isComponentAttributePresent == 1) && (elementID == -805))
										//{
										//	//CA("Here 1");
										//	dispName.Clear();			
										//	//CA(Kit_vec_tablerows[0][0]);
										//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);	
										//}
										//else if((isComponentAttributePresent == 1) && (elementID == -806))
										//{	//CA("Here 2");									
										//	dispName.Clear();	
										//	//CA(Kit_vec_tablerows[0][1]);
										//	dispName = ((Kit_vec_tablerows[itemIDIndex][1]));	
										//}
										//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
										//{
										//	dispName.Clear();	
										//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);			
										//}
										//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
										//{
										//	dispName.Clear();	
										//	dispName = (Kit_vec_tablerows[itemIDIndex][1]);			
										//}
										//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
										//{
										//	dispName.Clear();	
										//	dispName = (Kit_vec_tablerows[itemIDIndex][2]);			
										//}
										//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
										//{
										//	dispName.Clear();	
										//	dispName = (Kit_vec_tablerows[itemIDIndex][3]);			
										//}
										//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
										//{
										//	dispName.Clear();	
										//	dispName = (Kit_vec_tablerows[itemIDIndex][4]);			
										//	//CA(dispName);
										//}
										//else if((isComponentAttributePresent == 3) && (elementID == -812))
										//{	//CA("Here 3");									
										//	dispName.Clear();	
										//	//CA(Accessory_vec_tablerows[0][1]);
										//	dispName = ((Accessory_vec_tablerows[itemIDIndex][0]));	
										//}
										//else if((isComponentAttributePresent == 3) && (elementID == -813))
										//{	//CA("Here 4");									
										//	dispName.Clear();	
										//	//CA(Accessory_vec_tablerows[0][1]);
										//	dispName = ((Accessory_vec_tablerows[itemIDIndex][1]));	
										//}
										//else if((isComponentAttributePresent == 3) && (elementID == -814))
										//{	//CA("Here 5");									
										//	dispName.Clear();	
										//	//CA(Accessory_vec_tablerows[0][1]);
										//	dispName = ((Accessory_vec_tablerows[itemIDIndex][2]));	
										//}
										//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[itemIDIndex][4]) == "N"))
										//{
										//	//CA(" non catalog items");
										//	dispName.Clear();	
										//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
										//}
										else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
										{
											//VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),elementID,CurrentSubSectionID);
											/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemID,elementID,CurrentSubSectionID);
											if(vecPtr != NULL){
												if(vecPtr->size()> 0){
													dispName = vecPtr->at(0).getObjectValue();	
												}
											}*/
										}
										else if(elementID == -401  || elementID == -402  ||  elementID == -403)		//----For Make, Mode, Year . 
										{
											dispName = ""; // ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemID,elementID, languageID);
										}
										else if(elementID == -827)  //Number Keys
										{
											dispName.Clear();
											dispName.Append(itemIDIndex+1);
										}
										else 
										{
											//CA("catalog Items");
											dispName.Clear();	
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
										}
									}
									PMString attrVal;
									attrVal.AppendNumber(PMReal(itemID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
									
									attrVal.Clear();
									attrVal.AppendNumber(PMReal(pNode.getPubId()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									
									attrVal.Clear();
									attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("pbObjectId"),WideString(attrVal));	
								}
								else if( index == "4")
                                {
									if(field_val != -1)
									{
										CItemTableValue	oTableVal;
										VectorScreenTableInfoValue::iterator iter;
										for(iter = tableInfo->begin() ; iter != tableInfo->end() ; iter++)
										{				
											oTableVal = *iter;
											if(oTableVal.getTableTypeID() != field_val)
												continue;
                                    
                                            dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
                                            break;
											
										}
									}
									else
									{
										if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
										{
											TagStruct tagInfo;
											tagInfo.elementId = elementID;
											tagInfo.tagPtr = cellTextXMLElementPtr;
											tagInfo.typeId = pNode.getPubId();
											tagInfo.childId = childId;
											tagInfo.childTag = childTag;
											int32 header = strHeader.GetAsNumber();
											handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
										}
										else if(elementID == -803)  // For 'Letter Key'
										{
											dispName.Clear();
											int wholeNo =  itemIDIndex / 26;
											int remainder = itemIDIndex % 26;								

											if(wholeNo == 0)
											{// will print 'a' or 'b'... 'z'  upto 26 item ids
												dispName.Append(AlphabetArray[remainder]);
											}
											else  if(wholeNo <= 26)
											{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
												dispName.Append(AlphabetArray[wholeNo-1]);	
												dispName.Append(AlphabetArray[remainder]);	
											}
											else if(wholeNo <= 52)
											{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
												dispName.Append(AlphabetArray[0]);
												dispName.Append(AlphabetArray[wholeNo -26 -1]);	
												dispName.Append(AlphabetArray[remainder]);										
											}
										}
										else
										{
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID, CurrentSectionID, kFalse);
										}
										if(elementID == -827)  // For 'Number Key'
										{
											dispName.Clear();
											dispName.Append(itemIDIndex+1);
										}
										
									}
									PMString attrVal;
									attrVal.AppendNumber(PMReal(pNode.getPubId()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
										
									attrVal.Clear();
									attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("pbObjectId"),WideString(attrVal));

								}

							}
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("-1"));
							if(isCallFromTS ){
								PMString temp("");
								temp.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(0)));
								cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(temp));								
							}


							int32 delStart = -1, delEnd = -1;
							bool16 shouldDelete = kFalse;
							if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
							{//if(tagInfo.rowno == -117)
                                delStart = tagStartPos-1;
								if(dispName == "")							
									shouldDelete = kTrue;
							}
							if(shouldDelete == kTrue )
							{
								int32 tagStartPos1 = -1;
								int32 tagEndPos1 = -1;
						
								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

								TextIterator begin(textModel , tagEndPos + 1);
								TextIterator end(textModel , tagEndPos1 - 1);
								int32 addthis = 0;
								bool16 enterFound = kFalse;
								for(TextIterator iter = begin ;iter <= end ; iter++)
								{
									addthis++;
									if(*iter == kTextChar_CR)
									{
										enterFound = kTrue;
										break;
									}
										
								}
								if(enterFound)
								{
									//CA("enterFound == kTrue");
									delEnd = tagEndPos + 1 + addthis;
								}
								else
								{
									//CA("enterFound == kFalse");
									delEnd = tagEndPos1;
								}

								DeleteText( textModel,   delStart, delEnd- delStart  + 1);
								continue;
							}


							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//	textToInsert=iConverter->translateString(dispName);
							
							//CA("textToInsert = " + textToInsert);
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
						}
						
					}//end for..iterate through each column

			InterfacePtr<IDataSprayer> iDataSprayer((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!iDataSprayer)
			{	
				//CA("!iDtaSprayer");
				continue;
			}
			UIDRef tempUIDRef = boxUIDRef;				
			iDataSprayer->fitInlineImageInBoxInsideTableCell(tempUIDRef);
	   }//end for..tableIndex
	   errcode = kTrue;
	}while(0);

	return errcode;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
void TableUtility :: GetItemTableInTabbedTextForm(TagStruct & tagInfo,PMString & tabbedTextData)
{
	

	PMString elementName,colName;
	vector<double> FinalItemIds;
	do{
		if(((tagInfo.whichTab == 3) || tagInfo.whichTab == 4) && tagInfo.isTablePresent == kTrue)
		{//start if1

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
				break;
			}
			double sectionid = tagInfo.sectionID;

			vector<double>  vectorOfTableHeader;
			vector<vector<PMString> >  vectorOfRowByRowTableData;
			vector<double>  vec_items;
			bool8 isTranspose=kFalse;
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;
			if(tagInfo.whichTab == 4)
			{
				VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tagInfo.parentId, CurrentSectionID, tagInfo.languageID);
				if(!tableInfo)
				{
					//CA("tableinfo is NULL");
					break;
				}

				if(tableInfo->size()==0)
				{ 
					//CA(" tableInfo->size()==0");
					break;
				}
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it; 
				
				bool16 typeidFound=kFalse;
                for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
                {
                    oTableValue = *it;
                    if(oTableValue.getTableTypeID() == tagInfo.typeId)
                    {
                        typeidFound=kTrue;
                        break;
                    }
                }
                if(!typeidFound)
                {
                    //CA("!typeidFound");
                    break;
                }
  
                vectorOfTableHeader = oTableValue.getTableHeader();
                vectorOfRowByRowTableData = oTableValue.getTableData();
                vec_items = oTableValue.getItemIds();
                isTranspose = oTableValue.getTranspose();
			}
			else if(tagInfo.whichTab == 3)
			{
				VectorScreenTableInfoPtr tableInfo = NULL;
					if(pNode.getIsONEsource())
					{
						// For ONEsource
						//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(tagInfo.parentId);
					}else
					{
						//for publication 
						tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, tagInfo.parentId, tagInfo.languageID, kFalse);
					}
				if(!tableInfo)
				{
					//CA("tableinfo is NULL");
					break;
				}

				if(tableInfo->size()==0)
				{ 
					//CA(" tableInfo->size()==0");
					break;
				}
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				bool16 typeidFound=kFalse;
                for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
                {
                    oTableValue = *it;
                    if(oTableValue.getTableTypeID() == tagInfo.typeId)
                    {
                        typeidFound=kTrue;
                        break;
                    }
                }
                if(!typeidFound)
                {
                    break;
                }
                vectorOfTableHeader = oTableValue.getTableHeader();
                vectorOfRowByRowTableData = oTableValue.getTableData();
                vec_items = oTableValue.getItemIds();
                isTranspose = oTableValue.getTranspose();
			}
			
			int32 noOfColumns = static_cast<int32>(vectorOfTableHeader.size());
			int32 noOfRows =static_cast<int32>(vec_items.size());	

			PMString rawTabbedTextData;
			rawTabbedTextData.Clear();			
			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				
				//This for loop will spray the Item_copy_attributes value
				vector<vector<PMString> > ::iterator rowIterator;
				for(rowIterator = vectorOfRowByRowTableData.begin();rowIterator != vectorOfRowByRowTableData.end()-1;rowIterator++)
					{
						
						vector<PMString> :: iterator colIterator = rowIterator->begin();
						colIterator++;
						for(;colIterator!=  rowIterator->end()-1;colIterator++)
						{
							
							rawTabbedTextData.Append(*colIterator);							
							rawTabbedTextData.Append("\t");
							

						}
						rawTabbedTextData.Append(*colIterator);	
						rawTabbedTextData.Append("\r");

					}
					// optimization code..
					vector<PMString> :: iterator colIterator = rowIterator->begin();
					colIterator++;
					for(;colIterator!=  rowIterator->end()-1;colIterator++)
					{
						rawTabbedTextData.Append(*colIterator);
						rawTabbedTextData.Append("\t");
						

					}
					rawTabbedTextData.Append(*colIterator);
					
					

					PMString textToInsert("");
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					{
						tabbedTextData = rawTabbedTextData;
					}
					//else
					//	tabbedTextData=iConverter->translateString(rawTabbedTextData);
				
			}//end of if IsTranspose == kFalse
			else
			{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
				/*  
				In case of Transpose == kTrue..the table gets sprayed like 

					Header11 Data11 Data12 Data13..
					Header22 Data21 Data22 Data23..
					Header33 Data31 Data32 Data33..
					....
					....
				So First Column is HeaderColumn and subsequent columns are Data columns.
				*/

				vector<PMString> * transposeData = new vector<PMString>[noOfColumns];

				vector<vector<PMString> > ::iterator rowIterator;
				for(rowIterator = vectorOfRowByRowTableData.begin();rowIterator != vectorOfRowByRowTableData.end();rowIterator++)
					{
						vector<PMString> :: iterator colIterator = rowIterator->begin();
						colIterator++;
						for(int32 rowIndex = 0;colIterator!=  rowIterator->end();colIterator++,rowIndex++)
						{
							transposeData[rowIndex].push_back(*colIterator);
						}
					}

				for(int32 index = 0;index < noOfColumns-1;index++)
				{
					vector<PMString> ::iterator dataIterator = transposeData[index].begin();
					while(dataIterator != transposeData[index].end()-1)
					{
						rawTabbedTextData.Append(*dataIterator);
						rawTabbedTextData.Append("\t");
						dataIterator++;
					}
					rawTabbedTextData.Append(*dataIterator);
					rawTabbedTextData.Append("\r");
				}
				vector<PMString> ::iterator dataIterator = transposeData[noOfColumns-1].begin();
				while(dataIterator != transposeData[noOfColumns-1].end()-1)
				{
					rawTabbedTextData.Append(*dataIterator);
					rawTabbedTextData.Append("\t");
					dataIterator++;
				}
				rawTabbedTextData.Append(*dataIterator);

				delete []transposeData;
				
			PMString textToInsert("");
			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//if(!iConverter)
			{
				tabbedTextData = rawTabbedTextData;
			}
			//else
			//	tabbedTextData=iConverter->translateString(rawTabbedTextData);
							
			}//end if1
		}

	}while(kFalse);

}//end of function


void TableUtility ::AddTagToCellText
				(
					const GridAddress & cellAddr,
					const InterfacePtr<ITableModel> & tableModel,
                    PMString & dataToBeSprayed,
					InterfacePtr<ITextModel> & textModel,
					const XMLTagAttributeValue xmlTagAttrVal
				)
{
	do
	{
			InterfacePtr<ICellContent>cellContent(tableModel->QueryCellContentBoss(cellAddr));
			if(cellContent == NULL)
			{
				//CA("cellContent == NULL");
				break;
			}
			
			InterfacePtr<IXMLReferenceData> cellXMLRefData(cellContent,UseDefaultIID());
			if(cellXMLRefData == NULL)
			{
				//CA("cellXMLRefData == NULL");
				break;
			}
			XMLReference cellXMLReference = cellXMLRefData->GetReference();
			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLReference.Instantiate());
			if(cellXMLElementPtr == NULL)
			{
				//CA("cellXMLElementPtr == NULL");
				break;
			}
			//get the text tag in the cell
			int32 cellChildCount = cellXMLElementPtr->GetChildCount();
			InterfacePtr<ITableCommands> tableCommand(tableModel,UseDefaultIID());
			if(tableCommand == NULL)
			{
				//CA("tableCommand == NULL");
				break;
			}
			if(cellChildCount == 0)
			{
				XMLReference newXMLTagRef;
				int32 startPos = -1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(cellXMLElementPtr,&startPos,&endPos);
				endPos = startPos + dataToBeSprayed.NumUTF16TextChars()-1;

                dataToBeSprayed.ParseForEmbeddedCharacters();
				const WideString insertText(dataToBeSprayed);
                tableCommand->SetCellText(insertText, cellAddr);

				UIDRef tableTextModelUIDRef = ::GetUIDRef(textModel);
				ErrorCode err = Utils<IXMLElementCommands>()->CreateElement(WideString(xmlTagAttrVal.tagName),tableTextModelUIDRef,startPos,endPos+1,cellXMLReference,&newXMLTagRef);
				if(err == kFailure)
				{
					//CA("err == kFailure");
					break;
				}
				createAllXMLTagAttributes(newXMLTagRef,xmlTagAttrVal);				
				break;
			}
			else
			{
				XMLReference cellChildXMLRef = cellXMLElementPtr->GetNthChild(0);
				InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLRef.Instantiate());
				if(cellChildXMLElementPtr == NULL)
				{
					//CA("cellChildXMLElementPtr == NULL");
					break;
				}
				int32 startPos = -1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(cellChildXMLElementPtr,&startPos,&endPos);
				startPos = startPos + 1;
				endPos = endPos -1;
                dataToBeSprayed.ParseForEmbeddedCharacters();
				const WideString insertText(dataToBeSprayed);
				tableCommand->SetCellText(insertText, cellAddr);

				//change the TagName
				Utils<IXMLElementCommands>()->SetElement(cellChildXMLRef,WideString(xmlTagAttrVal.tagName));			
				//change the TagAttributes.		
				setAllXMLTagAttributeValues(cellChildXMLRef,xmlTagAttrVal);
			}
		}while(kFalse);
}



///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
//Following function is added by vijay on 16-10-2006/////////////////////////FROM HERE=============
//void TableUtility::fillDataInCMedCustomTable
//(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,int32& tableId,const UIDRef& BoxUIDRef)
//{
//	//CA("in fillDataInCMedCustomTable");
//	UIDRef boxUIDRef = BoxUIDRef;
//	do
//	{
//		IsDBTable = kTrue;
//		int32 IsAutoResize = 0;
//		if(tStruct.isAutoResize == 1)
//			IsAutoResize= 1;
//
//		if(tStruct.isAutoResize == 2)
//			IsAutoResize= 2;
//
//		if(tStruct.header == 1)//if(tStruct.colno == -1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//
//		int32 objectId = pNode.getPubId(); //Product object		
//
//		CMedCustomTableScreenValue* customtableInfoptr = ptrIAppFramework->GetONEsourceObjects_getMed_CustomTableScreenValue(objectId);
//		if(customtableInfoptr == NULL)
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::fillDataInCMedCustomTable: GetONEsourceObjects_getMed_CustomTableScreenValue's customtableInfoptr == NULL");
//			break;
//		}
//		CMedCustomTableScreenValue customtableInfo = *customtableInfoptr;//Currently it is empty ,And should be filled from DB
//		
//		//CA("Return to DataSprayer");
//		if(customtableInfo.HeaderList.size()==0){//replace v
//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::fillDataInCMedCustomTable: customtableInfo.HeaderList.size()==0");
//			break;
//		}
//
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{	//CA("Slection NULL");
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			return;
//		}
//
//		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		//selectionManager->DeselectAll(NULL); // deselect every active CSB
//		//layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//		
//		TableStyleUtils TabStyleObj;
//		TabStyleObj.SetTableModel(kTrue);
//		TabStyleObj.GetTableStyle();
//		TabStyleObj.getOverallTableStyle();
//
//		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		if(!txtMisSuite)
//		{
//			//CA("My Suit NULL");
//			//break;
//		}
//	
//		PMRect theArea;
//		PMReal rel;
//		int32 ht;
//		int32 wt;
//
//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		if(iGeometry==NULL)
//			break;
//
//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		rel = theArea.Height();
//		ht=::ToInt32(rel);
//		rel=theArea.Width();	
//		wt=::ToInt32(rel);
//
//		int32 numRows=0;
//		int32 numCols=0;
//
//		numRows =static_cast<int32>(customtableInfo.Tabledata.size());  
//		if(numRows<=0)
//			break;		
//
//		numCols = static_cast<int32>(customtableInfo.HeaderList.size());	
//		if(numCols<=0)
//			break;
//
//		/*PMString ASD(" no Rows : ");
//		ASD.AppendNumber(numRows);
//		ASD.Append("   no Clos : ");
//		ASD.AppendNumber(numCols);
//		CA(ASD);*/
//
//		bool8 isTranspose = kFalse;//Hardcoded as false for Medtronics
//		/*InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//		if(!tWrite)
//			break;*/
//
//		TableFlagForStandardProductTable = 1; // Since we are spraying Product Table
//
//		if(!isTranspose)
//		{
//			int32 i=0, j=0;
//			resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
//					
//			if(AddHeaderToTable)
//			{
//				/*putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef); following function is commented by vijay on 16-10-2006*/
//
//				do
//				{
//								
//					if(ptrIAppFramework == NULL)
//					{
//						//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//						break;
//					}
//					for(int k=0; k<customtableInfo.HeaderList.size(); k++)
//					{					
//						InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//						if (tableModel == NULL)
//						{
//							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::fillDataInCMedCustomTable::!tableModel");		
//							break;
//						}
//
//						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//						if(tableCommands==NULL)
//						{
//							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::fillDataInCMedCustomTable::!tableCommands");		
//							break;
//						}
//						//CA("Cell String : " + customtableInfo.HeaderList[k].getdisplayString());
//						WideString* wStr=new WideString(customtableInfo.HeaderList[k].getdisplayString());
//										
//						SlugStruct stencileInfo;						
//											
//						tableCommands->SetCellText(*wStr, GridAddress(0, k));
//						
//						stencileInfo.elementId =customtableInfo.HeaderList[k].getvalueAttributeId();												
//						PMString TableColName("");
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//				
//						stencileInfo.elementName = TableColName;
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tStruct.languageID;
//				
//						stencileInfo.typeId  = -1;//-2; 
//						stencileInfo.header = 1;	
//						stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
//						stencileInfo.parentTypeId = pNode.getTypeId();					
//						stencileInfo.whichTab = tStruct.whichTab;
//						stencileInfo.imgFlag = CurrentSectionID; 
//						stencileInfo.isAutoResize = tStruct.isAutoResize;
//						stencileInfo.col_no = tStruct.colno;
//						stencileInfo.tableTypeId = tStruct.typeId;
//						XMLReference txmlref;
//						//CA("DataSprayer 11.........");
//						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), "PSTable", txmlref, stencileInfo, 0, k, 0 );
//						delete wStr;
//					}
//				}
//				while(kFalse);
//			 i=1; j=0;
//			}
//
//			
//			int32 BodyRowCount =static_cast<int32>(customtableInfo.Tabledata.size());
//		/*	PMString ASD1(" BodyRowCount : ");
//			ASD1.AppendNumber(BodyRowCount);
//			CA(ASD1);*/
//			for(int32 count2 =0 ; count2 < BodyRowCount ;count2++) //new added by v
//			{	
//
//				VectorCustomItemTableRowInfo rowVector = customtableInfo.Tabledata[count2];
//				int32 rowVectorSize = static_cast<int32>(rowVector.size());
//				//PMString ASD1(" rowVectorSize : ");
//				//ASD1.AppendNumber(rowVectorSize);
//				//CA(ASD1);
//				for(int32 count1 =0 ; count1 < rowVectorSize ;count1++)//new added by v
//				{		
//					//PMString ASD("row : " );
//					//ASD.AppendNumber(i);
//					//ASD.Append("  col : ");
//					//ASD.AppendNumber(j);
//					//CA(ASD + "\n" + "Body Cell String : " + rowVector[count1].getdisplayString());
//					setTableRowColData(tableUIDRef,rowVector[count1].getdisplayString() , i, j);	
//					SlugStruct stencileInfo;
//
//					stencileInfo.elementId = rowVector[count1].getvalueAttributeId();
//				
//					PMString TableColName("");
//					TableColName.Append("ATTRID_");
//					TableColName.AppendNumber(rowVector[count1].getvalueAttributeId()/*vec_tableheaders[j-1]*/); // replaced v
//
//					stencileInfo.elementName = TableColName;
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//										
//					stencileInfo.typeId  = -1;//rowVector[count1].getItemId();
//					stencileInfo.typeId = rowVector[count1].getItemId();
//					stencileInfo.parentId = pNode.getPubId();
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					XMLReference txmlref;
//					//CA(tStruct.tagPtr->GetTagString());
//
//
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), "PSTable", txmlref, stencileInfo, i, j,0 );
//					j++;
//				}
//				i++;
//				j=0;
//			}
//			
//		}
//	
//		if(IsAutoResize == 1)
//		{
//			UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//
//					//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//					//if (!layoutSelectionSuite) 
//					//{	//CA("!layoutSelectionSuite");
//					//	return;
//					//}
//					//layoutSelectionSuite->DeselectAll();
//					//layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
//					//InterfacePtr<ITextMiscellanySuite> txtMisSuite1(static_cast<ITextMiscellanySuite* >
//					//( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//					//if(!txtMisSuite1)
//					//{
//					//	//CA("My Suit NULL");
//					//	return;
//					//}
//					//InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite1,UseDefaultIID());
//					//if(!frmContentSuite)
//					//{
//					//	//CA("IFrame Content Suite is null");
//					//	return;
//					//}
//			//		frmContentSuite->FitFrameToContent();
//			//CA("2222222");
//			//	if(frmContentSuite->CanFitContentProp())
//			//	{ CA("2 in Spray for this box");
//			//		frmContentSuite->FitContentProp();
//			//	}
//
//			//Commeted By Rahul to avoid Text Frame resizing for Lazboy
//			UIDList processedItems;
//			K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//			ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//			return ;
//		}		
//
//		if(IsAutoResize == 2)
//		{
//			//CA("Inside overflow");
//			ThreadTextFrame ThreadObj;
//			ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
//		}
//		
//		TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
//		TabStyleObj.ApplyTableStyle();
//		TabStyleObj.setTableStyle();
//	
//	}while(kFalse);
//	IsDBTable = kFalse;
//
//}
/////////////////////////////////////////////////////////////////////////////UPTO HERE


void TableUtility::fillDataInKitComponentTable
(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef, bool16 isKitTable)
{
	//CA("in fillDataInKitComponentTable");
//	UIDRef boxUIDRef = BoxUIDRef;
//	do
//	{
//		IsDBTable = kTrue;		//This metod is getting called only for DBTable.
//		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
//		if(tStruct.isAutoResize==1)
//			IsAutoResize= 1;
//		
//		if(tStruct.isAutoResize==2)
//			IsAutoResize= 2;
//
//		if(tStruct.header == 1)//if(tStruct.colno == -1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//
//		int32 objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		
//
//		int32 sectionid = -1;
//		if(global_project_level == 3)
//			sectionid = CurrentSubSectionID;
//		if(global_project_level == 2)
//			sectionid = CurrentSectionID;
//	
//		//VectorScreenTableInfoPtr tableInfo = NULL;
//		//
//		//if(pNode.getIsONEsource())
//		//{
//		//	// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
//		//	tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
//		//}else
//		//{
//		//	// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
//		//	//CA("Not ONEsource");
//		//	tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId);
//		//}
//		//if(!tableInfo)
//		//{
//		//	//CA("tableinfo is NULL");
//		//	return;
//		//}
//
//		//if(tableInfo->size()==0){
//		//	//CA(" tableInfo->size()==0");
//		//	break;
//		//}
//
//		//CItemTableValue OriginaloTableValue;
//		//VectorScreenTableInfoValue::iterator it1;
//		//it1 = tableInfo->begin();
//		//OriginaloTableValue = *it1;
//		//
//		//vector<int32> vec_item_new;
//		//vec_item_new = OriginaloTableValue.getItemIds();
//		//if(vec_item_new.size() == 0)
//		//{
//		//	//CA("vec_item_new.size() == 0");
//		//	break;
//		//}
//		int32 itemId = pNode.getPubId();  //vec_item_new[0]/*30017240*/;
//
//		/*PMString ASD("itemid: ");
//		ASD.AppendNumber(itemId);
//		CA(ASD);*/
//
//		VectorScreenTableInfoPtr KittableInfo = NULL;
//		
//		KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemId, tStruct.languageID, isKitTable); 
//		if(!KittableInfo)
//		{
//			//CA("KittableInfo is NULL");
//			return;
//		}
//
//		if(KittableInfo->size()==0){
//			//CA(" KittableInfo->size()==0");
//			break;
//		}
//
//		/* Output of above method is of following format from Application framework :
//		tableId = null
//		typeId = null
//		tableName = Kits
//		transpose = false
//		printHeader = true
//		tableHeader = [ -805, 11000005, 11000012, 11000033]
//		tableData = [[30021736, 1, , CGO60667M, Chicago Lakefront, 161.54]]
//		seqList = null
//		rowIdList = null
//		columnIdList = null
//		notesList = []
//		*/
//
//		CItemTableValue oTableValue;
//		VectorScreenTableInfoValue::iterator it  ;
//		it = KittableInfo->begin();
//		oTableValue = *it;
//			
//// Added by Rahul
//		//It checks the selection present or not........
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{	//CA("Slection NULL");
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			//CA("!layoutSelectionSuite");
//			return;
//		}
//		//It gets refernce to persistant objects present in the (database) document as long as document is open.
//		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		//selectionManager->DeselectAll(NULL); // deselect every active CSB
//		//layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//
//		/*TableStyleUtils TabStyleObj;
//		TabStyleObj.SetTableModel(kTrue);
//		TabStyleObj.GetTableStyle();	
//		TabStyleObj.getOverallTableStyle();*/
//
//		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		if(!txtMisSuite)
//		{
//			//CA("My Suit NULL");
//			//break;
//		}
//		//These values are taken from db
//		PMRect theArea;
//		PMReal rel;
//		int32 ht;
//		int32 wt;
//
//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		if(iGeometry==NULL)
//			break;
//
//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		rel = theArea.Height();
//		ht=::ToInt32(rel);
//		rel=theArea.Width();	
//		wt=::ToInt32(rel);
//		//----Comment removed by dattatray onc14/10 for testing
//		//PMString ASD1("Width = ");
//		//ASD1.AppendNumber(wt);
//		//CA(ASD1);
//		//----it should be commented by dattatray after testing
////////////////
//		int32 numRows=0;
//		int32 numCols=0;		
//
//		numRows =static_cast<int32>(oTableValue.getTableData().size());
//		if(numRows<=0)
//			break; 
//		numCols =static_cast<int32>(oTableValue.getTableHeader().size() ); 
//		if(numCols<=0)
//			break;
//		bool8 isTranspose = oTableValue.getTranspose();
//		//For testing by dattatray on 14/10
//		/*if(isTranspose == kTrue)
//			CA("isTranspose is kTrue");
//		PMString numRow("numRows::");
//		numRow.AppendNumber(numRows);
//		//CA(numRow);
//		PMString numCol("numcols::");
//		numCol.AppendNumber(numCols);
//		//CA(numCol);*/
//		//It should be removed by dattatray after testing
//	//	isTranspose = kTrue;
//		/*InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//		if(!tWrite)
//			break;*/
//
//		TableFlagForStandardProductTable = 0;  // Since We are spraying Item Table
//
//		if(isTranspose)
//		{
//			//CA("Inside isTranspose");
//			int32 i=0, j=0;
//			resizeTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht);
//		
//			vector<int32> vec_tableheaders;
//			vector<int32> vec_items;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//	
//			if(AddHeaderToTable){
//			putHeaderDataInKitComponentTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//			 i=1; j=0;
//			}
//
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			vec_items = oTableValue.getItemIds();
//
//			//vector<int32>::iterator temp;
//			/*	for(int i1=0;i1<vec_items.size();i1++)
//				{
//					PMString AS("vec Item : ");
//					AS.AppendNumber(vec_items[i1]);
//					CA(AS);
//				}*/
//			
//			vector<vector<PMString> >::iterator it1;
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				vector<PMString>::iterator it2;
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{								
//					setTableRowColData(tableUIDRef, (*it2), j, i);	
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j];
//				//	CAttributeValue oAttribVal;
//				//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//				//	PMString dispname = oAttribVal.getDisplay_name();
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//					PMString TableColName("");// = oAttribVal.getTable_col();
//					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
//				//	{
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(vec_tableheaders[j]);
//				//	}
//					//CA(TableColName);
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					stencileInfo.typeId = -1;
//					if(AddHeaderToTable)
//						stencileInfo.childId  = vec_items[i-1];
//					else
//						stencileInfo.childId  = vec_items[i];
//					stencileInfo.parentId = pNode.getPubId();
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.LanguageID = tStruct.languageID;
//
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					XMLReference txmlref;
//	//CA(tStruct.tagPtr->GetTagString());		
//
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
//					if(TableTagNameNew.Contains("PSTable"))
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSTable";						
//					}
//
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i,tableId );
//					j++;
//					
//					if(it2==(*it1).begin())
//					{
//						it2++; // This is to avoid Availabilty data spray which is in second position og it2
//						//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
//					}
//
//				}
//				i++;
//				j=0;
//			}
//			//tWrite->SetSlug(tableUIDRef,vec_tableheaders,vec_items,isTranspose);
//		}
//		else
//		{
//			//CA("Inside !isTranspose");
//			/*PMString AS(" tStruct.whichtab : ");
//			AS.AppendNumber(tStruct.whichTab);
//			CA(AS);*/
//			int32 i=0, j=0;
//			
//			//resizeTable gets called if Auto Re-Size is selected..
//			resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
//
//			vector<int32> vec_tableheaders;
//			vector<int32> vec_items;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//			if(AddHeaderToTable){
//			putHeaderDataInKitComponentTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//			i=1; j=0;
//			}
//
//			vec_items = oTableValue.getItemIds();
//
//			//vector<int32>::iterator temp;
//			/*for(int i1=0;i1<vec_items.size();i1++)
//			{
//				PMString AS;
//				AS.AppendNumber(vec_items[i1]);
//				CA(AS);
//			}*/
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			vector<vector<PMString> >::iterator it1;
//
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				vector<PMString>::iterator it2;
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{
//				
//					setTableRowColData(tableUIDRef, (*it2), i, j);
//					//CA((*it2));
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j];
//					/*CAttributeValue oAttribVal;
//					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//					PMString dispname = oAttribVal.getDisplay_name();*/
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j/*-1*/],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//					//CA(dispname);
//					PMString TableColName("");  // = oAttribVal.getTable_col();
//					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
//					//{
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(vec_tableheaders[j/*-1*/]);
//					/*}*/
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					stencileInfo.typeId = -1;
//					if(AddHeaderToTable)
//						stencileInfo.childId  = vec_items[i-1];
//					else
//						stencileInfo.childId  = vec_items[i];
//
//					stencileInfo.parentId = pNode.getPubId();
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					XMLReference txmlref;
//					//CA("Before Tag Table");
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
//					if(TableTagNameNew.Contains("PSTable"))
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSTable";						
//					}
//					//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j/*-1*/, tableId );
//					//CA("After Tag Table ...2");
//					
//					if(it2==(*it1).begin())
//					{
//						it2++; // This is to avoid Availabilty data spray which is in second position og it2
//						//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
//					}
//
//				j++;
//				}
//				i++;
//				j=0;
//			}
//			i=0;j=0;			
//		}
//		
//		if(IsAutoResize == 1)
//		{
//			//CA("Inside IsAutoResize");
//			UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//			
//					//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//					//if (!layoutSelectionSuite) 
//					//{	//CA("!layoutSelectionSuite");
//					//	return;
//					//}
//					//
//					//layoutSelectionSuite->DeselectAll();
//					//
//					//layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
//					//
//					//InterfacePtr<ITextMiscellanySuite> txtMisSuite1(static_cast<ITextMiscellanySuite* >
//					//( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//					//if(!txtMisSuite1)
//					//{
//					//	//CA("My Suit NULL");
//					//	return;
//					//}
//					//
//					//InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite1,UseDefaultIID());
//					//if(!frmContentSuite)
//					//{
//					//	//CA("IFrame Content Suite is null");
//					//	return;
//					//}
//			//		frmContentSuite->FitFrameToContent();
//			//CA("2222222");
//			//	if(frmContentSuite->CanFitContentProp())
//			//	{ CA("2 in Spray for this box");
//			//		frmContentSuite->FitContentProp();
//			//	}
//
//			//Commeted By Rahul to avoid Text Frame resizing for Lazboy
//			
//			UIDList processedItems;
//			K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//			
//			ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//			
//			return ;
//		}
//
//		if(IsAutoResize == 2)
//		{
//			//CA("Inside overflow");
//			ThreadTextFrame ThreadObj;
//			ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
//		}
//
//		
//		/*
//		TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
//		TabStyleObj.ApplyTableStyle();
//		TabStyleObj.setTableStyle();*/
//		
//	}while(kFalse);
//	IsDBTable = kFalse;

}


void TableUtility::putHeaderDataInKitComponentTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInKitComponentTable");
	//do
	//{
	//	HeadersizeVector.clear();
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
	//	if (tableModel == NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableFrame");
	//		break;
	//	}

	//	InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//	if(tableCommands==NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableCommands");
	//		break;
	//	}

	//	int i=0;
	//	//Adding Quantity & Avaibiablity 
	//	
	//	if(isTranspose)
	//	{
	//		WideString* wStr=new WideString("Quantity");
	//		tableCommands->SetCellText( *wStr, GridAddress(i, 0));
	//		SlugStruct stencileInfo;
	//		stencileInfo.elementId = -805;		
	//		stencileInfo.TagName = "Quantity";
	//		stencileInfo.LanguageID = tStruct.languageID;
	//		stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;		

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

	//		delete wStr;

	//	}
	//	else
	//	{			
	//		WideString* wStr=new WideString("Quantity");
	//		tableCommands->SetCellText(*wStr, GridAddress(0, i));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -805;		
	//		stencileInfo.TagName = "Quantity";
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
	//		delete wStr;
	//	}
	//	i++;
	//	// at first place we have -805 for Quantity so take the Attributeids from 2nd place onwards
	//	for(i=1;i<vec_tableheaders.size();i++)
	//	{
	//		//oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//		//PMString dispname = oAttribVal.getDisplay_name();

	//		PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//		
	//		WideString* wStr=new WideString(dispname);
	//		//CA(dispname);
	//		int32 StringLength = dispname.NumUTF16TextChars();
	//		HeadersizeVector.push_back(StringLength);

	//		if(isTranspose)
	//		{
	//			tableCommands->SetCellText(*wStr, GridAddress(i , 0));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = vec_tableheaders[i];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//			//CA("Inside PutHeaderDataIN Table");	CA(dispname);
	//			stencileInfo.elementName = dispname;

	//			PMString TableColName("");//oAttribVal.getTable_col();
	//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[i]);
	//			//}
	//			//	CA(TableColName);
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;				

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

	//		}
	//		else
	//		{
	//			/*PMString ASD("vec_tableheaders[i] : ");
	//			ASD.AppendNumber(vec_tableheaders[i]);
	//			CA(ASD);*/

	//			tableCommands->SetCellText(*wStr, GridAddress(0, i));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId =vec_tableheaders[i];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//			stencileInfo.elementName = dispname;
	//			//CA("Inside PutHeaderDataIN Table");	CA(dispname);
	//			PMString TableColName("");// oAttribVal.getTable_col();
	//			//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[i]);
	//			//}
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID =tStruct.languageID;
	//			//CA(TableColName);
	//			stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//			stencileInfo.header;
	//			stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;

	//			//CA(tStruct.tagPtr->GetTagString());
	//			//CA(stencileInfo.colName);
	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//		//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
	//		//	CA("After Tag Table");
	//		}
	//		delete wStr;
	//	}

	//	
	//	//if(isTranspose)
	//	//{
	//	//	WideString* wStr=new WideString("Availablity");
	//	//	tableCommands->SetCellText(*wStr, GridAddress(i, 0));
	//	//	SlugStruct stencileInfo;
	//	//	stencileInfo.elementId = -402;		
	//	//	stencileInfo.TagName = "Availablity";
	//	//	stencileInfo.LanguageID = tStruct.languageID;
	//	//	stencileInfo.typeId  = -2;  // TypeId is hardcodded to -2 for Header Elements.
	//	//	stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//	//	stencileInfo.parentTypeId = pNode.getTypeId();
	//	//	//4Aug..ItemTable
	//	//	//stencileInfo.whichTab = 4; // Item Attributes
	//	//	stencileInfo.whichTab = tStruct.whichTab;
	//	//	stencileInfo.imgFlag = CurrentSectionID; 
	//	//	stencileInfo.isAutoResize = tStruct.isAutoResize;
	//	//	stencileInfo.col_no = tStruct.colno;
	//	//	XMLReference txmlref;	
	//	//	
	//	//	PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//	//	if(TableTagNameNew.Contains("PSTable"))
	//	//	{
	//	//		//CA("PSTable Found");
	//	//	}
	//	//	else
	//	//	{				
	//	//		TableTagNameNew =  "PSTable";						
	//	//	}

	//	//	this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

	//	//}
	//	//else
	//	//{			
	//	//	WideString* wStr=new WideString("Quantity");
	//	//	tableCommands->SetCellText(*wStr, GridAddress(0, i));
	//	//	SlugStruct stencileInfo;	
	//	//	stencileInfo.elementId = -401;		
	//	//	stencileInfo.TagName = "Quantity";
	//	//	stencileInfo.LanguageID =tStruct.languageID;	
	//	//	stencileInfo.typeId  = -2; //vec_items[i];
	//	//	stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//	//	stencileInfo.parentTypeId = pNode.getTypeId();
	//	//	//4Aug..ItemTable
	//	//	//stencileInfo.whichTab = 4; // Item Attributes
	//	//	stencileInfo.whichTab = tStruct.whichTab;
	//	//	stencileInfo.imgFlag = CurrentSectionID; 
	//	//	stencileInfo.isAutoResize = tStruct.isAutoResize;
	//	//	stencileInfo.col_no = tStruct.colno;
	//	//	XMLReference txmlref;			

	//	//	PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//	//	if(TableTagNameNew.Contains("PSTable"))
	//	//	{
	//	//		//CA("PSTable Found");
	//	//	}
	//	//	else
	//	//	{				
	//	//		TableTagNameNew =  "PSTable";						
	//	//	}
	//	//	this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
	//	//}

	//}
	//while(kFalse);
}


bool16 TableUtility ::FillDataTableForItemOrProduct
			(
				XMLReference & boxXMLRef,
				UIDRef & tableModelUIDRef,
				int32 isProduct
			)
{

	bool16 result = kFalse;
	do
	{
		InterfacePtr<IIDXMLElement>boxXMLPtr(boxXMLRef.Instantiate());
		//Collect objectId,sectionid and typeId and languageId
		double objectId = -1;
		if(isProduct == 0)
			objectId = pNode.getPBObjectID();
		if(isProduct == 1)
			 objectId = pNode.getPubId();

		double sectionid = -1;
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;

		InterfacePtr<ITableModel> tableModel(tableModelUIDRef,UseDefaultIID());
		if(tableModel == NULL)
		{
			//CA("tableModel == NULL");
			break;
		}

		//Get the TextModel
		InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
		if(tableTextContent == NULL)
		{
			//CA("tableTextContent == NULL");
			break;
		}
		UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();

		InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel());
		if(textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}
		
		XMLTagAttributeValue xmlTagAttrVal;
		//collect all the attributes from the tag.
		//typeId,LanguageID are important
		getAllXMLTagAttributeValues(boxXMLRef,xmlTagAttrVal);		
		//SectionLevelItem has two important IDs.PubId and PBObjectID.
		//PBObjectID is used while spraying its item table.
		PMString parentIDstr("");
		parentIDstr.AppendNumber(PMReal(pNode.getPubId()));
		xmlTagAttrVal.parentID = parentIDstr;

		PMString parentTypeIDstr("");
		parentTypeIDstr.AppendNumber(PMReal(pNode.getTypeId()));
		xmlTagAttrVal.parentTypeID = parentTypeIDstr;

		PMString sectionIDstr("");
		sectionIDstr.AppendNumber(PMReal(sectionid));
		xmlTagAttrVal.sectionID =sectionIDstr;
		xmlTagAttrVal.tableFlag ="-12";//default..Parent of the tag is Product

		if(isProduct == 0)
		{
			//IMPORTANT: rowno stores the PBObjectID i.e the itemID.
			PMString rowno("");
			rowno.AppendNumber(PMReal(objectId));
			xmlTagAttrVal.rowno =rowno;	
			xmlTagAttrVal.tableFlag = "-1001";//the parent of the tag is Item and not the Product
		}

		//Update the FrameXMLTag i.e tagAttached To the Table
		setAllXMLTagAttributeValues(boxXMLRef ,xmlTagAttrVal);
		
		double typeId =xmlTagAttrVal.typeId.GetAsDouble();
		double languageId = xmlTagAttrVal.LanguageID.GetAsDouble();
		PMString colnoForTableFlag = xmlTagAttrVal.colno;
		PMString isHeaderFlag = xmlTagAttrVal.header;
		//Add XMLElement to table and add the attributes to the table.
		//Note that the cellTags are also attached while attaching tag to table.
		//TableTag has attributes but cellTag has no attributes.

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		int32 numRows=-1;
		int32 numCols=-1;
		bool8 isTranspose= kFalse;

		vector<double>  vec_tableheaders;
		vector<vector<PMString> >  vec_tablerows;
		vector<double>  vec_items;

		if(isProduct == 0)
		{

			VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, languageId );
			if(!tableInfo)
			{
				//CA("tableinfo is NULL");
				break;
			}
			
			VectorScreenTableInfoValue::iterator it;
			CItemTableValue oTableValue;
			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == typeId)
				{   				
					typeidFound=kTrue;				
					break;
				}
			}
			
			if(!typeidFound)
			{
				//CA("typeidFound == kFalse");
				break;
			}
			numRows =static_cast<int32>(oTableValue.getTableData().size());
			if(numRows<=0)
				break;
			numCols = static_cast<int32>(oTableValue.getTableHeader().size());
			if(numCols<=0)
				break;

			isTranspose = oTableValue.getTranspose();
			//Note that don't take the literal meaning of row and column
			//it has to be taken w.r.t the isTranspose flag.
			vec_tableheaders = oTableValue.getTableHeader();		
			vec_tablerows = oTableValue.getTableData();
			vec_items = oTableValue.getItemIds();
		}
		if(isProduct == 1)
		{
			VectorScreenTableInfoPtr tableInfo = NULL;		
			if(pNode.getIsONEsource())
			{
				// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
			}else
			{
				// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
				//CA("Not ONEsource");
				tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, languageId, kFalse);
			}
			if(!tableInfo)
			{
				//CA("tableinfo is NULL");
				break;
			}
			if(tableInfo->size()==0)
			{
				//CA(" tableInfo->size()==0");
				break;
			}	
		
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == typeId)
				{   				
					typeidFound=kTrue;				
					break;
				}
			}

			if(!typeidFound)
			{
				//CA("!typeidFound");
				break;
			}

			numRows =static_cast<int32>(oTableValue.getTableData().size());
			if(numRows<=0)
				break;
			numCols = static_cast<int32>(oTableValue.getTableHeader().size());
			if(numCols<=0)
				break;
			isTranspose = oTableValue.getTranspose();

			vec_tableheaders = oTableValue.getTableHeader();		
			vec_tablerows = oTableValue.getTableData();
			vec_items = oTableValue.getItemIds();
		}
		
		if(isTranspose == kFalse)
		{
			//rows are sprayed in rows of the table on document
			if(isHeaderFlag == "1")//add Table Header also 
			{
				numRows = numRows + 1;//add for header row
			}
		}
		else
		{
			//rows are sprayed in columns of the table on document
			//colums are sprayed in rows of the table on document
			int32 temp=0;
			temp = numRows;
			numRows = numCols;
			if(isHeaderFlag == "1")
				temp = temp + 1;
			numCols = temp;
		}
		//check the number of rows and colums of the current document table
		RowRange rowR = tableModel->GetTotalRows();
		ColRange colR = tableModel->GetTotalCols();
		int32 numberOfDocTableRows = rowR.count;
		int32 numberOfDocTableCols = colR.count;
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
			break;

		InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
		if(tableGeometry == NULL)
		{
			CA("tableGeometry == NULL");
			break;
		}
		PMReal rowHt = tableGeometry->GetRowHeights(numberOfDocTableRows-1);
		PMReal colWidth = tableGeometry->GetColWidths(numberOfDocTableCols-1);
		if(numberOfDocTableRows != numRows)
		{			
			if(numberOfDocTableRows < numRows)
			{
				//we have to add the additional rows at the end
				int32 difference = numRows-numberOfDocTableRows;
				RowRange rr(numberOfDocTableRows-1,difference);
				ErrorCode result = tableCommands->InsertRows(rr, Tables::eAfter, rowHt);
			}
			else
			{
				//we have to delete the surplus rows from bottom
				int32 difference = numberOfDocTableRows - numRows;
				RowRange rr(numberOfDocTableRows-difference,difference);
				ErrorCode result = tableCommands->DeleteRows(rr);
			}
		}
		if(numberOfDocTableCols != numCols)
		{
			if(numberOfDocTableCols < numCols)
			{
				//we have to add the additional cols at the end
				int32 difference = numCols-numberOfDocTableCols ;
				ColRange cr(numberOfDocTableCols-1,difference);
				ErrorCode result = tableCommands->InsertColumns(cr, Tables::eAfter,colWidth);				
			}
			else
			{
				//we have to delete the surplus rows from bottom
				int32 difference = numberOfDocTableCols - numCols ;
				ColRange cr(numberOfDocTableCols-difference-1,difference);
				ErrorCode result = tableCommands->DeleteColumns(cr);
				
			}
		}		
		
		int32 headerEntity = 0;//headerEntity can be row or column depending on the transpose
		vector<double> :: iterator colIterator;
		
		int32 headerEntity1 = 0;
		int32 headerEntity2 = 0;

		if(isHeaderFlag == "1")//Add Table Header also
		{
			for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity++)
			{//for loop of Header
				//Table Header tag have typeId = -2..ItemId is not required.
				//ID is taken from the vec_tableheaders elements.
				//Iterate through the first row of the document table.
				GridAddress cellAddr;
				if(isTranspose == kFalse)
				{
					cellAddr.Set(0,headerEntity);
				}
				else
					cellAddr.Set(headerEntity,0);
			
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName((*colIterator),languageId);
				PMString textToInsert("");
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//if(!iConverter)
				{
					textToInsert=dispname;					
				}
				//else
				//	textToInsert=iConverter->translateString(dispname);
			
				//Update the Tag and its attribute values.
				PMString tagName("ATTRID_");
				tagName.AppendNumber(PMReal((*colIterator)));

				PMString attrVal;
				xmlTagAttrVal.tagName = tagName;
				//ID..it is tableHeaderID			
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*colIterator)));
				xmlTagAttrVal.ID =attrVal;

				attrVal.Clear();
				attrVal.AppendNumber(1);
				xmlTagAttrVal.header = attrVal;

				AddTagToCellText
					(
						cellAddr,
						tableModel,
						textToInsert,
						textModel,
						xmlTagAttrVal
					);			
			
			}//end for loop of Header	
			headerEntity1 = 1;//means Spray the data from the next row or column.
		}
		//For each row the ID element(attribute Id) is same but the typeId(ItemID) changes.
		//ID is taken from the vec_tableheaders
		//vector<vector<PMString> >::iterator rowIterator;		
		//When isTranspose == kFalse
		//	headerEntity1 is row
		//	heaerEntity2 is column
		//When isTranspose == kTrue
		//	headerEntity1 is column
		//	headerEntity2 is row
		
		vector<double> :: iterator rowIterator;
		for(rowIterator=vec_items.begin(); rowIterator!=vec_items.end(); rowIterator++,headerEntity1++)
		{//start for 2
			//For each column the ID is different but the ItemID is same.
			vector<double>::iterator colIterator;
			headerEntity2 = 0;
			for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity2++)
			{				
				//row data feeding
				//ID is taken from the vec_tableheaders elements.
				//Iterate through the first row of the document table.	
				PMString dataToBeSprayed("");
				PMString strdataToBeSprayed("");
				/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific((*colIterator)) == kTrue){
					VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId((*rowIterator),(*colIterator),sectionid);
					if(vecPtr != NULL)
						if(vecPtr->size()> 0)
							strdataToBeSprayed = vecPtr->at(0).getObjectValue();
				}
				else*/ if((*colIterator) == -401 || (*colIterator) == -402 || (*colIterator) == -403){
					strdataToBeSprayed = ""; // ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId((*rowIterator),(*colIterator), languageId);
				}
				else{
					strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId((*rowIterator),(*colIterator),languageId, CurrentSectionID, kFalse);
				}
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//if(!iConverter)
				{
					dataToBeSprayed=(strdataToBeSprayed);					
				}
				//else
				//	dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);

				GridAddress cellAddr;
				if(isTranspose == kFalse)
                    cellAddr.Set(headerEntity1,headerEntity2);				
				else
					cellAddr.Set(headerEntity2,headerEntity1);
				//Update the Tag and its attribute values.
				PMString tagName("ATTRID_");
				tagName.AppendNumber(PMReal((*colIterator)));
				PMString attrVal;
				xmlTagAttrVal.tagName = tagName;
				//ID..it is attributeID			
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*colIterator)));
				xmlTagAttrVal.ID =attrVal;
				//typeId..it is 
				attrVal.Clear();
				attrVal.AppendNumber(PMReal((*rowIterator)));
				xmlTagAttrVal.childId = attrVal;
				AddTagToCellText
				(
					cellAddr,
					tableModel,
					dataToBeSprayed,
					textModel,
					xmlTagAttrVal
				);				
			}			
		}//end for 2
		result = kTrue;
	}while(kFalse);	
	return result;	
}//end FillDataInsideItemTableOfItem
////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void TableUtility:: sprayTableInsideTableCell(UIDRef& boxUIDRef, TagStruct & tagInfo)
{

		//This is a special case where inside a single table the TableData of multiple products is sprayed.
		//Here in the cell of a table there is ScreenTable of the product or item.and when
		//multiple product list or item list is sprayed in each row's cell,corressponding item's or 
		//product's ScreenTable is sprayed.
		do
		{
				UIDRef tableUIDRef(UIDRef::gNull);
				XMLContentReference xmlCntnRef = tagInfo.tagPtr->GetContentReference();
				InterfacePtr<IXMLReferenceData> xmlRefData(xmlCntnRef.Instantiate());
				if(xmlRefData == NULL)
				{
				//	CA("xmlRefData == NULL");
					break;
				}

				InterfacePtr<ITableModel> tableModel(xmlRefData,UseDefaultIID());
				if(tableModel == NULL)
				{
					//CA("tableModel == NULL");
					break;
				}
				int32 rowPatternRepeatCount = 1;//pNodeDataList_ForTableSprayInsideTableCell.size()-1;//pNodeDataList.size();

				int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
				int32 totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;

				int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
				GridArea gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
				InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
				if(tableGeometry == NULL)
				{
					//CA("tableGeometry == NULL");
					break;//breaks the for..tableIndex
				}
				PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1);
				//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
				if(tableCommands==NULL)
				{
					//CA("Err: invalid interface pointer ITableCommands");
					break;//continues the for..tableIndex
				}

				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);

				}
				if(rowPatternRepeatCount > 1) //again the same for making sure paste is called after copy
				{
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						//CA("canCopy == kFalse");
						break;
					}			
					//CA("6");
					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						//CA("tempPtr == NULL");
						break;
					}
					//CA("7");
					for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
						tempPtr = tableModel->Copy(gridAreaForEntireTable);
					}
				}
            
				for(int32 rowPatternIndex = 0;rowPatternIndex < rowPatternRepeatCount;rowPatternIndex++)
				{//start for rowPatternIndex
                    
					for(int32 indexOfRowInRowPattern = 0;indexOfRowInRowPattern <totalNumberOfRowsBeforeRowInsertion;indexOfRowInRowPattern++)
					{//start for indexOfRowInRowPattern
						for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
						{//start for..iterate through each column.
							int32 tagIndex = 
							rowPatternIndex * totalNumberOfRowsBeforeRowInsertion * totalNumberOfColumns +
							indexOfRowInRowPattern * totalNumberOfColumns + columnIndex;				
							XMLReference cellXMLElementRef = tagInfo.tagPtr->GetNthChild(tagIndex);
							
							//This is a tag attached to the cell.
							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
							//Get the text tag attached to the text inside the cell.
							//We are providing only one texttag inside cell.
							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
							{
								continue;
							}

							XMLContentReference cellXMLCntnRef = cellXMLElementPtr->GetContentReference();
							InterfacePtr<IXMLReferenceData>cellXMLRefData(cellXMLCntnRef.Instantiate());

							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(0);
							InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
							//The cell may be blank. i.e doesn't have any text tag inside it.
							if(cellTextTagPtr == NULL)
							{
								continue;
							}
							XMLContentReference xmlCntnRef = cellTextTagPtr->GetContentReference();
							InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());
							
							if(xmlCntnRef.IsTable())
							{
								
								InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
								if(innerTableModel == NULL)
								{
									//CA("innerTableModel == NULL");
									continue;
								}
								UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);

								XMLTagAttributeValue xmlTagAttrVal;
								//collect all the attributes from the tag.
								//typeId,LanguageID are important
								getAllXMLTagAttributeValues(cellTextXMLRef,xmlTagAttrVal);	

								TableUtility tUtils(NULL);

								if(xmlTagAttrVal.typeId.GetAsDouble() == -112)
								{
									tUtils.fillDataInCMedCustomTableInsideTable
									(
										cellTextXMLRef,
										innerTableModelUIDRef,
										pNode

									);
								}
								else
								{
									tUtils.FillDataTableForItemOrProduct
									(
										cellTextXMLRef,
										innerTableModelUIDRef,
										pNode.getIsProduct()
									);

								}

								InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
								ASSERT(fitFrameToContentCmd != nil);
								if (fitFrameToContentCmd == nil) {
								return;
								}
								fitFrameToContentCmd->SetItemList(UIDList(tagInfo.curBoxUIDRef));
								if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
								ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
								return;
								}
								
							}
							else if(cellTextTagPtr->GetAttributeValue(WideString("imgFlag"))==WideString("1"))
							{
							/*
								bool16 isGraphic = Utils<IXMLUtils>()->IsElementAGraphic(cellTextTagPtr);
								if(isGraphic)
								{
									CA("yes");
								}
								else
									CA("no");

								if(xmlCntnRef.IsInline())
								{
									InterfacePtr<IPathGeometry> iPathGeom(xmlRefData,UseDefaultIID());
									if(iPathGeom == NULL)
									{
										CA("iPathGeom == NULL");										
									}
									InterfacePtr<IShape> iShape(xmlRefData,UseDefaultIID());
									if(iShape == NULL)
									{
										CA("iShape == NULL");										
									}
									InterfacePtr<IInlineData> iILG(xmlRefData,UseDefaultIID());
									if(iILG == NULL)
									{
										CA("IInlineData == NULL");										
									}
									InterfacePtr<ITextFrame> iTf(xmlRefData,UseDefaultIID());
									if(iTf == NULL)
									{
										CA("ITextFrame == NULL");										
									}

									InterfacePtr<IStandOffItemData> iSOID(xmlRefData,UseDefaultIID());
									if(iSOID == NULL)
									{
										CA("IStandOffItemData == NULL");
									}

									InterfacePtr<IDCSInfo> iDcsInfo(xmlRefData,IID_IDCSINFO);
									if(iDcsInfo == NULL)
									{
										CA("IDCSInfo == NULL");
									}

									InterfacePtr<IGraphicFrameData> iGFD(xmlRefData,UseDefaultIID());
									if(iGFD == NULL)
									{
										CA("IGraphicFrameData == NULL");
									}

									InterfacePtr<IPlaceBehavior> iPLB(xmlRefData,UseDefaultIID());
									if(iPLB == NULL)
									{
										CA("IPlaceBehavior == NULL");
									}

									CA_NUM("frameKind : ", iPLB->GetFrameKind());

									InterfacePtr<IHierarchy> iHier(iPLB,UseDefaultIID());
									if(iHier == NULL)
									{
										CA("iHier == NULL");
										break;
									}
									
									UIDRef inlineUIDRef = xmlCntnRef.GetUIDRef();

									CA_NUM("UIDRef : ",inlineUIDRef.GetUID().Get());

									InterfacePtr<IPMUnknown> unknown(inlineUIDRef,IID_IUNKNOWN);
									if(unknown == NULL)
									{
										CA("unknown == NULL");
										break;
									}

									

									ClassID clsId = ::GetClass(unknown);

									CA_NUM("clsId :",clsId.Get());

									if(clsId == kPageItemBoss)
										CA("1");
									else if(clsId == kDrawablePageItemBoss)
										CA("2");
									else if(clsId == kFrameItemBoss)
										CA("3");
									else if(clsId == kSplineItemBoss)
										CA("4.99999");
									else if(clsId == kPlaceHolderItemBoss)
										CA("5");

									InterfacePtr<IHierarchy> parentHier(iHier->QueryParent(),UseDefaultIID());
									if(parentHier == NULL)
									{
										CA("Parent IHierarcy == NULL");
										break;
									}
									clsId = ::GetClass(parentHier);

									CA_NUM("clsId :",clsId.Get());

									if(clsId == kPageItemBoss)
										CA("1");
									else if(clsId == kDrawablePageItemBoss)
										CA("2");
									else if(clsId == kFrameItemBoss)
										CA("3");
									else if(clsId == kSplineItemBoss)
										CA("4.99999");
									else if(clsId == kPlaceHolderItemBoss)
										CA("5");

									UIDRef parentUIDRef = ::GetUIDRef(parentHier);

									UIDList uidList(parentUIDRef);

									CA_NUM("parentUIDRef : ",parentUIDRef.GetUID().Get());

									InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
									if(!iSelectionManager)
									{	//CA("Slection NULL");
										break;
									}

									InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
									if (!layoutSelectionSuite) {
										//CA("!layoutSelectionSuite");
										return;
									}
									CA("1");
									layoutSelectionSuite->DeselectAll();
									CA("2");
									layoutSelectionSuite->Select(uidList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
									//CA("inline");

									
									//UIDRef inlineUIDRef = xmlCntnRef.GetUIDRef();

									//InterfacePtr<IXMLTag>xmlTag(inlineUIDRef,UseDefaultIID());
									//if(xmlTag == NULL)
									//{
									//	CA("xmlTag == NULL");
									//	break;
									//}
									//CA(xmlTag->GetTagName());
									
									
									//InterfacePtr<IInlineData> inLineData(inlineUIDRef,UseDefaultIID());
									//if(inLineData == NULL)
									//{
									//	CA("inLineData == NULL");
									//	break;
									//}

									//TextIndex ilgIndex = inLineData->GetILGIndex();
									//CA_NUM("ilgIndex :",ilgIndex);
									
									

								}
								else
									CA("not inline");

								
								//CA("image tag");
								//InterfacePtr<IPMUnknown> frameUnknown(xmlRefData,IID_IUNKNOWN);
								//if(frameUnknown == NULL)
								//{
								//	CA("frameUnknown == NULL");
								//	break;
								//}
								//UIDRef frameUIDRef = ::GetUIDRef(frameUnknown);

								//CA_NUM("frameUID is :",frameUIDRef.GetUID().Get());
								

								
								

								//UIDList itemList(frameUIDRef);
								//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								//if(!iSelectionManager)
								//{ 
								//	//CA("Selection Manager is Null ");
								//	return;
								//}

								////UIDList itemList(boxUIDRef.GetDataBase(),boxUIDRef.GetUID());
								////	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
								//if (!layoutSelectionSuite) 
								//{	//CA("!layoutSelectionSuite");
								//	return;
								//}
								//layoutSelectionSuite->DeselectAll();
								//layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
								
								
								//InterfacePtr<ITransform> transform(frameUnknown,UseDefaultIID());
								//if(transform == NULL)
								//{
								//	CA("transform == NULL");
								//	break;
								//}
								//PMPoint center(0.0,0.0);
								//transform->RotateBy(45.0,center);
								//

								*/
								
							}
						}//end for ...interate through each column
					}//end for indexOfRowInRowPattern 
				}//end for rowPatternIndex
		}while(kFalse);
}//end function sprayTableInsideTableCell





void TableUtility::fillDataInCMedCustomTableInsideTable
(XMLReference & boxXMLRef, UIDRef& tableUIDRef, PublicationNode& pNode)
{
	//CA("in fillDataInCMedCustomTableInsideTable");
	//IIDXMLElement * boxXMLPtr = boxXMLRef.Instantiate();
//	InterfacePtr<IIDXMLElement>boxXMLPtr(boxXMLRef.Instantiate());
//	do
//	{
//		IsDBTable = kTrue;
//		int32 IsAutoResize = 0;
//		
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//
//		int32 objectId = pNode.getPubId(); //Product object		
//
//		CMedCustomTableScreenValue* customtableInfoptr = ptrIAppFramework->GetONEsourceObjects_getMed_CustomTableScreenValue(objectId);
//		if(customtableInfoptr == NULL)
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::putHeaderDataInKitComponentTable: Err: invalid interface pointer ITableFrame");
//			break;
//		}
//		CMedCustomTableScreenValue customtableInfo = *customtableInfoptr;//Currently it is empty ,And should be filled from DB
//		
//		//CA("Return to DataSprayer");
//		if(customtableInfo.HeaderList.size()==0){//replace v
//			break;
//		}
//
//		//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		//if(!iSelectionManager)
//		//{	//CA("Slection NULL");
//		//	break;
//		//}
//
//		//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		//if (!layoutSelectionSuite) {
//		//	return;
//		//}
//
//		//UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		////selectionManager->DeselectAll(NULL); // deselect every active CSB
//		//layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//
//		//TableStyleUtils TabStyleObj;
//		//TabStyleObj.SetTableModel(kTrue);
//		//TabStyleObj.GetTableStyle();
//		//TabStyleObj.getOverallTableStyle();
//
//		//InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//		//( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		//if(!txtMisSuite)
//		//{
//		//	//CA("My Suit NULL");
//		//	//break;
//		//}
//	
//		//PMRect theArea;
//		//PMReal rel;
//		//int32 ht;
//		//int32 wt;
//
//		//InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		//if(iGeometry==NULL)
//		//	break;
//
//		//theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		//rel = theArea.Height();
//		//ht=::ToInt32(rel);
//		//rel=theArea.Width();	
//		//wt=::ToInt32(rel);
//
//		InterfacePtr<ITableModel> tableModel(tableUIDRef,UseDefaultIID());
//		if(tableModel == NULL)
//		{
//		//	CA("tableModel == NULL");
//			break;
//		}
//		//Get the TextModel
//		InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
//		if(tableTextContent == NULL)
//		{
//			//CA("tableTextContent == NULL");
//			break;
//		}
//		UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();
//
//		InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel()/*,UseDefaultIID()*/);//Amit
//		if(textModel == NULL)
//		{
//			//CA("textModel == NULL");
//			break;
//		}
//
//		XMLTagAttributeValue xmlTagAttrVal;
//		//collect all the attributes from the tag.
//		//typeId,LanguageID are important
//		getAllXMLTagAttributeValues(boxXMLRef,xmlTagAttrVal);		
//		//SectionLevelItem has two important IDs.PubId and PBObjectID.
//		//PBObjectID is used while spraying its item table.
//		PMString parentIDstr("");
//		parentIDstr.AppendNumber(pNode.getPubId());		
//		xmlTagAttrVal.parentID = parentIDstr;
//
//		PMString parentTypeIDstr("");
//		parentTypeIDstr.AppendNumber(pNode.getTypeId());
//		xmlTagAttrVal.parentTypeID = parentTypeIDstr;
//
//		PMString sectionIDstr("");
//		sectionIDstr.AppendNumber(pNode.getParentId());
//		xmlTagAttrVal.sectionID =sectionIDstr;
//		xmlTagAttrVal.tableFlag ="-12";//default..Parent of the tag is Product
//
//		if(xmlTagAttrVal.isAutoResize.GetAsNumber() == 1)
//			IsAutoResize= 1;
//
//		if(xmlTagAttrVal.isAutoResize.GetAsNumber() == 2)
//			IsAutoResize= 2;
//
//		if(xmlTagAttrVal.header.GetAsNumber() == -1)//if(xmlTagAttrVal.colno.GetAsNumber() == -1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//		
////CA("2");
//		//Update the FrameXMLTag i.e tagAttached To the Table
//		setAllXMLTagAttributeValues(boxXMLRef ,xmlTagAttrVal);
//		
//		int32 typeId =xmlTagAttrVal.typeId.GetAsNumber();
//		int32 languageId = xmlTagAttrVal.LanguageID.GetAsNumber();
//		PMString colnoForTableFlag = xmlTagAttrVal.colno;
//		PMString isHeaderFlag = xmlTagAttrVal.header;
//
//
//		int32 numRows=0;
//		int32 numCols=0;
//
//		numRows =static_cast<int32> (customtableInfo.Tabledata.size());  
//		if(numRows<=0)
//			break;		
//
//		numCols =static_cast<int32>(customtableInfo.HeaderList.size());	
//		if(numCols<=0)
//			break;
//
//		/*PMString ASD(" no Rows : ");
//		ASD.AppendNumber(numRows);
//		ASD.Append("   no Clos : ");
//		ASD.AppendNumber(numCols);
//		CA(ASD);*/
//
//		bool8 isTranspose = kFalse;//Hardcoded as false for Medtronics
//		/*InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//		if(!tWrite)
//			break;*/
//
//		if(!isTranspose)
//		{
//			int32 i=0, j=0;
//			resizeTable(tableUIDRef, numRows, numCols, isTranspose, 0, 0);
//					
//			if(AddHeaderToTable)
//			{
//				/*putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef); following function is commented by vijay on 16-10-2006*/
//				do
//				{								
//					if(ptrIAppFramework == NULL)
//					{
//						//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//						break;
//					}
//					for(int k=0; k<customtableInfo.HeaderList.size(); k++)
//					{					
//						InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//						if (tableModel == NULL)
//						{
//							break;
//						}
//
//						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//						if(tableCommands==NULL)
//						{
//							break;
//						}
//						//CA("Cell String : " + customtableInfo.HeaderList[k].getdisplayString());
//						//WideString* wStr=new WideString(customtableInfo.HeaderList[k].getdisplayString());
//						//				
//						//SlugStruct stencileInfo;						
//						//					
//						//tableCommands->SetCellText(*wStr, GridAddress(0, k));
//						//
//						//stencileInfo.elementId =customtableInfo.HeaderList[k].getvalueAttributeId();												
//						//PMString TableColName("");
//						//TableColName.Append("ATTRID_");
//						//TableColName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//				
//						//stencileInfo.elementName = TableColName;
//						//stencileInfo.TagName = TableColName;
//						//stencileInfo.LanguageID =tStruct.languageID;
//				
//						//stencileInfo.typeId  = -2; 
//						//stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
//						//stencileInfo.parentTypeId = pNode.getTypeId();					
//						//stencileInfo.whichTab = tStruct.whichTab;
//						//stencileInfo.imgFlag = CurrentSectionID; 
//						//stencileInfo.isAutoResize = tStruct.isAutoResize;
//						//stencileInfo.col_no = tStruct.colno;
//						//XMLReference txmlref;
//						////CA("DataSprayer 11.........");
//						//this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), "PSTable", txmlref, stencileInfo, 0, k, 0 );
//
//
//						GridAddress cellAddr;						
//						cellAddr.Set(0, k);						
//						PMString dispname =customtableInfo.HeaderList[k].getdisplayString();
//						//CA("dispname: "+dispname );			
//						PMString textToInsert("");
//						//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//						//if(!iConverter)
//						{
//							textToInsert=dispname;					
//						}
//						//else
//						//	textToInsert=iConverter->translateString(dispname);
//					
//						//Update the Tag and its attribute values.
//						PMString tagName("ATTRID_");
//						tagName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//
//						///start
//						PMString attrVal;
//						//XMLTagAttributeValue xmlTagAttrVal;
//						//tagName
//						xmlTagAttrVal.tagName = tagName;
//						//ID..it is tableHeaderID			
//						attrVal.Clear();
//						attrVal.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//						xmlTagAttrVal.ID =attrVal;
//						//typeId..it is -2 for tableHeader row
//						attrVal.Clear();
//						/*attrVal.AppendNumber(-2);
//						xmlTagAttrVal.typeId=attrVal;*/
//						attrVal.AppendNumber(1);
//						xmlTagAttrVal.header=attrVal;
//
//						AddTagToCellText
//							(
//								cellAddr,
//								tableModel,
//								textToInsert,
//								textModel,
//								xmlTagAttrVal
//							);	
//
//					}
//				}
//				while(kFalse);
//			 i=1; j=0;
//			}
//
//			
//			int32 BodyRowCount = static_cast<int32>(customtableInfo.Tabledata.size());
//		/*	PMString ASD1(" BodyRowCount : ");
//			ASD1.AppendNumber(BodyRowCount);
//			CA(ASD1);*/
//			for(int32 count2 =0 ; count2 < BodyRowCount ;count2++) //new added by v
//			{	
//
//				VectorCustomItemTableRowInfo rowVector = customtableInfo.Tabledata[count2];
//				int32 rowVectorSize = static_cast<int32>(rowVector.size());
//				//PMString ASD1(" rowVectorSize : ");
//				//ASD1.AppendNumber(rowVectorSize);
//				//CA(ASD1);
//				for(int32 count1 =0 ; count1 < rowVectorSize ;count1++)//new added by v
//				{		
//					//PMString ASD("row : " );
//					//ASD.AppendNumber(i);
//					//ASD.Append("  col : ");
//					//ASD.AppendNumber(j);
//					//CA(ASD + "\n" + "Body Cell String : " + rowVector[count1].getdisplayString());
//					//setTableRowColData(tableUIDRef,rowVector[count1].getdisplayString() , i, j);	
//					//SlugStruct stencileInfo;
//
//					//stencileInfo.elementId = rowVector[count1].getvalueAttributeId();
//				
//					//PMString TableColName("");
//					//TableColName.Append("ATTRID_");
//					//TableColName.AppendNumber(rowVector[count1].getvalueAttributeId()/*vec_tableheaders[j-1]*/); // replaced v
//
//					//stencileInfo.elementName = TableColName;
//					//stencileInfo.TagName = TableColName;
//					//stencileInfo.LanguageID =tStruct.languageID;
//					//					
//					//stencileInfo.typeId  = rowVector[count1].getItemId();
//					//stencileInfo.parentId = pNode.getPubId();
//					//stencileInfo.parentTypeId = pNode.getTypeId();
//					////4Aug..ItemTable
//					////stencileInfo.whichTab = 4; // Item Attributes
//					//stencileInfo.whichTab = tStruct.whichTab;
//					//stencileInfo.imgFlag = CurrentSectionID; 
//					//stencileInfo.isAutoResize = tStruct.isAutoResize;
//					//stencileInfo.col_no = tStruct.colno;
//					//XMLReference txmlref;
//					////CA(tStruct.tagPtr->GetTagString());
//					//this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), "PSTable", txmlref, stencileInfo, i, j,0 );
//
//					PMString dataToBeSprayed("");
//					PMString strdataToBeSprayed("");
//					strdataToBeSprayed = rowVector[count1].getdisplayString();
//					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//					//if(!iConverter)
//					{
//						dataToBeSprayed=(strdataToBeSprayed);					
//					}
//					//else
//					//	dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);
//
//					//CA(dataToBeSprayed);
//					GridAddress cellAddr;					
//					cellAddr.Set(i, j);				
//					
//					//Update the Tag and its attribute values.
//					PMString tagName("ATTRID_");
//					tagName.AppendNumber(rowVector[count1].getvalueAttributeId());
//					///start
//					PMString attrVal;
//					//XMLTagAttributeValue xmlTagAttrVal;
//					//tagName
//					xmlTagAttrVal.tagName = tagName;
//					//ID..it is attributeID			
//					attrVal.Clear();
//					attrVal.AppendNumber(rowVector[count1].getvalueAttributeId());
//					xmlTagAttrVal.ID =attrVal;
//					//typeId..it is 
//					attrVal.Clear();
//					attrVal.AppendNumber(rowVector[count1].getItemId());
//					/*xmlTagAttrVal.typeId=attrVal;*/
//					xmlTagAttrVal.childId=attrVal;
//
//					AddTagToCellText
//					(
//						cellAddr,
//						tableModel,
//						dataToBeSprayed,
//						textModel,
//						xmlTagAttrVal
//
//					);
//					j++;
//				}
//				i++;
//				j=0;
//			}
//			
//		}
//	
//		//if(IsAutoResize == 1)
//		//{
//		//	UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//
//		//			//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		//			//if (!layoutSelectionSuite) 
//		//			//{	//CA("!layoutSelectionSuite");
//		//			//	return;
//		//			//}
//		//			//layoutSelectionSuite->DeselectAll();
//		//			//layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
//		//			//InterfacePtr<ITextMiscellanySuite> txtMisSuite1(static_cast<ITextMiscellanySuite* >
//		//			//( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		//			//if(!txtMisSuite1)
//		//			//{
//		//			//	//CA("My Suit NULL");
//		//			//	return;
//		//			//}
//		//			//InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite1,UseDefaultIID());
//		//			//if(!frmContentSuite)
//		//			//{
//		//			//	//CA("IFrame Content Suite is null");
//		//			//	return;
//		//			//}
//		//	//		frmContentSuite->FitFrameToContent();
//		//	//CA("2222222");
//		//	//	if(frmContentSuite->CanFitContentProp())
//		//	//	{ CA("2 in Spray for this box");
//		//	//		frmContentSuite->FitContentProp();
//		//	//	}
//
//		//	//Commeted By Rahul to avoid Text Frame resizing for Lazboy
//		//	UIDList processedItems;
//		//	K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//		//	ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//		//	return ;
//		//}		
//
//		//if(IsAutoResize == 2)
//		//{
//		//	//CA("Inside overflow");
//		//	ThreadTextFrame ThreadObj;
//		//	ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
//		//}
//		//
//		//TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
//		//TabStyleObj.ApplyTableStyle();
//		//TabStyleObj.setTableStyle();
//	
//	}while(kFalse);
//	IsDBTable = kFalse;

}




void TableUtility::fillDataInXRefTable
(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double & tableId,const UIDRef& BoxUIDRef)
{
	//CA("in fillDataInXRefTable");
	//UIDRef boxUIDRef = BoxUIDRef;
	//do
	//{
	//	IsDBTable = kTrue;		//This metod is getting called only for DBTable.
	//	int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
	//	if(tStruct.isAutoResize==1)
	//		IsAutoResize= 1;
	//	
	//	if(tStruct.isAutoResize==2)
	//		IsAutoResize= 2;

	//	if(tStruct.header == 1)//if(tStruct.colno == -1)
	//		AddHeaderToTable = kTrue;
	//	else
	//		AddHeaderToTable = kFalse;

	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	int32 objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		

	//	int32 sectionid = -1;
	//	if(global_project_level == 3)
	//		sectionid = CurrentSubSectionID;
	//	if(global_project_level == 2)
	//		sectionid = CurrentSectionID;
	//		
	//	int32 itemId = pNode.getPubId();  //vec_item_new[0]/*30017240*/;

	//	/*PMString ASD("itemid: ");
	//	ASD.AppendNumber(itemId);
	//	CA(ASD);*/

	//	VectorScreenTableInfoPtr XReftableInfo = NULL;
	//	
	//	XReftableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemId, tStruct.languageID); 
	//	if(!XReftableInfo)
	//	{
	//		//CA("XReftableInfo is NULL");
	//		return;
	//	}

	//	if(XReftableInfo->size()==0){
	//		//CA(" XReftableInfo->size()==0");
	//		break;
	//	}

	//	/* Output of above method is of following format from Application framework :
	//	com.apsiva.servletframework.model.ObjectTableScreenValue@fcfa52
	//	tableId = null
	//	typeId = null
	//	tableName = XRef
	//	transpose = false
	//	printHeader = true
	//	tableHeader = [Item Id, Cross-reference Type, Rating, Alternate, Comments, Catalog, 11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
	//	tableData =[[8, Competitor Reference, 5, false, , false, Test, DSA, 12023, , , ], 
	//		[27, Competitor Reference, 1, false, , false, New Manufacturer, 999, 90op, , , ], 
	//		[30022119, Manufacturer Reference, 5, false, , true, New Manufacturer, 289, Parquetry  Super Set, , 26.00, 18.95], 
	//		[3, Manufacturer Reference, 7, false, ed, false, New Manufacturer, 334, drt, , , ]]

	//	seqList = null
	//	rowIdList = null
	//	columnIdList = null
	//	notesList = []

	//	*/

	//	CItemTableValue oTableValue;
	//	VectorScreenTableInfoValue::iterator it  ;
	//	it = XReftableInfo->begin();
	//	oTableValue = *it;
	//		
	//	//It checks the selection present or not........
	//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
	//	if(!iSelectionManager)
	//	{	//CA("Slection NULL");
	//		break;
	//	}

	//	InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
	//	if (!layoutSelectionSuite) {
	//		//CA("!layoutSelectionSuite");
	//		return;
	//	}
	//	//It gets refernce to persistant objects present in the (database) document as long as document is open.
	//	UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
	//	//selectionManager->DeselectAll(NULL); // deselect every active CSB
	//	layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
	//	
	//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	//	if(!txtMisSuite)
	//	{
	//		//CA("My Suit NULL");
	//		//break;
	//	}
	//	//These values are taken from db
	//	PMRect theArea;
	//	PMReal rel;
	//	int32 ht;
	//	int32 wt;

	//	InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
	//	if(iGeometry==NULL)
	//		break;

	//	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	//	rel = theArea.Height();
	//	ht=::ToInt32(rel);
	//	rel=theArea.Width();	
	//	wt=::ToInt32(rel);
	//			
	//	int32 numRows=0;
	//	int32 numCols=0;		

	//	numRows =static_cast<int32>(oTableValue.getTableData().size());
	//	if(numRows<=0)
	//		break; 
	//	numCols =static_cast<int32>(oTableValue.getTableHeader().size() ); 
	//	if(numCols<=0)
	//		break;
	//	bool8 isTranspose = oTableValue.getTranspose();
	//	//For testing by dattatray on 14/10
	//	/*if(isTranspose == kTrue)
	//		CA("isTranspose is kTrue");*/
	//	PMString numRow("numRows::");
	//	numRow.AppendNumber(numRows);
	//	//CA(numRow);
	//	PMString numCol("numcols::");
	//	numCol.AppendNumber(numCols);
	//	//CA(numCol);
	//	//It should be removed by dattatray after testing
	////	isTranspose = kTrue;
	//	/*InterfacePtr<ITagWriter> tWrite
	//	((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
	//	if(!tWrite)
	//		break;*/

	//	TableFlagForStandardProductTable = 0;  // Since We are spraying Item Table
	//	
	//	//CA("Inside !isTranspose");
	//	/*PMString AS(" tStruct.whichtab : ");
	//	AS.AppendNumber(tStruct.whichTab);
	//	CA(AS);*/
	//	int32 i=0, j=0;
	//	
	//	//resizeTable gets called if Auto Re-Size is selected..
	//	resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);

	//	vector<int32> vec_tableheaders;
	//	vector<int32> vec_items;

	//	vec_tableheaders = oTableValue.getTableHeader();
	//	if(AddHeaderToTable){
	//		putHeaderDataInXRefTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
	//		i=1; j=0;
	//	}

	//	vec_items = oTableValue.getItemIds();

	//	//vector<int32>::iterator temp;
	//	/*for(int i1=0;i1<vec_items.size();i1++)
	//	{
	//		PMString AS;
	//		AS.AppendNumber(vec_items[i1]);
	//		CA(AS);
	//	}*/
	//	vector<vector<PMString> > vec_tablerows;
	//	vec_tablerows = oTableValue.getTableData();
	//	vector<vector<PMString> >::iterator it1;

	//	//tableData =[[ Competitor Reference, 5, false, , false, Test, DSA, 12023, , , ]
	//	//tableData =[ Manufacturer Reference, 5, false, , true, New Manufacturer, 289, Parquetry  Super Set, , 26.00, 18.95],	

	//	//tableHeader = [-807, -808, -809, -810, -811,  11000001, 11000005, 11000012, 11000020, 11000033, 11000043]

	//	  // This is to Print Regular Item Attributes first and then Hardcoded XREF attributes
	//	for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
	//	{
	//		j=5;
	//		vector<PMString>::iterator it2;
	//		for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//		{
	//			if(it2==(*it1).begin())
	//			{
	//				it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
	//				it2++;
	//				it2++;
	//				it2++;
	//				it2++;					
	//			}
	//		
	//			setTableRowColData(tableUIDRef, (*it2), i, (j-5));
	//			//CA((*it2));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = vec_tableheaders[j];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
	//			stencileInfo.elementName = dispname;
	//			//CA(dispname);
	//			PMString TableColName("");  // = oAttribVal.getTable_col();
	//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[j]);
	//			/*}*/
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID =tStruct.languageID;
	//			stencileInfo.typeId = -1;
	//			if(AddHeaderToTable)
	//				stencileInfo.childId  = vec_items[i-1];
	//			else
	//				stencileInfo.childId  = vec_items[i];

	//			stencileInfo.parentId = pNode.getPubId();
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;
	//			//CA("Before Tag Table");
	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
	//			this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, (j-5), tableId );
	//			//CA("After Tag Table ...2");				

	//		j++;
	//		}

	//		int32 k=0;
	//		for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//		{
	//			if(k >= 5)
	//				break;
	//			setTableRowColData(tableUIDRef, (*it2), i, (j-5));
	//			//CA((*it2));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = vec_tableheaders[k];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[k],tStruct.languageID );// changes is required language ID req.
	//			stencileInfo.elementName = dispname;
	//			//CA(dispname);
	//			PMString TableColName("");  // = oAttribVal.getTable_col();
	//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[k]);
	//			/*}*/
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID =tStruct.languageID;
	//			stencileInfo.typeId = -1;
	//			if(AddHeaderToTable)
	//				stencileInfo.childId  = vec_items[i-1];
	//			else
	//				stencileInfo.childId  = vec_items[i];

	//			stencileInfo.parentId = pNode.getPubId();
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;
	//			//CA("Before Tag Table");
	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
	//			this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, (j-5), tableId );
	//			//CA("After Tag Table ...2");		

	//			j++;
	//			k++;
	//		}
	//						
	//		
	//		i++;
	//		j=0;
	//	}
	//	i=0;j=0;			
	//	
	//	
	//	if(IsAutoResize == 1)
	//	{
	//		//CA("Inside IsAutoResize");
	//		UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());			
	//		UIDList processedItems;
	//		K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
	//		
	//		ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
	//		
	//		return ;
	//	}

	//	if(IsAutoResize == 2)
	//	{
	//		//CA("Inside overflow");
	//		ThreadTextFrame ThreadObj;
	//		ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
	//	}		
	//	
	//}while(kFalse);
	//IsDBTable = kFalse;

}


void TableUtility::putHeaderDataInXRefTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInKitComponentTable");
	//do
	//{
	//	HeadersizeVector.clear();
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
	//	if (tableModel == NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableFrame");
	//		break;
	//	}

	//	InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//	if(tableCommands==NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableCommands");
	//		break;
	//	}

	//	//tableHeader = [-807, -808, -809, -810, -811,  11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
	//	//Cross-reference Type, Rating, Alternate, Comments, Catalog 
	//	int i=0;			
	//	
	//	for(i=5;i<vec_tableheaders.size();i++)
	//	{
	//		/*PMString ASD(" First Value of i : ");
	//		ASD.AppendNumber(i);
	//		CA(ASD);*/
	//		PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//		
	//		WideString* wStr=new WideString(dispname);
	//		
	//		int32 StringLength = dispname.NumUTF16TextChars();
	//		HeadersizeVector.push_back(StringLength);
	//	
	//		/*PMString ASD("vec_tableheaders[i] : ");
	//		ASD.AppendNumber(vec_tableheaders[i]);
	//		CA(ASD);*/

	//		tableCommands->SetCellText(*wStr, GridAddress(0, (i- 5)));
	//		SlugStruct stencileInfo;
	//		stencileInfo.elementId =vec_tableheaders[i];			
	//		stencileInfo.elementName = dispname;
	//		//CA("Inside PutHeaderDataIN Table");	CA(dispname);
	//		PMString TableColName("");// oAttribVal.getTable_col();
	//		
	//		TableColName.Append("ATTRID_");
	//		TableColName.AppendNumber(vec_tableheaders[i]);
	//		
	//		stencileInfo.TagName = TableColName;
	//		stencileInfo.LanguageID =tStruct.languageID;
	//		//CA(TableColName);
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;

	//		//CA(tStruct.tagPtr->GetTagString());
	//		//CA(stencileInfo.colName);
	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5) , 0 );
	//		//	CA("After Tag Table");			
	//		delete wStr;
	//	}
	//	
	//	/*PMString ASD(" Value of i After For : ");
	//	ASD.AppendNumber(i);
	//	CA(ASD);*/

	//	{   // For Cross-reference Type Start
	//		/*PMString ASD(" Value of (i-5) : ");
	//		ASD.AppendNumber((i-5));
	//		CA(ASD);*/
	//		WideString* wStr=new WideString("Cross-reference Type");
	//		tableCommands->SetCellText(*wStr, GridAddress(0, (i-5)));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -807;		
	//		stencileInfo.TagName = "Cross-reference Type";
	//		stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
	//		delete wStr;			
	//		i++;
	//	}   // // For Cross Referance Type End

	//	{   // For Rating Start
	//		WideString* wStr=new WideString("Rating");
	//		tableCommands->SetCellText(*wStr, GridAddress(0, (i-5)));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -808;		
	//		stencileInfo.TagName = "Rating";
	//		stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
	//		delete wStr;			
	//		i++;
	//	}   // For Rating End

	//	{   // For Alternate Start
	//		WideString* wStr=new WideString("Alternate");
	//		tableCommands->SetCellText(*wStr, GridAddress(0, (i-5)));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -809;		
	//		stencileInfo.TagName = "Alternate";
	//		stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
	//		delete wStr;			
	//		i++;
	//	}   // For Alternate End

	//	{   // For Comments Start
	//		WideString* wStr=new WideString("Comments");
	//		tableCommands->SetCellText(*wStr, GridAddress(0, (i-5)));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -810;		
	//		stencileInfo.TagName = "Comments";
	//		stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0,(i-5), 0 );
	//		delete wStr;			
	//		i++;
	//	}   // For Alternate End

	//	{   // For Catalog Start
	//		WideString* wStr=new WideString("");   //// Catalog HEADER IS Blank.
	//		tableCommands->SetCellText(*wStr, GridAddress(0,(i-5)));
	//		SlugStruct stencileInfo;	
	//		stencileInfo.elementId = -811;		
	//		stencileInfo.TagName = "Catalog";
	//		stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
	//		stencileInfo.LanguageID =tStruct.languageID;	
	//		stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//		stencileInfo.header = 1;
	//		stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//		stencileInfo.parentTypeId = pNode.getTypeId();
	//		//4Aug..ItemTable
	//		//stencileInfo.whichTab = 4; // Item Attributes
	//		stencileInfo.whichTab = tStruct.whichTab;
	//		stencileInfo.imgFlag = CurrentSectionID; 
	//		stencileInfo.isAutoResize = tStruct.isAutoResize;
	//		stencileInfo.col_no = tStruct.colno;
	//		stencileInfo.tableTypeId = tStruct.typeId;
	//		XMLReference txmlref;			

	//		PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//		if(TableTagNameNew.Contains("PSTable"))
	//		{
	//			//CA("PSTable Found");
	//		}
	//		else
	//		{				
	//			TableTagNameNew =  "PSTable";						
	//		}
	//		this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
	//		delete wStr;			
	//		i++;
	//	}   // For Catalog End
	//	
	//}
	//while(kFalse);
}

void TableUtility::fillDataInAccessoryTable(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	////CA("Inside TableUtility::fillDataInAccessoryTable");
	//UIDRef boxUIDRef = BoxUIDRef;
	//do
	//{

	//	IsDBTable = kTrue;		//This metod is getting called only for DBTable.
	//	int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
	//	if(tStruct.isAutoResize==1)
	//		IsAutoResize= 1;
	//	
	//	if(tStruct.isAutoResize==2)
	//		IsAutoResize= 2;

	//	if(tStruct.header == 1)//if(tStruct.colno == -1)
	//		AddHeaderToTable = kTrue;
	//	else
	//		AddHeaderToTable = kFalse;

	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	int32 objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.

	//	int32 sectionid = -1;
	//	if(global_project_level == 3)
	//		sectionid = CurrentSubSectionID;
	//	if(global_project_level == 2)
	//		sectionid = CurrentSectionID;
	//		
	//	int32 itemId = pNode.getPubId();  //vec_item_new[0]/*30017240*/;

	//	/*PMString ASD("itemid: ");
	//	ASD.AppendNumber(itemId);
	//	CA(ASD);*/

	//	VectorScreenTableInfoPtr AccessorytableInfo = NULL;
	//		
	//	AccessorytableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemId, tStruct.languageID); 
	//	if(!AccessorytableInfo)
	//	{
	//		CA("AccessorytableInfo is NULL");
	//		return;
	//	}

	//	if(AccessorytableInfo->size()==0){
	//		CA(" AccessorytableInfo->size()==0");
	//		break;
	//	}

	//	/*PMString AccessorytableInfoSize = "";
	//	AccessorytableInfoSize.AppendNumber(AccessorytableInfo->size());
	//	CA("AccessorytableInfoSize = " + AccessorytableInfoSize);*/

	//	/* Output of above method is of following format from Application framework :
	//		com.apsiva.servletframework.model.ObjectTableScreenValue@fcfa52
	//		tableId = null
	//		typeId = null
	//		tableName = Accessory
	//		transpose = false
	//		printHeader = true
	//		tableHeader = [Item Id, Quantity, Required, Comments, 11000005, 11000012, 11000020, 11000033, 11000043]
	//		tableData =[[30021729, 1, False, , CGO60610P, Wall Poster, 24" x 36", 37.88, 31.99], 
	//					[30021730, 1, false, , CGO60610B, Desk Blotter, 18" x 24", 18.88, 12.99],
	//					[30021731, 1, false, , CGO60610A, Album Photo, 5" x 7", 4.88, 2.99], 
	//					[30021732, 1, false, , CGO60604M, Chicago from the North, 7' x 10', 161.81, 149.99], 
	//					[30021733, 1, false, , CGO60604P, Wall Poster, 24" x 36", 37.32, 31.99],
	//					[30021734, 1, false, , CGO60604B, Large Desk Blotter, 20" x 26", 18.45, 11.99], 
	//					[30021736, 1, false, , CGO60667M, Chicago Lakefront, 7' x 10', 161.54, 149.99], 
	//					[30021737, 1, false, , CGO60667S, Back-Lit Sign, 30" x 48", 137.37, 131.99], 
	//					[30021738, 1, false, , CGO60667P, Wall Poster, 24" x 36", 37.88, 32.99]]


	//		seqList = null
	//		rowIdList = null
	//		columnIdList = null
	//		notesList = []

	//		*/

	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;
	//		it = AccessorytableInfo->begin();
	//		oTableValue = *it;

	//		//It checks the selection present or not........
	//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
	//		if(!iSelectionManager)
	//		{	CA("Slection NULL");
	//			break;
	//		}

	//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
	//		if (!layoutSelectionSuite) {
	//			CA("!layoutSelectionSuite");
	//			return;
	//		}

	//		//It gets refernce to persistant objects present in the (database) document as long as document is open.
	//		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
	//		//selectionManager->DeselectAll(NULL); // deselect every active CSB
	//		layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
	//		
	//		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	//		if(!txtMisSuite)
	//		{
	//			CA("My Suit NULL");
	//			//break;
	//		}
	//		//These values are taken from db
	//		PMRect theArea;
	//		PMReal rel;
	//		int32 ht;
	//		int32 wt;

	//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
	//		if(iGeometry==NULL)
	//			break;

	//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	//		rel = theArea.Height();
	//		ht=::ToInt32(rel);
	//		rel=theArea.Width();	
	//		wt=::ToInt32(rel);
	//				
	//		int32 numRows=0;
	//		int32 numCols=0;		

	//		numRows =static_cast<int32>(oTableValue.getTableData().size());
	//		if(numRows<=0)
	//			break; 
	//		numCols =static_cast<int32>(oTableValue.getTableHeader().size() ); 
	//		if(numCols<=0)
	//			break;
	//		bool8 isTranspose = oTableValue.getTranspose();
	//		//For testing by dattatray on 14/10
	//		/*if(isTranspose == kTrue)
	//			CA("isTranspose is kTrue");*/
	//		PMString numRow("numRows::");
	//		numRow.AppendNumber(numRows);
	//		//CA(numRow);
	//		PMString numCol("numcols::");
	//		numCol.AppendNumber(numCols);
	//		//CA(numCol);
	//		
	//	//	isTranspose = kTrue;
	//		/*InterfacePtr<ITagWriter> tWrite
	//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
	//		if(!tWrite)
	//			break;*/

	//		TableFlagForStandardProductTable = 0;  // Since We are spraying Item Table
	//		
	//		//CA("Inside !isTranspose");
	//		/*PMString AS(" tStruct.whichtab : ");
	//		AS.AppendNumber(tStruct.whichTab);
	//		CA(AS);*/
	//		int32 i=0, j=0;
	//		
	//		//resizeTable gets called if Auto Re-Size is selected..
	//		resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);

	//		vector<int32> vec_tableheaders;
	//		vector<int32> vec_items;

	//		vec_tableheaders = oTableValue.getTableHeader();
	//		if(AddHeaderToTable)
	//		{
	//			putHeaderDataInAccessoryTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
	//			i=1; j=0;
	//		}

	//		vec_items = oTableValue.getItemIds();
	//		//vector<int32>::iterator temp;
	//		/*for(int i1=0;i1<vec_items.size();i1++)
	//		{
	//			PMString AS;
	//			AS.AppendNumber(vec_items[i1]);
	//			CA(AS);
	//		}*/
	//		vector<vector<PMString> > vec_tablerows;
	//		vec_tablerows = oTableValue.getTableData();
	//		vector<vector<PMString> >::iterator it1;

	//		for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
	//		{
	//			vector<PMString>::iterator it2;
	//			for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//			{	
 //                   setTableRowColData(tableUIDRef, (*it2), i,j);	
	//				SlugStruct stencileInfo;
	//				stencileInfo.elementId = vec_tableheaders[j];
	//			//	CAttributeValue oAttribVal;
	//			//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
	//			//	PMString dispname = oAttribVal.getDisplay_name();
	//				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
	//				stencileInfo.elementName = dispname;
	//				PMString TableColName("");// = oAttribVal.getTable_col();
	//				//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//	{
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(vec_tableheaders[j]);
	//			//	}
	//				//CA(TableColName);
	//				stencileInfo.TagName = TableColName;
	//				stencileInfo.LanguageID =tStruct.languageID;
	//				stencileInfo.typeId = -1;
	//				
	//				if(AddHeaderToTable)
	//					stencileInfo.childId  = vec_items[i-1];
	//				else
	//					stencileInfo.childId  = vec_items[i];

	//				stencileInfo.parentId = pNode.getPubId();
	//				stencileInfo.parentTypeId = pNode.getTypeId();
	//				//4Aug..ItemTable
	//				//stencileInfo.whichTab = 4; // Item Attributes
	//				stencileInfo.whichTab = tStruct.whichTab;
	//				stencileInfo.imgFlag = CurrentSectionID; 
	//				stencileInfo.isAutoResize = tStruct.isAutoResize;
	//				stencileInfo.LanguageID = tStruct.languageID;

	//				stencileInfo.col_no = tStruct.colno;
	//				stencileInfo.tableTypeId = tStruct.typeId;
	//				XMLReference txmlref;
	////CA(tStruct.tagPtr->GetTagString());		

	//				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//				if(TableTagNameNew.Contains("PSTable"))
	//				{
	//					//CA("PSTable Found");
	//				}
	//				else
	//				{				
	//					TableTagNameNew =  "PSTable";						
	//				}

	//				this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
	//				j++;
	//				
	//				//if(it2==(*it1).begin())
	//				//{
	//				//	it2++; // This is to avoid Availabilty data spray which is in second position og it2
	//				//	//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
	//				//}
	//			}
	//			i++;
	//			j=0;
	//		}
	//}while(kFalse);

}

void TableUtility::putHeaderDataInAccessoryTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInAccessoryTable");
	//do
	//{
	//	HeadersizeVector.clear();
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
	//	if (tableModel == NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableFrame");
	//		break;
	//	}

	//	InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//	if(tableCommands==NULL)
	//	{
	//	//	CA("Err: invalid interface pointer ITableCommands");
	//		break;
	//	}

	//	int i=0;
	//	
	//	//Adding Quantity , Required , Comments
	//	if(isTranspose)
	//	{
	//		//Quantity
	//		{
	//			WideString* wStr=new WideString("Quantity");
	//			tableCommands->SetCellText( *wStr, GridAddress(i, 0));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -812;		
	//			stencileInfo.TagName = "Quantity";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0, 0 );

	//			delete wStr;
	//		}
	//		i++;
	//		
	//		//Required
	//		{
	//			WideString* wStr=new WideString("Required");
	//			tableCommands->SetCellText( *wStr, GridAddress(i, 0));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -813;		
	//			stencileInfo.TagName = "Required";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0,0 );

	//			delete wStr;
	//		}

	//		i++;

	//		//Comments
	//		{
	//			WideString* wStr=new WideString("Comments");
	//			tableCommands->SetCellText( *wStr, GridAddress(i, 0));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -814;		
	//			stencileInfo.TagName = "Comments";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0, 0 );

	//			delete wStr;
	//		}
	//	}
	//	else
	//	{
	//		//Quantity
	//		{
	//			WideString* wStr=new WideString("Quantity");
	//			tableCommands->SetCellText( *wStr, GridAddress(0,i));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -812;		
	//			stencileInfo.TagName = "Quantity";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, 0,i, 0 );

	//			delete wStr;
	//		}
	//		i++;
	//		
	//		//Required
	//		{
	//			WideString* wStr=new WideString("Required");
	//			tableCommands->SetCellText( *wStr, GridAddress(0,i));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -813;		
	//			stencileInfo.TagName = "Required";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, 0 ,i ,0 );

	//			delete wStr;
	//		}

	//		i++;

	//		//Comments
	//		{
	//			WideString* wStr=new WideString("Comments");
	//			tableCommands->SetCellText( *wStr, GridAddress(0,i));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = -814;		
	//			stencileInfo.TagName = "Comments";
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;		

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo,0, i, 0 );

	//			delete wStr;
	//		}

	//	}
	//	i++;
	//	// at first place we have -812 for Quantity 
	//	// at second place -813 for Required
	//	// and at third place -814 for Comments
	//	//so take the Attributeids from 3nd place onwards
	//	for(;i<vec_tableheaders.size();i++)
	//	{
	//		//oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//		//PMString dispname = oAttribVal.getDisplay_name();

	//		PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//		
	//		WideString* wStr=new WideString(dispname);
	//		//CA(dispname);
	//		int32 StringLength = dispname.NumUTF16TextChars();
	//		HeadersizeVector.push_back(StringLength);

	//		if(isTranspose)
	//		{
	//			tableCommands->SetCellText(*wStr, GridAddress(i , 0));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId = vec_tableheaders[i];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//			//CA("Inside PutHeaderDataIN Table");	CA(dispname);
	//			stencileInfo.elementName = dispname;

	//			PMString TableColName("");//oAttribVal.getTable_col();
	//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[i]);
	//			//}
	//			//	CA(TableColName);
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID = tStruct.languageID;
	//			stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;				

	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

	//		}
	//		else
	//		{
	//			/*PMString ASD("vec_tableheaders[i] : ");
	//			ASD.AppendNumber(vec_tableheaders[i]);
	//			CA(ASD);*/

	//			tableCommands->SetCellText(*wStr, GridAddress(0, i));
	//			SlugStruct stencileInfo;
	//			stencileInfo.elementId =vec_tableheaders[i];
	//			/*CAttributeValue oAttribVal;
	//			oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
	//			PMString dispname = oAttribVal.getDisplay_name();*/
	//			PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
	//			stencileInfo.elementName = dispname;
	//			//CA("Inside PutHeaderDataIN Table");	CA(dispname);
	//			PMString TableColName("");// oAttribVal.getTable_col();
	//			//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//{
	//				TableColName.Append("ATTRID_");
	//				TableColName.AppendNumber(vec_tableheaders[i]);
	//			//}
	//			stencileInfo.TagName = TableColName;
	//			stencileInfo.LanguageID =tStruct.languageID;
	//			//CA(TableColName);
	//			stencileInfo.typeId  = -1;//-2; //vec_items[i];
	//			stencileInfo.header = 1;
	//			stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
	//			stencileInfo.parentTypeId = pNode.getTypeId();
	//			//4Aug..ItemTable
	//			//stencileInfo.whichTab = 4; // Item Attributes
	//			stencileInfo.whichTab = tStruct.whichTab;
	//			stencileInfo.imgFlag = CurrentSectionID; 
	//			stencileInfo.isAutoResize = tStruct.isAutoResize;
	//			stencileInfo.col_no = tStruct.colno;
	//			stencileInfo.tableTypeId = tStruct.typeId;
	//			XMLReference txmlref;

	//			//CA(tStruct.tagPtr->GetTagString());
	//			//CA(stencileInfo.colName);
	//			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//			if(TableTagNameNew.Contains("PSTable"))
	//			{
	//				//CA("PSTable Found");
	//			}
	//			else
	//			{				
	//				TableTagNameNew =  "PSTable";						
	//			}
	//		//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
	//			this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
	//		//	CA("After Tag Table");
	//		}
	//		delete wStr;
	//	}
	//}while(kFalse);
}

void TableUtility::fillDataInHybridTable(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{
		IsDBTable = kTrue;		
		int32 IsAutoResize = 0;
		if(tStruct.isAutoResize == 1)
			IsAutoResize= 1;
		
		if(tStruct.isAutoResize == 2)
			IsAutoResize= 2;
				
        AddHeaderToTable = kTrue;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
			return ;
		}
				
		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		if(imagePath!="")
		{
			const char *imageP=imagePath.GetPlatformString().c_str();
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
		}
		
		if(imagePath=="")
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::imagePath is blank");
			return ;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			break;
		}
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands == NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		
		
		double sectionid = -1;    
		if(global_project_level == 3)
		{
			//CA("global_project_level == 3");
			sectionid = CurrentSubSectionID;
		}
		if(global_project_level == 2)
		{
			//CA("sectionid:global_project_level == 2");
			sectionid = CurrentSectionID;
		}
		else
		{
			//CA("else part of sectionid:global_project_level == 2");
			sectionid = CurrentSectionID;
		}
		XMLReference tStructXMLRef = tStruct.tagPtr->GetXMLReference();
		///attaching parentID,SectionId.ParentTypeId to first tag
		PMString attribVal("");
		attribVal.AppendNumber(PMReal(objectId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentID"),WideString(attribVal));
		
		attribVal.Clear();
		attribVal.AppendNumber(PMReal(sectionid));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("sectionID"),WideString(attribVal));
		
		attribVal.Clear();
		attribVal.AppendNumber(PMReal(pNode.getTypeId()));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentTypeID"),WideString(attribVal));
		
		attribVal.Clear();
		attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("pbObjectId"),WideString(attribVal));	
		
		//for getting table informations in ptr tableInfo
		
		VectorAdvanceTableScreenValuePtr tableInfo = NULL;
		bool16 callFromTS = kFalse;
		TableSourceInfoValue *TableSourceInfoValueObjPtr = NULL;
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
	    if(ptrTableSourceHelper != nil)
	    {
			TableSourceInfoValueObjPtr = ptrTableSourceHelper->getTableSourceInfoValueObj();
			if(TableSourceInfoValueObjPtr)
				callFromTS = TableSourceInfoValueObjPtr->getIsCallFromTABLEsource();
	    }

		if(callFromTS == kTrue)
		{
			if(TableSourceInfoValueObjPtr)
				if(TableSourceInfoValueObjPtr->getIsTable())
					tableInfo = TableSourceInfoValueObjPtr->getVectorAdvanceTableScreenValuePtr();
		}
		else
		{
			if(tStruct.whichTab == 5)
			{
//				tableInfo =ptrIAppFramework->getHybridTableData(sectionid,objectId,tStruct.languageID,kTrue);
			}
			else if(tStruct.whichTab == 3)
			{
				int32 isProduct = 1;		
				tableInfo =ptrIAppFramework->getHybridTableData(sectionid,objectId,tStruct.languageID,isProduct, kFalse);
			}			
			else if(tStruct.whichTab == 4)
			{
				int32 isProduct = 0;		
				tableInfo =ptrIAppFramework->getHybridTableData(sectionid,objectId,tStruct.languageID,isProduct, kFalse);
			}	
		}
		if(!tableInfo )
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::TableUtility::fillDataInHybridTable::!tableInfo");
			return;
		}

		if( tableInfo->size() ==0 )
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::TableUtility::fillDataInHybridTable::tableInfo->size == 0");
			break;
		}
			
		//It checks the selection present or not........
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if( !iSelectionManager )
		{	
			//CA("Slection NULL");
			break;
		}
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			//CA("!layoutSelectionSuite");
			return;
		}
		//It gets refernce to persistant objects present in the (database) document as long as document is open.
		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
		
		//selectionManager->DeselectAll(NULL); // deselect every active CSB
		layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
		
		//These values are taken from db
		PMRect theArea;
		PMReal rel;
		int32 ht = 0;
		int32 wt = 0;
		
		//IGeometry is a required interface of page items,get the page and page geometry
		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
		if(iGeometry==NULL)
			break;
		//the matrix required to transform the object (a page item supporting ITransform) from inner coordinates to PasteBoard coordinates
		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
		
		rel = theArea.Height();
		ht=::ToInt32(rel);
		rel=theArea.Width();	
		wt=::ToInt32(rel);
	
		CAdvanceTableScreenValue oAdvanceTableScreenValue;
		
		VectorAdvanceTableScreenValue::iterator it;
		bool16 typeidFound = kFalse;
		
		if(callFromTS == kTrue)
		{
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oAdvanceTableScreenValue = *it;
	
				if(-115  ==  tStruct.typeId  && oAdvanceTableScreenValue.getTableId() == TableSourceInfoValueObjPtr->getVec_Table_ID().at(0))
				{
					typeidFound=kTrue;
					break;
				}
			}
		}
		else
		{
			//CA("Content Sprayer");
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oAdvanceTableScreenValue = *it;

				if(-115 == tStruct.typeId && (tableId == tStruct.tableId) && (((tableId != -1) && (oAdvanceTableScreenValue.getTableId() == tableId)) || (tableId == -1) ))
				{
					typeidFound=kTrue;
					break;
				}
			}
		}

		if(typeidFound != kTrue)
		{
			//CA("typeidFound != kTrue");
			break;
		}	
		tStruct.tableId = oAdvanceTableScreenValue.getTableId();
		tStruct.pbObjectId = pNode.getPBObjectID();

		attribVal.Clear();
		attribVal.AppendNumber(PMReal(oAdvanceTableScreenValue.getTableId()));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("tableId"),WideString(attribVal));	

		int32 bodyRows=0;
		int32 headerRows=0;
		int32 numRows=0;
		int32 numCols=0;		

		bodyRows=oAdvanceTableScreenValue.getBodyRowCount();
		if(bodyRows<=0)
			break;
	
		headerRows=oAdvanceTableScreenValue.getHeaderRowCount();
		numCols=oAdvanceTableScreenValue.getColumnCount();

		double tableTypeID = -115;
		numRows=bodyRows ;

		if(tStruct.whichTab == 4)
		{
			TableFlagForStandardProductTable = 2; // Since We are spraying Item Table.....this variable is used in TagTable to attach tag to complete tableTag.
		}
		else if(tStruct.whichTab == 5)
		{
			TableFlagForStandardProductTable = 3; // Since We are spraying section level Hybrid Table.
		}
		else 
			TableFlagForStandardProductTable = 1; // Since We are spraying Product Table

//		if(isTranspose )
//		{
//		
//			if(AddHeaderToTable)			
//				resizeTableForHybridTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht ,headerRows);
//			else
//				resizeTableForHybridTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht ,headerRows);
//			
//			int32 i=0,j=0;
//			
//
//			vector<vector<PMString> >  vec_tableheaders=oAdvanceTableScreenValue.getTableHeader();
//
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueHeader = oAdvanceTableScreenValue.getTableHeaderValues();
//			vector<CAdvanceTableCellValue > vec_CAdvanceTableCellValue;
//			
//		/*	PMString vec_vecCAdvanceTableCellValueSize("vec_vecCAdvanceTableCellValueHeaderSize = ");
//			vec_vecCAdvanceTableCellValueSize.AppendNumber(vec_vecCAdvanceTableCellValueHeader.size());
//			CA(vec_vecCAdvanceTableCellValueSize);
//		*/	
//			vector<vector<CAdvanceTableCellValue> >::iterator it6;
//			vector<CAdvanceTableCellValue>::iterator it5;
////////			
//	/*		for(it6 = vec_vecCAdvanceTableCellValueHeader.begin(); it6 != vec_vecCAdvanceTableCellValueHeader.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueHeader");
//				vec_CAdvanceTableCellValue = *it6;
//				for(it5 = vec_CAdvanceTableCellValue.begin(); it5 != vec_CAdvanceTableCellValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvanceTableCellValue");
//				}
//			}*/
//			
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueBody = oAdvanceTableScreenValue.getTableBodyDataValues();
//			vector<CAdvanceTableCellValue> vec_CAdvannceTableCellBobyValue;
//			
//			/*PMString vec_vecCAdvanceTableCellValueBodySize("vec_CAdvannceTableCellBobyValueSize = ");
//			vec_vecCAdvanceTableCellValueBodySize.AppendNumber(vec_vecCAdvanceTableCellValueBody.size());
//			//CA(vec_vecCAdvanceTableCellValueBodySize);*/
//
//			/*for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueBody");
//				vec_CAdvannceTableCellBobyValue = *it6;
//				for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvannceTableCellValue");
//				}
//			}*/
//
//			if(AddHeaderToTable)
//			{
//				tStruct.typeId = tableTypeID;
//				putHeaderDataInHybridTable(tableUIDRef,vec_tableheaders,isTranspose,tStruct,boxUIDRef,vec_vecCAdvanceTableCellValueHeader);
//				i=headerRows;
//				j=0;
//			}
//	
//			vector<TableRow> vec_tableBodyData=oAdvanceTableScreenValue.getTableBodyData();
//			vector<TableRow>::iterator it3;
//
//			for(it3 = vec_tableBodyData.begin(); it3 != vec_tableBodyData.end(); it3++)
//			{				
//				vector<CellData> vec_CellData=(*it3).tableRowContent;
//				PMString textToInsert("");
//				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//				if(!iConverter)
//				{
//					//CA("!iConverter");
//					break;	
//				}
//
//				vector<CellData>::iterator it4;
//				for(it4 = vec_CellData.begin(); it4 != vec_CellData.end(); it4++)
//				{	
//					CellData tempCellData=(*it4);
//					if(j>=numCols)
//					{
//						break;
//					}
//
//					TagList tagList;
//					textToInsert.Clear();
//					tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);
//					setTableRowColData(tableUIDRef, textToInsert, j, i);
//					int32 tagListSize =static_cast<int32>(tagList.size());	
//					if( tagList.size()==0 )
//					{
//						SlugStruct stencileInfo;
//						stencileInfo.whichTab   = 4;		
//						stencileInfo.header = -1;
//						stencileInfo.parentId	= objectId;					
//						stencileInfo.tagStartPos= -1;						
//						stencileInfo.tagEndPos  = -1;
//						stencileInfo.elementId  = -1;						
//						PMString CellTagName("");
//						CellTagName.Append("ATTRID_");
//						CellTagName.AppendNumber(-1);				
//						PMString TableColName("");
//						TableColName.Append("Static_Text");//Advance Table Cell Data
//						TableColName = keepOnlyAlphaNumeric(TableColName);
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tStruct.languageID;
//						//stencileInfo.parentId = pNode.getPubId();
//						stencileInfo.parentTypeId = pNode.getTypeId();
//						stencileInfo.imgFlag = CurrentSubSectionID; 
//						stencileInfo.sectionID = CurrentSubSectionID; 
//						stencileInfo.isAutoResize = tStruct.isAutoResize;
//						stencileInfo.col_no = tStruct.colno;
//						//stencileInfo.tableTypeId = tStruct.typeId;
//						stencileInfo.tableTypeId = tableTypeID;
//						stencileInfo.tableType = tStruct.tableType;
//						stencileInfo.tableId = oAdvanceTableScreenValue.getTableId();
//						stencileInfo.pbObjectId = pNode.getPBObjectID();
//						XMLReference txmlref;
//						//CA(tStruct.tagPtr->GetTagString());		
//						PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//						//CA("TableTagNameNew : " + TableTagNameNew);
//						if( TableTagNameNew.Contains("PSHybridTable") )
//						{
//							//CA("PSTable Found");
//						}
//						else
//						{				
//							TableTagNameNew =  "PSHybridTable";
//						}
//						//CA("inside static_text before this->TagTable");
//						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i, tableId );
//						//CA("after  this->TagTable");
//						j++;
//						continue;
//					}
//					for(int32 k = 0; k < tagListSize; k++)
//					{
//						//CA("inside tagListSize loop");	
//						TagStruct tInfo = tagList[k];
//						SlugStruct stencileInfo;
//						stencileInfo.whichTab = tInfo.whichTab;						
//						stencileInfo.typeId = tInfo.typeId;
//						stencileInfo.header = -1;
//						stencileInfo.parentId = objectId;				
//						stencileInfo.tagStartPos= tInfo.startIndex;						
//						stencileInfo.tagEndPos = tInfo.endIndex;					
//						stencileInfo.elementId = tInfo.elementId;					
//						PMString CellTagName("");
//						CellTagName.Append("ATTRID_");
//						CellTagName.AppendNumber(stencileInfo.elementId);				
//						PMString TableColName("");
//						TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//						TableColName = keepOnlyAlphaNumeric(TableColName);
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tStruct.languageID;
//						//stencileInfo.parentId = pNode.getPubId();					
//						stencileInfo.parentTypeId = pNode.getTypeId();					
//						stencileInfo.imgFlag = CurrentSubSectionID; 
//						stencileInfo.sectionID = CurrentSubSectionID;					
//						stencileInfo.isAutoResize = tStruct.isAutoResize;
//						stencileInfo.col_no = tStruct.colno;					
//						//stencileInfo.tableTypeId = tStruct.typeId;
//						stencileInfo.tableTypeId = tableTypeID;
//						stencileInfo.childId = tInfo.childId;
//						stencileInfo.childTag = tInfo.childTag;
//						stencileInfo.tableType = tStruct.tableType;
//						stencileInfo.tableId = oAdvanceTableScreenValue.getTableId();
//						stencileInfo.pbObjectId = pNode.getPBObjectID();
//						XMLReference txmlref;
//						//CA("tStruct.tagPtr->GetTagString()");					
//						PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
//						if( TableTagNameNew.Contains("PSHybridTable") )
//						{
//							//CA("PSTable Found");
//						}
//						else
//						{				
//							TableTagNameNew =  "PSHybridTable";
//						}
//						//CA("before this->TagTable");
//						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j,i,tableId );
//						//CA("after this->TagTable");
//					}
//					j++;
//				}					
//				i++;
//				j=0;
//			}	
//		}
//		else
		{
//			if(AddHeaderToTable)
//			{	
//				//CA("AddHeaderToTable");
//				resizeTableForHybridTable(tableUIDRef, numRows , numCols, isTranspose, wt, ht,headerRows);
//			}
//			else
//			{		
				//CA("AddHeaderToTable == kFalse");
				resizeTableForHybridTable(tableUIDRef, numRows, numCols, kFalse, wt, ht,headerRows);				
			
//			}			
			RowRange rowrange(tableModel->GetTotalRows()); 

			GridAddress  gridAddress1(0,0);
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
			if(!pTextSel)
			{
				//CA("pTextSel is null");
				break;
			}
			InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
			if(!tblTxtSel)
			{
				//CA("tblTxtSel is null");
				break;
			}
			tblTxtSel->SelectTextInCell( tableModel,gridAddress1);

			InterfacePtr<ITableSuite> tableSuite(static_cast<ITableSuite* >( Utils<ISelectionUtils>()->QuerySuite(ITableSuite::kDefaultIID,nil))); 
			if(!tableSuite) 
			{
				//CA("!!!tableSuite");
				break;
			}
			
			int32 cellHeight = ToInt32(tableSuite->GetCellWidth());
			int32 i=0,j=0;

			vector<CAdvanceTableCellValue > vec_CAdvanceTableCellValue = oAdvanceTableScreenValue.getCells();
			vector<CAdvanceTableCellValue>::iterator it1;
			for(it1 = vec_CAdvanceTableCellValue.begin(); it1 != vec_CAdvanceTableCellValue.end(); it1++)
			{
				CAdvanceTableCellValue cAdvCellObj = *it1;

				//first check need for merging cell.
				bool16 doMerge = kFalse;
				RowRange rowRange(cAdvCellObj.getRowId()-1, cAdvCellObj.getRowLength());
				ColRange colRange(cAdvCellObj.getColId()-1 , cAdvCellObj.getColLength()); 

				if((cAdvCellObj.getRowLength() > 1) || (cAdvCellObj.getColLength() > 1))
				{
					doMerge = kTrue;
				}
				
				if(doMerge)
				{
					GridArea gridArea(rowRange,colRange);	
					ErrorCode errorCode = tableCommands->MergeCells(gridArea);
				}	
				
				// Check if Text value present in Cell or not
				if(cAdvCellObj.getValue().NumUTF16TextChars() > 0)
				{
					

					if((cAdvCellObj.getFieldId() > -1) && (cAdvCellObj.getItemId() > -1) )
					{
						// PRINTsource Text Tag
						setTableRowColData(tableUIDRef,cAdvCellObj.getValue() , cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1);
                        
                        ptrIAppFramework->LogDebug("setTableRowColData done ");
                        
						SlugStruct stencileInfo;
						stencileInfo.whichTab   = 4;
						stencileInfo.typeId = -1;
						stencileInfo.parentId = objectId;
						stencileInfo.childId = cAdvCellObj.getItemId() ;
						stencileInfo.childTag =1;
						stencileInfo.tagStartPos= tStruct.startIndex;						
						stencileInfo.tagEndPos  = tStruct.endIndex;
						stencileInfo.elementId  = cAdvCellObj.getFieldId();		
						stencileInfo.isEventField = -1;
						PMString CellTagName("");
						CellTagName.Append("ATTRID_");
						CellTagName.AppendNumber(PMReal(stencileInfo.elementId));
						PMString TableColName("");
						TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
						TableColName = keepOnlyAlphaNumeric(TableColName);
						stencileInfo.TagName = TableColName;
						stencileInfo.LanguageID =tStruct.languageID;
						stencileInfo.parentTypeId = pNode.getTypeId();
						stencileInfo.imgFlag = -1 ; 
						stencileInfo.sectionID = CurrentSubSectionID; 
						stencileInfo.isAutoResize = tStruct.isAutoResize;
						stencileInfo.col_no = cAdvCellObj.getColId() -1;
						stencileInfo.tableTypeId = -1;
						stencileInfo.tableType = -1;
						stencileInfo.tableId = oAdvanceTableScreenValue.getTableId();
						stencileInfo.pbObjectId = pNode.getPBObjectID();
                        stencileInfo.field3 = cAdvCellObj.getCellId();
						XMLReference txmlref;
						PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
						if( TableTagNameNew.Contains("PSHybridTable") )
						{
							//CA("PSTable Found");
						}
						else
						{				
						TableTagNameNew =  "PSHybridTable";
						}
								
						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1,tableId );


					}
					else if((cAdvCellObj.getAssetTypeId() > -1) &&  (cAdvCellObj.getItemId() > -1))
					{
						// Add InlineImage
						int32 start=0;
						InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
						if (tableModel == NULL)
						{
							//CA("Err: invalid interface pointer ITableFrame");
							break;
						}
							
						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
						if(tableCommands==NULL)
						{
							//CA("Err: invalid interface pointer ITableCommands");
							break;
						}
						GridAddress  gridAddress( (cAdvCellObj.getRowId()-1) ,  (cAdvCellObj.getColId()-1) );
						tblTxtSel->SelectTextInCell( tableModel,gridAddress);

						InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
						if(!pTextTarget)
						{ 
							//CA("pTextTarget is null"); 
							continue;
						}
								
						RangeData rangData=pTextTarget->GetRange();
						start=rangData.End();

						InterfacePtr<ITextModel>txtModel(pTextTarget->QueryTextModel());
						if(!txtModel)
						{
							//CA("!!txtModel");
							continue;
						}
						
						RowRange rowRange((cAdvCellObj.getRowId()-1) ,(cAdvCellObj.getRowLength())) ;
						ErrorCode result = tableCommands->ResizeRows(rowRange,cellHeight+8);
                        //CA(" unable to resize row");
						ColRange colRange(cAdvCellObj.getColId()-1 , cAdvCellObj.getColLength());
							
						result = tableCommands->ResizeCols(colRange,/*108*/cellHeight+8);

						UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
								
                        //CA("calling InsertInline");
						this->InsertInline(textStoryUIDRef,start,cellHeight);
								
						SlugStruct stencileInfo;
						stencileInfo.whichTab   = 4;
						stencileInfo.typeId = cAdvCellObj.getAssetTypeId();
						stencileInfo.parentId = objectId;
						stencileInfo.childId = cAdvCellObj.getItemId() ;
						stencileInfo.childTag =1;
						stencileInfo.tagStartPos= tStruct.startIndex;						
						stencileInfo.tagEndPos  = tStruct.endIndex;
						stencileInfo.elementId  = -1;		
						stencileInfo.isEventField = -1;
						PMString CellTagName("");
						CellTagName.Append("ATTRID_");
						CellTagName.AppendNumber(PMReal(stencileInfo.elementId));
						PMString TableColName("");
						TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
						TableColName = keepOnlyAlphaNumeric(TableColName);
						stencileInfo.TagName = TableColName;
						stencileInfo.LanguageID =tStruct.languageID;
						stencileInfo.parentTypeId = pNode.getTypeId();
						stencileInfo.imgFlag = 1 ; 
						stencileInfo.sectionID = CurrentSubSectionID; 
						stencileInfo.isAutoResize = tStruct.isAutoResize;
						stencileInfo.col_no = cAdvCellObj.getColId() -1;
						stencileInfo.tableTypeId = -1;
						stencileInfo.tableType = oAdvanceTableScreenValue.getTableId();
						stencileInfo.tableId = -1;
						stencileInfo.pbObjectId = pNode.getPBObjectID();
                        stencileInfo.field3 = cAdvCellObj.getCellId();
						XMLReference txmlref;
						PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
						if( TableTagNameNew.Contains("PSHybridTable") )
						{
							//CA("PSTable Found");
						}
						else
						{				
						TableTagNameNew =  "PSHybridTable";
						}							
								
						XMLReference xmlref;
								
						IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
						if(!iDataSprayer)
						{	
							//CA("!iDtaSprayer");
							break;
						}

						//if(tInfo.whichTab == 4)
						//{
						//	//CA("before Spraying item image");
						//	iDataSprayer->sprayItemImageInsideHybridTable(newFrameUIDRef, tagInfo, tagInfo.parentId,imageno);
						//	//CA("After Spraying item image");										
						//}
						//if(tInfo.whichTab == 3)
						//{
						//	//CA("Spraying product image");
						//	iDataSprayer->fillImageInBox(newFrameUIDRef , tagInfo, tagInfo.parentId );
						//	//CA("After product image");
						//}
								
						//iDataSprayer->fitInlineImageInBoxInsideTableCell(newFrameUIDRef);

						PMString fileName = cAdvCellObj.getValue();
						double typeId = cAdvCellObj.getAssetTypeId();

						if(fileName=="")
							continue;

						do
						{
							SDKUtilities::Replace(fileName,"%20"," ");
						}while(fileName.IndexOfString("%20") != -1);

						PMString imagePathWithSubdir = imagePath;

						#ifdef WINDOWS
							SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
						#endif

						if((iDataSprayer->fileExists(imagePathWithSubdir,fileName)))
						{
							PMString total=imagePathWithSubdir+fileName;
						
							 #ifdef MACINTOSH
								SDKUtilities::convertToMacPath(total);
							#endif

							if(iDataSprayer->ImportFileInFrame(newFrameUIDRef, total))
							{		
								iDataSprayer->fitImageInBox(newFrameUIDRef);
							}
						}
						
						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, (cAdvCellObj.getRowId()-1), (cAdvCellObj.getColId()-1) ,tableId );
						
					}
					else
					{
						//Standalone Text
						setTableRowColData(tableUIDRef,cAdvCellObj.getValue() , cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1);
                        
                        SlugStruct stencileInfo;
						stencileInfo.whichTab   = 4;
						stencileInfo.typeId = -1;
						stencileInfo.parentId = pNode.getPubId(); ;
						stencileInfo.childId = cAdvCellObj.getItemId() ;
						stencileInfo.childTag =-1;
						stencileInfo.tagStartPos= tStruct.startIndex;
						stencileInfo.tagEndPos  = tStruct.endIndex;
						stencileInfo.elementId  = -900;  // for static text in Advance table cell
						stencileInfo.isEventField = -1;
						PMString CellTagName("");
						CellTagName.Append("ATTRID_");
						CellTagName.AppendNumber(PMReal(stencileInfo.elementId));
						PMString TableColName("");
						TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
						TableColName = keepOnlyAlphaNumeric(TableColName);
						stencileInfo.TagName = TableColName;
						stencileInfo.LanguageID =tStruct.languageID;
						stencileInfo.parentTypeId = pNode.getTypeId();
						stencileInfo.imgFlag = -1 ;
						stencileInfo.sectionID = CurrentSubSectionID;
						stencileInfo.isAutoResize = tStruct.isAutoResize;
						stencileInfo.col_no = cAdvCellObj.getColId() -1;
						stencileInfo.tableTypeId = -1;
						stencileInfo.tableType = -1;
						stencileInfo.tableId = oAdvanceTableScreenValue.getTableId();
						stencileInfo.pbObjectId = pNode.getPBObjectID();
                        stencileInfo.field3 = cAdvCellObj.getCellId();
						XMLReference txmlref;
						PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
						if( TableTagNameNew.Contains("PSHybridTable") )
						{
							//CA("PSTable Found");
						}
						else
						{
                            TableTagNameNew =  "PSHybridTable";
						}
                        
						this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1,tableId );
                        
					}
				}
				else
				{
					//Standalone Text
					setTableRowColData(tableUIDRef,"" , cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1);
                    
                    SlugStruct stencileInfo;
                    stencileInfo.whichTab   = 4;
                    stencileInfo.typeId = -1;
                    stencileInfo.parentId = pNode.getPubId(); ;
                    stencileInfo.childId = cAdvCellObj.getItemId() ;
                    stencileInfo.childTag =-1;
                    stencileInfo.tagStartPos= tStruct.startIndex;
                    stencileInfo.tagEndPos  = tStruct.endIndex;
                    stencileInfo.elementId  = -900;  // for static text in Advance table cell
                    stencileInfo.isEventField = -1;
                    PMString CellTagName("");
                    CellTagName.Append("ATTRID_");
                    CellTagName.AppendNumber(PMReal(stencileInfo.elementId));
                    PMString TableColName("");
                    TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
                    TableColName = keepOnlyAlphaNumeric(TableColName);
                    stencileInfo.TagName = TableColName;
                    stencileInfo.LanguageID =tStruct.languageID;
                    stencileInfo.parentTypeId = pNode.getTypeId();
                    stencileInfo.imgFlag = -1 ;
                    stencileInfo.sectionID = CurrentSubSectionID;
                    stencileInfo.isAutoResize = tStruct.isAutoResize;
                    stencileInfo.col_no = cAdvCellObj.getColId() -1;
                    stencileInfo.tableTypeId = -1;
                    stencileInfo.tableType = -1;
                    stencileInfo.tableId = oAdvanceTableScreenValue.getTableId();
                    stencileInfo.pbObjectId = pNode.getPBObjectID();
                    stencileInfo.field3 = cAdvCellObj.getCellId();
                    XMLReference txmlref;
                    PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
                    if( TableTagNameNew.Contains("PSHybridTable") )
                    {
                        //CA("PSTable Found");
                    }
                    else
                    {
                        TableTagNameNew =  "PSHybridTable";
                    }
                    
                    this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, cAdvCellObj.getRowId()-1 ,  cAdvCellObj.getColId()-1,tableId );
				}
			}
}

//void TableUtility::putHeaderDataInHybridTable(const UIDRef& tableUIDRef, vector<vector<PMString> > vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, vector<vector<CAdvanceTableCellValue> > &vec_vecCAdvanceTableCellValueHeader )
//{
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(!iConverter)
//		{	//CA("13");
//			return;
//		}
//
//		int32 i = 0;
//		int32 j = 0;
//
//		int32 p =0;
//		int32 q =0;
//		vector<vector<PMString> >::iterator it1;
//		for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
//		{	
//			//CA("inside vec_tableheaders for");
//			vector < PMString > vec_tableHeaderRow;			
//			vec_tableHeaderRow = (*it1);
//			vector < PMString >::iterator it2;
//		
//			for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
//			{	
//				//CA("inside vec_tableHeaderRow for");
//				PMString dispname = (*it2);
//				//CA(dispname);
//				InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//				if (tableModel == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableFrame");
//					break;
//				}
//
//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//				if(tableCommands == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableCommands");
//					break;
//				}
//				WideString* wStr=new WideString(dispname);
//				if(isTranspose )
//				{
//					iConverter->ChangeQutationMarkONOFFState(kFalse);
//					tableCommands->SetCellText(*wStr, GridAddress(i,j));
//					iConverter->ChangeQutationMarkONOFFState(kTrue);
//					SlugStruct stencileInfo;
//					
//					PMString TableColName("");
//					TableColName.Append("ATTRID_ATCD");
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID = tStruct.languageID;
//					stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
//					stencileInfo.header = 1;
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.sectionID = CurrentSubSectionID;
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					stencileInfo.tableType = tStruct.tableType;
//					stencileInfo.tableId = tStruct.tableId;
//					stencileInfo.pbObjectId = tStruct.pbObjectId;
//					XMLReference txmlref;
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					if( TableTagNameNew.Contains("PSHybridTable") )
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSHybridTable";
//					}
//					this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j, 0 );
//					i++;
//				}
//				else
//				{	
//					/*PMString test("test::");
//					test.Append(*wStr);
//					test.Append("\n");
//					test.AppendNumber(j);
//					test.Append("\n");
//					test.AppendNumber(i);*/
//					//CA(test);
//					//CA("test 0");
//				//	PMString ASD("P = ");
//				//	ASD.AppendNumber(p);		
//				//	ASD.Append("  q = ");
//				//	ASD.AppendNumber(q);
//				//	CA(ASD);
//
//					CAdvanceTableCellValue objCAdvanceTableCellValue = vec_vecCAdvanceTableCellValueHeader[p][q];
//				
//					//CA("test1");
//			//		bool16 isMerge = kFalse;
//			//		RowRange rowRange(objCAdvanceTableCellValue.rowStartId , 1);
//			//		ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//			//		if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//			//		{
//			//			isMerge = kTrue;
//	//Commented By Sachin	rowRange.Set(objCAdvanceTableCellValue.rowStartId, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//			//
//			//		}
//			//		if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//			//		{
//            //          isMerge = kTrue;
//// Commented By Sachin	colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId + 1);
//			//
//			//		}
//			//
//			//		if(isMerge)
//			//		{
//			//			GridArea gridArea(rowRange, colRange);
//			//			//ErrorCode errorCode = tableCommands->MergeCells(gridArea); //commented By Sachin
//			//		}
//					//CA("Before SetCellText");
//					iConverter->ChangeQutationMarkONOFFState(kFalse);
//				
//					ErrorCode errorCode = tableCommands->SetCellText(*wStr, GridAddress(objCAdvanceTableCellValue.rowId, objCAdvanceTableCellValue.colId));
//					iConverter->ChangeQutationMarkONOFFState(kTrue);
//					//CA("after SetCellText");
//					SlugStruct stencileInfo;			
//					stencileInfo.elementName = dispname;
//					PMString TableColName("");
//					TableColName.Append("ATTRID_ATCD");
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					stencileInfo.typeId  = -1;//-2; //vec_items[i];
//					stencileInfo.header = 1;
//					stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					stencileInfo.whichTab = tStruct.whichTab; 
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					stencileInfo.sectionID = CurrentSubSectionID;
//					stencileInfo.tableType = tStruct.tableType;
//					stencileInfo.tableId = tStruct.tableId;
//					stencileInfo.pbObjectId = tStruct.pbObjectId;
//					XMLReference txmlref;
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					//CA("TableTagNameNew : " + TableTagNameNew);
//					if( TableTagNameNew.Contains("PSHybridTable") )
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSHybridTable";
//						//CA("PSTable Not Found : "+ TableTagNameNew);
//					}
//					this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, objCAdvanceTableCellValue.rowId, objCAdvanceTableCellValue.colId, 0 );//### Added
//					i++;
//					
//				//	if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//				//	{
//				//		for(int z= 0; z < (objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId); z++ )
//				//		{
//				//	
//				//		}
//				//	}					
//					q++;										
//				}
//				delete wStr;
//			}
//			i=0;
//			j++;
//			q =0;
//			p++;
//		}
//		//***** Added From Here
//		
//		i = 0;
//		j = 0;
//		p = 0;
//		q = 0;
//		//vector<vector<PMString> >::iterator it1;
//		for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
//		{	
//			//CA("inside vec_tableheaders for");
//			/*vector < PMString > r;		*/	
//			vector < PMString > vec_tableHeaderRow;  //###
//
//			vec_tableHeaderRow = (*it1);
//			vector < PMString >::iterator it2;
//		
//			for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
//			{	
//				//CA("inside vec_tableHeaderRow for");
//				PMString dispname = (*it2);
//				//CA(dispname);
//				InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//				if (tableModel == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableFrame");
//					break;
//				}
//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//				if(tableCommands == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableCommands");
//					break;
//				}
//				//WideString* wStr=new WideString(dispname);
//				if(isTranspose )
//				{
//					//iConverter->ChangeQutationMarkONOFFState(kFalse);
//					//tableCommands->SetCellText(*wStr, GridAddress(i,j));
//					//iConverter->ChangeQutationMarkONOFFState(kTrue);
//					//SlugStruct stencileInfo;
//					//
//					//PMString TableColName("");
//					//TableColName.Append("ATTRID_ATCD");
//					//stencileInfo.TagName = TableColName;
//					//stencileInfo.LanguageID = tStruct.languageID;
//					//stencileInfo.typeId  = -2;  // TypeId is hardcodded to -2 for Header Elements.
//					//stencileInfo.whichTab = tStruct.whichTab;
//					//stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
//					//stencileInfo.parentTypeId = pNode.getTypeId();
//					//stencileInfo.imgFlag = CurrentSectionID; 
//					//stencileInfo.sectionID = CurrentSubSectionID;
//					//stencileInfo.isAutoResize = tStruct.isAutoResize;
//					//stencileInfo.col_no = tStruct.colno;
//					//stencileInfo.tableTypeId = tStruct.typeId;
//					//XMLReference txmlref;
//					//PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					//if( TableTagNameNew.Contains("PSHybridTable") )
//					//{
//					//	//CA("PSTable Found");
//					//}
//					//else
//					//{				
//					//	TableTagNameNew =  "PSHybridTable";
//					//}
//					//this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j, 0 );
//					//i++;
//				}
//				else
//				{						
//					CAdvanceTableCellValue objCAdvanceTableCellValue = vec_vecCAdvanceTableCellValueHeader[p][q];
//									
//					bool16 isMerge = kFalse;
//					RowRange rowRange(objCAdvanceTableCellValue.rowStartId , 1);
//					ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//					if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//					{
//						isMerge = kTrue;
//						rowRange.Set(objCAdvanceTableCellValue.rowStartId, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//
//					}
//					if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					{
//                        isMerge = kTrue;
//						colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId + 1);
//
//					}
//					if(isMerge)
//					{						
//						GridArea gridArea(rowRange, colRange);
//						ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//					}										
//					i++;					
//				//	if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//				//	{
//				//		for(int z= 0; z < (objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId); z++ )
//				//		{
//				//			//CA("q incremented for Merge");
//				//			//	q++;
//				//			//	it2++;
//				//		}
//				//	}					
//					q++;
//					{
//					/*PMString ASD("after increment q = ");					
//					ASD.AppendNumber(q);
//					CA(ASD);*/
//					}
//				}
//			}
//			i=0;
//			j++;
//			q =0;
//			p++;
//		}
//
//		//****
//
//
	}while(kFalse);
}

void TableUtility::resizeTableForHybridTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose, const int32& Width, const int32& Height, const int32& HeaderRows )
{
	do
	{
		ErrorCode result;

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 ColWidth=0;
		int32 RowHt=0;
		
        PMReal ColumnWidth(ColWidth);
		PMReal RowHeight(RowHt);

		int32 presentTotalRows =tableModel->GetTotalRows().count;
		int32 presentBodyRows =tableModel->GetBodyRows().count;
		int32 presentHeaderRows =tableModel->GetHeaderRows().count;
		
		int32 presentCols = tableModel->GetTotalCols().count;
	
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
			break;

		if(isTranspose)
		{
			if(AddHeaderToTable)
			{
				
				if((numRows + HeaderRows) > presentCols)
					result = tableCommands->InsertColumns(ColRange(presentCols-1,numRows + HeaderRows - presentCols), Tables::eAfter, 0);
				
				if((numRows + HeaderRows) < presentCols)
					result = tableCommands->DeleteColumns(ColRange(numRows + HeaderRows-1, presentCols - (numRows + HeaderRows)));
			}
			else
			{
				
				if(numRows > presentCols)
					result = tableCommands->InsertColumns(ColRange(presentCols-1,numRows - presentCols), Tables::eAfter, 0);
				if(numRows < presentCols)
					result = tableCommands->DeleteColumns(ColRange(numRows-1, presentCols-numRows));
			}

			if(numCols > presentTotalRows)
			{
				result = tableCommands->InsertRows(RowRange(presentTotalRows-1, numCols-presentTotalRows), Tables::eAfter, 0);
			}
			if(numCols < presentTotalRows)
			{
				result = tableCommands->DeleteRows(RowRange(numCols-1, presentTotalRows-numCols));
			}			
		}
		else
		{
			if(AddHeaderToTable)
			{
				if(presentHeaderRows > HeaderRows)
				{
					result = tableCommands->DeleteRows(RowRange(0, presentHeaderRows - HeaderRows));
				}
				if(presentHeaderRows < HeaderRows)
				{
					int32 preHeadRow = 0;
					if(presentHeaderRows == 0)
						preHeadRow =0;
					else if(presentHeaderRows > 0)
						preHeadRow = presentHeaderRows -1;

					result = tableCommands->InsertRows(RowRange(preHeadRow, HeaderRows - presentHeaderRows), Tables::eAfter, 0);
				}
				if(presentBodyRows < numRows)
				{				
					result = tableCommands->InsertRows(RowRange(presentBodyRows + HeaderRows -1, numRows - presentBodyRows), Tables::eAfter, 0);
				}
				if(presentBodyRows > numRows)
				{				
					result = tableCommands->DeleteRows(RowRange(numRows + HeaderRows -1, presentBodyRows - numRows));
				}
			}
			else
			{
				if(presentTotalRows < numRows)
				{				
					result = tableCommands->InsertRows(RowRange(presentTotalRows -1 , numRows - presentTotalRows), Tables::eAfter, 0);
				}
				if(presentTotalRows > numRows)
				{				
					result = tableCommands->DeleteRows(RowRange(numRows -1, presentTotalRows - numRows));
				}
			}

			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, 0);
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(numCols-1, presentCols-numCols));
			}

		}			
	}
	while(kFalse);
} 


ErrorCode TableUtility::InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex,int32 cellHeight)	
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do 
	{
		newFrameUIDRef = UIDRef::gNull;
		status = this->CreateFrame(storyUIDRef.GetDataBase(), newFrameUIDRef,cellHeight);
		if (status != kSuccess) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::InsertInline::status != kSuccess");
			break;
		}
		status = this->ChangeToInline(storyUIDRef, whereTextIndex, newFrameUIDRef);
		if (status != kSuccess) 
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::InsertInline::status != kSuccess");
		
		
	} while (false);
	return status;
}

ErrorCode TableUtility::CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef,int32 cellHeight)
{
	// You can make any type of frame into an inline. Here we make a new graphic frame.	
	//PMRect bounds(0, 0, 100, 100);
	PMRect bounds(0, 0, cellHeight, cellHeight);
	SDKLayoutHelper layoutHelper;
	newFrameUIDRef = layoutHelper.CreateRectangleFrame(UIDRef(database, kInvalidUID), bounds);
	if (newFrameUIDRef)
		return kSuccess;
	else
		return kFailure;
}

ErrorCode TableUtility::ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do {
		// Validate parameters.
		InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
		if(!textModel) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::!textModel");		
			break;
		}
		InterfacePtr<IGeometry> pageItemGeometry(frameUIDRef, UseDefaultIID());
		if (pageItemGeometry == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::pageItemGeometry == nil");		
			break;
		}

		// Insert character in text flow to anchor the inline.
		boost::shared_ptr<WideString>	insertMe(new WideString);

		insertMe->Append(kTextChar_Inline); 
		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
		InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(whereTextIndex, insertMe));
		if(!insertTextCmd) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::insertTextCmd== nil");		
			break;
		}
		status = CmdUtils::ProcessCommand(insertTextCmd);
		if(status != kSuccess) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::status != kSuccess");		
			break;
		}

		// Change the page item into an inline.
		InterfacePtr<ICommand> changeILGCmd(CmdUtils::CreateCommand(kChangeILGCmdBoss));
		if (changeILGCmd == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::changeILGCmd == nil");		
			break;
		}
		InterfacePtr<IRangeData> rangeData(changeILGCmd, UseDefaultIID());
		if (rangeData == nil)

		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::rangeData == nil");
			break;
		}
		rangeData->Set(whereTextIndex, whereTextIndex);
		InterfacePtr<IUIDData> ilgUIDData(changeILGCmd, UseDefaultIID());
		if (ilgUIDData == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::ChangeToInline::ilgUIDData == nil");		
			break;
		}
		ilgUIDData->Set(frameUIDRef);
		changeILGCmd->SetItemList(UIDList(textModel));
		status = CmdUtils::ProcessCommand(changeILGCmd);
		ASSERT(status == kSuccess);

	} while(kFalse);
	return status;
}

ErrorCode TableUtility::ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text)
{

	ErrorCode status = kFailure;
	do {
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}
		ASSERT(textModel);
		if (position < 0 || position >= textModel->TotalLength()) {
			//CA("position invalid");
			break;
		}
		if (length < 0 || length >= textModel->TotalLength()) {
			//CA("length invalid");
			break;
		}
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			//CA("!textModelCmds");
			break;
		}

		if(length == 0)
		{
			InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(position, text));
			ASSERT(insertTextCmd);		
			if (!insertTextCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(insertTextCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}

		}
		else
		{
    		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    		ASSERT(textModelCmds);
    		if (!textModelCmds) {
				//CA("!textModelCmds");
				break;
			}
			InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
			ASSERT(replaceCmd);
			if (!replaceCmd) {
				//CA("!replaceCmd");
				break;
			}
			status = CmdUtils::ProcessCommand(replaceCmd);
		}
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
	} while(false);
	return status;
}

ErrorCode TableUtility::SprayMultipleCustomTableFormTABLEsource(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	ErrorCode result =  kFailure;
	do{
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}

		if (textFrameUID == kInvalidUID)
		{
			break;//breaks the do{}while(kFalse)
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!graphicFrameHierarchy");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA(multiColumnItemHierarchy);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA(!multiColumnItemTextFrame);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}
		
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA(“!frameItemHierarchy”);
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!textFrame");
			break;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			break;
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount();

		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;
		}

		for(int32 tableIndex = 0 ; tableIndex < totalNumberOfTablesInsideTextFrame ; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == NULL)
				continue;

			bool16 HeaderRowPresent = kFalse;

			RowRange rowRange(0,0);
			rowRange = tableModel->GetHeaderRows();
			int32 headerStart = rowRange.start;
			int32 headerCount = rowRange.count;
			
			if(headerCount != 0)
			{
				//CA("HeaderPresent");
				HeaderRowPresent = kTrue;
			}
				
			UIDRef tableRef(::GetUIDRef(tableModel));
			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}			
			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
			int32 totalNumberOfRowsBeforeRowInsertion = 0;
			if(HeaderRowPresent)
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
			else
				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
			
			
			GridArea gridAreaForEntireTable,headerArea;
			if(HeaderRowPresent)
			{
				gridAreaForEntireTable.topRow = headerCount;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;

				headerArea.topRow = 0;
				headerArea.leftCol = 0;
				headerArea.bottomRow = headerCount;
				headerArea.rightCol = totalNumberOfColumns;
			}
			else
			{
				gridAreaForEntireTable.topRow = 0;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
			}
			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
			if(tableGeometry == NULL)
			{
				//CA("tableGeometry == NULL");
				break;
			}

			PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1);
			
			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;

			bool16 tableHeaderPresent = kFalse;	
			bool16 isProductItemPresent = kFalse;
			//Change the attributes of the tableTag.
			//ID..The older ID was -1 which is same as elementID of Product_Number.
			//Change it to someother value so that product_number and table id doesn't conflict.
			//we are already using ID = -101 for item_table_in_tabbed_text format.
			//so ID = -102
			
			PMString attributeVal("");
			XMLReference tagXMLRef = slugInfo.tagPtr->GetXMLReference();
            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef,WideString("ID"),WideString("-102"));
			//parentID
			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("parentID"),WideString( attributeVal));
			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("parentTypeID"),WideString(attributeVal));
			attributeVal.Clear();
			//sectionID
			attributeVal.AppendNumber(PMReal(sectionid));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("sectionID"),WideString(attributeVal));

			attributeVal.Clear();
			attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("pbObjectId"),WideString(attributeVal));
			

			attributeVal.Clear();
			//rowno of Table tag will now have the number of rows in repetating pattern excluding Header rows.
			attributeVal.AppendNumber(PMReal(totalNumberOfRowsBeforeRowInsertion));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("rowno"),WideString(attributeVal));
			attributeVal.Clear();


			if(pNode.getIsProduct() == 1)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("index"),WideString("3"));
			}
			if(pNode.getIsProduct() == 0)
			{
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("index"),WideString("4"));
			}

			for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
			{//start for tagIndex = 0
				XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
				//This is a tag attached to the cell.
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				//Get the text tag attached to the text inside the cell.
				//We are providing only one texttag inside cell.
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					continue;
				}
				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
				{
					XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
					InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
					//The cell may be blank. i.e doesn't have any text tag inside it.
					if(cellTextTagPtr == NULL)
					{
						continue;
					}

					//Now check the typeId attribute of the cellTextTag.
					if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1")
						//for spraying the whole item preset table inside table cell.
						|| cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103"))&& HeaderRowPresent == kFalse) 
					{
						//CA("typeId = -2 || ID == -103");
						tableHeaderPresent = kTrue;
					}

					//Ensure that the tag is of item  (i.e index =4) and not itemTable (i.e tableFlag =1).
					if(cellTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag")) != WideString("1"))
					{
						//CA("isProductItemPresent = kTrue");
						isProductItemPresent = kTrue;
					}
					//change the other necessary attributes of the the cellTextTag
					//parentID
					PMString attributeVal("");
					attributeVal.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attributeVal));
					attributeVal.Clear();

					//parentTypeID
					PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
					double ID = id.GetAsDouble();
								
					if((ID != -701) && (ID != -702) && (ID != -703) && (ID != -704))
					{
						attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentTypeID"),WideString(attributeVal));
						attributeVal.Clear();
					}

					//sectionID
					attributeVal.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("sectionID"),WideString(attributeVal));
					attributeVal.Clear();
				}
			}//end for tagIndex = 0 

			if(isProductItemPresent == kFalse)
			{
				//CA("isProductItemPresent == kFalse");
				for(int32 localTagIndex = 0;localTagIndex < slugInfo.tagPtr->GetChildCount();localTagIndex++)
				{//start for tagIndex = 0
					XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(localTagIndex);
					//This is a tag attached to the cell.
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}

					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
					{
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == NULL)
						{
							continue;
						}
						
						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strImageFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
						double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();
						double typeId = strTypeID.GetAsDouble();
						
						PMString strHeaderFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						int32 header = strHeaderFlag.GetAsNumber();

						PMString dispName("");
						if(strIndex == "4")//This is ItemTable..
						{
							makeTheTagImpotent(cellTextXMLElementPtr);
						}
						else if(strElementID == "-103")
						{
						//	do
						//	{
						//		//CA("here");
						//		VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
						//		if(typeValObj==NULL)
						//			break;

						//		VectorTypeInfoValue::iterator it1;

						//		bool16 typeIDFound = kFalse;
						//		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
						//		{	
						//			if(typeId == it1->getTypeId())
						//			{
						//				typeIDFound = kTrue;
						//				break;
						//			}			
						//		}
						//		if(typeIDFound)
						//			dispName = it1->getName();

						//		if(typeValObj)
						//			delete typeValObj;

						//	}while(kFalse);
						}
						else if(tableFlag == "0" && header == 1)
						{
							//CA("tableFlag == 0 && header == 1");
							CElementModel  cElementModelObj;
							bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
							if(result)
								dispName = cElementModelObj.getDisplayName();
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getTypeId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("childId"),WideString(attrVal));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableFlag"),WideString("-13"));

							
						}
						else if(strElementID == "-104")
						{//This is ProductItemTable data
							if(strIndex == "4")//This is ItemTable..
							{
								makeTheTagImpotent(cellTextXMLElementPtr);
							}
							else
							{
								//Product ItemTable
								PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
								TagStruct tagInfo;												
								tagInfo.whichTab = strIndex.GetAsNumber();
								tagInfo.parentId = pNode.getPubId();
								tagInfo.isTablePresent = tableFlag.GetAsNumber();
								tagInfo.typeId = typeId;
								tagInfo.sectionID = sectionid;								
								GetItemTableInTabbedTextForm(tagInfo,dispName);
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("ID"),WideString("-104"));
							}
						}
						else if(strImageFlag == "1")
						{
							XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
							UIDRef ref = cntentREF.GetUIDRef();
							if(ref == UIDRef::gNull)
							{
								//CA("ref == UIDRef::gNull");
								continue;
							}
																						
							InterfacePtr<ITagReader> itagReader
							((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
							if(!itagReader)
								continue ;
							
							TagList tList=itagReader->getTagsFromBox(ref);
							TagStruct tagInfoo;
							int numTags=static_cast<int> (tList.size());

							if(numTags<0)
							{
								continue;
							}

							tagInfoo=tList[0];

							IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
							if(!iDataSprayer)
							{	
								//CA("!iDtaSprayer");
								continue;
							}
														
							iDataSprayer->fillImageInBox (ref ,tagInfoo, tagInfoo.parentId);
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
							continue;
						}
						else if(tableFlag == "0")
						{
							//CA("product here");							
							dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID, CurrentSectionID, kTrue);
							PMString attrVal;
							attrVal.AppendNumber(PMReal(pNode.getTypeId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("childId"),WideString(attrVal));
							attrVal.Clear();
							attrVal.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attrVal));
						}
						
						int32 delStart = -1, delEnd = -1;
						bool16 shouldDelete = kFalse;
						if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
						{//if(tagInfo.rowno == -117)
                            delStart = tagStartPos-1;
							if(dispName == "")							
								shouldDelete = kTrue;
						}
						if(shouldDelete == kTrue )
						{
							int32 tagStartPos1 = -1;
							int32 tagEndPos1 = -1;

							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

							TextIterator begin(textModel , tagEndPos + 1);
							TextIterator end(textModel , tagEndPos1 - 1);
							int32 addthis = 0;
							bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter <= end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									enterFound = kTrue;
									break;
								}
									
							}
							if(enterFound)
							{
								//CA("enterFound == kTrue");
								delEnd = tagEndPos + 1 + addthis;
							}
							else
							{
								//CA("enterFound == kFalse");
								delEnd = tagEndPos1;
							}
							DeleteText( textModel,   delStart, delEnd- delStart  + 1);
							continue;
						}


						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=dispName;					
						}
						else
							textToInsert=iConverter->translateString(dispName);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);

						}     			
				}//end for tagIndex = 0
				
				IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
				if(!iDataSprayer)
				{	
					//CA("!iDtaSprayer");
					continue;
				}

				UIDRef tempUIDRef = boxUIDRef;
				iDataSprayer->fitInlineImageInBoxInsideTableCell(tempUIDRef);
				
				continue;
			}//end of product if

			//---------------
			VectorScreenTableInfoPtr tableInfo = NULL;
			VectorScreenTableInfoPtr itemTableInfo=NULL;
			TableSourceInfoValue *tableSourceInfoValueObj;
			bool16 isCallFromTS = kFalse;
			InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
			if(ptrTableSourceHelper != nil)
			{
				tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
				if(tableSourceInfoValueObj)
				{
					isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
					if(isCallFromTS )
					{
						if(tableSourceInfoValueObj->getIsProduct())
						{
							tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
							if(!tableInfo)
								break;
						}
						else
						{
							itemTableInfo = tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
							if(!itemTableInfo)
								break;
						}
					}
				}
			}

			//if(pNode.getIsProduct() == 1)
			{
				if(isCallFromTS)
				{
					/////31208
					GridArea bodyArea = tableModel->GetBodyArea();
					int32 bodyCellCount = 0;
					ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
					ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
					while (iterTable1 != endTable1)
					{
						bodyCellCount++;
						++iterTable1;
					}

					int32 tableSprayedCount = 0;
					for(int32 selectedTableIndex = 0 ; selectedTableIndex < tableSourceInfoValueObj->getVec_Table_ID().size() ; selectedTableIndex++)
					{
						int32 totalNumberOfRowsBeforeRowInsertion_new;
						if(selectedTableIndex == 0)
							totalNumberOfRowsBeforeRowInsertion_new  = totalNumberOfRowsBeforeRowInsertion;
						else
						{
							if(HeaderRowPresent)
								totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetBodyRows().count;
							else
								totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetTotalRows().count;
						}
							
						vector<double> vec_items;
						bool16 tableFound = kFalse;
						if(pNode.getIsProduct() == 1)
						{
							CItemTableValue oTableValue;
							VectorScreenTableInfoValue::iterator it;
							for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
							{				
								oTableValue = *it;		
								if(oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
								{
									vec_items = oTableValue.getItemIds();
									tableFound = kTrue;						
									break;
								}
							}
						}
						else
						{
							CItemTableValue	oTableValue;
							VectorScreenTableInfoValue::iterator it;
							for(it = itemTableInfo->begin() ; it != itemTableInfo->end() ; it++)
							{				
								oTableValue = *it;		
								if(oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
								{
									vec_items = oTableValue.getItemIds();
									tableFound = kTrue;						
									break;
								}
							}
						}
						if(tableFound == kFalse)
							continue;
						
						int32 TotalNoOfItems = static_cast<int32> (vec_items.size());
						if(TotalNoOfItems <= 0)
						{
							break;//breaks the for..tableIndex
						}
						
						int32 rowPatternRepeatCount =static_cast<int32>(vec_items.size());

						//we have to deal with 2 cases
						//case 1: when tableHeaders are absent
						//then the total number of rows i.e rowCount = FinalItemIds.size()-1
						//case 2 : when tableHeaders are present
						//then the total number of rows i.e rowCount = FinalItemsIds.size()
						if(tableHeaderPresent && selectedTableIndex == 0)
						{  				
							//rowsToBeStillAdded = rowsToBeStillAdded + 1;
							rowPatternRepeatCount = rowPatternRepeatCount + 1;
						}
				
						int32 rowsToBeStillAdded ;
						if(selectedTableIndex == 0)	
						{
							rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
							rowPatternRepeatCount--;
						}
						else
							rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion;

						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
						if(tableCommands==NULL)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
							break;//continues the for..tableIndex
						}
						GridArea bodyAreaBeforeAddingRow  = tableModel->GetBodyArea();
						GridArea gridAreaIncludingHeader;
						if(HeaderRowPresent)
						{
							if(selectedTableIndex != 0)
							{
								for(int32 rowIndex = 0 ; rowIndex < headerCount ;rowIndex++)
								{
									tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
								}

								gridAreaIncludingHeader = tableModel->GetBodyArea();

								bool16 canCopy = tableModel->CanCopy(headerArea);
								if(canCopy == kFalse)
								{
									//CA("canCopy == kFalse");
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
									break;
								}
								//CA("canCopy 1");
								TableMemento * tempPtr = tableModel->Copy(headerArea);
								if(tempPtr == NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
									break;
								}

                                tableModel->Paste(GridAddress((/*pasteIndex * */totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
                                tempPtr = tableModel->Copy(gridAreaForEntireTable);

								totalNumberOfRowsBeforeRowInsertion_new = totalNumberOfRowsBeforeRowInsertion_new + headerCount;
							}

							for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
							{
								tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
							}
							
							if(rowPatternRepeatCount >= 1) // again the same   imp for mutiple table spray sagar
							{
							
								bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
								if(canCopy == kFalse)
								{
									//CA("canCopy == kFalse");
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
									break;
								}
								
								TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
								if(tempPtr == NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
									break;
								}
								
								for(int32 pasteIndex = 1;pasteIndex <= rowPatternRepeatCount;pasteIndex++)
								{
									tableModel->Paste(GridAddress((pasteIndex * totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
									tempPtr = tableModel->Copy(gridAreaForEntireTable);
								}
							}
						}
						else
						{
							for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
							{
								tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+ rowIndex-1,1),Tables::eAfter,rowHeight);
							}
							
							if(rowPatternRepeatCount >= 1) // again the same  condition equal for multiple table imp
							{
								bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
								if(canCopy == kFalse)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
									break;
								}
								TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
								if(tempPtr == NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
									break;
								}
								for(int32 pasteIndex = 1;pasteIndex <= rowPatternRepeatCount;pasteIndex++)
								{
									tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
									tempPtr = tableModel->Copy(gridAreaForEntireTable);
								}
							}	
							
						}					

						if(HeaderRowPresent)
						{
							GridArea headerArea;
							if(selectedTableIndex == 0)
								headerArea = tableModel->GetHeaderArea();
							else
							{
								headerArea.leftCol = gridAreaIncludingHeader.leftCol;
								headerArea.bottomRow = gridAreaIncludingHeader.bottomRow;
								headerArea.rightCol = gridAreaIncludingHeader.rightCol;
								headerArea.topRow = bodyAreaBeforeAddingRow.bottomRow;
							}
							ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
							ITableModel::const_iterator endTable(tableModel->end(headerArea));
							while (iterTable != endTable)
							{
								GridAddress gridAddress = (*iterTable);         
								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
								if(cellContent == nil) 
								{
									//CA("cellContent == nil");
									break;
								}
								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
								if(!cellXMLReferenceData) {
									//CA("!cellXMLReferenceData");
									break;
								}
								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
								
								++iterTable;
							
								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
								//Also note that, cell may be blank i.e doesn't contain any textTag.
								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
								{
									continue;
								}
								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
								{
									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
									//Check if the cell is blank.
									if(cellTextXMLElementPtr == NULL)
									{
										continue;
									}

									//Get all the elementID and languageID from the cellTextXMLElement
									//Note ITagReader plugin methods are not used.
									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
									//check whether the tag specifies ProductCopyAttributes.
									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
									PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
									PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
									PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
									PMString strdataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
									
									PMString strHeaderVal =   cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
									PMString strChildTag =    cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

									double elementID = strElementID.GetAsDouble();
									double languageID = strLanguageID.GetAsDouble();

									int32 tagStartPos = -1;
									int32 tagEndPos = -1;
									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
									tagStartPos = tagStartPos + 1;
									tagEndPos = tagEndPos -1;

									PMString dispName("");
									////if normal tag present in header row...
									////then we are spraying the value related to the first item.
									double itemID = vec_items[0];
									
									if((strdataType == "4" && isCallFromTS) || (elementID == -121 && isCallFromTS))
									{
										if(pNode.getIsProduct() == 1 )
										{
											CItemTableValue oTableValue;
											VectorScreenTableInfoValue::iterator it;
											for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
											{				
												oTableValue = *it;		
												if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
												{
													strParentTypeID.clear();
													strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(strParentTypeID));
													
													strTypeId.Clear();
													strTypeId.AppendNumber(PMReal(oTableValue.getTableID()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTypeId));

													strTypeId.Clear();
													strTypeId.AppendNumber(PMReal(slugInfo.tableType));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(strTypeId));

													dispName = oTableValue.getName();
													break;
												}
											}
										}
										else
										{
											CItemTableValue	oTableValue;
											VectorScreenTableInfoValue::iterator it;
											for(it = itemTableInfo->begin() ; it != itemTableInfo->end() ; it++)
											{				
												oTableValue = *it;		
												if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
												{
													strParentTypeID.clear();
													strParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(strParentTypeID));
													
													strTypeId.Clear();
													strTypeId.AppendNumber(PMReal(oTableValue.getTableID()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(strTypeId));

													strTypeId.Clear();
													strTypeId.AppendNumber(PMReal(slugInfo.tableType));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(strTypeId));

													dispName = oTableValue.getName();
													break;
												}
											}
										}
														
									}
									else if(strTypeId == "-1")
									{
										double parentTypeID = strParentTypeID.GetAsDouble();

										//CA("#################normal tag present in header row###############");
										if(tableFlag == "1")
										{
											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
											if(index== "3")// || index == "4")
											{//Product ItemTable
												PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
												TagStruct tagInfo;												
												tagInfo.whichTab = index.GetAsNumber();
												if(index == "3")
													tagInfo.parentId = pNode.getPubId();

												tagInfo.isTablePresent = tableFlag.GetAsNumber();
												tagInfo.typeId = typeID.GetAsDouble();
												tagInfo.sectionID = sectionid;
												
													
												GetItemTableInTabbedTextForm(tagInfo,dispName);
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));
											}
											else if(index == "4")
											{//This is the Section level ItemTableTag and we have selected the product
											//for spraying
												makeTheTagImpotent(cellTextXMLElementPtr);
												
											}
											
										}
										else if(imgFlag == "1")
										{							
											XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
											UIDRef ref = cntentREF.GetUIDRef();
											if(ref == UIDRef::gNull)
											{
												//CA("ref == UIDRef::gNull");
												continue;
											}

											InterfacePtr<ITagReader> itagReader
											((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
											if(!itagReader)
												continue ;
											
											TagList tList=itagReader->getTagsFromBox(ref);
											TagStruct tagInfoo;
											int numTags=static_cast<int> (tList.size());

											if(numTags<0)
											{
												continue;
											}

											tagInfoo=tList[0];

											tagInfoo.parentId = pNode.getPubId();
											tagInfoo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
											tagInfoo.sectionID=CurrentSubSectionID;

											IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
											if(!iDataSprayer)
											{	
												//CA("!iDtaSprayer");
												continue;
											}

											iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
											
											PMString attrVal;
											attrVal.AppendNumber(PMReal(itemID));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
											
											for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
											{
												tList[tagIndex].tagPtr->Release();
											}
											
											continue;
										}
										else if(tableFlag == "0")
										{
											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
											if(index =="4")
											{
												if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
												{
													TagStruct tagInfo;
													tagInfo.elementId = elementID;
													tagInfo.tagPtr = cellTextXMLElementPtr;
													tagInfo.typeId = itemID;
													tagInfo.parentTypeID = parentTypeID;
													handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
												}
												else if(elementID == -803)  // For 'Letter Key'
												{											
													dispName = "a" ;	// For First item
												}	
												else if(elementID == -827)  // For 'Number Key'
												{											
													dispName = "1" ;	// For First item
												}	
												else
												{
													if(strHeaderVal == "1" && strChildTag == "-1")
													{
														dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
													}
													else
													{
														dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
													}
												}
												
												PMString attrVal;
												attrVal.AppendNumber(PMReal(itemID));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
											}
											else if( index == "3")
											{//Product
												dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(), elementID, languageID, CurrentSectionID, kTrue);
												PMString attrVal;
												attrVal.AppendNumber(PMReal(pNode.getPubId()));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
											}
										}
											
									}
									else if(imgFlag == "1")
									{
										XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
										UIDRef ref = cntentREF.GetUIDRef();
										if(ref == UIDRef::gNull)
										{
											int32 StartPos1 = -1;
											int32 EndPos1 = -1;
											Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									

											InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
												textModel->QueryStrand(kOwnedItemStrandBoss, 
												IItemStrand::kDefaultIID)));
											if (itemStrand == nil) {
												//CA("invalid itemStrand");
												continue;
											}
											//CA("going for Owened items");
											OwnedItemDataList ownedList;
											itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
											int32 count = ownedList.size();
											if(count > 0)
											{
												UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
												if(newRef == UIDRef::gNull)
												{
													//CA("ref == UIDRef::gNull....2");
													continue;
												}
												ref = newRef;
											}else
												continue;
										}
											

										InterfacePtr<ITagReader> itagReader
										((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
										if(!itagReader)
											continue ;
										
										TagList tList=itagReader->getTagsFromBox(ref);
										TagStruct tagInfoo;
										int numTags=static_cast<int> (tList.size());

										if(numTags<0)
										{
											continue;
										}

										tagInfoo=tList[0];

										tagInfoo.parentId = pNode.getPubId();
										tagInfoo.parentTypeID =  pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
										tagInfoo.sectionID=CurrentSubSectionID;

										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
										if(!iDataSprayer)
										{	
											//CA("!iDtaSprayer");
											continue;
										}

										iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
										
										PMString attrVal;
										attrVal.AppendNumber(PMReal(itemID));
										cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
										
										for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
										{
											tList[tagIndex].tagPtr->Release();
										}
										continue;
									}
									else
									{
										if(tableFlag == "1")
										{					
											//CA("inside cell table is present");
											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
											if(index== "3")
											{
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-103"));										
												do
												{
													PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
													double typeId = strTypeID.GetAsDouble();
													VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
													if(typeValObj==NULL)
													{
														ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
														break;
													}

													VectorTypeInfoValue::iterator it1;

													bool16 typeIDFound = kFalse;
													for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
													{	
														if(typeId == it1->getTypeId())
														{
															typeIDFound = kTrue;
															break;
														}			
													}
													if(typeIDFound)
														dispName = it1->getName();

													if(typeValObj)
														delete typeValObj;
												}while(kFalse);
														
											}
											//This is a special case.The tag is of product but the Item is
											//selected.So make this tag unproductive for refresh.(Impotent)
											else if(index == "4")
											{
												makeTheTagImpotent(cellTextXMLElementPtr);
											}
										}
										else
										{
											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
											if(index== "3")
											{//For product
												CElementModel  cElementModelObj;
												bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
												if(result)
													dispName = cElementModelObj.getDisplayName();
												PMString attrVal;
												attrVal.AppendNumber(PMReal(pNode.getTypeId()));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
																					
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableFlag"),WideString("-13"));
											}
											else if(index == "4")
											{
												if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
												{
													/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
													int32 parentTypeID = strParentTypeID.GetAsNumber();
													
													VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
													if(TypeInfoVectorPtr != NULL)
													{
														VectorTypeInfoValue::iterator it3;
														int32 Type_id = -1;
														PMString temp = "";
														PMString name = "";
														for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
														{
															Type_id = it3->getTypeId();
															if(parentTypeID == Type_id)
															{
																temp = it3->getName();
																if(elementID == -701)
																{
																	dispName = temp;
																}
																else if(elementID == -702)
																{
																	dispName = temp + " Suffix";
																}
															}														
														}													
													}
													if(TypeInfoVectorPtr)
														delete TypeInfoVectorPtr;*/
												}
												else if(elementID == -703)
												{
													dispName = "$Off";
												}
												else if(elementID == -704)
												{
													dispName = "%Off";
												}
												else
													dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

												//Modification of XMLTagAttributes.
												//modify the XMLTagAttribute "typeId" value to -2.
												//We have assumed that there is atleast one stencil with HeaderAttirbute
												//true .So assign for all the stencil in the header row , typeId == -2
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("1"));
											}
											
										}
									}

                                    //added to attach the tag values to ListandtableName
									PMString rowValue("");
									rowValue.AppendNumber(PMReal(gridAddress.row));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));

									PMString colValue("");
									colValue.AppendNumber(PMReal(gridAddress.col));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));

									PMString pbObjectIDStr("");
									pbObjectIDStr.AppendNumber(PMReal(pNode.getPBObjectID()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("pbObjectId"),WideString(pbObjectIDStr));

									int32 delStart = -1, delEnd = -1;
									bool16 shouldDelete = kFalse;
									if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
									{//if(tagInfo.rowno == -117)
                                        delStart = tagStartPos-1;
										if(dispName == "")							
											shouldDelete = kTrue;
									}
									if(shouldDelete == kTrue )
									{
										//CA("Going to delete 1 ");
										int32 tagStartPos1 = -1;
										int32 tagEndPos1 = -1;
										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

										TextIterator begin(textModel , tagEndPos + 1);
										TextIterator end(textModel , tagEndPos1 - 1);
										int32 addthis = 0;
										bool16 enterFound = kFalse;
										for(TextIterator iter = begin ;iter <= end ; iter++)
										{
											addthis++;
											if(*iter == kTextChar_CR)
											{
												enterFound = kTrue;
												break;
											}
												
										}
										if(enterFound)
										{
											//CA("enterFound == kTrue");
											delEnd = tagEndPos + 1 + addthis;
										}
										else
										{
											//CA("enterFound == kFalse");
											delEnd = tagEndPos1;
										}
										DeleteText( textModel,   delStart, delEnd- delStart  + 1);
										continue;
									}

									PMString textToInsert("");
									InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
									if(!iConverter)
									{
										textToInsert=dispName;					
									}
									else
										textToInsert=iConverter->translateString(dispName);

									//CA("textToInsert = " + textToInsert);
									//Spray the Header data .We don't have to change the tag attributes
									//as we have done it while copy and paste.
                                    textToInsert.ParseForEmbeddedCharacters();
									boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
									ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
								}						
							}//for..iterate through each row of the headerRowPattern						
						}
///////////////////////////////////////////////////////////////////////////////////////////
						if(tableHeaderPresent)
						{//if 2 tableHeaderPresent 
							//CA("tableHeaderPresent");
						//First rowPattern  now is of header element.
						//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
							for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
							{//for..iterate through each row of the repeatedRowPattern
								for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
								{//for..iterate through each column
									int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
									XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
									InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
									//We have considered that there is ONE and ONLY ONE text tag inside a cell.
									//Also note that, cell may be blank i.e doesn't contain any textTag.
									if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
									{
										continue;
									}
									for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
									{
										XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
										InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
										//Check if the cell is blank.
										if(cellTextXMLElementPtr == NULL)
										{
											continue;
										}

										//Get all the elementID and languageID from the cellTextXMLElement
										//Note ITagReader plugin methods are not used.
										PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
										PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
										//check whether the tag specifies ProductCopyAttributes.
										PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
										PMString valDataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));

										double elementID = strElementID.GetAsDouble();
										double languageID = strLanguageID.GetAsDouble();
										
										int32 tagStartPos = -1;
										int32 tagEndPos = -1;
										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
										tagStartPos = tagStartPos + 1;
										tagEndPos = tagEndPos -1;

										PMString dispName(""); 
										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										if(index == "3")
										{
											makeTheTagImpotent(cellTextXMLElementPtr);
										}					
										else if(tableFlag == "1")
										{
											if(valDataType == "4")	//-----for spray Table Name in custom Table --
											{
												PMString str_ParentTypeID("");

												if(pNode.getIsProduct() == 1 )	
												{
													CItemTableValue oTableValue;
													VectorScreenTableInfoValue::iterator it;
													for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
													{				
														oTableValue = *it;		
														if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
														{
															str_ParentTypeID.clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(str_ParentTypeID));
															
															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(str_ParentTypeID));

															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(slugInfo.tableType);
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(str_ParentTypeID));

															dispName = oTableValue.getName();
															break;
														}
													}
												}
												else
												{
													CItemTableValue	oTableValue;
													VectorScreenTableInfoValue::iterator it;
													for(it = itemTableInfo->begin() ; it != itemTableInfo->end() ; it++)
													{				
														oTableValue = *it;		
														if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
														{
															str_ParentTypeID.clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(str_ParentTypeID));
															
															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableID()));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(str_ParentTypeID));

															str_ParentTypeID.Clear();
															str_ParentTypeID.AppendNumber(PMReal(slugInfo.tableType));
															Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(str_ParentTypeID));

															dispName = oTableValue.getName();
															break;
														}
													}
												}
											}
											else
											{
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-103"));										
												do
												{
													PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
													double typeId = strTypeID.GetAsDouble();
													VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
													if(typeValObj==NULL)
													{
														ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
														break;
													}

													VectorTypeInfoValue::iterator it1;

													bool16 typeIDFound = kFalse;
													for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
													{	
													   if(typeId == it1->getTypeId())
														{
															typeIDFound = kTrue;
															break;
														}			
													}
													if(typeIDFound)
														dispName = it1->getName();

													if(typeValObj)
														delete typeValObj;
												}while(kFalse);
											}
											
										}
										else
										{
											//CA("tableFlag(header)	!= 1");
											//following code added by Tushar on 27/12/06
											if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
											{
												/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
												int32 parentTypeID = strParentTypeID.GetAsNumber();
												
												VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
												if(TypeInfoVectorPtr != NULL)
												{
													VectorTypeInfoValue::iterator it3;
													int32 Type_id = -1;
													PMString temp = "";
													PMString name = "";
													for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
													{
														Type_id = it3->getTypeId();
														if(parentTypeID == Type_id)
														{
															temp = it3->getName();
															if(elementID == -701)
															{
																dispName = temp;
															}
															else if(elementID == -702)
															{
																dispName = temp + " Suffix";
															}
														}
													}
												}

												if(TypeInfoVectorPtr)
													delete TypeInfoVectorPtr;*/
											}
											else if(elementID == -703)
											{
												dispName = "$Off";
											}
											else if(elementID == -704)
											{
												dispName = "%Off";
											}	
											else if((elementID == -805) )  // For 'Quantity'
											{
												//CA("Quantity Header");
												dispName = "Quantity";
											}
											else if((elementID == -806) )  // For 'Availability'
											{
												dispName = "Availability";			
											}
											else if(elementID == -807)
											{									
												dispName = "Cross-reference Type";
											}
											else if(elementID == -808)
											{									
												dispName = "Rating";
											}
											else if(elementID == -809)
											{									
												dispName = "Alternate";
											}
											else if(elementID == -810)
											{									
												dispName = "Comments";
											}
											else if(elementID == -811)
											{									
												dispName = "";
											}
											else if((elementID == -812) )  // For 'Quantity'
											{
												//CA("Quantity Header");
												dispName = "Quantity";
											}
											else if((elementID == -813) )  // For 'Required'
											{
												//CA("Required Header");
												dispName = "Required";
											}
											else if((elementID == -814) )  // For 'Comments'
											{
												//CA("Comments Header");
												dispName = "Comments";
											}
											else
											{
												//CA("else");
												dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
											}									
											
											//Modification of XMLTagAttributes.
											//modify the XMLTagAttribute "typeId" value to -2.
											//We have assumed that there is atleast one stencil with HeaderAttirbute
											//true .So assign for all the stencil in the header row , typeId == -2
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("1"));

										}
										

										int32 delStart = -1, delEnd = -1;
										bool16 shouldDelete = kFalse;
										if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
										{//if(tagInfo.rowno == -117)
											//if(delStart == tagInfo.startIndex)
												delStart = tagStartPos-1;
											//delEnd = tagEndPos;
											if(dispName == "")							
												shouldDelete = kTrue;
										}
										if(shouldDelete == kTrue )
										{
											//CA("Going to delete 1 ");
									
											int32 tagStartPos1 = -1;
											int32 tagEndPos1 = -1;
											Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

											TextIterator begin(textModel , tagEndPos + 1);
											TextIterator end(textModel , tagEndPos1 - 1);
											int32 addthis = 0;
											bool16 enterFound = kFalse;
											for(TextIterator iter = begin ;iter <= end ; iter++)
											{
												addthis++;
												if(*iter == kTextChar_CR)
												{
													enterFound = kTrue;
													break;
												}
													
											}
											if(enterFound)
											{
												//CA("enterFound == kTrue");
												delEnd = tagEndPos + 1 + addthis;
											}
											else
											{
												//CA("enterFound == kFalse");
												delEnd = tagEndPos1;
											}

											DeleteText( textModel,   delStart, delEnd- delStart  + 1);

											continue;
										}



										PMString textToInsert("");
										InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
										if(!iConverter)
										{
											textToInsert=dispName;					
										}
										else
											textToInsert=iConverter->translateString(dispName);
										//Spray the Header data .We don't have to change the tag attributes
										//as we have done it while copy and paste.
										//WideString insertData(textToInsert);
                                        textToInsert.ParseForEmbeddedCharacters();
										boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
										ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);

									}
								}//end for..iterate through each column.
							}//end for..iterate through each row of repeatedRowPattern.
							//All the subsequent rows will now spray the ItemValue.									
						}//end if 2 tableHeaderPresent
						
						GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);
						int32 ColIndex = -1;
						GridArea bodyArea = tableModel->GetBodyArea();
						
						ITableModel::const_iterator iterTable(tableModel->begin(bodyArea));
						ITableModel::const_iterator endTable(tableModel->end(bodyArea));
						while (iterTable != endTable)
						{
							GridAddress gridAddress = (*iterTable);
							if(tableHeaderPresent)
							if(gridAddress <= headerRowGridAddress){
								iterTable++;							
								continue;
							}
							if(selectedTableIndex != 0)
							{
								if(bodyAreaBeforeAddingRow.Contains(gridAddress) || gridAreaIncludingHeader.Contains(gridAddress))
								{
									++iterTable;
									continue;
								}
							}
							InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
							if(cellContent == nil) 
							{
								//CA("cellContent == nil");
								break;
							}
							InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
							if(!cellXMLReferenceData) {
								//CA("!cellXMLReferenceData");
								break;
							}
							XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
							++iterTable;

							
							ColIndex++;
							int32 itemIDIndex = ColIndex/bodyCellCount;

							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
							//We have considered that there is ONE and ONLY ONE text tag inside a cell.
							//Also note that, cell may be blank i.e doesn't contain any textTag.
							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
							{
								continue;
							}
							// Checking for number of tag element inside cell
							for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
							{
								XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
								InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());							
								//Check if the cell is blank.
								if(cellTextXMLElementPtr == NULL)
								{
									continue;
								}
								//Get all the elementID and languageID from the cellTextXMLElement
								//Note ITagReader plugin methods are not used.
								
								PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
								PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
								//check whether the tag specifies ProductCopyAttributes.
								PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
								PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
								PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
								
								int32 field_val = -1;
								PMString field_1 = cellTextXMLElementPtr->GetAttributeValue(WideString("field1"));
								field_val = field_1.GetAsNumber();	
								PMString type_Id = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
								double typeID = type_Id.GetAsDouble();
								
								double elementID = strElementID.GetAsDouble();
								double languageID = strLanguageID.GetAsDouble();
								double itemID = vec_items[itemIDIndex];
								double parentTypeID = strParentTypeID.GetAsDouble();

								int32 tagStartPos = -1;
								int32 tagEndPos = -1;
								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
								tagStartPos = tagStartPos + 1;
								tagEndPos = tagEndPos -1;

								PMString dispName("");
								if(tableFlag == "1")
								{
									//The following code will spray the table preset inside the table cell.
									//for copy ID = -104
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									if(index== "3")// || index == "4")
									{//Product ItemTable
										PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
										TagStruct tagInfo;												
										tagInfo.whichTab = index.GetAsNumber();
										if(index == "3")
											tagInfo.parentId = pNode.getPubId();
										/*else if(index == "4")
											tagInfo.parentId = pNode.getPBObjectID();*/

										tagInfo.isTablePresent = tableFlag.GetAsNumber();
										tagInfo.typeId = typeID.GetAsDouble();
										tagInfo.sectionID = sectionid;
										
											
										GetItemTableInTabbedTextForm(tagInfo,dispName);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString("-104"));
									}
									else if(index == "4")
									{//This is the Section level ItemTableTag and we have selected the product
									//for spraying
										PMString valDataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));//----
										if(valDataType == "4")	//-----for spray Table Name in custom Table --
										{
											//CA("valDataType == 4");
											PMString str_ParentTypeID("");
											CItemTableValue	oTableValue;
											if(typeID != -1)
											{
												//CA("typeID != -1");
												if(pNode.getIsProduct() == 1 )	
												{
													CItemTableValue oTableValue;
													VectorScreenTableInfoValue::iterator it;
													for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
													{				
														oTableValue = *it;	
														if(oTableValue.getTableTypeID() != typeID)
															continue;

														if((oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex)) && (typeID == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex)))
														{
															dispName = oTableValue.getName();
															break;
														}
													}
												}
												else
												{
													CItemTableValue	oTableValue;
													VectorScreenTableInfoValue::iterator iter;
													for(iter = itemTableInfo->begin() ; iter != itemTableInfo->end() ; iter++)
													{
														oTableValue = *iter;
														if(oTableValue.getTableTypeID() != typeID)
															continue;

														if((oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex)) && (typeID == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex)))
														{
															dispName = oTableValue.getName();
															break;
														}
													}
												}
											}
											else
											{
												if(pNode.getIsProduct() == 1 )	
												{
													VectorScreenTableInfoValue::iterator it;
													for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
													{				
														oTableValue = *it;
														if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
														{
															dispName = oTableValue.getName();
															break;
														}
													}
												}
												else
												{
													VectorScreenTableInfoValue::iterator it;
													for(it = itemTableInfo->begin() ; it != itemTableInfo->end() ; it++)
													{				
														oTableValue = *it;
														if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex))
														{
															dispName = oTableValue.getName();
															break;
														}
													}
												}
											}
											str_ParentTypeID.clear();
											str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableTypeID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(str_ParentTypeID));
											
											str_ParentTypeID.Clear();
											str_ParentTypeID.AppendNumber(PMReal(oTableValue.getTableID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(str_ParentTypeID));

											str_ParentTypeID.Clear();
											str_ParentTypeID.AppendNumber(slugInfo.tableType);
											Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(str_ParentTypeID));
											
										}
										else
										{
											makeTheTagImpotent(cellTextXMLElementPtr);
										}
									}
									PMString tableIDstr("");
									tableIDstr.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex)));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(tableIDstr));

									tableIDstr.Clear();
									tableIDstr.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex)));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(tableIDstr));

									tableIDstr.Clear();
									tableIDstr.AppendNumber(slugInfo.tableType);
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(tableIDstr));
									
								}
								else if(imgFlag == "1")
								{
									XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
									UIDRef ref = cntentREF.GetUIDRef();
									if(ref == UIDRef::gNull)
									{
										//CA("ref == UIDRef::gNull");
										continue;
									}

									InterfacePtr<ITagReader> itagReader
									((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
									if(!itagReader)
										continue ;
									
									TagList tList=itagReader->getTagsFromBox(ref);
									TagStruct tagInfoo;
									int numTags=static_cast<int>(tList.size());

									if(numTags<0)
									{
										continue;
									}

									tagInfoo=tList[0];

									tagInfoo.parentId = pNode.getPubId();
									tagInfoo.parentTypeID =  pNode.getTypeId();  //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
									tagInfoo.sectionID=CurrentSubSectionID;

									IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
									if(!iDataSprayer)
									{	
										//CA("!iDtaSprayer");
										continue;
									}

									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
									
									PMString attrVal;
									attrVal.AppendNumber(PMReal(itemID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									
									for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
									{
										tList[tagIndex].tagPtr->Release();
									}
									continue;
								}
								else if(tableFlag == "0")
								{
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
									PMString val_DataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));//----
									if(index =="4")
									{//Item
										if(field_val != -1)
										{
											if(pNode.getIsProduct() == 1 )
											{
												CItemTableValue	oTableVal;
												VectorScreenTableInfoValue::iterator iter;
												for(iter = tableInfo->begin() ; iter != tableInfo->end() ; iter++)
												{				
													oTableVal = *iter;
													if(oTableVal.getTableTypeID() != field_val)
														continue;
											
													if(oTableVal.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) )
													{
                                                        dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
														break;
													}
												}
											}
											else
											{

												CItemTableValue	oTableVal;
												VectorScreenTableInfoValue::iterator iter;
												for(iter = itemTableInfo->begin() ; iter != itemTableInfo->end() ; iter++)
												{				
													oTableVal = *iter;
													if(oTableVal.getTableTypeID() != field_val)
														continue;
											
													if(oTableVal.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex) )
													{
                                                        dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
														break;
													}
												}
											}
										}
										else
										{
											if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
											{
												TagStruct tagInfo;
												tagInfo.elementId = elementID;
												tagInfo.tagPtr = cellTextXMLElementPtr;
												tagInfo.typeId = itemID;
												tagInfo.parentTypeID = parentTypeID;
												handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);
											}
											else if(elementID == -803)  // For 'Letter Key'
											{
												dispName.Clear();
												int wholeNo =  itemIDIndex / 26;
												int remainder = itemIDIndex % 26;								

												if(wholeNo == 0)
												{// will print 'a' or 'b'... 'z'  upto 26 item ids
													dispName.Append(AlphabetArray[remainder]);
												}
												else  if(wholeNo <= 26)
												{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
													dispName.Append(AlphabetArray[wholeNo-1]);	
													dispName.Append(AlphabetArray[remainder]);	
												}
												else if(wholeNo <= 52)
												{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
													dispName.Append(AlphabetArray[0]);
													dispName.Append(AlphabetArray[wholeNo -26 -1]);	
													dispName.Append(AlphabetArray[remainder]);										
												}
											}
											else
											{
												dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
											}
											if(elementID == -827)  // For 'Number Key'
											{
												dispName.Clear();
												dispName.Append(itemIDIndex);	
											}
										}
										
										PMString attrVal;
										attrVal.AppendNumber(PMReal(itemID));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"),WideString(attrVal));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childTag"),WideString("1"));
									}
									else if( index == "3")
									{//Product
										dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(), elementID, languageID, CurrentSectionID, kTrue);
										PMString attrVal;
										attrVal.AppendNumber(PMReal(pNode.getTypeId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(attrVal));
										attrVal.Clear();

										attrVal.AppendNumber(PMReal(pNode.getPubId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("parentID"),WideString(attrVal));
									}
									PMString tableIDstr("");
									tableIDstr.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(selectedTableIndex)));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableId"),WideString(tableIDstr));
									
									tableIDstr.Clear();
									tableIDstr.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_TableType_ID().at(selectedTableIndex)));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("typeId"),WideString(tableIDstr));

									tableIDstr.Clear();
									tableIDstr.AppendNumber(slugInfo.tableType);
									Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(tableIDstr));
								}

                                //added to attach the tag Values to child elements
								PMString rowValue("");
								rowValue.AppendNumber(PMReal(gridAddress.row));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));

								PMString colValue("");
								colValue.AppendNumber(PMReal(gridAddress.col));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));

								PMString pbObjectIDStr("");
								pbObjectIDStr.AppendNumber(PMReal(pNode.getPBObjectID()));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("pbObjectId"),WideString(pbObjectIDStr));

								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("header"),WideString("-1"));
							
								int32 delStart = -1, delEnd = -1;
								bool16 shouldDelete = kFalse;
								if(cellTextXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty")) == WideString("1"))
								{//if(tagInfo.rowno == -117)
                                    delStart = tagStartPos-1;
									if(dispName == "")							
										shouldDelete = kTrue;
								}
								if(shouldDelete == kTrue )
								{
									int32 tagStartPos1 = -1;
									int32 tagEndPos1 = -1;

									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);

									TextIterator begin(textModel , tagEndPos + 1);
									TextIterator end(textModel , tagEndPos1 - 1);
									int32 addthis = 0;
									bool16 enterFound = kFalse;
									for(TextIterator iter = begin ;iter <= end ; iter++)
									{
										addthis++;
										if(*iter == kTextChar_CR)
										{
											enterFound = kTrue;
											break;
										}
									}
									if(enterFound)
									{
										//CA("enterFound == kTrue");
										delEnd = tagEndPos + 1 + addthis;
									}
									else
									{
										//CA("enterFound == kFalse");
										delEnd = tagEndPos1;
									}

									DeleteText( textModel,   delStart, delEnd- delStart  + 1);
									continue;
								}

								PMString textToInsert("");
								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
								if(!iConverter)
								{
									textToInsert=dispName;					
								}
								else
									textToInsert=iConverter->translateString(dispName);
								//Spray the Header data .We don't have to change the tag attributes
								//as we have done it while copy and paste.
                                textToInsert.ParseForEmbeddedCharacters();
								boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
								
								int32 length = -1;
								if(textToInsert.NumUTF16TextChars() > 0 && (tagEndPos-tagStartPos + 1) < 1)
									length = textToInsert.NumUTF16TextChars();
								else
									length = tagEndPos-tagStartPos + 1;

								ReplaceText(textModel,tagStartPos,length,insertData);
							}
						}
						tableSprayedCount--;
					}	

					attributeVal.Clear();
					attributeVal.AppendNumber(PMReal(tableSprayedCount));
					slugInfo.tagPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
				}
			}

			if(!isCallFromTS)
			{
				if(tableInfo)
				{
					tableInfo->clear();
					delete tableInfo;
				}
			}
		}
	}while(kFalse);
	return result;
}
/// following two functions were written for spraying all standard tables one below other
//void TableUtility::fillDataInTableForAllStandardTable(PublicationNode& pNode,/* TagStruct& tStruct,*/ int32& tableId,UIDRef& boxUIDRef)
//{
//	//CA("Inside fillDataInTableForAllStandardTable");
//	do{
//		IsDBTable = kTrue;	
//		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
//		
//		
//
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//		InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//		if(!itagReader)
//		{
//			ptrIAppFramework->LogError("CDataSprayer :: sprayForTaggedBox: itagReader == NULL");
//			break;
//		}
//
//		InterfacePtr<ISelectionManager>	iSelectionManager1 (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if(!iSelectionManager1)
//		{	//CA("Slection NULL");
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager1, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			//CA("!layoutSelectionSuite");
//			return;
//		}
//		//It gets refernce to persistant objects present in the (database) document as long as document is open.
//		UIDList CUrrentBoxUIDList(boxUIDRef.GetDataBase(), boxUIDRef.GetUID());
//		//selectionManager->DeselectAll(NULL); // deselect every active CSB
//		
//		//layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//
///////////////		Copying the table on clipboard
//							
//		InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
//		if (scrapSuite == nil)
//		{
//			//CA("CopySelectedItems: scrapSuite invalid");
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
//			break;
//		}
//		InterfacePtr<IClipboardController> clipController(gSession, UseDefaultIID());
//		if (clipController == nil)
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
//			break;
//		}
//		// Copy  the selection:
//		if(scrapSuite->CanCopy(clipController) != kTrue)
//			break;
//		if(scrapSuite->Copy(clipController) != kSuccess)
//			break;
//		
///////////////////
//		int32 objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		
//	
//
//		int32 sectionid = -1;
//		if(global_project_level == 3)
//			sectionid = CurrentSubSectionID;
//		if(global_project_level == 2)
//			sectionid = CurrentSectionID;
//
//		VectorScreenTableInfoPtr tableInfo = NULL;
//		if(pNode.getIsONEsource())
//		{
//			// For ONEsource mode
//			tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
//		}else
//		{
//			//For publication mode 
//			tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid , pNode.getPubId());
//		}
//
//		if(!tableInfo)
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::fillDataInTableForAllStandardTable::tableInfo is NULL");	
//			break;
//		}
//		
//		TagList tList=itagReader->getFrameTags(boxUIDRef);	
//		TagStruct tStruct=tList[0];
//		
//		if(tStruct.isAutoResize == 1)
//		IsAutoResize= 1;
//
//		if(tStruct.isAutoResize == 2)
//			IsAutoResize= 2;
//
//		if(tStruct.colno == -1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//
//
//		int32 i = 0;
//		PMReal margin = 0;
//		int32 numRows=0;
//		int32 numCols=0;
//		UIDRef BoxUIDRef = boxUIDRef;
//		CItemTableValue oTableValue;
//		vector<int32> vec_items;
//		vector<int32> vec_tableheaders;
//		bool8 isTranspose = kFalse;
//		VectorScreenTableInfoValue::iterator it1;
//		for(it1 = tableInfo->begin(); it1 != tableInfo->end(); it1++)
//		{	
//
//			oTableValue = *it1;
//			vec_items = oTableValue.getItemIds();
//			
//			if(vec_items.size() < 1)
//			{
//				//CA("continue");
//				continue;
//			}
//			
//			numRows = oTableValue.getTableData().size();
//			if(numRows<=0)
//				break;
//
//			numCols = oTableValue.getTableHeader().size();
//			if(numCols<=0)
//				break;
//			
//			vec_tableheaders = oTableValue.getTableHeader();
//			isTranspose = oTableValue.getTranspose();	
//			
//			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//			if(!iSelectionManager)
//			{	
//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::copySelectedItems::iSelectionManager is NULL");
//				break;
//			}
//			iSelectionManager->DeselectAll(nil);
//
//			InterfacePtr<IGeometry> iGeo(BoxUIDRef, UseDefaultIID());
//			if(!iGeo)
//			{
//				ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::fillDataInTableForAllStandardTable::!iGeo");								
//				break;
//			}
//			
//
//			if(i==0)
//			{
//				//CA("1==0");
//				PMReal rel;
//				int32 ht = 0;
//				int32 wt = 0;
//				PMRect area = iGeo->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeo)));
//				rel = area.Height();
//				ht=::ToInt32(rel);
//				rel=area.Width();	
//				wt=::ToInt32(rel);
//
//				TableUtility oTableUtil(this);
//				UIDRef tableUIDRef;
//			
//				if(oTableUtil.isTablePresent(BoxUIDRef, tableUIDRef))
//				{
//					//CA("Before resizeTable");
//					resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
//					//CA("After resizeTable");
//				}
//
//				oTableUtil.sprayAllStandardTables(isTranspose,BoxUIDRef,tableUIDRef,tStruct,oTableValue, numRows, numCols,vec_tableheaders,vec_items,tableId);
//				
//				if(IsAutoResize == 1)
//				{
//				//CA("Inside IsAutoResize");
//					UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//								
//					UIDList processedItems;
//					K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//				
//					ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//				
//				}
//				if(IsAutoResize == 2)
//				{
//					//CA("Inside overflow");
//					ThreadTextFrame ThreadObj;
//					ThreadObj.DoCreateAndThreadTextFrame(BoxUIDRef);
//				}
//				i++;
//				continue;
//			}
//
//			PMRect pageBounds = iGeo->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeo)));
//
//			if(HorizontalFlow==kTrue)
//			{
//				margin = pageBounds.Right() -pageBounds.Left() +2;
//				pageBounds.Top(pageBounds.Top());
//				pageBounds.Bottom(pageBounds.Bottom());
//				pageBounds.Left(pageBounds.Left()+margin);
//				pageBounds.Right(pageBounds.Right()+margin);
//			}
//			else
//			{
//				margin = pageBounds.Bottom() -pageBounds.Top() +2;
//				pageBounds.Top(pageBounds.Top()+margin);
//				pageBounds.Bottom(pageBounds.Bottom()+margin);
//				pageBounds.Left(pageBounds.Left());
//				pageBounds.Right(pageBounds.Right());
//			
//			}
////////////		paste and move the 2*2 copied table			
//			InterfacePtr<IControlView> controlView(::QueryFrontView());
//			if (controlView == nil)
//			{
//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
//				break;
//			}
//
//			if(scrapSuite->Paste(clipController, controlView) != kSuccess)
//				break;
//
//			UIDList copiedBoxUIDList;
//			
//			InterfacePtr<ISelectionManager>	iSelectionManager1 (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//			if(!iSelectionManager)
//			{	
//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::copySelectedItems::iSelectionManager is NULL");
//				break;
//			}
//			InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager1))); 
//			if(!txtMisSuite)
//			{
//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::copySelectedItems::!txtMisSuite");
//				break; 
//			}
//			txtMisSuite->GetUidList(copiedBoxUIDList);
//			BoxUIDRef = copiedBoxUIDList.GetRef(0);
//
//			IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//			if(!iDataSprayer){
//				break;		
//			}
//
//			PBPMPoint moveToPoints(pageBounds.Left(),pageBounds.Top());	
//			
//			InterfacePtr<IGeometry> geometryPtr(BoxUIDRef, IID_IGEOMETRY);
//			if(!geometryPtr)
//			{
//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::copySelectedItems::case 1:!geometryPtr");								
//				break;
//			}
//			PMRect boxBounds = geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
//			
//			PMReal top = boxBounds.Top();
//			PMReal left = boxBounds.Left();
//
//
//			PMReal left1 = moveToPoints.X() - left;
//			PMReal top1 = moveToPoints.Y() - top;
//
//			const PBPMPoint moveByPoints(left1, top1);
//			
//			Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
//			PBPMPoint referencePoint(PMPoint(0,0));
//
//			ErrorCode errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( copiedBoxUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y()));
//			if(errorCode !=  kSuccess)
//				break;
////////////////////////////////////////
//			PMReal rel;
//			int32 ht = 0;
//			int32 wt = 0;
//			rel = boxBounds.Height();
//			ht=::ToInt32(rel);
//			rel=boxBounds.Width();	
//			wt=::ToInt32(rel);
//
//
//			tList=itagReader->getFrameTags(BoxUIDRef);	
//			TagStruct tStruct=tList[0];
//
//			TableUtility oTableUtil(this);
//			UIDRef tableUIDRef;
//			if(oTableUtil.isTablePresent(BoxUIDRef, tableUIDRef))
//			{
//				resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
//			}
//	
//			oTableUtil.sprayAllStandardTables(isTranspose,BoxUIDRef,tableUIDRef,tStruct,oTableValue, numRows, numCols,vec_tableheaders,vec_items,tableId);
//		
//			if(IsAutoResize == 1)
//			{
//			//CA("Inside IsAutoResize");
//				UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//							
//				UIDList processedItems;
//				K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//			
//				ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//			
//			}
//			if(IsAutoResize == 2)
//			{
//				//CA("Inside overflow");
//				ThreadTextFrame ThreadObj;
//				ThreadObj.DoCreateAndThreadTextFrame(BoxUIDRef);
//			}
//
//			i++;
//		}
//		IsDBTable = kFalse;
//	}while(kFalse);
//}

//void TableUtility::sprayAllStandardTables(bool8 isTranspose,const UIDRef& BoxUIDRef,const UIDRef& tableUIDRef, TagStruct& tStruct,CItemTableValue oTableValue,int32 numRows, int32 numCols, vector<int32> vec_tableheaders,vector<int32> vec_items,int32& tableId)
//{
//	//CA("TableUtility::sprayAllStandardTables");
//	do{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//
//		if(isTranspose)
//		{
//			//CA("Inside isTranspose");
//			int32 i=0, j=0;
//				
//			tStruct.typeId = oTableValue.getTableTypeID();
//
//			if(AddHeaderToTable)
//			{
//				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//				i=1; j=0;
//			}
//
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			
//			vector<vector<PMString> >::iterator it1;
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				vector<PMString>::iterator it2;
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{					
//					if(j==0)
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//
//					setTableRowColData(tableUIDRef, (*it2), j-1, i);	
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j-1];
//				//	CAttributeValue oAttribVal;
//				//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//				//	PMString dispname = oAttribVal.getDisplay_name();
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//					PMString TableColName("");// = oAttribVal.getTable_col();
//					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
//				//	{
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(vec_tableheaders[j-1]);
//				//	}
//					//CA(TableColName);
//					TableColName = keepOnlyAlphaNumeric(TableColName);
//
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					if(AddHeaderToTable)
//						stencileInfo.typeId  = vec_items[i-1];
//					else
//						stencileInfo.typeId  = vec_items[i];
//					stencileInfo.parentId = pNode.getPubId();
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					//4Aug..ItemTable
//					stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
//					//stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSubSectionID; 
//					stencileInfo.sectionID = CurrentSubSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.LanguageID = tStruct.languageID;
//
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = oTableValue.getTableTypeID();
//					XMLReference txmlref;
//	//CA(tStruct.tagPtr->GetTagString());		
//
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					//CA("TableTagNameNew : " + TableTagNameNew);
//					if(TableTagNameNew.Contains("PSTable"))
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSTable";
//						//CA("PSTable Not Found : "+ TableTagNameNew);
//					}
//
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j-1, i,tableId );
//					j++;
//				}
//				i++;
//				j=0;
//			}
//		}
//		else
//		{
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			vector<vector<PMString> >::iterator it;
//			int32 i=0, j=0;
//	//PMString as("vec_tablerows.size()	:	");
//	//as.AppendNumber(vec_tablerows.size());
//	//CA(as);
//			tStruct.typeId = oTableValue.getTableTypeID();
//			
//			if(AddHeaderToTable){
//				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//				 i=1; j=0;
//				}
//
//			for(it = vec_tablerows.begin(); it != vec_tablerows.end(); it++)
//			{
//				//CA("Row");
//				vector<PMString>::iterator it1;
//				for(it1 = (*it).begin() ; it1 != (*it).end() ; it1++)
//				{
//					//CA("Coloum");
//					if(j==0 )
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//		
//					setTableRowColData(tableUIDRef, (*it1), i, j-1);
//
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j-1];
//					/*CAttributeValue oAttribVal;
//					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//					PMString dispname = oAttribVal.getDisplay_name();*/
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//	//PMString a("dispname	:	");
//	//a.Append(dispname);
//	//CA(a);
//					PMString TableColName("");  // = oAttribVal.getTable_col();
//				
//					TableColName.Append("ATTRID_");
//					TableColName.AppendNumber(vec_tableheaders[j-1]);
//
//					TableColName = keepOnlyAlphaNumeric(TableColName);
//
//				
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					if(AddHeaderToTable)
//						stencileInfo.typeId  = vec_items[i-1];
//					else
//						stencileInfo.typeId  = vec_items[i];
//					stencileInfo.parentId = pNode.getPubId();
//					stencileInfo.parentTypeId = pNode.getTypeId();
//					//4Aug..ItemTable
//					stencileInfo.whichTab = 4; // Item Attributes ////for cell tag of product standard Table
//					//stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSubSectionID; 
//					stencileInfo.sectionID = CurrentSubSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//
//					XMLReference txmlref;
//
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//
//					if(TableTagNameNew.Contains("PSTable"))
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSTable";
//						//CA("PSTable Not Found : "+ TableTagNameNew);
//					}
//					//CA("Before Tag Table");
//	//PMString h("tStruct.tagPtr->GetTagString()	:	");
//	//h.Append(tStruct.tagPtr->GetTagString());
//	//h.Append("\r");
//	//h.Append("TableTagNameNew	:	");
//	//h.Append(TableTagNameNew);
//	//h.Append("\r");
//	//h.Append("i	:	");
//	//h.AppendNumber(i);
//	//h.Append("\r");
//	//h.Append("j-1	:	");
//	//h.AppendNumber(j-1);
//	//h.Append("\r");
//	//h.Append("tableId	:	");
//	//h.AppendNumber(tableId);
//	//CA(h);
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j-1, tableId );
//					//CA("After Tag Table");
//					j++;
//				}
//				i++;
//				j=0;
//			}
//			i=0;j=0;
//		}
///////////////////////////	set parent tag
//		CObjectValue oVal;
//	
//		oVal = ptrIAppFramework->GETProduct_getObjectElementValue(pNode.getParentId());
//		int32 parentTypeID= oVal.getObject_type_id();
//		PMString attribVal("");
//		attribVal.AppendNumber(pNode.getParentId());
//		if(tStruct.tagPtr)	
//		{			
//			
//			tStruct.tagPtr->SetAttributeValue("parentID", attribVal);//ObjectID		
//			attribVal.Clear();
//
//			attribVal.AppendNumber(tStruct.imgFlag);		
//			tStruct.tagPtr->SetAttributeValue("imgFlag", attribVal);//SubsectionID		
//			attribVal.Clear();		
//		
//			attribVal.AppendNumber(oTableValue.getTableTypeID());	
//			tStruct.tagPtr->SetAttributeValue("typeId", attribVal);
//			attribVal.Clear();
//
//			if(global_project_level == 3)
//				attribVal.AppendNumber(CurrentSubSectionID);
//			if(global_project_level == 2)
//				attribVal.AppendNumber(CurrentSectionID);
//
//			tStruct.tagPtr->SetAttributeValue("sectionID", attribVal);//Publication ID
//			attribVal.Clear();
//			
//			attribVal.AppendNumber(parentTypeID);			// Need to check
//			tStruct.tagPtr->SetAttributeValue("parentTypeID", attribVal);//Parent Type ID
//		}	
////////////////////////////	End
//		//setTagParent(tStruct, idList[tStruct.whichTab-1], kTrue);//Re-Tag the box
//	}while(kFalse);
//
//}
///////////////////////////	upto this


//ErrorCode TableUtility::SprayMMYCustomTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
//{
//	ErrorCode errcode = kFailure;
//	ErrorCode result = kFailure;
//	int32 languageId = -1;
//	
//	do{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//		 	
//		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//		*/
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//
//		if (textFrameUID == kInvalidUID)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		//if (textFrame == NULL)
//		//{
//		//	break;//breaks the do{}while(kFalse)
//		//}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA(graphicFrameHierarchy);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!graphicFrameHierarchy");
//			break;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA(multiColumnItemHierarchy);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA(!multiColumnItemTextFrame);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
//			//CA("Its Not MultiColumn");
//			break;
//		}
//		
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA(“!frameItemHierarchy”);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!frameItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		textFrame(frameItemHierarchy, UseDefaultIID());
//		if (!textFrame) {
//			//CA("!!!ITextFrameColumn");
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!textFrame");
//			break;
//		}
//		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//		if (textModel == NULL)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==NULL)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//
//		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::totalNumberOfTablesInsideTextFrame < 0");																						
//			break;//breaks the do{}while(kFalse)
//		}
//
//		/*PMString totalNumberOfTablesInsideTextFrameStr = "";
//		totalNumberOfTablesInsideTextFrameStr.AppendNumber(totalNumberOfTablesInsideTextFrame);
//		CA("totalNumberOfTablesInsideTextFrame = " + totalNumberOfTablesInsideTextFrameStr);*/
//		
//		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//		{//for..tableIndex
//			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//			if(tableModel == NULL)
//				continue;//continues the for..tableIndex
//
//			bool16 HeaderRowPresent = kFalse;
//
//			RowRange rowRange(0,0);
//			rowRange = tableModel->GetHeaderRows();
//			int32 headerStart = rowRange.start;
//			int32 headerCount = rowRange.count;
//			
//			PMString HeaderStart = "";
//			HeaderStart.AppendNumber(headerStart);
//			PMString HeaderCount = "";
//			HeaderCount.AppendNumber(headerCount);
//		//	CA("HeaderStart = " + HeaderStart + ", HeaderCount =  " + HeaderCount);
//
//			if(headerCount != 0)
//			{
//				//CA("HeaderPresent");
//				HeaderRowPresent = kTrue;
//			}
//			
//			
//			UIDRef tableRef(::GetUIDRef(tableModel)); 
//			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//			//IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
//
//			languageId = slugInfo.languageID; 
//			XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();			
//
//			UIDRef ContentRef = contentRef.GetUIDRef();
//			if( ContentRef != tableRef)
//			{
//				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//				continue; //continues the for..tableIndex
//			}	
//			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
//					
//			int32 totalNumberOfRowsBeforeRowInsertion = 0;
//			if(HeaderRowPresent)
//				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
//			else
//				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
//
//			/*PMString numberOfRows = "";
//			numberOfRows.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//			CA("totalNumberOfRowsBeforeRowInsertion  = " + numberOfRows);*/
//            		
//			//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
//			GridArea gridAreaForEntireTable,headerArea;;
//			if(HeaderRowPresent)
//			{	//CA("fill gridAreaForEntireTable");			 
//				gridAreaForEntireTable.topRow = headerCount;
//				gridAreaForEntireTable.leftCol = 0;
//				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
//                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//			
//				headerArea.topRow = 0;
//				headerArea.leftCol = 0;
//				headerArea.bottomRow = headerCount;
//				headerArea.rightCol = totalNumberOfColumns;			
//			}
//			else
//			{
//				gridAreaForEntireTable.topRow = 0;
//				gridAreaForEntireTable.leftCol = 0;
//				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
//                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//			}
//
//			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
//			if(tableGeometry == NULL)
//			{
//				//CA("tableGeometry == NULL");
//				break;//breaks the for..tableIndex
//			}
//			PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 			
//			int32 sectionid = -1;
//			if(global_project_level == 3)
//				sectionid = CurrentSubSectionID;
//			if(global_project_level == 2)
//				sectionid = CurrentSectionID;
//
//			bool16 tableHeaderPresent = kFalse;
//		
//			//Change the attributes of the tableTag.
//			//ID..The older ID was -1 which is same as elementID of Product_Number.
//			//Change it to someother value so that product_number and table id doesn't conflict.
//			//we are already using ID = -101 for item_table_in_tabbed_text format.
//			//so ID = -102
//		
//			slugInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-102"));//--------------
//
//			//parentID
//			PMString attributeVal("");
//			attributeVal.AppendNumber(pNode.getPubId());
//			slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
//			attributeVal.Clear();
//			//parentTypeID
//			attributeVal.AppendNumber(pNode.getTypeId());
//			slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
//			attributeVal.Clear();
//			//sectionID
//			attributeVal.AppendNumber(sectionid);
//			slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
//			
//
//			attributeVal.Clear();
//			attributeVal.AppendNumber(pNode.getPBObjectID());
//			slugInfo.tagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));
//
//			
//			if(pNode.getIsProduct() == 1)
//			{
//				slugInfo.tagPtr->SetAttributeValue(WideString("index"),WideString("3"));
//			}
//			if(pNode.getIsProduct() == 0)
//			{
//				slugInfo.tagPtr->SetAttributeValue(WideString("index"),WideString("4"));
//			}
//
//			int32 field_1 = -1;		
//			int32 sort_by_attributeId = -1;
//			for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
//			{//start for tagIndex = 0
//				XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
//				//This is a tag attached to the cell.
//				//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//				//Get the text tag attached to the text inside the cell.
//				//We are providing only one texttag inside cell.
//				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//				{
//					continue;
//				}
//				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//				{
//					XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//					//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//					InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//					//The cell may be blank. i.e doesn't have any text tag inside it.
//					if(cellTextTagPtr == NULL)
//					{
//						continue;
//					}
//					
//					if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
//						 && HeaderRowPresent == kFalse)
//					{
//						//CA("header present");
//						tableHeaderPresent = kTrue;
//					}
//
//					PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
//					field_1 = fld_1.GetAsNumber();
//					
//					if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
//					{		
//						if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
//						{							
//							PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//							sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
//						}
//					}
//										
//					
//					//parentID
//					PMString attributeVal("");
//					attributeVal.AppendNumber(pNode.getPubId());
//					cellTextTagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
//					attributeVal.Clear();
//
//					PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//					int32 ID = id.GetAsNumber();
//											
//					//parentTypeID
//					if(ID != -701 && ID != -702 && ID != -703 && ID != -704)
//					{
//						attributeVal.AppendNumber(pNode.getTypeId());
//						cellTextTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
//						attributeVal.Clear();
//					}
//
//					//sectionID
//					attributeVal.AppendNumber(sectionid);
//					cellTextTagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
//					
//					attributeVal.Clear();
//					attributeVal.AppendNumber(slugInfo.tableType);
//					cellTextTagPtr->SetAttributeValue(WideString("tableType"),WideString(attributeVal));
//					
//					attributeVal.Clear();
//					attributeVal.AppendNumber(pNode.getPBObjectID());
//					cellTextTagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));					
//				}
//			}//end for tagIndex = 0 
//
//
//			
//			VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPBObjectID());
//			if(!tableInfo)
//			{
//				//CA("tableinfo is NULL");
//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
//				break;
//			}
//			
//			if(tableInfo->size()==0)
//			{ //CA("tableinfo size==0");
//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
//				break;
//			}
//			CItemTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			vector<int32> vec_items;
//			vector<int32> FinalItemIds;
//			FinalItemIds.clear();
//
//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			{
//				//CA("iterate through vector tableInfo ");
//				//for tabelInfo start
//				oTableValue = *it;				
//				vec_items = oTableValue.getItemIds();
//			
//				if(field_1 == -1)	//--------
//				{
//					if(FinalItemIds.size() == 0)
//					{
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 i=0; i<vec_items.size(); i++)
//						{	bool16 Flag = kFalse;
//							for(int32 j=0; j<FinalItemIds.size(); j++)
//							{
//								if(vec_items[i] == FinalItemIds[j])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[i]);
//						}
//					}
//				}
//				else
//				{
//					if(field_1 != oTableValue.getTableTypeID())
//						continue;
//
//					if(FinalItemIds.size() == 0)
//					{
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 i=0; i<vec_items.size(); i++)
//						{	bool16 Flag = kFalse;
//							for(int32 j=0; j<FinalItemIds.size(); j++)
//							{
//								if(vec_items[i] == FinalItemIds[j])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[i]);
//						}
//					}
//				}
//				
//			}//for tabelInfo end			
//			//end ItemTable Handling
//
//			/*PMString FinaiItemIdsStr("FinalItemIds size = ");
//			FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
//			CA(FinaiItemIdsStr);*/
//			
//			if(tableInfo)
//				delete tableInfo;
//
//			CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);
//				
//
//			int32 TotalNoOfItems = getMMYTableRows(MMYTablePtr);
//			if(TotalNoOfItems <= 0)
//			{
//				//CA("TotalNoOfItem <= 0");
//				makeTagInsideCellImpotent(slugInfo.tagPtr);
//				break;
//			}
//
//			GridArea bodyArea = tableModel->GetBodyArea();
//			int32 bodyCellCount = 0;
//			ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
//			ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
//			while (iterTable1 != endTable1)
//			{
//				bodyCellCount++;
//				++iterTable1;
//			}
//						
//			int32 rowPatternRepeatCount = TotalNoOfItems;//static_cast<int32>(FinalItemIds.size());			
//			//int32 rowsToBeStillAdded = TotalNoOfItems - totalNumberOfRowsBeforeRowInsertion;
//			//we have to deal with 2 cases
//			//case 1: when tableHeaders are absent
//			//then the total number of rows i.e rowCount = FinalItemIds.size()-1
//			//case 2 : when tableHeaders are present
//			//then the total number of rows i.e rowCount = FinalItemsIds.size()
//			if(tableHeaderPresent)
//			{  	
//				//CA("tableHeaderPresent");
//				//rowsToBeStillAdded = rowsToBeStillAdded + 1;
//				rowPatternRepeatCount = rowPatternRepeatCount + 1;
//			}
//			int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
//			
//			//PMString rowsToBeStillAddedStr = "";
//			//rowsToBeStillAddedStr.AppendNumber(rowsToBeStillAdded);
//			
//			//CA("rowsToBeStillAdded = " + rowsToBeStillAddedStr);
//
//			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//			if(tableCommands==NULL)
//			{
//				break;//continues the for..tableIndex
//			}
//
//			if(HeaderRowPresent)
//			{
//				//CA("HeaderRowPresent 1111");
//				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
//				{
//					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
//				}
//				
//				bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//				if(canCopy == kFalse)
//				{
//					//CA("canCopy == kFalse");
//					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
//					break;
//				}
//				//CA("canCopy");
//				
//				TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//				if(tempPtr == NULL)
//				{
//					ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
//					break;
//				}
//				
//				for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//				{
//					tableModel->Paste(GridAddress((pasteIndex * totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
//					tempPtr = tableModel->Copy(gridAreaForEntireTable);
//				}
//			}
//			else
//			{
//				//CA("HeaderRowPresent==kFalse");
//				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
//				{
//					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);
//
//				}
//				
//				bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//				if(canCopy == kFalse)
//				{
//					//CA("canCopy == kFalse");
//					break;
//				}
//				//CA("canCopy");
//				TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//				if(tempPtr == NULL)
//				{
//					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable) == NULL");
//					break;
//				}
//			
//				for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//				{
//					tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
//					tempPtr = tableModel->Copy(gridAreaForEntireTable);
//				}
//			}
//
//			int32 nonHeaderRowPatternIndex = 0;
//			
//			if(HeaderRowPresent)
//			{
//				CA("HeaderRowPresent 1");
//			//	for(int32 indexOfRowInTheHeaderRowPattern = 0 ; indexOfRowInTheHeaderRowPattern < headerCount ; indexOfRowInTheHeaderRowPattern++)
//			//	{//for..iterate through each row of the headerRowPattern
//			//		for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
//			//		{//for..iterate through each column
//						
//				//GridArea gridArea = tableModel->GetCellArea (GridAddress(indexOfRowInTheHeaderRowPattern,columnIndex));
//				GridArea headerArea = tableModel->GetHeaderArea();
//				ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
//				ITableModel::const_iterator endTable(tableModel->end(headerArea));
//				while (iterTable != endTable)
//				{
//					//CA("Table iterator");
//					GridAddress gridAddress = (*iterTable);         
//					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//					if(cellContent == nil) 
//					{
//						//CA("cellContent == nil");
//						break;
//					}
//					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//					if(!cellXMLReferenceData) {
//						//CA("!cellXMLReferenceData");
//						break;
//					}
//					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//					++iterTable;
//				//		int32 tagIndex = indexOfRowInTheHeaderRowPattern * totalNumberOfColumns + columnIndex;
//						
//				//		XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//						//Also note that, cell may be blank i.e doesn't contain any textTag.
//						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							continue;
//						}
//						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//						{
//							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//							//Check if the cell is blank.
//							if(cellTextXMLElementPtr == NULL)
//							{
//							continue;
//							}
//
//							//Get all the elementID and languageID from the cellTextXMLElement
//							//Note ITagReader plugin methods are not used.
//							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//							//check whether the tag specifies ProductCopyAttributes.
//							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//							PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//							PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));///11/05/07
//							PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//							PMString strDataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
//
//	
//							int32 elementID = strElementID.GetAsNumber();
//							int32 languageID = strLanguageID.GetAsNumber();
//													
//							int32 tagStartPos = -1;
//							int32 tagEndPos = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//							tagStartPos = tagStartPos + 1;
//							tagEndPos = tagEndPos -1;
//
//							PMString dispName("");
//							////if normal tag present in header row...
//							////then we are spraying the value related to the first item.
//							int32 itemID = FinalItemIds[0];
//
//							//if(strDataType == "4" && isCallFromTS)	//-------------
//							//{
//							//	//CA("Going to spray table Name");
//							//	CItemTableValue oTableValue;
//							//	VectorScreenTableInfoValue::iterator it;
//							//	for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
//							//	{				
//							//		oTableValue = *it;		
//							//		if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
//							//		{
//							//			dispName = oTableValue.getName();
//							//			attributeVal.Clear();
//							//			attributeVal.AppendNumber(oTableValue.getTableID());
//							//			cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
//							//			
//							//			attributeVal.Clear();
//							//			attributeVal.AppendNumber(oTableValue.getTableTypeID());
//							//			cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attributeVal));
//
//							//			//CA(dispName);
//							//			break;
//							//		}
//							//	}
//
//							//}
//							//else 
//							if(strTypeId == "-1")
//							{
//								//CA("strTypeId == -1");
//								int32 parentTypeID = strParentTypeID.GetAsNumber();
//
//								//CA("#################normal tag present in header row###############");
//								if(tableFlag == "1")
//								{
//									//CA("tableFlag == 1");
//									//added on 11July...serial blast day
//									//The following code will spray the table preset inside the table cell.
//									//for copy ID = -104
//									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									if(index== "3")// || index == "4")
//									{//Product ItemTable
//										PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//										TagStruct tagInfo;												
//										tagInfo.whichTab = index.GetAsNumber();
//										if(index == "3")
//											tagInfo.parentId = pNode.getPubId();
//										/*else if(index == "4")
//											tagInfo.parentId = pNode.getPBObjectID();*/
//
//										tagInfo.isTablePresent = tableFlag.GetAsNumber();
//										tagInfo.typeId = typeID.GetAsNumber();
//										tagInfo.sectionID = sectionid;
//																					
//										GetItemTableInTabbedTextForm(tagInfo,dispName);
//										cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
//										//CA(dispName);
//									}
//									//end added on 11July...serial blast day
//									else if(index == "4")
//									{//This is the Section level ItemTableTag and we have selected the product
//									//for spraying
//										makeTheTagImpotent(cellTextXMLElementPtr);										
//									}
//									
//								}
//
//									
//								else if(imgFlag == "1")
//								{							
//									XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//									UIDRef ref = cntentREF.GetUIDRef();
//									if(ref == UIDRef::gNull)
//									{
//										//CA("ref == UIDRef::gNull");
//										continue;
//									}
//
//									InterfacePtr<ITagReader> itagReader
//									((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//									if(!itagReader)
//										continue ;
//									
//									TagList tList=itagReader->getTagsFromBox(ref);
//									TagStruct tagInfoo;
//									int numTags=static_cast<int>(tList.size());
//
//									if(numTags<0)
//									{
//										continue;
//									}
//
//									tagInfoo=tList[0];
//
//									tagInfoo.parentId = pNode.getPubId();
//									tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//									tagInfoo.sectionID=CurrentSubSectionID;
//
//									IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//									if(!iDataSprayer)
//									{	
//										//CA("!iDtaSprayer");
//										continue;
//									}
//
//									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//									
//									PMString attrVal;
//									attrVal.AppendNumber(itemID);
//									cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//										
//									for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//									{
//										tList[tagIndex].tagPtr->Release();
//									}
//									continue;
//								}
//								else if(tableFlag == "0")
//								{
//									//CA("tableFlag == 0 AAA");
//									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									if(index =="4")
//									{//Item
//										//CA("index == 4 ");
//										//added on 22Sept..EventPrice addition.												
//										/*if(strElementID == "-701")
//										{
//											PMString  attributeIDfromNotes = "-1";
//											bool8 isNotesValid = getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
//											cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//											elementID = attributeIDfromNotes.GetAsNumber();
//										}*/
//										if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) != WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) != WideString("-101"))
//											itemID = pNode.getPubId();
//
//										//if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//										//{
//										//	TagStruct tagInfo;
//										//	tagInfo.elementId = elementID;
//										//	tagInfo.tagPtr = cellTextXMLElementPtr;
//										//	tagInfo.typeId = itemID;
//										//	tagInfo.parentTypeID = parentTypeID;//added by Tushar on 27/12/06
//										//	handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
//										//}
//										////ended on 22Sept..EventPrice addition.
//										//else if(elementID == -803)  // For 'Letter Key'
//										//{
//										//	dispName = "a" ;			// For First Item										
//										//}
//										//else if((isComponentAttributePresent == 1) && (elementID == -805) )  // For 'Quantity'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][0]);
//										//}
//										//else if((isComponentAttributePresent == 1) && (elementID == -806) )  // For 'Availability'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][1]);			
//										//}
//										//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][0]);			
//										//}
//										//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][1]);			
//										//}
//										//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][2]);			
//										//}
//										//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][3]);			
//										//}
//										//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
//										//{
//										//	dispName = (Kit_vec_tablerows[0][4]);			
//										//}
//										//else if((isComponentAttributePresent == 3) && (elementID == -812) )  // For 'Quantity'
//										//{
//										//	dispName = (Accessory_vec_tablerows[0][0]);
//										//}
//										//else if((isComponentAttributePresent == 3) && (elementID == -813) )  // For 'Required'
//										//{
//										//	dispName = (Accessory_vec_tablerows[0][1]);
//										//}
//										//else if((isComponentAttributePresent == 3) && (elementID == -814) )  // For 'Comments'
//										//{
//										//	dispName = (Accessory_vec_tablerows[0][2]);
//										//}
//										//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[0][4]) == "N"))
//										//{
//										//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
//										//}
//										//else if(elementID == -827)  // For 'Number Key'
//										//{
//										//	dispName = "1" ;
//										//}
//										//else
//										{
//											//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);										
//											dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);  // getting single item attributye data at a time.
//										}
//										
//										PMString attrVal;
//										attrVal.AppendNumber(itemID);
//										if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) == WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) == WideString("-101"))
//										{
//											//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//	 										cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//										}
//										else
//										{
//											PMString attrVal("");
//											attrVal.AppendNumber(pNode.getPubId());
//											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//										}
//									}
//									else if( index == "3")
//									{//Product
//										
//										dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID,kTrue);
//										PMString attrVal;
//										attrVal.AppendNumber(pNode.getTypeId());													
//										cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//										attrVal.Clear();
//
//										attrVal.AppendNumber(pNode.getPubId());
//										cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//									}
//								}								
//							}
//							else if(imgFlag == "1")
//							{							
//								XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//								UIDRef ref = cntentREF.GetUIDRef();
//								if(ref == UIDRef::gNull)
//								{
//									//CA("ref == UIDRef::gNull");
//									continue;
//								}
//
//								InterfacePtr<ITagReader> itagReader
//								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//								if(!itagReader)
//									continue ;
//								
//								TagList tList=itagReader->getTagsFromBox(ref);
//								TagStruct tagInfoo;
//								int numTags=static_cast<int>(tList.size());
//
//								if(numTags<0)
//								{
//									continue;
//								}
//
//								tagInfoo=tList[0];
//
//								tagInfoo.parentId = pNode.getPubId();
//								tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//								tagInfoo.sectionID=CurrentSubSectionID;
//
//								IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//								if(!iDataSprayer)
//								{	
//									CA("!iDtaSprayer");
//									continue;
//								}
//
//								iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//								
//								PMString attrVal;
//								attrVal.AppendNumber(itemID);
//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//								
//								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//								{
//									tList[tagIndex].tagPtr->Release();
//								}
//								
//								continue;
//							}
//							else
//							{							
//								if(tableFlag == "1")
//								{					
//									//CA("inside cell table is present");
//								//added on 11July...the serial blast day
//									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									if(index== "3")
//									{
//										cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
//										do
//										{
//											PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//		//CA(strTypeID);
//											int32 typeId = strTypeID.GetAsNumber();
//											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//											if(typeValObj==NULL)
//											{
//												ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
//												break;
//											}
//
//											VectorTypeInfoValue::iterator it1;
//
//											bool16 typeIDFound = kFalse;
//											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//											{	
//												if(typeId == it1->getType_id())
//													{
//														typeIDFound = kTrue;
//														break;
//													}			
//											}
//											if(typeIDFound)
//												dispName = it1->getName();
//
//											if(typeValObj)
//												delete typeValObj;
//										}while(kFalse);
//												
//									}
//									//This is a special case.The tag is of product but the Item is
//									//selected.So make this tag unproductive for refresh.(Impotent)
//									else if(index == "4")
//									{
//										makeTheTagImpotent(cellTextXMLElementPtr);
//										
//									}
//											
//											
//								}
//								else
//								{
//						
//									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									if(index== "3")
//									{//For product
//										CElementModel  cElementModelObj;
//										bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
//										if(result)
//											dispName = cElementModelObj.getDisplay_name();
//										PMString attrVal;
//										attrVal.AppendNumber(pNode.getTypeId());
//										cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//																			
//										cellTextXMLElementPtr->SetAttributeValue(WideString("tableFlag"),WideString("-13"));
//									}
//									else if(index == "4")
//									{
//										//following code added by Tushar on 27/12/06
//										if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
//										{
//											PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//											int32 parentTypeID = strParentTypeID.GetAsNumber();
//											
//											VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//											if(TypeInfoVectorPtr != NULL)
//											{
//												VectorTypeInfoValue::iterator it3;
//												int32 Type_id = -1;
//												PMString temp = "";
//												PMString name = "";
//												for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//												{
//													Type_id = it3->getType_id();
//													if(parentTypeID == Type_id)
//													{
//														temp = it3->getName();
//														if(elementID == -701)
//														{
//															dispName = temp;
//														}
//														else if(elementID == -702)
//														{
//															dispName = temp + " Suffix";
//														}
//													}														
//												}													
//											}
//											if(TypeInfoVectorPtr)
//												delete TypeInfoVectorPtr;
//										}
//										else if(elementID == -703)
//										{
//											dispName = "$Off";
//										}
//										else if(elementID == -704)
//										{
//											dispName = "%Off";
//										}
//										else if((elementID == -805) )  // For 'Quantity'
//										{
//											//CA("Quantity Header");
//											dispName = "Quantity";
//										}
//										else if((elementID == -806) )  // For 'Availability'
//										{
//											dispName = "Availability";			
//										}
//										else if(elementID == -807)
//										{									
//											dispName = "Cross-reference Type";
//										}
//										else if(elementID == -808)
//										{									
//											dispName = "Rating";
//										}
//										else if(elementID == -809)
//										{									
//											dispName = "Alternate";
//										}
//										else if(elementID == -810)
//										{									
//											dispName = "Comments";
//										}
//										else if(elementID == -811)
//										{									
//											dispName = "";
//										}
//										else if((elementID == -812) )  // For 'Quantity'
//										{
//											//CA("Quantity Header");
//											dispName = "Quantity";
//										}
//										else if((elementID == -813) )  // For 'Required'
//										{
//											//CA("Required Header");
//											dispName = "Required";
//										}
//										else if((elementID == -814) )  // For 'Comments'
//										{
//											//CA("Comments Header");
//											dispName = "Comments";
//										}
//
//										else
//											dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//
//										//Modification of XMLTagAttributes.
//										//modify the XMLTagAttribute "typeId" value to -2.
//										//We have assumed that there is atleast one stencil with HeaderAttirbute
//										//true .So assign for all the stencil in the header row , typeId == -2
//										cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
//										//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
//									}									
//								}
//							}
//							PMString textToInsert("");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(!iConverter)
//							{
//								textToInsert=dispName;					
//							}
//							else
//								textToInsert=iConverter->translateString(dispName);
//
//							//CA("textToInsert = " + textToInsert);
//							//Spray the Header data .We don't have to change the tag attributes
//							//as we have done it while copy and paste.
//							//WideString insertData(textToInsert);
//							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//							K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
//							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//						}
//			//		}//for..iterate through each column
//				}//for..iterate through each row of the headerRowPattern
//			
//				nonHeaderRowPatternIndex = 1;
//				rowPatternRepeatCount++;
//
//			}
//
//			if(tableHeaderPresent)
//			{//if 2 tableHeaderPresent 
//				//CA("tableHeaderPresent");
//			//First rowPattern  now is of header element.
//			//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
//			/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
//			s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//			CA(s);*/
//				for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
//				{//for..iterate through each row of the repeatedRowPattern
//					/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
//					s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//					CA(s);*/
//					for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
//					{//for..iterate through each column
//						int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
//						XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//						//Also note that, cell may be blank i.e doesn't contain any textTag.
//						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							continue;
//						}
//						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//						{
//							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//							//Check if the cell is blank.
//							if(cellTextXMLElementPtr == NULL)
//							{
//								continue;
//							}
//
//							//Get all the elementID and languageID from the cellTextXMLElement
//							//Note ITagReader plugin methods are not used.
//							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//							//check whether the tag specifies ProductCopyAttributes.
//							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//							
//							int32 elementID = strElementID.GetAsNumber();
//							int32 languageID = strLanguageID.GetAsNumber();
//							
//							int32 tagStartPos = -1;
//							int32 tagEndPos = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//							tagStartPos = tagStartPos + 1;
//							tagEndPos = tagEndPos -1;
//
//							PMString dispName(""); 
//							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//							if(index == "3")
//							{
//								makeTheTagImpotent(cellTextXMLElementPtr);
//							}					
//							else if(tableFlag == "1")
//							{	
//								cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
//								do
//								{
//									PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
////CA(strTypeID);
//									int32 typeId = strTypeID.GetAsNumber();
//									VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//									if(typeValObj==NULL)
//									{
//										ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
//										break;
//									}
//
//									VectorTypeInfoValue::iterator it1;
//
//									bool16 typeIDFound = kFalse;
//									for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//									{	
//										   if(typeId == it1->getType_id())
//											{
//												typeIDFound = kTrue;
//												break;
//											}			
//									}
//									if(typeIDFound)
//										dispName = it1->getName();
//
//									if(typeValObj)
//										delete typeValObj;
//								}while(kFalse);
//								//CA("You have to insert item item table here");
//								
//								//dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//								//Modification of XMLTagAttributes.
//								//modify the XMLTagAttribute "typeId" value to -2.
//								//We have assumed that there is atleast one stencil with HeaderAttirbute
//								//true .So assign for all the stencil in the header row , typeId == -2
//								//cellTextXMLElementPtr->SetAttributeValue("typeId","-2");
//								
//							}
//							else
//							{
//								//CA("tableFlag	!= 1");
//								//following code added by Tushar on 27/12/06
//								if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
//								{
//									PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//									int32 parentTypeID = strParentTypeID.GetAsNumber();
//									
//									VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//									if(TypeInfoVectorPtr != NULL)
//									{
//										VectorTypeInfoValue::iterator it3;
//										int32 Type_id = -1;
//										PMString temp = "";
//										PMString name = "";
//										for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//										{
//											Type_id = it3->getType_id();
//											if(parentTypeID == Type_id)
//											{
//												temp = it3->getName();
//												if(elementID == -701)
//												{
//													dispName = temp;
//												}
//												else if(elementID == -702)
//												{
//													dispName = temp + " Suffix";
//												}
//											}
//										}
//									}
//
//									if(TypeInfoVectorPtr)
//										delete TypeInfoVectorPtr;
//								}
//								else if(elementID == -703)
//								{
//									dispName = "$Off";
//								}
//								else if(elementID == -704)
//								{
//									dispName = "%Off";
//								}
//								else if(elementID == -401)  
//								{
//									//CA("Make");
//									dispName = "Make";
//								}
//								else if(elementID == -402)  
//								{
//									//CA("Model");
//									dispName = "Model";
//								}
//								else if(elementID == -403)  
//								{
//									//CA("Year");
//									dispName = "Year";
//								}
//								else if((elementID == -805) )  // For 'Quantity'
//								{
//									//CA("Quantity Header");
//									dispName = "Quantity";
//								}
//								else if((elementID == -806) )  // For 'Availability'
//								{
//									dispName = "Availability";			
//								}
//								else if(elementID == -807)
//								{									
//									dispName = "Cross-reference Type";
//								}
//								else if(elementID == -808)
//								{									
//									dispName = "Rating";
//								}
//								else if(elementID == -809)
//								{									
//									dispName = "Alternate";
//								}
//								else if(elementID == -810)
//								{									
//									dispName = "Comments";
//								}
//								else if(elementID == -811)
//								{									
//									dispName = "";
//								}
//								else if((elementID == -812) )  // For 'Quantity'
//								{
//									//CA("Quantity Header");
//									dispName = "Quantity";
//								}
//								else if((elementID == -813) )  // For 'Required'
//								{
//									//CA("Required Header");
//									dispName = "Required";
//								}
//								else if((elementID == -814) )  // For 'Comments'
//								{
//									//CA("Comments Header");
//									dispName = "Comments";
//								}
//								else
//								{
//									//CA("else");
//									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
//									//CA("dispName	:	"+dispName);
//
//								}									
//								//CA("dispName :" + dispName);
//								//Modification of XMLTagAttributes.
//								//modify the XMLTagAttribute "typeId" value to -2.
//								//We have assumed that there is atleast one stencil with HeaderAttirbute
//								//true .So assign for all the stencil in the header row , typeId == -2
//								cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
//								//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
//							}
//							
//							PMString textToInsert("");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(!iConverter)
//							{
//								textToInsert=dispName;					
//							}
//							else
//								textToInsert=iConverter->translateString(dispName);
//
//							//CA("textToInsert = " + textToInsert);
//							//Spray the Header data .We don't have to change the tag attributes
//							//as we have done it while copy and paste.
//							//WideString insertData(textToInsert);
//							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//							K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
//							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//						}
//					}//end for..iterate through each column.
//				}//end for..iterate through each row of repeatedRowPattern.
//				//All the subsequent rows will now spray the ItemValue.
//				nonHeaderRowPatternIndex = 1;					
//			}//end if 2 tableHeaderPresent
//
//			
//			GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);
//
//			int32 ColIndex = -1;
//			GridArea bodyArea_new = tableModel->GetBodyArea();
//			
//			ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
//			ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
//
//			bool16 isSameMake = kFalse;
//			bool16 isSameModel = kFalse;
//
//			int32 makeIndex = -1;
//			int32 modelIndex = -1;
//
//			int32 makeRepeatCnt = -1;
//			int32 modelRepeatCnt = -1;
//
//			int32 currentMakeModelCnt = -1;
//			int32 currentModelItemCnt = -1;
//			int32 currentModelItemIndex = -1;
//
//			int32 colIndexPerRow = -1;
//
//			vector<int32> itemIds;
//
//			Vec_CMakeModel::const_iterator itr;
//			Vec_CModelModel::const_iterator itr1;
//
//			CMakeModel *makeModelPtr = NULL;
//			CModelModel *modelModelPtr = NULL;
//
//			multimap<PMString, int32>sortItems;
//			multimap<PMString, int32>::iterator mapItr;
//			int32 ITEMID = -1;
//			int32 itemIDIndex = -1;
//
//			while (iterTable != endTable)
//			{
//				//CA("Iterating table");
//				GridAddress gridAddress = (*iterTable); 
//				if(tableHeaderPresent)
//					if(gridAddress <= headerRowGridAddress){
//						iterTable++;							
//						continue;
//					}
//
//				if(isSameMake == kFalse)
//				{
//					//CA("isSameMake == kFalse");
//					makeIndex++;
//
//					makeModelPtr = MMYTablePtr->vec_CMakeModelPtr->at(makeIndex);
//
//					currentMakeModelCnt = static_cast<int32>(makeModelPtr->vec_CModelModelPtr->size());
//
//					isSameMake = kTrue;
//				}
//
//				if(isSameModel == kFalse)
//				{
//					//CA("isSameModel == kFalse");
//					modelIndex++;
//					modelModelPtr = makeModelPtr->vec_CModelModelPtr->at(modelIndex);
//					
//					currentModelItemCnt = static_cast<int32>(modelModelPtr->vec_ItemYearPtr->size());
//					
//					itemIds.clear();
//					Vec_ItemYear::iterator itr2 = modelModelPtr->vec_ItemYearPtr->begin();
//					
//					for(;itr2 != modelModelPtr->vec_ItemYearPtr->end(); itr2++)
//					{
//						PMString itemIdStr("(*itr2)->item_Id = ");
//						itemIdStr.AppendNumber((*itr2)->item_Id);
//						//CA(itemIdStr);
//
//						itemIds.push_back((*itr2)->item_Id);
//					}
//
//					if(sort_by_attributeId != -1)
//					{
//						sortItems.clear();
//						vector<int32>::iterator itr3 = itemIds.begin();
//						for(;itr3 != itemIds.end(); itr3++)
//						{
//							PMString itemIdStr("*itr3 = ");
//							itemIdStr.AppendNumber(*itr3);
//							//CA(itemIdStr);
//							PMString sortAttributeValue = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(*itr3,sort_by_attributeId,languageId);
//							//CA("sortAttributeValue = " + sortAttributeValue);
//							sortItems.insert(multimap<PMString,int32>::value_type(sortAttributeValue,*itr3));
//						}
//					}
//					
//					currentModelItemIndex++;
//
//					isSameModel = kTrue;
//
//					mapItr = sortItems.begin();
//				}
//
//				InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//				if(cellContent == nil) 
//				{
//					break;
//				}
//				InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//				if(!cellXMLReferenceData) {
//					break;
//				}
//				XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//				++iterTable;
//
//				ColIndex++;
//				int32 rowIndex = ColIndex/bodyCellCount;
//				
//
//				colIndexPerRow++;	
//
//				if(itemIDIndex != rowIndex && mapItr != NULL)
//				{
//					ITEMID = mapItr->second;
//					mapItr++;
//					itemIDIndex = rowIndex;
//				}
//
//				//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());										
//				//Note that, cell may be blank i.e doesn't contain any textTag.
//				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//				{
//					//CA("continue");
//					continue;
//				}
//	
//				//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//				//so always take the 0th childTag of the Cell.
//				int32 cellChildCount = cellXMLElementPtr->GetChildCount();
//				for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount ;tagIndex1++)
//				{
//					XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//						
//					//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//					InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//					//Check if the cell is blank.
//					if(cellTextXMLElementPtr == NULL)
//					{	
//						continue;
//					}
//					
//					//Get all the elementID and languageID from the cellTextXMLElement
//					//Note ITagReader plugin methods are not used.
//					//CA(cellTextXMLElementPtr->GetTagString());
//
//					PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//					PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//					//check whether the tag specifies ProductCopyAttributes.
//					PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//					PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID")); //added by Tushar on 22/12/06
//					PMString imageFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//					/*CA("strParentTypeID =" + strParentTypeID);
//					CA("strElementID :" + strElementID);
//					CA("tableFlag :" + tableFlag);*/
//					
//					int32 elementID = strElementID.GetAsNumber();
//					int32 languageID = strLanguageID.GetAsNumber();
//					int32 itemID = ITEMID;//FinalItemIds[itemIDIndex];
//					int32 parentTypeID = strParentTypeID.GetAsNumber();	//added by Tushar on 22/12/06
//																 
//					int32 tagStartPos = -1;
//					int32 tagEndPos = -1;
//					Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//					tagStartPos = tagStartPos + 1;
//					tagEndPos = tagEndPos -1;
//				
//					PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
//					int32 isEventFieldVal = isEventFieldStr.GetAsNumber();
//
//					PMString dispName("");
//					PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//					if(index == "3")
//					{
//						makeTheTagImpotent(cellTextXMLElementPtr);
//					}
//					else if(tableFlag == "1")
//					{
//						//CA("tableFlag == 1");
//						
//						//The following code will spray the table preset inside the table cell.
//						//for copy ID = -104
//						
//						if(index == "4")
//						{//Item ItemTable
//							//CA("index == 4");
//
//							XMLContentReference xmlCntnRef = cellTextXMLElementPtr->GetContentReference();
//							InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());
//
//							bool16 isTablePresentInsideCell = xmlCntnRef.IsTable();
//							/////following functionallity is not yet completed 
//							//for section level item having items which contains their own item table. 
//							if(isTablePresentInsideCell)
//							{
//								//CA("isTablePresentInsideCell = kTrue");
//								//
//								//InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
//								//if(innerTableModel == NULL)
//								//{
//								//	CA("innerTableModel == NULL");
//								//	continue;
//								//}
//								//UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
//				
//								//XMLTagAttributeValue xmlTagAttrVal;
//								////collect all the attributes from the tag.
//								////typeId,LanguageID are important
//								//getAllXMLTagAttributeValues(cellTextXMLElementRef,xmlTagAttrVal);	
//
//								////CA("xmlTagAttrVal.typeId : " + xmlTagAttrVal.typeId);
//
//								///*if(xmlTagAttrVal.typeId.GetAsNumber() == -111)
//								//{
//								//	fillDataInCMedCustomTableInsideTable
//								//	(
//								//		cellTextXMLElementRef,
//								//		innerTableModelUIDRef,
//								//		pNode
//
//								//	);
//								//}
//								//else
//								//{*/
//								//	FillDataTableForItemOrProduct
//								//	(
//								//		cellTextXMLElementRef,
//								//		innerTableModelUIDRef,
//								//		kFalse
//								//	);
//								///*}*/
//
//								//UIDList itemList(slugInfo.curBoxUIDRef);
//								//UIDList processedItems;
//								//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//								//ErrorCode status =  ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//
//								//tableflag++;
//								//continue;
//							}
//
//							PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//							TagStruct tagInfo;												
//							tagInfo.whichTab = index.GetAsNumber();
//							tagInfo.parentId = pNode.getPBObjectID();
//
//							tagInfo.isTablePresent = tableFlag.GetAsNumber();
//							tagInfo.typeId = typeID.GetAsNumber();
//							tagInfo.sectionID = sectionid;
//							
//								
//							GetItemTableInTabbedTextForm(tagInfo,dispName);
//							/*UIDRef tempBox = boxUIDRef;
//							sprayTableInsideTableCell(tempBox,tagInfo);*/
//							cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
//
//							PMString attrVal;
//							attrVal.Clear();
//							attrVal.AppendNumber(pNode.getPubId());
//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//							attrVal.Clear();
//
//							attrVal.AppendNumber(pNode.getPBObjectID());
//							cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
//							//cellTextXMLElementPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));
//
//							//CA(dispName);
//						}
//						//end added on 11July...serial blast day
//						/*else if(index == "4")
//						{//Item ItemTable
//
//							CA("You have to add ItemItemTable here");													
//							dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID, languageID, kFalse );
//						
//							//Modify the XMLTagAttribute "typeID" .
//							//Set its value to itemID;
//							PMString attrVal;
//							attrVal.AppendNumber(itemID);
//							cellTextXMLElementPtr->SetAttributeValue("typeId",attrVal);								
//						}
//						*/
//					}
//					else if(imageFlag == "1")
//					{
//						XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//						UIDRef ref = cntentREF.GetUIDRef();
//						if(ref == UIDRef::gNull)
//						{
//							////CA("if(ref == UIDRef::gNull)");
//							int32 StartPos1 = -1;
//							int32 EndPos1 = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									
//
//							InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
//								textModel->QueryStrand(kOwnedItemStrandBoss, 
//								IItemStrand::kDefaultIID)));
//							if (itemStrand == nil) {
//								//CA("invalid itemStrand");
//								continue;
//							}
//							//CA("going for Owened items");
//							OwnedItemDataList ownedList;
//							itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
//							int32 count = ownedList.size();
//							
//						/*	PMString ASD("count : ");
//							ASD.AppendNumber(count);
//							CA(ASD);*/
//
//							if(count > 0)
//							{
//								//CA(" count > 0 ");
//								UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
//								if(newRef == UIDRef::gNull)
//								{
//									//CA("ref == UIDRef::gNull....2");
//									continue;
//								}
//								ref = newRef;
//							}else
//								continue;
//						}
//
//						InterfacePtr<ITagReader> itagReader
//						((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//						if(!itagReader)
//							continue ;
//						
//						TagList tList=itagReader->getTagsFromBox(ref);
//						TagStruct tagInfoo;
//						int numTags=static_cast<int>(tList.size());
//
//						if(numTags<=0)
//						{
//							continue;
//						}
//
//						tagInfoo=tList[0];
//
//						tagInfoo.parentId = itemID;//pNode.getPubId();
//						tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//						tagInfoo.sectionID=CurrentSubSectionID;
//
//						IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//						if(!iDataSprayer)
//						{	
//							//CA("!iDtaSprayer");
//							continue;
//						}
//
//						if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
//						|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
//						|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
//						|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
//						{
//							iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
//						}
//						else if(tagInfoo.elementId > 0)
//						{
//							isInlineImage = kTrue;
//							//CA("Calling fillPVAndMPVImageInBox 1");
//							ptrIAppFramework->clearAllStaticObjects();
//							iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo,itemID,kTrue);
//							iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
//												
//						}
//						else
//						{
//							iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//						}
//
//						
//					
//						PMString attrVal;
//						attrVal.AppendNumber(itemID);													
//						cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//						attrVal.Clear();
//						
//						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//						{
//							tList[tagIndex].tagPtr->Release();
//						}
//						continue;
//					}
//					else if(tableFlag == "0")
//					{
//						//CA("tableFlag == 0");
//						PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//						PMString colNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
//						PMString ischildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
//						if(index =="4" && ischildTag == "1")
//						{
//							//CA("index == 4 && ischildTag == 1");
//							if(elementID == -803)  // For 'Letter Key'
//							{
//								/*dispName.Clear();
//								if(itemIDIndex < 26)
//									dispName.Append(AlphabetArray[itemIDIndex]);
//								else
//									dispName = "a" ;*/	
//								dispName.Clear();
//								int wholeNo =  itemIDIndex / 26;
//								int remainder = itemIDIndex % 26;								
//
//								if(wholeNo == 0)
//								{// will print 'a' or 'b'... 'z'  upto 26 item ids
//									dispName.Append(AlphabetArray[remainder]);
//								}
//								else  if(wholeNo <= 26)
//								{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//									dispName.Append(AlphabetArray[wholeNo-1]);	
//									dispName.Append(AlphabetArray[remainder]);	
//								}
//								else if(wholeNo <= 52)
//								{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//									dispName.Append(AlphabetArray[0]);
//									dispName.Append(AlphabetArray[wholeNo -26 -1]);	
//									dispName.Append(AlphabetArray[remainder]);										
//								}
//							}
//							//else if((isComponentAttributePresent == 1) && (elementID == -805))
//							//{
//							//	//CA("Here 1");
//							//	dispName.Clear();			
//							//	//CA(Kit_vec_tablerows[0][0]);
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);	
//							//}
//							//else if((isComponentAttributePresent == 1) && (elementID == -806))
//							//{	//CA("Here 2");									
//							//	dispName.Clear();	
//							//	//CA(Kit_vec_tablerows[0][1]);
//							//	dispName = ((Kit_vec_tablerows[itemIDIndex][1]));	
//							//}
//							//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
//							//{
//							//	dispName.Clear();	
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);			
//							//}
//							//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
//							//{
//							//	dispName.Clear();	
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][1]);			
//							//}
//							//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
//							//{
//							//	dispName.Clear();	
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][2]);			
//							//}
//							//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
//							//{
//							//	dispName.Clear();	
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][3]);			
//							//}
//							//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
//							//{
//							//	dispName.Clear();	
//							//	dispName = (Kit_vec_tablerows[itemIDIndex][4]);			
//							//	//CA(dispName);
//							//}
//							//else if((isComponentAttributePresent == 3) && (elementID == -812))
//							//{	//CA("Here 3");									
//							//	dispName.Clear();	
//							//	//CA(Accessory_vec_tablerows[0][1]);
//							//	dispName = ((Accessory_vec_tablerows[itemIDIndex][0]));	
//							//}
//							//else if((isComponentAttributePresent == 3) && (elementID == -813))
//							//{	//CA("Here 4");									
//							//	dispName.Clear();	
//							//	//CA(Accessory_vec_tablerows[0][1]);
//							//	dispName = ((Accessory_vec_tablerows[itemIDIndex][1]));	
//							//}
//							//else if((isComponentAttributePresent == 3) && (elementID == -814))
//							//{	//CA("Here 5");									
//							//	dispName.Clear();	
//							//	//CA(Accessory_vec_tablerows[0][1]);
//							//	dispName = ((Accessory_vec_tablerows[itemIDIndex][2]));	
//							//}
//							//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[itemIDIndex][4]) == "N"))
//							//{
//							//	//CA(" non catalog items");
//							//	dispName.Clear();	
//							//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
//							//}
//							else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
//							{
//								VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),elementID,CurrentSubSectionID);
//								if(vecPtr != NULL){
//									if(vecPtr->size()> 0){
//										dispName = vecPtr->at(0).getObjectValue();	
//									}
//								}
//							}
//							//else if(elementID == -401  || elementID == -402  ||  elementID == -403)		//----For Make, Mode, Year . 
//							//{
//							//	dispName = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemID,elementID, languageID);
//							//}
//							else if(elementID == -401)		//----For Make, Mode, Year . 
//							{
//								dispName = makeModelPtr->getMake_Name();
//							}
//							else if(elementID == -402)		//----For Make, Mode, Year . 
//							{
//								dispName = modelModelPtr->getModel_Name();
//							}
//							else if(elementID == -403)		//----For Make, Mode, Year . 
//							{
//								Vec_ItemYear::iterator itemYearItr = modelModelPtr->vec_ItemYearPtr->begin();
//								for(; itemYearItr != modelModelPtr->vec_ItemYearPtr->end(); itemYearItr++)
//								{
//									if(itemID == (*itemYearItr)->item_Id)
//									{
//										dispName = (*itemYearItr)->year;
//									}
//
//								}
//								
//							}
//							else if(elementID == -827)  //**** Number Keys
//							{
//								dispName.Clear();
//								dispName.Append(itemIDIndex+1);
//							}
//							else 
//							{
//								//CA("catalog Items");
//								dispName.Clear();	
//								//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);
//								dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);
//							}
//							PMString attrVal;
//							attrVal.AppendNumber(itemID);
//							//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//							cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//							
//							attrVal.Clear();
//							attrVal.AppendNumber(pNode.getPubId());														
//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//							
//							attrVal.Clear();
//							attrVal.AppendNumber(pNode.getPBObjectID());
//							cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));	
//						}
//						else if( index == "4")
//                        {//SectionLevelItem
//							//CA("index == 4");
//						////added on 22Sept..EventPrice addition.
//							//if(strElementID == "-701")
//							//{
//							//	PMString  attributeIDfromNotes = "-1";
//							//	bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
//							//	cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//							//	elementID = attributeIDfromNotes.GetAsNumber();
//							//}
//							////ended on 22Sept..EventPrice addition.	
//							if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//							{
//								TagStruct tagInfo;
//								tagInfo.elementId = elementID;
//								tagInfo.tagPtr = cellTextXMLElementPtr;
//								tagInfo.typeId = pNode.getPubId();
//								handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);									
//								//PMString  attributeIDfromNotes = "-1";
//								//bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
//								//cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//								//elementID = attributeIDfromNotes.GetAsNumber();
//							}
//							else if(elementID == -803)  // For 'Letter Key'
//							{
//								/*dispName.Clear();
//								if(itemIDIndex < 26)
//									dispName.Append(AlphabetArray[itemIDIndex]);
//								else
//									dispName = "a" ;*/		
//								dispName.Clear();
//								int wholeNo =  itemIDIndex / 26;
//								int remainder = itemIDIndex % 26;								
//
//								if(wholeNo == 0)
//								{// will print 'a' or 'b'... 'z'  upto 26 item ids
//									dispName.Append(AlphabetArray[remainder]);
//								}
//								else  if(wholeNo <= 26)
//								{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//									dispName.Append(AlphabetArray[wholeNo-1]);	
//									dispName.Append(AlphabetArray[remainder]);	
//								}
//								else if(wholeNo <= 52)
//								{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//									dispName.Append(AlphabetArray[0]);
//									dispName.Append(AlphabetArray[wholeNo -26 -1]);	
//									dispName.Append(AlphabetArray[remainder]);										
//								}
//							}	
//							else
//								//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID,kTrue);
//								dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID);
//							if(elementID == -827)  // For 'Number Key'
//							{
//								dispName.Clear();
//								dispName.Append(itemIDIndex+1);
//							}
//							
//							PMString attrVal;
//							attrVal.AppendNumber(pNode.getPubId());	
//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//							//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//							//cellTextXMLElementPtr->SetAttributeValue(WideString("childTag"),WideString("1"));
//
//							cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//								
//							
//							attrVal.Clear();
//							attrVal.AppendNumber(pNode.getPBObjectID());
//							cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
//				
//						}
//
//					}
//					cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("-1"));
//					/*if(isCallFromTS ){
//						PMString temp("");
//						temp.AppendNumber(tableSourceInfoValueObj->getVec_Table_ID().at(0));
//						cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(temp));								
//					}*/
//					PMString textToInsert("");
//					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//					if(!iConverter)
//					{
//						textToInsert=dispName;					
//					}
//					else
//						textToInsert=iConverter->translateString(dispName);
//					//CA("textToInsert = " + textToInsert);
//					//Spray the Header data .We don't have to change the tag attributes
//					//as we have done it while copy and paste.	
//					//WideString insertData(textToInsert);
//					//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//					K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
//					ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//				}
//				
//				if(colIndexPerRow == bodyCellCount - 1)
//				{
//					//CA("colIndexPerRow == bodyCellCount - 1");
//					colIndexPerRow = -1;
//					currentModelItemIndex++;
//					if(currentModelItemIndex == currentModelItemCnt)
//					{
//						//CA("currentModelItemIndex == currentModelItemCnt");
//						if(modelIndex < currentMakeModelCnt - 1)
//						{
//							//CA("modelIndex < currentMakeModelCnt - 1");
//							isSameModel = kFalse;
//							currentModelItemCnt = -1;
//							currentModelItemIndex = -1;
//
//						}else
//						{
//							//CA("else");
//							isSameMake = kFalse;
//							isSameModel = kFalse;
//							modelIndex = -1;
//							currentMakeModelCnt = -1;
//							currentModelItemCnt = -1;
//							currentModelItemIndex = -1;
//						}
//					}
//				}	
//			}//end for..iterate through each column		
//		}
//		errcode = kTrue;
//	}while(0);
//	return errcode;
//}
//
//
//int32 TableUtility::getMMYTableRows(const CMMYTable* MMYTablePtr)
//{
//	int32 count = 0;
//
//	Vec_CMakeModel::const_iterator itr = MMYTablePtr->vec_CMakeModelPtr->begin();
//	Vec_CModelModel::const_iterator itr1;
//	for(; itr != MMYTablePtr->vec_CMakeModelPtr->end(); itr++)
//	{
//		itr1 = (*itr)->vec_CModelModelPtr->begin();
//		for(;itr1 != (*itr)->vec_CModelModelPtr->end(); itr1++)
//		{
//			int32 size = static_cast<int32>((*itr1)->vec_ItemYearPtr->size());
//			count = count + size;
//		}
//	}
//
//	return count;
//}


ErrorCode TableUtility::SprayMMYCustomTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
	//CA("TableUtility::SprayMMYCustomTable");
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	double languageId = -1;
	
//	do{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//		 	
//		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//		*/
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//
//		if (textFrameUID == kInvalidUID)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		//if (textFrame == NULL)
//		//{
//		//	break;//breaks the do{}while(kFalse)
//		//}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA(graphicFrameHierarchy);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!graphicFrameHierarchy");
//			break;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA(multiColumnItemHierarchy);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA(!multiColumnItemTextFrame);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
//			//CA("Its Not MultiColumn");
//			break;
//		}
//		
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA(“!frameItemHierarchy”);
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!frameItemHierarchy");
//			break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		textFrame(frameItemHierarchy, UseDefaultIID());
//		if (!textFrame) {
//			//CA("!!!ITextFrameColumn");
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!textFrame");
//			break;
//		}
//		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//		if (textModel == NULL)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		
//		UIDRef txtModelUIDRef = ::GetUIDRef(textModel);
//
//		
//		
//
//
//		
//		int32 len = textModel->GetPrimaryStoryThreadSpan ();
//		//CA_NUM("textModel->GetPrimaryStoryThreadSpan  = " , len);
//		
//		TextIndex threadStart;
//		int32 threadSpan;
//		TextIndex tableStartPosition = 0;
//		TextIndex tableEndPosition = 0;
//		TextIndex position = 0;
//
//
//		InterfacePtr<ITextStoryThread> storyThread(textModel->QueryStoryThread(position, &threadStart, &threadSpan));
//		if(storyThread == NULL)
//		{
//			//CA("storyThread == NULL");
//			return errcode; 
//		}
//
//		//CA_NUM("threadStart = " , threadStart);
//		//CA_NUM("threadSpan = " , threadSpan);
//		
//
//		bool16 findTable = InspectChars(textModel,threadStart, threadSpan - 1,tableStartPosition,tableEndPosition);
//	
//		
//		int32 startPos = tableStartPosition;
//		int32 endPos = startPos+tableEndPosition+1;
//	
//		//CA_NUM("startPos = " , startPos);
//		//CA_NUM("endPos = " , endPos);
//
//		int32 length = endPos - startPos;
//		int32 startPasteIndex = len - 1;
//		TextIndex destEnd = startPasteIndex + 0; 
//
//		int32 lineStartCharIndex = startPos;
//		int32 lineEndCharIndex = endPos;
//
//		
//
//		int32 tableTypeId = -1;		
//		int32 sort_by_attributeId = -1;
//		bool16 HeaderRowPresent = kFalse;
//		bool16 tableHeaderPresent = kFalse;
//
//		int32 sectionid = -1;
//		if(global_project_level == 3)
//			sectionid = CurrentSubSectionID;
//		if(global_project_level == 2)
//			sectionid = CurrentSectionID;
//	
//
//		for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
//		{//start for tagIndex = 0
//			XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
//			//This is a tag attached to the cell.
//			//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//			//Get the text tag attached to the text inside the cell.
//			//We are providing only one texttag inside cell.
//			if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//			{
//				continue;
//			}
//			for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//			{
//				XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//				//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//				InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//				//The cell may be blank. i.e doesn't have any text tag inside it.
//				if(cellTextTagPtr == NULL)
//				{
//					continue;
//				}
//				
//				if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
//					 && HeaderRowPresent == kFalse)
//				{
//					//CA("header present");
//					tableHeaderPresent = kTrue;
//				}
//
//				PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
//				tableTypeId = fld_1.GetAsNumber();
//				
//				if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
//				{		
//					if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
//					{							
//						PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//						sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
//					}
//				}
//									
//				
//				//parentID
//				PMString attributeVal("");
//				attributeVal.AppendNumber(pNode.getPubId());
//				cellTextTagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
//				attributeVal.Clear();
//
//				PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//				int32 ID = id.GetAsNumber();
//										
//				//parentTypeID
//				if(ID != -701 && ID != -702 && ID != -703 && ID != -704)
//				{
//					attributeVal.AppendNumber(pNode.getTypeId());
//					cellTextTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
//					attributeVal.Clear();
//				}
//
//				//sectionID
//				attributeVal.AppendNumber(sectionid);
//				cellTextTagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
//				
//				attributeVal.Clear();
//				attributeVal.AppendNumber(slugInfo.tableType);
//				cellTextTagPtr->SetAttributeValue(WideString("tableType"),WideString(attributeVal));
//				
//				attributeVal.Clear();
//				attributeVal.AppendNumber(pNode.getPBObjectID());
//				cellTextTagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));					
//			}
//		}//end for tagIndex = 0 
//
//
//
//		
//
//		vector<int32> FinalItemIds;
//		if(pNode.getIsProduct() == 1)	
//		{
//			VectorScreenTableInfoPtr tableInfo = NULL;
//			if(pNode.getIsONEsource())
//			{
//				// For ONEsource
//				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
//			}else
//			{	
//				// for publication
//				tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), slugInfo.languageID, kFalse);
//			}
//			if(!tableInfo)
//			{
//				break;//breaks the for..tableIndex
//			}
//			if(tableInfo->size()==0)
//			{ 
//				makeTagInsideCellImpotent(slugInfo.tagPtr); //added by Tushar on 23/12/06
//				break;//breaks the for..tableIndex
//			}
//					
//			CItemTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			vector<int32> vec_items;
//			FinalItemIds.clear();
//
//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			{				
//				oTableValue = *it;				
//				vec_items = oTableValue.getItemIds();
//			
//				if(tableTypeId == -1)	//---------
//				{
//					if(FinalItemIds.size() == 0)
//					{	//CA("FinalItemIds.size() == 0");
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
//						{	
//							bool16 Flag = kFalse;
//							for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
//							{
//								if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
//						}
//
//					}
//				}
//				else
//				{
//					 
//					if(tableTypeId  !=  oTableValue.getTableTypeID())
//						continue;
//
//					if(FinalItemIds.size() == 0)
//					{	//CA("FinalItemIds.size() == 0");
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
//						{	
//							bool16 Flag = kFalse;
//							for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
//							{
//								if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
//						}
//
//					}
//
//				}
//			}
//
//			if(tableInfo)
//			{
//				tableInfo->clear();
//				delete tableInfo;
//			}
//
//		}
//		else if(pNode.getIsProduct() == 0)	
//		{
//			VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPBObjectID());
//			if(!tableInfo)
//			{
//				//CA("tableinfo is NULL");
//				makeTagInsideCellImpotent(slugInfo.tagPtr); 
//				break;
//			}
//			
//			if(tableInfo->size()==0)
//			{ //CA("tableinfo size==0");
//				makeTagInsideCellImpotent(slugInfo.tagPtr); 
//				break;
//			}
//			CItemTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			vector<int32> vec_items;
//			
//			FinalItemIds.clear();
//
//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			{				
//				oTableValue = *it;				
//				vec_items = oTableValue.getItemIds();
//			
//				if(tableTypeId == -1)	//--------
//				{
//					if(FinalItemIds.size() == 0)
//					{
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 i=0; i<vec_items.size(); i++)
//						{	bool16 Flag = kFalse;
//							for(int32 j=0; j<FinalItemIds.size(); j++)
//							{
//								if(vec_items[i] == FinalItemIds[j])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[i]);
//						}
//					}
//				}
//				else
//				{
//					if(tableTypeId != oTableValue.getTableTypeID())
//						continue;
//
//					if(FinalItemIds.size() == 0)
//					{
//						FinalItemIds = vec_items;
//					}
//					else
//					{
//						for(int32 i=0; i<vec_items.size(); i++)
//						{	bool16 Flag = kFalse;
//							for(int32 j=0; j<FinalItemIds.size(); j++)
//							{
//								if(vec_items[i] == FinalItemIds[j])
//								{
//									Flag = kTrue;
//									break;
//								}				
//							}
//							if(!Flag)
//								FinalItemIds.push_back(vec_items[i]);
//						}
//					}
//				}				
//			}			
//		
//			if(tableInfo)
//				delete tableInfo;
//		}
//		if(static_cast<int32>(FinalItemIds.size()) == 0)
//			break;
//		/*PMString FinaiItemIdsStr("FinalItemIds size = ");
//			FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
//			CA(FinaiItemIdsStr);*/
//
//	//Change the attributes of the tableTag.
//		//ID..The older ID was -1 which is same as elementID of Product_Number.
//		//Change it to someother value so that product_number and table id doesn't conflict.
//		//we are already using ID = -101 for item_table_in_tabbed_text format.
//		//so ID = -102
//	
//		slugInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-102"));//--------------
//
//		//parentID
//		PMString attributeVal("");
//		attributeVal.AppendNumber(pNode.getPubId());
//		slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
//		attributeVal.Clear();
//		//parentTypeID
//		attributeVal.AppendNumber(pNode.getTypeId());
//		slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
//		attributeVal.Clear();
//		//sectionID
//		attributeVal.AppendNumber(sectionid);
//		slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
//		//PBObjectID
//		attributeVal.Clear();
//		attributeVal.AppendNumber(pNode.getPBObjectID());
//		slugInfo.tagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));
//		//tableTypeId
//		attributeVal.Clear();
//		attributeVal.AppendNumber(tableTypeId);
//		slugInfo.tagPtr->SetAttributeValue(WideString("field1"),WideString(attributeVal));
//		//index		
//		if(pNode.getIsProduct() == 1)
//		{
//			slugInfo.tagPtr->SetAttributeValue(WideString("index"),WideString("3"));
//		}
//		if(pNode.getIsProduct() == 0)
//		{
//			slugInfo.tagPtr->SetAttributeValue(WideString("index"),WideString("4"));
//		}
//		
//	//getMMYTableByItemIds
//		CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);
//
//	
//	//copyPaste MMY tables for no of Make Times
//		int32 totalMakeFound = static_cast<int32>(MMYTablePtr->vec_CMakeModelPtr->size());
//		int32 numberOfTimesTheTableToBeCopied = totalMakeFound - 1;
//		for(int32 tableIndex=0;tableIndex<numberOfTimesTheTableToBeCopied;tableIndex++)
//		{
//			do
//			{
//				// Create kCopyStoryRangeCmdBoss.
//				InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
//				if (copyStoryRangeCmd == nil) {//CA("copyStoryRangeCmd == nil");
//					break;
//				}
//
//				// Refer the command to the source story and range to be copied.
//				InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
//				if (sourceUIDData == nil) {//CA("sourceUIDData == nil");
//					break;
//				}
//				
//				sourceUIDData->Set(txtModelUIDRef);
//				InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
//				if (sourceRangeData == nil) {//CA("sourceRangeData == nil");
//					break;
//				}
//				
//				sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);
//
//				// Refer the command to the destination story and the range to be replaced.
//				UIDList itemList(txtModelUIDRef);
//				copyStoryRangeCmd->SetItemList(itemList);
//				
//				InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
//
//				destRangeData->Set(startPasteIndex, destEnd );
//
//				// Process CopyStoryRangeCmd
//				ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
//
//			}while(kFalse);
//		}
//
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==NULL)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//
//		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::totalNumberOfTablesInsideTextFrame < 0");																						
//			break;//breaks the do{}while(kFalse)
//		}
//
//		PMString totalNumberOfTablesInsideTextFrameStr = "";
//		totalNumberOfTablesInsideTextFrameStr.AppendNumber(totalNumberOfTablesInsideTextFrame);
//		//CA("totalNumberOfTablesInsideTextFrame = " + totalNumberOfTablesInsideTextFrameStr);
//
//		bool16 isSameMake = kFalse;
//		bool16 isSameModel = kFalse;
//
//		int32 makeIndex = -1;
//		int32 modelIndex = -1;
//
//		int32 makeRepeatCnt = -1;
//		int32 modelRepeatCnt = -1;
//
//		int32 currentMakeModelCnt = -1;
//		int32 currentModelItemCnt = -1;
//		int32 currentModelItemIndex = -1;
//
//		int32 colIndexPerRow = -1;
//
//		vector<int32> itemIds;
//
//		Vec_CMakeModel::const_iterator itr;
//		Vec_CModelModel::const_iterator itr1;
//
//		CMakeModel *makeModelPtr = NULL;
//		CModelModel *modelModelPtr = NULL;
//
//		multimap<double, int32>sortItems;
//		multimap<double, int32>::iterator mapItr;
//		int32 ITEMID = -1;
//		int32 itemIDIndex = -1;
//
//
//		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//		{//for..tableIndex
//
//			bool16 isMakeIDAttachedToTableTag = kFalse;
//			int32 getTableModelAtIndex = 0;
//			if(tableIndex == 0)
//			{
//				getTableModelAtIndex = 0;
//			}
//			else
//			{
//				getTableModelAtIndex = totalNumberOfTablesInsideTextFrame - tableIndex;
//				
//			}
//
//			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(getTableModelAtIndex));
//			if(tableModel == NULL)
//			{
//				//CA("continue");
//				continue;//continues the for..tableIndex
//			}
//	
//			//bool16 HeaderRowPresent = kFalse;
//
//			RowRange rowRange(0,0);
//			rowRange = tableModel->GetHeaderRows();
//			int32 headerStart = rowRange.start;
//			int32 headerCount = rowRange.count;
//			
//			PMString HeaderStart = "";
//			HeaderStart.AppendNumber(headerStart);
//			PMString HeaderCount = "";
//			HeaderCount.AppendNumber(headerCount);
//			//CA("HeaderStart = " + HeaderStart + ", HeaderCount =  " + HeaderCount);
//
//			if(headerCount != 0)
//			{
//				//CA("HeaderPresent");
//				HeaderRowPresent = kTrue;
//			}
//			
//			
//			UIDRef tableRef(::GetUIDRef(tableModel)); 
//			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//			//IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
//
//			languageId = slugInfo.languageID; 
//
//			//XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();			
//			//UIDRef ContentRef = contentRef.GetUIDRef();
//			//if( ContentRef != tableRef)
//			//{
//			//	CA("ContentRef != tableRef");
//			//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//			//	continue; //continues the for..tableIndex
//			//}
//
//			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
//					
//			int32 totalNumberOfRowsBeforeRowInsertion = 0;
//			if(HeaderRowPresent)
//				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
//			else
//				totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
//
//			/*PMString numberOfRows = "";
//			numberOfRows.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//			CA("totalNumberOfRowsBeforeRowInsertion  = " + numberOfRows);*/
//            		
//			//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
//			GridArea gridAreaForEntireTable,headerArea;;
//			if(HeaderRowPresent)
//			{	//CA("fill gridAreaForEntireTable");			 
//				gridAreaForEntireTable.topRow = headerCount;
//				gridAreaForEntireTable.leftCol = 0;
//				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
//                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//			
//				headerArea.topRow = 0;
//				headerArea.leftCol = 0;
//				headerArea.bottomRow = headerCount;
//				headerArea.rightCol = totalNumberOfColumns;			
//			}
//			else
//			{
//				gridAreaForEntireTable.topRow = 0;
//				gridAreaForEntireTable.leftCol = 0;
//				gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
//                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//			}
//
//			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
//			if(tableGeometry == NULL)
//			{
//				//CA("tableGeometry == NULL");
//				break;//breaks the for..tableIndex
//			}
//			PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 			
//			
//
//			//bool16 tableHeaderPresent = kFalse;
//		
//
//
//			//int32 tableTypeId = -1;		
//			//int32 sort_by_attributeId = -1;
//			//for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
//			//{//start for tagIndex = 0
//			//	XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
//			//	//This is a tag attached to the cell.
//			//	//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//			//	InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//			//	//Get the text tag attached to the text inside the cell.
//			//	//We are providing only one texttag inside cell.
//			//	if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//			//	{
//			//		continue;
//			//	}
//			//	for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//			//	{
//			//		XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//			//		//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//			//		InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//			//		//The cell may be blank. i.e doesn't have any text tag inside it.
//			//		if(cellTextTagPtr == NULL)
//			//		{
//			//			continue;
//			//		}
//			//		
//			//		if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
//			//			 && HeaderRowPresent == kFalse)
//			//		{
//			//			//CA("header present");
//			//			tableHeaderPresent = kTrue;
//			//		}
//
//			//		PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
//			//		tableTypeId = fld_1.GetAsNumber();
//			//		
//			//		if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
//			//		{		
//			//			if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
//			//			{							
//			//				PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//			//				sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
//			//			}
//			//		}
//			//							
//			//		
//			//		//parentID
//			//		PMString attributeVal("");
//			//		attributeVal.AppendNumber(pNode.getPubId());
//			//		cellTextTagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
//			//		attributeVal.Clear();
//
//			//		PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
//			//		int32 ID = id.GetAsNumber();
//			//								
//			//		//parentTypeID
//			//		if(ID != -701 && ID != -702 && ID != -703 && ID != -704)
//			//		{
//			//			attributeVal.AppendNumber(pNode.getTypeId());
//			//			cellTextTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
//			//			attributeVal.Clear();
//			//		}
//
//			//		//sectionID
//			//		attributeVal.AppendNumber(sectionid);
//			//		cellTextTagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
//			//		
//			//		attributeVal.Clear();
//			//		attributeVal.AppendNumber(slugInfo.tableType);
//			//		cellTextTagPtr->SetAttributeValue(WideString("tableType"),WideString(attributeVal));
//			//		
//			//		attributeVal.Clear();
//			//		attributeVal.AppendNumber(pNode.getPBObjectID());
//			//		cellTextTagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));					
//			//	}
//			//}//end for tagIndex = 0 
//
//
//			
//			//VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPBObjectID());
//			//if(!tableInfo)
//			//{
//			//	//CA("tableinfo is NULL");
//			//	makeTagInsideCellImpotent(slugInfo.tagPtr); 
//			//	break;
//			//}
//			//
//			//if(tableInfo->size()==0)
//			//{ //CA("tableinfo size==0");
//			//	makeTagInsideCellImpotent(slugInfo.tagPtr); 
//			//	break;
//			//}
//			//CItemTableValue oTableValue;
//			//VectorScreenTableInfoValue::iterator it;
//
//			//bool16 typeidFound=kFalse;
//			//vector<int32> vec_items;
//			//vector<int32> FinalItemIds;
//			//FinalItemIds.clear();
//
//			//for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			//{				
//			//	oTableValue = *it;				
//			//	vec_items = oTableValue.getItemIds();
//			//
//			//	if(tableTypeId == -1)	//--------
//			//	{
//			//		if(FinalItemIds.size() == 0)
//			//		{
//			//			FinalItemIds = vec_items;
//			//		}
//			//		else
//			//		{
//			//			for(int32 i=0; i<vec_items.size(); i++)
//			//			{	bool16 Flag = kFalse;
//			//				for(int32 j=0; j<FinalItemIds.size(); j++)
//			//				{
//			//					if(vec_items[i] == FinalItemIds[j])
//			//					{
//			//						Flag = kTrue;
//			//						break;
//			//					}				
//			//				}
//			//				if(!Flag)
//			//					FinalItemIds.push_back(vec_items[i]);
//			//			}
//			//		}
//			//	}
//			//	else
//			//	{
//			//		if(tableTypeId != oTableValue.getTableTypeID())
//			//			continue;
//
//			//		if(FinalItemIds.size() == 0)
//			//		{
//			//			FinalItemIds = vec_items;
//			//		}
//			//		else
//			//		{
//			//			for(int32 i=0; i<vec_items.size(); i++)
//			//			{	bool16 Flag = kFalse;
//			//				for(int32 j=0; j<FinalItemIds.size(); j++)
//			//				{
//			//					if(vec_items[i] == FinalItemIds[j])
//			//					{
//			//						Flag = kTrue;
//			//						break;
//			//					}				
//			//				}
//			//				if(!Flag)
//			//					FinalItemIds.push_back(vec_items[i]);
//			//			}
//			//		}
//			//	}
//			//	
//			//}
//
//			///*PMString FinaiItemIdsStr("FinalItemIds size = ");
//			//FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
//			//CA(FinaiItemIdsStr);*/
//			//
//			//if(tableInfo)
//			//	delete tableInfo;
//
//			//CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);
//			
//			//int32 TotalNoOfItems = getMMYTableRows(MMYTablePtr,tableIndex);
//			//if(TotalNoOfItems <= 0)
//			//{
//			//	//CA("TotalNoOfItem <= 0");
//			//	makeTagInsideCellImpotent(slugInfo.tagPtr);
//			//	break;
//			//}
//
//			GridArea bodyArea = tableModel->GetBodyArea();
//			int32 bodyCellCount = 0;
//			ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
//			ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
//			while (iterTable1 != endTable1)
//			{
//				bodyCellCount++;
//				++iterTable1;
//			}
//
//
//			/*bool16 isSameMake = kFalse;
//			bool16 isSameModel = kFalse;
//
//			int32 makeIndex = -1;
//			int32 modelIndex = -1;
//
//			int32 makeRepeatCnt = -1;
//			int32 modelRepeatCnt = -1;
//
//			int32 currentMakeModelCnt = -1;
//			int32 currentModelItemCnt = -1;
//			int32 currentModelItemIndex = -1;
//
//			int32 colIndexPerRow = -1;
//
//			vector<int32> itemIds;
//
//			Vec_CMakeModel::const_iterator itr;
//			Vec_CModelModel::const_iterator itr1;
//
//			CMakeModel *makeModelPtr = NULL;
//			CModelModel *modelModelPtr = NULL;
//
//			multimap<PMString, int32>sortItems;
//			multimap<PMString, int32>::iterator mapItr;
//			int32 ITEMID = -1;
//			int32 itemIDIndex = -1;*/
//
//
//			//int32 tableSprayedCount = 0;
//			//for(int32 selectedTableIndex = 0 ; selectedTableIndex < MMYTablePtr->vec_CMakeModelPtr->size() ; selectedTableIndex++)
//			{
//					
//				int32 totalNumberOfRowsBeforeRowInsertion_new;
//				//if(selectedTableIndex == 0)
//					totalNumberOfRowsBeforeRowInsertion_new  = totalNumberOfRowsBeforeRowInsertion;
//				/*else
//				{
//					if(HeaderRowPresent)
//						totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetBodyRows().count;
//					else
//						totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetTotalRows().count;
//				}*/
//				/*PMString d("totalNumberOfRowsBeforeRowInsertion : ");
//				d.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//				d.Append("\n totalNumberOfRowsBeforeRowInsertion_new : ");
//				d.AppendNumber(totalNumberOfRowsBeforeRowInsertion_new);
//				d.Append("\n headerCount : ");
//				d.AppendNumber(headerCount);*/
//				//CA(d);
//
//
//					//int32 rowPatternRepeatCount = TotalNoOfItems;			
//					int32 rowPatternRepeatCount = TotalNoOfItemsPerMake(MMYTablePtr, tableIndex/*selectedTableIndex*/);
//					//CA_NUM("rowPatternRepeatCount = ", rowPatternRepeatCount);
//					//int32 rowsToBeStillAdded = TotalNoOfItems - totalNumberOfRowsBeforeRowInsertion;
//					//we have to deal with 2 cases
//					//case 1: when tableHeaders are absent
//					//then the total number of rows i.e rowCount = FinalItemIds.size()-1
//					//case 2 : when tableHeaders are present
//					//then the total number of rows i.e rowCount = FinalItemsIds.size()
//					if(tableHeaderPresent /*&& selectedTableIndex == 0*/)
//					{  	
//						//CA("tableHeaderPresent");
//						//rowsToBeStillAdded = rowsToBeStillAdded + 1;
//						rowPatternRepeatCount = rowPatternRepeatCount + 1;
//					}
//					
//					//int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
//					int32 rowsToBeStillAdded ;
//					//if(selectedTableIndex == 0)	
//					{
//						rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
//						//rowPatternRepeatCount--;
//					}
//					//else
//					//	rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion;
//					
//
//					PMString rowsToBeStillAddedStr = "";
//					rowsToBeStillAddedStr.AppendNumber(rowsToBeStillAdded);					
//					//CA("rowsToBeStillAdded = " + rowsToBeStillAddedStr);
//
//					InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//					if(tableCommands==NULL)
//					{
//						break;//continues the for..tableIndex
//					}
//					
//					GridArea bodyAreaBeforeAddingRow  = tableModel->GetBodyArea();
//					GridArea gridAreaIncludingHeader;
//					if(HeaderRowPresent)
//					{
//						//CA("HeaderRowPresent 1111");
//						//if(selectedTableIndex != 0)
//						//{
//						//	for(int32 rowIndex = 0 ; rowIndex < headerCount ;rowIndex++)
//						//	{
//						//		tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
//						//	}
//
//						//	gridAreaIncludingHeader = tableModel->GetBodyArea();
//
//						//	bool16 canCopy = tableModel->CanCopy(headerArea);
//						//	if(canCopy == kFalse)
//						//	{
//						//		//CA("canCopy == kFalse");
//						//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
//						//		break;
//						//	}
//						//	//CA("canCopy");
//						//	TableMemento * tempPtr = tableModel->Copy(headerArea);
//						//	if(tempPtr == NULL)
//						//	{
//						//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
//						//		break;
//						//	}
//						//	//for(int32 pasteIndex = 1;pasteIndex <= headerCount;pasteIndex++)
//						//	//{
//						//		tableModel->Paste(GridAddress((/*pasteIndex * */totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
//						//		tempPtr = tableModel->Copy(gridAreaForEntireTable);									
//						//	//}
//						//	totalNumberOfRowsBeforeRowInsertion_new = totalNumberOfRowsBeforeRowInsertion_new + headerCount;
//						//
//						//	rowPatternRepeatCount++;
//						//}
//
//						for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
//						{
//							tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
//						}
//						
//						if(rowPatternRepeatCount > 1) //again the same
//						{
//							bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//							if(canCopy == kFalse)
//							{
//								//CA("canCopy == kFalse");
//								ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
//								break;
//							}
//							//CA("canCopy");
//							
//							TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//							if(tempPtr == NULL)
//							{
//								ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
//								break;
//							}
//							//CA_NUM("rowPatternRepeatCount = ", rowPatternRepeatCount);
//							
//							for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//							{
//								//CA_NUM("pasteIndex",pasteIndex);
//								tableModel->Paste(GridAddress((pasteIndex * totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
//								tempPtr = tableModel->Copy(gridAreaForEntireTable);
//							}
//							//return errcode;
//						}
//					}
//					else
//					{
//						//CA("HeaderRowPresent==kFalse");
//						for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
//						{
//							tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+ rowIndex-1,1),Tables::eAfter,rowHeight);
//
//						}
//						
//						if(rowPatternRepeatCount > 1) //again the same
//						{
//							bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//							if(canCopy == kFalse)
//							{
//								//CA("canCopy == kFalse");
//								break;
//							}
//							//CA("canCopy");
//							TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//							if(tempPtr == NULL)
//							{
//								ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable) == NULL");
//								break;
//							}
//						
//							for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//							{
//								tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
//								tempPtr = tableModel->Copy(gridAreaForEntireTable);
//							}
//						}
//					}
//
//					//int32 nonHeaderRowPatternIndex = 0;
//					
//					if(HeaderRowPresent)
//					{
//						//CA("HeaderRowPresent 1");
//					//	for(int32 indexOfRowInTheHeaderRowPattern = 0 ; indexOfRowInTheHeaderRowPattern < headerCount ; indexOfRowInTheHeaderRowPattern++)
//					//	{//for..iterate through each row of the headerRowPattern
//					//		for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
//					//		{//for..iterate through each column
//								
//						//GridArea gridArea = tableModel->GetCellArea (GridAddress(indexOfRowInTheHeaderRowPattern,columnIndex));
//						GridArea headerArea;
//						//if(selectedTableIndex == 0)
//							headerArea = tableModel->GetHeaderArea();
//						//else
//						//{
//						//	//CA("selectedTableIndex != 0");
//						//	headerArea.leftCol = gridAreaIncludingHeader.leftCol;
//						//	headerArea.bottomRow = gridAreaIncludingHeader.bottomRow;
//						//	headerArea.rightCol = gridAreaIncludingHeader.rightCol;
//						//	headerArea.topRow = bodyAreaBeforeAddingRow.bottomRow;
//						//}
//						ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
//						ITableModel::const_iterator endTable(tableModel->end(headerArea));
//						while (iterTable != endTable)
//						{
//							//CA("Table iterator");
//							GridAddress gridAddress = (*iterTable);         
//							InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//							if(cellContent == nil) 
//							{
//								//CA("cellContent == nil");
//								break;
//							}
//							InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//							if(!cellXMLReferenceData) {
//								//CA("!cellXMLReferenceData");
//								break;
//							}
//							XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//	
//							++iterTable;
//						//		int32 tagIndex = indexOfRowInTheHeaderRowPattern * totalNumberOfColumns + columnIndex;
//								
//						//		XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
//								//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//								if(!isMakeIDAttachedToTableTag)
//								{
//									XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
//									InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());
//
//									PMString attrVal("");
//									attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
//									tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
//									isMakeIDAttachedToTableTag = kTrue;
//								}
//								
//								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//								//Also note that, cell may be blank i.e doesn't contain any textTag.
//								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//								{
//									continue;
//								}
//								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//								{
//									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//									//Check if the cell is blank.
//									if(cellTextXMLElementPtr == NULL)
//									{
//									continue;
//									}
//
//									//Get all the elementID and languageID from the cellTextXMLElement
//									//Note ITagReader plugin methods are not used.
//									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//									//check whether the tag specifies ProductCopyAttributes.
//									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//									PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//									PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));///11/05/07
//									PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//									PMString strDataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
//
//			
//									int32 elementID = strElementID.GetAsNumber();
//									int32 languageID = strLanguageID.GetAsNumber();
//															
//									int32 tagStartPos = -1;
//									int32 tagEndPos = -1;
//									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//									tagStartPos = tagStartPos + 1;
//									tagEndPos = tagEndPos -1;
//
//									PMString dispName("");
//									////if normal tag present in header row...
//									////then we are spraying the value related to the first item.
//									int32 itemID = FinalItemIds[0];
//
//									//if(strDataType == "4" && isCallFromTS)	//-------------
//									//{
//									//	//CA("Going to spray table Name");
//									//	CItemTableValue oTableValue;
//									//	VectorScreenTableInfoValue::iterator it;
//									//	for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
//									//	{				
//									//		oTableValue = *it;		
//									//		if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
//									//		{
//									//			dispName = oTableValue.getName();
//									//			attributeVal.Clear();
//									//			attributeVal.AppendNumber(oTableValue.getTableID());
//									//			cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
//									//			
//									//			attributeVal.Clear();
//									//			attributeVal.AppendNumber(oTableValue.getTableTypeID());
//									//			cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attributeVal));
//
//									//			//CA(dispName);
//									//			break;
//									//		}
//									//	}
//
//									//}
//									//else 
//									if(strTypeId == "-1")
//									{
//										//CA("strTypeId == -1");
//										int32 parentTypeID = strParentTypeID.GetAsNumber();
//
//										//CA("#################normal tag present in header row###############");
//										if(tableFlag == "1")
//										{
//											//CA("tableFlag == 1");
//											//added on 11July...serial blast day
//											//The following code will spray the table preset inside the table cell.
//											//for copy ID = -104
//											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//											if(index== "3")// || index == "4")
//											{//Product ItemTable
//												PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//												TagStruct tagInfo;												
//												tagInfo.whichTab = index.GetAsNumber();
//												if(index == "3")
//													tagInfo.parentId = pNode.getPubId();
//												/*else if(index == "4")
//													tagInfo.parentId = pNode.getPBObjectID();*/
//
//												tagInfo.isTablePresent = tableFlag.GetAsNumber();
//												tagInfo.typeId = typeID.GetAsNumber();
//												tagInfo.sectionID = sectionid;
//																							
//												GetItemTableInTabbedTextForm(tagInfo,dispName);
//												cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
//												//CA(dispName);
//											}
//											//end added on 11July...serial blast day
//											else if(index == "4")
//											{//This is the Section level ItemTableTag and we have selected the product
//											//for spraying
//												makeTheTagImpotent(cellTextXMLElementPtr);										
//											}
//											
//										}
//
//											
//										else if(imgFlag == "1")
//										{							
//											XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//											UIDRef ref = cntentREF.GetUIDRef();
//											if(ref == UIDRef::gNull)
//											{
//												//CA("ref == UIDRef::gNull");
//												continue;
//											}
//
//											InterfacePtr<ITagReader> itagReader
//											((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//											if(!itagReader)
//												continue ;
//											
//											TagList tList=itagReader->getTagsFromBox(ref);
//											TagStruct tagInfoo;
//											int numTags=static_cast<int>(tList.size());
//
//											if(numTags<0)
//											{
//												continue;
//											}
//
//											tagInfoo=tList[0];
//
//											tagInfoo.parentId = pNode.getPubId();
//											tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//											tagInfoo.sectionID=CurrentSubSectionID;
//
//											IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//											if(!iDataSprayer)
//											{	
//												//CA("!iDtaSprayer");
//												continue;
//											}
//
//											iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//											
//											PMString attrVal;
//											attrVal.AppendNumber(itemID);
//											cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//												
//											for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//											{
//												tList[tagIndex].tagPtr->Release();
//											}
//											continue;
//										}
//										else if(tableFlag == "0")
//										{
//											//CA("tableFlag == 0 AAA");
//											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//											if(index =="4")
//											{//Item
//												//CA("index == 4 ");
//												//added on 22Sept..EventPrice addition.												
//												/*if(strElementID == "-701")
//												{
//													PMString  attributeIDfromNotes = "-1";
//													bool8 isNotesValid = getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
//													cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//													elementID = attributeIDfromNotes.GetAsNumber();
//												}*/
//												if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) != WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) != WideString("-101"))
//													itemID = pNode.getPubId();
//
//												//if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//												//{
//												//	TagStruct tagInfo;
//												//	tagInfo.elementId = elementID;
//												//	tagInfo.tagPtr = cellTextXMLElementPtr;
//												//	tagInfo.typeId = itemID;
//												//	tagInfo.parentTypeID = parentTypeID;//added by Tushar on 27/12/06
//												//	handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
//												//}
//												////ended on 22Sept..EventPrice addition.
//												//else if(elementID == -803)  // For 'Letter Key'
//												//{
//												//	dispName = "a" ;			// For First Item										
//												//}
//												//else if((isComponentAttributePresent == 1) && (elementID == -805) )  // For 'Quantity'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][0]);
//												//}
//												//else if((isComponentAttributePresent == 1) && (elementID == -806) )  // For 'Availability'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][1]);			
//												//}
//												//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][0]);			
//												//}
//												//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][1]);			
//												//}
//												//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][2]);			
//												//}
//												//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][3]);			
//												//}
//												//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
//												//{
//												//	dispName = (Kit_vec_tablerows[0][4]);			
//												//}
//												//else if((isComponentAttributePresent == 3) && (elementID == -812) )  // For 'Quantity'
//												//{
//												//	dispName = (Accessory_vec_tablerows[0][0]);
//												//}
//												//else if((isComponentAttributePresent == 3) && (elementID == -813) )  // For 'Required'
//												//{
//												//	dispName = (Accessory_vec_tablerows[0][1]);
//												//}
//												//else if((isComponentAttributePresent == 3) && (elementID == -814) )  // For 'Comments'
//												//{
//												//	dispName = (Accessory_vec_tablerows[0][2]);
//												//}
//												//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[0][4]) == "N"))
//												//{
//												//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
//												//}
//												//else if(elementID == -827)  // For 'Number Key'
//												//{
//												//	dispName = "1" ;
//												//}
//												if(elementID == -401)
//												{
//													dispName = MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_Name();
//
//													PMString attrVal("");
//													attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_id());
//													cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
//												}
//												else
//												{
//													dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);										
//													//dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);  // getting single item attributye data at a time.
////PMString a("itemID = ");
////a.AppendNumber(itemID);
////a.Append("\nelementID = ");
////a.AppendNumber(elementID);
////a.Append("\ndispName = ");
////a.Append(dispName);
////CA(a);
//												}
//												
//												PMString attrVal;
//												attrVal.AppendNumber(itemID);
//												if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) == WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) == WideString("-101"))
//												{
//													//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//	 												cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//												}
//												else
//												{
//													PMString attrVal("");
//													attrVal.AppendNumber(pNode.getPubId());
//													cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//												}
//											}
//											else if( index == "3")
//											{//Product
//												
//												dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID,kTrue);
//												PMString attrVal;
//												attrVal.AppendNumber(pNode.getTypeId());													
//												cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//												attrVal.Clear();
//
//												attrVal.AppendNumber(pNode.getPubId());
//												cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//											}
//										}								
//									}
//									else if(imgFlag == "1")
//									{							
//										XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//										UIDRef ref = cntentREF.GetUIDRef();
//										if(ref == UIDRef::gNull)
//										{
//											//CA("ref == UIDRef::gNull");
//											continue;
//										}
//
//										InterfacePtr<ITagReader> itagReader
//										((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//										if(!itagReader)
//											continue ;
//										
//										TagList tList=itagReader->getTagsFromBox(ref);
//										TagStruct tagInfoo;
//										int numTags=static_cast<int>(tList.size());
//
//										if(numTags<0)
//										{
//											continue;
//										}
//
//										tagInfoo=tList[0];
//
//										tagInfoo.parentId = pNode.getPubId();
//										tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//										tagInfoo.sectionID=CurrentSubSectionID;
//
//										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//										if(!iDataSprayer)
//										{	
//											CA("!iDtaSprayer");
//											continue;
//										}
//
//										iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//										
//										PMString attrVal;
//										attrVal.AppendNumber(itemID);
//										cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//										
//										for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//										{
//											tList[tagIndex].tagPtr->Release();
//										}
//										
//										continue;
//									}
//									else
//									{							
//										if(tableFlag == "1")
//										{					
//											//CA("inside cell table is present");
//										//added on 11July...the serial blast day
//											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//											if(index== "3")
//											{
//												cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
//												do
//												{
//													PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//				//CA(strTypeID);
//													int32 typeId = strTypeID.GetAsNumber();
//													VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//													if(typeValObj==NULL)
//													{
//														ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
//														break;
//													}
//
//													VectorTypeInfoValue::iterator it1;
//
//													bool16 typeIDFound = kFalse;
//													for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//													{	
//														if(typeId == it1->getType_id())
//															{
//																typeIDFound = kTrue;
//																break;
//															}			
//													}
//													if(typeIDFound)
//														dispName = it1->getName();
//
//													if(typeValObj)
//														delete typeValObj;
//												}while(kFalse);
//														
//											}
//											//This is a special case.The tag is of product but the Item is
//											//selected.So make this tag unproductive for refresh.(Impotent)
//											else if(index == "4")
//											{
//												makeTheTagImpotent(cellTextXMLElementPtr);
//												
//											}
//													
//													
//										}
//										else
//										{
//								
//											PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//											if(index== "3")
//											{//For product
//												CElementModel  cElementModelObj;
//												bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
//												if(result)
//													dispName = cElementModelObj.getDisplay_name();
//												PMString attrVal;
//												attrVal.AppendNumber(pNode.getTypeId());
//												cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//																					
//												cellTextXMLElementPtr->SetAttributeValue(WideString("tableFlag"),WideString("-13"));
//											}
//											else if(index == "4")
//											{
//												//following code added by Tushar on 27/12/06
//												if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
//												{
//													PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//													int32 parentTypeID = strParentTypeID.GetAsNumber();
//													
//													VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//													if(TypeInfoVectorPtr != NULL)
//													{
//														VectorTypeInfoValue::iterator it3;
//														int32 Type_id = -1;
//														PMString temp = "";
//														PMString name = "";
//														for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//														{
//															Type_id = it3->getType_id();
//															if(parentTypeID == Type_id)
//															{
//																temp = it3->getName();
//																if(elementID == -701)
//																{
//																	dispName = temp;
//																}
//																else if(elementID == -702)
//																{
//																	dispName = temp + " Suffix";
//																}
//															}														
//														}													
//													}
//													if(TypeInfoVectorPtr)
//														delete TypeInfoVectorPtr;
//												}
//												else if(elementID == -703)
//												{
//													dispName = "$Off";
//												}
//												else if(elementID == -704)
//												{
//													dispName = "%Off";
//												}
//												else if((elementID == -805) )  // For 'Quantity'
//												{
//													//CA("Quantity Header");
//													dispName = "Quantity";
//												}
//												else if((elementID == -806) )  // For 'Availability'
//												{
//													dispName = "Availability";			
//												}
//												else if(elementID == -807)
//												{									
//													dispName = "Cross-reference Type";
//												}
//												else if(elementID == -808)
//												{									
//													dispName = "Rating";
//												}
//												else if(elementID == -809)
//												{									
//													dispName = "Alternate";
//												}
//												else if(elementID == -810)
//												{									
//													dispName = "Comments";
//												}
//												else if(elementID == -811)
//												{									
//													dispName = "";
//												}
//												else if((elementID == -812) )  // For 'Quantity'
//												{
//													//CA("Quantity Header");
//													dispName = "Quantity";
//												}
//												else if((elementID == -813) )  // For 'Required'
//												{
//													//CA("Required Header");
//													dispName = "Required";
//												}
//												else if((elementID == -814) )  // For 'Comments'
//												{
//													//CA("Comments Header");
//													dispName = "Comments";
//												}
//
//												else
//													dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//
//												//Modification of XMLTagAttributes.
//												//modify the XMLTagAttribute "typeId" value to -2.
//												//We have assumed that there is atleast one stencil with HeaderAttirbute
//												//true .So assign for all the stencil in the header row , typeId == -2
//												cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
//												//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
//											}									
//										}
//									}
//									PMString textToInsert("");
//									InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//									if(!iConverter)
//									{
//										textToInsert=dispName;					
//									}
//									else
//										textToInsert=iConverter->translateString(dispName);
//
//									//CA("textToInsert = " + textToInsert);
//									//Spray the Header data .We don't have to change the tag attributes
//									//as we have done it while copy and paste.
//									//WideString insertData(textToInsert);
//									//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//									/*K2*/boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
//									
//									if(iConverter)
//										iConverter->ChangeQutationMarkONOFFState(kFalse);
//									ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//									if(iConverter)
//										iConverter->ChangeQutationMarkONOFFState(kTrue);
//								}
//					//		}//for..iterate through each column
//						}//for..iterate through each row of the headerRowPattern
//					
//						//nonHeaderRowPatternIndex = 1;
//						//rowPatternRepeatCount++;
//
//					}
//
//					if(tableHeaderPresent)
//					{//if 2 tableHeaderPresent 
//						//CA("tableHeaderPresent");
//					//First rowPattern  now is of header element.
//					//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
//					/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
//					s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//					CA(s);*/
//						for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
//						{//for..iterate through each row of the repeatedRowPattern
//							/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
//							s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
//							CA(s);*/
//							for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
//							{//for..iterate through each column
//								int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
//								XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
//								//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//								//Also note that, cell may be blank i.e doesn't contain any textTag.
//								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//								{
//									continue;
//								}
//								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//								{
//									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//									//Check if the cell is blank.
//									if(cellTextXMLElementPtr == NULL)
//									{
//										continue;
//									}
//
//									//Get all the elementID and languageID from the cellTextXMLElement
//									//Note ITagReader plugin methods are not used.
//									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//									//check whether the tag specifies ProductCopyAttributes.
//									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//									
//									int32 elementID = strElementID.GetAsNumber();
//									int32 languageID = strLanguageID.GetAsNumber();
//									
//									int32 tagStartPos = -1;
//									int32 tagEndPos = -1;
//									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//									tagStartPos = tagStartPos + 1;
//									tagEndPos = tagEndPos -1;
//
//									PMString dispName(""); 
//									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									if(index == "3")
//									{
//										makeTheTagImpotent(cellTextXMLElementPtr);
//									}					
//									else if(tableFlag == "1")
//									{	
//										cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
//										do
//										{
//											PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//		//CA(strTypeID);
//											int32 typeId = strTypeID.GetAsNumber();
//											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//											if(typeValObj==NULL)
//											{
//												ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
//												break;
//											}
//
//											VectorTypeInfoValue::iterator it1;
//
//											bool16 typeIDFound = kFalse;
//											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//											{	
//												   if(typeId == it1->getType_id())
//													{
//														typeIDFound = kTrue;
//														break;
//													}			
//											}
//											if(typeIDFound)
//												dispName = it1->getName();
//
//											if(typeValObj)
//												delete typeValObj;
//										}while(kFalse);
//										//CA("You have to insert item item table here");
//										
//										//dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//										//Modification of XMLTagAttributes.
//										//modify the XMLTagAttribute "typeId" value to -2.
//										//We have assumed that there is atleast one stencil with HeaderAttirbute
//										//true .So assign for all the stencil in the header row , typeId == -2
//										//cellTextXMLElementPtr->SetAttributeValue("typeId","-2");
//										
//									}
//									else
//									{
//										//CA("tableFlag	!= 1");
//										//following code added by Tushar on 27/12/06
//										if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
//										{
//											PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//											int32 parentTypeID = strParentTypeID.GetAsNumber();
//											
//											VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//											if(TypeInfoVectorPtr != NULL)
//											{
//												VectorTypeInfoValue::iterator it3;
//												int32 Type_id = -1;
//												PMString temp = "";
//												PMString name = "";
//												for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//												{
//													Type_id = it3->getType_id();
//													if(parentTypeID == Type_id)
//													{
//														temp = it3->getName();
//														if(elementID == -701)
//														{
//															dispName = temp;
//														}
//														else if(elementID == -702)
//														{
//															dispName = temp + " Suffix";
//														}
//													}
//												}
//											}
//
//											if(TypeInfoVectorPtr)
//												delete TypeInfoVectorPtr;
//										}
//										else if(elementID == -703)
//										{
//											dispName = "$Off";
//										}
//										else if(elementID == -704)
//										{
//											dispName = "%Off";
//										}
//										else if(elementID == -401)  
//										{
//											//CA("Make");
//											dispName = "Make";
//										}
//										else if(elementID == -402)  
//										{
//											//CA("Model");
//											dispName = "Model";
//										}
//										else if(elementID == -403)  
//										{
//											//CA("Year");
//											dispName = "Year";
//										}
//										else if((elementID == -805) )  // For 'Quantity'
//										{
//											//CA("Quantity Header");
//											dispName = "Quantity";
//										}
//										else if((elementID == -806) )  // For 'Availability'
//										{
//											dispName = "Availability";			
//										}
//										else if(elementID == -807)
//										{									
//											dispName = "Cross-reference Type";
//										}
//										else if(elementID == -808)
//										{									
//											dispName = "Rating";
//										}
//										else if(elementID == -809)
//										{									
//											dispName = "Alternate";
//										}
//										else if(elementID == -810)
//										{									
//											dispName = "Comments";
//										}
//										else if(elementID == -811)
//										{									
//											dispName = "";
//										}
//										else if((elementID == -812) )  // For 'Quantity'
//										{
//											//CA("Quantity Header");
//											dispName = "Quantity";
//										}
//										else if((elementID == -813) )  // For 'Required'
//										{
//											//CA("Required Header");
//											dispName = "Required";
//										}
//										else if((elementID == -814) )  // For 'Comments'
//										{
//											//CA("Comments Header");
//											dispName = "Comments";
//										}
//										else
//										{
//											//CA("else");
//											dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
//											//CA("dispName	:	"+dispName);
//
//										}									
//										//CA("dispName :" + dispName);
//										//Modification of XMLTagAttributes.
//										//modify the XMLTagAttribute "typeId" value to -2.
//										//We have assumed that there is atleast one stencil with HeaderAttirbute
//										//true .So assign for all the stencil in the header row , typeId == -2
//										cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
//										//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
//									}
//									
//									PMString textToInsert("");
//									InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//									if(!iConverter)
//									{
//										textToInsert=dispName;					
//									}
//									else
//										textToInsert=iConverter->translateString(dispName);
//
//									//CA("textToInsert = " + textToInsert);
//									//Spray the Header data .We don't have to change the tag attributes
//									//as we have done it while copy and paste.
//									//WideString insertData(textToInsert);
//									//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//									/*K2*/boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
//									
//									if(iConverter)
//										iConverter->ChangeQutationMarkONOFFState(kFalse);
//									ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//								
//									if(iConverter)
//										iConverter->ChangeQutationMarkONOFFState(kTrue);
//								}
//							}//end for..iterate through each column.
//						}//end for..iterate through each row of repeatedRowPattern.
//						//All the subsequent rows will now spray the ItemValue.
//	//nonHeaderRowPatternIndex = 1;					
//					}//end if 2 tableHeaderPresent
//
//					
//					GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);
//
//					int32 ColIndex = -1;
//					GridArea bodyArea_new = tableModel->GetBodyArea();
//					
//					ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
//					ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
//
//				/*	bool16 isSameMake = kFalse;
//					bool16 isSameModel = kFalse;
//
//					int32 makeIndex = -1;
//					int32 modelIndex = -1;
//
//					int32 makeRepeatCnt = -1;
//					int32 modelRepeatCnt = -1;
//
//					int32 currentMakeModelCnt = -1;
//					int32 currentModelItemCnt = -1;
//					int32 currentModelItemIndex = -1;
//
//					int32 colIndexPerRow = -1;
//
//					vector<int32> itemIds;
//
//					Vec_CMakeModel::const_iterator itr;
//					Vec_CModelModel::const_iterator itr1;
//
//					CMakeModel *makeModelPtr = NULL;
//					CModelModel *modelModelPtr = NULL;
//
//					multimap<PMString, int32>sortItems;
//					multimap<PMString, int32>::iterator mapItr;
//					int32 ITEMID = -1;
//					int32 itemIDIndex = -1;*/
//
//					PMString PrevYearString("");
//					PMString PrevModelNameString("");
//					while (iterTable != endTable)
//					{
//						//CA("Iterating table");
//						GridAddress gridAddress = (*iterTable); 
//						if(tableHeaderPresent)
//							if(gridAddress <= headerRowGridAddress){
//								iterTable++;							
//								continue;
//							}
//
//						/*if(selectedTableIndex != 0)
//						{
//							if(bodyAreaBeforeAddingRow.Contains(gridAddress) || gridAreaIncludingHeader.Contains(gridAddress))
//							{
//								++iterTable;
//								continue;
//							}
//						}*/
//
//						if(isSameMake == kFalse)
//						{
//							//CA("isSameMake == kFalse");
//							makeIndex++;
//
//							makeModelPtr = MMYTablePtr->vec_CMakeModelPtr->at(makeIndex);
//
//							currentMakeModelCnt = static_cast<int32>(makeModelPtr->vec_CModelModelPtr->size());
//
//							isSameMake = kTrue;
//						}
//
//						if(isSameModel == kFalse)
//						{
//							//CA("isSameModel == kFalse");
//							modelIndex++;
//							modelModelPtr = makeModelPtr->vec_CModelModelPtr->at(modelIndex);
//							
//							currentModelItemCnt = static_cast<int32>(modelModelPtr->vec_ItemYearPtr->size());
//							
//							itemIds.clear();
//							Vec_ItemYear::iterator itr2 = modelModelPtr->vec_ItemYearPtr->begin();
//							
//							for(;itr2 != modelModelPtr->vec_ItemYearPtr->end(); itr2++)
//							{
//								PMString itemIdStr("(*itr2)->item_Id = ");
//								itemIdStr.AppendNumber((*itr2)->item_Id);
//								//CA(itemIdStr);
//
//								itemIds.push_back((*itr2)->item_Id);
//							}
//
//							if(sort_by_attributeId != -1)
//							{
//								sortItems.clear();
//								vector<int32>::iterator itr3 = itemIds.begin();
//								for(;itr3 != itemIds.end(); itr3++)
//								{
//									PMString itemIdStr("*itr3 = ");
//									itemIdStr.AppendNumber(*itr3);
//									//CA(itemIdStr);
//									PMString sortAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(*itr3,sort_by_attributeId,languageId, CurrentSectionID, kFalse);										
//									//PMString sortAttributeValue = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(*itr3,sort_by_attributeId,languageId);
//									//CA("sortAttributeValue = " + sortAttributeValue);
//									double val = sortAttributeValue.GetAsDouble();
//									sortItems.insert(multimap<double,int32>::value_type(val,*itr3));
//								}
//							}
//							
//							currentModelItemIndex++;
//
//							isSameModel = kTrue;
//
//							mapItr = sortItems.begin();
//						}
//
//						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//						if(cellContent == nil) 
//						{
//							break;
//						}
//						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//						if(!cellXMLReferenceData) {
//							break;
//						}
//
//						
//
//
//						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//						++iterTable;
//
//						ColIndex++;
//						int32 rowIndex = ColIndex/bodyCellCount;
//						
//
//						colIndexPerRow++;	
//
//						if((itemIDIndex != rowIndex) /*&& (mapItr != NULL)*/)//---conv.-VS 2005 to VS 2008-----
//						{
//							ITEMID = mapItr->second;
//							mapItr++;
//							itemIDIndex = rowIndex;
//						}
//
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());										
//						if(!isMakeIDAttachedToTableTag)
//						{
//							XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
//							InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());
//
//							PMString attrVal("");
//							attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
//							tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
//							isMakeIDAttachedToTableTag = kTrue;
//						}
//						
//						//Note that, cell may be blank i.e doesn't contain any textTag.
//						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							//CA("continue");
//							continue;
//						}
//			
//						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//						//so always take the 0th childTag of the Cell.
//						int32 cellChildCount = cellXMLElementPtr->GetChildCount();
//						for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount ;tagIndex1++)
//						{
//							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//								
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//							//Check if the cell is blank.
//							if(cellTextXMLElementPtr == NULL)
//							{	
//								continue;
//							}
//							
//							//Get all the elementID and languageID from the cellTextXMLElement
//							//Note ITagReader plugin methods are not used.
//							//CA(cellTextXMLElementPtr->GetTagString());
//
//							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//							//check whether the tag specifies ProductCopyAttributes.
//							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID")); //added by Tushar on 22/12/06
//							PMString imageFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//							/*CA("strParentTypeID =" + strParentTypeID);
//							CA("strElementID :" + strElementID);
//							CA("tableFlag :" + tableFlag);*/
//							
//							int32 elementID = strElementID.GetAsNumber();
//							int32 languageID = strLanguageID.GetAsNumber();
//							int32 itemID = ITEMID;//FinalItemIds[itemIDIndex];
//							int32 parentTypeID = strParentTypeID.GetAsNumber();	//added by Tushar on 22/12/06
//																		 
//							int32 tagStartPos = -1;
//							int32 tagEndPos = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//							tagStartPos = tagStartPos + 1;
//							tagEndPos = tagEndPos -1;
//						
//							PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
//							int32 isEventFieldVal = isEventFieldStr.GetAsNumber();
//
//							PMString dispName("");
//							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//							if(index == "3")
//							{
//								makeTheTagImpotent(cellTextXMLElementPtr);
//							}
//							else if(tableFlag == "1")
//							{
//								//CA("tableFlag == 1");
//								
//								//The following code will spray the table preset inside the table cell.
//								//for copy ID = -104
//								
//								if(index == "4")
//								{//Item ItemTable
//									//CA("index == 4");
//
//									XMLContentReference xmlCntnRef = cellTextXMLElementPtr->GetContentReference();
//									InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());
//
//									bool16 isTablePresentInsideCell = xmlCntnRef.IsTable();
//									/////following functionallity is not yet completed 
//									//for section level item having items which contains their own item table. 
//									if(isTablePresentInsideCell)
//									{
//										//CA("isTablePresentInsideCell = kTrue");
//										//
//										//InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
//										//if(innerTableModel == NULL)
//										//{
//										//	CA("innerTableModel == NULL");
//										//	continue;
//										//}
//										//UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
//						
//										//XMLTagAttributeValue xmlTagAttrVal;
//										////collect all the attributes from the tag.
//										////typeId,LanguageID are important
//										//getAllXMLTagAttributeValues(cellTextXMLElementRef,xmlTagAttrVal);	
//
//										////CA("xmlTagAttrVal.typeId : " + xmlTagAttrVal.typeId);
//
//										///*if(xmlTagAttrVal.typeId.GetAsNumber() == -111)
//										//{
//										//	fillDataInCMedCustomTableInsideTable
//										//	(
//										//		cellTextXMLElementRef,
//										//		innerTableModelUIDRef,
//										//		pNode
//
//										//	);
//										//}
//										//else
//										//{*/
//										//	FillDataTableForItemOrProduct
//										//	(
//										//		cellTextXMLElementRef,
//										//		innerTableModelUIDRef,
//										//		kFalse
//										//	);
//										///*}*/
//
//										//UIDList itemList(slugInfo.curBoxUIDRef);
//										//UIDList processedItems;
//										//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//										//ErrorCode status =  ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//
//										//tableflag++;
//										//continue;
//									}
//
//									PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//									TagStruct tagInfo;												
//									tagInfo.whichTab = index.GetAsNumber();
//									tagInfo.parentId = pNode.getPBObjectID();
//
//									tagInfo.isTablePresent = tableFlag.GetAsNumber();
//									tagInfo.typeId = typeID.GetAsNumber();
//									tagInfo.sectionID = sectionid;
//									
//										
//									GetItemTableInTabbedTextForm(tagInfo,dispName);
//									/*UIDRef tempBox = boxUIDRef;
//									sprayTableInsideTableCell(tempBox,tagInfo);*/
//									cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
//
//									PMString attrVal;
//									attrVal.Clear();
//									attrVal.AppendNumber(pNode.getPubId());
//									cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//									attrVal.Clear();
//
//									attrVal.AppendNumber(pNode.getPBObjectID());
//									cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
//									//cellTextXMLElementPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));
//
//									//CA(dispName);
//								}
//								//end added on 11July...serial blast day
//								/*else if(index == "4")
//								{//Item ItemTable
//
//									CA("You have to add ItemItemTable here");													
//									dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID, languageID, kFalse );
//								
//									//Modify the XMLTagAttribute "typeID" .
//									//Set its value to itemID;
//									PMString attrVal;
//									attrVal.AppendNumber(itemID);
//									cellTextXMLElementPtr->SetAttributeValue("typeId",attrVal);								
//								}
//								*/
//							}
//							else if(imageFlag == "1")
//							{
//								XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
//								UIDRef ref = cntentREF.GetUIDRef();
//								if(ref == UIDRef::gNull)
//								{
//									////CA("if(ref == UIDRef::gNull)");
//									int32 StartPos1 = -1;
//									int32 EndPos1 = -1;
//									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									
//
//									InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
//										textModel->QueryStrand(kOwnedItemStrandBoss, 
//										IItemStrand::kDefaultIID)));
//									if (itemStrand == nil) {
//										//CA("invalid itemStrand");
//										continue;
//									}
//									//CA("going for Owened items");
//									OwnedItemDataList ownedList;
//									itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
//									int32 count = ownedList.size();
//									
//								/*	PMString ASD("count : ");
//									ASD.AppendNumber(count);
//									CA(ASD);*/
//
//									if(count > 0)
//									{
//										//CA(" count > 0 ");
//										UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
//										if(newRef == UIDRef::gNull)
//										{
//											//CA("ref == UIDRef::gNull....2");
//											continue;
//										}
//										ref = newRef;
//									}else
//										continue;
//								}
//
//								InterfacePtr<ITagReader> itagReader
//								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//								if(!itagReader)
//									continue ;
//								
//								TagList tList=itagReader->getTagsFromBox(ref);
//								TagStruct tagInfoo;
//								int numTags=static_cast<int>(tList.size());
//
//								if(numTags<=0)
//								{
//									continue;
//								}
//
//								tagInfoo=tList[0];
//
//								tagInfoo.parentId = itemID;//pNode.getPubId();
//								tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
//								tagInfoo.sectionID=CurrentSubSectionID;
//
//								IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//								if(!iDataSprayer)
//								{	
//									//CA("!iDtaSprayer");
//									continue;
//								}
//
//								if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
//								|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
//								|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
//								|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
//								{
//									iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
//								}
//								else if(tagInfoo.elementId > 0)
//								{
//									isInlineImage = kTrue;
//									//CA("Calling fillPVAndMPVImageInBox 1");
//									ptrIAppFramework->clearAllStaticObjects();
//									iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo,itemID,kTrue);
//									iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
//														
//								}
//								else
//								{
//									iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
//								}
//
//								
//							
//								PMString attrVal;
//								attrVal.AppendNumber(itemID);													
//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//								attrVal.Clear();
//								
//								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//								{
//									tList[tagIndex].tagPtr->Release();
//								}
//								continue;
//							}
//							else if(tableFlag == "0")
//							{
//								//CA("tableFlag == 0");
//								PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//								PMString colNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
//								PMString ischildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
//								PMString rowno = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//								if(index =="4" && ischildTag == "1")
//								{
//									//CA("index == 4 && ischildTag == 1");
//									if(elementID == -803)  // For 'Letter Key'
//									{
//										/*dispName.Clear();
//										if(itemIDIndex < 26)
//											dispName.Append(AlphabetArray[itemIDIndex]);
//										else
//											dispName = "a" ;*/	
//										dispName.Clear();
//										int wholeNo =  itemIDIndex / 26;
//										int remainder = itemIDIndex % 26;								
//
//										if(wholeNo == 0)
//										{// will print 'a' or 'b'... 'z'  upto 26 item ids
//											dispName.Append(AlphabetArray[remainder]);
//										}
//										else  if(wholeNo <= 26)
//										{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//											dispName.Append(AlphabetArray[wholeNo-1]);	
//											dispName.Append(AlphabetArray[remainder]);	
//										}
//										else if(wholeNo <= 52)
//										{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//											dispName.Append(AlphabetArray[0]);
//											dispName.Append(AlphabetArray[wholeNo -26 -1]);	
//											dispName.Append(AlphabetArray[remainder]);										
//										}
//									}
//									//else if((isComponentAttributePresent == 1) && (elementID == -805))
//									//{
//									//	//CA("Here 1");
//									//	dispName.Clear();			
//									//	//CA(Kit_vec_tablerows[0][0]);
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);	
//									//}
//									//else if((isComponentAttributePresent == 1) && (elementID == -806))
//									//{	//CA("Here 2");									
//									//	dispName.Clear();	
//									//	//CA(Kit_vec_tablerows[0][1]);
//									//	dispName = ((Kit_vec_tablerows[itemIDIndex][1]));	
//									//}
//									//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
//									//{
//									//	dispName.Clear();	
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);			
//									//}
//									//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
//									//{
//									//	dispName.Clear();	
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][1]);			
//									//}
//									//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
//									//{
//									//	dispName.Clear();	
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][2]);			
//									//}
//									//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
//									//{
//									//	dispName.Clear();	
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][3]);			
//									//}
//									//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
//									//{
//									//	dispName.Clear();	
//									//	dispName = (Kit_vec_tablerows[itemIDIndex][4]);			
//									//	//CA(dispName);
//									//}
//									//else if((isComponentAttributePresent == 3) && (elementID == -812))
//									//{	//CA("Here 3");									
//									//	dispName.Clear();	
//									//	//CA(Accessory_vec_tablerows[0][1]);
//									//	dispName = ((Accessory_vec_tablerows[itemIDIndex][0]));	
//									//}
//									//else if((isComponentAttributePresent == 3) && (elementID == -813))
//									//{	//CA("Here 4");									
//									//	dispName.Clear();	
//									//	//CA(Accessory_vec_tablerows[0][1]);
//									//	dispName = ((Accessory_vec_tablerows[itemIDIndex][1]));	
//									//}
//									//else if((isComponentAttributePresent == 3) && (elementID == -814))
//									//{	//CA("Here 5");									
//									//	dispName.Clear();	
//									//	//CA(Accessory_vec_tablerows[0][1]);
//									//	dispName = ((Accessory_vec_tablerows[itemIDIndex][2]));	
//									//}
//									//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[itemIDIndex][4]) == "N"))
//									//{
//									//	//CA(" non catalog items");
//									//	dispName.Clear();	
//									//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
//									//}
//									else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
//									{
//										VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),elementID,CurrentSubSectionID);
//										if(vecPtr != NULL){
//											if(vecPtr->size()> 0){
//												dispName = vecPtr->at(0).getObjectValue();	
//											}
//										}
//									}
//									//else if(elementID == -401  || elementID == -402  ||  elementID == -403)		//----For Make, Mode, Year . 
//									//{
//									//	dispName = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemID,elementID, languageID);
//									//}
//									else if(elementID == -401)		//----For Make, Mode, Year . 
//									{
//										dispName = makeModelPtr->getMake_Name();
//									}
//									else if(elementID == -402)		//----For Make, Mode, Year . 
//									{
//										dispName = modelModelPtr->getModel_Name();										
//										if(PrevModelNameString.IsEqual(dispName))
//										{
//											dispName = "";
//											PMString attrVal;
//											attrVal.AppendNumber(1);											
//											cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));
//
//										}
//										else
//											PrevModelNameString = dispName;
//									}
//									else if(elementID == -403)		//----For Make, Mode, Year . 
//									{
//										Vec_ItemYear::iterator itemYearItr = modelModelPtr->vec_ItemYearPtr->begin();
//										for(; itemYearItr != modelModelPtr->vec_ItemYearPtr->end(); itemYearItr++)
//										{
//											if(itemID == (*itemYearItr)->item_Id)
//											{
//												dispName = (*itemYearItr)->year;
//												if(PrevYearString.IsEqual(dispName))
//												{
//													dispName = "";
//													PMString attrVal;
//													attrVal.AppendNumber(1);											
//													cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));
//												}
//												else
//													PrevYearString = dispName;
//											}
//										}										
//									}
//									else if(elementID == -827)  //**** Number Keys
//									{
//										dispName.Clear();
//										dispName.Append(itemIDIndex+1);
//									}
//									else 
//									{
//										//CA("catalog Items");
//										dispName.Clear();	
//										dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID, CurrentSectionID, kFalse);
//										//dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);
//
//									}
//									PMString attrVal;
//									attrVal.AppendNumber(itemID);
//									//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//									cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//									
//									attrVal.Clear();
//									attrVal.AppendNumber(pNode.getPubId());														
//									cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//									
//									attrVal.Clear();
//									attrVal.AppendNumber(pNode.getPBObjectID());
//									cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));	
//								
//									if(rowno == "-904")
//									{
//
//										attrVal.Clear();
//										attrVal.AppendNumber(makeModelPtr->getMake_id());														
//										cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
//									
//										if(elementID != -401)		//----For Make, Mode, Year . 
//										{
//											attrVal.Clear();
//											attrVal.AppendNumber(modelModelPtr->getModel_Id());														
//											cellTextXMLElementPtr->SetAttributeValue(WideString("field4"),WideString(attrVal));
//										}
//									}
//								}
//								else if( index == "4")
//								{//SectionLevelItem
//									//CA("index == 4");
//								////added on 22Sept..EventPrice addition.
//									//if(strElementID == "-701")
//									//{
//									//	PMString  attributeIDfromNotes = "-1";
//									//	bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
//									//	cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//									//	elementID = attributeIDfromNotes.GetAsNumber();
//									//}
//									////ended on 22Sept..EventPrice addition.	
//									if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//									{
//										TagStruct tagInfo;
//										tagInfo.elementId = elementID;
//										tagInfo.tagPtr = cellTextXMLElementPtr;
//										tagInfo.typeId = pNode.getPubId();
//										handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,dispName);									
//										//PMString  attributeIDfromNotes = "-1";
//										//bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
//										//cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
//										//elementID = attributeIDfromNotes.GetAsNumber();
//									}
//									else if(elementID == -803)  // For 'Letter Key'
//									{
//										/*dispName.Clear();
//										if(itemIDIndex < 26)
//											dispName.Append(AlphabetArray[itemIDIndex]);
//										else
//											dispName = "a" ;*/		
//										dispName.Clear();
//										int wholeNo =  itemIDIndex / 26;
//										int remainder = itemIDIndex % 26;								
//
//										if(wholeNo == 0)
//										{// will print 'a' or 'b'... 'z'  upto 26 item ids
//											dispName.Append(AlphabetArray[remainder]);
//										}
//										else  if(wholeNo <= 26)
//										{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//											dispName.Append(AlphabetArray[wholeNo-1]);	
//											dispName.Append(AlphabetArray[remainder]);	
//										}
//										else if(wholeNo <= 52)
//										{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//											dispName.Append(AlphabetArray[0]);
//											dispName.Append(AlphabetArray[wholeNo -26 -1]);	
//											dispName.Append(AlphabetArray[remainder]);										
//										}
//									}	
//									else
//										dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID, CurrentSectionID, kFalse);
//										//dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID);
//									if(elementID == -827)  // For 'Number Key'
//									{
//										dispName.Clear();
//										dispName.Append(itemIDIndex+1);
//									}
//									
//									PMString attrVal;
//									attrVal.AppendNumber(pNode.getPubId());	
//									cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//									//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
//									//cellTextXMLElementPtr->SetAttributeValue(WideString("childTag"),WideString("1"));
//
//									cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
//										
//									
//									attrVal.Clear();
//									attrVal.AppendNumber(pNode.getPBObjectID());
//									cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
//						
//								}
//
//							}
//							cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("-1"));
//							/*if(isCallFromTS ){
//								PMString temp("");
//								temp.AppendNumber(tableSourceInfoValueObj->getVec_Table_ID().at(0));
//								cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(temp));								
//							}*/
//							PMString textToInsert("");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(!iConverter)
//							{
//								textToInsert=dispName;					
//							}
//							else
//								textToInsert=iConverter->translateString(dispName);
//							//CA("textToInsert = " + textToInsert);
//							//Spray the Header data .We don't have to change the tag attributes
//							//as we have done it while copy and paste.	
//							//WideString insertData(textToInsert);
//							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//							/*K2*/boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
//							
//							if(iConverter)
//								iConverter->ChangeQutationMarkONOFFState(kFalse);
//							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
//							
//							if(iConverter)
//								iConverter->ChangeQutationMarkONOFFState(kTrue);
//						}
//						
//						if(colIndexPerRow == bodyCellCount - 1)
//						{
//							//CA("colIndexPerRow == bodyCellCount - 1");
//							colIndexPerRow = -1;
//							currentModelItemIndex++;
//							if(currentModelItemIndex == currentModelItemCnt)
//							{
//								//CA("currentModelItemIndex == currentModelItemCnt");
//								if(modelIndex < currentMakeModelCnt - 1)
//								{
//									//CA("modelIndex < currentMakeModelCnt - 1");
//									isSameModel = kFalse;
//									currentModelItemCnt = -1;
//									currentModelItemIndex = -1;
//
//								}else
//								{
//									//CA("else");
//									isSameMake = kFalse;
//									isSameModel = kFalse;
//									modelIndex = -1;
//									currentMakeModelCnt = -1;
//									currentModelItemCnt = -1;
//									currentModelItemIndex = -1;
//								}
//							}
//						}	
//					}//end for..iterate through each column	
//					
//					//tableSprayedCount--;
//				}
//		}
//		
//		if(MMYTablePtr != NULL)
//			delete MMYTablePtr;
//		errcode = kTrue;
//	}while(0);
	return errcode;
}


//int32 TableUtility::getMMYTableRows(const CMMYTable* MMYTablePtr, int32 index)
//{
//	int32 count = 0;
//	
//	CMakeModel* make = MMYTablePtr->vec_CMakeModelPtr->at(index);
//	Vec_CModelModel::const_iterator itr1 = make->vec_CModelModelPtr->begin();
//	for(;itr1 != make->vec_CModelModelPtr->end(); itr1++)
//	{
//		int32 size = static_cast<int32>((*itr1)->vec_ItemYearPtr->size());
//		count = count + size;
//	}
//
//	/*Vec_CMakeModel::const_iterator itr = MMYTablePtr->vec_CMakeModelPtr->begin();
//	Vec_CModelModel::const_iterator itr1;
//	for(; itr != MMYTablePtr->vec_CMakeModelPtr->end(); itr++)
//	{
//		itr1 = (*itr)->vec_CModelModelPtr->begin();
//		for(;itr1 != (*itr)->vec_CModelModelPtr->end(); itr1++)
//		{
//			int32 size = static_cast<int32>((*itr1)->vec_ItemYearPtr->size());
//			count = count + size;
//		}
//	}*/
//
//
//
//	return count;
//}
//
//int32 TableUtility::TotalNoOfItemsPerMake(const CMMYTable* MMYTablePtr, const int32 selectedTableIndex)
//{
//	int32 count = 0;
//	int32 index = 0;
//	Vec_CMakeModel::const_iterator itr = MMYTablePtr->vec_CMakeModelPtr->begin();
//	Vec_CModelModel::const_iterator itr1;
//	for(; itr != MMYTablePtr->vec_CMakeModelPtr->end(); itr++,index++)
//	{
//		if(index == selectedTableIndex)
//		{
//			itr1 = (*itr)->vec_CModelModelPtr->begin();
//			for(;itr1 != (*itr)->vec_CModelModelPtr->end(); itr1++)
//			{
//				int32 size = static_cast<int32>((*itr1)->vec_ItemYearPtr->size());
//				count = count + size;
//			}
//			return count;
//		}
//	}
//
//	return count;
//
//}

bool16 TableUtility::InspectChars(InterfacePtr<ITextModel>& textModel,TextIndex startIndex, TextIndex endIndex, TextIndex& tableStartIndex, TextIndex& tableEndIndex)
{
	bool16 status = kFalse;
	do {
		if (!textModel) {
			break;
		}

		bool16 tableFound = kFalse;
		TextIterator begin(textModel, startIndex);
		TextIterator end(textModel, endIndex);
		
		for (TextIterator iter = begin; iter != end; iter++) {
			const UTF32TextChar characterCode = *iter;
			PMString character = this->GetCharacter(characterCode);
			if((character.IsEqual("<Table>") && tableFound == kFalse) || character.IsEqual("<TableContinued>"))
			{
				if(character.IsEqual("<Table>"))
				{
					tableStartIndex = iter.Position();	
					tableFound = kTrue;	
					status = kTrue;
					tableEndIndex = 0;
				}
				else
				{
					tableEndIndex++;
					
				}
			}
			else 
			{
				if(tableFound == kTrue)
					break;
			}
		}	
	} while(false);

	return status;
	
}

PMString TableUtility::GetCharacter(UTF32TextChar character)
{
	PMString result;

	if (character == kTextChar_CR)
	{
		result.Append("<CR>");
	}
	else if (character == kTextChar_SoftCR)
	{
		result.Append("<SoftCR>");
	}
	else if (character == kTextChar_Table)
	{
		result.Append("<Table>");
	}
	else if (character == kTextChar_TableContinued)
	{
		result.Append("<TableContinued>");
	}
	else if (character == kTextChar_ObjectReplacementCharacter)
	{
		result.Append("<Object>");
	}
	else
	{
		result.AppendW(character);
	}
	return result;
}

ErrorCode DeleteText(ITextModel* textModel, const TextIndex position, const int32 length)
{
 	ErrorCode status = kFailure;
	do {
		ASSERT(textModel);
		if (!textModel) {
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			CA("position invalid");
			break;
		}
		if (length < 1 || length >= textModel->TotalLength()) {
			CA("length invalid");
			break;
		}
   		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {CA("!textModelCmds");
			break;
		}
		InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(position, length));
		ASSERT(deleteCmd);
		if (!deleteCmd) {CA("!deleteCmd");
			break;
		}
		status = CmdUtils::ProcessCommand(deleteCmd);
		
	} while(false);
	return status;
}



ErrorCode TableUtility::SprayItemAttributeGroupInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
{
    //CA("TableUtility::SprayItemAttributeGroupInsideTable");

    bool16 isDeleteIfEmpty = kFalse;
    ErrorCode errcode = kFailure;
    ErrorCode result = kFailure;
    vector<double> FinalItemAttributeIds;
    do
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            break;
        }
        
        UID textFrameUID = kInvalidUID;
        InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
        if (graphicFrameDataOne)
        {
            textFrameUID = graphicFrameDataOne->GetTextContentUID();
        }
        
        if (textFrameUID == kInvalidUID)
        {
            break;
        }
        
        InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
        if (graphicFrameHierarchy == nil)
        {
            //CA(graphicFrameHierarchy);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!graphicFrameHierarchy");
            break;
        }
        
        InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
        if (!multiColumnItemHierarchy) {
            //CA(multiColumnItemHierarchy);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!multiColumnItemHierarchy");
            break;
        }
        
        InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
        if (!multiColumnItemTextFrame) {
            //CA(!multiColumnItemTextFrame);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!multiColumnItemTextFrame");
            //CA("Its Not MultiColumn");
            break;
        }
        
        InterfacePtr<IHierarchy>
        frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
        if (!frameItemHierarchy) {
            //CA(“!frameItemHierarchy”);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!frameItemHierarchy");
            break;
        }
        
        InterfacePtr<ITextFrameColumn>
        textFrame(frameItemHierarchy, UseDefaultIID());
        if (!textFrame) {
            //CA("!!!ITextFrameColumn");
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!textFrame");
            break;
        }
        
        InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
        if (textModel == NULL)
        {
            break;
        }
        UIDRef StoryUIDRef(::GetUIDRef(textModel));
        
        InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
        if(tableList==NULL)
        {
            break;
        }
        int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount();
        
        if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
        {
            break;
        }
        
        for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
        {//for..tableIndex
            InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
            if(tableModel == NULL)
                continue;//continues the for..tableIndex
            
            UIDRef tableRef(::GetUIDRef(tableModel));
            
            XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
            XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();
            
            UIDRef ContentRef = contentRef.GetUIDRef();
            if(ContentRef != tableRef)
            {
                //if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
                continue; //continues the for..tableIndex
            }
            int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
            
            int32 totalNumberOfRowsBeforeRowInsertion = 0;
            totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
            
            GridArea gridAreaForEntireTable;
            
            gridAreaForEntireTable.topRow = 0;
            gridAreaForEntireTable.leftCol = 0;
            gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
            gridAreaForEntireTable.rightCol = totalNumberOfColumns;
            
            InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
            if(tableGeometry == NULL)
            {
                //CA("tableGeometry == NULL");
                break;
            }
            
            PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1);
            
            double sectionid = -1;
            if(global_project_level == 3)
                sectionid = CurrentSubSectionID;
            if(global_project_level == 2)
                sectionid = CurrentSectionID;
            
            /*
             ////////////////////////////////////
             collect the CellTags from the first row.
             The structure of the TableXMLElementTag is as below
             TableXMLElementTag|(TableXMLElementTag has all printsource attributes.)
             |
             |
             --CellXMLElementTag|(CellXMLElementTag doesn't have any printsource attributes.)
             |
             |
             --TextXMLElementTag  (TextXMLElementTag has all the printsource attributes.)
             We are basically interested in the TextXMLElementTag.
             ////////////////////////////////////////
             */

            int32 field_1 = -1;
            PMString attributeVal("");
            
            attributeVal.Clear();
            attributeVal.AppendNumber(PMReal(pNode.getPubId()));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentID"),WideString(attributeVal));
            attributeVal.Clear();
            

            attributeVal.Clear();
            attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentTypeID"),WideString(attributeVal));
            attributeVal.Clear();
            
            attributeVal.AppendNumber(PMReal(sectionid));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("sectionID"),WideString(attributeVal));
            attributeVal.Clear();
            
            //rowno of Table tag will now have the number of rows in repetating pattern excluding Header rows.
            attributeVal.AppendNumber(PMReal(totalNumberOfRowsBeforeRowInsertion));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("rowno"),WideString(attributeVal));
            attributeVal.Clear();
            
            attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("pbObjectId"),WideString(attributeVal));
            attributeVal.Clear();
            
            if(pNode.getIsProduct() == 0)
            {
                Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("index"),WideString("4"));
            }
            
            isDeleteIfEmpty = kFalse;
            PMString groupKey = "";
            double attributeGroupId = -1;
            double languageID =1;
            
            for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
            {//start for tagIndex = 0
                XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
                //This is a tag attached to the cell.
                InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
                //Get the text tag attached to the text inside the cell.
                //We are providing only one texttag inside cell.
                if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
                {
                    continue;
                }
                for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
                {
                    XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
                    InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
                    //The cell may be blank. i.e doesn't have any text tag inside it.
                    if(cellTextTagPtr == NULL)
                    {
                        continue;
                    }
                    
                    PMString  header_Str    =  cellTextTagPtr->GetAttributeValue(WideString("header"));
                    PMString  childTag_Str  =  cellTextTagPtr->GetAttributeValue(WideString("childTag"));
                    PMString  deleteIfEmptyStr = cellTextTagPtr->GetAttributeValue(WideString("deleteIfEmpty"));
                    PMString  dataTypeStr    =  cellTextTagPtr->GetAttributeValue(WideString("dataType"));
                    PMString  groupKeyStr    =  cellTextTagPtr->GetAttributeValue(WideString("groupKey"));
                    PMString  idStr          =  cellTextTagPtr->GetAttributeValue(WideString("ID"));
                    PMString strLanguageID   =  cellTextTagPtr->GetAttributeValue(WideString("LanguageID"));

                    if(dataTypeStr != "6")
                    {
                        ptrIAppFramework->LogInfo("non AttributeGroup element found, skipping to next element");
                        continue;
                    }
                    
                    if(deleteIfEmptyStr == "1")
                        isDeleteIfEmpty = kTrue;
                    
                    groupKey = groupKeyStr;
                    attributeGroupId = idStr.GetAsDouble();
                    languageID = strLanguageID.GetAsDouble();
                    
                    PMString attributeVal("");
                    attributeVal.AppendNumber(PMReal(pNode.getPubId()));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"),WideString(attributeVal));
                    attributeVal.Clear();
                    
                    attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentTypeID"),WideString(attributeVal));
                    attributeVal.Clear();
                    
                    
                    attributeVal.AppendNumber(PMReal(sectionid));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("sectionID"),WideString(attributeVal));
                    attributeVal.Clear();
                    
                    attributeVal.AppendNumber(slugInfo.tableType);
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("tableType"),WideString(attributeVal));
                    attributeVal.Clear();
                    
                    attributeVal.AppendNumber(PMReal(pNode.getPBObjectID()));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("pbObjectId"),WideString(attributeVal));
                    attributeVal.Clear();
                    
                    PMString field1	= cellTextTagPtr->GetAttributeValue(WideString("field1"));
                    field_1 = field1.GetAsNumber();
                    
                }
            }//end for tagIndex = 0
            
            if(pNode.getIsProduct() == 0)
            {
                double languageID = ptrIAppFramework->getLocaleId();
                
                vector<double> itemAttrributeList;
                
                //if(groupKey != "")
                //    itemAttrributeList = ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(groupKey, pNode.getPubId());
                //else
                    itemAttrributeList = ptrIAppFramework->getItemAttributeListforAttributeGroupIdAndItemId( sectionid, pNode.getPubId(), attributeGroupId, languageID);
                
                if(itemAttrributeList.size() == 0 )
                {
                    PMString astr("");
                    astr.AppendNumber((attributeGroupId));
                    ptrIAppFramework->LogError("No Attributes found for attributeGroupId: " + astr + " & groupKey=" + groupKey );
                    continue;
                }
                
                
                bool16 typeidFound=kFalse;

                FinalItemAttributeIds.clear();
                
                if(isDeleteIfEmpty == kTrue)
                {
                    for(int32 index=0; index < itemAttrributeList.size(); index++)
                    {
                        PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),itemAttrributeList.at(index), languageID, CurrentSectionID, kFalse );
                        
                        if(itemAttributeValue == "")
                            continue;
                        else
                            FinalItemAttributeIds.push_back(itemAttrributeList.at(index));
                    }
                }
                else
                {
                    FinalItemAttributeIds = itemAttrributeList;
                }
                
            
            }
            
            attributeVal.Clear();
            attributeVal.AppendNumber(field_1);
            
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("field1"),WideString(attributeVal));
            attributeVal.Clear();
            
            attributeVal.AppendNumber(PMReal(attributeGroupId));
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("ID"),WideString(attributeVal));
            attributeVal.Clear();
            
            attributeVal.Append(groupKey);
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("groupKey"),WideString(attributeVal));
            attributeVal.Clear();
            
            attributeVal.Append("6");
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("dataType"),WideString(attributeVal));
            attributeVal.Clear();
            
            if(isDeleteIfEmpty)
            {
                attributeVal.Append("1");
                Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("deleteIfEmpty"),WideString(attributeVal));
                attributeVal.Clear();
            }
            
            int32 TotalNoOfAttributes =static_cast<int32>(FinalItemAttributeIds.size());
            if(TotalNoOfAttributes <= 0)
            {
                break;
            }
            
            GridArea bodyArea = tableModel->GetBodyArea();
            int32 bodyCellCount = 0;
            ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
            ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
            while (iterTable1 != endTable1)
            {
                bodyCellCount++;
                ++iterTable1;
            }
            
            int32 rowPatternRepeatCount =static_cast<int32>(FinalItemAttributeIds.size());

            
            int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
            
            InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
            if(tableCommands==NULL)
            {
                ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
                break;
            }
            
                
            for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
            {
                tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);
            }
            
            if(rowPatternRepeatCount > 1)
            {
                
                bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
                if(canCopy == kFalse)
                {
                    ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
                    break;
                }
                TableMemento* tempPtr = tableModel->Copy(gridAreaForEntireTable);
                if(tempPtr == NULL)
                {
                    ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
                    break;
                }
                
                for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
                {
                    tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr); // og
                    tempPtr = tableModel->Copy(gridAreaForEntireTable);
                }
            }
            
            
        
            int32 ColIndex = -1;
            GridArea bodyArea_new = tableModel->GetBodyArea();
            
            ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
            ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
            
            while (iterTable != endTable)
            {
                GridAddress gridAddress = (*iterTable); 

                InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
                if(cellContent == nil) 
                {
                    break;
                }
                InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
                if(!cellXMLReferenceData) {
                    break;
                }
                XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
                ++iterTable;
                
                ColIndex++;
                int32 itemIDIndex = ColIndex/bodyCellCount;
                InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
                
                //Note that, cell may be blank i.e doesn't contain any textTag.
                if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
                {
                    continue;
                }
                
                // Checking for number of tag element inside cell
                int32 cellChildCount = cellXMLElementPtr->GetChildCount();
                for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount;tagIndex1++)
                {
                    XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
                    InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());							
                    //Check if the cell is blank.
                    if(cellTextXMLElementPtr == NULL)
                    {
                        continue;
                    }
                    //Get all the elementID and languageID from the cellTextXMLElement
                    //Note ITagReader plugin methods are not used.
                    
                    PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
                    PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
                    //check whether the tag specifies ProductCopyAttributes.
                    PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
                    
                    int32 header = strHeader.GetAsNumber();	
                    double languageID = strLanguageID.GetAsDouble();
                    double elementId = FinalItemAttributeIds[itemIDIndex];
                    
                    int32 tagStartPos = -1;
                    int32 tagEndPos = -1;
                    Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
                    tagStartPos = tagStartPos + 1;
                    tagEndPos = tagEndPos -1;
                    
                    PMString dispName("");
                    
                    PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
                    if(index =="4")
                    {//Item

                        if(header == 1)
                        {
                            dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );
                        }
                        else
                        {
                            dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementId, languageID, CurrentSectionID, kFalse );
                        }
                        
                        
                        PMString attrVal;
                        attrVal.AppendNumber(PMReal(elementId));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString(attrVal));
                        attrVal.clear();
                        
                        attrVal.Append("-1");
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("dataType"),WideString(attrVal));
                        attrVal.clear();
                        
                        attrVal.Append("-1");
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(attrVal));
                        attrVal.clear();
                        
                    }
                    
                    //added to attach the tag Values to child elements
                    PMString rowValue("");
                    rowValue.AppendNumber(PMReal(gridAddress.row));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));
                    
                    PMString colValue("");
                    colValue.AppendNumber(PMReal(gridAddress.col));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));

                    PMString textToInsert("");
                    textToInsert=dispName;

                    textToInsert.ParseForEmbeddedCharacters();
                    boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
                    
                    ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
                }//end for..iterate through each cell each text tag
                
            }//end for..iterate through each column
            

            
        }//end for..tableIndex
        errcode = kTrue;
    }while(0);
    
    return errcode;
}
