#ifndef __SPCOMMONFUNCTION__

#define __SPCOMMONFUNCTION__

#include "VCPlugInHeaders.h"
#include "UIDRef.h"
#include "PMString.h"
#include "XMLReference.h"

class CommonFunction{
    
public:

	bool16 ImportFileInFrame(const UIDRef& imageBox,  PMString& fromPath);

	void fitImageInBox(const UIDRef& boxUIDRef);

	void addTagToGraphicFrame(UIDRef curBox,int32 selIndex,int32 selTab);

	XMLReference TagFrameElement(const XMLReference& newElementParent,UID frameUID, const PMString& frameTagName);

	PMString RemoveWhileSpace(PMString name);

	void attachAttributes(XMLReference* newTag,int32 selIndex,int32 selTab);

	int16 checkForOverLaps
	(UIDRef draggedItem, int32 spreadNumber, UIDRef& overlappingItem, UIDList& theList);

	int16 getBoxDimensions(UIDRef theBox, PMRect& bind);

	int16 deleteThisBox(UIDRef boxUIDRef);
	PMString keepOnlyAlphaNumeric(PMString name);

    bool16 fileExists(PMString& fromPath);

	// Vaibhav_21April
	//int16 chackWhetherTagOrNot();

};

#endif