//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
//#include "IApplication.h"
//#include "PaletteRefUtils.h"//Added
//#include "IPanelMgr.h"
//#include "IControlView.h"
//#include "IPanelControlData.h"
//#include "IXLibraryViewController.h"
//#include "PMString.h"
//#include "ITextControlData.h"
//// General includes:
//#include "CActionComponent.h"
//#include "CAlert.h"
//#include "K2Vector.h"
//#include "ISelectionManager.h"
//#include "ICommand.h"
//#include "CmdUtils.h"
//#include "IImportFileCmdData.h"
//#include "IPlaceGun.h"
//#include "IFrameContentSuite.h"
//#include "TransformUtils.h"
//#include "SDKLayoutHelper.h"
//#include "ISpread.h"
//#include "IPasteboardUtils.h"
////#include "LayoutUtils.h"  //Cs3 depricated
//#include "ILayoutUtils.h"   //Cs4
//#include "SDKUtilities.h"
//#include "ISelectionUtils.h"
//// Project includes:
//#include "PRImageHelper.h"
//#include "DCNID.h"
//#include "ITextMiscellanySuite.h"
//#include "ILoginHelper.h"
//#include "SDKListBoxHelper.h"
//#include "IActionStateList.h"
//#include "IAppFramework.h"
//#include "IDocument.h"
//#include "ILayoutUIUtils.h" //Cs4
////#include "LayoutUIUtils.h"
//#include "IMenuManager.h"
//#include "IActiveContext.h"
/////////////////////////////////
//#include "ICoreFilename.h"
//#include "ICoreFilenameUtils.h"
//#include "CAlert.h"
//#include "IClientOptions.h"
//#include "FileUtils.h"
//#include "IWindow.h"
//#include "IDataBase.h"
//#include "IApplication.h"
//#include "PaletteRefUtils.h"
//#include "IPanelMgr.h"
//#include "IListBoxController.h"
//#include "ILibrary.h"
//#include "LibraryProviderID.h"
//#include "IDFile.h"
//#include "LocaleSetting.h"
//#include "IWidgetParent.h"
//#include "PRImageHelper.h"
//#include "ISysFileData.h"
//#include "SDKFileHelper.h"
//#include "PlatformFileSystemIterator.h"
//#include "ISysFileData.h"
////#include "ISelectFolderDialog.h" //Cs3 Depricated
//#include "IOpenFileDialog.h"
//#include "SysFileList.h"
//#include "FileUtils.h"
//#include "IClientOptions.h"
//
//#include "AssetInfo.h"
//#include "AcquireModalCursor.h"

#include "DCNActionComponent.h"

#include "IDialogMgr.h"
#include "IApplication.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "PlatformFolderTraverser.h"


IDialog* DCNActionComponent::DlgPtr =0;

// Global Pointers
//IAppFramework* ptrIAppFramework = NULL;
//IClientOptions* ptrIClientOptions = NULL;
#define CA(X) CAlert::InformationAlert(X)
bool16 isThumb =kTrue;
K2Vector<PMString> FileList;
IPanelControlData* pnlDataPtr=NULL;
bool16 BrowseFolderOption ;
bool16 isBrowsePrintSource;
bool16 isThroughPrintSourceMenu = kFalse;
bool16 isShowPVImages = kFalse;
bool16 isShowPartnerImages = kFalse;


//extern K2Vector <PMString> imageVector1;
//extern K2Vector <PMString> vectorImageTypeTagName;
//extern K2Vector<PMString> imagefileName; 
//extern K2Vector<PMString> typeName; //For AutoSelect and First image name.
//used to call showProductImages and showItemImages
extern double objId;
extern double langaugeId;
extern double parId;
extern double parTypeId;
extern double sectId;
extern int32 isProd;
extern IPRImageHelper* prImage;

extern AssetInfo assetInfoObj;


namespace
{
	bool16 fitImageAndFrameToEachOther(int num)
	{
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{	
				return kFalse ;
			}

			InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
			if(!txtMisSuite)
			{	
				return kFalse ; 
			}

			switch(num)
			{
				case 3 :
				txtMisSuite->setFrameUser(3); // first resize photo ......
				break ;

				case 2 :
				txtMisSuite->setFrameUser(2); // then resize frame .....
				break ;
			}

			return kTrue ;
		}

}

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup ap7_imagesource

*/
//class DCNActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		DCNActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
/////////////////// Added on 31/10/2006   by Chetan Dogra /////
//		virtual PMString openMyDialog(PMString ffpath);
//		virtual PMString ReadFolderPath();
//		virtual void WriteFolderPath(PMString fPath);
//		virtual void fillDefaultImages();
//		virtual void UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget);
///////////////////////////////////////////////////////////////
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//		void	AddItemAction( IActiveContext* ac );
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(DCNActionComponent, kDCNActionComponentImpl)

/* DCNActionComponent Constructor
*/
DCNActionComponent::DCNActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void DCNActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	 
	switch (actionID.Get())
	{
		case kDCNPanelPSMenuActionID:
			{	
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
					return ;
				
				bool16 result=ptrIAppFramework->getLoginStatus();
				if(!result)
					return;
				
				isThroughPrintSourceMenu = kTrue;
				BrowseFolderOption = kTrue;
				InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			    if(ptrImageHelper == nil)
			    {
					CA("ProductImages plugin not found ");
					return;
			    }
				
				ptrImageHelper->showImagePanel();
				fillDefaultImages();  
				
				break;
			}


		case kDCNPopupAboutThisActionID:
		case kDCNAboutActionID:
		{
			do
			{
				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
				if ( app == NULL ) break;
				
				/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				if ( paletteMgr == NULL ) break;
				InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
				
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if ( panelMgr == NULL ) break;

				//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
				IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
				if ( !myPanel ) 
				{
					//CA("toms \001\004Panel not visible\001\004\n");
					break;
				}
				
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if (!panelControlData) 
				{
					TRACEFLOW("toms"," \001\004NO IPanelControlData\001\004\n");
					break;
				}
				pnlDataPtr=panelControlData;
				IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );

				if ( gridWidget == NULL )
				{
					TRACEFLOW("toms"," \001\004FAILED to find widgets\001\004\n");
					break;
				}
				
				InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );
		
				if ( gridController == NULL )
				{
					TRACEFLOW("toms"," \001\004NO IXLibraryViewController\001\004\n");
					break;
				}

				for(int i=FileList.size()-1; i>=0; i--)
				{
					gridController->DeleteViewItemWidgetAndUpdate(i);

				}
			}while(0);
			break;
		}
					
		case kDCNPopupItem1ActionID:
		{
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return ;
				
			bool16 result=ptrIAppFramework->getLoginStatus();
			if(!result)
				return;
				
			InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			if(ptrImageHelper == nil)
			{
				return;
			}
			ptrImageHelper->showProductImages(10000490, 1, 10000490, 73, 141);
			break;
		}

		//Following code is addd by vijay on 18/8/06

		case kDCNBrowseImageFolderActionID:
			{
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					//CA("Null ptrIAppFramework");
					return ;
				}
				
				bool16 result=ptrIAppFramework->getLoginStatus();
				if(!result)
				{
					//CA("Null result");				
					return;
				}
				
				BrowseFolderOption = kTrue;
				isBrowsePrintSource = kFalse;

				InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IID_IPRODUCTIMAGEIFACE))));
				//ptrImageHelper->EmptyImageGrid();//Comment is removed Because the images in BrowsePrintSource are also getting added in BrowseImageFolder so .. on 6/11/06

				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
				if(app == NULL) 
				{
					//CA("Null app");	
					break;
				}
				//Commented by Sachin sharma on 12/06/06
				/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				if(paletteMgr == NULL) 
				{
					break;
				}
				InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{
					//CA("Null panelMgr");	
					break;
				}
				//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
				IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
				if(!myPanel) 
				{
					//CA("kDCNPanelWidgetID is null");
					break;
				}
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("Null panelControlData");
					break;
				}
//added by vijay on 23/8/2006
				IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );
				InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );
				if(gridController == NULL )
				{
					//CA("Null gridController");
					break;
				}
				IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
				if(lstgridWidget == NULL)
				{
					//CA("Null lstgridWidget");
					break;
				}
				IControlView* thumbView = panelControlData->FindWidget( kDCNThumbPnlWidgetID);
				if(thumbView == NULL)
				{
					//CA("Null thumbView");
					break;
				}
				IControlView* multiTextView = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
				if(multiTextView == NULL)
				{
					//CA("Null multiTextView");
					break;
				}
				InterfacePtr<ITextControlData> txtData( multiTextView ,IID_ITEXTCONTROLDATA);
				if(txtData == nil)
				{
					//CA("Null txtData");
					break;
				}
				IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
				if(listImageView == NULL)
				{
					//CA("Null listImageView");
					return;
				}
				InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
				if(!iImageSysFile)
				{
					//CA("Null iImageSysFile");
					return;
				}

				/*SDKFileHelper fileHelper1("a.jpg");
				IDFile xFile1 = fileHelper1.GetIDFile();
				iImageSysFile->Set(xFile1);
				listImageView->Invalidate();*/

				IControlView* textViewlst = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
				InterfacePtr<ITextControlData> textPtrlst( textViewlst ,IID_ITEXTCONTROLDATA);
				//textPtrlst->SetString("");

				SDKListBoxHelper lstBoxHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
				IControlView* listBoxControlView = lstBoxHelper.FindCurrentListBox();
				InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);

				//	lstBoxHelper.EmptyCurrentListBox();
					
				//	typeName.Clear();
				//	imageVector1.Clear();
				//	imagefileName.Clear();			
			
				//	SDKFolderChooser folderChooser;
				//	folderChooser.SetTitle("IMAGEsource");
				//	folderChooser.ShowDialog();				

//Commented on 31/10/06 by Chetan Dogra 
//To Call "openMyDialog()" instead of "folderChooser.ShowDialog()"

//				SDKFolderChooser folderChooser;
//				folderChooser.SetTitle("IMAGEsource");
//			    folderChooser.ShowDialog();	
//till here

				PMString fPath;
				PMString pathOfImageFolder;
				fPath =	ReadFolderPath();
				pathOfImageFolder = openMyDialog(fPath);
				WriteFolderPath(pathOfImageFolder);
				if(pathOfImageFolder.IsEmpty() == kTrue)
				{
					//CA("Empty path returned from openMyDialog");
					pathOfImageFolder = fPath;
					WriteFolderPath(pathOfImageFolder);
					//CA("Please select an image folder.");
					break;
				}
				//CA("pathOfImageFolder : " + pathOfImageFolder);
				PMString fFilter("*.*"); 
				SDKFileHelper rootFileHelper(pathOfImageFolder);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				
                /*
                PlatformFileSystemIterator iter;
				if(!iter.IsDirectory(rootSysFile))
				{	
					//CA("Invalid Directory..");
					return ;
				}
				#ifdef WINDOWS
					// Windows dir iteration a little diff to Mac  ////// ***** CHANGED
				FileUtils::AppendPath(&rootSysFile,fFilter);
					//rootSysFile.Append(fFilter);
				#endif
				iter.SetStartingPath(rootSysFile);
                */
                
                
                
				//bool16 has= iter.FindFirstFile(sysFile,"*.*");
				static int imagecounter;
				PMString dataFile;
				PMString imageName;
				PMString extension;
				int32 index=0;
				bool16 isImagefileExists = kFalse;
                
                PlatformFolderTraverser folderTraverser(rootSysFile, kFalse, kTrue, kFalse, kTrue);
                IDFile sysFile;
                while (folderTraverser.Next(&sysFile))
                {
                    PMString extension;
                    FileUtils::GetExtension(sysFile, extension);
                    if ( extension   == PMString("DS_Store"))
                    {
                        continue;
                    }
                    
                    SDKFileHelper fileHelper(sysFile);
				
//			while(1)
//			{
//				bool16 hasNext;
//				if(has)
//				{
//					has=kFalse;
//					hasNext=kTrue;
//				}
//				else
//				{
//					hasNext=iter.FindNextFile(sysFile);
//				}
//				if(!hasNext)
//					break;
                    const IDFile fil=sysFile;
                    PMString dispfile=SDKUtilities::SysFileToPMString(&fil);
                    FileUtils::GetBaseFileName (dispfile,imageName);
                    FileUtils::GetExtension (fil,extension);
                    imageName.Append(".");
                    imageName.Append(extension);
                    PMString lcPath = dispfile;
                    //lcPath.ToLower();
                    // If it's some kind of graphic; set to be an eyeball?
                    // Quick hack for demo purposes
                    // It's a bit misleading; we can preview a PSD for instance,
                    // but although we can place PDF the image preview code doesn't render PDF
                    // So this is just approximately what we can preview
                    if (lcPath.Contains(PMString(".gif")) || lcPath.Contains(PMString(".GIF")) ||
                        lcPath.Contains(PMString(".jpg")) || lcPath.Contains(PMString(".JPG")) ||
                        lcPath.Contains(PMString(".tiff")) ||lcPath.Contains(PMString(".TIFF"))||
                        lcPath.Contains(PMString(".psd")) || lcPath.Contains(PMString(".PSD")) ||
                        lcPath.Contains(PMString(".ai")) ||	 lcPath.Contains(PMString(".AI")) ||
                        lcPath.Contains(PMString(".eps")) || lcPath.Contains(PMString(".EPS")) ||
                        lcPath.Contains(PMString(".jpeg")) ||lcPath.Contains(PMString(".JPEG")) ||
                        lcPath.Contains(PMString(".ps")) ||	 lcPath.Contains(PMString(".PS")) ||
                        lcPath.Contains(PMString(".tif")) || lcPath.Contains(PMString(".TIF")) ||
                        lcPath.Contains(PMString(".bmp"))||	 lcPath.Contains(PMString(".BMP"))
					
                        )
                    {

                        isImagefileExists = kTrue;
                        ptrImageHelper->WrapperForAddThumbnailImage(lcPath,imageName);
                        imageName.ParseForEmbeddedCharacters();
                        lstBoxHelper.AddElement(imageName,kDCNTextWidgetID,index);
                        //imageVector1.push_back(imageName);
                        assetInfoObj.imageVector1.push_back(lcPath);//imageVector1.push_back(lcPath);//lcPath
                        assetInfoObj.imagefileName.push_back(imageName);//imagefileName.push_back(imageName);
                        assetInfoObj.typeName.push_back(imageName);//typeName.push_back(imageName);
                        lcPath.Clear();
                        index++;
                    }
					
                }

				if(!isImagefileExists)
				{	//CA("!isImagefileExists");
					break;
				}
				gridController->Select(0);			
				SDKFileHelper fileHelper(assetInfoObj.imageVector1[0]);//imageVector1[0]);
				IDFile xFile = fileHelper.GetIDFile();
				iImageSysFile->Set(xFile);
				listImageView->Invalidate();
				IControlView* textView = panelControlData->FindWidget( kDCNMultilineTextWidgetID);
				InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
				PMString ThumbString(assetInfoObj.imagefileName[0]);//imagefileName[0]);
				//CA("ThumbString"+ThumbString);
				ThumbString.SetTranslatable(kFalse);
                ThumbString.ParseForEmbeddedCharacters();
				textPtr->SetString(ThumbString);
				textPtrlst->SetString(/*assetInfoObj.imagefileName[0]*/ThumbString);//imagefileName[0]);
				
				if(isThumb == kFalse)
				{
					listCntl->Select(0);
				}
			break;
		}


		case kDCNBrowsePrintSourceActionID:
		{	
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return ;
				
			bool16 result=ptrIAppFramework->getLoginStatus();
			if(!result)
				return;
			
			BrowseFolderOption = kFalse;
			isBrowsePrintSource = kTrue;
				goto THUMBNAIL;
					
			break;
		}
		case kDCNFitImageToFrameActionID :
		
		case kDCNFCPActionID2:
			 fitImageAndFrameToEachOther(3);
			 break ;

		case kDCNFitFrameToImageActionID :
		
		case kDCNFFTCActionID3:
			 fitImageAndFrameToEachOther(2);
			 break ;
//To Handle ThumbView Action on 4/08/2006 
		case kDCNThumbViewActionID4:
			{	
				THUMBNAIL :
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					//CA("No ptrIAppFramework");				
					return ;
				}
				
				bool16 result=ptrIAppFramework->getLoginStatus();
				if(!result)
				{
					//CA("Nahi result");
					return;	
				}
					
				isThumb=kTrue;
				InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			    if(ptrImageHelper == nil)
			    {
					CA("ProductImages plugin not found ");
					return;
			    }
//following if condition is added by vijay on 21/8/2006
				if( BrowseFolderOption == kFalse)
				{	
					ptrImageHelper->EmptyImageGrid();
					if(isProd == 1)
						ptrImageHelper->showProductImages(objId,langaugeId,parId,parTypeId,sectId);
					if(isProd == 0)
						ptrImageHelper->showItemImages(objId,langaugeId,parId,parTypeId,sectId, isProd);
				}

				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
				if(app == NULL) 
				 { 
					//CA("Nahi Application");
					break;
				 }
				
				//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				//if(paletteMgr == NULL) 
				//{ 	//CA("No IPaletteMgr");	
				//	break;
				//}
				//
				//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
				
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{		//CA("Nahi IPanelMgr");	
					break;
				}
				
				//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
				
				IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
				if(!myPanel) 
				{	
					//CA("Nahi PnlControlView11");
					break;
				}
				
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("Nahi PanelControlData");
					break;
				}
				
				IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
				if(lstgridWidget == NULL)
				{
					//CA("Nahi DCNListControlView");
					break;
				}
	
				lstgridWidget->HideView();//tO hide the ListBox And So It will Show ImagePanel
				IControlView* Widget = panelControlData->FindWidget( kDCNThumbPnlWidgetID);
				if(Widget == NULL)
				{
					//CA("Nahi pnlControlView");
					break;
				}
				Widget->ShowView();
//Added By Dattatray on 5/07/2006
				IControlView* iView =panelControlData->FindWidget(kDCNMultilineTextWidgetID);
				if(iView == nil)
				{
					//CA("Nahi iView");
					return;
				}

				int32 length=panelControlData->Length();
				/*PMString len("Length::");
				len.AppendNumber(length);
				CA(len);
			   */
				InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
				if(txtData == nil)
				{
					//CA("Nahi txtData");
					return;
				}	
			
				break;
			}
			
		
		case kDCNListViewActionID5:
			{	
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
					return ;
				
				bool16 result=ptrIAppFramework->getLoginStatus();
				if(!result)
					return;

				isThumb=kFalse;
				static bool16 isFirst=kFalse;
				K2Vector<PMString> copyFileName;//This Vector is used for When user clicked on ListViw menu multiple times For same Product.
				copyFileName= assetInfoObj.imagefileName; //imagefileName; //Every time Original File name From fileName vector is get copied into copyFileName vector
//Added By Dattatray For ListView Action on 3/08/2006
				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//CS4
				if(app == NULL) 
				 { 
					// CA("No Application");
					 break;
				 }
				
				//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				//if(paletteMgr == NULL) 
				//{ 
				//// CA("No IPaletteMgr");	
				//  break;
				//}
				//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
				
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{	
					//CA("No IPanelMgr");	
					break;
				}
				
				//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
				IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
				if(!myPanel) 
				{
					//CA("No PnlControlView22");
					break;
				}
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("No PanelControlData");
					break;
				}
				IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
				if(lstgridWidget == NULL)
				{
					//CA("No ListPanelControlView");
					break;
				}
				IControlView* thumbView = panelControlData->FindWidget( kDCNThumbPnlWidgetID);
				if(thumbView == NULL)
				{
					//CA("No thumbPanelControlView");
					break;
				}
				IControlView* multiTextView = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
				if(multiTextView == NULL)
				{
					//CA("No MultiTextView");
					break;
				}
				InterfacePtr<ITextControlData> txtData( multiTextView ,IID_ITEXTCONTROLDATA);
				if(txtData == nil)
				{
					//CA("no txtData");
					break;
				}
				IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
				if(listImageView == NULL)
				{
					//CA("No listImageView");
					return;
				}
				InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
				if(!iImageSysFile)
				{
					return;
				}

				thumbView->HideView(); //To Hide ThumbNell View
				lstgridWidget->ShowView(); // if ListBox is Hidden Then Show it
				//SDKListBoxHelper lstBoxHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
				//IControlView* listBoxControlView = lstBoxHelper.FindCurrentListBox();
				//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
				//ASSERT_MSG(listCntl != nil, "listCntl nil");
				//if(listCntl == nil) { // CA("listCntl is nil");
				//return;
				//}
				K2Vector <PMString> :: iterator the_iterator;
				the_iterator = assetInfoObj.imagefileName.begin();//imagefileName.begin();
				int32 index=0;
				//lstBoxHelper.EmptyCurrentListBox();
				PMString blankString("");
				blankString.SetTranslatable(kFalse);
				//To Fill The number of FileName related to selected images/products when user click FirstTime on ListView menu only
				K2Vector<PMString> ::iterator itr;
				int32 sizeImVe = assetInfoObj.imageVector1.size();//imageVector1.size();

				if(sizeImVe == 0)
				{
					SDKFileHelper fileHelper("C:\\asd.jpg");  //just to make it clear
					IDFile xFile = fileHelper.GetIDFile();
					iImageSysFile->Set(xFile);
					listImageView->Invalidate();	
					txtData->SetString(blankString);
					return;
				}
				itr = assetInfoObj.imageVector1.begin();//imageVector1.begin();
				PMString dataFile = *itr;
				SDKFileHelper fileHelper(dataFile);
				IDFile xFile = fileHelper.GetIDFile();
				iImageSysFile->Set(xFile);
				listImageView->Invalidate();
				if(BrowseFolderOption)//This if condtion is added by vijay on 22/8/2006
				{
					assetInfoObj.typeName.clear();//typeName.clear();
				}
				//following lines are added by vijay on 29/8/2006
				IControlView* textViewlst = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
				InterfacePtr<ITextControlData> textPtrlst( textViewlst ,IID_ITEXTCONTROLDATA);
				textPtrlst->SetString(blankString);

				if(isFirst==kFalse)
				{	
					
					while( the_iterator != assetInfoObj.imagefileName.end())//while( the_iterator != imagefileName.end()) 
					{	
						isFirst=kTrue; //Make this flag kTrue and it is static so Next time Control goes into else block
						PMString name = *the_iterator;

						if(BrowseFolderOption)//This if condtion is added by vijay on 22/8/2006
						{							
							assetInfoObj.typeName.push_back(name);//typeName.push_back(name);
						}
						//lstBoxHelper.AddElement(name,kDCNTextWidgetID,index);
						the_iterator++;
						index++;
					}
					//listCntl->Select(0);
				}
				else
				{
						the_iterator = copyFileName.begin();
						while( the_iterator!= copyFileName.end())
						{
							PMString name= *the_iterator;
							if(BrowseFolderOption)//This if condtion is added by vijay on 22/8/2006
							{						
							/*	PMString FileName = name;							
								int lastpos = 0;
								for (int i = 0 ; i< FileName.CharCount();i++)
									if ((FileName[i] == MACDELCHAR) || (FileName[i] == UNIXDELCHAR) || (FileName[i] == WINDELCHAR))
										lastpos = i;
								// At this point lastpos should point to the last delimeter, knock off the rest of the string.
								PMString * NewFile = FileName.Substring (lastpos +1);	
								name = (*NewFile);
							*/
								assetInfoObj.typeName.push_back(name);//typeName.push_back(name);
							}
							
							//lstBoxHelper.AddElement(name,kDCNTextWidgetID,index);
							the_iterator++;
							index++;
						}
						//listCntl->Select(0);
						copyFileName.clear();// It is cleared and copied by fileName vector at start.
					}
				//following code is added by vijay on 29/8/2006
				PMString imageName = assetInfoObj.imagefileName[0];
				imageName.SetTranslatable(kFalse);
                imageName.ParseForEmbeddedCharacters();
				textPtrlst->SetString(imageName);//imagefileName[0]);
										
					break;
		 }	

		case kDCNShowPVImagesActionID:     
		{
			InterfacePtr<IAppFramework> ptrIAppFramework1(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework1 == nil)
				return ;
			bool16 result=ptrIAppFramework1->getLoginStatus();
			if(!result)
				return;
			isShowPVImages =!isShowPVImages;
		
			InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		    if(ptrImageHelper == nil)
		    {
				//CA("ProductImages plugin not found ");
				return;
		    }
			
			AcquireWaitCursor awc;
			awc.Animate(); 

			if(isProd == 1)
				ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			break;
		}


		case kDCNShowPartnerImasgesActionID:     
		{
			InterfacePtr<IAppFramework> ptrIAppFramework2(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework2 == nil)
				return ;
			bool16 result=ptrIAppFramework2->getLoginStatus();
			if(!result)
				return;
			isShowPartnerImages =!isShowPartnerImages;

			InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		    if(ptrImageHelper == nil)
		    {
				//CA("ProductImages plugin not found ");
				return;
		    }
			
			AcquireWaitCursor awc;
			awc.Animate(); 

			if(isProd == 1)
				ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			break;
		}

		 default:
		 {
				break;
		 }
	}

}

void DCNActionComponent::AddItemAction( IActiveContext* ac )
{
	static	long	itemCount = 0;
	
	//CA("AddItemAction");
	
	do
	{
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if ( app == NULL ) break;
		
		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		if ( paletteMgr == NULL ) break;
		
		InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if ( panelMgr == NULL ) break;

		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if ( !myPanel ) 
		{
			//CA("No myPanel");
			break;
		}
		
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if (!panelControlData) 
		{
			//CA("No panelControlData");
			break;
		}
		
		IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );
//		IControlView* listWidget = panelControlData->FindWidget( kXLibraryItemListBoxWidgetId );
		if ( gridWidget == NULL )
		{
			//CA("No gridWidget");
			break;
		}
		
		InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );
//		InterfacePtr<IXLibraryViewController> listController( listWidget,IID_IXLIBRARYVIEWCONTROLLER );
		if ( gridController == NULL )
		{
			//CA("No gridController");
			break;
		}
		
		//char	buf[16];
		itemCount++;

		
		FileList.clear();
		FileList.push_back("C:\\Graphic1.ai");
		FileList.push_back("C:\\A1.jpg");
		FileList.push_back("C:\\A2.jpg");
		FileList.push_back("C:\\A3.jpg");
		FileList.push_back("C:\\A4.jpg");
		FileList.push_back("C:\\A5.jpg");
		FileList.push_back("C:\\A6.jpg");

		
		for(int i=0; i<FileList.size(); i++)
		{
			//make up a name for the item
		
		    //sprintf(buf,"Item %d",itemCount);						// Support code commented
		    //PMString useName(buf,-1,PMString::kNoTranslate);

			PMString useName(FileList[i]); // Added by Rahul
			int32 tmp = 0;
			IControlView* newGridItem = gridController->CreateViewItemWidget( useName,tmp);
	        //IControlView* newListItem = listController->CreateViewItemWidget( useName );
			if ( newGridItem != NULL )
			{
				//CA("Calling Function");
				gridController->InsertViewItemWidgetAndUpdate( newGridItem, kTrue, kTrue , FileList[i]);
	            //listController->InsertViewItemWidgetAndUpdate( newListItem, kTrue, kTrue ); 
			
				gridController->UpdateView();
	            //listController->UpdateView();	
			
				newGridItem->Release();
	            //newListItem->Release();
				//CA(" created and added item");
			}
			else
			{
				//CA("FAILED to create new item");
			}
		}

	}
	while ( false );
}

/* DoAbout
*/
void DCNActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kDCNAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}


//For Custom Enabling menu i.e. thumbnailview and listview .Default thumbnailview is selected
void DCNActionComponent::UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{
	do
	{
		  			
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
			

		
		IClientOptions* ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			CAlert::ErrorAlert("Interface for IClientOptions not found.");
			break;
		}
			

		bool16 result=ptrIAppFramework->getLoginStatus();

	/*if(isThumb == kTrue)
	{
		iListPtr->SetNthActionState(0,kEnabledAction);
		iListPtr->SetNthActionState(1,kEnabledAction);
		iListPtr->SetNthActionState(2,kEnabledAction);
		iListPtr->SetNthActionState(3,kEnabledAction);
		iListPtr->SetNthActionState(4,kEnabledAction);
		iListPtr->SetNthActionState(5,kEnabledAction);
	}
	if(isThumb == kFalse)
	{
			iListPtr->SetNthActionState(0,kEnabledAction);
			iListPtr->SetNthActionState(1,kEnabledAction);
			iListPtr->SetNthActionState(2,kEnabledAction);
			iListPtr->SetNthActionState(3,kEnabledAction);
			iListPtr->SetNthActionState(4,kEnabledAction);
			iListPtr->SetNthActionState(5,kEnabledAction);
	}*/

	for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
		{
			ActionID actionID = iListPtr->GetNthAction(iter);

			
			if(actionID == kDCNShowPVImagesActionID)
			{
				if(isShowPVImages)
					iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				else
					iListPtr->SetNthActionState(iter,kEnabledAction);
						
			}
	
			if(actionID == kDCNShowPartnerImasgesActionID)
			{
				if(isShowPartnerImages)
					iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				else
					iListPtr->SetNthActionState(iter,kEnabledAction);
						
			}
    
			if(actionID ==  kDCNThumbViewActionID4)
			{ 	
				if(isThumb)
                    iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				else
					iListPtr->SetNthActionState(iter,kEnabledAction);
			}	
			
			if(actionID ==  kDCNListViewActionID5 )
			{ 	
				if(isThumb)
					iListPtr->SetNthActionState(iter,kEnabledAction);
				else
					iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
			}	

			if(!isThroughPrintSourceMenu)
			{
				if(actionID ==  kDCNBrowsePrintSourceActionID)
				{ 
					
					if(isBrowsePrintSource)
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
						
				}

				if(actionID ==  kDCNBrowseImageFolderActionID )
				{	
					if(isBrowsePrintSource)
						iListPtr->SetNthActionState(iter,kEnabledAction );
					else
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				}
			}

			if(isThroughPrintSourceMenu)
			{
				if(actionID ==  kDCNBrowseImageFolderActionID)
				{ 
					
					if(BrowseFolderOption)
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
						
				}

				if(actionID ==  kDCNBrowsePrintSourceActionID )
				{	
					if(BrowseFolderOption)
						iListPtr->SetNthActionState(iter,kEnabledAction );
					else
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				}
			}
		
		switch(actionID.Get())
		{
			
			case kDCNPanelPSMenuActionID:
			case kDCNPanelWidgetActionID:
			{				
				if(result)
				{
					InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
					ASSERT(app);
					if(!app)
					{
						return;
					}
					
					/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						return;
					}
					InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
					
					InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						//CA("panelMgr is NULL");
						return;
					}
					
					if(panelMgr->IsPanelWithWidgetIDShown (kDCNPanelWidgetID))//if(panelMgr->IsPanelWithWidgetIDVisible(kDCNPanelWidgetID))//Commented By Sachin Sharma
					{
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
						iListPtr->SetNthActionState(iter,kEnabledAction);
					}

				}
				else
					{
						iListPtr->SetNthActionState(iter,kDisabled_Unselected);
					}	
				break;
			}
			
			/*case kDCNThumbViewActionID4:
				 {
					InterfacePtr<IApplication> app(gSession->QueryApplication());
					ASSERT(app);
					if(!app) 
					{
						return;
					}
					InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						return;
					}
					InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						return;
					}
					if(panelMgr->IsPanelWithWidgetIDVisible( kDCNThumbPnlWidgetID))
					{
						if(isThumb == kTrue)
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
					iListPtr->SetNthActionState(iter,kEnabledAction);
					}
					break;
				 }
				
				case kDCNListViewActionID5:
				 {
					InterfacePtr<IApplication> app(gSession->QueryApplication());
					ASSERT(app);
					if(!app)
					{
						return;
					}
					InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						return;
					}
					InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						return;
					}
					if(panelMgr->IsPanelWithWidgetIDVisible( kDCNPanelWidgetID))
					{
						if(isThumb == kFalse)
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
					iListPtr->SetNthActionState(iter,kEnabledAction);
					}
					break;
				}
*/

			}

		}
		}while(0);
		
}




//Added by Chetan Dogra on 31/10/06 //////////
PMString DCNActionComponent::openMyDialog(PMString folderPath)
{
	PMString fldrPath("");
	InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IID_IPRODUCTIMAGEIFACE))));

	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//CS4
	if(app == NULL) 
	{
		return fldrPath;
	}
	//Commented By Sachin sharma
	/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
	if(paletteMgr == NULL) 
	{
		return fldrPath;
	}

	InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL) 
	{
		return fldrPath;
	}
	//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
	IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
	if(!myPanel) 
	{
		//CA("abcd");
		return fldrPath;
	}
	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	if(!panelControlData) 
	{
		return fldrPath;
	}
	IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
	if(lstgridWidget == NULL)
	{
		return fldrPath;
	}
	SDKListBoxHelper lstBoxHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
	do {
		// Create the dialog.
		InterfacePtr</*ISelectFolderDialog*/IOpenFileDialog> selectFolderDialog(CreateObject2</*ISelectFolderDialog*/IOpenFileDialog>(kSelectFolderDialogBoss)); //Cs4
		ASSERT(selectFolderDialog);
		if(!selectFolderDialog) {
			break;
		}

		#ifdef WINDOWS
			//selectFolderDialog->SetBROWSEINFOFlags(BIF_DONTGOBELOWDOMAIN);//CS3
			//selectFolderDialog->SetAdditionalOFNFlags(OFN_ALLOWMULTISELECT);//Cs4
			selectFolderDialog->SetAdditionalFOSFlags(FOS_ALLOWMULTISELECT); //CC
		#endif
		
		#ifdef MACINTOSH
			//selectFolderDialog->SetNavDlgOpFlags(kNavDontAddTranslateItems /*| kNavAllowStationery */ | kNavDontAutoTranslate);
		#endif
		PMString fTitle("IMAGEsource");
		SDKUtilities asd;
		IDFile idFile= asd.PMStringToSysFile(&folderPath);
		// Show the dialog to let the user choose a folder.
		SysFileList  resultFolders;
		bool16 folderSelected =  selectFolderDialog->DoDialog(&idFile, 
															  resultFolders, 
															  kFalse, // only one selection
															  nil,
															  &fTitle);
		if(!folderSelected) {
			// Cancelled.
		/*ptrImageHelper->EmptyImageGrid();*/
			return fldrPath;
			
		}
		if(resultFolders.GetFileCount() != 1) {
			// We only want one result folder
			break;
		}
		ptrImageHelper->EmptyImageGrid();
		lstBoxHelper.EmptyCurrentListBox();
		assetInfoObj.typeName.clear();//typeName.clear();
		assetInfoObj.imageVector1.clear();//imageVector1.clear();
		assetInfoObj.imagefileName.clear();//imagefileName.clear();
		const IDFile temp = *resultFolders.GetNthFile(0);//idFile;
		fldrPath = FileUtils::SysFileToPMString(temp);
		//CA("fldrPath::"+fldrPath);

	} while(false);

	return fldrPath;
}

PMString DCNActionComponent::ReadFolderPath(void)
{
	PMString name("");
	//try
	//{
		InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
		if(ptrLogInHelper == nil)
		{
			CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
			return name ;
		}
		
		LoginInfoValue cserverInfoValue;
		LoginInfoValue cserverrInfovalue;
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				
		bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
		PMString envName = cserverInfoValue.getEnvName();
        bool16 rslt = ptrLogInHelper->getCurrentServerInfo(envName, cserverrInfovalue);
		if(rslt == 0)
		{
			ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
			name = clientInfoObj.getDocumentPath();
			//CA("ReadFolderPath "+name);
		}
		else {
			//CA("--getCurrentServerInfo failed--");
		}
	//}
	//catch(...)
	//{
	//	//CA("--UNKNOWN EXCEPTION -- ");
	//}

	return name;
}


void DCNActionComponent::WriteFolderPath(PMString fPath)
{
	InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
	if(ptrLogInHelper == nil)
	{
		//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return ;
	}
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;


	LoginInfoValue cserverInfoValue ;

	bool16 result = ptrLogInHelper->getCurrentServerInfo(/*name ,*/ cserverInfoValue) ;
	if(result)
	{
		//CA(" UPDATING SERVER INFO -- WRITING TO SERVER ");
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
		clientInfoObj.setDocumentPath(fPath) ;
		cserverInfoValue.setClientInfoValue(clientInfoObj);

		double ClientId = ptrIAppFramework->getClientID();
		result = ptrLogInHelper->editServerInfo(cserverInfoValue, ClientId) ;
	}

	//if(result)
	//	CA("path set");

	return;
}


void DCNActionComponent::fillDefaultImages()
{
	//CA(__FUNCTION__);		
	{	
		BrowseFolderOption = kTrue;
		isBrowsePrintSource = kFalse;

		InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IID_IPRODUCTIMAGEIFACE))));
		
		ptrImageHelper->EmptyImageGrid();

		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(app == NULL) 
		{
			//CA("app is NULL");
			return;
		}
		
		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		if(paletteMgr == NULL) 
		{
			return;
		}

		InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{
			//CA("panelMgr is NULL");
			return;
		}
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(!myPanel) 
		{
			//CA("myPanel is NULL");
			return;
		}
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(panelControlData == NULL)
		{
			//CA("panelControlData is NULL");
			return;
		}
//added by vijay on 23/8/2006
		IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );
		if(gridWidget == NULL)
		{
			//CA("gridWidget is NULL");
			return;
		}
		InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );

		if ( gridController == NULL )
		{
			//CA("001\004NO IXLibraryViewController\001\004\n");
			return;
		}
		
		
		IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
		if(lstgridWidget == NULL)
		{
			//CA("lstgridWidget == NULL");	
			return;
		}
		
		IControlView* thumbView = panelControlData->FindWidget( kDCNThumbPnlWidgetID);
		if(thumbView == NULL)
		{
			//CA("thumbView == NULL");	
			return;
		}
		
		IControlView* multiTextView = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
		if(multiTextView == NULL)
		{
			//CA("multiTextView == NULL");	
			return;
		}
		
		InterfacePtr<ITextControlData> txtData( multiTextView ,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("txtData == nil");	
			return;
		}
		
		IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
		if(listImageView == NULL)
		{
			//CA("listImageView == NULL");	
			return;
		}
		
		InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
		if(!iImageSysFile)
		{
			//CA("!iImageSysFile");	
			return;
		}

		SDKFileHelper fileHelper1("a.jpg");
		IDFile xFile1 = fileHelper1.GetIDFile();
		iImageSysFile->Set(xFile1);
		listImageView->Invalidate();

		IControlView* textViewlst = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
		if(textViewlst == NULL)
		{
			//CA("textViewlst == NULL");
			return;
		}
		InterfacePtr<ITextControlData> textPtrlst( textViewlst ,IID_ITEXTCONTROLDATA);
		if(textPtrlst == NULL)
		{
			//CA("textPtrlst is NULL");
			return;
		}
		PMString blankString("");
		blankString.SetTranslatable(kFalse);
		textPtrlst->SetString(blankString);
		//SDKListBoxHelper lstBoxHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
		//IControlView* listBoxControlView = lstBoxHelper.FindCurrentListBox();
		//if(listBoxControlView == NULL)
		//{
		//	//CA("listBoxControlView is NULL");
		//	return;
		//}
		//InterfacePtr<IListBoxController> listCntl(listBoxControlView, IID_ILISTBOXCONTROLLER);
		//if(listCntl == NULL)
		//{
		//	//CA("listCntl is NULL");
		//	return;
		//}
		//lstBoxHelper.EmptyCurrentListBox();
		assetInfoObj.typeName.clear();//typeName.clear();
		assetInfoObj.imageVector1.clear();//imageVector1.clear();
		assetInfoObj.imagefileName.clear();//imagefileName.clear();			

//Commented on 31/10/06 by Chetan Dogra 
//To Call "openMyDialog()" instead of "folderChooser.ShowDialog()"

		//SDKFolderChooser folderChooser;
		//folderChooser.SetTitle("IMAGEsource");
		//folderChooser.ShowDialog();	
		PMString pathOfImageFolder;
		pathOfImageFolder =	ReadFolderPath();
		//CA("pathOfImageFolder:::"+pathOfImageFolder);
		if(pathOfImageFolder.IsEmpty() == kTrue)
		{
			//CA("Please select an image folder.");
			return;
		}
		PMString fFilter("*.*"); 
		SDKFileHelper rootFileHelper(pathOfImageFolder);
		IDFile rootSysFile = rootFileHelper.GetIDFile();
		
        
//        PlatformFileSystemIterator iter;
//		if(!iter.IsDirectory(rootSysFile))
//		{	
//			//CA("Invalid Directory..");
//			return ;
//		}
//
//		#ifdef WINDOWS
//			// Windows dir iteration a little diff to Mac  ////// ***** CHANGED
//		FileUtils::AppendPath(&rootSysFile,fFilter);
//			//rootSysFile.Append(fFilter);
//		#endif
//		iter.SetStartingPath(rootSysFile);
		//bool16 has= iter.FindFirstFile(sysFile,"*.*");

		static int imagecounter;
		PMString dataFile;
		PMString imageName;
		PMString extension;
		int32 index=0;

		bool16 isImagefileExists = kFalse;
        
        
        PlatformFolderTraverser folderTraverser(rootSysFile, kFalse, kTrue, kFalse, kTrue);
        IDFile sysFile;
        while (folderTraverser.Next(&sysFile))
        {
            PMString extension;
            FileUtils::GetExtension(sysFile, extension);
            if ( extension   == PMString("DS_Store"))
            {
                continue;
            }
            
            SDKFileHelper fileHelper(sysFile);
            
//		while(1)
//		{
//			bool16 hasNext;
//			if(has){
//				has=kFalse;
//				hasNext=kTrue;
//			}
//			else{
//			hasNext=iter.FindNextFile(sysFile);
//			}
//			if(!hasNext)
//				break;
			const IDFile fil=sysFile;
			PMString dispfile=SDKUtilities::SysFileToPMString(&fil);
			FileUtils::GetBaseFileName (dispfile,imageName);  
			FileUtils::GetExtension (fil,extension);
			imageName.Append(".");
			imageName.Append(extension);
			PMString lcPath = dispfile;
			//lcPath.ToLower();
			// If it's some kind of graphic; set to be an eyeball?
			// Quick hack for demo purposes
			// It's a bit misleading; we can preview a PSD for instance,
			// but although we can place PDF the image preview code doesn't render PDF
			// So this is just approximately what we can preview
			if (lcPath.Contains(PMString(".gif")) || lcPath.Contains(PMString(".GIF")) ||
				lcPath.Contains(PMString(".jpg")) || lcPath.Contains(PMString(".JPG")) ||
				lcPath.Contains(PMString(".tiff")) ||lcPath.Contains(PMString(".TIFF"))||
				lcPath.Contains(PMString(".psd")) || lcPath.Contains(PMString(".PSD")) ||
				lcPath.Contains(PMString(".ai")) ||	 lcPath.Contains(PMString(".AI")) ||
				lcPath.Contains(PMString(".eps")) || lcPath.Contains(PMString(".EPS")) ||
				lcPath.Contains(PMString(".jpeg")) ||lcPath.Contains(PMString(".JPEG")) ||
				lcPath.Contains(PMString(".ps")) ||	 lcPath.Contains(PMString(".PS")) ||
				lcPath.Contains(PMString(".tif")) || lcPath.Contains(PMString(".TIF")) ||
				lcPath.Contains(PMString(".bmp"))||	 lcPath.Contains(PMString(".BMP"))
				
				)
			{	

				isImagefileExists = kTrue;
				ptrImageHelper->WrapperForAddThumbnailImage(lcPath,imageName);
				//lstBoxHelper.AddElement(imageName,kDCNTextWidgetID,index);					
		
				assetInfoObj.imageVector1.push_back(lcPath);//imageVector1.push_back(lcPath);//Added on 6/11
				assetInfoObj.imagefileName.push_back(imageName);//imagefileName.push_back(imageName);					
				assetInfoObj.typeName.push_back(imageName);//typeName.push_back(imageName);
				lcPath.Clear();
				index++;				
			}
				
		}
		if(!isImagefileExists)
		{	
			//CA("!isImagefileExists");
			return;
		}
		
		gridController->Select(0);			
		SDKFileHelper fileHelper(assetInfoObj.imageVector1[0]);//imageVector1[0]);
		IDFile xFile = fileHelper.GetIDFile();
		iImageSysFile->Set(xFile);
		listImageView->Invalidate();

		IControlView* textView = panelControlData->FindWidget( kDCNMultilineTextWidgetID);
		InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
		PMString ThumbString(assetInfoObj.imagefileName[0]);//imagefileName[0]);
		ThumbString.SetTranslatable(kFalse);
        ThumbString.ParseForEmbeddedCharacters();
		textPtr->SetString(ThumbString);
		textPtrlst->SetString(/*assetInfoObj.imagefileName[0]*/ThumbString);//imagefileName[0]);

		if(isThumb == kFalse)
		{
			//listCntl->Select(0);
		}
	}
}

void DCNActionComponent::OpenFilterDialog()
{
	do
	{

	// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		//ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		//ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kDCNPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kFilterDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);//::kModeless );//
		//ASSERT(dialog);
		if (dialog == nil) {
			break;
		}
		DlgPtr = dialog;

		// Open the dialog.
		dialog->Open(); 
		//CA(" -- ActionComponent  opendialog done -- ");
		//CA("ActionComponent 17");
		
		
	} while (false);			
}

void DCNActionComponent::CloseFilterDialog()
{
	//CA("Inside CloseDialog()");
	//CA("ActionComponent 18");
	if(DlgPtr){
		if(DlgPtr->IsOpen())
		{
			//CA("ActionComponent 19");
			DlgPtr->Close();
			//CA("ActionComponent 20");
		}
	}
}