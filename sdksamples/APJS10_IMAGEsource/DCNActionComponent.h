#include "VCPlugInHeaders.h"
// Interface includes:
#include "IApplication.h"
#include "PaletteRefUtils.h"//Added
#include "IPanelMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IXLibraryViewController.h"
#include "PMString.h"
#include "ITextControlData.h"
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "K2Vector.h"
#include "ISelectionManager.h"
#include "ICommand.h"
#include "CmdUtils.h"
//#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "IFrameContentSuite.h"
#include "TransformUtils.h"
#include "SDKLayoutHelper.h"
#include "ISpread.h"
#include "IPasteboardUtils.h"
//#include "LayoutUtils.h"  //Cs3 depricated
#include "ILayoutUtils.h"   //Cs4
#include "SDKUtilities.h"
#include "ISelectionUtils.h"
// Project includes:
#include "PRImageHelper.h"
#include "DCNID.h"
#include "ITextMiscellanySuite.h"
#include "ILoginHelper.h"
#include "SDKListBoxHelper.h"
#include "IActionStateList.h"
#include "IAppFramework.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h" //Cs4
//#include "LayoutUIUtils.h"
#include "IMenuManager.h"
#include "IActiveContext.h"
///////////////////////////////
#include "ICoreFilename.h"
#include "ICoreFilenameUtils.h"
#include "CAlert.h"
#include "IClientOptions.h"
#include "FileUtils.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IListBoxController.h"
#include "ILibrary.h"
#include "LibraryProviderID.h"
#include "IDFile.h"
#include "LocaleSetting.h"
#include "IWidgetParent.h"
#include "PRImageHelper.h"
#include "ISysFileData.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "ISysFileData.h"
//#include "ISelectFolderDialog.h" //Cs3 Depricated
#include "IOpenFileDialog.h"
#include "SysFileList.h"
#include "FileUtils.h"
#include "IClientOptions.h"

#include "AssetInfo.h"
#include "AcquireModalCursor.h"

#include "IDialog.h"

class DCNActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		DCNActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
///////////////// Added on 31/10/2006   by Chetan Dogra /////
		virtual PMString openMyDialog(PMString ffpath);
		virtual PMString ReadFolderPath();
		virtual void WriteFolderPath(PMString fPath);
		virtual void fillDefaultImages();
		virtual void UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget);

		void OpenFilterDialog();
		void CloseFilterDialog();
/////////////////////////////////////////////////////////////
	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		void	AddItemAction( IActiveContext* ac );

		static IDialog* DlgPtr;
};