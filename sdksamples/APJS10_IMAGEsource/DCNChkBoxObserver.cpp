//========================================================================================
//  
//  $File: //depot/indesign_6.0/gm/source/sdksamples/basicdialog/BscDlgDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2008/08/18 16:29:43 $
//  
//  $Revision: #1 $
//  
//  $Change: 643585 $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"
// Project includes:
#include "DCNID.h"
#include "IAppFramework.h"
#include "ITriStateControlData.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "ISelectionUtils.h"

#include "PRImageHelper.h"
#include "ISelectionManager.h"
#include "AcquireModalCursor.h"
#include "CAlert.h"

#include "DCNActionComponent.h"

#define CA(X) CAlert::InformationAlert(X)
/**	Implements IObserver based on the partial implementation CDialogObserver; 
	allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	
	 @ingroup basicdialog
	
*/
extern bool16 isShowPVImages;
extern bool16 isShowPartnerImages;
extern int32 isProd;
extern double objId;
extern double langaugeId;
extern double parId;
extern  double parTypeId;
extern double sectId;
class DCNChkBoxObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		DCNChkBoxObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~DCNChkBoxObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. 
				Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		bool16 fitImageAndFrameToEachOtherOBS(int num);
		


};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(DCNChkBoxObserver, kDCNChkBoxObserverImpl)

/* AutoAttach
*/
void DCNChkBoxObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("BscDlgDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to BasicDialog's info button widget.
		AttachToWidget(kPartnerImagesWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kPickListImagesWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kFitFrametoContentButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kFitContentProportionallyButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kDCNFilterButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		

		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void DCNChkBoxObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("BscDlgDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from BasicDialog's info button widget.
		DetachFromWidget(kPartnerImagesWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kPickListImagesWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kFitFrametoContentButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kFitContentProportionallyButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kDCNFilterButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void DCNChkBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("BscDlgDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		if(theSelectedWidget == kFitFrametoContentButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget== kFitFrametoContentButtonWidgetID && theChange == kTrueStateMessage");
			this->fitImageAndFrameToEachOtherOBS(2);
			break;	
		}

		else if(theSelectedWidget == kFitContentProportionallyButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kFitContentProportionallyButtonWidgetID && && theChange == kTrueStateMessage");
			this->fitImageAndFrameToEachOtherOBS(3);
			break;
		}
		
//******************************************************************************************************
	
		else if(theSelectedWidget == kPickListImagesWidgetID )
		{
			
			//CA("kPickListImagesWidgetID");
			InterfacePtr<IAppFramework> ptrIAppFramework1(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework1 == nil)
				return ;
			bool16 result=ptrIAppFramework1->getLoginStatus();
			if(!result)
				return;

			if(theChange == kTrueStateMessage)
				isShowPVImages = kTrue;
			else
				isShowPVImages = kFalse;
		
			InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		    if(ptrImageHelper == nil)
		    {
				//CA("ProductImages plugin not found ");
				return;
		    }
			
			AcquireWaitCursor awc;
			awc.Animate(); 

			if(isProd == 1)
				ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			break;
		}

		else if(theSelectedWidget == kPartnerImagesWidgetID)
		{
			//CA("status1 == kTrue for partner images");
			
			InterfacePtr<IAppFramework> ptrIAppFramework2(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework2 == nil)
				return ;
			bool16 result=ptrIAppFramework2->getLoginStatus();
			if(!result)
				return;
			//isShowPartnerImages =!isShowPartnerImages;
			if(theChange == kTrueStateMessage)
				isShowPartnerImages = kTrue;
			else
				isShowPartnerImages = kFalse;
			

			InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		    if(ptrImageHelper == nil)
		    {
				//CA("ProductImages plugin not found ");
				return;
		    }
			
			AcquireWaitCursor awc;
			awc.Animate(); 

			if(isProd == 1)
				ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			break;
		}
		

		else if(theSelectedWidget == kDCNFilterButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kDCNFilterButtonWidgetID && && theChange == kTrueStateMessage");
			AcquireWaitCursor awc;
			awc.Animate(); 

			DCNActionComponent ac(this);
			ac.OpenFilterDialog();
			break;
		}

//*********************************************************************************************************

	} while (false);
}

bool16 DCNChkBoxObserver::fitImageAndFrameToEachOtherOBS(int num)
{
		//CA("bool16 DCNChkBoxObserver::fitImageAndFrameToEachOtherOBS(int num)");
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			return kFalse ;
		}

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
		if(!txtMisSuite)
		{	
			return kFalse ; 
		}

		switch(num)
		{		
			case 1 :
			txtMisSuite->setFrameUser(1); // then FitContentToFrame .....
			break ;

			case 2 :
			txtMisSuite->setFrameUser(2); // FitFrameToContent ......
			break ;

			case 3 :
			txtMisSuite->setFrameUser(3); // FitContentProp ......
			break ;

			case 4 :
			txtMisSuite->setFrameUser(4); //CenterContentInFrame ......
			break ;

			case 5 :
			txtMisSuite->setFrameUser(5); // FillFrameProp ......
			break ;
			
		/*	case 6 :
			txtMisSuite->setFrameUser(6); // FillFrameProp ......*/
			default:
				break;
		}

		return kTrue ;
	}

// End, BscDlgDialogObserver.cpp.