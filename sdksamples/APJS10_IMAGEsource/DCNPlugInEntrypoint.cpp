#include "VCPlugInHeaders.h"
#include "DCNPlugInEntrypoint.h"
#include "CAlert.h"
#include "DCNID.h"
#include "DCNLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"


#define CA(z) CAlert::InformationAlert(z)

#ifdef WINDOWS
	ITypeLib* DCNPlugInEntrypoint::fDCNTypeLib = nil;
#endif

bool16 DCNPlugInEntrypoint::Load(ISession* theSession)
{
	//CA("DCNPlugInEntrypoint::Load");
	bool16 retVal=kFalse;
	do{
		//CA("in loop...");
		InterfacePtr<IRegisterLoginEvent> regEvt((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt){
			//CA("regEvt==nil");
			return kFalse;
		}
		//CA("before  registerLoginEvent");
		regEvt->registerLoginEvent(kDCNLoginEventsHandler); 
		//CA("After  registerLoginEvent");
	}while(kFalse);
	
	/*InterfacePtr<IApplication> app(gSession->QueryApplication());
	if(app == NULL) 
	{ 
		CA("No Application");
		return kFalse;
	}
				 
	InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
	if(paletteMgr == NULL) 
	{ 
		CA("No IPaletteMgr");	
		return kFalse;
	}
				
	InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
	if(panelMgr == NULL) 
	{	
		CA("No IPanelMgr");	
		return kFalse;
	}
				
	IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
	if(!myPanel) 
	{
		CA("No PnlControlView");
		return kFalse;
	}
				
	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	if(!panelControlData) 
	{
		CA("No PanelControlData");
		return kFalse;
	
	}
	IControlView* Widget = panelControlData->FindWidget( kDCNPanelWidgetID);
	if(Widget == NULL)
	{
		CA("No ListControlView");
		return kFalse;
	}
	Widget->Show();*/
	//CA("Before return kTRue..");
	return kTrue;
}

static DCNPlugInEntrypoint gPlugIn;

IPlugIn* GetPlugIn(){
	
	return &gPlugIn;
}
