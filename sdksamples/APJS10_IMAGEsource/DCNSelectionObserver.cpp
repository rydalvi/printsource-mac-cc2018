#include "VCPluginHeaders.h"

// Interface includes
#include "ISelectionManager.h"
#include "ITableSuite.h"
#include "ITableAttrRealNumber.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextValue.h"
#include "CAlert.h"
#include "ControlStripID.h"
#include "CEventHandler.h"
#include "IBoolData.h"
#include "IApplication.h"
//#include "IPaletteMgr.h"//Commented BY SAchin Sharma
#include "PaletteRefUtils.h"//Added
#include "IPanelMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
//#include "Utills.h"
#include"IDocumentList.h"
#include"IDocument.h"
#include"IDocumentUtils.h"
#include"IApplication.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
#include "SelectionObserver.h"
#include "IGraphicFrameData.h"
#include "IEvent.h"
#include "IListBoxController.h"
#include "SDKFileHelper.h"
#include "StreamUtil.h"
#include "ISysFileData.h"
#include "IListBoxController.h"
#include "SDKListBoxHelper.h"
#include "ITextControlData.h"

///////////////////
#include "ICommand.h"
#include "CmdUtils.h"
//#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "IReplaceCmdData.h"
#include "FileUtils.h"
#include "IHierarchy.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "SDKLayoutHelper.h"
#include "ISpread.h"
#include "IPasteboardUtils.h"
//#include "LayoutUtils.h" //CS3 Depricated
#include "ILayoutUtils.h" //CS4
#include "SDKUtilities.h"
#include "KeyboardDefs.h"	
#include "DCNID.h"
#include "ILayoutUIUtils.h"
#include "CEventHandler.h"
#include "IEventHandler.h"
#include "IBoolData.h"
#include "IPMStream.h"
#include "StreamUtil.h"
#include "IURIUtils.h" //Cs4
#include "IImportResourceCmdData.h"

#include "AssetInfo.h"
#define CA(X) CAlert::InformationAlert(X)
extern int32 sel;
//extern K2Vector<PMString> imageVector1 ;
//extern K2Vector<PMString> typeName;
extern K2Vector<int32> libSelectionList;
extern K2Vector<int32> curSelection ;
extern IControlView* listImageCtrView;


extern AssetInfo assetInfoObj;

/**
	Class to observe changes in the active context, and change UI when attributes change.
	@ingroup tableattributes
 */
 class DCNSelectionObserver : public CEventHandler
{
	public:
		/** 
			Constructor.
		*/
		DCNSelectionObserver(IPMUnknown *boss);
		/**
			Destructor.
		*/
		virtual ~DCNSelectionObserver(){};
		virtual bool16 KeyDown(IEvent* e);
		bool16 DoUP(int32 counter);
		bool16 DoDown(int32 counter);
		bool16 ButtonDblClk(IEvent* e);
};

CREATE_PMINTERFACE(DCNSelectionObserver, kMYframeObserverImpl);

DCNSelectionObserver::DCNSelectionObserver(IPMUnknown *boss) :
	CEventHandler(boss)
{
	

}

bool16 DCNSelectionObserver::ButtonDblClk(IEvent* e)
{
	//CA("DCNSelectionObserver::ButtonDblClk");
	bool16 result=kFalse;
	bool16 ImGFlg;
	PMRect theArea;
	PMReal rel;
	int32 ht;
	int32 wt;
		do{
		/*InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel == nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
		IControlView* DCNLstboxCntrlView= listHelper.FindCurrentListBox();
		if(DCNLstboxCntrlView== nil) 
		{
			CA("DCNLstboxCntrlView== nil");
			break;
		}

		InterfacePtr<IListBoxController> listCntl(DCNLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		if(listCntl == nil) 
		{
			CA("listCntl== nil");
			break;
		}

		
		listCntl->GetSelected( curSelection ) ;*/
	
///////////////////////////////////////

		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(app == NULL) 
			{ 
			//CA("No Application");
			break;
			}
			//Commmented By Sachin SHarma 
		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		if(paletteMgr == NULL) 
		{ 
			CA("No IPaletteMgr");	
			break;
		}
		
		InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{	
			//CA("No IPanelMgr");	
			break;
		}
		
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(!myPanel) 
		{
			//CA("No PnlControlView55");
			break;
		}
		
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(!panelControlData) 
		{
			//CA("No PanelControlData");
			break;
		}
		
		IControlView* Widget =panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
		if(Widget == NULL)
		{
			//CA("No ControlView");
			break;
		}
		
		IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
		if(listImageView == NULL)
		{
			//CA("No listImageView");
			break;
		}
		//listImageCtrView =listImageView;
		InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
		//ASSERT(iImageSysFile);
		if(!iImageSysFile)
		{
			//CA("!iImageSysFile");
			return kFalse;
		}
		InterfacePtr<ITextControlData> txtData(Widget,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			return kFalse;
		}

	//////////////////////////////////////////////////////////////////////////////////////////////
		const int kSelectionLength =  curSelection.size() ;

		int curSelRowIndex=-1;
		PMString curSelRowStr("");

		/* getting current selection here */
		if (kSelectionLength> 0 )
		{
			curSelRowIndex=curSelection[0];
			int32 select =0;
			PMString autosel;
			K2Vector<PMString> ::iterator itr;
			K2Vector<PMString> ::iterator itrone;
			itr = assetInfoObj.typeName.begin();
			itrone = assetInfoObj.imageVector1.begin();
			while(itr != assetInfoObj.typeName.end() || itrone != assetInfoObj.imageVector1.end() )
			{
			
				if(select == curSelRowIndex)
				{
					autosel = *itr;

					PMString dataFile = *itrone;
					
					int ins =  autosel.IndexOfString("  ");

					PMString* NewString = autosel.Substring(ins +1 );
					//CA(*NewString);
					NewString->SetTranslatable(kFalse);
                    NewString->ParseForEmbeddedCharacters();
					txtData->SetString(*NewString);
					//CA("autosel"+autosel);
					SDKFileHelper fileHelper(dataFile);
					//CA("dataFile"+dataFile);
					listImageCtrView->Validate();

					IDFile xFile = fileHelper.GetIDFile();

					//iImageSysFile->Set(xFile);
				
					listImageCtrView->Invalidate();
					break;
				}
				itr++;
				itrone++;
				select++;
			}
		}
	}while(0);

	//CA("wait");
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	//CA("Slection nil");
		//LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
		return result;
	}
	
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{	//LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
		//CA("My Suit nil");
		return result; 
	}
	
	UIDList uidLst;
	txtMisSuite->GetUidList(uidLst);
	UIDRef imageBox=uidLst.GetRef(0);

	InterfacePtr<IGeometry> iGeometry(imageBox, UseDefaultIID());
	if(iGeometry==nil){
		//LogPtr->LogError("Geometry of Image box is not found IN Selection obsever iGeometry is NIL");
		return kFalse;
	}

	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	rel = theArea.Height();
	ht=::ToInt32(rel);
	rel=theArea.Width();	
	wt=::ToInt32(rel);

	//CA("1");

	if(uidLst.Length()!=0)
	{	
		do{
			InterfacePtr<IGraphicFrameData> frameData(imageBox,IID_IGRAPHICFRAMEDATA);
			if (frameData)
			{
				if (frameData->HasContent())
				{	//CA("yes it has content already");
					//LogPtr->LogDebug("Image box already has Picture in image box IN Selection obsever ");
					//PREMediatorClass::
					txtMisSuite->setFrameUser(2);
					ImGFlg=kFalse;
					break;
					/*if (frameData->IsGraphicFrame())
					{
					}*/
				}
			}
			PMString imPath="";
			//CA("imPath : " + imPath);
			imPath.Append(assetInfoObj.imageVector1[libSelectionList[0]]);
			//CA("imPath : " + imPath);

			IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
			IDataBase* db = ::GetDataBase(doc); 
			if (db == nil){
				//LogPtr->LogError("Databse is not found IN Selection obsever db is NIL");			
				return kFalse;
			}

			IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&imPath));	
			InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
			if(!importCmd){ 
				//LogPtr->LogError("IN Selection obsever cant import the image");			
				return kFalse;
			}
				
			//*********** Commented For Cs3 Depricated

			//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
			//if(!importFileCmdData){
			//	CA("!importFileCmdData DCNSelectionObserver::ButtonDblClick");
			//	return kFalse;
			//}			
			//importFileCmdData->Set(db, sysFile, kMinimalUI);

			//********************
				
			//**** Added By Sachin Sharma  cs4
			URI tmpURI;
			Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
			InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
			if (importFileCmdData == nil) {
				CA(" importResourceCmdData == nil ");
				return kFalse;
			}	
			importFileCmdData->Set(db,tmpURI,kMinimalUI);
			//**


			ErrorCode err = CmdUtils::ProcessCommand(importCmd);
			if(err != kSuccess) 
				return kFalse;

			InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
			if(!placeGun)
				return kFalse;
			
			//UIDRef placedItem(db, placeGun->GetItemUID());//Commented By Sachin Sharma  
			UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());//Added

			InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
			if (replaceCmd == nil)
				return kFalse;

			InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
			if(!iRepData)
				return kFalse;
		//CA("2");	
			iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);

			ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
			if(status==kFailure)
				return kFalse;
			
			txtMisSuite->setFrameUser(3);

		}while(kFalse);
	}



	return kTrue;
}

bool16 DCNSelectionObserver::KeyDown(IEvent* e)
{  
		//CA("KeyDown");
	bool16 result = kFalse;
	////////// Code to bypass the Evenhandling while Selection is present on Document By Dattatray on 24/08
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(iSelectionManager)
	{	
		
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(txtMisSuite)
	{	
		//CA("My Suit nil");
		return kFalse; 
	}

	
	if(e == nil)
	{
		//CA("No Event");
		return kFalse;
	}
	const SysChar	systemkey = e->GetChar();
	PMString f;
	//Apsiva 9
	//switch(systemkey){
	//case kUpArrowKey: //CA("kUpArrowKey");
	//	        	result=DoUP(1);
	//				break;
	//case kDownArrowKey: // CA("kDownArrowKey");
	//				result=DoDown(1);						
	//				  break;
	//case kPageUpKey:	
	//				result=DoUP(8);
	//					break;
	//case kPageDownKey:  
	//				result=DoDown(8);
	//					break;
	//default :		
	//				result=kFalse;	
	//				break;
	//}
			return kFalse;	
		 //return CEventHandler::KeyDown(e);
}
bool16 DCNSelectionObserver::DoUP(int32 counter)
{
		//CA(__FUNCTION__);
		/////////Added By Dattatray on 24/08
		bool16 result=kFalse;
		PMString dataFile;
		PMString data;
		int32 imgRowCount1 =0;
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//CS4
		if(app == NULL) 
		{ 
			//CA("No Application");
			return kFalse; 
		}
		//Commented By Sachin sharma
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		//if(paletteMgr == NULL) 
		//{ 
		//	//CA("No IPaletteMgr");	
		//	return kFalse; 
		//}
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{	
			//CA("No IPanelMgr");	
			return kFalse; 
		}
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(!myPanel) 
		{
			//CA("No PnlControlView66");
			return kFalse; 
		}
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(!panelControlData) 
		{
			//CA("No PanelControlData");
			return kFalse; 
		}
		IControlView* listPanelView = panelControlData->FindWidget( kDCNlstPnlWidgetID);
		if(listPanelView == NULL)
		{
			//CA("No listPanelView");
			return kFalse; 
		}
		IControlView* listBox = panelControlData->FindWidget( kDCNlstviewListboxWidgetID);
		if(listBox == NULL)
		{
			//CA("No listBox");
			return kFalse; 
		}
		IControlView* listImageView = panelControlData->FindWidget( kDCNListViewImageWidgetID);
		if(listImageView == NULL)
		{
			//CA("No listImageView");
			return kFalse; 
		}

		IControlView* textControlView =panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
		if(textControlView == NULL)
		{
			//CA("No textControlView");
			return kFalse; 
		}

		InterfacePtr<ITextControlData> txtData(textControlView,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			return kFalse; 
		}
		////////
		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		if(listCntl == nil) 
		{
			
			return kFalse; 
		}
		int32 getIndex =listCntl->GetSelected(); 
		int32 totalItem=listCntl->GetMaximumVisibleItems();
		listCntl->DeselectAll();
		SDKListBoxHelper listHelper(listPanelView,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
		int32 elementCount=listHelper.GetElementCount();
		
	/*	PMString ASD("getIndex : ");
		ASD.AppendNumber(getIndex);
		CA(ASD);*/

		if(counter == 1)
			imgRowCount1=getIndex-counter;
		else
			imgRowCount1=getIndex-counter;
		if(imgRowCount1<0)
			imgRowCount1=0;

		/*PMString ASD("getIndex : ");
		ASD.AppendNumber(getIndex);
		ASD.Append("  imgRowCount1 : ");
		ASD.AppendNumber(imgRowCount1);
		CA(ASD);*/
		listCntl->Deselect(getIndex,kTrue,kTrue);
		listCntl->Select(imgRowCount1,kTrue,kTrue); 
		K2Vector<PMString> ::iterator itr;
		K2Vector<PMString> ::iterator itrone;
		itrone = assetInfoObj.imageVector1.begin();
		itr = assetInfoObj.typeName.begin();
		int32 select=0;
		while(itr != assetInfoObj.typeName.end() || itrone != assetInfoObj.imageVector1.end() )
		{
			if(select == imgRowCount1)
			{
				 dataFile = *itrone;
				 data = *itr;
				 break;
			}
			itrone++;
			itr++;
			select++;
		}
		
		SDKFileHelper fileHelper(dataFile);
		IDFile xFile = fileHelper.GetIDFile();
		InterfacePtr<IPMStream> iDataFileStream(StreamUtil::CreateFileStreamReadLazy(xFile));
		if (iDataFileStream == nil)
		{	
			//CA("canNot open");
			return kTrue;
		}
		InterfacePtr<ISysFileData>iSysFileData(listImageView, IID_ISYSFILEDATA);
		if(!iSysFileData)
		{	
			//CA("Syts Nil");
			return kTrue;
		}
		iSysFileData->Set(xFile);
		int ins =  data.IndexOfString("  ");
		PMString* NewString = data.Substring(ins +1 );
		NewString->SetTranslatable(kFalse);
        NewString->ParseForEmbeddedCharacters();
		txtData->SetString( *NewString);
		listImageView->Invalidate();
		return kTrue;
}
bool16 DCNSelectionObserver::DoDown(int32 counter)
{

		//CA(__FUNCTION__);
		bool16 result=kFalse;
		PMString dataFile;
		PMString data;
		int32 imgRowCount;
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//cs4
		if(app == NULL) 
		{ 
			//CA("No Application");
			return kFalse; 
		}
		//Commented By Sachin sharma 
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		//if(paletteMgr == NULL) 
		//{ 
		//	//CA("No IPaletteMgr");	
		//	return kFalse; 
		//}
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{	
			//CA("No IPanelMgr");	
			return kFalse; 
		}
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(!myPanel) 
		{
			//CA("No PnlControlView77");
			return kFalse; 
		}
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(!panelControlData) 
		{
			//CA("No PanelControlData");
			return kFalse; 
		}
		IControlView* listPanelView = panelControlData->FindWidget( kDCNlstPnlWidgetID);
		if(listPanelView == NULL)
		{
			//CA("No listPanelView");
			return kFalse; 
		}
		IControlView* listBox = panelControlData->FindWidget( kDCNlstviewListboxWidgetID);
		if(listBox == NULL)
		{
			//CA("No listBox");
			return kFalse; 
		}
		

		IControlView* listImageView = panelControlData->FindWidget( kDCNListViewImageWidgetID);
		if(listImageView == NULL)
		{
			//CA("No listImageView");
			return kFalse; 
		}

		IControlView* textControlView =panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
		if(textControlView == NULL)
		{
			//CA("No textControlView");
			return kFalse; 
		}
		InterfacePtr<ITextControlData> txtData(textControlView,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			return kFalse; 
		}
		////////
		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		if(listCntl == nil) 
		{
			return kFalse; 
		}
		int32 getIndex =listCntl->GetSelected(); //Get Currently selected element index
		int32 totalItem=listCntl->GetMaximumVisibleItems();
		listCntl->DeselectAll();
		SDKListBoxHelper listHelper(listPanelView,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
		int32 elementCount=listHelper.GetElementCount();//Get total element counrt in the listbox
		if(counter == 1)
		 {	
			imgRowCount=getIndex + counter;//For DownKey
			if(imgRowCount >= elementCount)
				imgRowCount=elementCount-1;
		 }
		else
		{
			imgRowCount=getIndex + 7 ;//For Pageup
				if(imgRowCount > elementCount)
				imgRowCount=elementCount-1;//If it goes beyond the total element count in the listbox
		}
		 
		
		listCntl->Select(imgRowCount,kTrue,kTrue); 
		//It is For single image of currently selected element and multiline text widget.
		K2Vector<PMString> ::iterator itr;
		K2Vector<PMString> ::iterator itrone;
		itr = assetInfoObj.typeName.begin();
		itrone = assetInfoObj.imageVector1.begin();
		int32 select=0;
		while(itr != assetInfoObj.typeName.end() || itrone != assetInfoObj.imageVector1.end() )
		{
			if(select == imgRowCount)
			{
				 dataFile = *itrone;
				 data = *itr;
				 break;
			}
			itrone++;
			itr++;
			select++;
		}
		SDKFileHelper fileHelper(dataFile);
		IDFile xFile = fileHelper.GetIDFile();
		InterfacePtr<IPMStream> iDataFileStream(StreamUtil::CreateFileStreamReadLazy(xFile));
		if (iDataFileStream == nil)
		{	
			//CA("canNot open");
			return kTrue;
		}
		iDataFileStream->Close();
		InterfacePtr<ISysFileData>iSysFileData(listImageView, IID_ISYSFILEDATA);
		if(!iSysFileData)
		{	
			//CA("Syts Nil");
			return kTrue;
		}
		iSysFileData->Set(xFile);
		
		if(data.IsEmpty() == kFalse)
		{
			int ins =  data.IndexOfString("  ");
			PMString* NewString = data.Substring(ins +1 );
			NewString->SetTranslatable(kFalse);
            NewString->ParseForEmbeddedCharacters();
			txtData->SetString( *NewString);
		}
		listImageView->Invalidate();
		return kTrue;		
	
}
