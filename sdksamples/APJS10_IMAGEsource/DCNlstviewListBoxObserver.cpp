#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "DCNID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include <K2Vector.h> 
#include "IListControlData.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IApplication.h"
#include "IApplication.h"
//#include "IPaletteMgr.h" //Commented By Sachin Sharma on 23/06/07
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "ISysFileData.h"
#include "IDFile.h"
#include "SDKFileHelper.h"
#include "IBoolData.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"

#include "AssetInfo.h"


#define CA(x)	CAlert::InformationAlert(x)
//extern K2Vector<PMString> imageVector1 ;
//extern K2Vector<PMString> imagefileName;
PMString imPath;
K2Vector<int32> curSelection ;
//extern K2Vector<PMString> typeName;
extern PMString data;

extern AssetInfo assetInfoObj;

IControlView* listImageCtrView =NULL;

//PMString dataFile("C:\\a\\two.jpg");
class DCNlstviewListBoxObserver : public CObserver
{
public:
	DCNlstviewListBoxObserver(IPMUnknown *boss);
	~DCNlstviewListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
private:
	IEventHandler* piEventHandler;
};

CREATE_PMINTERFACE(DCNlstviewListBoxObserver, kDCNlstviewListBoxObserverImpl)

DCNlstviewListBoxObserver::DCNlstviewListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

DCNlstviewListBoxObserver::~DCNlstviewListBoxObserver()
{
}

void DCNlstviewListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}

	InterfacePtr<IApplication> piApp(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	InterfacePtr<IEventDispatcher> piEventDispatcher(piApp, UseDefaultIID());
	this->piEventHandler = (IEventHandler*) ::CreateObject(kDCNActiveSelectionBoss,IID_IEVENTHANDLER);
	this->piEventHandler->AddRef();
	piEventDispatcher->Push(this->piEventHandler );
}

void DCNlstviewListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);  
		
	}
	InterfacePtr<IApplication> piApp(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	InterfacePtr<IEventDispatcher> piEventDispatcher(piApp, UseDefaultIID());
			// Remove it from the event handler stack
	piEventDispatcher->Remove(this->piEventHandler);
			// Decrease refcount
	this->piEventHandler->Release();
	this->piEventHandler = nil;
	//CA("TPLProjectListBoxObserver Detach");
}

void DCNlstviewListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			

			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
				break;

			SDKListBoxHelper listHelper(this, kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
			IControlView* DCNLstboxCntrlView= listHelper.FindCurrentListBox();
			if(DCNLstboxCntrlView== nil) 
				break;

			InterfacePtr<IListBoxController> listCntl(DCNLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				break;

			
			listCntl->GetSelected( curSelection ) ;
			
		///////////////////////////////////////

				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
				if(app == NULL) 
				 { 
					//CA("No Application");
					break;
				 }
				 //Commented By Sachin sharma On 23/06/07
				//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager());//QueryPaletteManager()); Modified By Sachin sharma On 23/06/07
				//if(paletteMgr == NULL) 
				//{ 
				////	CA("No IPaletteMgr");	
				//	break;
				//}
				//
				//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{	
					//CA("No IPanelMgr");	
					break;
				}
				
				//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
				IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
				if(!myPanel) 
				{
					//CA("No PnlControlView44");
					break;
				}
				
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("No PanelControlData");
					break;
				}
				
				IControlView* Widget =panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
				if(Widget == NULL)
				{
					//CA("No ControlView");
					break;
				}
				
				IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
				if(listImageView == NULL)
				{
					//CA("No listImageView");
					break;
				}
				listImageCtrView =listImageView;
				InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
				//ASSERT(iImageSysFile);
				if(!iImageSysFile)
				{
					return;
				}
				InterfacePtr<ITextControlData> txtData(Widget,IID_ITEXTCONTROLDATA);
				if(txtData == nil)
				{
					//CA("no txtData");
					return;
				}

			//////////////////////////////////////////////////////////////////////////////////////////////
			const int kSelectionLength =  curSelection.size() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				curSelRowIndex=curSelection[0];
				int32 select =0;
				PMString autosel;
				K2Vector<PMString> ::iterator itr;
				K2Vector<PMString> ::iterator itrone;
				itr = assetInfoObj.typeName.begin();
				itrone = assetInfoObj.imageVector1.begin();
				while(itr != assetInfoObj.typeName.end() || itrone != assetInfoObj.imageVector1.end() )
				{
				
					if(select == curSelRowIndex)
					{
						autosel = *itr;

						PMString dataFile = *itrone;
						
						int ins =  autosel.IndexOfString("  ");

						PMString* NewString = autosel.Substring(ins +1 );
						//CA(*NewString);
						NewString->SetTranslatable(kFalse);
                        NewString->ParseForEmbeddedCharacters();
						txtData->SetString(*NewString);
						//CA("autosel"+autosel);
						SDKFileHelper fileHelper(dataFile);
						//CA("dataFile"+dataFile);
						IDFile xFile = fileHelper.GetIDFile();
						listImageCtrView->Validate();

						iImageSysFile->Set(xFile);

						listImageCtrView->Invalidate();

						break;
					}
					itr++;
					itrone++;
					select++;
				}
			}
			
			//	CA("6");		
		} while(0);
	
	}

		
		
		
	


}

