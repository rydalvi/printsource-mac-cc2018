#include "VCPlugInHeaders.h"

//interfaces
#include "ITip.h"
//#include "ITool.h"
#include "IControlView.h"
#include "IWidgetUtils.h"
#include "IXLibraryButtonData.h"

//includes
#include "PMString.h"
#include "Trace.h"
#include "HelperInterface.h"
#include "StringUtils.h"

#include "DCNID.h"

class MyTip : public ITip
{ 
	public: 
		MyTip(IPMUnknown *boss); 
		virtual ~MyTip(); 
		virtual PMString GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/); 
		virtual bool16  UpdateToolTipOnMouseMove(); 
		DECLARE_HELPER_METHODS()
}; 

CREATE_PMINTERFACE( MyTip,kMyTipImpl )
DEFINE_HELPER_METHODS( MyTip )

MyTip::MyTip(IPMUnknown *boss)
	:HELPER_METHODS_INIT(boss)
{
}

MyTip::~MyTip()
{
}

PMString MyTip::GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/) 
{ 
	InterfacePtr<IXLibraryButtonData> buttonData(this, IID_IXLIBRARYBUTTONDATA);
	ASSERT(buttonData);		if (!buttonData)	return PMString();

	return buttonData->GetName();
} 


bool16  MyTip::UpdateToolTipOnMouseMove()
{

	return kFalse;
}