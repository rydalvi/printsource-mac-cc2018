#include "VCPlugInHeaders.h"
#include "IAppFramework.h"
#include "ICoreFilename.h"
#include "SDKUtilities.h"
#include "ICoreFilenameUtils.h"
#include "CAlert.h"
#include "DCNID.h"
#include "CAlert.h"
#include "PRImageHelper.h"
#include "IPanelControlData.h"
#include "IClientOptions.h"
#include "SDKListBoxHelper.h"
#include "K2Vector.h" 
#include "FileUtils.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IListBoxController.h"
#include "ILibrary.h"
#include "LibraryProviderID.h"
#include "IDFile.h"
#include "LocaleSetting.h"
#include "IXLibraryViewController.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "PMString.h"
#include "ISysFileData.h"
#include "SDKFileHelper.h"
#include "IListBoxController.h"
#include "IImageFormatManager.h"
#include "StreamUtil.h"
#include "IImageReadFormat.h"
#include "IImageAttributes.h"
#include "IImportProvider.h"
#include "PaletteRef.h"

#include "AssetInfo.h"

#include "ILoginHelper.h"
#include "CDialogController.h"

#ifdef MACINTOSH
#include "MacFileUtils.h"
#include "MFileUtility.h"
#endif

//extern IAppFramework* ptrIAppFramework;
//extern IClientOptions* ptrIClientOptions;

using namespace std ;
#define CA(x) CAlert::InformationAlert(x)
#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

extern IPanelControlData *global_panel_control_data ;
extern ITextControlData* textControlDataPtr;
extern bool16 BrowseFolderOption ;
extern bool16 isBrowsePrintSource;
extern bool16 isShowPVImages; ///////////////param
extern bool16 isShowPartnerImages;

//K2Vector<PMString> imageType ;  //
//K2Vector<PMString> vectorImageTypeTagName;//
//K2Vector<int32> imageTypeID ;//
//K2Vector<int32> imageMPVID ;//
//K2Vector<PMString> imageVector1 ;//
//K2Vector<PMString> imagefileName;//
//K2Vector<PMString> typeName;//
//K2Vector<int32> itemIDS ;//

K2Vector<bool16> imageBoolFlagVector;
//PMString  imageType_ ;
double global_lang_id ;
double global_section_id ;
double global_parent_id ;
double global_parent_type_id ;
double globle_item_parenttype_id;
int32 global_item_start_position ;

double global_product_id ;
double global_product_type_id ;

PMString imagePath("");
//PMString useName("");

extern bool16 isThumb ;
int32 index1 =0;      //Cs4    ...CS3 ===== index ==index1
//Used to call showProducImages and ShowItem Images in DCNActionComponent
double objId=0;
double langaugeId=0;
double parId=0;
double parTypeId=0;
double sectId=0;
int32 isProd=0;
IPRImageHelper* prImage = NULL;

vector<double> attributesIDSWithPVMPV;
vector<double> elementIDSWithPVMPV;

bool16 isProductImageAdded = kFalse;
IControlView* globalPanelControlView=NULL;

AssetInfo assetInfoObj;

namespace
{
	bool16 fileExists(PMString& path, PMString& name)
	{
		PMString theName("");
		theName.Append(path);
		theName.Append(name);

        #ifdef MACINTOSH
        {
            InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
            if(ptrIAppFramework == NULL)
            {
                //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
                return kFalse;
            }
            
            ptrIAppFramework->getUnixPath(theName);
        }
        #endif
        
        std::string tempString(theName.GetPlatformString());
        const char *file= const_cast<char *> (tempString.c_str());
        //CA("theName  :  "+theName);
        FILE *fp=NULL;
        
        fp=std::fopen(file, "r");
        if(fp!=NULL)
        {
            std::fclose(fp);
            return kTrue;
        }
        return kFalse;
	}
}

class PRImageHelper : public CPMUnknown<IPRImageHelper>
{
public :
	PRImageHelper();
	PRImageHelper(IPMUnknown*  boss);
	~PRImageHelper();
	virtual void showImagePanel() ;
	virtual void showProductImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID) ;
	virtual void positionImagePanel(int32 left , int32 righ , int32 top , int32 bottom);
	virtual void closeImagePanel();
	virtual void showItemImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID, int32 isProduct);
	virtual bool16 isImagePanelOpen();
	IPMUnknown* boss_ ;
	virtual bool16 EmptyImageGrid();
	//added by vijay on 19/8/2006	
	virtual void WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName);

	virtual void setattributesWithPVMPV(vector<double>temp,bool16 isProduct);
	virtual bool16 isAtt_EleIDSVecFilled();
private:
	 void AddThumbnailImage(PMString path, PMString DisplayName);
	 bool16 EmptyThumbnailImageGrid();


};

CREATE_PMINTERFACE(PRImageHelper , kProductImageIFaceImpl)

PRImageHelper::PRImageHelper(IPMUnknown* boss):CPMUnknown<IPRImageHelper>(boss){
	boss_ = boss ;
	}

PRImageHelper::~PRImageHelper(){}

void PRImageHelper :: showImagePanel()
{
	//CA(__FUNCTION__);
	do{
		prImage =this;
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication == nil)
		{
			//CA("No iApplication");		
			break ;
		}
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());//QueryPaletteManager());Modified By sachin Sharma on 23/06/07
		//if(iPaletteMgr == nil)
		//	break ;
		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr , UseDefaultIID()); 
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == NULL)
		{
			//CA("No iPanelMgr");
			break ;
		}

		////	------------- show panel ---------------- 
		//iPanelMgr->ShowPanelByWidgetID(kDCNPanelWidgetID);
		////IControlView *icontrolView = iPanelMgr->GetVisiblePanel(kDCNPanelWidgetID); 
		//bool16 shownStatus = iPanelMgr->DoesPanelExist(kDCNPanelWidgetID);
		//if(shownStatus == kTrue)
		//	CA("Shown");
		//else
		//	CA("Not shown");

		const ActionID MyPalleteActionID = kDCNPanelWidgetActionID;
		iPanelMgr->ShowPanelByMenuID(MyPalleteActionID);
		
//For Getting PanelControlView
		uint32 numPanels = iPanelMgr->GetPanelCount();
		for (int32 i = 0 ; i < numPanels ; i++)
		{
			ActionID actionID;
			UID	panelUID;
			WidgetID	widgetID;
			PMString	panelName;
			ScriptID	scriptID;
			bool16 gotPanelInfo = kFalse;
			
			gotPanelInfo = iPanelMgr->GetNthPanelInfo(i, panelUID, &actionID, &widgetID, &panelName, &scriptID);

			if(gotPanelInfo && (actionID == MyPalleteActionID) )
			{
				globalPanelControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
				//if(globalPanelControlView == NULL)
					//CAlert::InformationAlert("still i m not getting");
			}
		}
//--------------
		//iPanelMgr->SetPanelResizabilityByWidgetID(kDCNPanelWidgetID,kTrue); 
		//IControlView *icontrolView = iPanelMgr->GetVisiblePanelFromActionID(MyPalleteActionID); 
		//IControlView *icontrolView = iPanelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		if(globalPanelControlView)
		{
			//globalPanelControlView->Resize(PMPoint(PMReal(/*207*//*600*/400) , PMReal(291/*450*/)));
		}
		else
		{
			//CA("icontrolView of a panel is  nil");
			//break;
		}
		//Added By Dattatray to show default thumview
		if(isThumb == kTrue)
		{
			//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager());//QueryPaletteManager());Modified By Sachin sharma on 23/06/07
			//if(paletteMgr == NULL) 
			//{ 
			//		
			//	break;
			//}
			//
			//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
			/*IControlView* myPanel = iPanelMgr->GetVisiblePanel(kDCNPanelWidgetID);
			if(!myPanel) 
			{
				CA("No PnlControlView");
				break;
			}*/
			
			InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
			if(!panelControlData) 
			{
				//CA("No PanelControlData");
				break;
			}
			
			IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
			//IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstviewListboxWidgetID);
			if(lstgridWidget == NULL)
			{
				//CA("No ListControlView");
				break;
			}
			//iPanelMgr->HidePanelByWidgetID(kDCNlstPnlWidgetID);
			lstgridWidget->HideView();//tO hide the ListBox And So It will Show ImagePanel
			/*IControlView* Widget = panelControlData->FindWidget( kDCNPanelWidgetID);
			if(Widget == NULL)
			{
				//CA("No ListControlView");
				break;
			}*/
			//Widget->ShowView();
			globalPanelControlView->ShowView();
			IControlView* thumbWidget = panelControlData->FindWidget( kDCNThumbPnlWidgetID);
			if(thumbWidget == NULL)
			{
				//CA("No thumbControlView");
				break;
			}
			thumbWidget->ShowView();
			//iPanelMgr->ShowPanelByWidgetID(kDCNThumbPnlWidgetID);
		}
		
		//---------
		InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
		if(ptrLogInHelper == nil)
		{
			//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
			return ;
		}
		
		LoginInfoValue cserverInfoValue ;
		bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;

		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
		int32 disPartnerImgVal = clientInfoObj.getDisplayPartnerImages();
		int32 disPickListImgVal = clientInfoObj.getDisplayPickListImages();


		if(disPartnerImgVal  == 1)
		{
			isShowPartnerImages = kTrue;
			if(isProd == 1)
				this->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				this->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);
		}
		else
		{
			isShowPartnerImages = kFalse;
		}

		if(disPickListImgVal  == 1)
		{
			isShowPVImages = kTrue;
			if(isProd == 1)
				this->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
			else
				this->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);
		}
		else
		{
			isShowPVImages = kFalse;
		}


	}
	while(kFalse);
}


void PRImageHelper :: showProductImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID )
{
	BrowseFolderOption = kFalse;
	isBrowsePrintSource = kTrue;
	//CA(__FUNCTION__);
	isProd = 1;
	objId=objectID;
	langaugeId=langID;
	parId=parentID;
	parTypeId=parentTypeID;
	sectId=sectionID;
	isProductImageAdded = kFalse;
	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	if(app == NULL)
		return;
	
	//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager());//QueryPaletteManager()); Modified By Sachin sharma on 23/06/07
	//if(paletteMgr == NULL)
	//	return;
	//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL)
	{
		//CA("showProductImages::panelMgr is NULL");
		return;
	}
	/*IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
	if(myPanel == NULL)
	{
		//CA("showProductImages::myPanel is NULL");	
		return;
	}*/

	if(globalPanelControlView == NULL)
	{
		const ActionID MyPalleteActionID = kDCNPanelWidgetActionID;
		uint32 numPanels = panelMgr->GetPanelCount();
		for (int32 i = 0 ; i < numPanels ; i++)
		{
			ActionID actionID;
			UID	panelUID;
			WidgetID	widgetID;
			PMString	panelName;
			ScriptID	scriptID;
			bool16 gotPanelInfo = kFalse;
			
			gotPanelInfo = panelMgr->GetNthPanelInfo(i, panelUID, &actionID, &widgetID, &panelName, &scriptID);

			if(gotPanelInfo && (actionID == MyPalleteActionID) )
			{
				globalPanelControlView = panelMgr->GetPanelFromActionID(MyPalleteActionID);
				//if(globalPanelControlView == NULL)
					//CAlert::InformationAlert("still i m not getting");
			}
		}
	}

	InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
	if(panelControlData == NULL)
	{
		//CA("showProductImages::panelControlData is NULL");	
		return;
	}
	IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
	if(lstgridWidget == NULL)
	{
		//CA("showProductImages::lstgridWidget is NULL");		
		return;
	}
	IControlView* textView = panelControlData->FindWidget( kDCNMultilineTextWidgetID);
	if(textView == NULL)
	{
		//CA("showProductImages::textView is NULL");		
		return;
	}
	IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
	if(listImageView == NULL)
	{
		//CA("showProductImages::listImageView is NULL");	
		return;
	}
	InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
	if(!iImageSysFile)
	{
		//CA("showProductImages::iImageSysFile is NULL");	
		return;
	}
	InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
	if(textPtr == NULL)
	{
		//CA("showProductImages::textPtr is NULL");		
		return;
	}
	PMString blank("");
	blank.SetTranslatable(kFalse);
	textPtr->SetString(blank);


	IControlView* textView1 = panelControlData->FindWidget( kDCNFileSizeTextWidgetID);
	if(textView == NULL)
	{
		//CA("showProductImages::textView is NULL");		
		return;
	}
	InterfacePtr<ITextControlData> textPtr1( textView1 ,IID_ITEXTCONTROLDATA);
	if(textPtr == NULL)
	{
		//CA("showProductImages::textPtr is NULL");		
		return;
	}
	textPtr1->SetString(blank);

	//SDKListBoxHelper listHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
	//listHelper.EmptyCurrentListBox();
	//...............................
	//IControlView* listBoxControlView = listHelper.FindCurrentListBox();
	//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
	//	ASSERT_MSG(listCntl != nil, "listCntl nil");
	//	if(listCntl == nil) {  
	//		//CA("showProductImages::listCntl is NULL");	
	//		return;
	//}
	this->EmptyImageGrid();
	
	//imageType.clear();
	//vectorImageTypeTagName.clear();
	//imageTypeID.clear();
	//imageMPVID.clear();
    //imageVector1.clear();
	//itemIDS.clear();
	assetInfoObj.clearAll();

	imageBoolFlagVector.clear();

	global_lang_id = langID ;
	global_section_id = sectionID ;
	global_parent_id = parentID ;
	global_parent_type_id = parentTypeID ;
	global_product_id =  objectID ; 
		index1=0;  //cs3 === index

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions == nil)
	{
		CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return ;
	}
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return ;
		
	//ptrIAppFramework->clearAllStaticObjects();
	//VectorTypeInfoPtr typeValObj1 = ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
	//if(typeValObj1 == nil)
	//return ;
	//int32 size = typeValObj1->size() ;
    
	int32 count = 0 ;
	VectorTypeInfoValue :: iterator it2 ;
	bool16 Flag1stSelected = kFalse; 

	//for(it2 = typeValObj1->begin() ; it2 != typeValObj1->end() ; it2++)
	//{	
		//int32 Imagetypeid = it2->getType_id() ;				
		double objectId =  objectID ;			
		//CObjectValue oVal ;
		//oVal = ptrIAppFramework->GETProduct_getObjectElementValue(objectId);  // new added in appFramework
		//int32 parentTypeID = oVal.getObject_type_id();

		PMString ImageTypeName("") ;
		//Apsiva9 Comment
		//ImageTypeName.Append(ptrIAppFramework->GETProduct_getObjectAndObjectValuesByObjectId(objectID)) ;
		//if(ImageTypeName=="")
		//	return;	

		PMString imagePath = ptrIClientOptions->getImageDownloadPath();
		if(imagePath != ""){
			//CA(imagePath);
			const char *imageP=imagePath.GetPlatformString().c_str();
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
		}

		if(imagePath == "")
			return ;			
		
		bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
		if(isPresent==FALSE)
		{
			this->closeImagePanel();
			return;
		}


		do
		{			
			//VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,parentTypeID,Imagetypeid);
			int32 isProducts  = 1; // for Item Group Images
			double typeId = -1; // To grab all images under Item Group
			VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId, sectionID, isProducts, typeId  );
			if(AssetValuePtrObj == NULL)
				continue ;

			if(AssetValuePtrObj->size() ==0)
				continue ;			

			//global_product_type_id = Imagetypeid ;
			//PMString typIDString("");
			//typIDString.AppendNumber(Imagetypeid);			
			//imageTypeID.push_back(Imagetypeid);
		
			//ImageTypeName.Append(" : ") ;
			//ImageTypeName.Append(it2->getName());
			//PMString ImageTypeName_ = it2->getName();
			
			int imageTextIndex = 0;			
			PMString fileName("");
			double assetID ;	
			//PMString imagePath = ptrIClientOptions->getImageDownloadPath();
			//if(imagePath != ""){
			//	//CA(imagePath);
			//	char *imageP=imagePath.GrabCString();
			//	if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			//		#ifdef MACINTOSH
			//			imagePath+="/";
			//		#else
			//			imagePath+="\\";
			//		#endif
			//}

			//if(imagePath == "")
			//	return ;			
			//
			//bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
			//if(isPresent==FALSE)
			//{
			//	this->closeImagePanel();
			//	return;
			//}
			

			
			//CA("imagePathWithSubdir"+imagePathWithSubdir);
			VectorAssetValue::iterator it; // iterator of Asset value
			CAssetValue objCAssetvalue;
			for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
			{
				objCAssetvalue = *it;
				fileName = objCAssetvalue.geturl();
				//CA("fileName"+fileName);
				assetID = objCAssetvalue.getAsset_id();					

				if(fileName=="")
				continue ;

				double ImageTypeId = objCAssetvalue.getType_id();
				double image_mpv = objCAssetvalue.getMpv_value_id();
						
				PMString TypeName = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(ImageTypeId);
				//CA("TypeName : "+TypeName);
				PMString NewImageNameString("");
				NewImageNameString.Append(ImageTypeName);
				NewImageNameString.Append(" : ") ;
				NewImageNameString.Append(TypeName);
				PMString ImageTypeName_ = TypeName;

				//CA("fileName : "+ fileName);
				//imageType_ = fileName;
				do{
					SDKUtilities::Replace(fileName,"%20"," ");
				}while(fileName.IndexOfString("%20") != -1);

				PMString imagePathWithSubdirNew(""); 
				imagePathWithSubdirNew.Append(imagePath);
				PMString typeCodeString;

				typeCodeString = ""; //ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(assetID);
				//CA("typeCodeString"+typeCodeString );				
				//Following three lines are commented and instead of that, code following that is added.
				/*if(typeCodeString.NumUTF16TextChars()!=0)
					imagePathWithSubdir.Append(typeCodeString);	

				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));*/

				if(typeCodeString.NumUTF16TextChars()!=0)
				{ 
				
					PMString TempString = typeCodeString;		
					CharCounter ABC = 0;
					bool16 Flag = kTrue;
					bool16 isFirst = kTrue;
					do
					{
						
						if(TempString.Contains("/", ABC))
						{
			 				ABC = TempString.IndexOfString("/"); 				
						
		 					PMString * FirstString = TempString.Substring(0, ABC);		 		
		 					PMString newsubString = *FirstString;		 	
						 				 	
		 					if(!isFirst)
		 					{
								#ifdef MACINTOSH 
									imagePathWithSubdirNew.Append(":");
								#else
									imagePathWithSubdirNew.Append("\\");
								#endif
							}		 					
		 					 					
		 					imagePathWithSubdirNew.Append(newsubString);
							if(ptrIAppFramework->getAssetServerOption() == 0)
			 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);
						 	
		 					isFirst = kFalse;
						 	
		 					PMString * SecondString = TempString.Substring(ABC+1);
		 					PMString SSSTring =  *SecondString;		 		
		 					TempString = SSSTring;	
							if(FirstString)
								delete FirstString;
							if(SecondString)
								delete SecondString;
						}
						else
						{				
							if(!isFirst){						
								
								#ifdef MACINTOSH							
									imagePathWithSubdirNew.Append(":");
								#else
									imagePathWithSubdirNew.Append("\\");
								#endif	
							}
			 					
		 					imagePathWithSubdirNew.Append(TempString);
							if(ptrIAppFramework->getAssetServerOption() == 0)
			 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);		 		
		 					isFirst = kFalse;				
							Flag= kFalse;
							
							//CA(" 2 : "+ imagePathWithSubdir);
						}				
						
					}while(Flag);
				}

				if(typeCodeString != "")
					SDKUtilities ::AppendPathSeparator(imagePathWithSubdirNew);
				//CA("imagePathWithSubdir"+imagePathWithSubdir + " fileName : "+ fileName);
				
				//if(!fileExists(imagePathWithSubdirNew,fileName))
				//{	
				//
				//	if(ptrIAppFramework->getAssetServerOption() == 0)
				//	{
				//		//if(!ptrIAppFramework->GETAsset_downLoadProductAsset(assetID,imagePathWithSubdirNew))
				//		//{	
				//		//	//CA("No fileExists");
				//		//	continue ;
				//		//}
				//	}
				//}
				//CA(imagePathWithSubdir);
				
                if(!fileExists(imagePathWithSubdirNew,fileName)){
					//CA("No FileExixts");
					continue ;
				}

				PMString ImagePath = imagePathWithSubdirNew + fileName;
				
                #ifdef MACINTOSH
                    SDKUtilities::convertToMacPath(ImagePath);
                #endif
				//CA("ImagePath" +ImagePath );
				assetInfoObj.imageMPVID.push_back(image_mpv);//imageMPVID.push_back(image_mpv);
				assetInfoObj.imageTypeID.push_back(ImageTypeId);//imageTypeID.push_back(ImageTypeId);

				assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.
				
				//PMString useName("FileName: "+ fileName + "  ");
				
				PMString useName(fileName + "  ");
				useName.Append("ImageType: " + ImageTypeName);
				useName.Append(NewImageNameString);
				assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);//Added By Dattatray
				
				//IControlView* lstboxControlView = panelControlData->FindWidget(kImageListboxWidgetIDM);
				
				//CA("ImagePath::"+ImagePath);
				
				assetInfoObj.imageVector1.push_back(ImagePath);//imageVector1.push_back(ImagePath);
				assetInfoObj.imageType.push_back(NewImageNameString);//imageType.push_back(NewImageNameString);
				assetInfoObj.itemIDS.push_back(-1);//itemIDS.push_back(-1); // ADDDING -1 BECAUSE THIS IS PRODUCT , IT CAN NOT HAVE ITEM ID 
				assetInfoObj.vectorImageTypeTagName.push_back(ImageTypeName_);//vectorImageTypeTagName.push_back(ImageTypeName_);
				assetInfoObj.attributeID.push_back(-1);
				assetInfoObj.PVImageIndex.push_back(-1);
				
		 		//listHelper.AddElementImage(lstboxControlView ,ImageTypeName ,ImagePath, kPRLCustomPanelViewWidgetIDM , 0,0,1);
				
				//CA("ImagePath for Product : "+ImagePath);
				
				if(isThumb == kTrue)
					this->AddThumbnailImage(ImagePath, NewImageNameString);				
				else
				{	
					//CA("Prod"+fileName);
					//listHelper.AddElement(fileName,kDCNTextWidgetID,index1);
					index1++;
					fileName="";
					//listCntl->Select(0);
					Flag1stSelected = kTrue;
					isProductImageAdded = kTrue;
				}				
				
			}
//12-april
			if(AssetValuePtrObj)
				delete AssetValuePtrObj;
//12-april
		}while(0);
		
		if(isShowPartnerImages)
		{
		//CA("isShowPartnerImages == kTrueproduct");
			do{	///Added for Brand,Manufacture,Supplier Images
				//ptrIAppFramework->clearAllStaticObjects();
				//VectorTypeInfoPtr typeValObj = ptrIAppFramework->GetItemOrProductBMSImageTypes(kTrue);
				//if(typeValObj == NULL){
				//	//CA("typeValObj == NULL");
				//	continue ;
				//}
				//if(typeValObj->size() == 0)
				//{
				//	//CA("typeValObj->size() == 0");
				//	continue;
				//}
				
				PMString imagePathWithSubdir = imagePath;
				
				int32 whichTab = 3; // for Item Group
				double partTypeId = -1;
				for( partTypeId =  -207 ; partTypeId > -223 ;  partTypeId-- )
				{
					CAssetValue PartAssetValue = ptrIAppFramework->getPartnerAssetValueByPartnerIdImageTypeId(objectID, sectionID, langID, partTypeId, whichTab); 
					PMString fileName = PartAssetValue.geturl();
					if(fileName == ""){
						//CA("continue");
						continue ;
					}
					do
					{
						SDKUtilities::Replace(fileName,"%20"," ");
					}while(fileName.IndexOfString("%20") != -1);
					
					//CA("fileName : "+fileName);

					PMString TypeName = PartAssetValue.geturl();
					//CA("TypeName : "+TypeName);

					PMString NewImageNameString("");
					//NewImageNameString.Append(ImageTypeName);
					//NewImageNameString.Append(" : ") ;
					//NewImageNameString.Append(TypeName);
					
					PMString ImageTypeName_ = TypeName;
					
					PMString NewImagePathWithSubDirectory = imagePathWithSubdir;
					
					PMString typeCodeString = ""; //ptrIAppFramework->GetAssets_getAssetFolderForBMSandPubLogo(objectID, cTypeModel.getType_id()); 
					if(typeCodeString.NumUTF16TextChars()!=0)
					{ 
						//CA("typeCodeString : "+typeCodeString);
						PMString TempString = typeCodeString;		
						CharCounter ABC = 0;
						bool16 Flag = kTrue;
						bool16 isFirst = kTrue;
						do
						{					
							if(TempString.Contains("/", ABC))
							{
	 							ABC = TempString.IndexOfString("/"); 				
								
 								PMString * FirstString = TempString.Substring(0, ABC);		 		
 								PMString newsubString = *FirstString;		 	
									 			
 								if(!isFirst){
	
									
									#ifdef MACINTOSH
										NewImagePathWithSubDirectory.Append(":");
									#else	
										NewImagePathWithSubDirectory.Append("\\");
									#endif	
								}
								
 								NewImagePathWithSubDirectory.Append(newsubString);	
								if(ptrIAppFramework->getAssetServerOption() == 0)
		 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);					 		
 								isFirst = kFalse;					 		
 								PMString * SecondString = TempString.Substring(ABC+1);
 								PMString SSSTring =  *SecondString;		 		
 								TempString = SSSTring;	
								if(FirstString)
									delete FirstString;

								if(SecondString)
									delete SecondString;

							}
							else
							{				
								if(!isFirst){
 									#ifdef MACINTOSH
										NewImagePathWithSubDirectory.Append(":");
									#else	
										NewImagePathWithSubDirectory.Append("\\");
									#endif	
								}					
 								NewImagePathWithSubDirectory.Append(TempString);	
								if(ptrIAppFramework->getAssetServerOption() == 0)
	 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);		 		
 								isFirst = kFalse;				
								Flag= kFalse;
								
								//CA(" 2 : "+ imagePathWithSubdir);
							}				
						}while(Flag);
					}
					//if(ptrIAppFramework->getAssetServerOption() == 0)
					//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory));

					//SDKUtilities ::AppendPathSeparator(NewImagePathWithSubDirectory);
					//CA("NewImagePathWithSubDirectory : "+NewImagePathWithSubDirectory +"\rfileName : "+fileName);
					/*if(!fileExists(NewImagePathWithSubDirectory,fileName))
					{
						if(ptrIAppFramework->getAssetServerOption() == 0)
						{
							if(!ptrIAppFramework->GETAsset_downloadProductBMSAsset(objectID, cTypeModel.getType_id(),NewImagePathWithSubDirectory))
							{
								ptrIAppFramework->LogDebug("AP46_DCN41162::PRImageHelper::showItemImages::!GETAsset_downloadItemBMSAsset");				
								continue;
							}
						}
					}*/

					if(!fileExists(NewImagePathWithSubDirectory,fileName))
					{
						//CA("File not exist");
						continue ;
					}
					
					PMString ImagePath = NewImagePathWithSubDirectory + fileName;
                    
                    #ifdef MACINTOSH
                        SDKUtilities::convertToMacPath(ImagePath);
                    #endif

					assetInfoObj.imageVector1.push_back(ImagePath);
				
					//int32 image_mpv = objCAssetvalue.getMpv_value_id();
					
					assetInfoObj.imageMPVID.push_back(-1);

					assetInfoObj.imageTypeID.push_back(partTypeId);

					assetInfoObj.imageType.push_back(NewImageNameString);
					//CA("ImageTypeName_ : "+ImageTypeName_);
					assetInfoObj.vectorImageTypeTagName.push_back(ImageTypeName_);						
					assetInfoObj.itemIDS.push_back(-1);//itemIDS.push_back(*itn);
					assetInfoObj.PVImageIndex.push_back(-1);							
					assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.

					//PMString useName("FileName: "+ fileName + "  ");
					
					PMString useName(fileName + "  ");
					
					//useName.Append("ImageType: " + ImageTypeName);
					
					useName.Append(NewImageNameString);
					assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);//Added By Dattatray
					assetInfoObj.attributeID.push_back(-1);

					if(isThumb == kTrue)
						this->AddThumbnailImage(ImagePath,NewImageNameString );
					else
					{
						//CA("Item"+fileName);
						//listHelper.AddElement(fileName,kDCNTextWidgetID,index1);
						index1++;
						fileName="";
						//listCntl->Select(0);
						Flag1stSelected = kTrue;
					}
				}
			
			}while(kFalse);
		}
		
		
		do{ ///// added for PV/MPV Images
			if(elementIDSWithPVMPV.size() > 0  && isShowPVImages  == kTrue)//////////////////////param
			{
			//CA("elementIDSWithPVMPV.size() > 0  && isShowPVImages  == kTrue");
				for(int32 index = 0 ; index < elementIDSWithPVMPV.size() ; index++)
				{
					for(int32 imageIndex = 1 ; imageIndex <=5 ; imageIndex++)
					{
						bool16 returnResult = kFalse;
						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(imageIndex);
						if(returnResult)
						{
							double pickListGroupId = ptrIAppFramework->StructureCache_getPickListGroupIdByElementMPVId(elementIDSWithPVMPV[index]);
							VectorAssetValuePtr AssetValuePtrObject = NULL;
							AssetValuePtrObject = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(objectId,elementIDSWithPVMPV[index], sectionID, langID,1, pickListGroupId, imageIndex);
							if(AssetValuePtrObject == NULL){
								//CA("AssetValuePtrObj == NULL");
								continue ;
							}
							if(AssetValuePtrObject->size() == 0)
							{
								//CA("AssetValuePtrObj->size() == 0");
								continue;
							}
							
							//PMString imagePath = ptrIClientOptions->getImageDownloadPath();
							//if(imagePath != ""){
							//	char *imageP=imagePath.GrabCString();
							//	if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
							//		#ifdef MACINTOSH
							//			imagePath+="/";
							//		#else
							//			imagePath+="\\";
							//		#endif
							//	
							//}

							//if(imagePath == "")
							//	return ;					
							//
							////CA(imagePath);
							//bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
							//if(isPresent==FALSE)
							//{
							//	//CA("isPresent==FALSE");
							//	this->closeImagePanel();
							//	return;
							//}					

							PMString imagePathWithSubdir = imagePath;

							CElementModel elementModel;
							bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementIDSWithPVMPV[index],elementModel,langID);
							if(!result)
								continue;
							
							PMString attributeName = elementModel.getDisplayName();

							VectorAssetValue::iterator it; // iterator of Asset value
							CAssetValue objCAssetvalue;
							for(it = AssetValuePtrObject->begin();it!=AssetValuePtrObject->end();it++)
							{
								objCAssetvalue = *it;
								PMString fileName = objCAssetvalue.geturl();
								//CA("fileName"+fileName);
								double assetID = objCAssetvalue.getAsset_id();					
								
								if(fileName=="")
									continue;
								
								//PMString ImageTypeName = itemModel.getItemNo();
								double ImageTypeId = objCAssetvalue.getType_id();
								double image_mpv = objCAssetvalue.getMpv_value_id();
								
								PMString NewImageNameString("");
								NewImageNameString.Append(ImageTypeName);
								NewImageNameString.Append(" : ") ;
								NewImageNameString.Append(attributeName);
								//PMString ImageTypeName_ = TypeName;

								//CA("fileName : "+ fileName);
								//imageType_ = fileName;
								do{
									SDKUtilities::Replace(fileName,"%20"," ");
								}while(fileName.IndexOfString("%20") != -1);

								PMString imagePathWithSubdirNew(""); 
								imagePathWithSubdirNew.Append(imagePath);
								
								PMString OriginalImagePath("");
								OriginalImagePath = imagePath;
								
								#ifdef MACINTOSH
									SDKUtilities::convertToMacPath(imagePathWithSubdirNew);
								#endif	

								PMString typeCodeString("");
								//typeCodeString.Append(objCAssetvalue.getDescription());
								//typeCodeString = ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(assetID);
								
								if(typeCodeString.NumUTF16TextChars()!=0)
								{ 
									if(typeCodeString.Contains("/", typeCodeString.NumUTF16TextChars()-1))
									{
										PMString *Temp1 =  typeCodeString.Substring(0, typeCodeString.NumUTF16TextChars()-1);	
										PMString temp2 = *Temp1;
										typeCodeString = temp2;
									}
									PMString TempString = typeCodeString;		
									CharCounter ABC = 0;
									bool16 Flag = kTrue;
									bool16 isFirst = kTrue;
									do
									{
										
										if(TempString.Contains("/", ABC))
										{
			 								ABC = TempString.IndexOfString("/"); 				
										
		 									PMString * FirstString = TempString.Substring(0, ABC);		 		
		 									PMString newsubString = *FirstString;		 	
										 				 	
		 									if(!isFirst){
												#ifdef MACINTOSH
													imagePathWithSubdirNew.Append(":");
												#else
													imagePathWithSubdirNew.Append("\\");
												#endif		
											}									
		 									
		 									imagePathWithSubdirNew.Append(newsubString);
											if(ptrIAppFramework->getAssetServerOption() == 0)
			 									FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);
										 	
		 									isFirst = kFalse;
										 	
		 									PMString * SecondString = TempString.Substring(ABC+1);
		 									PMString SSSTring =  *SecondString;		 		
		 									TempString = SSSTring;	
											if(FirstString)
												delete FirstString;
											if(SecondString)
												delete SecondString;
										}
										else
										{				
											if(!isFirst){
												#ifdef WINDOWS
													imagePathWithSubdirNew.Append(":");
												#else
													imagePathWithSubdirNew.Append("\\");
												#endif
											}		 									
		 									
											imagePathWithSubdirNew.Append(TempString);
											if(ptrIAppFramework->getAssetServerOption() == 0)
			 									FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);		 		
		 									isFirst = kFalse;				
											Flag= kFalse;
											
											//CA(" 2 : "+ imagePathWithSubdir);
										}				
										
									}while(Flag);
									#ifdef MACINTOSH
										OriginalImagePath.Append(typeCodeString);
										SDKUtilities::convertToMacPath(imagePathWithSubdirNew);
									#endif									
								}

								//SDKUtilities ::AppendPathSeparator(imagePathWithSubdirNew);
								//CA("imagePathWithSubdir"+imagePathWithSubdir + " fileName : "+ fileName);
								
								//if(!fileExists(imagePathWithSubdirNew,fileName))
								//{	
								//
								//	if(ptrIAppFramework->getAssetServerOption() == 0)
								//	{
								//		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);
								//		if(!ptrIAppFramework->GETAsset_downLoadPVMPVAsset(objCAssetvalue.getP_description(),fileName,imagePathWithSubdir))
								//		{	
								//			//CA("GETAsset_downLoadItemAsset Failed");
								//			continue;
								//		}
								//	}
								//}
								//CA(imagePathWithSubdir);
								if(!fileExists(imagePathWithSubdirNew,fileName)){
								//	//CA("No FileExixts");
									continue ;
								}

								PMString ImagePath = imagePathWithSubdirNew + fileName;
								//CA("PV Image Sprsay");
								//CA(ImagePath);

                                #ifdef MACINTOSH
                                    SDKUtilities::convertToMacPath(ImagePath);
                                #endif
                                
								//CA("ImagePath" +ImagePath );
								assetInfoObj.imageMPVID.push_back(image_mpv);//imageMPVID.push_back(image_mpv);
								assetInfoObj.imageTypeID.push_back(elementModel.getPicklistGroupId());
								
								assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.
								
								PMString useName(fileName + "  ");
								useName.Append(NewImageNameString);
								assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);//Added By Dattatray
								
								//CA("ImagePath::"+ImagePath);
								
								assetInfoObj.imageVector1.push_back(ImagePath);//imageVector1.push_back(ImagePath);
								assetInfoObj.imageType.push_back(NewImageNameString);//imageType.push_back(NewImageNameString);
								assetInfoObj.itemIDS.push_back(-1);//itemIDS.push_back(-1); // ADDDING -1 BECAUSE THIS IS PRODUCT , IT CAN NOT HAVE ITEM ID 
								assetInfoObj.vectorImageTypeTagName.push_back(attributeName);//vectorImageTypeTagName.push_back(ImageTypeName_);
								assetInfoObj.attributeID.push_back(elementIDSWithPVMPV[index]);
								assetInfoObj.PVImageIndex.push_back(imageIndex);
		 							//CA("ImagePath for Product : "+ImagePath);
								
								if(isThumb == kTrue)
									this->AddThumbnailImage(ImagePath, NewImageNameString);				
								else
								{	
									//CA("Prod"+fileName);
									//listHelper.AddElement(fileName,kDCNTextWidgetID,index);
									index++;
									fileName="";
									//listCntl->Select(0);
									Flag1stSelected = kTrue;
									isProductImageAdded = kTrue;
								}				
								
							}
							if(AssetValuePtrObject)
								delete AssetValuePtrObject;
						}
					}
				}
			}
		}while(kFalse); /////END
		
	//} // ---- for ----- 
		if(isThumb == kFalse && Flag1stSelected == kTrue) //To Select AutoSelect FileName in the multiLineTextWidget
		{	 			
			K2Vector<PMString> :: iterator listItr;
			listItr = assetInfoObj.typeName.begin();//typeName.begin();
			PMString listText = *listItr;
			int ins =  listText.IndexOfString("  ");
			PMString* NewString = listText.Substring(ins +2 );
			NewString->SetTranslatable(kFalse);			
			//CA(*NewString);
			NewString->SetTranslatable(kFalse);
            NewString->ParseForEmbeddedCharacters();
			textPtr->SetString(*NewString);
			K2Vector<PMString> ::iterator itr;
			itr = assetInfoObj.imageVector1.begin();//imageVector1.begin();
			PMString dataFile = *itr;
			
			//CA(dataFile);
			
			SDKFileHelper fileHelper(dataFile);
			IDFile xFile = fileHelper.GetIDFile();
			iImageSysFile->Set(xFile);
			listImageView->Invalidate();			
		}
		else if(isThumb == kFalse && Flag1stSelected == kFalse)
		{			
			PMString blankString("");
			blankString.SetTranslatable(kFalse);
			textPtr->SetString(blankString);
			SDKFileHelper fileHelper("C:\\asd.jpg");
			IDFile xFile = fileHelper.GetIDFile();
			iImageSysFile->Set(xFile);
			listImageView->Invalidate();	
		}
	
		global_item_start_position = assetInfoObj.imageVector1.size() - 1;//imageVector1.size() - 1 ;
		this->showItemImages(objectID ,langID , parentID , parentTypeID , sectionID, kTrue) ;    
}




void PRImageHelper :: positionImagePanel(int32 TopBound, int32 LeftBound, int32 BottomBound, int32 RightBound)
{
	//CA(__FUNCTION__);
	do
	{
		//GSysRect PalleteBounds ;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			return ;
		}

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication == nil)
		{
			//CA("iApplication == nil");
			break;
		}
		
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());//QueryPaletteManager());Modified By Sachin Sharma on 23/06/07
		//if(iPaletteMgr==nil)
		//	break;
		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");
			break;
		}

		
		//UID paletteUID = kInvalidUID ;
		const ActionID MyPalleteActionID = kDCNPanelWidgetActionID ;
		//paletteUID = iPanelMgr->GetPaletteUID(MyPalleteActionID) ;  //Commented By aschin Sharma on 23/06/07

		//Added By sachin Sharma on 23/06/07
		IControlView* iControlView = NULL;
		iControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(iControlView==NULL)
		{
			//CA("CTBActionComponent::DoPallete::iControlView == nil");		
			break;
		}
		
		PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(iControlView );
		if(!palRef.IsValid())
		{
			//CA("palRef is invalid");
			break;
		}
		
		PaletteRef parentRef = PaletteRefUtils::GetParentOfPalette( palRef ); 
		if(!parentRef.IsValid())
		{
			//CA("parentRef is invalid");
			break;
		}
		PaletteRef parentparentRef = PaletteRefUtils::GetParentOfPalette( parentRef ); 
		if(!parentparentRef.IsValid())
		{
			//CA("parentparentRef is invalid");
			break;
		}
		//PaletteRefUtils::ShowHidePalette(paletteRef,PaletteRefUtils::kShowPalette);  //Commented By Sachin sharma
		
		//=============================================
		//IWindow* palette = nil;
		//IDataBase* db = ::GetDataBase(gSession);
		//if (db == nil)
		//{
			//CA("db == nil");				
			//break;
		//}
		//palette = static_cast<IWindow*>(db->Instantiate(paletteUID, IWindow::kDefaultIID));
		//if (palette == nil)
		//{
			//CA("could not query palette based on given UID!kDCNPanelWidgetActionID ");
			//break;
		//}		
		//palette->AddRef();
		//PalleteBounds  = palette->GetFrameBBox();
		
		SysRect PalleteBound = PaletteRefUtils::GetPaletteBounds(parentparentRef);
        
        int32 TemplateTop = 0;
        int32 TemplateLeft = 0;
        int32 TemplateBottom = 0;
        int32 TemplateRight = 0;
        
        TemplateTop = SysRectTop(PalleteBound);
        TemplateLeft = SysRectLeft(PalleteBound);
        TemplateRight = SysRectRight(PalleteBound);
        TemplateBottom =SysRectBottom(PalleteBound);
		
		//PMString temp = "";
		//temp.Append("top =");
		//temp.AppendNumber(TemplateTop);
		//temp.Append(", left = ");
		//temp.AppendNumber(TemplateLeft);
		//temp.Append(" , bottom = ");
		//temp.AppendNumber(TemplateBottom);
		//temp.Append(" , right = ");
		//temp.AppendNumber(TemplateRight);
		//CA(temp);
		int32 NewLeftBound = 0 ;
		if(LeftBound - 207 < 0)
			NewLeftBound = RightBound -5 ;
		else
			NewLeftBound = LeftBound - (TemplateRight - TemplateLeft -5 );
		//palette->SetPosition(NewLeftBound , TopBound , kTrue);
		//PaletteRefUtils::SetPalettePosition(parentparentRef,NewLeftBound,TopBound);
	
	}
	while(kFalse);
}

void PRImageHelper :: closeImagePanel()
{
	//CA(__FUNCTION__);
	do{
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication == nil)
			break;
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager()); //QueryPaletteManager());Modified By sachin sharma on 23/06/07
		//if(iPaletteMgr == nil) 
		//	break;
		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
			break;

		//PMLocaleId nLocale = LocaleSetting::GetLocale();
		
		if(iPanelMgr->IsPanelWithWidgetIDShown(kDCNPanelWidgetID)) 
			iPanelMgr->HidePanelByWidgetID(kDCNPanelWidgetID);

		attributesIDSWithPVMPV.clear();
		elementIDSWithPVMPV.clear();

	}
	while(kFalse);
}


void PRImageHelper::showItemImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID, int32 isProduct)
{
	//CA(__FUNCTION__);
	//int32 index=0;
	isProd=isProduct;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("showItemImages::ptrIAppFramework is NULL");	
		return ;
	}
	//ptrIAppFramework->clearAllStaticObjects();
	//VectorTypeInfoPtr typeValObj_iamge = ptrIAppFramework->AttributeCache_getItemImages();
	//if(typeValObj_iamge == nil)
	//{
		//return ;
	//}
	
	//For Item file name in the listBox
	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
	if(app == NULL)
	{
		//CA("showItemImages::app is NULL");	
		return;
	}
	
	//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager());//QueryPaletteManager()); Modified By sachin sharma on 23/06/07
	//if(paletteMgr == NULL)
	//	return;
	//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
	
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL)
	{
		//CA("showItemImages::panelMgr is NULL");		
		return;
	}
	
	/*IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
	if(myPanel == NULL)
	{
		//CA("showItemImages::myPanel is NULL");		
		return;
	}*/
	if(globalPanelControlView == NULL)
	{
		const ActionID MyPalleteActionID = kDCNPanelWidgetActionID;
		uint32 numPanels = panelMgr->GetPanelCount();
		for (int32 i = 0 ; i < numPanels ; i++)
		{
			ActionID actionID;
			UID	panelUID;
			WidgetID	widgetID;
			PMString	panelName;
			ScriptID	scriptID;
			bool16 gotPanelInfo = kFalse;
			
			gotPanelInfo = panelMgr->GetNthPanelInfo(i, panelUID, &actionID, &widgetID, &panelName, &scriptID);

			if(gotPanelInfo && (actionID == MyPalleteActionID) )
			{
				globalPanelControlView = panelMgr->GetPanelFromActionID(MyPalleteActionID);
				//if(globalPanelControlView == NULL)
					//CAlert::InformationAlert("still i m not getting");
			}
		}
	}

	InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
	if(panelControlData == NULL)
	{
		//CA("showItemImages::panelControlData is NULL");	
		return;
	}
	IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
	if(lstgridWidget == NULL)
	{
		//CA("showItemImages::lstgridWidget is NULL");		
		return;
	}
	IControlView* textView = panelControlData->FindWidget( kDCNLstViewMultilineTextWidgetID);
	if(textView == NULL)
	{
		//CA("showItemImages::textView is NULL");
		return;
	}

	IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
	if(listImageView == NULL)
	{
		//CA("showItemImages::listImageView is NULL");
		return;
	}
	InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
	if(!iImageSysFile)
	{
		//CA("showItemImages::iImageSysFile is NULL");
		return;
	}
	
	InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
	if(textPtr == NULL)
	{
		//CA("showItemImages::textPtr is NULL");	
		return;
	}
			
	//SDKListBoxHelper listHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
	//IControlView* listBoxControlView = listHelper.FindCurrentListBox();
	//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
	//	ASSERT_MSG(listCntl != nil, "listCntl nil");
	//	if(listCntl == nil) { //CA("showItemImages::listCntl is NULL");
	//		return;
	//}

	VectorLongIntPtr itemids = NULL;
	VectorLongIntValue singleItemvect;
	singleItemvect.clear();

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions == nil)
	{
		//CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return ;
	}

	if(isProduct == 1)
	{		
		itemids = ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(objectID, sectionID);
		if(itemids == NULL){ //CA("showItemImages::itemids is NULL");
			return;
		}
		if(itemids->size() == 0){ //CA("itemids->size() == 0");
			return;
		}
	}
	if(isProduct == 0)
	{
		//CA("isProduct == 0");
		BrowseFolderOption = kFalse;
		isBrowsePrintSource = kTrue;
		if(isThumb)
			this->EmptyImageGrid();
		/*else
			listHelper.EmptyCurrentListBox();*/

		isProductImageAdded = kFalse;
		this->EmptyImageGrid();
		
		//imageType.clear();
		//vectorImageTypeTagName.clear();
		//imageTypeID.clear();
		//imageMPVID.clear();
		//imageVector1.clear();
		//itemIDS.clear();
		
		assetInfoObj.clearAll();

		
		imageBoolFlagVector.clear();

		global_lang_id = langID ;
		global_section_id = sectionID ;
		global_parent_id = parentID ;
		//global_parent_type_id = parentTypeID ;
		global_product_id =  objectID ;
		global_item_start_position =-1;

		objId=objectID;
		langaugeId=langID;
		parId=parentID;
		parTypeId=parentTypeID;
		sectId=sectionID;
		
		double pubObjectID;
		CPbObjectValue CPbObjectValueobj = ptrIAppFramework->getPbObjectValueBySectionIdObjectId(sectId, objId, isProduct, langaugeId );
		pubObjectID = CPbObjectValueobj.getPub_object_id();

		singleItemvect.push_back(objectID);
		itemids =  &singleItemvect;
		vector<double>itemIDSFromItemTables;
		int32 size = static_cast<int32>(singleItemvect.size());
		/////////////////////////////////////	To Show item's images inside item
		for(int32 indx = 0 ; indx <  size ; indx++)
		{
			itemIDSFromItemTables.push_back(singleItemvect[indx]);
			do{
				
				VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(objectID, sectId, langaugeId );
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP46_DCN41162::PRImageHelper::showItemImages::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
					break;
				}
				if(tableInfo->size()==0)
				{ 
					ptrIAppFramework->LogInfo("AP46_DCN41162::PRImageHelper::showItemImages: table size = 0");
					break;
				}
				
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;

				bool16 typeidFound=kFalse;
				vector<double> vec_items;
				
				for(it = tableInfo->begin() ; it!=tableInfo->end() ; it++)
				{//for tabelInfo start				
					oTableValue = *it;				
					vec_items = oTableValue.getItemIds();
				
					for(int32 i = 0; i < vec_items.size() ; i++)
					{	bool16 Flag = kFalse;
						for(int32 j = 0 ; j < singleItemvect.size() ; j++)
						{
							if(vec_items[i] == singleItemvect[j])
							{
								Flag = kTrue;
								break;
							}				
						}
						if(!Flag)
						{
							singleItemvect.push_back(vec_items[i]);
							itemIDSFromItemTables.push_back(vec_items[i]);
						}
					}
					
				}//for tabelInfo end
				if(tableInfo)
					delete tableInfo;
			}while(kFalse);
		}
		///////////////////////////END
		index1 =0; 
		
	//	imagePath = ptrIClientOptions->getImageDownloadPath();
	//	bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
	//	this->closeImagePanel();
	
	}

	VectorLongIntValue::iterator itn ;
	
	//int32 size = typeValObj_iamge->size() ;

	int32 count = 0 ;

	//VectorTypeInfoValue::iterator it2;

	/*PMString Typecode = ptrIAppFramework->GetStaticFieldStringFromInterface("com/apsiva/commonframework/Types", "PARENT_PRODUCT_TYPE");
	int32 parentTypeID1 = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	globle_item_parenttype_id = parentTypeID1;*/
	bool16 Flag1stSelected = kFalse;
	
	for(itn = itemids->begin() ; itn != itemids->end() ; itn++)
	{
		//ptrIAppFramework->clearAllStaticObjects();
		//for(it2 = typeValObj_iamge->begin() ; it2 != typeValObj_iamge->end() ; it2++)
		{	
			PMString imagePath = ptrIClientOptions->getImageDownloadPath();
			if(imagePath != ""){
				const char *imageP=imagePath.GetPlatformString().c_str();
				if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
					#ifdef MACINTOSH
						imagePath+="/";
					#else
						imagePath+="\\";
					#endif
				
			}
			if(imagePath == "")
				return ;
			bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
			if(isPresent==FALSE)
			{
				//CA("isPresent==FALSE");
				this->closeImagePanel();
				return;
			}

			CPbObjectValue cpbObjectValObj = ptrIAppFramework->getPbObjectValueBySectionIdObjectId(sectionID, *itn, 0, langID);
			CItemModel itemModel = cpbObjectValObj.getItemModel();
			//int32 Imagetypeid = it2->getType_id() ;
			do{	
				//VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(*itn , parentTypeID1 , Imagetypeid);
				VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(*itn , sectId, 0 );
				if(AssetValuePtrObj == NULL){
					//CA(" AssetValuePtrObj == NULL ");
					continue ;
				}

				if(AssetValuePtrObj->size() ==0){
					//CA(" AssetValuePtrObj->size() == 0 ");
					continue ;
				}

				//CA("AssetValuePtrObj->size() : ");
				//CAI(AssetValuePtrObj->size());

				/*PMString imagePath = ptrIClientOptions->getImageDownloadPath();
				if(imagePath != ""){
					char *imageP=imagePath.GrabCString();
					if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
						#ifdef MACINTOSH
							imagePath+="/";
						#else
							imagePath+="\\";
						#endif
					
				}

				if(imagePath == "")
					return ;					
				
				//CA(imagePath);
				bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
				if(isPresent==FALSE)
				{
					this->closeImagePanel();
					return;
				}					
				*/
				PMString imagePathWithSubdir = imagePath;

				//PMString typIDString("");
				//typIDString.AppendNumber(Imagetypeid);
				//imageTypeID.push_back(Imagetypeid); //-- commented for changing place ..

				
				//CItemModel itemModel = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(*itn , langID);
				PMString ImageTypeName =itemModel.getItemNo() ;
			
				/*ImageTypeName.Append(" : ");
				  ImageTypeName.Append(it2->getName());
				  PMString ImageTypeName_ = it2->getName();*/					
				/*PMString imageCode = it2->getCode();*/
				
				int imageTextIndex = 0;
				PMString fileName("");
				double assetID ;
				CObjectValue oVal ;
				double objectId =  objectID ;
				VectorAssetValue::iterator it; // iterator of Asset value
				CAssetValue objCAssetvalue;
				for(it = AssetValuePtrObj->begin() ; it != AssetValuePtrObj->end() ;it++)
				{
					objCAssetvalue = *it;
					fileName = objCAssetvalue.geturl();
					//CA(" file name is " + fileName);
					assetID = objCAssetvalue.getAsset_id();					
					if(fileName == ""){							
						continue ;
					}
					//imageType_ = fileName;
					
					/*int32 image_mpv = objCAssetvalue.getMpv_value_id();
					imageMPVID.push_back(image_mpv);*/
					double Imagetypeid = objCAssetvalue.getType_id();
					PMString TypeName = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(Imagetypeid);
					//CA("TypeName : "+TypeName);
					PMString NewImageNameString("");
					NewImageNameString.Append(ImageTypeName);
					NewImageNameString.Append(" : ") ;
					NewImageNameString.Append(TypeName);
					PMString ImageTypeName_ = TypeName;
					do{
						SDKUtilities::Replace(fileName,"%20"," ");
					}while(fileName.IndexOfString("%20") != -1);

					PMString typeCodeString;
					PMString NewImagePathWithSubDirectory = imagePathWithSubdir;
					
					PMString OriginalImagePath("");
					OriginalImagePath = imagePath;
					
					#ifdef MACINTOSH 
						SDKUtilities::convertToMacPath(NewImagePathWithSubDirectory);
					#endif

					typeCodeString = ""; //ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(assetID);
					
					//CA(typeCodeString);
					//Following three lines are commented and instead of that, code following that is added.
					//if(typeCodeString.NumUTF16TextChars()!=0)
					//	imagePathWithSubdir.Append(typeCodeString);	
					//FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
					//CA("For Simple Image typeCodeString : "+typeCodeString);
					if(typeCodeString.NumUTF16TextChars()!=0)
					{ 
					
						PMString TempString = typeCodeString;		
						CharCounter ABC = 0;
						bool16 Flag = kTrue;
						bool16 isFirst = kTrue;
						do
						{
							
							if(TempString.Contains("/", ABC))
							{
	 							ABC = TempString.IndexOfString("/"); 				
						 	
 								PMString * FirstString = TempString.Substring(0, ABC);		 		
 								PMString newsubString = *FirstString;		 	
						 				 		
 								if(!isFirst){
									#ifdef MACINTOSH
										NewImagePathWithSubDirectory.Append(":");
									#else	
										NewImagePathWithSubDirectory.Append("\\");
									#endif	
								}						 		
 								NewImagePathWithSubDirectory.Append(newsubString);	
								if(ptrIAppFramework->getAssetServerOption() == 0)
	 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);
						 		
 								isFirst = kFalse;
						 		
 								PMString * SecondString = TempString.Substring(ABC+1);
 								PMString SSSTring =  *SecondString;		 		
 								TempString = SSSTring;	
								if(FirstString)
									delete FirstString;
								if(SecondString)
									delete SecondString;
							}
							else
							{				
								if(!isFirst){
									#ifdef MACINTOSH
										NewImagePathWithSubDirectory.Append(":");
									#else
										NewImagePathWithSubDirectory.Append("\\");
									#endif	
						 		}			 		
 								NewImagePathWithSubDirectory.Append(TempString);	
								if(ptrIAppFramework->getAssetServerOption() == 0)
	 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);		 		
 								isFirst = kFalse;				
								Flag= kFalse;										
								//CA(" 2 : "+ imagePathWithSubdir);
							}										
						}while(Flag);
						OriginalImagePath.Append(typeCodeString);
						#ifdef MACINTOSH
							OriginalImagePath+="/";
							SDKUtilities::convertToMacPath(NewImagePathWithSubDirectory);
						#else
							OriginalImagePath+="\\";
						#endif						
					}

					
					
					//SDKUtilities ::AppendPathSeparator(NewImagePathWithSubDirectory);
					
					//CA("NewImagePathWithSubDirectory : "+ NewImagePathWithSubDirectory +"   File Name :"+ fileName);
					//if(!fileExists(NewImagePathWithSubDirectory,fileName))
					//{
					//	if(ptrIAppFramework->getAssetServerOption() == 0)
					//	{
					//		//if(!ptrIAppFramework->GETAsset_downLoadItemAsset(assetID,NewImagePathWithSubDirectory))
					//		//{	
					//		//	//CAI(__LINE__);
					//		//	continue ;
					//		//}
					//	}
					//}

		
					if(!fileExists(NewImagePathWithSubDirectory,fileName))
					{
						//CAI(__LINE__);
						continue ;
					}

					PMString ImagePath = NewImagePathWithSubDirectory + fileName;
                    
                    #ifdef MACINTOSH
                        SDKUtilities::convertToMacPath(ImagePath);
                    #endif

					assetInfoObj.imageVector1.push_back(ImagePath);//imageVector1.push_back(ImagePath);
					/*int32 size=imageVector1.size();
					PMString sz;
					sz.AppendNumber(size);
					CA(sz);*/
					double image_mpv = objCAssetvalue.getMpv_value_id();
					assetInfoObj.imageMPVID.push_back(image_mpv);//imageMPVID.push_back(image_mpv);

					assetInfoObj.imageTypeID.push_back(Imagetypeid);//imageTypeID.push_back(Imagetypeid);

					assetInfoObj.imageType.push_back(NewImageNameString);//imageType.push_back(NewImageNameString);
					//CA("ImageTypeName_ : "+ImageTypeName_);
					assetInfoObj.vectorImageTypeTagName.push_back(ImageTypeName_);//vectorImageTypeTagName.push_back(ImageTypeName_);							
					assetInfoObj.itemIDS.push_back(*itn);//itemIDS.push_back(*itn);
												
					assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.
					assetInfoObj.PVImageIndex.push_back(-1);
					//PMString useName("FileName: "+ fileName + "  ");
					
					PMString useName(fileName + "  ");
					
					//useName.Append("ImageType: " + ImageTypeName);
					
					useName.Append(NewImageNameString);
					assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);//Added By Dattatray
					assetInfoObj.attributeID.push_back(-1);

					if(isThumb == kTrue)
						this->AddThumbnailImage(ImagePath,NewImageNameString );
					else
					{
						//CA("Item"+fileName);
						//listHelper.AddElement(fileName,kDCNTextWidgetID,index);
						index1++;
						fileName="";
						//listCntl->Select(0);
						Flag1stSelected = kTrue;
					}
				}
				if(AssetValuePtrObj)
				{
					AssetValuePtrObj->clear();
					delete AssetValuePtrObj;
				}

			}while(0);	
////////////////////////Amit 
			if(isShowPartnerImages == kTrue)
			{
				//CA("isShowPartnerImages == kTrueitem");
				do{	
					///For Brand,Manufacture,supplier images
					//ptrIAppFramework->clearAllStaticObjects();
					//VectorTypeInfoPtr typeValObj = ptrIAppFramework->GetItemOrProductBMSImageTypes(kFalse);
					//if(typeValObj == NULL){
					//	//CA("typeValObj == NULL");
					//	continue ;
					//}
					//if(typeValObj->size() == 0)
					//{
					//	//CA("typeValObj->size() == 0");
					//	continue;
					//}
					
					PMString imagePathWithSubdir = imagePath;
					//CItemModel itemModel = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(*itn , langID);
					PMString ImageTypeName = itemModel.getItemNo() ;
					/*PMString s("ImageTypeName : ");
					s.Append(ImageTypeName);
					CA(s);*/
					
					int32 whichTab = 4; // for Item Group
					double partTypeId = -1;
					for( partTypeId =  -207 ; partTypeId > -223 ;  partTypeId--)
					{
						CAssetValue PartAssetValue = ptrIAppFramework->getPartnerAssetValueByPartnerIdImageTypeId(*itn, sectionID, langID, partTypeId, whichTab); 
						PMString fileName = PartAssetValue.geturl();
						if(fileName == ""){
							//CA("continue");
							continue ;
						}
						do
						{
							SDKUtilities::Replace(fileName,"%20"," ");
						}while(fileName.IndexOfString("%20") != -1);
						
						//CA("fileName : "+fileName);

						PMString TypeName = PartAssetValue.geturl();
						//CA("TypeName : "+TypeName);

						PMString NewImageNameString("");
						/*NewImageNameString.Append(ImageTypeName);
						NewImageNameString.Append(" : ") ;
						NewImageNameString.Append(TypeName);*/
						
						PMString ImageTypeName_ = TypeName;
						
						PMString NewImagePathWithSubDirectory = imagePathWithSubdir;
						
						PMString typeCodeString = "";  //ptrIAppFramework->GetAssets_getAssetFolderForBMSandPubLogo(*itn, cTypeModel.getType_id()); 
						if(typeCodeString.NumUTF16TextChars()!=0)
						{ 
							//CA("typeCodeString : "+typeCodeString);
							PMString TempString = typeCodeString;		
							CharCounter ABC = 0;
							bool16 Flag = kTrue;
							bool16 isFirst = kTrue;
							do
							{					
								if(TempString.Contains("/", ABC))
								{
		 							ABC = TempString.IndexOfString("/"); 				
									
	 								PMString * FirstString = TempString.Substring(0, ABC);		 		
	 								PMString newsubString = *FirstString;		 	
										 			
	 								if(!isFirst){
										#ifdef MACINTOSH
											NewImagePathWithSubDirectory.Append(":");
										#else
											NewImagePathWithSubDirectory.Append("\\");
										#endif	
									} 									
									
	 								NewImagePathWithSubDirectory.Append(newsubString);	
									if(ptrIAppFramework->getAssetServerOption() == 0)
			 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);					 		
	 								isFirst = kFalse;					 		
	 								PMString * SecondString = TempString.Substring(ABC+1);
	 								PMString SSSTring =  *SecondString;		 		
	 								TempString = SSSTring;	
									if(FirstString)
										delete FirstString;

									if(SecondString)
										delete SecondString;

								}
								else
								{				
									if(!isFirst)
	 								NewImagePathWithSubDirectory.Append("\\");
									
	 								NewImagePathWithSubDirectory.Append(TempString);	
									if(ptrIAppFramework->getAssetServerOption() == 0)
		 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);		 		
	 								isFirst = kFalse;				
									Flag= kFalse;
									
									//CA(" 2 : "+ imagePathWithSubdir);
								}				
							}while(Flag);
						}
						//if(ptrIAppFramework->getAssetServerOption() == 0)
						//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory));
							
						#ifdef MACINTOSH						
							SDKUtilities::convertToMacPath(NewImagePathWithSubDirectory );  //**********SSS
						#endif
													

						//SDKUtilities ::AppendPathSeparator(NewImagePathWithSubDirectory);
						//CA("NewImagePathWithSubDirectory : "+NewImagePathWithSubDirectory +"\rfileName : "+fileName);
						/*if(!fileExists(NewImagePathWithSubDirectory,fileName))
						{
							if(ptrIAppFramework->getAssetServerOption() == 0)
							{
								if(!ptrIAppFramework->GETAsset_downloadItemBMSAsset(*itn, cTypeModel.getType_id(),NewImagePathWithSubDirectory))
								{
									ptrIAppFramework->LogDebug("AP46_DCN41162::PRImageHelper::showItemImages::!GETAsset_downloadItemBMSAsset");				
									continue;
								}
							}
						}*/

						if(!fileExists(NewImagePathWithSubDirectory,fileName))
						{
							//CA("File not exist");
							continue ;
						}
						
						
						PMString ImagePath = NewImagePathWithSubDirectory + fileName;
                        
                        #ifdef MACINTOSH
                            SDKUtilities::convertToMacPath(ImagePath);
                        #endif

						assetInfoObj.imageVector1.push_back(ImagePath);
					
						//int32 image_mpv = objCAssetvalue.getMpv_value_id();
						
						assetInfoObj.imageMPVID.push_back(-1);

						assetInfoObj.imageTypeID.push_back(partTypeId);

						assetInfoObj.imageType.push_back(NewImageNameString);
						//CA("ImageTypeName_ : "+ImageTypeName_);
						assetInfoObj.vectorImageTypeTagName.push_back(ImageTypeName_);						
						assetInfoObj.itemIDS.push_back(*itn);//itemIDS.push_back(*itn);
													
						assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.
						assetInfoObj.PVImageIndex.push_back(-1);
						//PMString useName("FileName: "+ fileName + "  ");
						
						PMString useName(fileName + "  ");
						
						//useName.Append("ImageType: " + ImageTypeName);
						
						useName.Append(NewImageNameString);
						assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);//Added By Dattatray
						assetInfoObj.attributeID.push_back(-1);

						if(isThumb == kTrue)
							this->AddThumbnailImage(ImagePath,NewImageNameString );
						else
						{
							//CA("Item"+fileName);
							//listHelper.AddElement(fileName,kDCNTextWidgetID,index);
							index1++;
							fileName="";
							//listCntl->Select(0);
							Flag1stSelected = kTrue;
						}
					}

				}while(kFalse);
			}

			if(attributesIDSWithPVMPV.size() > 0 && isShowPVImages == kTrue) //For PV/MPV Images
			{
				//CA("attributesWithPVMPV.size() > 0");
				do{
					/*CPbObjectValue cpbObjectValObj = ptrIAppFramework->getPbObjectValueBySectionIdObjectId(sectionID, *itn, 0, langID);
					CItemModel itemModel = cpbObjectValObj.getItemModel();*/

					for(int32 index = 0 ; index < attributesIDSWithPVMPV.size() ; index++)
					{
						/*PMString a("attributesIDSWithPVMPV[index] : ");
						a.AppendNumber(attributesIDSWithPVMPV[index]);
						a.Append("\r *itn : ");
						a.AppendNumber(*itn);
						a.Append("\r langID : ");
						a.AppendNumber(langID);
						CA(a);*/
						for(int32 imageIndex = 1 ; imageIndex <=5 ; imageIndex++)
						{
							bool16 returnResult = kFalse;
							returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(imageIndex);
							if(returnResult)
							{
								double pickListGroupId = ptrIAppFramework->StructureCache_getPickListGroupIdByAttributeMPVId(attributesIDSWithPVMPV[index]);
								VectorAssetValuePtr AssetValuePtrObject = NULL;
								AssetValuePtrObject = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(*itn,attributesIDSWithPVMPV[index],  sectionID, langID, 0, pickListGroupId, imageIndex);
								if(AssetValuePtrObject == NULL){
									//CA("AssetValuePtrObj == NULL");
									continue ;
								}
								if(AssetValuePtrObject->size() == 0)
								{
									//CA("AssetValuePtrObj->size() == 0");
									continue;
								}
								
								/*PMString imagePath = ptrIClientOptions->getImageDownloadPath();
								if(imagePath != ""){
									char *imageP=imagePath.GrabCString();
									if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
										#ifdef MACINTOSH
											imagePath+="/";
										#else
											imagePath+="\\";
										#endif
									
								}

								if(imagePath == "")
									return ;					
								
								//CA(imagePath);
								bool8 isPresent = ptrIClientOptions->checkforFolderExists(imagePath);
								if(isPresent==FALSE)
								{
									//CA("isPresent==FALSE");
									this->closeImagePanel();
									return;
								}
								*/					

								PMString imagePathWithSubdir = imagePath;
								//PMString attributeName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(attributesIDSWithPVMPV[index], langID);
															
								PMString attributeName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(attributesIDSWithPVMPV[index], langID);

								
								PMString ImageTypeName = itemModel.getItemNo();
								//CA("ImageTypeName : "+ImageTypeName);						
								int imageTextIndex = 0;
								PMString fileName("");
								//int32 assetID ;
								CObjectValue oVal ;
								double objectId =  objectID ;
								VectorAssetValue::iterator it1; // iterator of Asset value
								CAssetValue objCAssetvalue;
								for(it1 = AssetValuePtrObject->begin() ; it1 != AssetValuePtrObject->end() ;it1++)
								{
									objCAssetvalue = *it1;
									fileName = objCAssetvalue.geturl();
									//CA(" file name is " + fileName);
									//assetID = objCAssetvalue.getAsset_id();					
									if(fileName == ""){							
										continue ;
									}
									//imageType_ = fileName;
												
																	
									PMString NewImageNameString("");
									NewImageNameString.Append(ImageTypeName);
									NewImageNameString.Append(" : ") ;
									NewImageNameString.Append(attributeName);
									
									do{
										SDKUtilities::Replace(fileName,"%20"," ");
									}while(fileName.IndexOfString("%20") != -1);

									PMString NewImagePathWithSubDirectory = imagePathWithSubdir;
									PMString OriginalImagePath("");
								//	CA(" 2191   ImagePath: " + imagePath);
									OriginalImagePath= imagePath;
									
									#ifdef MACINTOSH 
										SDKUtilities::convertToMacPath(NewImagePathWithSubDirectory);
									#endif									
																						
									//NewImagePathWithSubDirectory.Append(objCAssetvalue.getP_description());
									PMString typeCodeString(""); // (objCAssetvalue.getP_description());
									if(typeCodeString.NumUTF16TextChars()!=0)
									{ 
									
										if(typeCodeString.Contains("/", typeCodeString.NumUTF16TextChars()-1))
										{
											PMString *Temp1 =  typeCodeString.Substring(0, typeCodeString.NumUTF16TextChars()-1);	
											PMString temp2 = *Temp1;
											typeCodeString = temp2;
										}

										PMString TempString = typeCodeString;		
										CharCounter ABC = 0;
										bool16 Flag = kTrue;
										bool16 isFirst = kTrue;
										do
										{
											
											if(TempString.Contains("/", ABC))
											{
 												ABC = TempString.IndexOfString("/"); 				
										 	
												PMString * FirstString = TempString.Substring(0, ABC);		 		
												PMString newsubString = *FirstString;		 	
										 				 		
												if(!isFirst){
													#ifdef MACINTOSH
														NewImagePathWithSubDirectory.Append(":");
													#else 
														NewImagePathWithSubDirectory.Append("\\");
													#endif
												}
										 		
												NewImagePathWithSubDirectory.Append(newsubString);	
												if(ptrIAppFramework->getAssetServerOption() == 0)
 													FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);
										 		
												isFirst = kFalse;
										 		
												PMString * SecondString = TempString.Substring(ABC+1);
												PMString SSSTring =  *SecondString;		 		
												TempString = SSSTring;	
												if(FirstString)
													delete FirstString;
												if(SecondString)
													delete SecondString;
											}
											else
											{				
												if(!isFirst){
													#ifdef MACINTOSH
														NewImagePathWithSubDirectory.Append(":");
													#else
															NewImagePathWithSubDirectory.Append("\\");
													#endif		
												}
										 		
												NewImagePathWithSubDirectory.Append(TempString);	
												if(ptrIAppFramework->getAssetServerOption() == 0)
 													FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);		 		
												isFirst = kFalse;				
												Flag= kFalse;										
												//CA(" 2 : "+ imagePathWithSubdir);
											}										
										}while(Flag);
									}
									//#ifdef MACINTOSH
									//	NewImagePathWithSubDirectory.Append(":");
									//#else
									//	NewImagePathWithSubDirectory.Append("\\");
									//#endif
									//CA("NewImagePathWithSubDirectory : "+ NewImagePathWithSubDirectory +"   File Name :"+ fileName);
									//if(!fileExists(NewImagePathWithSubDirectory,fileName))
									//{
									//	//CA("File Not Exist 1");
									//	if(ptrIAppFramework->getAssetServerOption() == 0)
									//	{
									//		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);
									//		if(!ptrIAppFramework->GETAsset_downLoadPVMPVAsset(objCAssetvalue.getP_description(),fileName,imagePathWithSubdir))
									//		{	
									//			//CA("GETAsset_downLoadItemAsset Failed");
									//			continue;
									//		}
									//	}
									//}

									if(!fileExists(NewImagePathWithSubDirectory,fileName))
									{
										//CA("File Not Exist 2");
										continue ;
									}

									PMString ImagePath = NewImagePathWithSubDirectory + fileName;
                                    
                                    #ifdef MACINTOSH
                                        SDKUtilities::convertToMacPath(ImagePath);
                                    #endif

									assetInfoObj.imageVector1.push_back(ImagePath);//imageVector1.push_back(ImagePath);
									double image_mpv = objCAssetvalue.getMpv_value_id();
									assetInfoObj.imageMPVID.push_back(image_mpv);//imageMPVID.push_back(image_mpv);
									
									
											//imageTypeID.push_back(AttributeInfoPtr->at(0).getPv_type_id());
									assetInfoObj.imageTypeID.push_back(pickListGroupId);
										
									
									
									assetInfoObj.vectorImageTypeTagName.push_back(attributeName);//vectorImageTypeTagName.push_back(attributeName);							
									assetInfoObj.itemIDS.push_back(*itn);//itemIDS.push_back(*itn);
																
									assetInfoObj.imagefileName.push_back(fileName);//imagefileName.push_back(fileName);
									
									assetInfoObj.imageType.push_back(NewImageNameString);//imageType.push_back(NewImageNameString);
									
									PMString useName(fileName + "  ");
									useName.Append(NewImageNameString);
									assetInfoObj.typeName.push_back(useName);//typeName.push_back(useName);
									assetInfoObj.attributeID.push_back(attributesIDSWithPVMPV[index]);
									assetInfoObj.PVImageIndex.push_back(imageIndex);
									if(isThumb == kTrue)
									{
										//CA("isThumb == kTrue");
										//CA("ImagePath : "+ImagePath);
										//CA("NewImageNameString : "+NewImageNameString);
										this->AddThumbnailImage(ImagePath,NewImageNameString );
									}
									else
									{
										//CA("fileName : "+fileName);
										//PMString a("index : ");
										//a.AppendNumber(index);
										//CA(a);
										//listHelper.AddElement(fileName,kDCNTextWidgetID,index);
										index++;
										fileName="";
										//listCntl->Select(0);
										Flag1stSelected = kTrue;
									}

								}
											
								if(AssetValuePtrObject)
								{
									AssetValuePtrObject->clear();		
									delete AssetValuePtrObject;
								}
							}
						}
					}
				}while(kFalse);
			}
//////////////////////End Amit
		} // ---- for inner 
	}
	if(isThumb == kFalse && Flag1stSelected == kTrue  )//To Show AutoSelect FileName in DCNLstViewMultilineTextWidget
	{
		K2Vector<PMString> :: iterator listItr;
		listItr =	assetInfoObj.typeName.begin();//typeName.begin();
		PMString listText = *listItr;
		int ins =  listText.IndexOfString("  ");
		PMString* NewString = listText.Substring(ins +2 );
		//CA(*NewString);
		NewString->SetTranslatable(kFalse);
        NewString->ParseForEmbeddedCharacters();
		textPtr->SetString(*NewString);
		K2Vector<PMString> ::iterator itr;
		itr = assetInfoObj.imageVector1.begin();//imageVector1.begin();
		PMString dataFile = *itr;
		SDKFileHelper fileHelper(dataFile);
		IDFile xFile = fileHelper.GetIDFile();
		iImageSysFile->Set(xFile);
		listImageView->Invalidate();			

	}
	else if(isThumb == kFalse && Flag1stSelected == kFalse && isProductImageAdded == kFalse)
	{
		//CA("Empty");
		PMString blankString("");
		blankString.SetTranslatable(kFalse);
		textPtr->SetString(blankString);
		SDKFileHelper fileHelper("C:\\asd.jpg");
		IDFile xFile = fileHelper.GetIDFile();
		iImageSysFile->Set(xFile);
		listImageView->Invalidate();	
	}
}


bool16 PRImageHelper :: isImagePanelOpen()
{
	//CA(__FUNCTION__);
	bool16 result = kFalse ;

	do{
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());
		if(iApplication == nil)
		{
			//CA("isImagePanelOpen::iApplication is NULL");
			break;
		}
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());  //QueryPaletteManager()); Modified By Sachin sharma 0n 23/06/07
		//if(iPaletteMgr == nil)
		//	break;

		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("isImagePanelOpen::iPanelMgr is NULL");		
			break;
		}

		PMLocaleId nLocale = LocaleSetting::GetLocale();

		if(iPanelMgr->IsPanelWithWidgetIDShown(kDCNPanelWidgetID)) 
			result = kTrue ;

	}
	while(kFalse);
	//CA(" return image panel  opening ");
	return result ;
}

void PRImageHelper::AddThumbnailImage(PMString path, PMString DisplayName)
{
	//CA("AddThumbnailImage");
	do{
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if ( app == NULL )
		{
			//CA("AddThumbnailImage::app is NULL");		
			break;
		}
		
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager())      //QueryPaletteManager()); Modified By Sachin sharma 23-06-07
		//if ( paletteMgr == NULL ) break;
		//
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if ( panelMgr == NULL ) 
		{
			//CA("AddThumbnailImage::panelMgr is NULL");		
			break;
		}

		/*IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		if ( !myPanel ){
			//CA("toms  \001\004Panel not visible\001\004\n");
			break;
		}
		*/
		
		InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
		if (!panelControlData) 
		{
			//CA("toms \001\004NO IPanelControlData\001\004\n");
			break;
		}
		
		IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );
		if ( gridWidget == NULL )
		{
			//CA("toms \001\004FAILED to find widgets\001\004\n");
			break;
		}
		
		InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );
		if ( gridController == NULL )
		{
			//CA("toms \001\004NO IXLibraryViewController\001\004\n");
			break;
		}
				
			// make up a name for the item
			//sprintf(buf,"Item %d",itemCount);						// Support code commented
			//PMString UseName(buf,-1,PMString::kNoTranslate);
			//CA(path);
			PMString FileName("");
			FileUtils::GetFileName(path, FileName);

			//imagefileName.push_back(FileName);//This Vector is used in DCNActionComponent to show filename.

			PMString useName("");
			PMString userName(DisplayName); 
			if(	(BrowseFolderOption == kTrue) && (isBrowsePrintSource == kFalse))
			{
				useName.Append(FileName);
			}
			else if((BrowseFolderOption == kFalse) && (isBrowsePrintSource == kTrue))
			{
				
				useName.Append(FileName + "  ");
				useName.Append(DisplayName);
			}
			//typeName.push_back(useName);
			//CA("useName::"+useName);
			//do
			//{
			//	//CA("path::"+path);
			//	SDKFileHelper fileHelper(path);//("C:\\ais6.jpg");
			//	IDFile xFile = fileHelper.GetIDFile();

			//	InterfacePtr<IPMStream>
			//	iFileStream(StreamUtil::CreateFileStreamRead(xFile));
			//	if(iFileStream == nil)
			//	{	//CA("File Stream nil");
			//		break;
			//	}

			//	// check if there is an image format that recognizes this image file
			//	InterfacePtr<IImageFormatManager>
			//		iImageFormatManager((IImageFormatManager*)::CreateObject(
			//		kImageFormatManager, IID_IIMAGEFORMATMANAGER));
			//	//ASSERT(iImageFormatManager);
			//	if (iImageFormatManager == nil)
			//	{	//CA("image format manager nil");
			//		break;
			//	}
			//	InterfacePtr<IImageReadFormat> iImageReadFormat(
			//		iImageFormatManager->QueryImageReadFormat(iFileStream));
			//	// Don't assert here: if we can't get this interface, it just
			//	// means it's a format that we can't preview. Fair enough, fail quietly
			//	if (iImageReadFormat == nil)
			//	{	//CA("Cant read image Due to low Quality");
			//		imageBoolFlagVector.push_back(kTrue);					
			//		break;
			//	}

			//	InterfacePtr<IImageAttributes>
			//	iSrcAttr((IImageAttributes*)::CreateObject(kImageObject,IID_IIMAGEATTRIBUTES));
			//	//ASSERT(iSrcAttr);
			//	if(iSrcAttr == nil)
			//	{	//CA("Attrbute nil");
			//		break;
			//	}

			//	if (iImageReadFormat->CanRead(iFileStream) == kFalse)
			//	{	//CA("image format nil");
			//		break;
			//	}
			//	iImageReadFormat->Open(iFileStream);
			//	iImageReadFormat->GetImageAttributes(iSrcAttr);

			//	uint32 nWidthSrc, nHeightSrc/*, nWidthDst, nHeightDst*/;

			//	iSrcAttr->GetTag(IImageAttributes::kPMTagImageWidth, &nWidthSrc);
			//	iSrcAttr->GetTag(IImageAttributes::kPMTagImageHeight, &nHeightSrc);
			//	if ((nWidthSrc <= 0) || (nHeightSrc <= 0))
			//	{	//CA("image hieght not supported");
			//		break;
			//	}

			//	/*PMString ASD("nWidthSrc : " );
			//	ASD.AppendNumber(nWidthSrc);
			//	ASD.Append("  nHeightSrc : ");
			//	ASD.AppendNumber(nHeightSrc);
			//	CA(ASD);*/

			//	if(nWidthSrc> 150)
					imageBoolFlagVector.push_back(kTrue);	
			//	else
			//		imageBoolFlagVector.push_back(kFalse);	

			//}while(0);
        
            MFileUtility  fileUtility(path);
            IDFile imgFile = fileUtility.GetSysFile();
        
        
			//IDFile imgFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&path));
        
			int32 fileSize = fileUtility.GetFileSize();
        
            if(fileSize == 0)
                return;
        
			IControlView* newGridItem = gridController->CreateViewItemWidget( useName,fileSize);
			if ( newGridItem != NULL )
			{
				//CA("Calling 2");
				gridController->InsertViewItemWidgetAndUpdate( newGridItem, kTrue, kTrue , path);
				
				gridController->UpdateView();
			
				newGridItem->Release();
			
				//CA("toms created and added item: %s\n " );
			}
			else
			{
				//CA("toms \001\004FAILED to create new item\001\004\n");
			}
	}
	while ( false );
}

bool16 PRImageHelper::EmptyThumbnailImageGrid()
{
	//CA(__FUNCTION__);
	//imagefileName.clear(); //This Vector is used in DCNActionComponent i.e. it is global so when product selection chages in productFinder plugin,it is cleared and filled with new filename.
	//typeName.clear();
	bool16 Flag = kFalse;
	do{
		if(assetInfoObj.imageVector1.size() <= 0)// imageVector1.size() <= 0)
			break;
		
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if ( app == NULL ) break;
		
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager());   //QueryPaletteManager());Modified By Sachinsharma on 23/06/07
		//if ( paletteMgr == NULL ) break;
		//
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if ( panelMgr == NULL ) break;

		/*IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		if ( !myPanel ) 
		{
			//CA("toms \001\004Panel not visible\001\004\n");
			break;
		}*/
		
		InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
		if (!panelControlData) 
		{
			//CA("toms \001\004NO IPanelControlData\001\004\n");
			break;
		}
		
		IControlView* gridWidget = panelControlData->FindWidget( kXLibraryItemGridWidgetId );
		if ( gridWidget == NULL )
		{
			//CA("toms \001\004FAILED to find widgets\001\004\n");
			break;
		}
		
		InterfacePtr<IXLibraryViewController> gridController( gridWidget,IID_IXLIBRARYVIEWCONTROLLER );
		if ( gridController == NULL )
		{
			//CA("toms \001\004NO IXLibraryViewController\001\004\n");
			break;
		}

		for(int i = assetInfoObj.imageVector1.size() - 1 ;i>=0; i--)//for(int i=imageVector1.size()-1; i>=0; i--)
		{
			gridController->DeleteViewItemWidgetAndUpdate(i);
		}
				
		Flag = kTrue;

	}while(0);
	
	assetInfoObj.clearAll();

	return Flag;
}

bool16 PRImageHelper::EmptyImageGrid(){

	//CA("PRImageHelper::EmptyImageGrid()");
	this->EmptyThumbnailImageGrid();	
	do
	{
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(app == NULL) 
		{ 
			//CA("EmptyImageGrid::No Application");
			break;
		}
		
		
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager()); //QueryPaletteManager()); modified By Sachin sharma 0n 23/06/07
		//if(paletteMgr == NULL) 
		//{ //CA("No IPaletteMgr");	
		//	break;
		//}
		//
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{		//CA("No IPanelMgr");	
			break;
		}
		
		/*IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		if(!myPanel) 
		{	CA("No PnlControlView");
			break;
		}*/
		
		InterfacePtr<IPanelControlData> panelControlData(globalPanelControlView, UseDefaultIID());
		if(!panelControlData) 
		{
			//CA("No PanelControlData");
			break;
		}
			
		IControlView* iView =panelControlData->FindWidget(kDCNMultilineTextWidgetID);
		if(iView == nil)
		{
			//CA("no iView");
			break;
		}

		InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			break;
		}	
		PMString blankString("");
		blankString.SetTranslatable(kFalse);
		txtData->SetString(blankString);


		IControlView* iView1 =panelControlData->FindWidget(kDCNFileSizeTextWidgetID);
		if(iView == nil)
		{
			//CA("no iView");
			break;
		}

		InterfacePtr<ITextControlData> txtData1(iView1,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			break;
		}	
		txtData1->SetString(blankString);
	}while(0);
	return (kTrue);
}

//added by vijay on 19/8/2006
//This method is wrapper method for AddThumbnailImage
 
 void PRImageHelper::WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName)
 {	
	 this->AddThumbnailImage(wrpperPath , wrpperDisplayName);
 }
 
 void PRImageHelper::setattributesWithPVMPV(vector<double>temp,bool16 isProduct)
 {
	//CA("PRImageHelper::setattributesWithPVMPV");
	if(isProduct)
		elementIDSWithPVMPV = temp;
	else
		attributesIDSWithPVMPV = temp;

 }
bool16 PRImageHelper::isAtt_EleIDSVecFilled()
{
	bool16 attOReleIDSVecFilled = kFalse;
	if(elementIDSWithPVMPV.size() > 0 && attributesIDSWithPVMPV.size() > 0)
		attOReleIDSVecFilled = kTrue;
	
	return attOReleIDSVecFilled;
 }