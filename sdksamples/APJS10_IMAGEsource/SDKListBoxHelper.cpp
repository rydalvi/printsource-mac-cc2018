//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/common/SDKListBoxHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
// Interface includes

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IWidgetUtils.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
// implem includes
#include "PersistUtils.h" // GetDatabase
#include "IPalettePanelUtils.h"

#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CAlert.h"

#include "SDKListBoxHelper.h"
#define CA(X) CAlert :: InformationAlert(X);

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown* owner, const PluginID& pluginID, 
								   const WidgetID& listBoxID, const WidgetID& owningWidgetID) : fOwner(owner), 
													fOwningPluginID(pluginID), fListboxID(listBoxID),
													fOwningWidgetID(owningWidgetID)
{

}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
}




IControlView * SDKListBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
		return nil;
	
	IControlView * listBoxControlView = nil;
	do {
		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		//ASSERT_MSG(iPanelControlData != nil, "SDKListBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) {
			//CA("No iPanelControlData");
			break;
		}
		listBoxControlView = 	iPanelControlData->FindWidget(fListboxID);
		//ASSERT_MSG(listBoxControlView != nil, "SDKListBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) {
			//CA("No listBoxControlView");
			break;
		}
	
	} while(0);

	return listBoxControlView;
}

 

void SDKListBoxHelper::AddElement( const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{

	//	CA(__FUNCTION__);

	if(!verifyState())
		return;

	do			// false loop
	{   
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			//CA("No ListBox");
			break;
		}
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) {
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwningPluginID, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {
			break;
		}
		this->AddListElementWidget(newElView, displayName, updateWidgetId, atIndex);
	
	}
	while (false);			// false loop
}


void SDKListBoxHelper::RemoveElementAt(int indexRemove)
{
	//CA(__FUNCTION__);
	if(!verifyState())
		return;

	do			// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) {
			// Don't remove outside of list data bounds
			break;
		}
		removeCellWidget(listBox, indexRemove);
		listControlData->Remove(indexRemove);
	}
	while (false);			// false loop
}



void SDKListBoxHelper::RemoveLastElement()
{
	//CA(__FUNCTION__);
	if(!verifyState())
		return;

	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex >= 0) {		
			listControlData->Remove(lastIndex);
			removeCellWidget(listBox, lastIndex);
		}
		
	}
	while (false);
}



int SDKListBoxHelper::GetElementCount() 
{
	//CA(__FUNCTION__);
	int retval=0;
	do {
	
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {

		if(listBox==nil) break;
		// recall that when the element is added, it is added as a child of the cell-panel
		// widget. Therefore, navigate to the cell panel and remove the child at the specified
		// index. Simultaneously, remove the corresponding element from the list controldata.
		// +
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) {
			break;
		}
		
		InterfacePtr<IControlView> view(cellPanelData->GetWidget(removeIndex), IID_ICONTROLVIEW);
		ASSERT_MSG(view != nil,"SDKListBoxHelper::removeCellWidget() view nil");
		
		cellPanelData->RemoveWidget(removeIndex);
		
		if( view )
			Utils<IWidgetUtils>()->DeleteWidgetAndChildren(view);
		// -

	} while(0);

}


void SDKListBoxHelper::AddListElementWidget(InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex)
{	
	//CA(__FUNCTION__);
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) {
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) {
			//CA("No IPanelcontrolData");
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ( (nameTextView == nil)  ) {
			//CA("No IControlView");
			break;
		}
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			//CA("No ITextControlData");
			break;
		}	
		
		newEltext->SetString(displayName, kTrue, kTrue);
		
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::AddListElementWidget() Cannot get panelData");
		if(panelData == nil) {
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::AddListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::AddListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) {
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "SDKListBoxHelper::AddListElementWidget() listData nil");
		if(listData == nil) { 
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}


void SDKListBoxHelper::EmptyCurrentListBox()
{
	//CA(__FUNCTION__);
	do {
		
		IControlView* listBoxControlView = this->FindCurrentListBox();
		if(listBoxControlView == nil) {
			
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) {
			
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) {
		
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) {
			
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) {
			
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}

