#include "VCPlugInHeaders.h"	

#include "ILayoutTarget.h"
#include "CAlert.h"

#include "ITextMiscellanySuite.h"
#include "CmdUtils.h"
#include "IFrameContentSuite.h"
//#include "PageItemUtils.h" //CS3 Depricated
#include "IPageItemUtils.h" //Cs4
//#include "PREMediatorClass.h"
#define CA(X) CAlert::InformationAlert(X)

class TextMiscellanySuiteLayoutCSB : public CPMUnknown<ITextMiscellanySuite>
{
public :

	TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss);
	virtual	~TextMiscellanySuiteLayoutCSB(void);
	virtual bool16 GetUidList(UIDList &);
	virtual bool16 setFrameUser(int32);

};

CREATE_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kDCNTextMiscellanySuiteLayoutCSBImpl)

TextMiscellanySuiteLayoutCSB::TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

/* Destructor
*/
TextMiscellanySuiteLayoutCSB::~TextMiscellanySuiteLayoutCSB(void)
{
}

bool16 TextMiscellanySuiteLayoutCSB::GetUidList(UIDList & TempUidList)
{
	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	const UIDList	selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	TempUidList = selectedItems;	
	return 1;
}
bool16 TextMiscellanySuiteLayoutCSB::setFrameUser(int32 OptionSelected)
{
	InterfacePtr<IFrameContentSuite>iFrame(this, IID_IFRAMECONTENTSUITE);
	switch(OptionSelected)
	{
	case 1:	//CA("case1");
			iFrame->FitContentToFrame();
			break;
	case 2:	//CA("case2");
			iFrame->FitFrameToContent();
			break;
	case 3:	//CA("case3");
			iFrame->FitContentProp();
			break;
	case 4:	//CA("case4");
			iFrame->CenterContentInFrame();
			break;
	case 5:
			iFrame->FillFrameProp();
			break;
	default:	break;
	}
	return kTrue;
}