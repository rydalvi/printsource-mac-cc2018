//========================================================================================
//
//  Owner: Adobe devsup
//
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  All information contained herein is, and remains
//  the property of Adobe Systems Incorporated and its suppliers,
//  if any.  The intellectual and technical concepts contained
//  herein are proprietary to Adobe Systems Incorporated and its
//  suppliers and may be covered by U.S. and Foreign Patents,
//  patents in process, and are protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material
//  is strictly forbidden unless prior written permission is obtained
//  from Adobe Systems Incorporated.
//
//========================================================================================

#ifdef __ODFRC__

//========================================================================================
// Show Info Dialog resource
//========================================================================================
resource XLibraryInfoPanel (kXLibraryInfoDialogRsrcID + index_enUS)
{
    __FILE__,__LINE__,
    kXLibraryPanelWidgetID0, kPMRsrcID_None,                    // WidgetId, RsrcId
    kBindNone,                            // Resize Constraints
    Frame(0,0,500,145)        // Frame
    kTrue, kTrue,                        // Visible, Enabled
    "Item Information",
    {
        DefaultButtonWidget
        (
            kOKButtonWidgetID,            // WidgetID
            kSysButtonPMRsrcId,            // RsrcId
            kBindNone,                    // Resize constraints
            Frame(390,15,485,35)        // Frame
            kTrue,                        // Visible
            kTrue,                        // Enabled
            "OK",                        // Panel name
        ),

        CancelButtonWidget
        (
            kCancelButton_WidgetID,        // WidgetID
            kSysButtonPMRsrcId,            // RsrcId
            kBindNone,                    // Resize constraints
            Frame(390,45,485,65)        // Frame
            kTrue,                        // Visible
            kTrue,                        // Enabled
            "Cancel",                    // Initial text
            kTrue,                            // Cancel changes to Reset
        ),

        StaticTextWidget
        (
            kXLibraryPanelWidgetID1,            // WidgetId
            kSysStaticTextPMRsrcId,            // RsrcId
            kBindNone,                        // Frame binding
            Frame(82,15,205,35)        // Frame
            kTrue, kTrue, kAlignRight,        // Visible, Enabled, Alignment
            kDontEllipsize,kTrue,                    // Ellipsize style
            kDCNEmptyDialogKey,                    // Initial text
            0, //kInfoNameWidgetId                // ShortCut link
        )
    }
};

#endif