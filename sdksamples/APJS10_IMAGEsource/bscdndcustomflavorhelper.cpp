//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdragdrop/BscDNDCustomFlavorHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

//Interface includes:
#include "CDragDropTargetFlavorHelper.h"
#include "IControlView.h"
#include "ILayoutUIUtils.h"
#include "ILayoutControlData.h"
#include "ICommand.h"
#include "IDataExchangeHandler.h"
#include "IGeometry.h"
#include "IPageItemLayerData.h"
#include "IPageItemScrapUtils.h"
#include "IPasteboardUtils.h"
#include "IDocument.h"
#include "IPageItemScrapData.h"
#include "ISpread.h"
#include "IShape.h"  //Added by SAchin Sharma on 25/06/07
#include "ILayoutSelectionSuite.h"

#include "IGeometryFacade.h"




//#include "IBoundsUtils.h"  //Commented By sachin Sharma on 23/06/07
#include "PMBezierCurve.h"

//#include "IMoveRelativeCmdData.h"   //Commented By Sachin sharma on 23/06/07
#include "IMoveTrackerData.h"  //added by sachin sharma
#include "ILayoutCmdData.h"
//#include "IBoundsUtils.h"//   Commented by sachin sharma o 23/06/07
#include "PMBezierCurve.h"

#include "IApplication.h"
//#include "IPaletteMgr.h"// Commented By Sachin sharma 23/06/07
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "SDKListBoxHelper.h"
#include <IListBoxController.h>

// General includes:
#include "PersistUtils.h"
#include "ErrorUtils.h"
#include "Utils.h"
#include "DataObjectIterator.h"
#include "CmdUtils.h"
#include "ICopyCmdData.h"
#include "ITransformCmdData.h"
#include "LayerID.h"
//#include "MediatorClass.h"

// Project includes:
#include "DCNID.h"
#include "CAlert.h"
#include "SDKUtilities.h"
#include "OpenPlaceID.h"
//#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "IReplaceCmdData.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ISelectionUtils.h"
#include "IHierarchy.h"
#include "CommonFunction.h"
#include "IListBoxController.h"
#include "PMString.h"
#include "SDKLayoutHelper.h"
#include "ISwatchList.h"
#include "IPathUtils.h"
#include "ISwatchList.h"
#include "ISwatchUtils.h"
#include "IGraphicAttributeUtils.h"
#include "K2Vector.tpp"
#include "AssetInfo.h"

#define CA(x) CAlert::InformationAlert(x)
#define CAI(X,Y) {PMString ASD(X);\
	ASD.Append(" : ");\
	ASD.AppendNumber(Y);\
	CAlert::InformationAlert(ASD);}
/** Extends the DND target capabilities of the layout widget to
	accept our custom flavor object.
	@ingroup basicdragdrop
*/
IListBoxController* listcontroller=NULL;
int32 ind;
//extern K2Vector<PMString> imageVector1 ;
int32 imageIndex= -1;
extern K2Vector<int32> libSelectionList;
extern PMString imPath;
extern K2Vector<int32> curSelection ;
extern  bool16 isThumb;
extern  bool16 BrowseFolderOption;
extern  bool16 isThroughPrintSourceMenu;
extern K2Vector<bool16> imageBoolFlagVector;
extern IListBoxController *global_list_box_controller ; 


extern AssetInfo assetInfoObj;
class BscDNDCustomFlavorHelper : public CDragDropTargetFlavorHelper
{
public:
	/**
		Constructor.
		@param boss IN the boss class the interface is aggregated onto.
	*/
	BscDNDCustomFlavorHelper(IPMUnknown* boss);
	/**
		Destructor.
	*/
	virtual	~BscDNDCustomFlavorHelper();

	/**
		Determines whether we can handle the flavors in the drag.
		@param target IN the target the mouse is currently over.
		@param dataIter IN iterator providing access to the data objects within the drag.
		@param fromSource IN the source of the drag.
		@param controller IN the drag drop controller mediating the drag.
		@return a target response (either won't accept or drop will copy).

		@see DragDrop::TargetResponse
	*/
	virtual DragDrop::TargetResponse CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const;

	/**
		performs the actual drag. Because we know that our custom flavor is masquerading and is 
		really a page item we must take a copy of the page item, then move this copy to the drop zone.
		We know we have been dropped on a widget (because we are called as part of that widget responding to
		a drop of the custom flavor).
		@param target IN the target for this drop.
		@param controller IN the drag drop controller that is mediating the drag.
		@param action IN what the drop means (i.e. copy, move etc)
		@return kSuccess if the drop is executed without error, kFailure, otherwise.
	*/
	virtual ErrorCode ProcessDragDropCommand(IDragDropTarget*, IDragDropController*, DragDrop::eCommandType);

	DECLARE_HELPER_METHODS()
	/*bool16 ImportFileInFrame
	(const UIDRef& imageBox, const PMString& fromPath);

	void fitImageInBox(const UIDRef& boxUIDRef);*/
	//virtual UIDRef CreateRectangleFrame(ILayoutControlData* layoutControlData,const PMRect& boundsInPageCoords);
	virtual ErrorCode CreatePicturebox(UIDRef&, UIDRef, PMRect, PMReal strokeWeight=0.0);


	//Added by aschin Sharma on 26/06/07
	enum   LayoutScrollChoice { kDontScrollLayoutSelection, kScrollIntoViewIfNecessary, kAlwaysCenterInView } ;


};


//--------------------------------------------------------------------------------------
// Class LayoutDDTargetFileFlavorHelper
//--------------------------------------------------------------------------------------
CREATE_PMINTERFACE(BscDNDCustomFlavorHelper, kDCNDNDCustomFlavorHelperImpl)
DEFINE_HELPER_METHODS(BscDNDCustomFlavorHelper)



BscDNDCustomFlavorHelper::BscDNDCustomFlavorHelper(IPMUnknown* boss) : CDragDropTargetFlavorHelper(boss), HELPER_METHODS_INIT(boss)
{
}

BscDNDCustomFlavorHelper::~BscDNDCustomFlavorHelper()
{
}

/* Determine if we can handle the drop type */
DragDrop::TargetResponse 
BscDNDCustomFlavorHelper::CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const
{

	// Check the available external flavors to see if we can handle any of them
	if (dataIter != nil)
	{
		// Test for swatches in the drag
		DataExchangeResponse response;		
		response = dataIter->FlavorExistsWithPriorityInAllObjects(customFlavorAssetBr);
		if (response.CanDo())
		{
			//CA(" DragDrop::TargetResponse True ");
			return DragDrop::TargetResponse(response, DragDrop::kDropWillCopy);
		}
	}
	//CA("DragDrop::kWontAcceptTargetResponse");
	return DragDrop::kWontAcceptTargetResponse;
}

/* process the drop, this method called if CouldAcceptTypes returns valid response */
ErrorCode BscDNDCustomFlavorHelper::ProcessDragDropCommand( IDragDropTarget* 			target, 
															IDragDropController* 		controller,
															DragDrop::eCommandType		action )
{	
	//CA(__FUNCTION__);
	ErrorCode stat = kFailure;
	ICommandSequence* sequence = CmdUtils::BeginCommandSequence();
	if (sequence == nil){
		//CA("Cannot create command sequence?");
		return stat;
	}

	sequence->SetName(PMString("ProcessDragDropCommand", PMString::kUnknownEncoding)); //Cs4
	do{
		if (action != DragDrop::kDropCommand) { 
			//CA("!action");
			break;
		}

		InterfacePtr<IControlView> layoutView(target, UseDefaultIID());
		if (!layoutView) { 
			//CA("nil layoutView");
			break;
		}

		InterfacePtr<ILayoutControlData> layoutData(target, UseDefaultIID());
		if (!layoutData) { 
			//CA("nil layoutData");
			break;
		}


		//Position of mouse is found where the item is dragged in the Layout
		GSysPoint where = controller->GetDragMouseLocation();
		//Co-Ordinates are converted to PasteBoard
		PMPoint currentPoint = Utils<ILayoutUIUtils>()->GlobalToPasteboard(layoutView, where);

		// Determine the target spread to drop the items into.
		InterfacePtr<ISpread> targetSpread(Utils<IPasteboardUtils>()->QueryNearestSpread(layoutView, currentPoint));
		if (!targetSpread) { 
			//CA("!targetSpread");
			break;
		}

		UIDRef targetSpreadUIDRef = ::GetUIDRef(targetSpread);
		if (targetSpreadUIDRef.GetUID() == kInvalidUID){// CA("Invalid UID"); 
			break; }
	
		//imageIndex = libSelectionList[0] ;
		const UIDRef spreadRef=layoutData->GetSpreadRef();

		if ( targetSpreadUIDRef != spreadRef)       //::GetUIDRef(layoutData->GetSpread())) Commented By sachin Sharma
		{			
			InterfacePtr<ICommand> setSpreadCmd(CmdUtils::CreateCommand(kSetSpreadCmdBoss));
			if (!setSpreadCmd) {
				//CA("setSpreadCmd is null");
				break;
			}

			InterfacePtr<ILayoutCmdData> setSpreadLayoutCmdData(setSpreadCmd, UseDefaultIID());
			if (!setSpreadLayoutCmdData) { 
				//CA("setSpreadLayoutCmdData is null");
				break;
			}

			setSpreadLayoutCmdData->Set(::GetUIDRef(layoutData->GetDocument()), layoutData);
			setSpreadCmd->SetItemList(UIDList(targetSpreadUIDRef));
			stat = CmdUtils::ProcessCommand(setSpreadCmd);
		}

		// Get a list of the page items in the drag.
		stat = controller->InternalizeDrag(kNoExternalFlavor, customFlavorAssetBr);
		if (stat != kSuccess) { 

			//CA("ffff");
			break;
		}

		InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
		if (!handler) { 
			//CA("handler is null");
			stat = kFailure;
			break;
		}

		InterfacePtr<IPageItemScrapData> pageItemData(handler, UseDefaultIID());
		if (!pageItemData) { 
			stat = kFailure;
			//CA("pageItemData is null");
			break;
		}

		InterfacePtr<IPageItemLayerData> layerData(handler, IID_IPAGEITEMLAYERDATA);
		if (!layerData) { 
			stat = kFailure;
			//CA("IPageItemLayerData is NULL");
			break;
		}
		//.............................
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(app == NULL) 
		{ 
			//CA("No Application");
			break;
		}
		//Modified By Sachin Sharma on 25/06/07
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager()); //QueryPaletteManager()); Modified Bt SAchin Sharma on 23/06/07
		//if(paletteMgr == NULL) 
		//{ 
		// CA("No IPaletteMgr");	
		//  break;
		//}
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
		
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL) 
		{	
			//CA("No IPanelMgr");	
			break;
		}
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);  
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(!myPanel) 
		{
			//CA("No PnlControlView");
			break;
		}
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(!panelControlData) 
		{
			//CA("No PanelControlData");
			break;
		}
		IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
		if(lstgridWidget == NULL)
		{
			//CA("No ListPanelControlView");
			break;
		}
		IControlView* lstViewImageControlView = panelControlData->FindWidget(kDCNlstViewImagePnlWidgetID);
		if(lstViewImageControlView == NULL)
		{
			//CA("No lstViewImageControlView");
			break;
		}

		//SDKListBoxHelper lstBoxHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
		//IControlView* listBoxControlView = lstBoxHelper.FindCurrentListBox();
		//if(listBoxControlView == NULL)
		//{
			//CA("listBoxControlView is null");
		//}
		//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		//if(listCntl == nil) 
		//{
		//	 //CA("listCntl is nil");
		//	break;
		//}
		//listcontroller=listCntl;
		//.............................

		UIDList* draggedItemList = pageItemData->CreateUIDList();		
		// Duplicate the drag content in the target spread.Parent the duplicated page item in the active spread layer.
		UIDRef parent(targetSpreadUIDRef.GetDataBase(), layoutData->GetActiveLayerUID());
		// See IPageItemLayerData if you want to maintain layer information across the drop.
		K2Vector<PMString>* layerNameList = nil;
		K2Vector<int32>* layerIndexList = nil;
//+++==================================================================
//Callers of the IPageItemScrapUtils::CreateDuplicateCommand() should be replaced with the
//following sequence:
		InterfacePtr<ICommand>	duplicateCmd( CmdUtils::CreateCommand(kDuplicateCmdBoss));	
		ASSERT(duplicateCmd);
		if (!duplicateCmd) {
			delete draggedItemList;
			stat = kFailure;
			break;
		}
		InterfacePtr<ICopyCmdData> cmdData(duplicateCmd, IID_ICOPYCMDDATA);
		// Put items on designated parent layer.
		cmdData->Set(duplicateCmd, draggedItemList, parent);
		cmdData->SetOffset( PMPoint(0,0));

		stat = CmdUtils::ProcessCommand(duplicateCmd);
		const UIDList* copiedItemList = duplicateCmd->GetItemList();
		ASSERT_MSG(stat == kSuccess && copiedItemList != nil, "Failed to duplicate dropped item");
		if (stat != kSuccess || copiedItemList== NULL) {
			break;
		}


		int32 lengthOfCopiedItem = copiedItemList->Length();
		PMPoint AddSpace(0,0);
		UIDRef globalUIDRef = UIDRef::gNull;
		bool16 checkOverLap = kFalse , checkForFirstTime = kFalse;
		PMReal margin = 0;
		PMRect pageBounds;
		CommonFunction commonFunctionObj;

		for(int p = 0; p < lengthOfCopiedItem; p++)
		{
			PMString fromPath("");
			UIDRef newUIDRef ;
			UIDList uidList;
			PMString image("");
			
			
			//Co-Ordinates of the Particular Image is calculated 
			
			/*PMRect itemsBoundingBox =GetStrokeBoundingBox();
			
			PMPoint itemsBoundsCenter = itemsBoundingBox.GetCenter();*/
//==================================================================================
			PMRect itemsBoundingBox = Utils<Facade::IGeometryFacade>()->GetItemsBounds(*copiedItemList, Transform::PasteboardCoordinates(), Geometry::OuterStrokeBounds());
			PMPoint itemsBoundsCenter = itemsBoundingBox.GetCenter();

//=================================================================================


			//Command is created due to Un-do,Re-do,
//			InterfacePtr<ICommand> moveRelativeCmd(CmdUtils::CreateCommand(/*kTransformPageItemsCmdBoss*/kMoveRelativeCmdBoss ));
//			if (!moveRelativeCmd) { 
//				stat = kFailure;
//				CA("moveRelativeCmd iss");
//				break;
//			}
//
//			InterfacePtr<IMoveTrackerData> moveRelativeCmdData(moveRelativeCmd, UseDefaultIID());
//			if (!moveRelativeCmdData) { 
//				CA("moveRelativeCmdData is null");
//				stat = kFailure;
//				break;
//			}
//			//Commented By Sachin Sharma
//			//moveRelativeCmdData->Set(currentPoint - itemsBoundsCenter + AddSpace);
//			//moveRelativeCmd->SetItemList(UIDList(copiedItemList->GetRef(p)));
////========================================================
//			PBPMPoint&  pnt(currentPoint - itemsBoundsCenter + AddSpace);
//			UIDList* myUidList=const_cast<UIDList*>(copiedItemList);
//			moveRelativeCmdData->Set(myUidList,pnt,0,kTrue);
////==========================================
//			moveRelativeCmd->SetItemList(UIDList(copiedItemList->GetRef(p)));
//			stat = CmdUtils::ProcessCommand(moveRelativeCmd);
//			if (stat != kSuccess) {
//				CA("Failure");
//				break;
//			}
			InterfacePtr<ICommand> moveRelativeCmd(CmdUtils::CreateCommand(/*kMoveRelativeCmdBoss*/kTransformPageItemsCmdBoss));
			ASSERT(moveRelativeCmd);
			if (!moveRelativeCmd) 
			{
				//CA("moveRelativeCmd is NULL");
				break;
			}
		
			/*InterfacePtr<IMoveRelativeCmdData> moveRelativeCmdData(moveRelativeCmd, UseDefaultIID());
			ASSERT(moveRelativeCmdData);
			if (!moveRelativeCmdData) 
			{
				break;
			}
			moveRelativeCmdData->Set(currentPoint - itemsBoundsCenter);*/

			PMPoint delta(currentPoint - itemsBoundsCenter + AddSpace);
			InterfacePtr<ITransformCmdData> moveRelativeCmdData (moveRelativeCmd, UseDefaultIID()); 
			if(moveRelativeCmdData == NULL)
			{
				//CA("moveRelativeCmdData is NULL");
				break;
			}
			moveRelativeCmdData->SetTransformData( Transform::PasteboardCoordinates(), PMPoint(0,0), Transform::TranslateBy(delta.X(), delta.Y())); 
			
			moveRelativeCmd->SetItemList(UIDList(*copiedItemList));
			stat = CmdUtils::ProcessCommand(moveRelativeCmd);
			ASSERT_MSG(stat == kSuccess, "Failed to position dropped items");
			if (stat != kSuccess) 
			{
				break;
			}


			
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
			if (!layoutSelectionSuite){ 
				break;
			}
				
			PMReal Xdist = AddSpace.X();
			Xdist = Xdist + 120.0;
			AddSpace.X(Xdist);

			UIDList imageBoxUIDLIst = *(moveRelativeCmd->GetItemList());
			UIDRef ImageUidRef = UIDRef::gNull;
			if(checkForFirstTime == kTrue)
				ImageUidRef = imageBoxUIDLIst.GetRef(0);
			else
				ImageUidRef = imageBoxUIDLIst.GetRef(p);
			//UIDRef ImageUidRef = imageBoxUIDLIst.GetRef(libSelectionList[p]);
			
			fromPath.Append(assetInfoObj.imageVector1[libSelectionList[p]]);//imageVector1[libSelectionList[p]]);
			
			if(globalUIDRef != UIDRef::gNull){
				newUIDRef = globalUIDRef;
			}
			//CA(fromPath + " :1 st ");
			if(!commonFunctionObj.checkForOverLaps(ImageUidRef,0,newUIDRef,uidList)&& checkOverLap == kFalse){
				checkForFirstTime = kTrue;
				bool16 check = commonFunctionObj.ImportFileInFrame(ImageUidRef,fromPath);
				if(check == kFalse)
				{
					//CA("Problem in ImportFileIn Box ");
					return stat;
				}
				
				if(imageBoolFlagVector[p])
				{
					//CA("Before fitImageInBox");
					commonFunctionObj.fitImageInBox(ImageUidRef);
				}
				
				layoutSelectionSuite->SelectPageItems (*(moveRelativeCmd->GetItemList()), Selection::kAddTo ,Selection::kDontScrollLayoutSelection );
				
				//if(isThumb == kTrue){
					int32 num = libSelectionList[p] ;
					ind=num;
					if( BrowseFolderOption == kFalse )
					{	
						//CA("Before addTagToGraphicFrame");
						commonFunctionObj.addTagToGraphicFrame(ImageUidRef,num,1);
					}
					
				//}
				/*if(isThumb == kFalse){
					CA("inside addTag");
					int32 number =curSelection[p];
					PMString data;
					data.AppendNumber(number);
					commonFunctionObj.addTagToGraphicFrame(ImageUidRef,number,1);
				}*/
			}
			else{
				//CA("Else" + fromPath);
				checkOverLap = kTrue;
				//if(isThumb == kTrue)
				bool16 check = commonFunctionObj.ImportFileInFrame(newUIDRef,fromPath);
				//if(isThumb == kFalse){
				//	//CA("insideimport");
				//bool16 check1 = commonFunctionObj.ImportFileInFrame(newUIDRef,image);
				//}
				if(imageBoolFlagVector[p]){
					//CA("Before fitImageInBox");
					commonFunctionObj.fitImageInBox(newUIDRef);
				}

				if(margin == 0){
					commonFunctionObj.getBoxDimensions(newUIDRef,pageBounds);
					margin = pageBounds.Right() -pageBounds.Left() +2;
				}

				pageBounds.Top(pageBounds.Top());
				pageBounds.Bottom(pageBounds.Bottom());
				pageBounds.Left(pageBounds.Left()+margin);
				pageBounds.Right(pageBounds.Right()+margin);
				
				if(lengthOfCopiedItem >= 1 && p < (lengthOfCopiedItem - 1)){
					UIDRef newTarget;
					InterfacePtr<IHierarchy> iHier(newUIDRef, UseDefaultIID());
					if(!iHier)
					{
						//CA("iHier is NULL");					
						break;				
					}
					UIDRef parentUIDRef(newUIDRef.GetDataBase(), iHier->GetParentUID());
					this->CreatePicturebox(newTarget,parentUIDRef,pageBounds);
					//layoutSelectionSuite->DeselectAll();
//+++=======================================================Added By SAchin Sharma on 26/06/07
					InterfacePtr<IControlView> layoutView(target, UseDefaultIID());
					if (!layoutView) { 
						//CA("layoutView is NULL");
					break;
					}	
//+++===============================================================================
					//layoutSelectionSuite->SelectInView(newTarget.GetUID(),Selection::kAddTo,Selection::kDontScrollLayoutSelection,0,kTrue,kScrollIntoViewIfNecessary);  //Modified By Sachin sharma on 25/06/07 Select Method is Replaced by SelectInView
					//layoutSelectionSuite->Select(*(moveRelativeCmd->GetItemList()), Selection::kAddTo ,Selection::kDontScrollLayoutSelection );
					layoutSelectionSuite->SelectInView(layoutView, itemsBoundingBox,Selection::kAddTo ,0,kTrue,Selection::kScrollIntoViewIfNecessary );  //Modified By Sachin sharma on 25/06/07 Select Method is Replaced by SelectInView
					globalUIDRef = newTarget;
				}
				margin = pageBounds.Right() -pageBounds.Left() +2;
				//if(isThumb == kTrue)
				//{
					int32 num = libSelectionList[p] ;
					if( BrowseFolderOption == kFalse )
					{
						
						commonFunctionObj.addTagToGraphicFrame(newUIDRef,num,1);
					}
				//}
				//if(isThumb == kFalse)
				//{	//CA("inside kFalse");
				//	int32 number = libSelectionList[p];
				//	commonFunctionObj.addTagToGraphicFrame(newUIDRef,number,1);
				//}
				commonFunctionObj.deleteThisBox(/*ImageUidRef*/imageBoxUIDLIst.GetRef(0));
			}

		}
	} while(false);
	stat=kSuccess;

	//
	//if(stat == kSuccess)
	//{
	//		// Everything completed so end the command sequence
	//	CmdUtils::EndCommandSequence(sequence);
	//}
	//else
	//{
	//	// Abort the sequence and roll back the changes
	//	
	//	CmdUtils::AbortCommandSequence(sequence);
	//}
	CmdUtils::EndCommandSequence(sequence);//Added By Sachin Sharma 
	//listcontroller->DeselectAll(kTrue,kTrue);
	//listcontroller->Select(ind);
	return stat;
	
}

// End, BscDNDCustomFlavorHelper.cpp.

//bool16 BscDNDCustomFlavorHelper::ImportFileInFrame
//(const UIDRef& imageBox, const PMString& fromPath)
//{
//
//	if(imageBox.GetUID() == kInvalidUID)	 CA("InValid UID");
//
//	CA(fromPath);
//	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
//	if(!fileExists) 
//		return kFalse;	
//	
//	/*IDocument *docPtr=::GetFrontDocument();
//	IDataBase* db = ::GetDataBase(docPtr);*/
//
//	//IDocument* doc =::GetFrontDocument(); 
//	CA("1.1");
//	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
//	IDataBase* db = ::GetDataBase(doc); 
//	CA("1.2");
//	if (db == nil)
//		return kFalse;
//	CA("1.3");
//	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
//	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
//	if(!importCmd) 
//		return kFalse;
//	CA("1.4");		
//	InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
//	if(!importFileCmdData)
//		return kFalse;
//	CA("1.5");
//	importFileCmdData->Set(db, sysFile, kMinimalUI);
//	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
//	if(err != kSuccess) 
//		return kFalse;
//	
//	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
//	if(!placeGun)
//		return kFalse;
//	
//	UIDRef placedItem(db, placeGun->GetItemUID());
//	
//	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
//	if (replaceCmd == nil)
//		return kFalse;
//	CA("1.6");
//	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
//	if(!iRepData)
//		return kFalse;
//	
//	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);
//	CA("1.7");
//	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
//	if(status==kFailure)
//	{
//		CA("status==kFailure");	
//		return kFalse;
//	}
//	return kTrue;
//}
//
//void BscDNDCustomFlavorHelper::fitImageInBox(const UIDRef& boxUIDRef)
//{
//	do 
//	{
//		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
//		if(!iAlignCmd)
//			return;
//
//		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
//		if(!iHier)
//			return;
//
//		if(iHier->GetChildCount()==0)//There may not be any image at all????
//			return;
//
//		UID childUID=iHier->GetChildUID(0);
//
//		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
//
//
//		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
//
//		CmdUtils::ProcessCommand(iAlignCmd);
//	} while (false);
//}


//UIDRef BscDNDCustomFlavorHelper::CreateRectangleFrame(ILayoutControlData* layoutControlData,const PMRect& boundsInPageCoords)
//{
//	UIDRef result = UIDRef::gNull;
//	SDKLayoutHelper layoutHelper;
//	do {
//		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
//		if (activeSpreadLayerHierarchy == nil) {break;}
//
//		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);
//		if (layoutControlData->GetPage() == kInvalidUID) {break;}
//
//		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
//		PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, boundsInPageCoords);
//
//		result = layoutHelper.CreateRectangleFrame(parentUIDRef, boundsInParentCoords);
//	} while(false);
//	return result;
//}


ErrorCode BscDNDCustomFlavorHelper::CreatePicturebox(UIDRef& newPageItem, UIDRef parent, PMRect rect, PMReal strokeWeight)
{
	//CA("CreatePicturebox inside");
	ErrorCode returnValue = kFailure;
	do{
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil){
			CAlert::InformationAlert("ICommandSequence nil");
			break;
		}

		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil){
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure){
			ASSERT_FAIL("Failed to stroke the spline?");
			CAlert::InformationAlert("ProcessCommand failure");
			break;
		}

		//InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		//if(iSwatchList==nil){
		//	ASSERT_FAIL("Cannot get swatch list?");
		//	CAlert::InformationAlert("ISwatchList nil");
		//	break;
		//}

		//UID blackUID = iSwatchList->/*GetBlackSwatchUID*/GetNoneSwatchUID();
		//
		//InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		//if(applyStrokeCmd==nil){
		//	ASSERT_FAIL("Cannot create the command to render the stroke?");
		//	CAlert::InformationAlert("ICommand nil");
		//	break;
		//}
		//
		//if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess){
		//	ASSERT_FAIL("Failed to render the stroke?");
		//	CAlert::InformationAlert("ProcessCommand failre");
		//	break;
		//}
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	}
	while(false);
	return returnValue;
}