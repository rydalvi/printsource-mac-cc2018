#include "VCPluginHeaders.h"

// Interface includes
#include "ISelectionManager.h"
#include "ITableSuite.h"
#include "ITableAttrRealNumber.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextValue.h"
#include "CAlert.h"
#include "ControlStripID.h"
#include "CEventHandler.h"
#include "IBoolData.h"
//#include "Utills.h"
#include"IDocumentList.h"
#include"IDocument.h"
#include"IDocumentUtils.h"
#include"IApplication.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "PREMediatorClass.h"
#include "SelectionObserver.h"
#include "IGraphicFrameData.h"
#include "IEvent.h"
#include "IListBoxController.h"
#include "SDKFileHelper.h"
#include "StreamUtil.h"
#include "ISysFileData.h"
//#include "ILoggerFile.h"
///////////////////
#include "ICommand.h"
#include "CmdUtils.h"
#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "IReplaceCmdData.h"
#include "FileUtils.h"
#include "IHierarchy.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "SDKLayoutHelper.h"
#include "ISpread.h"
#include "IPasteboardUtils.h"
#include "LayoutUtils.h"
#include "SDKUtilities.h"

//S#include "GlobalData.h"
#include "KeyboardDefs.h"	
#include <ICommand.h>
#include <CmdUtils.h>
#include <IImportFileCmdData.h>
#include <IPlaceGun.h>
#include <IFrameContentSuite.h>



#include "DCNID.h"
#define CA(X) CAlert::InformationAlert(X)


//Conversion CS2
#include "ILayoutUIUtils.h"

extern K2Vector<PMString> imageVector1 ;
extern int32 imageIndex ;  

namespace
{
	bool16 fitImageAndFrameToEachOther(int num)
	{
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{	
				return kFalse ;
			}

			InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
			if(!txtMisSuite)
			{	
				return kFalse ; 
			}

			switch(num)
			{
				case 3 :
				txtMisSuite->setFrameUser(3); // first resize photo ......
				break ;

				case 2 :
				txtMisSuite->setFrameUser(2); // then resize frame .....
				break ;
			}

			return kTrue ;
		}

}


/**
	Class to observe changes in the active context, and change UI when attributes change.
	@ingroup tableattributes
 */
class ActSel : public CEventHandler
{
	public :
		
		ActSel(IPMUnknown *boss);

		virtual ~ActSel()
		{
		
		};

		/*
			on this double click , 
			image and frame will be resized to fit to each other
		*/
		virtual bool16 ButtonDblClk(IEvent* e);

		IPMUnknown *boss_ ;
		
};

CREATE_PMINTERFACE(ActSel, kDCNFrameObserverImpl);

ActSel::ActSel(IPMUnknown *boss) :
	CEventHandler(boss)
{
	boss_ = boss ;
}

bool16 ActSel::ButtonDblClk(IEvent* e)
{
	/*
		on double click we always fit image to frame ..
		hence input to function is 3 
	*/
	bool16 result = fitImageAndFrameToEachOther(3);

	return CEventHandler::ButtonDblClk(e);
}
