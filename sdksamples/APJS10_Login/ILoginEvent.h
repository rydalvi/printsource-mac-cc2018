#ifndef __ILoginEvent_h__
#define __ILoginEvent_h__

#include "VCPlugInHeaders.h"
#include "IPMUnknown.h"
#include "LNGID.h"

class ILoginEvent : public IPMUnknown
{
	public:	
		int32 num;
		enum { kDefaultIID = IID_ILOGINEVENT };
		virtual bool16 userLoggedIn()=0;
		virtual bool16 userLoggedOut()=0;
		virtual bool16 resetPlugIn()=0;
};
#endif