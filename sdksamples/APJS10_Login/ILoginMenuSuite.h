#ifndef _ILoginMenuSuite_
#define _ILoginMenuSuite_

// Interface includes:
#include "IPMUnknown.h"
#include "ShuksanID.h"
#include "LNGID.h"


class ILoginMenuSuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ILNGMNU_ISUITE };
public:
	virtual bool16 CanApplyLoginMenu(void )=0;
	virtual bool16 CanApplyLoginMenuForDoc(void )=0;
	
};
#endif // _ITextMiscellanySuite_
