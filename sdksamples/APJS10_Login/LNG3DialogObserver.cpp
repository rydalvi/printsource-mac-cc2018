/*
//	File:	LNG3DialogObserver.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"
// Project includes:
#include "LNGID.h"
#include "IAppFramework.h"
#include "envData.h"

/** LNG3DialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class LNG3DialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG3DialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~LNG3DialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG3DialogObserver, kLNG3DialogObserverImpl)

/* AutoAttach
*/
void LNG3DialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){			
			break;
		}
		// Attach to other widgets you want to handle dynamically here.		
		AttachToWidget(kLNG3OkButtonWidgetID, IID_IBOOLEANCONTROLDATA,panelControlData);

	} while (false);
}

/* AutoDetach
*/
void LNG3DialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){			
			break;
		}
		// Detach from other widgets you handle dynamically here.		
		DetachFromWidget(kLNG3OkButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);		
	} while (false);
}

/* Update
*/
void LNG3DialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil){
			CAlert::InformationAlert("");
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		if(theSelectedWidget== kOKButtonWidgetID && theChange == kTrueStateMessage)
		{
					
			bool16 result;				
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				break;
			}
			// update the existing record
			result=ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(EvnironmentData::getServerProperties(),EvnironmentData::getIsNewServer(),"c:\\","AvailableServers.properties");
			if(result==-1){
				ptrIAppFramework->UTILITYMngr_dispErrorMessage();
				break;
			}
			if(EvnironmentData::getIsNewServer()==TRUE){ // new server
				//LOGINMngr_deleteUser(EvnironmentData::getSelectedserverEnvName(),"c:\\","AvailableServers.properties");
				//addnewserver();
			}
			else{
				// update erver if environment and servername changes
			}
		}		
	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, LNG3DialogObserver.cpp.



