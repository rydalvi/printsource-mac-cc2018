//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "LNGID.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "LNGActionComponent.h"
#include "LNGMediator.h"
#include "LNGPlugInEntrypoint.h"
#include "ILoginEvent.h"
#include "LoginHelper.h"
#include "LNGTreeDataCache.h"

extern PMString selectedClientName;


//#define CA(X) CAlert::InformationAlert(X)
IDialog* CurrFirstDlgPtr=0;
IDialog* CurrLatestDlgPtr=0;

IDialog* LoginDlgPtr=0;
IDialog* LaunchPadDlgPtr=0;

extern bool16 bResult;

PMString LNGActionComponent::printSourceUpdatesURL= "" ;  //**** For Updates

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup ap7_login

 CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(LNGActionComponent, kLNGLaunchpadDlgActionCompImpl)
IDialog* LNGActionComponent::curDlgPtr = 0 ;

/* LNGActionComponent Constructor
*/
LNGActionComponent::LNGActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}


LNGActionComponent::~LNGActionComponent()
{	
	InterfacePtr<IApplication>app(GetExecutionContextSession()->QueryApplication());
	if(app)
	{
		//CA("app");
		IApplication::ApplicationStateType type1 = app->GetApplicationState();
		if(type1 == IApplication::kShuttingDown)
		{
			//CA("before PurgeMemory Login");
			///*gSession*/GetExecutionContextSession()->PurgeMemory(5);
			//CA("After PurgeMemory");
			//::exit(0);
		}
	}
}

IDialog* LNGActionComponent::getDlgPtr(void)
{
	return curDlgPtr;
}

/* DoAction
*/

void LNGActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	
	switch (actionID.Get())
	{
		case kLNGAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kLNGDialogActionID:
		{
			//CA("case kLNGDialogActionID");
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				break;
			} 
			
			bool16 status = ptrIAppFramework->getLoginStatus();
			if(status != kTrue)
			{	  
				//CA("status != kTrue");

				this->DoDialog(2); // opening the Login Dialog	
				LoginHelper LoginHelperObj(this);		
		
				VectorLoginInfoPtr result=NULL;
				result=	LoginHelperObj.getAvailableServers();
				if(result== NULL){
					PMString NoEnVSet(kLNGNoEnVSetTextKey);
					CAlert::InformationAlert(NoEnVSet);
					//CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
					break;
				}
				
			}
			else
				showLogoutPrintDialog();

			break;
		}

//		case kLNGResetActionID:
//		{
//			
//			//CA("inside kLNGResetActionID");
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == nil){
//				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//				break;
//			} 
//			
//			bool16 status = ptrIAppFramework->getLoginStatus();
//			if(status == kTrue)
//			{	 
//				fireResetPlugins();
//			}
//			break;
//		}
		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void LNGActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kLNGAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void LNGActionComponent::DoDialog(int dlgID)
{
	int id=kLaunchpadDialogResourceID;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		//ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		//ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		if(dlgID==2)		 
		id=kLoginDialogRsrcID; 
		 else if(dlgID==3)
			id=kProp2DialogRsrcID; 
		 else if (dlgID==4) 			  
			 id =kProp2DialogRsrcID; 
		 else if(dlgID==5)
			 id = kPropDialogRsrcID;

		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kLNGPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			id, //kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog;
		if(dlgID == 2)
			dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::/*kModeless*/ kMovableModal);
		else
			dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);

		//ASSERT(dialog);
		if (dialog == nil) {
			break;
		}
		curDlgPtr = dialog;
		if(id==kLoginDialogRsrcID/*kLaunchpadDialogResourceID*/)
			CurrFirstDlgPtr = dialog;
		CurrLatestDlgPtr = dialog;
		// Open the dialog.
		dialog->Open();
		
	
	} while (false);			
}

void LNGActionComponent::showLogoutPrintDialog(void)
{	
	bool8 WBdlgOpenFlag=kFalse,TmpldlgOpenFlag=kFalse,PGSdlgOpenFlag=kFalse;
	do
	{	
//		InterfacePtr<ITemplatesDialogCloser> TmpldlgCloser((ITemplatesDialogCloser*) ::CreateObject(kTemplatesDialogCloserBoss,IID_ITEMPLATEDIALOGCLOSER));
//		if(TmpldlgCloser == nil){
			//CAlert::InformationAlert("Fail to get ITemplatesDialogCloser interface....");
			//break;
//		}
	//	TmpldlgOpenFlag = TmpldlgCloser->IsDialogOpen();

//		InterfacePtr<IPGSDialogCloser> PGSdlgCloser((IPGSDialogCloser*) ::CreateObject(kPGSDialogCloserBoss,IID_IPGSDIALOGCLOSER));
//		if(PGSdlgCloser == nil){
			//CAlert::InformationAlert("Fail to get IPGSDialogCloser interface....");
			//break;
//		}
		//PGSdlgOpenFlag = PGSdlgCloser->IsDialogOpen();
//		InterfacePtr<IWBDialogCloser> WBdlgCloser((IWBDialogCloser*) ::CreateObject(kWBDialogCloserBoss,IID_IWBDIALOGCLOSER));
//		if(WBdlgCloser == nil){
			//CAlert::InformationAlert("Fail to get IWBDialogCloser interface....");
			//break;
//		}
		//WBdlgOpenFlag = WBdlgCloser->IsDialogOpen();
//		InterfacePtr<ISprayer> iPageSprayer((ISprayer*)::CreateObject(kSPRDialogBoss,IID_ISPRAYER));	
//		if(iPageSprayer == nil)	{ //CA("Sprayer not Detected");
		//break;
//		}
		//if(TmpldlgOpenFlag || PGSdlgOpenFlag || WBdlgOpenFlag)		
//		{
			PMString message("You will be logged out of Catsy. This will close all the active Catsy dialogs/palettes. Do you still want to continue?   ");
			message.SetTranslatable(kFalse);
			PMString ok_btn("OK"); //1
			ok_btn.SetTranslatable(kFalse);
			PMString cancel_btn("Cancel");//2
			cancel_btn.SetTranslatable(kFalse);
			int32 isLogout= CAlert::ModalAlert(message,ok_btn,cancel_btn,"",2,CAlert::eQuestionIcon);
			if(isLogout==1)
			{
				//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					CAlert::InformationAlert("Pointer to IAppFramework is nil.");
					break;
				}
				ptrIAppFramework->LOGINMngr_logoutCurrentUser();
					fireUserLoggedOut();

				LNGTreeDataCache dc;
				dc.clearMap();

				selectedClientName = "";

				bResult = kFalse;
				ptrIAppFramework->StructureCache_clearInstance();
				ptrIAppFramework->EventCache_clearInstance();
				ptrIAppFramework->EventCache_clearCurrentSectionData();
				ptrIAppFramework->clearConfigCache();

//					if(TmpldlgCloser)
//					TmpldlgCloser->closeTemplDialog();
//				if(PGSdlgCloser)
//					PGSdlgCloser->closePGSDialog();
				
//				if(WBdlgCloser)
//				WBdlgCloser->closeWBDialog();


//				if(iPageSprayer)
//				iPageSprayer->CloseSprayerDialog();

				
			}
//	}
	}while(kFalse);
}

void LNGActionComponent::fireUserLoggedOut(void)
{	
	vector<int32 > observers=getMediators()->getObservers();
	vector<int32 >::iterator it;
	
	for(it=observers.begin();it!=observers.end();it++)
	{	
		int32 i = *it;
		InterfacePtr<ILoginEvent> evt
			((ILoginEvent*) ::CreateObject(i,IID_ILOGINEVENT));
		if(!evt)
		{
			//CA("Invalid logEvtHndler in LNGActionComponent");
			continue;
		}		
		evt->userLoggedOut();
		
	}
}

void LNGActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	//CA("LNGActionComponent::UpdateActionStates");
	if (ac == nil)
	{
		//ASSERT(ac);
		return;
	}

	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
	{
		ActionID actionID = listToUpdate->GetNthAction(iter);
		if (actionID.Get() == kLNGDialogActionID) 
		{
			//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return;
			//CA("Checking inside Updateaction States");
			bool16 result=(bool16)ptrIAppFramework->getLoginStatus();
		
				listToUpdate->SetNthActionState(iter, kEnabledAction);
			
			
				if(result == kTrue)
				{	
					PMString logout(kLNGLogoutStringKey);
					listToUpdate->SetNthActionName(iter,logout);
					//listToUpdate->SetNthActionName(iter,"&Logout");

	
				}
				else
				{
					PMString login(kLNGLoginStringKey);
					listToUpdate->SetNthActionName(iter,login);
					//listToUpdate->SetNthActionName(iter,"&Login");
				}
		
		}
//		else if (actionID.Get() == kLNGResetActionID) 
//		{
//			//CA("LNGActionComponent::UpdateActionStates :: inside kLNGResetActionID");
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == nil)
//				return;
//			bool16 result=(bool16)ptrIAppFramework->getLoginStatus();
//		
//			if(result == kTrue)
//				listToUpdate->SetNthActionState(iter, kEnabledAction);
//
//		}
	}
}

void LNGActionComponent::CloseDialog(void)
{
	//CA("LNGActionComponent::CloseDialog");
	if(CurrFirstDlgPtr){
		if(CurrFirstDlgPtr->IsOpen())
			CurrFirstDlgPtr->Close();
	}
}

void LNGActionComponent::CloseLatestDialog(void)
{
	//CA("LNGActionComponent::CloseLatestDialog");
	if(CurrLatestDlgPtr){
		if(CurrLatestDlgPtr->IsOpen())
			CurrLatestDlgPtr->Close();
	}
}

bool16 LNGActionComponent::fireResetPlugins()
{
	LNGMediator* md  = getMediators();
	vector <int32> observers = md->getObservers();
	vector <int32>::iterator it;
	for(it = observers.begin(); it != observers.end(); it++)
	{
		int32 id = *it;
		InterfacePtr<ILoginEvent> iLoginEventPtr((ILoginEvent*)::CreateObject(id,IID_ILOGINEVENT));
		if(!iLoginEventPtr)
		{
			continue;
		}
		iLoginEventPtr->resetPlugIn();
	}
	return kTrue;
return kTrue;
}

//  Code generated by DollyXs code generator

