#include "VCPlugInHeaders.h"
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "LNGID.h"




class LNGActionComponent : public CActionComponent
{
	public:

		static PMString printSourceUpdatesURL ;  //**** For Updates

		LNGActionComponent(IPMUnknown* boss);
		~LNGActionComponent();
		void	DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		void	DoDialog(int);
//		void	UpdateActionStates(IActionStateList* );
		void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

		static	IDialog* getDlgPtr(void);
		void	fireUserLoggedOut(void);
		void CloseDialog(void);
		void CloseLatestDialog(void);

		bool16 fireResetPlugins();
	private:
		static	IDialog* curDlgPtr;
		/** Pops this plug-in's about box. */
		void	DoAbout();
		//shows Logout from PRINT Dialog
		void	showLogoutPrintDialog(void);	
};	
