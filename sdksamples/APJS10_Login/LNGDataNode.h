#ifndef __LNGDataNode_H__
#define __LNGDataNode_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class LNGDataNode
{
private:
	PMString clientIdAndNameStr;
	double clientId;
	double ParentId;
	int32 seqNumber;
	int32 childCount;
	PMString clientName;
	PMString displayName;
	PMString cmUrl;

		
public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	LNGDataNode():clientIdAndNameStr(""), clientId(0),  childCount(0), seqNumber(0),clientName(""),displayName(""),cmUrl(""){}
	/*
		One time access to all the private members
	*/
	void setAll(PMString idName, double clientId,double ParentId, int32 seqNumber, int32 childCount,PMString clientNAME, PMString displayName, PMString cmUrl)
	{
		this->clientIdAndNameStr=idName;
		this->clientId=clientId;
		this->ParentId = ParentId;	
		this->seqNumber=seqNumber;
		this->childCount=childCount;	
		this->clientName = clientNAME;
		this->displayName = displayName;
		this->cmUrl = cmUrl;
	}
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->clientIdAndNameStr; }
	/*
		@returns the id of the publication
	*/
	double getClientId(void) { return this->clientId; }
		
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	
	/*
		Sets the publication id for the publication
		@returns none
	*/
	double getParentId(void) { return this->ParentId; }

	void setParentId(double ParentId) { this->ParentId = ParentId; }
	void setClientId(double clientId) { this->clientId=clientId; }	
	
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setName(PMString& Name) { this->clientIdAndNameStr = Name; }
	
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	PMString getClientName(void) { return this->clientName; }
	void setClientName(PMString& Name) { this->clientName = Name; }

	PMString getDisplayName(void) { return this->displayName; }
	void setDisplayName(PMString& Name) { this->displayName = Name; }

	PMString getCmUrl(void) { return this->cmUrl; }
	void setCmUrl(PMString& Name) { this->cmUrl = Name; }
};

typedef vector<LNGDataNode> LNGDataNodeList;

#endif