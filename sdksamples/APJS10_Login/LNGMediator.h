#ifndef __LNGMediator_h__
#define __LNGMediator_h__

#include "VCPlugInHeaders.h"
#include "LNGID.h"
#include "ILoginEvent.h"
#include "vector"
#include "CAlert.h"

#define CA(z) CAlert::InformationAlert(z)

using namespace std;

class LNGMediator
{
	private:
		vector<int32> observers;
	public:
		vector<int32> getObservers(void)
		{
			return observers;
		}
		void addToObservers(int32 bossClsId)
		{
			observers.push_back(bossClsId);
		}
};


#endif