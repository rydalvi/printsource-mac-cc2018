//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/basicmenu/LNGMnuSuiteLayoutCSB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

//===========================
//  INCLUDES
//===========================

#include "VCPlugInHeaders.h"								// For MSVC

// Interface headers
#include "ILayoutTarget.h"
#include "ILoginMenuSuite.h"										// Superclass declaration
#include "IAppFramework.h"
#include "CAlert.h"
// Other headers
#include "CmdUtils.h"

// Project headers

//	CLASS DECLARATIONS

/** 
	Layout concrete selection boss (CSB) IBscMnuSuite implementation.  The suite implementations on the CSBs are 
	responsible for doing all of the model specific work.  The implementation of a CSB suite deals with the 
	model format of its owning boss. The only information that the suite should need is from sibling interfaces 
	on the CSB, primarily the target interface. This interface specifies which items to act upon. For the 
	LayoutCSB, the target interface is ILayoutTarget which wraps a UIDList. So usually you see the methods in
	this CSB will first get an InterfacePtr to a ILayoutTarget, then it gets the UIDList of the selected items
	from the pointer.
	
    Note that the client does not interact with the CSB (which has all the real implementation of the suite) 
    directly, the client interacts with the ASB only.  Also note that the Suite API shouldn't contain model 
    data that is specific to a selection format (layout uidLists, text model/range, etc) so that the client 
    code can be completely decoupled from the underlying CSB.
	 
	@ingroup basicmenu
	@author Lee Huang
*/
class LNGMnuSuiteLayoutCSB : public CPMUnknown<ILoginMenuSuite>
{
public:
	/**
		Constructor.
		@param iBoss interface ptr from boss object on which this interface is aggregated.
	*/
	LNGMnuSuiteLayoutCSB(IPMUnknown* iBoss);
	
	/** Destructor. */
	virtual					~LNGMnuSuiteLayoutCSB(void);

//	Suite Member functions
public:
	/**
		See IBscMnuSuite::CanApplyBscMnu
	*/
	virtual bool16			CanApplyLoginMenu(void);
	virtual bool16			CanApplyLoginMenuForDoc(void );
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(LNGMnuSuiteLayoutCSB, kLNGMnuSuiteLayoutCSBImpl)

/* Constructor
*/
LNGMnuSuiteLayoutCSB::LNGMnuSuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ILoginMenuSuite>(iBoss)
{
}

/* Destructor
*/
LNGMnuSuiteLayoutCSB::~LNGMnuSuiteLayoutCSB(void)
{
}

#pragma mark -
/* CanApplyBscMnu
*/
bool16 LNGMnuSuiteLayoutCSB::CanApplyLoginMenu(void)
{
	//InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	//const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));

	//// if the current selection has at least one drawable page item
	//// then we will return true so the client code can apply the data
	//if (selectedItems.Length() > 0)
	//{
	//	return (kTrue);
	//}
	//else
	//{
	//	return (kFalse);
	//}

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return (kFalse);
			}
	bool16 status = ptrIAppFramework->LOGINMngr_getLoginStatus();
	if (status)
	{
		return (kTrue);
	}
	else
	{
		return (kFalse);
	}
}

bool16 LNGMnuSuiteLayoutCSB::CanApplyLoginMenuForDoc(void)
{
	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));

	// if the current selection has at least one drawable page item
	// then we will return true so the client code can apply the data
	if (selectedItems.Length() > 0)
	{
		return (kTrue);
	}
	else
	{
		return (kFalse);
	}
}