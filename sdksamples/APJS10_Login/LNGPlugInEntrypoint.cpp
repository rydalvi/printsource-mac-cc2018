#include "VCPlugInHeaders.h"
#include "LNGPlugInEntrypoint.h"
#include "CAlert.h"
#include "LNGMediator.h"

#define CA(z) CAlert::InformationAlert(z)

//static LNGMediator gMedObj;
static LNGMediator* LNGMed;

bool16 LNGPlugInEntrypoint::Load(ISession* theSession)
{
	//CA("LNGPlugInEntrypoint::Load");
	if(LNGMed == NULL)
	{
		LNGMed = new LNGMediator();
		//LNGMediator md;
		//gMedObj=md;
	} 
	return kTrue;
}

LNGMediator* getMediators(void)
{
//	CA("Retrieveing Login Mediators");
	if(LNGMed == NULL)
	{
		LNGMed = new LNGMediator();		
	}
	return LNGMed;//&gMedObj;
}

bool16 LNGPlugInEntrypoint::Unload()
{
	//CA("LNGPlugInEntrypoint::Unload");
	//gMedObj = nil;
//	gSession = nil;
	delete LNGMed;
	return kTrue;
}

/* Global
*/
static LNGPlugInEntrypoint gPlugIn;
/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{	
	return &gPlugIn;
}
