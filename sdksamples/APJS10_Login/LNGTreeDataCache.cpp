#include "VCPlugInHeaders.h"
#include "LNGTreeDataCache.h"
#include "CAlert.h"
#include "vector"

#define CA(X) CAlert::InformationAlert(X);
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

map<int32, LNGDataNode>* LNGTreeDataCache::dataCache=NULL;

LNGTreeDataCache::LNGTreeDataCache()
{
	if(dataCache)
		return;
	dataCache=new map<int32, LNGDataNode>;
}

bool16 LNGTreeDataCache::getAllIdForLevel(int level, int32& numIds, vector<int32>& idList)
{
	int32 flag=0;
	LNGDataNode pNode;
	if(!dataCache)
	{
		dataCache=new map<int32, LNGDataNode>;
		return kFalse;
	}
	map<int32, LNGDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		/*if(pNode.getLevel()==level)
		{
			flag++;
			idList.push_back(pNode.getPubId());
		}*/
	}
	numIds=flag;
	return kTrue;
}

bool16 LNGTreeDataCache::isExist(int32 id, LNGDataNode& pNode)
{
	if(!dataCache)
	{
		//CA("!dataCache");
		dataCache=new map<int32, LNGDataNode>;
		return kFalse;
	}

	map<int32, LNGDataNode>::iterator mapIterator;

	//CAI(static_cast<int32>(dataCache->size()));

	mapIterator=dataCache->find(id);
	if(mapIterator==dataCache->end())
	{
		//CA("mapIterator==dataCache->end()");	
		return kFalse;
	}

	pNode=(*mapIterator).second;
	return kTrue;
}

bool16 LNGTreeDataCache::add(LNGDataNode& pNodeToAdd)
{
	if(!dataCache)
		dataCache=new map<int32, LNGDataNode>;

	map<int32, LNGDataNode>::iterator mapIterator;
	LNGDataNode firstNode, anyNode;
	int32 removalId=-1;

	if((dataCache->size()+1)<dataCache->max_size())//Cache has some space left
	{
		mapIterator=dataCache->find(pNodeToAdd.getClientId());
		if(mapIterator==dataCache->end())//Not found. Insert it!!
		{
			//CAlert::InformationAlert("before insert");
			dataCache->insert(map<int32, LNGDataNode>::value_type(pNodeToAdd.getClientId(), pNodeToAdd));
			return kTrue;
		}
		//Node exists...Increase the hit count
		firstNode=(*mapIterator).second;
	
		return kTrue;
	}

	CAlert::ErrorAlert("System is low on resources. Please close some applications and proceed.");

	//We do not have any space left...Remove the element which is MOST accessed

	mapIterator=dataCache->begin();
	firstNode=(*mapIterator).second;

	removalId=firstNode.getClientId();
	
	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		anyNode=(*mapIterator).second;

					
	}
	dataCache->erase(removalId);
	dataCache->insert(map<int32, LNGDataNode>::value_type(pNodeToAdd.getClientId(), pNodeToAdd));
	return kTrue;
}

bool16 LNGTreeDataCache::clearMap(void)
{
	if(!dataCache)
		return kFalse;
	dataCache->erase(dataCache->begin(), dataCache->end());
	delete dataCache;
	dataCache=NULL;
	return kTrue;
}

bool16 LNGTreeDataCache::isExist(int32 parentId, int32 sequence, LNGDataNode& pNode)
{
	int flag=0;
	if(!dataCache)
	{
		dataCache=new map<int32, LNGDataNode>;
		return kFalse;
	}
	map<int32, LNGDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		if(pNode.getParentId()==parentId)
		{
			if(pNode.getSequence()==sequence)
			{
				pNode=(*mapIterator).second;
				flag=1;
				break;
			}
		}
	}

	if(!flag)
		return kFalse;
	return kTrue;
}