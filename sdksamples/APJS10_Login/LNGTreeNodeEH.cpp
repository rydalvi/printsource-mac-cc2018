//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "LNGID.h"
//#include "TPLMediatorClass.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "ITextFrame.h"         //Removed from CS3 API
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITreeViewController.h"
#include "UIDNodeID.h"
//#include "TPLDataNode.h"
//#include "TPLTreeDataCache.h"
//#include "TPLCommonFunctions.h"
//#include "TPLManipulateInline.h" 
#include "ILayoutSelectionSuite.h"
#include "ITableModelList.h"
#include "ILayoutUIUtils.h"
#include "IXMLUtils.h"
#include "ITextStoryThreadDict.h"
//#include "ITblBscSuite.h"
#include <IFrameUtils.h>
#include "CAlert.h"
//#include "IMessageServer.h"
//---------CS3 Addition-----------------//
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "SDKLayoutHelper.h"
#include "IGraphicFrameData.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITriStateControlData.h"

#include "IDialogController.h"
#include "ILoginHelper.h"
#include "LoginHelper.h"

#include "IAppFramework.h"
#include "dataStore.h"
#include "LNGTreeDataCache.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "ITextControlData.h"
#include "ICounterControlData.h"
#include "LNGActionComponent.h"
#include "IClientOptions.h"
#include "LoginDialogControler.h"

#define FILENAME			PMString("TPLTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

PMString selectedClientName;
double selectedClientId =-1;
int32 selectedTreeRow ;
PMString selectedTreeRowEnvName;
extern VectorClientModelPtr vectorClientModelPtr;
extern PMString UserNameStr;
extern PMString PasswordStr;

extern int32 displayEnvList ;
//#include "PnlTrvFileNodeID.h"
//#include "PnlTrvUtils.h"

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	PnlTrvNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~PnlTrvNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( PnlTrvNodeEH, kLNGTrvNodeEHImpl)

	
PnlTrvNodeEH::PnlTrvNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 PnlTrvNodeEH::ButtonDblClk(IEvent* e) 
{
	//CA("PnlTrvNodeEH::ButtonDblClk");	

	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	
	IControlView* TreePanelCtrlView = nil;
	if(displayEnvList == 0)
	{
		IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
		if(panelControlData == nil)
		{
			CA("panelControlData == nil");
			return retval;
		}

		TreePanelCtrlView = panelControlData->FindWidget(kLNGTreeViewWidgetID);
		if (TreePanelCtrlView == nil)
		{
			CA("TreePanelCtrlView is nil");
			return retval;
		}
	}
	else if(displayEnvList == 1)
	{
		IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();
		if(panelControlData == nil)
		{
			CA("panelControlData == nil");
			return retval;
		}	

		TreePanelCtrlView = panelControlData->FindWidget(kLaunchpadTreeViewWidgetID);
		if (TreePanelCtrlView == nil)
		{
			CA("TreePanelCtrlView is nil");
			return retval;
		}
	}

	InterfacePtr<ITreeViewController> treeViewMgr(TreePanelCtrlView, UseDefaultIID());
	if(treeViewMgr==nil)
	{
		//CA("treeViewMgr is NULL");
		return kFalse;
	}

	NodeIDList selectedItem;
	treeViewMgr->GetSelectedItems(selectedItem);
	if(selectedItem.size()<=0)
	{
		//CA("selectedItem.size()<=0");
		return kFalse;
	}
	
	NodeID nid=selectedItem[0];
	TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);

	int32 uid= uidNodeIDTemp->GetUID().Get();
	UID TextFrameuid = uidNodeIDTemp->GetUID();
	LNGDataNode pNode;
	LNGTreeDataCache dc;

	dc.isExist(uid, pNode);
	selectedTreeRow = pNode.getSequence();
	selectedTreeRowEnvName = pNode.getDisplayName();
	/*PMString pfName = pNode.getName();
	CA("pfName = " + pfName);*/
	selectedClientName = "";
	selectedClientName.AppendNumber(PMReal(pNode.getClientId()));
	selectedClientId = pNode.getClientId();
	ptrIAppFramework->setClientID(selectedClientId);
	if(displayEnvList == 1)
		return retval;
	//vectorClientModelPtr = NULL;
	LoginHelper LngHelpObj(this);
	PMString errorMsg("");
	bool16 bResult=LngHelpObj.loginUser(UserNameStr,PasswordStr,selectedClientName,vectorClientModelPtr, errorMsg);

	selectedClientName = "";
	PMString logTxt("");
	if(bResult==TRUE)
	{
		PMString envName = EvnironmentData::getSelectedServerEnvName();
		
		//logTxt.Append ("    ");			
		logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
		logTxt.Append (" ");
		logTxt.Append ("User ");
		logTxt.Append (UserNameStr );
		logTxt .Append (" connected successfully to " + envName + " !");
		logTxt .Append ("  ");
		//logTxt.Append ("    ");
		ptrIAppFramework->LogDebug( "User " + UserNameStr + " connected successfully to " + envName + " !");
					
		logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
		logTxt.Append (" ");
		logTxt.Append ("Loading client caches ... ");
		logTxt.Append ("        ");
		
		//CA("Successfully connected to " + envName + " !");

		
		IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
		if (panelControlData == nil)
			return kFalse;

		IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
		if (iMultilineTextExpanderControlView == nil)
			return kFalse;

		InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
		if (iTextControlData == nil) 
			return kFalse;

		IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
		if (iScrollBarControlView == nil)
			return kFalse;

		InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
		if(counterData==nil) 	
			return  kFalse;


		int32 minValue = counterData->GetMinimum();	
		counterData->SetValue(minValue);
		logTxt.SetTranslatable(kFalse);
		iTextControlData->SetString(logTxt);
		int32 maxValue = counterData->GetMaximum();	
		counterData->SetValue(maxValue);
//13-march
		/*RangeProgressBar progressBar("PRINTsource Login", 0, 1000, kFalse, kFalse);
		progressBar.SetTaskText("Connecting to " + envName );
		for(int32 i=0; i<1000; i++)
		{	
			progressBar.SetTaskText("Connecting to " + envName + "...");
			progressBar.SetPosition(i);
		}
		progressBar.Abort();	*/
		if(bResult)
		{							
			//ptrIAppFramework->loadAllCaches();

			counterData->SetValue(maxValue);
			//Sleep(1000);
			
			logTxt.Clear();				
		}
		
		LNGActionComponent actionObj(this);
		actionObj.CloseDialog();
		
		EvnironmentData::setServerStatus(1);
		if(EvnironmentData::getServerStatus()==1)
		{
			//CA("EvnironmentData::getServerStatus()==1");	
			InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
			if(cop != nil && bResult)
			{
				//CAlert::InformationAlert("Got the Options Interface");
				PMString name("");
				PMString LocaleName("");
				//CAlert::InformationAlert("-- calling get dufault locale and publication name --");
				cop->setdoRefreshFlag(kTrue);
				double localeID = cop->getDefaultLocale(LocaleName);
				double pubID = cop->getDefPublication(name);
				PMString imagePath = cop->getImageDownloadPath();
				//CAlert::InformationAlert(" --publication name-- "+name);
				//CAlert::InformationAlert(" --locale name-- "+LocaleName);
			}
			else{
				if(cop == nil)
					CAlert::InformationAlert(" Options plugin not available !!");
			}
		}	

		LNG2DialogController lng(this);
		lng.fireUserLoggedIn();			
		
		//iMultilineTextExpanderControlView->Invalidate();
		//iScrollBarControlView->Invalidate();

		
		int32 isEditServer=0;
		if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
		{	
			LoginInfoValue serverProperties  = EvnironmentData::getServerProperties();
			//serverProperties.setSchema(UserNameStr);
			//serverProperties.setDbAppenderForPassword(PasswordStr);
			serverProperties.setPassword(PasswordStr);
			//serverProperties.setServerPort(1);
			//int32 clientID = -1;
			if(LngHelpObj.editServerInfo(serverProperties, selectedClientId) == TRUE){
				EvnironmentData::setSelectedServerProperties(serverProperties);
				//CAlert::InformationAlert("edit existing server");
			}
		}
		
		LoginInfoValue currentServerInfo;

		VectorLoginInfoPtr result1=NULL;
		result1=	LngHelpObj.getAvailableServers();
		if(result1== NULL){
			PMString asd(" Please Create New Environment and Server name/URL Properties. ");
			asd.SetTranslatable(kFalse);
			CAlert::InformationAlert(asd); //AvailableServers File not found...
			return kFalse;
		}
		if(result1->size()==0){
			//CAlert::InformationAlert("Available Servers File show zero entries.");
			return kFalse;
		}	

		VectorLoginInfoValue::iterator it;
		for(it = result1->begin(); it != result1->end(); it++)
		{
			currentServerInfo = *it;	
			
			if(envName != currentServerInfo.getEnvName())
			{
				//CA("inside for = " + currentServerInfo.getEnvName());
				//currentServerInfo.setServerPort(0);
				//int32 clientID = -1;
				//if(LngHelpObj.editServerInfo(currentServerInfo,clientID) == TRUE){
				//	//CAlert::InformationAlert("edit existing server");
				//}				
			}			
		}
		delete result1;						
	}
	if(bResult!=TRUE)
	{
		//SetTextControlData(kLoginUserNameTextEditWidgetID, "");
		//SetTextControlData(kLoginPasswordTextEditWidgetID, "");
		EvnironmentData::setServerStatus(-1);
//13-march
//			logTxt.Append ("  ");
		logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
		logTxt.Append (" ");
		PMString loginErrorString; // = ptrIAppFramework->getLoginErrorString ();
		if(loginErrorString.NumUTF16TextChars() == 0)
		{
			loginErrorString = "Some unknwon error occured. Please contact your System Administrator!" ;
		}
		logTxt .Append(loginErrorString);
		ptrIAppFramework->LogError(loginErrorString);
		logTxt .Append ("  ");
		//int32 minValue = counterData->GetMinimum();	
		//counterData->SetValue(minValue);
		//iTextControlData->SetString(logTxt);
		//int32 maxValue = counterData->GetMaximum();	
		//counterData->SetValue(maxValue);

 					
	}


	return kFalse;
} 

bool16 PnlTrvNodeEH::LButtonDn(IEvent* e)
{
	//CA("LButton Down");
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_ILNGTRVSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	

	IControlView* TreePanelCtrlView = nil;
    IControlView* iLaunchpadPropButtonWidgetControlView = nil;
    IControlView* iLaunchpadDeleteButtonWidgetControlView = nil;
    
	if(displayEnvList == 0)
	{
		IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
		if(panelControlData == nil)
		{
			CA("panelControlData == nil");
			return retval;
		}

		TreePanelCtrlView = panelControlData->FindWidget(kLNGTreeViewWidgetID);
		if (TreePanelCtrlView == nil)
		{
			CA("TreePanelCtrlView is nil");
			return retval;
		}
	}
	else if(displayEnvList == 1)
	{
		IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();
		if(panelControlData == nil)
		{
			CA("panelControlData == nil");
			return retval;
		}	

		TreePanelCtrlView = panelControlData->FindWidget(kLaunchpadTreeViewWidgetID);
		if (TreePanelCtrlView == nil)
		{
			CA("TreePanelCtrlView is nil");
			return retval;
		}
        
        iLaunchpadPropButtonWidgetControlView = panelControlData->FindWidget(kLaunchpadPropButtonWidgetID);
        iLaunchpadDeleteButtonWidgetControlView = panelControlData->FindWidget(kLaunchpadDeleteButtonWidgetID);

	}


	InterfacePtr<ITreeViewController> treeViewMgr(TreePanelCtrlView, UseDefaultIID());
	if(treeViewMgr==nil)
	{
		//CA("treeViewMgr is NULL");
		return kFalse;
	}

	NodeIDList selectedItem;
	treeViewMgr->GetSelectedItems(selectedItem);
	if(selectedItem.size()<=0)
	{
		//CA("Please select a client to continue.");
		return kFalse;
	}
    
	NodeID nid=selectedItem[0];
	TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);

	int32 uid= uidNodeIDTemp->GetUID().Get();
	UID TextFrameuid = uidNodeIDTemp->GetUID();
	LNGDataNode pNode;
	LNGTreeDataCache dc;

	//CAI(uid);
	if(dc.isExist(uid, pNode))
	{
		PMString pfName = pNode.getName();
	
		selectedClientName = "";
		selectedClientName.AppendNumber(pNode.getClientId()); //pNode.getClientName()
		selectedClientId = pNode.getClientId();
		selectedTreeRow = pNode.getSequence();
		selectedTreeRowEnvName = pNode.getDisplayName();

		ptrIAppFramework->setClientID(selectedClientId);
		//CA("selectedClientName = " + selectedClientName);	
	}
	
    // Enable and Disable Properties Button
    if (iLaunchpadPropButtonWidgetControlView != nil)
    {
        if (selectedTreeRow==-1 || selectedTreeRowEnvName.empty())
        {
            iLaunchpadPropButtonWidgetControlView->Disable();
            iLaunchpadDeleteButtonWidgetControlView->Disable();
        }
        else
        {
            iLaunchpadPropButtonWidgetControlView->Enable();
            iLaunchpadDeleteButtonWidgetControlView->Enable();
        }
    }
	
	return retval;

}
//	end, File:	PnlTrvNodeEH.cpp
