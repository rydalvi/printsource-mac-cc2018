#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "UIDNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "LNGID.h"
#include "LNGTreeModel.h"
#include "LNGDataNode.h"
//#include "LNGMediatorClass.h"
#include "CAlert.h"
#include "LNGTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("LNGTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 

extern int32 displayEnvList;

class LNGTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	LNGTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~LNGTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const UID& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(LNGTreeViewWidgetMgr, kLNGTreeViewWidgetMgrImpl)

LNGTreeViewWidgetMgr::LNGTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* LNGTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kLNGPluginID, 
							kViewRsrcType, 
							kLNGTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	//ASSERT(retval);
	return retval;
}

WidgetID LNGTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kLNGTreePanelNodeWidgetID;
}

bool16 LNGTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	//CA("ApplyNodeToWidget");
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
	
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		//ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

	/*	IControlView*   expanderWidget = panelControlData->FindWidget(kLNGTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}

		IControlView*   TextIconWidget = panelControlData->FindWidget(kLNGTreeCopyIconSuiteWidgetID);
		ASSERT(TextIconWidget);
		if(TextIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::TextIconWidget is nil");
			break;
		}

		IControlView*   TextParaIconWidget = panelControlData->FindWidget(kLNGTreeCopyParaIconSuiteWidgetID);
		ASSERT(TextParaIconWidget);
		if(TextParaIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::TextParaIconWidget is nil");		
			break;
		}

		IControlView*   ImageIconWidget = panelControlData->FindWidget(kLNGTreeImageIconSuiteWidgetID);
		ASSERT(ImageIconWidget);
		if(ImageIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::ImageIconWidget is nil");
			break;
		}

		IControlView*   ParaImageIconWidget = panelControlData->FindWidget(kLNGTreeParaImageIconSuiteWidgetID);
		ASSERT(ParaImageIconWidget);
		if(ParaImageIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::ParaImageIconWidget is nil");
			break;
		}

		IControlView*   PVBoxIconWidget = panelControlData->FindWidget(kLNGTreePVBoxIconSuiteWidgetID);
		ASSERT(PVBoxIconWidget);
		if(PVBoxIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::PVBoxIconWidget is nil");
			break;
		}

		IControlView*   TableIconWidget = panelControlData->FindWidget(kLNGTreeTableIconSuiteWidgetID);
		ASSERT(TableIconWidget);
		if(TableIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGSelectionObserver::ApplyNodeIDToWidget::TableIconWidget is nil");
			break;
		}
		IControlView*   ListIconWidget = panelControlData->FindWidget(kLNGTreeListIconSuiteWidgetID);
		if(ListIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGSelectionObserver::ApplyNodeIDToWidget::ListIconWidget is nil");
			break;
		}
	//following controlView added on 13/12/06 by Tushar 
		IControlView*   DollerIconWidget = panelControlData->FindWidget(kLNGTreeDollorIconSuiteWidgetID);
		ASSERT(DollerIconWidget);
		if(DollerIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::DollerIconWidget is nil");		
			break;
		}
	//added on 10/01/06 by Dattatray
		IControlView*   ProductGroupNameIconWidget = panelControlData->FindWidget(kLNGProductGroupNameIconSuiteWidgetID);
		ASSERT(ProductGroupNameIconWidget);
		if(ProductGroupNameIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::ProductGroupNameIconWidget is nil");		
			break;
		}
	*/	

		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		//ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		LNGTreeModel model;
		TreeNodePtr<UIDNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->GetUID().Get();

		LNGDataNode pNode;
		LNGTreeDataCache dc;

		dc.isExist(uid, pNode);

		TreeNodePtr<UIDNodeID>  uidNodeID(node);
		//ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		if(displayEnvList == 0)
		{
			int32 *RowNo= NULL;
			int32 LocalRowNO;
			RowNo = &LocalRowNO;

			//CA("pNode.getName()  = " + pNode.getName());
			PMString stringToDisplay( this->getNodeText(uidNodeID->GetUID(), RowNo));
			stringToDisplay.SetTranslatable( kFalse );

			IControlView* displayStringView = panelControlData->FindWidget( kLNGTreeNodeNameWidgetID );
			//ASSERT(displayStringView);
			if(displayStringView == nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
				break;
			}

			displayStringView->ShowView();

			InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
			//ASSERT(textControlData);
			if(textControlData== nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
				break;		
			}
		
			/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			break;
			PMString stringToInsert("");
			stringToInsert.Append(iConverter->handleAmpersandCase(stringToDisplay));
			*/
			stringToDisplay.SetTranslatable(kFalse);
            stringToDisplay.ParseForEmbeddedCharacters();
			textControlData->SetString(stringToDisplay);

			IControlView* LacPadEnvNameView = panelControlData->FindWidget( kLaunchpadListBoxEnvTextWidgetID );
			//ASSERT(LacPadEnvNameView);
			if(LacPadEnvNameView != nil) 
			{
				LacPadEnvNameView->HideView();
			}

			IControlView* LacCmUrlView = panelControlData->FindWidget( kLaunchpadListBoxSrvTextWidgetID );
			//ASSERT(LacCmUrlView);
			if(LacCmUrlView != nil) 
			{
				LacCmUrlView->HideView();
			}

			IControlView* LacIconView = panelControlData->FindWidget( kLaunchpadIconSuiteWidgetID );
			//ASSERT(LacIconView);
			if(LacIconView != nil) 
			{
				LacIconView->HideView();
			}
		
			this->indent( node, widget, displayStringView );
		}
		else if(displayEnvList == 1)
		{			

			IControlView* displayStringView = panelControlData->FindWidget( kLNGTreeNodeNameWidgetID );
			//ASSERT(displayStringView);
			if(displayStringView != nil) 
			{
				displayStringView->HideView();
			}

			IControlView* LacIconView = panelControlData->FindWidget( kLaunchpadIconSuiteWidgetID );
			//ASSERT(LacIconView);
			if(LacIconView != nil) 
			{
				LacIconView->ShowView();
			}

			IControlView* LacPadEnvNameView = panelControlData->FindWidget( kLaunchpadListBoxEnvTextWidgetID );
			//ASSERT(LacPadEnvNameView);
			if(LacPadEnvNameView != nil) 
			{
				LacPadEnvNameView->ShowView();

				InterfacePtr<ITextControlData>  textControlData( LacPadEnvNameView, UseDefaultIID() );
				//ASSERT(textControlData);
				if(textControlData!= nil)
				{
					PMString stringToDisplay(pNode.getDisplayName());
					stringToDisplay.SetTranslatable( kFalse );
                    stringToDisplay.ParseForEmbeddedCharacters();
					textControlData->SetString(stringToDisplay);	
				}
			}

			IControlView* LacCmUrlView = panelControlData->FindWidget( kLaunchpadListBoxSrvTextWidgetID );
			ASSERT(LacCmUrlView);
			if(LacCmUrlView != nil) 
			{
				LacCmUrlView->ShowView();

				InterfacePtr<ITextControlData>  textControlData( LacCmUrlView, UseDefaultIID() );
				//ASSERT(textControlData);
				if(textControlData!= nil)
				{
					PMString stringToDisplay(pNode.getCmUrl());
					stringToDisplay.SetTranslatable( kFalse );
                    stringToDisplay.ParseForEmbeddedCharacters();
					textControlData->SetString(stringToDisplay);	
				}
				this->indent( node, widget, LacCmUrlView );
			}
		}

	} while(kFalse);
	return kTrue;
}

PMReal LNGTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<UIDNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		LNGTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->GetUID());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString LNGTreeViewWidgetMgr::getNodeText(const UID& uid, int32 *RowNo) const
{	//CA("getNodeText");
	LNGTreeModel model;
	return model.ToString(uid, RowNo);
}

void LNGTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}
	
