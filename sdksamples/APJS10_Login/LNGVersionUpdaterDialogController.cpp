#include "VCPlugInHeaders.h"

//#include "LoginDialogController.h"
#include "CDialogController.h"
#include "SystemUtils.h"
//#include "IAppFramework.h"
#include "CAlert.h"
#include "LNGID.h"
#include "dataStore.h"
#include "LNGMediator.h"
//#include "LNGPlugInEntrypoint.h"
#include "ILoginEvent.h"
//8-march
#include "IPanelControlData.h"
#include "IControlView.h"
#include "listBoxHelperFuncs.h"
#include "LoginHelper.h"
#include "ICounterControlData.h"
#include "ITextControlData.h"
#include "dataStore.h"
#include "time.h"
#include "ProgressBar.h"
#include "LNGActionComponent.h"


//extern  PMString printSourceUpdatesURL;


class LNGVersionUpdaterDialogController: public CDialogController
{
	public:
		LNGVersionUpdaterDialogController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~LNGVersionUpdaterDialogController() {}
		void InitializeDialogFields(IActiveContext*); 

		WidgetID ValidateDialogFields(IActiveContext*);
		void ApplyDialogFields(IActiveContext*, const WidgetID&);	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNGVersionUpdaterDialogController, kLNGVersionUpdaterDialogControllerImpl)


void LNGVersionUpdaterDialogController::InitializeDialogFields(IActiveContext* myContext)
{
	/*CA("  LNGVersionUpdaterDialogController::InitializeDialogFields ");
	CA(LNGActionComponent::printSourceUpdatesURL);*/
	do{
		CDialogController::InitializeDialogFields(myContext);	

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("InitializeDialogFields panelControlData nil");
			return ;
		}
		IControlView* iControlView = panelControlData->FindWidget(kVersionUpdaterURLTextEditWidgetID);
		if (iControlView == nil)
			return;
		InterfacePtr<ITextControlData> urlTextWidget(iControlView,UseDefaultIID());
		if (urlTextWidget == nil) {
			CAlert::InformationAlert("CatalofNameTextControlData 1 nil");
			return;
		}	

		//CA(LNGActionComponent::printSourceUpdatesURL);
		urlTextWidget->SetString(LNGActionComponent::printSourceUpdatesURL);
		
	}while(kFalse);
}

WidgetID LNGVersionUpdaterDialogController::ValidateDialogFields(IActiveContext*)
{
	WidgetID newWidgetID;
	return newWidgetID;
}
void LNGVersionUpdaterDialogController::ApplyDialogFields(IActiveContext*, const WidgetID&)
{
	
}