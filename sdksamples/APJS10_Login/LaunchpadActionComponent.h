#include "VCPlugInHeaders.h"
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "LNGID.h"

class LNGActionComponent : public CActionComponent
{
	public:
		LNGActionComponent(IPMUnknown* boss);
		~LNGActionComponent();
		void	DoAction(ActionID actionID);
		void	DoDialog(int);
//		void	UpdateActionStates(IActionStateList* );
		void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

		static	IDialog* getDlgPtr(void);
		void	fireUserLoggedOut(void);
	private:
		static	IDialog* curDlgPtr;
		/** Pops this plug-in's about box. */
		void	DoAbout();
		//shows Logout from PRINT Dialog
		void	showLogoutPrintDialog(void);
};	
