/*
//	File:	LNGDialogController.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "ITextControlData.h"
#include "CAlert.h"
// Project includes:
#include "LNGID.h"
#include "dataStore.h"
#include "IDropDownListController.h"
#include "IAppFramework.h"
#include "ITreeViewMgr.h"
#include "LNGTreeModel.h"
#include "LNGTreeDataCache.h"

#define CA(X) CAlert::InformationAlert(X)

bool16 JDBCEnable = kFalse;
extern int32 displayEnvList;

/** LNGDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
class LNGDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNGDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~LNGDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext*); 

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateFields to be called. When all widgets are valid, 
			ApplyFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext*);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		

		virtual void changeStatusText(PMString& envName,bool8 isDefaultText=FALSE);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNGDialogController, kLNGLaunchpadDlgControllerImpl)

/* ApplyFields
*/
void LNGDialogController::InitializeDialogFields(IActiveContext* myContext)
{
	CDialogController::InitializeDialogFields(myContext);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return;
	}

	//JDBCEnable = ptrIAppFramework->isJDBCEnable();

	/*if(JDBCEnable == kTrue)
		CA("JDBCEnable == kTrue");
	else
		CA("JDBCEnable == kFalse");*/
	
	PMString str("Please select an environment to connect to.");
	changeStatusText(str,TRUE);
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeDialogFields panelControlData nil");
		return ;
	}

	//Cs4 Commented From Here
	/*IControlView* iControlView = panelControlData->FindWidget(kSelectModeWidgetID);
	if (iControlView == nil){
		CAlert::InformationAlert("Ap_46Login::LaunchpadDialogController::InitialiseDialogFields::iControlView == nil...127");
		return;
	}

	InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
	if (iDropDownListController == nil){
		CAlert::InformationAlert("iDropDownListController == nil");
		return;
	} *///cs4
 
	IControlView* iControlView1 = panelControlData->FindWidget(kLaunchpadSerStaticTextWidgetID);
	if (iControlView1 == nil){
		CAlert::InformationAlert("iControlView1 == nil");
		return;
	}

	InterfacePtr<ITextControlData> textControlData(iControlView1,UseDefaultIID());
	if (textControlData == nil){
		CAlert::InformationAlert("versionDropListData nil");	
		return;
	}

	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}*/
	/*if(EvnironmentData::SelctedConnMode== -1 || EvnironmentData::SelctedConnMode== 0)
	{*/	//CA("Set connection mode to 0 ie. HTTP");
		//EvnironmentData::SelctedConnMode=0;
		int32 selectedRowIndex=0;
//		iDropDownListController->Select(0);  //cs3
//		selectedRowIndex = iDropDownListController->GetSelected();
		PMString status("Catsy Hostname/URL");//("Server Name/URL");
		status.SetTranslatable(kFalse);
		textControlData->SetString(status);
		iControlView1->Invalidate();
		//ptrIAppFramework->setSelectedConnMode(0); // for Http
	/*}
	else if(EvnironmentData::SelctedConnMode== 1)
	{	*/
		//EvnironmentData::SelctedConnMode=1;
		//int32 selectedRowIndex=1; 
		//iDropDownListController->Select(1);
		//selectedRowIndex = iDropDownListController->GetSelected();
		//PMString status("Server Name/URL");
		//textControlData->SetString(status);
		//iControlView1->Invalidate();
		//ptrIAppFramework->setSelectedConnMode(1); // for Server
	//}

	IControlView* view = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
	if(view == nil){
		//CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}
	//listBoxHelper listHelper(this, kLNGPluginID);
	//listHelper.AddElement(view,EvnironmentData::getServerProperties());

	view->ShowView();
	view->Enable();

	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
    
    // Disable Properties/Delete Button
    IControlView* iLaunchpadPropButtonWidgetControlView = panelControlData->FindWidget(kLaunchpadPropButtonWidgetID);
    IControlView* iLaunchpadDeleteButtonWidgetControlView = panelControlData->FindWidget(kLaunchpadDeleteButtonWidgetID);
    
    if (iLaunchpadPropButtonWidgetControlView != nil)
    {
        iLaunchpadPropButtonWidgetControlView->Disable();
    }
    if (iLaunchpadDeleteButtonWidgetControlView != nil)
    {
        iLaunchpadDeleteButtonWidgetControlView->Disable();
    }

    
}

/* ValidateFields
*/
WidgetID LNGDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	//CAlert::InformationAlert("ValidateFields...");
	// Put code to validate widget values here.

	return result;
}

/* ApplyFields
*/
void LNGDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetid) 
{
	// Replace with code that gathers widget values and applies them.
	//CAlert::InformationAlert("ApplyFields...");
	//if(EvnironmentData::getSelectedserver()>=0)
    PMString asd = EvnironmentData::getSelectedServerEnvName();
	changeStatusText(asd);

}
//  Generated by Dolly build 17: template "Dialog".
// End, LNGDialogController.cpp.

void LNGDialogController::changeStatusText(PMString& endName,bool8 isDefaultText)
{
	PMString status("");
	if(isDefaultText==FALSE){ // connecting string
		status+="Connecting to"; 
		status+=endName;
		status+="...   ";
	}
	else status+=endName;

	do{
		/*InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(kLaunchpadStatusTextWidgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView nil");
			break;
		}	
		InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
		if (textControlData == nil){
			CAlert::InformationAlert("versionDropListData nil");	
			break;
		}
		textControlData->SetString(status);*/
	}
	while(0);
}
