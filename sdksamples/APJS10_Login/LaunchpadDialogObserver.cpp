/*
//	File:	LNGDialogObserver.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "ITextControlData.h"
#include "CAlert.h"
// Project includes:
#include "LNGID.h"
#include "LNGActionComponent.h"
//#include "listBoxHelperFuncs.h"
#include "dataStore.h"
#include "ILoginHelper.h"
#include "IDropDownListController.h"
#include "IAppFramework.h"
#include "ListBoxObserver.h"
#include "LoginHelper.h"

#include "IDialogController.h"
#include "IStringListControlData.h"
#include "ITreeViewMgr.h"
#include "LNGTreeModel.h"
#include "LNGTreeDataCache.h"
//#include "Mediator.h"
//12-march
bool16 isValidServerStatus;
extern int32 selectedTreeRow; 
extern PMString selectedTreeRowEnvName;
//12-march
#define CA(X) CAlert::InformationAlert(X)

IPanelControlData * LauchPadPanelControlData = nil;
 extern int32 displayEnvList ;

/** LNGDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class LNGDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNGDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) { bp=boss;}

		/** Destructor. */
		virtual ~LNGDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		void modifyLaunchpadListData(void);
	private:
		IPMUnknown* bp;
		void changeStatusText(PMString& endName,bool8 isDefText);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNGDialogObserver, kLNGLaunchpadDlgObserverImpl)

/* AutoAttach
*/
void LNGDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

		LauchPadPanelControlData = panelControlData;
		if (panelControlData == nil){
			ASSERT_FAIL("LNGDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}

		EvnironmentData::setlauncpadPanel(panelControlData);
		//AttachToWidget(kListBoxWidgetID,panelControlData);

		// Attach to other widgets you want to handle dynamically here.		
		//this is deleted by alok
		AttachToWidget(kLaunchpadPropButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kLaunchpadNewButtonWidgetID,   IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kLaunchpadDeleteButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kSelectModeWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kFirstLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kFirstCancelButton_WidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
	} while (false);
}

/* AutoDetach
*/
void LNGDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			ASSERT_FAIL("LNGDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		// Detach from other widgets you handle dynamically here.		
		//this is deleted by alok
		DetachFromWidget(kLaunchpadPropButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kLaunchpadNewButtonWidgetID,   IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kLaunchpadDeleteButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);		
		DetachFromWidget(kSelectModeWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);		
		DetachFromWidget(kFirstLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);	
		DetachFromWidget(kFirstCancelButton_WidgetID, IID_ITRISTATECONTROLDATA, panelControlData);	
		//DetachFromWidget(kLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
	} while (false);
}

/* Update
*/
void LNGDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	//CAlert::InformationAlert("LNGDialogObserver::Update");
	do
	{
		PMString envName;
		int selection=-1;		

		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("LNGDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();		
		// close the dialog
		
		//listBoxHelper listHelper(this, kLNGPluginID);
		
		if ((theSelectedWidget== kFirstLoginButtonWidgetID || theSelectedWidget== kOKButtonWidgetID) && theChange == kTrueStateMessage)
		{ 
			//CA("kOKButtonWidgetID...LNGDialogObserver::Update");
			//listHelper.getselectedEnvironmentName(&selection,envName);
		
			EvnironmentData::setSelectedserver(selectedTreeRow);
			//CA("Before setSelectedServerEnvName : " + envName );
			EvnironmentData::setSelectedServerEnvName(/*envName*/selectedTreeRowEnvName);
			
			IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
			
			IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
			if (iHostDropDownWidgetControlView == nil)
				break ;

			InterfacePtr<IStringListControlData> HostDropListData(iHostDropDownWidgetControlView,UseDefaultIID());
			if(HostDropListData == nil)
				break;	

			InterfacePtr<IDropDownListController> HostDropListController(HostDropListData,UseDefaultIID());
			if (HostDropListController == nil)		
				break;			
			
			HostDropListController->Select(EvnironmentData::getSelectedserver());

			//	 
			//if(selection==-1){
			//	CAlert::InformationAlert("Please Select Environment   ");
			//	break;
			//}		
			//changeStatusText(envName,FALSE);
 
			//LoginHelper lngHelperObj(this);
			///*bool16 bStatus */isValidServerStatus = lngHelperObj.isValidServer(envName);	//12-march		
			// 
			////if(bStatus != TRUE)
			////{march
			////	//CAlert::InformationAlert("Invalid Server... ");
			////	PMString str("Please select an environment to connect to.");
			////	changeStatusText(str,TRUE);
			////	break;
			////}

			//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			//if(ptrIAppFramework == nil){
			//	CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			//	return ;
			//}

			//if(EvnironmentData::SelctedConnMode == 1)
			//{	//CA("Server Connection Selected");
			//	ptrIAppFramework->setSelectedConnMode(1); // for Server	

			//	bool16 bStatus = lngHelperObj.setCurrentServer(envName);			
			//	if(bStatus  != TRUE){
			//		//CAlert::InformationAlert("Failed to set Current Server... ");
			//		PMString str("Please select an environment to connect to.");
			//		changeStatusText(str,TRUE);
			//		break;
			//	}  
			//}
			//if(EvnironmentData::SelctedConnMode == 0)
			//{
			//	ptrIAppFramework->setSelectedConnMode(0); // for Http	
			//	//CA("Http Connection Selected");
			//}
			////Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
			//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);// it will close the dialog
		
			//LNGActionComponent actionObj(bp);
			//actionObj.DoDialog(2);

			//if(theSelectedWidget== kFirstLoginButtonWidgetID){
				CDialogObserver::CloseDialog();
				break;
			//}
		}
		if((theSelectedWidget==kFirstCancelButton_WidgetID || theSelectedWidget== kCancelButton_WidgetID ) && theChange == kTrueStateMessage)
		{
			//CA("launchpaddialog cancel button");
			// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
			CDialogObserver::Update(theChange, theSubject, protocol, changedBy);// it will close the dialog
			if(theSelectedWidget==kFirstCancelButton_WidgetID){
				CDialogObserver::CloseDialog();
				break;
			}
		}
		else if (theSelectedWidget == kLaunchpadPropButtonWidgetID && theChange == kTrueStateMessage)
		{
			//listHelper.getselectedEnvironmentName(&selection,envName);
			EvnironmentData::setSelectedserver(/*selection*/selectedTreeRow);
			//CA("Before setSelectedServerEnvName : " + envName );
			EvnironmentData::setSelectedServerEnvName(/*envName*/selectedTreeRowEnvName);
			if(selectedTreeRow==-1 || selectedTreeRowEnvName.empty()){
				PMString asd("Please Select Environment   ");
				asd.SetTranslatable(kFalse);
				CAlert::InformationAlert(asd);
				break;
			}
			 
			EvnironmentData::setIsEditServerFlag(TRUE); // edit existing server
			LoginHelper lngHelperObj(this);
			LoginInfoValue currentServerInfo;
			if(lngHelperObj.getCurrentServerInfo(envName,currentServerInfo)==-1)
			{ // CAlert::InformationAlert("2.2.");
				break;
			}
			LNGActionComponent actionObj(bp);

			if(EvnironmentData::SelctedConnMode == 0)
				actionObj.DoDialog(5);
			else if(EvnironmentData::SelctedConnMode == 1)
				actionObj.DoDialog(5);

			//CAlert::InformationAlert("After closing edit dialog");
		} 
		else if (theSelectedWidget == kLaunchpadNewButtonWidgetID && theChange == kTrueStateMessage)
		{	
			//CAlert::InformationAlert("After closing new dialog");		
			EvnironmentData::setIsEditServerFlag(FALSE); // create new server
			LNGActionComponent actionObj(bp);
			
			EvnironmentData::SelctedConnMode = 0;
				actionObj.DoDialog(5);
			//	else if(EvnironmentData::SelctedConnMode == 1)
			//	{	
			//		actionObj.DoDialog(5);
			//	}
		}
		else if (theSelectedWidget == kLaunchpadDeleteButtonWidgetID && theChange == kTrueStateMessage)
		{
			//listHelper.getselectedEnvironmentName(&selection,envName);		
			EvnironmentData::setSelectedserver(/*selection*/selectedTreeRow);
			//CA("Before setSelectedServerEnvName : " + envName );
			EvnironmentData::setSelectedServerEnvName(/*envName*/selectedTreeRowEnvName);

			if(selectedTreeRow==-1){
				CAlert::InformationAlert("Please Select Environment, you want to delete...  ");
				break;
			}
			PMString message("Are you sure, you want to delete the environment: ");			
			message+=/*envName*/selectedTreeRowEnvName;
			message+="     ";
			message.SetTranslatable(kFalse);
			PMString ok_btn("OK"); //1
			ok_btn.SetTranslatable(kFalse);
			PMString cancel_btn("Cancel");//2		
			cancel_btn.SetTranslatable(kFalse);
			int isDeleteServer= CAlert::ModalAlert(message,ok_btn,cancel_btn,"",2,CAlert::eQuestionIcon);
			if(isDeleteServer==1){
				LoginHelper lngHelperObj(this);
				bool16 bStatus = lngHelperObj.deleteServer(selectedTreeRowEnvName);
				if(bStatus!=-1)
				{
					//listHelper.RemoveElementAt(selection);				
					
					IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
			
					IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
					if (iHostDropDownWidgetControlView == nil)
						return ;

					InterfacePtr<IStringListControlData> HostDropListData(iHostDropDownWidgetControlView,UseDefaultIID());
					if(HostDropListData == nil)
						return;
						
					HostDropListData->RemoveString(selectedTreeRow, kFalse, kFalse);

					IControlView * userNameView = panelControlData->FindWidget(kLoginUserNameTextEditWidgetID);
					if(userNameView == nil)
						return;

					InterfacePtr<ITextControlData> iTextControlData1(userNameView,UseDefaultIID());
					if(iTextControlData1 == nil)
						return ;
					
					PMString blankString("");
					blankString.SetTranslatable(kFalse);
					iTextControlData1->SetString(blankString);

					IControlView * passwordView = panelControlData->FindWidget(kLoginPasswordTextEditWidgetID);
					if(passwordView == nil)
						return ;
					
					InterfacePtr<ITextControlData> iTextControlData2(passwordView,UseDefaultIID());
					if(iTextControlData2 == nil)
						return ;
							
					iTextControlData2->SetString(blankString);

					modifyLaunchpadListData();
				}
			}
			else if(isDeleteServer==2){	
				//CAlert::InformationAlert("Cancel button");
			}
		}
		//else if (theSelectedWidget== kSelectModeWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		//{
		//	InterfacePtr<IDropDownListController> iDropDownListController(controlView, UseDefaultIID());
		//	if (iDropDownListController == nil)
		//		return;

		//	int32 selectedRowIndex=-1; 
		//	selectedRowIndex = iDropDownListController->GetSelected();
		//	
		//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		//	if(ptrIAppFramework == nil){
		//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		//		return ;
		//	}			
		//	
		//	PMString status("");
		//	if(selectedRowIndex == 0)
		//	{
		//		status.Append("Server URL");
		//		EvnironmentData::SelctedConnMode= 0;
		//		ptrIAppFramework->setSelectedConnMode(0); // for Http
		//	}
		//	else if(selectedRowIndex == 1)
		//	{
		//		status.Append("Server Name");
		//		EvnironmentData::SelctedConnMode=1;
		//		ptrIAppFramework->setSelectedConnMode(1); // for Server
		//	}
		//	InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		//	if (panelControlData == nil){
		//		CAlert::InformationAlert("panelControlData nil");
		//		break;
		//	}

		//	IControlView* iControlView1 = panelControlData->FindWidget(kLaunchpadSerStaticTextWidgetID);
		//	if (iControlView1 == nil){
		//		CAlert::InformationAlert("iControlView of status text nil");
		//		break;
		//	}	
		//	InterfacePtr<ITextControlData> textControlData(iControlView1,UseDefaultIID());
		//	if (textControlData == nil){
		//		return;
		//	}
		//	textControlData->SetString(status);
		//	iControlView1->Invalidate();
		//	
		//	LNGActionComponent actionObj(bp);
		//	actionObj.CloseDialog();
		//	actionObj.DoDialog(1);

		//}
		
	} while (false);
	//CAlert::InformationAlert("after While end");
}
//  Generated by Dolly build 17: template "Dialog".
// End, LNGDialogObserver.cpp.

void LNGDialogObserver::changeStatusText(PMString& endName,bool8 isDefaultText)
{	
	PMString status("");
	if(isDefaultText==FALSE){ // connecting string
		status+="Connecting to"; 
		status+=endName;
		status+="...   ";
	}
	else status+=endName;
	do{
		/*InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(kLaunchpadStatusTextWidgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView of status text nil");
			break;
		}	
		InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
		if (textControlData == nil){
			CAlert::InformationAlert("versionDropListData nil");	
			break;
		}
		textControlData->SetString(status);
		iControlView->Invalidate();*/
        //CAlert::InformationAlert(endName);
		IDialog* dlgPtr = LNGActionComponent::getDlgPtr();
		if(dlgPtr){			
			IControlView* view = dlgPtr->GetDialogPanel();
			view->Invalidate();
		}
		
	}
	while(0);
}

void LNGDialogObserver::modifyLaunchpadListData(void)
{	 
	
	IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();

	IControlView* view = panelControlData->FindWidget(kLaunchpadTreeViewWidgetID);
	if(view == nil){
		//CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}	

	view->ShowView();
	view->Enable();
			
	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
}