#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
//#include "IAppFramework.h"
#include "CAlert.h"
#include "LNGID.h"
#include "dataStore.h"
#include "LNGMediator.h"
#include "LNGPlugInEntrypoint.h"
#include "ILoginEvent.h"
//8-march
#include "IPanelControlData.h"
#include "IControlView.h"
#include "listBoxHelperFuncs.h"
#include "LoginHelper.h"
#include "ICounterControlData.h"
#include "ITextControlData.h"
#include "dataStore.h"
#include "time.h"
#include "ProgressBar.h"
//8-march
//12-march
extern bool16 isValidServerStatus;
//PMString logTxt;
//bool16 bResult;	
//12-march
class LNG2DialogController : public CDialogController
{
	public:
		LNG2DialogController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~LNG2DialogController() {}
		virtual void InitializeDialogFields(IActiveContext*); 

		virtual WidgetID ValidateDialogFields(IActiveContext*);
		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);

		void fireUserLoggedIn();
};