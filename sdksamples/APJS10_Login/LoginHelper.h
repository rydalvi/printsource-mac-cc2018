#ifndef __LOGINHELPERC_H__
#define __LOGINHELPERC_H__

#include "VCPlugInHeaders.h"
#include "IAppFramework.h"
#include "ILoginHelper.h"


class LoginHelper : public CPMUnknown<ILoginHelper>
{
public:
	LoginHelper(IPMUnknown* boss);
	~LoginHelper();
	/*virtual*/ VectorLoginInfoPtr getAvailableServers(void);
    /*virtual*/ //VectorVendorInfoPtr getVendors(void);
    /*virtual*/ bool16 getCurrentServerInfo(PMString&, LoginInfoValue&);
	/*virtual*/ bool16 addServerInfo(LoginInfoValue&);
	/*virtual*/ bool16 editServerInfo(LoginInfoValue& ,double clientID);
	/*virtual*/ bool16 deleteServer(PMString& envName);
	/*virtual*/ bool16 isValidServer(PMString &dbServerName);
	/*virtual*/ bool16 setCurrentServer(PMString &envName);
	/*virtual*/ bool16 loginUser(PMString &envName,PMString &password,PMString &clientName,VectorClientModelPtr vectorClientModelPtr, PMString & errorMsg);
	
	/*virtual*/ bool8 isAvailableServersFileExists(PMString &folderName);		
	/*virtual*/ bool8 isEnvironmentKeyWordExistsInFile(PMString &fPath);
	/*virtual*/ bool8 createAvailableServersFile(PMString &folderName);		
	/*virtual*/ bool8 getAppFolder(PMString &appFolder);
	/*virtual*/ void  appendFileName(PMString &folderName);
	/*virtual*/ bool16 getCurrentServerInfo(LoginInfoValue&);//--added on 17 feb ....
	void setEditedServerInfo(LoginInfoValue);

	void callfireResetPlugins();

	ClientInfoValue getCurrentClientInfoValue(void);
private:

	LoginInfoValue currentServerValue;
	//CVendorInfoValue vendors;
};

#endif 