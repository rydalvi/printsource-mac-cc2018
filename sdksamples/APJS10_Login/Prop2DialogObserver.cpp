/*
//	File:	LNG4DialogObserver.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "LNGActionComponent.h"

// Project includes:
#include "LNGID.h"
//#include "IAppFramework.h"
#include "dataStore.h"
#include "ILoginHelper.h"
#include "IWidgetParent.h"
//#include "listBoxHelperFuncs.h"

/** LNG4DialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class LNG4DialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG4DialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~LNG4DialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();	
		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		/**
			This routine is a template for attaching a widget to a subject to be observed.
			@param iPanelControlData panel that contains the widget which we want to attach to.
			@param widgetID	the widget id that we want to attach to.
			@param interfaceID protocol we are observering.
		*/
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		
		/**
			This routine is a template for detaching a widget from a subject being observed.
			@param iPanelControlData panel that contains the widget which we want to detach from.
			@param widgetID	the widget id that we want to detach from.
			@param interfaceID protocol we are observering.
		*/
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	private:
		//void addVersionsForSelectedVendor(IControlView*);
		void validatePropFeilds(PMString&);
		//void EnableAllTheControls(void);
		//void addVersions(PMString& );		
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG4DialogObserver, kLNGProp2DlgObserverImpl)

/* AutoAttach
*/

void LNG4DialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{			
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		AttachWidget(panelControlData, kOKButtonWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(panelControlData, kProp2ConnModeDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	} while (false);
	//CAlert::InformationAlert("Autoattatch ");
}

/* AutoDetach
*/
void LNG4DialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("AutoDetach panelControlData nil");
			break;
		}
		// Detach from other widgets you handle dynamically here.		
		//
		DetachWidget(panelControlData, kOKButtonWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(panelControlData, kProp2ConnModeDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	} while (false);
	//CAlert::InformationAlert("AutoDetach ");
}

/* Update
*/
void LNG4DialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject,
	const PMIID &protocol,
	void* changedBy
)
{
	//CAlert::InformationAlert("PropDialogObserver::Update");
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	 //listBoxHelper objlistbox(this, kLNGPluginID);

	do
	{ 
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil){
			CAlert::InformationAlert("controlView nil");
			break;
		}		
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if (theSelectedWidget== kProp2ConnModeDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			InterfacePtr<IDropDownListController> iDropDownListController(controlView, UseDefaultIID());
			if (iDropDownListController == nil)
				return;

			int32 selectedRowIndex=-1; 
			selectedRowIndex = iDropDownListController->GetSelected();
			
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return ;
			}		

			if(selectedRowIndex == 0)
			{				
				EvnironmentData::SelctedConnMode= 0;
				//ptrIAppFramework->setSelectedConnMode(0); // for Http
			}
			else if(selectedRowIndex == 1)
			{				
				EvnironmentData::SelctedConnMode=1;
				//ptrIAppFramework->setSelectedConnMode(1); // for Server

				LNGActionComponent actionObj(this);
				actionObj.CloseLatestDialog();
				actionObj.DoDialog(5);
			}
		}
	} while (false);
	//CAlert::InformationAlert("update");
}
//  Generated by Dolly build 17: template "Dialog".
// End, LNG4DialogObserver.cpp.

/*void LNG4DialogObserver::addVersionsForSelectedVendor(IControlView* vendorControlView)
{
	do{
		CAlert::InformationAlert("ha iam in add versionforselectedvendor after update");
		InterfacePtr<IStringListControlData> vendorDropListData(vendorControlView,UseDefaultIID());	
		if (vendorDropListData == nil)		{
			CAlert::InformationAlert("Vendor vendorDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
		if (vendorDropListController == nil){
			CAlert::InformationAlert("vendorDropListController nil");
			break;
		}
		int32 selectedRow = vendorDropListController->GetSelected();
		if(selectedRow <0){
			CAlert::InformationAlert("invalid row");
			break;
		}		
		PMString vendorName = vendorDropListData->GetString(selectedRow);		
		validatePropFeilds(vendorName);		
		addVersions(vendorName);
	}while(0);
} */

/*
void LNG4DialogObserver::EnableAllTheControls(void)
{	
	do{	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("EnableAllTheControls panelControlData nil");
			break;
		}
		
		IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" servername controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" port controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" username controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" password controlView nil");
			break;
		}
		controlView->Enable(kTrue);
	
		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
		if (controlView == nil){
			CAlert::InformationAlert(" schema controlView nil");
			break;
		}		
		controlView->Enable(kTrue);	
	}while(0);
}
*/
/*void LNG4DialogObserver::validatePropFeilds(PMString& vendorName)
{
	PMString oracle("Oracle"),sql("SQLServer"),access("Access");
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		IControlView* controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
			break;
		}
		controlView->Enable(kTrue);

		if(vendorName == oracle || vendorName == sql){
			//CAlert::InformationAlert("oracle or sql");
			controlView=nil;
			controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields servername controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				break;
			}
			controlView->Enable(kTrue);		

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields username controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields password controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			if(vendorName == oracle ){
				controlView=nil;			
				controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields schema controlView nil");
					break;
				}
				controlView->Disable(kTrue);
			}
		}
		if(vendorName == access || vendorName == sql){
			controlView=nil;
			//CAlert::InformationAlert("access or sql");
			controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields schema controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			if(vendorName == access ){
				controlView=nil;
				controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields servername controlView nil");
					break;
				}
				controlView->Disable(kTrue);

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields port controlView nil");
					break;
				}
				controlView->Disable(kTrue);		

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields username controlView nil");
					break;
				}
				controlView->Disable(kTrue);

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields password controlView nil");
					break;
				}
				controlView->Disable(kTrue);
			}
		}
	}while(0);
}*/


/*void LNG4DialogObserver::addVersions(PMString& selectedeVendorName)
{	
	//CAlert::InformationAlert("addVersions");
	LoginHelper LngHelpObj;
	vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
	VectorVendorInfoValue::iterator it;
	
	do{
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView nil");
			break;
		}

		InterfacePtr<IStringListControlData> versionDropListData(iControlView,UseDefaultIID());
		if (versionDropListData == nil){
			CAlert::InformationAlert("versionDropListData nil");	
			break;
		}
		versionDropListData->Clear(kFalse, kFalse);

		for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		{		
			int32 isVendorMatch = selectedeVendorName.Compare(TRUE,it->getVendorName());
			if(isVendorMatch==0)
			{
				vector<PMString> versions;
				vector<PMString>::iterator versionItrator;
				versions=it->getVendorVersion();
				int j=0;
				for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
				{
					if(versionDropListData)
						versionDropListData->AddString(*versionItrator, j, kFalse, kFalse);
					j++;				
				}	
			}
		}
	}while(0);
}*/

/*	::AttachWidget
*/
void LNG4DialogObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{	
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil){
			ASSERT_FAIL("widget has no ISubject... Ouch!");
			break;
		}
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); // only do once
}

/*	::DetachWidget
*/
void LNG4DialogObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
		{
			ASSERT_FAIL("iControlView invalid. Where the widget are you?");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
		{
			ASSERT_FAIL("PstLst Panel widget has no ISubject... Ouch!");
			break;
		}
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}
