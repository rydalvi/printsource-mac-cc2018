/*
//	File:	LNG3DialogController.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "IStringListControlData.h"

// Project includes:
#include "LNGID.h"

#include "dataStore.h"
#include "listBoxHelperFuncs.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "LoginHelper.h"
#include "ITextControlData.h"

#include "CAlert.h"
#include "dataStore.h"
#include "ILoginHelper.h"

//#define CA(X) CAlert::InformationAlert(X)
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("MyControlView.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}


 extern bool16 ISChangeInModewhileEdit1 ;
extern bool16 JDBCEnable;


/** LNG3DialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
class LNG3DialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG3DialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~LNG3DialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext*); 

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateFields to be called. When all widgets are valid, 
			ApplyFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext*);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);

		virtual void disableControls(void);
		virtual void EnableAllTheControls(void);
		virtual void DisableControlsDependOnServer(PMString& vendorName);
		virtual PMString getSelectedVendor(void);
		virtual PMString getSelectedVersion(void);
		virtual void  addVersions(PMString& versionName);
		void modifyLaunchpadListData(void);
		void addServerToLaunchpadList(void);

		void modifyLoginHostDropDownList(void);
		void addServerToLoginHostDropDownList(void);

};