#include "VCPlugInHeaders.h"

// Interface includes:
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "ICounterControlData.h"
#include "IPMStream.h"

// General includes:
#include "ShuksanID.h"
#include "PMString.h"
#include "K2Vector.h"
//#include "PalettePanelUtils.h"
#include "Trace.h"
#include "CmdUtils.h"

// Project includes:
#include "SaveLog.h"
#include "LNGID.h"
#include "CAlert.h"
#include <Utils.h>
#include "IPalettePanelUtils.h"

 //#include "Mediator.h"
#include "LoginDialogObserver.h"


extern IPanelControlData* iPanelCntrlDataPtr;
/*
	Factory methods. Single instance used.
*/
SaveLog* SaveLog::fSaveLog  = nil;

/*
*/
SaveLog* SaveLog::Instance()
{
	if (fSaveLog == nil)
	{
		fSaveLog = new SaveLog(kLoginDialogWidgetID,kLNGMultilineTextExpanderWidgetID,kScrollBarWidgetID);
	}
	return fSaveLog;
}

/*
*/
void SaveLog::DeleteInstance()
{
	if (fSaveLog != nil)
		delete fSaveLog;
	fSaveLog = nil;
}

/*
*/
SaveLog::SaveLog
(
 const WidgetID& LoginDialogWidgetID, 
 const WidgetID& HTIMultilineTextExpanderWidgetID,
 const WidgetID& scrollBarWidgetID
) 
: 
	fLogTextControlData(nil),
	fScrollBarCounterData(nil)
{
	do
	{
		// Find the panel.		
		////////////InterfacePtr<IPanelControlData> panelControlData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(LoginDialogWidgetID));
		////////////if (panelControlData == nil)
		////////////{
		////////////	// Commented out so that when the script provider tries to access an instance
		////////////	// of SaveLog when the panel is closed, this assert doesn't show. 
		////////////	// (sadahiro 04-mar-2005)
		////////////	CAlert::InformationAlert("panelControlData invalid");
		////////////	break;
		////////////}		
		{			
			// Cache an interface pointer to the log's text control data. 						
			IControlView* controlView = iPanelCntrlDataPtr->FindWidget(HTIMultilineTextExpanderWidgetID); 
			if (controlView == nil)
			{
			CAlert::InformationAlert("SaveLog::SaveLog::controlView invalid for HTIMultilineTextExpanderWidgetID ");
				break;
			}	
			
			InterfacePtr<ITextControlData> logTextControlData(controlView, UseDefaultIID());
			if (logTextControlData == nil)
			{
				CAlert::InformationAlert("invalid logTextControlData");
				break;
			}
			fLogTextControlData = logTextControlData;
		}

		{
			// Cache an interface pointer to the log's scroll bar.
			IControlView* controlView = iPanelCntrlDataPtr->FindWidget(scrollBarWidgetID);
			if (controlView == nil)
			{
				CAlert::InformationAlert("controlView invalid for scrollBarWidget");
				break;
			}
			InterfacePtr<ICounterControlData> counterData(controlView, UseDefaultIID());
			if(counterData==nil) {
				CAlert::InformationAlert("counterData nil");
				break;
			}
			fScrollBarCounterData = counterData;
		}

		// Restore the log from the widget state.
		fLog = fLogTextControlData->GetString();
		this->UpdateTextControl();

	} while (false);
}

/*
*/
SaveLog::~SaveLog()
{
}

/*
*/
void SaveLog::UpdateTextControl()
{
	do {
		if (fLogTextControlData == nil) {
			CAlert::InformationAlert("fLogTextControlData == nil");
			break;
		}
		// Seems that for this to work correctly with the control,
		// it's necessary to scroll the widget to the top before appending- otherwise
		// some odd behaviour is observed.
		this->ScrollToTop();
		fLog.SetTranslatable(kFalse);
        fLog.ParseForEmbeddedCharacters();
		fLogTextControlData->SetString(fLog);
		this->ScrollToBottom();

	} while(false);
}

/*
*/
void SaveLog::Clear()
{
	fLog.Clear();
	this->UpdateTextControl();
}

/*
*/
void SaveLog::Save(InterfacePtr<IPMStream> stream)
{
	const uchar kInternalNewLine='\n';
#ifdef MACINTOSH
		const char* kExternalNewLine="\r";
#else
		const char* kExternalNewLine="\r\n";
#endif
	for (int32 i = 0; i < fLog.CharCount(); i++)
	{
//9-march
		PlatformChar pc;
		if(fLog[i] == ' ' && fLog [i+1] == ' ')
		{
			pc = '\n';
		}
		else
		pc = fLog[i];
//9-march


		// if dual-byte char, output as is, highbyte first.
		if (pc.IsTwoByte()) 
		{
			uchar	hb = pc.HighByte(), 
					lb = pc.LowByte();
			stream->XferByte(&hb, 1);
			stream->XferByte(&lb, 1);
		}
		else // non-dualbyte char
		{
			uchar c = pc.GetAsOneByteChar();
			if (c == kInternalNewLine)
			{
#ifdef MACINTOSH
			stream->XferByte((uchar*)kExternalNewLine, 1);
#else
			stream->XferByte((uchar*)kExternalNewLine, 2);
#endif
			}
			else
			{
				stream->XferByte(&c, 1);
			}
		}
	}
}
/*
*/
void SaveLog::ScrollToTop()
{
	do {
		if(fScrollBarCounterData==nil) {
			CAlert::InformationAlert("fScrollBarCounterData nil");
			break;
		}
		int32 minValue = fScrollBarCounterData->GetMinimum();	
		fScrollBarCounterData->SetValue(minValue);
	} while(false);
}

/*
*/
void SaveLog::ScrollToBottom()
{
	do {
		if(fScrollBarCounterData==nil) {
			CAlert::InformationAlert("fScrollBarCounterData nil");
			break;
		}
		int32 maxValue = fScrollBarCounterData->GetMaximum();	
		fScrollBarCounterData->SetValue(maxValue);
	} while(false);
}


void SaveLog::updateLog()
{
	fLog = fLogTextControlData->GetString();
	this->UpdateTextControl();
}

// End, SaveLog.cpp.
