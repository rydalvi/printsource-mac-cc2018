#include "VCPlugInHeaders.h"
#include "PMString.h"
#include "dataStore.h"
#include "CAlert.h"

#define CA(x) CAlert::InformationAlert(x)

PMString EvnironmentData::envName("");
PMString EvnironmentData::availableServerFilePath("");
PMString EvnironmentData::availableServerFileName("AvailableServers.properties");
LoginInfoValue EvnironmentData::selectedServerProperties;
int EvnironmentData::SelctedConnMode=-1;
int EvnironmentData::selectedVendorRow=-1;
int EvnironmentData::selectedServer=-1;
bool16 EvnironmentData::serverStatus=-1;
bool8 EvnironmentData::isNewServer=FALSE;

IPanelControlData * EvnironmentData::launcpadPanel;
IPanelControlData * EvnironmentData::loginPanel; 
extern PMString selectedClientName;

LoginInfoValue&  EvnironmentData::getServerProperties(void)
{	
	 
	return EvnironmentData::selectedServerProperties;
}
void  EvnironmentData::setSelectedServerProperties(LoginInfoValue& value)
{
	//CA("Inside EvnironmentData::setSelectedServerProperties");
	//CA(value.getEnvName());
	selectedServerProperties=value;
	selectedServerProperties.setEnvName(value.getEnvName());
	selectedServerProperties.setClientList(value.getClientList());
	//selectedServerProperties.setDisplayPartnerImages(value.getDisplayPartnerImages());
	//selectedServerProperties.setDisplayPickListImages(value.getDisplayPickListImages());
	//selectedServerProperties.setSchema(value.getSchema());
	//selectedServerProperties.setdbAppenderForPassword(value.getdbAppenderForPassword());

	//if(EvnironmentData::SelctedConnMode == 0)
	//{
		selectedServerProperties.setCmurl(value.getCmurl());
	//}
	//else if(EvnironmentData::SelctedConnMode == 1){
	//	/*selectedServerProperties.setServerName(value.getServerName());
	//	selectedServerProperties.setServerPort(value.getServerPort());
	//	selectedServerProperties.setSchema(value.getSchema());
	//	selectedServerProperties.setUserName(value.getUserName());
	//	selectedServerProperties.setPassword(value.getPassword());*/
	//}
/////21-feb
	//	selectedServerProperties .setLogLevel(value.getLogLevel ());
	//	selectedServerProperties .setAssetStatus (value.getAssetStatus ());
	//	selectedServerProperties .setMissingFlag (value.getMissingFlag ());
////
}

bool16  EvnironmentData::getServerStatus(void)
{
	return serverStatus;
}
void EvnironmentData::setServerStatus(bool16 status)
{
	serverStatus=status;
}

bool8  EvnironmentData::getIsEditServerFlag(void)
{
	return isNewServer;
}
void EvnironmentData::setIsEditServerFlag(bool8 isNew)
{
	isNewServer=isNew;
}

int  EvnironmentData::getSelectedserver(void)
{
	return selectedServer;
}
void EvnironmentData::setSelectedserver(int i)
{
	selectedServer=i;
}

PMString EvnironmentData::getAvailableServerFileName(void)
{
	return availableServerFileName;
}


PMString EvnironmentData::getAvailableServerFilePath(void)
{
	return availableServerFilePath;
}

void EvnironmentData::setAvailableServerFilePath(PMString& str)
{
	availableServerFilePath.SetString(str);
}

PMString EvnironmentData::getSelectedServerEnvName()
{
	return envName;
}
void EvnironmentData::setSelectedServerEnvName(PMString& str)
{
	envName.SetString(str);
}

int EvnironmentData::getPrevSelectedVendor(void)
{
	return selectedVendorRow;
}

void EvnironmentData::setPrevSelectedVendor(int row)
{
	selectedVendorRow=row;
}

IPanelControlData* EvnironmentData::getlauncpadPanel(void)
{
	return launcpadPanel;
}
void EvnironmentData::setlauncpadPanel(IPanelControlData * paneldata)
{
	launcpadPanel=paneldata;
}

//---added on 17 feb -----
void EvnironmentData :: setProject(PMString project)
{
	ClientInfoValue clientInfoObj = selectedServerProperties.getClientInfoByClientNo(selectedClientName);
	clientInfoObj.setProject(project);
	selectedServerProperties.setClientInfoValue(clientInfoObj);
}

void EvnironmentData :: setLanguage(PMString lang)
{
	ClientInfoValue clientInfoObj = selectedServerProperties.getClientInfoByClientNo(selectedClientName);
	clientInfoObj.setLanguage(lang);
	selectedServerProperties.setClientInfoValue(clientInfoObj);
}

IPanelControlData* EvnironmentData::getLoginPanel(void)
{
	return loginPanel;
}
void EvnironmentData::setLoginPanel(IPanelControlData * paneldata)
{
	loginPanel=paneldata;
}
