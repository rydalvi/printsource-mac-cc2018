/*
//
//	File:	listBoxHelper.h
//
//
//	Date:	6-Mar-20010
//  
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__

//#include"Value\LoginInfoValue.h"

class IPMUnknown;
class IControlView;
class IPanelControlData;
class LoginInfoValue;
/**
	Encapsulates detail of working with the list-box API. 
	@author Ian Paterson
*/
class listBoxHelper
{
public:
	/**
		Constructor.
		@param fOwner reference to boss object using this helper.
		@param pluginId Identifier of plug-in that resources can be found in.
	*/
	listBoxHelper(IPMUnknown * fOwner, int32 pluginId);
	virtual ~listBoxHelper();

	/**
		Add an element with the specified name at a given location in the listbox.
		@param displayName string value
		@param updateWidgetId the text widget ID within the cell
		@param atIndex specify the location, by default, the end of the list-box.
	*/
	void AddElements(int atIndex = -2 /* kEnd */);

	void AddElement(IControlView * listBox,LoginInfoValue& currentServerInfo);

	/**
		Method to remove a list-box element by position.
		@param indexRemove position of element to remove.
	*/
	void RemoveElementAt(int indexRemove);
	
	/**
		Method to remove the last element in the list-box.
	*/
	void RemoveLastElement();

	/**
		Query for the list-box on the panel that is currently visible, assumes one visible at a time.
		@return reference to the current listbox, not add-ref'd.
	*/
	IControlView * FindCurrentListBox();

	/**
		Method to delete all the elements in the list-box.
	*/
	void EmptyCurrentListBox();

	/**
		Accessor for the number of elements in encapsulated list-box.
	*/
	int GetElementCount();

	/**
		get selected server Environment name		
	*/
	void SelectPrevSelectedElement(IControlView* listBox);
	void SelectLastElement(IControlView* listbox);
	void getselectedEnvironmentName(int* selectedRow,PMString& selectedEnvName);
	
	void modifyListElementData(IPanelControlData  *iPanelControlData);//***DBG

private:
	// impl methods
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	/**
		helper method to a new list element widget.
	*/
	void addListElementWidget(IControlView* listbox,InterfacePtr<IControlView> & elView, LoginInfoValue *serverValue,int atIndex);

	/**
		Helper method to remove the
	*/
	void removeCellWidget(IControlView * listBox, int removeIndex);
	IPMUnknown * fOwner;
	int32 fOwnerPluginID;
};


#endif // __SDKListBoxHelper_h__



