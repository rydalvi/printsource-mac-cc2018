#ifndef __APPFRAME_H__
#define __APPFRAME_H__


#include "vector"

#include "Value/ServerInfoValue.h"
#include "Value/UserInfoValue.h"
#include "Value/Attribute.h"
#include "Value/VendorInfoValue.h"
#include "Value/LocaleValue.h"
#include "Value/AssetValue.h"
#include "Value/ObjectValue.h"
#include "Value/ItemValue.h"
#include "Value/ProductValue.h"
#include "Value/CatalogValue.h"
#include "Value/TypeValue.h"
#include "Value/TableHeaderValue.h"
#include "Value/ProductTableScreenValue.h"*/

//#include "ServerInfoValue.h"


using namespace std;

typedef vector<CServerInfoValue> VectorServerInfoValue, *VectorServerInfoPtr;
typedef vector<CAttribute> VectorAttribute, *VectorAttributePtr;
typedef vector<CVendorInfoValue> VectorVendorInfoValue, *VectorVendorInfoPtr;
typedef vector<CLocale> VectorLocaleValue, *VectorLocaleValuePtr;

#ifdef AP_TEST_EXPORTS 
#define AP_TEST_API __declspec(dllexport) 
#else 
#define AP_TEST_API __declspec(dllimport) 
#endif 


/*extern "C" class TEST_API  vinit{
public: 	
		int fun(int);
};*/
extern "C" AP_TEST_API int Apptest(int x);

enum ApplicationEnum {Indesign, Quark, Arobat};

extern "C" class AP_TEST_API AppFramework
{
	
private:
	static bool IndesignLoginStatus;
	static bool QuarkLoginStatus;
	static bool ArobatLoginStatus;
	
	static CUserInfoValue IndesignUserInfo;
	static CUserInfoValue QuarkUserInfo;
	static CUserInfoValue ArobatUserInfo;

	static int SelectedConnMode;  // 0 for Http & 1 for Server 
	static long Catalog_ID;
	static long Locale_ID;

public:
	static char SelectedServerURL[1000];

	bool SetURL();
	bool SetLocalMode();

	void setCatalog_ID(long catalog_id);
	void setLocale_ID(long locale_id);
	long getCatalog_ID();
	long getLocale_ID();

	int UTILITYMngr_dispErrorMessage(void);

	void setSelectedConnMode(int mode);
	int getSelectedConnMode();

	bool LOGINMngr_getLoginStatus(ApplicationEnum);
	bool LOGINMngr_isValidServerURL(char*);
	bool LOGINMngr_isValidServer(char* serverName, int port);
	VectorServerInfoPtr LOGINMngr_getAvailableServers(char* fullPath, char* fileName);
	bool LOGINMngr_updateOrCreateNewServerProperties(CServerInfoValue oServerInfo, int updateflag, char* fullPath, char* fileName);
	bool LOGINMngr_deleteUser(char* envName, char* fullPath, char* fileName);
	bool LOGINMngr_loginUser(char* szUserName,char* szUserPassword, ApplicationEnum AppEnum);
	CUserInfoValue LOGINMngr_getUserProfile(ApplicationEnum);
	bool LOGINMngr_logoutCurrentUser(ApplicationEnum AppEnum);
	void LOGINMngr_setLoginStatus(bool, ApplicationEnum);
	VectorVendorInfoPtr LOGINMngr_getVendorVersions(void);
	bool LOGINMngr_setDBServer(CServerInfoValue serverValue, char* fullPath, char* fileName);
	int isDirectory(char *path);

	
	VectorAttributePtr getGenCopyAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getGenImageAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getSecCopyAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getSecImageAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getProductCopyAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getProductImageAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getItemCopyAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getItemImageAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getProdItemCopyAttributes(long Catalog_id, long Locale_id);
	VectorAttributePtr getProdItemImageAttributes(long Catalog_id, long Locale_id);

	VectorAttributePtr getAttributesVectorList(long Catalog_id, long Locale_id, char* MethodName);
	VectorTypeValuePtr TYPECache_getTypesForGroup(char* typeGroupCode, ApplicationEnum AppEnum);
	VectorTableHeaderValuePtr getTablePresetNamesForCatalog(long MasterCatalogID);

	VectorLocaleValuePtr getAllLocales(void);
	VectorCatalogValuePtr getAllCatalogsByLocaleID(long Locale_ID);
	VectorCatalogValuePtr getSections(long Catalog_ID,long Locale_ID=Locale_ID);
	VectorProductValuePtr getAllProductsForSection(long catalog_id);

	char* getCatalogAttributeValue(long CatalogID, long Attr_Loc_Id);
	char* getProductAttrbuteValue(long SectionID, long ProductID, long Attr_Loc_Id);
	char* getItemAttrbuteValue(long ItemID, long Attr_Loc_Id);
	VectorProductTableScreenValuePTR getProductScreenTableValues(long SectionID, long ProductID, long TableID); 

	char* SPCLCharMngr_getSpecialCharByCode(char* code);
	



	//VectorAttributePtr getItemParametrics(void);

};
#endif