/*
//	File:	OptnsDlgDialogCreator.cpp
//
//	Date:	07-Jun-2001
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "CPMUnknown.h"

// Project includes:
#include "IClientOptionsManip.h"
#include "IAppFramework.h"
#include "HelperInterface.h"
#include "OptionsValue.h"
#include "OptnsDlgID.h"
#include "PMString.h"
#include "CAlert.h"

#include "SDKUtilities.h"

#include <fstream>
#include <string>

class ClientOptionsManip : public CPMUnknown<IClientOptionsManip> 
{
	public:
			/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ClientOptionsManip(IPMUnknown*  boss);
		/** 
			Destructor.
		*/
		virtual ~ClientOptionsManip();

		virtual bool8 IsClientOptionsFileExiats(void);
		virtual bool8 CreateOptionsFile(PMString&);
		virtual PMString GetOptionsFilePath(void);		
		virtual OptionsValue& ReadClientOptions(void);
		virtual void WriteClientOptions(OptionsValue&);
		virtual PMString getStringByKey(PMString& ,PMString& );
		virtual PMString getCurrentPublications(void);
		virtual PMString getCurrentImagePath(void);
		virtual PMString getCurrentDocPath(void);
		virtual bool8 checkforPublicationExistance(PMString);

	private:
		//PMString curPub;
		//PMString curImageDir;
		//PMString curDocDir;
		OptionsValue OptionsObj;
		PMString OptionsFilepath;
};


CREATE_PMINTERFACE(ClientOptionsManip, kClientOptionsManipImpl)

/* ClientOptions Constructor
*/
ClientOptionsManip::ClientOptionsManip(IPMUnknown* boss): 
	CPMUnknown<IClientOptionsManip>(boss)	
{
}

/* FrmLblData Destructor
*/
ClientOptionsManip::~ClientOptionsManip()
{
	// Add code to delete extra private data, if any.
}

bool8 ClientOptionsManip::IsClientOptionsFileExiats(void)
{
	ErrorCode status=kFailure;
	PMString path = GetOptionsFilePath();
	status = SDKUtilities::FileExistsForRead(path);
	if(status==kFailure)		
		return FALSE;		
	return TRUE;
}

bool8 ClientOptionsManip::CreateOptionsFile(PMString& filePath)
{
	std::ofstream outFile;
	char* path=filePath.GrabCString();
	outFile.open(path,std::ios::out);		
	if(outFile.fail()){			
		return FALSE;
	}
	// write default data
	PMString str("defpublication");
	str+="=";	
	str+="\n";
	char* pub = str.GrabCString();
	outFile.write(pub,std::strlen(pub));

	str.Clear();
	str ="imagespath";
	str+="=";
	str+="\n";	
	char* ipath = str.GrabCString();
	outFile.write(ipath,std::strlen(ipath));

	str.Clear();
	str ="indesigndocpath";
	str+="=";
	str+="\n";
	char* dpath = str.GrabCString();
	outFile.write(dpath,std::strlen(dpath));
	outFile.close();
	return TRUE;
}
/*
bool8 ClientOptionsManip::CreateOptionsFile(PMString& filePath)
{
	std::ofstream outFile;
	char* path=filePath.GrabCString();
	outFile.open(path,std::ios::out);		
	if(outFile.fail()){			
		return FALSE;
	}
	// write default data
	PMString str("defpublication");
	str+="=";
	str+="American Express";	
	str+="\n";
	char* pub = str.GrabCString();
	outFile.write(pub,strlen(pub));

	str.Clear();
	str ="imagespath";
	str+="=";	
#ifdef MACINTOSH
	str+=":";
#else
	str+="C:\\";
#endif
	str+="\n";	
	char* ipath = str.GrabCString();
	outFile.write(ipath,strlen(ipath));

	str.Clear();
	str ="indesigndocpath";
	str+="=";	
#ifdef MACINTOSH
	str+=":";
#else
	str+="C:\\";
#endif
	str+="\n";
	char* dpath = str.GrabCString();
	outFile.write(dpath,strlen(dpath));

	outFile.close();
	return TRUE;
}
*/

// Getting application(indesign.exe) path later appending plugin path
PMString ClientOptionsManip::GetOptionsFilePath(void)
{	
	SDKUtilities::GetApplicationFolder(OptionsFilepath);
	
	PMString pluginFolderStr("");
	// appending '\'
#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif
	// appending folder name
	pluginFolderStr+="Plug-ins";

	// appending '\'
#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif
	// appending file name
	pluginFolderStr+="ClientOptions.properties";

	OptionsFilepath+=pluginFolderStr;
	return OptionsFilepath;
}

OptionsValue& ClientOptionsManip::ReadClientOptions(void)
{
	std::ifstream inFile;		
	PMString x("");
	OptionsObj.setPublication(x);
	OptionsObj.setImagePath(x);
	OptionsObj.setIndesignDocPath(x);

	PMString filePath = GetOptionsFilePath();
	char* path=filePath.GrabCString();
	inFile.open(path,std::ios::in);	
	if(inFile.fail()){
		CAlert::InformationAlert("Fail to oprn file");
		return OptionsObj;
	}	
	inFile.seekg(std::ios::beg);	
	char str[256];
	
	PMString pubName("defpublication");
	PMString imagePath("imagespath");
	PMString docPath("indesigndocpath");
		
	while(!inFile.eof()){
		std::strcpy(str,"");
		inFile.getline(str,255,'\n');
		PMString lineText(str);
		do
		{
			//CAlert::InformationAlert(lineText);			
			CharCounter flag = lineText.IndexOfString("defpublication");
			if(flag !=-1) // found pubName
			{
				//CAlert::InformationAlert("found the pub");
				PMString txt = getStringByKey(lineText,pubName);				
				OptionsObj.setPublication(txt);	
				break;
			}			
			
			flag = lineText.IndexOfString("imagespath");
			if( flag !=-1)// found ImagePath
			{
				//CAlert::InformationAlert("found the image path");
				PMString txt = getStringByKey(lineText,imagePath);				
				OptionsObj.setImagePath(txt);
	
				break;
			}
			
			flag = lineText.IndexOfString("indesigndocpath");
			if(flag !=-1)// found docPath
			{
				//CAlert::InformationAlert("found the doc path");
				PMString txt = getStringByKey(lineText,docPath);				
				OptionsObj.setIndesignDocPath(txt);
		
				break;
			}		
		}while(0);
	}
	inFile.close();
	return OptionsObj;
}

PMString ClientOptionsManip::getCurrentPublications(void)
{	
	OptionsValue curOptions = ReadClientOptions();	
	return curOptions.getPublication();
}


PMString ClientOptionsManip::getCurrentImagePath(void)
{	
	OptionsValue curOptions = ReadClientOptions();
	return curOptions.getImagePath();
}


PMString ClientOptionsManip::getCurrentDocPath(void)
{	
	OptionsValue curOptions = ReadClientOptions();
	return curOptions.getIndesignDocPath();
}

//std::ifstream* fileptr,
// return 1 for publicationname, 2 for Imagepath, 3 document path
PMString ClientOptionsManip::getStringByKey(PMString& line,PMString& searchString)
{
	//char* str = line.GrabCString();
	//int stringLength = strlen(str);
	int stringLength = line.Length();
	int searchStringLength = searchString.Length();

	if((searchStringLength+1)==stringLength)
	{
		///CAlert::InformationAlert("there is no def values for "+searchString);
		return PMString("");
	}
	CharCounter pos = line.IndexOfCharacter('=');
	if(pos == 0 || pos == -1){
		//CAlert::InformationAlert("getStringByKey position 0 or -1");
		return PMString("");
	}	

	if(pos == searchStringLength && pos > stringLength)
		return PMString("");

	PMString *tmp = line.Substring(pos+1);
	PMString x(*tmp);
	
	return x;
	
}

void ClientOptionsManip::WriteClientOptions(OptionsValue& optnsObj)
{
	OptionsObj = optnsObj;
	PMString filePath = GetOptionsFilePath();

	std::ofstream outFile;
	char* path=filePath.GrabCString();
	outFile.open(path,std::ios::out);		
	if(outFile.fail()){			
		return ;
	}
	// write default data
	PMString str("defpublication");
	str+="=";	
	str+=OptionsObj.getPublication();
	str+="\n";
	char* pub = str.GrabCString();
	outFile.write(pub,std::strlen(pub));

	str.Clear();
	str ="imagespath";
	str+="=";
	str+=OptionsObj.getImagePath();
	str+="\n";	
	char* ipath = str.GrabCString();
	outFile.write(ipath,std::strlen(ipath));

	str.Clear();
	str ="indesigndocpath";
	str+="=";
	str+=OptionsObj.getIndesignDocPath();
	str+="\n";
	char* dpath = str.GrabCString();
	outFile.write(dpath,std::strlen(dpath));
	outFile.close();	
}

bool8 ClientOptionsManip::checkforPublicationExistance(PMString pubName)
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Fail to CreateObject of Application FrameWork.");
			break;
		}
		
		VectorPubInfoPtr VectorPublInfoValuePtr = ptrIAppFramework->PUBMngr_findAllPublications();		

		if(VectorPublInfoValuePtr == nil){
			CAlert::InformationAlert("There is no Publications...");
			break;
		}

		VectorPublInfoValue::iterator it;
		for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
		{			
			PMString str("");
			str= it->getName();
			if(pubName == str)
				return TRUE;		
		}
	}while(0);

	return FALSE;
}
/*
bool8 ClientOptionsManip::checkforFolderExistance(PMString path)
{

}*/