#ifndef __COMMONFUNCTIONS_H__
#define __COMMONFUNCTIONS_H__

#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"

#define DRAGMODE	1

ITool* queryTool(const ClassID& toolClass);
int changeMode(int whichMode);
int embedBoxIntoBox(UIDRef toEmbed, UIDRef bigBox);
int getSelectionFromLayout(UIDRef& selectedBox);
void insertOrAppend(int selectedRowIndex, PMString theContent, int choice, int whichTab);
int getBoxDimensions(UIDRef theBox, PMRect& bind);
int checkForOverLaps(UIDRef draggedItem, int32 spreadNumber, UIDRef& overlappingItem, UIDList& theList);
int convertBoxToTextBox(UIDRef boxUIDRef);
int appendTextIntoBox(UIDRef boxUIDRef, PMString textToInsert, bool16 prompt=kFalse);
int applySlugToBox(UIDRef boxUIDRef, int selIndex, int selTab);
int16 refreshLstbox(UID);
void updateIcon(IControlView* lstcntrlview, int32 index, bool16 checked);

#endif __COMMONFUNCTIONS_H__