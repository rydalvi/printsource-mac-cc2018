
#ifndef __IClientOptionsManip__
#define __IClientOptionsManip__

#include "IPMUnknown.h"
#include "PMString.h"
#include "OptionsValue.h"
#include "OptnsDlgID.h"

class IClientOptionsManip : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_ICLIENTOPTIONSMANIP };
		
		virtual bool8 IsClientOptionsFileExiats(void)=0;
		virtual bool8 CreateOptionsFile(PMString&)=0;
		virtual PMString GetOptionsFilePath(void)=0;		
		virtual OptionsValue& ReadClientOptions(void)=0;	
		virtual void WriteClientOptions(OptionsValue&)=0;
		virtual PMString getStringByKey(PMString& ,PMString& )=0;
		virtual PMString getCurrentPublications(void)=0;		
		virtual PMString getCurrentImagePath(void)=0;
		virtual PMString getCurrentDocPath(void)=0;
		virtual bool8 checkforPublicationExistance(PMString)=0;
};

#endif