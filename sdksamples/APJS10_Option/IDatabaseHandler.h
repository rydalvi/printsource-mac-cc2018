#ifndef __IDatabaseHandler_h__
#define __IDatabaseHandler_h__

#include "IPMUnknown.h"
#include "VCPlugInHeaders.h"
#include "vector"
#include "OptnsSlug.h"

class IDatabaseHandler : public IPMUnknown
{
	virtual void setSlug(vector<OptnsSlug> v)=0;
	virtual vector<OptnsSlug> getSlug()=0;
};

#endif