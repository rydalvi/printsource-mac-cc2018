
#ifndef _IOptnsListSuite__h_
#define _IOptnsListSuite__h_

//	INCLUDES
#include "IPMUnknown.h"
#include "ShuksanID.h"
#include "PMRect.h"
#include "OptnsDlgID.h"

class UIDList;

class IPstLstSuite : public IPMUnknown
{
//	Data Types
public:
	enum { kDefaultIID = IID_IPSTLST_ISUITE };

	struct PstLstDataDescription
	{
		public:
		typedef	object_type	data_type;
		
		PMString	fUIDStr;
		PMString	fName;
	};
	typedef K2Vector<PstLstDataDescription> PstLstDataVector;

//	Member functions
public:

	/**
		@return true if the CreateNewPstLstData options be called, false otherwise.
	*/
	virtual bool16			CanCreateNewPstLstData(void) = 0;

	/**
		add a new IPstLstData to the IPstLstUIDList of all selected page items
		@param theData the IPstLstData (a PMString) to be set onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		CreateNewPstLstData(const PMString& theData) = 0;

	/**
		Retrive the data associated with one of the current selected items. 
		
		@param pstlstDataList list of the PstLstData of the current selected items. The PstLstData
		is a structure that contains the UID of the IPstLstDat and its data string.
	*/
	virtual void			GetPstLstDataDescription(PstLstDataVector& pstlstDataVector) = 0;

	/**
		@return true if the CanModifyPstLstData options be called, false otherwise.
	*/
	virtual bool16			CanModifyPstLstData(void) = 0;
	
	/**
		Modify the current selected IPstLstData of the IPstLstUIDLst on all selected page items with the input data (theData)
		@param theData the IPstLstData (a PMString) to be modified onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		ModifyPstLstData(const PMString& theData) = 0;

	/**
		@return true if the CanDeletePstLstData options be called, false otherwise.
	*/
	virtual bool16			CanDeletePstLstData(void) = 0;
	
	/**
		Delete the current selected IPstLstData from the IPstLstUIDLst of all selected page items.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		DeletePstLstData(void) = 0;

	/**
		Get the current selected UID's position index as appeared in the drop down list.  
		@param uidListIndex the current selected UID's position index as represented in the drop down list.
	*/
	virtual void			GetSelectedItemIndex(int32& uidListIndex) = 0;

	/** 
		Tell PstLstUIDList that a new UID has been selected from the drop down list
		@param uidListIndex the newly selected UID's position index to be modified onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode 		SetSelectedItemIndex(int32 uidListIndex) = 0;
};

#endif // _IPstLstSuite_