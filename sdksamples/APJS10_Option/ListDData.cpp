#include "VCPluginHeaders.h"
#include "string.h"

class ListData
{
	private:
		int id;
		int typeId;
		char* tableCol;
		char* name;
	public :
		void setId(int id) 
		{
			this->id = id;
		}
		void setTypeID(int typeId) 
		{
			this->typeId = typeId;
		}
		void setTableCol(char* tableCol)
		{
			strcpy(this->tableCol, tableCol);
		}
		void setName(char* name)
		{
			strcpy(this->name, name);
		}

		int getId()
		{
			return this->id;
		}
		int getTypeID()
		{
			return this->typeId;
		}
		char* getTableCol()
		{
			return this->tableCol;
		}
		char* getName()
		{
			return this->name;
		}
};