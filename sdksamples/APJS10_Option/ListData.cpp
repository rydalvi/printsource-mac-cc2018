#include "VCPluginHeaders.h"
#include "string.h"
#include "listData.h"
#include "CAlert.h"

#define CA(Z) CAlert::InformationAlert(Z)

vector<listInfo> listData::GenPanelListboxRows;
vector<listInfo> listData::AttribPanelListboxRows;
vector<listInfo> listData::TaskPanelListRows;
vector<listInfo> listData::ItemPanelListRows;
vector<listInfo> listData::ImageInfoVector;

typedef vector<listInfo> vect;

/* Inserting data in to Vector */
void listData::setData
(int16 listboxType, int16 index, int32 id, int32 typeId, PMString tablecol, PMString name, bool16 state, PMString code, bool16 flag) 
{
	listInfo listinfo;
	listinfo.id = id;
	listinfo.index = index;
	listinfo.typeId = typeId;
	listinfo.isSelected = state;
	listinfo.name = name;
	listinfo.tableCol =  tablecol;
	listinfo.isDragged = kFalse;
	listinfo.code = code;
	listinfo.isImageFlag=flag;
	switch(listboxType)
	{
		case 1:
			GenPanelListboxRows.push_back(listinfo);
			break;
		case 2:
			AttribPanelListboxRows.push_back(listinfo);
			break;
		case 3:
			TaskPanelListRows.push_back(listinfo);
			break;
		case 4:
			ItemPanelListRows.push_back(listinfo);
			break;
	}
}

listInfo listData::getData(int16 listboxType, int16 index)
{
	listInfo listinfo;
	switch(listboxType)
	{
		case 1:
			listinfo = this->GenPanelListboxRows[index];
			break;
		case 2:
			listinfo = this->AttribPanelListboxRows[index];
			break;
		case 3:
			listinfo = this->TaskPanelListRows[index];
			break;
		case 4:
			listinfo = this->ItemPanelListRows[index];
			break;

	}
	return listinfo;
}

int32 listData::returnListVectorSize(int16 listboxType)
{
	switch(listboxType)
	{
		case 1:
			return GenPanelListboxRows.size();
			break;
		case 2:
			return AttribPanelListboxRows.size();
			break;
		case 3:
			return TaskPanelListRows.size();
			break;
		case 4:
			return ItemPanelListRows.size();
			break;

	}
	return 0;
}

void listData::ClearVector(int16 listboxType)
{
	switch(listboxType)
	{
		case 1:
			GenPanelListboxRows.erase
				(GenPanelListboxRows.begin(), GenPanelListboxRows.end());
			break;
		case 2:
			AttribPanelListboxRows.erase
				(AttribPanelListboxRows.begin(), AttribPanelListboxRows.end());
			break;
		case 3:
			TaskPanelListRows.erase
				(TaskPanelListRows.begin(), TaskPanelListRows.end());
			break;
		case 4:
			ItemPanelListRows.erase
				(ItemPanelListRows.begin(), ItemPanelListRows.end());
			break;
	}
}

void listData::setUID(int16 listboxType, int16 index, UID boxuid)
{
	listInfo listinfo;
	vect::iterator theIterator;

	switch(listboxType)
	{
		case 1:
			for (theIterator = GenPanelListboxRows.begin(); theIterator != GenPanelListboxRows.end(); theIterator++)
			{
				if((*theIterator).index == index)
				{
					(*theIterator).boxUID = boxuid;
					(*theIterator).isDragged = kTrue;
				}
			}
			break;
		case 2:
			for (theIterator = AttribPanelListboxRows.begin(); theIterator != AttribPanelListboxRows.end(); theIterator++)
			{
				if((*theIterator).index == index)
				{
					(*theIterator).boxUID = boxuid;
					(*theIterator).isDragged = kTrue;
				}
			}
			break;
		case 3:
			for (theIterator = TaskPanelListRows.begin(); theIterator != TaskPanelListRows.end(); theIterator++)
			{
				if((*theIterator).index == index)
				{
					(*theIterator).boxUID = boxuid;
					(*theIterator).isDragged = kTrue;
				}
			}
			break;
		case 4:
			for (theIterator = ItemPanelListRows.begin(); theIterator != ItemPanelListRows.end(); theIterator++)
			{
				if((*theIterator).index == index)
				{
					(*theIterator).boxUID = boxuid;
					(*theIterator).isDragged = kTrue;
				}
			}
			break;
	}
}

bool16 listData::checkIfBoxIsDragged(int16 listboxType, int16 index)
{
	listInfo listinfo;
	vect::iterator myIterator;

	switch(listboxType)
	{
		case 1:
			for (myIterator = GenPanelListboxRows.begin(); myIterator != GenPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					return (*myIterator).isDragged;
				}
			}
			break;
		case 2:
			for (myIterator = AttribPanelListboxRows.begin(); myIterator != AttribPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					return (*myIterator).isDragged;
				}
			}
			break;
		case 3:
			for (myIterator = TaskPanelListRows.begin(); myIterator != TaskPanelListRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					return (*myIterator).isDragged;
				}
			}
			break;
		case 4:
			for (myIterator = ItemPanelListRows.begin(); myIterator != ItemPanelListRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					return (*myIterator).isDragged;
				}
			}
			break;
	}
	return kFalse;
}

void listData::resetDraggedBox(int16 listboxType, int16 index, bool16 flag)
{
	listInfo listinfo;
	vect::iterator myIterator;

	switch(listboxType)
	{
		case 1:
			for (myIterator = GenPanelListboxRows.begin(); myIterator != GenPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 2:
			for (myIterator = AttribPanelListboxRows.begin(); myIterator != AttribPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 3:
			for (myIterator = TaskPanelListRows.begin(); myIterator != TaskPanelListRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 4:
			for (myIterator = ItemPanelListRows.begin(); myIterator != ItemPanelListRows.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
	}
}


int16 listData::getRowForIndex
(int16 listboxType, UID boxuid)
{
	listInfo listinfo;
	vect::iterator myIterator;

	switch(listboxType)
	{
		case 1:
			for (myIterator = GenPanelListboxRows.begin(); myIterator != GenPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					return (*myIterator).index;
				}
			}
			break;
		case 2:
			for (myIterator = AttribPanelListboxRows.begin(); myIterator != AttribPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					return (*myIterator).index;
				}
			}
			break;
		case 3:
			for (myIterator = TaskPanelListRows.begin(); myIterator != TaskPanelListRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					return (*myIterator).index;
				}
			}
			break;
		case 4:
			for (myIterator = ItemPanelListRows.begin(); myIterator != ItemPanelListRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					return (*myIterator).index;
				}
			}
			break;
	}
	return -1;
}

UID listData::getUID(int16 listboxType, int16 index)
{
	listInfo listinfo;
	switch(listboxType)
	{
		case 1:
			listinfo = this->GenPanelListboxRows[index];
			break;
		case 2:
			listinfo = this->AttribPanelListboxRows[index];
			break;
		case 3:
			listinfo = this->TaskPanelListRows[index];
			break;
		case 4:
			listinfo = this->ItemPanelListRows[index];
			break;
	}
	return listinfo.boxUID;
}

void listData::markBoxAsDragged(int16 listboxType, UID boxuid)
{
	listInfo listinfo;
	vect::iterator myIterator;

	switch(listboxType)
	{
		case 1:
			for (myIterator = GenPanelListboxRows.begin(); myIterator != GenPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					(*myIterator).isDragged=kTrue;
				}
			}
			break;
		case 2:
			for (myIterator = AttribPanelListboxRows.begin(); myIterator != AttribPanelListboxRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					(*myIterator).isDragged=kTrue;
				}
			}
			break;
		case 3:
			for (myIterator = TaskPanelListRows.begin(); myIterator != TaskPanelListRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					(*myIterator).isDragged=kTrue;
				}
			}
			break;
		case 4:
			for (myIterator = ItemPanelListRows.begin(); myIterator != ItemPanelListRows.end(); myIterator++)
			{
				if((*myIterator).boxUID == boxuid)
				{
					(*myIterator).isDragged=kTrue;
				}
			}
			break;
	}
}

/* input - UID of box */
/* output - listbox type and current row index */
int16 listData::getLstTypeAndCurRowIndex
(UID curItemUID, int16* lstboxType,  int16* curRowIndex)
{
	listInfo listinfo;
	vect::iterator myIterator;

	for (myIterator = GenPanelListboxRows.begin(); myIterator != GenPanelListboxRows.end(); myIterator++)
	{
		if((*myIterator).boxUID == curItemUID)
		{
			*lstboxType = 1;
			*curRowIndex = (*myIterator).index;
			return 1;
		}
	}
	for (myIterator=AttribPanelListboxRows.begin(); myIterator != AttribPanelListboxRows.end(); myIterator++)
	{
		if((*myIterator).boxUID == curItemUID)
		{
			*lstboxType = 2;
			*curRowIndex = (*myIterator).index;
			return 1;
		}
	}
	for (myIterator=TaskPanelListRows.begin(); myIterator != TaskPanelListRows.end(); myIterator++)
	{
		if((*myIterator).boxUID == curItemUID)
		{
			*lstboxType = 3;
			*curRowIndex = (*myIterator).index;
			return 1;
		}
	}
	for (myIterator=ItemPanelListRows.begin(); myIterator != ItemPanelListRows.end(); myIterator++)
	{
		if((*myIterator).boxUID == curItemUID)
		{
			*lstboxType = 4;
			*curRowIndex = (*myIterator).index;
			return 1;
		}
	}
	return -1;
}