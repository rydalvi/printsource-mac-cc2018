#ifndef __LISTDATA_H__
#define __LISTDATA_H__
#include "VCPlugInHeaders.h"
#include "vector"

using namespace std;

class listInfo
{
	public:
		int16 index;
		int32 id;
		int32 typeId;
		PMString tableCol;
		PMString name;
		bool16 isSelected;
		UID boxUID;
		bool16 isDragged;
		PMString code;
		bool16 isImageFlag; // kFalse:Text box kTrue:Image box
};

class listData
{
	private :
		static vector<listInfo> GenPanelListboxRows;
		static vector<listInfo> AttribPanelListboxRows;
		static vector<listInfo> TaskPanelListRows;
		static vector<listInfo> ItemPanelListRows;
		static vector<listInfo> ImageInfoVector;
		
	public:
		void setData
		(int16 listboxType, int16 index, int32 id, int32 typeId, PMString tablecol, PMString name, bool16 state, PMString, bool16);
		listInfo getData(int16 listboxType, int16 index);
		int32 returnListVectorSize(int16 listboxType);
		void ClearVector(int16 listboxType);
		void setUID(int16 listboxType, int16 index, UID boxuid);
		UID  getUID(int16 listboxType, int16 index);
		int16 getRowForIndex(int16 listboxType, UID boxuid);
		bool16 checkIfBoxIsDragged(int16 listboxType, int16 index);
		void resetDraggedBox(int16 listboxType, int16 index, bool16 flag);
		void markBoxAsDragged(int16 listboxType, UID boxuid);	
		int16 getLstTypeAndCurRowIndex(UID curItemUID, int16* lstboxType,  int16* curRowIndex);
};
#endif