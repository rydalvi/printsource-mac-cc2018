#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "OptnsDlgID.h"
#include "SystemUtils.h"
#include "SDKUtilities.h"
#include "SDKListBoxHelper.h"
#include "IAppFramework.h"
#include "IPanelControlData.h"
#include "ISelection.h"
#include "IApplication.h"
#include "k2smartptr.h"
#include "LayoutUtils.h"
#include "listData.h"
#include "CmdUtils.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "ITextModel.h"
#include "FrameUtils.h"
#include "ISpecifier.h"
#include "IFrameList.h"
#include "ITextFrame.h"
#include "IHierarchy.h"
#include "ILayoutTarget.h"
#include "ITransform.h"
#include "ITextFocusManager.h"
#include "IDocument.h"
#include "ISpreadList.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "ISpread.h"
#include "IScrapItem.h"
#include "OptnsDragEventFinder.h"

#define CA(x)	CAlert::InformationAlert(x)
#define DRAGMODE	1

class OptnsAttribpanelLstBoxObserver : public CObserver
{
public:
	OptnsAttribpanelLstBoxObserver(IPMUnknown *boss);
	~OptnsAttribpanelLstBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	void insertTextIntoBox(int, PMString);
	int changeMode(int whichMode);
	ITool* queryTool(const ClassID& toolClass);	

private:
	void updateListBox(bool16 isAttaching);
};

CREATE_PMINTERFACE(OptnsAttribpanelLstBoxObserver, kOptnsAttribpanelLstBoxObserverImpl)

OptnsAttribpanelLstBoxObserver::OptnsAttribpanelLstBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

OptnsAttribpanelLstBoxObserver::~OptnsAttribpanelLstBoxObserver()
{
}

void OptnsAttribpanelLstBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void OptnsAttribpanelLstBoxObserver::AutoDetach()
{
	updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this);
	}
}

void OptnsAttribpanelLstBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	int selectedRowIndex=-1;
	PMString name("");

	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				CA("panel is nil");
				break;
			}

			SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
			IControlView * listBoxCntrlView = listHelper.FindCurrentListBox(panel, 2);
			if(listBoxCntrlView == nil) 
			{
				CA("listbox controlview is nil");
				break;
			}

			InterfacePtr<IListBoxController> listCntl(listBoxCntrlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(listCntl == nil) 
			{
				CA("listboxcntrller is nil");
				break;
			}

			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;

			const int kSelectionLength =  multipleSelection.Length() ;
			PMString currentSelectionRowString("");

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				selectedRowIndex=multipleSelection[0];
				
				listData attribPanelListData;
				int32 vectSize = attribPanelListData.returnListVectorSize(2);

				/* For getting data of the selected row of listbox */
				for(int y=0; y<vectSize; y++)
				{
					if(y==selectedRowIndex)
					{
						listInfo list = attribPanelListData.getData(2,y);
						currentSelectionRowString = list.name;
						OptnsDragEventFinder::isImgflag=list.isImageFlag;
						break;
					}
				}
			}

			bool flag=kFalse;
			UID currentSelection=-1;
			do
			{
				InterfacePtr<ISelection> selection(::QuerySelection());
				if (selection == nil)
				{
					CA("Selection nil");
					break;
				}
				
				scoped_ptr<UIDList> selectedUIDList( selection->CreateUIDList() );
				if (selectedUIDList == nil)
				{
//					CA("SelectedUID list nil");
					break;
				}

				const int32 listLength=selectedUIDList->Length();
				
				if(listLength==0)
				{
					//CA("listlen is zero");
					break;
				}
				currentSelection=selectedUIDList->GetRef(0).GetUID();
				flag=kTrue;
			}while(kFalse);

		//	if(flag)
			{
				OptnsDragEventFinder::listboxType = 2;
				OptnsDragEventFinder::selectedRowIndex=selectedRowIndex;
				OptnsDragEventFinder::currentSelectionRowString=currentSelectionRowString;
				OptnsDragEventFinder::cntrlView= listBoxCntrlView;
			}
		} while(0);
	}
}

void OptnsAttribpanelLstBoxObserver::updateListBox(bool16 isAttaching)
{
}

