//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
#include "CSelectableDialogObserver.h"
#include "SDKUtilities.h"
#include "CAlert.h"

// Project includes:
#include "OptnsDlgID.h"
#include "OptionsUtils.h"
#include "OptionsStaticData.h"
#include "OptionsValue.h"

/** OptnsDlgDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 


// Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup apjs9_option
*/
class OptnsDlgDialogObserver : public CSelectableDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		OptnsDlgDialogObserver(IPMUnknown* boss) : CSelectableDialogObserver(boss) {}//CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~OptnsDlgDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(OptnsDlgDialogObserver, kOptnsDlgDialogObserverImpl)

/* AutoAttach
*/
void OptnsDlgDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("OptnsDlgDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to SelectableDialog's info button widget.
		//AttachToWidget(kGeneralPanelWidgetID, IID_IK2SERVICEPROVIDER, panelControlData);
		//AttachToWidget(kTasksPanelWidgetID, IID_IK2SERVICEPROVIDER, panelControlData);
		//AttachToWidget(kApplyButtonWidgetID ,IID_IBOOLEANCONTROLDATA ,panelControlData);
		//AttachToWidget(kOptnsOKButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);

		// Attach to other widgets you want to handle dynamically here.

	} while (kFalse);
}

/* AutoDetach
*/
void OptnsDlgDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur 
	// (selectable dialog listbox, OK and Cancel buttons, etc.).
	CSelectableDialogObserver::AutoDetach();

	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("OptnsDlgDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		// Detach from other widgets you handle dynamically here.
		//DetachFromWidget(kGeneralPanelWidgetID, IID_IK2SERVICEPROVIDER, panelControlData);		
		//DetachFromWidget(kTasksPanelWidgetID, IID_IK2SERVICEPROVIDER, panelControlData);
		//DetachFromWidget(kApplyButtonWidgetID ,IID_IBOOLEANCONTROLDATA ,panelControlData);
		//DetachFromWidget(kOptnsOKButtonWidgetID , IID_ITRISTATECONTROLDATA ,panelControlData);

	} while (kFalse);
}

/* Update
*/
void OptnsDlgDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call base class Update function so that default behavior will still 
	// occur (selectable dialog listbox, OK and Cancel buttons, etc.).
	CSelectableDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	//CAlert::InformationAlert("options dilaog update");
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//CAlert::InformationAlert("controlView nil");
			break;
		}

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			CAlert::InformationAlert("Update panelControlData nil");
			return ;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this

		//if(theSelectedWidget == kApplyButtonWidgetID && theChange == kTrueStateMessage)
		//{
				/*IControlView* pubView =	panelControlData->FindWidget(kPublicationStaticWidgetID);
				if(pubView)*/
			//CAlert::InformationAlert("Inside Apply Button ----------");
		//}

		//if(theSelectedWidget == kOptnsOKButtonWidgetID && theChange == kTrueStateMessage)
		//{
		//	CAlert::InformationAlert("theSelectedWidget == kOptnsOKButtonWidgetID");
		//	//CAlert::InformationAlert("Inside OK Button-------");
		//}

	} while (false);
}

//  Code generated by DollyXs code generator
