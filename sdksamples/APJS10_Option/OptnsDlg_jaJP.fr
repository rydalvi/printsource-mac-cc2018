//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
			kOptnsDlgCompanyKey,					kOptnsDlgCompanyValue,					
			kOptnsDlgAboutMenuKey,					kOptnsDlgPluginName "[JP]...",
			kOptnsDlgMainMenuKey,					"Main[JP]",
			kOptnsDlgOptionsMenuKey,				kOptnsDlgPluginName,

		// ----- Command strings

               // kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

               
                // ----- Window strings

                // ----- Panel/dialog strings
		kOptnsDlgDialogTitleKey,			kOptnsDlgPluginName,//"Options",
		kGeneralPanelTitleKey,				"General",
		kAttribsPanelTitleKey,				"Attributes",
		kTasksPanelTitleKey,				"Tasks",

               // ----- Error strings

			
                // ----- Misc strings
                kOptnsDlgAboutBoxStringKey,			kOptnsDlgPluginName " [JP], version " kOptnsDlgVersion " by " kOptnsDlgAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
                
                kSCTImageRepositryStringKey, "Select Image Repository Directory",
                kSCTMapRepositryStringKey, "Map To Repository",

        }

};

#endif // __ODFRC__
