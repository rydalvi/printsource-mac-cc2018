#include "VCPlugInHeaders.h"
#include "OptnsDragEventFinder.h"
#include "IControlView.h"

PMString OptnsDragEventFinder::currentSelectionRowString("");
int OptnsDragEventFinder::selectedRowIndex=-1;
IControlView* OptnsDragEventFinder::cntrlView=nil;
int OptnsDragEventFinder::listboxType=1;
int OptnsDragEventFinder::whichRadBtnSelected=1;
int OptnsDragEventFinder::currentTab=1;
bool16 OptnsDragEventFinder::isImgflag=kFalse;
IControlView* OptnsDragEventFinder::lst1CntrlView=nil;
IControlView* OptnsDragEventFinder::lst2CntrlView=nil;
IControlView* OptnsDragEventFinder::lst3CntrlView=nil;
IControlView* OptnsDragEventFinder::lst4CntrlView=nil;