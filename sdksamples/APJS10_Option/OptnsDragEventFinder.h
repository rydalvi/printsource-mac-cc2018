#ifndef __OPTNSDRAGEVENTFINDER_H__
#define __OPTNSDRAGEVENTFINDER_H__

#include "PMString.h"
#include "IControlView.h"

/* 
This is the mediator class among the two classes viz. 
OptnsGenPanelCustFlavHlpr.cpp and OptnsGenpanelLstBoxObserver.cpp
*/
class OptnsDragEventFinder
{
	public:
		static UIDRef currentSelectedItemUIDRef;
		static int selectedRowIndex;
		static PMString currentSelectionRowString;
		//static bool16 didUserDragBox;
		static IControlView* cntrlView;
		static int listboxType;
		static int whichRadBtnSelected; // 1: Append 2:Embed
		static int currentTab; //1:General 2:Attribute 3:Task 
		static bool16 isImgflag;
		static IControlView* lst1CntrlView;
		static IControlView* lst2CntrlView;
		static IControlView* lst3CntrlView;
		static IControlView* lst4CntrlView;
};

#endif