#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsGenPanelController : public CDialogController
{
	public:
		OptnsGenPanelController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~OptnsGenPanelController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
};

CREATE_PMINTERFACE(OptnsGenPanelController, kGenPanelControllerImpl)

void OptnsGenPanelController::InitializeFields() 
{
	CDialogController::InitializeFields();
	this->SetTriStateControlData(kAppendRadBtnWidgetID, kTrue);
	this->SetTriStateControlData(kEmbedRadBtnWidgetID, kFalse);
}

WidgetID OptnsGenPanelController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void OptnsGenPanelController::ApplyFields(const WidgetID& widgetId) 
{
}
