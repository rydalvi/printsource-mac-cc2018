#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsItemPanelController : public CDialogController
{
	public:
		OptnsItemPanelController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~OptnsItemPanelController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
};

CREATE_PMINTERFACE(OptnsItemPanelController, kItemPanelControllerImpl)

void OptnsItemPanelController::InitializeFields() 
{
	CDialogController::InitializeFields();
	this->SetTriStateControlData(kAppendRadBtnWidgetID, kTrue);
	this->SetTriStateControlData(kEmbedRadBtnWidgetID, kFalse);
}

WidgetID OptnsItemPanelController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void OptnsItemPanelController::ApplyFields(const WidgetID& widgetId) 
{
}
