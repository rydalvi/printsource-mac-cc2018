#include "VCPlugInHeaders.h"
#include "CPanelCreator.h"
#include "CAlert.h"
#include "OptnsDlgID.h"

class ItemPanelCreator : public CPanelCreator
{
	public:
		ItemPanelCreator(IPMUnknown *boss) : CPanelCreator(boss) {}
		virtual ~ItemPanelCreator() {}
		virtual RsrcID GetPanelRsrcID() const;
};

CREATE_PMINTERFACE(ItemPanelCreator, kItemPanelCreatorImpl)

RsrcID ItemPanelCreator::GetPanelRsrcID() const
{
	return kItemPanelCreatorResourceID;
}
