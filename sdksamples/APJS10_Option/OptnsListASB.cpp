
// INCLUDES
#include "VCPlugInHeaders.h"								// For MSVC

// Interface headers
#include "ICommandSequence.h"
#include "ISelectionManager.h"
#include "IOptnsListSuite.h"							// Superclass declaration

// ID headers

// Other headers
#include "CmdUtils.h"
#include "HelperInterface.h"
#include "SelectionASBTemplates.tpp"
#include "SelectionExtTemplates.tpp"

//	CLASS FORWARDS
	class ClassInfo;

//	CLASS DECLARATIONS
/** 
	Abstract selection boss (ASB) \Ref{IPstLstSuite} implementation.  The purpose of this integrator suite is
	to determine how to forward the client request on to the CSB suite(s).  Note that the client does not
	interact with the CSB (which has all the real implementation of the suite) directly, the client interacts 
	with the ASB only.  Also note that the Suite API shouldn't contain model data that is specific to a 
	selection format (layout uidLists, text model/range, etc) so that the client code can be completely 
	decoupled from the underlying CSB.

	This suite is an advanced suite which uses selection extension.  The selection extension is a bridge 
	between a suite implementation with an unknown interface (IPstLstData) and the selection architecture. The
	main reason this extension is needed is so we can be notified of current selection's model change (thru
	SelectionAttributeChanged).
*/
class PstLstASB : public CPMUnknown<IPstLstSuite>
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	PstLstASB(IPMUnknown* iBoss);
	
	/** Destructor. */
	virtual		~PstLstASB(void);

//	Member functions
public:
	/**
		@return kTrue if any of the active CSBs allow adding new IPstLstData, kFalse otherwise.
	*/
	virtual bool16			CanCreateNewPstLstData(void);

	/**
		Create a sequence and call each CSB to process it's commands (kPstLstNewDataCmdBoss).
		@param theData the IPstLstData (a PMString) to be set onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		CreateNewPstLstData(const PMString& theData);
	
	/**
		@return kTrue if any of the active CSBs allow modifying current IPstLstData, kFalse otherwise.
	*/
	virtual bool16			CanModifyPstLstData(void);
	
	/**
		Create a sequence and call each CSB to process it's commands (kPstLstModifyDataCmdBoss).
		@param theData the IPstLstData (a PMString) to be modified onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		ModifyPstLstData(const PMString& theData);
	
	/**
		@return kTrue if any of the active CSBs allow deleting current selected IPstLstData of the current selections, kFalse otherwise.
	*/
	virtual bool16			CanDeletePstLstData(void);
	
	/**
		Create a sequence and call each CSB to process it's commands (kPstLstDeleteDataCmdBoss).
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		DeletePstLstData(void);

	/**
		Call each enabled CSB's GetPstLstDataDescription()
		@param pstlstDataList list of the PstLstData of the current selected items. The PstLstData
		is a structure that contains the UID of the IPstLstDat and its data string.
	*/
	virtual void		 	GetPstLstDataDescription(PstLstDataVector& pstlstDataVector);

	/**
		Call each enabled CSB's GetSelectedItemIndex()
		@param uidListIndex the current selected UID's position index as represented in the drop down list.
	*/
	virtual void			GetSelectedItemIndex(int32& uidListIndex);
	
	/**
		Create a sequence and call each CSB to process it's commands (kPstLstSelectDataCmdBoss).
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode 		SetSelectedItemIndex(int32 uidListIndex);

	//--------------------------------------------------------------------------------
	//	Selection Extention functions 
	//--------------------------------------------------------------------------------
	/**
		This function is called when the selection sub-system starts up. Any suite initialization
		code can be placed in this funciton. Called for suites on the ASB and CSBs.
	*/
	virtual void				Startup			 					(void) {}

	/**
		This function is called when the selection sub-system shuts down. Any suite termination 
		code can be placed in this funciton. Called for suites on the ASB and CSBs.
	*/
	virtual void				Shutdown			 				(void) {}

	/**
		This function is called when an item is added to or removed from the selection.  Suites on the 
		changing CSB and the ASB are called.	
	*/
	virtual void				SelectionChanged 					(void*, SuiteBroadcastData*) {}

	/**
		This function is only used on the Layout CSB.
	*/
	virtual void				SelectionAttributeChanged 			(SuiteBroadcastData*, void*) {}

	/**
		This is only supported on the ASB and is rarely used. If a suite on a CSB needs to communicate with it's
		integrator it sends a message via the IConcreteCSB's BroadcastToIntegratorSuite(). This in turn is received 
		and forwarded onto the integrator suite.	
	*/
	virtual void				HandleIntegratorSuiteMessage		(void*, const ClassID&, ISubject*, const PMIID&, void*) {}

	/**
		This function is only used on the Layout CSB.
	*/
	virtual ProtocolCollection*	CreateObserverProtocolCollection 	(void) {return nil;}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(PstLstASB, kPstLstASBImpl)

/*
	Instatiate the selection extension template and create an InDesign interface.
	The parameter to the template is the suite implementation class.
*/
template class SelectionExt<PstLstASB>;
CREATE_PMINTERFACE(SelectionExt<PstLstASB>, kPstLstSuiteASBSelectionExtImpl)

/*	PstLstASB Constructor
*/
PstLstASB::PstLstASB(IPMUnknown* iBoss) :
	CPMUnknown<IPstLstSuite>(iBoss)
{
}

/*	PstLstASB Destructor
*/
PstLstASB::~PstLstASB(void)
{
}

#pragma mark-
/*	PstLstASB::CanCreateNewPstLstData
*/
bool16 PstLstASB::CanCreateNewPstLstData(void)
{
	return (AnyCSBSupports(make_functor(&IPstLstSuite::CanCreateNewPstLstData), this));
}

/*	PstLstASB::CreateNewPstLstData
*/
ErrorCode PstLstASB::CreateNewPstLstData(const PMString&	newPstLstData )
{
	return (Process(make_functor(&IPstLstSuite::CreateNewPstLstData, newPstLstData), this, IID_IPSTLST_ISUITE));
}

/*	PstLstASB::CanModifyPstLstData
*/
bool16 PstLstASB::CanModifyPstLstData(void)
{
	return (AnyCSBSupports(make_functor(&IPstLstSuite::CanModifyPstLstData), this));
}

/*	PstLstASB::ModifyPstLstData
*/
ErrorCode PstLstASB::ModifyPstLstData(const PMString&	newPstLstData )
{
	return (Process(make_functor(&IPstLstSuite::ModifyPstLstData, newPstLstData), this, IID_IPSTLST_ISUITE));
}

/*	PstLstASB::CanDeletePstLstData
*/
bool16 PstLstASB::CanDeletePstLstData(void)
{
	return (AnyCSBSupports(make_functor(&IPstLstSuite::CanDeletePstLstData), this));
}

/*	PstLstASB::DeletePstLstData
*/
ErrorCode PstLstASB::DeletePstLstData(void)
{
	return (Process(make_functor(&IPstLstSuite::DeletePstLstData), this, IID_IPSTLST_ISUITE));
}

/*	PstLstASB::GetPstLstDataDescription
*/
void PstLstASB::GetPstLstDataDescription(PstLstDataVector& pstlstDataVector)
{
	CallEach(make_functor_void(&IPstLstSuite::GetPstLstDataDescription, pstlstDataVector), this);
}

/*	PstLstASB::GetSelectedItemIndex
*/
void PstLstASB::GetSelectedItemIndex(int32& uidListIndex)
{
	CallEach(make_functor_void(&IPstLstSuite::GetSelectedItemIndex, uidListIndex), this);
}

/*	PstLstASB::SetSelectedItemIndex
*/
ErrorCode PstLstASB::SetSelectedItemIndex(int32 uidListIndex)
{
	return (Process(make_functor(&IPstLstSuite::SetSelectedItemIndex, uidListIndex), this, IID_IPSTLST_ISUITE));
}
