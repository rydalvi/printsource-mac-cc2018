#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "CPMUnknown.h"
#include "IOptnsListData.h"
#include "listData.h"
#include "OptnsDragEventFinder.h" 

class IPMStream;

class PstLstData : public CPMUnknown<IPstLstData>
{
	public:
		PstLstData(IPMUnknown* boss);
		virtual const PMString&	GetName();
		virtual void SetName(const PMString& theData);
		virtual void GetOtherInfo(int32* eleId, int32* typid);

	private:
		PMString	fUIDStr;
		PMString	fName;
		int32 elementId;
		int32 typeId;
		int32 parentId;
		int32 reserved1;
		int32 reserved2;
		PMString reservedStr1;
		PMString reservedStr2;

};

CREATE_PMINTERFACE(PstLstData, kPstLstDataImpl)

PstLstData::PstLstData(IPMUnknown* boss)
: CPMUnknown<IPstLstData>(boss)
{
}

void PstLstData::SetName(const PMString& newData)
{
	if (fName != newData)
	{
		fName = newData;
		listData lstdta;
		listInfo info;

		info  = lstdta.getData
			(OptnsDragEventFinder::listboxType,
			 OptnsDragEventFinder::selectedRowIndex);
		
		elementId = info.id;
		typeId = info.typeId;
		parentId=-1;
		reserved1=-1;
		reserved2=-1;
	}
}

const PMString& PstLstData::GetName()
{
	return fName;
}

void PstLstData::GetOtherInfo(int32 *eleId, int32 *typid)
{
	*eleId=elementId;
	*typid=typeId;
}
