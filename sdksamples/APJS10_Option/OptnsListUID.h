
#ifndef __OptnsListUID_h__
#define __OptnsListUID_h__

//Interface includes
#include "IOptnsListUID.h"
#include "IPMStream.h"
#include "CPMUnknown.h"

//General includes
#include "HelperInterface.h"

class PstLstUIDList : public CPMUnknown<IPstLstUIDList>
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PstLstUIDList(IPMUnknown* boss);
		
		/**
			Destructor, the UIDList is deleted.
		*/
		virtual ~PstLstUIDList();
		
		/**
			The selected UID of the list is set with the parameter being passed in.  It is called
			mainly by a undo operation of a command, for example, a "New" command will add a new
			UID to the list and the list will update its selected UID with the new one, the command
			then should remember the old selected UID for undo operation, so in an undo of a command
			it can call this method to reset to the old state.
			
			@param uid the UID of the object to be set as the selected one
		*/
		virtual void SetSelectedUID(const UID& uid);
		
		/**
			This function returns the UID of its current selection.
			@return UID the UID of the current selection (fSelectedUID).
		*/
		virtual const UID& GetSelectedUID();
		
		/**
			When a user selects a new item from the drop down list, its corresponding UID list needs
			to be updated with the current selection change, fSelectedUID will be updated with the 
			selected object's UID in this routine.
			
			@param index the new current selection index.
		*/
		virtual void SetSelectedIndex(const int32& index);
	
		/**
			This function returns the index of its current selection as represented in the UID list.
			@return index of the current selection.
		*/
		virtual int32 GetSelectedIndex();
		
		/**
			This function appends an uid of an object onto the UID list that it maintains.
			@param uid the UID of the object to be appened to the UID list.
		*/
		virtual void Append(const UID& uid);
		
		/**
			This function removes an uid of an object from the UID list that it maintains.
			@param uid the UID of the object to be removed from the UID list.
		*/
		virtual void Remove(const UID& uid);
	
		/**
			@return UID list this class maintains.
		*/
		virtual const UIDList* GetUIDList();
	
		/**
			@return the UIDRef of the current selected object.
		*/
		virtual UIDRef GetSelectedUIDRef();
	
		/**
			Given an index, this routine will returns its corresponding UIDRef of the object as referenced by the
			index.
			@param index the index of the object in the UID list that the user is interested in.
			@return the UIDRef of the object that the user is inquiring about.
		*/
		virtual UIDRef GetRef(int32 index);
		
		/** 
			@return the database that stores the UID list 
		*/
		virtual IDataBase* GetDataBase();
		
		/**
			Because this is a persistent interface, it must support the ReadWrite method. This method is used for 
			both writing information out to the database and reading information in from the database.
			
			The infomation it reads/writes include the current selected UID and the whole UID list.

			@param stream contains the stream to be written or read.
			@param implementation the implementation ID.
		*/
		virtual void ReadWrite(IPMStream* s, ImplementationID prop);
	
	private:
		UID fSelectedUID;
		UIDList* fUIDList;
		void ReadWriteUIDList(IPMStream* s, ImplementationID prop);
};

#endif // __PstLstUIDList_h__

// End, PstLstUIDList.h.
