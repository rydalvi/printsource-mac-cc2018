#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"
#include "listData.h"
#include "OptnsDragEventFinder.h"
#include "IControlView.h"
#include "IListControlData.h"	
#include "IListBoxController.h"
#include "SDKListBoxHelper.h"
#include "ITextControlData.h"
#include "IDialogController.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsTaskPanelObserver : public CDialogObserver
{
	public:
		OptnsTaskPanelObserver(IPMUnknown* boss) : CDialogObserver(boss) {}
		virtual ~OptnsTaskPanelObserver() {}
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		void clearSelection(InterfacePtr<IPanelControlData> panelCntrlData);
};

CREATE_PMINTERFACE(OptnsTaskPanelObserver, kTaskPanelObserverImpl)

void OptnsTaskPanelObserver::AutoAttach()
{
	//CA("Task panel");
/* Here setting the current panel index */
OptnsDragEventFinder::currentTab=3;

	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			CA("panelControlData is nil");
			break;
		}
		this->clearSelection(panelControlData);
		AttachToWidget(kTaskPanelListBoxWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		AttachToWidget(kAppendRadBtnWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kEmbedRadBtnWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
	} while (false);
}

void OptnsTaskPanelObserver::clearSelection(InterfacePtr<IPanelControlData> panelCntrlData)
{
	do
	{
		IControlView* textView1 = panelCntrlData->FindWidget(kTaskPanelIDWidgetID);
		if(textView1==nil) 
		{
			CAlert::InformationAlert("IControlView is nil");
			break;
		}
		InterfacePtr<ITextControlData> iData1(textView1, UseDefaultIID());
		if(iData1==nil)
		{
			CAlert::InformationAlert("ITextControlData is nil");
			break;
		}
		iData1->SetString("");

		IControlView* textView2 = panelCntrlData->FindWidget(kTaskPanelNameWidgetID);
		if(textView2==nil)
		{
			CAlert::InformationAlert("IControlView is nil");
			break;
		}
		InterfacePtr<ITextControlData> iData2(textView2, UseDefaultIID());
		if(iData2==nil)
		{
			CAlert::InformationAlert("ITextControlData is nil");
			break;
		}
		iData2->SetString("");

		IControlView* textView3 = panelCntrlData->FindWidget(kTaskPanelColumnWidgetID);
		if(textView3==nil)
		{
			CAlert::InformationAlert("IControlView is nil");
			break;
		}
		InterfacePtr<ITextControlData> iData3(textView3, UseDefaultIID());
		if(iData3==nil)
		{
			CAlert::InformationAlert("ITextControlData is nil");
			break;
		}
		iData3->SetString("");
	}while(kFalse);
}

void OptnsTaskPanelObserver::AutoDetach()
{
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			CA("panelControlData is nil");
			break;
		}
		DetachFromWidget(kTaskPanelListBoxWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		DetachFromWidget(kAppendRadBtnWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kEmbedRadBtnWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
	} while (false);
}

void OptnsTaskPanelObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				CA("panel is nil");
				break;
			}

			SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
			IControlView * listBoxCntrlView = listHelper.FindCurrentListBox(panel, 3);
			if(listBoxCntrlView == nil) 
			{
				CA("listbox controlview is nil");
				break;
			}

			InterfacePtr<IListBoxController> listCntl(listBoxCntrlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(listCntl == nil) 
			{
				CA("listboxcntrller is nil");
				break;
			}

			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;

			const int kSelectionLength =  multipleSelection.Length() ;

			int selectedRowIndex=-1;

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				selectedRowIndex=multipleSelection[0];
				//CA("Got selection");
				PMString str("selecterowindex ");
				str.AppendNumber(selectedRowIndex );
				//CA(str);
			}

			listData genPanelListData;

			int32 vectSize = genPanelListData.returnListVectorSize(3);
			PMString vectsize("Vector size ");
			vectsize.AppendNumber(vectSize);
			//CA(vectsize);

			PMString idstr("");
			PMString typeidstr("");
			PMString name("");
			PMString tablecolstr("");

			for(int y=0; y<vectSize; y++)
			{
				if(y==selectedRowIndex)
				{
					//CA("inside if stmt");
					listInfo list = genPanelListData.getData(3,y);
					idstr.AppendNumber(list.id);
					//CA(idstr);

					typeidstr.AppendNumber(list.typeId);
					//CA(typeidstr);

					tablecolstr = list.tableCol;
					//CA(list.tableCol);

					name = list.name;
					//CA(list.name);
					break;
				}
			}

			IControlView* textView1 = panel->FindWidget(kTaskPanelIDWidgetID);
			if(textView1==nil) 
			{
				CAlert::InformationAlert("IControlView is nil");
				break;
			}
			InterfacePtr<ITextControlData> iData1(textView1, UseDefaultIID());
			if(iData1==nil)
			{
				CAlert::InformationAlert("ITextControlData is nil");
				break;
			}
			iData1->SetString(idstr);

			IControlView* textView2 = panel->FindWidget(kTaskPanelNameWidgetID);
			if(textView2==nil)
			{
				CAlert::InformationAlert("IControlView is nil");
				break;
			}
			InterfacePtr<ITextControlData> iData2(textView2, UseDefaultIID());
			if(iData2==nil)
			{
				CAlert::InformationAlert("ITextControlData is nil");
				break;
			}
			iData2->SetString(name);

			IControlView* textView3 = panel->FindWidget(kTaskPanelColumnWidgetID);
			if(textView3==nil)
			{
				CAlert::InformationAlert("IControlView is nil");
				break;
			}
			InterfacePtr<ITextControlData> iData3(textView3, UseDefaultIID());
			if(iData3==nil)
			{
				CAlert::InformationAlert("ITextControlData is nil");
				break;
			}
			iData3->SetString(tablecolstr);

		}while(kFalse);
	}
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
	if (controlView == nil)
	{
		ASSERT_FAIL("DBTDialogObserver::Update() controlView invalid");
		CA("controlview nil");
	}
	WidgetID theSelectedWidget = controlView->GetWidgetID();
	if (theSelectedWidget == kAppendRadBtnWidgetID  && theChange == kTrueStateMessage)
	{
		InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
		if(theCntrller==nil)
		{
			CA("theCntrller is nil");
			return;
		}
		theCntrller->SetTriStateControlData(kEmbedRadBtnWidgetID, kFalse);
	}
	if (theSelectedWidget == kEmbedRadBtnWidgetID  && theChange == kTrueStateMessage)
	{
		InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
		if(theCntrller==nil)
		{
			CA("theCntrller is nil");
			return;
		}
		theCntrller->SetTriStateControlData(kAppendRadBtnWidgetID, kFalse);
	}
/* Getting which Radio Button is selected */
	InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
	if(panel==nil)
	{
		CA("panel is nil");
		return;
	}
	InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
	if(theCntrller==nil)
	{
		CA("theCntrller is nil");
		return;
	}

	ITriStateControlData::TriState appendRadBtnstate;
	appendRadBtnstate = theCntrller->GetTriStateControlData(kAppendRadBtnWidgetID, panel);
	if(appendRadBtnstate==ITriStateControlData::kSelected)
	{
		OptnsDragEventFinder::whichRadBtnSelected=1;
		//CA("Append RadioBtn Selected");
	}

	ITriStateControlData::TriState embedRadBtnstate;
	embedRadBtnstate = theCntrller->GetTriStateControlData(kEmbedRadBtnWidgetID, panel);
	if(embedRadBtnstate==ITriStateControlData::kSelected)
	{
		OptnsDragEventFinder::whichRadBtnSelected=2;
		//CA("Embed RadioBtn Selected");
	}
}
