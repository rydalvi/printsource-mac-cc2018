/*
//	File:	PathBrowseDlgController.cpp
//
//	Date:	5-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

// Project includes:
#include "OptnsDlgID.h"
#include "OptionsStaticData.h"
#include "OptionsUtils.h"
#include "CAlert.h"
#include "ITextControlData.h"

#define CA(X) CAlert::InformationAlert(X);

/** PathBrowseDlgController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
class PathBrowseDlgController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PathBrowseDlgController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~PathBrowseDlgController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields( IActiveContext* );

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			this method to be called. When all widgets are valid, 
			ApplyFields will be called.		
			@param myContext
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields( IActiveContext* );

		/**
			Retrieve the values from the widgets and act on them.
			@param myContext
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields( IActiveContext* , const WidgetID& );
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(PathBrowseDlgController, kPathBrowseDlgControllerImpl)

/* ApplyFields
*/
void PathBrowseDlgController::InitializeDialogFields(IActiveContext* mtContext) 
{
	//CA("PathBrowseDlgController::InitializeDialogFields");
	CDialogController::InitializeDialogFields(mtContext);
	do{
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
			break;

		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		ClientOptionsManip cops;			
		OptionsValue optnsObj = cops.ReadClientOptions();
		
		if(optnsObj.getAssetStatus() == 2){
			//CA("Download");
			dlgController->SetTriStateControlData(kselectDownloadImagesToWidgetID, kTrue, nil,kTrue,kTrue);
			
			IControlView* iSelectImagePathButtonCntrlView=panelControlData->FindWidget(kselectimagePathButtonWidgetID);
			if(iSelectImagePathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectImagePathButtonCntrlView->HideView();

			IControlView* iSelectDownloadPathButtonCntrlView=panelControlData->FindWidget(kselectDownloadImagePathButtonWidgetID);
			if(iSelectDownloadPathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectDownloadPathButtonCntrlView->ShowView();
		
			IControlView* iselectimagePathStaticTextWidgetID1=panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
			if(iselectimagePathStaticTextWidgetID1==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectimagePathStaticTextWidgetID1->HideView();

			IControlView* iselectDownloadImagePathStaticTextWidgetID=panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
			if(iselectDownloadImagePathStaticTextWidgetID==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectDownloadImagePathStaticTextWidgetID->ShowView();

			IControlView* iControlView = panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
			if (iControlView == nil){
				//CAlert::InformationAlert("iControlView nil");
				break;
			}	
			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				break;
			}
            PMString imagePath = optnsObj.getImagePath();
            imagePath.ParseForEmbeddedCharacters();
			textControlData->SetString(imagePath);
		}
		else{
			//CA("MAP");
			dlgController->SetTriStateControlData(kselectMaptoRepositryWidgetID, kTrue, nil,kTrue,kTrue);
			
			IControlView* iSelectImagePathButtonCntrlView=panelControlData->FindWidget(kselectimagePathButtonWidgetID);
			if(iSelectImagePathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectImagePathButtonCntrlView->ShowView();

			IControlView* iSelectDownloadPathButtonCntrlView=panelControlData->FindWidget(kselectDownloadImagePathButtonWidgetID);
			if(iSelectDownloadPathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectDownloadPathButtonCntrlView->HideView();
			
			IControlView* iselectimagePathStaticTextWidgetID1=panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
			if(iselectimagePathStaticTextWidgetID1==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectimagePathStaticTextWidgetID1->ShowView();

			IControlView* iselectDownloadImagePathStaticTextWidgetID=panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
			if(iselectDownloadImagePathStaticTextWidgetID==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectDownloadImagePathStaticTextWidgetID->HideView();
			
			IControlView* iControlView = panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
			if (iControlView == nil){
				//CAlert::InformationAlert("iControlView nil");
				break;
			}	
			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				break;
			}
			PMString imgpth(kBlankStringKey);
			imgpth.SetTranslatable(kFalse);
			imgpth = optnsObj.getImagePath();
            imgpth.ParseForEmbeddedCharacters();
			textControlData->SetString(imgpth);
		}
	}while(kFalse);

}

/* ValidateFields
*/
WidgetID PathBrowseDlgController::ValidateDialogFields(IActiveContext* myContext) 
{
	//CA("PathBrowseDlgController::ValidateDialogFields");
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.

	return result;
}

/* ApplyFields
*/
void PathBrowseDlgController::ApplyDialogFields(IActiveContext* myContext , const WidgetID&  widgetID) 
{
	// Replace with code that gathers widget values and applies them.
	

	//SystemBeep();  
}
//  Generated by Dolly build 17: template "Dialog".
// End, PathBrowseDlgController.cpp.
	