/*
//	File:	PubBrowseDlgObserver.cpp
//
//	Date:	5-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "PMString.h"
#include "InterfacePtr.h"
#include "ITextControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

// General includes:
#include "CDialogObserver.h"
#include "SystemUtils.h"
#include "CAlert.h"

// Project includes:
#include "OptnsDlgID.h"
#include "OptionsStaticData.h"
#include "OptionsUtils.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "IDialogController.h"

extern VectorPubBrowseDropDownListData LanguageDropDwonList;
extern VectorPubBrowseDropDownListData CatalogDropDownList;
//#include "ApplicationFrameHeader.h"
#define CA(X) CAlert::InformationAlert(X);

/** PubBrowseDlgObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class PubBrowseDlgObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PubBrowseDlgObserver(IPMUnknown* boss) : CDialogObserver(boss) { }

		/** Destructor. */
		virtual ~PubBrowseDlgObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/

		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);	
		void PopulateCatalogDropDown(double);
	private:
		PMString GetSelectedPublicationName(IControlView*, double & selectedpubId); //added argument(selectedpubId) by mane
		PMString GetSelectedLocaleName(IControlView*, double &selectedlangId); //added argument(selectedlangId) by mane
		PMString publicationName;	  
		PMString localeName;
		PMString projectId;
		PMString localeId;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(PubBrowseDlgObserver, kPublicationDialogObserverImpl)

/* AutoAttach
*/
void PubBrowseDlgObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			//CA("PubBrowseDlgObserver::AutoAttach() panelControlData invalid");
			break;
		}		
		// Attach to other widgets you want to handle dynamically here.
		AttachWidget(panelControlData, kPubDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(panelControlData, kLocaleDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		
	} while (false);
}

/* AutoDetach
*/
void PubBrowseDlgObserver::AutoDetach()
{	
	CDialogObserver::AutoDetach();
	do
	{	// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			//CA("PubBrowseDlgObserver::AutoDetach() panelControlData invalid");
			break;
		}		
		// Detach from other widgets you handle dynamically here.		
		DetachWidget(panelControlData, kPubDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA);		
		DetachWidget(panelControlData, kLocaleDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA);		

	} while (false);
}

/* Update
*/
void PubBrowseDlgObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);	
	do
	{
		//CA("PubBrowseDlgObserver");
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ASSERT_FAIL("PubBrowseDlgObserver::Update() controlView invalid");
			//CAlert::InformationAlert("Observer : controlView invalid");
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if(theSelectedWidget ==kLocaleDropDownWidgetID)
		{
			//CA("theSelectedWidget ==kLocaleDropDownWidgetID");		
			
//			int32 selectedlangId;
//			localeName = GetSelectedLocaleName(controlView, selectedlangId);
//			PersistData::setLocaleName(localeName);
//			PersistData::setPubId(selectedlangId);
//			//CA("localeName : " + localeName);
//			ClientOptionsManip cop;
//			int32 LocaleID= cop.checkforLocaleExistance(localeName,selectedlangId);
//			localeId.Clear();
//			localeId.AppendNumber(LocaleID);
//			PersistData::setLocaleID(LocaleID);
//			// changes on 27 JAN
//			int32 localeID = PersistData::getLocaleID();
//			PopulateCatalogDropDown(localeID);
//////////////26MAR09
//			OptionsValue optnsObj = cop.ReadClientOptions();
//			optnsObj.setDefaultLocale(localeName);
//			optnsObj.setLocaleID(localeId);
//			cop.WriteClientOptions(optnsObj);
///////////////	END
//			//CA("localeId : "+ localeId);
//			// end changes vaibhav
			
		
		}
		else if(theSelectedWidget ==kPubDropDownWidgetID)
		{
			//AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav
			//CA("theSelectedWidget ==kPubDropDownWidgetID");
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == nil){
//				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//				return ;
//			}
// //=================added by mane===========================
//			int32 selectedpubId; 
//			publicationName = GetSelectedPublicationName(controlView, selectedpubId); 
//			PersistData::setPubName(publicationName); 
//			PersistData::setPubId(selectedpubId);     
//			ClientOptionsManip cop;
//
//			int32 PubID= cop.checkforPublicationExistance(publicationName, selectedpubId);
//			PersistData::setPubId(PubID);
//			projectId.Clear();
//			projectId.AppendNumber(PubID);
///////////////26MAR09	
//			OptionsValue optnsObj = cop.ReadClientOptions();
//			optnsObj.setPublicationName(publicationName);
//			optnsObj.setProjectID(projectId);
//			cop.WriteClientOptions(optnsObj);
//////////////END
			//CA("projectId : " + projectId);
			//if(ptrIAppFramework !=nil) commented by vaibhav
				//delete ptrIAppFramework;
		
		}
		else if(theSelectedWidget ==kOKButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget ==kOKButtonWidgetID ");
			// if publication browse dialog showing with options dialog	
			
			if(PersistData::getPublicationDialogType()==1)
			{
				//CA("PersistData::getPublicationDialogType()==1");
				do{
					IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
					if(panelControlData==nil){
						//CAlert::InformationAlert("General panel control data nil");
						break;
					}

					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == nil){
						CAlert::InformationAlert("Pointer to IAppFramework is nil.");
						return ;
					}

					IControlView* iControlView = panelControlData->FindWidget(kPublicationStaticWidgetID);
					if(iControlView == nil){
						//CAlert::InformationAlert("Fail to find publication static text widget");
						break;
					}	

					InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
					if (textControlData == nil){
						//CAlert::InformationAlert("ITextControlData nil");	
						break;
					}
	
                    PMString name = PersistData::getPubName();
                    name.SetTranslatable(kFalse);
                    name.ParseForEmbeddedCharacters();
					textControlData->SetString(name);

					IControlView* iControlView1 = panelControlData->FindWidget(kLocaleStaticWidgetID);
					if(iControlView1 == nil){
						//CAlert::InformationAlert("Fail to find publication static text widget");
						break;
					}			
					InterfacePtr<ITextControlData> textControlData1(iControlView1,UseDefaultIID());
					if (textControlData1 == nil){
						//CAlert::InformationAlert("ITextControlData1 nil");	
						break;
					}
	
					textControlData1->SetString((PersistData::getLocaleName().SetTranslatable(kFalse)));

					OptionsValue optnsObj = PersistData::getOptions();
                    PMString localName = PersistData::getLocaleName();
					optnsObj.setDefaultLocale(localName);
                    PMString pubName = PersistData::getPubName();
					optnsObj.setPublicationName(pubName);

					PMString projectIdString("");
					projectIdString.AppendNumber(PMReal(PersistData::getPubId()));
					optnsObj.setProjectID(projectIdString);	

					PMString localeIdString("");
					localeIdString.AppendNumber(PMReal(PersistData::getLocaleID()));
					optnsObj.setLocaleID(localeIdString);	

					PersistData::setOptions(optnsObj);
					ptrIAppFramework->setLocaleId(PersistData::getLocaleID());
					ptrIAppFramework->setCatalogId(PersistData::getPubId());
				//CA("2");
				}while(kFalse);
			}// else showing only pub browse dialog					
			else if(PersistData::getPublicationDialogType()==2 )
			{
				//CA("PersistData::getPublicationDialogType()==2");
				do
				{
					//CA("3");
//					ClientOptionsManip cops;				
//					///*bool8 isFileExists=cops.IsClientOptionsFileExiats();
					//if(isFileExists==FALSE){
					//	PMString filePath = cops.GetOptionsFilePath();
					//	if(cops.CreateOptionsFile(filePath)==FALSE){
					//		CAlert::InformationAlert("Faile to Create File");
					//		break;
					//	}
					//}*/
//					OptionsValue optnsObj = cops.ReadClientOptions();
//
//					//if(localeName.NumUTF16TextChars() == 0)
					//{
					//	InterfacePtr<IDialogController> LoginDialogControllerObj(this,UseDefaultIID());
					//	if (LoginDialogControllerObj == nil){
					//		CAlert::InformationAlert("LoginDialogControllerObj nil");
					//		break;
					//	}
					//	
					//	InterfacePtr<IStringListControlData> LocaleDropListData(
					//		LoginDialogControllerObj->QueryListControlDataInterface(kLocaleDropDownWidgetID));
					//	if(LocaleDropListData == nil){
					//		CAlert::InformationAlert("LocaleDropListData nil");
					//		return;
					//	}
					//	
					//	InterfacePtr<IDropDownListController> LocaleDropListControllerx(LocaleDropListData,UseDefaultIID());
					//	if (LocaleDropListControllerx == nil){
					//		CAlert::InformationAlert("LocaleDropListControllerx nil");
					//		return;
					//	}
					//	
					//	int32 selectedLangIndex = LocaleDropListControllerx->GetSelected();
					//								
					//	PMString LocaleNamestring = LocaleDropListData->GetString(selectedLangIndex);
					//	//localeName = LocaleNamestring;

					//}
//
//					if(localeName.NumUTF16TextChars() != 0)
//					{
//						optnsObj.setDefaultLocale(localeName);
//						optnsObj.setLocaleID(localeId);
//					}
//					else
//					{
//						//CA("OK button... local name len is 0");
//						localeName = optnsObj.getDefaultLocale();
//						localeId = optnsObj.getLocaleID();
//					}
//					
//					//CA("localeName in ok button OBS : " + localeName);
//					PMString ASD("localeId : ");
//					ASD.Append(localeId);
//					ASD.Append("\nlocaleName	:	");
//					ASD.Append(localeName);
//					ASD.Append("\npublicationName	:	");
//					ASD.Append(publicationName);
//					//CA(ASD);
//					
///					optnsObj.setPublicationName(publicationName);
//					optnsObj.setProjectID(projectId);					
//					cops.WriteClientOptions(optnsObj);
//	//CA("4");
				}while(kFalse);
			}
		}
		else if(theSelectedWidget ==kCancelButton_WidgetID && theChange == kTrueStateMessage)
		{
			//PersistData::setPubBrowseDlgCancelPressFlag(TRUE);
		}
	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, PubBrowseDlgObserver.cpp.

PMString PubBrowseDlgObserver::GetSelectedPublicationName(IControlView* pubControlView, double & selectedpubId)
{
	//CA("PubBrowseDlgObserver::GetSelectedPublicationName");
	PMString pubName("");
	do{
		InterfacePtr<IStringListControlData> pubDropListData(pubControlView,UseDefaultIID());	
		if(pubDropListData == nil){
			//CAlert::InformationAlert("pubDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> pubDropListController(pubDropListData, UseDefaultIID());
		if(pubDropListController == nil){
			//CAlert::InformationAlert("pubDropListController nil");
			break;
		}
		int32 selectedRow = pubDropListController->GetSelected();

		
		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			break;
		}

		selectedpubId = CatalogDropDownList[selectedRow].ID; //aaded by mane
		pubName = pubDropListData->GetString(selectedRow);
		if(selectedRow>=0){
			InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
			if (panelControlData == nil){
				//CAlert::InformationAlert("panelControlData nil");
				break;
			}
			IControlView* controlView = panelControlData->FindWidget(kOKButtonWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("Publication Browse Button ControlView nil");
				break;
			}
			if(!controlView->GetEnableState())
				controlView->Enable(kTrue);
		}

	}while(0);	

	return pubName;
} 

PMString PubBrowseDlgObserver::GetSelectedLocaleName(IControlView* LocaleControlView, double & selectedlangId)
{	
	//CA("PubBrowseDlgObserver::GetSelectedLocaleName");
	PMString locName("");
	do{
		InterfacePtr<IStringListControlData> LocaleDropListData(LocaleControlView,UseDefaultIID());	
		if(LocaleDropListData == nil){
			//CAlert::InformationAlert("LocaleDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> LocaleDropListController(LocaleDropListData, UseDefaultIID());
		if(LocaleDropListController == nil){
			//CAlert::InformationAlert("LocaleDropListController nil");
			break;
		}
		int32 selectedRow = LocaleDropListController->GetSelected();
		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			break;
		}
		selectedlangId = LanguageDropDwonList[selectedRow].ID; //aaded by mane
		locName = LocaleDropListData->GetString(selectedRow);
		if(selectedRow>=0){
			InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
			if (panelControlData == nil){
				//CAlert::InformationAlert("panelControlData nil");
				break;
			}
			IControlView* controlView = panelControlData->FindWidget(kPubDropDownWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("Publication Browse Button ControlView nil");
				break;
			}
			IControlView* controlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
			if (controlView1 == nil){
				//CAlert::InformationAlert("Ok Button ControlView nil");
				break;
			}
			if(!controlView->GetEnableState()){
				controlView->Enable(kTrue);
				//controlView1->Enable(kTrue);
			}
		}
	}while(0);	
	return locName;
} 


void PubBrowseDlgObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{	
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil){
			//ASSERT_FAIL("widget has no ISubject... Ouch!");
			break;
		}
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); // only do once
}

/*	::DetachWidget
*/
void PubBrowseDlgObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
		{
			//ASSERT_FAIL("iControlView invalid. Where the widget are you?");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
		{
			//ASSERT_FAIL("PstLst Panel widget has no ISubject... Ouch!");
			break;
		}
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void PubBrowseDlgObserver::PopulateCatalogDropDown(double localeID)
{
	do
	{
		//CA("After click over language drop down list");
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			//CAlert::InformationAlert("panelControlData nil");
			return;
		}
		IControlView* controlView = panelControlData->FindWidget(kPubDropDownWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("Publication Browse Button ControlView nil");
			return;
		}
		InterfacePtr<IDropDownListController> PubDropListController(controlView,UseDefaultIID());
		if (PubDropListController == nil){
			//CAlert::InformationAlert("PubDropListController nil");
			return;
		}
		InterfacePtr<IStringListControlData> PubDropListData(PubDropListController, UseDefaultIID());
		if (PubDropListData == nil)
		{
			//ASSERT_FAIL("iStringListControlData invalid");
			break;
		}
		///*InterfacePtr<IStringListControlData> PubDropListData(
		//	this->QueryListControlDataInterface(kPubDropDownWidgetID));
		//if(PubDropListData == nil){
		//	CAlert::InformationAlert("PubDropListData nil");
		//	*/return;
		//}

		PubDropListData->Clear(kFalse,kFalse);		
		IControlView* controlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
		if (controlView1 == nil){
			//CAlert::InformationAlert("Publication Browse Button ControlView nil");
			return;
		}
		//AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav
		LanguageDropDwonList.clear();//added by mane

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

		if(ptrIAppFramework == nil){
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return ;
		}
		
		/*bool16 ClarFlag1 =*/ ptrIAppFramework->EventCache_clearInstance();
		/*if(ClarFlag1)
			CA("ProjectCache Cleared");*/

	//added by Tushar on 20/09 START////////////////////////////////////////////////			
		int i = 0;
		CatalogDropDownList.clear();

		VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
		if(vectClassInfoValuePtr!=nil)
		{		
			//CA("vectClassInfoValuePtr==nil");			
			VectorClassInfoValue::iterator it1;			

			for(it1=vectClassInfoValuePtr->begin(); it1!=vectClassInfoValuePtr->end(); it1++)
			{
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

				PMString tempclassificationName;
				//int32 tempclassificationId;
				if(!iConverter)
					tempclassificationName = it1->getClassification_name();
				else{
					tempclassificationName = iConverter->translateString(it1->getClassification_name());
					//classificationName = tempclassificationName;
					//tempclassificationName = iConverter->handleAmpersandCase(tempclassificationName);
				}
			
				PubDropListData->AddString(tempclassificationName, i, kFalse, kFalse);

				PubBrowseDropDownListData CatalogRowObject; //added by mane
				CatalogRowObject.name = tempclassificationName;
				CatalogRowObject.ID = it1->getClass_id();
				CatalogDropDownList.push_back(CatalogRowObject);
				i++;
			}
//12-april
			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;
//12-april
		}
			
//added by Tushar on 20/09 END/////////////////////////////////////////////////

		VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllProjects(localeID);
		if(VectorPublInfoValuePtr == nil){
				//CAlert::InformationAlert("No catalogs available for selected language!");
				PubDropListController->Select(-1);

				IControlView* controlView = panelControlData->FindWidget(kOKButtonWidgetID);
					if (controlView == nil){
						//CAlert::InformationAlert("Publication Browse Button ControlView nil");
						break;
					}
				controlView->Disable();
				break;
			}
			
			if(VectorPublInfoValuePtr->size() == 0)
			{
				//CAlert::InformationAlert("No catalogs available for selected Language!");
				PubDropListController->Select(-1);

				IControlView* controlView = panelControlData->FindWidget(kOKButtonWidgetID);
				if (controlView == nil){
					//CAlert::InformationAlert("Publication Browse Button ControlView nil");
					break;
				}
				controlView->Disable();

				break;
			}
			//VectorCatalogValue::iterator it;	changes by Vaibhav	
		
			VectorPubModel::iterator it;
			
			for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
			{			
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		
				PMString CatlogName("");
				PMString TempBuffer("");

				//if(ptrIAppFramework->CONFIGCACHE_getCmShowEventNumber())
				//{
				//	
				//	TempBuffer = 	it->getNumber()+"-"+it->getName();	 
				//	//CA(" TempBuffer  =" +TempBuffer);
				//}
				//else
				{					
					TempBuffer = it->getName();
				}

				if(iConverter){ 
					CatlogName=iConverter->translateString(TempBuffer);
				}
				else{ 
					CatlogName = TempBuffer;
				}
				//PMString CatalogName(it->getcatalog_name()) /* it->getcatalog_name() */;		@vaibhav	
				//PMString CatalogName(it->getName()) /* it->getcatalog_name() */;			
				//CA("PubBrowseDLGOB "+CatalogName);
				PubDropListData->AddString(CatlogName, i, kFalse, kFalse);

				PubBrowseDropDownListData CatalogRowObject;
				CatalogRowObject.name = CatlogName;
				CatalogRowObject.ID = it->getEventId();
				CatalogDropDownList.push_back(CatalogRowObject);
				i++;
			}

			//PubDropListController->Select(-1);
			PubDropListController->Select(0);
//12-april
			if(VectorPublInfoValuePtr)
				delete VectorPublInfoValuePtr;
//12-april
		//	if(ptrIAppFramework !=nil) commented by vaibhav
		//		delete ptrIAppFramework;
	}while(0);	
				
}