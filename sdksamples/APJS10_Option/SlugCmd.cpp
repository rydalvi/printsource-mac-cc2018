#include "VCPlugInHeaders.h"
#include "ErrorUtils.h"
#include "PageItemUtils.h"	 
#include "ISlugData.h"
#include "OptnsDlgID.h"
#include "Command.h"
#include "vector"
#include "SlugStructure.h"
#include "CAlert.h"
#include "CommonFunctions.h"

typedef vector<SlugStruct> SlugList;

#define CA(Z) CAlert::InformationAlert(Z)

class SlugCmd : public Command
{
	public:
		SlugCmd(IPMUnknown* boss);
		~SlugCmd();
		bool16 LowMemIsOK() const { return kFalse; }


	protected:
		void Do();
		void Undo();
		void Redo();
		void DoNotify();

	private:
		virtual PMString* CreateUndoName();
		virtual PMString* CreateRedoName();
		SlugList* prevDataList;
		PMString* elemName;
		PMString* colName;
		int32 fItemCount;	
};

CREATE_PMINTERFACE(SlugCmd, kSlugCmdImpl)

SlugCmd::SlugCmd(IPMUnknown* boss) :Command(boss)
{
	prevDataList=nil;
	elemName=nil;
	colName=nil;
}

SlugCmd::~SlugCmd()
{
	if(prevDataList)
		delete [] prevDataList;
	if(elemName)
		delete elemName;
	if(colName)
		delete colName;
}

void SlugCmd::Do()
{
	fItemCount = fItemList.Length();
	if (fItemCount == 0)
		return;

	prevDataList=new SlugList[fItemCount];
	elemName=new PMString;
	colName=new PMString;

	InterfacePtr<ISlugData> commandData(this, UseDefaultIID());
	if (commandData == nil)
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
	}
	else
	{		
		for(int32 frameCount=0; frameCount<fItemCount; frameCount++)
		{
			InterfacePtr<ISlugData> frameData(fItemList.GetRef(frameCount), UseDefaultIID());
			if(frameData != nil)
			{
				frameData->GetList(prevDataList[frameCount], *elemName, *colName);
				SlugList anotherTempList;
				PMString tempName;
				PMString tempCol;

				commandData->GetList(anotherTempList, tempName, tempCol);
				frameData->SetList(anotherTempList, tempName, tempCol);
			}	
		}
	}
}


void SlugCmd::Redo()
{
	this->Undo();
}

void SlugCmd::Undo()
{
	SlugList tempList;
	PMString tempStr;
	PMString tempCol;
	for(int32 frameCount = 0; frameCount < fItemCount; frameCount++)
	{
		InterfacePtr<ISlugData> frameData(fItemList.GetRef(frameCount), UseDefaultIID());
		if(frameData != nil)
		{
			frameData->GetList(tempList, tempStr, tempCol);
			frameData->SetList(prevDataList[frameCount], *elemName, *colName);
			prevDataList[frameCount]=tempList;
			*elemName=tempStr;
			*colName=tempCol;

		}	
	}
	refreshLstbox(kInvalidUID);
}

void SlugCmd::DoNotify()
{
	if (fItemCount == 0)
	{
		ASSERT_FAIL("SlugCmd::DoNotify: empty item list");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
	}
	else
	{
		PageItemUtils::NotifyDocumentObservers
		(
			fItemList, 
			kLocationChangedMessage, 
			IID_ITRANSFORM_DOCUMENT, 
			this
		);
	}
}

PMString* SlugCmd::CreateUndoName()
{
	PMString* string = new PMString("Set Slug");
	return string;
}

PMString* SlugCmd::CreateRedoName()
{
	PMString* string = new PMString("UnSet Slug");
	return string;
}






