/*
//	File:	GeneralPanelObserver.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ISubject.h"
#include "SysFileList.h"
#include "IDFile.h"
//#include "ISelectFolderDialog.h" //Deprecated
#include "IOpenFileDialog.h" //Cs4
#include "SDKUtilities.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ITriStateControlData.h"/*21-feb*/

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"

// Project includes:
#include "OptnsDlgID.h"
#include "IWidgetParent.h"
#include "PubBrowseDlgCreator.h"
#include "OptionsUtils.h"
#include "IClientOptions.h"
#include "OptionsValue.h"
#include "OptionsStaticData.h"
#include "IAppFramework.h"
#include "ILoginHelper.h"
#include "ITextControlData.h"
#include "ISpecialChar.h"
//#include "ApplicationFrameHeader.h"
#include "PlatformChar.h"

#include "IPanelMgr.h"
#include "IWindow.h"
#include "PaletteRefUtils.h"
#include "ICategoryBrowser.h"
#include "CTBID.h"
#include "ILayoutUIUtils.h"
#include "AcquireModalCursor.h" 
//#include "IExportTemplate.h"
#include "AcquireModalCursor.h" 

#include "SDKFileHelper.h"


//#define CA(X) CAlert::InformationAlert(X)


inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(PMReal(num));
	return x;
}
//#define CA(X) CAlert::InformationAlert \
//	( \
//		PMString("MyControlView.cpp") + PMString("\n") + \
//		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
//		PMString("\n Message : ")+ X \
//	)

#define CA(X) CAlert::InformationAlert(X)
	
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}

// Constants
const PlatformChar UNIXDELCHAR('/');
const PlatformChar MACDELCHAR(':');
const PlatformChar WINDELCHAR('\\');

/** GeneralPanelObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class GeneralPanelObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		GeneralPanelObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~GeneralPanelObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();	
		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		/**
			This routine is a template for attaching a widget to a subject to be observed.
			@param iPanelControlData panel that contains the widget which we want to attach to.
			@param widgetID	the widget id that we want to attach to.
			@param interfaceID protocol we are observering.
		*/
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		
		/**
			This routine is a template for detaching a widget from a subject being observed.
			@param iPanelControlData panel that contains the widget which we want to detach from.
			@param widgetID	the widget id that we want to detach from.
			@param interfaceID protocol we are observering.
		*/
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	private:
		void setControlText(WidgetID,PMString);	
		void ShowBrowseDialog(PMString&,PMString&);
		void setDefaultValuesOfGeneralTab(IPanelControlData*);
		void ReadClientOptions(void);
		void closeOptnsDialog(void);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(GeneralPanelObserver, kGeneralPanelObserverImpl)

/* AutoAttach
*/
void GeneralPanelObserver::AutoAttach()
{	
	CDialogObserver::AutoAttach();	
	do
	{		
		// Get the IPanelControlData interface for the dialog:
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){			
			CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}
		//CA("OptnsDlgDialogController::InitializeDialogFields");
		
		//added by Tushar on 26/04....missing flag related addition
		IControlView* iMissingChkboxCntrlView=panelControlData->FindWidget(kMissingFlagWidgetID);
		if(iMissingChkboxCntrlView==nil)
		{	//CA("iMissingChkboxCntrlView nil");
			return;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldataMissingFlag(iMissingChkboxCntrlView, UseDefaultIID());
		if(itristatecontroldataMissingFlag==nil) 
		{	
			//CA("itristatecontroldataMissingFlag nil");
			return;
		}

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		}

		bool16 status = ptrIAppFramework->getMissingFlag();
		
		if(status)
			itristatecontroldataMissingFlag->Select();
		else
			itristatecontroldataMissingFlag->Deselect();
	

		////added by Tushar on 26/04....missing flag related addition

		// storing panelControlData for updating selected publication name from publication Dialog
		PersistData::setGeneralPanelControlData(panelControlData);
		// Attach to other widgets you want to handle dynamically here.
		AttachToWidget(kLocaleButtonWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kPunNameButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kImagePathButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kDownloadImagePathButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kOptnsMaptoRepositryWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kOptnsDownloadImagesToWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//AttachToWidget(kDocPathButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);// commented 23 Feb Vaibhav
		AttachToWidget(kOptnsOKButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kApplyButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kOptsCancelButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kUploadButtonWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kResetButtonWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		//added by avinash
		AttachToWidget(kMissingFlagWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kShowObjectCountWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		AttachToWidget(kLogLevelDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA ,panelControlData);	
		AttachToWidget(kByPassPromptWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		//upto here

		ReadClientOptions();
		setDefaultValuesOfGeneralTab(panelControlData);	
	} while (false);
	
}
/* AutoDetach
*/
void GeneralPanelObserver::AutoDetach()
{
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("AutoDetach panelControlData nil");
			break;
		}
		// Detach from other widgets you handle dynamically here.				
		DetachFromWidget(kLocaleButtonWidgetID,		IID_ITRISTATECONTROLDATA,panelControlData);
        DetachFromWidget(kPunNameButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kImagePathButtonWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kDownloadImagePathButtonWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kOptnsMaptoRepositryWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kOptnsDownloadImagesToWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
	//	DetachFromWidget(kDocPathButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData); // commented 23 Feb Vaibhav
		DetachFromWidget(kOptnsOKButtonWidgetID , IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kApplyButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kOptsCancelButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kUploadButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kResetButtonWidgetID ,IID_ITRISTATECONTROLDATA ,panelControlData);
		//added by avinash
		DetachFromWidget(kMissingFlagWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kShowObjectCountWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		DetachFromWidget(kLogLevelDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA, panelControlData);
		DetachFromWidget(kByPassPromptWidgetID,IID_ITRISTATECONTROLDATA ,panelControlData);
		//upto here
	} while (false);
}

/* Update
*/
void GeneralPanelObserver::Update(
	const ClassID& theChange,ISubject* theSubject, 
		const PMIID &protocol,void* changedBy)
{
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);	
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		}
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){	
			
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}
		//CA("GeneralPanelObserver::Update");
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil){
			
			//CAlert::InformationAlert("controlView nil");
			break;
		}
		IControlView* iUploadButtonCntrlView=panelControlData->FindWidget(kUploadButtonWidgetID);
		if(iUploadButtonCntrlView){	
			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(iDoc)
				iUploadButtonCntrlView->Enable();
			else
				iUploadButtonCntrlView->Disable();
		}

		//CA("controlView");
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		if(theSelectedWidget==kLocaleButtonWidgetID && theChange == kTrueStateMessage)
		{

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here


			//CA("theSelectedWidget==kLocaleButtonWidgetID");
			// for showing with options dialog			
			/*PersistData::setPublicationDialogType(1);
			PubBrowseDlgCreator pubDlagObj(this);
			pubDlagObj.CreateDialog();*/
			
			//----------

			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;

			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil)
			{
				//CA("iApplication==nil");
				break;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				break;
			}
			UID paletteUID = kInvalidUID;
			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;		
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;	

			const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

			IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(pnlControlView == NULL)
			{
				//CA("pnlControlView is NULL");
				break;
			}

			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}

			InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			if(palette == NULL)
			{
				//CA("palette is NULL");
				break;
			}

			InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
			PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
			/*if(palRef.IsValid())*/
				//CA("validPaletRef");
			//----------------New CS3 Changes--------------------//
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
			
			if(palette)						//if(palette)
			{
			//	CA("palette____2");
				palette->AddRef();
				GSysRect PalleteBounds = palette->GetFrameBBox();
				//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
                #ifdef WINDOWS
                TemplateTop		= PalleteBounds.top -30;
                TemplateLeft	= PalleteBounds.left;
                TemplateRight	= PalleteBounds.right;
                TemplateBottom	= PalleteBounds.bottom;
                #else
                TemplateTop		=SysRectTop(PalleteBounds)-30;
                TemplateLeft    =SysRectLeft(PalleteBounds);
                TemplateRight   =SysRectRight(PalleteBounds);
                TemplateBottom  =SysRectBottom(PalleteBounds); 
                #endif
				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
					return ;
				}
				CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 ,-1);
			}
		}
		
		else if(theSelectedWidget==kPunNameButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget==kPunNameButtonWidgetID");
			// for showing with options dialog
			/*PersistData::setPublicationDialogType(1);
			PubBrowseDlgCreator pubDlagObj(this);
			pubDlagObj.CreateDialog();*/

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil)
			{
				//CA("iApplication==nil");
				break;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				break;
			}
			UID paletteUID = kInvalidUID;
			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;		
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;	

			const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

			IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(pnlControlView == NULL)
			{
				//CA("pnlControlView is NULL");
				break;
			}

			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}
			InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			if(palette == NULL)
			{
				//CA("palette is NULL");
				break;
			}
			InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
			PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
			/*if(palRef.IsValid())*/
				//CA("validPaletRef");
			//----------------New CS3 Changes--------------------//
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
			
			if(palette)						//if(palette)
			{
				//CA("palette______3");
				palette->AddRef();
				GSysRect PalleteBounds = palette->GetFrameBBox();
				//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
                
                #ifdef WINDOWS
                TemplateTop		= PalleteBounds.top -30;
                TemplateLeft	= PalleteBounds.left;
                TemplateRight	= PalleteBounds.right;
                TemplateBottom	= PalleteBounds.bottom;
                #else
                TemplateTop		=SysRectTop(PalleteBounds)-30;
                TemplateLeft    =SysRectLeft(PalleteBounds);
                TemplateRight   =SysRectRight(PalleteBounds);
                TemplateBottom  =SysRectBottom(PalleteBounds);
                #endif
				
                
		
				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
					return ;
				}
				CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 ,-1);
			}

		}
		
		else if(theSelectedWidget==kImagePathButtonWidgetID && theChange == kTrueStateMessage)
		{

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here	

			//CA("kImagePathButtonWidgetID");
			AcquireWaitCursor awc;
			awc.Animate();

			PMString infoMsg("Browse for Image Working Directory");
			PMString path("");
			OptionsValue optnsObj = PersistData::getOptions();
			path = optnsObj.getImagePath();

			ShowBrowseDialog(path,infoMsg);

			setControlText(kImagePathStaticTextWidgetID,path);
			optnsObj.setImagePath(path);
			PersistData::setOptions(optnsObj);
		}
		
		else if(theSelectedWidget==kDownloadImagePathButtonWidgetID && theChange == kTrueStateMessage)
		{

			// added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			//CA("kImagePathButtonWidgetID");
			AcquireWaitCursor awc;
			awc.Animate();

			PMString infoMsg("Browse for Image Working Directory");
			PMString path("");
			OptionsValue optnsObj = PersistData::getOptions();
			path = optnsObj.getImagePath();
			ShowBrowseDialog(path,infoMsg);
			setControlText(kDownloadImagePathStaticTextWidgetID,path);
			optnsObj.setImagePath(path);
			PersistData::setOptions(optnsObj);

		}
		
		else if(theSelectedWidget==kOptnsMaptoRepositryWidgetID && theChange == kTrueStateMessage)
		{
			IControlView* iOptnsImagePathTextCntrlView=panelControlData->FindWidget(kImagePathStaticTextWidgetID);
			if(iOptnsImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here


			IControlView* iOptnsDownloadImagePathTextCntrlView=panelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
			if(iOptnsDownloadImagePathTextCntrlView==nil) 
			{
				//CA("iPropDownloadImagePathTextCntrlView==nil");
				return;
			}
			IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImagePathButtonWidgetID);
			
			if(iImgPathButtonCntrlView==nil) 
				return;
			IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kDownloadImagePathButtonWidgetID);
			
			if(iDownloadImgPathButtonCntrlView==nil) 
				return;
			
			//InterfacePtr<ITextControlData>clearDownloadImagePathTextControlData(iOptnsDownloadImagePathTextCntrlView, UseDefaultIID());
			//if(clearDownloadImagePathTextControlData==nil)
			//	break;
					
			if(!iOptnsImagePathTextCntrlView->IsVisible())
				iOptnsImagePathTextCntrlView->ShowView();
			
			if(iOptnsDownloadImagePathTextCntrlView->IsVisible())
			{
				
				//clearDownloadImagePathTextControlData->SetString("");
				iOptnsDownloadImagePathTextCntrlView->HideView();
			}
			
			if(!iImgPathButtonCntrlView->IsVisible())
				iImgPathButtonCntrlView->ShowView();
			
			if(iDownloadImgPathButtonCntrlView->IsVisible())
				iDownloadImgPathButtonCntrlView->HideView();			
		}
		
		else if(theSelectedWidget==kOptnsDownloadImagesToWidgetID && theChange == kTrueStateMessage)
		{
			IControlView* iOptnsImagePathTextCntrlView=panelControlData->FindWidget(kImagePathStaticTextWidgetID);
			if(iOptnsImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			IControlView* iOptnsDownloadImagePathTextCntrlView=panelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
			if(iOptnsDownloadImagePathTextCntrlView==nil) 
			{
				//CA("iPropDownloadImagePathTextCntrlView==nil");
				return;
			}
			IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImagePathButtonWidgetID);
			if(iImgPathButtonCntrlView==nil) 
				return;
			IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kDownloadImagePathButtonWidgetID);
			if(iDownloadImgPathButtonCntrlView==nil) 
				return;
			//InterfacePtr<ITextControlData>clearImagePathTextControlData(iOptnsImagePathTextCntrlView, UseDefaultIID());
			//if(clearImagePathTextControlData==nil)
			//	break;
					
			if(!iOptnsDownloadImagePathTextCntrlView->IsVisible())//iPropDownloadImagePathTextCntrlView
				iOptnsDownloadImagePathTextCntrlView->ShowView();
			
			if(iOptnsImagePathTextCntrlView->IsVisible())//iOptnsImagePathTextCntrlView
			{
				//clearImagePathTextControlData->SetString("");
				iOptnsImagePathTextCntrlView->HideView ();
			}
			
			if(!iDownloadImgPathButtonCntrlView->IsVisible())
				iDownloadImgPathButtonCntrlView->ShowView();
			
			if(iImgPathButtonCntrlView->IsVisible())
				iImgPathButtonCntrlView->HideView();
			
			
		}
		else if(theSelectedWidget == kOptnsOKButtonWidgetID && theChange == kTrueStateMessage)		
		{
			//CA("theSelectedWidget == kOptnsOKButtonWidgetID_11111");
			////added by Tushar on 26/04....missing flag related addition
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			}
			
			IControlView* iMissingChkboxCntrlView=panelControlData->FindWidget(kMissingFlagWidgetID);
			if(iMissingChkboxCntrlView==nil) 
				return;

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here
			
			InterfacePtr<ITriStateControlData> itristatecontroldataMissingFlag(iMissingChkboxCntrlView, UseDefaultIID());
			if(itristatecontroldataMissingFlag==nil) 
				return;

			IControlView* iOptnsMaptoRepositoryControlView=panelControlData->FindWidget(kOptnsMaptoRepositryWidgetID);
			if(iOptnsMaptoRepositoryControlView==nil) 
			{
				//CA("iPropMaptoRepositoryControlView==nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iOptnsMaptoRepositoryControlView, UseDefaultIID());
			if(itristatecontroldataMaptoRepository==nil)
			{
				//CA("itristatecontroldataMaptoRepository==nil");
				return;
			}

			IControlView* iOptnsDownloadImagestoControlView=panelControlData->FindWidget(kOptnsDownloadImagesToWidgetID);
			if(iOptnsDownloadImagestoControlView==nil) 
			{
				//CA("iPropDownloadImagestoControlView==nil");
				return;
			}	
			InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iOptnsDownloadImagestoControlView, UseDefaultIID());
			if(itristatecontroldataDownloadImagesto==nil)
			{
				//CA("itristatecontroldataDownloadImagesto==nil");
				return;
			}
			IControlView* iOptnsDownloadImagePathTextCntrlView=panelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
			if(iOptnsDownloadImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsDownloadImagePathTextCntrlView==nil");
				return;
			}
			InterfacePtr<ITextControlData>imagetextDownloadImage(iOptnsDownloadImagePathTextCntrlView, UseDefaultIID());
			if(imagetextDownloadImage==nil)
			{
				//CA("imagetextDownloadImage==nil");
				return;
			}

			IControlView* iOptnsImagePathTextCntrlView=panelControlData->FindWidget(kImagePathStaticTextWidgetID);
			if(iOptnsImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;
			}
			InterfacePtr<ITextControlData>imagetextMapToRepository(iOptnsImagePathTextCntrlView, UseDefaultIID());
			if(imagetextMapToRepository==nil)
			{
				//CA("imagetextMapToRepository==nil");
				return;
			}
			IControlView* iLoglevelControlView = panelControlData->FindWidget(kLogLevelDropDownWidgetID);
			if (iLoglevelControlView == nil)
				return;
			InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());
			if (iLoglevelDropDownListController == nil)
				return;

			ClientOptionsManip cops;
			PMString imagepath;
		
			
			InterfacePtr<ITextControlData> pubText(controlView , kPublicationStaticWidgetID);
			InterfacePtr<ITextControlData> languageText(controlView , kLocaleStaticWidgetID);
			OptionsValue opts = PersistData::getOptions();
			PMString name = PersistData::getPubName();
//CA("name  :  "+name);
			PMString pub_ID("");
			pub_ID.AppendNumber(PMReal(PersistData::getPubId()));
//CA(pub_ID);
			if(name.NumUTF16TextChars() > 0){
				//CAlert::InformationAlert("name.NumUTF16TextChars() > 0");
				opts.setPublicationName(name);
				opts.setProjectID(pub_ID);//-------
                
                double localeID = PersistData::getLocaleID();
                
				PMString languageIdStr("");
				languageIdStr.AppendNumber(PMReal(localeID));
				opts.setLocaleID(languageIdStr);
                
				PersistData::setOptions(opts);
			}
			if(pubText)
			{
				//CA("pubText");
				opts.setPublicationName(const_cast<PMString& >(pubText->GetString()));
			}
				
			//Added by nitin			
			if(itristatecontroldataMaptoRepository->IsSelected())
			{
				PMString images=imagetextMapToRepository->GetString();
				if(images == "")
				{
					CA("Please Select Map To Repository");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(images));
				iOptnsDownloadImagePathTextCntrlView->ShowView();
				imagetextDownloadImage->SetString("");
				iOptnsDownloadImagePathTextCntrlView->HideView();
			}
			else //if(iOptnsDownloadImagePathTextCntrlView->IsVisible())
			{
				PMString images=imagetextDownloadImage->GetString();
				if(images == "")
				{
					CA("Please Select Download Images To");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(images));
				iOptnsImagePathTextCntrlView->ShowView();
				imagetextMapToRepository->SetString("");
				iOptnsImagePathTextCntrlView->HideView();
			}
			
			if(languageText)
				opts.setDefaultLocale(const_cast<PMString& >(languageText->GetString()));
			
			opts.setLogLevel (iLoglevelDropDownListController->GetSelected());
		
			
			if(itristatecontroldataMaptoRepository->IsSelected())
				opts.setAssetStatus(1);
			else
				opts.setAssetStatus(0);
//21-feb
			//Added by nitin
			bool16 status = itristatecontroldataMissingFlag->IsSelected();
			if(status == kTrue)
			{
				opts.setMissingFlag(1);			
			}
			else
			{
				opts.setMissingFlag(0);				
			}
			//upto here
			cops.WriteClientOptions(/*PersistData::getOptions()*/opts);
			//CAlert::InformationAlert("After Set");
			if(theSelectedWidget == kOptnsOKButtonWidgetID){
				closeOptnsDialog();
			}
		}
		else if (theSelectedWidget == kApplyButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box
			// writing optings into file
			//CAlert::InformationAlert("Inside Apply Button");
			AcquireWaitCursor awc;
			awc.Animate();
		
            #ifdef WINDOWS
			Sleep(1000);    //CS4   UnComment Later Cs4*******
            #else
			for(int32 j=0; j <100; j++);
            #endif

			InterfacePtr<ITextControlData> pubText(controlView , kPublicationStaticWidgetID);		
			InterfacePtr<ITextControlData> languageText(controlView , kLocaleStaticWidgetID);
			// added by avinash
			InterfacePtr<ITextControlData> messageText(controlView , kOptnsSettingsSavedStaticTextWidgetID);
			// upto here
			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			IControlView* iOptnsMaptoRepositoryControlView=panelControlData->FindWidget(kOptnsMaptoRepositryWidgetID);
			if(iOptnsMaptoRepositoryControlView==nil) 
			{
				//CA("iPropMaptoRepositoryControlView==nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iOptnsMaptoRepositoryControlView, UseDefaultIID());
			if(itristatecontroldataMaptoRepository==nil)
			{
				//CA("itristatecontroldataMaptoRepository==nil");
				return;
			}

			IControlView* iOptnsDownloadImagestoControlView=panelControlData->FindWidget(kOptnsDownloadImagesToWidgetID);
			if(iOptnsDownloadImagestoControlView==nil) 
			{
				//CA("iPropDownloadImagestoControlView==nil");
				return;
			}	
			InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iOptnsDownloadImagestoControlView, UseDefaultIID());
			if(itristatecontroldataDownloadImagesto==nil)
			{
				//CA("itristatecontroldataDownloadImagesto==nil");
				return;
			}
			IControlView* iOptnsDownloadImagePathTextCntrlView=panelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
			if(iOptnsDownloadImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsDownloadImagePathTextCntrlView==nil");
				return;
			}
			InterfacePtr<ITextControlData>imagetextDownloadImage(iOptnsDownloadImagePathTextCntrlView, UseDefaultIID());
			if(imagetextDownloadImage==nil)
			{
				//CA("imagetextDownloadImage==nil");
				return;
			}

			IControlView* iOptnsImagePathTextCntrlView=panelControlData->FindWidget(kImagePathStaticTextWidgetID);
			if(iOptnsImagePathTextCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;
			}
			InterfacePtr<ITextControlData>imagetextMapToRepository(iOptnsImagePathTextCntrlView, UseDefaultIID());
			if(imagetextMapToRepository==nil)
			{
				//CA("imagetextMapToRepository==nil");
				return;
			}
			IControlView* iLoglevelControlView = panelControlData->FindWidget(kLogLevelDropDownWidgetID);
			if (iLoglevelControlView == nil)
				return;
			InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());
			if (iLoglevelDropDownListController == nil)
				return;


			ClientOptionsManip cops;
			OptionsValue opts = PersistData::getOptions();
			PMString name = PersistData::getPubName();
			
//CA("name  :  "+name);
			PMString pub_ID("");
			pub_ID.AppendNumber(PMReal(PersistData::getPubId()));
			if(name.NumUTF16TextChars() > 0)
			{
				//CA("name.NumUTF16TextChars() > 0");
				opts.setPublicationName(name);
				opts.setProjectID(pub_ID);	//--------

				double localeID = PersistData::getLocaleID();

				PMString languageIdStr("");
				languageIdStr.AppendNumber(PMReal(localeID));
				opts.setLocaleID(languageIdStr);
				
				PersistData::setOptions(opts);


			}		
			if(pubText)
				opts.setPublicationName(const_cast<PMString& >(pubText->GetString()));
			
			//added by nitin
			if(itristatecontroldataMaptoRepository->IsSelected())
			{
				// added by avinash
				InterfacePtr<ITextControlData> textControlData1(iControlView,UseDefaultIID());
				if (textControlData == nil){
					CAlert::InformationAlert("ITextControlData nil");	
					break;
				}

				textControlData1->SetString("");			
				// upto here

				PMString images=imagetextMapToRepository->GetString();
				if(images == "")
				{
					CA("Please Select Map To Repository");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(imagetextMapToRepository->GetString()));
				iOptnsDownloadImagePathTextCntrlView->ShowView();
				imagetextDownloadImage->SetString("");
				iOptnsDownloadImagePathTextCntrlView->HideView();
			}
			else 
			{
				// added by avinash
				InterfacePtr<ITextControlData> textControlData1(iControlView,UseDefaultIID());
				if (textControlData == nil){
					CAlert::InformationAlert("ITextControlData nil");	
					break;
				}

				textControlData1->SetString("");			
				// upto here
				PMString images=imagetextDownloadImage->GetString();
				if(images == "")
				{
					CA("Please Select Download Images To");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(imagetextDownloadImage->GetString()));
				iOptnsImagePathTextCntrlView->ShowView();
				imagetextMapToRepository->SetString("");
				iOptnsImagePathTextCntrlView->HideView();
			}
		
		
			if(languageText){
				opts.setDefaultLocale(const_cast<PMString& >(languageText->GetString()));
				}

		
			opts.setLogLevel(iLoglevelDropDownListController->GetSelected());
			
			if(itristatecontroldataMaptoRepository->IsSelected())
				opts.setAssetStatus(1);
			else
				opts.setAssetStatus(0);

			//-------
			IControlView* iShowObjectCountCntrlView=panelControlData->FindWidget(kShowObjectCountWidgetID);
			if(iShowObjectCountCntrlView==nil)
			{	//CA("iShowObjectCountCntrlView nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristateShowObjectCount(iShowObjectCountCntrlView, UseDefaultIID());
			if(itristateShowObjectCount==nil) 
			{	
				//CA("itristateShowObjectCount nil");
				return;
			}

			if(itristateShowObjectCount->IsSelected())
			{				
				opts.setShowObjCountFlag(1);
			}
			else
			{
				opts.setShowObjCountFlag(0);
			}

			IControlView* iByPassPromptCntrlView=panelControlData->FindWidget(kByPassPromptWidgetID);
			if(iByPassPromptCntrlView==nil)
			{	//CA("iByPassPromptCntrlView nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristateByPassPrompt(iByPassPromptCntrlView, UseDefaultIID());
			if(itristateByPassPrompt==nil) 
			{	
				//CA("itristateByPassPrompt nil");
				return;
			}

			if(itristateByPassPrompt->IsSelected())
			{
				opts.setByPassForSingleSelectionSprayFlag(1);
			}
			else
			{

				opts.setByPassForSingleSelectionSprayFlag(2);
			}


			IControlView* iControlViewForHorizontalSpacing = panelControlData->FindWidget(kOptHorizontalSpacingWidgetID);
			if(iControlViewForHorizontalSpacing == nil){
				//CAlert::InformationAlert("Fail to find kOptHorizontalSpacingWidgetID static text widget");
				break;
			}

			InterfacePtr<ITextControlData> HorizontalSpacingTextControlData(iControlViewForHorizontalSpacing,UseDefaultIID());
			if (HorizontalSpacingTextControlData == nil)
			{
				CAlert::InformationAlert("HorizontalSpacingTextControlData nil");	
				break;
			}
			PMString value;
			value = HorizontalSpacingTextControlData->GetString();
			if(value != "")
			{
				opts.setHorizontalSpacing(value.GetAsNumber());
			}
			else
				opts.setHorizontalSpacing(5);

			IControlView* iControlViewForVerticalSpacing = panelControlData->FindWidget(kOptVerticalSpacingWidgetID);
			if(iControlViewForVerticalSpacing == nil){
				//CAlert::InformationAlert("Fail to find kOptVerticalSpacingWidgetID static text widget");
				break;
			}

			InterfacePtr<ITextControlData> VerticalSpacingTextControlData(iControlViewForVerticalSpacing,UseDefaultIID());
			if (VerticalSpacingTextControlData == nil)
			{
				CAlert::InformationAlert("VerticalSpacingTextControlData nil");	
				break;
			}
			value.Clear();
			value = VerticalSpacingTextControlData->GetString();
			if(value != "")
			{
				opts.setVerticalSpacing(value.GetAsNumber());
			}
			else
				opts.setVerticalSpacing(5);	


			cops.WriteClientOptions(/*PersistData::getOptions()*/opts);

			//CA("Settings saved successfully!");
			
			//IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			//if(iControlView == nil){
			//	//CAlert::InformationAlert("Fail to find publication static text widget");
			//	break;
			//}	

			InterfacePtr<ITextControlData> textControlData1(iControlView,UseDefaultIID());
			if (textControlData == nil){
				CAlert::InformationAlert("ITextControlData nil");	
				break;
			}
			PMString ASD("Your settings are saved successfully!");
			ASD.SetTranslatable(kFalse);
			textControlData1->SetString(ASD);			
			// upto here

		}	
		else if((theSelectedWidget == kOptsCancelButtonWidgetID ||  theSelectedWidget == kOptsCancelButtonWidgetID) && theChange == kTrueStateMessage)
		{	
			if(theSelectedWidget == kOptsCancelButtonWidgetID){
				closeOptnsDialog();	
			}
		}
		else if(theSelectedWidget == kOKButtonWidgetID){
			//// Written just to avoid crash after hitting Enter key
		}
		else if (theSelectedWidget == kUploadButtonWidgetID && theChange == kTrueStateMessage)
		{

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			//CA("kUploadButtonWidgetID");
			//Apsiva 9 Comment
	//		InterfacePtr<IExportTemplate> iExportTemplate(static_cast<IExportTemplate*> (CreateObject(kExportTemplateBoss,IID_IEXPORTTEMPLATE)));
	//		if(iExportTemplate)
	//			iExportTemplate->openExportTemplateDialog();							


		}
		else if(theSelectedWidget == kResetButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kResetButtonWidgetID");
			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
			if(ptrLogInHelper)
			{
				ptrLogInHelper->callfireResetPlugins();
			}
		}
		if(theSelectedWidget==kShowObjectCountWidgetID && (theChange == kTrueStateMessage || theChange == kFalseStateMessage))
		{
			//CA("Show Object");
			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here
		}
		if(theSelectedWidget==kMissingFlagWidgetID && (theChange == kTrueStateMessage || theChange == kFalseStateMessage))
		{
			//CA("Show Object");
			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here
		}

		if(theSelectedWidget==kLogLevelDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("Dropp Down change");
			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here					
		}


		//if(theSelectedWidget==kLogLevelDropDownWidgetID && theChange == kTrueStateMessage)
		//{
			//CA("Dropp Down change");
			////added by avinash
			//IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			//if(iControlView == nil){
			//	//CAlert::InformationAlert("Fail to find publication static text widget");
			//	return;
			//}	

			//InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			//if (textControlData == nil){
			//	//CAlert::InformationAlert("ITextControlData nil");	
			//	return;
			//}

			//textControlData->SetString("");			
			//// upto here					
		//}
		
		//
		// commented 23 Feb Vaibhav
		/*else if(theSelectedWidget==kDocPathButtonWidgetID && theChange == kTrueStateMessage)
		{
			PMString infoMsg("Browse for Document Download Path");
			PMString path("");
			OptionsValue optnsObj = PersistData::getOptions();
			path = optnsObj.getIndesignDocPath();
			ShowBrowseDialog(path,infoMsg);			
			setControlText(kDocpathStaticTextWidgetID,path);
			optnsObj.setIndesignDocPath(path);
			PersistData::setOptions(optnsObj);			
		}		*/
	
		if(theSelectedWidget==kByPassPromptWidgetID && (theChange == kTrueStateMessage || theChange == kFalseStateMessage))
		{
			//CA("isnide update when the slected widget id is kByPassPromptWidgetID");		

			IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}
			
			textControlData->SetString("");	

			
			// upto here
		}
	} while (false);
}

void GeneralPanelObserver::setDefaultValuesOfGeneralTab(IPanelControlData* panelControlData)
{
	//CA("GeneralPanelObserver::setDefaultValuesOfGeneralTab");
	PMString filePath("");
	OptionsValue defClientOptions;
//21-feb
	//InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//if (panelControlData == nil){			
	//	CAlert::InformationAlert("AutoAttach panelControlData nil");
	//	return;
	//}

	IControlView* iLoglevelControlView = panelControlData->FindWidget(kLogLevelDropDownWidgetID);
	if (iLoglevelControlView == nil)
		return;

	//added by avinash
	IControlView* iControlView = panelControlData->FindWidget(kOptnsSettingsSavedStaticTextWidgetID);
	if(iControlView == nil){
		//CAlert::InformationAlert("Fail to find publication static text widget");
		return;
	}	

	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	if (textControlData == nil){
		//CAlert::InformationAlert("ITextControlData nil");	
		return;
	}

	textControlData->SetString("");			
	// upto here

	InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());
	if (iLoglevelDropDownListController == nil)
		return;
	//commented by nitin
	//IControlView* iChkboxCntrlView=panelControlData->FindWidget(kSameAsAssetWidgetID);
	//if(iChkboxCntrlView==nil) 
	//	return;

	//InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
	//if(itristatecontroldata==nil) 
	//	return;

	//Added by nitin
	IControlView* iOptnsMaptoRepositoryControlView=panelControlData->FindWidget(kOptnsMaptoRepositryWidgetID);
	if(iOptnsMaptoRepositoryControlView==nil) 
	{
		//CA("iPropMaptoRepositoryControlView==nil");
		return;
	}
	IControlView* iOptnsDownloadImagestoControlView=panelControlData->FindWidget(kOptnsDownloadImagesToWidgetID);
	if(iOptnsDownloadImagestoControlView==nil) 
	{
		//CA("iPropDownloadImagestoControlView==nil");
		return;
	}
	
	
	InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iOptnsMaptoRepositoryControlView, UseDefaultIID());
	if(itristatecontroldataMaptoRepository==nil)
	{
		//CA("itristatecontroldataMaptoRepository==nil");
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iOptnsDownloadImagestoControlView, UseDefaultIID());
	if(itristatecontroldataDownloadImagesto==nil)
	{
		//CA("itristatecontroldataDownloadImagesto==nil");
		return;
	}
	IControlView* iOptnsDownloadImagePathTextCntrlView=panelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
	if(iOptnsDownloadImagePathTextCntrlView==nil) 
	{
		//CA("iPropDownloadImagePathTextCntrlView==nil");
		return;
	}
	IControlView* iOptnsImagePathTextCntrlView=panelControlData->FindWidget(kImagePathStaticTextWidgetID);
	if(iOptnsImagePathTextCntrlView==nil) 
	{
		//CA("iOptnsImagePathTextCntrlView==nil");
		return;
	}
	InterfacePtr<ITextControlData>clearDownloadImagePathTextControlData(iOptnsDownloadImagePathTextCntrlView, UseDefaultIID());
	if(clearDownloadImagePathTextControlData==nil)
		return;
	InterfacePtr<ITextControlData>clearImagePathTextControlData(iOptnsImagePathTextCntrlView, UseDefaultIID());
	if(clearImagePathTextControlData==nil)
		return;
			

	//upto here

	//21-feb
	//nitin
	//CA(folderPath);
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return;
		}
		//PMString folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();
		//int lastpos = 0;
		//for (int i = 0 ; i< folderPath.CharCount();i++)
		//{		
		//	if ((folderPath[i] == MACDELCHAR) || (folderPath[i] == UNIXDELCHAR) || (folderPath[i] == WINDELCHAR))
		//					lastpos = i;
		//}
		//// At this point lastpos should point to the last delimeter, knock off the rest of the string.
		//folderPath.Truncate(folderPath.CharCount()-lastpos);
		//CA(folderPath);

/////////////////////////
	InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return  ;
	}

	//------------
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(!ptrIClientOptions)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::No ptrIClientOptions ");					
		return ;
	}

	double lang_id = -1 ;
	PMString lang_name("");

	lang_id = ptrIClientOptions->getDefaultLocale(lang_name);
	if(lang_id == -1)
		return;
	
	PMString defPubName("");
	double defPubId=-1;
	defPubId = ptrIClientOptions->getDefPublication(defPubName);
	if(defPubId <= 0)
	{
		//CA(" defPubId <= 0 ");
		return ;
	}
	
	LoginInfoValue cserverInfoValue;
	/*PMString ASD("");*/

	PMString localename("");
	PMString CatalogName("");
	double CurrentLocID=-1;
	bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);



	if(result)
	{
		
		double clientID = ptrIAppFramework->getClientID();
		PMString ClientNo("");
		ClientNo.AppendNumber(PMReal(clientID));
		ClientInfoValue currentClientInfoValueObj =  cserverInfoValue.getClientInfoByClientNo(ClientNo);
		//-------
		IControlView* iShowObjectCountCntrlView=panelControlData->FindWidget(kShowObjectCountWidgetID);
		if(iShowObjectCountCntrlView==nil)
		{	//CA("iShowObjectCountCntrlView nil");
			return;
		}
		InterfacePtr<ITriStateControlData> itristateShowObjectCount(iShowObjectCountCntrlView, UseDefaultIID());
		if(itristateShowObjectCount==nil) 
		{	
			//CA("itristateShowObjectCount nil");
			return;
		}

		if(currentClientInfoValueObj.getShowObjectCountFlag())
			itristateShowObjectCount->Select() ;
		else
			itristateShowObjectCount->Deselect() ;


		IControlView* iByPassPromptCntrlView=panelControlData->FindWidget(kByPassPromptWidgetID);
		if(iByPassPromptCntrlView==nil)
		{	//CA("iByPassPromptCntrlView nil");
			return;
		}
		InterfacePtr<ITriStateControlData> itristateByPassPrompt(iByPassPromptCntrlView, UseDefaultIID());
		if(itristateByPassPrompt==nil) 
		{	
			//CA("itristateByPassPrompt nil");
			return;
		}

		if(currentClientInfoValueObj.getByPassForSingleSelectionSprayFlag() == 0 || currentClientInfoValueObj.getByPassForSingleSelectionSprayFlag() == 1)
			itristateByPassPrompt->Select();
		else
			itristateByPassPrompt->Deselect();

		

		IControlView* iControlViewForVerticalSpacing = panelControlData->FindWidget(kOptVerticalSpacingWidgetID);
		if(iControlViewForVerticalSpacing == nil){
			//CAlert::InformationAlert("Fail to find publication static text widget");
			return;
		}

		InterfacePtr<ITextControlData> textControlDataVerticalSpacing(iControlViewForVerticalSpacing,UseDefaultIID());
		if (textControlDataVerticalSpacing == nil)
		{
			CAlert::InformationAlert("ITextControlData nil");	
			return;
		}
		PMString value("");
		value.AppendNumber(currentClientInfoValueObj.getVerticalSpacing());
		if(currentClientInfoValueObj.getVerticalSpacing() == 0)
		{
			value = "5";
			textControlDataVerticalSpacing->SetString(value);
		}
		else
			textControlDataVerticalSpacing->SetString(value);


		IControlView* iControlViewForHorizontalSpacing = panelControlData->FindWidget(kOptHorizontalSpacingWidgetID);
		if(iControlViewForHorizontalSpacing == nil){
			//CAlert::InformationAlert("Fail to find publication static text widget");
			return;
		}

		InterfacePtr<ITextControlData> textControlDataHorizontalSpacing(iControlViewForHorizontalSpacing,UseDefaultIID());
		if (textControlDataHorizontalSpacing == nil)
		{
			CAlert::InformationAlert("ITextControlData nil");	
			return;
		}
		value.Clear();
		value.AppendNumber(currentClientInfoValueObj.getHorizontalSpacing());

		if(currentClientInfoValueObj.getHorizontalSpacing() == 0)
		{
			value = "5";
			textControlDataHorizontalSpacing->SetString(value);
		}
		else
			textControlDataHorizontalSpacing->SetString(value);
		//textControlDataHorizontalSpacing->SetString(value);










		if((currentClientInfoValueObj.getLanguage()).NumUTF16TextChars() != 0)
		{
			//CA("1");
			VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
			if(VectorLocValPtr == NULL)
			{
				//CAlert::InformationAlert("VectorLocValPtr == NULL");
				ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::VectorLocValPtr == NULL ");
				return;
			}
						
			VectorLanguageModel::iterator it;
			
			for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
			{		
				//CA("LNG NAME : "+it->getLangugeName()); 
				double LocID = it->getLanguageID();
				CurrentLocID = (currentClientInfoValueObj.getLanguage()).GetAsDouble();

				if(LocID == CurrentLocID )
				{//CA("LocID == CurrentLocID");
					localename.Clear();
					localename.Append(it->getLangugeName());
					break;
				}
			}
//12-april
			//if(VectorLocValPtr)
				//delete VectorLocValPtr;
//12-april
		}
		//CA("defPubName   :  "+defPubName);
		if((currentClientInfoValueObj.getProject()).NumUTF16TextChars() != 0)
		{
			//VectorPubModelPtr vec_pub_model = NULL;
			//VectorClassInfoPtr vect = NULL;
			//CA("2");
			//if(ptrIAppFramework->get_isONEsourceMode())
			//{
			//	CA("ONE");
			//	vect = ptrIAppFramework->ClassificationTree_getAllChildren(defPubId,lang_id);
			//	if(!vect)
			//		return;
			//	int32 vectSize =static_cast<int32>(vect->size());
			//	PMString d("vectSize  :  ");
			//	d.AppendNumber(vectSize);
			//	CA(d);

			//}
			//else
			//{
			//	CA("!!!ONE");
			//	vec_pub_model = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(defPubId , lang_id);
			//	if(vec_pub_model == nil)
			//	{
			//		CA("vec_pub_model == nil");
			//		return;
			//	}
			//	if(vec_pub_model->size() <= 0)
			//		return ;
			//	PMString s("vec_pub_model->size()  :  ");				
			//	s.AppendNumber(static_cast<int32>(vec_pub_model->size()));
			//	CA(s);
			//}
//CA("cserverInfoValue.getProject()   ::   "+cserverInfoValue.getProject());
			int flag=0;
			
			
			//CClassificationValue classValueObj = ptrIAppFramework->ClassificationTree_getCClassificationValueByClassId(defPubId,lang_id);
			//int32 class_id = classValueObj.getClass_id();

			//if(class_id == defPubId)
			//{
			//	//CA("class_id == defPubId");
			//	flag = 1;
			//	PMString TempBuffer = classValueObj.getClassification_name();
			//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//	if(iConverter)
			//	{
			//		CatalogName=iConverter->translateString(TempBuffer);
			//	}
			//	else
			//		CatalogName = TempBuffer;

			//	ptrIAppFramework->set_isONEsourceMode(kTrue);

			//}


			if(flag != 1)
			{
				//CA("3");
				
				//VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllChildren(defPubId,lang_id);
				//VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->PUBMgr_getAllProjects((cserverInfoValue.getLanguage()).GetAsNumber());	//---------
				CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(defPubId,lang_id);
								
				//int32 publicatID = pubModelRootValue.getEventId();
				double pubRootID = pubModelRootValue.getRootID();
				
				CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_id);
				CatalogName.Clear();
				PMString TempBuffer = pubModelValue.getName();
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(iConverter)
				{
					CatalogName=iConverter->translateString(TempBuffer);
				}
				else
					CatalogName = TempBuffer;

				ptrIAppFramework->set_isONEsourceMode(kFalse);
//PMString w("publicatID  :  ");				
//w.AppendNumber(publicatID);
//w.Append("\npubModelValue.getName()  :  ");
//w.Append(pubModelValue.getName());
//CA(w);

//				VectorPubModel::iterator it;
//				
//				for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
//				{	
//					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//					
//					int32 ProjID = it->getEventId();
//					int32 CurrProjID = (cserverInfoValue.getProject()).GetAsNumber();
//					if(ProjID == CurrProjID )
//					{
//						CA("3-ProjID == CurrProjID");
//						CatalogName.Clear();
//						PMString TempBuffer = it->getName();
//						if(iConverter){ 
//							CatalogName=iConverter->translateString(TempBuffer);
//						}
//						else				
//							CatalogName = TempBuffer;
//						
//						ptrIAppFramework->set_isONEsourceMode(kFalse);
//						break;
//					}
//				}
////12-april
//				if(VectorPublInfoValuePtr)
//					delete VectorPublInfoValuePtr;
//12-april
			}
		}
		//CA("lng  =  "+localename);
		//CA("pub  =  "+CatalogName);
		//CA(cserverInfoValue.getImagePath());


		if(ptrIAppFramework->get_isONEsourceMode())
		{
			CatalogName.clear();
			//CA("ptrIAppFramework->get_isONEsourceMode()");
			VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_id); 
			if(vectClsValuePtr == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::ectClsValuePtr == nil ");
				return ;
			}
			VectorClassInfoValue::iterator itr;
			itr = vectClsValuePtr->begin();
			CatalogName.Append(itr->getClassification_name());

		}

	
		PersistData::setLocaleName(localename);
		PersistData::setPubName(CatalogName);

		setControlText(kLocaleStaticWidgetID , localename/*cserverInfoValue.getLanguage()*/);
		setControlText(kPublicationStaticWidgetID , CatalogName/*cserverInfoValue.getProject()*/);
	
		setControlText(kCatalogPathStaticTextWidgetID ,"" /*folderPath*/ ); 
		setControlText(kOptnsClientEnvNameStaticTextWidgetID , cserverInfoValue.getEnvName()); // added by nitin
		setControlText(kOptnsClientServerUrlStaticTextWidgetID , cserverInfoValue.getCmurl()); // added by nitin
		
		iLoglevelDropDownListController->Select(currentClientInfoValueObj.getLogLevel());
		//Commented by nitin
		//if(cserverInfoValue.getAssetStatus())
		//	itristatecontroldata->Select();
		//else
		//	itristatecontroldata->Deselect();
		//added by nitin
		if(currentClientInfoValueObj.getAssetStatus())
		{
			itristatecontroldataMaptoRepository->Select();
			setControlText(kImagePathStaticTextWidgetID , currentClientInfoValueObj.getAssetserverpath());
			clearDownloadImagePathTextControlData->SetString("");
			
		}
		else
		{
			itristatecontroldataDownloadImagesto->Select();
			setControlText(kDownloadImagePathStaticTextWidgetID , currentClientInfoValueObj.getAssetserverpath());
			clearImagePathTextControlData->SetString("");
		}
	}
	
	do
	{
//		ClientOptionsManip cops;
//		bool8 isFileExists=FALSE;
//		filePath = cops.GetOptionsFilePath();
//		if(filePath.NumUTF16TextChars()<=1)
//		{
//			CAlert::InformationAlert("File path nil");
//			break;
//		}
//		isFileExists=cops.IsClientOptionsFileExiats();
//		if(isFileExists==FALSE){
//			if(cops.CreateOptionsFile(filePath)==FALSE)
//			{
//				CAlert::InformationAlert("Faile to Create File");
//				break;
//			}
//		}		
//		defClientOptions = cops.ReadClientOptions();
//		/////////////////////////////////////////		
//		// if publication does't exists
//		PMString Lstr = defClientOptions.getDefaultLocale();
//		if(Lstr.NumUTF16TextChars() == 0){
//			setControlText(kLocaleStaticWidgetID,PMString(""));
//		}
//		else if(cops.checkforLocaleExistance(Lstr) == -1)
//		{	
//			PMString message("The current Locale \"");
//			message += Lstr;
//			message += "\" does not exist. Please select another Locale.  ";
//			PMString ok_btn("OK");			
//			CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);			
//			defClientOptions.setDefaultLocale(PMString(""));
//			setControlText(kLocaleStaticWidgetID,PMString(""));
//			PersistData::setOptions(defClientOptions);
//			cops.WriteClientOptions(defClientOptions);
//		}
//		else{
//			setControlText(kLocaleStaticWidgetID,defClientOptions.getDefaultLocale());
//		}
//		int32 LocaleID = cops.checkforLocaleExistance(defClientOptions.getDefaultLocale());
//		ptrIAppFramework->setLocale_ID(LocaleID);
//		PMString str = defClientOptions.getPublicationName();
//
//		if(str.NumUTF16TextChars() == 0){
//			setControlText(kPublicationStaticWidgetID,PMString(""));
//		}
//		else if(cops.checkforPublicationExistance(str) == -1){
////CA("Premal 2");
//			PMString message("The current catalog \"");
//			message += str;
//			message += "\" does not exist. Please select another catalog.  ";
//			PMString ok_btn("OK");			
//			CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);			
//			defClientOptions.setPublicationName(PMString(""));
//			setControlText(kPublicationStaticWidgetID,PMString(""));
//			PersistData::setOptions(defClientOptions);
//			cops.WriteClientOptions(defClientOptions);
//		}
//		else{
////CA("Premal 3");
//			setControlText(kPublicationStaticWidgetID,defClientOptions.getPublicationName());
//		}
//
//		int32 CatalogID = cops.checkforPublicationExistance(defClientOptions.getPublicationName());
//		ptrIAppFramework->setCatalog_ID(CatalogID);
//		setControlText(kImagePathStaticTextWidgetID,defClientOptions.getImagePath());
//		setControlText(kDocpathStaticTextWidgetID,defClientOptions.getIndesignDocPath());
		/////////////////////////////////////////

		// Enble/Disable the Publication Browse button with Login Status
		IControlView* controlView = panelControlData->FindWidget(kPunNameButtonWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("Publication Browse Button ControlView nil");
			break;
		}

		IControlView* controlView1 = panelControlData->FindWidget(kLocaleButtonWidgetID);
		if (controlView1 == nil){
			CAlert::InformationAlert("Publication Browse Button ControlView nil");
			break;
		}

		/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Fail to create AppFramework Object.");
			break;
		}*/
		
        //bool16 status = ptrIAppFramework->getLoginStatus(Indesign);		 commented by vaibhav
		bool16 status = ptrIAppFramework->getLoginStatus();		
		if(status==TRUE){			
			controlView->Enable(kTrue);
			controlView1->Enable(kTrue);
		}
		else{
			controlView->Disable(kTrue);
			controlView1->Disable(kTrue);
		}
	}	
	while(0);
	//if(ptrIAppFramework !=nil)  commented by vaibhav
	//		delete ptrIAppFramework;
	PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
	//CA("Out");
}

void GeneralPanelObserver::ShowBrowseDialog(PMString& path,PMString& infoMsg)
{
	//CA("GeneralPanelObserver::ShowBrowseDialog");
	do {
		//CA("GeneralPanelObserver::ShowBrowseDialog");
		
    	SysFileList	fileList;
    	InterfacePtr</*ISelectFolderDialog*/IOpenFileDialog>	openDlg(::CreateObject2</*ISelectFolderDialog*/IOpenFileDialog>(kSelectFolderDialogBoss)); //Cs4
    	if(!openDlg)
    	{
    		CAlert::InformationAlert("openDlg...");	
    		break;
    	}
    	
    	infoMsg.SetTranslatable(kFalse);
		IDFile	fPath;	
		if (SDKUtilities::AbsolutePathToSysFile(path, fPath) != kSuccess){
			CAlert::InformationAlert("Can not convert to SysFile format...");
			break;
		}
    	if (openDlg->DoDialog(&fPath, fileList, kFalse, nil, &infoMsg))
    	{   
		
    		IDFile	folderPath = *fileList.GetNthFile(0);
			
    		PMString	folderName;    		
     		//folderPath.SetTranslatable(kFalse);
            #ifdef WINDOWS
            folderName = folderPath.GetString();      //Cs4 Please Un Comment
            #else
            SDKUtilities::GetPathName(folderPath,folderName);
            #endif
			if(folderName.NumUTF16TextChars()>0){				
				path.clear();  // Depricated In cs4 /*Clear()*/  Instead use clear()
				path.SetString(folderName);
				path.Append("\\");
                path.ParseForEmbeddedCharacters();
			}			
    	}
		//CA("at the end of method GeneralPanelObserver::ShowBrowseDialog");				

	} while(false);	
}

void GeneralPanelObserver::setControlText(WidgetID widgetID,PMString txt)
{
	do{
		//CA("GeneralPanelObserver::setControlText");
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView nil");
			break;
		}	
		InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
		if (textControlData == nil){
			CAlert::InformationAlert("ITextControlData nil");	
			break;
		}
		txt.SetTranslatable(kFalse);
        txt.ParseForEmbeddedCharacters();
		textControlData->SetString(txt);
	}
	while(0);
}

void GeneralPanelObserver::ReadClientOptions(void)
{
	//CA("GeneralPanelObserver::ReadClientOptions");
	PMString filePath("");
	OptionsValue optnsObj;

	do{
		ClientOptionsManip optnsUtilsObj;
		bool8 isFileExists=FALSE;
		filePath = optnsUtilsObj.GetOptionsFilePath();
		if(filePath.NumUTF16TextChars()<=1){
			CAlert::InformationAlert("File path nil");
			break;
		}		
		/*isFileExists=optnsUtilsObj.IsClientOptionsFileExiats();
		if(isFileExists==FALSE){			
			if(optnsUtilsObj.CreateOptionsFile(filePath)==FALSE){
				CAlert::InformationAlert("Faile to Create File");
				break;
			}
		}*/
		optnsObj = optnsUtilsObj.ReadClientOptions();
		//CA("General PanelObserver here");
		PersistData::setOptions(optnsObj);
		//CA("optnsObj.PublicationName   ==   "+optnsObj.getPublicationName());
		
	}while(0);
} 

void GeneralPanelObserver::closeOptnsDialog(void)
{
	InterfacePtr<IWidgetParent> parentHolder(this, IID_IWIDGETPARENT);
	if(parentHolder==nil){
		CAlert::InformationAlert("parentHolder nil");
		return;
	}
	InterfacePtr<IDialog> dialog((IDialog*) parentHolder->QueryParentFor(IID_IDIALOG));
	if(dialog==nil){
		CAlert::InformationAlert("dialog nil");
		return;
	}
	dialog->Close();
}