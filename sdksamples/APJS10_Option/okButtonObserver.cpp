/*
//	File:	OkButtonObserver.cpp
//
//	Date:	6-Aug-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IDialog.h"

// Implementation includes:
#include "CDialogObserver.h"
#include "CAlert.h"
#include "ITextControlData.h"
//21-feb
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "ITriStateControlData.h"
//21-feb
// Project:
//#include "OptionsValue.h"
#include "OptionsUtils.h"
#include "OptionsStaticData.h"
#include "SDKUtilities.h"
#include "OptnsDlgID.h"
#include "ILoginHelper.h"

#define CA(X) CAlert::InformationAlert(X)

/** OkButtonObserver
	Allows dynamic processing of icon button widget state changes, in this case
	the tab dialog's info button. 

	Implements IObserver based on the partial implementation CObserver. 

	@author Lee Huang
*/
class OkButtonObserver : public CObserver
{
	public:	
		/**
			Constructor.
			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		OkButtonObserver(IPMUnknown* boss);

		/** Destructor. */
		virtual ~OkButtonObserver() {}

		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		/** 
			Called by the application to allow the observer to attach to the 
			subjects to be observed. In this case the tab dialog's info button widget.
		*/
        virtual void AutoAttach();

		/** 
			Called by the application to allow the observer to detach from the 
			subjects being observed. 
		*/
        virtual void AutoDetach();
		void closeOptnsDialog(void);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(OkButtonObserver, kOptnsTabOKBtnObserverImpl)

OkButtonObserver::OkButtonObserver(IPMUnknown* boss)
	 : CObserver(boss)
{
}

/*	Update
*/
void OkButtonObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	//CObserver::Update(theChange, theSubject, protocol, changedBy);

//21-feb
	IPanelControlData* iPanelControlData = PersistData::getGeneralPanelControlData();

	IControlView* iLoglevelControlView = iPanelControlData->FindWidget(kLogLevelDropDownWidgetID);
	if (iLoglevelControlView == nil)
		return;

	InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());
	if (iLoglevelDropDownListController == nil)
		return;
	//Commented by nitin
	//IControlView* iChkboxCntrlView=iPanelControlData->FindWidget(kSameAsAssetWidgetID);
	//if(iChkboxCntrlView==nil) 
	//	return;

	//InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
	//if(itristatecontroldata==nil) 
	//	return;
	//added by nitin
	IControlView* iOptnsMaptoRepositoryControlView=iPanelControlData->FindWidget(kOptnsMaptoRepositryWidgetID);
	if(iOptnsMaptoRepositoryControlView==nil) 
	{
		//CA("iPropMaptoRepositoryControlView==nil");
		return;
	}
	IControlView* iOptnsDownloadImagestoControlView=iPanelControlData->FindWidget(kOptnsDownloadImagesToWidgetID);
	if(iOptnsDownloadImagestoControlView==nil) 
	{
		//CA("iPropDownloadImagestoControlView==nil");
		return;
	}	
	InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iOptnsMaptoRepositoryControlView, UseDefaultIID());
	if(itristatecontroldataMaptoRepository==nil)
	{
		//CA("itristatecontroldataMaptoRepository==nil");
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iOptnsDownloadImagestoControlView, UseDefaultIID());
	if(itristatecontroldataDownloadImagesto==nil)
	{
		//CA("itristatecontroldataDownloadImagesto==nil");
		return;
	}
	IControlView* iOptnsDownloadImagePathTextCntrlView=iPanelControlData->FindWidget(kDownloadImagePathStaticTextWidgetID);
	if(iOptnsDownloadImagePathTextCntrlView==nil) 
	{
		//CA("iOptnsDownloadImagePathTextCntrlView==nil");
		return;
	}
	IControlView* iOptnsImagePathTextCntrlView=iPanelControlData->FindWidget(kImagePathStaticTextWidgetID);
	if(iOptnsImagePathTextCntrlView==nil) 
	{
		//CA("iOptnsImagePathTextCntrlView==nil");
		return;
	}

	InterfacePtr<ITextControlData>imagetextMapToRepository(iOptnsImagePathTextCntrlView, UseDefaultIID());
	if(imagetextMapToRepository==nil)
	{
		//CA("imagetextMapToRepository==nil");
		return;
	}
	InterfacePtr<ITextControlData>imagetextDownloadImage(iOptnsDownloadImagePathTextCntrlView, UseDefaultIID());
	if(imagetextDownloadImage==nil)
	{
		//CA("imagetextDownloadImage==nil");
		return;
	}

	//upto here
//21-feb
	InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);


	if (view != nil)
	{
		// Get the button ID from the view
		WidgetID theSelectedWidget = view->GetWidgetID();
		//if (theSelectedWidget == kOKButtonWidgetID && theChange == kTrueStateMessage)
		if (theSelectedWidget == kOptnsOKButtonWidgetID && theChange == kTrueStateMessage)		
		{
			
			////added by Tushar on 26/04....missing flag related addition

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			}
			
			IControlView* iMissingChkboxCntrlView=iPanelControlData->FindWidget(kMissingFlagWidgetID);
			if(iMissingChkboxCntrlView==nil) 
				return;
			
			InterfacePtr<ITriStateControlData> itristatecontroldataMissingFlag(iMissingChkboxCntrlView, UseDefaultIID());
			if(itristatecontroldataMissingFlag==nil) 
				return;
			
			ClientOptionsManip cops;
			PMString imagepath;
			//---------------------//
			//kPublicationStaticWidgetID
			//kImagePathStaticTextWidgetID
			//kDocpathStaticTextWidgetID
			//kLocaleStaticWidgetID
			//--------------------//
			
			InterfacePtr<ITextControlData> pubText(view , kPublicationStaticWidgetID);

		//	InterfacePtr<ITextControlData> documentText(view , kDocpathStaticTextWidgetID); commented 23 Feb Vaibhav
			InterfacePtr<ITextControlData> languageText(view , kLocaleStaticWidgetID);

			OptionsValue opts = PersistData::getOptions();
			PMString name = PersistData::getPubName();
//CA("name  :  "+name);
			PMString pub_ID("");
			pub_ID.AppendNumber(PersistData::getPubId());
//CA(pub_ID);
			if(name.NumUTF16TextChars() > 0){
				//CAlert::InformationAlert("name.NumUTF16TextChars() > 0");
				opts.setPublicationName(name);
				opts.setProjectID(pub_ID);//-------
				PersistData::setOptions(opts);
			}

			if(pubText)
			{
				//CA("pubText");
				opts.setPublicationName(const_cast<PMString& >(pubText->GetString()));
			}
				
			//Added by nitin
			
			if(itristatecontroldataMaptoRepository->IsSelected())
			{
				PMString images=imagetextMapToRepository->GetString();
				if(images == "")
				{
					CA("Please Select Map To Repository");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(images));
				iOptnsDownloadImagePathTextCntrlView->Show();
				imagetextDownloadImage->SetString("");
				iOptnsDownloadImagePathTextCntrlView->Hide();
			}
			else //if(iOptnsDownloadImagePathTextCntrlView->IsVisible())
			{
				PMString images=imagetextDownloadImage->GetString();
				if(images == "")
				{
					CA("Please Select Download Images To");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(images));
				iOptnsImagePathTextCntrlView->Show();
				imagetextMapToRepository->SetString("");
				iOptnsImagePathTextCntrlView->Hide();
			}
			//uptohere////////
		
			/*	if(imageText)
				opts.setImagePath(const_cast<PMString& >(imageText->GetString()));*/

			/// commented Vaibhav 23 Feb
			/*if(documentText)
				opts.setIndesignDocPath(const_cast<PMString& >(documentText->GetString()));*/

			if(languageText)
				opts.setDefaultLocale(const_cast<PMString& >(languageText->GetString()));
			////-----added 17 feb 06
			//InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			//if(ptrLogInHelper == nil)
			//{
			//	CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
			//	return ;
			//}

			//ptrLogInHelper->setEditedServerInfo(pubText->GetString() , languageText->GetString());
			////-------------------

//21-feb
			opts.setLogLevel (iLoglevelDropDownListController->GetSelected());
			
			//commented by nitin
			//if(itristatecontroldata->IsSelected())
			//	opts.setAssetStatus(1);
			//else
			//	opts.setAssetStatus(0);
			
			if(itristatecontroldataMaptoRepository->IsSelected())
				opts.setAssetStatus(1);
			else
				opts.setAssetStatus(0);

//21-feb
			//Added by nitin
			bool16 status = itristatecontroldataMissingFlag->IsSelected();
			if(status == kTrue)
			{
				opts.setMissingFlag(1);
				//CA("missing flag set to  kTrue");
				//ptrIAppFramework->setMissingFlag(kTrue);
			}
			else
			{
				opts.setMissingFlag(0);
				//CA("missing flag set to  kFalse");
				//ptrIAppFramework->setMissingFlag(kFalse);
			}
			//upto here

			cops.WriteClientOptions(/*PersistData::getOptions()*/opts);

			//CAlert::InformationAlert("After Set");
			closeOptnsDialog();
		}
		else if (theSelectedWidget == kApplyButtonWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box
			// writing optings into file

			//CAlert::InformationAlert("Inside Apply Button");

			InterfacePtr<ITextControlData> pubText(view , kPublicationStaticWidgetID);
		//	InterfacePtr<ITextControlData> documentText(view , kDocpathStaticTextWidgetID); // commented Vaibhav 23 Feb
			InterfacePtr<ITextControlData> languageText(view , kLocaleStaticWidgetID);

			ClientOptionsManip cops;			
			OptionsValue opts = PersistData::getOptions();
			PMString name = PersistData::getPubName();
			
//CA("name  :  "+name);
			PMString pub_ID("");
			pub_ID.AppendNumber(PersistData::getPubId());
			if(name.NumUTF16TextChars() > 0)
			{
				//CA("name.NumUTF16TextChars() > 0");
				opts.setPublicationName(name);
				opts.setProjectID(pub_ID);	//--------
				PersistData::setOptions(opts);
			}		

			if(pubText)
				opts.setPublicationName(const_cast<PMString& >(pubText->GetString()));
			
			//added by nitin
			if(itristatecontroldataMaptoRepository->IsSelected())
			{
				PMString images=imagetextMapToRepository->GetString();
				if(images == "")
				{
					CA("Please Select Map To Repository");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(imagetextMapToRepository->GetString()));
				iOptnsDownloadImagePathTextCntrlView->Show();
				imagetextDownloadImage->SetString("");
				iOptnsDownloadImagePathTextCntrlView->Hide();
			}
			else 
			{
				PMString images=imagetextDownloadImage->GetString();
				if(images == "")
				{
					CA("Please Select Download Images To");
					return;
				}
				opts.setImagePath(const_cast<PMString& >(imagetextDownloadImage->GetString()));
				iOptnsImagePathTextCntrlView->Show();
				imagetextMapToRepository->SetString("");
				iOptnsImagePathTextCntrlView->Hide();
			}
			
			//Commeneted by nitin
			//if(imageText)
			//	opts.setImagePath(const_cast<PMString& >(imageText->GetString()));

			// commented vaibhav 23 Feb
			/*if(documentText)
				opts.setIndesignDocPath(const_cast<PMString& >(documentText->GetString()));*/

			if(languageText)
				opts.setDefaultLocale(const_cast<PMString& >(languageText->GetString()));

			////-----added 17 feb 06
			//InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			//if(ptrLogInHelper == nil)
			//{
			//	CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
			//	return ;
			//}

			//ptrLogInHelper->setEditedServerInfo(pubText->GetString() , languageText->GetString());
			////-------------------
//21-feb
			opts.setLogLevel (iLoglevelDropDownListController->GetSelected());
			//Commented by nitin
			//if(itristatecontroldata->IsSelected())
			//	opts.setAssetStatus(1);
			//else
			//	opts.setAssetStatus(0);
			
			//added by nitin
			if(itristatecontroldataMaptoRepository->IsSelected())
				opts.setAssetStatus(1);
			else
				opts.setAssetStatus(0);
//21-feb
			cops.WriteClientOptions(/*PersistData::getOptions()*/opts);

		}

    }
}


void OkButtonObserver::closeOptnsDialog(void)
{
	InterfacePtr<IWidgetParent> parentHolder(this, IID_IWIDGETPARENT);
	if(parentHolder==nil){
		CAlert::InformationAlert("parentHolder nil");
		return;
	}

	InterfacePtr<IDialog> dialog((IDialog*) parentHolder->QueryParentFor(IID_IDIALOG));
	if(dialog==nil){
		CAlert::InformationAlert("dialog nil");
		return;
	}
	dialog->Close();
}
/*	AutoAttach
*/
void OkButtonObserver::AutoAttach()
{
	// Need to call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	
    CObserver::AutoAttach();
	
	// Get the IPanelControlData interface for the dialog:
	InterfacePtr<ISubject> iconSub(this, UseDefaultIID());

	if (iconSub != nil)
	{
		// Now we attach to SelectableDialog's info button widget:
		if (! iconSub->IsAttached (this, IID_IBOOLEANCONTROLDATA, IID_IOBSERVER))
		{
			iconSub->AttachObserver (this, IID_IBOOLEANCONTROLDATA, IID_IOBSERVER);
		}
	}
}

/*	AutoDetach
*/
void OkButtonObserver::AutoDetach()
{
	// Need to call parent's AutoDetach() so that default behavior
	// in the dialog will occur.
    CObserver::AutoDetach();

	// Get the IPanelControlData interface for the dialog:
	InterfacePtr<ISubject> iconSub(this, UseDefaultIID());
	if (iconSub != nil)
	{
		// Now we detach from SelectableDialog's info button widget:
		if (iconSub->IsAttached (this, IID_IBOOLEANCONTROLDATA, IID_IOBSERVER))
		{
			iconSub->DetachObserver (this, IID_IBOOLEANCONTROLDATA, IID_IOBSERVER);
		}
	}
}

// End, OkButtonObserver.cpp