
#ifndef PERSISTDAT_H
#define PERSISTDAT_H

#include"PMString.h"
#include"IPanelControlData.h"
#include"OptionsValue.h"

class PersistData
{	
	static InterfacePtr<IPanelControlData> generalPanelControlData;
	static OptionsValue curOptions;
	static PMString PubName;
	// 1 showing with options dialog, 2 stand alone, 0 invalid
	static bool16 PublicationDialogType;
	static bool16 PathBrowseDialogType;
	
public:

	static PMString& getPubName(void);
	static void  setPubName(PMString&);
	static OptionsValue& getOptions(void);
	static void  setOptions(OptionsValue&);
	static InterfacePtr<IPanelControlData> getGeneralPanelControlData(void);
	static void setGeneralPanelControlData(InterfacePtr<IPanelControlData> );

	static bool16 getPublicationDialogType(void);
	static void   setPublicationDialogType(bool16);

	static bool16 getPathBrowseDialogType(void);
	static void   setPathBrowseDialogType(bool16);
};

#endif