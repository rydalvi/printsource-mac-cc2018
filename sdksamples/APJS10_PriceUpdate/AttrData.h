#ifndef __ATTRData_h__
#define __ATTRData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class AttrData{
	private:
		double pubId;
		PMString pubName;
		PMString table_col;
		double attributeID;
        bool16 eventSpecificField;
	public:
		AttrData(){
			this->pubId=-1;
			this->pubName.Clear();
			this->table_col.Clear();
			this->attributeID = -1;
            this->eventSpecificField = kFalse;
		}
		double getPubId(void){
			return this->pubId;
		}
		void setPubId(double id){
			this->pubId=id;
		}
		PMString getPubName(void){
			return this->pubName;
		}
		void setPubName(PMString name){
			this->pubName=name;
		}
		PMString getTable_col(void){
			return this->table_col;
		}
		void setTable_col(PMString lvl){
			this->table_col=lvl;
		}
		void setAttribute_ID(double attrID)
		{
			attributeID = attrID;
		}
		double getAttribute_ID()
		{
			return attributeID;
		}
        void setEventSpecificField(bool16 flag)
        {
            eventSpecificField = flag;
        }
    
        bool16 isEventSpecificField()
        {
            return eventSpecificField;
        }
};

class AttributeData{
	private:
		static vector<AttrData> AttrInfoVector;
	public:
		void setAttrVectorInfo(double,PMString,PMString,double);
		AttrData getAttrVectorInfo(int32);
		void clearAttrVector();
};
#endif