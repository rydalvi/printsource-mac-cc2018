#ifndef __COMMONFUNCTIONS_H__
#define __COMMONFUNCTIONS_H__
#include "XMLTagAttributeValue.h"
#include "TagStruct.h"
#include "IIDXMLElement.h"
#include "ITextModel.h"

void collectAllXMLTagAttributeValue(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal);
void setAllXMLTagAttributeValue(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal);
void createPRINTsourceXMLTagAttributes(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal);
PMString prepareTagName(PMString name);
void updateTheTagWithModifiedAttributeValues(IIDXMLElement * &XMLElementTagPtr);
void replaceOldTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo);
void refreshTheItemAttribute(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo);
void refreshTheItemHeader(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo);
void replaceTheOldItemHeaderWithNewItemHeader(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo);
ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text);

#endif