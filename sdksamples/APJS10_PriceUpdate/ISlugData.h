#ifndef __ISlugData_h__
#define __ISlugData_h__

#include "IPMUnknown.h"
#include "PUPID.h"
#include "SlugStructure.h"
#include "vector"

using namespace std;

typedef vector<SlugStruct> SlugList;

class PMString;

class ISlugData : public IPMUnknown
{
public:
	enum	{kDefaultIID = IID_ISLUGDATA1};
	virtual void SetList(SlugList&, PMString&, PMString&) = 0;
	virtual const bool16 GetList(SlugList&, PMString&, PMString&) = 0;
};

#endif
