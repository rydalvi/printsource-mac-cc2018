#ifndef _ITABLEUTILITY_H__
#define _ITABLEUTILITY_H__

class PublicationNode;
#include "vector"
#include "ITableModel.h"
#include "ITableTextSelection.h"
#include "XMLReference.h"
#include "SlugStructure.h"
#include "ITool.h"
#include "TagStruct.h"
#include "DSFID.h"

#define DRAGMODE		1
#define CONTENTSMODE	2
using namespace std;

class ITableUtility: public IPMUnknown
{
public:

	enum { kDefaultIID = IID_ITABLEUTILITY };

	virtual bool16 isTablePresent(const UIDRef&, UIDRef&)=0;
	virtual void fillDataInTable(const UIDRef&, PublicationNode&, TagStruct& , int32&,const UIDRef&)=0;
	//virtual void fillDataInTable(const UIDRef , int32 objectId, int32 ,int32& )=0;
	virtual void resizeTable(const UIDRef&, const int32&, const int32&, bool16,const int32&,const int32&)=0;
	virtual void setTableRowColData(const UIDRef&, const PMString&, const int32&, const int32&)=0;
	virtual void putHeaderDataInTable(const UIDRef&, vector<int32>, bool16, TagStruct& )=0;
	virtual void SetSlug(const UIDRef&, vector<int32>, vector<int32>,bool8)=0;
	virtual void Slugreader(const UIDRef&, const int32&, const int32&,int32&,int32&)=0;
	virtual PMString TabbedTableData(const UIDRef&, PublicationNode&, int32, int32&)=0;
	virtual void AddTexttoTabbedTable(const UIDRef&,PMString,vector<int32>, vector<int32>,bool8)=0;
	virtual void AddSlugtoText(int32,int32)=0;
	virtual void IterateThroughTable(InterfacePtr<ITableModel>&)=0;
	virtual void NewSlugreader(InterfacePtr<ITableModel> &tblModel, const int32& rowIndex, const int32& colIndex,int32& Attr_id,int32& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel)=0;
	virtual void TextSlugReader(const UIDRef& tableUIDRef)=0;
	virtual bool8 IsAttrInTextSlug(const UIDRef& tableUIDRef,int32 Attr_id)=0;
	virtual bool8 UpdateDataInTextSlug(const UIDRef& tableUIDRef,int32 Attr_id,PMString Table_col)=0;
	virtual ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)=0;

	virtual void attachAttributes(XMLReference *newTag, bool16 IsPRINTsourceRootTag, SlugStruct& stencilInfoData, int rowno, int colno)=0;
	virtual ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, SlugStruct& stencilInfoData)=0;
	virtual ErrorCode AddTableAndCellElements(const UIDRef& tableModelUIDRef, const UIDRef& BoxUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								int32 TableID
								)=0;
	virtual ErrorCode TagStory(const PMString& tagName,const UIDRef& textModelUIDRef, const UIDRef& boxRef)=0;
	virtual ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								SlugStruct& stencilInfoData,
								int32 rowno, int32 colno, int32 TableID)=0;
	virtual UIDRef AcquireTag(const UIDRef& boxUIDRef, const PMString& tagName, bool16 &ISTagPresent)=0;
	virtual int16 convertBoxToTextBox(UIDRef boxUIDRef)=0;
	virtual int changeMode(int whichMode)=0;
	virtual ITool* queryTool(const ClassID& toolClass)=0;

};

#endif