#ifndef __ListBoxData_h__
#define __ListBoxData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "vector"
using namespace std;
//#include "MSysType.h"


//typedef vector<PMString> FileList;
class ListBoxData
{
public:
	IDFile fileName;
	bool16 isChecked;
	int isBook;	  /// 0= Independent Document, 1= Book Name , 2= Document inside Book
	ListBoxData(){};
	//static FileList fileList;
};
class SelectedDoc
{
public:
	IDFile docName;
	int isBook;	  /// 0= Independent Document, 1= Book Name , 2= Document inside Book
	SelectedDoc()
	{
	}
};
typedef vector<ListBoxData> DataList;
//class List
//{
//public:
//	static vector<ListBoxData> lstBoxData;
//	static vector<SelectedDoc> selDoc;
//};
#endif