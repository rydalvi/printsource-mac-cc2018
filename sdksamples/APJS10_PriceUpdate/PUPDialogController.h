#ifndef __PUPDIALOGCONTROLLER_H__
#define __PUPDIALOGCONTROLLER_H__


#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
#include "IAppFramework.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "Vector"
// Project includes:
#include "PUPID.h"
#include "AttrData.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "GlobalData.h"

//#include "CAlert.h"
//#define CA(X) CAlert::InformationAlert(X)

/** PUPDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.
*/
class PUPDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PUPDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~PUPDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
		void CallDialogInitaliser(bool16 ISFirstDialog);

};

#endif