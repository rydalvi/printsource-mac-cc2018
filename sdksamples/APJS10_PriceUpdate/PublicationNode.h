#ifndef __PUBLICATIONNODE_H__
#define __PUBLICATIONNODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class PublicationNode
{
private:
	PMString strPublicationName;
	int32 pubId;
	int32 parentId;
	int level;
	int32 seqNumber;
	int32 childCount;
	int hitCount;
	int32 referenceId;
	int32 typeId;
	int32 DesignerAction;
	int32 NewProduct;
	int32 PBObjectID;

public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	PublicationNode():strPublicationName(""), pubId(0), level(0), childCount(0), hitCount(0)
		, seqNumber(0){}
	/*
		One time access to all the private members
	*/
	void setAll(PMString& pubName, int32 pubId, int32 parentId, int level,
		 int32 seqNumber, int32 childCount, int hitCount, int32 TypeID , int32 ReferenceId)
	{
		this->strPublicationName=pubName;
		this->pubId=pubId;
		this->parentId=parentId;
		this->level=level;
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
		this->typeId =TypeID; 
		this->referenceId = ReferenceId;
	}

	int32 getReferenceId(void) { return this->referenceId; }
	void setReferenceId(int32 id) { this->referenceId=id; }
	int32 getTypeId(void) { return this->typeId; }
	void setTypeId(int32 id) { this->typeId=id; }
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->strPublicationName; }
	/*
		@returns the id of the publication
	*/
	int32 getPubId(void) { return this->pubId; }
	/*
		@returns the parent id for that node
	*/
	int32 getParentId(void) { return this->parentId; }
	/*
		@returns the level (distance from the root) of the publication
	*/
	int getLevel(void) { return this->level; }
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int getHitCount(void) { return this->hitCount; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	void setPubId(int32 pubId) { this->pubId=pubId; }
	/*
		Sets the parent id for the publication
		@returns none
	*/
	void setParentId(int32 parentId) { this->parentId=parentId; }
	/*
		Sets the level for the publication
		@returns none
	*/
	void setLevel(int level) { this->level=level; }
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setPublicationName(PMString& pubName) { this->strPublicationName=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int hitCount) { this->hitCount=hitCount; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	void setDesignerAction(int32 DesignerAction){ this->DesignerAction=DesignerAction; }

	int32 getDesignerAction(void) { return this->DesignerAction; }

	void setNewProduct(int32 NewProduct){this->NewProduct= NewProduct;}

	int32 getNewProduct(void) {return this->NewProduct; }	

	void setPBObjectID(int32 PBObjectID){this->PBObjectID= PBObjectID;}

	int32 getPBObjectID(void) {return this->PBObjectID; }	

};

typedef vector<PublicationNode>PublicationNodeList ;

#endif