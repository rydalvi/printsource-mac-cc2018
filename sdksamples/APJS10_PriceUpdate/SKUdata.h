#ifndef __SKUDATA_H__
#define __SKUDATA _H__

#include"vector"

using namespace std;

class SKUData
{
public:
	
	PMString OriginalData;
	PMString ChangedData;
	PMString DocPath;
	PMString Date;
	int32 PageNo;
	PMString ColumName;
	int32 ItemID;	
	PMString attributeName; //*** Added By Sachin Sharma
	PMString baseNumber; //*****Added By Sachin Sharma

	SKUData()
	{
		OriginalData.Append("");
		ChangedData.Append("");
		DocPath.Append("");
		Date.Append("");
		PageNo=-1;
		ColumName.Append("");
		ItemID = -1;
		attributeName.Append(""); //*** Added By SachinS Sharma
		baseNumber.Append("");//****Added By Sachin Sharma 
		
	}
};

typedef vector<SKUData> SKUDataList;

#endif