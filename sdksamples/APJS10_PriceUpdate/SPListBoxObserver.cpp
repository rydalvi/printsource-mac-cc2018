#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "PUPID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
//#include "TPLMediatorClass.h"
//#include "TPLListboxData.h"
#include "IListControlData.h"
//#include "GlobalData.h"
//#include "FilterData.h"
//#include "MediatorClass.h"
//#include "IDataSprayer.h"
//#include "PublicationNode.h"
#include "ListBoxData.h"


#define CA(x)	CAlert::InformationAlert(x)
//int32 PFRow = -1;
//extern FilterDataList samitvflist;
//extern PublicationNodeList pNodeDataList;
//extern int32 CurrentSelectedSection;
//extern int32 CurrentSelectedPublicationID;

int32 CurrentLstBoxIndex;
extern vector<ListBoxData> lstBoxData;
extern vector<SelectedDoc> selDoc;

class SPListBoxObserver : public CObserver
{
public:
	SPListBoxObserver(IPMUnknown *boss);
	~SPListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
private:
	void updateListBox(bool16 isAttaching);
};

CREATE_PMINTERFACE(SPListBoxObserver, kSPListBoxObserverImpl)

SPListBoxObserver::SPListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

SPListBoxObserver::~SPListBoxObserver()
{
}

void SPListBoxObserver::AutoAttach()
{
	
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
		
	}
	//updateListBox(kTrue);
}

void SPListBoxObserver::AutoDetach()
{
	updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IUNKNOWN);
	}
}

void SPListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CA("In Update");
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			//CA("11");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				CA("panelControlData == nil");
				return;
			}

			SDKListBoxHelper listHelper(this, kPUPPluginID);
			/*if(listHelper==nil)
				CA("hiu");*/
			IControlView * listControlView = listHelper.FindCurrentListBox(panelControlData, 1);
			if(!listControlView) 
			{
				CA("ListControl View is null");
				return;
			}
				//CA("listBox == nil");
				
			InterfacePtr<IListBoxController> listBoxController(listControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(!listBoxController)
			{
				CA("ListBoxController is null");
				break;
			}
			K2Vector<int32> multipleSelection ;
			
			listBoxController->GetSelected( multipleSelection ) ;
			
			const int kSelectionLength =  multipleSelection.Length() ;
			
			if (kSelectionLength> 0 )
			{
				for(int index=0;index<kSelectionLength;index++)
				{
					if(/*List::*/lstBoxData[multipleSelection[index]].isChecked)
						/*List::*/lstBoxData[multipleSelection[index]].isChecked=kFalse;
					else
						/*List::*/lstBoxData[multipleSelection[index]].isChecked=kTrue;
				
					listHelper.CheckUncheckRow(listControlView,multipleSelection[index],/*List::*/lstBoxData[multipleSelection[index]].isChecked);
				}
			}

		} while(0);
	}
}


void SPListBoxObserver::updateListBox(bool16 isAttaching)
{
			/*CA("11");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				CA("panelControlData == nil");
				return;
			}

			SDKListBoxHelper listHelper(this, kSPPluginID);
			IControlView * listBox = listHelper.FindCurrentListBox(panelControlData, 2);
			if(listBox == nil) {
				CA("listBox == nil");
			}
			PMString as;
			as.Append("Amit kuamr");

			listHelper.AddElement(listBox , as, kSPRFltOptTextWidgetID,0);*/
}
