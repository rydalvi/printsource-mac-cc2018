#ifndef _XMLTAGATTRIBUTEVALUE__H__
#define _XMLTAGATTRIBUTEVALUE__H__

struct XMLTagAttributeValue
	{
		PMString tagName;
		//
		PMString ID;
		PMString typeId;
		PMString header;
		PMString isEventField;
		PMString deleteIfEmpty;
		PMString dataType;
		PMString isAutoResize;
		PMString LanguageID;
		PMString index;
		PMString pbObjectId;
		PMString parentID;
		PMString childId;
		PMString sectionID;
		PMString parentTypeID;
		PMString isSprayItemPerFrame;
		PMString catLevel;
		PMString imgFlag;
		PMString imageIndex;
		PMString flowDir;
		PMString childTag;
		PMString tableFlag;
		PMString tableType;
		PMString tableId;
				
		PMString rowno;
		PMString colno;
		XMLTagAttributeValue()
		{
			tagName = "";

			ID = "";
			typeId = "";
			header = "";
			isEventField = "";
			deleteIfEmpty = "";
			dataType = "";
			isAutoResize = "";
			LanguageID = "";
			index = "";
			pbObjectId = "";
			parentID = "";
			childId = "";
			sectionID = "";
			parentTypeID = "";
			isSprayItemPerFrame = "";
			catLevel = "";
			imgFlag = "";
			imageIndex = "";
			flowDir = "";
			childTag = "";
			tableFlag = "";
			tableType = "";
			tableId = "";
	
			rowno = "";
			colno = "";				
		}
	};

#endif