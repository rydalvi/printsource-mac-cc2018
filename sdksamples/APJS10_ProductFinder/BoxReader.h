#ifndef __BOXREADER_H__
#define __BOXREADER_H__

#include "VCPluginHeaders.h"
#include "PageData.h"

class BoxReader
{
public:
	bool16 getBoxInformation(const UIDRef&, PageData&);
	bool16 getBoxInformation(const UIDRef&, PageData&, int32, int32);
protected:
	bool16 objectIDExists(const PageData&, const TagStruct&, int32*);
	bool16 elementIDExists(const ElementInfoList&, double, PMString& elementName);
	bool16 AppendElementInfo(const TagStruct&, PageData&, int32, bool16 isTaggedFrame=kFalse);
	bool16 AppendObjectInfo(const TagStruct&, PageData&, bool16 isTaggedFrame=kFalse);
	
};

#endif