#include "VCPlugInHeaders.h"//This File should be included first and after that remaining header files be included
#include "IContentSprayer.h"
#include "CPMUnknown.h"
#include "SPID.h"
#include "CAlert.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "SPSelectionObserver.h"
#include "MediatorClass.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "SectionData.h"
//#include "ISpecialChar.h"
#include "ISPXLibraryButtonData.h"
#include "SDKListBoxHelper.h"
#include "IClientOptions.h"
#include "SPActionComponent.h"
#include "SPTreeDataCache.h"
#include "ITreeViewMgr.h"
#include "SPTreeModel.h"




#define CA(X)		CAlert::InformationAlert(X)
double global_classID;
extern bool8 FilterFlag;
extern VectorObjectInfoPtr global_vectorObjectValuePtr;
extern VectorItemModelPtr global_vectorItemModelPtr;

extern double global_lang_id ;	//---------
extern bool16 isONEsource ;
extern VectorPubModel global_vector_pubmodel ;
//extern vector<bool8> GlobalDesignerActionFlags;
extern bool16 displayAll;
extern double CurrentSelectedPublicationID;
extern SPTreeDataCache dc;
extern PublicationNodeList pNodeDataList; 

class ContentSprayer :public CPMUnknown<IContentSprayer>
{	
	public:
		ContentSprayer(IPMUnknown*);
		~ContentSprayer();
		 //void isClass();
		 bool16 setClassNameAndClassID(const double classID, const PMString className , bool16 checkONEsource, vector<CPubModel>* pCPubModelVec = NULL,int32 isCheckCount = 0, ItemProductCountMapPtr temProductSectionCountPtr = NULL);
		 void fillListForSearch(const VectorObjectInfoPtr vectorObjectValuePtr,const VectorItemModelPtr vectorItemModelPtr);
		 void openContentSprayerPanelFromRefreshContent(double sectionID,double langID);
		 void closeContentSprayerPanelFromRefreshContent(double sectionID,double langID);
};


CREATE_PMINTERFACE( ContentSprayer,kContentSprayerImpl)

ContentSprayer::ContentSprayer(IPMUnknown* boss):CPMUnknown<IContentSprayer>(boss)
{}
ContentSprayer::~ContentSprayer()
{}


bool16 ContentSprayer::setClassNameAndClassID(const double classID, const PMString className ,bool16 checkONEsource, vector<CPubModel>* pCPubModelVec ,int32 isCheckCount , ItemProductCountMapPtr temProductSectionCountPtr)
{
	//CA(__FUNCTION__);
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
    
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(!ptrIClientOptions)
	{
		ptrIAppFramework->LogError("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::No ptrIClientOptions ");					
		return kFalse;
	}

	double deflang_id = -1 ;
	PMString lang_name("");

	deflang_id = ptrIClientOptions->getDefaultLocale(lang_name);

	ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID " + lang_name);

	global_lang_id = deflang_id;
	isONEsource = checkONEsource ;
	global_classID =classID; //It is used on refreshButton click to populate listBox.

	Mediator md;
	InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
	if(app == NULL) 
	{ 
		//CA("No Application");
		return kFalse;
	}
		
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL) 
	{	
		//CA("No IPanelMgr");	
		return kFalse;
	}
	
	IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
	if(!myPanel) 
	{
		PMString name(className);
		PMString nilSectionName = "";
		double nilSectionID = -1;
		Mediator::sectionID = classID;
		if(ptrIAppFramework->get_isONEsourceMode())
		{
			ptrIClientOptions->setPublication(name , Mediator::sectionID);
		}
		else
		{
			//ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID 4");
			CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(Mediator::sectionID,global_lang_id);
			double rootPubID = pubModelRootValue.getRootID();
			CPubModel pubRootValue = ptrIAppFramework->getpubModelByPubID(rootPubID,global_lang_id);
			PMString rootPubName = pubRootValue.getName();

			//ptrIClientOptions->setPublication(rootPubName , rootPubID);
			if(rootPubID == Mediator::sectionID)
			{
				//ptrIClientOptions->setSection(nilSectionName,nilSectionID );
			}
			else
			{
				//ptrIClientOptions->setSection(name,Mediator::sectionID );
			}

			//ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID 5");
			
		} 
		//return kFalse;
	}

	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	if(!panelControlData) 
	{
		//CA("No SPPanelWidgetControlData");
		return kFalse;
	}
	
	IControlView* multiTextControlView = panelControlData->FindWidget( kSPMultilineTextWidgetID);
	if(multiTextControlView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No multiTextControlView");
		return kFalse;
	}

	InterfacePtr<ITextControlData> txtData(multiTextControlView,IID_ITEXTCONTROLDATA);
	if(txtData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No txtData");
		return kFalse;
	}

	IControlView* SectionDropListCntrlView = panelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
	if(SectionDropListCntrlView == nil)
	{
		//CA("SectionDropListCntrlView == nil");
		return kFalse;
	}

	InterfacePtr<IDropDownListController> SectionDropListCntrler(SectionDropListCntrlView, UseDefaultIID());
	if(SectionDropListCntrler==nil)
	{
		//CA("SectionDropListCntrler is nil");
		return kFalse;
	}
	
	InterfacePtr<IStringListControlData> SectionDropListCntrlData(SectionDropListCntrler, UseDefaultIID());
	if(SectionDropListCntrlData==nil)
	{
		//CA("SectionDropListCntrlData is nil");
		return kFalse;
	}

	IControlView* pubNameControlView = panelControlData->FindWidget( kSPPubNameWidgetID);
	if(pubNameControlView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameControlView");
		return kFalse;
	}

	InterfacePtr<ITextControlData> pubNameTxtData(pubNameControlView,IID_ITEXTCONTROLDATA);
	if(pubNameTxtData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameTxtData");
		return kFalse;
	}

	IControlView *sectionText1View = panelControlData->FindWidget(kSectionText_1WidgetID);
	if(sectionText1View == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No sectionText1View");				
		return kFalse;
	}

	SectionDropListCntrlView->Enable();

	IControlView* spTreeListBoxWidgetView = panelControlData->FindWidget(kSPTreeListBoxWidgetID);

	InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
	if(!treeViewMgr)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
		return kFalse;
	}

	SectionDropListCntrlData->Clear(); 
	dc.clearMap();
	SPTreeModel pModel;
	PMString pfName("Root");
	pModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	pModel.GetRootUID();
	treeViewMgr->ChangeRoot();

	pNodeDataList.clear();
	
	SectionData publdata;
	publdata.clearSectionVector();
	publdata.clearSubsectionVector();
	
	PMString as;
	as.Append("--Select--");
	as.SetTranslatable(kFalse);
	SectionDropListCntrlData->AddString(as);
	int32 vec_PubSize = 0 ;
	int32 vect_OneSrcSize = 0;
	PMString pubNAME = "";
	PMString newClassName = "";
	int32 selRowIndex = 1;
	
	if(isONEsource)		//----For ONEsource--------
	{
		//CA("isONEsource_1");
		ptrIAppFramework->set_isONEsourceMode(kTrue); 

		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(global_lang_id); 
		if(vectClsValuePtr == nil){
			ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::vectClsValuePtr == nil ");
			return kFalse;
		}
		VectorClassInfoValue::iterator itr;
		itr = vectClsValuePtr->begin();
		
		newClassName = itr->getClassification_name();

		CClassificationValue classValueObj = ptrIAppFramework->StructureCache_getCClassificationValueByClassId(global_classID,global_lang_id);
		double parent_ID = classValueObj.getParent_id();

		VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(parent_ID, global_lang_id);
		if(vect==nil)
		{
			//CA("vect==nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::vect==nil");
			return kFalse;

		}
		
		int32 Cat_index = 1;
		VectorClassInfoValue::iterator it1;
		vect_OneSrcSize = static_cast<int32>(vect->size());
		for(it1=vect->begin(); it1!=vect->end(); it1++ , Cat_index++)
		{
			PMString clsName = it1->getClassification_name();
			double clsID = it1->getClass_id();
			double clsParentID 	= it1->getParent_id();
			int32 clsLevel 	= it1->getClass_level();
			
			if(classID == clsID)
			{
				selRowIndex = Cat_index;
			}
			/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				clsName=clsName;
				return kFalse;
			}*/
			//clsName=iConverter->translateString(clsName);
			publdata.setSectionVectorInfo(clsID , clsName ,clsLevel , clsParentID , -1 , "");

			SectionDropListCntrlData->AddString(clsName);

		}
		if(vectClsValuePtr)
			delete vectClsValuePtr;
		if(vect)
			delete vect;
	}
	else
	{
		//----For Publication-------
		//CA("!!!!isONESource");

		CPubModel pubModelRootValue;
									
		double pubParent_ID = -1;
		double rootPubID = -1;

		if(pCPubModelVec != NULL)
		{
			if(pCPubModelVec->size() > 0)
			{
				rootPubID = pCPubModelVec->at(0).getRootID();
			}
		}
		else
		{
			pubModelRootValue = ptrIAppFramework->getpubModelByPubID(global_classID,global_lang_id);
			
			pubParent_ID = pubModelRootValue.getParentId();
			rootPubID = pubModelRootValue.getRootID();

			/*PMString asd("pubParent_ID: ");
			asd.AppendNumber(pubParent_ID);
			asd.Append("  rootPubID: ");
			asd.AppendNumber(rootPubID);
			asd.Append("  global_classID: ");
			asd.AppendNumber(global_classID);
			asd.Append("  global_lang_id: ");
			asd.AppendNumber(global_lang_id);
			ptrIAppFramework->LogError(asd);*/
		}
		
		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(rootPubID,global_lang_id);
		PMString secName("");
		PMString TempBuff = pubModelValue.getName();
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			pubNAME=iConverter->translateString(TempBuff);
		}
		else*/
			pubNAME = TempBuff;

		ptrIClientOptions->setPublication(pubNAME,rootPubID);
		
		CurrentSelectedPublicationID  = rootPubID;

		VectorPubModelPtr vec_pubmodelptr = NULL;
		if(global_classID == rootPubID)
		{

			vec_pubmodelptr = ptrIAppFramework->/*getAllSubsectionsByPubIdAndLanguageId*/ProjectCache_getAllChildren(global_classID,global_lang_id);
			
			if(vec_pubmodelptr == NULL)
			{
				//CA("vec_pubmodelptr == NULL");
				PMString catyName("");
				catyName = "Event"; // ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Publication"),global_lang_id);
				catyName.Append(" : ");
				catyName.Append(pubNAME);
                catyName.ParseForEmbeddedCharacters();
				pubNameTxtData->SetString(catyName);
				SPSelectionObserver spSelctionObj(this);
				spSelctionObj.populateProductListforSection(global_classID);
				return kFalse;
			}		
		}
		else
		{
			if(pCPubModelVec == NULL)
			{
				vec_pubmodelptr = ptrIAppFramework->ProjectCache_getAllChildren(pubParent_ID,global_lang_id);
			}
			else
			{
				vec_pubmodelptr = pCPubModelVec;
			}
            
			if(vec_pubmodelptr == NULL)
			{
				ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::ProjectCache_getAllChildren'vec_pubmodelptr==nil");
				return kFalse;
			}
		}

		vec_PubSize = static_cast<int32>(vec_pubmodelptr->size());
		if(vec_PubSize == 0 && pCPubModelVec == NULL)
		{
			//CA("vec_PubSize == 0");
			delete vec_pubmodelptr;
			return kFalse;
		}

		
		VectorPubModel::iterator Itr;

		int32 index= 1;

		for(Itr = vec_pubmodelptr->begin(); Itr != vec_pubmodelptr->end(); Itr++ , index++)
		{
			double pubid=Itr->getEventId();
			PMString pubname=Itr->getName();
			int32 lvl= 0  ; //it->getLevel_no();
			double rootid = Itr->getRootID();
			double Typeid = Itr->getTypeID();
			PMString PubComment = ""; //Itr->getComments();

			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//if(!iConverter)
			//{
			//	//CA("!iConverter");
			//	pubname=pubname;
			//	return kFalse;
			//}
			//pubname = iConverter->translateString(pubname);
			//CA("pubname  :  "+pubname);
			if(classID == pubid)
			{
				selRowIndex = index ;
			}
			publdata.setSectionVectorInfo(pubid,pubname,lvl, rootid, Typeid, PubComment);

			if(isCheckCount)
			{
				if(temProductSectionCountPtr == NULL)
				{
					ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::ItemProductCountMapPtr::temProductSectionCountPtr == NULL");
					return kFalse;
				}
				if(temProductSectionCountPtr->size() == 0)
				{
					ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::ItemProductCountMapPtr::temProductSectionCountPtr->size() == 0");
					return kFalse;
				}
				map<double, double>::iterator mapItr;
				mapItr = temProductSectionCountPtr->find(pubid);
				int32 secChild_Count = mapItr->second;

				pubname.Append(" (");
				pubname.AppendNumber(secChild_Count);
				pubname.Append(")");
				SectionDropListCntrlData->AddString(pubname);

			}
			else
			{
				pubname.SetTranslatable(kFalse);
				SectionDropListCntrlData->AddString(pubname);
				//ptrIAppFramework->LogDebug("Added String to section dropdown");
				//ptrIAppFramework->LogDebug(pubname);
			}

		}
		
		if(vec_pubmodelptr && pCPubModelVec == NULL)
			delete vec_pubmodelptr;
	}
	
	if(isONEsource)	
	{
		if(vect_OneSrcSize > 0)
		{
			SectionDropListCntrler->Select(selRowIndex/*1*/);
			PubData clssficNameData = publdata.getSectionVectorInfo(0);
			PMString clssficName = clssficNameData.getPubName();
			InterfacePtr<ISPXLibraryButtonData> commData(SectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			if(commData)
			{
				commData->SetName(clssficName);
			}

		}
		else
		{
			SectionDropListCntrler->Select(0);
			PMString clsComment("");
			InterfacePtr<ISPXLibraryButtonData> commData( SectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			if ( commData ){
				commData->SetName(clsComment);
			}
		}
	}
	else
	{
		if(vec_PubSize >0 )
		{
			ptrIAppFramework->LogInfo("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID 28");
			SectionDropListCntrler->Select(selRowIndex);

			PubData Temppubldata = publdata.getSectionVectorInfo(0);
			PMString SectionComment = Temppubldata.getPubName();
			PMString seccomm = Temppubldata.getComment();
			if(seccomm.NumUTF16TextChars() >0)
			{
				SectionComment.Append("\n");
				SectionComment.Append(seccomm);
			}
			InterfacePtr<ISPXLibraryButtonData> data(SectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			if ( data ){
				//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
				data->SetName(SectionComment);
			}

		}
		else
		{
			SectionDropListCntrler->Select(0);
			
			PMString SectionComment("");
			InterfacePtr<ISPXLibraryButtonData> data( SectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			if ( data ){
				//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
				data->SetName(SectionComment);
			}
		}
	}
	md.setCurrentSelectedRow(selRowIndex);

	//The ListBox and MultilineTextWidget should get updated from AP46_CategoryBrowser only if selected mode is ONEsource(Master) otherwise for rest Publication it should not work.
	
	/*bool16 currentOneSource =kFalse;
	currentOneSource = ptrIAppFramework->get_isONEsourceMode();
	if(currentOneSource == kTrue)
	{*/
		//CA("currentOneSource == kTrue");
		//CA("className  :  "+className);
		//txtData->SetString(className);//To Set Name on MultilineTextWidget when global_project_level=4
	
	PMString categoryName = "";
	if(!isONEsource)
	{
		categoryName = "Event"; //ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Publication"),global_lang_id);
		categoryName.Append(" : ");
		//categoryName.Append(className);
		categoryName.Append(pubNAME);
	}
	else
	{
		categoryName.Append(newClassName);
	}

	categoryName.SetTranslatable(kFalse);
    categoryName.ParseForEmbeddedCharacters();
	pubNameTxtData->SetString(categoryName);

	/*SPSelectionObserver spSelctionObj(this);		//----------
	spSelctionObj.populateProductListforSection(classID);*/

	//FilterButton is Hide and show in Publication;
	if(FilterFlag == kTrue)
	{
		if(	md.getAssignBtnView() == nil)
		{
		}
		else
		{
			md.getAssignBtnView()->HideView();	
		}

		if(md.getGreenFilterBtnView() == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No getGreenFilterBtnView");
			return kFalse;
		}
		else
			md.getGreenFilterBtnView()->ShowView();
	}
	else
	{
		if(md.getGreenFilterBtnView() == nil)
		{
		}
		else
			md.getGreenFilterBtnView()->HideView();

		if(md.getAssignBtnView() == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No getAssignBtnView");
			return kFalse;
		}
		else
			md.getAssignBtnView()->ShowView();	
		
	}

	/*}*/
	//else
	//{
	//	//CA("currentOneSource == kFalse");
	//	//CA("className  :  "+className);
	//	pubNameTxtData->SetString(className);
	//	SPSelectionObserver spSelctionObj(this);
	//	spSelctionObj.populateProductListforSection(classID);
	//	//FilterButton is Hide and show in Publication;
	//	if(md.getGreenFilterBtnView() == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No getGreenFilterBtnView");
	//		return kFalse;
	//	}
	//	else
	//		md.getGreenFilterBtnView()->HideView();
	//	if(md.getAssignBtnView() == nil)
	//	{
	//				ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No getAssignBtnView");
	//				return kFalse;
	//	}
	//	else
	//		md.getAssignBtnView()->ShowView();	
	//}	

	return kTrue ;
}

void ContentSprayer::fillListForSearch(const VectorObjectInfoPtr vectorObjectValuePtr, const VectorItemModelPtr vectorItemModelPtr)
{
	//global_vectorObjectValuePtr->clear();
	//global_vectorItemModelPtr->clear();
	global_vectorObjectValuePtr = vectorObjectValuePtr;
	global_vectorItemModelPtr = vectorItemModelPtr;

	SPSelectionObserver spSelctionObj(this);
	spSelctionObj.populateProductListforSectionforSearch(vectorObjectValuePtr , vectorItemModelPtr);
}

void ContentSprayer::openContentSprayerPanelFromRefreshContent(double sectionID,double langID){
	//CA("ContentSprayer::openContentSprayerPanelFromRefreshContent");
	//do{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//	{
	//		//CA("ptrIAppFramework == nil");
	//		return;
	//	}
	//	
	//	InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
	//	if(iApplication==nil)
	//	{	
	//		//CA("iApplication");
	//		break;
	//	}
	//	InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
	//	if(iPanelMgr == nil)
	//	{			
	//		break;
	//	}
	//	if(iPanelMgr->IsPanelWithWidgetIDShown(kSPPanelWidgetActionID) == kFalse){
	//		displayAll = kFalse;
	//		SPActionComponent spActionComponentObj(this);
	//		spActionComponentObj.DoPalette();
	//	}	
	//	CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(sectionID,langID);
	//	
	//	PMString TempBuff = pubModelRootValue.getName();
	//	//int32 PubID = pubModelRootValue.getRootID();
	////	CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(PubID,langID);
	////	PMString TempBuff = pubModelValue.getName();
	//	PMString PubName("");
	//	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//	//if(iConverter)
	//	//{
	//	//	PubName=iConverter->translateString(TempBuff);
	//	//}
	//	//else
	//		PubName = TempBuff;

	//	FilterFlag = kTrue;
	//	GlobalDesignerActionFlags.clear();
	//	GlobalDesignerActionFlags.push_back(kTrue);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);
	//	GlobalDesignerActionFlags.push_back(kFalse);

	//	VectorLanguageModelPtr vecLangModel = ptrIAppFramework->/*LNGCache_getAllLanguages*/StructureCache_getAllLanguages();
	//	if(vecLangModel == NULL){
	//		return;
	//	}
	//	if(vecLangModel->size() == 0){
	//		//delete vecLangModel;
	//		return;
	//	}
	//	PMString langName("");
	//	for(int32 index = 0; index < vecLangModel->size() ; index++){
	//		int32 langaugeID = vecLangModel->at(index).getLanguageID();
	//		if(langaugeID == langID){
	//			langName = vecLangModel->at(index).getLangugeName();
	//			break;
	//		}
	//	}
	//	//if(vecLangModel)
	//		//delete vecLangModel;
	//	
	//	if(langName != ""){
	//		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//		if(!ptrIClientOptions)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::No ptrIClientOptions ");					
	//			return;
	//		}
	//		ptrIClientOptions->setLocale(langName,langID);
	//	}

	//	this->setClassNameAndClassID(sectionID,PubName,kFalse);
	//	//this->setClassNameAndClassID(PubID,PubName,kFalse);
	//}while(kFalse);
}
void ContentSprayer::closeContentSprayerPanelFromRefreshContent(double sectionID,double langID){

	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	//CA("ptrIAppFramework == nil");
	//	return;
	//}
	//InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication()); //Cs4
	//if(app == NULL) 
	//{ 
	//	//CA("No Application");
	//	return ;
	//}
	//InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	//if(panelMgr == NULL) 
	//{	
	//	//CA("No IPanelMgr");	
	//	return ;
	//}
	//
	//IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
	//if(!myPanel) 
	//{	
	//	//CA("No SPPanelWidgetControlView");
	//	return ;
	//}
	//InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	//if(!panelControlData) 
	//{
	//	//CA("No SPPanelWidgetControlData");
	//	return ;
	//}

	//IControlView* AssignBtnView=panelControlData->FindWidget(kAssignButtonWidgetID);
	//if(AssignBtnView)
	//{
	//	//CA("LoadPaletteData 21");
	//	AssignBtnView->ShowView();
	//	AssignBtnView->Enable();

	//}			
	//
	//IControlView* GreenFilterBtnView=panelControlData->FindWidget(kGreenFilterButtonWidgetID);
	//if(GreenFilterBtnView)
	//{
	//	//CA("LoadPaletteData 23");
	//	GreenFilterBtnView->Disable();
	//	GreenFilterBtnView->HideView();
	//}

	//FilterFlag = kFalse;
	//displayAll = kTrue;
	//GlobalDesignerActionFlags.clear();
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);
	//GlobalDesignerActionFlags.push_back(kFalse);

	//CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(sectionID,langID);
	//	
	//PMString TempBuff = pubModelRootValue.getName();
	//PMString PubName("");
	////InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	////if(iConverter)
	////{
	////	PubName=iConverter->translateString(TempBuff);
	////}
	////else
	//	PubName = TempBuff;

	//VectorLanguageModelPtr vecLangModel = ptrIAppFramework->/*LNGCache_getAllLanguages*/StructureCache_getAllLanguages();
	//if(vecLangModel == NULL){
	//	return;
	//}
	//if(vecLangModel->size() == 0){
	//	//delete vecLangModel;
	//	return;
	//}
	//PMString langName("");
	//for(int32 index = 0; index < vecLangModel->size() ; index++){
	//	int32 langaugeID = vecLangModel->at(index).getLanguageID();
	//	if(langaugeID == langID){
	//		langName = vecLangModel->at(index).getLangugeName();
	//		break;
	//	}
	//}
	////if(vecLangModel)
	//	//delete vecLangModel;
	//
	//if(langName != ""){
	//	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//	if(!ptrIClientOptions)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::No ptrIClientOptions ");					
	//		return;
	//	}
	//	ptrIClientOptions->setLocale(langName,langID);
	//}

	//this->setClassNameAndClassID(sectionID,PubName,kFalse);

	//SPActionComponent spActionComponentObj(this);
	//spActionComponentObj.ClosePSPPalette();	
}
