#ifndef __RoleData_h__
#define __RoleData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"



class RoleData
{
	private:
		double role_id;
		PMString role_Name;
		PMString role_Description;
	public:
		RoleData(){
			this->role_id=-1;
			this->role_Name.Clear();
			this->role_Description.Clear();
		}
		RoleData(double id,PMString name,PMString roleDescription)
		{
			this->role_id=id;
			this->role_Name = name;
			this->role_Description = roleDescription;

		}
		double getrole_id(void){
			return this->role_id;
		}
		void setrole_id(int32 id){
			this->role_id=id;
		}
		PMString getrole_Name(void){
			return this->role_Name;
		}
		void setrole_Name(PMString name)
		{
			this->role_Name=name;
		}

		PMString getrole_Description(void)
		{
			return this->role_Description;
		}
		void setrole_Description(PMString name)
		{
			this->role_Description=name;
		}
		
};
#endif