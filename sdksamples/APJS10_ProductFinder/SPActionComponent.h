#ifndef SPAC_H
#define SPAC_H

#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IAppFramework.h"
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "IActionStateList.h"


class SPActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		SPActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		virtual void DoDialog();
		void CloseDialog();
		//void UpdateActionStates (IActionStateList* iListPtr);
		void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

		void ClosePSPPalette();
		void DoPalette();

		static IDialog* DlgPtr;
		//static IPaletteMgr* palettePanelPtr;//Commented By SAchin Sharma
		

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Opens this plug-in's dialog. */
		
		
	       /** Encapsulates functionality for the MenuItem1 menu item. */
		void DoMenuItem1(IActiveContext* ac);



};
#endif
