//========================================================================================
//  
//  $File: $
//  
//  Owner: Sagar Hagawne
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "SPID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "CAlert.h"
#include "FilterData.h"
#include "AFWID.h"
#include "IAppFramework.h"
#include "IDropDownListController.h"
//#include "GlobalData.h"
#include "IListBoxController.h"
//#include "ILoggerFile.h"
#include "IStringListControlData.h"
#include "ITriStateControlData.h"

//#define CA(x)	CAlert::InformationAlert(x)
//-----------------------------------------------------------------------------------------------//
#define CA(x)	CAlert::InformationAlert(x)
#define YP(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)
//------------------------------------------------------------------------------------------------//
extern FilterDataList samitvflist;
extern bool8 IsNewSectionSelected;
extern FilterDataList tempMileStoneListBoxValues;

bool16 displayAll = kTrue;

/** SPDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.	
	@ingroup content sprayer
*/
class SPDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SPDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~SPDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(SPDialogController, kSPDialogControllerImpl)

/* ApplyFields
*/
void SPDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA(" in SPDialogController InitializeDialogFields ");
	CDialogController::InitializeDialogFields(dlgContext);

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
    		return;

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogController::InitializeDialogFields::No panelControlData");
		return;
	}

	//CA("1");


	SDKListBoxHelper listHelper(this, kSPPluginID);
	IControlView * listBox = listHelper.FindCurrentListBox(panelControlData, 2);
	if(listBox == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogController::InitializeDialogFields::No listBox");
		return;
	}


	bool16 result = ptrIAppFramework->CONFIGCACHE_getPM_DisplayWorkflow();
	//result = kTrue ;
	if(!result)
	{
		/*
			following code will 
			disable topmost part of milestone dialog 
		*/

		/*
			first try to minimise the dialog -- kSPDialogWidgetID
		*/
		PMReal r1(200);
		PMReal r2(440 - 165 ); //PMReal r2(423 - 165 );

		IControlView * dialogControlView  = panelControlData->FindWidget(kSPDialogWidgetID);
		if(dialogControlView)
			dialogControlView->Resize(PMPoint(r1 , r2));

		//hide first froup panel 
		IControlView * groupPanelControlView  = panelControlData->FindWidget(kFirstGroupPanelWidgetID);
		if(groupPanelControlView)
		{
			groupPanelControlView->HideView();
		}

		//hide milestone text ...........
		IControlView * FilterByMilestoneTextView  = panelControlData->FindWidget(kFilterByMilestoneTextWidgetID);
		if(FilterByMilestoneTextView)
		{
			FilterByMilestoneTextView->HideView();
		}

		//move designer milestone text widget ...........
		IControlView * FilterByDesignerMilestoneTextView  = panelControlData->FindWidget(kFilterByDesignerMilestoneTextWidgetID);
		if(FilterByDesignerMilestoneTextView)
		{
			FilterByDesignerMilestoneTextView->MoveTo(PMPoint(PMReal(50) ,PMReal(42)));
			//FilterByDesignerMilestoneTextView->MoveTo(PMPoint(PMReal(50) ,PMReal(22)));
		}

		//kFilterByDesignerActionsRadioWidgetID
		IControlView * FilterByDesignerActionsRadioWidgetView  = panelControlData->FindWidget(kFilterByDesignerActionsRadioWidgetID);
		if(FilterByDesignerActionsRadioWidgetView)
		{
			//FilterByDesignerActionsRadioWidgetView->MoveTo(PMPoint(PMReal(25) ,PMReal(22)));
			FilterByDesignerActionsRadioWidgetView->MoveTo(PMPoint(PMReal(25) ,PMReal(42)));
			InterfacePtr<ITriStateControlData> radiobuttonstate(FilterByDesignerActionsRadioWidgetView , IID_ITRISTATECONTROLDATA);
			radiobuttonstate->Select();
		}

		//move second group panel widget ...
		IControlView * secondGroupPanelControlView  = panelControlData->FindWidget(kSecondGroupPanelWidgetID);
		if(secondGroupPanelControlView)
		{
			//secondGroupPanelControlView->MoveTo(PMPoint(PMReal(5) ,PMReal(43)));
			secondGroupPanelControlView->MoveTo(PMPoint(PMReal(5) ,PMReal(63)));
		}

		/*
			disable listbox .....
		*/
		listBox->Disable();
		//listBox->Resize(PMPoint(PMReal(0) , PMReal(0)));
		listBox->HideView();

		/*
			disable FilterByMilestonesRadiobutton ...
		*/
		IControlView * FilterByMilestonesRadioView  = panelControlData->FindWidget( kFilterByMilestonesRadioWidgetID);
		if(FilterByMilestonesRadioView)
		{
			FilterByMilestonesRadioView->Disable();
			FilterByMilestonesRadioView->HideView();
		}

		/*
			disable roleDropDownView
		*/
		IControlView *	roleDropDownView = panelControlData->FindWidget(kRoleDropDownWidgetID);
		if(roleDropDownView)
		{
			roleDropDownView->Disable();
			roleDropDownView->HideView();
		}
		
		/*
			disable userDropDownView
		*/
		IControlView *	userDropDownView = panelControlData->FindWidget(kUserDropDownWidgetID);
		if(userDropDownView)
		{
			InterfacePtr<IDropDownListController> userDropDownListController(userDropDownView , UseDefaultIID());
			InterfacePtr<IStringListControlData> userDropDownListControlData(userDropDownListController, UseDefaultIID());
			userDropDownListControlData->Clear(kFalse, kFalse);
			userDropDownListController->Select(-1);
			userDropDownView->Disable();
			userDropDownView->HideView();
		}

		/*
			disable dueRadioWidgetView
		*/
		IControlView * dueRadioWidgetView = panelControlData->FindWidget(kDueRadioWidgetID);
		if(dueRadioWidgetView)
		{
			dueRadioWidgetView->Disable();
			dueRadioWidgetView->HideView();
		}

		/*
			disable completeRadioWidgetView
		*/
		IControlView * completeRadioWidgetView = panelControlData->FindWidget(kCompleteRadioWidgetID);
		if(dueRadioWidgetView)
		{
			completeRadioWidgetView->Disable();
			completeRadioWidgetView->HideView();
		}
		
		/*
				kShowAllProductsRadioWidgetID
				move ShowAllProductsRadioWidget up
		*/
		IControlView * ShowAllProductsRadioWidgetView = panelControlData->FindWidget(kShowAllProductsRadioWidgetID);
		if(ShowAllProductsRadioWidgetView)
		{
			ShowAllProductsRadioWidgetView->MoveTo(PMPoint(PMReal(25) ,PMReal(22)));
			//ShowAllProductsRadioWidgetView->MoveTo(PMPoint(PMReal(25) ,PMReal(230)));
			if(displayAll == kTrue){
				InterfacePtr<ITriStateControlData> radiobuttonstate(ShowAllProductsRadioWidgetView , IID_ITRISTATECONTROLDATA);
				radiobuttonstate->Select();

				IControlView * NewProductsCheckWidgetView = panelControlData->FindWidget(kNewProductsCheckWidgetID);
				if(!NewProductsCheckWidgetView)
				{
					//CA("NewProductsCheckWidgetView is nil");
					return;
				}

				//CA("-- 3 $$ ");
				IControlView * AddToSpreadWidgetView = panelControlData->FindWidget(kAddToSpreadWidgetID);
					if(!AddToSpreadWidgetView)
				{
					//CA("AddToSpreadWidgetView is nil");
					return;
				}

				//CA("-- 4 $$ ");
				IControlView * UpdateSpreadWidgetView = panelControlData->FindWidget(kUpdateSpreadWidgetID);
				if(!UpdateSpreadWidgetView)
				{
					//CA("UpdateSpreadWidgetView is nil");
					return;
				}
					
				//CA("-- 5 $$ ");
				IControlView * UpdateCopyCheckView = panelControlData->FindWidget(kUpdateCopyCheckWidgetID);
				if(!UpdateCopyCheckView)
				{
					//CA("UpdateCopyCheckView is nil");
					return;
				}
				 
				//CA("-- 6 $$ ");
				IControlView * UpdateArtCheckView  = panelControlData->FindWidget(kUpdateArtCheckWidgetID);
				if(!UpdateArtCheckView)
				{
					//CA("UpdateArtCheckView is nil");
					return;
				}
				 
				//CA("-- 7 $$ ");
				IControlView * UpdateItemCheckView = panelControlData->FindWidget(kUpdateItemCheckWidgetID);
				if(!UpdateItemCheckView)
				{
					//CA("UpdateItemCheckView is nil");
					return;
				}
			
				//CA("-- 8 $$ ");
				IControlView *	DeleteFromSpreadWidgetView = panelControlData->FindWidget(kDeleteFromSpreadWidgetID);
				if(!DeleteFromSpreadWidgetView)
				{
					//CA("DeleteFromSpreadWidgetView is nil");
					return;
				}

				IControlView *	StarCheckBoxWidgetView = panelControlData->FindWidget(kStarCheckBoxWidgetID);
				if(!StarCheckBoxWidgetView)
				{
					//CA("StarCheckBoxWidgetView is nil");
					return;
				}
				InterfacePtr<ITriStateControlData>NewProductsCheckWidgetTriState(NewProductsCheckWidgetView,UseDefaultIID());
				InterfacePtr<ITriStateControlData>AddToSpreadWidgetTriState(AddToSpreadWidgetView,UseDefaultIID());
				InterfacePtr<ITriStateControlData>UpdateCopyCheckWidgetTriState(UpdateCopyCheckView,UseDefaultIID());
				InterfacePtr<ITriStateControlData>UpdateArtCheckWidgetTriState (UpdateArtCheckView ,UseDefaultIID());
				InterfacePtr<ITriStateControlData>UpdateItemCheckWidgetTriState(UpdateItemCheckView,UseDefaultIID());
				InterfacePtr<ITriStateControlData>UpdateSpreadWidgetTriState (UpdateSpreadWidgetView ,UseDefaultIID());
				InterfacePtr<ITriStateControlData>DeleteFromSpreadWidgetTriState(DeleteFromSpreadWidgetView,UseDefaultIID());
				InterfacePtr<ITriStateControlData>StarCheckBoxWidgetTriState(StarCheckBoxWidgetView,UseDefaultIID());

				NewProductsCheckWidgetTriState->Deselect();
				AddToSpreadWidgetTriState->Deselect();
				UpdateCopyCheckWidgetTriState->Deselect();
				UpdateArtCheckWidgetTriState->Deselect();
				UpdateItemCheckWidgetTriState->Deselect();
				UpdateSpreadWidgetTriState->Deselect();
				DeleteFromSpreadWidgetTriState->Deselect();
				StarCheckBoxWidgetTriState->Deselect();

				NewProductsCheckWidgetView->Disable();
				AddToSpreadWidgetView->Disable();
				UpdateSpreadWidgetView->Disable();
				UpdateCopyCheckView->Disable();
				UpdateArtCheckView->Disable();
				UpdateItemCheckView->Disable();
				DeleteFromSpreadWidgetView->Disable();
				StarCheckBoxWidgetView->Disable();
				
			}
		}
		
		/*
			kShowAllTextWidgetID
			move up show all text 
		*/
		IControlView * ShowAllTextWidgetView = panelControlData->FindWidget(kShowAllTextWidgetID);
		if(ShowAllTextWidgetView)
		{
			ShowAllTextWidgetView->MoveTo(PMPoint(PMReal(50) ,PMReal(26)));
			//ShowAllTextWidgetView->MoveTo(PMPoint(PMReal(50) ,PMReal(230)));
		}

		/*
			move clear , cancle and ok button up ....
		*/
		//---------------------------------------------------------------------------------------//
		IControlView * okView = panelControlData->FindWidget(kFilterButtonWidgetID);
		if(okView)
		{
			okView->MoveTo(PMPoint(PMReal(5) ,PMReal(248)));
		}

		IControlView * clearView = panelControlData->FindWidget(kDialogClearButtonWidgetID);
		if(clearView)
		{
			clearView->MoveTo(PMPoint(PMReal(71) ,PMReal(248)));
		}

		IControlView * cancleView = panelControlData->FindWidget(kCancelButtonWidgetID);
		if(cancleView)
		{
			cancleView->MoveTo(PMPoint(PMReal(137) ,PMReal(248)));
		}
		//--------------------------------------------------------------------------------------//
	}
		
	if(result)
	{
		/*
			Enable listbox .....
		*/
		listBox->Enable();

		/*
			Enable FilterByMilestonesRadiobutton ...
		*/
		IControlView * FilterByMilestonesRadioView  = panelControlData->FindWidget( kFilterByMilestonesRadioWidgetID);
		if(FilterByMilestonesRadioView)
			FilterByMilestonesRadioView->Enable();

		/*
			Enable roleDropDownView
		*/
		IControlView *	roleDropDownView = panelControlData->FindWidget(kRoleDropDownWidgetID);
		if(roleDropDownView)
			roleDropDownView->Enable();
		
		/*
			Enable userDropDownView
		*/
		IControlView *	userDropDownView = panelControlData->FindWidget(kUserDropDownWidgetID);
		if(userDropDownView)
		{
			InterfacePtr<IDropDownListController> userDropDownListController(userDropDownView , UseDefaultIID());
			//userDropDownListController->Select(-1);
			userDropDownView->Enable();
		}
		/*
			Enable dueRadioWidgetView
		*/
		IControlView * dueRadioWidgetView = panelControlData->FindWidget(kDueRadioWidgetID);
		if(dueRadioWidgetView)
			dueRadioWidgetView->Enable();

		/*
			Enable completeRadioWidgetView
		*/
		IControlView * completeRadioWidgetView = panelControlData->FindWidget(kCompleteRadioWidgetID);
		if(dueRadioWidgetView)
			completeRadioWidgetView->Enable();
	}
	//CA(" 2 ");

	listHelper.EmptyCurrentListBox(panelControlData, 2);
	//listHelper.AddElement(listBox , as, kSPRFltOptTextWidgetID,0);

	bool16 isUserLoggedIn = ptrIAppFramework->getLoginStatus();
	if(!isUserLoggedIn)
	{
		return;
	}
	/*
	else
	{
		return;
	}
	*/

	//CA(" --- calling app framework method --- ");

	VectorMilestonePtr	Milestoneptr = nil;
	
	Milestoneptr = ptrIAppFramework->MilestoneCache_getMilestonesByRetrieveAccess(1);

	//CA(" 4 ");

	if(Milestoneptr)
	{
		VectorMilestoneModel::iterator it;
		int32 i = 0;
		//CA(" 7 ");
		listHelper.EmptyCurrentListBox(panelControlData,2);
		//CA(" 8 ");
		samitvflist.clear();
		//CA(" 9 ");
		it = Milestoneptr->begin();
		//CA(" 10 ");
		//PMString v("the mileston ptr is size ");
		//v.AppendNumber((*Milestoneptr).size());
		
		//if(it == nil)
		//{
		//	//CA(" empty list , returning ");
		//	return;
		//}

		for(it = Milestoneptr->begin() ; it != Milestoneptr->end() ; it++)
		{
				//CA(" 11 ");
				//listHelper.AddElement(listBox , it->getName(), kSPRFltOptTextWidgetID, i);
				FilterData rData;
				PMString name("");
				//name.AppendNumber(it->getCode());
				name = it->getCode();
				//CA(" 12 ");
				if(/*IsNewSectionSelected*/1) // commented on 7 feb 2006 ..
				{
					//CA(" 13 in if ");
					if(result)
						listHelper.AddElement(listBox , /*name*/  it->getName(), kSPRFltOptTextWidgetID, i);
					i++;
					rData.isSelected = kTrue;
					rData.milestone_id = it->getId();
					samitvflist.push_back(rData);
				}
				else
				{
					//CA(" 14 in else ");
					if(result)
					    listHelper.AddElement(listBox , it->getName(), kSPRFltOptTextWidgetID, i);
					//CA(" 14.1 ");
					//i++;
					if(tempMileStoneListBoxValues.size() <= 0)
						continue ;
					rData.isSelected = tempMileStoneListBoxValues[i].isSelected;
					//CA(" 14.2 ");
					rData.milestone_id =tempMileStoneListBoxValues[i].milestone_id ;
					//CA(" 14.3 ");
					samitvflist.push_back(rData);
					//CA(" 14 .4 ");
				}
				
				//CA(" 15 ");
				samitvflist.push_back(rData);
				//CA(" 16");
				i++;

		}//---- for loop ends here .....									
//12-april
		if(Milestoneptr)
			delete Milestoneptr;
//12-april
	}
			
}

/* ValidateFields
*/
WidgetID SPDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.   
	return result;
}

/* ApplyFields
*/
void SPDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}

//  Code generated by DollyXs code generator
