//========================================================================================
//  
//  $File: $
//  
//  Owner: Sagar Hagawne
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
// Project includes:
#include "IDataSprayer.h"
#include "SPID.h"
#include "FilterData.h"
#include "CAlert.h"
#include "SPActionComponent.h"
#include "SDKListBoxHelper.h"
#include "IAppFramework.h"
#include "MediatorClass.h"
#include "ISpecialChar.h"
#include "PublicationNode.h"
#include "IListBoxController.h"
#include "IListControlData.h"
#include "ITriStateControlData.h"
#include "SPSelectionObserver.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "RoleData.h"
#include "UserData.h"
#include "vector"
// Chetan --
#include "SPPRImageHelper.h"  
#include "SPXLibraryItemGridEH.h"
// Chetan --
#include "AcquireModalCursor.h"
#include "SDKListBoxHelper.h"
#define CA(x)	CAlert::InformationAlert(x)
#define YP(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

extern FilterDataList samitvflist;
extern PublicationNodeList pNodeDataList; 
extern bool8 FilterFlag;


vector<bool8> GlobalDesignerActionFlags;
//vector<bool8> retainDesignerActionFlagsValues;
vector<RoleData>	vRoleData;
vector<UserData>    vUserData;
bool8 milestone_selected;

bool8 ShowAllProductsRadioTriStateFlag ;
bool8 FilterByDesignerActionsRadioTriStateFlag ;
bool8 FilterByMilestonesRadioTriStateFlag ;

extern bool8 IsNewSectionSelected;
extern int32 global_project_level ;
extern int32 global_subsection_id ;
extern int32 global_classID;

bool8 completeRadioSelected;
FilterDataList tempMileStoneListBoxValues;
int32 selecteduserID,selectedUserRow,selectedroleID,selectedRoleRow;
int32 flag;
extern int32 CurrentSelectedSubSection;
extern int32 CurrentSelectedSection;
extern int32 ListFlag; // Global Flag when ListFlag = 0 select All, 1 = Products Only, 2 = Items Only.
extern int32 global_lang_id ;
extern int32 CurrentSelectedProductRow ;
extern int32 CurrentSelectedPublicationID;
extern bool16 isItemHorizontalFlow;
// Chetan --
extern bool16 isThumb;
extern K2Vector<PMString> imageVector2 ;
VectorPubObjectMilestoneValuePtr ptrForMilestoneThumbnail;

extern bool16 displayAll;




// Chetan --
/** Implements IObserver based on the partial implementation CDialogObserver.
	@ingroup content sprayer*/

class SPDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SPDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~SPDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);

		void populateRoleDropDownList();
		void populateUserDropDownList(int32 roleId);
		void populateProductList(int32 classId);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(SPDialogObserver, kSPDialogObserverImpl)

/* AutoAttach
*/
void SPDialogObserver::AutoAttach()
{
	static bool8 IsAutoAttachCalledFirstTime = kTrue;
	
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return;

	do
	{
		//CA("Inside AutoAttach");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			if(!panelControlData)
			{
				//CA("panelcontroldata nil");
				break;
			}
			
			SDKListBoxHelper listHelperInsideAutoAttach(this, kSPPluginID);
			IControlView * listBoxView = listHelperInsideAutoAttach.FindCurrentListBox(panelControlData, 2);

			if(!listBoxView)
			{
				//CA("No list box found");
				break;
			}
			InterfacePtr<IListControlData>iListControlData(listBoxView,UseDefaultIID());
			if(!iListControlData)
			{
				//CA("No list box controldata found");
				break;
			}

			//CA("Inside AutoAttach 3");
			PMString len("The length of the list is ");
			len.AppendNumber(iListControlData->Length());
			//CA(len);
			do{
					if(iListControlData->Length() <= 0)
					{
						//CA("iListControlData length is <=0");
						break;
					}
			for(int i=0;i< iListControlData->Length();i++)
			{
				listHelperInsideAutoAttach.CheckUncheckRow(listBoxView, i, samitvflist[i].isSelected);
				//samitvflist[i].isSelected=kTrue;
			}
			}while(kFalse);

			tempMileStoneListBoxValues.clear();
			tempMileStoneListBoxValues = samitvflist;

			//ShowAllProductsRadioTriStateFlag = kFalse;
			//FilterByDesignerActionsRadioTriStateFlag = kFalse;
			//FilterByMilestonesRadioTriStateFlag = kFalse;
			//CA("Inside AutoAttach 5");

		//ASSERT(panelControlData);

		/*if(!panelControlData) 
		{
			break;
		}*/
		// Attach to other widgets you want to handle dynamically here.
		 AttachToWidget(kFilterButtonWidgetID,      IID_ITRISTATECONTROLDATA,	panelControlData);
		 AttachToWidget(kDialogClearButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		 AttachToWidget(kUpdateSpreadWidgetID,		IID_ITRISTATECONTROLDATA,	panelControlData);

		 AttachToWidget(kFilterByMilestonesRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		 AttachToWidget(kFilterByDesignerActionsRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		 AttachToWidget(kShowAllProductsRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);

		 //New Addition added by Yogesh on 23.2.05 on 1.12 am
		AttachToWidget(kRoleDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,	panelControlData);
		AttachToWidget(kUserDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,	panelControlData);
		AttachToWidget(kCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		
		 //ended by Yogesh on 23.2.05 on 1.12 am
																	
		 //start 01/03/05
		 //AttachToWidget(kNewProductsCheckWidgetID,		IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kUpdateCopyCheckWidgetID,			IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kUpdateArtCheckWidgetID,			IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kUpdateItemCheckWidgetID,			IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kAddToSpreadRadioWidgetID,		IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kUpdateSpreadRadioWidgetID,		IID_ITRISTATECONTROLDATA,	panelControlData);
		 //AttachToWidget(kDeleteFromSpreadRadioWidgetID,   IID_ITRISTATECONTROLDATA,	panelControlData);
		 //end 01/03/05

		 //will be put inside a function
		// TriState state = kSelected;
	/*	IControlView * MerchItemsCompleteCheckWidgetView = panelControlData->FindWidget(kMerchItemsCompleteWidgetID);
		IControlView * ArtCompleteCheckWidgetView = panelControlData->FindWidget(kArtCompleteWidgetID);
		IControlView * CopyCompleteCheckWidgetView = panelControlData->FindWidget(kCopyCompleteWidgetID);
		IControlView * DesignCompleteCheckWidgetView = panelControlData->FindWidget(kDesignCompleteWidgetID);
	*/
		
		if(IsAutoAttachCalledFirstTime)
		{
			populateRoleDropDownList(); //
			//CA(" LIST POPULATED ");
			IsAutoAttachCalledFirstTime = kFalse;
		}

		


			//CA(" K--1 ");
			IControlView * NewProductsCheckWidgetView = panelControlData->FindWidget(kNewProductsCheckWidgetID);
			if(!NewProductsCheckWidgetView)
			{
				//CA("NewProductsCheckWidgetView is nil");
				break;
			}
		 
			//CA(" K--2 ");
			 IControlView * AddToSpreadWidgetView = panelControlData->FindWidget(kAddToSpreadWidgetID);
			if(!AddToSpreadWidgetView)
			{
				//CA("AddToSpreadWidgetView is nil");
				break;
			}

			//CA(" K--3 ");
			IControlView * UpdateSpreadWidgetView = panelControlData->FindWidget(kUpdateSpreadWidgetID);
			if(!UpdateSpreadWidgetView)
			{
				//CA("UpdateSpreadWidgetView is nil");
				break;
			}

			//CA(" K--4 ");
			IControlView * UpdateCopyCheckView = panelControlData->FindWidget(kUpdateCopyCheckWidgetID);
			if(!UpdateCopyCheckView)
			{
				//CA("UpdateCopyCheckView is nil");
				break;
			}

			//CA(" K--5 ");
			IControlView * UpdateArtCheckView  = panelControlData->FindWidget(kUpdateArtCheckWidgetID);
			if(!UpdateArtCheckView)
			{
				//CA("UpdateArtCheckView is nil");
				break;
			}

			//CA(" K--6 ");
			 IControlView * UpdateItemCheckView = panelControlData->FindWidget(kUpdateItemCheckWidgetID);
			 if(!UpdateItemCheckView)
			{
				//CA("UpdateItemCheckView is nil");
				break;
			}

			//CA(" K--7 ");
			IControlView *	DeleteFromSpreadWidgetView = panelControlData->FindWidget(kDeleteFromSpreadWidgetID);
			if(!DeleteFromSpreadWidgetView)
			{
				//CA("DeleteFromSpreadWidgetView is nil");
				break;
			}

			IControlView *	StarCheckBoxWidgetView = panelControlData->FindWidget(kStarCheckBoxWidgetID);
			if(!StarCheckBoxWidgetView)
			{
				//CA("StarCheckBoxWidgetView is nil");
				break;
			}


			//CA(" K--8");
			IControlView * FilterByMilestonesRadioView  = panelControlData->FindWidget( kFilterByMilestonesRadioWidgetID);
			if(!FilterByMilestonesRadioView)
			{
				//CA("FilterByMilestonesRadioView is nil");
				break;
			}

			//CA(" K--9 ");
			IControlView * FilterByDesignerActionsRadioView = panelControlData->FindWidget( kFilterByDesignerActionsRadioWidgetID);
			if(!FilterByDesignerActionsRadioView)
			{
				//CA("FilterByDesignerActionsRadioView is nil");
				break;
			}

			//CA(" K--10 ");
			IControlView *	ShowAllProductsRadioView = panelControlData->FindWidget(kShowAllProductsRadioWidgetID);
			if(!ShowAllProductsRadioView)
			{
				//CA("ShowAllProductsRadioView is nil");
				break;
			}

			//CA(" K--11 ");
			IControlView * completeRadioView = panelControlData->FindWidget(kCompleteRadioWidgetID);
			if(!completeRadioView)
			{
				//CA("completeRadioView is nil");
				break;
			}

			//CA(" K--12 ");
			IControlView * dueRadioView = panelControlData->FindWidget(kDueRadioWidgetID);	
			if(!dueRadioView)
			{
				//CA("dueRadioView  is nil");
				break;
			}

			//CA(" K--13 ");
			IControlView *	roleDropDownView = panelControlData->FindWidget(kRoleDropDownWidgetID);
			if(!roleDropDownView)
			{
				//CA("roleDropDownView is nil");
				break;
			}

			//CA(" K--14 ");
			InterfacePtr<IStringListControlData> roleDropListData(roleDropDownView,UseDefaultIID());	
			if (roleDropListData==nil)
			{
				//CA("roleDropListData is nil");
				break;
			}

			//CA(" K--15 ");
			InterfacePtr<IDropDownListController> roleDropListController(roleDropListData, UseDefaultIID());
			if (roleDropListController==nil)
			{
               // CA("roleDropListController is nil");
				break;
				
			}

			/*	InterfacePtr<IStringListControlData> roleDropDownListControlData(roleDropListController, UseDefaultIID());
				if(roleDropDownListControlData==nil)
				break;
			*/
			//CA(" K--16 ");
			IControlView *	userDropDownView = panelControlData->FindWidget(kUserDropDownWidgetID);
			if(!userDropDownView)
			{
				//CA("userDropDownView is nil");
				break;
			}

			//CA(" K--17 ");
			InterfacePtr<IStringListControlData> userDropDownListData(userDropDownView,UseDefaultIID());	
			if (userDropDownListData==nil)
			{
				//CA("userDropDownListData is nil");
				break;
			}

			//CA(" K--18 ");
			InterfacePtr<IDropDownListController> userDropDownController(userDropDownListData, UseDefaultIID());
			if (userDropDownController==nil)
			{
				//CA("userDropDwonController is nil");			
				break;
			}

			/*InterfacePtr<IStringListControlData> userDropDownListControlData(userDropDownController, UseDefaultIID());
		
			if(userDropDownListControlData==nil)
			break;*/		
		 
			//CA(" K--19 ");
			InterfacePtr<ITriStateControlData>NewProductsCheckWidgetTriState(NewProductsCheckWidgetView,UseDefaultIID());
			 if(!NewProductsCheckWidgetTriState)
			{
				//CA("NewProductsCheckWidgetTriState is nil");
				break;
			}

			//CA(" K--20 ");
			InterfacePtr<ITriStateControlData>AddToSpreadWidgetTriState(AddToSpreadWidgetView,UseDefaultIID());
			if(!AddToSpreadWidgetTriState)
			{
				//CA("AddToSpreadWidgetTriState is nil");
				break;
			}

			//CA(" K--21 ");
			InterfacePtr<ITriStateControlData>UpdateSpreadWidgetTriState(UpdateSpreadWidgetView,UseDefaultIID()); 
			 if(!UpdateSpreadWidgetTriState)
			{
				//CA("UpdateSpreadWidgetTriState is nil");
				break;
			}

			//CA(" K--22 ");
			InterfacePtr<ITriStateControlData>UpdateCopyCheckWidgetTriState(UpdateCopyCheckView,UseDefaultIID());
			if(!UpdateCopyCheckWidgetTriState)
			{
				//CA("UpdateCopyCheckWidgetTriState is nil");
				break;
			}

			//CA(" K--23 ");
			InterfacePtr<ITriStateControlData>UpdateArtCheckWidgetTriState (UpdateArtCheckView ,UseDefaultIID());
			if(!UpdateArtCheckWidgetTriState)
			{
				//CA("UpdateArtCheckWidgetTriState is nil");
				break;
			}

			//CA(" K--24 ");
			InterfacePtr<ITriStateControlData>UpdateItemCheckWidgetTriState(UpdateItemCheckView,UseDefaultIID());
			if(!UpdateItemCheckWidgetTriState)
			{
				//CA("UpdateItemCheckWidgetTriState is nil");
				break;
			}

			//CA(" K--25 ");
			InterfacePtr<ITriStateControlData>DeleteFromSpreadWidgetTriState(DeleteFromSpreadWidgetView,UseDefaultIID());
			if(!DeleteFromSpreadWidgetTriState)
			{
				//CA("DeleteFromSpreadWidgetTriState is nil");
				break;
			}

			InterfacePtr<ITriStateControlData> StarCheckBoxWidgetTriState(StarCheckBoxWidgetView,UseDefaultIID());
			if(!StarCheckBoxWidgetTriState)
			{
				//CA("StarCheckBoxWidgetTriState is nil");
				break;
			}

			//CA(" K--26 ");
			InterfacePtr<ITriStateControlData>FilterByMilestonesRadioTriState (FilterByMilestonesRadioView  ,UseDefaultIID());
			if(!FilterByMilestonesRadioTriState)
			{
				//CA("FilterByMilestonesRadioTriState is nil");
				break;
			}

			//CA(" K--27 ");
			InterfacePtr<ITriStateControlData>FilterByDesignerActionsRadioTriState(FilterByDesignerActionsRadioView,UseDefaultIID());
			if(!FilterByDesignerActionsRadioTriState)
			{
				//CA("FilterByDesignerActionsRadioTriState is nil");
				break;
			}

			//CA(" K--28 ");
			InterfacePtr<ITriStateControlData>ShowAllProductsRadioTriState(ShowAllProductsRadioView ,UseDefaultIID());
			if(!ShowAllProductsRadioTriState)
			{
				//CA("ShowAllProductsRadioTriState is nil");
				break;
			}

			//CA(" K--29 ");
			InterfacePtr<ITriStateControlData>completeRadioTriState(completeRadioView,UseDefaultIID());
			if(!completeRadioTriState)
			{
				//CA("completeRadioTriState is nil");
				break;
			}

			//CA(" K--30 ");
			InterfacePtr<ITriStateControlData>dueRadioTriState(dueRadioView ,UseDefaultIID());
			if(!dueRadioTriState)
			{
				//CA("dueRadioTriState is nil");
				break;
			}

		

		 //added by Yogesh on 10/2/5
			//CA(" K--31 ");
		 if(IsNewSectionSelected)
		 {
		 
		 if(NewProductsCheckWidgetTriState)
			 NewProductsCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(AddToSpreadWidgetTriState)
			 AddToSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(UpdateSpreadWidgetTriState)
			 UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(UpdateCopyCheckWidgetTriState)
			 UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(UpdateArtCheckWidgetTriState)
			 UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kSelected);
		 if(UpdateItemCheckWidgetTriState)
			UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(UpdateSpreadWidgetTriState)
			 UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
		 if(DeleteFromSpreadWidgetTriState)
			 DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);		
		  if(StarCheckBoxWidgetTriState)
			 StarCheckBoxWidgetTriState->SetState(ITriStateControlData::kSelected);

		 if(FilterByMilestonesRadioTriState)
			 FilterByMilestonesRadioTriState->SetState(ITriStateControlData::kSelected);
		 //completeRadioTriState->SetState(ITriStateControlData::kSelected);	
		 if(dueRadioTriState)
			 dueRadioTriState->SetState(ITriStateControlData::kSelected);
		 if(roleDropListController)
			 roleDropListController->Select(0);
		 if(userDropDownController)
			 userDropDownController->Select(0);
		 selectedRoleRow=0;
		 selectedUserRow=0;

			
			 tempMileStoneListBoxValues.clear();

		 	for(int i=0;i<=iListControlData->Length();i++)
			{
				if(samitvflist.size() <= 0)
					break ;
				listHelperInsideAutoAttach.CheckUncheckRow(listBoxView, i, true);
				//CA(" k -- 32 ");
				samitvflist[i].isSelected=kTrue;
			}
			
		 }
		 else
		 {
			 samitvflist = tempMileStoneListBoxValues;
			 for(int i=0;i<=iListControlData->Length();i++)
			{
				if(samitvflist.size() <= 0)
					break ;
				listHelperInsideAutoAttach.CheckUncheckRow(listBoxView, i, samitvflist[i].isSelected);
				//CA(" k -- 33 ");
			}

			 //New product flag
			 if(GlobalDesignerActionFlags.size()<=0)
			 {
				 //CA("No data in GlobalDesignerActionFlags");
				 break;
			 }
			 
			 if(GlobalDesignerActionFlags[0])
			 {
				  NewProductsCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
			 }
			 else				
			 {
				  NewProductsCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }

			// CA("Inside AutoAttach 10.1");
			 //Addtospread flag
			 if(GlobalDesignerActionFlags[1])
			 {
				 AddToSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
		
			 }
			 else
				 AddToSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);

			// CA("Inside AutoAttach 10.2");
			 //Updatespreadwidget
			 if(GlobalDesignerActionFlags[2])
			 {
				   UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
			 }
			 else
					UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
		
			 //updatecopy
			 if(GlobalDesignerActionFlags[3])
			 {
				 UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
		
			 }
			 else
				UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 
			 //updateart
			 if(GlobalDesignerActionFlags[4])
			 {
				  UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kSelected);
		 
			 }
			 else
				 UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kUnselected);
		
			
			 //udpate item
			 if(GlobalDesignerActionFlags[5])
			 {
				  UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kSelected);
				 
			 }
			 else
				UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
			
			
			 //delete from spread
			 if(GlobalDesignerActionFlags[6])
			 {
				 DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kSelected);
			 }
			 else
				 DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);

			 //Starred Product
			 if(GlobalDesignerActionFlags[7])
			 {
				 StarCheckBoxWidgetTriState->SetState(ITriStateControlData::kSelected);
			 }
			 else
				 StarCheckBoxWidgetTriState->SetState(ITriStateControlData::kUnselected);
			
			 
			if(completeRadioSelected)
			{
				 completeRadioTriState->SetState(ITriStateControlData::kSelected);	

			}
			else
				 completeRadioTriState->SetState(ITriStateControlData::kUnselected);	

			 
			roleDropListController->Select(selectedRoleRow);
			userDropDownController->Select(selectedUserRow);



			 /*if(!GlobalDesignerActionFlags[1])
			 {
				  AddToSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }
			 if(!GlobalDesignerActionFlags[2])
			 {
				  UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }
			 if(!retainDesignerActionFlagsValues[3])
			 {
				  UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }
			 if(!retainDesignerActionFlagsValues[4])
			 {
				   UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kUnselected);
			 }
			 if(!retainDesignerActionFlagsValues[5])
			 {
				  UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }

			 if(!retainDesignerActionFlagsValues[6])
			 {
				 DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
			 }*/

			 //CA("Inside AutoAttach 11");


			 

		 }
		
		 //ended by Yogesh on 10/2/5

		/*UpdateCopyCheckView->Disable();
		UpdateArtCheckView->Disable(); 
		UpdateItemCheckView->Disable();*/

		//CA("Inside AutoAttach DONE ");							

	} while (kFalse);
}

/* AutoDetach
*/
void SPDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
	//	CA("Inside AutoDetach");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.
	    DetachFromWidget(kFilterButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		DetachFromWidget(kDialogClearButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		DetachFromWidget(kUpdateSpreadWidgetID,		IID_ITRISTATECONTROLDATA,	panelControlData);


		 DetachFromWidget(kFilterByMilestonesRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		 DetachFromWidget(kFilterByDesignerActionsRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		 DetachFromWidget(kShowAllProductsRadioWidgetID,IID_ITRISTATECONTROLDATA,	panelControlData);
		 DetachFromWidget(kRoleDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,	panelControlData);
		 DetachFromWidget(kUserDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,	panelControlData);
		 DetachFromWidget(kCancelButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		 

		 } while (kFalse);
}

/* Update
*/
void SPDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	//CA("Inside dialog update ");
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	do
	{		
		//CA(" -- 1 $$ ");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) 
		{
			break;
		}
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) 
		{
			break;
		}

		//////////////////////////////////////////////////////////////////////////////
		/*SDKListBoxHelper listHelperInsideUpdate(this, kSPPluginID);
		IControlView * listBoxView = listHelperInsideUpdate.FindCurrentListBox(panelControlData, 2);
		if(!listBoxView)
		{
			CA("listBoxView is nil");
			break;
		}*/		
			//CA("-- 2 $$ ");
		IControlView * NewProductsCheckWidgetView = panelControlData->FindWidget(kNewProductsCheckWidgetID);
		if(!NewProductsCheckWidgetView)
		{
			//CA("NewProductsCheckWidgetView is nil");
			break;
		}

		//CA("-- 3 $$ ");
		IControlView * AddToSpreadWidgetView = panelControlData->FindWidget(kAddToSpreadWidgetID);
			if(!AddToSpreadWidgetView)
		{
			//CA("AddToSpreadWidgetView is nil");
			break;
		}

		//CA("-- 4 $$ ");
		IControlView * UpdateSpreadWidgetView = panelControlData->FindWidget(kUpdateSpreadWidgetID);
		if(!UpdateSpreadWidgetView)
		{
			//CA("UpdateSpreadWidgetView is nil");
			break;
		}
			
		//CA("-- 5 $$ ");
		IControlView * UpdateCopyCheckView = panelControlData->FindWidget(kUpdateCopyCheckWidgetID);
		if(!UpdateCopyCheckView)
		{
			//CA("UpdateCopyCheckView is nil");
			break;
		}
		 
		//CA("-- 6 $$ ");
		IControlView * UpdateArtCheckView  = panelControlData->FindWidget(kUpdateArtCheckWidgetID);
		if(!UpdateArtCheckView)
		{
			//CA("UpdateArtCheckView is nil");
			break;
		}
		 
		//CA("-- 7 $$ ");
		IControlView * UpdateItemCheckView = panelControlData->FindWidget(kUpdateItemCheckWidgetID);
		if(!UpdateItemCheckView)
		{
			//CA("UpdateItemCheckView is nil");
			break;
		}
	
		//CA("-- 8 $$ ");
		IControlView *	DeleteFromSpreadWidgetView = panelControlData->FindWidget(kDeleteFromSpreadWidgetID);
		if(!DeleteFromSpreadWidgetView)
		{
			//CA("DeleteFromSpreadWidgetView is nil");
			break;
		}

		IControlView *	StarCheckBoxWidgetView = panelControlData->FindWidget(kStarCheckBoxWidgetID);
		if(!StarCheckBoxWidgetView)
		{
			//CA("StarCheckBoxWidgetView is nil");
			break;
		}

		//CA("-- 9 $$ ");
		IControlView * FilterByMilestonesRadioView  = panelControlData->FindWidget( kFilterByMilestonesRadioWidgetID);
		if(!FilterByMilestonesRadioView)
		{
			//CA("FilterByMilestonesRadioView is nil");
			break;
		}
		
		//CA("-- 10 $$ ");
		IControlView * FilterByDesignerActionsRadioView = panelControlData->FindWidget( kFilterByDesignerActionsRadioWidgetID);
		if(!FilterByDesignerActionsRadioView)
		{
			//CA("FilterByDesignerActionsRadioView is nil");
			break;
		}
		 
		//CA("-- 11 $$ ");
		IControlView *	ShowAllProductsRadioView = panelControlData->FindWidget(kShowAllProductsRadioWidgetID);
		if(!ShowAllProductsRadioView)
		{
			//CA("ShowAllProductsRadioView is nil");
			break;
		}

		//New Addition added by Yogesh on 23.2.05 on 1.19 am
			//CA("-- 12 $$ ");
		IControlView *	roleDropDownView = panelControlData->FindWidget(kRoleDropDownWidgetID);
		if(!roleDropDownView)
		{
			//CA("roleDropDownView is nil");
			break;
		}
		
		//CA("-- 13 $$ ");
		IControlView *	userDropDownView = panelControlData->FindWidget(kUserDropDownWidgetID);
		if(!userDropDownView)
		{
			//CA("userDropDownView is nil");
			break;
		}

		IControlView * dueRadioWidgetView = panelControlData->FindWidget(kDueRadioWidgetID);
		if(!dueRadioWidgetView)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No dueRadioWidgetView");
			break;
		}
		IControlView * completeRadioWidgetView = panelControlData->FindWidget(kCompleteRadioWidgetID);
		if(!completeRadioWidgetView)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No completeRadioWidgetView");
			break;
		}

		IControlView * clusterPanelWidgetView = panelControlData->FindWidget(kClusterPanelWidgetID);
		if(!clusterPanelWidgetView)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No clusterPanelWidgetView");
			break;
		}
		 //ended by Yogesh on 23.2.05 on 1.19 am
		//CA("-- 14 $$ ");
		InterfacePtr<ITriStateControlData>NewProductsCheckWidgetTriState(NewProductsCheckWidgetView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>AddToSpreadWidgetTriState(AddToSpreadWidgetView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>UpdateCopyCheckWidgetTriState(UpdateCopyCheckView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>UpdateArtCheckWidgetTriState (UpdateArtCheckView ,UseDefaultIID());
		InterfacePtr<ITriStateControlData>UpdateItemCheckWidgetTriState(UpdateItemCheckView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>UpdateSpreadWidgetTriState (UpdateSpreadWidgetView ,UseDefaultIID());
		InterfacePtr<ITriStateControlData>DeleteFromSpreadWidgetTriState(DeleteFromSpreadWidgetView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>StarCheckBoxWidgetTriState(StarCheckBoxWidgetView,UseDefaultIID());
		

		InterfacePtr<ITriStateControlData>FilterByMilestonesRadioTriState (FilterByMilestonesRadioView  ,UseDefaultIID());
		InterfacePtr<ITriStateControlData>FilterByDesignerActionsRadioTriState(FilterByDesignerActionsRadioView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>ShowAllProductsRadioTriState(ShowAllProductsRadioView ,UseDefaultIID());
		

		InterfacePtr<ITriStateControlData>dueRadioTriState(dueRadioWidgetView,UseDefaultIID());
		InterfacePtr<ITriStateControlData>completeRadioTriState(completeRadioWidgetView ,UseDefaultIID());
		

		//New Addition added by Yogesh on 23.2.05 on 1.21 am
		//InterfacePtr<ITriStateControlData>ShowAllProductsRadioTriState(ShowAllProductsRadioView ,UseDefaultIID());
		//ended by Yogesh on 23.2.05 on 1.21 am

		//CA("-- 15 $$ ");

		if( !(NewProductsCheckWidgetTriState &&
			AddToSpreadWidgetTriState &&
			UpdateCopyCheckWidgetTriState &&
			UpdateArtCheckWidgetTriState &&
			UpdateItemCheckWidgetTriState &&
			UpdateSpreadWidgetTriState &&
			DeleteFromSpreadWidgetTriState && StarCheckBoxWidgetTriState && dueRadioTriState && completeRadioTriState))
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::NO tristate control data for widgets available");
			break;
		}
		
		
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		if (theSelectedWidget == kRoleDropDownWidgetID && theChange == kPopupChangeStateMessage)
		{
			//ShowAllProductsRadioTriStateFlag = kFalse;
			//CA(" -- RoleDropDownWidgetID clicked  -- ");
			
			InterfacePtr<IControlView>roleDropDownControlView(theSubject,UseDefaultIID());
			if(roleDropDownControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropDownControlView");
				break;
			}

			InterfacePtr<IStringListControlData> roleDropListData(roleDropDownControlView,UseDefaultIID());	
			if (roleDropListData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropListData");			
				break;
			}

			InterfacePtr<IDropDownListController> roleDropListController(roleDropListData, UseDefaultIID());

			if (roleDropListController==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropListController");						
				break;
			}

			int32 selectedRow = roleDropListController->GetSelected();

				if(selectedRow <1)
				{
					IControlView * userDropDownListView = panelControlData->FindWidget(kUserDropDownWidgetID);
					if(userDropDownListView==nil)
					{
						ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No userDropDownListView");											
						break;		
					}

			
					InterfacePtr<IDropDownListController> userDropDownListController(userDropDownListView, UseDefaultIID());
					if(userDropDownListController == nil)
					{
						ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No userDropDownListController");											
						break;
					}
	
					InterfacePtr<IStringListControlData> userDropDownListControlData(userDropDownListController, UseDefaultIID());
		
					if(userDropDownListControlData==nil)
					{
						ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No userDropDownListControlData");																
						break;
					}
		
					userDropDownListControlData->Clear(kFalse, kFalse);

					InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
					if(ptrIAppFramework == nil)
            			return;
					bool16 result = ptrIAppFramework->CONFIGCACHE_getPM_DisplayWorkflow();

					PMString str1("--All Users --");

					if(result)
					{
						userDropDownListControlData->AddString(str1);
						userDropDownListController->Select(0);
					}
					else
					{
						userDropDownListController->Select(-1);
					}

					break; // -- commented on 14 feb 2006 ---
				}


				//if(vRoleData.size()>=(selectedRow - 2)+1))
				int32 roleId = vRoleData[selectedRow-1].getrole_id();
				populateUserDropDownList(roleId);				
			
		}


		if (theSelectedWidget == kFilterByMilestonesRadioWidgetID && theChange==kTrueStateMessage)
		{
			//ShowAllProductsRadioTriStateFlag = kFalse;
			//CA(" -- kFilterByMilestonesRadioWidgetID -- ");
			SDKListBoxHelper listHelperInsideUpdate(this, kSPPluginID);
			IControlView * listBoxView = listHelperInsideUpdate.FindCurrentListBox(panelControlData, 2);					
			if(!listBoxView)
			{
				//CA("No list box found");
				break;
			}

			listBoxView->Enable();
			clusterPanelWidgetView->Enable();
			NewProductsCheckWidgetView->Disable();
			AddToSpreadWidgetView->Disable();
			UpdateSpreadWidgetView->Disable();
			UpdateCopyCheckView->Disable();
			UpdateArtCheckView->Disable();
			UpdateItemCheckView->Disable();
			DeleteFromSpreadWidgetView->Disable();
			StarCheckBoxWidgetView->Disable();

		}


		if (theSelectedWidget == kFilterByDesignerActionsRadioWidgetID && theChange==kTrueStateMessage)
		{
			//ShowAllProductsRadioTriStateFlag = kFalse;
			//CA(" -- kFilterByDesignerActionsRadioWidgetID -- ");

			SDKListBoxHelper listHelperInsideUpdate(this, kSPPluginID);
			IControlView * listBoxView = listHelperInsideUpdate.FindCurrentListBox(panelControlData, 2);
			if(!listBoxView)
			{
				//CA("No list box found");
				break;
			}

			displayAll = kFalse;

			listBoxView->Disable();
			clusterPanelWidgetView->Disable();
			NewProductsCheckWidgetView->Enable();
			AddToSpreadWidgetView->Enable();
			UpdateSpreadWidgetView->Enable();
			DeleteFromSpreadWidgetView->Enable();
			StarCheckBoxWidgetView->Enable();

			if(UpdateSpreadWidgetTriState->IsSelected())
			{
				//	CA("1");
				//CA(" -- 4 -- ");
				UpdateCopyCheckView->Enable();
				UpdateArtCheckView->Enable();
				UpdateItemCheckView->Enable();
				break;
			}
			if(UpdateSpreadWidgetTriState->IsDeselected())
			{
				//CA("2");
				//CA(" -- 5 -- ");
				UpdateCopyCheckView->Disable();
				UpdateArtCheckView->Disable();
				UpdateItemCheckView->Disable();
				break;
			}

		}

		if (theSelectedWidget == kShowAllProductsRadioWidgetID && theChange==kTrueStateMessage)
		{
			//CA(" -- theSelectedWidget == kShowAllProductsRadioWidgetID -- ");
			SDKListBoxHelper listHelperInsideUpdate(this, kSPPluginID);
			IControlView * listBoxView = listHelperInsideUpdate.FindCurrentListBox(panelControlData, 2);
			if(!listBoxView)
			{
				//CA("listBoxView is nil");
				break;
			}
			displayAll = kTrue;

			NewProductsCheckWidgetTriState->Deselect();
			AddToSpreadWidgetTriState->Deselect();
			UpdateCopyCheckWidgetTriState->Deselect();
			UpdateArtCheckWidgetTriState->Deselect();
			UpdateItemCheckWidgetTriState->Deselect();
			UpdateSpreadWidgetTriState->Deselect();
			DeleteFromSpreadWidgetTriState->Deselect();
			StarCheckBoxWidgetTriState->Deselect();

			listBoxView->Disable();
			clusterPanelWidgetView->Disable();
			NewProductsCheckWidgetView->Disable();
			AddToSpreadWidgetView->Disable();
			UpdateSpreadWidgetView->Disable();
			UpdateCopyCheckView->Disable();
			UpdateArtCheckView->Disable();
			UpdateItemCheckView->Disable();
			DeleteFromSpreadWidgetView->Disable();
			StarCheckBoxWidgetView->Disable();

		}
		if (theSelectedWidget == kUpdateSpreadWidgetID) //&& theChange==kTrueStateMessage)
		{
			//CA(" -- theSelectedWidget == kUpdateSpreadWidgetID --");
			InterfacePtr<ITriStateControlData>UpdateSpreadWidgetTriState (controlView ,UseDefaultIID());

			if(!UpdateSpreadWidgetTriState)
			{
				break;
			}
			
			if(UpdateSpreadWidgetTriState->IsSelected())
			{
				//	CA("1");
				UpdateCopyCheckView->Enable();
				UpdateArtCheckView->Enable();
				UpdateItemCheckView->Enable();
				break;
			}
			if(UpdateSpreadWidgetTriState->IsDeselected())
			{
				//CA("2");
				UpdateCopyCheckView->Disable();
				UpdateArtCheckView->Disable();
				UpdateItemCheckView->Disable();
				break;
			}
		}

		if (theSelectedWidget == kDialogClearButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA(" -- 8 -- ");
			//CA("Inside clear button"); 
			
			Mediator m;
			IPanelControlData* iPanelControlData;
			iPanelControlData=m.getMainPanelCtrlData();
			if(!iPanelControlData)
			{
				//CA("No panelControlData");
				break;
			}
			//CA("Inside clear button..1");

			if(FilterByMilestonesRadioTriState->IsSelected())
			{
				//CA("FilterByMilestonesRadioTriState->IsSelected");
				SDKListBoxHelper listHelperInsideUpdateInsideClear(this, kSPPluginID);
				IControlView * listBoxView = listHelperInsideUpdateInsideClear.FindCurrentListBox(panelControlData, 2);
				if(!listBoxView)
				{
					//CA("No listbox view");
					break;
				}
				InterfacePtr<IListControlData>iListControlData(listBoxView,UseDefaultIID());
				if(!iListControlData)
				{
					//CA("No listcontroldata");
					break;
				}
				//start 26.2.05
				if((iListControlData->Length())<=0)
				{
					break;
				}
				//end 26.2.05
				if(!(tempMileStoneListBoxValues.size()==iListControlData->Length()))
				{
					//CA("tempMileStoneListBoxValues is nil");
					break;
				}
				for(int i=0; i < iListControlData->Length();i++)
				{
					listHelperInsideUpdateInsideClear.CheckUncheckRow(listBoxView, i, false);
					//samitvflist[i].isSelected=kFalse;
					tempMileStoneListBoxValues[i].isSelected = kFalse;
				}

				break;
				
				
			}
			else if(FilterByDesignerActionsRadioTriState->IsSelected())
			{
			
				//for newproduct
					/*if(NewProductsCheckWidgetTriState->IsSelected())
					{
						
						//anyFlagSelected = kTrue;
						retainDesignerActionFlagsValues.push_back(kTrue);
					}
					else if(NewProductsCheckWidgetTriState->IsDeselected())
					{
									
						retainDesignerActionFlagsValues.push_back(kFalse);
					}
					//for addtospread
					if(AddToSpreadWidgetTriState->IsSelected())
					{
						//anyFlagSelected = kTrue;
						retainDesignerActionFlagsValues.push_back(kTrue);
					}

					else if(AddToSpreadWidgetTriState->IsDeselected())
					{
						retainDesignerActionFlagsValues.push_back(kFalse);
					}
					
					//for update spread
					if(UpdateSpreadWidgetTriState->IsSelected())//Designer Action UpdateSpread selected
					{
						//anyFlagSelected = kTrue;
						retainDesignerActionFlagsValues.push_back(kTrue);
					}

					else if(UpdateSpreadWidgetTriState->IsDeselected())//Designer Action UpdateSpread selected
					{
						//anyFlagSelected = kTrue;
						retainDesignerActionFlagsValues.push_back(kFalse);
					}

					//for update copy
					if(UpdateCopyCheckWidgetTriState->IsSelected())
						{
								//CA("Selected 8");//Designer action UpdateCopy selected
								retainDesignerActionFlagsValues.push_back(kTrue);	
						}
							
					else if(UpdateCopyCheckWidgetTriState->IsDeselected())
						{
								//CA("UnSelected 8");//Designer Action UpdateCopy not selected.
							retainDesignerActionFlagsValues.push_back(kFalse);
						}
							
					//for update art
					if(UpdateArtCheckWidgetTriState ->IsSelected())
						{
								//CA("Selected 9");//Designer action UpdateArt selected
							retainDesignerActionFlagsValues.push_back(kTrue);	
						}

					else if(UpdateArtCheckWidgetTriState ->IsDeselected())
						{
								//CA("UnSelected 9");//Designer Action UpdateArt not selected.
							retainDesignerActionFlagsValues.push_back(kFalse);
						}

					//for update item table
					if(UpdateItemCheckWidgetTriState->IsSelected())
						{
								//CA("Selected 10");//Designer Action UpdateItemCheck selected.
							retainDesignerActionFlagsValues.push_back(kTrue);	
						}

					else if(UpdateItemCheckWidgetTriState->IsDeselected())
						{
							//CA("UnSelected 10");//Designer Action UpdateItemCheck not selected.
							retainDesignerActionFlagsValues.push_back(kFalse);
						}	
		
					//for delete from spread
					if(DeleteFromSpreadWidgetTriState->IsSelected())
					{
						//anyFlagSelected = kTrue;
						//CA("Selected 11");//Designer Action DeleteFromSpread selected.
						retainDesignerActionFlagsValues.push_back(kTrue);
					}

					else if(DeleteFromSpreadWidgetTriState->IsDeselected())
					{
						//CA("UnSelected 11");//Designer Action DeleteFromSpread not selected.
						retainDesignerActionFlagsValues.push_back(kFalse);
					}*/

				NewProductsCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
				AddToSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
				UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
				UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kUnselected);
				UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
				UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
				DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
				StarCheckBoxWidgetTriState->SetState(ITriStateControlData::kUnselected);
				break;
			}
		}


		/*
			ok button click processed ehre ---------------------------------
		*/
		if((theSelectedWidget == kFilterButtonWidgetID  || theSelectedWidget == kOKButtonWidgetID)&& theChange==kTrueStateMessage)
		{
// Chetan --		
			InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
			if(ptrImageHelper == nil)
			{
				CA("ProductImages plugin not found ");
				break;
			}
			ptrImageHelper->EmptyImageGrid();
			imageVector2.clear();
		////
			Mediator md;
			IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
			if(iPanelControlData == nil)
			{
				CA("panelCntrlData is nil");
				break;
			}
		/////
			IControlView* listBox = iPanelControlData->FindWidget(kSPListBoxWrapperGroupPanelWidgetID);
			//IControlView* lstWidget = panelControlData->FindWidget( kSPListBoxWidgetID);			
			IControlView* thumb = iPanelControlData->FindWidget(kSPThumbPnlWidgetID);
			if(isThumb){
				//ptrImageHelper->EmptyImageGrid();
				listBox ->Hide ();
				//lstWidget->HideView();
				thumb ->Show ();
			}
			else
			{
				/*ptrImageHelper->EmptyImageGrid();*/
				thumb->Hide ();
				listBox->ShowView();
				//lstWidget->ShowView();
			}
// Chetan --
			SPSelectionObserver spSelectionObj(this);
			do
			{
				//added by Yogesh on 10/2/5
				//tempMileStoneListBoxValues = samitvflist;				
				samitvflist = tempMileStoneListBoxValues;
				IsNewSectionSelected = kFalse;
			
				AcquireWaitCursor adb;					
				adb.Animate();
				if(!(md.getAssignBtnView() && md.getGreenFilterBtnView()) )
				{
					//CA("No controlView of the buttons from the mediator class");
					break;
				}

				/*if(FilterByMilestonesRadioTriState->IsSelected())
					{
						md.getAssignBtnView()->ShowView();		
						md.getGreenFilterBtnView()->HideView();	
						md.getAssignBtnView()->Enable();
					}
				*/
				//ended by Yogesh on 10/2/5
				//start 4.2.5
				//Following variable is define globally
				vector<bool8> DesignerActionFlags;// = nil;
					//end
				//GlobalDesignerActionFlags.clear();
				DesignerActionFlags.clear();
				bool8 anyFlagSelected = kFalse;
				//Start of Designer Actions.

				if(NewProductsCheckWidgetTriState->IsSelected())
				{
					//anyFlagSelected = kTrue;
					DesignerActionFlags.push_back(kTrue);
				}

				if(NewProductsCheckWidgetTriState->IsDeselected())
				{
					DesignerActionFlags.push_back(kFalse);
				}

				if(AddToSpreadWidgetTriState->IsSelected())
				{
					//anyFlagSelected = kTrue;
					DesignerActionFlags.push_back(kTrue);
				}

				if(AddToSpreadWidgetTriState->IsDeselected())
				{
					DesignerActionFlags.push_back(kFalse);
				}
					
				if(UpdateSpreadWidgetTriState->IsSelected())//Designer Action UpdateSpread selected
				{
					//anyFlagSelected = kTrue;
					DesignerActionFlags.push_back(kTrue);	
					if(UpdateCopyCheckWidgetTriState->IsSelected())
					{
						//CA("Selected 8");//Designer action UpdateCopy selected
						DesignerActionFlags.push_back(kTrue);	
					}
					
					if(UpdateCopyCheckWidgetTriState->IsDeselected())
					{
						//CA("UnSelected 8");//Designer Action UpdateCopy not selected.
						DesignerActionFlags.push_back(kFalse);
					}
					

					if(UpdateArtCheckWidgetTriState ->IsSelected())
					{
						//CA("Selected 9");//Designer action UpdateArt selected
						DesignerActionFlags.push_back(kTrue);	
					}

					if(UpdateArtCheckWidgetTriState ->IsDeselected())
					{
						//CA("UnSelected 9");//Designer Action UpdateArt not selected.
						DesignerActionFlags.push_back(kFalse);
					}


					if(UpdateItemCheckWidgetTriState->IsSelected())
					{
						//CA("Selected 10");//Designer Action UpdateItemCheck selected.
						DesignerActionFlags.push_back(kTrue);	
					}

					if(UpdateItemCheckWidgetTriState->IsDeselected())
					{
						//CA("UnSelected 10");//Designer Action UpdateItemCheck not selected.
						DesignerActionFlags.push_back(kFalse);
					}
				}

				if(UpdateSpreadWidgetTriState->IsDeselected())
				{
					//Designer Action UpdateSpread not selected. This will disable 
					//the 3 designer actions.i.d updatecopy,updateart and updateitem
					DesignerActionFlags.push_back(kFalse);
					DesignerActionFlags.push_back(kFalse);
					DesignerActionFlags.push_back(kFalse);
					DesignerActionFlags.push_back(kFalse);
				}		
		
				if(DeleteFromSpreadWidgetTriState->IsSelected())
				{
					//anyFlagSelected = kTrue;
					//CA("Selected 11");//Designer Action DeleteFromSpread selected.
					DesignerActionFlags.push_back(kTrue);
				}
							

				if(DeleteFromSpreadWidgetTriState->IsDeselected())
				{
					//CA("UnSelected 11");//Designer Action DeleteFromSpread not selected.
					DesignerActionFlags.push_back(kFalse);
				}
				
				if(StarCheckBoxWidgetTriState->IsSelected())
				{
					//anyFlagSelected = kTrue;
					//CA("Selected 11");//Designer Action DeleteFromSpread selected.
					DesignerActionFlags.push_back(kTrue);
				}
				
				if(StarCheckBoxWidgetTriState->IsDeselected())
				{
					//CA("UnSelected 11");//Designer Action DeleteFromSpread not selected.
					DesignerActionFlags.push_back(kFalse);
				}
				//CA("Inside Filter button..2");
				//5.4.05
				/*
				SPActionComponent Acc(this);
				Acc.CloseDialog();
				*/

				FilterFlag=kTrue;
				//CA("Inside Filter button..3");
				//SPSelectionObserver addProductNow(this);

				//addProductNow.AddProductsToListbox(DesignerActionFlags );
				GlobalDesignerActionFlags.clear();
				GlobalDesignerActionFlags = DesignerActionFlags;

				//PublicatioNode pNode will be used as a element in the PublicationNodeList pNodeDataList.
				///////////////////////////////////////////
				PublicationNode pNode;
				Mediator md;					
			
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					//CAlert::InformationAlert("Err ptrIAppFramework is nil");						
					break;
				}
				bool16 isONEsource = kFalse;			
				isONEsource=ptrIAppFramework->get_isONEsourceMode();

				int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");		
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));		
				////if(!iConverter)
				//{
				//	CA("No iconverter");
				//	break;
				////}/
				VectorPubObjectMilestoneValuePPtr VectorFamilyInfoValuePtr = nil;	
				//VectorPubObjectValuePPtr VectorFamilyInfoValuePtr = nil ;
				PMString milestoneid("");
				bool8 check=FALSE;

				milestone_selected=kFalse;
				  
				for(int32 i=0;i<samitvflist.size();i++)//
				{						
					if(samitvflist[i].isSelected==kTrue)
					{
						milestone_selected=kTrue;
						if(check==TRUE)
						milestoneid.Append(",");
						milestoneid.AppendNumber(samitvflist[i].milestone_id);
						check=TRUE;
					}
				}

				//completeRadioWidgetView,dueRadioWidgetView,userDropDownView,roleDropDownView

				if(completeRadioTriState->IsSelected())
				{
					completeRadioSelected = kTrue;
				}
				else
				{
					completeRadioSelected = kFalse;
				}
				InterfacePtr<IStringListControlData> roleDropListData(roleDropDownView,UseDefaultIID());	
				if (roleDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropListData");																					
					break;
				}
				InterfacePtr<IDropDownListController> roleDropListController(roleDropListData, UseDefaultIID());

				if (roleDropListController==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropListController");					
					break;
				}

				selectedRoleRow = roleDropListController->GetSelected();
				//PMString srow("The selected row is");
				//srow.AppendNumber(selectedRoleRow);
				//CA(srow);

				if(selectedRoleRow >1)
				{
					selectedroleID = vRoleData[selectedRoleRow-1].getrole_id();
					//PMString sroleid("The selected role id is");
					//sroleid.AppendNumber(selectedroleID);
					//CA(sroleid);
				}
				else
				selectedroleID = -1;//for nil
				InterfacePtr<IStringListControlData> userDropListData(userDropDownView,UseDefaultIID());	
				if (roleDropListData==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropListData");					
					break;
				}

				InterfacePtr<IDropDownListController> userDropListController(userDropListData, UseDefaultIID());

				if (userDropListController==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No userDropListController");										
					break;
				}

				selectedUserRow=userDropListController->GetSelected();

				if(selectedUserRow >0)
				{
					selecteduserID = vUserData[selectedUserRow-1].getuser_id();
				}
				else
					selecteduserID = -1;//for nil

				int32 curSelSubecId;
				curSelSubecId = md.getCurrSectionID();

				if(FilterByMilestonesRadioTriState->IsSelected())
				{
					if(md.getAssignBtnView()==nil)
					{
						//CA("Dialog Observer::m.getAssignBtnView()==nil");
					}
					else						
						md.getAssignBtnView()->HideView();		

					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();	
						md.getGreenFilterBtnView()->Enable();
					}

					FilterByMilestonesRadioTriStateFlag = kTrue;
					ShowAllProductsRadioTriStateFlag = kFalse;
					FilterByDesignerActionsRadioTriStateFlag = kFalse;

					DesignerActionFlags[0]=DesignerActionFlags[1]=DesignerActionFlags[2]=DesignerActionFlags[3]=DesignerActionFlags[4]=DesignerActionFlags[5]=DesignerActionFlags[6]=kFalse;
					if(milestone_selected)
					{
							//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GrabCString(),kTrue);
						//29/10/05
						//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GrabCString(),completeRadioSelected, selectedroleID,selecteduserID);
						curSelSubecId = /*global_subsection_id*/ md.getCurrSubSectionID(); 
						//VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GrabCString(),completeRadioSelected, selectedroleID,selecteduserID); //Cs3
						VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(curSelSubecId,const_cast<char*>(milestoneid.GrabCString()/*GetPlatformString().c_str()*/),completeRadioSelected, selectedroleID,selecteduserID); //Cs4
						FilterFlag = kFalse;

					}
					else
					{
							//CA("Inside FilterButton 5.2");
							curSelSubecId = /*global_subsection_id*/md.getCurrSubSectionID() ; 
							VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
							//VectorFamilyInfoValuePtr = nil;
					}
				}

				if(ShowAllProductsRadioTriState->IsSelected())
				{				
					//CA("ShowAllProductsRadioTriState->IsSelected()");
					FilterByMilestonesRadioTriStateFlag =kFalse;
					ShowAllProductsRadioTriStateFlag = kTrue;
					FilterByDesignerActionsRadioTriStateFlag = kFalse;
					
					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->HideView();	
					}
					
					if(md.getAssignBtnView()==nil)
					{
						//CA("Dialog Observer::md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Enable();
					}
					
					curSelSubecId = /*global_subsection_id*/ md.getCurrSubSectionID() ; 
					if(isONEsource == kTrue)
					{
						curSelSubecId = global_classID;
						/*VectorObjectInfoPtr vectorObjectValuePtr = NULL;
						PMString language_name("");
						int32 language_id = -1 ;
						InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
						if(ptrIClientOptions == nil)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
						{
							//CAlert::ErrorAlert("Interface for IClientOptions not found.");
								break;
						}
						language_id = ptrIClientOptions->getDefaultLocale(language_name);
						vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(curSelSubecId,language_id);*/

						/*SPSelectionObserver sproduct(this);
						sproduct.populateProductListforSection(curSelSubecId);*/
						
						//this->populateProductList(curSelSubecId);
						spSelectionObj.populateProductListforSection(curSelSubecId);
						FilterFlag = kFalse;
						break;

					}

					DesignerActionFlags[0]=DesignerActionFlags[1]=DesignerActionFlags[2]=DesignerActionFlags[3]=DesignerActionFlags[4]=DesignerActionFlags[5]=DesignerActionFlags[6]=kFalse;
					if(isONEsource == kFalse)
					{
						//CA("1234");

						
						spSelectionObj.populateProductListforSection(/*curSelSubecId*/CurrentSelectedSubSection);
						FilterFlag = kFalse;
						//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
						break;
					}					
						
				}

				if(FilterByDesignerActionsRadioTriState->IsSelected())
				{				
					FilterByMilestonesRadioTriStateFlag = kFalse;
					ShowAllProductsRadioTriStateFlag = kFalse;
					FilterByDesignerActionsRadioTriStateFlag = kTrue;
					//md.getAssignBtnView()->ShowView();		
					//md.getGreenFilterBtnView()->HideView();	
					//md.getAssignBtnView()->Enable();
					
					if(!(DesignerActionFlags[0] || DesignerActionFlags[1] || DesignerActionFlags[2] || DesignerActionFlags[3] || DesignerActionFlags[4] || DesignerActionFlags[5] || DesignerActionFlags[6] || DesignerActionFlags[7]))
					{	
						//CA("SPDialogObserver 1736 \n inside if Before CAlling populateProductList ...");
						if(md.getGreenFilterBtnView()==nil)
						{
							//CA("Dialog Observer::md.getGreenFilterBtnView()==nil");
						}
						else
						{
							md.getGreenFilterBtnView()->HideView();	
						}
						
						if(md.getAssignBtnView()==nil)
						{
							//CA("Dialog Observer::md.getAssignBtnView()==nil");
						}
						else
						{
							md.getAssignBtnView()->ShowView();
							md.getAssignBtnView()->Enable();
						}

						if(isONEsource == kFalse)
							curSelSubecId = md.getCurrSubSectionID() ; 
						else
							curSelSubecId = global_classID;
						//this->populateProductList(curSelSubecId);
						spSelectionObj.populateProductListforSection(curSelSubecId);
						
						VectorFamilyInfoValuePtr = nil;
						FilterFlag = kFalse;
						break;

					}
					else 
					{
						//CA("SPDialogObserver 1736 \n inside else Before CAlling populateProductList ...");
						if(isONEsource == kFalse)
						{//	Commented By Rahul	

							if(md.getAssignBtnView()==nil)
							{
								//CA("Dialog Observer::md.getAssignBtnView()==nil");
							}
							else
							{
								md.getAssignBtnView()->HideView();		
							}
							
							if(md.getGreenFilterBtnView()==nil)
							{
								//CA("Dialog Observer::md.getGreenFilterBtnView()==nil");
							}
							else
							{
								md.getGreenFilterBtnView()->ShowView();	
								md.getGreenFilterBtnView()->Enable();
							}
				
							//CA("populateProductListforSectionWithDesignerActions...1825");
							//29/10/05
							//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GrabCString(),DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6]);
							curSelSubecId = /*global_subsection_id*/ md.getCurrSubSectionID() ; 
							//CA(" CALLING GetProjectProductAction_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions ");
						//Latest Comment	//VectorFamilyInfoValuePtr =	ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GrabCString(),DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6]);
									//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
							spSelectionObj.populateProductListforSectionWithDesignerActions(curSelSubecId,DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6], DesignerActionFlags[7] );

							DesignerActionFlags.clear();
							SPActionComponent Acc(this);
							Acc.CloseDialog();
							break;

						}
						else
						{	
//// Chetan --
							GlobalDesignerActionFlags.clear();
							FilterFlag = kFalse;
							NewProductsCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
							AddToSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
							UpdateCopyCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
							UpdateArtCheckWidgetTriState ->SetState(ITriStateControlData::kUnselected);
							UpdateItemCheckWidgetTriState->SetState(ITriStateControlData::kUnselected);
							UpdateSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
							DeleteFromSpreadWidgetTriState->SetState(ITriStateControlData::kUnselected);
							StarCheckBoxWidgetTriState->SetState(ITriStateControlData::kUnselected);		
//// Chetan --
							curSelSubecId = global_classID;
							//this->populateProductList(curSelSubecId);
							spSelectionObj.populateProductListforSection(curSelSubecId);

							DesignerActionFlags.clear();
							SPActionComponent Acc(this);
							Acc.CloseDialog();
							break;							
						}		

					}
				}

				pNodeDataList.clear();						
				IControlView * FirstListBoxView = md.getListBoxView();
                       						
				if(!FirstListBoxView)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No FirstListBoxView");							return;
				}
					
				SPSelectionObserver sObserver(this);	// Chetan -- 22/8/07
				PMString as;
				as.Append("Amit");
				//InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				IPanelControlData* iPanelControlData;
				iPanelControlData=md.getMainPanelCtrlData();
				
				if(!iPanelControlData)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No iPanelControlData");
					break;
				}
						
				SDKListBoxHelper listHelperInsideUpdateInsideFilter(this, kSPPluginID);

				if(VectorFamilyInfoValuePtr == nil && isONEsource == kFalse)
				{
					
					listHelperInsideUpdateInsideFilter.EmptyCurrentListBox(iPanelControlData,1);
					break;
				}

				if(VectorFamilyInfoValuePtr->size()==0 && isONEsource == kFalse)
				{
					ptrIAppFramework->LogInfo("AP7_ProductFinder::SPDialogObserver::Update::No Product Found");
					//delete VectorFamilyInfoValuePtr;
					break;
				}

				//ptrForMilestoneThumbnail = VectorFamilyInfoValuePtr;   // Chetan -- 22/8/07

				/* -- commented on 31 jan 06 ---- */

				listHelperInsideUpdateInsideFilter.EmptyCurrentListBox(iPanelControlData,1);
	
				VectorPubObjectValuePointer::iterator it1;
				VectorPubObjectValue::iterator it2;
		
				int count=0;
				bool8 none_of_the_designer_action_selected = kFalse;
				vector<PMString> vectorpNodeGetName;// = nil;
				vector<int> vectorIconCount;// = nil;
				//vector<CPbObjectValue> vectorForThumb;    // Chetan -- 22/8/07
				
				ItemMapPtr pItemMap = NULL;   //*******
				PMString itemCountString("");
				int32 itemCountNum=0;

				if(isONEsource == kFalse){
				for(it1 = VectorFamilyInfoValuePtr->begin(); it1 != VectorFamilyInfoValuePtr->end(); it1++)
				{
					///CA("inside 1st for loop");
					flag++;
//ptrForMilestoneThumbnail = VectorFamilyInfoValuePtr->;
					for(it2=(*it1)->begin(); it2 !=(*it1)->end(); it2++)
					{	//CA("inside 2nd for loop");
						//CPbObjectValue obj = (*it2);         // Chetan -- 22/8/07
						//vectorForThumb.push_back (obj);
						pNode.setLevel(it2->getObjectValue().getLevel_no());		
						pNode.setParentId(it2->getObjectValue().getParent_id());
						pNode.setSequence(it2->getIndex());
						pNode.setPubId(it2->getObjectValue().getObject_id());
						if(!iConverter)
						{
							pNode.setPublicationName(it2->getObjectValue().getName());
						}
						else
						{
							pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
						}
						
						pNode.setChildCount(it2->getObjectValue().getChildCount());
						pNode.setReferenceId(it2->getObjectValue().getRef_id());
						pNode.setTypeId(it2->getObjectValue().getObject_type_id());
						pNode.setPBObjectID(it2->getPub_object_id());
						pNode.setIsStarred(it2->getStarredFlag1());

						int32 NewProductFlag =0;
						if(it2->getNew_product() == kTrue)
						{	
							NewProductFlag =2;
							pNode.setNewProduct(1);
						}
						else
						{	NewProductFlag =1;
							pNode.setNewProduct(0);
						}

						int32 StarFlag =1;
						if(it2->getStarredFlag1()) // if Selected Green Star
							StarFlag =2;

						int icon_count = 111 ;//for default i.e all icons are hidden

						if((FilterByDesignerActionsRadioTriState->IsSelected())&&(DesignerActionFlags[0] || DesignerActionFlags[1] || DesignerActionFlags[2] || DesignerActionFlags[3] || DesignerActionFlags[4] || DesignerActionFlags[5] || DesignerActionFlags[6]))
						{
							none_of_the_designer_action_selected = kFalse;
							if(it2->getNew_product() || it2->getAdd_to_spread() || it2->getUpdate_spread() || it2->getUpdate_copy() || it2->getUpdate_art() || it2->getUpdate_item_table() ||  it2->getDelete_from_spread() )
							{
								if((it2->getNew_product()&& DesignerActionFlags[0]) || (it2->getAdd_to_spread() && DesignerActionFlags[1]) || (it2->getUpdate_spread() && DesignerActionFlags[2]) || (it2->getUpdate_copy() && DesignerActionFlags[3]) || (it2->getUpdate_art() && DesignerActionFlags[4]) || (it2->getUpdate_item_table() && DesignerActionFlags[5]) ||  (it2->getDelete_from_spread() && DesignerActionFlags[6]))
								{
									pNodeDataList.push_back(pNode);
									SPSelectionObserver sp(this);									
									//icon_count =sp.GetIconCountWhenDesignerActionSelected(it2 , DesignerActionFlags);
									icon_count = sp.GetIconCountForUnfilteredProductList(it2);
								}
								else
									continue;
							}
							else
								continue;
						}
						else
						{
							//list of unfiltered products	
							pNodeDataList.push_back(pNode);
							none_of_the_designer_action_selected = kTrue;
							icon_count = sObserver.GetIconCountForUnfilteredProductList(it2);
						}	//all icons are hidden i.e no icon
						
						//vectorpNodeGetName.push_back(pNode.getName());
						//CA("before add element");
						//listHelperInsideUpdateInsideFilter.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );
						//CA("after addelement");
						//added on 15.2.05
						//////if( (!none_of_the_designer_action_selected) && (pNodeDataList.size()>0))
						//////{
						//////	SPSelectionObserver sp(this);									
						//////	icon_count =sp.GetIconCountWhenDesignerActionSelected(it2 , DesignerActionFlags);
						//////	//vectorIconCount.push_back(icon_count);
						//////}
						if(!isThumb)
						{														
							if(itemCountNum  >1)
							{
								itemCountString.Append("(");
								itemCountString.AppendNumber(itemCountNum);
								itemCountString.Append(")");
							}
							else
								itemCountString.Append("");

							listHelperInsideUpdateInsideFilter.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );							
							listHelperInsideUpdateInsideFilter.SetDesignerAction(FirstListBoxView, count, kTrue,icon_count, StarFlag,NewProductFlag,itemCountString);//2.2.5			
							count++; 

							itemCountString.Clear();
							itemCountNum =0;

						}
						else
						{
							//CA("isThumb == kTrue");
							sObserver.showImageAsThumbnail(pNode, curSelSubecId);
						}								
					}//inner for completed
					//CA("ccc");
					//ptrForMilestoneThumbnail = &vectorForThumb;  // Chetan -- 22/8/07
				//	CA("ddd");
				}
			}
				//---------------------outer for loop end here -------------------------------------//

				/*
					now keep first row of list box selected if on of element > 1 ;
					added on 21 feb 04.38pm , tuesday ..
				*/
				//---------------------------------------------------------------------------------//
						
						InterfacePtr<IListControlData>listcontrol(FirstListBoxView , UseDefaultIID());
						if(!listcontrol)
						{
							ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No listcontrol");
							break;
						}
						if(listcontrol->Length()>0 || imageVector2.size()>0)
						{		
							bool16 isListBoxPopulated = kTrue;
							InterfacePtr<IListBoxController> listStringControlData(FirstListBoxView , UseDefaultIID());
							if(listStringControlData)
							{
								listStringControlData->Select(0);
								int32 CurrentSelectedProductRow = 0; // First Product of List      
////////// Chetan -- 22/8/07
//								VectorPubObjectMilestoneValue::iterator it2;			
//								it2 = ptrForMilestoneThumbnail->begin();
//								int32 iconCount = sObserver.GetIconCountForUnfilteredProductList (it2);
//
//								PMString str("");
//
//								bool16 isProduct = kFalse;
//								if(it2->getisProduct())
//								{
//									isProduct = kTrue;
//									if(!iConverter)
//										str = it2->getObjectValue().getName();
//									else
//										str = iConverter->translateString(it2->getObjectValue().getName());
//					
//								}
//								else
//								{
//									isProduct = kFalse;
//									if(!iConverter)
//									{
//										PMString ItemNO(it2->getItemModel().getItemNo());
//										PMString ItemDesp(it2->getItemModel().getItemDesc());
//										if(ItemDesp != "")
//										{
//											ItemNO.Append(": "+ ItemDesp);
//										}
//										str = ItemNO;
//									}
//									else
//									{
//										PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
//										PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
//										if(ItemDesp != "")
//										{
//											ItemNO.Append(": "+ ItemDesp);
//										}
//										str = ItemNO;
//									}
//								}
//
//								int32 NewProductFlag =0;
//								if(it2->getNew_product()  == kTrue){
//									NewProductFlag =2;
//								}
//								else{
//									NewProductFlag =1;
//								}
//
//								int32 StarFlag =0;
//								if(it2->getStarredFlag1()== kTrue) // if Selected Green Star
//									StarFlag =2;
//								else
//									StarFlag = 1;
//
//								SPXLibraryItemGridEH spXLibraryItemGridEHObj(this);
//								spXLibraryItemGridEHObj.SetDesignerActionThumbnail (iPanelControlData, str, isProduct, iconCount, StarFlag, NewProductFlag);
////////// Chetan --
								//-------------------------------------------------------------------------------------//
								IControlView *view1 = iPanelControlData->FindWidget(kSPRefreshWidgetID);
								if(view1)
								{
									view1->Enable();
								}

								IControlView *view2 = iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
								if(view2)
								{
									view2->Enable();
								}

								IControlView *view3 = iPanelControlData->FindWidget(kAssignButtonWidgetID);
								if(view3)
								{
									view3->Enable();
								}

								IControlView *view4 = iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
								if(view4)
								{   
									view4->Enable();
								}

								/*IControlView *view5 = iPanelControlData->FindWidget(kLocateButtonWidgetID);
								if(view5) //--COMMENTED ON 25 FEB 2006 --
								{	
									view5->Enable();
								}*/

								IControlView *view6 = iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
								if(view6)
								{	
									view6->Enable();
								}

								InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
								if(!DataSprayerPtr)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No DataSprayerPtr");
									break;
								}

								md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
								md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());
								
								int32 CurrentSelectedSection =    md.getCurrSectionID(); 
								int32 CurrentSelectedPublicationID = md.getSelectedPubId();
								int32 CurrentSelectedSubSection = md.getCurrSubSectionID();

								/*CA_NUM(987 , " current selected sub section id " , CurrentSelectedSubSection);
								CA_NUM(988 , " current selected section id " , CurrentSelectedSection);
								CA_NUM(989 , " current selected pub id " , CurrentSelectedPublicationID);*/

								DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);
				
								DataSprayerPtr->setFlow(isItemHorizontalFlow);
						
							}
							else
							{
								if(md.getSprayButtonView()){ /*CA("Disabling Spray Button.");*/
									md.getSprayButtonView()->Disable();}
								if(md.getAssignBtnView())
										md.getAssignBtnView()->Disable();
								/*if(md.getLocateWidgetView())//--COMMENTED ON 25 FEB 2006 --
										md.getLocateWidgetView()->Disable();*/
								if(md.getPreviewWidgetView())
										md.getPreviewWidgetView()->Disable();
								if(md.getSubSecSprayButtonView())
									md.getSubSecSprayButtonView()->Disable();
								if(md.getRefreshButtonView())
									md.getRefreshButtonView()->Disable();
							}
						}

						//---------------------------------------------------------------------------------//
						DesignerActionFlags.clear();

					
					}while(0);
					
					if(theSelectedWidget == kFilterButtonWidgetID){
						SPActionComponent Acc(this);
						Acc.CloseDialog();
					}
					break;
				}

		if((theSelectedWidget == kCancelButton_WidgetID  || theSelectedWidget == kCancelButtonWidgetID)&& theChange==kTrueStateMessage)
		{
			tempMileStoneListBoxValues = samitvflist;
			if(theSelectedWidget == kCancelButtonWidgetID){
				SPActionComponent Acc(this);
				Acc.CloseDialog();
			}
		}
		
	} while (kFalse);
}


//New addition added by Yogesh on 22.2.05 at 9.54 pm
void SPDialogObserver::populateRoleDropDownList()
{
	//CA("inside populateroledropdownlist");
	
	vRoleData.clear();
	
	do
	{
		//CA("1");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;
		//CA("2");

		bool16 result = ptrIAppFramework->CONFIGCACHE_getPM_DisplayWorkflow();

		InterfacePtr<IPanelControlData>iPanelControlData(this, UseDefaultIID());
		if(!iPanelControlData)
		{
			//CA("panelcontroldata nil");
			break;
		}
		//CA("3");
		IControlView * roleDropDownListView = iPanelControlData->FindWidget(kRoleDropDownWidgetID);
		if(roleDropDownListView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropDownListView");		
			break;
		}
		
		//CA("4");
		InterfacePtr<IDropDownListController> roleDropDownListController(roleDropDownListView, UseDefaultIID());
		if(roleDropDownListController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropDownListController");		
			break;
		}
		//CA("5");
		InterfacePtr<IStringListControlData> roleDropDownListControlData(roleDropDownListController, UseDefaultIID());
		
		if(roleDropDownListControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::Update::No roleDropDownListControlData");				
			break;
		}
		//CA("6");
		roleDropDownListControlData->Clear(kFalse, kFalse);
		PMString str1("--All Roles--");//index 0
		roleDropDownListControlData->AddString(str1);
		//roleDropDownListControlData->AddString("ALL");//index 1

				//CA(" calling RoleCache_getAllRoles ");

				//VectorRoleInfoValuePtr roleInfoValuePtr = ptrIAppFramework->RoleCache_getAllRoles();
				VectorRoleModelPtr roleInfoValuePtr = ptrIAppFramework->RoleCache_getAllRoles();
			    //VectorPubInfoPtr pubSubSecInfoVectPtr=ptrIAppFramework->PUBMngr_findSectionById(sectionID, TRUE);
			    //VectorPubInfoPtr pubSubSecInfoVectPtr = ptrIAppFramework->PBMngr_findSectionListByPublicationID(defPubId);
		
				if(roleInfoValuePtr)
				{
						//VectorRoleInfoValue::iterator it;
						VectorRoleModel::iterator it;
						//for(it=roleInfoValuePtr->begin(); it!=roleInfoValuePtr->end(); it++)
						for(it = roleInfoValuePtr->begin(); it != roleInfoValuePtr->end() ; it++)
						{
								int32 role_id = it->id ;
								//CA_NUM(625 , " role_id " , role_id);
								PMString role_Name = it->name ;
								//CA(" filling role dropdown list with name  " + role_Name);
								PMString role_Description = it->getDescription();						
								RoleData rData(role_id,role_Name,role_Description);
								vRoleData.push_back(rData);
								if(result)
									roleDropDownListControlData->AddString(role_Name);

								/*int32 sectid=it->getPublication_id();
								PMString pubname=it->getName();
								int32 lvl=it->getLevel_no();
								publdata.setSectionVectorInfo(sectid,pubname,lvl);
								subSectionDropListCtrlData->AddString(pubname);*/

						}

						//CA(" filling role drop down done ");
		
		}
		/*
			commented temporarly on 11 - feb -- 4.01 pm 
		*/
		if(result)
			roleDropDownListController->Select(0);

		if(!result)
			roleDropDownListController->Select(-1);

		//CA(" LEAVING POPULATE ROLE DROPDOWN LIST ");
	}
	while(kFalse);

}
//ended by Yogesh on 22.2.05 at 9.54 pm

void SPDialogObserver::populateUserDropDownList(int32 roleId)
{
	//CA("inside populateroledropdownlist");
	int noOfRows = 0;
	vUserData.clear();
	
	do
	{
		//CA("1");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;
		//CA("2");

		bool16 result = ptrIAppFramework->CONFIGCACHE_getPM_DisplayWorkflow();
		/*if(result)
			CA(" -- CONFIGCACHE_getPM_DisplayWorkflow - true "); */

		InterfacePtr<IPanelControlData>iPanelControlData(this, UseDefaultIID());
		if(!iPanelControlData)
		{
			//CA("panelcontroldata nil");
			break;
		}
		//CA("3");
		IControlView * userDropDownListView = iPanelControlData->FindWidget(kUserDropDownWidgetID);
		if(userDropDownListView==nil)
			break;
		
		//CA("4");
		InterfacePtr<IDropDownListController> userDropDownListController(userDropDownListView, UseDefaultIID());
		if(userDropDownListController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::populateUserDropDownList::No userDropDownListController");		
			break;
		}
		//CA("5");
		InterfacePtr<IStringListControlData> userDropDownListControlData(userDropDownListController, UseDefaultIID());
		
		if(userDropDownListControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::populateUserDropDownList::No userDropDownListControlData");				
			break;
		}
		//CA("6");
		userDropDownListControlData->Clear(kFalse, kFalse);
		PMString str1("--All Users--");//index 0
		if(result)
			userDropDownListControlData->AddString(str1);
		//PMString str2("All");//index 1
		//userDropDownListControlData->AddString(str2);

			//The DropDownlist adds from 1
			VectorUserModelPtr userInfoValuePtr = ptrIAppFramework->UserCache_getAllUsersByRoleId(roleId);
			//VectorUserInfoValuePtr userInfoValuePtr = ptrIAppFramework->UserCache_getAllUsersByRole(roleId);
			//VectorPubInfoPtr pubSubSecInfoVectPtr=ptrIAppFramework->PUBMngr_findSectionById(sectionID, TRUE);
			//	VectorPubInfoPtr pubSubSecInfoVectPtr = ptrIAppFramework->PBMngr_findSectionListByPublicationID(defPubId);
		
				if(userInfoValuePtr)
				{
					//VectorUserInfoValue::iterator it;
					VectorUserModel::iterator it ;
					for(it=userInfoValuePtr->begin(); it!=userInfoValuePtr->end(); it++)
					{
						
						int32 user_id = it->getId();
						PMString user_Name = it->getName();
						UserData uData(user_id,user_Name);
						vUserData.push_back(uData);

						//roleDropDownListControlData->AddString(role_Name);
						if(result)
							userDropDownListControlData->AddString(user_Name);
						/*int32 sectid=it->getPublication_id();
						PMString pubname=it->getName();
						int32 lvl=it->getLevel_no();
						publdata.setSectionVectorInfo(sectid,pubname,lvl);
						subSectionDropListCtrlData->AddString(pubname);*/

					}
				}

				if(result)
				for(int32 k = 0 ; k < userDropDownListControlData->Length();k++)
				{
					for(int32 i=(userDropDownListControlData->Length()-1);i>=0;i--)
					{
						/*
							commented temporarly on 11 feb 4.01 pm
						*/
						userDropDownListController->Select(i);
					}
				}

				if(!result)
				{
					/*CA("-- inside clear all --");*/
					userDropDownListControlData->Clear(kFalse, kFalse);
					userDropDownListController->Select(-1);
				}

	}while(kFalse);
	
}
//Added On 8/11 By Dattatray to populate only ProductList when in Filter Product Dialog show all products in section radiobutton is selected.
void SPDialogObserver::populateProductList(int32 classId)
{
	// This is for ONEsource
	VectorObjectInfoPtr vectorObjectValuePtr = NULL;
	PMString language_name("");
	int32 language_id = -1 ;
	PublicationNode pNode;
	//PublicationNodeList pNodeDataList;
	IPanelControlData* iPanelControlData;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Err ptrIAppFramework is nil");						
		return;
	}
	
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions == nil)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::populateProductList::No ptrIClientOptions");
		return;
	}
	
	language_id = ptrIClientOptions->getDefaultLocale(language_name);

	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));		
	if(iConverter == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::populateProductList::No iConverter");
		return;
	}
	
	vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(classId,language_id);
	if(vectorObjectValuePtr == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPDialogObserver::populateProductList::No vectorObjectValuePtr");
		return;
	}
	
	if(vectorObjectValuePtr->size() == 0)
	{
		//CA("vectorObjectValuePtr == 0");
		return;
	}
	
	int count=0;
	Mediator md;
    vector<PMString> vectorpNodeGetName;// = nil;
	iPanelControlData=md.getMainPanelCtrlData();
	SDKListBoxHelper listHelperInsidePopulateProductList(this, kSPPluginID);
	listHelperInsidePopulateProductList.EmptyCurrentListBox(iPanelControlData, 1);
	pNodeDataList.clear();

	PMString itemCountString("");
	int32 itemCountNum =0;
	
	IControlView * FirstListBoxView = md.getListBoxView();
    if(!FirstListBoxView)
	{
		//CA("!FirstListBoxView");
		return;
	}
	
	int32 size = static_cast<int32>(vectorObjectValuePtr->size());
	
	if(size > 0)
	 {	
		 
		VectorObjectInfoValue::iterator it2;

			
			for(it2 = vectorObjectValuePtr->begin(); it2 != vectorObjectValuePtr->end(); it2++)
			{	
				
				pNode.setPBObjectID(classId);
				pNode.setSequence(it2->getseq_order());	
				pNode.setIsProduct(1);
				pNode.setIsONEsource(kTrue);						
									
				pNode.setLevel(it2->getLevel_no());
				pNode.setParentId(it2->getclass_id());					
				pNode.setPubId(it2->getObject_id());

				if(!iConverter)
					pNode.setPublicationName(it2->getName());
				else
					pNode.setPublicationName(iConverter->translateString(it2->getName()));

				if(itemCountNum  >1)
				{
					itemCountString.Append("(");
					itemCountString.AppendNumber(itemCountNum);
					itemCountString.Append(")");
				}
				else
					itemCountString.Append("");



				int icon_count = 111 ;//for default i.e all icons are hidden
				pNode.setChildCount(it2->getChildCount());
				pNode.setReferenceId(it2->getRef_id());
				pNode.setTypeId(it2->getObject_type_id());
				pNodeDataList.push_back(pNode);
				listHelperInsidePopulateProductList.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );
				listHelperInsidePopulateProductList.SetDesignerAction(FirstListBoxView, count, 1,icon_count,0,0,itemCountString);//2.2.5 		
				count++;	
				itemCountString.Clear();	
				itemCountNum =0;										// 0,0 is passed to hide Star & New Product icon in ONEsource Mode
			}

	 }

	return ;
}


//ended by Yogesh on 22.2.05 at 9.54 pm





