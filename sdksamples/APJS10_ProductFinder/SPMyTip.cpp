#include "VCPlugInHeaders.h"

//interfaces
#include "ITip.h"
//#include "ITool.h"
#include "IControlView.h"
#include "IWidgetUtils.h"
#include "ISPXLibraryButtonData.h"

//includes
#include "PMString.h"
#include "Trace.h"
#include "HelperInterface.h"
#include "StringUtils.h"
#include "CAlert.h"
#include "SPID.h"

class SPMyTip : public ITip
{ 
	public: 
		SPMyTip(IPMUnknown *boss); 
		virtual ~SPMyTip(); 
		virtual PMString GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/); 
		virtual bool16 UpdateToolTipOnMouseMove  (  ) ; 
		DECLARE_HELPER_METHODS()
}; 

CREATE_PMINTERFACE( SPMyTip,kSPMyTipImpl )
DEFINE_HELPER_METHODS( SPMyTip )

SPMyTip::SPMyTip(IPMUnknown *boss)
	:HELPER_METHODS_INIT(boss)
{
}

SPMyTip::~SPMyTip()
{
}

PMString SPMyTip::GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/) 
{ 
	InterfacePtr<ISPXLibraryButtonData> buttonData(this, IID_ISPXLIBRARYBUTTONDATA);
	ASSERT(buttonData);		
	if (!buttonData)	
	{
		//CAlert::InformationAlert("!buttonData");
		return PMString();
	}
	
	//CAlert::InformationAlert(buttonData->GetName());
	
	return buttonData->GetName();
} 

bool16 SPMyTip::UpdateToolTipOnMouseMove( ) 
 {
	 return kTrue;
 }