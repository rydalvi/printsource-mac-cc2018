#ifndef __SPPlugInEntrypoint_h__
#define __SPPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class SPPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif