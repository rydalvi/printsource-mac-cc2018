#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "SPID.h"
#include "SPTreeModel.h"
#include "CAlert.h"
#include "MediatorClass.h"
#include "PublicationNode.h"
#include "SPTreeDataCache.h"
//#include "GlobalData.h"


#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("SPTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X);//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

SPTreeDataCache dc;

int32 SPTreeModel::root=0;
PMString SPTreeModel::publicationName;
double SPTreeModel::classId=0;
//extern TPLDataNodeList ProjectDataNodeList;
extern PublicationNodeList pNodeDataList; 

int32 RowCountForTree = -1;
//extern int32 SelectedRowNo;
static int32 NodeCount =0;


SPTreeModel::SPTreeModel()
{
	//root=-1;
	//classId=11002;
}

SPTreeModel::~SPTreeModel() 
{
}


int32 SPTreeModel::GetRootUID() const
{	
	if(classId<=0)
		return 0;// kInvalidUID;
	
	PublicationNode pNode;
	if(dc.isExist(root, pNode))
	{
		return root;
	}
	
	dc.clearMap();
	
	
	PublicationNodeList CurrentNodeList;
	CurrentNodeList.clear();

    NodeCount =0;
	CurrentNodeList = pNodeDataList;

	RowCountForTree = 0;
	PMString name("Root");
	pNode.setPubId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setPublicationName(name);
	pNode.setSequence(0);
    pNode.setTreeNodeId(root);
    pNode.setTreeParentNodeId(-2);
    
	dc.add(pNode);
    NodeCount++;
    
    //CA_NUM("Child Count:", pNode.getChildCount());
	
	int count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
				pNode = CurrentNodeList[i];
        
                pNode.setTreeNodeId(NodeCount);
                pNode.setTreeParentNodeId(root);
                //CA_NUM("NodeCount:", NodeCount);
				pNode.setParentId(root);
				pNode.setSequence(i);
				count++;
				pNode.setPubId(CurrentNodeList[i].getPubId());
				PMString ASD(CurrentNodeList[i].getName());
				pNode.setPublicationName(ASD);
				//CA(ASD);
                //CAlert::InformationAlert(ASD);
				pNode.setChildCount(0);
                pNode.setHitCount(CurrentNodeList[i].getHitCount());
				pNode.setIsProduct(CurrentNodeList[i].getIsProduct());
				pNode.setDesignerAction(CurrentNodeList[i].getDesignerAction());
				pNode.setIconCount(CurrentNodeList[i].getIconCount());
				pNode.setIsONEsource(CurrentNodeList[i].getIsONEsource());
				pNode.setChildItemCount(CurrentNodeList[i].getChildItemCount());
				pNode.setPBObjectID(CurrentNodeList[i].getPBObjectID());
				pNode.setSectionID(CurrentNodeList[i].getSectionID());
				pNode.setPublicationID(CurrentNodeList[i].getPublicationID());
                NodeCount++;

				
			dc.add(pNode);
	}
	return root;
}


int32 SPTreeModel::GetRootCount() const
{
	int32 retval=0;
	PublicationNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 SPTreeModel::GetChildCount(const int32& uid) const
{
	PublicationNode pNode;

	if(dc.isExist(uid, pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

int32 SPTreeModel::GetParentUID(const int32& uid) const
{
	PublicationNode pNode;

	if(dc.isExist(uid, pNode))
	{
		if(pNode.getParentId()==-2)
			return 0; //kInvalidUID;
		return pNode.getTreeParentNodeId();
	}
	return 0; //kInvalidUID;
}


int32 SPTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	PublicationNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, pNode))
		return pNode.getSequence();
	return -1;
}


int32 SPTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
	PublicationNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID, index, pNode))
		return pNode.getTreeNodeId();
	return 0; // kInvalidUID;
}


int32 SPTreeModel::GetNthRootChild(const int32& index) const
{
	PublicationNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getTreeNodeId();
	}
	return 0; //kInvalidUID;
}

int32 SPTreeModel::GetIndexForRootChild(const int32& uid) const
{
	PublicationNode pNode;
	if(dc.isExist(uid, pNode))
		return pNode.getSequence();
	return -1;
}

PMString SPTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	PublicationNode pNode;
	PMString name("");

	if(dc.isExist(uid, pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getName();
	}
	return name;
}


int32 SPTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	PublicationNode pNode;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode))
		return (1);
	return -1;
}
