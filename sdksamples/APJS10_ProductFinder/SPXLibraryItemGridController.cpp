
#include "VCPlugInHeaders.h"

// ----- Interfaces -----

#include "IActiveContext.h"
#include "IBooleanControlData.h"
#include "IColorViewData.h"
#include "IControlView.h"
#include "ICounterControlData.h"
#include "IDocument.h"
#include "IGridAttributes.h"
#include "ISPXLibraryButtonData.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
#include "IPanorama.h"
#include "IScrollBarPanoramaSync.h"
#include "ISession.h"
#include "ISubject.h"
#include "ITransform.h"
#include "IWidgetParent.h"
#include "IWorkspace.h"
#include "IWidgetUtils.h"
#include "ITextControlData.h"

// ----- Includes -----

#include "CreateObject.h"
#include "HelperInterface.h"
#include "K2Vector.tpp"
#include "SPXLibraryItemGridController.h"
#include "PMPoint.h"
#include "PMRect.h"
#include "Utils.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "IBoolData.h"
#include "StreamUtil.h"
#include "IPMStream.h"
#include "ISysFileData.h"
#include "K2Vector.h" 
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "TransformUtils.h"

// ----- ID.h files -----

#include "DocumentID.h"
#include "SPID.h"

#define CA(X) CAlert::InformationAlert(X);
bool16 firstTimeCheck = kFalse;
extern bool16 isThumb ;

ITextControlData* textControlDataPtr =NULL; //It is declared as a global bcoz it is used  with multilinetextwidget
const int kNoSelection = -1;
extern K2Vector<PMString> imageVector2;
extern K2Vector<PMString> typeName; //For AutoSelect and First image name.
PMString data("");
extern IControlView* listImageCtrView;
extern bool16 BrowseFolderOption1;
extern  bool16 isThroughPrintSourceMenu;
//extern bool16 flag;
//========================================================================================
// CLASS SPXLibraryItemGridController
//========================================================================================

DEFINE_HELPER_METHODS(SPXLibraryItemGridController)

CREATE_PMINTERFACE(SPXLibraryItemGridController, kSPXLibraryItemGridControllerImpl)

//========================================================================================
// METHODS SPXLibraryItemGridController
//========================================================================================

SPXLibraryItemGridController::SPXLibraryItemGridController(IPMUnknown *boss) :	HELPER_METHODS_INIT(boss),fViewType(kThumbnailView),fScrollSync(nil)
{
}

SPXLibraryItemGridController::~SPXLibraryItemGridController()
{
	if ( fScrollSync ) {
		fScrollSync->Release();
		fScrollSync = nil;
	}
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::CreateViewItemWidget
//---------------------------------------------------------------
IControlView* SPXLibraryItemGridController::CreateViewItemWidget(PMString& useName)
{//CA(__FUNCTION__);
	// Create the Thumbnail View Button widget
	IControlView* view = (IControlView*) ::CreateObject(kSPXLibraryItemButtonBoss, IID_ICONTROLVIEW);
	if(view == nil)
	{
		CA("view == nil");
		return view;
	}
	view->Init( kDefaultWidgetId, PMRect(0,0,20,20) );   //PMRect(0,0,20,20)
//CA("SPXLibraryItemGridController::CreateViewItemWidget");
	InterfacePtr<ISPXLibraryButtonData> data( view,IID_ISPXLIBRARYBUTTONDATA );
	if ( data ){
		data->SetName( useName );
	}
	return view;
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::InsertViewItemWidget
//---------------------------------------------------------------
void SPXLibraryItemGridController::InsertViewItemWidget(IControlView* viewWidget)
{
	// Add the Widget to the Grid Panel at the end
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	panelControl->AddWidget(viewWidget);
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::InsertViewItemWidgetAndUpdate
//---------------------------------------------------------------
void SPXLibraryItemGridController::InsertViewItemWidgetAndUpdate(IControlView* viewWidget, bool16 autoSelect, bool16 autoScroll,const PMString &path)
{//CA(__FUNCTION__);
	// Add the Widget to the Grid Panel at the end
	
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	
	panelControl->AddWidget(viewWidget);
	bool16 select=kTrue;
	// Code By Rahul to set Image to the grid	
	do{
		SDKFileHelper fileHelper(path);//("C:\\a\\two.jpg");
		IDFile xFile = fileHelper.GetIDFile();
	
		InterfacePtr<IBoolData> iBoolData(viewWidget, UseDefaultIID());
		if(!iBoolData){	
			//CA("!iBoolData");
			break ;
		}

		iBoolData->Set(kFalse);
		viewWidget->Invalidate();
		// below, if we could find the stream, we'll set this true
		InterfacePtr<IPMStream> iDataFileStream(StreamUtil::CreateFileStreamReadLazy(xFile));
		if (iDataFileStream == nil)
		{	//CA("iDataFileStream == nil");
			// If nil, we couldn't have opened this
			break;
		}
		iDataFileStream->Close();

		InterfacePtr<ISysFileData> iSysFileData(viewWidget , IID_ISYSFILEDATA);
		if(!iSysFileData)
		{
			//CA(" 1245  addListElementImageWidget ");
			break;

		}
		iSysFileData->Set(xFile);
		
		// It should be a good file since we got this far
		iBoolData->Set(kTrue);
		viewWidget->Invalidate();
		
	
	}while(0);
	//till here-- Rahul

	InterfacePtr<IListBoxController> listController(this, IID_ILISTBOXCONTROLLER);

			//////////////////Added By Dattatray////////////////////
	InterfacePtr<IBooleanControlData>	buttonData( viewWidget, IID_IBOOLEANCONTROLDATA );
	InterfacePtr<ISPXLibraryButtonData> btnData(viewWidget, IID_ISPXLIBRARYBUTTONDATA);
		if(btnData== nil)
		{
			//CA("No btnData");
			return;
		}
		 PMString str("");
		 str=btnData->GetName();  //Get Name of the image and its type
		 
	InterfacePtr<IWidgetParent> btnparent(viewWidget, IID_IWIDGETPARENT);
	IPMUnknown* unknownbtn=btnparent->GetParent();
		if(unknownbtn == nil)
		{
			//CA("no unknownbtn");
			return;
		 }
	InterfacePtr<IWidgetParent> gridparent(unknownbtn, IID_IWIDGETPARENT);
	IPMUnknown* unknowngrid=gridparent->GetParent();
		if(unknowngrid == nil)
		{
			//CA("no unknowngrid");
			return;
		}
	InterfacePtr<IControlView> ctr(unknowngrid,UseDefaultIID());
		if(ctr == nil)
		{
			//CA("no ctr");
			return;
		}
	InterfacePtr<IWidgetParent> panelparent(ctr, IID_IWIDGETPARENT);
	
	IPMUnknown* unknownpanel=panelparent->GetParent();
		if(unknownpanel == nil)
		{
			//CA("no unknownpanel");
			return;
		}
		
	InterfacePtr<IControlView> ctr1(unknownpanel,UseDefaultIID());
		if(ctr1 == nil)
		{
			//CA("no ctr1");
			return;
		}
	InterfacePtr<IPanelControlData> iData(ctr1,IID_IPANELCONTROLDATA);
		if(iData == nil)
		{
			//CA("no iData");
			return;
		 }
    //// Chetan -- Commented coz no multiline widget is present here..
	////IControlView* iView =iData->FindWidget(kDCNMultilineTextWidgetID);
	////	if(iView == nil)
	////	{
	////		//CA("no iView");
	////		return;
	////	}


	////	InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
	////	if(txtData == nil)
	////	{
	////		//CA("no txtData");
	////		return;
	////	}
	////	textControlDataPtr=txtData;
	
	///////////////For ListView AutoSelect///////////////////////////////////////

			InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//cs4
			if(app == NULL) 
			 { 
				// CA("No Application");
				 return;
			 }
			//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
			//if(paletteMgr == NULL) 
			//{ 
			// // CA("No IPaletteMgr");	
			//  return;
			//}
			InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
			if(panelMgr == NULL) 
			{	
				//CA("No IPanelMgr");	
				return;
			}

			IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID); // Chetan --
			if(!myPanel) 
			{
				//CA("No PnlControlView");
				return;
			}
	
			InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
			if(!panelControlData) 
			{
				//CA("No PanelControlData");
				return;
			}
		//// Chetan -- Commented coz there is no kDCNLstViewMultilineTextWidgetID
		////IControlView* lstTextWidget = panelControlData->FindWidget(kDCNLstViewMultilineTextWidgetID);
		////if(lstTextWidget== NULL)
		////{
		////	//CA("No lstTextWidget");
		////	return;
		////}
		////InterfacePtr<ITextControlData> listTextData(lstTextWidget,IID_ITEXTCONTROLDATA);
		////if(listTextData == nil)
		////{
		////	//CA("no txtData");
		////	return;
		////}
	
	/////////////////////////////////////////////////////////upto here

	// Check if this widget should be automatically selected
		
		K2Vector <PMString> :: iterator the_iterator;
		//&& (BrowseFolderOption1 == kFalse)) is commented bcoz AutoSelection is not working in Browse Image Folder so.. on 6/11/06.
		if(	(typeName.size() != 0) )//&& (BrowseFolderOption1 == kFalse))
		{			
			the_iterator = typeName.begin();
			PMString data1  = *the_iterator;
			//CA("data1 : "+ data1);
			if ( autoSelect) 		
			{

				// Deselect any previously selected widgets		
				listController->DeselectAll(kTrue,kFalse);
				
				//select zeroth widget
				//CA("selecting 0th item/product");
				listController->Select(0,kTrue,kTrue);
					
					//txtData->SetString(data1); Chetan --
				
					int ins =  data.IndexOfString("  ");
					PMString* NewString = data.Substring(ins +2 );
					
					//listTextData->SetString(*NewString); Chetan --
			}
		}
		
	 // Recompute the size of the Grid
		UpdateGridDimensions();
	
	// Reconfigure all the items
		SortItems();

	//Added By Dattatray
		listController->ScrollItemIntoView(0);
		//if(firstTimeCheck==kFalse)
		//{
		//	firstTimeCheck =kTrue;
	// If desired, scroll to make the new widget visible
	//if ( autoScroll ) ScrollIntoView( viewWidget );
			//txtData->SetString(str);
			////////upto here
			
		//}
			
	/*K2Vector <PMString> :: iterator the_iterator;
	the_iterator = typeName.begin();
	PMString data = *the_iterator;
	txtData->SetString(data);*/
	Redraw();
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::ViewItemWidgetChanged
//---------------------------------------------------------------
void SPXLibraryItemGridController::ViewItemWidgetChanged(IControlView* viewWidget,bool16 autoScroll)
{
	//CA(__FUNCTION__);
	// Reconfigure all the items
	SortItems();
	Redraw();
	
	// If desired, scroll to make the new widget visible
	if ( autoScroll ) ScrollIntoView( viewWidget );
}

void SPXLibraryItemGridController::ViewItemWidgetChanged(int32 position,bool16 autoScroll)
{
	//CA(__FUNCTION__);
	// Get the widget by position
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	IControlView* viewWidget = panelControl->GetWidget( position);
	
	// Notify the widget changed by actual widget pointer
	ViewItemWidgetChanged( viewWidget, autoScroll );
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::DeleteViewItemWidget
//---------------------------------------------------------------
void SPXLibraryItemGridController::DeleteViewItemWidget(IControlView* viewWidget)
{
	
	// Get the widget's position
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	int32 index = panelControl->GetIndex( viewWidget);
	
	// Delete by position
	DeleteViewItemWidget(index);
}


void SPXLibraryItemGridController::DeleteViewItemWidget(int32 position)
{
	// Deselect the widget
	InterfacePtr<IListBoxController> listController(this, IID_ILISTBOXCONTROLLER);
	listController->Deselect(position,kFalse,kTrue);
	
	// Remove the Widget from the Grid
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	IControlView * widgetToRemove = panelControl->GetWidget(position);
	//Added By Yatin
	if(widgetToRemove==nil)
		return;

	Utils<IWidgetUtils>()->DeleteWidgetAndChildren(widgetToRemove);
	panelControl->RemoveWidget(position);
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::DeleteViewItemWidgetAndUpdate
//---------------------------------------------------------------
void SPXLibraryItemGridController::DeleteViewItemWidgetAndUpdate(IControlView* viewWidget)
{
	// Get the widget's position
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	int32 index = panelControl->GetIndex( viewWidget);
	
	// Delete by position
	DeleteViewItemWidgetAndUpdate(index);
}


void SPXLibraryItemGridController::DeleteViewItemWidgetAndUpdate(int32 position)
{
	
	// Deselect the widget
	InterfacePtr<IListBoxController> listController(this, IID_ILISTBOXCONTROLLER);
	listController->Deselect(position,kFalse,kTrue);
	
	// Remove the Widget from the Grid
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	IControlView * widgetToRemove = panelControl->GetWidget(position);

	//Added By Yatin
	if(widgetToRemove==nil)
		return;

	Utils<IWidgetUtils>()->DeleteWidgetAndChildren(widgetToRemove);
	panelControl->RemoveWidget(position);

	// Recompute the size of the Grid
	UpdateGridDimensions();
	listController->DeselectAll();

	// Sort and Redraw
	SortItems();
	Redraw();
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::SelectViewItemWidget
//---------------------------------------------------------------
void SPXLibraryItemGridController::SelectViewItemWidget(IControlView* viewWidget)
{//CA(__FUNCTION__);
	// Get the widget's position
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	int32 index = panelControl->GetIndex( viewWidget);
	
	// Select the widget
	InterfacePtr<IListBoxController> listController(this, IID_ILISTBOXCONTROLLER);
	listController->Select(index,kTrue,kTrue);
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::DeselectViewItemWidget
//---------------------------------------------------------------
void SPXLibraryItemGridController::DeselectViewItemWidget(IControlView* viewWidget)
{
	// Get the widget's position
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	int32 index = panelControl->GetIndex( viewWidget);
	
	// Deselect the widget
	InterfacePtr<IListBoxController> listController(this, IID_ILISTBOXCONTROLLER);
	listController->Deselect(index,kTrue,kTrue);
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::UpdateView
//---------------------------------------------------------------
void SPXLibraryItemGridController::UpdateView()
{
	// Recompute the size of the Grid
	UpdateGridDimensions();
	
	// Resort the items in the view
	SortItems();
	
	// Update the Scrollbars
	UpdateScrollBars();
	
	// Invalidate the view so it gets redrawn
	InterfacePtr<IControlView> gridView(this, IID_ICONTROLVIEW);
	gridView->Invalidate();
	//firstTimeCheck = kFalse;
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::SizeChanged
//---------------------------------------------------------------
void SPXLibraryItemGridController::SizeChanged()
{
	// Recompute the size of the Grid
	UpdateGridDimensions();
	UpdateGridLayout();
	
	if ( fScrollSync ) {
		fScrollSync->SizeChanged();
		fScrollSync->UpdateScrollBars();	
	}
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::SetupScrollBarConnections
//---------------------------------------------------------------
void SPXLibraryItemGridController::SetupScrollBarConnections()
{
	// Remove any existing scrollbar connection
	if (fScrollSync) {
		fScrollSync->Release();
		fScrollSync = nil;	
	}
	
	// Get interfaces needed for the scrollbar connection
	InterfacePtr<IPanorama> gridPanorama(this, IID_IPANORAMA);
	InterfacePtr<IWidgetParent> parent( (SPXLibraryItemGridController*) this, IID_IWIDGETPARENT);
	InterfacePtr<IPanelControlData> parentPanel( parent->GetParent(), IID_IPANELCONTROLDATA);
	IControlView* scrollbarView = parentPanel->FindWidget(kSPXLibraryItemScrollBarWidgetId);
	
	// Set up scroll bar for Y direction scrolling in thumbnail grid sub panel, there is no X scroll bar
	fScrollSync = (IScrollBarPanoramaSync*) ::CreateObject(kGridScrollBarPanoramaSyncBoss, IID_ISCROLLBARPANORAMASYNC);
	fScrollSync->Init(nil, scrollbarView, gridPanorama, kFalse);
}

void SPXLibraryItemGridController::ReleaseScrollBar()
{
	if (fScrollSync ){
		ICounterControlData* scrollBarCtl = fScrollSync->GetScrollBarY();
		InterfacePtr<IControlView> scrollViewCtl( scrollBarCtl, IID_ICONTROLVIEW );
		scrollViewCtl->WindowClosed();
	}
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::UpdateScrollBars
//---------------------------------------------------------------
void SPXLibraryItemGridController::UpdateScrollBars()
{
	if ( fScrollSync ) {
		fScrollSync->SizeChanged();
		fScrollSync->UpdateScrollBars();	
	}
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::SetViewType
//---------------------------------------------------------------
void SPXLibraryItemGridController::SetViewType( ViewType viewType )
{//CA(__FUNCTION__);
	// Check if the new view type is the same as the current type
	if ( viewType == fViewType ) return;
	
	// Get the view widget at the center of the visible view
	IControlView* centerWidget = GetViewCenterViewItemWidget();
	
	// Set the new view type
	fViewType = viewType;
	
	// Change the grid dimensions and re-layout the items
	UpdateGridDimensions();
	UpdateGridLayout();
	
	// Update the Scrollbars
	UpdateScrollBars();
	
	// Invalidate the view so it gets  n
	InterfacePtr<IControlView> gridView(this, IID_ICONTROLVIEW);

	if ( fViewType == ISPXLibraryViewController::kListView ){
		
		gridView->HideView();
		if (fScrollSync ){
			ICounterControlData* scrollBarCtl = fScrollSync->GetScrollBarY();
			InterfacePtr<IControlView> scrollViewCtl( scrollBarCtl, IID_ICONTROLVIEW );
			scrollViewCtl->HideView();
		}
	}
	else{
		gridView->ShowView();
		gridView->Invalidate();
		if (fScrollSync )
		{
			ICounterControlData* scrollBarCtl = fScrollSync->GetScrollBarY();
			InterfacePtr<IControlView> scrollViewCtl( scrollBarCtl, IID_ICONTROLVIEW );
			scrollViewCtl->ShowView();
		}
	}

	// Scroll to show the old center widget
	if ( centerWidget ) ScrollIntoView( centerWidget );
}

void SPXLibraryItemGridController::SetSortOrder( SortType sortMethod )
{
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::SortItems
//---------------------------------------------------------------
void SPXLibraryItemGridController::SortItems()
{
	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	const int32 length = panelData->Length(); // length = how many widgets in the grid

	// sort items some how or other
	UpdateGridLayout();
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::Redraw
//---------------------------------------------------------------
void SPXLibraryItemGridController::Redraw()
{
	// Update the Scrollbars
	UpdateScrollBars();
	
	// Invalidate the view so it gets redrawn
	InterfacePtr<IControlView> gridView(this, IID_ICONTROLVIEW);
	gridView->Invalidate();
	

}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::UpdateGridDimensions
//---------------------------------------------------------------
void SPXLibraryItemGridController::UpdateGridDimensions()
{
	// Get the current grid cell size and border width
	InterfacePtr<IGridAttributes> gridAttributes(this, IID_IGRIDATTRIBUTES);
	PMReal borderWidth = gridAttributes->GetBorderWidth();
	PMPoint cellSize;
	int32 columns;
	int32 rows;

	// Set the cell dimensions
	cellSize.X( kSPGridCellWidth );
	cellSize.Y( kSPGridCellHeight - kSPGridDelta);
	
	// Determine the number of rows and columns in the grid
	InterfacePtr<IControlView> view(this, IID_ICONTROLVIEW);
	columns = ::ToInt32((view->GetFrame().Dimensions().X() + cellSize.X()/10.0)/(cellSize.X() + borderWidth));
	InterfacePtr<IPanelControlData> panelControl(this, IID_IPANELCONTROLDATA);
	rows = panelControl->Length() / columns;
	if (panelControl->Length() % columns != 0)
		rows++;
	
	// Update the grid attributes with the number of rows and columns
	gridAttributes->Set(PMPoint(columns, rows), cellSize, borderWidth);
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::UpdateGridLayout
//---------------------------------------------------------------
void SPXLibraryItemGridController::UpdateGridLayout()
{
	InterfacePtr<IPanelControlData> panel(this, IID_IPANELCONTROLDATA);

	// Get information about the grid dimensions
	InterfacePtr<IGridAttributes> attrs(panel, IID_IGRIDATTRIBUTES);
	const PMPoint gridSize = attrs->GetGridDimensions();
	const PMPoint cellSize = attrs->GetCellDimensions();
	const PMReal borderWidth = attrs->GetBorderWidth();
	
	// Initialize working variables
	int32 y = 0;
	int32 x = 0;
	const int32 itemCount = panel->Length();
	const int32 columns = ::ToInt32( gridSize.X() );
	
	// Loop over each row
	for (int32 index = 0; index < itemCount; y++){
		PMRect frame;
		// Loop over each column in the row
		for (; x < columns && index < itemCount; x++, index++){
			IControlView* view = panel->GetWidget(index);
			frame = PMRect(x * (cellSize.X() + borderWidth),
						   y * (cellSize.Y() + borderWidth),
						   (x + 1) * (cellSize.X() + borderWidth),
						   (y + 1) * (cellSize.Y() + borderWidth));
			view->SetFrame(frame);
		}
		x = 0;
	}
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::GetViewCenterViewItemWidget
//---------------------------------------------------------------
IControlView* SPXLibraryItemGridController::GetViewCenterViewItemWidget()
{
	// Check first that there are actually view widgets in the panel
	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	const int32 length = panelData->Length();
	if ( length == 0 ) return nil;
	if ( !fScrollSync ) return nil;
	
	// Get the center point of the visible area of the panorama
	IPanorama* panorama = fScrollSync->GetPanorama();
	PMPoint visibleCenter = panorama->GetViewCenter();
	
	// Transform the center point to System coordinates
	InterfacePtr<IControlView> view(panorama, IID_ICONTROLVIEW);
	ITransform* transform =  (ITransform*)view->GetContentToWindowTransform();
	//transform->Transform(&visibleCenter);
	::TransformInnerPointToParent(transform,&visibleCenter);
	SysPoint sysVisibleCenter( ToSys(visibleCenter) );
	
	// Search for the widget under the center point
	IControlView* centerWidget = panelData->FindWidget( sysVisibleCenter, 2 );
	
	// If the only widget found was the grid view itself, use the last widget in the list
	if ( centerWidget == view ) {
		centerWidget = panelData->GetWidget(length-1);
	}
	return centerWidget;
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::ScrollIntoView
//---------------------------------------------------------------
void SPXLibraryItemGridController::ScrollIntoView(IControlView* widget)
{
	InterfacePtr<IPanorama> iPanorama(this, IID_IPANORAMA);
	InterfacePtr<IControlView>	iPanelView(this, IID_ICONTROLVIEW);

	PMPoint scrollTo(0,0);
//	iPanorama->ScrollTo(scrollTo,kFalse);

	PMRect		frame = iPanelView->GetFrame();
	PMPoint		scrollOffset = -iPanorama->GetScrollOffset();
	PMRect		bounds = iPanorama->GetBounds();
	scrollOffset -= bounds.LeftTop();
	frame.MoveTo(scrollOffset);

	// Get item frame
	InterfacePtr<IPanelControlData>	iPanelData(this, IID_IPANELCONTROLDATA);
	PMRect	itemFrame = widget->GetFrame();

	// itemFrame.MoveRel(0,1);		// This puts the item on course with the panorama.
	// Check to see if item already in view
	itemFrame.Inset(0,1);
	
	if (! (frame.PointIn(itemFrame.LeftTop())  || frame.PointIn(itemFrame.RightBottom())) )
	{
		iPanorama->ScrollTo(scrollTo,kFalse);
		scrollOffset = -iPanorama->GetScrollOffset();
		bounds = iPanorama->GetBounds();
		scrollOffset -= bounds.LeftTop();
		frame.MoveTo(scrollOffset);
		if (! (frame.PointIn(itemFrame.LeftTop())  || frame.PointIn(itemFrame.RightBottom())) )
		{
			PMPoint viewSize;
			iPanorama->GetPanoramaDeltaMultiple(viewSize);
			
			PMReal scrollOffsetY = itemFrame.Top();
			if (scrollOffsetY + viewSize.Y() > bounds.Bottom())
			{
				scrollOffsetY = bounds.Bottom() - viewSize.Y();
			}
			scrollTo.Y() = -scrollOffsetY;
			iPanorama->ScrollTo(scrollTo);
		}
	}
}


//---------------------------------------------------------------
//  SPXLibraryItemGridController::GetScrollValue
//---------------------------------------------------------------
int32 SPXLibraryItemGridController::GetScrollValue()
{
	// Get the Vertical Scroll Bar's current position
	int32 retScrollValue = 0;
	if ( fScrollSync ) {
		ICounterControlData* scrollBarCtl = fScrollSync->GetScrollBarY();
		retScrollValue = scrollBarCtl->GetValue();
		if ( fViewType == ISPXLibraryViewController::kListView  )
			retScrollValue /= 20;
	}
	return retScrollValue;
}

//---------------------------------------------------------------
//  SPXLibraryItemGridController::SetScrollValue
//---------------------------------------------------------------
void SPXLibraryItemGridController::SetScrollValue(int32 value)
{
	// Set the Vertical Scroll Bar's current position
	if ( fScrollSync ) {
		ICounterControlData* scrollBarCtl = fScrollSync->GetScrollBarY();
		scrollBarCtl->SetValue(value);
	}
}



void SPXLibraryItemGridController::Select(int32 index, bool16 invalidate, bool16 notifyOnChange)
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	IControlView*	view = panelData->GetWidget( index );	
	
	InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
	//Added By Yatin
	if(buttonData == nil)
		return;

	buttonData->Select();
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}

void SPXLibraryItemGridController::SelectAll(bool16 invalidate, bool16 notifyOnChange)
{
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	
	for (int32 i=0; i<panelData->Length(); i++ ){
		IControlView*	view = panelData->GetWidget( i );
		if(view == nil)
			break;

		InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
		if(buttonData == nil)
			return;

		buttonData->Select();
		view = NULL;
	}
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}

void SPXLibraryItemGridController::Deselect(int32 index, bool16 invalidate, bool16 notifyOnChange)
{
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	IControlView*	view = panelData->GetWidget( index );	
	if(view == nil)
		return;

	InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
	if(buttonData == nil)
		return;

	buttonData->Deselect();

	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}



void SPXLibraryItemGridController::DeselectAll(bool16 invalidate, bool16 notifyOnChange)
{
	
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );

	//Actually should not be needed now, but nevertheless will not harm any body
	IControlView * cellPanelView = panelData->FindWidget(kCellPanelWidgetID);
	if ( cellPanelView ) panelData->RemoveWidget( (int32)0 );
	
	for (int32 i=0; i<panelData->Length(); i++ )
	{
		IControlView*	view = panelData->GetWidget(i);	
		if(view == nil)
			break;

		InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
		if(buttonData == nil)
			break;

		buttonData->Deselect();
	}
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
	
}



bool16 SPXLibraryItemGridController::IsSelected(int32 index) const
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	IControlView*	view = panelData->GetWidget( index );	
	if(view == nil)
		return kFalse;
	
	InterfacePtr<IBooleanControlData> buttonData( view, IID_IBOOLEANCONTROLDATA );
	if(buttonData == nil)
		return kFalse;

	return buttonData->IsSelected();
}


int32 SPXLibraryItemGridController::GetSelected() const
{
	// Loop through all the items in the grid
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	for (int32 i = 0; i < panelData->Length(); i++)
	{
		// If this item is selected, return it
		if (IsSelected(i)) return i;
	}
	
	return kNoSelection;
}


void SPXLibraryItemGridController::GetSelected(K2Vector<int32>& multipleSelection) const
{
	// start the selection off nice and fresh
	multipleSelection.clear();
	
	// Loop through all the items in the grid
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	for (int32 i = 0; i < panelData->Length(); i++)
	{
		// If this item is selected, add it to the list
		if (IsSelected(i))
			multipleSelection.push_back(i);
	}
}

//---------------------------------------------------------------
// SPXLibraryItemGridController::UpdateSelectionObservers
//---------------------------------------------------------------
void SPXLibraryItemGridController::UpdateSelectionObservers()
{
	//CA(__FUNCTION__);
	// Get the list of currently selected library items
	K2Vector<int32> libSelectionList;
	GetSelected( libSelectionList );
	
	// Send the change message
	InterfacePtr<ISubject> subject( this, IID_ISUBJECT);
	//subject->Change( 0, IID_IXLIBRARYSELECTIONOBSERVER, (void*) libSelectionList.Length() );
	
}

