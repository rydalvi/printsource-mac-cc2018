
#include "VCPluginHeaders.h"


// ----- Interface Includes -----

#include "IPanelControlData.h"

// ----- Implementation Includes -----

#include "InterfacePtr.h"
#include "ISPXLibraryViewController.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "DVPanelView.h"
#include "IGraphicsPort.h"
#include "AGMGraphicsContext.h"
#include "Utils.h"
#include "IWidgetUtils.h"

#include "SPID.h"

#define kMinGridWidth			64.0
#define kMinGridHeight			77.0


class SPXLibraryItemGridPanel : public DVPanelView
{
public:
	// ----- Initialization -----
	
	SPXLibraryItemGridPanel(IPMUnknown *boss);
	virtual ~SPXLibraryItemGridPanel();

	virtual void		ReadWrite(IPMStream *s, ImplementationID prop);
	
	virtual void		Draw(IViewPort* viewPort, SysRgn updateRgn);
	virtual void		Show(bool16 doShow = kTrue);
	virtual void 		Resize(const PMPoint& dimensions, bool16 invalidate);

	virtual void 		WindowChanged();

	virtual PMPoint		ConstrainDimensions(const PMPoint& dimensions) const; 
	
	virtual void		AdaptToParentsSize(const PMPoint& delta);

private:

	void				DrawBackground(IGraphicsPort* gPort, const PMRect& viewRt);
	void				DrawCellOutlines(IGraphicsPort* gPort, const PMRect& viewRt);
	
	bool16 fStartUp;
};

CREATE_PERSIST_PMINTERFACE(SPXLibraryItemGridPanel, kSPXLibraryItemGridPanelImpl)

//========================================================================================
// CLASS SPXLibraryItemGridPanel
//========================================================================================

SPXLibraryItemGridPanel::SPXLibraryItemGridPanel(IPMUnknown *boss) :
	DVPanelView(boss), fStartUp( kFalse )
{
}

SPXLibraryItemGridPanel::~SPXLibraryItemGridPanel()
{
}

void SPXLibraryItemGridPanel::Show(bool16 doShow )
{
	DVPanelView::Show(doShow);

	// Tell the view controller to setup the scroll bars
	InterfacePtr<ISPXLibraryViewController> viewController( this, IID_ISPXLIBRARYVIEWCONTROLLER );
	viewController->SetupScrollBarConnections();
}


void SPXLibraryItemGridPanel::ReadWrite(IPMStream *s, ImplementationID prop)
{
	DVPanelView::ReadWrite(s, prop);

	if ( !fStartUp && s->IsReading() )
		fStartUp = kTrue;
}

void SPXLibraryItemGridPanel::Draw(IViewPort* viewPort, SysRgn updateRgn)
{
	AGMGraphicsContext gc(viewPort, this, updateRgn);
	InterfacePtr<IGraphicsPort> gPort(gc.GetViewPort(), IID_IGRAPHICSPORT);
		
	gPort->gsave();
	
	SysRect vbbox = GetBBox();
	//--vbbox.right, --vbbox.bottom;
			
	PMRect viewRt(vbbox);
	gc.GetViewToContentTransform/*GetInverseTransform*/().Transform(&viewRt);

	// Clip the children so they dont hammer the borders
//	++vbbox.top;
	SysRgn gridContentRgn = ::CreateRectSysRgn (vbbox);
	SysRgn clippedUpdateRgn = nil;
	if (updateRgn != nil)
		clippedUpdateRgn = ::CopySysRgn(updateRgn);
	::IntersectSysRgn(gridContentRgn, clippedUpdateRgn, clippedUpdateRgn);

	DrawBackground(gPort, viewRt);
//	DVPanelView::Draw(viewPort, clippedUpdateRgn);
	DrawCellOutlines(gPort, viewRt);
	
	::DeleteSysRgn(gridContentRgn);
	::DeleteSysRgn(clippedUpdateRgn);
	
	gPort->grestore();
}

//========================================================================================
// Resize
//========================================================================================
void SPXLibraryItemGridPanel::Resize(const PMPoint& dimensions, bool16 invalidate)
{
	PMRect frame = this->GetFrame();
	const PMReal nDeltaX = dimensions.X() - frame.Width();
	const PMReal nDeltaY = dimensions.Y() - frame.Height();
	
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );

	if (invalidate)
		Invalidate();

	DVPanelView::Resize( frame.Dimensions(), invalidate );
	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	Utils<IWidgetUtils>()->GridPanel_PositionWidgets(panelData, 0, panelData->Length() - 1);
	
	PreDirty();
}


//========================================================================================
// ConstrainDimensions
//========================================================================================
PMPoint SPXLibraryItemGridPanel::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint newPoint(dimensions);
	if ( newPoint.X() < kMinGridWidth ) newPoint.X(kMinGridWidth);
	if ( newPoint.Y() < kMinGridHeight ) newPoint.Y(kMinGridHeight);
	return newPoint;
}

void SPXLibraryItemGridPanel::WindowChanged()
{
	if ( !fStartUp )
		DVPanelView::WindowChanged();
	fStartUp = kFalse;
}

void SPXLibraryItemGridPanel::AdaptToParentsSize(const PMPoint& delta)
{

	PMRect frame = GetFrame();
	
	PMPoint dimensions = frame.Dimensions() + delta;
	PMPoint constrainedDimensions = ConstrainDimensions(dimensions);
	frame.SetDimensions(constrainedDimensions.X(), constrainedDimensions.Y());
	
	SetFrame(frame);
}

void SPXLibraryItemGridPanel::DrawBackground(IGraphicsPort* gPort, const PMRect& viewRt)
{
	Utils<IWidgetUtils>()->DrawPaletteFill(this,gPort,&viewRt);

//	gPort->setrgbcolor(PMReal(0), PMReal(0), PMReal(0));
//	gPort->rectstroke(viewRt);
}

void SPXLibraryItemGridPanel::DrawCellOutlines(IGraphicsPort* gPort, const PMRect& viewRt)
{
	InterfacePtr<IPanelControlData> panel(this, IID_IPANELCONTROLDATA);
	
	gPort->setrgbcolor(PMReal(0), PMReal(0), PMReal(0));
	gPort->setlinewidth(0.0);

	PMRect frame;
	for (int32 i = 0; i < panel->Length(); i++)
	{
		PMRect	frame = panel->GetWidget(i)->GetFrame();
		gPort->rectpath(frame);
	}

	gPort->stroke();
}
