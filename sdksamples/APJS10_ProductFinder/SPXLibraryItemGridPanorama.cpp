
#include "VCPlugInHeaders.h"

// ----- Interface Includes -----
#include "IGridAttributes.h"
#include "IPanelControlData.h"

// ----- Implementation Includes -----
#include "CPanorama.h"

#include "SPID.h"
#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)

class SPXLibraryItemGridPanorama : public CPanorama
{
// ----- Constructors/desctructors
public:
	SPXLibraryItemGridPanorama(IPMUnknown *boss);	
	virtual ~SPXLibraryItemGridPanorama() { }

// ----- Panorama attribtues
public:
	virtual PMRect		GetBounds() const;
	virtual void		GetPanoramaDelta(PMPoint& delta) const;
	virtual void		GetPanoramaDeltaMultiple(PMPoint& delta) const;
};

CREATE_PMINTERFACE(SPXLibraryItemGridPanorama, kSPXLibraryItemGridPanoramaImpl)

SPXLibraryItemGridPanorama::SPXLibraryItemGridPanorama(IPMUnknown *boss) :
	CPanorama(boss)
{
}

//----------------------------------------------------------------------------------------
// SPXLibraryItemGridPanorama::GetBounds
//----------------------------------------------------------------------------------------

PMRect SPXLibraryItemGridPanorama::GetBounds() const
{//CA(__FUNCTION__);
	// ----- Based on how many child widgets are in the grid and the grid dimensions,
	//		 calculate the bounds of the panorama. [amb]
	
	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	InterfacePtr<IGridAttributes> attrs(this, IID_IGRIDATTRIBUTES);

	PMPoint gridDims = attrs->GetGridDimensions();
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kSPGridDelta);
	
	int16 rows = panelData->Length() / ::ToInt16(gridDims.X());
	if (panelData->Length() % ::ToInt16(gridDims.X()) != 0)
		rows++;

	// Remember to remove the outermost borders
	return PMRect(0, 0, gridDims.X() * cellSpacing.X() -1.0 , rows * cellSpacing.Y() -1.0);
}

//----------------------------------------------------------------------------------------
// SPXLibraryItemGridPanorama::GetPanoramaDelta
//----------------------------------------------------------------------------------------

void SPXLibraryItemGridPanorama::GetPanoramaDelta(PMPoint& delta) const
{//CA(__FUNCTION__);
	InterfacePtr<IGridAttributes> attrs( this, IID_IGRIDATTRIBUTES);
	// 1 Point is added because of Bug#461581
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kSPGridDelta);

	// ----- Set increment to scroll by one cell at a time.
	// x is 0 since there isn't a horizontal scroll bar.
	delta.X(0);
	delta.Y(cellSpacing.Y() / GetYScaleFactor());
}

//----------------------------------------------------------------------------------------
// SPXLibraryItemGridPanorama::GetPanoramaDeltaMultiple
//----------------------------------------------------------------------------------------

void SPXLibraryItemGridPanorama::GetPanoramaDeltaMultiple(PMPoint& delta) const
{//CA(__FUNCTION__);
	InterfacePtr<IGridAttributes> attrs(this, IID_IGRIDATTRIBUTES);
	InterfacePtr<IControlView> view(this, IID_ICONTROLVIEW);
	PMRect viewRt(view->GetBBox());

	PMPoint gridDims = attrs->GetGridDimensions();
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kSPGridDelta);

	// ----- Set increment multiple to scroll by the size of the view.
	// ----- Round to nearest cell size multiple	
	delta.X(0);
	delta.Y(cellSpacing.Y()*Round(viewRt.Height()/cellSpacing.Y()) / GetYScaleFactor());
}