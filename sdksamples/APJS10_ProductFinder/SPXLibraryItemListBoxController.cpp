
#include "VCPlugInHeaders.h"

#include "IListBoxController.h"

#include "ISubject.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IBooleanControlData.h"


#include "WidgetID.h"
#include "InterfacePtr.h"
#include "HelperInterface.h"
#include "K2Vector.tpp"

#include "SPID.h"
#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)

//========================================================================================
// CLASS: SPXLibraryItemListBoxController Definition
//========================================================================================

class SPXLibraryItemListBoxController : public IListBoxController
{
public:
	SPXLibraryItemListBoxController(IPMUnknown *boss);

	virtual void			Select(int32 index, bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue);
	virtual void			SelectAll(bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue);
	virtual void 			Deselect(int32 index, bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue );
	virtual void			DeselectAll(bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue);
	
	virtual bool16			IsSelected(int32 index) const;
	virtual int32			GetSelected() const;
	virtual void			GetSelected(K2Vector<int32>& multipleSelection) const;

	// ----- The following methods are not used.
	virtual void			SetMasterItem(int32 itemNumber) { }
	virtual int32			GetMasterItem() const { return 0; }
	virtual void			SetClickItem(int32 itemNumber) { }
	virtual int32			GetClickItem() const { return 0; }							
	virtual int32			FindRowHit(const PMPoint& clickPoint) { return 0; }
	virtual eListBoxPart	ClickedWhichPart(const PMPoint& clickPoint) { return kInNothing; }
	virtual void			ScrollList(const eScrollDirection direction) { }
	virtual void			ScrollItemIntoView(int32 itemNumber) { }
	virtual int32			GetFirstVisibleItem() const { return 0; }
	virtual int32			GetMaximumVisibleItems() const { return 0; }
	virtual int32			Search(const PMString& subString) const { return -1; }
	virtual PMString		GetNthItemString(int32 n) const { return ""; }
	virtual void			GetVisibleItemBounds(int32 item, PMRect& newBounds) { }
	virtual eScrollDirection	CheckScrollDirection(eScrollDirection direction) { return kNoScroll; }
	virtual eScrollDirection	CheckDragScroll(const PMPoint& where) { return kNoScroll; }
	virtual void                SetSelected( const K2Vector<int32>& vecIndices, bool16 bInvalidate, bool16 bNotify );

	virtual void ProcessSelectionRules(IEvent* event, int32 index);


protected:
	void					UpdateSelectionObservers();
	
DECLARE_HELPER_METHODS()
};

DEFINE_HELPER_METHODS(SPXLibraryItemListBoxController)
CREATE_PMINTERFACE(SPXLibraryItemListBoxController, kSPXLibraryItemListBoxControllerImpl)


const int kNothingSelected = -1;


//========================================================================================
// METHODS: SPXLibraryItemListBoxController
//========================================================================================

//========================================================================================
// ----- Constructor
//========================================================================================
SPXLibraryItemListBoxController::SPXLibraryItemListBoxController(IPMUnknown *boss) : HELPER_METHODS_INIT(boss)
{
}

//========================================================================================
// ----- Destructor
//========================================================================================
void SPXLibraryItemListBoxController::Select(int32	index, bool16 invalidate, bool16 notifyOnChange)
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	IControlView*	view = panelData->GetWidget( index );	
	if(view==nil)
		return;
	
	InterfacePtr<IBooleanControlData> buttonData( view, IID_IBOOLEANCONTROLDATA );
	if(buttonData == nil)
		return;

	buttonData->Select();
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}

void SPXLibraryItemListBoxController::SetSelected( const K2Vector<int32>& vecIndices, bool16 bInvalidate, bool16 bNotify )
{//CA(__FUNCTION__);
	ASSERT_UNIMPLEMENTED();
}

void SPXLibraryItemListBoxController::SelectAll(bool16 invalidate, bool16 notifyOnChange)
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	
	for (int32 i=0; i<panelData->Length(); i++ )
	{
		IControlView*	view = panelData->GetWidget( i );	
		if(view == nil)	
			break;

		InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
		if(buttonData==nil)
			break;

		buttonData->Select();
	}
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}



void SPXLibraryItemListBoxController::Deselect(int32 index, bool16 invalidate, bool16 notifyOnChange)
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	IControlView* view = panelData->GetWidget( index );	
	if(view == nil)	
		return;

	InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
	if(buttonData==nil)
		return;

	buttonData->Deselect();
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}



void SPXLibraryItemListBoxController::DeselectAll(bool16 invalidate, bool16 notifyOnChange)
{//CA(__FUNCTION__);
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );

	//Actually should not be needed now, but nevertheless will not harm any body
	IControlView * cellPanelView = panelData->FindWidget(kCellPanelWidgetID);
	if ( cellPanelView ) panelData->RemoveWidget( (int32)0 );
	
	for (int32 i=0; i<panelData->Length(); i++ )
	{
		IControlView*	view = panelData->GetWidget(i);	
		InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
		buttonData->Deselect();
	}
	
	// Notify observers if desired
	if ( notifyOnChange )
		UpdateSelectionObservers();
}



bool16 SPXLibraryItemListBoxController::IsSelected(int32 index) const
{
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	IControlView*	view = panelData->GetWidget( index );	
	
	InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
	return buttonData->IsSelected();
}



int32 SPXLibraryItemListBoxController::GetSelected() const
{
	// Loop through all the items in the grid
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	for (int32 i = 0; i < panelData->Length(); i++){
		// If this item is selected, return it
		if (IsSelected(i)) return i;
	}
	return kNoSelection;
}

void SPXLibraryItemListBoxController::GetSelected(K2Vector<int32>& multipleSelection) const
{
	// start the selection off nice and fresh
	multipleSelection.clear();
	
	// Loop through all the items in the grid
	InterfacePtr<IPanelControlData> panelData( (IPMUnknown*)this, IID_IPANELCONTROLDATA );
	for (int32 i = 0; i < panelData->Length(); i++)
	{
		// If this item is selected, add it to the list
		if (IsSelected(i))
			multipleSelection.push_back(i);
	}
}

//---------------------------------------------------------------
// SPXLibraryItemListBoxController::UpdateSelectionObservers
//---------------------------------------------------------------
void SPXLibraryItemListBoxController::UpdateSelectionObservers()
{//CA(__FUNCTION__);
	// Get the list of currently selected library items
	K2Vector<int32> libSelectionList;
	GetSelected( libSelectionList );
	
	// Send the change message
	InterfacePtr<ISubject> subject( this, IID_ISUBJECT);
	//Chetan -- Commented coz no selection observer is being used
	//subject->Change( 0, IID_ISPXLIBRARYSELECTIONOBSERVER, (void*) libSelectionList.Length() );
}


void SPXLibraryItemListBoxController::ProcessSelectionRules(IEvent* event, int32 index)
{
	
}