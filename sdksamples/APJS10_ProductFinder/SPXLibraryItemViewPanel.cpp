
#include "VCPluginHeaders.h"


// ----- Interface Includes -----

#include "IPanelControlData.h"
#include "IControlView.h"
#include "IGridAttributes.h"
#include "IGraphicsPort.h"
#include "IViewPort.h"
#include "IWidgetParent.h"
#include "IWorkspace.h"
#include "ISession.h"
#include "IInterfaceColors.h"
#include "IWindow.h"
#include "ISPXLibraryViewController.h"

// ----- Implementation Includes -----

#include "InterfacePtr.h"
#include "DVPanelView.h"
#include "AGMGraphicsContext.h"
#include "CreateObject.h"
#include "DVControlView.h"

#include "SPID.h"

class SPXLibraryItemViewPanel : public DVPanelView
{
public:
	// ----- Initialization -----
	
	SPXLibraryItemViewPanel(IPMUnknown *boss);
	virtual ~SPXLibraryItemViewPanel();
	
	virtual void			ReadWrite(IPMStream *s, ImplementationID prop);

	virtual void			Show(bool16 doShow = kTrue);
	virtual void 			Resize(const PMPoint& dimensions, bool16 invalidate); 
	virtual void			WindowChanged(); 

	virtual PMPoint			ConstrainDimensions(const PMPoint& dimensions) const; 

//	virtual void			WindowClosed();

};

CREATE_PERSIST_PMINTERFACE(SPXLibraryItemViewPanel, kSPXLibraryItemViewPanelImpl)

//========================================================================================
// CLASS SPXLibraryItemViewPanel
//========================================================================================

SPXLibraryItemViewPanel::SPXLibraryItemViewPanel(IPMUnknown *boss) :
	DVPanelView(boss)
{
}

SPXLibraryItemViewPanel::~SPXLibraryItemViewPanel()
{
}

void SPXLibraryItemViewPanel::Show(bool16 doShow)
{
	DVPanelView::Show(doShow);
}


void SPXLibraryItemViewPanel::ReadWrite(IPMStream *s, ImplementationID prop)
{
	DVPanelView::ReadWrite(s, prop);
}


//========================================================================================
// Resize
//========================================================================================
void SPXLibraryItemViewPanel::Resize(const PMPoint& dimensions, bool16 invalidate)
{
	PMRect frame = this->GetFrame();
	const PMReal nDeltaX = dimensions.X() - frame.Width();
	const PMReal nDeltaY = dimensions.Y() - frame.Height();
	
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	
	
	// ----- CControlView::Resize does smart invalidation, by only invalidating the
	//		 newly exposed part of the view. But if your data is layed out differently
	//		 because of a resize that doesn't work. So here we invalidate the whole view
	//		 before calling resize. [amb] 
	// 		 [tjg -- copied this comment and call from SwatchesPanel::RenderListView.cpp]
	if (invalidate)
		Invalidate();
	
	//DVControlView::Resize( frame.Dimensions(), invalidate );

	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	IControlView*					childView;
	
	// Move the item panel scroll bar
	childView = panelData->FindWidget( kSPXLibraryItemScrollBarWidgetId );
	frame = childView->GetFrame();
	frame.Left( frame.Left() + nDeltaX );
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	childView->SetFrame( frame );

	// Resize the item grid panel
	childView = panelData->FindWidget( kSPXLibraryItemGridWidgetId );
	frame = childView->GetFrame();
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	childView->Resize( frame.Dimensions(), invalidate );
	
	// Notify the view controller that the panel was resized
	childView = panelData->FindWidget( kSPXLibraryItemGridWidgetId );
	InterfacePtr<ISPXLibraryViewController> viewController( childView, IID_ISPXLIBRARYVIEWCONTROLLER );
	viewController->SizeChanged();

	PreDirty();
}


//========================================================================================
// WindowChanged
//========================================================================================
void SPXLibraryItemViewPanel::WindowChanged()
{
	DVPanelView::WindowChanged();

	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	IControlView*					childView;
	
	InterfacePtr<IWidgetParent> widgetPtr(this, IID_IWIDGETPARENT);
	InterfacePtr<IWindow> window((IWindow*) widgetPtr->QueryParentFor(IID_IWINDOW));
	
	if (window) {
		
		// Tell the view controller to setup the scroll bars
		childView = panelData->FindWidget( kSPXLibraryItemGridWidgetId );
		InterfacePtr<ISPXLibraryViewController> viewController( childView, IID_ISPXLIBRARYVIEWCONTROLLER );
		viewController->SetupScrollBarConnections();
	}
	else
	{
		//Hope this might be a place from where we can release the scroll bar
		//taking the hint that the parent window has vanished
		childView = panelData->FindWidget( kSPXLibraryItemGridWidgetId );
		InterfacePtr<ISPXLibraryViewController> viewController( childView, IID_ISPXLIBRARYVIEWCONTROLLER );
		viewController->ReleaseScrollBar();
	}
}


//========================================================================================
// ConstrainDimensions
//========================================================================================
PMPoint SPXLibraryItemViewPanel::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim(dimensions);
/*
	// Width can vary if not above maximum or below minimum
	if(constrainedDim.X() > kMaxAssetLibWidth  )
		constrainedDim.X( kMaxAssetLibWidth );
	else if(constrainedDim.X() < kMinAssetLibWidth  )
		constrainedDim.X( kMinAssetLibWidth );
	
	// Height can vary if not above maximum or below minimum
	if(constrainedDim.Y() > kMaxAssetLibHeight  )
		constrainedDim.Y( kMaxAssetLibHeight );
	else if(constrainedDim.Y() < kMinAssetLibHeight  )
		constrainedDim.Y( kMinAssetLibHeight );
*/
	InterfacePtr<const IPanelControlData>	panel( this, IID_IPANELCONTROLDATA );
	IControlView* view = panel->FindWidget( kSPXLibraryItemGridWidgetId);
	
	PMReal scrollBarWidth = panel->FindWidget(kSPXLibraryItemScrollBarWidgetId)->GetFrame().Dimensions().X();
	
	constrainedDim.X(constrainedDim.X() - scrollBarWidth);
  	constrainedDim = view->ConstrainDimensions(constrainedDim);
  	constrainedDim.X(constrainedDim.X() + scrollBarWidth);

	return constrainedDim;
}