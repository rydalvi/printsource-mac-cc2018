#ifndef __SUBSECTIONSPRAYER_H__
#define __SUBSECTIONSPRAYER_H__

#include "VCPluginHeaders.h"
#include "DSFID.h"
#include "IDataSprayer.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "vector"
# include "CSprayStencilInfo.h"

#include "sectionData.h"

class RangeProgressBar;

using namespace std;



//class DynFrameStruct
//{
//public:
//	int HorzCnt;
//	int VertCnt;
//	bool16 isLastHorzFrame;
//	PMRect BoxBounds;
//};
//typedef vector<DynFrameStruct> FrameBoundsList;

class BoxBounds
{
public:
	PMReal Top;
	PMReal Bottom;
	PMReal Left;
	PMReal Right;
	UIDRef BoxUIDRef;
	PMReal compressShift;
	PMReal enlargeShift;
	bool16 shiftUP;
	bool16 isUnderImage;
	bool16 isMoved;

	BoxBounds()
	{
		Top = 0.0;
		Bottom= 0.0;
		Left= 0.0;
		Right= 0.0;
		compressShift= 0.0;
		enlargeShift= 0.0;
		shiftUP = kFalse;
		isUnderImage= kFalse;
		isMoved = kFalse;

	}

};

typedef vector<BoxBounds> vectorBoxBounds;


class SubSectionSprayer 
{
public:
	//pure virtual function "startSpraying" should be overridden
	SubSectionSprayer()
	{
		sprayedProductIndex = 0; //to start with the second product
	}
	void startSpraying(void);
	void startSprayingSubSection(void);

	//void DoDialog();

	PMString selectedSubSection;

//private:
	//subsectionsprayer related
	bool16 getCurrentPage(UIDRef&, UIDRef&);
	bool16 getSelectedBoxIds(UIDList&);
	bool16 getMaxLimitsOfBoxes(const UIDList&, PMRect&, vectorBoxBounds &boxboundlist);
	void getMaxHorizSprayCount(const PMRect&, const PMRect&, int16&);
	void getMaxVertSprayCount(const PMRect&, const PMRect&, int16&);
	bool16 getMarginBounds(const UIDRef&, PMRect&);
	bool16 getPageBounds(const UIDRef&, PMRect&);
	void sprayPage(const UIDRef&, const UIDList&, const PMRect&, const PBPMPoint&, int32&, RangeProgressBar&, bool16);
	//bool16 willBoxesOverlap(const PMRect&, const PMRect&, const int16&, const int16&, PBPMPoint&);
	bool16 getBoxPosition(const PMRect&, const PMRect&, const int16&, const int16&, PBPMPoint&);
	bool16 CopySelectedItems();
	void moveBoxes(const UIDList&, const PBPMPoint&);
	bool16 getAllBoxIds(const UIDList&);
	void sprayFirstProductOfSubSection(const UIDList&);
	void sprayFromSecondProductOfSubSection(const UIDList& , const PMRect& , const PBPMPoint& , int32, RangeProgressBar&,bool16 &);
	bool8 isBoxParent(const UIDRef&, IDataBase*);
	UIDList getParentSelectionsOnly(UIDList&, IDataBase*);
	
	void callSprayForThisBox(UIDRef, TagList&);
	bool16 callIsFrameTagged(const UIDRef&);
	bool16 callSprayForTaggedBox(const UIDRef&);
	
//	void setDataSprayersUIDList(vector<int32>);
//	void setSSIdInPFTreeModel(int32);
//	bool16 getAllIdForLevel(int32, int32&, vector<int32>&);
//	void getAllSetOfIds(int32, vector<int32>&);
	void setPublicationID(double);
 	void setImagePath(PMString);

	vector<double> allPFIdList;
	int32 sprayedProductIndex;
	bool16 getAllIdForLevel(int32& numIds, vector<double>& idList);
	bool16 getMaxLimitsOfBoxesForProductList(vector<PMRect> boxMarginList, PMRect& maxBounds);
	bool16 getBoxPositionForResizableFrame(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, const int16& horizCnt,const int16& vertCnt,  FrameBoundsList ProductBlockList, PBPMPoint& moveToPoints);
	void sprayPageWithResizableFrame(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag);

	void moveAutoResizeBoxAfterSpray(const UIDList& copiedBoxUIDList, vectorBoxBounds vectorCurBoxBoundsBeforeSpray );
	int deleteThisBoxUIDList(UIDList boxUIDList);

	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);
	ErrorCode AdjustMaxLimitsOfBoxes(const UIDList& boxList, PMRect PagemaxBounds);

	void sprayPageWithResizableFrameNew(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag);
	bool16 getBoxPositionForResizableFrameNew( PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag);
	bool16 getBoxPositionForResizableFrameNewVerticalFlow( PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag);
	
	bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxUidref,const UIDList &selectUIDList);

	bool16 getStencilInfo(const UIDList& productSelUIDList,CSprayStencilInfo& objCSprayStencilInfo);
	
	UIDList selectUIDList;
	PMString imagePath;

	bool16 sprayPageWithResizableFrameForSection(bool16 toggleFlag,PMRect &marginBoxBounds,FrameBoundsList &ProdBlockBoundList,int16 &horizCnt,int16 &vertCnt);
	bool16 doesExist(TagList &tagList,const UIDList &selectUIDList);
	bool16 getAllBoxIdsForGroupFrames( UIDList& tempFrameList);

	void getAllChildSections(vector<PubData>& SectionIDList, double sectionId, double languageId);

	bool16 isTemplateWithinPage(PMRect& origMaxBoxBounds, PMRect& marginBoxBounds, IDataBase* database, UID templatePageUIDForRightPageCheck);

	bool16 getBoxPositionForFrame( PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints);

	bool16 addNewPageHere(PMRect& PagemarginBoxBounds);
	void deletePage(int32 deletelastPage);

    PMString converIntVectorToUniqueIdString(vector<double> idVector);


};

#endif
