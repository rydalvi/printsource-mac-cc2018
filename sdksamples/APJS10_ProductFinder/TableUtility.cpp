#include "VCPluginHeaders.h"
//#include "ITextFrame.h"   //Commented By Sachin Sharma on 29/06/07
#include "IMultiColumnTextFrame.h"   //Added  on 29/06/07
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ITableCommands.h"
#include "TableUtility.h"
#include "IFrameUtils.h"
#include "IAppFramework.h"
#include "CAlert.h"
#include "ITagReader.h"
//#include "ISpecialChar.h"
#include "IClientOptions.h"
//#include "IDataSprayer.h"
#include "IGraphicFrameData.h"

#define CA(x) CAlert::InformationAlert(x)


bool16 TableUtility::isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef, int32 tableNumber)
{
	bool16 returnValue=kFalse;

	do
	{
		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
			break;
		
		//Added By Sachin sharma on 29/06/07
		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
			break;*/
//+==================
		InterfacePtr<IMultiColumnTextFrame> iMultiColumntextFrame(boxUIDRef.GetDataBase(), textFrameUID, UseDefaultIID());
		if(!iMultiColumntextFrame)
		{
			ASSERT(iMultiColumntextFrame);
			break;
		}
//=+++++======++++++++++
		InterfacePtr<ITextModel> textModel(iMultiColumntextFrame->QueryTextModel());
		if (textModel == nil)
			break;

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
			break;

		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0)
			break;

		tableIndex=tableNumber-1;

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == nil) 
			break;
		
		UIDRef tableRef(::GetUIDRef(tableModel));
		
		tableUIDRef=tableRef;

		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}

/*void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose)
{
	do
	{
		ErrorCode result;
		//CA("Resize");
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalCols().count;
		int32 presentCols = tableModel->GetTotalRows().count;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
			break;

		if(isTranspose)
		{
			result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(100.0));
		
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
			}
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}
		}
		else
		{
			if(tableCommands==nil)
				break;
			else
			{
				result = tableCommands->InsertRows(RowRange(1, 2), Tables::eAfter, PMReal(0.0));
			}
			
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows-1), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows+1));
			}			
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}

		}

		

	}
	while(kFalse);
}*/

void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose)
{
	
	do
	{
		ErrorCode result;

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;
		
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
		{
			break;
		}

		if(!isTranspose)
		{	
			if(presentRows<(numRows+1))
			{
				// Awasthi
				
				result = tableCommands->InsertRows(RowRange(presentRows-1, (numRows+1)-presentRows), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>(numRows+1))
			{
				
				result = tableCommands->DeleteRows(RowRange(0, presentRows-(numRows+1)));
			}
			
			if(presentCols<numCols)
			{
				//  Awasthi
				
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}

			// Disabled by Awasthi ( Not required insertion)
			//for table header
			//if(isTranspose)
			//{
				
			//	result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(30.0));
			//}
			//else
			//{
				
			//	result = tableCommands->InsertRows(RowRange(0, 1), Tables::eBefore, PMReal(30.0));
			//}
		}
		else
		{
			if(presentRows<numRows)
			{
				// Awasthi
				
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
			}
			
			if(presentCols<(numCols+1))
			{
				//  Awasthi
				
				result = tableCommands->InsertColumns(ColRange(presentCols-1, (numCols+1)-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>(numCols+1))
			{
				
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-(numCols+1)));
			}

		}

	}
	while(kFalse);
	
}



void TableUtility::setTableRowColData
(const UIDRef& tableUIDRef, const PMString& data, const int32& rowIndex, const int32& colIndex)
{
	do
	{
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
			break;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
			break;

		WideString* wStr=new WideString(data./*GetPlatformString().c_str()*/GetPlatformString().c_str());  //Cs4

		tableCommands->SetCellText(*wStr, GridAddress(rowIndex, colIndex));
		if(wStr)
			delete wStr;
	}
	while(kFalse);
}

void TableUtility::fillDataInTable
(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId ,double CurrSectionid)
{
	
//	VectorScreenTableInfoPtr result=NULL;
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//			break;
//		}
//
//		
//		
//		//result = ptrIAppFramework->OBJTABLECntrller_getAllScreenTables(objectId);
//		//-- COMMENTED 27 JAN 2006 --
//		//result = ptrIAppFramework->OBJTABLECntrller_getAllScreenTablesBySectionidObjectid(CurrSectionid, objectId);
//		
//		if(!result)
//			return;
//	
//		int32 numRows=0;
//		int32 numCols=0;
//
//		CObjectTableValue oTableValue;
//		VectorScreenTableInfoValue::iterator it;
//
//		bool16 typeidFound=kFalse;
//		for(it = result->begin(); it!=result->end(); it++)
//		{
//			//CA("3.1");
//			oTableValue = *it;
//			if(oTableValue.getTableTypeID() == tableTypeId)
//			{
//				//CA("3.2");
//				typeidFound=kTrue;
//				break;
//			}
//			//CA("3.3");
//		}
//
//		//CA("4");
//		
//		if(!typeidFound)
//			break;
//		//CA("5");
//		numRows = oTableValue.getTableData().size();
//		if(numRows<=0)
//			break;
//
//		//CA("6");
//		numCols = oTableValue.getTableHeader().size();
//		if(numCols<=0)
//			break;
//		//CA("7");
//		bool8 isTranspose = oTableValue.getTranspose();
//		//CA("8");
////isTranspose = kTrue;
//
//		if(isTranspose)
//		{
//			//CA("is transpose");
//			int32 i=1, j=0;
//			//Awasthi
//			resizeTable(tableUIDRef, numCols, numRows, isTranspose);
//			//End		
//			vector<int32> vec_tableheaders;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//
//			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose);
//
//			vector<vector<PMString> > vec_tablerows;
//
//			vec_tablerows = oTableValue.getTableData();
//			//PMString temp1("Size in sprayer: ");
//			//temp1.AppendNumber(vec_tablerows.size());
//			//CA(temp1);
//
//			vector<vector<PMString> >::iterator it1;
//
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				//CA("row iterator");
//				vector<PMString>::iterator it2;
//				//PMString temp2("Size of cols in sprayer: ");
//				//temp2.AppendNumber((*it1).size());
//				//CA(temp2);
//
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{
//					//CA("col iterator");
//					if(j==0)
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//
//					setTableRowColData(tableUIDRef, (*it2), j-1, i);
//					j++;
//				}
//				i++;
//				j=0;
//				//break;
//			}
//		}
//		else
//		{
//			//CA("is not transpose");
//			int32 i=1, j=0;
//			// Awasthi
//			resizeTable(tableUIDRef, numRows, numCols, isTranspose);
//			// End
//			vector<int32> vec_tableheaders;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//
//			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose);
//
//			vector<vector<PMString> > vec_tablerows;
//
//			vec_tablerows = oTableValue.getTableData();
//			//PMString temp1("Size in sprayer: ");
//			//temp1.AppendNumber(vec_tablerows.size());
//			//CA(temp1);
//
//			vector<vector<PMString> >::iterator it1;
//
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				//CA("row iterator");
//				vector<PMString>::iterator it2;
//				//PMString temp2("Size of cols in sprayer: ");
//				//temp2.AppendNumber((*it1).size());
//				//CA(temp2);
//
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{
//					//CA("col iterator");
//					if(j==0)
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//
//					setTableRowColData(tableUIDRef, (*it2), i, j-1);
//					j++;
//				}
//				i++;
//				j=0;
//				//break;
//			}
//			
//		}
//		
//	}
//	while(kFalse);
//
//	//CA("9");
//	
//	delete result;
}

void TableUtility::putHeaderDataInTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose)
{
	//CAttributeValue oAttribVal;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}

		for(int i=0;i<vec_tableheaders.size();i++)
		{
			/*
				COMMENTED ON 27 JAN 2006 FOR COMPILATION 
			*/
			//oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
			//PMString dispname = oAttribVal.getDisplay_name();
			////CA(dispname);
			//InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
			//if (tableModel == nil)
			//{
			//	CA("Err: invalid interface pointer ITableFrame");
			//	break;
			//}

			//InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			//if(tableCommands==nil)
			//{
			//	CA("Err: invalid interface pointer ITableCommands");
			//	break;
			//}
			////CA(data.GetPlatformString().c_str());
			//WideString* wStr=new WideString(dispname);
			//if(isTranspose)
			//	tableCommands->SetCellText(*wStr, GridAddress(i, 0));
			//else
			//	tableCommands->SetCellText(*wStr, GridAddress(0, i));
		}
	}
	while(kFalse);
}
