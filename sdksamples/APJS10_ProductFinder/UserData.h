#ifndef __UserData_h__
#define __UserData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"



class UserData
{
	private:
		PMString userName;
		//PMString userPassword;
		double userID;
		//int32 roleId;
		//int32 supplierId;
		//bool16 includeSubDir;
	public:
		UserData(){
			this->userName="";
			//this->userPassword="";
			this->userID=-1;
			//this->roleId=-1;
			//this->supplierId=-1;
			//this->includeSubDir=kFalse;
		}

		UserData(int32 uID,PMString uName)
		{
			userID = uID;
			userName= uName;
		}

		double getuser_id(void){
			return this->userID;
		}
		void setuser_id(double id){
			this->userID=id;
		}
		
		/*int32 getrole_id(void){
			return this->roleId;
		}
		void setrole_id(int32 id){
			this->roleId=id;
		}*/
		PMString getuser_Name(void){
			return this->userName;
		}
		void setuser_Name(PMString name)
		{
			this->userName=name;
		}

		
};
#endif