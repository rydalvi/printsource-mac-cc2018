#include "VCPluginHeaders.h"
#include "ProductSpray.h"
#include "CAlert.h"
#include "ISelectionUtils.h"
#include "IHierarchy.h"
#include "IDataSprayer.h"
#include "ILayoutUtils.h"
#include "IDocument.h"
#include "ITextMiscellanySuite.h"
#include "ITagReader.h"
#include "PublicationNode.h"
#include "TableStyleUtils.h"
#include "ILayoutSelectionSuite.h"
#include "ILayoutUIUtils.h"
#include "SubSectionSprayer.h"
#include "IPageItemTypeUtils.h"

#include "ISpreadList.h"
#include "ISpread.h"
#include "IPageList.h"
#include "IPageSetupPrefs.h"
#include "IPageCmdData.h"
#include "PreferenceUtils.h"
#include "ILayoutCmdData.h"
#include "IGeometry.h"
#include "IScrapSuite.h"
#include "IMasterPage.h"

#include "IMargins.h"
#include "IColumns.h"

#include "ProgressBar.h"
#include "IBoolData.h"
#include "IXMLAttributeCommands.h"


#define CA(x) CAlert::InformationAlert(x)

extern int32 CurrentLstBoxIndex;
extern int32 CurrentSelectedProductRow;
extern PublicationNodeList pNodeDataList; 
extern double CurrentSelectedSection;
extern double CurrentSelectedPublicationID;
extern double CurrentSelectedSubSection;
extern bool16 isSprayItemPerFrameFlag;
extern bool16 isItemHorizontalFlow;
extern K2Vector<double> CurrentListofProducts;
extern bool16 searchResult;

char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";

extern PMRect addPageSplCase_marginBBox;
extern PMRealList addPageSplCase_columns;

void ProductSpray::startSpraying(void)
{	
	//CA("ProductSpray::startSpraying");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying:itagReader == nil");
		return ;
	}
	IDocument* doc=Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
	if(doc==nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::No doc");
		return;
	}

	IDataBase* database = ::GetDataBase(doc);
	if(database==nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::No database");
		return;
	}

	Utils<ILayoutUIUtils>()->InvalidateViews(doc);

	int32 LengthList = 0;
	UIDList selectUIDList(database);
	bool16 result = kFalse;
	bool16 isSprayedInSingleItemSpray = kFalse;
	result= this->getAllBoxIds(selectUIDList);
	
	LengthList = selectUIDList.Length(); 	
	if(LengthList == 0)
	{  
		//CA("LengthList == 0");
		return;
	}




//	CSprayStencilInfo objCSprayStencilInfo;

//	SubSectionSprayer SSSobj;

	//result = SSSobj.getStencilInfo(selectUIDList,objCSprayStencilInfo);
	//if(result == kTrue)
	//{	


	//	GetSectionData getSectionData;	
	//	
	//	if(objCSprayStencilInfo.isCopy)			
	//		getSectionData.addCopyFlag = kTrue;			
	//	if(objCSprayStencilInfo.isProductCopy)
	//		getSectionData.addProductCopyFlag = kTrue;
	//	if(objCSprayStencilInfo.isSectionCopy)
	//	{
	//		getSectionData.addSectionLevelCopyAttrFlag = kTrue;
	//		getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
	//		getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;
	//	}

	//	if(objCSprayStencilInfo.isAsset)		
	//		getSectionData.addImageFlag = kTrue;		
	//	
	//	if(objCSprayStencilInfo.isProductAsset)
	//		getSectionData.addProductImageFlag = kTrue;

	//	if(objCSprayStencilInfo.isAsset && !objCSprayStencilInfo.isCopy)
	//		getSectionData.addCopyFlag = kTrue;

	//	if(objCSprayStencilInfo.isProductAsset && !objCSprayStencilInfo.isProductCopy)
	//		getSectionData.addProductCopyFlag = kTrue;
	//	
	//	if(objCSprayStencilInfo.isBMSAssets){
	//		getSectionData.addImageFlag = kTrue;
	//		getSectionData.addItemBMSAssetsFlag = kTrue;
	//	}
	//	if(objCSprayStencilInfo.isProductBMSAssets)
	//	{
	//		getSectionData.addImageFlag = kTrue;
	//		getSectionData.addProductBMSAssetsFlag = kTrue;
	//	}
	//	if(objCSprayStencilInfo.isSectionLevelBMSAssets)
	//		getSectionData.addPubLogoAssetFlag = kTrue;


	//	if(objCSprayStencilInfo.isDBTable)
	//	{				
	//		getSectionData.addDBTableFlag = kTrue;
	//		//getSectionData.addProductDBTableFlag = kTrue; //for Item Group Lists spray

	//		getSectionData.addChildCopyAndImageFlag = kTrue;
	//	}
	//	if(objCSprayStencilInfo.isProductDBTable)
	//	{
	//		getSectionData.addProductDBTableFlag = kTrue;
	//		getSectionData.addChildCopyAndImageFlag = kTrue;
	//	}
	//	if(objCSprayStencilInfo.isCustomTablePresent)
	//		getSectionData.addCustomTablePresentFlag = kTrue;
	//	
	//	
	//	if(objCSprayStencilInfo.isItemPVMPVAssets)
	//	{
	//		getSectionData.addItemPVMPVAssetFlag = kTrue;
	//		if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
	//				getSectionData.itemPVAssetIdList.push_back(objCSprayStencilInfo.itemPVAssetIdList[i]);				
	//	}
	//	if(objCSprayStencilInfo.isProductPVMPVAssets)
	//	{
	//		getSectionData.addProductPVMPVAssetFlag = kTrue;
	//		if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
	//				getSectionData.productPVAssetIdList.push_back(objCSprayStencilInfo.productPVAssetIdList[i]);	
	//	}
	//	if(objCSprayStencilInfo.isSectionPVMPVAssets)
	//	{
	//		getSectionData.addSectionPVMPVAssetFlag = kTrue;
	//		if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
	//				getSectionData.sectionPVAssetIdList.push_back(objCSprayStencilInfo.sectionPVAssetIdList[i]);	
	//	}
	//	if(objCSprayStencilInfo.isPublicationPVMPVAssets)
	//	{
	//		getSectionData.addPublicationPVMPVAssetFlag = kTrue;
	//		if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
	//				getSectionData.publicationPVAssetIdList.push_back(objCSprayStencilInfo.publicationPVAssetIdList[i]);	
	//	}
	//	if(objCSprayStencilInfo.isCatagoryPVMPVAssets)
	//	{
	//		getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
	//		if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
	//				getSectionData.catagoryPVAssetIdList.push_back(objCSprayStencilInfo.catagoryPVAssetIdList[i]);	
	//	}
	//	
	//	if(objCSprayStencilInfo.isCategoryImages)
	//	{
	//		getSectionData.addCategoryImages = kTrue;
	//		if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
	//				getSectionData.categoryAssetIdList.push_back(objCSprayStencilInfo.categoryAssetIdList[i]);	
	//	}

	//	if(objCSprayStencilInfo.isEventSectionImages)
	//	{
	//		getSectionData.addEventSectionImages = kTrue;
	//		if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
	//			for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
	//				getSectionData.eventSectionAssetIdList.push_back(objCSprayStencilInfo.eventSectionAssetIdList[i]);	
	//	}
	//			
	//	if(objCSprayStencilInfo.isHyTable)
	//		getSectionData.addHyTableFlag = kTrue;
	//	if(objCSprayStencilInfo.isProductHyTable)
	//		getSectionData.addHyTableFlag = kTrue;
	//	if(objCSprayStencilInfo.isSectionLevelHyTable)
	//		getSectionData.addHyTableFlag = kTrue;

	//	if(objCSprayStencilInfo.isChildTag)			
	//		getSectionData.addChildCopyAndImageFlag = kTrue;			

	//	if(objCSprayStencilInfo.isProductChildTag)
	//		getSectionData.addProductChildCopyAndImageFlag = kTrue;

	//	if(objCSprayStencilInfo.isEventField)
	//		getSectionData.isEventField = kTrue;

	//	if(getSectionData.addChildCopyAndImageFlag)
	//	{
	//		getSectionData.addDBTableFlag = kTrue;
	//		getSectionData.addProductDBTableFlag = kTrue;
	//	}

	//	if(getSectionData.addProductChildCopyAndImageFlag)
	//		getSectionData.addCustomTablePresentFlag = kTrue;
	//	
	//		
	//	getSectionData.isOneSource = kFalse;
	//	
	//	getSectionData.addComponentTableFlag = kFalse;
	//	getSectionData.addAccessoryTableFlag = kFalse;
	//	getSectionData.addXRefTableFlag = kFalse;
	//	
	//	/*getSectionData.addSectionLevelCopyAttrFlag = kFalse;
	//	getSectionData.addPublicationLevelCopyAttrFlag = kFalse;
	//	getSectionData.addCatagoryLevelCopyAttrFlag = kFalse;*/

	//	getSectionData.SectionId = CurrentSelectedSubSection;
	//	getSectionData.PublicationId = CurrentSelectedPublicationID ;
	//	
	//	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//	if(ptrIClientOptions==nil)
	//	{
	//		//CAlert::ErrorAlert("Interface for IClientOptions not found.");
	//		return;
	//	}

	//	PMString language_name("");
	//	getSectionData.languageId = ptrIClientOptions->getDefaultLocale(language_name);

	//	//getSectionData.languageId = 1;
	//	getSectionData.CatagoryId = -1;

	//	/*if(getSectionData.isOneSource)
	//		CA("getSectionData.isOneSource == kTrue");
	//	else
	//		CA("getSectionData.isOneSource == kFalse");*/
	////CA("3");
	//	
	//			
	//	if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
	//		getSectionData.itemIdList.push_back(pNodeDataList[CurrentSelectedProductRow].getPubId());
	//	if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
	//		getSectionData.productIdList.push_back(pNodeDataList[CurrentSelectedProductRow].getPubId());
	//	if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 2)
	//		getSectionData.hybridIdList.push_back(pNodeDataList[CurrentSelectedProductRow].getPubId());
	//	

	//	getSectionData.isGetWholePublicationOrCatagoryDataFlag = kFalse;
	//	

	////CA("4");
	//	//progressBar.SetTaskText("Retrieving Data From Server ...");
	//	
	//	//AcquireWaitCursor awc ;
	//	//awc.Animate(); 

	//	ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);		
	//}




	//TagReader tReader;
	TagList tagList;
	vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
	PMRect CopiedItemMaxBoxBoundsBforeSpray;
	result = kFalse;
	SubSectionSprayer sspObj;
	result = sspObj.getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinderPalete:ProductSpray::startSpraying:Pointer to DataSprayerPtr not found");
		return;
	}

	if(searchResult)
		DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],pNodeDataList[CurrentSelectedProductRow].getSectionID(), pNodeDataList[CurrentSelectedProductRow].getPublicationID(), pNodeDataList[CurrentSelectedProductRow].getSubSectionID());
	else
		DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);

	DataSprayerPtr->setFlow(isItemHorizontalFlow);
	DataSprayerPtr->getAllIds(pNodeDataList[CurrentSelectedProductRow].getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data

	InterfacePtr<IClientOptions > ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::No ptrIClientOptions");
		return;
	}
//	PMString imagePath("");
//
//	imagePath=ptrIClientOptions->getImageDownloadPath();
//	if(imagePath!="")
//	{
//		char *imageP=imagePath.GetPlatformString().c_str();
//		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
//			#ifdef MACINTOSH
//				imagePath+=":";
//			#else
//				imagePath+="\\";
//			#endif
//	}

//// New functionality added for Indaba for spraying Products items in their Individual Stencils.
	isSprayItemPerFrameFlag = checkIsSprayItemPerFrameTag(selectUIDList , isItemHorizontalFlow);

	bool16 isOriginalFrameSprayedInSprayItemPerFrame = kFalse;

	if(isSprayItemPerFrameFlag)
	{
		int32 field1_val = -1;
		//CA("isSprayItemPerFrameFlag");
		UIDList ItemFrameUIDList(selectUIDList.GetDataBase());
		bool16 addedNewPageInSprayItemPerFrameFlow = kFalse;	

		for(int i=0; i<LengthList; i++)
		{
			tagList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));
			if(tagList.size()<=0)//This can be a Tagged Frame
			{	
				//CA(" tagList.size()<=0 ");
				if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
				{	
					tagList.clear();
					tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));
					if(tagList.size()==0)//Ordinary box
					{					
						continue ;
					}	
					
					for(int32 j=0; j <tagList.size(); j++)
					{
						if(tagList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
						{
							//CA("Item Tag Found");
							ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
							if(tagList[j].imgFlag != 1)
								field1_val = tagList[j].field1;
							break; // break out from for loop 
						}
					}
				}
				else
				{
					// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
					//CA(" else DataSprayerPtr->isFrameTagged");
					InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
					if(!iHier)
					{
						//CA(" !iHier >> Continue ");
						continue;
					}
					UID kidUID;				
					int32 numKids=iHier->GetChildCount();				
					IIDXMLElement* ptr = NULL;

					for(int j=0;j<numKids;j++)
					{
						//CA("Inside For Loop");
						bool16 Flag12 =  kFalse;
						kidUID=iHier->GetChildUID(j);
						UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
						TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
						if(NewList.size()<=0)//This can be a Tagged Frame
						{
							NewList.clear();
							NewList=itagReader->getFrameTags(selectUIDList.GetRef(i));
							if(NewList.size()==0)//Ordinary box
							{					
								continue ;
							}	
							
							for(int32 j=0; j <NewList.size(); j++)
							{
								if(NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
								{
									//CA("Item Tag Found");
									if(tagList[j].imgFlag != 1)
										field1_val = tagList[j].field1;
									ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
									Flag12 = kTrue;
									break; // break out from for loop
								}
							}
						}
						else
						{
							for(int32 j=0; j <NewList.size(); j++)
							{
								if(NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
								{
									//CA("Item Tag Found");
									if(tagList[j].imgFlag != 1)
										field1_val = tagList[j].field1;
									ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
									Flag12 = kTrue;
									break; // break out from for loop
								}
							}
						}

						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
						if(Flag12)
							break;
					}
				}
			}
			else
			{
				for(int32 j=0; j <tagList.size(); j++)
				{
					if(tagList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
					{
						//CA("Item Tag Found");
						if(tagList[j].imgFlag != 1)
							field1_val = tagList[j].field1;
						ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());					
						break; // break out from for loop
					}
				}
			}
			for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
			{
				tagList[tagIndex].tagPtr->Release();
			}
		}
		int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
		if(ItemFrameUIDListSize == 0)
		{
			//CA("ItemFrameUIDListSize == 0");
		}

		
		int AlphabetArrayCount =0;
		if((pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1) || (pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0))
		{
			//CA("A");
			VectorLongIntPtr ItemIDInfo = NULL;
			vector<double> FinalItemIds;
			do
			{
				if(pNodeDataList[CurrentSelectedProductRow].getIsONEsource())
				{
					//CA("ONEsource mode");
					//If ONEsource mode  is selected and Table stencils for item is selected.Then to get all information of table.
					/*if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1){
						ItemIDInfo=ptrIAppFramework->GETProduct_getAllItemIDsFromTables(pNodeDataList[CurrentSelectedProductRow].getPubId());
					}*/
				}
				else
				{
					//CA("publication mode");
					//If publication mode is selected.					
					/*PMString ASD("ObjectID : ");
					ASD.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());
					ASD.Append("  CurrentSectionID :  " );
					ASD.AppendNumber(CurrentSelectedSubSection);
					CA(ASD);*/
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
					{
						if(field1_val == -1)
						{
							ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(pNodeDataList[CurrentSelectedProductRow].getPubId(), CurrentSelectedSubSection);
						}
						else
						{
							VectorScreenTableInfoPtr tableInfo = NULL;
							tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSelectedSubSection, pNodeDataList[CurrentSelectedProductRow].getPubId(),kTrue);
							
							if(!tableInfo)
							{
								ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
								//ItemAbsentinProductFlag = kTrue;
								break;
							}

							vector<double>::iterator itrID;		

							vector<double> vec_items;
							CItemTableValue oTableSourceValue;
							VectorScreenTableInfoValue::iterator itr;
							for(itr = tableInfo->begin(); itr!=tableInfo->end(); itr++)
							{
								oTableSourceValue = *itr;	
								
								double table_Type_ID = oTableSourceValue.getTableTypeID();
								if(table_Type_ID != field1_val)
									continue;

								vec_items = oTableSourceValue.getItemIds();
								if(FinalItemIds.size() == 0)
								{
									FinalItemIds = vec_items;
									/*for(int32 index = 0 ; index < FinalItemIds.size() ; index++)
									{
										vecTableID.push_back(table_ID);
										vecTableTypeID.push_back(table_Type_ID);
									}*/
								}
								else
								{
									for(int32 i=0; i<vec_items.size(); i++)
									{
										bool16 Flag = kFalse;
										for(int32 j=0; j<FinalItemIds.size(); j++)
										{
											if(vec_items[i] == FinalItemIds[j])
											{
												Flag = kTrue;
												break;
											}				
										}
										if(!Flag )
										{
											FinalItemIds.push_back(vec_items[i]);											
										}										
									}
								}
							}						
							
							if(tableInfo)
								delete tableInfo;


							ItemIDInfo = &FinalItemIds;
						}
					}
					else if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
					{
						do
						{				
							double langId = 91;
							VectorScreenTableInfoPtr tableInfo=
								ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNodeDataList[CurrentSelectedProductRow].getPubId(), CurrentSelectedSubSection, langId );
							if(!tableInfo)
							{
								ptrIAppFramework->LogDebug("ProductSpray::startSpraying::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
								break;
							}
							if(tableInfo->size()==0)
							{ 
								ptrIAppFramework->LogInfo("ProductSpray::startSpraying: table size = 0");
								break;
							}
							CItemTableValue oTableValue;
							VectorScreenTableInfoValue::iterator it;

							bool16 typeidFound=kFalse;
							vector<double> vec_items;
							

							for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
							{//for tabelInfo start				
								oTableValue = *it;				
								vec_items = oTableValue.getItemIds();
							
								if(field1_val == -1)
								{
									if(FinalItemIds.size() == 0)
									{
										FinalItemIds = vec_items;
									}
									else
									{
										for(int32 i=0; i<vec_items.size(); i++)
										{	bool16 Flag = kFalse;
											for(int32 j=0; j<FinalItemIds.size(); j++)
											{
												if(vec_items[i] == FinalItemIds[j])
												{
													Flag = kTrue;
													break;
												}				
											}
											if(!Flag)
												FinalItemIds.push_back(vec_items[i]);
										}
									}
								}
								else
								{
									if(field1_val != oTableValue.getTableTypeID())
										continue;

									if(FinalItemIds.size() == 0)
									{
										FinalItemIds = vec_items;
									}
									else
									{
										for(int32 i=0; i<vec_items.size(); i++)
										{	bool16 Flag = kFalse;
											for(int32 j=0; j<FinalItemIds.size(); j++)
											{
												if(vec_items[i] == FinalItemIds[j])
												{
													Flag = kTrue;
													break;
												}				
											}
											if(!Flag)
												FinalItemIds.push_back(vec_items[i]);
										}
									}
								}
							}//for tabelInfo end
							if(tableInfo)
								delete tableInfo;

						}while(kFalse);
						ItemIDInfo = &FinalItemIds;
					}
				}
				if(!ItemIDInfo)
				{	
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo is NULL");																										
					return;
				}
				
				if(ItemIDInfo->size()==0)
				{
					//CA("ItemIDInfo->size()==0");
					ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo->size()==0");
					break;
				}

				
				//-------
				InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
				if (layoutData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
					break;
				}

				UIDRef newSpreadRef = layoutData->GetSpreadRef();
				
				UID pageUID1 = layoutData->GetPage();
				if(pageUID1 == kInvalidUID)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
					break;
				}

				IDocument* doc1=Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
				if(doc1==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::No doc");
					return;
				}


				 InterfacePtr<IPageList> iPageList(doc1,UseDefaultIID());
				 if (iPageList == nil){
					// CA("iPageList == nil");
					 break;
				 }
		 
				 int32 totalNumOfPages = iPageList->GetPageCount();
				/* if (totalNumOfPages == 1){
					 CA("Operation would leave document with no pages!");
					 break;
				 }*/

				
				 InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)doc1,UseDefaultIID());
				if (iSpreadList1==nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
					return ;
				}
				/*int32 SpreadPage_Count = 0;
				UIDRef CurrentSpradUIDRef =UIDRef::gNull; ;*/
		
				for(int numSp_1=0; numSp_1< iSpreadList1->GetSpreadCount(); numSp_1++)
				{
					UIDRef temp_spreadUIDRef1(database, iSpreadList1->GetNthSpreadUID(numSp_1));
					if(newSpreadRef == temp_spreadUIDRef1)
					{
						//CA("newSpreadRef == temp_spreadUIDRef1");
						InterfacePtr<ISpread> spread(temp_spreadUIDRef1, UseDefaultIID());
						if(!spread)
						{
							ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
							return ;
						}
						int numPages=spread->GetNumPages();
						
						if(numPages == 1)
							pageUID1 = spread->GetNthPageUID(numPages-1);
						else
						{
							int32 pageIndexfound = 0;
							bool16 forBreakFlag = kFalse;
							for(int32 k=0; k<numPages ; k++ )
							{
								UIDList itemsOnPageUIDList(selectUIDList.GetDataBase()) ;
								spread->GetItemsOnPage(k, &itemsOnPageUIDList);

								if(itemsOnPageUIDList.Length() >= selectUIDList.Length())
								{
									for(int32 p=0; p < itemsOnPageUIDList.Length() ; p++ )
									{
										for(int32 q=0; q < selectUIDList.Length() ; q++)
										{
											if(itemsOnPageUIDList.GetRef(p) == selectUIDList.GetRef(q) )
											{
												pageIndexfound = k;
												forBreakFlag = kTrue;
												pageUID1 = spread->GetNthPageUID(pageIndexfound);
												break;
											}										
										}
										if(forBreakFlag)
											break;
									}
									if(forBreakFlag)
										break;
								}
							}							
							
						}
						break;
					}
				}
					

				 int32 SprayPageCount = 0;
				 int32 SpraySpreadCount =0;
				 UIDRef spreadRef = UIDRef::gNull;
				 UIDRef CurrPageRef = UIDRef::gNull;
				 bool16 isNewPageAddInBetweenExistingPage = kFalse;
				 int32 currPageIndex = 0;
				 

				 if(totalNumOfPages > 1 && pageUID1 != iPageList->GetNthPageUID(totalNumOfPages -1))
				 {
					//CA("New Condition");
					for(int32 j=0; j < totalNumOfPages; j++)
					{
						//CA("New Condition qq"); 
						UID LoopPageUID = iPageList->GetNthPageUID(j);
						if(pageUID1 == LoopPageUID)
						{
							
							SprayPageCount = j;
							spreadRef=layoutData->GetSpreadRef();
						
							isNewPageAddInBetweenExistingPage = kTrue;
							
							UIDRef temp_pageRef(database, pageUID1);
							CurrPageRef = temp_pageRef;
							
							currPageIndex = iPageList->GetPageIndex(pageUID1)  ;

							break;
						}
					}
				 }

				UIDRef originalPageUIDRef, originalSpreadUIDRef;

				UID addPage_MasterPageUIDOfTemplate;
				
				if(isNewPageAddInBetweenExistingPage == kTrue)	//----
				{
					originalPageUIDRef = CurrPageRef;
					originalSpreadUIDRef = spreadRef;

					
					

					InterfacePtr<IMasterPage> ptrIMasterPage(originalPageUIDRef, UseDefaultIID());
					if(ptrIMasterPage != NULL)
					{
						addPage_MasterPageUIDOfTemplate = ptrIMasterPage->GetMasterSpreadUID/*GetMasterPageUID*/();//----CS5--
					}

					InterfacePtr<ITransform> transform(originalPageUIDRef, UseDefaultIID());					
					if (!transform) {//CA("!transform ");
						break;
					}
					
					InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
					// Note it's OK if the page does not have margins.
					if (margins) {
						margins->GetMargins(&addPageSplCase_marginBBox.Left(), &addPageSplCase_marginBBox.Top(), &addPageSplCase_marginBBox.Right(), &addPageSplCase_marginBBox.Bottom(), kTrue);
					}
				
				}
				else
				{
					result = sspObj.getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
					if(result == kFalse)
					{ 
						ptrIAppFramework->LogError("AP7_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
						break;
					}
			
				}

				PMRect PagemarginBoxBounds;
				result = sspObj.getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
				if(result == kFalse)
				{
					result = sspObj.getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
					if(result == kFalse)
						break;
				}

				PMRect ItemFramesStencilMaxBounds = kZeroRect;
				vectorBoxBounds ItemFramesBoxBoundVector;

				result = sspObj.getMaxLimitsOfBoxes(ItemFrameUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
				if(result == kFalse)
					break;
				
				PMReal LeftMark = (ItemFramesStencilMaxBounds.Left());
				PMReal BottomMark = (ItemFramesStencilMaxBounds.Bottom());
				PMReal TopMark = (ItemFramesStencilMaxBounds.Top());
				PMReal TopMarkForFrameHeight = (ItemFramesStencilMaxBounds.Top());
				PMReal RightMark = (ItemFramesStencilMaxBounds.Right());

				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");										
					break;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB

				layoutSelectionSuite->SelectPageItems(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
				//copy the selected items
				sspObj.CopySelectedItems();
				//now get the copied item list
				UIDList FirstcopiedBoxUIDList;
				result = sspObj.getSelectedBoxIds(FirstcopiedBoxUIDList);
				if(result == kFalse)
					break;

				PBPMPoint moveToPoints_1(PagemarginBoxBounds.Left(), PagemarginBoxBounds.Top());	
				sspObj.moveBoxes(FirstcopiedBoxUIDList, moveToPoints_1);


				// For First Item of Product
				VectorLongIntValue::iterator it1;
				it1 = ItemIDInfo->begin();
				double FirstItemId = *it1;
				it1++;
				AlphabetArrayCount =0;

				for(int i=0; i<ItemFrameUIDListSize; i++)
				{
					//CA("ItemFrameUIDListSize");
					tagList=itagReader->getTagsFromBox(ItemFrameUIDList.GetRef(i));
					if(tagList.size()<=0)//This can be a Tagged Frame
					{	
						//CA(" tagList.size()<=0 ");
						if(DataSprayerPtr->isFrameTagged(ItemFrameUIDList.GetRef(i)))
						{	
							//CA("Tagged frame");
							tagList.clear();
							//CA("4");
							tagList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
							if(tagList.size()==0)//Ordinary box
							{					
								continue ;
							}	
							
							for(int32 j=0; j <tagList.size(); j++)
							{
								XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
								if(tagList[j].whichTab == 4)
								{
									//CA("tagList[j].whichTab == 4");
									if(tagList[j].imgFlag == 1)
									{
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(FirstItemId));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
										
										attributeValue.clear();
										attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
									}
									else
									{
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(FirstItemId));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));

										PMString childTag("1");
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));

										if(tagList[j].elementId == -803)
										{
											attributeValue.Clear();
											if(AlphabetArrayCount < 26)
												attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
											else
												attributeValue.Append("a");
											
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
										}
										else if(tagList[j].elementId == -827)
										{
											attributeValue.Clear();
											int32 numberKey = AlphabetArrayCount + 1;
											
											attributeValue.AppendNumber(numberKey);

											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
										}
									}
									PMString attributeValue;
									attributeValue.AppendNumber(-777);
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
								}
							}
						}
						else
						{
							// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
							//CA(" else DataSprayerPtr->isFrameTagged");
							InterfacePtr<IHierarchy> iHier(ItemFrameUIDList.GetRef(i), UseDefaultIID());
							if(!iHier)
							{
								//CA(" !iHier >> Continue ");
								continue;
							}
							UID kidUID;				
							int32 numKids=iHier->GetChildCount();				
							IIDXMLElement* ptr = NULL;

							for(int j=0;j<numKids;j++)
							{
								//CA("Inside For Loop");
								bool16 Flag12 =  kFalse;
								kidUID=iHier->GetChildUID(j);
								UIDRef boxRef(ItemFrameUIDList.GetDataBase(), kidUID);			
								TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
								if(NewList.size()<=0)//This can be a Tagged Frame
								{
									NewList.clear();
									NewList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
									if(NewList.size()==0)//Ordinary box
									{					
										continue ;
									}	
									
									for(int32 j=0; j <NewList.size(); j++)
									{
										if(NewList[j].whichTab == 4)
										{
											XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
											if(tagList[j].imgFlag == 1)
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(FirstItemId));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
												
												attributeValue.clear();
												attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4
											}
											else
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(FirstItemId));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));

												PMString childTag("1");
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));

												if(tagList[j].elementId == -803)
												{
													attributeValue.Clear();
													if(AlphabetArrayCount < 26)
														attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
													else
														attributeValue.Append("a");
													
													Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
												}
												else if(tagList[j].elementId == -827)
												{
													attributeValue.Clear();
													int32 numberKey = AlphabetArrayCount + 1;
													
													attributeValue.AppendNumber(numberKey);

													Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
												}
											}
											PMString attributeValue;
											attributeValue.AppendNumber(-777);
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
										}
									}
								}
								else
								{
									for(int32 j=0; j <NewList.size(); j++)
									{
										if(NewList[j].whichTab == 4)
										{
											XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();

											if(NewList[j].imgFlag == 1)
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(FirstItemId));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
												
												attributeValue.clear();
												attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4
											}
											else
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(FirstItemId));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
												Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));

												PMString childTag("1");
												Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));
												if(NewList[j].elementId == -803)
												{
													attributeValue.Clear();
													if(AlphabetArrayCount < 26)
														attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
													else
														attributeValue.Append("a");
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
												}
												else if(tagList[j].elementId == -827)
												{
													attributeValue.Clear();
													int32 numberKey = AlphabetArrayCount + 1;
													
													attributeValue.AppendNumber(PMReal(numberKey));

													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
												}
											}
											PMString attributeValue;
											attributeValue.AppendNumber(-777);
											Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
										}
									}
								}
								//------------
								for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
								{
									NewList[tagIndex].tagPtr->Release();
								}
								if(Flag12)
									break;
							}
						}
					}
					else
					{
						//CA("tagList.size() > 0");
						for(int32 j=0; j <tagList.size(); j++)
						{
							XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
							if(tagList[j].whichTab == 4)
							{
								//CA("tagList[j].whichTab == 4");
								if(tagList[j].imgFlag == 1)
								{
									//CA("tagList[j].imgFlag == 1");
									PMString attributeValue;
									attributeValue.AppendNumber(PMReal(FirstItemId));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
									
									attributeValue.clear();
									attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4
								}
								else
								{
									//CA("tagList[j].imgFlag != 1");
									PMString attributeValue;
									attributeValue.AppendNumber(PMReal(FirstItemId));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));

									PMString childTag("1");
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));

									if(tagList[j].elementId == -803)
									{
										attributeValue.Clear();
										if(AlphabetArrayCount < 26)
											attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
										else
											attributeValue.Append("a");

										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
									}
									else if(tagList[j].elementId == -827)
									{
										attributeValue.Clear();
										int32 numberKey = AlphabetArrayCount + 1;
										
										attributeValue.AppendNumber(numberKey);

										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
									}
								}
								
								PMString attributeValue;								
								attributeValue.AppendNumber(-777);
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
							}
						}
					}

					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						tagList[tagIndex].tagPtr->Release();
					}
				}
				
				DataSprayerPtr->setFlow(isItemHorizontalFlow);
				DataSprayerPtr->ClearNewImageFrameList();

				////// Spraying original frames first:
				ICommandSequence *seq=CmdUtils::BeginCommandSequence();
				for(int i=0; i<LengthList; i++)
				{ 
					//CA("iterating LengthList");
					PMString allInfo;
					tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

					if(tagList.size()<=0)//This can be a Tagged Frame
					{	
						//CA(" tagList.size()<=0 ");
						if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
						{	
							//CA("isFrameTagged");
							bool16 flaG = kFalse;		

							tagList.clear();
							tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));

							if(tagList.size()==0)//Ordinary box
							{					
								continue ;
							}

							//CA("Frame Tags Found");
							InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) {
								break;
							}
					
							selectionManager->DeselectAll(nil); // deselect every active CSB
							layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
						
						//	CA("Before sprayForTaggedBox");
							DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));						
						}
						else
						{
							// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
							//CA(" else DataSprayerPtr->isFrameTagged");
							InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
							if(!iHier)
							{
								//CA(" !iHier >> Continue ");
								continue;
							}
							UID kidUID;				
							int32 numKids=iHier->GetChildCount();				
							IIDXMLElement* ptr = NULL;

							for(int j=0;j<numKids;j++)
							{
								//CA("Inside For Loop");
								kidUID=iHier->GetChildUID(j);
								UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);			
								TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
								if(NewList.size()<=0)//This can be a Tagged Frame
								{
									if(DataSprayerPtr->isFrameTagged(boxRef))
									{	
										//CA("isFrameTagged(selectUIDList.GetRef(i))");
										DataSprayerPtr->sprayForTaggedBox(boxRef);				
									}
									continue;
								}
								DataSprayerPtr->sprayForThisBox(boxRef, NewList);

								//------------
								for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
								{
									NewList[tagIndex].tagPtr->Release();
								}
							}
						}
						continue;
					}
					
					bool16 flaG = kFalse;
					DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
				
					if(flaG)
					{			
						InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
						if (!layoutSelectionSuite) {
							break;
						}
						selectionManager->DeselectAll(nil); // deselect every active CSB

						layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added

						TagList NewTagList;
						NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

						if(NewTagList.size()==0)//Ordinary box
						{
							return ;
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
						{
							NewTagList[tagIndex].tagPtr->Release();
						}
						
					}

					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						tagList[tagIndex].tagPtr->Release();
					}
				}

				UIDList newTempUIDList(ItemFrameUIDList);
				VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

				if(newAddedFrameUIDListAfterSpray.size() > 0)
				{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
					for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
					{					
						newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
					}
				}


				sspObj.moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);
				isSprayedInSingleItemSpray = kTrue;
				CmdUtils::EndCommandSequence(seq);

				isOriginalFrameSprayedInSprayItemPerFrame = kTrue; // Original Frame Sprayed no need to Spray it again
				
				
				result = sspObj.getMaxLimitsOfBoxes(newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
				if(result == kFalse)
					break;

				// Right now only for Vertical Flow
				PMReal LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
				PMReal BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
				PMReal TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
				PMReal RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

				BottomMarkAfterSpray = BottomMarkAfterSpray + 5; 

				PMReal MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
				PMReal MaxRightMarkSprayHZ = RightMarkAfterSpray;

				PMReal box_Height =  ItemFramesStencilMaxBounds.Bottom() - ItemFramesStencilMaxBounds.Top();
				PMReal box_Width  = ItemFramesStencilMaxBounds.Right() - ItemFramesStencilMaxBounds.Left();
				bool16 verticalSprayBottomFrameFlag = kFalse;
				bool16 horizontalSprayBottomFrameFlag = kFalse;
				int32 countVal = 0;

				do
				{
					if(isItemHorizontalFlow == kFalse)
					{
						if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
						{
							// For Vertical Flow.........
							//CA("Going out of Bottom Margin ");
							LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + 5.0;
							TopMarkAfterSpray = TopMark;
							BottomMarkAfterSpray = TopMark /*- 5.0*/;
							/*PMString ASD("LeftMarkAfterSpray : " );
							ASD.AppendNumber(LeftMarkAfterSpray);
							ASD.Append("   TopMarkAfterSpray : ");
							ASD.AppendNumber(TopMarkAfterSpray);
							CA(ASD);*/
							
							if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
							{
								//CA("Add new Page");
								verticalSprayBottomFrameFlag = kTrue;
								countVal++;
								continue;
							}
							

						}
						else
						{
							if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - /*TopMark*/TopMarkForFrameHeight))
							{  // if there is no space for next frame below 
								//CA(" No NEXT frame on Bottom side");
								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) + 5.0;
								BottomMarkAfterSpray = TopMark /*- 5.0*/;
								TopMarkAfterSpray = TopMark;
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
								{
									verticalSprayBottomFrameFlag = kTrue;
									countVal++;
									continue;
								}
								else	
								{
									verticalSprayBottomFrameFlag = kFalse;
									if(countVal >= 1)
									{
										BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - 5*/;
										TopMarkAfterSpray = PagemarginBoxBounds.Top();
										LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + 5;
										RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
									
									}
								}

							}
							else
							{
								//CA("else verticalSpray");
								verticalSprayBottomFrameFlag = kFalse;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom()) + 5;
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
							}

							if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
								MaxRightMarkSprayHZ = RightMarkAfterSpray;
							
							
						}
					}
					else
					{
						if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
						{
							//CA("Going out of Right Margin overflow ");	
							LeftMarkAfterSpray = LeftMark;
							TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
							RightMarkAfterSpray = LeftMark - 5.0;
							if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
							{
								horizontalSprayBottomFrameFlag = kTrue;
								countVal++;
								continue;

							}
							//else
							//{
							//	selectionManager->DeselectAll(nil); // deselect every active CSB
							//	//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin Sharma on 2/07/07
							//	layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/ newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
							//	
							//	if(ItemFramesStencilMaxBounds.Left() != LeftMark)
							//	{
							//		//CA("ItemFramesStencilMaxBounds.Left() != LeftMark");
							//		sspObj.deleteThisBoxUIDList(newTempUIDList);
							//		--it1;
							//		--AlphabetArrayCount;
							//		continue;
							//	}

							//	//PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
							//	//sspObj.moveBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, NewmoveToPoints);
							//	////CA("After Moving Boxes ");
							//	ItemFramesStencilMaxBounds = kZeroRect;
							//	result = sspObj.getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
							//	if(result == kFalse)
							//		break;

							//	LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
							//	BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
							//	TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
							//	RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

							//	if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
							//		MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
							//}

						}
						else
						{
							if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (RightMark - LeftMark))
							{  // if there is no space for next frame on right side 
								//CA("No Next Frame on Right Side ");
								LeftMarkAfterSpray = LeftMark;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

								TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
								RightMarkAfterSpray = LeftMark - 5.0;

								//---------
								if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
								{
									horizontalSprayBottomFrameFlag = kTrue;
									countVal++;
									continue;
								}
								else
								{
									horizontalSprayBottomFrameFlag = kFalse;
									if(countVal >= 1)
									{
										BottomMarkAfterSpray = box_Height + ItemFramesStencilMaxBounds.Bottom();
										TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom()+5;
										
										LeftMarkAfterSpray = PagemarginBoxBounds.Left();//ItemFramesStencilMaxBounds.Left();
										RightMarkAfterSpray = PagemarginBoxBounds.Left() /*- 5*/;//ItemFramesStencilMaxBounds.Right();
										
									
									}
								}


							}else
							{
								//CA("else 111111");
								horizontalSprayBottomFrameFlag = kFalse;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());						

								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
							}
						}

					}

				}while(0);

				//-------
				

				for(; it1 != ItemIDInfo->end(); it1++)
				{
					//CA("Inside Loop ");
					PMReal NewLeft = 0.0;
					PMReal NewTop = 0.0;
					
					if(isItemHorizontalFlow == kFalse)
					{
						//CA("isItemHorizontalFlow == kFalse");
						NewLeft = LeftMarkAfterSpray;
						NewTop =  BottomMarkAfterSpray ;
                        
						if(((PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())) || verticalSprayBottomFrameFlag )
						{
							//CA("AddNewPage : PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right() || verticalSprayBottomFrameFlag");
							
							UID pageUID;
							UIDRef pageRef = UIDRef::gNull;
							UIDRef spreadUIDRef = UIDRef::gNull;

							int32 pageInsertPosition = 0;
							if(isNewPageAddInBetweenExistingPage == kFalse)
							{
								//CA("isNewPageAddInBetweenExistingPage == kFalse");
								Utils<ILayoutUIUtils>()->AddNewPage();
							
								IDocument* fntDoc1 = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(fntDoc1==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::fntDoc==nil");	
									return ;
								}
								IDataBase* database1 = ::GetDataBase(fntDoc1);
								if(database==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::database==nil");			
									return ;
								}
								InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)fntDoc1,UseDefaultIID());
								if (iSpreadList1==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
									return ;
								}
						
								for(int numSp=0; numSp< iSpreadList1->GetSpreadCount(); numSp++)
								{
									if( (iSpreadList1->GetSpreadCount()-1) > numSp )
									{
										continue;
									}
									UIDRef temp_spreadUIDRef(database, iSpreadList1->GetNthSpreadUID(numSp));
									spreadUIDRef = temp_spreadUIDRef;

									InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
									if(!spread)
									{
										ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
										return ;
									}
									int numPages=spread->GetNumPages();
									
									pageUID = spread->GetNthPageUID(numPages-1);
									UIDRef temp_pageRef(database, pageUID);
									pageRef = temp_pageRef;

								}
							}
							else
							{
								currPageIndex++;
								SprayPageCount++;
									
								//CA("----My Page Add Cond.----------");
								IDocument* fntDoc_2 = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(fntDoc_2==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::fntDoc_1==nil");	
									return ;
								}

								
								 database = ::GetDataBase(fntDoc_2);
								if(database==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::database==nil");			
									return ;
								}

								InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)fntDoc_2,UseDefaultIID());
								if (iSpreadList1==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
									return ;
								}
								int32 SpreadPage_Count = 0;
								UIDRef CurrentSpradUIDRef =UIDRef::gNull; ;
						
								for(int numSp=0; numSp< iSpreadList1->GetSpreadCount(); numSp++)
								{
									
									UIDRef temp_spreadUIDRef(database, iSpreadList1->GetNthSpreadUID(numSp));
									

									InterfacePtr<ISpread> spread(temp_spreadUIDRef, UseDefaultIID());
									if(!spread)
									{
										ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
										return ;
									}
									int numPages=spread->GetNumPages();

									SpreadPage_Count = SpreadPage_Count +numPages;
									
									if(SprayPageCount > SpreadPage_Count-1)
										continue;

									if(SprayPageCount <= SpreadPage_Count-1)
									{
										CurrentSpradUIDRef = temp_spreadUIDRef;
										SpraySpreadCount = numSp;
										break;
									}

								}

								 InterfacePtr<IPageList> iPageList(fntDoc_2,UseDefaultIID());
								 if (iPageList == nil){
									 //CA("iPageList == nil");
									 break;
								 }

								UID currentPageUID =  iPageList->GetNthPageUID(SprayPageCount);
								PageType page_TYPE= iPageList->GetPageType(currentPageUID);
								pageInsertPosition =0;
								if(page_TYPE == kLeftPage)
								{
									pageInsertPosition =0; 
								}
								else if(page_TYPE == kRightPage)
								{
									pageInsertPosition = 1; 
								}
								else
								{
									pageInsertPosition = 0; 
								}
								
								this->CreatePages(fntDoc_2, CurrentSpradUIDRef ,1 , pageInsertPosition, kTrue);
								
	
								//-----
								IDocument* fntDoc_1 = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(fntDoc_1==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::fntDoc_1==nil");	
									return ;
								}
								

								
								IDataBase* curr_Database = ::GetDataBase(fntDoc_1);
								if(curr_Database==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::database==nil");			
									return ;
								}

								InterfacePtr<ISpreadList> iSpreadList2((IPMUnknown*)fntDoc_1,UseDefaultIID());
								if (iSpreadList1==nil)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
									return ;
								}

								InterfacePtr<IPageList> iPageList_1(fntDoc_1,UseDefaultIID());
								 if (iPageList_1 == nil){
									// CA("iPageList_1 == nil");
									 break;
								 }
								


								UID pageUID2 = iPageList_1->GetNthPageUID(SprayPageCount);									
								
								UIDRef temp_PageRef1(curr_Database, pageUID2);
								CurrPageRef = temp_PageRef1;
								pageRef = temp_PageRef1;

								int32 index2 = iPageList_1->GetPageIndex(pageUID2);
								
								IControlView* layoutView = Utils<ILayoutUIUtils>()->QueryFrontView();
								if (!layoutView) { 
									//CA("layoutView is NULL");
									break;
								}						
					
								UIDRef firstSreadUIDRef(curr_Database, iSpreadList2->GetNthSpreadUID(SpraySpreadCount)); 
								InterfacePtr<IGeometry> spreadGeo(firstSreadUIDRef, UseDefaultIID());
								if (!spreadGeo) {
									//CA("!spreadGeo");
									break;
								}
								InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(layoutView, spreadGeo, kTrue));
								CmdUtils::ProcessCommand(showSprdCmd);

								if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
									//CA("MakeScrollToSpreadCmd failed");
									break;
								}

								//-------------
								InterfacePtr<IMasterPage> ptrIMasterPage(pageRef, UseDefaultIID());
								if(ptrIMasterPage != NULL)
								{
									ptrIMasterPage->SetMasterPageData/*SetMasterPageUID*/(addPage_MasterPageUIDOfTemplate);//-----CS5---
								}
								
								InterfacePtr<ITransform> transform(pageRef, UseDefaultIID());					
								if (!transform) {//CA("!transform ");
									break;
								}
								
								InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
								// Note it's OK if the page does not have margins.
								if (margins) {
									//CA(" before margins->SetMargins");
									margins->SetMargins(addPageSplCase_marginBBox.Left(), addPageSplCase_marginBBox.Top(), addPageSplCase_marginBBox.Right(), addPageSplCase_marginBBox.Bottom());
								}

								InterfacePtr<IColumns> ptrIColumns(transform, IID_ICOLUMNS);
								// Note it's OK if the page does not have margins.
								if (ptrIColumns) {//CA("before SetColumns");
									ptrIColumns->SetColumns(addPageSplCase_columns);
								}
								
								layoutView->Release();

								CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
								
							}

							result = sspObj.getMarginBounds(pageRef, PagemarginBoxBounds);
							if(result == kFalse)
							{
								result = sspObj.getPageBounds(pageRef, PagemarginBoxBounds);
								if(result == kFalse)
									break;
								
							}
							//CA("NewPage Added");
							NewLeft = PagemarginBoxBounds.Left();
							NewTop = PagemarginBoxBounds.Top();

							MaxBottomMarkSprayHZ = PagemarginBoxBounds.Top();
							MaxRightMarkSprayHZ = PagemarginBoxBounds.Left();

							TopMark =  PagemarginBoxBounds.Top();
							addedNewPageInSprayItemPerFrameFlow = kTrue;
							
							
						}
						
						//else if ((PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom()) && !verticalSprayBottomFrameFlag )
						//{
						//	// For Vertical Flow.........
						//	CA("Going out of Bottom Margin**************");
						//	LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + 5.0;
						//	TopMarkAfterSpray = TopMark;
						//	BottomMarkAfterSpray = TopMark /*- 5.0*/;
						//	
						//	NewLeft = LeftMarkAfterSpray;
						//	NewTop =  BottomMarkAfterSpray;
						//}

					}
					else
					{
						NewLeft = RightMarkAfterSpray + 5.0; // Horizontal Spacing 5 
						NewTop =  TopMarkAfterSpray ; 
						
						//-------
						if(PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom() ||  horizontalSprayBottomFrameFlag)
						{
							//CA("PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom()");
							Utils<ILayoutUIUtils>()->AddNewPage();
							
							IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
							if(fntDoc==nil)
							{
								ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::fntDoc==nil");	
								return ;
							}
							IDataBase* database = ::GetDataBase(fntDoc);
							if(database==nil)
							{
								ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::database==nil");			
								return ;
							}
							InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
							if (iSpreadList==nil)
							{
								ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
								return ;
							}
							
							UID pageUID;
							UIDRef pageRef = UIDRef::gNull;
							UIDRef spreadUIDRef = UIDRef::gNull;

							for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
							{
								if( (iSpreadList->GetSpreadCount()-1) > numSp )
								{
									continue;
								}
								UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
								spreadUIDRef = temp_spreadUIDRef;

								InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
								if(!spread)
								{
									ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
									return ;
								}
								int numPages=spread->GetNumPages();
								pageUID = spread->GetNthPageUID(numPages-1);
								UIDRef temp_pageRef(database, pageUID);
								pageRef = temp_pageRef;

							}

							result = sspObj.getMarginBounds(pageRef, PagemarginBoxBounds);
							if(result == kFalse)
							{
								result = sspObj.getPageBounds(pageRef, PagemarginBoxBounds);
								if(result == kFalse)
									break;
								
							}
							
							NewLeft = PagemarginBoxBounds.Left() /*+ 5.0*/;//PagemarginBoxBounds.Right() + 5.0;
							NewTop = PagemarginBoxBounds.Top();

						}
					}
					AlphabetArrayCount++;		

					selectionManager->DeselectAll(nil); // deselect every active CSB
					layoutSelectionSuite->SelectPageItems(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

					//copy the selected items
					sspObj.CopySelectedItems();
					
					UIDList SecondcopiedBoxUIDList;
					result = sspObj.getSelectedBoxIds(SecondcopiedBoxUIDList);
					if(result == kFalse)
						break;
					
					int32 SecondcopiedBoxUIDListSize = SecondcopiedBoxUIDList.Length();
					if(SecondcopiedBoxUIDListSize == 0)
						continue;
				
					PBPMPoint moveToPoints(NewLeft, NewTop);	
					sspObj.moveBoxes(SecondcopiedBoxUIDList, moveToPoints);

					vectorBoxBounds SecvectorCopiedBoxBoundsBforeSpray;
					PMRect SecCopiedItemMaxBoxBoundsBforeSpray;
					result = kFalse;					
					result = sspObj.getMaxLimitsOfBoxes(SecondcopiedBoxUIDList, SecCopiedItemMaxBoxBoundsBforeSpray, SecvectorCopiedBoxBoundsBforeSpray);

					double SecondItemId = *it1;

					for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
					{
						tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
						if(tagList.size()<=0)//This can be a Tagged Frame
						{	
							//CA(" tagList.size()<=0 A");
							if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
							{	
								tagList.clear();
								tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
								if(tagList.size()==0)//Ordinary box
								{					
									continue ;
								}	
								
								for(int32 j=0; j <tagList.size(); j++)
								{
									XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
									if(tagList[j].whichTab == 4)
									{
										////CA("Item Tag Found");
										//ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
										//break; // break out from for loop
										if(tagList[j].imgFlag == 1)
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(SecondItemId));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
											
											attributeValue.clear();
											attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4

										}
										else
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(SecondItemId));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));

											PMString childTag("1");
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
											
											if(tagList[j].elementId == -803)
											{
												attributeValue.Clear();
												if(AlphabetArrayCount < 26)
													attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
												else
													attributeValue.Append("a");
												
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
											}
											else if(tagList[j].elementId == -827)
											{
												attributeValue.Clear();
												int32 numberKey = AlphabetArrayCount + 1;
												
												attributeValue.AppendNumber(numberKey);

												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
											}
										}
										PMString attributeValue;								
										attributeValue.AppendNumber(-777);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
									}
								}
							}
							else
							{
								// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
								//CA(" else DataSprayerPtr->isFrameTagged");
								InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
								if(!iHier)
								{
									//CA(" !iHier >> Continue ");
									continue;
								}
								UID kidUID;				
								int32 numKids=iHier->GetChildCount();				
								IIDXMLElement* ptr = NULL;

								for(int j=0;j<numKids;j++)
								{
									//CA("Inside For Loop");
									bool16 Flag12 =  kFalse;
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
									TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
									if(NewList.size()<=0)//This can be a Tagged Frame
									{
										NewList.clear();
										NewList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
										if(NewList.size()==0)//Ordinary box
										{					
											continue ;
										}	
										
										for(int32 j=0; j <NewList.size(); j++)
										{
											XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
											XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();

											if(NewList[j].whichTab == 4)
											{
												if(NewList[j].imgFlag == 1)
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(SecondItemId));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
													
													attributeValue.clear();
													attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4
												}
												else
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(SecondItemId));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));

													PMString childTag("1");
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));

													if(NewList[j].elementId == -803)
													{
														attributeValue.Clear();
														if(AlphabetArrayCount < 26)
															attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
														else
															attributeValue.Append("a");
														Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
													}
													else if(tagList[j].elementId == -827)
													{
														attributeValue.Clear();
														int32 numberKey = AlphabetArrayCount + 1;
														
														attributeValue.AppendNumber(numberKey);

														Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
													}
												}

												PMString attributeValue;								
												attributeValue.AppendNumber(-777);
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
											}
										}
									}
									else
									{
										for(int32 j=0; j <NewList.size(); j++)
										{
											XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
											XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();
											if(NewList[j].whichTab == 4)
											{
												if(NewList[j].imgFlag == 1)
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(SecondItemId));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
													
													attributeValue.clear();
													attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue)); //Cs4
												}
												else
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(SecondItemId));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));

													PMString childTag("1");
													Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));

													if(NewList[j].elementId == -803)
													{
														attributeValue.Clear();
														if(AlphabetArrayCount < 26)
															attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
														else
															attributeValue.Append("a");

														Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
													}
													else if(tagList[j].elementId == -827)
													{
														attributeValue.Clear();
														int32 numberKey = AlphabetArrayCount + 1;
														
														attributeValue.AppendNumber(numberKey);

														Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
													}

												}
												PMString attributeValue;								
												attributeValue.AppendNumber(-777);
												Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
											}
										}
									}
									//------------
									for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
									{
										NewList[tagIndex].tagPtr->Release();
									}
								
									if(Flag12)
										break;
								}
							}
						}
						else
						{
							//CA("tagList.size() > 0 A");
							for(int32 j=0; j <tagList.size(); j++)
							{
								XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
								if(tagList[j].whichTab == 4)
								{
									//CA("tagList[j].whichTab == 4");
									if(tagList[j].imgFlag == 1)
									{
										//CA("tagList[j].imgFlag == 1");
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(SecondItemId));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
											
										attributeValue.clear();
										attributeValue.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));//Cs4
									}
									else
									{
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(SecondItemId));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));

										PMString childTag("1");
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));

										if(tagList[j].elementId == -803)
										{
											attributeValue.Clear();
											if(AlphabetArrayCount < 26)
												attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
											else
												attributeValue.Append("a");
											
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
										}
										else if(tagList[j].elementId == -827)
										{
											attributeValue.Clear();
											int32 numberKey = AlphabetArrayCount + 1;
											
											attributeValue.AppendNumber(numberKey);

											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
										}
									}
									PMString attributeValue;								
									attributeValue.AppendNumber(-777);
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
								}
							}
						}

						for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
						{
							tagList[tagIndex].tagPtr->Release();
						}
					}

					////// Spraying Second Frame onwords.
					DataSprayerPtr->setFlow(isItemHorizontalFlow);
					DataSprayerPtr->ClearNewImageFrameList();

					ICommandSequence *seq=CmdUtils::BeginCommandSequence();
					for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
					{ 
						PMString allInfo;	
						//CA("before calling itagReader->getTagsFromBox()");
						tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
						//CA("after calling itagReader->getTagsFromBox()");
						if(tagList.size()<=0)//This can be a Tagged Frame
						{	
							//CA(" tagList.size()<=0 ");
							if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
							{	
								bool16 flaG = kFalse;		

								tagList.clear();
								tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));

								if(tagList.size()==0)//Ordinary box
								{					
									continue ;
								}

								InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
								if (!layoutSelectionSuite) {
									break;
								}
						
								selectionManager->DeselectAll(nil); // deselect every active CSB
								layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
							
								DataSprayerPtr->sprayForTaggedBox(SecondcopiedBoxUIDList.GetRef(i));						
							}
							else
							{
								// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
								//CA(" else DataSprayerPtr->isFrameTagged");
								InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
								if(!iHier)
								{
									//CA(" !iHier >> Continue ");
									continue;
								}
								UID kidUID;				
								int32 numKids=iHier->GetChildCount();				
								IIDXMLElement* ptr = NULL;

								for(int j=0;j<numKids;j++)
								{
									//CA("Inside For Loop");
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
									TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
									if(NewList.size()<=0)//This can be a Tagged Frame
									{
										if(DataSprayerPtr->isFrameTagged(boxRef))
										{	
											//CA("isFrameTagged(selectUIDList.GetRef(i))");
											DataSprayerPtr->sprayForTaggedBox(boxRef);				
										}
										continue;
									}
									DataSprayerPtr->sprayForThisBox(boxRef, NewList);
									//------------
									for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
									{
										NewList[tagIndex].tagPtr->Release();
									}
								}
							}
							continue;
						}
						
						bool16 flaG = kFalse;			
						DataSprayerPtr->sprayForThisBox(SecondcopiedBoxUIDList.GetRef(i), tagList);
						if(flaG)
						{
							//CA("flaG");
							InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) {
								break;
							}
							selectionManager->DeselectAll(nil); // deselect every active CSB

							layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//adde

							TagList NewTagList;
							NewTagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));

							if(NewTagList.size()==0)//Ordinary box
							{
								return ;
							}
							//------------
							for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
							{
								NewTagList[tagIndex].tagPtr->Release();
							}
							
						}

						for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
						{
							tagList[tagIndex].tagPtr->Release();
						}
					}

					sspObj.moveAutoResizeBoxAfterSpray(SecondcopiedBoxUIDList, SecvectorCopiedBoxBoundsBforeSpray);

					CmdUtils::EndCommandSequence(seq);
					
					UIDList newTempUIDList(SecondcopiedBoxUIDList);
					VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

					if(newAddedFrameUIDListAfterSpray.size() > 0)
					{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
						for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
						{					
							newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
						}
					}

					ItemFramesStencilMaxBounds = kZeroRect;
					result = sspObj.getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
					if(result == kFalse)
						break;

					if(isItemHorizontalFlow == kFalse)
					{
						if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
						{
							// For Vertical Flow.........
							//CA("Going out of Bottom Margin ");
							LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + 5.0;
							TopMarkAfterSpray = TopMark;
							BottomMarkAfterSpray = TopMark /*- 5.0*/;
							/*PMString ASD("LeftMarkAfterSpray : " );
							ASD.AppendNumber(LeftMarkAfterSpray);
							ASD.Append("   TopMarkAfterSpray : ");
							ASD.AppendNumber(TopMarkAfterSpray);
							CA(ASD);*/
							
							if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
							{
								verticalSprayBottomFrameFlag = kTrue;
								countVal++;
								//continue;
							}
							//else
							//{
								//CA("else");
								selectionManager->DeselectAll(nil); // deselect every active CSB
								layoutSelectionSuite->SelectPageItems (newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
								if(ItemFramesStencilMaxBounds.Top() != TopMark)
								{
									//CA("ItemFramesStencilMaxBounds.Top() != TopMark Deleting frames");
									sspObj.deleteThisBoxUIDList(newTempUIDList);
									--it1;
									--AlphabetArrayCount;
									continue;
								}
																
								//PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
								//sspObj.moveBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, NewmoveToPoints);
								////CA("After Moving Boxes ");
								//ItemFramesStencilMaxBounds = kZeroRect;
								//result = sspObj.getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
								//if(result == kFalse)
								//	break;

								//LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								//BottomMarkAfterSpray = TopMark /*(ItemFramesStencilMaxBounds.Top())*/;
								//TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								//RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
						//	}


						}
						else
						{
							if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - TopMarkForFrameHeight))
							{  // if there is no space for next frame below 
								//CA(" No NEXT frame on Bottom side");
								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) + 5.0;
								BottomMarkAfterSpray = TopMark ;
								TopMarkAfterSpray = TopMark;
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
								{
									verticalSprayBottomFrameFlag = kTrue;
									countVal++;
									continue;
								}
								else	
								{
									verticalSprayBottomFrameFlag = kFalse;
									if(countVal >= 1)
									{
										BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - 5*/;
										TopMarkAfterSpray = PagemarginBoxBounds.Top();
										LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + 5;
										RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
									
									}
								}

							}
							else
							{
								//CA("else verticalSpray");
								verticalSprayBottomFrameFlag = kFalse;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom()) + 5;
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
							}

							if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
								MaxRightMarkSprayHZ = RightMarkAfterSpray;
							
							
						}
					}
					else
					{
						if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
						{
							//CA("Going out of Right Margin overflow ");	
							LeftMarkAfterSpray = LeftMark;
							TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
							RightMarkAfterSpray = LeftMark - 5.0;
							if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
							{
								horizontalSprayBottomFrameFlag = kTrue;
								countVal++;
								continue;

							}
							else
							{
								selectionManager->DeselectAll(nil); // deselect every active CSB
								layoutSelectionSuite->SelectPageItems( newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
								
								if(ItemFramesStencilMaxBounds.Left() != LeftMark)
								{
									//CA("ItemFramesStencilMaxBounds.Left() != LeftMark");
									sspObj.deleteThisBoxUIDList(newTempUIDList);
									--it1;
									--AlphabetArrayCount;
									continue;
								}

								//PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
								//sspObj.moveBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, NewmoveToPoints);
								////CA("After Moving Boxes ");
								ItemFramesStencilMaxBounds = kZeroRect;
								result = sspObj.getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
								if(result == kFalse)
									break;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
							}

						}
						else
						{
							if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (RightMark - LeftMark))
							{  // if there is no space for next frame on right side 
								//CA("No Next Frame on Right Side ");
								LeftMarkAfterSpray = LeftMark;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

								TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
								RightMarkAfterSpray = LeftMark - 5.0;

								//---------
								if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
								{
									horizontalSprayBottomFrameFlag = kTrue;
									countVal++;
									continue;
								}
								else
								{
									horizontalSprayBottomFrameFlag = kFalse;
									if(countVal >= 1)
									{
										BottomMarkAfterSpray = box_Height + ItemFramesStencilMaxBounds.Bottom();
										TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom()+5;
										
										LeftMarkAfterSpray = PagemarginBoxBounds.Left();//ItemFramesStencilMaxBounds.Left();
										RightMarkAfterSpray = PagemarginBoxBounds.Left() /*- 5*/;//ItemFramesStencilMaxBounds.Right();
										
									
									}
								}


							}else
							{
								//CA("else 111111");
								horizontalSprayBottomFrameFlag = kFalse;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());						

								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
							}
						}

					}

					CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
					
				}

				sspObj.deleteThisBoxUIDList(FirstcopiedBoxUIDList);

			}while(0);
		}
	}

	
	if(isOriginalFrameSprayedInSprayItemPerFrame == kFalse)
	{
		ICommandSequence *seq=CmdUtils::BeginCommandSequence();
		for(int i=0; i<LengthList; i++)
		{ 
			PMString allInfo;	

			tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
			if(tagList.size()<=0)//This can be a Tagged Frame
			{	
				//CA(" tagList.size()<=0 ");
				if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
				{	
					//CA("isFrameTagged");
					bool16 flaG = kFalse;		

					tagList.clear();
					tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));

					if(tagList.size()==0)//Ordinary box
					{					
						continue ;
					}

					//CA("Frame Tags Found");
					InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) {
						break;
					}
			
					selectionManager->DeselectAll(nil); // deselect every active CSB

					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added

					/*if(tagList[0].isTablePresent == kTrue)
					{				
						flaG = kTrue;
						TableStyleUtils TabStyleObj;		
						TabStyleObj.SetTableModel(kTrue);		
						TabStyleObj.GetTableStyle();			
						TabStyleObj.getOverallTableStyle();			
					}*/
					//CA("Before sprayForTaggedBox");
					DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));
						
					//if(flaG)
					//{
					//			
					//	selectionManager->DeselectAll(nil); // deselect every active CSB
					//	layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	

					//	TagList NewTagList;
					//	NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

					//	if(NewTagList.size()==0)//Ordinary box
					//	{
					//		return ;
					//	}
					//		
					//	/*for(int32 j=0; j<NewTagList.size(); j++)
					//	{	
					//		if(tagList[j].isTablePresent == kTrue)
					//		{	
					//			TableStyleUtils TabStyleObj;
					//			TabStyleObj.SetTableModel(kFalse, j);
					//			TabStyleObj.ApplyTableStyle();
					//			TabStyleObj.setTableStyle();
					//			flaG= kFalse;
					//		}
					//	}*/
					//}
					
				}
				else
				{
					// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
					//CA(" else DataSprayerPtr->isFrameTagged");
					InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());	
					if(!iHier)
					{
						//CA(" !iHier >> Continue ");
						continue;
					}
					UID kidUID;				
					int32 numKids=iHier->GetChildCount();				
					IIDXMLElement* ptr = NULL;

					for(int j=0;j<numKids;j++)
					{
						//CA("Inside For Loop");
						kidUID=iHier->GetChildUID(j);
						UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);	
						if(selectUIDList.Contains(boxRef.GetUID())) // Added this if AS THERE WAS ISSUE WITH CUSTOME TABLE SPRAY FOR MASTER ITEM
						{
							//CA("box already present");
							continue;
						}
						TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
						if(NewList.size()<=0)//This can be a Tagged Frame
						{
							if(DataSprayerPtr->isFrameTagged(boxRef))
							{	
								//CA("isFrameTagged(selectUIDList.GetRef(i))");
								DataSprayerPtr->sprayForTaggedBox(boxRef);				
							}
							continue;
						}
						//CA("Before sprayFor 3");
						DataSprayerPtr->sprayForThisBox(boxRef, NewList);
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}
						
				}
				//	CA("Before Continue");
				continue;
			}
			
			bool16 flaG = kFalse;
			
			/*if(tagList[0].isTablePresent == kTrue)
			{	
				flaG = kTrue;
				TableStyleUtils TabStyleObj;			
				TabStyleObj.SetTableModel(kTrue);			
				TabStyleObj.GetTableStyle();				
				TabStyleObj.getOverallTableStyle();			
			}*/

			//CA("Before sprayForThisBox BBB");
			DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
			//CA("After sprayForThisBox");
			if(flaG)
			{	
				//CA("flaG");
				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
					break;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB
				//layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Commented By SAchi shaema on 2/07/07
				layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added

				TagList NewTagList;
				NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

				if(NewTagList.size()==0)//Ordinary box
				{
					return ;
				}
				//------------
				for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
				{
					NewTagList[tagIndex].tagPtr->Release();
				}
					
			//	for(int32 j=0; j<NewTagList.size(); j++)
			//	{	//CA("1");
					//if(tagList[0].isTablePresent == kTrue)
					//{	//CA("2");
					//	TableStyleUtils TabStyleObj;
					//	TabStyleObj.SetTableModel(kFalse, 0);
					//	TabStyleObj.ApplyTableStyle();
					//	TabStyleObj.setTableStyle();
					//	flaG= kFalse;
					//}
			//	}
			}

			for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
			{
				tagList[tagIndex].tagPtr->Release();
			}
		}

		/*PMString ASD1("vectorCopiedBoxBoundsBforeSpray Size : ");
		ASD1.AppendNumber(vectorCopiedBoxBoundsBforeSpray.size());
		ASD1.Append("selectUIDList.size() : ");
		ASD1.AppendNumber(selectUIDList.size());
		CA(ASD1);*/
        
		if(isSprayedInSingleItemSpray == kFalse)
		{
			UIDList newTempUIDList(selectUIDList);
			VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();
			

			//if(newAddedFrameUIDListAfterSpray.size() > 0)
			//{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
			//	for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
			//	{					
			//		newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);

			//		//UIDList OnlyNewTempUIDList(selectUIDList.GetDataBase());
			//		//OnlyNewTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
			//		//PMRect NewItemFramesStencilMaxBounds = kZeroRect;
			//		//vectorBoxBounds NewItemFramesBoxBoundVector;
			//		//
			//		//result = sspObj.getMaxLimitsOfBoxes(OnlyNewTempUIDList, NewItemFramesStencilMaxBounds, NewItemFramesBoxBoundVector);
			//		//if(result == kFalse){
			//		//	// do not do anything 
			//		//}else
			//		//{
			//		//	vectorCopiedBoxBoundsBforeSpray.push_back(NewItemFramesBoxBoundVector[0]);
			//		//}
			//		
			//	}
			//}
			//CA("isSprayedInSingleItemSpray == kFalse");
			sspObj.moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);
			
			
		}
		//CA("15");
		CmdUtils::EndCommandSequence(seq);
		seq = nil;
	}

	//------------
	for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
	{
		tagList[tagIndex].tagPtr->Release();
	}

}

bool16 ProductSpray::getAllBoxIds(UIDList& selectUIDList)
{
	//CA(__FUNCTION__);
	UIDList tempList(selectUIDList.GetDataBase());
	

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::getAllBoxIds::No itagReader");			
		return kFalse;
	}

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	
		//CA("ReturniSelectionManager");
		return kFalse;
	}

	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		//CA("returntxtMisSuite");//
		return kFalse; 
	}
	
	txtMisSuite->GetUidList(selectUIDList);

	const int32 listLength=selectUIDList.Length();	
	if(listLength==0){  //CA("listLength==0 ");
		return kFalse;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinder::ProductSpray::getAllBoxIds:Pointre to DataSprayerPtr not found");
		return kFalse;
	}
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
		{
			//CA(" !iHier >> Continue ");
			continue;
		}
		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();

		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			IIDXMLElement* ptr = NULL;
			for(int j=0;j<numKids;j++)
			{
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);	


				InterfacePtr<IHierarchy> iHierarchy(boxRef, UseDefaultIID());
				if(!iHierarchy)
				{
					//CA(" !iHier >> Continue ");
					continue;
				}
				UID newkidUID;
				
				int32 numNewKids=iHierarchy->GetChildCount();

				bool16 isGroupFrameAgain = kFalse ;
				isGroupFrameAgain = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);

				if(isGroupFrameAgain == kTrue) 
				{
					IIDXMLElement* newPtr = NULL;
					for(int k=0;k<numNewKids;k++)
					{
						//CA("Inside For Loop");
						newkidUID=iHierarchy->GetChildUID(k);
						UIDRef childBoxRef(selectUIDList.GetDataBase(), newkidUID);	

					
						//CA("isGroupFrame == kTrue");
						TagList NewList = itagReader->getTagsFromBox(childBoxRef, &newPtr);

						if(!doesExist(NewList,tempList))
						{
							tempList.Append(newkidUID);				
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}
				}
				else
				{
					TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
					if(!doesExist(NewList,tempList))
					{
						tempList.Append(kidUID);				
					}
					for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
					{
						NewList[tagIndex].tagPtr->Release();
					}
				}			
			}
		}
		else
		{
			//CA("isGroupFrame == kFalse");
			tempList.Append(selectUIDList.GetRef(i).GetUID());
		}
	}

	selectUIDList = tempList;
	return kTrue;
}


bool16 ProductSpray::doesExist(IIDXMLElement * ptr, UIDRef BoxUidref,const UIDList &selectUIDList)
{
	//TagReader tReader;
	IIDXMLElement *xmlPtr;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return kFalse;
		}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return kFalse;
	}
	

	for(int i=0; i<selectUIDList.Length(); i++)
	{	
	//	tReader.getTagsFromBox(selectUIDList.GetRef(i), &xmlPtr);
		TagList Newtaglist = itagReader->getTagsFromBox(selectUIDList.GetRef(i), &xmlPtr);
		if(ptr==xmlPtr)
		{
			//ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::doesExist::ptr==xmlPtr");			
			return kTrue;
		}
		//------------
		for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
		{
			Newtaglist[tagIndex].tagPtr->Release();
		}		
	}
	return kFalse;
}

bool16 ProductSpray::doesExist(TagList &tagList,const UIDList &selectUIDList)
{
//CA("ProductSpray::doesExist");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));
		
		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
		{
			//CA("continue");
			continue;
		}
		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			return kTrue;
		}
	}
	//CA("return kFalse");
	return kFalse;
}


int32 ProductSpray::checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow)
{
	//CA("ProductSpray::doesExist");
	int32 result = 0;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return result;
	}
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return result;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0 )
		{
			//CA("continue");
			continue;
		}

		for(int32 j=0; j <tList.size(); j++)
		{
			if(tList[j].whichTab == 4)
			{
				if(tList[j].isSprayItemPerFrame == 1)
				{
					isItemHorizontalFlow = kTrue;
					result = 1;
					break;
				}
				else if(tList[j].isSprayItemPerFrame == 2)
				{
					isItemHorizontalFlow = kFalse;
					result = 1;
					break;
				}
                
			}
            if(tList[j].isSprayItemPerFrame == 3) // for PV image spray in way of SprayItemPerFrame functionality.
            {
                if(tList[j].flowDir == 0)
                {
                    isItemHorizontalFlow = kTrue;
                }
                else
                {
                    isItemHorizontalFlow = kFalse;
                }
                
                result = 2;
                break;
            }
		}
		//added for clearing the taglist testing
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}

		if(result)
			break;

	}
	//CA("return kFalse");
	return result;
}


ErrorCode ProductSpray::CreatePages(const IDocument* iDocument, const UIDRef spreadToAddTo,const int16 numPagesToInsert, const int16 pageToInsertAt, const bool16 allowShuffle)
{
	//CA("Create page Call");
	ErrorCode status = kFailure;
	do {
		// different page sizes within a document leads to undefined behaviour
		// go to the document preferences to get the current page size
		InterfacePtr<IPageSetupPrefs> iPageSetupPrefs(static_cast<IPageSetupPrefs *>(::QueryPreferences(IID_IPAGEPREFERENCES, iDocument)));
		if (iPageSetupPrefs == nil){
			//CA("iPageSetupPrefs == nil");
			break;
		}

		InterfacePtr<ICommand> iNewPageCmd(CmdUtils::CreateCommand(kNewPageCmdBoss));
		if (iNewPageCmd == nil){
			//CA("iNewPageCmd == nil");
			break;
		}
		InterfacePtr<IPageCmdData> iPageCmdData(iNewPageCmd,UseDefaultIID());
		if (iPageCmdData == nil){
			//CA("iNewPageCmd == nil");
			break;
		}

		ISpread::BindingSide bindingSide = ISpread::kDefaultBindingSide;

		iPageCmdData->SetNewPageCmdData (spreadToAddTo,numPagesToInsert,pageToInsertAt);


		InterfacePtr<IBoolData> iBoolData(iNewPageCmd,UseDefaultIID());
		if (iBoolData == nil){
			//CA("iBoolData == nil");
			break;
		}
		iBoolData->Set(allowShuffle);

		status = CmdUtils::ProcessCommand(iNewPageCmd);

	} while(false);
	return status;	
}

bool16 ProductSpray::checkIsHorizontalFlowFlagPresent(const UIDList &selectUIDList)
{
	//CA("ProductSpray::doesExist");
	bool16 isItemHorizontalFlowFlag = kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0 )
		{
			//CA("continue");
			continue;
		}

		for(int32 j=0; j <tList.size(); j++)
		{
			if(tList[j].whichTab == 4 || tList[j].isSprayItemPerFrame == 3)
			{
				if(tList[j].isSprayItemPerFrame == 1 )
				{
					//CA("if(tList[j].isSprayItemPerFrame == 1)");
					isItemHorizontalFlowFlag = kTrue;
					//return kTrue;
				}
				else if(tList[j].isSprayItemPerFrame == 2 )
				{
					//CA("if(tList[j].isSprayItemPerFrame == 2}");
					isItemHorizontalFlowFlag = kFalse;
					//return kTrue;
				}
				else if(tList[j].flowDir == 0)
				{
					//CA("else if(tList[j].flowDir == 0)");
					isItemHorizontalFlowFlag = kTrue;
					//return kTrue;
				}
				else if(tList[j].flowDir == 1)
				{
					//CA("else if(tList[j].flowDir == 1)");
					isItemHorizontalFlowFlag = kFalse;
					//return kTrue;
				}
			}
		}
		//added for clearing the taglist testing
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}

	}
	
	return isItemHorizontalFlowFlag;
}

//till here