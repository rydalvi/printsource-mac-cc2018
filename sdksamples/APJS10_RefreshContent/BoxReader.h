#ifndef __BOXREADER_H__
#define __BOXREADER_H__

#include "VCPluginHeaders.h"
#include "PageData.h"

#include "TagStruct.h"
#include "ITextModel.h"
#include "SlugStructure.h"

class BoxReader
{
public:
	bool16 getBoxInformation(const UIDRef&, PageData&);
	bool16 getBoxInformation(const UIDRef&, PageData&, int32, int32);
protected:
	bool16 objectIDExists(const PageData&, const TagStruct&, int32*);
	bool16 elementIDExists(const ElementInfoList&, double, PMString& elementName);
	bool16 AppendElementInfo(const TagStruct&, PageData&, int32, bool16 isTaggedFrame=kFalse);
	bool16 AppendObjectInfo(const TagStruct&, PageData&, bool16 isTaggedFrame=kFalse);
//added on 21Jun
	bool16 itemIDelementIDExists(const ElementInfoList& eList, double elementID,double itemID, PMString& elementName);
	void sprayItemTableInTabbedTextForm(const TagStruct & tagInfo,InterfacePtr<ITextModel> & textModel);
	void addAttributesToANewlyCreatedTag(SlugStruct & newTagToBeAdded ,XMLReference & createdElement);
    bool16 GetTextstoryFromBox(ITextModel* iModel, int32 startIndex, int32 finishIndex, PMString& story);
//end on 21Jun	
};

#endif
