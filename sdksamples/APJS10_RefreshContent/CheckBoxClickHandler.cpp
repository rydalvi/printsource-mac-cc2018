#include "VCPluginHeaders.h"
#include "RfhId.h"
#include "CAlert.h"
#include "CEventHandler.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IListBoxController.h"
#include "ListBoxHelper.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("CheckBoxClickHandler.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

class CheckBoxClickHandler : public CEventHandler
{
public:
	CheckBoxClickHandler (IPMUnknown *boss);
	bool16 LButtonUp(IEvent* e) 
	{	//CA("LButtonUp");
		CEventHandler::LButtonUp(e);
		SDKListBoxHelper ListHelper(this, kRfhPluginID);
		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER); // useDefaultIID() not defined for this interface 
		if(listCntl == nil)
		{
			CAlert::InformationAlert("IListBoxController nil");
			return kFalse;
		}

		int32 realrow = listCntl->GetSelected();
		PMString  abc1("RaRow no  : ");
		abc1.AppendNumber(realrow);
		return kFalse;
	}

/*	bool16 LButtonDn(IEvent* e) 
	{
		CEventHandler::LButtonDn(e);
		//CA("LButtonDn");
		SDKListBoxHelper ListHelper(this, kRfhPluginID);
		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER); // useDefaultIID() not defined for this interface 
		if(listCntl == nil)
		{
		CAlert::InformationAlert("IListBoxController nil");
		return kFalse;
		}

		//int32 realrow = listCntl->GetSelected();

		K2Vector<int32> curSelection ;
		listCntl->GetSelected(curSelection ) ;
		const int kSelectionLength =  curSelection.Length();
		if(kSelectionLength<=0)
			return kFalse; 
		PMString  abc1("RaRow no  : ");
		abc1.AppendNumber(curSelection[0]);
		//CA(abc1);
		return kFalse;
	}
*/
};

CREATE_PMINTERFACE(CheckBoxClickHandler, kEventHandlerImpl)


CheckBoxClickHandler::CheckBoxClickHandler(IPMUnknown *boss): CEventHandler(boss)
{
 
}
