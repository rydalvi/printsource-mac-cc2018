#include "VCPluginHeaders.h"
#include "CommonFunctions.h"
#include "PMString.h"

#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "IXMLUtils.h"
//#include "ITextFrame.h"
#include "ITableModel.h"
#include "ITableModelList.h"
#include "IFrameUtils.h"

//7Aug..ItemTable Handling
#include "IXMLAttributeCommands.h"

//7Aug..ItemTable Handling
#include "ITableTextContent.h"
#include "ICellContent.h"
#include "IXMLReferenceData.h"
#include "ITableCommands.h"
#include "ITextParcelList.h"
#include "ITextStoryThreadDict.h"
#include "ITextStoryThread.h"
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "TextIterator.h"
#include "K2SmartPtr.h"
#include "VOSSavedData.h"
#include "IGraphicFrameData.h"
#include "IUIDData.h"
#include "IRangeData.h"
#include "CAlert.h"
#include "CTUnicodeTranslator.h"
//#include "ITagWriter.h"
#include "ITagReader.h"
#include "IDataSprayer.h"
#include "Refresh.h"

#include "ITableGeometry.h"
#include "IItemStrand.h"
#include "ITextModelCmds.h"
#include "ITableUtility.h"
#include "TableUtility.h"

#include <map>
/// Global Pointers
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
map<double,double>itemnew;//by amarjit patil
vector<double>itemIID;//by amarjit patil
vector<double>elementiid;//by amarjit patil
extern vector<double>item_attribute_ids;//by amarjit patil
extern vector<double>itemgroup_attributeids;//by amarjit patil
//MapInMapptr MapInMapptr_var123 =NULL;
extern vector<double>itemgroupids;
extern vector<double> itemgroup_attributeids;//by Amarjit patil
extern bool16 issrtucturebuttenselected ;//= kFalse;
extern bool16 iscontentbuttonselected ;//= kFalse;

extern vector<double>ItemIdsForIndesignTable;
extern vector<double>AttibuteIdsForIndesignTable;

vector<double>ChildIDvec;
vector<double>VectorChildID;
map<double,double>chilIDMap;
extern bool16 Framelevelstructurevariable;
extern bool16 Framelevelcontentvariable;
extern bool16 Booklevelstructurevariable;
extern bool16 Booklevelcontentvariable;
bool16 deleterowornot=kFalse;

vector<double>FItemID;
vector<double>FAttributeID;
vector<PMString>FValue;
multimap<vector<double>, vector<PMString> > *FResult;

extern bool16 IsFrameRefreshSelected;
extern bool16 IsBookrefreshSelected;

///////////

PMString  numToPMString(int32 num);

PMString imageDownLoadPath;

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("CommonFunctions.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";


struct listTableInfo{
    int32 listtableNameTagRowIndex;
    int32 childItemPresent;
    vector<double> tableID;
};

struct rowInfo
{
    int32 rowno;
    bool16 isHeader;
    PMString Data;
    double itemId;
    double tableId;
    int32 numberOfColoumns;
    double parentId;
};

struct TableInfostruct
{
    double tableId;
    double parentId;
    bool16 isHederPresentInTable;
    int32 headerRowStart;
    int32 headerRowEnd;
    int32 bodyRowStart;
    int32 bodyRowEnd;
    vector<double> itemIds;
    PMString FirstRowData;
};


bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story);
PMString prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ')
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}
//Method Purpose: remove the Special character from the Name. 
PMString keepOnlyAlphaNumeric(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}
	return tagName ;
}
void  addAttributesToANewlyCreatedTag(SlugStruct & newTagToBeAdded ,XMLReference & createdElement)
{
		//IIDXMLElement * createdTagElement = createdElement.Instantiate();
		InterfacePtr<IIDXMLElement>createdTagElement(createdElement.Instantiate());

		PMString attributeName;
		PMString attributeValue;
		ErrorCode err = kFailure;

		attributeName = "ID";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.elementId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue)); //Cs4
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "typeId";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.typeId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "header";
		attributeValue.AppendNumber(newTagToBeAdded.header);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "isEventField";
		if(newTagToBeAdded.isEventField)
			attributeValue.AppendNumber(1);
		else
			attributeValue.AppendNumber(-1);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "deleteIfEmpty";
		attributeValue.AppendNumber(newTagToBeAdded.deleteIfEmpty);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "dataType";
		attributeValue.AppendNumber(newTagToBeAdded.dataType);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "isAutoResize";
		attributeValue.AppendNumber(newTagToBeAdded.isAutoResize);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "LanguageID";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.LanguageID));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "index";
		attributeValue.AppendNumber(newTagToBeAdded.whichTab);
		///createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "pbObjectId";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.pbObjectId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "parentID";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.parentId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "childId";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.childId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "sectionID";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.sectionID));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "parentTypeID";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.parentTypeId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));//Cs4
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "isSprayItemPerFrame";
		attributeValue.AppendNumber(newTagToBeAdded.isSprayItemPerFrame);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));//Cs4
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "catLevel";
		attributeValue.AppendNumber(newTagToBeAdded.catLevel);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));//Cs4
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "imgFlag";
		attributeValue.AppendNumber(newTagToBeAdded.imgFlag);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "imageIndex";
		attributeValue.AppendNumber(newTagToBeAdded.imageIndex);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "flowDir";
		attributeValue.AppendNumber(newTagToBeAdded.flowDir);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "childTag";
		attributeValue.AppendNumber(newTagToBeAdded.childTag);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "tableFlag";
		attributeValue.AppendNumber(newTagToBeAdded.tableFlag);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "tableType";
		attributeValue.AppendNumber(newTagToBeAdded.tableType);
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "tableId";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.tableId));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
		
		attributeName = "rowno";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.row_no));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "colno";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.col_no));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "field1";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.field1));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "field2";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.field2));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "field3";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.field3));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "field4";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.field4));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();

		attributeName = "field5";
		attributeValue.AppendNumber(PMReal(newTagToBeAdded.field5));
		//createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
		attributeName.Clear();
		attributeValue.Clear();
    
        attributeName = "groupKey";
        attributeValue.Append((newTagToBeAdded.groupKey));
        //createdTagElement->AddAttribute(WideString(attributeName),WideString(attributeValue));
        err = Utils<IXMLAttributeCommands>()->CreateAttribute(createdElement,WideString(attributeName),WideString(attributeValue));
        attributeName.Clear();
        attributeValue.Clear();
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////3Aug Item Table Preset Start ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
void sprayItemItemTableScreenPrintInTabbedTextForm( TagStruct & tagInfo,InterfacePtr<ITextModel> & textModel)
{
	//CA("sprayItemItemTableScreenPrintInTabbedTextForm");
	PMString elementName,colName;
	//7Jun
	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	UIDRef  txtModelUIDRef = ::GetUIDRef(textModel);
	//InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	
	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;
	do{
						
						
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

		double pubId = tagInfo.parentId;
		double sectionId = tagInfo.sectionID;
		double typeId = tagInfo.typeId;
		double parentTypeId = tagInfo.parentTypeID;
		int32 isAutoResize = tagInfo.isAutoResize;
		double languageId = tagInfo.languageID;
		//IMPORTANT: rowno stored the PBObjectID i.e the itemID.
		double objectId = tagInfo.parentId;//tagInfo.pbObjectId;//tagInfo.rowno;
		double pbObjId = tagInfo.pbObjectId;
		int32 colnoForTableFlag = tagInfo.header;//tagInfo.colno;
		
		//ptrIAppFramework->clearAllStaticObjects();
		VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pubId, sectionId, languageId );
		if(!tableInfo)
		{
			//isTableSizeZero = kTrue;
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::sprayItemItemTableScreenPrintlnTabbedTextForm::tableinfo is nil");
			break;
		}

		if(tableInfo->size()==0)
		{ 
			ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions::sprayItemItemTableScreenPrintlnTabbedTextForm::tableInfo->size()==0");
			//isTableSizeZero = kTrue;
			break;
		}
		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;
		bool16 typeidFound=kFalse;
		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{
			oTableValue = *it;
			if(oTableValue.getTableTypeID() == tagInfo.typeId && oTableValue.getTableID() == tagInfo.tableId)
			{   				
				typeidFound=kTrue;				
				break;
			}
		}
		if(!typeidFound)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::sprayItemItemTableScreenPrintlnTabbedTextForm::typeidFound==nil");
			break;
		}
		if(tableInfo)
			delete tableInfo;

		vector<double>  vectorOfTableHeader = oTableValue.getTableHeader();
		vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
		vector<double>  vec_items = oTableValue.getItemIds();
		bool8 isTranspose = oTableValue.getTranspose();
		vector<vector<SlugStruct> > RowList;
		vector<SlugStruct> listOfNewTagsAdded;

		int32 noOfColumns = static_cast<int32>(vectorOfTableHeader.size());
		int32 noOfRows = static_cast<int32>(vec_items.size());

		IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
		int32 tagStartPos = 0;
		int32 tagEndPos =0; 
		
		Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);

		tagStartPos = tagStartPos + 1;
		tagEndPos = tagEndPos -1;

		tabbedTextData.Clear();
		replaceItemWithTabbedTextFrom = tagStartPos;
		replaceItemWithTabbedTextTo	= tagEndPos;
		
		if(!isTranspose)
		{
			/*  
			In case of Transpose == kFalse..the table gets sprayed like 
				Header11 Header12  Header13 ...
				Data11	 Data12	   Data13
				Data21	 Data22	   Data23
				....
				....
				So first row is Header Row and subsequent rows are DataRows.
			*/
			//The for loop below will spray the Table Header in left to right fashion.
			//Note typeId == -2 for TableHeaders.
			if(colnoForTableFlag == 1/*colnoForTableFlag != -555*/)
			{
				for(int32 headerIndex = 0;headerIndex < noOfColumns;headerIndex++)
				{
					double elementId = vectorOfTableHeader[headerIndex];

					SlugStruct newTagToBeAdded;
					newTagToBeAdded.elementId = elementId;
					newTagToBeAdded.typeId = -1;//-2;
					newTagToBeAdded.header = 1;
					newTagToBeAdded.parentId = pubId;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.parentTypeId = typeId;
					newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageId );
					newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
					newTagToBeAdded.LanguageID = languageId;			//PMString colName;
					newTagToBeAdded.whichTab = 4 ;
					newTagToBeAdded.sectionID = sectionId ;
					newTagToBeAdded.isAutoResize = isAutoResize;
					newTagToBeAdded.tableFlag = -1001;//-12;
					newTagToBeAdded.row_no=-1/*objectId*/;
					newTagToBeAdded.pbObjectId = pbObjId;
					newTagToBeAdded.col_no = -1/*colnoForTableFlag*/;	
					newTagToBeAdded.tableId = tagInfo.tableId;
					newTagToBeAdded.tableType = tagInfo.tableType;
					
					 //listOfNewTagsAdded.push_back(newTagToBeAdded);
					
					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
					
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);
					if(headerIndex != noOfColumns - 1)
					{
						insertPMStringText.Append("\t");						
					}
					else
					{
						insertPMStringText.Append("\r");
					}

					tabbedTextData.Append(insertPMStringText);
					

					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
					/*WideString insertText(insertPMStringText);
					if(headerIndex == 0)
					{
						//CA("headerIndex == 0");
						textModel->Replace(kTrue,tagStartPos,tagEndPos-tagStartPos + 1 ,&insertText);
						//textModel->Replace(kTrue,1 ,7,&insertText);
						//return;
					}
					else
					{
						textModel->Insert(kTrue,tagStartPos,&insertText);
					}*/				
					 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
					
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;
					 listOfNewTagsAdded.push_back(newTagToBeAdded);
					 // XMLReference createdElement;
					// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
					 tagStartPos = tagEndPos+ 1 + 2;
		
				}
				//RowList.push_back(listOfNewTagsAdded);
				//listOfNewTagsAdded.clear();
			}

			//This for loop will spray the Item_copy_attributes value
			for(int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++)
			{
				int32 itemId = vec_items[itemIndex];
				for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
				{
					double elementId = vectorOfTableHeader[headerIndex];
					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

					SlugStruct newTagToBeAdded;
					newTagToBeAdded.elementId = elementId;
					newTagToBeAdded.typeId = -1/*itemId*/;
					newTagToBeAdded.childId = itemId;
					newTagToBeAdded.childTag = 1;
					newTagToBeAdded.parentId = pubId;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.parentTypeId = typeId;
					/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue){
						VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								newTagToBeAdded.elementName = vecPtr->at(0).getObjectValue();
							delete vecPtr;
						}
					}
					else*/
					{
						newTagToBeAdded.elementName =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID, tagInfo.sectionID, /*kTrue*/kFalse);//23OCT09//Amit
					}
					newTagToBeAdded.TagName = prepareTagName(tagName);
					newTagToBeAdded.LanguageID = languageId;			//PMString colName;
					newTagToBeAdded.whichTab =4 ;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.sectionID = sectionId;
					newTagToBeAdded.isAutoResize = isAutoResize;
					newTagToBeAdded.tableFlag = -1001;//-12;
					newTagToBeAdded.row_no= -1/*objectId*/;
					newTagToBeAdded.pbObjectId=pbObjId;
					newTagToBeAdded.col_no=-1;						
					
					newTagToBeAdded.tableId = tagInfo.tableId;
					newTagToBeAdded.tableType = tagInfo.tableType;	
					 //start
					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);
					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
					if(headerIndex != noOfColumns - 1)
					{
						insertPMStringText.Append("\t");
						
					}
					else if(itemIndex != noOfRows-1)
					{
						insertPMStringText.Append("\r");
					}
					tabbedTextData.Append(insertPMStringText);
					//WideString insertText(insertPMStringText);
					
					//textModel->Insert(kTrue,tagStartPos,&insertText);
									
					 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;
					 listOfNewTagsAdded.push_back(newTagToBeAdded);
					 //XMLReference createdElement;
					 //Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

					 tagStartPos = tagEndPos+ 1 + 2;

					 //end
					 
					//listOfNewTagsAdded.push_back(newTagToBeAdded);
		
				}

				//RowList.push_back(listOfNewTagsAdded);
				//listOfNewTagsAdded.clear();

			}				
		}//end of if IsTranspose == kFalse
		else
		{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
			/*  
			In case of Transpose == kTrue..the table gets sprayed like 

				Header11 Data11 Data12 Data13..
				Header22 Data21 Data22 Data23..
				Header33 Data31 Data32 Data33..
				....
				....
			So First Column is HeaderColumn and subsequent columns are Data columns.
			*/
			
			for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
			{
				double elementId = vectorOfTableHeader[headerIndex];
				//noOfRows in the for loop below is noOfRows + 1.
				//As we have to add first column which is of table headers.
				//In the earlier part(i.e in the if block of IsTranspose == kFalse)
				//we have added the header row in a separate loop.
				for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
				{
					double itemId =-1;
					int32 header = -1;
					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
					PMString elementName;
					SlugStruct newTagToBeAdded;
					if(itemIndex == 0 && colnoForTableFlag == 1/*colnoForTableFlag == -555*/)
					{
						//itemId == -2 for header.
						//itemId = -2;
						itemId = -1;
						elementName = tagName;
						header = 1;
					}
					else
					{
						itemId = vec_items[itemIndex-1];
						/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue){
							VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
							if(vecPtr != NULL)
							{
								if(vecPtr->size()> 0)
									elementName = vecPtr->at(0).getObjectValue();
								delete vecPtr;
							}
								
							
						}
						else*/
						{
							elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID, tagInfo.sectionID, /*kTrue*/kFalse);//23OCT09//Amit
						}
						header = -1;
					}			
					newTagToBeAdded.elementId = elementId;
					newTagToBeAdded.typeId = -1/*itemId*/;
					newTagToBeAdded.header = header;
					newTagToBeAdded.childId = itemId;
					if(itemId == -1)
						newTagToBeAdded.childTag = 1;
					else
						newTagToBeAdded.childTag = -1;
					newTagToBeAdded.parentId = pubId;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.parentTypeId = typeId;
					newTagToBeAdded.elementName = elementName;
					newTagToBeAdded.TagName = prepareTagName(tagName);
					newTagToBeAdded.LanguageID = languageId;			//PMString colName;
					newTagToBeAdded.whichTab =4 ;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.sectionID = sectionId;
					newTagToBeAdded.isAutoResize = isAutoResize;
					newTagToBeAdded.tableFlag = -1001;//-12;
					newTagToBeAdded.row_no=-1; //objectId;
					newTagToBeAdded.pbObjectId = pbObjId;
					newTagToBeAdded.col_no=-1;
					newTagToBeAdded.tableId = tagInfo.tableId;
					newTagToBeAdded.tableType = tagInfo.tableType;
					
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);
					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
					if(itemIndex != noOfRows)
					{
						insertPMStringText.Append("\t");
						
					}
					else
					{
						insertPMStringText.Append("\r");
					}
					tabbedTextData.Append(insertPMStringText);
								
					 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;

					 listOfNewTagsAdded.push_back(newTagToBeAdded);
					 
					 tagStartPos = tagEndPos+ 1 + 2;						 
				}				

			}

		}
		PMString textToInsert("");
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			textToInsert=tabbedTextData;					
		}
		else{
			textToInsert=iConverter->translateString(tabbedTextData);
			iConverter->ChangeQutationMarkONOFFState(kFalse);
		}
		//WideString insertText(textToInsert);	
//CS3 Change	textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
		//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
		ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
		{

			SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
			XMLReference createdElement;
			Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
			addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
		}		
			
	}while(kFalse);
	
	
//			}//end for1
}//end of function
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////3Aug Item Table Preset End /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


//////////////////added on 21Jun//////////////////////
//This code is for refresh
//This function is copied from the CDataSprayer.cpp file from AP45_DataSprayer.pln
//Only some minor changes are made in this function.
//As the same function in CDataSprayer.cpp works on the virginTag and here this funciton
//works on sprayedTag
void sprayItemTableInTabbedTextForm(const TagStruct & tagInfo,InterfacePtr<ITextModel> & textModel)
{
	//CA("inside sprayItemTableInTabbedTextForm");
	PMString elementName,colName;
	//7Jun
	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	UIDRef  txtModelUIDRef = ::GetUIDRef(textModel);
	//InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	
	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;
	
			
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}				

		IIDXMLElement* itemTagPtr = tagInfo.tagPtr;
		if(itemTagPtr == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::sprayItemTablelnTabbedTextForm::itemTagPtr == nil");
			break;
		}
				
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					

		/////end   8Jun///////
		
		double sectionID = tagInfo.sectionID;
		double productID = tagInfo.parentId;
		double parentTypeID = tagInfo.parentTypeID;

		int32 colnoForTableFlag = tagInfo.header;//tagInfo.colno;
		//PublicationNode pNode;	
		VectorScreenTableInfoPtr tableInfo=NULL;
		if(/*pNode.getIsONEsource()*/ptrIAppFramework->get_isONEsourceMode())
		{
			// For ONEsource mode
			
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(productID);
		}else
		{
			//For Publication
			tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, productID, tagInfo.languageID);
		}
		if(!tableInfo)
		{
			//isTableSizeZero = kTrue;
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::sprayItemTablelnTabbedTextForm::tableinfo is nil");
			break;
		}

		if(tableInfo->size()==0)
		{ 				
			//isTableSizeZero = kTrue;
			break;
		}
		

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;
		bool16 typeidFound=kFalse;
		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{
			oTableValue = *it;
			
			if(oTableValue.getTableTypeID() == tagInfo.typeId && oTableValue.getTableID() == tagInfo.tableId)
			{   	
				typeidFound=kTrue;				
				break;
			}
		}
		if(!typeidFound)
		{	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::sprayItemTablelnTabbedTextForm::!typeidFound");
			break;
		}

		if(tableInfo)
			delete tableInfo;
		vector<double>  vectorOfTableHeader = oTableValue.getTableHeader();
		vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
		vector<double> vec_items = oTableValue.getItemIds();
		bool8 isTranspose = oTableValue.getTranspose();
		vector<vector<SlugStruct> > RowList;
		vector<SlugStruct> listOfNewTagsAdded;

		int32/*int64*/ noOfColumns =static_cast<int32> (vectorOfTableHeader.size());
		int32/*int64*/ noOfRows =static_cast<int32>(vec_items.size());

		
		int32 tagStartPos = 0;
		int32 tagEndPos =0; 
		
		
		Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
		//CA_NUM("ItemTagStartPos :",tagStartPos);
		//CA_NUM("ItemTagEndPos :",tagEndPos);		

		tagStartPos = tagStartPos + 1;
		tagEndPos = tagEndPos -1;

		tabbedTextData.Clear();
		replaceItemWithTabbedTextFrom = tagStartPos;
		replaceItemWithTabbedTextTo	= tagEndPos;
		
		if(!isTranspose)
		{
			
			if(colnoForTableFlag == 1/*colnoForTableFlag != -555*/)
			{
				
				for(int32 headerIndex = 0;headerIndex < noOfColumns;headerIndex++)
				{
					double elementID = vectorOfTableHeader[headerIndex];

					 SlugStruct newTagToBeAdded;
					 newTagToBeAdded.elementId = elementID;
					 newTagToBeAdded.typeId = -1;//-2;
					 newTagToBeAdded.header = 1;
					 newTagToBeAdded.parentId = productID;
					 newTagToBeAdded.imgFlag = 0;
					 newTagToBeAdded.parentTypeId = parentTypeID;
					 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,tagInfo.languageID );
					 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
					 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
					 //newTagToBeAdded.whichTab =3 ;
					 newTagToBeAdded.whichTab =4 ;
					 newTagToBeAdded.imgFlag = 0;
					 newTagToBeAdded.sectionID = sectionID ;
					 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
					 newTagToBeAdded.tableFlag = -12;
					 newTagToBeAdded.row_no=-1;
					 newTagToBeAdded.col_no=-1;
					 

					 //listOfNewTagsAdded.push_back(newTagToBeAdded);
					
					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
					
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);

					if(headerIndex != noOfColumns - 1)
					{
						insertPMStringText.Append("\t");						
					}
					else
					{
						insertPMStringText.Append("\r");
					}

					tabbedTextData.Append(insertPMStringText);
					

					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						
					 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
					
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;
					 listOfNewTagsAdded.push_back(newTagToBeAdded);						
					 tagStartPos = tagEndPos+ 1 + 2;
		
				}
				//RowList.push_back(listOfNewTagsAdded);
				//listOfNewTagsAdded.clear();
			}

			
			for(int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++)
			{
				double itemID = vec_items[itemIndex];
				for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
				{
					double elementID = vectorOfTableHeader[headerIndex];
					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,tagInfo.languageID );

					SlugStruct newTagToBeAdded;
					newTagToBeAdded.elementId = elementID;
					newTagToBeAdded.typeId = -1;//itemID;
					newTagToBeAdded.childId = itemID;
					newTagToBeAdded.childTag = 1;
					newTagToBeAdded.parentId = productID;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.parentTypeId = parentTypeID;
					/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementID) == kTrue){
						VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemID,elementID,tagInfo.sectionID);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								newTagToBeAdded.elementName = vecPtr->at(0).getObjectValue();
							delete vecPtr;
						}
					}
					else*/
					{
						newTagToBeAdded.elementName =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,tagInfo.languageID, tagInfo.sectionID, /*kTrue*/kFalse);//23OCT09//Amit
					}
					newTagToBeAdded.TagName = prepareTagName(tagName);
					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
					// newTagToBeAdded.whichTab =3 ;
					newTagToBeAdded.whichTab =4 ;
					newTagToBeAdded.imgFlag = 0;
					newTagToBeAdded.sectionID = sectionID;
					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
					newTagToBeAdded.tableFlag = -12;
					newTagToBeAdded.row_no=-1;
					newTagToBeAdded.col_no=-1;

					 //start
					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);
					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
					if(headerIndex != noOfColumns - 1)
					{
						insertPMStringText.Append("\t");
						
					}
					else if(itemIndex != noOfRows-1)
					{
						insertPMStringText.Append("\r");
					}
					tabbedTextData.Append(insertPMStringText);
					//WideString insertText(insertPMStringText);
					
					//textModel->Insert(kTrue,tagStartPos,&insertText);
									
					 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;
					 listOfNewTagsAdded.push_back(newTagToBeAdded);
					 //XMLReference createdElement;
					 //Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

					 tagStartPos = tagEndPos+ 1 + 2;

					 //end
					 
					//listOfNewTagsAdded.push_back(newTagToBeAdded);
		
				}

				//RowList.push_back(listOfNewTagsAdded);
				//listOfNewTagsAdded.clear();

			}				
		}//end of if IsTranspose == kFalse
		else
		{//else of if IsTranspose ==kFalse...i.e headers are in first column
				
			
			for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
			{
				double elementID = vectorOfTableHeader[headerIndex];
				//noOfRows in the for loop below is noOfRows + 1.
				//As we have to add first column which is of table headers.
				//In the earlier part(i.e in the if block of IsTranspose == kFalse)
				//we have added the header row in a separate loop.
				for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
				{
					double itemID =-1;
					int32 header = -1;
					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,tagInfo.languageID );
					PMString elementName;
					SlugStruct newTagToBeAdded;
					if(itemIndex == 0 && colnoForTableFlag == 1/*colnoForTableFlag != -555*/)
					{
						//itemId == -2 for header.
						//itemID = -2;
						itemID = -1;
						elementName = tagName;
						header = 1;
					}
					else
					{
						if(itemIndex == 0 && colnoForTableFlag == -1/*-555*/)
						{
							continue;
						}
						itemID = vec_items[itemIndex-1];
						/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementID) == kTrue){
							VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemID,elementID,tagInfo.sectionID);
							if(vecPtr != NULL)
							{
								if(vecPtr->size()> 0)
									elementName = vecPtr->at(0).getObjectValue();
								delete vecPtr;
							}
						}
						else*/
						{
							elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,tagInfo.languageID, tagInfo.sectionID, /*kTrue*/kFalse);//23OCT09//Amit
						}
						header = -1;
					}				
					 newTagToBeAdded.elementId = elementID;
					 newTagToBeAdded.typeId = -1; //itemID;
					 newTagToBeAdded.childId = itemID;
					 newTagToBeAdded.childTag = 1;
					 newTagToBeAdded.header = header;
					 newTagToBeAdded.parentId = productID;
					 newTagToBeAdded.imgFlag = 0;
					 newTagToBeAdded.parentTypeId = parentTypeID;
					 newTagToBeAdded.elementName = elementName;
					 newTagToBeAdded.TagName = prepareTagName(tagName);
					 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
					// newTagToBeAdded.whichTab =3 ;
					 newTagToBeAdded.whichTab =4 ;
					 newTagToBeAdded.imgFlag = 0;
					 newTagToBeAdded.sectionID = sectionID;
					 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
					 newTagToBeAdded.pbObjectId = tagInfo.pbObjectId;
					 newTagToBeAdded.tableFlag = -12;
					 newTagToBeAdded.row_no=-1;
					 newTagToBeAdded.col_no=-1;
					
					PMString insertPMStringText;
					insertPMStringText.Append(newTagToBeAdded.elementName);
					if(!iConverter)
					{
						insertPMStringText=insertPMStringText;					
					}
					else
						insertPMStringText=iConverter->translateString(insertPMStringText);
					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
					if(itemIndex != noOfRows)
					{
						insertPMStringText.Append("\t");
						
					}
					else
					{
						insertPMStringText.Append("\r");
					}
					tabbedTextData.Append(insertPMStringText);
								
					 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
					 newTagToBeAdded.tagStartPos=tagStartPos;
					 newTagToBeAdded.tagEndPos=tagEndPos;

					 listOfNewTagsAdded.push_back(newTagToBeAdded);
					 
					 tagStartPos = tagEndPos+ 1 + 2;			 
		
				}
			}
		}
		PMString textToInsert("");
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			textToInsert=tabbedTextData;					
		}
		else{
			textToInsert=iConverter->translateString(tabbedTextData);
			iConverter->ChangeQutationMarkONOFFState(kFalse);
		}
		//WideString insertText(textToInsert);	
//CS3 Change	textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
		//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
		ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);


		if(iConverter)
			iConverter->ChangeQutationMarkONOFFState(kTrue);
		for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
		{

			SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
			XMLReference createdElement;
			Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
			addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

		}
		
		
	}while(kFalse);
	
	
//			}//end for1
}//end of function

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
ErrorCode RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
/*
	Note that the functionality implemented in this function is copied and modified 
	from the similar function  
	ErrorCode TableUtility :: SprayIndividualItemCopyAttributesInTable
	(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
	present in the TableUtility.cpp in AP45_DataSprayer plugin source code.
	For detailed documenation of the functionality please refere the original above 
	mentioned function.As comments are removed here for brevity.
*/
	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;//breaks the do{}while(kFalse)
		}		 	
		
		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrameUID == kInvalidUID");
			break;//breaks the do{}while(kFalse)
		}
/*	//////////CS3 Change
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrame == nil");
			break;//breaks the do{}while(kFalse)	
		}	
*/	
//////////////////	Added by Amit		 
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			//CA("Its Not MultiColumn");	 
			break;
		}

		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			break;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			break;
		}

//////////////////	End
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textModel == nil");
			break;//breaks the do{}while(kFalse)
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;//breaks the do{}while(kFalse)
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		
		
		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;//breaks the do{}while(kFalse)
		}
				
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == nil)
				continue;//continues the for..tableIndex
			
			UIDRef tableRef(::GetUIDRef(tableModel)); 
			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
			IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}			
			//Get the SectionID,ProductID from the tableXMLElementTagPtr
			PMString strSectionID =  tableXMLElementPtr->GetAttributeValue(WideString("sectionID")); //Cs4
			PMString strProductID =  tableXMLElementPtr->GetAttributeValue(WideString("parentID")); //Cs4
			PMString strLanguageID = tableXMLElementPtr->GetAttributeValue(WideString("LanguageID")); //Cs4

			//added on 25Setp..EventPrice addition.
			//This is important addition.You will come to know from this value whether the table
			//contains Item's data or Product's data.
			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
			//ended on 25Setp..EventPrice addition.
			
			double sectionID = strSectionID.GetAsDouble();
			double productID = strProductID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();

           /* This part is commented on 16Aug.
		   //It is not required.As tags inside the table cells are refreshed individually.
			VectorScreenTableInfoPtr tableInfo=
				ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID,productID );
			if(!tableInfo)
				{
						
					CA("tableinfo is nil");
					break;//breaks the for..tableIndex
				}
			if(tableInfo->size()==0)
				{ 
					CA(" tableInfo->size()==0");						
					break;//breaks the for..tableIndex
				}
					
			CObjectTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<int32> vec_items;
			FinalItemIds.clear();

			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{				
				oTableValue = *it;				
				vec_items = oTableValue.getItemIds();
			
				if(FinalItemIds.size() == 0)
				{
					FinalItemIds = vec_items;
				}
				else
				{
					for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
					{	
						bool16 Flag = kFalse;
						for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
						{
							if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
							{
								Flag = kTrue;
								break;
							}				
						}
						if(!Flag)
							FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
					}

				}
			}
			
			int32 TotalNoOfItems = FinalItemIds.size();
			if(TotalNoOfItems <= 0)
				break;//breaks the for..tableIndex
			//End comment 16Aug.
			*/
			//////////////////////////////////////////////////////////////////////
			/*
			////////////////////////////////////
			collect the CellXMLElementTag from the first row.
			The structure of the TableXMLElementTag is as below
			  TableXMLElementTag|(TableXMLElementTag has all printsource attributes.)
								|
								|
								--CellXMLElementTag|(CellXMLElementTag doesn't have any printsource attributes.)
												   |
												   |
												    --CellTextXMLElementTag  (TextXMLElementTag has all the printsource attributes.)
			We are basically interested in the CellTextXMLElementTag.
			////////////////////////////////////////
			*/

				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
				{//start for tagIndex = 0
				
					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}

					for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
					{ // iterate thruogh cell's tags
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == nil)
						{
							continue;
						}
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID")); //Cs4
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
						PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
						PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));

						//added by Tushar on 26/12/06
						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));						
						PMString strDataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));											
						int32 dataType = strDataType.GetAsNumber();

						PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						PMString strChildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
						int32 header = strHeader.GetAsNumber();	
						double childId = strChildId.GetAsDouble();
						int32 childTag = strChildTag.GetAsNumber();
						

						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 		double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();
						double typeID	= strTypeID.GetAsDouble();
						int32 rowno	=	strRowNo.GetAsDouble();
						//added by Tushar on 26/12/06
						double parentTypeID	= strParentTypeID.GetAsDouble();
						
						/*CA("strElementID :" + strElementID);
						CA("strLanguageID :" + strLanguageID);
						CA("strParentTypeID :" + strParentTypeID);*/

						

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						//We will come across three types of tags.
						//1. ProductCopyAttributes tableFlag == 0
						//for all other attributes tableFlag == 1
						//2. TableHeader..typeId == -2
						//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
						PMString dispName("");
						if(tableFlag == "-15")
						{//This is a impotent tag.Don't process this.	
							//CA("tableFlag == -15");
							continue;
						}

						else if(tableFlag == "0")
						{	//CA("tableFlag == 0");
							//The cell contains only copy attributes
							//added on 25Sept..EventPrice addition.

							//ended on 25Sept..EventPrice addtion.
							if(strIndex == "3")
							{//Product copy attribute
								dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionID, kFalse);
							}
							else if(strIndex == "4" && header == 1/*typeID == -2*/)
							{//	CA("strIndex == 4 && typeID == -2");
								//Item copy attribue header
								//following code added by Tushar on 27/12/06
								if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
								{
									//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
									//if(TypeInfoVectorPtr != NULL)
									//{//CA("TypeInfoVectorPtr != NULL");
									//	VectorTypeInfoValue::iterator it3;
									//	int32 Type_id = -1;
									//	PMString temp = "";
									//	PMString name = "";
									//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									//	{
									//		Type_id = it3->getTypeId();
									//		if(parentTypeID == Type_id)
									//		{
									//			temp = it3->getName();
									//			if(elementID == -701)
									//			{
									//				dispName = temp;
									//			}
									//			else if(elementID == -702)
									//			{
									//				dispName = temp + " Suffix";
									//			}
									//		}
									//	}
									//}
									//if(TypeInfoVectorPtr)
									//	delete TypeInfoVectorPtr;
								}
								else if(elementID == -703)
								{
									//CA("dispName = $Off");
									dispName = "$Off";
								}
								else if(elementID == -704)
								{
									//CA("dispName = %Off");
									dispName = "%Off";
								}		
										
								//upto here added by Tushar on 27/12/06

								//added on 25Sept..EventPrice addition.
								//if(elementID == -701)
								//{
								//	dispName.SetCString("Event Price");
								//	//result = kTrue;								
								//}
								//else if(elementID == -702)
								//{
								//	dispName.SetCString("Event Suffix");
								//	//result = kTrue;								
								//}
								//else if(elementID == -703 )
								//{
								//	dispName.SetCString("$ Off");
								//	//result = kTrue;								
								//}
								//else if(elementID == -704)
								//{
								//	dispName.SetCString("% Off");
								//	//result = kTrue;					
								//}
								//ended on 25Sept..EventPrice addition.
								else
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
							}
							else if(strIndex == "4")
							{//CA("strIndex == 4");
								//Item copy attribute 
								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
								{//CA("common functions 1219");
									PublicationNode pNode;
									pNode.setPubId(productID);

									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
									pNode.setPBObjectID(strPBObjectID.GetAsDouble());
									if(indexValueAttachedToTable == "4")
									{
										
										pNode.setIsProduct(0);
									}
									else if(indexValueAttachedToTable == "3")
									{
										pNode.setIsProduct(1);
									}
									if(rowno != -1)
									{
										//CA("sETTING IsONEsource flag as false");
										pNode.setIsONEsource(kFalse);
									}
									TagStruct tagInfo;
									tagInfo.elementId = elementID;
									tagInfo.languageID = languageID;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = sectionID;
									tagInfo.tagPtr = cellTextXMLElementPtr;
									//added by Tushar on 26/12/06
									tagInfo.header = header;
									tagInfo.childId = childId;
									tagInfo.childTag = childTag;
									tagInfo.parentTypeID = parentTypeID;
									
									handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
								}
								else if(cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField")) == WideString("1"))
								{
									//CA("tagInfo.isEventField");
									/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(typeID,elementID,sectionID);
									if(vecPtr != NULL)
									{
										if(vecPtr->size()> 0)
											dispName = vecPtr->at(0).getObjectValue();	
										delete vecPtr;
									}*/
									//CA("itemAttributeValue = " + itemAttributeValue);
								}
								else
									dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeID*/childId,elementID,languageID, sectionID, /*kTrue*/kFalse);//23OCT09//Amit
							}
							
						}
						else if(tableFlag == "1")
						{//the cell contains the table
							if(strIndex == "3")
							{
								if(strElementID == "-103")
								{//Print the table header..table type
									do
									{
										//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
	//CA(strTypeID);					//int32 typeId = strTypeID.GetAsNumber();
										
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}

										VectorTypeInfoValue::iterator it1;

										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse); 
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									tagInfo.parentId = strParentID.GetAsDouble();
									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);

								}

							}
							else if(strIndex == "4")
							{
								if(strElementID == "-103")
								{
									do
									{
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}
										VectorTypeInfoValue::iterator it1;
										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse);
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									//This is just a temporary adjustment
									PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"/*"pbObjectId"*//*"rowno"*/));//Cs4
									tagInfo.parentId = pbObjectId.GetAsDouble();

									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);

								}

							}
						}
						else if(tableFlag == "-13")
						{//the cell contains the product copy attribute header
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
								if(result)
								dispName = cElementModelObj.getDisplayName();
						}

						/*
						if(tableFlag == "0" && strIndex == "3")//Non Header ProductCopyAttributes
						{							
							dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID,kFalse);
						}
						else if(tableFlag == "1")
						{
							if(typeID == -2)//Item_Table Header
								{
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
								}
							//Itemstable Header inside table cell..
							else if(strElementID == "-103")
								{
									do
											{
												PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
	//CA(strTypeID);
												int32 typeId = strTypeID.GetAsNumber();
												VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
												if(typeValObj==nil)
													break;

												VectorTypeInfoValue::iterator it1;

												bool16 typeIDFound = kFalse;
												for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
												{	
													if(typeId == it1->getTypeId())
														{
															typeIDFound = kTrue;
															break;
														}			
												}
												if(typeIDFound)
													dispName = it1->getName();
											}while(kFalse); 								
											
								}
							//ItemsTable data in tabbed text format inside table cell
							else if(strElementID == "-104")
								{
									
									PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									tagInfo.parentId = strParentID.GetAsNumber();
									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsNumber();
									GetItemTableInTabbedTextForm(tagInfo,dispName);
								}
							else //ItemCopyAttributes
								{
									dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(typeID,elementID, languageID, kFalse );									
								}
						}
						else if(tableFlag == "-13")//Header Product_copy_attributes
						{
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
		//CA_NUM("result ",result);
								if(result)
								dispName = cElementModelObj.getDisplayName();
						}
						*/
						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=dispName;					
						}
						else
							textToInsert=iConverter->translateString(dispName);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
						//WideString insertData(textToInsert);
//CA_NUM("tagStartPos ",tagStartPos);
//CA_NUM("tagEndPos ",tagEndPos);
//	CS3 Change			textModel->Replace(kTrue,tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);
					}
				}//end for tagIndex = 0 				
	   }//end for..tableIndex		
	   errcode = kSuccess;
	}while(0);
	return errcode;
}
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
void GetItemTableInTabbedTextForm(TagStruct & tagInfo,PMString & tabbedTextData)
{
	//CA("GetItemTableInTabbedTextForm");
	PMString elementName,colName;
	vector<double> FinalItemIds;
	do{
		vector<double>  vectorOfTableHeader;// = oTableValue.getTableHeader();
		vector<vector<PMString> >  vectorOfRowByRowTableData;// = oTableValue.getTableData();
		vector<double>  vec_items;// = oTableValue.getItemIds();
		bool8 isTranspose = kFalse;// = oTableValue.getTranspose();
		vector<vector<SlugStruct> > RowList;
		vector<SlugStruct> listOfNewTagsAdded;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}

		if(tagInfo.whichTab == 3 && tagInfo.isTablePresent == kTrue)
		{//start if1
//CA("inside if");
			double sectionid = tagInfo.sectionID;
			
			//PublicationNode pNode;	
			VectorScreenTableInfoPtr tableInfo=NULL;
			if(ptrIAppFramework->get_isONEsourceMode())
			{
				// For ONEsource mode
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(tagInfo.parentId);
			}else
			{
				//For publication
				tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, tagInfo.parentId, tagInfo.languageID);
			}
			if(!tableInfo)
			{
				//isTableSizeZero = kTrue;
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::GetItemTableInTabbedTextForm::tableinfo is nil");
				break;
			}

			if(tableInfo->size()==0)
			{ 
				//isTableSizeZero = kTrue;
				break;
			}
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			bool16 typeidFound=kFalse;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{
					oTableValue = *it;
					if(oTableValue.getTableTypeID() == tagInfo.typeId)
					{   				
						typeidFound=kTrue;				
						break;
					}
				}
				if(!typeidFound)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::GetItemTableInTabbedTextForm::!typeidFound");				
					break;
				}

				vectorOfTableHeader = oTableValue.getTableHeader();
				vectorOfRowByRowTableData = oTableValue.getTableData();
				vec_items = oTableValue.getItemIds();
				isTranspose = oTableValue.getTranspose();
				
				if(tableInfo)
					delete tableInfo;
				
		}
		else if(tagInfo.whichTab == 4 && tagInfo.isTablePresent == kTrue)
		{
			double sectionid = tagInfo.sectionID;
			
			//ptrIAppFramework->clearAllStaticObjects();
			VectorScreenTableInfoPtr tableInfo=
				ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tagInfo.parentId, sectionid, tagInfo.languageID);
			if(!tableInfo)
			{
				//isTableSizeZero = kTrue;
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::GetItemTableInTabbedTextForm::tableinfo is nil");
				break;
			}

			if(tableInfo->size()==0)
			{ 
				//isTableSizeZero = kTrue;
				break;
			}
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			bool16 typeidFound=kFalse;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{
					oTableValue = *it;
					if(oTableValue.getTableTypeID() == tagInfo.typeId)
					{   				
						typeidFound=kTrue;				
						break;
					}
				}
				if(!typeidFound)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::GetItemTableInTabbedTextForm::!typeidFound");				
					break;
				}

				vectorOfTableHeader = oTableValue.getTableHeader();
				vectorOfRowByRowTableData = oTableValue.getTableData();
				vec_items = oTableValue.getItemIds();
				isTranspose = oTableValue.getTranspose();
				if(tableInfo)
					delete tableInfo;

		}
			

			int32/*int64*/ noOfColumns =static_cast<int32> (vectorOfTableHeader.size());
			int32/*int64*/ noOfRows =static_cast<int32> (vec_items.size());			
			PMString rawTabbedTextData;
			rawTabbedTextData.Clear();			
			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				//Note typeId == -2 for TableHeaders.
					
				//We don't require headers.
				/*
					for(int32 headerIndex = 0;headerIndex < noOfColumns;headerIndex++)
					{
						int32 elementId = vectorOfTableHeader[headerIndex];						 
						PMString elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
										
						tabbedTextData.Append(elementName);
						if(headerIndex != noOfColumns - 1)
						{
							tabbedTextData.Append("\t");						
						}
						else
						{
							tabbedTextData.Append("\n");
						}

									
					}
				*/
					
				//This for loop will spray the Item_copy_attributes value
				vector<vector<PMString> > ::iterator rowIterator;
				for(rowIterator = vectorOfRowByRowTableData.begin();rowIterator != vectorOfRowByRowTableData.end()-1;rowIterator++)
					{
						
						vector<PMString> :: iterator colIterator = rowIterator->begin();
						colIterator++;
						for(;colIterator!=  rowIterator->end()-1;colIterator++)
						{
							
							rawTabbedTextData.Append(*colIterator);							
							rawTabbedTextData.Append("\t");
							

						}
						rawTabbedTextData.Append(*colIterator);	
						rawTabbedTextData.Append("\r");

					}
					// optimization code..
					vector<PMString> :: iterator colIterator = rowIterator->begin();
					colIterator++;
					for(;colIterator!=  rowIterator->end()-1;colIterator++)
					{
						rawTabbedTextData.Append(*colIterator);
						rawTabbedTextData.Append("\t");
						

					}
					rawTabbedTextData.Append(*colIterator);
					
					

					PMString textToInsert("");
					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						tabbedTextData = rawTabbedTextData;
					}
					else
						tabbedTextData=iConverter->translateString(rawTabbedTextData);
			}//end of if IsTranspose == kFalse
			else
			{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
				/*  
				In case of Transpose == kTrue..the table gets sprayed like 

					Header11 Data11 Data12 Data13..
					Header22 Data21 Data22 Data23..
					Header33 Data31 Data32 Data33..
					....
					....
				So First Column is HeaderColumn and subsequent columns are Data columns.
				*/
				
				vector<PMString> * transposeData = new vector<PMString>[noOfColumns];

				vector<vector<PMString> > ::iterator rowIterator;
				for(rowIterator = vectorOfRowByRowTableData.begin();rowIterator != vectorOfRowByRowTableData.end();rowIterator++)
					{
						vector<PMString> :: iterator colIterator = rowIterator->begin();
						colIterator++;
						for(int32 rowIndex = 0;colIterator!=  rowIterator->end();colIterator++,rowIndex++)
						{
							transposeData[rowIndex].push_back(*colIterator);
							//rawTabbedTextData.Append(*colIterator);
							//rawTabbedTextData.Append("\t");
							

						}
						//rawTabbedTextData.Append("\n");

					}

				for(int32 index = 0;index < noOfColumns-1;index++)
				{
					vector<PMString> ::iterator dataIterator = transposeData[index].begin();
					while(dataIterator != transposeData[index].end()-1)
					{
						rawTabbedTextData.Append(*dataIterator);
						rawTabbedTextData.Append("\t");
						dataIterator++;
					}
					rawTabbedTextData.Append(*dataIterator);
					rawTabbedTextData.Append("\r");
				}
				vector<PMString> ::iterator dataIterator = transposeData[noOfColumns-1].begin();
				while(dataIterator != transposeData[noOfColumns-1].end()-1)
				{
					rawTabbedTextData.Append(*dataIterator);
					rawTabbedTextData.Append("\t");
					dataIterator++;
				}
				rawTabbedTextData.Append(*dataIterator);

				PMString textToInsert("");
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(!iConverter)
				{
					tabbedTextData = rawTabbedTextData;
				}
				else
					tabbedTextData=iConverter->translateString(rawTabbedTextData);
				
				if(transposeData)	//--------
					delete[] transposeData;
		}		
			
//CA(tabbedTextData);
	}while(kFalse);
	
	
//			}//end for1
}//end of function
//end added on 11July...the serial blast day

void getAllXMLTagAttributeValues(XMLReference & xmlTagReference,XMLTagAttributeValue & tagAttrVal)
	{
		//IIDXMLElement * tagXMLElementPtr = xmlTagReference.Instantiate();
		InterfacePtr<IIDXMLElement>tagXMLElementPtr(xmlTagReference.Instantiate());

		tagAttrVal.ID = tagXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
		tagAttrVal.typeId = tagXMLElementPtr->GetAttributeValue(WideString("typeId"));		
		tagAttrVal. index = tagXMLElementPtr->GetAttributeValue(WideString("index"));
		tagAttrVal. imgFlag = tagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
		tagAttrVal.parentID = tagXMLElementPtr->GetAttributeValue(WideString("parentID"));
		tagAttrVal. parentTypeID = tagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
		tagAttrVal. sectionID = tagXMLElementPtr->GetAttributeValue(WideString("sectionID"));
		tagAttrVal. tableFlag = tagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
		tagAttrVal. LanguageID = tagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
		tagAttrVal. isAutoResize = tagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));
		tagAttrVal. rowno = tagXMLElementPtr->GetAttributeValue(WideString("rowno"));
		tagAttrVal. colno = tagXMLElementPtr->GetAttributeValue(WideString("colno"));

		tagAttrVal. header = tagXMLElementPtr->GetAttributeValue(WideString("header"));
		tagAttrVal. isEventField = tagXMLElementPtr->GetAttributeValue(WideString("isEventField"));
		tagAttrVal. deleteIfEmpty = tagXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty"));
		tagAttrVal. dataType = tagXMLElementPtr->GetAttributeValue(WideString("dataType"));
		tagAttrVal. pbObjectId = tagXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
		tagAttrVal. childId = tagXMLElementPtr->GetAttributeValue(WideString("childId"));
		tagAttrVal. isSprayItemPerFrame = tagXMLElementPtr->GetAttributeValue(WideString("isSprayItemPerFrame"));
		tagAttrVal. catLevel = tagXMLElementPtr->GetAttributeValue(WideString("catLevel"));
		tagAttrVal. imageIndex = tagXMLElementPtr->GetAttributeValue(WideString("imageIndex"));
		tagAttrVal. flowDir = tagXMLElementPtr->GetAttributeValue(WideString("flowDir"));
		tagAttrVal. childTag = tagXMLElementPtr->GetAttributeValue(WideString("childTag"));
		tagAttrVal. tableType = tagXMLElementPtr->GetAttributeValue(WideString("tableType"));
		tagAttrVal. tableId = tagXMLElementPtr->GetAttributeValue(WideString("tableId"));
		tagAttrVal. field1 = tagXMLElementPtr->GetAttributeValue(WideString("field1"));
		tagAttrVal. field2 = tagXMLElementPtr->GetAttributeValue(WideString("field2"));
		tagAttrVal. field3 = tagXMLElementPtr->GetAttributeValue(WideString("field3"));
		tagAttrVal. field4 = tagXMLElementPtr->GetAttributeValue(WideString("field4"));
		tagAttrVal. field5 = tagXMLElementPtr->GetAttributeValue(WideString("field5"));
        try{
            tagAttrVal. groupKey = tagXMLElementPtr->GetAttributeValue(WideString("groupKey"));
        }catch (...){
            
        }
	}

void setAllXMLTagAttributeValues(XMLReference & xmlTagReference ,const XMLTagAttributeValue & tagAttrVal)
	{
		
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("ID"),WideString(tagAttrVal.ID)); //Cs4
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("typeId"),WideString(tagAttrVal.typeId));		
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("index"),WideString(tagAttrVal.index));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentID"),WideString(tagAttrVal.parentID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("sectionID"),WideString(tagAttrVal.sectionID));
	
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("rowno"),WideString(tagAttrVal.rowno));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("colno"),WideString(tagAttrVal.colno));

		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("header"),WideString(tagAttrVal.header));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isEventField"),WideString(tagAttrVal.isEventField));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("dataType"),WideString(tagAttrVal.dataType));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("childId"),WideString(tagAttrVal.childId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("catLevel"),WideString(tagAttrVal.catLevel));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("flowDir"),WideString(tagAttrVal.flowDir));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("childTag"),WideString(tagAttrVal.childTag));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("tableType"),WideString(tagAttrVal.tableType));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("tableId"),WideString(tagAttrVal.tableId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field1"),WideString(tagAttrVal.field1));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field2"),WideString(tagAttrVal.field2));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field3"),WideString(tagAttrVal.field3));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field4"),WideString(tagAttrVal.field4));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field5"),WideString(tagAttrVal.field5));
        try{
            Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("groupKey"),WideString(tagAttrVal.groupKey));
        }catch (...){
            
        }
	}

void createAllXMLTagAttributes(XMLReference & xmlRef,const XMLTagAttributeValue & tagAttrVal)
	{
		
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("ID"),WideString(tagAttrVal.ID)); //Cs4
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("typeId"),WideString(tagAttrVal.typeId));	
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("header"),WideString(tagAttrVal.header));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isEventField"),WideString(tagAttrVal.isEventField));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("dataType"),WideString(tagAttrVal.dataType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("index"),WideString(tagAttrVal.index));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentID"),WideString(tagAttrVal.parentID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childId"),WideString(tagAttrVal.childId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("sectionID"),WideString(tagAttrVal.sectionID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("catLevel"),WideString(tagAttrVal.catLevel));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("flowDir"),WideString(tagAttrVal.flowDir));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childTag"),WideString(tagAttrVal.childTag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableFlag"),WideString(tagAttrVal.tableFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableType"),WideString(tagAttrVal.tableType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableId"),WideString(tagAttrVal.tableId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("rowno"),WideString(tagAttrVal.rowno));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("colno"),WideString(tagAttrVal.colno));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field1"),WideString(tagAttrVal.field1));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field2"),WideString(tagAttrVal.field2));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field3"),WideString(tagAttrVal.field3));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field4"),WideString(tagAttrVal.field4));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field5"),WideString(tagAttrVal.field5));
        Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("groupKey"),WideString(tagAttrVal.groupKey));

	}

void AddTagToCellText
				(
					const GridAddress & cellAddr,
					const InterfacePtr<ITableModel> & tableModel,
                    PMString & dataToBeSprayed,
					InterfacePtr<ITextModel> & textModel,
					const XMLTagAttributeValue xmlTagAttrVal
				)
{
	do
	{
			InterfacePtr<ICellContent>cellContent(tableModel->QueryCellContentBoss(cellAddr)/*,UseDefaultIID()*/);
			if(cellContent == nil)
			{
				//CA("cellContent == nil");
				break;
			}
			
			InterfacePtr<IXMLReferenceData> cellXMLRefData(cellContent,UseDefaultIID());
			if(cellXMLRefData == nil)
			{
				//CA("cellXMLRefData == nil");
				break;
			}
			XMLReference cellXMLReference = cellXMLRefData->GetReference();
			//IIDXMLElement * cellXMLElementPtr = cellXMLReference.Instantiate();
			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLReference.Instantiate());
			if(cellXMLElementPtr == nil)
			{
				//CA("cellXMLElementPtr == nil");
				break;
			}
			//get the text tag in the cell
			int32 cellChildCount = cellXMLElementPtr->GetChildCount();
			if(cellChildCount == 0)
			{
				InterfacePtr<ITableCommands> tableCommand(tableModel,UseDefaultIID());
				if(tableCommand == nil)
				{
					//CA("tableCommand == nil");
					break;
				}

				GridSpan span(0,0);
				GridArea cellGridArea(cellAddr,span);

				//try 9 Aug
				/*
				//This approach is not working when the cells are blank
				InterfacePtr<ITextParcelList> textParcelList(cellContent,UseDefaultIID());
				if(textParcelList == nil)
				{
					CA("textParcelList == nil");
					break;
				}
					int32 textStart = textParcelList->GetThreadStart();
					int32 textEnd = textParcelList->GetThreadSpan();
		CA_NUM("textStart = ",textStart);
		CA_NUM("textEnd = ",textEnd);
				*/
				InterfacePtr<ITextStoryThreadDict> textStoryThreadDict(tableModel,UseDefaultIID());
				if(textStoryThreadDict == nil)
				{
					//CA("textStoryThreadDict == nil");
					break;
				}
				GridID cellGridId = tableModel->GetGridID(cellAddr);
				InterfacePtr<ITextStoryThread> cellTextThread(textStoryThreadDict->QueryThread(cellGridId));
				//ITextStoryThread * cellTextThread = textStoryThreadDict->QueryThread(cellGridId);
				if(cellTextThread == nil)
				{
					//CA("cellTextThread == nil");
					break;
				}
				int32 textStart = cellTextThread->GetTextStart();
				int32 textEnd = cellTextThread->GetTextSpan();
//CA_NUM("textStart = ",textStart);
//CA_NUM("textEnd = ",textEnd);

			//try end
				//tableCommand->ClearContent(cellGridArea);
				//return;
				
				//CA("cellChildCount == 0");
				//UIDRef tableModelUIDRef(::GetUIDRef(tableModel));
				//GridID cellGridID = tableModel->GetGridID(cellAddr);				
				//
				XMLReference newXMLTagRef;
				//ErrorCode err = Utils<IXMLElementCommands>()->CreateElement(tagName,tableModelUIDRef,cellGridID,&newXMLTagRef);
				int32 startPos = -1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(cellXMLElementPtr,&startPos,&endPos);
				endPos = startPos + dataToBeSprayed.NumUTF16TextChars()-1;
				//WideString insertText(dataToBeSprayed);	
				///////textModel->Insert(kTrue,startPos,&insertText);
				//if(textEnd > 1)
//  Cs3 Change		textModel->Replace(kTrue,textStart,textEnd-1,&insertText);
					
				//textModel->Replace(textStart,textEnd-1,&insertText);
                dataToBeSprayed.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(dataToBeSprayed));
				ReplaceText(textModel,textStart,textEnd-1 ,insertText);
				
				//else
					//textModel->Insert(kTrue,textStart,&insertText);
				//startPos = startPos + 1;
				//endPos = endPos -1;
				UIDRef tableTextModelUIDRef = ::GetUIDRef(textModel);
				ErrorCode err = Utils<IXMLElementCommands>()->CreateElement(WideString(xmlTagAttrVal.tagName),tableTextModelUIDRef,startPos,endPos+1,cellXMLReference,&newXMLTagRef);
				//ErrorCode err = Utils<IXMLElementCommands>()->CreateElement(tagName,tableTextModelUIDRef,cellGridID,&newXMLTagRef);
				if(err == kFailure)
				{
					//CA("err == kFailure");
					break;
				}
				createAllXMLTagAttributes(newXMLTagRef,xmlTagAttrVal);				
				break;
			}
			else
			{
				XMLReference cellChildXMLRef = cellXMLElementPtr->GetNthChild(0);
				//IIDXMLElement * cellChildXMLElementPtr = cellChildXMLRef.Instantiate();
				InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLRef.Instantiate());
				if(cellChildXMLElementPtr == nil)
				{
					//CA("cellChildXMLElementPtr == nil");
					break;
				}
				int32 startPos = -1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(cellChildXMLElementPtr,&startPos,&endPos);
				startPos = startPos + 1;
				endPos = endPos -1;			
				Utils<IXMLElementCommands>()->SetElement(cellChildXMLRef,cellChildXMLElementPtr->GetTagUID());  //Cs4	
				//Replace the cellText value between the tag.
				//WideString insertText(dataToBeSprayed);	
//	CS3 Change	textModel->Replace(kTrue,startPos,endPos-startPos + 1,&insertText);	
				//textModel->Replace(startPos,endPos-startPos + 1,&insertText);
                dataToBeSprayed.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(dataToBeSprayed));
				//ReplaceText(startPos,endPos-startPos + 1 ,insertText);
				ReplaceText(textModel,startPos,endPos-startPos + 1 ,insertText);
				//change the TagName  GetTagUID()
				//Utils<IXMLElementCommands>()->SetElement(cellChildXMLRef,xmlTagAttrVal.tagName);//Cs3			
						
				//change the TagAttributes.		
				setAllXMLTagAttributeValues(cellChildXMLRef,xmlTagAttrVal);
			}
		}while(kFalse);
}


bool8 getAttributeIDFromNotesNew(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes)
	{
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		//if(ptrIAppFramework == nil)
		//	return kFalse;

		//int32 eventPriceTableTypeID = ptrIAppFramework->getPubItemPriceTableType();
		//
		////CA_NUM("EventPriceTableTypeID : ",eventPriceTableTypeID);
		//attributeIDfromNotes = "-1";
		bool8 isNotesValid = kFalse;
		//int32 isProduct = pNode.getIsProduct();
		//
		//do
		//{
		//	if(isProduct == 1)
		//	{
		//		VectorScreenTableInfoPtr tableInfo=NULL;
		//		if(pNode.getIsONEsource())
		//	    {
		//			// For ONEsource mode
		//			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		//		}else
		//		{
		//			//For publication 
		//			int32 langId = 91;
		//			tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), langId );
		//		}
		//		if(!tableInfo)
		//		{
		//			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::GetAttributeIDFromNotesNew::!tableInfo");				
		//			break;
		//		}
		//		if(tableInfo->size()==0)
		//		{ 
		//			break;
		//		}
		//		bool8 isEventPriceTableIDPresent =kFalse;
		//		CItemTableValue oTableValue;
		//		VectorScreenTableInfoValue::iterator it;
		//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		//		{	
		//			if(it->getTableTypeID() == eventPriceTableTypeID)
		//			{
		//				oTableValue = *it;
		//				isEventPriceTableIDPresent = kTrue;
		//				break;
		//			}
		//			
		//		}
		//		if(isEventPriceTableIDPresent == kFalse)
		//		{					
		//			break;
		//		}

		//		vector <int32> vec_ItemIDs = oTableValue.getItemIds();
		//		vector <PMString> vec_notes =  oTableValue.getNotesList();
		//		vector <int32> :: iterator itemIterator;
		//		vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
		//		for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
		//		{
		//			if(selectedItemID == *itemIterator)
		//			{
		//				attributeIDfromNotes = *notesIterator;
		//				isNotesValid = kTrue;
		//				break;
		//			}
		//		}
		//		if(tableInfo)
		//			delete tableInfo;
		//	}
		//	if(isProduct == 0)
		//	{
		//		//CA_NUM("pNode.getPBObjectID() : ",pNode.getPBObjectID());
		//		//ptrIAppFramework->clearAllStaticObjects();
		//	int32 langId = 91; // Apsiva hardcoded
		//		VectorScreenTableInfoPtr tableInfo=
		//				ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, langId);
		//		if(!tableInfo)
		//		{
		//			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::GetAttributeIDFromNotesNew::!tableInfo");								
		//			break;
		//		}
		//		
		//		if(tableInfo->size()==0)
		//		{ 
		//			break;
		//		}
		//		bool8 isEventPriceTableIDPresent =kFalse;
		//		CItemTableValue oTableValue;
		//		VectorScreenTableInfoValue::iterator it;
		//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		//		{	
		//			if(it->getTableTypeID() == eventPriceTableTypeID)
		//			{
		//				oTableValue = *it;
		//				isEventPriceTableIDPresent = kTrue;
		//				break;
		//			}
		//			
		//		}
		//		if(isEventPriceTableIDPresent == kFalse)
		//		{
		//			break;
		//		}

		//		vector <int32> vec_ItemIDs = oTableValue.getItemIds();
		//		//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
		//		vector <PMString> vec_notes =  oTableValue.getNotesList();
		//		//CA_NUM("vec_notes.size() : ",vec_notes.size());
		//		vector <int32> :: iterator itemIterator;
		//		vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
		//		for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
		//		{
		//			//CA_NUM("*itemIterator : ",*itemIterator);
		//			//CA("*notesIterator : "+*notesIterator);
		//			if(selectedItemID == *itemIterator)
		//			{
		//				attributeIDfromNotes = *notesIterator;
		//				isNotesValid = kTrue;
		//				break;
		//			}
		//		}
		//		if(tableInfo)
		//			delete tableInfo;
		//	}
		//}while(kFalse);

		return isNotesValid;
	}

	//void handleSprayingOfEventPriceRelatedAdditions(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttributeValue)
	//{//added on 22Sept..EventPrice addition.
	//	//CA("handleSprayingOfEventPriceRelatedAdditions");
	//	int32 eventTabletypeId = tagInfo.parentTypeID;//added on 26/12/06 by Tushar
	//	int32 SalePriceTabletypeId = -1;
	//	int32 RegularPriceTabletypeId = -1;
	//	//CA_NUM("tagInfo.parentTypeID() :",tagInfo.parentTypeID);//added on 26/12/06 by Tushar

	//	int32 elementId = tagInfo.elementId;
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//		return ;
	//	int32 sectionid =tagInfo.sectionID;
	//	//int32 pbObjectID = -1;
	//	//if(pNode.getIsProduct() == kTrue)
	//	//{
	//	//	if(global_project_level == 3)
	//	//		sectionid = CurrentSubSectionID;
	//	//	if(global_project_level == 2)
	//	//		sectionid = CurrentSectionID;
	//		
	//	//}
	//	//CA_NUM("tagInfo.typeId() :",tagInfo.typeId);
	//	PMString attributeIDfromNotes("-1");
	//	PMString attributeIDfromNotesSales("-1");
	//	PMString attributeIDfromNotesRegular("-1");
	//	//bool8 isValidNotes =  getAttributeIDFromNotesNew(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes);//commented on 26/12/06 by Tushar
	//	if(elementId == -703 || elementId == -704)
	//	{
	//		SalePriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemSalePriceTableType();
	//		RegularPriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemRegularPriceTableType();
	//		//CA_NUM("SalePriceTabletypeId :",SalePriceTabletypeId);
	//		//CA_NUM("RegularPriceTabletypeId :",RegularPriceTabletypeId);
	//		
	//		bool8 isValidNotes =  getAttributeIDFromNotes(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesSales,attributeIDfromNotesRegular,SalePriceTabletypeId,RegularPriceTabletypeId);
	//		//bool8 isValidNotes1 =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesRegular,RegularPriceTabletypeId);
	//	}
	//	else 
	//	bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,eventTabletypeId);
	//	//CA("attributeIDfromNotes = " + attributeIDfromNotes);
	//	//CA_NUM("tagInfo.typeId() :",tagInfo.typeId);
	//	
	//	if(elementId == -701)
	//	{
	//		//tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);//commented on 29/12/06 by Tushar
	//		//tagInfo.elementId =attributeIDfromNotes.GetAsNumber();//commented on 29/12/06 by Tushar
	//		//CA_NUM("tagInfo.elementID :",tagInfo.elementId);
	//		//CA_NUM("tagInfo.langaugeID :",tagInfo.languageID);
	//		itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID, kTrue );
	//		//CA(itemAttributeValue);
	//	}
	//	else if(elementId == -702)
	//	{
	//		itemAttributeValue = ptrIAppFramework->getPriceSuffix(attributeIDfromNotes.GetAsNumber());
	//	}
	//	else if(elementId == -703 || elementId == -704)
	//	{
	//			if(attributeIDfromNotesSales == "" && attributeIDfromNotesRegular == "")
	//			{
	//				itemAttributeValue = "";
	//				return;
	//			}
	//			else if(attributeIDfromNotesSales == "" || attributeIDfromNotesSales == "")
	//			{
	//				itemAttributeValue = "";
	//				return;
	//			}	
	//			double eventPrice = 0;
	//			double regularPrice = 0;

	//			//int32 eventPriceID = attributeIDfromNotes.GetAsNumber();
	//			
	//			do
	//			{
	//				PMString eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesSales.GetAsNumber(), tagInfo.languageID, kTrue );
	//				//PMString eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,eventPriceID, tagInfo.languageID, kTrue );
	//				if(eventPriceValue == "")
	//				{
	//					itemAttributeValue = "";
	//					return;
	//				}
	//				//CA("eventPriceValue "+ eventPriceValue);
	//				//start 4Oct..Mac price issue solution..
	//				//Remove the '$' character which appears either at first or last location
	//				//in a string.
	//				if((*eventPriceValue./*SubWString*/Substring(0,1)) == "$") //Cs4
	//				{//if the "$" is at start of the string.
	//					
	//					eventPriceValue = (*eventPriceValue./*SubWString*/Substring(1));//Cs4
	//				}

	//			//	if((*eventPriceValue.SubWString(eventPriceValue.NumUTF16TextChars()-1,1)) == "$")
	//			//	{//if the "$" is at last of the string.
	//			//		eventPriceValue = (*eventPriceValue.SubWString(0,eventPriceValue.NumUTF16TextChars()-1));
	//			//	}
	//				//end 4Oct..Mac price issue solution..

	//				PMString ::ConversionError * pErr = nil;
	//				eventPrice = eventPriceValue.GetAsDouble(pErr,nil);
	//				if(( pErr != nil) &&(*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
	//				{
	//					itemAttributeValue = "";
	//					return;
	//				}
	//				//CA_NUM("eventPrice:",eventPrice);		
	//				
	//			}while(kFalse);
	//			
	//			
	//			do
	//			{
	//				//int32 regularPriceID = ptrIAppFramework->getRegularPriceAttributeId();//commented on 26/12/06 by Tushar
	//				//int32 regularPriceID = ptrIAppFramework->getEventPriceAttributeId(eventTabletypeId);//added by Tushar on 26/12/06
	//				//if(regularPriceID == -1 )
	//				//{
	//				//	//CA("regularPriceID == -1");
	//				//	itemAttributeValue = "";
	//				//	return;
	//				//}
	//				PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesRegular.GetAsNumber(), tagInfo.languageID, kTrue );
	//				//PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,regularPriceID, tagInfo.languageID, kTrue );
	//				if(regularPriceValue == "")
	//				{
	//					itemAttributeValue = "";
	//					return;
	//				}

	//				//start 4Oct..Mac price issue solution..
	//				//Remove the '$' character which appears either at first or last location
	//				//in a string.
	//				if((*regularPriceValue./*SubWString*/Substring(0,1)) == "$") //Cs4
	//				{
	//					
	//					regularPriceValue = (*regularPriceValue./*SubWString*/Substring(1)); //Cs4
	//				}

	//			//	if((*regularPriceValue.SubWString(regularPriceValue.NumUTF16TextChars()-1,1)) == "$")
	//			//	{
	//			//		regularPriceValue = (*regularPriceValue.SubWString(0,regularPriceValue.NumUTF16TextChars()-1));
	//			//	}
	//				//end 4Oct..Mac price issue solution..
	//
	//				PMString ::ConversionError * pErr = nil;
	//				regularPrice = regularPriceValue.GetAsDouble(pErr,nil);
	//				if(( pErr != nil) && (*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
	//				{
	//					itemAttributeValue = "";
	//					return;
	//				}
	//				
	//			}while(kFalse);

	//			//PMReal realDifference = Round(regularPrice - eventPrice);
	//			//int32 intDifference = ToInt32(realDifference);
	//			if(elementId == -703)
	//			{
	//				//PMReal temp;
	//				//PMReal nearestDollar = ::Ceiling(realDifference);
	//				//itemAttributeValue.AppendNumber(nearestDollar);
	//				//itemAttributeValue.AppendNumber(intDifference);
	//				//itemAttributeValue.AppendNumber(realDifference);

	//				int32 realDifference = ptrIAppFramework->getDollerDifference(regularPrice ,eventPrice);
	//				itemAttributeValue.AppendNumber(realDifference);

	//			}
	//			else if(elementId == -704)
	//			{
	//				itemAttributeValue = "";
	//				if(regularPrice != 0)
	//				{
	//					//int32 actualPercentage = (int32)((realDifference/regularPrice) * 100);
	//					//int32 percentageDiff = ToInt32(actualPercentage);
	//					//itemAttributeValue.AppendNumber(percentageDiff);
	//					int32 percentageDiff = ptrIAppFramework->getPercentageDifference(regularPrice ,eventPrice);
	//					itemAttributeValue.AppendNumber(percentageDiff);	

	//				}
	//			}
	//	}
	//	//ended on 22Sept..EventPrice addition.

	////ended on 22Sept..EventPrice addition.
	//}


//Added By Dattatray on 17/11 to handle Refresh of Custom Table in Tab Text Form ..
void sprayCMedCustomTableScreenPrintInTabbedTextForm(TagStruct & tagInfo,InterfacePtr<ITextModel> & textModel)
{
//	UIDRef txtModelUIDRef = UIDRef::gNull;
//	//InterfacePtr<ITextModel> textModel;
//	PublicationNode pNode;
//	
//	int32 replaceItemWithTabbedTextFrom = -1;
//	int32 replaceItemWithTabbedTextTo = -1;
//	PMString tabbedTextData;
//	do{
//		//if(tagInfo.elementId == -1 && tagInfo.typeId == -111)
//		if(tagInfo.typeId == -111)
//		{//start if1
//			
//			txtModelUIDRef = ::GetUIDRef(textModel);
//			
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == nil)
//			{
//				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//				break;
//			}
//			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//
//			int32 sectionid = -1;
//			sectionid = tagInfo.sectionID;
//			
//			
//			PMString attrVal;
//			attrVal.AppendNumber(tagInfo.parentId);
//			tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal)); //cs4
//			attrVal.Clear();
//			//
//			
//			attrVal.AppendNumber(tagInfo.parentTypeID);
//			tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
//			attrVal.Clear();
//			//
//			attrVal.AppendNumber(sectionid);
//			tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal)); //Cs4
//			attrVal.Clear();
//			//
//			//tableFlag =-11 to identify that this is the "item" table tag in the 
//			//tabbed text format.
//			tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-11")); //Cs4
//			//change the ID from -1 to -101, as -1 is elementId for productNumber.
//			tagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-101"));
//
//			//end update the attributes of the parent tag
//			IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
//			int32 tagStartPos = 0;
//			int32 tagEndPos =0; 			
//			Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
//			tagStartPos = tagStartPos + 1;
//			tagEndPos = tagEndPos -1;
//
//			tabbedTextData.Clear();
//			replaceItemWithTabbedTextFrom = tagStartPos;
//			replaceItemWithTabbedTextTo	= tagEndPos;
//			
//			bool16 isTableDataPresent = kTrue;
//			
//			CMedCustomTableScreenValue* customtableInfoptr = ptrIAppFramework->GetONEsourceObjects_getMed_CustomTableScreenValue(tagInfo.parentId);
//			if(customtableInfoptr == NULL)
//			{	
//				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::sprayCMedCustomTableScreenPrintInTabbedTextForm::customtableInfoptr == NULL");
//				break;
//			}
//			CMedCustomTableScreenValue customtableInfo = *customtableInfoptr;
//
//				if(!customtableInfoptr)
//				{
//					isTableDataPresent = kFalse;
//					break;
//				}
//			int32/*int64*/ numRows=0;
//			int32/*int64*/ numCols=0;
//
//			numRows =static_cast<int32> (customtableInfo.Tabledata.size());  
//			if(numRows<=0)
//			{			
//				break;		
//			}
//			numCols = static_cast<int32>(customtableInfo.HeaderList.size());	
//			if(numCols<=0)
//			{
//				break;
//			}
//			
//			vector<vector<SlugStruct> > RowList;
//			vector<SlugStruct> listOfNewTagsAdded;
//
//			//Check for the tableHeaderFlag at colno
//			int32 colnoAsHeaderFlag = tagInfo.colno;
//			//update the attributes of the parent tag
//			bool8 isTranspose = kFalse;//Hardcoded as false for Medtronics
//			if(!isTranspose)
//			{
//				/*  
//				In case of Transpose == kFalse..the table gets sprayed like 
//					Header11 Header12  Header13 ...
//					Data11	 Data12	   Data13
//					Data21	 Data22	   Data23
//					....
//					....
//					So first row is Header Row and subsequent rows are DataRows.
//				*/
//				//The for loop below will spray the Table Header in left to right fashion.
//				//Note typeId == -2 for TableHeaders.
//				
//				if(colnoAsHeaderFlag != -555)//Spray table with the headers.
//				{
//					
//					for(int k=0; k<customtableInfo.HeaderList.size(); k++)
//					{						
//						SlugStruct newTagToBeAdded;
//						newTagToBeAdded.elementId = customtableInfo.HeaderList[k].getvalueAttributeId();;
//						newTagToBeAdded.typeId = -2;
//						newTagToBeAdded.parentId = tagInfo.parentId;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
//						
//						PMString TableColName("");
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//
//						newTagToBeAdded.elementName = customtableInfo.HeaderList[k].getdisplayString();
//						newTagToBeAdded.TagName = TableColName;
//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						newTagToBeAdded.whichTab =tagInfo.whichTab;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.sectionID = sectionid ;
//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						newTagToBeAdded.tableFlag = -12;
//						newTagToBeAdded.row_no=-1;
//						newTagToBeAdded.col_no=tagInfo.colno;				 
//
//						PMString insertPMStringText;
//						insertPMStringText.Append(newTagToBeAdded.elementName);
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//						if(k != numCols - 1)
//						{
//							insertPMStringText.Append("\t");						
//						}
//						else
//						{
//							insertPMStringText.Append("\r");
//						}
//
//						tabbedTextData.Append(insertPMStringText);
//						
//
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//							
//						 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
//						
//						 newTagToBeAdded.tagStartPos=tagStartPos;
//						 newTagToBeAdded.tagEndPos=tagEndPos;
//						 listOfNewTagsAdded.push_back(newTagToBeAdded);
//						
//						 tagStartPos = tagEndPos+ 1 + 2;
//			
//					}
//					
//				}
//				
//				//////////////This for loop will spray the Item_copy_attributes value
//				for(int32 count2 = 0;count2 <numRows;count2++)
//				{
//					
//					VectorCustomItemTableRowInfo rowVector = customtableInfo.Tabledata[count2];
//					int32/*int64*/ rowVectorSize =static_cast<int32> (rowVector.size());
//					
//					for(int32 count1 =0 ; count1 < rowVectorSize ;count1++)
//					{
//						PMString TableColName("");
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(rowVector[count1].getvalueAttributeId());
//										
//						SlugStruct newTagToBeAdded;
//						newTagToBeAdded.elementId = rowVector[count1].getvalueAttributeId();
//						newTagToBeAdded.typeId = rowVector[count1].getItemId();
//						newTagToBeAdded.parentId = tagInfo.parentId;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
//						newTagToBeAdded.elementName =rowVector[count1].getdisplayString()/*TableColName*/;
//						newTagToBeAdded.TagName = TableColName;
//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						newTagToBeAdded.whichTab =tagInfo.whichTab;
//						newTagToBeAdded.imgFlag = 0; 
//						newTagToBeAdded.sectionID = sectionid;
//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						newTagToBeAdded.tableFlag = -12;
//						newTagToBeAdded.row_no=-1;
//						newTagToBeAdded.col_no=colnoAsHeaderFlag;
//						 
//						PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
//						PMString insertPMStringText;
//						insertPMStringText.Append(newTagToBeAdded.elementName);
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//						if(count1 != numCols - 1)
//						{
//							insertPMStringText.Append("\t");
//						}
//						else if(count2 != numRows-1)
//						{
//							insertPMStringText.Append("\r");
//						}
//						tabbedTextData.Append(insertPMStringText);
//						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
//						newTagToBeAdded.tagStartPos=tagStartPos;
//						newTagToBeAdded.tagEndPos=tagEndPos;
//						listOfNewTagsAdded.push_back(newTagToBeAdded);
//						tagStartPos = tagEndPos+ 1 + 2;
//					}
//
//				}				
//			}
//
//				PMString textToInsert("");
//		
//				if(!iConverter)
//				{
//					textToInsert=tabbedTextData;					
//				}
//				else{
//					textToInsert=iConverter->translateString(tabbedTextData);
//					iConverter->ChangeQutationMarkONOFFState(kFalse);
//				}
//				WideString insertText(textToInsert);
//			//	textModel->Delete(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1);	///	CS3 Change
//				textModel->Delete(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1);
////CS3 Change	textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
//				textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
//				if(iConverter)
//					iConverter->ChangeQutationMarkONOFFState(kTrue);
//				for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
//				{
//
//					SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
//					XMLReference createdElement;
//					Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
//					addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
//
//				}
//		
//			}
//	}while(kFalse);
}

//following method added by Tushar on 18/12/06
bool8 getAttributeIDFromNotesNewByElementId(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes,double eventPriceTableTypeID)
{
	//CA("getAttributeIDFromNotesNewByElementId");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	//int32 eventPriceTableTypeID = ptrIAppFramework->getPubItemPriceTableType();

	//CA_NUM("EventPriceTableTypeID : ",eventPriceTableTypeID);
	attributeIDfromNotes = "-1";
	bool8 isNotesValid = kFalse;
	int32 isProduct = pNode.getIsProduct();

	do
	{
		if(isProduct == 1)
		{
			VectorScreenTableInfoPtr tableInfo = NULL;
			//commented by Rahul........     not implemented for oneSource
			//if(pNode.getIsONEsource())
			//{
			//	// For ONEsource
			//	CA("ONEsource");
			//	CA_NUM("pNode.getPubId() : ",pNode.getPubId());
			//	tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
			//}else
			//{
				//For publication mode is selected
				//CA_NUM("pNode.getPubId() : ",pNode.getPubId());
				double langId = 91;
				tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), langId);
			//}
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::getAttributeIDFromNotesNewByElementId::tableInfo == NULL");
				break;
			}
			if(tableInfo->size()==0)
			{ 
				break;
			}
			bool8 isEventPriceTableIDPresent =kFalse;
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == eventPriceTableTypeID)
				{
					oTableValue = *it;
					isEventPriceTableIDPresent = kTrue;
					break;
				}
				
			}
			if(isEventPriceTableIDPresent == kFalse)
			{
				break;
			}

			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			//CA_NUM("vec_notes.size() : ",vec_notes.size());
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotes = *notesIterator;
					//CA("attributeIDfromNotes = " + attributeIDfromNotes);
					isNotesValid = kTrue;
					break;
				}
			}
			if(tableInfo)
				delete tableInfo;
		}	
		if(isProduct == 0)
		{
			//CA("isProduct == kfalse");
			//CA_NUM("pNode.getPBObjectID : ",pNode.getPBObjectID());
			double langId =91; // Apsiva Hardcoded
			//ptrIAppFramework->clearAllStaticObjects();
			VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, langId);
            if(tableInfo == NULL)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::getAttributeIDFromNotesNewByElementId::tableinfo is nil");
				break;
			}
			
			if(tableInfo->size() ==0)
			{
				break;
			}
			//CA_NUM("tableInfo->size() : ",tableInfo->size());
			bool8 isEventPriceTableIDPresent =kFalse;
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == eventPriceTableTypeID)
				{
					oTableValue = *it;
					isEventPriceTableIDPresent = kTrue;
					break;
				}
				
			}
			if(isEventPriceTableIDPresent == kFalse)
			{
				break;
			}

			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			//CA_NUM("vec_notes.size() : ",vec_notes.size());
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				//CA_NUM("*itemIterator : ",*itemIterator);
				//CA("*notesIterator : "+*notesIterator);
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotes = *notesIterator;
					//CA("attributeIDfromNotes = " + attributeIDfromNotes);
					isNotesValid = kTrue;
					break;
				}
			}

			if(tableInfo)
				delete tableInfo;

		}
	}while(kFalse);

return isNotesValid;
}

//following method added by Tushar on 15/01/06
bool8 getAttributeIDFromNotes(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotesSales,PMString & attributeIDfromNotesRegular,double SalePriceTabletypeId,double RegularPriceTabletypeId)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	
	attributeIDfromNotesSales = "-1";
	attributeIDfromNotesRegular = "-1";

	bool8 isNotesValid = kFalse;
	int32 isProduct = pNode.getIsProduct();
	
	do
	{
		if(isProduct == 1)
		{
			VectorScreenTableInfoPtr tableInfo = NULL;
			if(pNode.getIsONEsource())
			{
				// For ONEsource
				//CA_NUM(" one source pNode.getPubId : ",pNode.getPubId());
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
			}else
			{
				//For publication mode is selected
				//CA_NUM("pNode.getPubId : ",pNode.getPubId());
				double langId = 91;
				tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), langId);
			}
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::getAttributeIDFromNotes::!tableInfo");
				break;
			}
			if(tableInfo->size()==0)
			{ 
				break;
			}
			//CA_NUM("tableInfo->size() : ",tableInfo->size());
			bool8 isSalePriceTableIDPresent =kFalse;
			bool8 isRegularPriceTableIDPresent =kFalse;
			
			CItemTableValue oTableValue;
			CItemTableValue oTableValue1;

			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == SalePriceTabletypeId)
				{
					oTableValue = *it;
					isSalePriceTableIDPresent = kTrue;
					
				}
				if(it->getTableTypeID() == RegularPriceTabletypeId)
				{
					oTableValue1 = *it;
					isRegularPriceTableIDPresent = kTrue;
					
				}
				if(isSalePriceTableIDPresent == kTrue && isRegularPriceTableIDPresent == kTrue)
				{
					break;
				}
			}
			
			if(isSalePriceTableIDPresent == kFalse || isRegularPriceTableIDPresent == kFalse)
			{
				break;
			}

			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			//CA_NUM("vec_notes.size() : ",vec_notes.size());
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesSales = *notesIterator;
					//CA("attributeIDfromNotesSales = " + attributeIDfromNotesSales);
					isNotesValid = kTrue;
					break;
				}
			}
			vec_ItemIDs.clear();
			vec_ItemIDs = oTableValue1.getItemIds();
			//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
			vec_notes.clear();
			vec_notes =  oTableValue1.getNotesList();
			//CA_NUM("vec_notes.size() : ",vec_notes.size());
			notesIterator =vec_notes.begin();
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesRegular = *notesIterator;
					//CA("attributeIDfromNotesRegular = " + attributeIDfromNotesRegular);
					isNotesValid = kTrue;
					break;
				}
			}
			if(tableInfo)
				delete tableInfo;

		}
		if(isProduct == 0)
		{
			//CA_NUM("pNode.getPBObjectID : ",pNode.getPBObjectID());
			double langId = 91; //Apsiva Hardcoded
			//ptrIAppFramework->clearAllStaticObjects();
			VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, langId);
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::getAttributeIDFromNotes::!tableInfo");
				break;
			}
			
			if(tableInfo->size()==0)
			{					
				break;
			}
			//CA_NUM("tableInfo->size() : ",tableInfo->size());
			bool8 isSalePriceTableIDPresent =kFalse;
			bool8 isRegularPriceTableIDPresent =kFalse;

			CItemTableValue oTableValue;
			CItemTableValue oTableValue1;

			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == SalePriceTabletypeId)
				{//CA("isEventPriceTableIDPresent = kTrue");
					oTableValue = *it;
					isSalePriceTableIDPresent = kTrue;
				}
				if(it->getTableTypeID() == RegularPriceTabletypeId)
				{//CA("22222222222isEventPriceTableIDPresent = kTrue");
					oTableValue1 = *it;
					isRegularPriceTableIDPresent = kTrue;
				}
				if(isSalePriceTableIDPresent == kTrue && isRegularPriceTableIDPresent == kTrue)
				{
					break;
				}
			}

			if(isSalePriceTableIDPresent == kFalse || isRegularPriceTableIDPresent == kFalse)
				break;
	
			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			//CA_NUM("vec_ItemIDs.size() : ",vec_ItemIDs.size());
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			//CA_NUM("vec_notes.size() : ",vec_notes.size());
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesSales = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
			
			vec_ItemIDs.clear();
			vec_ItemIDs = oTableValue1.getItemIds();
			vec_notes.clear();
			vec_notes =  oTableValue1.getNotesList();

			notesIterator =vec_notes.begin();
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesRegular = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
			if(tableInfo)
				delete tableInfo;
		}
		
	}while(kFalse);

	return isNotesValid;
}



//ErrorCode RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(const UIDRef& boxUIDRef, TagStruct& slugInfo)
//{
//	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion");
///*
//	Note that the functionality implemented in this function is copied and modified 
//	from the similar function  
//	ErrorCode TableUtility :: SprayIndividualItemCopyAttributesInTable
//	(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
//	present in the TableUtility.cpp in AP45_DataSprayer plugin source code.
//	For detailed documenation of the functionality please refere the original above 
//	mentioned function.As comments are removed here for brevity.
//*/
//
//	ErrorCode errcode = kFailure;
//	ErrorCode result = kFailure;
//	vector<int32> FinalItemIds;
//	bool16 AddHeaderToTable = kFalse;
//
//	do
//	{		
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//			break;//breaks the do{}while(kFalse)
//		}		 	
//		
//		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/		
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		if (textFrameUID == kInvalidUID)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrameUID == kInvalidUID");
//			break;//breaks the do{}while(kFalse)
//		}
//		//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		//if (textFrame == nil)
//		//{
//		//	ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrame == nil");
//		//	break;//breaks the do{}while(kFalse)
//		//}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			break;
//		}
//
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			break;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!!ITextFrameColumn");
//			break;
//		}
//		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//		if (textModel == nil)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textModel == nil");
//			break;//breaks the do{}while(kFalse)
//		}
//		UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==nil)
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//		
//		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//		{
//			break;//breaks the do{}while(kFalse)
//		}
//				
//		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//		{//for..tableIndex
//			
//			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//			if(tableModel == nil)
//				continue;//continues the for..tableIndex
//
//			int32 totalNumberColumns = tableModel->GetTotalCols().count;
//			
//			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//			if(tableCommands==NULL)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
//				continue;//continues the for..tableIndex
//			}
//			
//			UIDRef tableRef(::GetUIDRef(tableModel)); 
//
//			int32 tableRef12 =tableRef.GetUID().Get();
//	
//			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//			IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
//			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
//			UIDRef ContentRef = contentRef.GetUIDRef();
//
//			int32 ContentRef12=ContentRef.GetUID().Get();
//
//
//			int32 header1 = slugInfo.header;
//			 int32 totalRowsofcustumtable = tableModel->GetTotalRows().count-1;
//			 int32 totalsColsofcustumtable = tableModel->GetTotalCols().count;
//			 int32 totalheadersofcustumtable = tableModel->GetHeaderRows().count;//GetHeaderRows 
//
//			 if(header1==1 && totalheadersofcustumtable==0)
//			 {
//				 //CA("if(header1==1 && totalheadersofcustumtable==0)");
//				totalheadersofcustumtable=1;
//				totalRowsofcustumtable=totalRowsofcustumtable-1;
//			 }
//
//			 if(totalheadersofcustumtable==0)
//			 {
//				 //CA(" if(totalheadersofcustumtable==0)");
//				totalRowsofcustumtable = tableModel->GetTotalRows().count;
//			 }
//			 else
//			 {
//				// CA("else");
//				totalRowsofcustumtable = tableModel->GetTotalRows().count-1;
//			 }
//
//			 PMString e("totalRowsofcustumtable : ");
//			 e.AppendNumber(totalRowsofcustumtable);
//			 e.Append("\n");
//			 e.Append("totalsColsofcustumtable :");
//			 e.AppendNumber(totalsColsofcustumtable);
//			 e.Append("\n");
//			 e.Append("totalheadersofcustumtable : ");
//			 e.AppendNumber(totalheadersofcustumtable);
//			// CA(e);
//
//
//			
//			if( ContentRef != tableRef)
//			{
//				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//				continue; //continues the for..tableIndex
//			}		
//			
//			//Get the SectionID,ProductID from the tableXMLElementTagPtr
//			PMString strSectionID =  tableXMLElementPtr->GetAttributeValue(WideString("sectionID")); //Cs4
//			PMString strProductID =  tableXMLElementPtr->GetAttributeValue(WideString("parentID"));
//			PMString strLanguageID = tableXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//			//This is important addition.You will come to know from this value whether the table
//			//contains Item's data or Product's data.
//			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index"));
//			PMString RowCountForRepetaionPatternString = tableXMLElementPtr->GetAttributeValue(WideString("rowno"));			
//			
//			PMString strTableID = tableXMLElementPtr->GetAttributeValue(WideString("tableId"));
//			int32 tableID = strTableID.GetAsNumber();
//			if(tableID < -1){
//				//CA("before calling RefreshMultipleListsSprayedInOneTable");
//				errcode = RefreshMultipleListsSprayedInOneTable(boxUIDRef, slugInfo);
//				return errcode;
//				continue;
//			}
//			
//			int32 sectionID = strSectionID.GetAsNumber();
//			int32 productID = strProductID.GetAsNumber();
//			int32 languageID = strLanguageID.GetAsNumber();
//			int32 RowCountForRepetaionPattern = RowCountForRepetaionPatternString.GetAsNumber();
//
//			//CA("RowCountForRepetaionPattern	:: ");
//			//CAI(RowCountForRepetaionPattern);
//
//			bool16 IsAutoResize = kFalse;
//			if(slugInfo.isAutoResize)
//				IsAutoResize= kTrue;		
//
//			if(slugInfo.header == 1)
//				AddHeaderToTable = kTrue;
//			else
//				AddHeaderToTable = kFalse;
//
//			bool16 HeaderRowPresent = kFalse;
//
//			RowRange rowRange(0,0);
//			rowRange = tableModel->GetHeaderRows();
//			int32 headerStart = rowRange.start;
//			int32 headerCount = rowRange.count;
//
//			if(headerCount != 0)
//			{	//CA("HeaderPresent");
//				HeaderRowPresent = kTrue;
//			}
//           
//			VectorScreenTableInfoPtr tableInfo= NULL;
//			bool16 isSectionLevelItemItemPresent = kFalse;
//			int32 isComponentAttributePresent =0;
//			vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
//												  //values inside it for spraying 'Quantity' & 'Availability' attributes.
//
//			CObjectTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			vector<int32> vec_items;
//			FinalItemIds.clear();
//
//			int32 field1 = -1;
//			chilIDMap.clear();
//			ChildIDvec.clear();
//			if(slugInfo.whichTab == 3)
//			{
//				//CA("if(slugInfo.whichTab == 3)");
//				// Checking in table whether there are any item attribute tag present or not, 
//				//if no item tag are present then refresh product or section tags by cell and continue.
//				bool16 isItemTagsPresent = kFalse;
//				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//				{//start for tagIndex = 0
//
//					//CA("for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)");
//					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//					//This is a tag attached to the cell.
//					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//					//Get the text tag attached to the text inside the cell.
//					//We are providing only one texttag inside cell.
//					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//					{
//						continue;
//					}
//					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//					{
//						//CA("for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)");
//						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//						//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//						//The cell may be blank. i.e doesn't have any text tag inside it.
//						if(cellTextTagPtr == NULL)
//						{
//							continue;
//						}
//
////Now check the typeId attribute of the cellTextTag.
//						if((cellTextTagPtr->GetAttributeValue(WideString("typeId")) == WideString("-2")
//							//The below line added on 11July...the serial blast day.
//							//for spraying the whole item preset table inside table cell.
//							|| (cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103")) && HeaderRowPresent == kFalse))
//						{
//							//CA("header present");
//							AddHeaderToTable = kTrue;
//						}
//						PMString  field_1 = cellTextTagPtr->GetAttributeValue(WideString("field1")); //-----
//						field1 = field_1.GetAsNumber();
//
//						PMString strchildId = cellTextTagPtr->GetAttributeValue(WideString("childId"));//Cs4
//						int32 childId = strchildId.GetAsNumber();
//
//						PMString strindex1 = cellTextTagPtr->GetAttributeValue(WideString("index"));//Cs4
//						int32 index11 = strindex1.GetAsNumber();
//
//						int32 size1 = ChildIDvec.size();
//						PMString k1("ChildIDvec :");
//						k1.AppendNumber(size1);
//						//CA(k1);
//						/*ChildIDvec.push_back(childId);*/
//						bool16 flag =kFalse;
//						if(index11==4)
//						{
//							/*bool16 flag =kFalse;*/
//							if(size1>0)
//							{
//								for(int32 i=0;i<size1;i++)
//								{
//									if(ChildIDvec.at(i)==childId)
//										flag=kTrue;
//								}
//							}
//							if(flag==kFalse && childId!=-1)
//								ChildIDvec.push_back(childId);
//						}
//
//
//						PMString k("ChildID :");
//						k.AppendNumber(childId);
//						k.Append("	::Size	::	");
//						k.AppendNumber(ChildIDvec.size());
//						//CA(k);
//
//						chilIDMap.insert(map<int32,int32>::value_type(childId,field1));
//						
//
//						//Now check the typeId attribute of the cellTextTag.
//						if((cellTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4")))
//						{
//							//CA("Item Attribute Found");
//							isItemTagsPresent = kTrue;
//						}											
//					}
//				}//end for tagIndex = 0 
//
//				
//				if(isItemTagsPresent == kFalse)
//				{  
//					//CA("if(isItemTagsPresent == kFalse)");
//					// If no item Level tag Present then spray it by cell and return out of function
//					FinalItemIds.clear();
//					FinalItemIds.push_back(productID);
//					PMString k2("FinalItemIds :-	");
//					for(int32 i=0;i<FinalItemIds.size();i++)
//					{				
//						k2.AppendNumber(FinalItemIds.at(i));
//						k2.Append("	::	");
//					}
//					CA(k2);
//					for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//					{//start for tagIndex = 0
//						//CA("for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)");
//						XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//						//This is a tag attached to the cell.
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						
//						//Get the text tag attached to the text inside the cell.
//						//We are providing only one texttag inside cell.
//						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							//CA("if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)");
//							continue;
//						}
//
//						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//						{ // iterate thruogh cell's tags
//							//CA("for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)	::	iterate thruogh cell's tags");
//							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//							//The cell may be blank. i.e doesn't have any text tag inside it.
//							if(cellTextXMLElementPtr == nil)
//							{
//								//CA("if(cellTextXMLElementPtr == nil)");
//								continue;
//							}
//							//Get all the elementID and languageID from the cellTextXMLElement
//							//Note ITagReader plugin methods are not used.
//							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//							PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//							PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//							PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
//							PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//
//							//added by Tushar on 26/12/06
//							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//							//check whether the tag specifies ProductCopyAttributes.
//							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//                 			int32 elementID = strElementID.GetAsNumber();
//							int32 languageID = strLanguageID.GetAsNumber();
//							int32 typeID	= strTypeID.GetAsNumber();
//							int32 rowno	=	strRowNo.GetAsNumber();
//							int32 index = strIndex.GetAsNumber();
//							//added by Tushar on 26/12/06
//							int32 parentTypeID	= strParentTypeID.GetAsNumber();
//							
//							//CA("rowno :" + rowno);
//							PMString row("rowno : ");
//							row.AppendNumber(rowno);
//							//CA(row);
//							//CA("strLanguageID :" + strLanguageID);
//							//CA("strParentTypeID :" + strParentTypeID);
//
//							int32 tagStartPos = -1;
//							int32 tagEndPos = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//							tagStartPos = tagStartPos + 1;
//							tagEndPos = tagEndPos -1;
//
//							//We will come across three types of tags.
//							//1. ProductCopyAttributes tableFlag == 0
//							//for all other attributes tableFlag == 1
//							//2. TableHeader..typeId == -2
//							//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
//							PMString dispName("");
//							if(tableFlag == "-15")
//							{//This is a impotent tag.Don't process this.	
//								//CA("tableFlag == -15");
//								continue;
//							}
//							else if(tableFlag == "0")
//							{	//CA("tableFlag == 0");
//								//The cell contains only copy attributes
//								//added on 25Sept..EventPrice addition.
//
//								//ended on 25Sept..EventPrice addtion.
//								if(strIndex == "3")
//								{//Product copy attribute
//									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID,kFalse);
//									PMString nm("dispName : ");
//									nm.Append(dispName);
//									nm.Append("\n");
//									nm.Append("productID : ");
//									nm.AppendNumber(productID);
//									nm.Append("\n");
//									nm.Append("elementID : ");
//									nm.AppendNumber(elementID);
//									//CA(nm);
//								}							
//							}
//							else if(tableFlag == "1")
//							{//the cell contains the table
//								//CA("the cell contains the table");
//								if(strIndex == "3")
//								{
//									if(strElementID == "-103")
//									{//Print the table header..table type
//										do
//										{
//											//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
//											//int32 typeId = strTypeID.GetAsNumber();
//											
//											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//											if(typeValObj==nil)
//											{
//												ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
//												break;
//											}
//
//											VectorTypeInfoValue::iterator it1;
//
//											bool16 typeIDFound = kFalse;
//											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//											{	
//												if(typeID == it1->getTypeId())
//												{
//													typeIDFound = kTrue;
//													break;
//												}			
//											}
//											if(typeIDFound)
//												dispName = it1->getName();
//											if(typeValObj)
//												delete typeValObj;
//										}while(kFalse); 
//									}
//									else if(strElementID == "-104")
//									{//Print the entire table
//										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
//										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
//										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
//										TagStruct tagInfo;												
//										tagInfo.whichTab = strIndex.GetAsNumber();
//										tagInfo.parentId = strParentID.GetAsNumber();
//										tagInfo.isTablePresent = kTrue;
//										tagInfo.typeId = typeID;
//										tagInfo.sectionID = strSectionID.GetAsNumber();
//										GetItemTableInTabbedTextForm(tagInfo,dispName);
//									}
//								}
//								
//							}
//							else if(tableFlag == "-13")
//							{//the cell contains the product copy attribute header
//								CElementModel cElementModelObj;
//								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
//								if(result)
//								dispName = cElementModelObj.getDisplayName();
//							}							
//							PMString textToInsert("");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(!iConverter)
//							{
//								textToInsert=dispName;					
//							}
//							else{
//								textToInsert=iConverter->translateString(dispName);
//								iConverter->ChangeQutationMarkONOFFState(kFalse);		
//							}
//							//Spray the Header data .We don't have to change the tag attributes
//							//as we have done it while copy and paste.
//							WideString insertData(textToInsert);
//							//CA_NUM("tagStartPos ",tagStartPos);
//							//CA_NUM("tagEndPos ",tagEndPos);
//							textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//							if(iConverter)
//							{
//								iConverter->ChangeQutationMarkONOFFState(kTrue);
//							}
//						}
//					}//end for tagIndex = 0 
//
//					//CA("Break after refresh only Product Tags inside table");
//					break; // Break after refreshing table without item tag itself.
//				}
//
//				if(ptrIAppFramework->get_isONEsourceMode())
//				{
//					 
//					//CA("123 For ONESOURCE");
//					tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(productID);
//				}else
//				{
//					//For publication
//					//CA("234 FOR PUBLICATION");
//					tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, productID);
//				}
//				
//				if(tableInfo != NULL && tableInfo->size()==0)
//				{ 
//					//CA(" tableInfo->size()==0");
//					delete tableInfo;
//					break;//breaks the for..tableIndex
//				}
//				int32 tableID = slugInfo.tableId;
//				if(tableInfo->size()>0)
//				{
//					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//					{	
//					//CA("Inside for loop ");
//					oTableValue = *it;	
//					if(tableID > 0)
//						if(tableID != oTableValue.getTableID())
//							continue;
//
//					if(IsBookrefreshSelected==kTrue)
//					{
//						if(/*Framelevelstructurevariable==kTrue ||*/ Booklevelstructurevariable==kTrue)
//						{
//							//CA("Structure is true");
//							vec_items = oTableValue.getItemIds();
//						}
//						else if(/*Framelevelcontentvariable==kTrue ||*/ Booklevelcontentvariable==kTrue)
//						{
//							//CA("content is true");
//							for(int32 i=0;i<ChildIDvec.size();i++)
//							{
//								vec_items.push_back(ChildIDvec.at(i));
//							}
//						}
//					}
//					else if(IsFrameRefreshSelected==kTrue)
//					{
//						if(Framelevelstructurevariable==kTrue /*|| Booklevelstructurevariable==kTrue*/)
//						{
//							//CA("Structure is true");
//							vec_items = oTableValue.getItemIds();
//						}
//						else if(Framelevelcontentvariable==kTrue /*|| Booklevelcontentvariable==kTrue*/)
//						{
//							//CA("content is true");
//							for(int32 i=0;i<ChildIDvec.size();i++)
//							{
//								vec_items.push_back(ChildIDvec.at(i));
//								PMString aa("ChildIDvec : ");
//								aa.AppendNumber(ChildIDvec.at(i));
//								//CA(aa);
//							}
//						}
//					
//					}
//					
//					
//					int32 size = static_cast<int32>(vec_items.size());
//					for(int32 ii=0;ii<size;ii++)
//					{
//						PMString tb1("ItemID :");
//						tb1.AppendNumber(vec_items.at(ii));
//						//CA(tb1);
//					}
//				
//					if(field1 == -1)
//					{
//						if(FinalItemIds.size() == 0)
//						{
//							FinalItemIds = vec_items;
//						}
//						else
//						{
//							for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
//							{	
//								bool16 Flag = kFalse;
//								for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
//								{
//									if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
//									{
//										Flag = kTrue;
//										break;
//									}				
//								}
//								if(!Flag)
//									FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
//							}
//						}
//					}
//					else
//					{
//						if(field1 != oTableValue.getTableTypeID())
//							continue;
//						
//						if(FinalItemIds.size() == 0)
//						{
//							FinalItemIds = vec_items;
//						}
//						else
//						{
//							for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
//							{	
//								bool16 Flag = kFalse;
//								for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
//								{
//									if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
//									{
//										Flag = kTrue;
//										break;
//									}				
//								}
//								if(!Flag)
//									FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
//							}
//						}
//					}
//
//				}
//				}
//			}
//			else if(slugInfo.whichTab == 4)
//			{		
//			//	CA("slugInfo.whichTab == 4");
//
//				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//				{//start for tagIndex = 0
//				//	CA("for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)");
//					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//					//This is a tag attached to the cell.
//					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//					//Get the text tag attached to the text inside the cell.
//					//We are providing only one texttag inside cell.
//					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//					{
//						//CA("if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)");
//						continue;
//					}
//
//					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//					{
//						//CA("for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)");
//						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//						//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//						//The cell may be blank. i.e doesn't have any text tag inside it.
//						if(cellTextTagPtr == NULL)
//						{
//						//	CA("if(cellTextTagPtr == NULL)");
//							continue;
//						}
//
//						PMString  field_1 = cellTextTagPtr->GetAttributeValue(WideString("field1")); //-----
//						field1 = field_1.GetAsNumber();
//
//						PMString tableflagstr = cellTextTagPtr->GetAttributeValue(WideString("tableFlag"));
//						int32 tableflag = tableflagstr.GetAsNumber();
//
//						
//
//						PMString childtagstr = cellTextTagPtr->GetAttributeValue(WideString("childTag"));
//						int32 childtag=childtagstr.GetAsNumber();
//
//						PMString pbObjectId = cellTextTagPtr->GetAttributeValue(WideString("pbObjectId"/*"rowno"*/));//Cs4
//						int32 pubobject = pbObjectId.GetAsNumber();
//
//
//						PMString tb("pubobject :");
//						tb.AppendNumber(pubobject);
//						tb.Append("\n");
//						tb.Append("ChildTag : ");
//						tb.AppendNumber(childtag);
//						//CA(tb);
//						//Now check the typeId attribute of the cellTextTag.
//						if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1")
//							//The below line added on 11July...the serial blast day.
//							//for spraying the whole item preset table inside table cell.
//							|| (cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103")) && HeaderRowPresent == kFalse))
//						{
//							//CA("header present");
//							AddHeaderToTable = kTrue;
//						}
//
//						//if(cellTextTagPtr->GetAttributeValue(WideString("colno")) == WideString("-101") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
//						if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))	
//						{
//							isSectionLevelItemItemPresent = kTrue;
//							if(pubobject==-1)
//								isSectionLevelItemItemPresent = kFalse;
//							
//							if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-901"))
//								isComponentAttributePresent = 1;
//							else if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-902"))
//								isComponentAttributePresent = 2;
//							else if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-903"))
//								isComponentAttributePresent = 3;
//						}						
//					}
//				}//end for tagIndex = 0 
//
//				int32 totalsrowsofcustomTable11 = tableModel->GetTotalRows().count;
//
//				
//
//				if(isSectionLevelItemItemPresent == kTrue)//child==1 and tableflag==0
//				{
//					//CA("if(isSectionLevelItemItemPresent == kTrue)");
//					int32 cPBObjectID = slugInfo.pbObjectId;
//					int32 languageId = slugInfo.languageID;
//					if(isComponentAttributePresent == 0)
//					{
//						//CA("isComponentAttributePresent == 0");
//						PMString aa("cPBObjectID : ");
//						aa.AppendNumber(cPBObjectID);
//						//CA(aa);
//						ptrIAppFramework->clearAllStaticObjects();
//						VectorScreenTableInfoPtr ItemtableInfo =
//							ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(cPBObjectID );
//
//						//CA("ItemtableInfo	::	");
//						CAI(ItemtableInfo->size());
//						if(!ItemtableInfo)
//						{
//						//	CA("tableinfo is NULL");
//							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
//							break;
//						}
//						
//						if(ItemtableInfo->size()==0)
//						{
//							//CA("tableinfo size==0");
//							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
//							break;
//						}
//						CItemTableValue oTableValue;
//						VectorScreenTableInfoValue::iterator it;
//
//						bool16 typeidFound=kFalse;
//						vector<int32> vec_items;
//						FinalItemIds.clear();
//
//						if(iscontentbuttonselected==kTrue)
//						{
//							//CA("if(iscontentbuttonselected==kTrue)");
//							for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
//							{
//						//	CA("iterate through vector tableInfo ");
//							//for tabelInfo start				
//							oTableValue = *it;
//
//							if(slugInfo.tableId > 0)
//								if(slugInfo.tableId != oTableValue.getTableID())
//									continue;
//							
//							vec_items =ItemIdsForIndesignTable;/* oTableValue.getItemIds();*/
//
//							PMString aa("vec_items : ");
//							aa.AppendNumber(static_cast<int32>(vec_items.size()));
//							//CA(aa);
//							
//							if(field1 == -1)	//--------
//							{
//								if(FinalItemIds.size() == 0)
//								{
//									FinalItemIds = vec_items;
//								}
//								else
//								{
//									for(int32 i=0; i<vec_items.size(); i++)
//									{	bool16 Flag = kFalse;
//										for(int32 j=0; j<FinalItemIds.size(); j++)
//										{
//											if(vec_items[i] == FinalItemIds[j])
//											{
//												Flag = kTrue;
//												break;
//											}				
//										}
//										if(!Flag)
//											FinalItemIds.push_back(vec_items[i]);
//									}
//								}
//							}
//							else
//							{
//								if(field1 != oTableValue.getTableTypeID())
//									continue;
//
//								if(FinalItemIds.size() == 0)
//								{
//									FinalItemIds = vec_items;
//								}
//								else
//								{
//									for(int32 i=0; i<vec_items.size(); i++)
//									{	bool16 Flag = kFalse;
//										for(int32 j=0; j<FinalItemIds.size(); j++)
//										{
//											if(vec_items[i] == FinalItemIds[j])
//											{
//												Flag = kTrue;
//												break;
//											}				
//										}
//										if(!Flag)
//											FinalItemIds.push_back(vec_items[i]);
//									}
//								}
//							}
//
//						}//for tabelInfo end			
//						//end ItemTable Handling
//						PMString a("FinalItemIds.size()	:	");
//						a.AppendNumber(static_cast<int32>(FinalItemIds.size()));
//						//CA(a);
//						if(ItemtableInfo)
//							delete ItemtableInfo;
//						
//						}
//						else if(issrtucturebuttenselected==kTrue)
//						{
//							//CA("else if(issrtucturebuttenselected==kTrue)");
//						for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
//						{
//							//CA("iterate through vector tableInfo ");
//							//for tabelInfo start				
//							oTableValue = *it;
//PMString a("slugInfo.tableId	:	");
//a.AppendNumber(slugInfo.tableId);
//a.Append("\noTableValue.getTableID()	:	");
//a.AppendNumber(oTableValue.getTableID());
////CA(a);
//							if(slugInfo.tableId > 0)
//								if(slugInfo.tableId != oTableValue.getTableID())
//									continue;
//							
//							vec_items = oTableValue.getItemIds();
//
//							PMString aa("vec_items : ");
//							aa.AppendNumber(static_cast<int32>(vec_items.size()));
//							//CA(aa);
//							
//							if(field1 == -1)	//--------
//							{
//								if(FinalItemIds.size() == 0)
//								{
//									FinalItemIds = vec_items;
//								}
//								else
//								{
//									for(int32 i=0; i<vec_items.size(); i++)
//									{	bool16 Flag = kFalse;
//										for(int32 j=0; j<FinalItemIds.size(); j++)
//										{
//											if(vec_items[i] == FinalItemIds[j])
//											{
//												Flag = kTrue;
//												break;
//											}				
//										}
//										if(!Flag)
//											FinalItemIds.push_back(vec_items[i]);
//									}
//								}
//							}
//							else
//							{
//								if(field1 != oTableValue.getTableTypeID())
//									continue;
//
//								if(FinalItemIds.size() == 0)
//								{
//									FinalItemIds = vec_items;
//								}
//								else
//								{
//									for(int32 i=0; i<vec_items.size(); i++)
//									{	bool16 Flag = kFalse;
//										for(int32 j=0; j<FinalItemIds.size(); j++)
//										{
//											if(vec_items[i] == FinalItemIds[j])
//											{
//												Flag = kTrue;
//												break;
//											}				
//										}
//										if(!Flag)
//											FinalItemIds.push_back(vec_items[i]);
//									}
//								}
//							}
//
//						}//for tabelInfo end			
//						//end ItemTable Handling
//						PMString a("FinalItemIds.size()	:	");
//						a.AppendNumber(static_cast<int32>(FinalItemIds.size()));
//						///CA(a);
//						if(ItemtableInfo)
//							delete ItemtableInfo;
//						}
//					}
//					else 
//					{
//					//	CA("isComponentAttributePresent!=0=======");
//						VectorScreenTableInfoPtr KXAItemtableInfo = NULL;
//						if(isComponentAttributePresent == 1)
//						{
//							bool16 isKitTable = kFalse; // we are spraying component table.
//							KXAItemtableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(productID, languageId, isKitTable); 
//						}
//						else if(isComponentAttributePresent == 2)
//						{
//							KXAItemtableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(productID, languageId); 
//						}
//						else if(isComponentAttributePresent == 3)
//						{
//							KXAItemtableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(productID, languageId); 
//						}
//
//						if(!KXAItemtableInfo)
//						{
//							//CA("KittableInfo is NULL");
//							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
//							break;
//						}
//
//						if(KXAItemtableInfo->size()==0){
//							//CA(" KittableInfo->size()==0");
//							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
//							break;
//						}
//
//						CObjectTableValue oTableValue;
//						VectorScreenTableInfoValue::iterator it  ;						
//
//						bool16 typeidFound=kFalse;
//						vector<int32> vec_items;
//						FinalItemIds.clear();
//
//						it = KXAItemtableInfo->begin();
//						{//for tabelInfo start				
//							oTableValue = *it;				
//							vec_items = oTableValue.getItemIds();
//						
//							if(FinalItemIds.size() == 0)
//							{
//								FinalItemIds = vec_items;
//							}
//							else
//							{
//								for(int32 i=0; i<vec_items.size(); i++)
//								{	bool16 Flag = kFalse;
//									for(int32 j=0; j<FinalItemIds.size(); j++)
//									{
//										if(vec_items[i] == FinalItemIds[j])
//										{
//											Flag = kTrue;
//											break;
//										}				
//									}
//									if(!Flag)
//										FinalItemIds.push_back(vec_items[i]);
//								}
//							}
//						}//for tabelInfo end
//						
//						Kit_vec_tablerows = oTableValue.getTableData();
//						if(KXAItemtableInfo)
//							delete KXAItemtableInfo;
//					}
//				}
//				else
//				{  // If Only Section Level (Pub Item) Item Present then spray it by cell and return out of function
//					//CA("isSectionLevelItemItemPresent == kFalse");
//					//CA("if(isSectionLevelItemItemPresent == kFalse");
//					FinalItemIds.clear();
//					FinalItemIds.push_back(productID);
//					
//					for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//					{//start for tagIndex = 0
//					
//						XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//						//This is a tag attached to the cell.
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						
//						//Get the text tag attached to the text inside the cell.
//						//We are providing only one texttag inside cell.
//						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							continue;
//						}
//
//						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//						{ // iterate thruogh cell's tags
//							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//							//The cell may be blank. i.e doesn't have any text tag inside it.
//							if(cellTextXMLElementPtr == nil)
//							{
//								continue;
//							}
//							//Get all the elementID and languageID from the cellTextXMLElement
//							//Note ITagReader plugin methods are not used.
//							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));//Cs4
//							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//							PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//							PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//							PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
//							PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//
//							PMString strheader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
//							int32 header = strheader.GetAsNumber();
//
//							//added by Tushar on 26/12/06
//							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//							//check whether the tag specifies ProductCopyAttributes.
//							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//                 			int32 elementID = strElementID.GetAsNumber();
//							int32 parentID = strParentID.GetAsNumber();
//							int32 languageID = strLanguageID.GetAsNumber();
//							int32 typeID	= strTypeID.GetAsNumber();
//							int32 rowno	=	strRowNo.GetAsNumber();
//							//added by Tushar on 26/12/06
//							int32 parentTypeID	= strParentTypeID.GetAsNumber();
//							
//							/*CA("strElementID :" + strElementID);
//							CA("strLanguageID :" + strLanguageID);
//							CA("strParentTypeID :" + strParentTypeID);*/
//
//							int32 tagStartPos = -1;
//							int32 tagEndPos = -1;
//							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//							tagStartPos = tagStartPos + 1;
//							tagEndPos = tagEndPos -1;
//
//							//We will come across three types of tags.
//							//1. ProductCopyAttributes tableFlag == 0
//							//for all other attributes tableFlag == 1
//							//2. TableHeader..typeId == -2
//							//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
//							PMString dispName("");
//							if(tableFlag == "-15")
//							{//This is a impotent tag.Don't process this.	
//								//CA("tableFlag == -15");
//								continue;
//							}
//							else if(tableFlag == "0")
//							{	//CA("tableFlag == 0");
//								//The cell contains only copy attributes
//								//added on 25Sept..EventPrice addition.
//
//								//ended on 25Sept..EventPrice addtion.
//								if(strIndex == "3")
//								{//Product copy attribute
//									//CA("dispName");
//									PMString aa("productID : ");
//									aa.AppendNumber(productID);
//									aa.Append("\n");
//									aa.Append("elementID : ");
//									aa.AppendNumber(elementID);
//									aa.Append("\n");
//									aa.Append("languageID :");
//									aa.AppendNumber(languageID);
//									//CA(aa);
//
//									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID,kFalse);
//								}
//								else if(strIndex == "4" && header == 1)
//								{//	CA("strIndex == 4 && typeID == -2");
//									//Item copy attribue header
//									//following code added by Tushar on 27/12/06
//									if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
//									{
//										VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//										if(TypeInfoVectorPtr != NULL)
//										{//CA("TypeInfoVectorPtr != NULL");
//											VectorTypeInfoValue::iterator it3;
//											int32 Type_id = -1;
//											PMString temp = "";
//											PMString name = "";
//											for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//											{
//												Type_id = it3->getTypeId();
//												if(parentTypeID == Type_id)
//												{
//													temp = it3->getName();
//													if(elementID == -701)
//													{
//														dispName = temp;
//													}
//													else if(elementID == -702)
//													{
//														dispName = temp + " Suffix";
//													}
//												}
//											}
//										}
//										if(TypeInfoVectorPtr)
//											delete TypeInfoVectorPtr;
//									}
//									else if(elementID == -703)
//									{
//										//CA("dispName = $Off");
//										dispName = "$Off";
//									}
//									else if(elementID == -704)
//									{
//										//CA("dispName = %Off");
//										dispName = "%Off";
//									}		
//											
//									//upto here added by Tushar on 27/12/06									
//									else
//									{
//										//CA("11");
//										dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//									}
//								}
//								else if(strIndex == "4")
//								{//CA("strIndex == 4");
//									//Item copy attribute 
//									if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//									{//CA("common functions 1219");
//										PublicationNode pNode;
//										pNode.setPubId(productID);
//
//										PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")); //Cs4
//										pNode.setPBObjectID(strPBObjectID.GetAsNumber());
//										if(indexValueAttachedToTable == "4")
//										{
//											
//											pNode.setIsProduct(0);
//										}
//										else if(indexValueAttachedToTable == "3")
//										{
//											pNode.setIsProduct(1);
//										}
//										if(rowno != -1)
//										{
//											//CA("sETTING IsONEsource flag as false");
//											pNode.setIsONEsource(kFalse);
//										}
//										TagStruct tagInfo;
//										tagInfo.elementId = elementID;
//										tagInfo.languageID = languageID;
//										tagInfo.typeId = typeID;
//										tagInfo.sectionID = sectionID;
//										tagInfo.tagPtr = cellTextXMLElementPtr;
//										//added by Tushar on 26/12/06
//										tagInfo.parentTypeID = parentTypeID;										
//										handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
//									}
//									else if(cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField")) == WideString("1"))
//									{
//										//CA("tagInfo.isEventField");
//										VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(typeID,elementID,sectionID);
//										if(vecPtr != NULL)
//										{
//											if(vecPtr->size()> 0)
//												dispName = vecPtr->at(0).getObjectValue();	
//											delete vecPtr;
//										}
//										//CA("itemAttributeValue = " + itemAttributeValue);
//									}
//									else{
//										//CA("12");
//										PMString aa("typeID : ");
//									aa.AppendNumber(typeID);
//									aa.Append("\n");
//									aa.Append("elementID : ");
//									aa.AppendNumber(elementID);
//									aa.Append("\n");
//									aa.Append("languageID :");
//									aa.AppendNumber(languageID);
//									//CA(aa);
//										dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parentID,elementID,languageID,/*kTrue*/kFalse);//23OCT09//Amit
//									}
//								}
//								
//							}
//							else if(tableFlag == "1")
//							{//the cell contains the table
//								if(strIndex == "3")
//								{
//									if(strElementID == "-103")
//									{//Print the table header..table type
//										do
//										{
//											//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
//											//int32 typeId = strTypeID.GetAsNumber();
//											
//											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//											if(typeValObj==nil)
//											{
//												ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
//												break;
//											}
//
//											VectorTypeInfoValue::iterator it1;
//
//											bool16 typeIDFound = kFalse;
//											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//											{	
//													if(typeID == it1->getTypeId())
//													{
//														typeIDFound = kTrue;
//														break;
//													}			
//											}
//											if(typeIDFound)
//												dispName = it1->getName();
//											if(typeValObj)
//												delete typeValObj;
//										}while(kFalse); 
//									}
//									else if(strElementID == "-104")
//									{//Print the entire table
//										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
//										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
//										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
//										TagStruct tagInfo;												
//										tagInfo.whichTab = strIndex.GetAsNumber();
//										tagInfo.parentId = strParentID.GetAsNumber();
//										tagInfo.isTablePresent = kTrue;
//										tagInfo.typeId = typeID;
//										tagInfo.sectionID = strSectionID.GetAsNumber();
//										GetItemTableInTabbedTextForm(tagInfo,dispName);
//									}
//								}
//								else if(strIndex == "4")
//								{
//									if(strElementID == "-103")
//									{
//										do
//										{
//											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//											if(typeValObj==nil)
//											{
//												ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
//												break;
//											}
//											VectorTypeInfoValue::iterator it1;
//											bool16 typeIDFound = kFalse;
//											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//											{	
//													if(typeID == it1->getTypeId())
//													{
//														typeIDFound = kTrue;
//														break;
//													}			
//											}
//											if(typeIDFound)
//												dispName = it1->getName();
//											if(typeValObj)
//												delete typeValObj;
//										}while(kFalse);
//									}
//									else if(strElementID == "-104")
//									{//Print the entire table
//										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
//										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
//										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
//										TagStruct tagInfo;												
//										tagInfo.whichTab = strIndex.GetAsNumber();
//										//This is just a temporary adjustment
//										PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")); //Cs4
//										tagInfo.parentId = pbObjectId.GetAsNumber();
//
//										tagInfo.isTablePresent = kTrue;
//										tagInfo.typeId = typeID;
//										tagInfo.sectionID = strSectionID.GetAsNumber();
//										GetItemTableInTabbedTextForm(tagInfo,dispName);
//									}
//								}
//							}
//							else if(tableFlag == "-13")
//							{//the cell contains the product copy attribute header
//									CElementModel cElementModelObj;
//									bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
//									if(result)
//									dispName = cElementModelObj.getDisplayName();
//									//CA("13");
//							}							
//							PMString textToInsert("");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(!iConverter)
//							{
//								textToInsert=dispName;									
//							}
//							else{
//								textToInsert=iConverter->translateString(dispName);
//								iConverter->ChangeQutationMarkONOFFState(kFalse);
//							}
//							//Spray the Header data .We don't have to change the tag attributes
//							//as we have done it while copy and paste.
//							WideString insertData(textToInsert);
//							//CA_NUM("tagStartPos ",tagStartPos);
//							//CA_NUM("tagEndPos ",tagEndPos);
//							textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//							if(iConverter)
//							{
//								iConverter->ChangeQutationMarkONOFFState(kTrue);			
//							}
//						}
//					}//end for tagIndex = 0 
//					break; // Break after refreshing table with only Pub Item itself.
//				}
//			}
//			int32 totalsrowsofcustomTable12 = tableModel->GetTotalRows().count;
//			//CA("totalsrowsofcustomTable12 :: ");
//			//CAI(totalsrowsofcustomTable12);
//			
//			
//			int32 TotalNoOfItems =static_cast<int32> (FinalItemIds.size());
//			//CA("TotalNoOfItems :: ");
//			//CAI(TotalNoOfItems);
//			if(TotalNoOfItems <= 0)
//				break;	//breaks the for..tableIndex
//
//			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
//			//CA("totalNumberOfColumns :: ");
//			//CAI(totalNumberOfColumns);
//
//			int32 totalNumberOfBodyRowsBeforeRefresh = 0;
//			if(HeaderRowPresent)
//			{
//				totalNumberOfBodyRowsBeforeRefresh =  tableModel->GetBodyRows().count;
//				//CA("totalNumberOfBodyRowsBeforeRefresh :: ");
//			//	CAI(totalNumberOfBodyRowsBeforeRefresh);
//			}
//			else
//			{
//				totalNumberOfBodyRowsBeforeRefresh =  tableModel->GetTotalRows().count;
//				//CA("totalNumberOfBodyRowsBeforeRefresh else :: ");
//				///CAI(totalNumberOfBodyRowsBeforeRefresh);
//			}
//			
//
//			GridArea gridAreaForEntireTable;
//			//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
//			if(HeaderRowPresent)
//			{	
//				gridAreaForEntireTable.topRow = headerCount;
//				gridAreaForEntireTable.leftCol = 0;
//				gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern+headerCount;
//                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//			}
//			else
//			{
//				if(AddHeaderToTable)
//				{
//					gridAreaForEntireTable.topRow = RowCountForRepetaionPattern;
//					gridAreaForEntireTable.leftCol = 0;
//					gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern + RowCountForRepetaionPattern;
//					gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//				}
//				else
//				{
//					gridAreaForEntireTable.topRow = 0;
//					gridAreaForEntireTable.leftCol = 0;
//					gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern;
//					gridAreaForEntireTable.rightCol = totalNumberOfColumns;
//					//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
//				}
//			}
//			
//
//				
//	
//			int32 TotalNoOfBodyRowsWithOutHeaders = totalNumberOfBodyRowsBeforeRefresh;
//
//			//CA("before TotalNoOfBodyRowsWithOutHeaders :: ");
//				//CAI(TotalNoOfBodyRowsWithOutHeaders);
//
//			TotalNoOfBodyRowsWithOutHeaders = TotalNoOfBodyRowsWithOutHeaders - RowCountForRepetaionPattern;
//			if(AddHeaderToTable && (!HeaderRowPresent)) // in case of Header Elements in Body rows ie. No Header(*IndesignTable) rows are present in Table  
//			{
//				TotalNoOfBodyRowsWithOutHeaders = TotalNoOfBodyRowsWithOutHeaders - RowCountForRepetaionPattern;
//
//				//CA("TotalNoOfBodyRowsWithOutHeaders :: ");
//				//CAI(TotalNoOfBodyRowsWithOutHeaders);
//			}
//			// Now check for available Body rows and required body rows.			
//
//			int32 RequiredNoOfBodyRowsForRefreah = (TotalNoOfItems * RowCountForRepetaionPattern );
//			PMString r1("RequiredNoOfBodyRowsForRefreah : ");
//			r1.AppendNumber(RequiredNoOfBodyRowsForRefreah);
//			r1.Append("\n");
//			r1.Append("TotalNoOfBodyRowsWithOutHeaders : ");
//			r1.AppendNumber(TotalNoOfBodyRowsWithOutHeaders);
//			r1.Append("\n");
//			r1.Append("TotalNoOfItems : ");
//			r1.AppendNumber(TotalNoOfItems);
//			r1.Append("\n");
//			r1.Append("RowCountForRepetaionPattern : ");
//			r1.AppendNumber(RowCountForRepetaionPattern);
//			//CA(r1);
//
//			if((RequiredNoOfBodyRowsForRefreah ) == (TotalNoOfBodyRowsWithOutHeaders))
//			{
//				//CA(" No of items before & after Refresh are same ie. no change in item counts");
//			}
//			else if((RequiredNoOfBodyRowsForRefreah ) > (TotalNoOfBodyRowsWithOutHeaders))
//			{
//				//CA("Need to Add rows");
//				deleterowornot=kTrue;
//				int32 NoOfRowsToBeAdded = RequiredNoOfBodyRowsForRefreah - TotalNoOfBodyRowsWithOutHeaders;
//				//CA(" NoOfRowsToBeAdded :: ");
//				//CAI(NoOfRowsToBeAdded);
//				int32 rowPatternRepeatCount = (NoOfRowsToBeAdded / RowCountForRepetaionPattern);
//				//CA(" rowPatternRepeatCount :: ");
//				///CAI(rowPatternRepeatCount);
//				if(HeaderRowPresent)
//				{
//					//CA("HeaderRowPresent");
//					int32 totalsrowsofcustomTable13 = tableModel->GetTotalRows().count;
//					PMString row("Rows2 : ");
//					row.AppendNumber(totalsrowsofcustomTable13);
//					row.Append("\n");
//					row.Append("NoOfRowsToBeAdded : ");
//					row.AppendNumber(NoOfRowsToBeAdded);
//					//CA(row);
//
//					result = tableCommands->InsertRows(RowRange(totalNumberOfBodyRowsBeforeRefresh+ headerCount -1, NoOfRowsToBeAdded), Tables::eAfter, 0);
//
//					int32 totalsrowsofcustomTable12 = tableModel->GetTotalRows().count;
//						PMString ow("Rows3 : ");
//					ow.AppendNumber(totalsrowsofcustomTable12);
//					//CA(ow);
//					
//					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//					if(canCopy == kFalse)
//					{
//						//CA("canCopy == kFalse");
//						ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
//						break;
//					}
//				//	CA("canCopy");
//					
//					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//					if(tempPtr == NULL)
//					{
//						//CA("if(tempPtr == NULL)");
//						ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
//						break;
//					}
//					
//					for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//					{
//						tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh+headerCount),0),ITableModel::eAll,tempPtr);
//						tempPtr = tableModel->Copy(gridAreaForEntireTable);
//					}
//					int32 totalsrowsofcustomTable08 = tableModel->GetTotalRows().count;
//					PMString rows22("Rows22 : ");
//					rows22.AppendNumber(totalsrowsofcustomTable08);
//					//CA(rows22);
//				}
//				else
//				{	
//					//CA("Not HeaderRowPresent ");
//
//					int32 totalsrowsofcustomTable13 = tableModel->GetTotalRows().count;
//					PMString row("Rows2 : ");
//					row.AppendNumber(totalsrowsofcustomTable13);
//					row.Append("\n");
//					row.Append("NoOfRowsToBeAdded : ");
//					row.AppendNumber(NoOfRowsToBeAdded);
//					//CA(row);
//
//					result = tableCommands->InsertRows(RowRange(totalNumberOfBodyRowsBeforeRefresh -1, NoOfRowsToBeAdded), Tables::eAfter, 0);
//
//					int32 totalsrowsofcustomTable14 = tableModel->GetTotalRows().count;
//					PMString ro("Rows3 : ");
//					ro.AppendNumber(totalsrowsofcustomTable14);
//					//CA(ro);
//
//					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
//					if(canCopy == kFalse)
//					{
//						//CA("canCopy == kFalse");
//						ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
//						break;
//					}
//					//CA("canCopy");					
//					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
//					if(tempPtr == NULL)
//					{
//						//CA("if(tempPtr == NULL)--");
//						ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
//						break;
//					}
//					
//					for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
//					{
//						PMString ASD("GridAddress : ");
//						ASD.AppendNumber((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh));
//						ASD.Append("  totalNumberOfBodyRowsBeforeRefresh : ");
//						ASD.AppendNumber(totalNumberOfBodyRowsBeforeRefresh);
//						ASD.Append("  RowCountForRepetaionPattern : ");
//						ASD.AppendNumber(RowCountForRepetaionPattern);
//						ASD.Append("  pasteIndex : ");
//						ASD.AppendNumber(pasteIndex);
//						//CA(ASD);
//						tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh),0),ITableModel::eAll,tempPtr);
//						tempPtr = tableModel->Copy(gridAreaForEntireTable);
//					}
//					
//				}
//
//			}
//			else if((RequiredNoOfBodyRowsForRefreah ) < (TotalNoOfBodyRowsWithOutHeaders))
//			{
//				//CA("Need to Delete rows");
//				int32 NoOfRowsToBeDeleted = TotalNoOfBodyRowsWithOutHeaders - RequiredNoOfBodyRowsForRefreah;
//				
//				if(HeaderRowPresent)
//				{
//					result = tableCommands->DeleteRows(RowRange(RequiredNoOfBodyRowsForRefreah+ headerCount -1 , NoOfRowsToBeDeleted));
//				}
//				else
//				{
//					result = tableCommands->DeleteRows(RowRange(RequiredNoOfBodyRowsForRefreah - 1 , NoOfRowsToBeDeleted));
//				}
//			}
//
//			int32 totalsrowsofcustomTable11 = tableModel->GetTotalRows().count;
//
//				PMString rows("Rows1 : ");
//				rows.AppendNumber(totalsrowsofcustomTable11);
//				//CA(rows);
//			
//			int32 TotalNoOFRowsBeforeRefresh =  tableModel->GetTotalRows().count;
//			rows.clear();
//			rows.Append("TotalNoOFRowsBeforeRefresh	");
//			rows.AppendNumber(TotalNoOFRowsBeforeRefresh);
//				//CA(rows);
//			int32 StartCountForBodyRow = 0;
//			if(HeaderRowPresent)
//			{
//
//				StartCountForBodyRow = headerCount;
//				rows.clear();
//				rows.Append("StartCountForBodyRow	");
//				rows.AppendNumber(StartCountForBodyRow);
//				//CA(rows);
//			}
//			else
//			{
//				if(AddHeaderToTable)
//				{
//					StartCountForBodyRow = RowCountForRepetaionPattern;
//					rows.clear();
//					rows.Append("StartCountForBodyRow else	");
//					rows.AppendNumber(StartCountForBodyRow);
//					//CA(rows);
//				}
//				else
//					StartCountForBodyRow =0;
//				
//			}
//			
//			rows.clear();
//			rows.Append("StartCountForBodyRow outer	");
//			rows.AppendNumber(StartCountForBodyRow);
//			//CA(rows);
//
//			int32 totalBodyCell = 0;
//			GridArea bodyArea_new = tableModel->GetBodyArea();
//			ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
//			ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
//			while (iterTable != endTable)
//			{
//				totalBodyCell++;
//				++iterTable;
//			}
//
//			rows.clear();
//			rows.Append("totalBodyCell 	");
//			rows.AppendNumber(totalBodyCell);
//			//CA(rows);
//
//			int32 bodyCellsForItem = totalBodyCell/(tableModel->GetBodyRows().count/RowCountForRepetaionPattern);
//
//			rows.clear();
//			rows.Append("bodyCellsForItem 	");
//			rows.AppendNumber(bodyCellsForItem);
//			//CA(rows);
//
//			GridAddress headerRowGridAddress(RowCountForRepetaionPattern-1,bodyCellsForItem-1);
//
//		//	for(int32 p=0; p < TotalNoOfItems ; p++)
//		//	{				
//		//		for(int32 nonHeaderRowIndex =0; nonHeaderRowIndex < RowCountForRepetaionPattern; nonHeaderRowIndex++)
//		//		{
//		//			for(int32 columnIndex = 0;columnIndex <totalNumberOfColumns; columnIndex++)
//		//			{//for..iterate through each column
//		//				int32 tagIndex = (StartCountForBodyRow* totalNumberOfColumns ) + (p * RowCountForRepetaionPattern * totalNumberOfColumns ) + (nonHeaderRowIndex * totalNumberOfColumns) +columnIndex;
//		//				
//		//				/*PMString ASD("tagIndex : ");
//		//				ASD.AppendNumber(tagIndex);
//		//				CA(ASD);*/
//	
//		//				XMLReference cellXMLElementRef = tableXMLElementPtr->GetNthChild(tagIndex);
//		
//					int32 ColIndex = -1;
//					ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea_new));
//					ITableModel::const_iterator endTable1(tableModel->end(bodyArea_new));
//					while (iterTable1 != endTable1)
//					{
//						//CA("while (iterTable1 != endTable1)");
//						GridAddress gridAddress = (*iterTable1); 
//						if(AddHeaderToTable)
//							if(gridAddress <= headerRowGridAddress){
//								iterTable1++;							
//								continue;
//							}
//						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//						if(cellContent == nil) 
//						{
//							break;
//						}
//						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//						if(!cellXMLReferenceData) {
//							break;
//						}
//						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//						++iterTable1;
//
//						ColIndex++;
//						int32 itemIndex = ColIndex/bodyCellsForItem;
//					
//						rows.clear();
//						rows.Append("ColIndex 	");
//						rows.AppendNumber(ColIndex);
//						rows.Append("	::bodyCellsForItem ::	");
//						rows.AppendNumber(bodyCellsForItem);
//						rows.Append("	::itemIndex ::	");
//						rows.AppendNumber(itemIndex);
//						//CA(rows);
//
//			
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
//						//Also note that, cell may be blank i.e doesn't contain any textTag.
//						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							//CA("if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)");
//							continue;
//						}
//						PMString s("cellXMLElementPtr->GetChildCount() : ");
//						s.AppendNumber(cellXMLElementPtr->GetChildCount());
//						//CA(s);
//						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//						{
//							//CA("for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)");
//							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
//							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
//							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
//					
//							//Check if the cell is blank.
//							if(cellTextXMLElementPtr == NULL)
//							{
//								//CA("Check if the cell is blank. if(cellTextXMLElementPtr == NULL)");
//								continue;
//							}
//							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
//							PMString imgFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//
//							if(index == "3")
//								continue;
//							else if(index == "4")
//							{
//								//CA("index == 4");
//								PMString ItemID("");
//								ItemID.AppendNumber(itemIndex);//(p);
//								
//								if(imgFlag == "1"){
//									//CA("For Image ");
//									cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(ItemID));
//								}
//								else{
//									cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(ItemID));
//								}
//							}
//						}
//					}
//
//		//		}
//		//	}
//				
////Now refreshing Individual Cell
//				
//			//	for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//			//	{//start for tagIndex = 0
//
//				GridArea headerArea = tableModel->GetHeaderArea();
//				GridArea tableArea_new = tableModel->GetTotalArea();
//				ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
//				ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
//				int32 totalsColsofcustumtable11 = tableModel->GetTotalCols().count;
//				
//			
//				while (iterTable2 != endTable2)
//				{
//					//CA("while (iterTable2 != endTable2)");
//					
//						totalBodyCell++;
//						
//						PMString s1("totalBodyCell : ");
//						s1.AppendNumber(totalBodyCell);
//						//CA(s1);
//
//						GridAddress gridAddress = (*iterTable2);         
//						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//						if(cellContent == nil) 
//						{
//						//	CA("cellContent == nil");
//							break;
//						}
//						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//						if(!cellXMLReferenceData) {
//						//	CA("!cellXMLReferenceData");
//							break;
//						}
//						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//						++iterTable2;
//
//						//XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//
//						//This is a tag attached to the cell.
//						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//						
//						//Get the text tag attached to the text inside the cell.
//						//We are providing only one texttag inside cell.
//						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//						{
//							continue;
//						}
//						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//					{ // iterate thruogh cell's tags
//						//CA("Iterating thruogh cell's tags " );
//						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//						//The cell may be blank. i.e doesn't have any text tag inside it.
//						if(cellTextXMLElementPtr == nil)
//						{
//
//							//CA("if(cellTextXMLElementPtr == nil) The cell may be blank. i.e doesn't have any text tag inside it. " );
//							continue;
//						}
//						//Get all the elementID and languageID from the cellTextXMLElement
//						//Note ITagReader plugin methods are not used.
//						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
//						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID")); //Cs4
//						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//						PMString strImgFlag = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//						PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
//						PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//						PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
//						PMString strheader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
//						PMString strdataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
//						int32 header = strheader.GetAsNumber();
//
//						//added by Tushar on 26/12/06
//						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//						//check whether the tag specifies ProductCopyAttributes.
//						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//						PMString tableIdStr =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
//						PMString pbObjectIdStr =   cellTextXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
//                 		PMString sectionIDStr =   cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));
//						PMString imgFlagStr =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
//						PMString imgIndex =   cellTextXMLElementPtr->GetAttributeValue(WideString("imageIndex"));
//						
//						int32 elementID = strElementID.GetAsNumber();
//						int32 languageID = strLanguageID.GetAsNumber();
//
//						int32 sequenceNo;
//						if(imgFlagStr == "1")
//							sequenceNo = strParentID.GetAsNumber();
//						else
//							sequenceNo = strTypeID.GetAsNumber();
//						int32 typeID = sequenceNo;
//						
//						bool16 isHeaderCell = headerArea.Contains(gridAddress);
//
//						if(header != 1 && isHeaderCell == kFalse)  // for Header Elements
//						{
//							//CA("if(header != 1 && isHeaderCell == kFalse)  // for Header Elements");
//							typeID	= FinalItemIds[sequenceNo];
//						}
//						else if(isHeaderCell == kTrue)
//						{
//							//CA("else if(isHeaderCell == kTrue)");
//							typeID = strProductID.GetAsNumber();
//						}
//						PMString NewTypeId("");
//						NewTypeId.AppendNumber(typeID);
//						//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(NewTypeId)); //Cs4
//						if(imgFlagStr != "1"){
//							//CA("if(imgFlagStr!=1)");
//							cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-1"));
//							cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(NewTypeId)); 
//						}
//						else{
//							//CA("For Image");
//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(NewTypeId));
//						}
//						int32 rowno	=	strRowNo.GetAsNumber();
//						//added by Tushar on 26/12/06
//						int32 parentTypeID	= strParentTypeID.GetAsNumber();
//						
//						//CA("strElementID :" + strElementID);
//						//CA("strLanguageID :" + strLanguageID);
//						//CA("strParentTypeID :" + strParentTypeID);
//
//						int32 tagStartPos = -1;
//						int32 tagEndPos = -1;
//						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//						tagStartPos = tagStartPos + 1;
//						tagEndPos = tagEndPos -1;
//
//						//We will come across three types of tags.
//						//1. ProductCopyAttributes tableFlag == 0
//						//for all other attributes tableFlag == 1
//						//2. TableHeader..typeId == -2
//						//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
//						PMString dispName("");
//						if(tableFlag == "-15")
//						{//This is a impotent tag.Don't process this.	
//							//CA("tableFlag == -15");
//							continue;
//						}
//						else if(tableFlag == "0")
//						{	
//							//CA("tableFlag == 0");
//							//The cell contains only copy attributes
//							//added on 25Sept..EventPrice addition.
//
//							//ended on 25Sept..EventPrice addtion.
//							if(strdataType == "4"){
//								//CA("strdataType == 4");
//								if(indexValueAttachedToTable == "3")
//								{
//									//CA("indexValueAttachedToTable == 3");							
//															
//									int32 objectId = strParentID.GetAsNumber(); 
//									VectorScreenTableInfoPtr tableInfo = NULL;
//									CObjectTableValue oTableValue;	
//									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
//									//CA("Not ONEsource");
//									do
//									{
//										tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionIDStr.GetAsNumber(), objectId, kTrue);
//										if(!tableInfo)
//										{
//											//CA("!tableInfo");
//											ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!tableInfo");
//											break;
//										}
//										if(tableInfo->size()==0)
//										{
//											//CA(" tableInfo->size()==0");
//											ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
//											break;
//										}
//										
//										VectorScreenTableInfoValue::iterator it;
//										bool16 typeidFound=kFalse;
//										//CA("for Loop");
//										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//										{
//											oTableValue = *it;
//											if(oTableValue.getTableID() == tableIdStr.GetAsNumber())
//											{   
//												//CA("typeidFound=kTrue;");
//												typeidFound=kTrue;				
//												break;
//											}
//										}
//										if(typeidFound)
//										{
//											dispName = oTableValue.getTableName();	
//										}
//										else
//										{
//											//ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!typeidFound");
//											dispName = "";
//										}
//									}while(0);									
//								
//									if(tableInfo)
//										delete tableInfo;
//								}
//								else if(indexValueAttachedToTable == "4"){
//									//CA("indexValueAttachedToTable == 4");
//									do{
//										VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pbObjectIdStr.GetAsNumber());
//										if(!tableInfo)
//										{
//											//CA("tableinfo is NULL");
//											break;
//										}
//										
//										if(tableInfo->size()==0)
//										{ 
//											//CA("tableinfo size==0");
//											break;
//										}
//										CItemTableValue oTableValue;
//										VectorScreenTableInfoValue::iterator it;
//
//										bool16 typeidFound=kFalse;
//										vector<int32> vec_items;
//										FinalItemIds.clear();
//										dispName == "";	
//										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//										{
//											//CA("iterate through vector tableInfo ");
//											//for tabelInfo start
//											oTableValue = *it;	
//											if(oTableValue.getTableID() == tableIdStr.GetAsNumber()){
//												dispName = oTableValue.getName();
//												break;
//											}											
//										}//for tabelInfo end			
//										//end ItemTable Handling
//										
//										if(tableInfo)
//											delete tableInfo;
//									}while(kFalse);
//								}
//							}
//							else if(strIndex == "3")
//							{//Product copy attribute
//								//CA("14");
//								VectorScreenTableInfoPtr tableInfo = NULL;
//								tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(slugInfo.pbObjectId);
//								if(!tableInfo)
//								{
//									//CA("if(!tableInfo)");
//									ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
//									break;
//								}
//
//								if(tableInfo->size() == 0)
//								{
//									//CA("tableInfo->size() == 0");
//									ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo->size() == 0");	
//									break;
//								}
//
//								CObjectTableValue oTableValue;
//								VectorScreenTableInfoValue::iterator it12;
//								for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
//								{	
//									oTableValue = *it12;
//
//									 //if(/*slugInfo.typeId == -3 && */slugInfo.elementId == -121)
//									//{
//										dispName = oTableValue.getName();
//										//CA("tempBuffer01 = " + dispName);
//										break;
//									//}
//								}
//								//dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID,kFalse);
//							}
//							else if(strIndex == "4" && header == 1)
//							{
//								//CA("strIndex == 4 && header == 1");
//								//Item copy attribue header
//								//following code added by Tushar on 27/12/06
//								if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
//								{
//									VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//									if(TypeInfoVectorPtr != NULL)
//									{//CA("TypeInfoVectorPtr != NULL");
//										VectorTypeInfoValue::iterator it3;
//										int32 Type_id = -1;
//										PMString temp = "";
//										PMString name = "";
//										for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//										{
//											Type_id = it3->getTypeId();
//											if(parentTypeID == Type_id)
//											{
//												temp = it3->getName();
//												if(elementID == -701)
//												{
//													dispName = temp;
//												}
//												else if(elementID == -702)
//												{
//													dispName = temp + " Suffix";
//												}
//											}
//										}
//									}
//									if(TypeInfoVectorPtr)
//										delete TypeInfoVectorPtr;
//								}
//								else if(elementID == -703)
//								{
//									//CA("dispName = $Off");
//									dispName = "$Off";
//								}
//								else if(elementID == -704)
//								{
//									//CA("dispName = %Off");
//									dispName = "%Off";
//								}
//								else if(elementID == -805)
//								{									
//									dispName = "Quantity";
//                                }
//								else if(elementID == -806)
//								{									
//									dispName = "Availablity";
//                                }
//								else if(elementID == -807)
//								{									
//									dispName = "Cross-reference Type";
//                                }
//								else if(elementID == -808)
//								{									
//									dispName = "Rating";
//                                }
//								else if(elementID == -809)
//								{									
//									dispName = "Alternate";
//                                }
//								else if(elementID == -810)
//								{									
//									dispName = "Comments";
//                                }
//								else if(elementID == -811)
//								{									
//									dispName = "";
//                                }
//								else if(elementID == -812)
//								{									
//									dispName = "Quantity";
//                                }
//								else if(elementID == -813)
//								{									
//									dispName = "Required";
//                                }
//								else if(elementID == -814)
//								{									
//									dispName = "Comments";
//                                }
//								else
//								{
//									//CA("dispName 15");
//									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
//								}
//							}
//							else if(strIndex == "4")
//							{
//								//CA("strIndex == 4");
//								//Item copy attribute 
//								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//								{//CA("common functions 1219");
//									PublicationNode pNode;
//									pNode.setPubId(productID);
//
//									//PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")); //cs4
//									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
//									
//									pNode.setPBObjectID(strPBObjectID.GetAsNumber());
//									if(indexValueAttachedToTable == "4")
//									{
//										
//										pNode.setIsProduct(0);
//									}
//									else if(indexValueAttachedToTable == "3")
//									{
//										pNode.setIsProduct(1);
//									}
//									if(rowno != -1)
//									{
//										//CA("sETTING IsONEsource flag as false");
//										pNode.setIsONEsource(kFalse);
//									}
//									TagStruct tagInfo;
//									tagInfo.elementId = elementID;
//									tagInfo.languageID = languageID;
//									tagInfo.typeId = -1;//typeID;
//									tagInfo.childId = typeID;
//									tagInfo.header = header;
//									tagInfo.imgFlag = 1;
//									tagInfo.sectionID = sectionID;
//									tagInfo.tagPtr = cellTextXMLElementPtr;
//									//added by Tushar on 26/12/06
//									tagInfo.parentTypeID = parentTypeID;
//									tagInfo.pbObjectId = strPBObjectID.GetAsNumber();
//
//									handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
//								}
//								
//								else if(elementID == -803)  // For 'Letter Key'
//								{									
//									dispName.Clear();
//									int wholeNo =  sequenceNo / 26;
//									int remainder = sequenceNo % 26;								
//
//									if(wholeNo == 0)
//									{// will print 'a' or 'b'... 'z'  upto 26 item ids
//										dispName.Append(AlphabetArray[remainder]);
//									}
//									else  if(wholeNo <= 26)
//									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//										dispName.Append(AlphabetArray[wholeNo-1]);	
//										dispName.Append(AlphabetArray[remainder]);	
//									}
//									else if(wholeNo <= 52)
//									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//										dispName.Append(AlphabetArray[0]);
//										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
//										dispName.Append(AlphabetArray[remainder]);										
//									}
//								}
//								else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
//								}
//								else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
//								}
//								else if((elementID == -807) && (isComponentAttributePresent == 2))
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
//                                }
//								else if((elementID == -808) && (isComponentAttributePresent == 2))
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
//                                }
//								else if((elementID == -809) && (isComponentAttributePresent == 2))
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][2]);	
//                                }
//								else if((elementID == -810) && (isComponentAttributePresent == 2))
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][3]);	
//                                }
//								else if((elementID == -811) && (isComponentAttributePresent == 2))
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][4]);	
//                                }
//								else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
//								}
//								else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
//								}
//								else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
//								{
//									dispName = (Kit_vec_tablerows[sequenceNo][2]);	
//								}
//								else if(strImgFlag == "1"){
//									//CA("Got it");
//									TagStruct tagInfo;
//									tagInfo.elementId = elementID;
//									tagInfo.languageID = languageID;
//									tagInfo.typeId = strTypeID.GetAsNumber();
//									tagInfo.parentId = typeID;
//									tagInfo.whichTab = 4;
//									tagInfo.header = header;
//									tagInfo.sectionID = sectionID;
//									tagInfo.colno = strColNo.GetAsNumber();
//									tagInfo.rowno = strRowNo.GetAsNumber();
//									tagInfo.imageIndex = imgIndex.GetAsNumber();
//									tagInfo.tagPtr = cellTextXMLElementPtr;
//									//added by Tushar on 26/12/06
//									tagInfo.parentTypeID = parentTypeID;
//									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
//									tagInfo.pbObjectId = strPBObjectID.GetAsNumber();
//
//									XMLContentReference contentRef = cellTextXMLElementPtr->GetContentReference();																									
//									UIDRef ContentRef = contentRef.GetUIDRef();
//
//									if(imageDownLoadPath == "")
//									{
//										InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
//										if(ptrIClientOptions==nil)
//										{
//											ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
//											break;
//										}
//										//PMString imagePath=ptrIClientOptions->getImageDownloadPath();
//										imageDownLoadPath=ptrIClientOptions->getImageDownloadPath();
//										
//									}
//									PMString imagePath = imageDownLoadPath;
//									//CA("imagePath 1	:	"+imagePath);	
//									//Commented by Yogesh Joshi on 1 March 06
//									if(imagePath!="")
//									{
//										const char *imageP=(imagePath.GrabCString()); //Cs4
//										 
//										#ifdef MACINTOSH
//											if(imageP[std::strlen(imageP)-1]!=':')
//												imagePath+=":";
//										#else
//											if(imageP[std::strlen(imageP)-1]!='\\')
//												imagePath+="\\";
//										#endif
//									}
//
//									Refresh refresh;
//									refresh.fillImageInBox(ContentRef, tagInfo, typeID, imagePath, kFalse);
//								}
//								else
//								{
//									if((isComponentAttributePresent == 2) && ((Kit_vec_tablerows[sequenceNo][4]) == "N"))
//									{
//										//CA("dispName 16");
//										dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(typeID,elementID,languageID);
//									}
//									else if(cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField")) == WideString("1"))
//									{
//										//CA("tagInfo.isEventField");
//										VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(typeID,elementID,sectionIDStr.GetAsNumber());
//										if(vecPtr != NULL)
//										{
//											if(vecPtr->size()> 0)
//												dispName = vecPtr->at(0).getObjectValue();	
//											delete vecPtr;
//										}
//										//CA("itemAttributeValue = " + itemAttributeValue);
//									}
//									else{
//										
//										dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(typeID,elementID,languageID,/*kTrue*/kFalse);//23OCT09//Amit
//										PMString q("ItemID : ");
//										q.AppendNumber(typeID);
//										q.Append("\n");
//										q.Append("elementID : ");
//										q.AppendNumber(elementID);
//										q.Append("\n");
//										q.Append("String : ");
//										q.Append(dispName);
//										//CA(q);
//									}
//								}
//							}
//							
//						}
//						else if(tableFlag == "1")
//						{//the cell contins the table
//							//CA("else if(tableFlag == 1)");
//							if(strIndex == "3")
//							{
//							//	CA("if(strIndex == 3)");
//								if(strElementID == "-103")
//								{//Print the table header..table type
//									do
//									{
//										//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
//										//CA(strTypeID);
//										//int32 typeId = strTypeID.GetAsNumber();
//										
//										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//										if(typeValObj==nil)
//										{
//										//	CA("if(typeValObj==nil)");
//											ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
//											break;
//										}
//
//										VectorTypeInfoValue::iterator it1;
//
//										bool16 typeIDFound = kFalse;
//										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//										{	
//											if(typeID == it1->getTypeId())
//											{
//											//	CA("if(typeID == it1->getTypeId())");
//												typeIDFound = kTrue;
//												break;
//											}			
//										}
//										if(typeIDFound)
//											dispName = it1->getName();
//										//CA("dispName	::	"+dispName);
//										if(typeValObj)
//											delete typeValObj;
//									}while(kFalse); 
//								}
//								else if(strElementID == "-104")
//								{//Print the entire table
//									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
//									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
//									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
//									TagStruct tagInfo;												
//									tagInfo.whichTab = strIndex.GetAsNumber();
//									tagInfo.parentId = strParentID.GetAsNumber();
//									tagInfo.isTablePresent = kTrue;
//									tagInfo.typeId = typeID;
//									tagInfo.sectionID = strSectionID.GetAsNumber();
//
//									GetItemTableInTabbedTextForm(tagInfo,dispName);
//								}
//
//							}
//							else if(strIndex == "4")
//							{
//								//CA("else if(strIndex == 4)");
//								if(strElementID == "-103")
//								{
//									//CA("if(strElementID == -103)");
//									do
//									{
//										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
//										if(typeValObj==nil)
//										{
//											//CA("if(typeValObj==nil)");
//											ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
//											break;
//										}
//										VectorTypeInfoValue::iterator it1;
//										bool16 typeIDFound = kFalse;
//										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//										{	
//												if(typeID == it1->getTypeId())
//												{
//													typeIDFound = kTrue;
//													break;
//												}			
//										}
//										if(typeIDFound)
//											dispName = it1->getName();
//
//										//CA("dispName	::	"+dispName);
//
//										if(typeValObj)
//											delete typeValObj;
//									}while(kFalse);
//								}
//								else if(strElementID == "-104")
//								{//Print the entire table
//									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
//									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
//									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
//									TagStruct tagInfo;												
//									tagInfo.whichTab = strIndex.GetAsNumber();
//									//This is just a temporary adjustment
//									//PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")); //Cs4
//									PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
//									tagInfo.parentId = pbObjectId.GetAsNumber();
//
//									tagInfo.isTablePresent = kTrue;
//									tagInfo.typeId = typeID;
//									tagInfo.sectionID = strSectionID.GetAsNumber();
//
//									GetItemTableInTabbedTextForm(tagInfo,dispName);
//
//								}
//
//							}
//						}
//						else if(tableFlag == "-13"){//the cell contains the product copy attribute header
//							CElementModel cElementModelObj;
//							bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
//							if(result)
//							dispName = cElementModelObj.getDisplayName();
//						}
//						
//						PMString textToInsert("");
//						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//						if(!iConverter)
//						{
//							//CA("if(!iConverter)");
//							textToInsert=dispName;
//						}
//						else{
//							iConverter->ChangeQutationMarkONOFFState(kFalse);	
//							textToInsert=iConverter->translateString(dispName);
//						}
//						//Spray the Header data .We don't have to change the tag attributes
//						//as we have done it while copy and paste.
//						WideString insertData(textToInsert);
//						//CA_NUM("tagStartPos ",tagStartPos);
//						//CA_NUM("tagEndPos ",tagEndPos);
//						textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
//						if(iConverter)
//						{
//							iConverter->ChangeQutationMarkONOFFState(kTrue);			
//						}
//					}
//				}//end for tagIndex = 0 
//
//	   }//end for..tableIndex
//	   errcode = kSuccess;
//	}while(0);
//	return errcode;     
//}
//
//

ErrorCode RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
/*
	Note that the functionality implemented in this function is copied and modified 
	from the similar function  
	ErrorCode TableUtility :: SprayIndividualItemCopyAttributesInTable
	(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)
	present in the TableUtility.cpp in AP45_DataSprayer plugin source code.
	For detailed documenation of the functionality please refere the original above 
	mentioned function.As comments are removed here for brevity.
*/
	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	bool16 AddHeaderToTable = kFalse;
	VectorScreenTableInfoPtr ItemtableInfo = NULL;
	VectorScreenTableInfoPtr tableInfo= NULL;

	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;//breaks the do{}while(kFalse)
		}		 	
		
		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/		
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrameUID == kInvalidUID");
			break;//breaks the do{}while(kFalse)
		}
		//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		//if (textFrame == nil)
		//{
		//	ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrame == nil");
		//	break;//breaks the do{}while(kFalse)
		//}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textModel == nil");
			break;//breaks the do{}while(kFalse)
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;//breaks the do{}while(kFalse)
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;//breaks the do{}while(kFalse)
		}
				
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == nil)
				continue;//continues the for..tableIndex

			int32 totalNumberColumns = tableModel->GetTotalCols().count;
			
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
				continue;//continues the for..tableIndex
			}
			
			UIDRef tableRef(::GetUIDRef(tableModel)); 

			int32 tableRef12 =tableRef.GetUID().Get();
	
			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
			IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
			UIDRef ContentRef = contentRef.GetUIDRef();

			int32 ContentRef12=ContentRef.GetUID().Get();


			int32 header1 = slugInfo.header;
			 int32 totalRowsofcustumtable = tableModel->GetTotalRows().count;
			 int32 totalsColsofcustumtable = tableModel->GetTotalCols().count;
			 int32 totalheadersofcustumtable = tableModel->GetHeaderRows().count;//GetHeaderRows 

			 if(header1==1 && totalheadersofcustumtable==0)
			 {
				 //CA("if(header1==1 && totalheadersofcustumtable==0)");
				totalheadersofcustumtable=1;
				totalRowsofcustumtable=totalRowsofcustumtable-1;
			 }
			 else if(totalheadersofcustumtable==0)
			 {
				 //CA(" if(totalheadersofcustumtable==0)");
				totalRowsofcustumtable = tableModel->GetTotalRows().count;
			 }
			 else
			 {
				// CA("else");
				totalRowsofcustumtable = tableModel->GetTotalRows().count-totalheadersofcustumtable;
			 }

			/* PMString e("totalRowsofcustumtable : ");
			 e.AppendNumber(totalRowsofcustumtable);
			 e.Append("\n");
			 e.Append("totalsColsofcustumtable :");
			 e.AppendNumber(totalsColsofcustumtable);
			 e.Append("\n");
			 e.Append("totalheadersofcustumtable : ");
			 e.AppendNumber(totalheadersofcustumtable);*/
			// CA(e);


			
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}		

			//Get the SectionID,ProductID from the tableXMLElementTagPtr
			PMString strSectionID =  tableXMLElementPtr->GetAttributeValue(WideString("sectionID")); //Cs4
			PMString strProductID =  tableXMLElementPtr->GetAttributeValue(WideString("parentID"));
			PMString strLanguageID = tableXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
			//This is important addition.You will come to know from this value whether the table
			//contains Item's data or Product's data.
			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index"));
			PMString RowCountForRepetaionPatternString = tableXMLElementPtr->GetAttributeValue(WideString("rowno"));			
			
			PMString strTableID = tableXMLElementPtr->GetAttributeValue(WideString("tableId"));
			double tableID = strTableID.GetAsDouble();
			if(tableID < -1){
				PMString idValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("ID"));
				//CA("before calling RefreshMultipleListsSprayedInOneTable");
				//CA(" :: "+ idValueAttachedToTable );
				
				errcode = RefreshMultipleListsSprayedInOneTable(boxUIDRef, slugInfo);
				return errcode;
				//continue;
			}

			double sectionID = strSectionID.GetAsDouble();
			double productID = strProductID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();
			int32 RowCountForRepetaionPattern = RowCountForRepetaionPatternString.GetAsNumber();

			//CA("RowCountForRepetaionPattern	:: ");
			//CAI(RowCountForRepetaionPattern);

			bool16 IsAutoResize = kFalse;
			if(slugInfo.isAutoResize)
				IsAutoResize= kTrue;		

			if(slugInfo.header == 1)
				AddHeaderToTable = kTrue;
			else
				AddHeaderToTable = kFalse;

			bool16 HeaderRowPresent = kFalse;

			RowRange rowRange(0,0);
			rowRange = tableModel->GetHeaderRows();
			int32 headerStart = rowRange.start;
			int32 headerCount = rowRange.count;

			if(headerCount != 0)
			{	//CA("HeaderPresent");
				HeaderRowPresent = kTrue;
			}
           
			
			bool16 isSectionLevelItemItemPresent = kFalse;
			int32 isComponentAttributePresent =0;
			vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
												  //values inside it for spraying 'Quantity' & 'Availability' attributes.

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();

			int32 field1 = -1;
			chilIDMap.clear();
			ChildIDvec.clear();
			if(slugInfo.whichTab == 3)
			{
				//CA("if(slugInfo.whichTab == 3)");
				// Checking in table whether there are any item attribute tag present or not, 
				//if no item tag are present then refresh product or section tags by cell and continue.
				bool16 isItemTagsPresent = kFalse;
				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
				{//start for tagIndex = 0

					//CA("for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)");
					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}
					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
					{
						//CA("for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)");
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
						//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextTagPtr == NULL)
						{
							continue;
						}

						PMString  field_1 = cellTextTagPtr->GetAttributeValue(WideString("field1")); //-----
						field1 = field_1.GetAsNumber();

						PMString strchildId = cellTextTagPtr->GetAttributeValue(WideString("childId"));//Cs4
						double childId = strchildId.GetAsDouble();

						PMString strindex1 = cellTextTagPtr->GetAttributeValue(WideString("index"));//Cs4
						int32 index11 = strindex1.GetAsNumber();

						int32 size1 = (int32)ChildIDvec.size();
						PMString k1("ChildIDvec :");
						k1.AppendNumber(size1);
						//CA(k1);
						/*ChildIDvec.push_back(childId);*/
						bool16 flag =kFalse;
						if(index11==4)
						{
							/*bool16 flag =kFalse;*/
							if(size1>0)
							{
								for(int32 i=0;i<size1;i++)
								{
									if(ChildIDvec.at(i)==childId)
										flag=kTrue;
								}
							}
							if(flag==kFalse && childId!=-1)
								ChildIDvec.push_back(childId);
						}


						//PMString k("ChildID :");
						//k.AppendNumber(childId);
						//k.Append("	::Size	::	");
						//k.AppendNumber((int32)ChildIDvec.size());
						//CA(k);

						chilIDMap.insert(map<double,double>::value_type(childId,field1));
						

						//Now check the typeId attribute of the cellTextTag.
						if((cellTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4")))
						{
							//CA("Item Attribute Found");
							isItemTagsPresent = kTrue;
						}											
					}
				}//end for tagIndex = 0 

				
				if(isItemTagsPresent == kFalse)
				{  
					//CA("if(isItemTagsPresent == kFalse)");
					// If no item Level tag Present then spray it by cell and return out of function
					FinalItemIds.clear();
					FinalItemIds.push_back(productID);
					PMString k2("FinalItemIds :-	");
					for(int32 i=0;i<FinalItemIds.size();i++)
					{				
						k2.AppendNumber(PMReal(FinalItemIds.at(i)));
						k2.Append("	::	");
					}
					//CA(k2);
					for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
					{//start for tagIndex = 0
						//CA("for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)");
						XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
						//This is a tag attached to the cell.
						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						
						//Get the text tag attached to the text inside the cell.
						//We are providing only one texttag inside cell.
						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
						{
							//CA("if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)");
							continue;
						}

						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
						{ // iterate thruogh cell's tags
							//CA("for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)	::	iterate thruogh cell's tags");
							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
							//The cell may be blank. i.e doesn't have any text tag inside it.
							if(cellTextXMLElementPtr == nil)
							{
								//CA("if(cellTextXMLElementPtr == nil)");
								continue;
							}
							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
							PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));

							//added by Tushar on 26/12/06
							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 			double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
							double typeID	= strTypeID.GetAsDouble();
							double rowno	=	strRowNo.GetAsDouble();
							int32 index = strIndex.GetAsNumber();
							//added by Tushar on 26/12/06
							double parentTypeID	= strParentTypeID.GetAsDouble();
							
							//CA("rowno :" + rowno);
							//PMString row("rowno : ");
							//row.AppendNumber(rowno);
							//CA(row);
							//CA("strLanguageID :" + strLanguageID);
							//CA("strParentTypeID :" + strParentTypeID);

							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							//We will come across three types of tags.
							//1. ProductCopyAttributes tableFlag == 0
							//for all other attributes tableFlag == 1
							//2. TableHeader..typeId == -2
							//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
							PMString dispName("");
							if(tableFlag == "-15")
							{//This is a impotent tag.Don't process this.	
								//CA("tableFlag == -15");
								continue;
							}
							else if(tableFlag == "0")
							{//	CA("tableFlag == 0");
								//The cell contains only copy attributes
								//added on 25Sept..EventPrice addition.

								//ended on 25Sept..EventPrice addtion.
								if(strIndex == "3")
								{//Product copy attribute
									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionID, kFalse);
									/*PMString nm("dispName : ");
									nm.Append(dispName);
									nm.Append("\n");
									nm.Append("productID : ");
									nm.AppendNumber(productID);
									nm.Append("\n");
									nm.Append("elementID : ");
									nm.AppendNumber(elementID);*/
									//CA(nm);
								}							
							}
							else if(tableFlag == "1")
							{//the cell contains the table
								//CA("the cell contains the table");
								if(strIndex == "3")
								{
									if(strElementID == "-103")
									{//Print the table header..table type
										do
										{
											//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
											//int32 typeId = strTypeID.GetAsNumber();
											
											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
											if(typeValObj==nil)
											{
												ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
												break;
											}

											VectorTypeInfoValue::iterator it1;

											bool16 typeIDFound = kFalse;
											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
											{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
											}
											if(typeIDFound)
												dispName = it1->getName();
											if(typeValObj)
												delete typeValObj;
										}while(kFalse); 
									}
									else if(strElementID == "-104")
									{//Print the entire table
										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
										TagStruct tagInfo;												
										tagInfo.whichTab = strIndex.GetAsNumber();
										tagInfo.parentId = strParentID.GetAsDouble();
										tagInfo.isTablePresent = kTrue;
										tagInfo.typeId = typeID;
										tagInfo.sectionID = strSectionID.GetAsDouble();
										GetItemTableInTabbedTextForm(tagInfo,dispName);
									}
								}
								
							}
							else if(tableFlag == "-13")
							{//the cell contains the product copy attribute header
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
								if(result)
								dispName = cElementModelObj.getDisplayName();
							}							
							PMString textToInsert("");
							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								textToInsert=dispName;					
							}
							else{
								textToInsert=iConverter->translateString(dispName);
								iConverter->ChangeQutationMarkONOFFState(kFalse);		
							}
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
							//WideString insertData(textToInsert);							
							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);

							if(iConverter)
							{
								iConverter->ChangeQutationMarkONOFFState(kTrue);
							}
						}
					}//end for tagIndex = 0 

					//CA("Break after refresh only Product Tags inside table");
					break; // Break after refreshing table without item tag itself.
				}

				if(ptrIAppFramework->get_isONEsourceMode())
				{
					// For ONEsource mode
					//CA("123 For ONESOURCE");
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(productID);
				}else
				{
					//For publication
					//CA("234 FOR PUBLICATION");
					tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, productID, languageID);
				}
				
				if(tableInfo != NULL && tableInfo->size()==0)
				{ 
					///CA(" tableInfo->size()==0");
					delete tableInfo;
					break;//breaks the for..tableIndex
				}
	
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{				
					oTableValue = *it;		

					if(!((tableID == -1 ) || (tableID == oTableValue.getTableID())))
						continue;

					vec_items = oTableValue.getItemIds();
				
					if(field1 == -1)
					{
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
							{	
								bool16 Flag = kFalse;
								for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
								{
									if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
							}
						}
					}
					else
					{
						if(field1 != oTableValue.getTableTypeID())
							continue;
						
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
							{	
								bool16 Flag = kFalse;
								for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
								{
									if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
							}
						}
					}

				}
			}
			else if(slugInfo.whichTab == 4)
			{		
				
				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
				{//start for tagIndex = 0
					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}
					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
					{
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
						InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextTagPtr == NULL)
							{
								continue;
							}
											
						//Now check the typeId attribute of the cellTextTag.
						if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1")
							//The below line added on 11July...the serial blast day.
							//for spraying the whole item preset table inside table cell.
							|| (cellTextTagPtr->GetAttributeValue(WideString("ID")) == WideString("-103")) && HeaderRowPresent == kFalse))
						{
							//CA("header present");
							AddHeaderToTable = kTrue;
						}

						if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
						//if(cellTextTagPtr->GetAttributeValue(WideString("colno")) == WideString("-101") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
						{
							isSectionLevelItemItemPresent = kTrue;
							//CA("SectionLevelItemItemPresent == kTrue");
							if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-901"))
								isComponentAttributePresent = 1;
							else if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-902"))
								isComponentAttributePresent = 2;
							else if(cellTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-903"))
								isComponentAttributePresent = 3;
						}						
					}
				}//end for tagIndex = 0 

				if(isSectionLevelItemItemPresent == kTrue)
				{
					//CPbObjectValue* cPBObjectValuePtr =  ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(sectionID, productID);
					//CPbObjectValue* cPBObjectValuePtr =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(sectionID, productID);
					//int32 cPBObjectID = cPBObjectValuePtr->getPub_object_id();
					double languageId = slugInfo.languageID;
					if(isComponentAttributePresent == 0)
					{
						//ptrIAppFramework->clearAllStaticObjects();
						ItemtableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(productID, sectionID , languageId);

						if(!ItemtableInfo)
						{
							//CA("tableinfo is NULL");
							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
							break;
						}
						
						if(ItemtableInfo->size()==0)
						{ //CA("tableinfo size==0");
							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
							break;
						}
						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it;

						bool16 typeidFound=kFalse;
						vector<double> vec_items;
						FinalItemIds.clear();

						for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
						{
							//CA("iterate through vector tableInfo ");
							//for tabelInfo start				
							oTableValue = *it;				

							if(!((tableID == -1 ) || (tableID == oTableValue.getTableID())))
								continue;

							vec_items = oTableValue.getItemIds();
						
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}//for tabelInfo end			
						//end ItemTable Handling
						/*if(ItemtableInfo)
							delete ItemtableInfo;*/
					}
					else 
					{
						VectorScreenTableInfoPtr KXAItemtableInfo = NULL;
						if(isComponentAttributePresent == 1)
						{
							bool16 isKitTable = kFalse; // we are spraying component table.
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(productID, languageId, isKitTable); 
						}
						else if(isComponentAttributePresent == 2)
						{
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(productID, languageId); 
						}
						else if(isComponentAttributePresent == 3)
						{
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(productID, languageId); 
						}

						if(!KXAItemtableInfo)
						{
							//CA("KittableInfo is NULL");
							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
							break;
						}

						if(KXAItemtableInfo->size()==0){
							//CA(" KittableInfo->size()==0");
							makeTagInsideCellImpotent(tableXMLElementPtr); //added by Tushar on 23/12/06
							break;
						}

						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it  ;						

						bool16 typeidFound=kFalse;
						vector<double> vec_items;
						FinalItemIds.clear();

						it = KXAItemtableInfo->begin();
						{//for tabelInfo start				
							oTableValue = *it;				
							vec_items = oTableValue.getItemIds();
						
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}//for tabelInfo end
						
						Kit_vec_tablerows = oTableValue.getTableData();
						if(KXAItemtableInfo)
							delete KXAItemtableInfo;
					}
				}
				else
				{  // If Only Section Level (Pub Item) Item Present then spray it by cell and return out of function
					FinalItemIds.clear();
					FinalItemIds.push_back(productID);
					
					for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
					{//start for tagIndex = 0
					
						XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
						//This is a tag attached to the cell.
						InterfacePtr<IIDXMLElement> cellXMLElementPtr( cellXMLElementRef.Instantiate());
						
						//Get the text tag attached to the text inside the cell.
						//We are providing only one texttag inside cell.
						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
						{
							continue;
						}

						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
						{ // iterate thruogh cell's tags
							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
							InterfacePtr<IIDXMLElement> cellTextXMLElementPtr( cellTextXMLRef.Instantiate());
							//The cell may be blank. i.e doesn't have any text tag inside it.
							if(cellTextXMLElementPtr == nil)
							{
								continue;
							}
							//Get all the elementID and languageID from the cellTextXMLElement
							//Note ITagReader plugin methods are not used.
							PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
							PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
							PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
							PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
							PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
							PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

							//added by Tushar on 26/12/06
							PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
							//check whether the tag specifies ProductCopyAttributes.
							PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 			double elementID = strElementID.GetAsDouble();
							double languageID = strLanguageID.GetAsDouble();
							double typeID	= strTypeID.GetAsDouble();
							double rowno	=	strRowNo.GetAsDouble();
							int32 header = strHeader.GetAsNumber();
							double childID = strChildID.GetAsDouble();
							int32 childTag = strChildTag.GetAsNumber();
							//added by Tushar on 26/12/06
							double parentTypeID	= strParentTypeID.GetAsDouble();

							
							
							/*CA("strElementID :" + strElementID);
							CA("strLanguageID :" + strLanguageID);
							CA("strParentTypeID :" + strParentTypeID);*/

							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							//We will come across three types of tags.
							//1. ProductCopyAttributes tableFlag == 0
							//for all other attributes tableFlag == 1
							//2. TableHeader..typeId == -2
							//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
							PMString dispName("");
							if(tableFlag == "-15")
							{//This is a impotent tag.Don't process this.	
								//CA("tableFlag == -15");
								continue;
							}
							else if(tableFlag == "0")
							{	//CA("tableFlag == 0");
								//The cell contains only copy attributes
								//added on 25Sept..EventPrice addition.

								//ended on 25Sept..EventPrice addtion.
								if(strIndex == "3")
								{//Product copy attribute
									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionID, kFalse);
								}
								else if(strIndex == "4" && /*typeID == -2*/ header == 1)
								{//	CA("strIndex == 4 && typeID == -2");
									//Item copy attribue header
									//following code added by Tushar on 27/12/06
									if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
									{
										//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
										//if(TypeInfoVectorPtr != NULL)
										//{//CA("TypeInfoVectorPtr != NULL");
										//	VectorTypeInfoValue::iterator it3;
										//	int32 Type_id = -1;
										//	PMString temp = "";
										//	PMString name = "";
										//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
										//	{
										//		Type_id = it3->getTypeId();
										//		if(parentTypeID == Type_id)
										//		{
										//			temp = it3->getName();
										//			if(elementID == -701)
										//			{
										//				dispName = temp;
										//			}
										//			else if(elementID == -702)
										//			{
										//				dispName = temp + " Suffix";
										//			}
										//		}
										//	}
										//}
										//if(TypeInfoVectorPtr)
										//	delete TypeInfoVectorPtr;
									}
									else if(elementID == -703)
									{
										//CA("dispName = $Off");
										dispName = "$Off";
									}
									else if(elementID == -704)
									{
										//CA("dispName = %Off");
										dispName = "%Off";
									}		
											
									//upto here added by Tushar on 27/12/06									
									else
										dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
								}
								else if(strIndex == "4")
								{//CA("strIndex == 4");
									//Item copy attribute 
									if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
									{//CA("common functions 1219");
										PublicationNode pNode;
										pNode.setPubId(productID);

										PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
										pNode.setPBObjectID(strPBObjectID.GetAsDouble());
										if(indexValueAttachedToTable == "4")
										{
											
											pNode.setIsProduct(0);
										}
										else if(indexValueAttachedToTable == "3")
										{
											pNode.setIsProduct(1);
										}
										if(rowno != -1)
										{
											//CA("sETTING IsONEsource flag as false");
											pNode.setIsONEsource(kFalse);
										}
										TagStruct tagInfo;
										tagInfo.elementId = elementID;
										tagInfo.languageID = languageID;
										tagInfo.typeId = typeID;
										tagInfo.sectionID = sectionID;
										tagInfo.tagPtr = cellTextXMLElementPtr;
										//added by Tushar on 26/12/06
										tagInfo.header = header;
										tagInfo.childId = childID;
										tagInfo.childTag = childTag;
										tagInfo.parentTypeID = parentTypeID;										
										handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
									}
									else
										dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childID,elementID,languageID, sectionID, kTrue);
								}
								
							}
							else if(tableFlag == "1")
							{//the cell contains the table
								if(strIndex == "3")
								{
									if(strElementID == "-103")
									{//Print the table header..table type
										do
										{
											//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
											//int32 typeId = strTypeID.GetAsNumber();
											
											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
											if(typeValObj==nil)
											{
												ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
												break;
											}

											VectorTypeInfoValue::iterator it1;

											bool16 typeIDFound = kFalse;
											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
											{	
													if(typeID == it1->getTypeId())
													{
														typeIDFound = kTrue;
														break;
													}			
											}
											if(typeIDFound)
												dispName = it1->getName();
											if(typeValObj)
												delete typeValObj;
										}while(kFalse); 
									}
									else if(strElementID == "-104")
									{//Print the entire table
										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
										TagStruct tagInfo;												
										tagInfo.whichTab = strIndex.GetAsNumber();
										tagInfo.parentId = strParentID.GetAsDouble();
										tagInfo.isTablePresent = kTrue;
										tagInfo.typeId = typeID;
										tagInfo.sectionID = strSectionID.GetAsDouble();
										GetItemTableInTabbedTextForm(tagInfo,dispName);
									}
								}
								else if(strIndex == "4")
								{
									if(strElementID == "-103")
									{
										do
										{
											VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
											if(typeValObj==nil)
											{
												ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
												break;
											}
											VectorTypeInfoValue::iterator it1;
											bool16 typeIDFound = kFalse;
											for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
											{	
													if(typeID == it1->getTypeId())
													{
														typeIDFound = kTrue;
														break;
													}			
											}
											if(typeIDFound)
												dispName = it1->getName();
											if(typeValObj)
												delete typeValObj;
										}while(kFalse);
									}
									else if(strElementID == "-104")
									{//Print the entire table
										//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
										//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
										//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
										TagStruct tagInfo;												
										tagInfo.whichTab = strIndex.GetAsNumber();
										//This is just a temporary adjustment
										PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
										tagInfo.parentId = pbObjectId.GetAsDouble();

										tagInfo.isTablePresent = kTrue;
										tagInfo.typeId = typeID;
										tagInfo.sectionID = strSectionID.GetAsDouble();
										GetItemTableInTabbedTextForm(tagInfo,dispName);
									}
								}
							}
							else if(tableFlag == "-13")
							{//the cell contains the product copy attribute header
									CElementModel cElementModelObj;
									bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
									if(result)
									dispName = cElementModelObj.getDisplayName();
							}							
							PMString textToInsert("");
							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								textToInsert=dispName;					
							}
							else
								textToInsert=iConverter->translateString(dispName);
							//Spray the Header data .We don't have to change the tag attributes
							//as we have done it while copy and paste.
							//WideString insertData(textToInsert);
							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);
						}
					}//end for tagIndex = 0 


					break; // Break after refreshing table with only Pub Item itself.
				}
			}
			
			
			int32 TotalNoOfItems = (int32)FinalItemIds.size();
			if(TotalNoOfItems <= 0)
				break;	//breaks the for..tableIndex
		
			int32 totalNumberOfColumns = tableModel->GetTotalCols().count;

			int32 totalNumberOfBodyRowsBeforeRefresh = 0;
			if(HeaderRowPresent)
				totalNumberOfBodyRowsBeforeRefresh =  tableModel->GetBodyRows().count;
			else
				totalNumberOfBodyRowsBeforeRefresh =  tableModel->GetTotalRows().count;
			

			GridArea gridAreaForEntireTable;
			//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
			if(HeaderRowPresent)
			{	//CA("fill gridAreaForEntireTable");			 
				gridAreaForEntireTable.topRow = headerCount;
				gridAreaForEntireTable.leftCol = 0;
				gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern+headerCount;
                gridAreaForEntireTable.rightCol = totalNumberOfColumns;
				//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
			}
			else
			{
				if(AddHeaderToTable)
				{
					gridAreaForEntireTable.topRow = RowCountForRepetaionPattern;
					gridAreaForEntireTable.leftCol = 0;
					gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern + RowCountForRepetaionPattern;
					gridAreaForEntireTable.rightCol = totalNumberOfColumns;
				}
				else
				{
					gridAreaForEntireTable.topRow = 0;
					gridAreaForEntireTable.leftCol = 0;
					gridAreaForEntireTable.bottomRow = RowCountForRepetaionPattern;
					gridAreaForEntireTable.rightCol = totalNumberOfColumns;
					//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
				}
			}
			
			int32 TotalNoOfBodyRowsWithOutHeaders = totalNumberOfBodyRowsBeforeRefresh;
			if(AddHeaderToTable && (!HeaderRowPresent)) // in case of Header Elements in Body rows ie. No Header(*IndesignTable) rows are present in Table  
				TotalNoOfBodyRowsWithOutHeaders = TotalNoOfBodyRowsWithOutHeaders - RowCountForRepetaionPattern;

			// Now check for available Body rows and required body rows.			

			int32 RequiredNoOfBodyRowsForRefreah = (TotalNoOfItems * RowCountForRepetaionPattern );

			if((RequiredNoOfBodyRowsForRefreah ) == (TotalNoOfBodyRowsWithOutHeaders))
			{
				//CA(" No of items before & after Refresh are same ie. no change in item counts");
			}
			else if((RequiredNoOfBodyRowsForRefreah ) > (TotalNoOfBodyRowsWithOutHeaders))
			{
				int32 NoOfRowsToBeAdded = RequiredNoOfBodyRowsForRefreah - TotalNoOfBodyRowsWithOutHeaders;
				int32 rowPatternRepeatCount = (NoOfRowsToBeAdded / RowCountForRepetaionPattern);
				if(HeaderRowPresent)
				{
					result = tableCommands->InsertRows(RowRange(totalNumberOfBodyRowsBeforeRefresh+ headerCount -1, NoOfRowsToBeAdded), Tables::eAfter, 0);
					
					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						//CA("canCopy == kFalse");
						ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
						break;
					}
					//CA("canCopy");
					
					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
						break;
					}
					
					for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh+headerCount),0),ITableModel::eAll,tempPtr);
						tempPtr = tableModel->Copy(gridAreaForEntireTable);
					}
				}
				else
				{					
					result = tableCommands->InsertRows(RowRange(totalNumberOfBodyRowsBeforeRefresh -1, NoOfRowsToBeAdded), Tables::eAfter, 0);

					bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
					if(canCopy == kFalse)
					{
						//CA("canCopy == kFalse");
						ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
						break;
					}
					//CA("canCopy");					
					TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
					if(tempPtr == NULL)
					{
						ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
						break;
					}
					
					for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
					{
						/*PMString ASD("GridAddress : ");
						ASD.AppendNumber((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh));
						ASD.Append("  totalNumberOfBodyRowsBeforeRefresh : ");
						ASD.AppendNumber(totalNumberOfBodyRowsBeforeRefresh);
						ASD.Append("  RowCountForRepetaionPattern : ");
						ASD.AppendNumber(RowCountForRepetaionPattern);
						ASD.Append("  pasteIndex : ");
						ASD.AppendNumber(pasteIndex);
						CA(ASD);*/
						tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh),0),ITableModel::eAll,tempPtr);
						tempPtr = tableModel->Copy(gridAreaForEntireTable);
					}
				}

			}
			else if((RequiredNoOfBodyRowsForRefreah ) < (TotalNoOfBodyRowsWithOutHeaders))
			{
				int32 NoOfRowsToBeDeleted = TotalNoOfBodyRowsWithOutHeaders - RequiredNoOfBodyRowsForRefreah;
				
				if(HeaderRowPresent)
				{
					result = tableCommands->DeleteRows(RowRange(RequiredNoOfBodyRowsForRefreah+ headerCount -1 , NoOfRowsToBeDeleted));
				}
				else
				{
					result = tableCommands->DeleteRows(RowRange(RequiredNoOfBodyRowsForRefreah - 1 , NoOfRowsToBeDeleted));
				}
			}

			
			int32 TotalNoOFRowsBeforeRefresh =  tableModel->GetTotalRows().count;
			
			int32 StartCountForBodyRow = 0;
			if(HeaderRowPresent)
				StartCountForBodyRow = headerCount;
			else
			{
				if(AddHeaderToTable)
					StartCountForBodyRow = RowCountForRepetaionPattern;
				else
					StartCountForBodyRow =0;
			}
			
			// find out start TagIndex of first Boby row cell.
			int32 firstBodyCellTagIndex = 0;
            int32 childCellCount = tableXMLElementPtr->GetChildCount();
			for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
			{
				XMLReference cellXMLElementRef = tableXMLElementPtr->GetNthChild(tagIndex);
				InterfacePtr<IIDXMLElement> cellXMLElementPtr( cellXMLElementRef.Instantiate());
				//We have considered that there is ONE and ONLY ONE text tag inside a cell.
				//Also note that, cell may be blank i.e doesn't contain any textTag.
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					continue;
				}
				bool16 flagFound = kFalse;
				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
				{
					XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
					InterfacePtr<IIDXMLElement> cellTextXMLElementPtr( cellTextXMLElementRef.Instantiate());
					
					//Check if the cell is blank.
					if(cellTextXMLElementPtr == NULL)
					{
						continue;
					}
					PMString rowNoStr = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
					PMString colNoStr = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));

					int32 rowNo = rowNoStr.GetAsNumber();
					int32 colno = colNoStr.GetAsNumber();

					if(rowNo == (totalheadersofcustumtable ) && colno== 0 )
					{
						firstBodyCellTagIndex =tagIndex;
						flagFound = kTrue;
						break;
					}
				}
				if (flagFound)
					break;

			}

			for(int32 p=0; p < TotalNoOfItems ; p++)
			{				
				for(int32 nonHeaderRowIndex =0; nonHeaderRowIndex < RowCountForRepetaionPattern; nonHeaderRowIndex++)
				{
					for(int32 columnIndex = 0;columnIndex <totalNumberOfColumns; columnIndex++)
					{//for..iterate through each column
						//int32 tagIndex = (StartCountForBodyRow* totalNumberOfColumns ) + (p * RowCountForRepetaionPattern * totalNumberOfColumns ) + (nonHeaderRowIndex * totalNumberOfColumns) +columnIndex;
						int32 tagIndex = firstBodyCellTagIndex + (p * RowCountForRepetaionPattern * totalNumberOfColumns ) + (nonHeaderRowIndex * totalNumberOfColumns) +columnIndex;
						
						
						/*PMString ASD("tagIndex : ");
						ASD.AppendNumber(tagIndex);
						CA(ASD);*/
	
						if(tagIndex >= tableXMLElementPtr->GetChildCount())
							continue;

						XMLReference cellXMLElementRef = tableXMLElementPtr->GetNthChild(tagIndex);
						InterfacePtr<IIDXMLElement> cellXMLElementPtr( cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							continue;
						}
						
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
							InterfacePtr<IIDXMLElement> cellTextXMLElementPtr( cellTextXMLElementRef.Instantiate());
					
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								continue;
							}
							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));

							if(index == "3")
								continue;
							else if(index == "4")
							{
								PMString ItemID("");
								ItemID.AppendNumber(PMReal(FinalItemIds.at(p)));
								//cellTextXMLElementPtr->SetAttributeValue(WideString("childId"), WideString(ItemID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"), WideString(ItemID));
							}
						}
					}
				}
					

			}
					
//Now refreshing Individual Cell
				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
				{//start for tagIndex = 0
				
					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					InterfacePtr<IIDXMLElement> cellXMLElementPtr ( cellXMLElementRef.Instantiate());
					
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}

					for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
					{ // iterate thruogh cell's tags
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
						InterfacePtr<IIDXMLElement> cellTextXMLElementPtr( cellTextXMLRef.Instantiate());
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == nil)
						{
							continue;
						}
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
						PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
						PMString strheader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						PMString strchildId = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
						PMString strDataType = cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));
						PMString strTableId = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));

						//added by Tushar on 26/12/06
						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 		double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();
						int32 header = strheader.GetAsNumber();
						double childId = strchildId.GetAsDouble();
						int32 childTag = strChildTag.GetAsNumber();
						int32 sequenceNo = strTypeID.GetAsNumber();
						double typeID = sequenceNo;
						int32 dataType = strDataType.GetAsNumber();
						double tableId = strTableId.GetAsDouble();

						//if(sequenceNo != -2)  // for Header Elements
						//	typeID	= FinalItemIds[sequenceNo];
						/*if(header == 1)
							typeID	= FinalItemIds[0];*/
						

						PMString NewTypeId("");
						NewTypeId.AppendNumber(PMReal(typeID));
						//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"), WideString(NewTypeId));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("typeId"), WideString(NewTypeId));

						int32 rowno	=	strRowNo.GetAsDouble();
						//added by Tushar on 26/12/06
						double parentTypeID	= strParentTypeID.GetAsDouble();
						
						/*CA("strElementID :" + strElementID);
						CA("strLanguageID :" + strLanguageID);
						CA("strParentTypeID :" + strParentTypeID);*/

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						//We will come across three types of tags.
						//1. ProductCopyAttributes tableFlag == 0
						//for all other attributes tableFlag == 1
						//2. TableHeader..typeId == -2
						//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
						PMString dispName("");
						if(tableFlag == "-15")
						{//This is a impotent tag.Don't process this.	
							//CA("tableFlag == -15");
							continue;
						}
						else if(tableFlag == "0")
						{	//CA("tableFlag == 0");
							//The cell contains only copy attributes
							//added on 25Sept..EventPrice addition.

							//ended on 25Sept..EventPrice addtion.
							if(strIndex == "3")
							{//Product copy attribute
								// Handling of List Name , Discription, Notes1 -5 for ItemGroup 

								if(dataType == 4 ||dataType == 5)
								{
									for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
									{				
										oTableValue = *it;	
										if((dataType == 4 || elementID == -121 ) && tableId== oTableValue.getTableID())
										{
											dispName = oTableValue.getName();
											//CA("tempBuffer01 = " + tempBuffer);
											break;
										}
										else if(dataType == 5) 
										{
											if(elementID == -983 && tableId == oTableValue.getTableID())//---List Description
											{
												//CA("tagInfo.elementId == -983");
												dispName = oTableValue.getDescription();	
											}
											else if(elementID == -982 && tableId == oTableValue.getTableID())//---Stencil Name
											{
												dispName = oTableValue.getstencil_name();
											}
											else if(elementID == -980 && tableId == oTableValue.getTableID())//---for Note_1 
											{
												//CA("for Note_1 ");
												dispName = oTableValue.getNotesList().at(0);	
											}
											else if(elementID == -979 && tableId == oTableValue.getTableID())//---for Note_2 
											{
												dispName = oTableValue.getNotesList().at(1);		
											}
											else if(slugInfo.elementId == -978 && tableId == oTableValue.getTableID())//---for Note_3
											{
												dispName = oTableValue.getNotesList().at(2);		
											}
											else if(elementID == -977 && tableId == oTableValue.getTableID())//---for Note_4
											{
												dispName = oTableValue.getNotesList().at(3);		
											}
											else if(elementID == -976 && tableId == oTableValue.getTableID())//---for Note_5 
											{
												dispName = oTableValue.getNotesList().at(4);	
											}
										}
									}
								}
								else
								{
									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionID, kFalse);
								}
							}
							else if(strIndex == "4" && /*typeID == -2*/ header == 1)
							{//	CA("strIndex == 4 && typeID == -2");
								//Item copy attribue header
								//following code added by Tushar on 27/12/06
								if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
								{
									//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
									//if(TypeInfoVectorPtr != NULL)
									//{//CA("TypeInfoVectorPtr != NULL");
									//	VectorTypeInfoValue::iterator it3;
									//	int32 Type_id = -1;
									//	PMString temp = "";
									//	PMString name = "";
									//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									//	{
									//		Type_id = it3->getTypeId();
									//		if(parentTypeID == Type_id)
									//		{
									//			temp = it3->getName();
									//			if(elementID == -701)
									//			{
									//				dispName = temp;
									//			}
									//			else if(elementID == -702)
									//			{
									//				dispName = temp + " Suffix";
									//			}
									//		}
									//	}
									//}
									//if(TypeInfoVectorPtr)
									//	delete TypeInfoVectorPtr;
								}
								else if(elementID == -703)
								{
									//CA("dispName = $Off");
									dispName = "$Off";
								}
								else if(elementID == -704)
								{
									//CA("dispName = %Off");
									dispName = "%Off";
								}
								else if(elementID == -805)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -806)
								{									
									dispName = "Availablity";
                                }
								else if(elementID == -807)
								{									
									dispName = "Cross-reference Type";
                                }
								else if(elementID == -808)
								{									
									dispName = "Rating";
                                }
								else if(elementID == -809)
								{									
									dispName = "Alternate";
                                }
								else if(elementID == -810)
								{									
									dispName = "Comments";
                                }
								else if(elementID == -811)
								{									
									dispName = "";
                                }
								else if(elementID == -812)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -813)
								{									
									dispName = "Required";
                                }
								else if(elementID == -814)
								{									
									dispName = "Comments";
                                }
								else
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
							}
							else if(strIndex == "4")
							{//CA("strIndex == 4");
								//Item copy attribute 
								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
								{//CA("common functions 1219");
									PublicationNode pNode;
									pNode.setPubId(productID);

									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
									pNode.setPBObjectID(strPBObjectID.GetAsDouble());
									if(indexValueAttachedToTable == "4")
									{
										
										pNode.setIsProduct(0);
									}
									else if(indexValueAttachedToTable == "3")
									{
										pNode.setIsProduct(1);
									}
									if(rowno != -1)
									{
										//CA("sETTING IsONEsource flag as false");
										pNode.setIsONEsource(kFalse);
									}
									TagStruct tagInfo;
									tagInfo.elementId = elementID;
									tagInfo.languageID = languageID;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = sectionID;
									tagInfo.tagPtr = cellTextXMLElementPtr;
									tagInfo.header = header;
									tagInfo.childId = childId;
									tagInfo.childTag = childTag;
									//added by Tushar on 26/12/06
									tagInfo.parentTypeID = parentTypeID;
									
									handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
								}
								
								else if(elementID == -803)  // For 'Letter Key'
								{									
									dispName.Clear();
									int wholeNo =  sequenceNo / 26;
									int remainder = sequenceNo % 26;								

									if(wholeNo == 0)
									{// will print 'a' or 'b'... 'z'  upto 26 item ids
										dispName.Append(AlphabetArray[remainder]);
									}
									else  if(wholeNo <= 26)
									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
										dispName.Append(AlphabetArray[wholeNo-1]);	
										dispName.Append(AlphabetArray[remainder]);	
									}
									else if(wholeNo <= 52)
									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
										dispName.Append(AlphabetArray[0]);
										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
										dispName.Append(AlphabetArray[remainder]);										
									}
								}
								else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
								{
									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
								{
									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -807) && (isComponentAttributePresent == 2))
								{
									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
                                }
								else if((elementID == -808) && (isComponentAttributePresent == 2))
								{
									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
                                }
								else if((elementID == -809) && (isComponentAttributePresent == 2))
								{
									dispName = (Kit_vec_tablerows[sequenceNo][2]);	
                                }
								else if((elementID == -810) && (isComponentAttributePresent == 2))
								{
									dispName = (Kit_vec_tablerows[sequenceNo][3]);	
                                }
								else if((elementID == -811) && (isComponentAttributePresent == 2))
								{
									dispName = (Kit_vec_tablerows[sequenceNo][4]);	
                                }
								else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
								{
									dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
								{
									dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
								{
									dispName = (Kit_vec_tablerows[sequenceNo][2]);	
								}
								// Handling of List Name , Discription, Notes1 -5 for Item
								if(dataType == 4 ||dataType == 5)
								{
									for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
									{				
										oTableValue = *it;	

										if((dataType == 4 || elementID == -121 ) && tableId== oTableValue.getTableID())
										{
											dispName = oTableValue.getName();
											//CA("tempBuffer01 = " + tempBuffer);
											break;
										}
										else if(dataType == 5) 
										{
											if(elementID == -983 && tableId == oTableValue.getTableID())//---List Description
											{
												//CA("tagInfo.elementId == -983");
												dispName = oTableValue.getDescription();	
											}
											else if(elementID == -982 && tableId == oTableValue.getTableID())//---Stencil Name
											{
												dispName = oTableValue.getstencil_name();
											}
											else if(elementID == -980 && tableId == oTableValue.getTableID())//---for Note_1 
											{
												//CA("for Note_1 ");
												dispName = oTableValue.getNotesList().at(0);	
											}
											else if(elementID == -979 && tableId == oTableValue.getTableID())//---for Note_2 
											{
												dispName = oTableValue.getNotesList().at(1);		
											}
											else if(slugInfo.elementId == -978 && tableId == oTableValue.getTableID())//---for Note_3
											{
												dispName = oTableValue.getNotesList().at(2);		
											}
											else if(elementID == -977 && tableId == oTableValue.getTableID())//---for Note_4
											{
												dispName = oTableValue.getNotesList().at(3);		
											}
											else if(elementID == -976 && tableId == oTableValue.getTableID())//---for Note_5 
											{
												dispName = oTableValue.getNotesList().at(4);	
											}
										}
									}
								}
								else
								{
									if((isComponentAttributePresent == 2) && ((Kit_vec_tablerows[sequenceNo][4]) == "N"))
									{
										//dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(typeID,elementID,languageID);
									}
									else
									 dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID, sectionID, kTrue);
								}
							}
							
						}
						else if(tableFlag == "1")
						{//the cell contains the table
							if(strIndex == "3")
							{
								if(strElementID == "-103")
								{//Print the table header..table type
									do
									{
										//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
	//CA(strTypeID);					//int32 typeId = strTypeID.GetAsNumber();
										
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}

										VectorTypeInfoValue::iterator it1;

										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
											if(typeID == it1->getTypeId())
											{
												typeIDFound = kTrue;
												break;
											}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse); 
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									tagInfo.parentId = strParentID.GetAsDouble();
									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);
								}

							}
							else if(strIndex == "4")
							{
								if(strElementID == "-103")
								{
									do
									{
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}
										VectorTypeInfoValue::iterator it1;
										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse);
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									//This is just a temporary adjustment
									PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
									tagInfo.parentId = pbObjectId.GetAsDouble();

									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);

								}

							}
						}
						else if(tableFlag == "-13")
						{//the cell contains the product copy attribute header
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
								if(result)
								dispName = cElementModelObj.getDisplayName();
						}
						
						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=dispName;					
						}
						else
							textToInsert=iConverter->translateString(dispName);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
						//WideString insertData(textToInsert);					
						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);
					}
				}//end for tagIndex = 0 

	   }//end for..tableIndex
	   errcode = kSuccess;
	}while(0);
	return errcode;     
}


void makeTheTagImpotent(IIDXMLElement * xmlPtr)
{
	//This is a special case.The tag is of product but the Item is
	//selected.Or the tag is of Item and the product is selected.
	//So make this tag unproductive for refresh.(Impotent)		
	do
	{
		int32 tagStart = -1;
		int32 tagEnd = -1;
		Utils<IXMLUtils>()->GetElementIndices(xmlPtr,&tagStart,&tagEnd);
		tagStart = tagStart +1;
		tagEnd = tagEnd - 1;

		InterfacePtr<ITextStoryThread> textStoryThread(xmlPtr->QueryContentTextStoryThread()/*,UseDefaultIID()*/);
		if(textStoryThread == NULL)
		{
			//CA("textStoryThread == NULL");
			break;
		}
		InterfacePtr<ITextModel> textModel(textStoryThread->QueryTextModel()/*,UseDefaultIID()*/);
		if(textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}

		PMString str("");
		//WideString data(str);
		//textModel->Replace(tagStart,tagEnd-tagStart + 1,&data);

		boost::shared_ptr<WideString> insertText(new WideString(str));
		ReplaceText(textModel,tagStart,tagEnd-tagStart + 1 ,insertText);

		//change the XMLTagAttribute values also
		/*PMString attributeValue;
		attributeValue.AppendNumber(pNode.getPubId());
		xmlPtr->SetAttributeValue("parentID",attributeValue);

		attributeValue.Clear();
		attributeValue.AppendNumber(pNode.getTypeId());
		xmlPtr->SetAttributeValue("parentTypeID",attributeValue);

		int32 sectionID = -1;
		if(global_project_level == 3)
			sectionID = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionID = CurrentSectionID;

		attributeValue.Clear();
		attributeValue.AppendNumber(sectionID);
		xmlPtr->SetAttributeValue("sectionID",attributeValue);*/

		//The tableFlag = -15 to indicate that this is impotent tag.
		//xmlPtr->SetAttributeValue(WideString("tableFlag"),WideString("-15")); //Cs4
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlPtr->GetXMLReference(), WideString("tableFlag"),WideString("-15"));
	}while(kFalse);
}


void makeTagInsideCellImpotent(IIDXMLElement * tableXMLElementPtr)
{
	//This is a special case.The tag is of product but the Item is
	//selected.Or the tag is of Item and the product is selected.
	//So make this tag unproductive for refresh.(Impotent)		
	do
	{
		for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
		{
			XMLReference cellXMLElementRef = tableXMLElementPtr->GetNthChild(tagIndex);
			//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
			for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
			{
				XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
				//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
				InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
				makeTheTagImpotent(cellTextTagPtr);
			}
		}
        }while(kFalse);
}

ErrorCode RefreshCustomTabbedTextNewoption(const UIDRef& boxID,TagStruct& tagInfo)
{
	//CA("RefreshCustomTabbedTextNewoption");

	
	
	ErrorCode result = kFailure;
	do
	{
		InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
			//return ;
		}
		vector<double> FinalItemIds;
		VectorChildID.clear();
		FinalItemIds.clear();
		itemnew.clear();
		itemIID.clear();

		TagList tList; //,tList_checkForHybrid;
	    tList =  itagReader->getTagsFromBox(boxID);
		for(int32 t=0;t<tList.size();t++)
		{
			if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3))
			{
				int32 childTagCount = tList[t].tagPtr->GetChildCount();
				PMString a("childTagCount :");
				a.AppendNumber(childTagCount);
				//CA(a);
				
				for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
				{
				    elementiid.clear();
				    itemIID.clear();
					XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
					//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
					if(childTagXMLElementPtr == nil)
					continue;
			        PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
					double childId22 = strchildId22.GetAsDouble();
					
					if(childId22!=-1)
					{
						PMString a("First ID :");
						a.AppendNumber(PMReal(childId22));
						//CA(a);
						FinalItemIds.push_back(childId22);
					}
					
					PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
					double ID22 = strID22.GetAsDouble();	
					if(childId22!=-1)
					itemnew.insert(map<double,double>::value_type(childId22,ID22));

				   int32 elementCount=childTagXMLElementPtr->GetChildCount();
				   PMString k("2nd :");
					k.AppendNumber(elementCount);
					//CA(k);
				   
						for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
						{
						     XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
						     InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
							 if(childElement == nil)
								continue;
								PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
								double childId11 = strchildId11.GetAsDouble();
								
								if(childId11!=-1)
								{
									PMString a("Second ID :");
									a.AppendNumber(childId11);
									//CA(a);
									FinalItemIds.push_back(childId11);
								}
								
								PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
								double ID11 = strID11.GetAsDouble();	
								if(childId11!=-1)
								itemnew.insert(map<double,double>::value_type(childId11,ID11));

							int32 elementcount1=childElement->GetChildCount();
							PMString j("3rd :");
						    j.AppendNumber(elementcount1);
						   // CA(j);
							for(int32 count1=0;count1<elementcount1;count1++)
							{
								TagStruct tagInfo;
								XMLReference elementXMLref123 = childElement->GetNthChild(count1);
						        InterfacePtr<IIDXMLElement>childElement123(elementXMLref123.Instantiate());
							    if(childElement123 == nil)
								  continue;
								PMString strchildId = childElement123->GetAttributeValue(WideString("childId"));//Cs4
								double childId = strchildId.GetAsDouble();
								if(childId!=-1)
								{
									PMString a("Third ID :");
									a.AppendNumber(childId);
									//CA(a);
									FinalItemIds.push_back(childId);
								}
								
								PMString strID = childElement123->GetAttributeValue(WideString("ID"));//CS4
								double ID = strID.GetAsDouble();	
								if(childId!=-1)
								itemnew.insert(map<double,double>::value_type(childId,ID));

								int32 elementcount2= childElement123->GetChildCount();
								PMString h("4h :");
								h.AppendNumber(elementcount2);
								//CA(h);
								for(int32 count2=0;count2<elementcount2;count2++)
								{

									TagStruct tagInfo;
									XMLReference elementXMLref321 = childElement123->GetNthChild(count2);
									InterfacePtr<IIDXMLElement>childElement321(elementXMLref321.Instantiate());
									if(childElement123 == nil)
									  continue;
									PMString strchildId44 = childElement321->GetAttributeValue(WideString("childId"));//Cs4
									double childId44 = strchildId44.GetAsDouble();
									if(childId44!=-1)
									{
										PMString a("Four ID :");
										a.AppendNumber(childId44);
										//CA(a);
										FinalItemIds.push_back(childId44);
									}
									
									PMString strID44 = childElement321->GetAttributeValue(WideString("ID"));//CS4
									double ID44 = strID44.GetAsDouble();	
									if(childId44!=-1)
									itemnew.insert(map<double,double>::value_type(childId44,ID44));
								
								}
							}
						}
	            }
             }
          }
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		
		bool16 FinalItemIdSizeIsZero = kFalse;

		IIDXMLElement *customTabbedTextTagPtr = tagInfo.tagPtr;

		PMString strParentID =  customTabbedTextTagPtr->GetAttributeValue(WideString("parentID")); //Cs4
		PMString strLanguageID =  customTabbedTextTagPtr->GetAttributeValue(WideString("LanguageID")); //Cs4
		PMString strSectionID = customTabbedTextTagPtr->GetAttributeValue(WideString("sectionID"));//Cs4
		
		//-------------
		int32 field1 = -1;
		bool16 IsPivotList=kFalse;
		bool16 flag =kFalse;
		for(int32 t=0;t<tList.size();t++)
		{
			if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3) )
			{
				int32 childTagCount = tList[t].tagPtr->GetChildCount();
				PMString l("1st :");
				l.AppendNumber(childTagCount);
				//CA(l);


				VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr1 = NULL;
				VectorHtmlTrackerValue vectorObj1 ;
				vectorHtmlTrackerSPTBPtr1 = &vectorObj1;


				for(int32 tagIndex = 0;tagIndex < childTagCount;tagIndex++)
				{
					XMLReference cellXMLElementRef = tList[t].tagPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil )
					{
						//CA("cellXMLElementPtr==nil ");
						continue;
					}
					PMString strfield1 = cellXMLElementPtr->GetAttributeValue(WideString("field1")); 
					field1 =  strfield1.GetAsNumber();

					PMString strTypeID = cellXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
					PMString strSectionID = cellXMLElementPtr->GetAttributeValue(WideString("sectionID"));
					PMString strElementID = cellXMLElementPtr->GetAttributeValue(WideString("ID"));
					PMString strLanguageID = cellXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
					PMString strParentTypeID = cellXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
					PMString strIndex = cellXMLElementPtr->GetAttributeValue(WideString("index"));
					//PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

					PMString strHeader = cellXMLElementPtr->GetAttributeValue(WideString("header"));
					PMString strchildId = cellXMLElementPtr->GetAttributeValue(WideString("childId"));
					PMString strchildTag = cellXMLElementPtr->GetAttributeValue(WideString("childTag"));
					PMString strProductID =  cellXMLElementPtr->GetAttributeValue(WideString("parentID"));

					int32 header = strHeader.GetAsNumber();
					double childId = strchildId.GetAsDouble();
					int32 childTag = strchildTag.GetAsNumber();
					double parentID = strProductID.GetAsDouble();

					double typeId = strTypeID.GetAsDouble();
					double sectionID = strSectionID.GetAsDouble();
					double elementID = strElementID.GetAsDouble();
					double languageID = strLanguageID.GetAsDouble();
					double parentTypeID = strParentTypeID.GetAsDouble();
					int32 index = strIndex.GetAsNumber();
					PMString ch2("childId : ");
					ch2.AppendNumber(childId);
					ch2.Append("\n");
					ch2.Append("parentId : ");
					ch2.AppendNumber(parentID);
					ch2.Append("\n");
					ch2.Append("Header : ");
					ch2.AppendNumber(header);
					//CA(ch2);
					//============
					bool16 flag =kFalse;
					int32 size1 = (int32)VectorChildID.size();
						/*ChildIDvec.push_back(childId);*/
						
						
						if(size1>0)
						{
							for(int32 i=0;i<size1;i++)
							{
								if(VectorChildID.at(i)==childId)
									flag=kTrue;
							}
						}
						if(flag==kFalse && childId!=-1)
							VectorChildID.push_back(childId);

						//===============================

					PMString itemAttributeValue("");
					if(typeId==-57)
					{
						IsPivotList=kTrue;
						itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId,elementID, languageID, sectionID, /*kTrue*/kFalse );//23OCT09//Amit

						PMString ch2("childId : ");
						ch2.AppendNumber(childId);
						ch2.Append("\n");
						ch2.Append("elementID : ");
						ch2.AppendNumber(elementID);
						ch2.Append("\n");
						ch2.Append("string : ");
						ch2.Append(itemAttributeValue);
						//CA(ch2);
					}

					int32 tagStartPos = -1;
				int32 tagEndPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(cellXMLElementPtr,&tagStartPos,&tagEndPos);

				InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElementPtr->QueryContentTextStoryThread());
				if(textStoryThread == NULL)
				{
					//CA("textStoryThread == NULL");
					break;
				}
				InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
				if(textModelptr == NULL)
				{
					//CA("textModel == NULL");
					break;
				}

				tagStartPos = tagStartPos + 1;
				tagEndPos = tagEndPos -1;

				PMString textToInsert("");
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(!iConverter)
				{
					//CA("24");
					textToInsert=itemAttributeValue;					
				}
				else						
				{
					//CA("25");
					//textToInsert=iConverter->translateString(itemAttributeValue);
					vectorHtmlTrackerSPTBPtr1->clear();
					textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr1);
					iConverter->ChangeQutationMarkONOFFState(kFalse);
				}	
				/*WideString insertText(textToInsert);
				textModelptr->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);*/
                textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
				ReplaceText(textModelptr,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);
				if(iConverter)
				{
					//CA("26");
					iConverter->ChangeQutationMarkONOFFState(kTrue);			
				}
				}
			}
		}



		/*for(int32 i=0;i<VectorChildID.size();i++)
		{
			PMString z("IG ID :");
			z.AppendNumber(VectorChildID.at(i));
			CA(z);
		}*/
		if(IsPivotList==kFalse)
		{
			double itemID_or_productID = strParentID.GetAsDouble();
			double languageId = strLanguageID.GetAsDouble();
			double sectionID = strSectionID.GetAsDouble();

			bool16 isSectionLevelItemItemPresent = kFalse;
			int32 isComponentAttributePresent = 0;
			vector<vector<PMString> > Kit_vec_tablerows;  //To store Component's 'tableData' so that we can use
		
	////get all itemIDs of section level item or product.............

		do{
			//CA("bb");
			if(customTabbedTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4"))//Cs4
			{
				//CA("cc");
				//CA("customTabbedTextTagPtr->GetAttributeValue(index) == 4 ");
				if(customTabbedTextTagPtr->GetAttributeValue(/*WideString("colno")) == WideString("-101")*/WideString("childTag")) == WideString("1") && customTabbedTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
				{
					//CA("dd");
					isSectionLevelItemItemPresent = kTrue;
					//CA("SectionLevelItemItemPresent == kTrue");
					if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-901"))
						isComponentAttributePresent = 1;
					else if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-902"))
						isComponentAttributePresent = 2;
					else if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-903"))
						isComponentAttributePresent = 3;
				}

				if(isSectionLevelItemItemPresent == kTrue)
				{
					//CA("ee");
					//CA("isSectionLevelItemItemPresent == kTrue");			
					/////get pub_object_id
					
					//CPbObjectValue* ptrCPbObjectValue =ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(tagInfo.sectionID,tagInfo.parentId);
					//int32 pub_object_id = ptrCPbObjectValue->getPub_object_id();
					//PMString name = ptrCPbObjectValue->getName();
	//CA("Name::"+name);
					
					PMString pub_object_idStr = customTabbedTextTagPtr->GetAttributeValue(WideString("pbObjectId"));
					double pub_object_id = pub_object_idStr.GetAsDouble();

					if(isComponentAttributePresent == 0)
					{
						//for(int32 i=0;i<FinalItemIds.size();i++)
						//{
						//	PMString s("CHILDID :");
						//	s.AppendNumber(FinalItemIds.at(i));
						//	//CA(s);
						//
						//}
						//map<int32,int32>::iterator itrn;
						//for(itrn=itemnew.begin();itrn!=itemnew.end();itrn++)
						//{
						//	int32 itemidd=itrn->first;
						//	FinalItemIds.push_back(itemidd);

						//	PMString s("CHILDID :");
						//	s.AppendNumber(itemidd);
						//	//CA(s);
						//}


						//CA("ff");
						//CA("isComponentAttributePresent == 0");
						
						//VectorScreenTableInfoPtr tableInfo = 
						//	ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pub_object_id);
						//
						//if(!tableInfo)
						//{
						//	//CA("gg");
						//	ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshCustomTabbedText::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
						//	break;
						//}

						//if(tableInfo->size()==0)
						//{ 
						//	//CA("hh");
						//	FinalItemIdSizeIsZero = kTrue;
						//	ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText: table size = 0");
						//	break;
						//}

						//VectorScreenTableInfoValue ::iterator it;
						//CItemTableValue oTableValue;

						//bool16 typeidFound=kFalse;
						//vector<int32> vec_items;
						//FinalItemIds.clear();

						//PMString tableIDStr = customTabbedTextTagPtr->GetAttributeValue(WideString("tableId"));
						//int32 tableID = tableIDStr.GetAsNumber();


						//for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						//{//for tabelInfo start
						//	//CA("ii");
						//	oTableValue = *it;				
						//	if(tableID > 0)
						//		if(tableID != oTableValue.getTableID())
						//			continue;

						//	vec_items = oTableValue.getItemIds();
						//	
						//	if( field1 == -1 )	//------
						//	{
						//		//CA("field1 == -1");
						//		if(FinalItemIds.size() == 0)
						//		{
						//			//CA("jj");
						//			FinalItemIds = vec_items;
						//		}
						//		else
						//		{
						//			//CA("kk");
						//			for(int32 i=0; i<vec_items.size(); i++)
						//			{	bool16 Flag = kFalse;

						//				for(int32 j=0; j<FinalItemIds.size(); j++)
						//				{
						//					if(vec_items[i] == FinalItemIds[j])
						//					{
						//						Flag = kTrue;
						//						break;
						//					}				
						//				}
						//				if(!Flag)
						//					FinalItemIds.push_back(vec_items[i]);
						//			}
						//		 }
						//	}
						//	else
						//	{
						//		//CA("ll");
						//		if(field1 != oTableValue.getTableTypeID())
						//			continue;

						//		if(FinalItemIds.size() == 0)
						//		{
						//			FinalItemIds = vec_items;
						//		}
						//		else
						//		{
						//			//CA("mm");
						//			for(int32 i=0; i<vec_items.size(); i++)
						//			{	bool16 Flag = kFalse;
						//				for(int32 j=0; j<FinalItemIds.size(); j++)
						//				{
						//					if(vec_items[i] == FinalItemIds[j])
						//					{
						//						Flag = kTrue;
						//						break;
						//					}				
						//				}
						//				if(!Flag)
						//					FinalItemIds.push_back(vec_items[i]);
						//			}
						//		}
						//	}
						//}//for tabelInfo end
						//if(tableInfo)
						//	delete tableInfo;
					}
					else 
					{
						//CA("nn");
						VectorScreenTableInfoPtr KXAItemtableInfo = NULL;
						if(isComponentAttributePresent == 1)
						{
							bool16 isKitTable = kFalse; // we are spraying component table.
							
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemID_or_productID, languageId, isKitTable); 
							
						}
						else if(isComponentAttributePresent == 2)
						{
							
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
							
						}
						else if(isComponentAttributePresent == 3)
						{
							
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
							
						}

						if(!KXAItemtableInfo)
						{
							//CA("KittableInfo is NULL");
							break;
						}

						if(KXAItemtableInfo->size()==0){
							//CA(" KittableInfo->size()==0");
							FinalItemIdSizeIsZero = kTrue;
							break;
						}

						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it ;						

						bool16 typeidFound=kFalse;
						vector<double> vec_items;
						FinalItemIds.clear();

						it = KXAItemtableInfo->begin();
						{//for tabelInfo start				
							oTableValue = *it;				
							vec_items = VectorChildID;/*oTableValue.getItemIds();*/
						
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = VectorChildID;/*vec_items;*/
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}//for tabelInfo end
						
						Kit_vec_tablerows = oTableValue.getTableData();
						if(KXAItemtableInfo)
							delete KXAItemtableInfo;
					}
				}
			}

			if(customTabbedTextTagPtr->GetAttributeValue(WideString("index")) == WideString("3"))
			{
				//CA("oo");
				VectorScreenTableInfoPtr tableInfo=NULL;
				if(ptrIAppFramework->get_isONEsourceMode())
				{
					// For ONEsource mode
					
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(itemID_or_productID);
					
				}else
				{
					
					//For Publication
					tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, itemID_or_productID, languageId, kTrue);
					
				}
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
					//ItemAbsentinProductFlag = kTrue;
					break;
				}
				if(tableInfo->size()==0)
				{ 
					//CA("tableInfo->size()==0");
					FinalItemIdSizeIsZero = kTrue;
					ptrIAppFramework->LogInfo("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo->size()==0");	
					//ItemAbsentinProductFlag = kTrue;
					break;
				}
				
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;

				bool16 typeidFound=kFalse;
				vector<double> vec_items;
				FinalItemIds.clear();

				PMString tableIDStr = customTabbedTextTagPtr->GetAttributeValue(WideString("tableId"));
				double tableID = tableIDStr.GetAsDouble();

				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{//for tabelInfo start	
					
					oTableValue = *it;	
					
					if(tableID > 0)
						if(tableID != oTableValue.getTableID())
							continue;

					vec_items =VectorChildID; /*oTableValue.getItemIds();*/
				
					if(field1 == -1)	//--------
					{
						
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
							}
						}
					}
					else
					{
						
						if(field1 != oTableValue.getTableTypeID())
							continue;

						if(FinalItemIds.size() == 0)
						{
							FinalItemIds =VectorChildID; /*vec_items;*/
						}
						else
						{
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
							}
						}
					}

				}//for tabelInfo end
				if(tableInfo)
					delete tableInfo;
			}

		}while(false);

		

		if(FinalItemIds.size()<=0)
		{
			
			ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText::FinalItemIds.size()<=0");
			break;
		}
				
		int32 finalItemIDsize =static_cast<int32> (FinalItemIds.size());
		vector<double>ItemIds_Vec;
		ItemIds_Vec.clear();
		for(int32 i=0;i<finalItemIDsize;i++)
		{
			int32 size=static_cast<int32>(ItemIds_Vec.size());
			if(size==0)
			{
				//CA("if(size==0)");
				ItemIds_Vec.push_back(FinalItemIds.at(i));
				PMString q1("Id1 : ");
				q1.AppendNumber(FinalItemIds.at(i));
				//CA(q1);
			}
			else
			{
				int32 check=0;
				for(int32 j=0;j<ItemIds_Vec.size();j++)
				{
					if(ItemIds_Vec.at(j)==FinalItemIds.at(i))
					{
						//CA("check=1;");
						check=1;
					}
				}
				if(check==0)
				{
					//CA("if(check==0)");
					ItemIds_Vec.push_back(FinalItemIds.at(i));
					PMString q1("Id2 : ");
					q1.AppendNumber(FinalItemIds.at(i));
					//CA(q1);
				}
			}
		}
		int32 ItemIdvecsize=static_cast<int32>(ItemIds_Vec.size());//ItemIds_Vec is made as a unique vector
		PMString q1("size : ");
		q1.AppendNumber(ItemIdvecsize);
		//CA(q1);
		

	///check whether header present or not in that box
		bool16 isHeaderPresentInBox = kFalse;
			
		int32 childCount = customTabbedTextTagPtr->GetChildCount();
		

		for(int32 t=0;t<tList.size();t++)
		{
		
			PMString k("Index : ");
			k.AppendNumber(tList[t].imgFlag);
			k.Append("\n");
			k.Append("Image : ");
			k.AppendNumber(tList[t].whichTab);
			//CA(k);
			if(tList[t].imgFlag!=1 && tList[t].whichTab==3 )
			{
				int32 childTagCount = tList[t].tagPtr->GetChildCount();
				PMString a2("Ccount: ");a2.AppendNumber(childTagCount);//CA(a2);

				
				for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
				{
					//CA("A");
					XMLReference childXMLReference = tList[t].tagPtr->GetNthChild(childTagIndex);
					//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
					InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

					
						PMString header = childXMLElement->GetAttributeValue(WideString("header")); //Cs4
						if(header == "1")
						{	
							//CA("isHeaderPresentInBox = kTrue");
							isHeaderPresentInBox = kTrue;
							break;
						}
						
				}
			}
		}
	/////////////////////

	////count number of newline characters present in current box
		int32 newLineCharacters = 0;

		//InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
		//if(unknown == NULL)
		//{
		//	//CA("unknown == NULL");
		//	break;
		//}
		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			//CA("22");
			//CA("textFrameUID == kInvalidUID");
			break;
		}	
		//InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		//if (textFrame == NULL)
		//{
		//	//CA("textFrame == NULL");
		//	break;
		//}	

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
		//	CA("33");
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}
		//IIDXMLElement * customTabbedTextTagPtr = tagInfo.tagPtr;

		TextIndex startPos=0, endPos=0;
		Utils<IXMLUtils>()->GetElementIndices(customTabbedTextTagPtr, &startPos, &endPos);
		
		//PMString asdd("\nStart index = ");
		//asdd.AppendNumber(startPos);
		//asdd.Append("\n End index = ");
		//asdd.AppendNumber(endPos);
		//CA(asdd);

		//TextIndex startPos =-1;
		//TextIndex endPos = -1;

		//startPos = textFrame->TextStart();
		//endPos = startPos + textModel->GetPrimaryStoryThreadSpan() - 1;

		TextIterator begin(textModel , startPos + 1);
		TextIterator end(textModel , endPos - 1);

		for(TextIterator iter = begin ;iter <= end ; iter++)
		{
			 if(*iter == kTextChar_CR)
			 {
				 //CA("newLineCharacters");
				 newLineCharacters++;
			 }
		}

//		newLineCharacters = newLineCharacters - 1;//It is used to not consider the NewLine character which we inserted at end in DataSprayer.
//
		//PMString noOfNewLineCharacters("noOfNewLineCharacters = ");
		//noOfNewLineCharacters.AppendNumber(newLineCharacters);
		//CA(noOfNewLineCharacters);

		bool16 onlyOneLinePresent = kFalse; ///if only one line is present in text frame...
		if(newLineCharacters == 0)
		{
			//CA("newLineCharacters == 0");
			onlyOneLinePresent = kTrue;
		}

	/////////////////////
		
	/////check whether number of items currently present in box is same as FinalItemIds size
	///if it is equal then there is no need to add or delete rows from box in case of headerPresent in box...
	///otherwise there is need to add or delete rows from the box.....
	
		bool16 isNeedToDeleteRows = kFalse;
		bool16 isNeedToAddRows = kFalse;
		int32 noOfLinesToBeAddedOrDeleted = 0;

		
		if(isHeaderPresentInBox)
		{//CA("isHeaderPresentInBox");
			if(finalItemIDsize != newLineCharacters)
			{
				if(finalItemIDsize < newLineCharacters)
				{
					isNeedToDeleteRows = kTrue;
					noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize;
					//noOfLinesToBeAddedOrDeleted = ItemIdvecsize - finalItemIDsize;
				}
				else
				{
					isNeedToAddRows = kTrue;
					//noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters;
					noOfLinesToBeAddedOrDeleted = ItemIdvecsize - newLineCharacters;
				}
			}	
		}
		else
		{
			//CA("!isHeaderPresentInBox");
			if(finalItemIDsize != newLineCharacters + 1)
			{
				if(finalItemIDsize <= newLineCharacters)
				{
					isNeedToDeleteRows = kTrue;
					if(FinalItemIdSizeIsZero == kTrue)
					{
						noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize;
					}
					else
					{
						noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize + 1;

					}
				}
				else
				{
					
					isNeedToAddRows = kTrue;
					//noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters - 1;//ItemIdvecsize
					noOfLinesToBeAddedOrDeleted = ItemIdvecsize - newLineCharacters - 1;

				}
			}
		}

		//if(isNeedToAddRows)
		//{
		//	PMString noOfLinesToBeAdded("noOfLinesToBeAdded = ");
		//	noOfLinesToBeAdded.AppendNumber(noOfLinesToBeAddedOrDeleted);
		//	CA(noOfLinesToBeAdded);
		//}

		/*if(isNeedToDeleteRows)
		{
			PMString noOfLinesToBeDeleted("noOfLinesToBeDeleted = ");
			noOfLinesToBeDeleted.AppendNumber(noOfLinesToBeAddedOrDeleted);
			CA(noOfLinesToBeDeleted);
		}*/
	///////////////////////////////////////

	////add no of lines
		if(!onlyOneLinePresent && isNeedToAddRows)////if more than one lines are already present in text Frame
													//and we want to add more in that
		{
			//CA("98");
			//CA("!onlyOneLinePresent && isNeedToAddRows");
			//CA("!onlyOneLinePresent && isNeedToAddRows");
			int32 startIndexForCopy = startPos + 1/*0*/;
			
			int32 j = 0;
			for(TextIterator iter = begin ;iter <= end ; iter++)
			{
				if(*iter == kTextChar_CR)
				{
					j++;
					if(j == newLineCharacters)
					{
						startIndexForCopy = iter.Position();
						break;
					}
				}
			}
			
			int32 length = endPos - startIndexForCopy;
			//int32 length = endPos - startIndexForCopy;
			boost::shared_ptr< PasteData > pasteData;
			PMString ch("newLineCharacters : ");
				ch.AppendNumber(newLineCharacters);
				//CA(ch);

				int32 NoOfLines=newLineCharacters+1;
			if(noOfLinesToBeAddedOrDeleted>0)
			{
			for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
			{
				//PMString StartIndexStr("startIndexForCopy");
				//StartIndexStr.AppendNumber(startIndexForCopy);
				//StartIndexStr.Append(" , length = ");
				//StartIndexStr.AppendNumber(length);
				//StartIndexStr.Append(" , endPos = ");
				//StartIndexStr.AppendNumber(endPos);
				//CA(StartIndexStr);
				textModel->CopyRange(startIndexForCopy,length,pasteData);
				textModel->Paste(/*kFalse,*/endPos /*-1*/,pasteData);
				endPos = endPos + length;
			}		
			}
		}

		if(onlyOneLinePresent && isNeedToAddRows)
		{
			//CA("99");
			//CA("onlyOneLinePresent && isNeedToAddRows");
			WideString* newLineCharacter = new WideString("\r");
			
			//int32 startIndexForCopy = 0;

			int32 startIndexForCopy = startPos + 1/*1*/;
			//int32 length = endPos -1 -startIndexForCopy;
			int32 length = endPos - startIndexForCopy;


			boost::shared_ptr< PasteData > pasteData;
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kFalse);			
			}

			PMString ch("noOfLinesToBeAddedOrDeleted ====: ");
				ch.AppendNumber(noOfLinesToBeAddedOrDeleted);
				//CA(ch);

			//for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
			//{
			//	//textModel->CopyRange(startIndexForCopy,length,pasteData);
			//	//textModel->Insert(/*kFalse,*/endPos -1,newLineCharacter);
			//	//textModel->Paste(/*kFalse,*/endPos,pasteData);
			//	//endPos = endPos + length;

			//	textModel->CopyRange(startIndexForCopy,length,pasteData);
			//	//textModel->Insert(kFalse,endPos -1,newLineCharacter);
			//	textModel->Insert(endPos ,newLineCharacter);
			//	//textModel->Paste(kFalse,endPos,pasteData);
			//	textModel->Paste(endPos+1,pasteData);
			//	//endPos = endPos + length;
			//	endPos = endPos+ 1 + length;
			//}
			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kTrue);			
			}
			if(newLineCharacter)
				delete newLineCharacter;
		}

	/////Delete no of lines
		if(!onlyOneLinePresent && isNeedToDeleteRows)
		{
			//CA("100");
			if(isHeaderPresentInBox)
			{
				//CA("if(isHeaderPresentInBox)");
				int32 startIndexForDelete = 0;
			
				int32 j = 0;
				for(TextIterator iter = begin ;iter <= end ; iter++)
				{
					if(*iter == kTextChar_CR)
					{
						j++;
						if(j == finalItemIDsize + 1)
						{
							startIndexForDelete = iter.Position();
							break;
						}
					}
				}

				int32 length = endPos - startIndexForDelete;

				textModel->Delete(startIndexForDelete,length);
			}
			else
			{
				int32 startIndexForDelete = 0;
			
				int32 j = 0;
				for(TextIterator iter = begin ;iter <= end ; iter++)
				{
					if(*iter == kTextChar_CR)
					{
						if(FinalItemIdSizeIsZero)
						{
							startIndexForDelete = iter.Position();
							break;
						}

						j++;
						if(j == finalItemIDsize)
						{
							startIndexForDelete = iter.Position();
							break;
						}
					}
				}


				//startIndexForDelete -= 1;
				int32 length = endPos - startIndexForDelete;

				textModel->Delete(startIndexForDelete,length);
			}
		}

//CA("Successfully added or deleted line in text frame");
		
	////////if FinalItemIdSizeIsZero then make all child tags of customTabbedText Impotent
		if(FinalItemIdSizeIsZero)
		{
		//	CA("101");
			if(!isHeaderPresentInBox)
			{
				//CA("if(!isHeaderPresentInBox)");
				/*IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
				int32 newChildCount = customTabbedTextXMLElement->GetChildCount();*/

				TagList tList1 =  itagReader->getTagsFromBox(boxID);
				for(int32 t=0;t<tList1.size();t++)
				{
					if(tList1[t].imgFlag!=1 && tList1[t].whichTab==4 )
					{
						int32 newChildCount = tList1[t].tagPtr->GetChildCount();
						for(int32 i = 0; i < newChildCount; i++)
						{

						//ptrIAppFramework->clearAllStaticObjects();
						XMLReference childXMLReference = tList1[t].tagPtr->GetNthChild(i);
						
						InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

						int32 tagStart = -1;
						int32 tagEnd = -1;
						Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStart,&tagEnd);
						tagStart = tagStart +1;
						tagEnd = tagEnd - 1;

						InterfacePtr<ITextStoryThread> textStoryThread(childXMLElement->QueryContentTextStoryThread());
						if(textStoryThread == NULL)
						{
							//CA("textStoryThread == NULL");
							break;
						}
						InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
						if(textModelptr == NULL)
						{
							//CA("textModel == NULL");
							break;
						}

						PMString str("");
						//WideString data(str);
						//textModelptr->Replace(tagStart,tagEnd-tagStart + 1,&data);
						boost::shared_ptr<WideString> insertText(new WideString(str));
						ReplaceText(textModelptr,tagStart,tagEnd-tagStart + 1 ,insertText);

						PMString itemID("-1");
						//childXMLElement->SetAttributeValue(WideString("typeId"),WideString(itemID)); //Cs4
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString("typeId"),WideString(itemID));
						}
				    }
			   }

				for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
				{
					tList1[tagIndex].tagPtr->Release();
				}
			}
			////Leaks found here
			//for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			//{
			//	tList[tagIndex].tagPtr->Release();
			//}
			break;
		}
	////////

	////////change typeID of all tags as per the FinalItemIds
		/*IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
		int32 newChildCount = customTabbedTextXMLElement->GetChildCount();

		PMString strNewChildCount("newChildCount = ");
		strNewChildCount.AppendNumber(newChildCount);
		CA(strNewChildCount);*/
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}

		tList =  itagReader->getTagsFromBox(boxID);
		for(int32 t=0;t<tList.size();t++)
		{
			if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3))
			{
				int32 newChildCount = tList[t].tagPtr->GetChildCount();
				PMString ch("Childcnt1 : ");
				ch.AppendNumber(newChildCount);
				//CA(ch);

				
				
		        int32 noOfTagsPresentPerRow = 0;
		        if(isNeedToDeleteRows)
		        {
			     noOfTagsPresentPerRow = newChildCount / (newLineCharacters - noOfLinesToBeAddedOrDeleted + 1);
		        }
		        else
		        {
			      noOfTagsPresentPerRow = newChildCount / (newLineCharacters + noOfLinesToBeAddedOrDeleted + 1);
		        }
				PMString p("noOfTagsPresentPerRow :");
				p.AppendNumber(noOfTagsPresentPerRow);
				p.Append("\n");
				p.Append("newChildCount 1 : ");
				p.AppendNumber(newChildCount);
				p.Append("\n");
				p.Append("noOfLinesToBeAddedOrDeleted :");
				p.AppendNumber(noOfLinesToBeAddedOrDeleted);
				p.Append("\n");
				p.Append("newLineCharacters :");
				p.AppendNumber(newLineCharacters);
				//CA(p);
	
				int32 index = 0;
				if(isHeaderPresentInBox)
				index = noOfTagsPresentPerRow;

				vector<double> ::iterator itr;
				itr = FinalItemIds.begin();


				//for(int i = 0; i < finalItemIDsize; i++ ,itr++)
				//{
				//	PMString itemID("");
				//	itemID.AppendNumber(*itr);
				//	for(int j = 0 ; j < noOfTagsPresentPerRow ; j++)
				//	{
				//	/*PMString temp("itemID = ");
				//	temp.Append(itemID);
				//	temp.Append(" , index = ");
				//	temp.AppendNumber(index);
				//	CA(temp);*/

				//		XMLReference childXMLReference = tList[t].tagPtr->GetNthChild(index);
				//	//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
				//	InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
				//				
				//	//childXMLElement->SetAttributeValue(WideString("typeId"),WideString(itemID)); //Cs4
				//	childXMLElement->SetAttributeValue(WideString("childId"),WideString(itemID));
				//	index++;
				//   }		
		  //     }

				///////////////////////
	
				////////now we can refresh each tags 
				VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
				VectorHtmlTrackerValue vectorObj ;
				vectorHtmlTrackerSPTBPtr = &vectorObj;

				int32 j = 0;//temp variable
				int32 sequenceNo = 0; //index of itemID present in FinalItemIds.

				//For first level

//=============================================
				

//==============================================================
	//for(int32 t=0;t<tList.size();t++)
	{
		//CA("A");
		
		if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3))
		{
			//CA("B");
			//int32 childTagCount = tList[t].tagPtr->GetChildCount();
			for(int32 i = 0; i <newChildCount; i++)
		    {
			//CA("Final for loop");
			//ptrIAppFramework->clearAllStaticObjects();
			XMLReference childXMLReference = tList[t].tagPtr->GetNthChild(i);
			//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

			

			//int32 j2 = 0;//temp variable
			//int32 sequenceNo2 = 0; //index of itemID present in FinalItemIds.
			//for(int32 k=0;k<childcount1;k++)
			//{
			// XMLReference AchildXMLReference = childXMLElement->GetNthChild(k);
			// InterfacePtr<IIDXMLElement>AchildXMLElement(AchildXMLReference.Instantiate());

			// int32 childcount2=AchildXMLElement->GetChildCount();
			// int32 j3 = 0;//temp variable
			// int32 sequenceNo3 = 0; //index of itemID present in FinalItemIds.
			// for(int32 x=0;x<childcount2;x++)
			// {
			//   CA("childcount2");	 
			//   XMLReference BchildXMLReference = AchildXMLElement->GetNthChild(x);
			//   InterfacePtr<IIDXMLElement>BchildXMLElement(BchildXMLReference.Instantiate());

			//   PMString strTypeID3 = BchildXMLElement->GetAttributeValue(WideString("typeId")); //Cs4
			//   PMString strSectionID3 = BchildXMLElement->GetAttributeValue(WideString("sectionID"));
			//   PMString strElementID3 = BchildXMLElement->GetAttributeValue(WideString("ID"));
			//   PMString strLanguageID3 = BchildXMLElement->GetAttributeValue(WideString("LanguageID"));
			//   PMString strParentTypeID3 = BchildXMLElement->GetAttributeValue(WideString("parentTypeID"));
			//   PMString strIndex3 = BchildXMLElement->GetAttributeValue(WideString("index"));
			//   //PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

			//   PMString strHeader3 = BchildXMLElement->GetAttributeValue(WideString("header"));
			//   PMString strchildId3 = BchildXMLElement->GetAttributeValue(WideString("childId"));
			//   PMString strchildTag3 = BchildXMLElement->GetAttributeValue(WideString("childTag"));

			//   int32 header3 = strHeader3.GetAsNumber();
			//   int32 childId3 = strchildId3.GetAsNumber();
			//   PMString w("childId2 :");
			//   w.AppendNumber(childId3);
			//   //CA(w);
			//   int32 childTag3 = strchildTag3.GetAsNumber();

			//   int32 typeId3 = strTypeID3.GetAsNumber();
			//   int32 sectionID3 = strSectionID3.GetAsNumber();
			//   int32 elementID3 = strElementID3.GetAsNumber();
			//   PMString w1("elementID2 :");
			//   w1.AppendNumber(elementID3);
			//   //CA(w1);

			//   int32 languageID3 = strLanguageID3.GetAsNumber();
			//   int32 parentTypeID3 = strParentTypeID3.GetAsNumber();
			//   int32 index3 = strIndex3.GetAsNumber();
		
			//   PMString itemAttributeValue3("");
			//	if(header3 != 1)
			//	{
			//	    if(j3 < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
			//	    {
			//		j3++;
			//	    }
			//	    else
			//	    {
			//		sequenceNo3++;
			//		j3 = 1;
			//	    }
			//	   if(elementID3 == -803)  // For 'Letter Key'
			//	   {	
			//		 //CA("22");
			//		 itemAttributeValue3.Clear();
			//		 int wholeNo3 =  sequenceNo3 / 26;
			//		 int remainder3= sequenceNo3 % 26;								

			//		 if(wholeNo3 == 0)
			//		 {// will print 'a' or 'b'... 'z'  upto 26 item ids
			//			itemAttributeValue3.Append(AlphabetArray[remainder3]);
			//		 }
			//		 else  if(wholeNo3 <= 26)
			//		 {// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
			//			itemAttributeValue3.Append(AlphabetArray[wholeNo3-1]);	
			//			itemAttributeValue3.Append(AlphabetArray[remainder3]);	
			//		 }
			//		 else if(wholeNo3 <= 52)
			//		 {// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
			//			itemAttributeValue3.Append(AlphabetArray[0]);
			//			itemAttributeValue3.Append(AlphabetArray[wholeNo3 -26 -1]);	
			//			itemAttributeValue3.Append(AlphabetArray[remainder3]);										
			//		 }
			//	   }
			//	   else
			//	   {
			//	    itemAttributeValue3 = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId3,elementID3, languageID3, /*kTrue*/kFalse );//23OCT09//Amit
			//		PMString str("string :");
			//		str.Append(itemAttributeValue3);
			//		CA(str);
			//	   }
			//	   int32 tagStartPos3 = -1;
			//       int32 tagEndPos3 = -1;
			//       Utils<IXMLUtils>()->GetElementIndices(BchildXMLElement,&tagStartPos3,&tagEndPos3);
			//       tagStartPos3 = tagStartPos3 + 1;
			//       tagEndPos3 = tagEndPos3 -1;

			//       PMString textToInsert3("");
			//       InterfacePtr<ISpecialChar> iConverter3(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//       if(!iConverter3)
			//        {
			//	    CA("24");
			//	    textToInsert3=itemAttributeValue3;					
			//        }
			//      else						
			//        {
			//	     //CA("25");
			//	     //textToInsert=iConverter->translateString(itemAttributeValue);
			//	     vectorHtmlTrackerSPTBPtr->clear();
			//	     textToInsert3=iConverter3->translateStringNew(itemAttributeValue3, vectorHtmlTrackerSPTBPtr);
			//	     iConverter3->ChangeQutationMarkONOFFState(kFalse);
			//        }	
			//        WideString insertText3(textToInsert3);
			//        textModel->Replace(tagStartPos3,tagEndPos3-tagStartPos3 +1 ,&insertText3);
			//       if(iConverter3)
			//       {
			//	   //CA("26");
			//	    iConverter3->ChangeQutationMarkONOFFState(kTrue);			
			//       }
			//	}
			//}
			// 
			//// }

			// PMString strTypeID2 = AchildXMLElement->GetAttributeValue(WideString("typeId")); //Cs4
			//PMString strSectionID2 = AchildXMLElement->GetAttributeValue(WideString("sectionID"));
			//PMString strElementID2 = AchildXMLElement->GetAttributeValue(WideString("ID"));
			//PMString strLanguageID2 = AchildXMLElement->GetAttributeValue(WideString("LanguageID"));
			//PMString strParentTypeID2 = AchildXMLElement->GetAttributeValue(WideString("parentTypeID"));
			//PMString strIndex2 = AchildXMLElement->GetAttributeValue(WideString("index"));
			////PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

			//PMString strHeader2 = AchildXMLElement->GetAttributeValue(WideString("header"));
			//PMString strchildId2 = AchildXMLElement->GetAttributeValue(WideString("childId"));
			//PMString strchildTag2 = AchildXMLElement->GetAttributeValue(WideString("childTag"));

			//int32 header2 = strHeader2.GetAsNumber();
			//int32 childId2 = strchildId2.GetAsNumber();
			//PMString w("childId2 :");
			//w.AppendNumber(childId2);
			////CA(w);
			//int32 childTag2 = strchildTag2.GetAsNumber();

			//int32 typeId2 = strTypeID2.GetAsNumber();
			//int32 sectionID2 = strSectionID2.GetAsNumber();
			//int32 elementID2 = strElementID2.GetAsNumber();
			//PMString w1("elementID2 :");
			//w1.AppendNumber(elementID2);
			////CA(w1);

			//int32 languageID2 = strLanguageID2.GetAsNumber();
			//int32 parentTypeID2 = strParentTypeID2.GetAsNumber();
			//int32 index2 = strIndex2.GetAsNumber();
		
			//PMString itemAttributeValue2("");
			//	if(header2 != 1)
			//	{
			//	    if(j2 < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
			//	    {
			//		j2++;
			//	    }
			//	    else
			//	    {
			//		sequenceNo2++;
			//		j2 = 1;
			//	    }
			//	   if(elementID2 == -803)  // For 'Letter Key'
			//	   {	
			//		 //CA("22");
			//		 itemAttributeValue2.Clear();
			//		 int wholeNo2 =  sequenceNo2 / 26;
			//		 int remainder2 = sequenceNo2 % 26;								

			//		 if(wholeNo2 == 0)
			//		 {// will print 'a' or 'b'... 'z'  upto 26 item ids
			//			itemAttributeValue2.Append(AlphabetArray[remainder2]);
			//		 }
			//		 else  if(wholeNo2 <= 26)
			//		 {// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
			//			itemAttributeValue2.Append(AlphabetArray[wholeNo2-1]);	
			//			itemAttributeValue2.Append(AlphabetArray[remainder2]);	
			//		 }
			//		 else if(wholeNo2 <= 52)
			//		 {// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
			//			itemAttributeValue2.Append(AlphabetArray[0]);
			//			itemAttributeValue2.Append(AlphabetArray[wholeNo2 -26 -1]);	
			//			itemAttributeValue2.Append(AlphabetArray[remainder2]);										
			//		 }
			//	   }
			//	   else
			//	   {
			//	    itemAttributeValue2 = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId2,elementID2, languageID2, /*kTrue*/kFalse );//23OCT09//Amit
			//		PMString str("string :");
			//		str.Append(itemAttributeValue2);
			//		//CA(str);
			//	   }
			//	   int32 tagStartPos2 = -1;
			//       int32 tagEndPos2 = -1;
			//       Utils<IXMLUtils>()->GetElementIndices(AchildXMLElement,&tagStartPos2,&tagEndPos2);
			//       tagStartPos2 = tagStartPos2 + 1;
			//       tagEndPos2 = tagEndPos2 -1;

			//       PMString textToInsert2("");
			//       InterfacePtr<ISpecialChar> iConverter2(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//       if(!iConverter2)
			//        {
			//	    CA("24");
			//	    textToInsert2=itemAttributeValue2;					
			//        }
			//      else						
			//        {
			//	     //CA("25");
			//	     //textToInsert=iConverter->translateString(itemAttributeValue);
			//	     vectorHtmlTrackerSPTBPtr->clear();
			//	     textToInsert2=iConverter2->translateStringNew(itemAttributeValue2, vectorHtmlTrackerSPTBPtr);
			//	     iConverter2->ChangeQutationMarkONOFFState(kFalse);
			//        }	
			//        WideString insertText2(textToInsert2);
			//        textModel->Replace(tagStartPos2,tagEndPos2-tagStartPos2 +1 ,&insertText2);
			//       if(iConverter2)
			//       {
			//	   //CA("26");
			//	    iConverter2->ChangeQutationMarkONOFFState(kTrue);			
			//       }
			//	}
			//}
			
			PMString strTypeID = childXMLElement->GetAttributeValue(WideString("typeId")); //Cs4
			PMString strSectionID = childXMLElement->GetAttributeValue(WideString("sectionID"));
			PMString strElementID = childXMLElement->GetAttributeValue(WideString("ID"));
			PMString strLanguageID = childXMLElement->GetAttributeValue(WideString("LanguageID"));
			PMString strParentTypeID = childXMLElement->GetAttributeValue(WideString("parentTypeID"));
			PMString strIndex = childXMLElement->GetAttributeValue(WideString("index"));
			//PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

			PMString strHeader = childXMLElement->GetAttributeValue(WideString("header"));
			PMString strchildId = childXMLElement->GetAttributeValue(WideString("childId"));
			PMString strchildTag = childXMLElement->GetAttributeValue(WideString("childTag"));
			PMString strProductID =  childXMLElement->GetAttributeValue(WideString("parentID"));


			int32 header = strHeader.GetAsNumber();
			double childId = strchildId.GetAsDouble();
			int32 childTag = strchildTag.GetAsNumber();

			double typeId = strTypeID.GetAsDouble();
			double sectionID = strSectionID.GetAsDouble();
			double elementID = strElementID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();
			double parentTypeID = strParentTypeID.GetAsDouble();
			int32 index = strIndex.GetAsNumber();
			double parentid = strProductID.GetAsDouble();

            int32 childcount1=childXMLElement->GetChildCount();
			PMString ch1("childId--: ");
			ch1.AppendNumber(childId);
			ch1.Append("\n");
			ch1.Append("parentid : ");
			ch1.AppendNumber(parentid);
			ch1.Append("\n");
			ch1.Append("header : ");
			ch1.AppendNumber(header);
			//CA(ch1);

			int32 j1 = 0;//temp variable
			int32 sequenceNo1 = 0;
			//for second level
			for(int32 x=0;x<childcount1;x++)
			{
				
				XMLReference AchildXMLReference = childXMLElement->GetNthChild(x);
			    //IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			    InterfacePtr<IIDXMLElement>childXMLElement12(AchildXMLReference.Instantiate());

				PMString strTypeID1 = childXMLElement12->GetAttributeValue(WideString("typeId")); //Cs4
				PMString strSectionID1 = childXMLElement12->GetAttributeValue(WideString("sectionID"));
				PMString strElementID1 = childXMLElement12->GetAttributeValue(WideString("ID"));
				PMString strLanguageID1 = childXMLElement12->GetAttributeValue(WideString("LanguageID"));
				PMString strParentTypeID1 = childXMLElement12->GetAttributeValue(WideString("parentTypeID"));
				PMString strIndex1 = childXMLElement12->GetAttributeValue(WideString("index"));
				//PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

				PMString strHeader1 = childXMLElement12->GetAttributeValue(WideString("header"));
				PMString strchildId1 = childXMLElement12->GetAttributeValue(WideString("childId"));
				PMString strchildTag1 = childXMLElement12->GetAttributeValue(WideString("childTag"));

				int32 header1 = strHeader1.GetAsNumber();
				double childId1 = strchildId1.GetAsDouble();
				int32 childTag1 = strchildTag1.GetAsNumber();

				double typeId1 = strTypeID1.GetAsDouble();
				double sectionID1 = strSectionID1.GetAsDouble();
				double elementID1 = strElementID1.GetAsDouble();
				double languageID1 = strLanguageID1.GetAsDouble();
				double parentTypeID1 = strParentTypeID1.GetAsDouble();
				int32 index1 = strIndex1.GetAsNumber();

				 int32 childcount2=childXMLElement12->GetChildCount();
                 //for third level
				 int32 j34 = 0;//temp variable
			     int32 sequenceNo34 = 0;
				 for(int32 y=0;y<childcount2;y++)
				 {
				   XMLReference BchildXMLReference = childXMLElement12->GetNthChild(y);
			       //IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			       InterfacePtr<IIDXMLElement>childXMLElement34(BchildXMLReference.Instantiate());

				  PMString strTypeID34 = childXMLElement34->GetAttributeValue(WideString("typeId")); //Cs4
				  PMString strSectionID34 = childXMLElement34->GetAttributeValue(WideString("sectionID"));
				  PMString strElementID34 = childXMLElement34->GetAttributeValue(WideString("ID"));
				  PMString strLanguageID34 = childXMLElement34->GetAttributeValue(WideString("LanguageID"));
				  PMString strParentTypeID34 = childXMLElement34->GetAttributeValue(WideString("parentTypeID"));
				  PMString strIndex34 = childXMLElement34->GetAttributeValue(WideString("index"));
				  //PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

				 PMString strHeader34 = childXMLElement34->GetAttributeValue(WideString("header"));
				 PMString strchildId34 = childXMLElement34->GetAttributeValue(WideString("childId"));
				 PMString strchildTag34 = childXMLElement34->GetAttributeValue(WideString("childTag"));

				 int32 header34 = strHeader34.GetAsNumber();
				 double childId34 = strchildId34.GetAsDouble();
				 int32 childTag34 = strchildTag34.GetAsNumber();

				 double typeId34 = strTypeID34.GetAsDouble();
				 double sectionID34 = strSectionID34.GetAsDouble();
				 double elementID34 = strElementID34.GetAsDouble();
				 double languageID34 = strLanguageID34.GetAsDouble();
				 double parentTypeID34 = strParentTypeID34.GetAsDouble();
				 int32 index34 = strIndex34.GetAsNumber();

				 PMString itemAttributeValue34("");
			     if(header34 != 1)
			     {
				  //CA("20");
				  if(j34 < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
				  {
					j34++;
				  }
				  else
				 {
					sequenceNo34++;
				 	j34 = 1;
				 }
				
			   if(elementID34 == -803)  // For 'Letter Key'
				{	
					//CA("22");
					itemAttributeValue34.Clear();
					int wholeNo34 =  sequenceNo34 / 26;
					int remainder34 = sequenceNo34 % 26;								

					if(wholeNo34 == 0)
					{// will print 'a' or 'b'... 'z'  upto 26 item ids
						itemAttributeValue34.Append(AlphabetArray[remainder34]);
					}
					else  if(wholeNo34 <= 26)
					{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
						itemAttributeValue34.Append(AlphabetArray[wholeNo34-1]);	
						itemAttributeValue34.Append(AlphabetArray[remainder34]);	
					}
					else if(wholeNo34 <= 34)
					{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
						itemAttributeValue34.Append(AlphabetArray[0]);
						itemAttributeValue34.Append(AlphabetArray[wholeNo34 -26 -1]);	
						itemAttributeValue34.Append(AlphabetArray[remainder34]);										
					}
				}
			   else
				{
					PMString q2("childid :");
					q2.AppendNumber(childId34);
					//CA(q2);
					PMString q3("element:");
					q3.AppendNumber(elementID34);
					//CA(q3);
                    itemAttributeValue34 = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId34,elementID34, languageID34, sectionID34, /*kTrue*/kFalse );//23OCT09//Amit
					PMString q4("valu11111 :");
					q4.Append(itemAttributeValue34);
					//CA(q4);
					
				}
			}
			int32 tagStartPos34 = -1;
			int32 tagEndPos34 = -1;
			Utils<IXMLUtils>()->GetElementIndices(childXMLElement34,&tagStartPos34,&tagEndPos34);
			tagStartPos34 = tagStartPos34 + 1;
			tagEndPos34 = tagEndPos34 -1;

			PMString textToInsert34("");
			InterfacePtr<ISpecialChar> iConverter34(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter34)
			{
				//CA("24");
				textToInsert34=itemAttributeValue34;					
			}
			else						
			{
				//CA("25");
				//textToInsert=iConverter->translateString(itemAttributeValue);
				vectorHtmlTrackerSPTBPtr->clear();
				textToInsert34=iConverter34->translateStringNew(itemAttributeValue34, vectorHtmlTrackerSPTBPtr);
				iConverter34->ChangeQutationMarkONOFFState(kFalse);
			}	
			//WideString insertText34(textToInsert34);
			//textModel->Replace(tagStartPos34,tagEndPos34-tagStartPos34 +1 ,&insertText34);
                     textToInsert34.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert34));
			ReplaceText(textModel,tagStartPos34,tagEndPos34-tagStartPos34 +1 ,insertText);

			if(iConverter34)
			{
				//CA("26");
				iConverter34->ChangeQutationMarkONOFFState(kTrue);			
			}
		}
 //End of third level
				PMString itemAttributeValue1("");
			   if(header1 != 1)
			   {
				//CA("20");
				if(j1 < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
				{
					j1++;
				}
				else
				{
					sequenceNo1++;
					j1 = 1;
				}
				
			   if(elementID1 == -803)  // For 'Letter Key'
				{	
					//CA("23");
					itemAttributeValue1.Clear();
					int wholeNo1 =  sequenceNo1 / 26;
					int remainder1 = sequenceNo1 % 26;								

					if(wholeNo1 == 0)
					{// will print 'a' or 'b'... 'z'  upto 26 item ids
						itemAttributeValue1.Append(AlphabetArray[remainder1]);
					}
					else  if(wholeNo1 <= 26)
					{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
						itemAttributeValue1.Append(AlphabetArray[wholeNo1-1]);	
						itemAttributeValue1.Append(AlphabetArray[remainder1]);	
					}
					else if(wholeNo1 <= 52)
					{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
						itemAttributeValue1.Append(AlphabetArray[0]);
						itemAttributeValue1.Append(AlphabetArray[wholeNo1 -26 -1]);	
						itemAttributeValue1.Append(AlphabetArray[remainder1]);										
					}
				}
			   else
				{
					PMString q2("childid :");
					q2.AppendNumber(childId1);
					//CA(q2);
					PMString q3("element:");
					q3.AppendNumber(elementID1);
					//CA(q3);
                    itemAttributeValue1 = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId1,elementID1, languageID1, sectionID1, /*kTrue*/kFalse );//23OCT09//Amit
					PMString q4("valu2222 :");
					q4.Append(itemAttributeValue1);
					//CA(q4);
					
				}
			}
			//int32 tagStartPos1 = -1;
			//int32 tagEndPos1 = -1;
			//Utils<IXMLUtils>()->GetElementIndices(childXMLElement12,&tagStartPos1,&tagEndPos1);
			//tagStartPos1 = tagStartPos1 + 1;
			//tagEndPos1 = tagEndPos1 -1;

			//PMString textToInsert1("");
			//InterfacePtr<ISpecialChar> iConverter1(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//if(!iConverter1)
			//{
			//	CA("24");
			//	textToInsert1=itemAttributeValue1;					
			//}
			//else						
			//{
			//	//CA("25");
			//	//textToInsert=iConverter->translateString(itemAttributeValue);
			//	vectorHtmlTrackerSPTBPtr->clear();
			//	textToInsert1=iConverter1->translateStringNew(itemAttributeValue1, vectorHtmlTrackerSPTBPtr);
			//	iConverter1->ChangeQutationMarkONOFFState(kFalse);
			//}	
			//WideString insertText1(textToInsert1);
			//textModel->Replace(tagStartPos1,tagEndPos1-tagStartPos1 +1 ,&insertText1);
			//if(iConverter1)
			//{
			//	//CA("26");
			//	iConverter1->ChangeQutationMarkONOFFState(kTrue);			
			//}
		}
		//end of second level
		
		
		{
			
			PMString itemAttributeValue("");
			if(header != 1)
			{
				//CA("20");
				if(j < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
				{
					j++;
				}
				else
				{
					sequenceNo++;
					j = 1;
				}
				
			   if(elementID == -803)  // For 'Letter Key'
				{	
					//CA("24");
					itemAttributeValue.Clear();
					int wholeNo =  sequenceNo / 26;
					int remainder = sequenceNo % 26;								

					if(wholeNo == 0)
					{// will print 'a' or 'b'... 'z'  upto 26 item ids
						itemAttributeValue.Append(AlphabetArray[remainder]);
					}
					else  if(wholeNo <= 26)
					{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
						itemAttributeValue.Append(AlphabetArray[wholeNo-1]);	
						itemAttributeValue.Append(AlphabetArray[remainder]);	
					}
					else if(wholeNo <= 52)
					{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
						itemAttributeValue.Append(AlphabetArray[0]);
						itemAttributeValue.Append(AlphabetArray[wholeNo -26 -1]);	
						itemAttributeValue.Append(AlphabetArray[remainder]);										
					}
				}
			   else
				{
					//CA("Before giving Debug");
                   itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId,elementID, languageID, sectionID, /*kTrue*/kFalse );//23OCT09//Amit
				   PMString ch2("childId : ");
					ch2.AppendNumber(childId);
					/*ch2.Append("\n");
					ch2.Append("elementID : ");
					ch2.AppendNumber(elementID);
					ch2.Append("\n");
					
					ch2.Append("LangId : ");
					ch2.AppendNumber(languageID);*/
					ch2.Append("\n");
					ch2.Append("string : ");
					ch2.Append(itemAttributeValue);
					//CA(ch2);
					
				}
			   //CA("A");
			}
			else if(header==1)
			{
				itemAttributeValue= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
			}
			//CA("B");
			int32 tagStartPos = -1;
			int32 tagEndPos = -1;
			Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			PMString textToInsert("");
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				//CA("24");
				textToInsert=itemAttributeValue;					
			}
			else						
			{
				//CA("25");
				//textToInsert=iConverter->translateString(itemAttributeValue);
				vectorHtmlTrackerSPTBPtr->clear();
				textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
				iConverter->ChangeQutationMarkONOFFState(kFalse);
			}	
			//WideString insertText(textToInsert);
			//textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);

			if(iConverter)
			{
				//CA("26");
				iConverter->ChangeQutationMarkONOFFState(kTrue);			
			}
			//enf of first level
		}
		
		}
		}
		}
		/*for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}*/

		}
		}
		////leaks found here
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		result = kSuccess;
	}while(kFalse);
 	return result;
}
/////////////////// new code start
ErrorCode RefreshCustomTabbedText(const UIDRef& boxID,TagStruct& tagInfo)
{
	//CA("inside RefreshCustomTabbedText");
	ErrorCode result = kFailure;
	do
	{
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		vector<double> FinalItemIds;
		bool16 FinalItemIdSizeIsZero = kFalse;

		IIDXMLElement *customTabbedTextTagPtr = tagInfo.tagPtr;

		PMString strParentID =  customTabbedTextTagPtr->GetAttributeValue(WideString("parentID")); //Cs4
		PMString strLanguageID =  customTabbedTextTagPtr->GetAttributeValue(WideString("LanguageID")); //Cs4
		PMString strSectionID = customTabbedTextTagPtr->GetAttributeValue(WideString("sectionID"));//Cs4
		
		//-------------
		double field1 = -1;
		double elmentId1 = -1;
		double ParentId1 = -1;
		double ChildId1 = -1;
//===================================Pivot List(We Dont want to refresh Pivot List for Structure option)=================================
		InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
			//return ;
		}

		bool16 IsPivotList=kFalse;
		TagList tList,tList_checkForHybrid;
		 tList =  itagReader->getTagsFromBox(boxID);
		for(int32 t=0;t<tList.size();t++)
		{
			if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3) )
			{
				int32 childTagCount = tList[t].tagPtr->GetChildCount();
				PMString l("1st :");
				l.AppendNumber(childTagCount);
				//CA(l);


				VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr1 = NULL;
				VectorHtmlTrackerValue vectorObj1 ;
				vectorHtmlTrackerSPTBPtr1 = &vectorObj1;


				for(int32 tagIndex = 0;tagIndex < childTagCount;tagIndex++)
				{
					XMLReference cellXMLElementRef = tList[t].tagPtr->GetNthChild(tagIndex);
					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil )
					{
						//CA("cellXMLElementPtr==nil ");
						continue;
					}
					PMString strfield1 = cellXMLElementPtr->GetAttributeValue(WideString("field1")); 
					field1 =  strfield1.GetAsNumber();

					PMString strTypeID = cellXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
					PMString strSectionID = cellXMLElementPtr->GetAttributeValue(WideString("sectionID"));
					PMString strElementID = cellXMLElementPtr->GetAttributeValue(WideString("ID"));
					PMString strLanguageID = cellXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
					PMString strParentTypeID = cellXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
					PMString strIndex = cellXMLElementPtr->GetAttributeValue(WideString("index"));
					//PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));//by amarjit patil

					PMString strHeader = cellXMLElementPtr->GetAttributeValue(WideString("header"));
					PMString strchildId = cellXMLElementPtr->GetAttributeValue(WideString("childId"));
					PMString strchildTag = cellXMLElementPtr->GetAttributeValue(WideString("childTag"));

					int32 header = strHeader.GetAsNumber();
					double childId = strchildId.GetAsDouble();
					int32 childTag = strchildTag.GetAsNumber();

					double typeId = strTypeID.GetAsDouble();
					double sectionID = strSectionID.GetAsDouble();
					double elementID = strElementID.GetAsDouble();
					double languageID = strLanguageID.GetAsDouble();
					double parentTypeID = strParentTypeID.GetAsDouble();
					int32 index = strIndex.GetAsNumber();

					PMString itemAttributeValue("");
					if(typeId==-57 || iscontentbuttonselected == kTrue) // Refreshing table by Content or Pivote List.
					{
						IsPivotList=kTrue;

						if(header == 1)
						{
							if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
							{
								// 
							}
							else if(elementID == -703)
							{
								//CA("dispName = $Off");
								itemAttributeValue = "$Off";
							}
							else if(elementID == -704)
							{
								//CA("dispName = %Off");
								itemAttributeValue = "%Off";
							}
							else if(elementID == -805)
							{									
								itemAttributeValue = "Quantity";
							}
							else if(elementID == -806)
							{									
								itemAttributeValue = "Availablity";
							}
							else if(elementID == -807)
							{									
								itemAttributeValue = "Cross-reference Type";
							}
							else if(elementID == -808)
							{									
								itemAttributeValue = "Rating";
							}
							else if(elementID == -809)
							{									
								itemAttributeValue = "Alternate";
							}
							else if(elementID == -810)
							{									
								itemAttributeValue = "Comments";
							}
							else if(elementID == -811)
							{									
								itemAttributeValue = "";
							}
							else if(elementID == -812)
							{									
								itemAttributeValue = "Quantity";
							}
							else if(elementID == -813)
							{									
								itemAttributeValue = "Required";
							}
							else if(elementID == -814)
							{									
								itemAttributeValue = "Comments";
							}
							else
								itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
						}
						else
						{

							itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId,elementID, languageID, sectionID, /*kTrue*/kFalse );//23OCT09//Amit

						}
						/*PMString ch2("childId : ");
						ch2.AppendNumber(childId);
						ch2.Append("\n");
						ch2.Append("elementID : ");
						ch2.AppendNumber(elementID);
						ch2.Append("\n");
						ch2.Append("string : ");
						ch2.Append(itemAttributeValue);*/
						//CA(ch2);
					
						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellXMLElementPtr,&tagStartPos,&tagEndPos);

						InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElementPtr->QueryContentTextStoryThread());
						if(textStoryThread == NULL)
						{
							//CA("textStoryThread == NULL");
							break;
						}
						InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
						if(textModelptr == NULL)
						{
							//CA("textModel == NULL");
							break;
						}

						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							//CA("24");
							textToInsert=itemAttributeValue;					
						}
						else						
						{
							//CA("25");
							//textToInsert=iConverter->translateString(itemAttributeValue);
							vectorHtmlTrackerSPTBPtr1->clear();
							textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr1);
							iConverter->ChangeQutationMarkONOFFState(kFalse);
						}	
						//WideString insertText(textToInsert);
						//textModelptr->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
						//Apsiva 9 comment
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModelptr,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);
						if(iConverter)
						{
							//CA("26");
							iConverter->ChangeQutationMarkONOFFState(kTrue);			
						}
					}
					
				}
			}
		}


//==============================End===============================================================
	if(IsPivotList==kFalse)  //  now refreshing tabbed text table by Structure option
	{
		//CA("A");
		int32 ChildCount=customTabbedTextTagPtr->GetChildCount();
		
		for(int32 tagIndex = 0;tagIndex < customTabbedTextTagPtr->GetChildCount();tagIndex++)
		{
			XMLReference cellXMLElementRef =   customTabbedTextTagPtr->GetNthChild(tagIndex);
			//This is a tag attached to the cell.
			//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
			//Get the text tag attached to the text inside the cell.
			//We are providing only one texttag inside cell.
			if(cellXMLElementPtr==nil )
			{
				//CA("cellXMLElementPtr==nil ");
				continue;
			}
			PMString strfield1 = cellXMLElementPtr->GetAttributeValue(WideString("field1")); 
			field1 =  strfield1.GetAsNumber();

			PMString strId1 = cellXMLElementPtr->GetAttributeValue(WideString("ID")); 
			elmentId1 =  strId1.GetAsDouble();

			PMString strParentId1 = cellXMLElementPtr->GetAttributeValue(WideString("parentID")); 
			ParentId1 =  strParentId1.GetAsDouble();

			PMString strChildId = cellXMLElementPtr->GetAttributeValue(WideString("childId")); 
			ChildId1 =  strChildId.GetAsDouble();
			if(elmentId1==-786)
			{

				/*PMString qq("parentID : ");
				qq.AppendNumber(ParentId1);
				qq.Append("\n");
				qq.Append("childId : ");
				qq.AppendNumber(ChildId1);
				qq.Append("\n");
				qq.Append("ID : ");
				qq.AppendNumber(elmentId1);*/
				//qq.Append();
				//CA(qq);
			}

		}

		//CA("B");
		double itemID_or_productID = strParentID.GetAsDouble();
		double languageId = strLanguageID.GetAsDouble();
		double sectionID = strSectionID.GetAsDouble();

		bool16 isSectionLevelItemItemPresent = kFalse;
		int32 isComponentAttributePresent = 0;
		vector<vector<PMString> > Kit_vec_tablerows;  //To store Component's 'tableData' so that we can use
		
	////get all itemIDs of section level item or product.............

		do{
			if(customTabbedTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4"))//Cs4
			{
				//CA("customTabbedTextTagPtr->GetAttributeValue(index) == 4 ");
				if(customTabbedTextTagPtr->GetAttributeValue(/*WideString("colno")) == WideString("-101")*/WideString("childTag")) == WideString("1") && customTabbedTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1"))
				{
					isSectionLevelItemItemPresent = kTrue;
					//CA("SectionLevelItemItemPresent == kTrue");
					if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-901"))
						isComponentAttributePresent = 1;
					else if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-902"))
						isComponentAttributePresent = 2;
					else if(customTabbedTextTagPtr->GetAttributeValue(WideString("rowno")) == WideString("-903"))
						isComponentAttributePresent = 3;
				}

				if(isSectionLevelItemItemPresent == kTrue)
				{
					//CA("isSectionLevelItemItemPresent == kTrue");			
					/////get pub_object_id
					
					//CPbObjectValue* ptrCPbObjectValue =ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(tagInfo.sectionID,tagInfo.parentId);
					//int32 pub_object_id = ptrCPbObjectValue->getPub_object_id();
					//PMString name = ptrCPbObjectValue->getName();
	//CA("Name::"+name);
					
					PMString pub_object_idStr = customTabbedTextTagPtr->GetAttributeValue(WideString("pbObjectId"));
					double pub_object_id = pub_object_idStr.GetAsDouble();
					
					if(isComponentAttributePresent == 0)
					{
						//CA("isComponentAttributePresent == 0");
						VectorScreenTableInfoPtr tableInfo = 
						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tagInfo.parentId, tagInfo.sectionID, tagInfo.languageID);

						if(!tableInfo)
						{
							//CA("!tableInfo");
							ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshCustomTabbedText::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
							break;
						}

						if(tableInfo->size()==0)
						{ 
							//CA("tableInfo->size()==0");
							FinalItemIdSizeIsZero = kTrue;
							ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText: table size = 0");
							break;
						}

						VectorScreenTableInfoValue ::iterator it;
						CItemTableValue oTableValue;

						bool16 typeidFound=kFalse;
						vector<double> vec_items;
						FinalItemIds.clear();

						PMString tableIDStr = customTabbedTextTagPtr->GetAttributeValue(WideString("tableId"));
						double tableID = tableIDStr.GetAsDouble();


						for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						{//for tabelInfo start
							oTableValue = *it;				
							if(tableID > 0)
								if(tableID != oTableValue.getTableID())
									continue;

							vec_items = oTableValue.getItemIds();
							
							if( field1 == -1 )	//------
							{
								//CA("field1 == -1");
								if(FinalItemIds.size() == 0)
								{
									FinalItemIds = vec_items;
								}
								else
								{
									for(int32 i=0; i<vec_items.size(); i++)
									{	bool16 Flag = kFalse;

										for(int32 j=0; j<FinalItemIds.size(); j++)
										{
											if(vec_items[i] == FinalItemIds[j])
											{
												Flag = kTrue;
												break;
											}				
										}
										if(!Flag)
											FinalItemIds.push_back(vec_items[i]);
									}
								}
							}
							else
							{
								if(field1 != oTableValue.getTableTypeID())
									continue;

								if(FinalItemIds.size() == 0)
								{
									FinalItemIds = vec_items;
								}
								else
								{
									for(int32 i=0; i<vec_items.size(); i++)
									{	bool16 Flag = kFalse;
										for(int32 j=0; j<FinalItemIds.size(); j++)
										{
											if(vec_items[i] == FinalItemIds[j])
											{
												Flag = kTrue;
												break;
											}				
										}
										if(!Flag)
											FinalItemIds.push_back(vec_items[i]);
									}
								}
							}
						}//for tabelInfo end
						if(tableInfo)
							delete tableInfo;
					}
					else 
					{
						VectorScreenTableInfoPtr KXAItemtableInfo = NULL;
						if(isComponentAttributePresent == 1)
						{
							bool16 isKitTable = kFalse; // we are spraying component table.
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemID_or_productID, languageId, isKitTable); 
						}
						else if(isComponentAttributePresent == 2)
						{
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
						}
						else if(isComponentAttributePresent == 3)
						{
							//KXAItemtableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
						}

						if(!KXAItemtableInfo)
						{
							//CA("KittableInfo is NULL");
							break;
						}

						if(KXAItemtableInfo->size()==0){
							//CA(" KittableInfo->size()==0");
							FinalItemIdSizeIsZero = kTrue;
							break;
						}

						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it  ;						

						bool16 typeidFound=kFalse;
						vector<double> vec_items;
						FinalItemIds.clear();

						it = KXAItemtableInfo->begin();
						{//for tabelInfo start				
							oTableValue = *it;				
							vec_items = oTableValue.getItemIds();
						
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag)
										FinalItemIds.push_back(vec_items[i]);
								}
							}
						}//for tabelInfo end
						
						Kit_vec_tablerows = oTableValue.getTableData();
						if(KXAItemtableInfo)
							delete KXAItemtableInfo;
					}
				}
			}

			if(customTabbedTextTagPtr->GetAttributeValue(WideString("index")) == WideString("3"))
			{
				VectorScreenTableInfoPtr tableInfo=NULL;
				if(ptrIAppFramework->get_isONEsourceMode())
				{
					// For ONEsource mode
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(itemID_or_productID);
				}else
				{
					//For Publication
					tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, itemID_or_productID, languageId,  kTrue);
				}
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
					//ItemAbsentinProductFlag = kTrue;
					break;
				}
				if(tableInfo->size()==0)
				{ 
					//CA("tableInfo->size()==0");
					FinalItemIdSizeIsZero = kTrue;
					ptrIAppFramework->LogInfo("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo->size()==0");	
					//ItemAbsentinProductFlag = kTrue;
					break;
				}
				
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;

				bool16 typeidFound=kFalse;
				vector<double> vec_items;
				FinalItemIds.clear();

				PMString tableIDStr = customTabbedTextTagPtr->GetAttributeValue(WideString("tableId"));
				double tableID = tableIDStr.GetAsDouble();

				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{//for tabelInfo start		
					//CA("Inside for loop");
					oTableValue = *it;	
					
					if(tableID > 0)
						if(tableID != oTableValue.getTableID())
							continue;

					vec_items = oTableValue.getItemIds();
				
					if(field1 == -1)	//--------
					{
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
							}
						}
					}
					else
					{
						if(field1 != oTableValue.getTableTypeID())
							continue;

						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
							}
						}
					}

				}//for tabelInfo end
				if(tableInfo)
					delete tableInfo;
			}

		}while(false);

		if(FinalItemIds.size()<=0)
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText::FinalItemIds.size()<=0");
			break;
		}
				
		int32 finalItemIDsize =static_cast<int32> (FinalItemIds.size());

	///check whether header present or not in that box
		bool16 isHeaderPresentInBox = kFalse;
			
		int32 childCount = customTabbedTextTagPtr->GetChildCount();

		for(int i =0; i< childCount ;i++)
		{
			XMLReference childXMLReference = customTabbedTextTagPtr->GetNthChild(i);
			//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

			PMString header = childXMLElement->GetAttributeValue(WideString("header")); //Cs4
			if(header == "1")
			{	
				//CA("isHeaderPresentInBox = kTrue");
				isHeaderPresentInBox = kTrue;
				break;
			}
		}
	/////////////////////

	////count number of newline characters present in current box
		int32 newLineCharacters = 0;

		//InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
		//if(unknown == NULL)
		//{
		//	//CA("unknown == NULL");
		//	break;
		//}
		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			//CA("textFrameUID == kInvalidUID");
			break;
		}	
		//InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		//if (textFrame == NULL)
		//{
		//	//CA("textFrame == NULL");
		//	break;
		//}	

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}

		TextIndex startPos=0, endPos=0;
		Utils<IXMLUtils>()->GetElementIndices(customTabbedTextTagPtr, &startPos, &endPos);
		
		//PMString asdd("\nStart index = ");
		//asdd.AppendNumber(startPos);
		//asdd.Append("\n End index = ");
		//asdd.AppendNumber(endPos);
		//CA(asdd);

		//TextIndex startPos =-1;
		//TextIndex endPos = -1;

		//startPos = textFrame->TextStart();
		//endPos = startPos + textModel->GetPrimaryStoryThreadSpan() - 1;

		TextIterator begin(textModel , startPos + 1);
		TextIterator end(textModel , endPos - 1);

		for(TextIterator iter = begin ;iter <= end ; iter++)
		{
			 if(*iter == kTextChar_CR)
			 {
				newLineCharacters++;
			 }
		}

//		newLineCharacters = newLineCharacters - 1;//It is used to not consider the NewLine character which we inserted at end in DataSprayer.
//
		//PMString noOfNewLineCharacters("noOfNewLineCharacters = ");
		//noOfNewLineCharacters.AppendNumber(newLineCharacters);
		//CA(noOfNewLineCharacters);

		bool16 onlyOneLinePresent = kFalse; ///if only one line is present in text frame...
		if(newLineCharacters == 0)
		{
			onlyOneLinePresent = kTrue;
		}

	/////////////////////
		
	/////check whether number of items currently present in box is same as FinalItemIds size
	///if it is equal then there is no need to add or delete rows from box in case of headerPresent in box...
	///otherwise there is need to add or delete rows from the box.....
	
		bool16 isNeedToDeleteRows = kFalse;
		bool16 isNeedToAddRows = kFalse;
		int32 noOfLinesToBeAddedOrDeleted = 0;
		if(isHeaderPresentInBox)
		{
			if(finalItemIDsize != newLineCharacters)
			{
				if(finalItemIDsize < newLineCharacters)
				{
					isNeedToDeleteRows = kTrue;
					noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize;
				}
				else
				{
					isNeedToAddRows = kTrue;
					noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters;
				}
			}	
		}
		else
		{
			if(finalItemIDsize != newLineCharacters + 1)
			{
				if(finalItemIDsize <= newLineCharacters)
				{
					isNeedToDeleteRows = kTrue;
					if(FinalItemIdSizeIsZero == kTrue)
						noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize;
					else
						noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize + 1;
				}
				else
				{
					isNeedToAddRows = kTrue;
					noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters - 1;
				}
			}
		}

		//if(isNeedToAddRows)
		//{
		//	PMString noOfLinesToBeAdded("noOfLinesToBeAdded = ");
		//	noOfLinesToBeAdded.AppendNumber(noOfLinesToBeAddedOrDeleted);
		//	CA(noOfLinesToBeAdded);
		//}

		/*if(isNeedToDeleteRows)
		{
			PMString noOfLinesToBeDeleted("noOfLinesToBeDeleted = ");
			noOfLinesToBeDeleted.AppendNumber(noOfLinesToBeAddedOrDeleted);
			CA(noOfLinesToBeDeleted);
		}*/
	///////////////////////////////////////

	////add no of lines
		if(!onlyOneLinePresent && isNeedToAddRows)////if more than one lines are already present in text Frame
													//and we want to add more in that
		{
			//CA("!onlyOneLinePresent && isNeedToAddRows");
			int32 startIndexForCopy = startPos + 1/*0*/;
			
			int32 j = 0;
			for(TextIterator iter = begin ;iter <= end ; iter++)
			{
				if(*iter == kTextChar_CR)
				{
					j++;
					if(j == newLineCharacters)
					{
						startIndexForCopy = iter.Position();
						break;
					}
				}
			}
			
			int32 length = endPos - startIndexForCopy;
			//int32 length = endPos - startIndexForCopy;
			/*K2*/boost::shared_ptr< PasteData > pasteData;	//----CS5---
			
			for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
			{
				//PMString StartIndexStr("startIndexForCopy");
				//StartIndexStr.AppendNumber(startIndexForCopy);
				//StartIndexStr.Append(" , length = ");
				//StartIndexStr.AppendNumber(length);
				//StartIndexStr.Append(" , endPos = ");
				//StartIndexStr.AppendNumber(endPos);
				//CA(StartIndexStr);
				textModel->CopyRange(startIndexForCopy,length,pasteData);
				textModel->Paste(/*kFalse,*/endPos /*-1*/,pasteData);
				endPos = endPos + length;
			}		
		}

		if(onlyOneLinePresent && isNeedToAddRows)
		{
			//CA("onlyOneLinePresent && isNeedToAddRows");
			WideString* newLineCharacter = new WideString("\r");
			
			//int32 startIndexForCopy = 0;

			int32 startIndexForCopy = startPos + 1/*1*/;
			//int32 length = endPos -1 -startIndexForCopy;
			int32 length = endPos - startIndexForCopy;


			/*K2*/boost::shared_ptr< PasteData > pasteData;	//---CS5--
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kFalse);			
			}
			PMString a("noOfLinesToBeAddedOrDeleted : ");
			a.AppendNumber(noOfLinesToBeAddedOrDeleted);
			//CA(a);
			for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
			{
				////textModel->CopyRange(startIndexForCopy,length,pasteData);
				////textModel->Insert(/*kFalse,*/endPos -1,newLineCharacter);
				////textModel->Paste(/*kFalse,*/endPos,pasteData);
				////endPos = endPos + length;

				textModel->CopyRange(startIndexForCopy,length,pasteData);
				//textModel->Insert(kFalse,endPos -1,newLineCharacter);
				textModel->Insert(endPos ,newLineCharacter);
				//textModel->Paste(kFalse,endPos,pasteData);
				textModel->Paste(endPos+1,pasteData);
				//endPos = endPos + length;
				endPos = endPos+ 1 + length;
			}
			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kTrue);			
			}
			if(newLineCharacter)
				delete newLineCharacter;
		}

	/////Delete no of lines
		if(!onlyOneLinePresent && isNeedToDeleteRows)
		{
			//CA("!onlyOneLinePresent && isNeedToDeleteRows");
			if(isHeaderPresentInBox)
			{
				int32 startIndexForDelete = 0;
			
				int32 j = 0;
				for(TextIterator iter = begin ;iter <= end ; iter++)
				{
					if(*iter == kTextChar_CR)
					{
						j++;
						if(j == finalItemIDsize + 1)
						{
							startIndexForDelete = iter.Position();
							break;
						}
					}
				}

				int32 length = endPos - startIndexForDelete;

				textModel->Delete(startIndexForDelete,length);
			}
			else
			{
				int32 startIndexForDelete = 0;
			
				int32 j = 0;
				for(TextIterator iter = begin ;iter <= end ; iter++)
				{
					if(*iter == kTextChar_CR)
					{
						if(FinalItemIdSizeIsZero)
						{
							startIndexForDelete = iter.Position();
							break;
						}

						j++;
						if(j == finalItemIDsize)
						{
							startIndexForDelete = iter.Position();
							break;
						}
					}
				}


				//startIndexForDelete -= 1;
				int32 length = endPos - startIndexForDelete;
				
				PMString StartIndexStr("startIndexForDelete");
				StartIndexStr.AppendNumber(startIndexForDelete);
				StartIndexStr.Append(" , length = ");
				StartIndexStr.AppendNumber(length);
				
				//CA(StartIndexStr);

				textModel->Delete(startIndexForDelete,length);
			}
		}

//CA("Successfully added or deleted line in text frame");
		
	////////if FinalItemIdSizeIsZero then make all child tags of customTabbedText Impotent
		if(FinalItemIdSizeIsZero)
		{
			if(!isHeaderPresentInBox)
			{
				IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
				int32 newChildCount = customTabbedTextXMLElement->GetChildCount();

				

				for(int32 i = 0; i < newChildCount; i++)
				{
					//ptrIAppFramework->clearAllStaticObjects();
					XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(i);
					//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
					InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

					int32 tagStart = -1;
					int32 tagEnd = -1;
					Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStart,&tagEnd);
					tagStart = tagStart +1;
					tagEnd = tagEnd - 1;

					InterfacePtr<ITextStoryThread> textStoryThread(childXMLElement->QueryContentTextStoryThread());
					if(textStoryThread == NULL)
					{
						//CA("textStoryThread == NULL");
						break;
					}
					InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
					if(textModelptr == NULL)
					{
						//CA("textModel == NULL");
						break;
					}

					PMString str("");
					//WideString data(str);
					//textModelptr->Replace(tagStart,tagEnd-tagStart + 1,&data);
					boost::shared_ptr<WideString> insertText(new WideString(str));
					ReplaceText(textModelptr,tagStart,tagEnd-tagStart + 1 ,insertText);

					PMString itemID("-1");
					//childXMLElement->SetAttributeValue(WideString("typeId"),WideString(itemID)); //Cs4
					Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString("typeId"),WideString(itemID)); 

				}
			}
			break;
		}
	////////

	////////change typeID of all tags as per the FinalItemIds
		IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
		int32 newChildCount = customTabbedTextXMLElement->GetChildCount();

		int32 noOfTagsPresentPerRow = 0;
		
		if(isNeedToDeleteRows)
			noOfTagsPresentPerRow = newChildCount / (newLineCharacters - noOfLinesToBeAddedOrDeleted + 1);
		else
			noOfTagsPresentPerRow = newChildCount / (newLineCharacters + noOfLinesToBeAddedOrDeleted + 1);
		
	
		int32 index = 0;
		if(isHeaderPresentInBox)
			index = noOfTagsPresentPerRow;

		vector<double> ::iterator itr;
		itr = FinalItemIds.begin();


		for(int i = 0; i < finalItemIDsize; i++ ,itr++)
		{
			PMString itemID("");
			itemID.AppendNumber(PMReal(*itr));
			for(int j = 0 ; j < noOfTagsPresentPerRow ; j++)
			{

				XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(index);
				//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
				InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
							
				//childXMLElement->SetAttributeValue(WideString("typeId"),WideString(itemID)); //Cs4
				//childXMLElement->SetAttributeValue(WideString("childId"),WideString(itemID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString("childId"),WideString(itemID));
				index++;
			}		
		}

	///////////////////////
	
	////////now we can refresh each tags 
		VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
		VectorHtmlTrackerValue vectorObj ;
		vectorHtmlTrackerSPTBPtr = &vectorObj;

		int32 j = 0;//temp variable
		int32 sequenceNo = 0; //index of itemID present in FinalItemIds.
		PMString ss("newChildCount-- : ");
				ss.AppendNumber(newChildCount);
				//CA(ss);

		for(int32 i = 0; i < newChildCount; i++)
		{
			//ptrIAppFramework->clearAllStaticObjects();
			XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(i);
			//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
			
			PMString strTypeID = childXMLElement->GetAttributeValue(WideString("typeId")); //Cs4
			PMString strSectionID = childXMLElement->GetAttributeValue(WideString("sectionID"));
			PMString strElementID = childXMLElement->GetAttributeValue(WideString("ID"));
			PMString strLanguageID = childXMLElement->GetAttributeValue(WideString("LanguageID"));
			PMString strParentTypeID = childXMLElement->GetAttributeValue(WideString("parentTypeID"));
			//PMString strIndex = childXMLElement->GetAttributeValue("index");
			PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));

			PMString strHeader = childXMLElement->GetAttributeValue(WideString("header"));
			PMString strchildId = childXMLElement->GetAttributeValue(WideString("childId"));
			PMString strchildTag = childXMLElement->GetAttributeValue(WideString("childTag"));//parentID
			PMString strParentTag = childXMLElement->GetAttributeValue(WideString("parentID"));

			int32 header = strHeader.GetAsNumber();
			double childId = strchildId.GetAsDouble();
			int32 childTag = strchildTag.GetAsNumber();
			double parentId = strParentTag.GetAsDouble();

			double typeId = strTypeID.GetAsDouble();
			double sectionID = strSectionID.GetAsDouble();
			double elementID = strElementID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();
			double parentTypeID = strParentTypeID.GetAsDouble();
			int32 index = strIndex.GetAsNumber();
			/*PMString ww("parentID : ");
			ww.AppendNumber(parentId);
			ww.Append("\n");
			ww.Append("ChildId : ");
			ww.AppendNumber(childId);
			ww.Append("\n");
			ww.Append("AttributeId : ");
			ww.AppendNumber(elementID);
			ww.Append("\n");
			ww.Append("childTag : ");
			ww.AppendNumber(childTag);
			ww.Append("\n");
			ww.Append("Index : ");
			ww.AppendNumber(index);*/
			//CA(ww);

			PMString itemAttributeValue("");

			//CA("1234");
			//if(elementID==-786)
			//{
			//	itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parentId ,elementID, languageID, kTrue);
			//	//itemAttributeValue=ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(parentId ,elementID, languageID, kTrue);
			//	PMString ss("itemAttributeValue-- : ");
			//	ss.Append(itemAttributeValue);
			//	//CA(ss);
			//}
		
			
			if(/*typeId == -2*/header == 1)
			{
				if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
				{
					//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
					//if(TypeInfoVectorPtr != NULL)
					//{//CA("TypeInfoVectorPtr != NULL");
					//	VectorTypeInfoValue::iterator it3;
					//	int32 Type_id = -1;
					//	PMString temp = "";
					//	PMString name = "";
					//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
					//	{
					//		Type_id = it3->getTypeId();
					//		if(parentTypeID == Type_id)
					//		{
					//			temp = it3->getName();
					//			if(elementID == -701)
					//			{
					//				itemAttributeValue = temp;
					//			}
					//			else if(elementID == -702)
					//			{
					//				itemAttributeValue = temp + " Suffix";
					//			}
					//		}
					//	}
					//}
					//if(TypeInfoVectorPtr)
					//	delete TypeInfoVectorPtr;
				}
				else if(elementID == -703)
				{
					//CA("dispName = $Off");
					itemAttributeValue = "$Off";
				}
				else if(elementID == -704)
				{
					//CA("dispName = %Off");
					itemAttributeValue = "%Off";
				}
				else if(elementID == -805)
				{									
					itemAttributeValue = "Quantity";
                }
				else if(elementID == -806)
				{									
					itemAttributeValue = "Availablity";
                }
				else if(elementID == -807)
				{									
					itemAttributeValue = "Cross-reference Type";
                }
				else if(elementID == -808)
				{									
					itemAttributeValue = "Rating";
                }
				else if(elementID == -809)
				{									
					itemAttributeValue = "Alternate";
                }
				else if(elementID == -810)
				{									
					itemAttributeValue = "Comments";
                }
				else if(elementID == -811)
				{									
					itemAttributeValue = "";
                }
				else if(elementID == -812)
				{									
					itemAttributeValue = "Quantity";
                }
				else if(elementID == -813)
				{									
					itemAttributeValue = "Required";
                }
				else if(elementID == -814)
				{									
					itemAttributeValue = "Comments";
                }
				else
					itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
			}
			else
			{
				if(j < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
				{
					j++;
				}
				else
				{
					sequenceNo++;
					j = 1;
				}
				if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
				{
					//CA("elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704");
					PublicationNode pNode;
					pNode.setPubId(itemID_or_productID);

					//PMString strPBObjectID = childXMLElement->GetAttributeValue(WideString("rowno"));//Cs4
					PMString strPBObjectID = childXMLElement->GetAttributeValue(WideString("pbObjectId"));
					double rowno = strPBObjectID.GetAsDouble();
					pNode.setPBObjectID(strPBObjectID.GetAsDouble());
					if(index == 4)
					{
						pNode.setIsProduct(0);
					}
					else if(index == 3)
					{
						pNode.setIsProduct(1);
					}
					if(rowno != -1)
					{
						//CA("sETTING IsONEsource flag as false");
						pNode.setIsONEsource(kFalse);
					}
					TagStruct tagInfo;
					tagInfo.elementId = elementID;
					tagInfo.languageID = languageID;
					tagInfo.typeId = typeId;
					tagInfo.sectionID = sectionID;
					tagInfo.tagPtr = childXMLElement;
					tagInfo.header = header;
					tagInfo.childId = childId;
					tagInfo.childTag = childTag;
					tagInfo.parentTypeID = parentTypeID;
					
					handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,itemAttributeValue);
					//CA("itemAttributeValue = " + itemAttributeValue);
				}
				else if(elementID == -803)  // For 'Letter Key'
				{		
					//CA("26");
					itemAttributeValue.Clear();
					int wholeNo =  sequenceNo / 26;
					int remainder = sequenceNo % 26;								

					if(wholeNo == 0)
					{// will print 'a' or 'b'... 'z'  upto 26 item ids
						itemAttributeValue.Append(AlphabetArray[remainder]);
					}
					else  if(wholeNo <= 26)
					{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
						itemAttributeValue.Append(AlphabetArray[wholeNo-1]);	
						itemAttributeValue.Append(AlphabetArray[remainder]);	
					}
					else if(wholeNo <= 52)
					{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
						itemAttributeValue.Append(AlphabetArray[0]);
						itemAttributeValue.Append(AlphabetArray[wholeNo -26 -1]);	
						itemAttributeValue.Append(AlphabetArray[remainder]);										
					}
				}
				else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
				}
				else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
				}
				else if((elementID == -807) && (isComponentAttributePresent == 2))
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
                }
				else if((elementID == -808) && (isComponentAttributePresent == 2))
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
                }
				else if((elementID == -809) && (isComponentAttributePresent == 2))
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][2]);	
                }
				else if((elementID == -810) && (isComponentAttributePresent == 2))
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][3]);	
                }
				else if((elementID == -811) && (isComponentAttributePresent == 2))
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][4]);	
                }
				else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
				}
				else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
				}
				else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
				{
					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][2]);	
				}
				else
				{
					if((isComponentAttributePresent == 2) && ((Kit_vec_tablerows[sequenceNo][4]) == "N"))
					{
						//itemAttributeValue = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(/*typeId*/childId,elementID,languageID);
					}
					else if(childXMLElement->GetAttributeValue(WideString("isEventField")) == WideString("1"))
					{
						//CA("tagInfo.isEventField");
						/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(childId,elementID,sectionID);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								itemAttributeValue = vecPtr->at(0).getObjectValue();	
							delete vecPtr;
						}*/
						//CA("itemAttributeValue = " + itemAttributeValue);
					}
					else{
                        itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId,elementID, languageID, sectionID, /*kTrue*/kFalse );//23OCT09//Amit
					}
				}
			}
			int32 tagStartPos = -1;
			int32 tagEndPos = -1;
			Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			PMString textToInsert("");
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=itemAttributeValue;					
			}
			else						
			{
				//textToInsert=iConverter->translateString(itemAttributeValue);
				vectorHtmlTrackerSPTBPtr->clear();
				textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
				iConverter->ChangeQutationMarkONOFFState(kFalse);
			}	
			//WideString insertText(textToInsert);
			//textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);

			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kTrue);			
			}
		}
	}
	
	//leaks found here
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	result = kSuccess;
	}while(kFalse);
 	return result;
}


ErrorCode RefreshAllStandardTables(const UIDRef& boxID,TagStruct& tagInfo)
{
	//CA("inside RefreshAllStandardTables");
	ErrorCode result = kFailure;
	do
	{
		bool16 ItemAbsentinProductFlag = kFalse;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::!itagReader");	
			break;
		}
		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			//CA("Pointre to DataSprayerPtr not found");//
			return kFalse;
		}

		PublicationNode pNode;
		PMString PubName("PubName");
		pNode.setAll(PubName, tagInfo.parentId, tagInfo.sectionID,3,1,1,1,tagInfo.typeId,0,kTrue,kFalse);
		
		DataSprayerPtr->FillPnodeStruct(pNode, tagInfo.sectionID, 1, tagInfo.sectionID);

		VectorScreenTableInfoPtr tableInfo = NULL;
		if(pNode.getIsONEsource())
		{
		
			// For ONEsource mode
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		}else
		{
			//For publication mode 
			tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID , pNode.getPubId(), tagInfo.languageID);
		}

		if(!tableInfo)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::tableInfo is NULL");	
			ItemAbsentinProductFlag = kTrue;
			break;
		}

		CItemTableValue oTableValue1;
		vector<double> vec_items1;
		vector<double> vec_tableTypeID;
		VectorScreenTableInfoValue::iterator it;
		for(it = tableInfo->begin(); it != tableInfo->end(); it++)
		{	
			oTableValue1 = *it;
			vec_tableTypeID.push_back(oTableValue1.getTableTypeID());

			if(it == tableInfo->begin())
				vec_items1 = oTableValue1.getItemIds();
		}


		
	
		IIDXMLElement *AllStandardTablesTagPtr = tagInfo.tagPtr;

		////count number of newline characters present in current box
		int32 newLineCharacters = 0;

		//InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
		//if(unknown == NULL)
		//{
		//	//CA("unknown == NULL");
		//	break;
		//}
		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			//CA("textFrameUID == kInvalidUID");
			break;
		}	
		//InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		//if (textFrame == NULL)
		//{
		//	//CA("textFrame == NULL");
		//	break;
		//}	

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == NULL)
		{
			//CA("textModel == NULL");
			break;
		}

		TextIndex startPos=0, endPos=0;
		Utils<IXMLUtils>()->GetElementIndices(AllStandardTablesTagPtr, &startPos, &endPos);
		
		//PMString asdd("\nStart index = ");
		//asdd.AppendNumber(startPos);
		//asdd.Append("\n End index = ");
		//asdd.AppendNumber(endPos);
		//CA(asdd);

		//TextIndex startPos =-1;
		//TextIndex endPos = -1;

		//startPos = textFrame->TextStart();
		//endPos = startPos + textModel->GetPrimaryStoryThreadSpan() - 1;

		TextIterator begin(textModel , startPos + 1);
		TextIterator end(textModel , endPos - 1);

		for(TextIterator iter = begin ;iter <= end ; iter++)
		{
			 if(*iter == kTextChar_CR)
			 {
				newLineCharacters++;
			 }
		}

		int32 childCount = AllStandardTablesTagPtr->GetChildCount();
		bool16 headerPresentInTheRow = kFalse;
		bool16 itemAttributePresent = kFalse;
		for(int32 index = 0 ; index < childCount ; index++ )
		{
			XMLReference childXMLReference = AllStandardTablesTagPtr->GetNthChild(index);
			//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
			InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());

			PMString header = childXMLElement->GetAttributeValue(WideString("header")); //Cs4
			if(header == "1")
				headerPresentInTheRow = kTrue;

			PMString whichTab = childXMLElement->GetAttributeValue(WideString("index")); //Cs4
			if(whichTab == "4")
			{
				itemAttributePresent = kTrue;
				break;
			}
		}
		
		if(itemAttributePresent)
		{
			int32 newLineChar = 0;
			int32 startIndexForDelete = -1;
			int32 tableNameStartIndex = 0;
			int32 tableNameEndIndex = 0;
			int32 itemAttributesStartIndex = -1;
			int32 itemAttributesEndIndex = 0;
			int32 numberOfTagsInALine = 0;
			bool16 ItemAbsentinProductFlag = kFalse;

			for(TextIterator iter = begin ;iter <= end ; iter++)
			{
				 if(*iter == kTextChar_CR)
				 {
					newLineChar++;
					if(headerPresentInTheRow)
					{
						if(newLineChar == 3)
						{						
							startIndexForDelete = iter.Position();
							break;
						}
					}
					else 
					{
						if(newLineChar == 2)
						{						
							startIndexForDelete = iter.Position();
							break;
						}
					}
				 }
			}
			
			int32 lengthToBeDelete = endPos - startIndexForDelete;
			
			if(startIndexForDelete != -1)
				textModel->Delete(startIndexForDelete,lengthToBeDelete);		//Deleting tags other than First Table Name And item Attributes present in the line belo table name
			
			TextIndex startPos1=0, endPos1=0;
			Utils<IXMLUtils>()->GetElementIndices(AllStandardTablesTagPtr, &startPos1, &endPos1);
			
					
			int32 childCount = AllStandardTablesTagPtr->GetChildCount();
			for(int32 index = 0 ; index < childCount ; index++ )
			{
				XMLReference childXMLReference = AllStandardTablesTagPtr->GetNthChild(index);
				//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
				InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
				PMString tagName = childXMLElement->GetTagString();

				if(tagName.Compare(kTrue,"Standard_Table")==0)
				{
					//CA("tagName.Compare(kTrue,Standard_Table)==0");
					TextIndex startPos2=0, endPos2=0;
					Utils<IXMLUtils>()->GetElementIndices(childXMLElement, &startPos2, &endPos2);
					tableNameStartIndex = startPos2;
					tableNameEndIndex = endPos2+1;
				}
				else if(childXMLElement->GetAttributeValue(WideString("header")) == WideString("1") )
				{
					TextIndex startPos2=0, endPos2=0;
					Utils<IXMLUtils>()->GetElementIndices(childXMLElement, &startPos2, &endPos2);
					itemAttributesStartIndex = endPos2+1;
				}

			}
			if(itemAttributesStartIndex == -1)
				itemAttributesStartIndex = tableNameEndIndex;
			itemAttributesEndIndex = endPos1;
			
			int32 startPasteIndex = endPos1;

			TagList tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			int numTags=static_cast<int>(tList.size());
			if(numTags<0)
			{
				ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::numTags<0");	
				return kFalse;
			}

//PMString a("tList.size()	:	");
//a.AppendNumber(tList.size());
//CA(a);

			//Find all the tags which are in the current line.
			for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
			{
				//CA("Inside numTags For loop ");
				TagStruct & tagInfo = tList[normalTagIndex];
				IIDXMLElement * tagPtr = tagInfo.tagPtr;
				int32 startPosn=-1;
				int32 endPosn = -1;
				Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPosn,&endPosn);

				if(startPosn >= itemAttributesStartIndex && endPosn <= itemAttributesEndIndex)
				{
					//CA("startPos >= lineStartCharIndex && endPos <= lineEndCharIndex");
					numberOfTagsInALine++;
					if(headerPresentInTheRow)
					{
						//CA("headerPresentInTheRow");
						PMString attrIndex("index");
						PMString indexValue  = tagPtr->GetAttributeValue(WideString(attrIndex));
						PMString strTableFlag = tagPtr->GetAttributeValue(WideString("tableFlag"));
						if(strTableFlag == "-15")
						{
							continue;
						}
						else if(indexValue == "4" && strTableFlag != "1")
						{																
							
							//tagPtr->SetAttributeValue(WideString("header"),WideString("1")); //Cs4
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(), WideString("header"),WideString("1"));
							//change  the tableFlag to -12 for item_copy_attributes in text frame.
							//tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-12")); //Cs4
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(),WideString("tableFlag"),WideString("-12")); 
						}
						else if(indexValue == "3" && strTableFlag != "1") 
						{
							
							//Header For nonItemCopyAttributes
							//tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-13"));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(),WideString("tableFlag"),WideString("-13"));
						}
						//start under test
						else if(indexValue == "3" && strTableFlag == "1")
						{
							//tagPtr->SetAttributeValue(WideString("ID"),WideString("-103"));	
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(),WideString("ID"),WideString("-103"));	
						}
					}
					///end under test
					else
					{
						//CA("else");	

						PMString attrIndex("index");
						PMString indexValue  = tagPtr->GetAttributeValue(WideString(attrIndex));
						PMString strTableFlag = tagPtr->GetAttributeValue(WideString("tableFlag"));	

						//added on 22Sept..EventPrice added.
						PMString strID = tagPtr->GetAttributeValue(WideString("ID"));
						//ended on 22Sept..EventPrice added.
						if(strTableFlag == "-15")
						{
							continue;
						}
						//added on 22Sept..EventPrice added.
						else if(indexValue == "4" && strTableFlag != "1" /*&& strID == "-701"*/)
						{
							//CA("indexValue == 4 && strTableFlag != 1");																					
							PMString attributeValue;

							attributeValue.AppendNumber(PMReal(vec_items1[0]));
							//tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(),WideString("typeId"),WideString(attributeValue));
							//change  the tableFlag to -12 for item_copy_attributes in text frame.
							//tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-12"));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(), WideString("tableFlag"),WideString("-12"));

							if(tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
							{
								//CA("Here 3");
								attributeValue.Clear();										
								attributeValue.Append("a");												
								//tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagPtr->GetXMLReference(),WideString("rowno"),WideString(attributeValue));
							}
						}								
					}						
				}
			}
//CA_NUM("numberOfTagsInALine : ",numberOfTagsInALine);
			
			UIDRef txtModelUIDRef = UIDRef::gNull;
			txtModelUIDRef = ::GetUIDRef(textModel);

			vector<double> FinalItemIds;
			TextIndex destEnd = startPasteIndex;
			vector<double> vec_items;
			int32 i = 0;
			for(it = tableInfo->begin(); it != tableInfo->end(); it++,i++)
			{	
				oTableValue1 = *it;
				vec_items = oTableValue1.getItemIds();

				/*K2*/boost::shared_ptr< PasteData > pasteData;	//---CS5--
				
				if(tList[0].tagPtr->GetTagString() == WideString("Standard_Table")) //Cs4
				{
//CA("isTableNamePresent");
					if(i != 0)
					{
					
						//CA("i != 0");
						do{
							InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
							if (copyStoryRangeCmd == nil) 
							{
								//CA("copyStoryRangeCmd == nil");
								break;
							}
	
							// Refer the command to the source story and range to be copied.
							InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceUIDData == nil) 
							{
								//CA("sourceUIDData == nil");
								break;
							}
						
							sourceUIDData->Set(txtModelUIDRef);
							InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceRangeData == nil) 
							{
								//CA("sourceRangeData == nil");
								break;
							}
							//PMString z("tableNameStartIndex	:	");
							//z.AppendNumber(tableNameStartIndex);
							//z.Append("\r");
							//z.Append("tableNameEndIndex	:	");
							//z.AppendNumber(tableNameEndIndex);
							//CA(z);
					
							sourceRangeData->Set(tableNameStartIndex, tableNameEndIndex);

							// Refer the command to the destination story and the range to be replaced.
							UIDList itemList(txtModelUIDRef);
							copyStoryRangeCmd->SetItemList(itemList);
							
							WideString* newLineCharacter = new WideString("\r");
							textModel->Insert(startPasteIndex ,newLineCharacter);
							startPasteIndex++;
							InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

							destRangeData->Set(startPasteIndex, destEnd);
	
							// Process CopyStoryRangeCmd
							ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

							if(newLineCharacter)
								delete newLineCharacter;

						}while(kFalse);
	
						startPasteIndex = startPasteIndex + (tableNameEndIndex - tableNameStartIndex);
						destEnd = startPasteIndex;	
					
					}
				}

				for(int k1 = 0 ; k1 < vec_items.size() ; k1++)
				{
					if(i == 0 && k1 == 0 )
						FinalItemIds.push_back(vec_items[k1]);
					else
					{
						//CA("Copying");
						do{
							InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
							if (copyStoryRangeCmd == nil) 
							{
								//CA("copyStoryRangeCmd == nil");
								break;
							}
	
							// Refer the command to the source story and range to be copied.
							InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceUIDData == nil) 
							{
								//CA("sourceUIDData == nil");
								break;
							}
						
							sourceUIDData->Set(txtModelUIDRef);
							InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceRangeData == nil) 
							{
								//CA("sourceRangeData == nil");
								break;
							}
							//PMString z("itemAttributeStartIndex	:	");
							//z.AppendNumber(itemAttributeStartIndex);
							//z.Append("\r");
							//z.Append("itemAttributsEndIndex	:	");
							//z.AppendNumber(itemAttributsEndIndex);
							//CA(z);
						//	sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);
							sourceRangeData->Set(itemAttributesStartIndex, itemAttributesEndIndex);

							// Refer the command to the destination story and the range to be replaced.
							UIDList itemList(txtModelUIDRef);
							copyStoryRangeCmd->SetItemList(itemList);
							
							InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

							destRangeData->Set(startPasteIndex, destEnd );
	
							// Process CopyStoryRangeCmd
							ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

							FinalItemIds.push_back(vec_items[k1]);

						}while(kFalse);
						startPasteIndex = startPasteIndex + itemAttributesEndIndex - itemAttributesStartIndex;
						destEnd = startPasteIndex;

					}
				}
			}			

//CA("Copied.........");

			TagList newTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			if( ((static_cast<int32>(newTagList.size())) < 0))
			{
				ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::newTagList.size(<0");
				return kFalse;
			}
			
			int32 pastedTagCount = 0;
			int32 tableNameIndex = 0;
	//PMString q("newTagList.size()	:	");
	//q.AppendNumber(newTagList.size());
	//CA(q);
			for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
			{
	//CA("inside newTagList.size() for loop");
				TagStruct & newTagInfo = newTagList[newTagIndex];
				//IIDXMLElement * newXMLTagPtr = newTagInfo.tagPtr;
				if(newTagInfo.tagPtr->GetTagString() == WideString("Standard_Table")) //Cs4
				{
	//CA("Table name.....");
	//PMString q("typeId	:	");
	//q.AppendNumber(vec_tableTypeID[tableNameIndex]);
	//CA(q);
					PMString attributeValue;
					attributeValue.AppendNumber(PMReal(vec_tableTypeID[tableNameIndex]));
					//newTagInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("typeId"),WideString(attributeValue));

					tableNameIndex++;
					continue;
				}
				int32 newStartPos = -1;
				int32 newEndPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);

				if(newStartPos >= itemAttributesStartIndex && newEndPos <= startPasteIndex-1)
				{				
	//CA("Item Attribute.......");
					PMString attrIndex("index");
					PMString typeID = newTagInfo.tagPtr->GetAttributeValue(WideString("typeId"));
					
					PMString indexValue  = newTagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString strTableFlag = newTagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
					//added on 22Sept..EventPrice addition.
					PMString strID = newTagInfo.tagPtr->GetAttributeValue(WideString("ID"));
					//ended on 22Sept..EventPrice addition.
					double itemID =-1;
					//CA_NUM("pastedTagCount / numberOfTagsInALine : ",pastedTagCount / numberOfTagsInALine);				
					int32 itemIdSeqNo = 0;
					
					itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine];// + 1];
					itemIdSeqNo = ((pastedTagCount / numberOfTagsInALine));// +1 );
				
					PMString attributeValue;
					attributeValue.AppendNumber(PMReal(itemID));
					if(strTableFlag == "-15")
					{
						continue;
					}
					else if(indexValue == "4" && strTableFlag != "1" )
					{
						//CA("indexValue == 4 && strTableFlag != 1");
						//PMString  attributeIDfromNotes = "-1";
						//bool8 isNotesValid =  getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
						//newXMLTagPtr->SetAttributeValue("ID",attributeIDfromNotes);
						//attach the itemID to typeID as usual.

						//newTagInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("typeId"),WideString(attributeValue));
						//change  the tableFlag to -12 for item_copy_attributes in text frame.
						//newTagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-12")); //Cs4
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("tableFlag"),WideString("-12"));

						//	CA("Here 1");
						if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803")) //Cs4
						{
							//CA("Here 2");
							/*attributeValue.Clear();
							if(itemIdSeqNo < 26)
								attributeValue.Append(AlphabetArray[itemIdSeqNo]);
							else
								attributeValue.Append("a");	*/	
							attributeValue.Clear();
							int wholeNo =  itemIdSeqNo / 26;
							int remainder = itemIdSeqNo % 26;								

							if(wholeNo == 0)
							{// will print 'a' or 'b'... 'z'  upto 26 item ids
								attributeValue.Append(AlphabetArray[remainder]);
							}
							else  if(wholeNo <= 26)
							{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
								attributeValue.Append(AlphabetArray[wholeNo-1]);	
								attributeValue.Append(AlphabetArray[remainder]);	
							}
							else if(wholeNo <= 52)
							{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
								attributeValue.Append(AlphabetArray[0]);
								attributeValue.Append(AlphabetArray[wholeNo -26 -1]);	
								attributeValue.Append(AlphabetArray[remainder]);										
							}
							//newTagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("rowno"),WideString(attributeValue));
						}
					}
					else if(indexValue == "3" && strTableFlag != "1")
					{
						//newTagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("0"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("tableFlag"),WideString("0"));
					}
					//start under test
					else if(indexValue == "3" && strTableFlag == "1")
					{
						//newTagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-1"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(), WideString("ID"),WideString("-1"));
					}
					//end under test

					pastedTagCount++;
					
				}
			}
			//------------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
			{
				newTagList[tagIndex].tagPtr->Release();
			}
		}
		else
		{
			int32 newLineChar = 0;
			int32 startIndexForDelete = -1;
			int32 tableNameStartIndex = 0;
			int32 tableNameEndIndex = 0;
					
			for(TextIterator iter = begin ;iter <= end ; iter++)
			{
				 if(*iter == kTextChar_CR)
				 {
					newLineChar++;
					if(newLineChar == 1)
					{						
						startIndexForDelete = iter.Position();
						break;
					}
				 }
			}
			
			int32 lengthToBeDelete = endPos - startIndexForDelete;
			
			if(startIndexForDelete != -1)
				textModel->Delete(startIndexForDelete,lengthToBeDelete);		//Deleting tags other than First Table Name
			
			TextIndex startPos1 = 0, endPos1 = 0;
			Utils<IXMLUtils>()->GetElementIndices(AllStandardTablesTagPtr, &startPos1, &endPos1);
			int32 childCount = AllStandardTablesTagPtr->GetChildCount();
			for(int32 index = 0 ; index < childCount ; index++ )
			{
				XMLReference childXMLReference = AllStandardTablesTagPtr->GetNthChild(index);
				//IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
				InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
				PMString tagName = childXMLElement->GetTagString();

				if(tagName.Compare(kTrue,"Standard_Table")==0)
				{
					//CA("tagName.Compare(kTrue,Standard_Table)==0");
					TextIndex startPos2=0, endPos2=0;
					Utils<IXMLUtils>()->GetElementIndices(childXMLElement, &startPos2, &endPos2);
					tableNameStartIndex = startPos2;
					tableNameEndIndex = endPos2+1;
				}
			}
			
			TagList tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			int numTags=static_cast<int32>(tList.size());
			if(numTags<0)
			{
				ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::numTags<0");	
				return kFalse;
			}
			
			int32 startPasteIndex = endPos1;

			UIDRef txtModelUIDRef = UIDRef::gNull;
			txtModelUIDRef = ::GetUIDRef(textModel);

			TextIndex destEnd = startPasteIndex;
			
			int32 i = 0;
			for(it = tableInfo->begin(); it != tableInfo->end(); it++,i++)
			{	
				oTableValue1 = *it;
				
				/*K2*/boost::shared_ptr< PasteData > pasteData;	//---CS5--
				
				if(tList[0].tagPtr->GetTagString() == WideString("Standard_Table"))
				{
//CA("isTableNamePresent");
					if(i != 0)
					{
					
						//CA("i != 0");
						do{
							InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
							if (copyStoryRangeCmd == nil) 
							{
								//CA("copyStoryRangeCmd == nil");
								break;
							}
	
							// Refer the command to the source story and range to be copied.
							InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceUIDData == nil) 
							{
								//CA("sourceUIDData == nil");
								break;
							}
						
							sourceUIDData->Set(txtModelUIDRef);
							InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
							if (sourceRangeData == nil) 
							{
								//CA("sourceRangeData == nil");
								break;
							}
							//PMString z("tableNameStartIndex	:	");
							//z.AppendNumber(tableNameStartIndex);
							//z.Append("\r");
							//z.Append("tableNameEndIndex	:	");
							//z.AppendNumber(tableNameEndIndex);
							//CA(z);
					
							sourceRangeData->Set(tableNameStartIndex, tableNameEndIndex);

							// Refer the command to the destination story and the range to be replaced.
							UIDList itemList(txtModelUIDRef);
							copyStoryRangeCmd->SetItemList(itemList);
							
							WideString* newLineCharacter = new WideString("\r");
							textModel->Insert(startPasteIndex ,newLineCharacter);
							startPasteIndex++;
							InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

							destRangeData->Set(startPasteIndex, destEnd);
	
							// Process CopyStoryRangeCmd
							ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

							if(newLineCharacter)
								delete newLineCharacter;

						}while(kFalse);
	
						startPasteIndex = startPasteIndex + (tableNameEndIndex - tableNameStartIndex);
						destEnd = startPasteIndex;	
					
					}
				}
			}
			TagList newTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			if( ((static_cast<int32>(newTagList.size())) < 0))
			{
				ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::newTagList.size(<0");	
				return kFalse;
			}
			
			int32 tableNameIndex = 0;
	//PMString q("newTagList.size()	:	");
	//q.AppendNumber(newTagList.size());
	//CA(q);
			for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
			{
	//CA("inside newTagList.size() for loop");
				TagStruct & newTagInfo = newTagList[newTagIndex];
			//	IIDXMLElement * newXMLTagPtr = newTagInfo.tagPtr;
				if(newTagInfo.tagPtr->GetTagString() == WideString("Standard_Table")) //Cs4
				{
	//CA("Table name.....");
	//PMString q("typeId	:	");
	//q.AppendNumber(vec_tableTypeID[tableNameIndex]);
	//CA(q);
					PMString attributeValue;
					attributeValue.AppendNumber(PMReal(vec_tableTypeID[tableNameIndex]));
					//newTagInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfo.tagPtr->GetXMLReference(),WideString("typeId"),WideString(attributeValue));

					tableNameIndex++;
				}
			}
			//-------lalit-----
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
			{
				newTagList[tagIndex].tagPtr->Release();
			}
		}

//CA("Going for Spray");
		sprayForAllStandardTable(boxID,pNode,textModel);
			
	//	}
 
		result = kSuccess;
	}while(kFalse);
 	return result;
}
void sprayForAllStandardTable(UIDRef boxUIDRef,PublicationNode pNode,InterfacePtr<ITextModel> textModel)
{
//CA("inside sprayForAllStandardTable");
	do
	{	
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunction::RefreshAllStanardTables::!itagReader");	
			break;
		}

		TagList tList=itagReader->getTagsFromBox(boxUIDRef);
		//if(tList.size()<0)
        if( ((static_cast<int32>(tList.size())) < 0))
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunction::sprayForAllStandardTable::tList.size()<0");	
			return;
		}
		for(int32 index = 0 ; index < tList.size(); index++)
		{
			if(tList[index].tableType == 7)
			{
				//CA("tList[index].typeId == -116");
				TagStruct tagInfo = tList[index];
				PMString attributeValue1("");
				PMString attributeName1 = "parentID";
				attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
				//tagInfo.tagPtr->SetAttributeValue(WideString(attributeName1),WideString(attributeValue1)); //Cs4
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo.tagPtr->GetXMLReference(), WideString(attributeName1),WideString(attributeValue1));
				attributeName1.Clear();
				attributeValue1.Clear();

				int32 childCount  = tagInfo.tagPtr->GetChildCount();
				
				/*PMString ChildCount("childCount	:	");
				ChildCount.AppendNumber(childCount);
				CA(ChildCount);*/


				double elementId; 
				double typeId;
				double parentId; 
				int16 whichTab; 
				int32 imgFlag; 
				double sectionID; 
				double childParentTypeID; 
				double languageID;
				int32 isAutoResize;
				double rowno;
				double colno;
				double childId;
				//bool16 isTablePresent;
				TagStruct childTagInfo;
				
				for(int j = 0; j < childCount ; j++)
				{
					//CA("Child");
					XMLReference childXMLReference = tagInfo.tagPtr->GetNthChild(j);
					//IIDXMLElement * childReference = childXMLReference.Instantiate();
					InterfacePtr<IIDXMLElement>childReference(childXMLReference.Instantiate());

					PMString temp("");
					temp = childReference->GetAttributeValue(WideString("ID")); //Cs4
					elementId = temp.GetAsDouble();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("typeId"));
					typeId = temp.GetAsDouble();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("parentID")); 
					parentId = temp.GetAsDouble();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("index")); 
					whichTab = temp.GetAsNumber();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("imgFlag"));
					imgFlag = temp.GetAsNumber();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("sectionID"));
					sectionID = temp.GetAsDouble();
					temp.Clear();			
						
					temp = childReference->GetAttributeValue(WideString("parentTypeID"));
					childParentTypeID = temp.GetAsDouble();
					temp.Clear();					
					
					temp = childReference->GetAttributeValue(WideString("LanguageID"));
					languageID = temp.GetAsDouble();
					temp.Clear();			
					
					temp = childReference->GetAttributeValue(WideString("isAutoResize"));
					isAutoResize = temp.GetAsNumber();
					temp.Clear();			
					
					temp = childReference->GetAttributeValue(WideString("rowno"));
					rowno = temp.GetAsDouble();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("colno"));
					colno = temp.GetAsDouble();
					temp.Clear();

					temp = childReference->GetAttributeValue(WideString("childId"));
					childId = temp.GetAsDouble();
					temp.Clear();

					PMString strheader = childReference->GetAttributeValue(WideString("header"));
					int32 header = strheader.GetAsNumber();

					
					childTagInfo.elementId = elementId;
					childTagInfo.typeId = typeId;
					childTagInfo.parentId = parentId;
					childTagInfo.whichTab = whichTab;
					childTagInfo.imgFlag = imgFlag;
					childTagInfo.sectionID = sectionID;
					childTagInfo.parentTypeID = childParentTypeID;
					childTagInfo.isAutoResize = isAutoResize;
					childTagInfo.rowno = rowno;
					childTagInfo.colno = colno;
					childTagInfo.tagPtr = childReference; 
					childTagInfo.childId = childId;
					childTagInfo.header = header;
					
					
					PMString itemAttributeValue("");
					if(header == 1)
					{	
						double sectionid = sectionID;
						
						
						//CA("tagInfo.typeId == -2");
						//For table header attribute.						
						//added by Tushar on 27/12/06	
						//CA_NUM("tagInfo.parentTypeID : ",tagInfo.parentTypeID);
						if(elementId == -701 || elementId == -702)
						{
							//int32 parentTypeID = tagInfo.parentTypeID;
							/*int32 parentTypeID = childParentTypeID;
							
							VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
							if(TypeInfoVectorPtr != NULL)
							{
								VectorTypeInfoValue::iterator it3;
								int32 Type_id = -1;
								PMString temp = "";
								PMString name = "";
								for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
								{
									Type_id = it3->getTypeId();
									if(parentTypeID == Type_id)
									{
										temp = it3->getName();
										if(elementId == -701)
										{
											itemAttributeValue = temp;
										}
										else if(elementId == -702)
										{
											itemAttributeValue = temp + " Suffix";
										}
									}
								}
							}*/
						}
						else if(elementId == -703)
						{
							itemAttributeValue = "$Off";
						}
						else if(elementId == -704)
						{
							itemAttributeValue = "%Off";
						}	
						else if(elementId == -805 || elementId == -806 || elementId == -807 || elementId == -808|| elementId == -809 || elementId == -810 || elementId == -811 || elementId == -812 || elementId == -813 || elementId == -814)
						{
							itemAttributeValue = childReference->GetAttributeValue(WideString("rowno")); //Cs4
						}						
						else	
							itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );
						

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(childReference,&tagStartPos,&tagEndPos);
						
						VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
						VectorHtmlTrackerValue vectorObj ;
						vectorHtmlTrackerSPTBPtr = &vectorObj;

						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=itemAttributeValue;					
						}
						else						
						{
							//textToInsert=iConverter->translateString(itemAttributeValue);
							vectorHtmlTrackerSPTBPtr->clear();
							textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
						}	
						//CA("textToInsert : " +textToInsert);

						//WideString insertText(textToInsert);						
						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);

					}//end table header
					else
					{
						//int32 elementId = tagInfo.elementId;
						//CA_NUM("childTagInfo.typeId : ",childTagInfo.typeId);
						//CA_NUM("childTagInfo.elementId : ",childTagInfo.elementId);
						//CA_NUM("childTagInfo.whichTab : ",childTagInfo.whichTab);
						//CA_NUM("tagInfo.parentTypeID : ",tagInfo.parentTypeID);
						

						double sectionid = sectionID;
						

						if(childTagInfo.dataType == 4/*childTagInfo.elementId == -121*/)
						{
							//CA("childTagInfo.dataType == 4");
							PMString textToInsert("");				
						


							if(childTagInfo.whichTab == 3 || childTagInfo.whichTab == 4)
							{
								double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		
				
												
								VectorScreenTableInfoPtr tableInfo = NULL;
								
								if(pNode.getIsONEsource())
								{
									// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
									//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
								}else
								{
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
									//CA("Not ONEsource");
                                    if(childTagInfo.whichTab == 3 )
                                    {
                                        tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, languageID, kTrue);
                                    }
                                    else if(childTagInfo.whichTab == 4 )
                                    {
                                        tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(objectId, sectionid, languageID);
                                    }
								}
								if(!tableInfo)
								{
									//CA("!tableInfo");
									ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!tableInfo");
									return;
								}

								if(tableInfo->size()==0){
									//CA(" tableInfo->size()==0");
									ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
									break;
								}

								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;

								bool16 typeidFound=kFalse;
								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									oTableValue = *it;
									if(oTableValue.getTableTypeID() == childTagInfo.typeId)
									{   				
										typeidFound=kTrue;				
										break;
									}
								}

								if(typeidFound)
								{
									textToInsert = oTableValue.getName();	
								}
								else
								{
									ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!typeidFound");
									textToInsert = "";
								}


								if(tableInfo)
									delete tableInfo;
							}
					//CA("textToInsert = " + textToInsert);

							TextIndex startPos=-1;
							TextIndex endPos = -1;

							int32 tagStartPos = 0;
							int32 tagEndPos =0; 			
							Utils<IXMLUtils>()->GetElementIndices(childTagInfo.tagPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;
						
							//WideString insertText(textToInsert);
							//textModel->Replace(tagStartPos,tagEndPos- tagStartPos+1,&insertText);					
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);
						}
						else
						{
							
							//CA("Spraying Normal item details");
							itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(/*typeId*/childId,elementId, languageID, sectionid, kFalse );
							
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(childTagInfo.tagPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;
							
							VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
							VectorHtmlTrackerValue vectorObj ;
							vectorHtmlTrackerSPTBPtr = &vectorObj;

							PMString textToInsert("");
							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								textToInsert=itemAttributeValue;					
							}
							else						
							{
								//textToInsert=iConverter->translateString(itemAttributeValue);
								vectorHtmlTrackerSPTBPtr->clear();
								textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
							}	
						//CA("textToInsert : " +textToInsert);

							//WideString insertText(textToInsert);
							//textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
                            textToInsert.ParseForEmbeddedCharacters();
							boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);
							
						}		
				
					}
				}
				
			}
		}
		//--------lalit------
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}

	}while(kFalse);
}

/////////////////// new code end
//////////////ErrorCode RefreshCustomTabbedText(const UIDRef& boxID,TagStruct& tagInfo)
//////////////{
//////////////	//CA("inside RefreshCustomTabbedText");
//////////////	ErrorCode result = kFailure;
//////////////	do
//////////////	{
//////////////		
//////////////		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//////////////		if(ptrIAppFramework == NULL)
//////////////		{
//////////////			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//////////////			break;
//////////////		}
////////////////CA(" tag string = "+tagInfo.tagPtr->GetTagString());
//////////////		vector<int32> FinalItemIds;
//////////////
//////////////		IIDXMLElement *customTabbedTextTagPtr = tagInfo.tagPtr;
//////////////PMString tagName=customTabbedTextTagPtr->GetTagString();
//////////////CA(tagName);
//////////////		PMString strParentID =  customTabbedTextTagPtr->GetAttributeValue("parentID");
//////////////		PMString strLanguageID =  customTabbedTextTagPtr->GetAttributeValue("LanguageID");
//////////////		PMString strSectionID = customTabbedTextTagPtr->GetAttributeValue("sectionID");
//////////////
//////////////		int32 itemID_or_productID = strParentID.GetAsNumber();
//////////////		int32 languageId = strLanguageID.GetAsNumber();
//////////////		int32 sectionID = strSectionID.GetAsNumber();
//////////////
//////////////		bool16 isSectionLevelItemItemPresent = kFalse;
//////////////		int32 isComponentAttributePresent = 0;
//////////////		vector<vector<PMString> > Kit_vec_tablerows;  //To store Component's 'tableData' so that we can use
//////////////		
//////////////	////get all itemIDs of section level item or product.............
//////////////		if(customTabbedTextTagPtr->GetAttributeValue("index") == "4")
//////////////		{
//////////////			//isSectionLevelItemItemPresent = kFalse;
//////////////			if(customTabbedTextTagPtr->GetAttributeValue("colno") == "-101" && customTabbedTextTagPtr->GetAttributeValue("tableFlag")!= "1")
//////////////			{
//////////////				isSectionLevelItemItemPresent = kTrue;
//////////////				//CA("SectionLevelItemItemPresent == kTrue");
//////////////				if(customTabbedTextTagPtr->GetAttributeValue("rowno") == "-901")
//////////////					isComponentAttributePresent = 1;
//////////////				else if(customTabbedTextTagPtr->GetAttributeValue("rowno") == "-902")
//////////////					isComponentAttributePresent = 2;
//////////////				else if(customTabbedTextTagPtr->GetAttributeValue("rowno") == "-903")
//////////////					isComponentAttributePresent = 3;
//////////////			}
//////////////
//////////////			if(isSectionLevelItemItemPresent == kTrue)
//////////////			{
//////////////				/////get pub_object_id
//////////////				CPbObjectValue* ptrCPbObjectValue =ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(tagInfo.sectionID,tagInfo.parentId);
//////////////				int32 pub_object_id = ptrCPbObjectValue->getPub_object_id();
//////////////
////////////////PMString name = ptrCPbObjectValue->getName();
////////////////CA("Name::"+name);
////////////////PMString objectIDstr("pub_object_id = ");
////////////////objectIDstr.AppendNumber(pub_object_id);
////////////////CA(objectIDstr);
//////////////
//////////////				if(isComponentAttributePresent == 0)
//////////////				{
//////////////					//CA("isComponentAttributePresent == 0");
//////////////					//ptrIAppFramework->clearAllStaticObjects();
//////////////					VectorScreenTableInfoPtr tableInfo = 
//////////////						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pub_object_id);
//////////////					if(!tableInfo)
//////////////					{
//////////////						//CA("!tableInfo");
//////////////						ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshCustomTabbedText::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
//////////////						break;
//////////////					}
//////////////
//////////////					if(tableInfo->size()==0)
//////////////					{ //CA("tableInfo->size()==0");
//////////////						ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText: table size = 0");
//////////////						break;
//////////////					}
//////////////
////////////////////PMString temp("tableInfo->size() = ");
////////////////////temp.AppendNumber(tableInfo->size());
////////////////////CA(temp);
//////////////
//////////////					VectorScreenTableInfoValue ::iterator it;
//////////////					CItemTableValue oTableValue;
//////////////
//////////////					bool16 typeidFound=kFalse;
//////////////					vector<int32> vec_items;
//////////////					vec_items.clear();
//////////////					FinalItemIds.clear();
//////////////
//////////////					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//////////////					{//for tabelInfo start
//////////////						oTableValue = *it;				
//////////////						vec_items = oTableValue.getItemIds();
//////////////					
//////////////						if(FinalItemIds.size() == 0)
//////////////						{
//////////////							//CA("FinalItemIds.size() == 0");
//////////////							FinalItemIds = vec_items;
//////////////							
//////////////						}
//////////////						else
//////////////						{
//////////////							for(int32 i=0; i<vec_items.size(); i++)
//////////////							{	bool16 Flag = kFalse;
//////////////								for(int32 j=0; j<FinalItemIds.size(); j++)
//////////////								{
//////////////									if(vec_items[i] == FinalItemIds[j])
//////////////									{
//////////////										Flag = kTrue;
//////////////										break;
//////////////									}				
//////////////								}
//////////////								if(!Flag)
//////////////									FinalItemIds.push_back(vec_items[i]);
//////////////							}
//////////////						}
//////////////					}//for tabelInfo end
//////////////					if(tableInfo)
//////////////						delete tableInfo;
//////////////				}
//////////////				else 
//////////////				{
//////////////					VectorScreenTableInfoPtr KXAItemtableInfo = NULL;
//////////////					if(isComponentAttributePresent == 1)
//////////////					{
//////////////						bool16 isKitTable = kFalse; // we are spraying component table.
//////////////						KXAItemtableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemID_or_productID, languageId, isKitTable); 
//////////////					}
//////////////					else if(isComponentAttributePresent == 2)
//////////////					{
//////////////						KXAItemtableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
//////////////					}
//////////////					else if(isComponentAttributePresent == 3)
//////////////					{
//////////////						KXAItemtableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemID_or_productID, languageId); 
//////////////					}
//////////////
//////////////					if(!KXAItemtableInfo)
//////////////					{
//////////////						//CA("KittableInfo is NULL");
//////////////						break;
//////////////					}
//////////////
//////////////					if(KXAItemtableInfo->size()==0){
//////////////						//CA(" KittableInfo->size()==0");
//////////////						break;
//////////////					}
//////////////
//////////////					CObjectTableValue oTableValue;
//////////////					VectorScreenTableInfoValue::iterator it  ;						
//////////////
//////////////					bool16 typeidFound=kFalse;
//////////////					vector<int32> vec_items;
//////////////					FinalItemIds.clear();
//////////////
//////////////					it = KXAItemtableInfo->begin();
//////////////					{//for tabelInfo start				
//////////////						oTableValue = *it;				
//////////////						vec_items = oTableValue.getItemIds();
//////////////					
//////////////						if(FinalItemIds.size() == 0)
//////////////						{
//////////////							FinalItemIds = vec_items;
//////////////						}
//////////////						else
//////////////						{
//////////////							for(int32 i=0; i<vec_items.size(); i++)
//////////////							{	bool16 Flag = kFalse;
//////////////								for(int32 j=0; j<FinalItemIds.size(); j++)
//////////////								{
//////////////									if(vec_items[i] == FinalItemIds[j])
//////////////									{
//////////////										Flag = kTrue;
//////////////										break;
//////////////									}				
//////////////								}
//////////////								if(!Flag)
//////////////									FinalItemIds.push_back(vec_items[i]);
//////////////							}
//////////////						}
//////////////					}//for tabelInfo end
//////////////					
//////////////					Kit_vec_tablerows = oTableValue.getTableData();
//////////////					if(KXAItemtableInfo)
//////////////						delete KXAItemtableInfo;
//////////////				}
//////////////			}
//////////////		}
//////////////
//////////////		if(customTabbedTextTagPtr->GetAttributeValue("index") == "3")
//////////////		{
//////////////			VectorScreenTableInfoPtr tableInfo=NULL;
//////////////			if(ptrIAppFramework->get_isONEsourceMode())
//////////////			{
//////////////				// For ONEsource mode
//////////////				tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(itemID_or_productID);
//////////////			}else
//////////////			{
//////////////				//For Publication
//////////////				tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, itemID_or_productID);
//////////////			}
//////////////			if(!tableInfo)
//////////////			{
//////////////				ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
//////////////				//ItemAbsentinProductFlag = kTrue;
//////////////				break;
//////////////			}
//////////////			if(tableInfo->size()==0)
//////////////			{ 
//////////////				//CA("tableInfo->size()==0");
//////////////				ptrIAppFramework->LogInfo("AP7_RefreshContent::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo->size()==0");	
//////////////				//ItemAbsentinProductFlag = kTrue;
//////////////				break;
//////////////			}
//////////////
//////////////			CObjectTableValue oTableValue;
//////////////			VectorScreenTableInfoValue::iterator it;
//////////////
//////////////			bool16 typeidFound=kFalse;
//////////////			vector<int32> vec_items;
//////////////			FinalItemIds.clear();
//////////////
//////////////			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//////////////			{//for tabelInfo start				
//////////////				oTableValue = *it;				
//////////////				vec_items = oTableValue.getItemIds();
//////////////			
//////////////				if(FinalItemIds.size() == 0)
//////////////				{
//////////////					FinalItemIds = vec_items;
//////////////				}
//////////////				else
//////////////				{
//////////////					for(int32 i=0; i<vec_items.size(); i++)
//////////////					{	bool16 Flag = kFalse;
//////////////						for(int32 j=0; j<FinalItemIds.size(); j++)
//////////////						{
//////////////							if(vec_items[i] == FinalItemIds[j])
//////////////							{
//////////////								Flag = kTrue;
//////////////								break;
//////////////							}				
//////////////						}
//////////////						if(!Flag)
//////////////							FinalItemIds.push_back(vec_items[i]);
//////////////					}
//////////////				}
//////////////			}//for tabelInfo end
//////////////			if(tableInfo)
//////////////				delete tableInfo;
//////////////			
//////////////		}
//////////////
//////////////		if(FinalItemIds.size()<=0)
//////////////		{			
//////////////			ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions ::RefreshCustomTabbedText::FinalItemIds.size()<=0");
//////////////			break;
//////////////		}
//////////////				
//////////////		int32 finalItemIDsize = FinalItemIds.size();
//////////////PMString sdsd("\nfinal item id size = ");
//////////////sdsd.AppendNumber(finalItemIDsize);
//////////////CA(sdsd);
//////////////	///check whether header present or not in that box
//////////////		bool16 isHeaderPresentInBox = kFalse;
//////////////			
//////////////		int32 childCount = customTabbedTextTagPtr->GetChildCount();
//////////////
//////////////		for(int i =0; i< childCount ;i++)
//////////////		{
//////////////			XMLReference childXMLReference = customTabbedTextTagPtr->GetNthChild(i);
//////////////			IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
//////////////
//////////////			PMString typeID = childXMLElement->GetAttributeValue("typeId");
//////////////			if(typeID == "-2")
//////////////			{	
//////////////				//CA("isHeaderPresentInBox = kTrue");
//////////////				isHeaderPresentInBox = kTrue;
//////////////				break;
//////////////			}
//////////////		}
//////////////	/////////////////////
//////////////
//////////////	////count number of newline characters present in current box
//////////////		int32 newLineCharacters = 0;
//////////////
//////////////		//InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
//////////////		//if(unknown == NULL)
//////////////		//{
//////////////		//	//CA("unknown == NULL");
//////////////		//	break;
//////////////		//}
//////////////		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//////////////		UID textFrameUID = kInvalidUID;
//////////////		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
//////////////		if (graphicFrameDataOne) 
//////////////		{
//////////////			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//////////////		}
//////////////		if (textFrameUID == kInvalidUID)
//////////////		{
//////////////			//CA("textFrameUID == kInvalidUID");
//////////////			break;
//////////////		}	
//////////////		//InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);  CS3 change
//////////////		//if (textFrame == NULL)
//////////////		//{
//////////////		//	//CA("textFrame == NULL");
//////////////		//	break;
//////////////		//}	
//////////////
//////////////		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
//////////////		if (graphicFrameHierarchy == nil) 
//////////////		{
//////////////			//CA("graphicFrameHierarchy is NULL");
//////////////			break;
//////////////		}
//////////////						
//////////////		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//////////////		if (!multiColumnItemHierarchy) {
//////////////			//CA("multiColumnItemHierarchy is NULL");
//////////////			break;
//////////////		}
//////////////
//////////////		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//////////////		if (!multiColumnItemTextFrame) {
//////////////			//CA("multiColumnItemTextFrame is NULL");
//////////////			break;
//////////////		}
//////////////		InterfacePtr<IHierarchy>
//////////////		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//////////////		if (!frameItemHierarchy) {
//////////////			//CA("frameItemHierarchy is NULL");
//////////////			break;
//////////////		}
//////////////
//////////////		InterfacePtr<ITextFrameColumn>	frameItemTFC(frameItemHierarchy, UseDefaultIID());
//////////////		if (!frameItemTFC) {
//////////////			//CA("!!!ITextFrameColumn");
//////////////			break;
//////////////		}
//////////////		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//////////////		if (textModel == NULL)
//////////////		{
//////////////			//CA("textModel == NULL");
//////////////			break;
//////////////		}
//////////////		
//////////////			UIDRef txtModelUIDRef = UIDRef::gNull;
//////////////			txtModelUIDRef = ::GetUIDRef(textModel);
//////////////
//////////////
//////////////		TextIndex startPos =-1;
//////////////		TextIndex endPos = -1;
//////////////
//////////////		TextIndex sIndex=0, eIndex=0;
//////////////		Utils<IXMLUtils>()->GetElementIndices(customTabbedTextTagPtr, &sIndex, &eIndex);
//////////////
//////////////		startPos = sIndex;
//////////////		endPos = eIndex;
//////////////
//////////////		TextIterator begin(textModel , startPos + 1);
//////////////		TextIterator end(textModel , endPos - 1);
//////////////
//////////////PMString asdd("\nStart index = ");
//////////////asdd.AppendNumber(sIndex);
//////////////asdd.Append("\n End index = ");
//////////////asdd.AppendNumber(eIndex);
//////////////CA(asdd);
//////////////
//////////////		for(TextIterator iter = begin ;iter <= end ; iter++)
//////////////		{
//////////////			 if(*iter == kTextChar_CR)
//////////////			 {CA("if(*iter == kTextChar_CR)");
//////////////				++newLineCharacters;
//////////////			 }
//////////////		}
//////////////PMString noOfNewLineCharacters("\nNo Of New Line Characters before decrementing = ");
//////////////noOfNewLineCharacters.AppendNumber(newLineCharacters);
//////////////CA(noOfNewLineCharacters);
//////////////		newLineCharacters = newLineCharacters - 1;//It is used to not consider the NewLine character which we inserted at end in DataSprayer.
//////////////
//////////////PMString noOfNewLineCharacterss("\nNo Of New Line Characters after decrementing = ");
//////////////noOfNewLineCharacterss.AppendNumber(newLineCharacters);
//////////////CA(noOfNewLineCharacterss);
//////////////
//////////////
//////////////		bool16 onlyOneLinePresent = kFalse; ///if only one line is present in text frame...
//////////////		if(newLineCharacters == 0 || newLineCharacters == -1)
//////////////		{
//////////////			onlyOneLinePresent = kTrue;
//////////////		}
//////////////
//////////////	/////////////////////
//////////////		
//////////////	/////check whether number of items currently present in box is same as FinalItemIds size
//////////////	///if it is equal then there is no need to add or delete rows from box in case of headerPresent in box...
//////////////	///otherwise there is need to add or delete rows from the box.....
//////////////	
//////////////		bool16 isNeedToDeleteRows = kFalse;
//////////////		bool16 isNeedToAddRows = kFalse;
//////////////		int32 noOfLinesToBeAddedOrDeleted = 0;
//////////////		if(isHeaderPresentInBox)
//////////////		{
//////////////			if(finalItemIDsize != newLineCharacters)
//////////////			{
//////////////				if(finalItemIDsize < newLineCharacters )
//////////////				{
//////////////					isNeedToDeleteRows = kTrue;
//////////////					noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize;
//////////////				}
//////////////				else
//////////////				{
//////////////					if(newLineCharacters == -1)
//////////////					{
//////////////						isNeedToAddRows = kTrue;
//////////////						noOfLinesToBeAddedOrDeleted = finalItemIDsize - 1;
//////////////					}
//////////////					else
//////////////					{
//////////////						isNeedToAddRows = kTrue;
//////////////						noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters;
//////////////					}
//////////////				}
//////////////			}	
//////////////		}
//////////////		else
//////////////		{CA("Else 1");
//////////////			if(finalItemIDsize != newLineCharacters /*+ 1*/)
//////////////			{CA("if 1");
//////////////				if(finalItemIDsize <= newLineCharacters)
//////////////				{
//////////////					CA("if 2");
//////////////					isNeedToDeleteRows = kTrue;
//////////////					noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemIDsize + 1;
//////////////				}
//////////////				else
//////////////				{
//////////////					CA("else 2");
//////////////					if(newLineCharacters == -1)
//////////////					{
//////////////						isNeedToAddRows = kTrue;
//////////////						noOfLinesToBeAddedOrDeleted = finalItemIDsize - 1;
//////////////					}
//////////////					else
//////////////					{
//////////////						isNeedToAddRows = kTrue;
//////////////						noOfLinesToBeAddedOrDeleted = finalItemIDsize - newLineCharacters - 1;
//////////////					}
//////////////				}
//////////////			}
//////////////		}
//////////////PMString qwe("\nno Of Lines To Be Added Or Deleted = ");
//////////////qwe.AppendNumber(noOfLinesToBeAddedOrDeleted);
//////////////CA(qwe);
//////////////		/*if(isNeedToAddRows)
//////////////		{
//////////////			PMString noOfLinesToBeAdded("noOfLinesToBeAdded = ");
//////////////			noOfLinesToBeAdded.AppendNumber(noOfLinesToBeAddedOrDeleted);
//////////////			CA(noOfLinesToBeAdded);
//////////////		}
//////////////
//////////////		if(isNeedToDeleteRows)
//////////////		{
//////////////			PMString noOfLinesToBeDeleted("noOfLinesToBeDeleted = ");
//////////////			noOfLinesToBeDeleted.AppendNumber(noOfLinesToBeAddedOrDeleted);
//////////////			CA(noOfLinesToBeDeleted);
//////////////		}*/
//////////////	///////////////////////////////////////
//////////////
//////////////	////add no of lines
//////////////
//////////////////
//////////////	IIDXMLElement* customTabbedTextXMLElementt = tagInfo.tagPtr;
//////////////	int32 newChildCountt = customTabbedTextXMLElementt->GetChildCount();
//////////////
//////////////	PMString strNewChildCountt("\nBefore new Child Count = ");
//////////////	strNewChildCountt.AppendNumber(newChildCountt);
//////////////	CA(strNewChildCountt);
///////////////////
//////////////		if(!onlyOneLinePresent && isNeedToAddRows)////if more than one lines are already present in text Frame and we want to add more in that
//////////////		{//CA("if(!onlyOneLinePresent && isNeedToAddRows)");
//////////////			int32 startIndexForCopy = 0;
//////////////			
//////////////			int32 j = 0;
//////////////			for(TextIterator iter = begin ;iter <= end ; iter++)
//////////////			{//CA("first for");
//////////////				if(*iter == kTextChar_CR)
//////////////				{
//////////////					j++;
//////////////					if(j == newLineCharacters)
//////////////					{//CA("if(j == newLineCharacters)");
//////////////						startIndexForCopy = iter.Position();
//////////////						break;
//////////////					}
//////////////				}
//////////////			}
//////////////			
//////////////	CA_NUM("endPos : ",endPos);
//////////////	CA_NUM("Start index Copy = ",startIndexForCopy);
//////////////			int32 length = endPos -2 - startIndexForCopy;
//////////////PMString len("Length = ");
//////////////len.AppendNumber(length);
//////////////CA(len);
//////////////			K2::shared_ptr< PasteData > pasteData;
//////////////			
//////////////			/*for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
//////////////			{
//////////////				textModel->CopyRange(startIndexForCopy,length,pasteData);
//////////////				textModel->Paste(endPos -1,pasteData);
//////////////				endPos = endPos + length;
//////////////			}	*/	
//////////////				
//////////////			
//////////////				/* startPasteIndex--;*/
//////////////			
//////////////				int32 startPasteIndex = /*startIndexForCopy + length + 1;*/endPos -2;
//////////////				TextIndex destEnd =  startPasteIndex; 
//////////////				
//////////////
//////////////
//////////////
//////////////			for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
//////////////			{//CA("intended for");
//////////////				textModel->CopyRange(startIndexForCopy,length,pasteData);
//////////////				textModel->Paste(startPasteIndex,pasteData);
//////////////				endPos = endPos + length;
//////////////			}	
//////////////	}
//////////////		if(onlyOneLinePresent && isNeedToAddRows)
//////////////		{CA("if(onlyOneLinePresent && isNeedToAddRows)");
//////////////			WideString* newLineCharacter = new WideString("\r");
//////////////
//////////////			int32 startIndexForCopy = startPos;
//////////////
//////////////			int32 length = endPos - startPos ;
////////////////////////////CA_NUM("length : ",length);
////////////////////////////CA_NUM("endPos : ",endPos);
/////////////////////////////////////////////// chetan 23 jan
////////////////////////////
////////////////////////////		TextIterator begin(textModel , endPos - length-1 );
////////////////////////////		TextIterator end(textModel , endPos-1);
////////////////////////////PMString ddd("\nStart position = ");
////////////////////////////ddd.AppendNumber(endPos - length);
////////////////////////////CA(ddd);
////////////////////////////PMString story("\n story =");
////////////////////////////	for (TextIterator iter = begin; iter <= end; iter++)
////////////////////////////	{
////////////////////////////		//const textchar characterCode = (*iter).GetTextChar();
////////////////////////////		UTF16TextChar characterCode; 
////////////////////////////		int32 len=1; 
////////////////////////////		(*iter).ToUTF16(&characterCode,&len);
////////////////////////////		char buf;
////////////////////////////		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
////////////////////////////		story.Append(buf);
////////////////////////////	}
////////////////////////////story.Append("=");
////////////////////////////CA(story);
/////////////////////////////////////////////// chetan 23 jan
////////////////////////////////
//////////////			K2::shared_ptr< PasteData > pasteData;
//////////////////////////////PMString zxz("start Index For Copy = ");
//////////////////////////////zxz.AppendNumber(startIndexForCopy);
//////////////////////////////CA(zxz);
//////////////			for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
//////////////			{CA("for wwwwwwwwwwwww");
//////////////				textModel->CopyRange(startIndexForCopy+1/*startPos*/ ,length-1/*endPos-startPos*/,pasteData);CA("1111");
//////////////				textModel->Insert(endPos ,newLineCharacter);CA("2222");
//////////////				//textModel->Paste(endPos,pasteData);				
//////////////				textModel->Paste(endPos+1 ,pasteData);
//////////////				/*startPos = endPos+1;*/
//////////////				endPos = endPos + length;
//////////////			}
//////////////			delete newLineCharacter;
//////////////		}
//////////////
//////////////	/////Delete no of lines
//////////////		if(!onlyOneLinePresent && isNeedToDeleteRows)
//////////////		{CA("if(!onlyOneLinePresent && isNeedToDeleteRows)");
//////////////			if(isHeaderPresentInBox)
//////////////			{
//////////////				int32 startIndexForDelete = 0;
//////////////			
//////////////				int32 j = 0;
//////////////				for(TextIterator iter = begin ;iter <= end ; iter++)
//////////////				{
//////////////					if(*iter == kTextChar_CR)
//////////////					{
//////////////						j++;
//////////////						if(j == finalItemIDsize + 1)
//////////////						{
//////////////							startIndexForDelete = iter.Position();
//////////////							break;
//////////////						}
//////////////					}
//////////////				}
//////////////
//////////////				int32 length = endPos - 1 - startIndexForDelete;
//////////////
//////////////				textModel->Delete(startIndexForDelete,length);
//////////////			}
//////////////			else
//////////////			{
//////////////				int32 startIndexForDelete = 0;
//////////////			
//////////////				int32 j = 0;
//////////////				for(TextIterator iter = begin ;iter <= end ; iter++)
//////////////				{
//////////////					if(*iter == kTextChar_CR)
//////////////					{
//////////////						j++;
//////////////						if(j == finalItemIDsize)
//////////////						{
//////////////							startIndexForDelete = iter.Position();
//////////////							break;
//////////////						}
//////////////					}
//////////////				}
//////////////PMString zxz("start Index For Delete = ");
//////////////zxz.AppendNumber(startIndexForDelete);
//////////////CA(zxz);
//////////////				int32 length = endPos - 1 - startIndexForDelete;
//////////////
//////////////				textModel->Delete(startIndexForDelete,length);
//////////////			}
//////////////		}
//////////////
//////////////		//CA("Successfully added or deleted line in text frame");
//////////////
//////////////	////////change typeID of all tags as per the FinalItemIds
//////////////		IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
//////////////		int32 newChildCount = customTabbedTextXMLElement->GetChildCount();
/////////////////
//////////////	PMString strNewChildCount("\nAfter new Child Count = ");
//////////////	strNewChildCount.AppendNumber(newChildCount);
//////////////	CA(strNewChildCount);
/////////////////
////////////////return result;
//////////////		int32 noOfTagsPresentPerRow = 0;
//////////////		
//////////////		if(isNeedToDeleteRows)
//////////////			noOfTagsPresentPerRow = newChildCount / (newLineCharacters - noOfLinesToBeAddedOrDeleted + 1);
//////////////		else
//////////////		{
//////////////			if(newLineCharacters == -1)
//////////////			{
//////////////				noOfTagsPresentPerRow = 1;
//////////////			}
//////////////			else
//////////////			{
//////////////				noOfTagsPresentPerRow = newChildCount / (newLineCharacters + noOfLinesToBeAddedOrDeleted + 1);
//////////////			}
//////////////		}
//////////////		
//////////////PMString strNoOfTagsPresentPerRow("\nNo Of Tags Present Per Row = ");
//////////////strNoOfTagsPresentPerRow.AppendNumber(noOfTagsPresentPerRow);
////////////////CA(strNoOfTagsPresentPerRow);
//////////////	
//////////////		int32 index = 0;
//////////////		if(isHeaderPresentInBox)
//////////////			index = noOfTagsPresentPerRow;
//////////////
//////////////		vector<int32> ::iterator itr;
//////////////		itr = FinalItemIds.begin();
//////////////
//////////////
//////////////		for(int i = 0; i < finalItemIDsize; i++ ,itr++)
//////////////		{
//////////////			PMString itemID("");
//////////////			itemID.AppendNumber(*itr);
//////////////			for(int j = 0 ; j < noOfTagsPresentPerRow ; j++)
//////////////			{
//////////////				PMString temp("itemID = ");
//////////////				temp.Append(itemID);
//////////////				temp.Append(" , index = ");
//////////////				temp.AppendNumber(index);
//////////////				CA(temp);
//////////////
//////////////				XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(index);
//////////////				IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
//////////////							
//////////////				childXMLElement->SetAttributeValue("typeId" , itemID);
//////////////				index++;
//////////////			}		
//////////////		}
//////////////
//////////////	///////////////////////
//////////////CA("before refreshing");
//////////////	////////now we can refresh each tags 
//////////////		VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
//////////////		VectorHtmlTrackerValue vectorObj ;
//////////////		vectorHtmlTrackerSPTBPtr = &vectorObj;
//////////////
//////////////		int32 j = 0;//temp variable
//////////////		int32 sequenceNo = 0; //index of itemID present in FinalItemIds.
//////////////		for(int32 i = 0; i < newChildCount; i++)
//////////////		{
//////////////			XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(i);
//////////////			IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
//////////////			
//////////////			PMString strTypeID = childXMLElement->GetAttributeValue("typeId");
//////////////			PMString strSectionID = childXMLElement->GetAttributeValue("sectionID");
//////////////			PMString strElementID = childXMLElement->GetAttributeValue("ID");
//////////////			PMString strLanguageID = childXMLElement->GetAttributeValue("LanguageID");
//////////////			PMString strParentTypeID = childXMLElement->GetAttributeValue("parentTypeID");
//////////////			//PMString strIndex = childXMLElement->GetAttributeValue("index");
//////////////			PMString strIndex = customTabbedTextXMLElement->GetAttributeValue("index");
////////////////CA("Type ID = "+strTypeID);
//////////////
//////////////			int32 typeId = strTypeID.GetAsNumber();
//////////////			int32 sectionID = strSectionID.GetAsNumber();
//////////////			int32 elementID = strElementID.GetAsNumber();
//////////////			int32 languageID = strLanguageID.GetAsNumber();
//////////////			int32 parentTypeID = strParentTypeID.GetAsNumber();
//////////////			int32 index = strIndex.GetAsNumber();
//////////////
//////////////			PMString itemAttributeValue("");
//////////////			if(typeId == -2)
//////////////			{
//////////////				if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
//////////////				{
//////////////					VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
//////////////					if(TypeInfoVectorPtr != NULL)
//////////////					{//CA("TypeInfoVectorPtr != NULL");
//////////////						VectorTypeInfoValue::iterator it3;
//////////////						int32 Type_id = -1;
//////////////						PMString temp = "";
//////////////						PMString name = "";
//////////////						for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
//////////////						{
//////////////							Type_id = it3->getTypeId();
//////////////							if(parentTypeID == Type_id)
//////////////							{
//////////////								temp = it3->getName();
//////////////								if(elementID == -701)
//////////////								{
//////////////									itemAttributeValue = temp;
//////////////								}
//////////////								else if(elementID == -702)
//////////////								{
//////////////									itemAttributeValue = temp + " Suffix";
//////////////								}
//////////////							}
//////////////						}
//////////////					}
//////////////					if(TypeInfoVectorPtr)
//////////////						delete TypeInfoVectorPtr;
//////////////				}
//////////////				else if(elementID == -703)
//////////////				{
//////////////					//CA("dispName = $Off");
//////////////					itemAttributeValue = "$Off";
//////////////				}
//////////////				else if(elementID == -704)
//////////////				{
//////////////					//CA("dispName = %Off");
//////////////					itemAttributeValue = "%Off";
//////////////				}
//////////////				else if(elementID == -805)
//////////////				{									
//////////////					itemAttributeValue = "Quantity";
//////////////                }
//////////////				else if(elementID == -806)
//////////////				{									
//////////////					itemAttributeValue = "Availablity";
//////////////                }
//////////////				else if(elementID == -807)
//////////////				{									
//////////////					itemAttributeValue = "Cross-reference Type";
//////////////                }
//////////////				else if(elementID == -808)
//////////////				{									
//////////////					itemAttributeValue = "Rating";
//////////////                }
//////////////				else if(elementID == -809)
//////////////				{									
//////////////					itemAttributeValue = "Alternate";
//////////////                }
//////////////				else if(elementID == -810)
//////////////				{									
//////////////					itemAttributeValue = "Comments";
//////////////                }
//////////////				else if(elementID == -811)
//////////////				{									
//////////////					itemAttributeValue = "";
//////////////                }
//////////////				else if(elementID == -812)
//////////////				{									
//////////////					itemAttributeValue = "Quantity";
//////////////                }
//////////////				else if(elementID == -813)
//////////////				{									
//////////////					itemAttributeValue = "Required";
//////////////                }
//////////////				else if(elementID == -814)
//////////////				{									
//////////////					itemAttributeValue = "Comments";
//////////////                }
//////////////				else
//////////////					itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
//////////////			}
//////////////			else
//////////////			{
//////////////				if(j < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
//////////////				{
//////////////					j++;
//////////////				}
//////////////				else
//////////////				{
//////////////					sequenceNo++;
//////////////					j = 1;
//////////////				}
//////////////				if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
//////////////				{
//////////////					PublicationNode pNode;
//////////////					pNode.setPubId(itemID_or_productID);
//////////////
//////////////					PMString strPBObjectID = childXMLElement->GetAttributeValue("rowno");
//////////////					int32 rowno = strPBObjectID.GetAsNumber();
//////////////					pNode.setPBObjectID(strPBObjectID.GetAsNumber());
//////////////					if(index == 4)
//////////////					{
//////////////						
//////////////						pNode.setIsProduct(0);
//////////////					}
//////////////					else if(index == 3)
//////////////					{
//////////////						pNode.setIsProduct(1);
//////////////					}
//////////////					if(rowno != -1)
//////////////					{
//////////////						//CA("sETTING IsONEsource flag as false");
//////////////						pNode.setIsONEsource(kFalse);
//////////////					}
//////////////					TagStruct tagInfo;
//////////////					tagInfo.elementId = elementID;
//////////////					tagInfo.languageID = languageID;
//////////////					tagInfo.typeId = typeId;
//////////////					tagInfo.sectionID = sectionID;
//////////////					tagInfo.tagPtr = childXMLElement;
//////////////				
//////////////					tagInfo.parentTypeID = parentTypeID;
//////////////					
//////////////					handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,itemAttributeValue);
//////////////				}
//////////////				else if(elementID == -803)  // For 'Letter Key'
//////////////				{									
//////////////					itemAttributeValue.Clear();
//////////////					int wholeNo =  sequenceNo / 26;
//////////////					int remainder = sequenceNo % 26;								
//////////////
//////////////					if(wholeNo == 0)
//////////////					{// will print 'a' or 'b'... 'z'  upto 26 item ids
//////////////						itemAttributeValue.Append(AlphabetArray[remainder]);
//////////////					}
//////////////					else  if(wholeNo <= 26)
//////////////					{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
//////////////						itemAttributeValue.Append(AlphabetArray[wholeNo-1]);	
//////////////						itemAttributeValue.Append(AlphabetArray[remainder]);	
//////////////					}
//////////////					else if(wholeNo <= 52)
//////////////					{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
//////////////						itemAttributeValue.Append(AlphabetArray[0]);
//////////////						itemAttributeValue.Append(AlphabetArray[wholeNo -26 -1]);	
//////////////						itemAttributeValue.Append(AlphabetArray[remainder]);										
//////////////					}
//////////////				}
//////////////				else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
//////////////				}
//////////////				else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
//////////////				}
//////////////				else if((elementID == -807) && (isComponentAttributePresent == 2))
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
//////////////                }
//////////////				else if((elementID == -808) && (isComponentAttributePresent == 2))
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
//////////////                }
//////////////				else if((elementID == -809) && (isComponentAttributePresent == 2))
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][2]);	
//////////////                }
//////////////				else if((elementID == -810) && (isComponentAttributePresent == 2))
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][3]);	
//////////////                }
//////////////				else if((elementID == -811) && (isComponentAttributePresent == 2))
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][4]);	
//////////////                }
//////////////				else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][0]);	
//////////////				}
//////////////				else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][1]);	
//////////////				}
//////////////				else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
//////////////				{
//////////////					itemAttributeValue = (Kit_vec_tablerows[sequenceNo][2]);	
//////////////				}
//////////////				else
//////////////				{
//////////////					if((isComponentAttributePresent == 2) && ((Kit_vec_tablerows[sequenceNo][4]) == "N"))
//////////////					{
//////////////						itemAttributeValue = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(typeId,elementID,languageID);
//////////////					}
//////////////					else
//////////////                        itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(typeId,elementID, languageID, kTrue );
//////////////				}
//////////////			}
//////////////			int32 tagStartPos = -1;
//////////////			int32 tagEndPos = -1;
//////////////			Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStartPos,&tagEndPos);
//////////////			tagStartPos = tagStartPos + 1;
//////////////			tagEndPos = tagEndPos -1;
//////////////
//////////////			PMString textToInsert("");
//////////////			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//////////////			if(!iConverter)
//////////////			{
//////////////				textToInsert=itemAttributeValue;					
//////////////			}
//////////////			else						
//////////////			{
//////////////				//textToInsert=iConverter->translateString(itemAttributeValue);
//////////////				vectorHtmlTrackerSPTBPtr->clear();
//////////////				textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
//////////////			}	
//////////////CA("textToInsert : " +textToInsert);
//////////////
//////////////			WideString insertText(textToInsert);
//////////////			result = textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
//////////////		}
//////////////	
//////////////		result = kSuccess;
//////////////
//////////////	}while(kFalse);
//////////////	return result;
//////////////}


ErrorCode RefreshMMYCustomTable(const UIDRef& boxUIDRef,TagStruct& tagInfo)
{
	//CA("RefreshMMYCustomTable");
	ErrorCode result = kFailure;
	//do
	//{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	UID textFrameUID = kInvalidUID;
	//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	//	if (graphicFrameDataOne) 
	//	{
	//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//	}

	//	if (textFrameUID == kInvalidUID)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}
	//	
	//	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
	//	if (graphicFrameHierarchy == nil) 
	//	{
	//		//CA(graphicFrameHierarchy);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!graphicFrameHierarchy");
	//		break;
	//	}
	//					
	//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//	if (!multiColumnItemHierarchy) {
	//		//CA(multiColumnItemHierarchy);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
	//		break;
	//	}

	//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//	if (!multiColumnItemTextFrame) {
	//		//CA(!multiColumnItemTextFrame);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
	//		//CA("Its Not MultiColumn");
	//		break;
	//	}
	//	
	//	InterfacePtr<IHierarchy>
	//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	//	if (!frameItemHierarchy) {
	//		//CA(“!frameItemHierarchy”);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!frameItemHierarchy");
	//		break;
	//	}

	//	InterfacePtr<ITextFrameColumn>
	//	textFrame(frameItemHierarchy, UseDefaultIID());
	//	if (!textFrame) {
	//		//CA("!!!ITextFrameColumn");
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!textFrame");
	//		break;
	//	}
	//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	//	if (textModel == NULL)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}

	//	UIDRef txtModelUIDRef = ::GetUIDRef(textModel);



	////find out total MMY items 
	//	vector<int32> FinalItemIds;
	//	if(tagInfo.whichTab == 3)
	//	{
	//		VectorScreenTableInfoPtr tableInfo = NULL;
	//		
	//		// for publication
	//		tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.parentId, tagInfo.languageID,  kTrue);
	//	
	//		if(!tableInfo)
	//		{
	//			break;
	//		}
	//		if(tableInfo->size()==0)
	//		{ 
	//			makeTagInsideCellImpotent(tagInfo.tagPtr);
	//			break;
	//		}
	//				
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;

	//		bool16 typeidFound=kFalse;
	//		vector<int32> vec_items;
	//		FinalItemIds.clear();

	//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		{				
	//			oTableValue = *it;				
	//			vec_items = oTableValue.getItemIds();
	//		
	//			if(tagInfo.field1 == -1)	//---------
	//			{
	//				if(FinalItemIds.size() == 0)
	//				{	//CA("FinalItemIds.size() == 0");
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
	//					{	
	//						bool16 Flag = kFalse;
	//						for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
	//						{
	//							if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
	//					}

	//				}
	//			}
	//			else
	//			{
	//				 
	//				if(tagInfo.field1  !=  oTableValue.getTableTypeID())
	//					continue;

	//				if(FinalItemIds.size() == 0)
	//				{	//CA("FinalItemIds.size() == 0");
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
	//					{	
	//						bool16 Flag = kFalse;
	//						for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
	//						{
	//							if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
	//					}

	//				}

	//			}
	//		}

	//		if(tableInfo)
	//		{
	//			tableInfo->clear();
	//			delete tableInfo;
	//		}
	//	}
	//	else if(tagInfo.whichTab == 4)
	//	{
	//		VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tagInfo.parentId, tagInfo.sectionID  ,tagInfo.languageID );
	//		if(!tableInfo)
	//		{
	//			//CA("tableinfo is NULL");
	//			makeTagInsideCellImpotent(tagInfo.tagPtr); 
	//			break;
	//		}
	//		
	//		if(tableInfo->size()==0)
	//		{ //CA("tableinfo size==0");
	//			makeTagInsideCellImpotent(tagInfo.tagPtr); 
	//			break;
	//		}
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;

	//		bool16 typeidFound=kFalse;
	//		vector<int32> vec_items;
	//		
	//		FinalItemIds.clear();

	//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		{				
	//			oTableValue = *it;				
	//			vec_items = oTableValue.getItemIds();
	//		
	//			if(tagInfo.field1 == -1)	//--------
	//			{
	//				if(FinalItemIds.size() == 0)
	//				{
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 i=0; i<vec_items.size(); i++)
	//					{	bool16 Flag = kFalse;
	//						for(int32 j=0; j<FinalItemIds.size(); j++)
	//						{
	//							if(vec_items[i] == FinalItemIds[j])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[i]);
	//					}
	//				}
	//			}
	//			else
	//			{
	//				if(tagInfo.field1 != oTableValue.getTableTypeID())
	//					continue;

	//				if(FinalItemIds.size() == 0)
	//				{
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 i=0; i<vec_items.size(); i++)
	//					{	bool16 Flag = kFalse;
	//						for(int32 j=0; j<FinalItemIds.size(); j++)
	//						{
	//							if(vec_items[i] == FinalItemIds[j])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[i]);
	//					}
	//				}
	//			}
	//			
	//		}		
	//	
	//		if(tableInfo)
	//			delete tableInfo;
	//	}

	//	PMString FinaiItemIdsStr("FinalItemIds size = ");
	//	FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
	//	//CA(FinaiItemIdsStr);

	//	
	////getMMYTableByItemIds
	//	CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);

	//	PMString noOfMMYTables = "";
	//	noOfMMYTables.AppendNumber(static_cast<int32>(MMYTablePtr->vec_CMakeModelPtr->size()));
	//	//CA("MMYTablePtr->vec_CMakeModelPtr->size() = " + noOfMMYTables);
	//
	////check the no of table tags in frame and no of make tables in MMYTablePtr
	//	//if both are equal no need to add or delete tables
	//	//else if(no of table tags in frame < make tables in MMYTablePtr)
	//		//add tables to document
	//	//else if(no of table tags in frame > make tables in MMYTablePtr)
	//		//delete tables from document


	//	int32 noOfMMYTablesPresntInFrame = findOutHowManyMMYTablesInTextFrame(tagInfo);
	//	int32 noOfMakeFound = static_cast<int32>(MMYTablePtr->vec_CMakeModelPtr->size());

	//	PMString noOfMMYTablesPresntInFrameStr = "";
	//	noOfMMYTablesPresntInFrameStr.AppendNumber(noOfMMYTablesPresntInFrame);
	//	//CA("noOfMMYTablesPresntInFrameStr = " + noOfMMYTablesPresntInFrameStr);

	//	PMString noOfMakeFoundStr = "";
	//	noOfMakeFoundStr.AppendNumber(noOfMakeFound);
	//	//CA("noOfMakeFoundStr = " + noOfMakeFoundStr);

	//	if(noOfMMYTablesPresntInFrame < noOfMakeFound)
	//	{
	//		//CA("need to add tables");

	//		//int32 len = textModel->GetPrimaryStoryThreadSpan ();
	//		////CA_NUM("textModel->GetPrimaryStoryThreadSpan  = " , len);
	//		//
	//		//TextIndex threadStart;
	//		//int32 threadSpan;
	//		//TextIndex tableStartPosition = 0;
	//		//TextIndex tableEndPosition = 0;
	//		//TextIndex position = 0;


	//		//InterfacePtr<ITextStoryThread> storyThread(textModel->QueryStoryThread(position, &threadStart, &threadSpan));
	//		//if(storyThread == NULL)
	//		//{
	//		//	//CA("storyThread == NULL");
	//		//	return result; 
	//		//}

	//		////CA_NUM("threadStart = " , threadStart);
	//		////CA_NUM("threadSpan = " , threadSpan);
	//		//

	//		//bool16 findTable = InspectChars(textModel,threadStart, threadSpan - 1,tableStartPosition,tableEndPosition);
	//	
	//		//
	//		//int32 startPos = tableStartPosition;
	//		//int32 endPos = startPos+tableEndPosition+1;
	//	
	//		////CA_NUM("startPos = " , startPos);
	//		////CA_NUM("endPos = " , endPos);

	//		//int32 length = endPos - startPos;
	//		//int32 startPasteIndex = len - 1;
	//		//TextIndex destEnd = startPasteIndex + 0; 

	//		//int32 lineStartCharIndex = startPos;
	//		//int32 lineEndCharIndex = endPos;


	//		////copyPaste MMY tables for no of Make Times
	//		//int32 numberOfTimesTheTableToBeCopied = noOfMakeFound - noOfMMYTablesPresntInFrame;
	//		//for(int32 tableIndex=0;tableIndex<numberOfTimesTheTableToBeCopied;tableIndex++)
	//		//{
	//		//	do
	//		//	{
	//		//		// Create kCopyStoryRangeCmdBoss.
	//		//		InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
	//		//		if (copyStoryRangeCmd == nil) {CA("copyStoryRangeCmd == nil");
	//		//			break;
	//		//		}

	//		//		// Refer the command to the source story and range to be copied.
	//		//		InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
	//		//		if (sourceUIDData == nil) {CA("sourceUIDData == nil");
	//		//			break;
	//		//		}
	//		//		
	//		//		sourceUIDData->Set(txtModelUIDRef);
	//		//		InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
	//		//		if (sourceRangeData == nil) {CA("sourceRangeData == nil");
	//		//			break;
	//		//		}
	//		//		
	//		//		sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);

	//		//		// Refer the command to the destination story and the range to be replaced.
	//		//		UIDList itemList(txtModelUIDRef);
	//		//		copyStoryRangeCmd->SetItemList(itemList);
	//		//		
	//		//		InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

	//		//		destRangeData->Set(startPasteIndex, destEnd );

	//		//		// Process CopyStoryRangeCmd
	//		//		ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

	//		//	}while(kFalse);
	//		//}
	//		int32 noOfTablesToBeAdded = noOfMakeFound - noOfMMYTablesPresntInFrame;
	//		addTableToFrame(textModel, noOfTablesToBeAdded);
	//	}
	//	else if(noOfMMYTablesPresntInFrame > noOfMakeFound)
	//	{
	//		//CA("need to delete tables");
	//		int32 noOfTablesToBeDelete = noOfMMYTablesPresntInFrame - noOfMakeFound;
	//		deleteTableAndContents(tagInfo, noOfTablesToBeDelete);
	//	}
	//
	////for each table
	//	//find out current table tag's make_Id whether present in MMYTablePtr 
	//		//if it present checks the no of model in tag as well as in MMYTablePtr
	//			//there are 3 cases whether both are same...so no need to add or delete rows from document table
	//			//Otherwise we need to add or delete rows
	//	


	//	int32 CurrentSubSectionID = tagInfo.sectionID;
	//	int32 PARENTID = tagInfo.parentId;
	//	
	//	int32 tableTypeId = -1;		
	//	int32 sort_by_attributeId = -1;
	//	bool16 HeaderRowPresent = kFalse;
	//	bool16 tableHeaderPresent = kFalse;
	//	int32 languageId = -1;

	//	for(int32 tagIndex = 0;tagIndex < tagInfo.tagPtr->GetChildCount();tagIndex++)
	//	{//start for tagIndex = 0
	//		XMLReference cellXMLElementRef =   tagInfo.tagPtr->GetNthChild(tagIndex);
	//		//This is a tag attached to the cell.
	//		//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//		InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//		//Get the text tag attached to the text inside the cell.
	//		//We are providing only one texttag inside cell.
	//		if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//		{
	//			continue;
	//		}
	//		for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//		{
	//			XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//			//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
	//			InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
	//			//The cell may be blank. i.e doesn't have any text tag inside it.
	//			if(cellTextTagPtr == NULL)
	//			{
	//				continue;
	//			}
	//			
	//			if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
	//				 && HeaderRowPresent == kFalse)
	//			{
	//				//CA("header present");
	//				tableHeaderPresent = kTrue;
	//			}

	//			PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
	//			tableTypeId = fld_1.GetAsNumber();
	//			
	//			if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
	//			{		
	//				if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
	//				{							
	//					PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
	//					sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
	//				}
	//			}			
	//		}
	//	}//end for tagIndex = 0 


	//	InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
	//	if(tableList==NULL)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}
	//	int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

	//	if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::totalNumberOfTablesInsideTextFrame < 0");																						
	//		break;//breaks the do{}while(kFalse)
	//	}

	//	PMString totalNumberOfTablesInsideTextFrameStr = "";
	//	totalNumberOfTablesInsideTextFrameStr.AppendNumber(totalNumberOfTablesInsideTextFrame);
	//	//CA("totalNumberOfTablesInsideTextFrame = " + totalNumberOfTablesInsideTextFrameStr);

	//	bool16 isSameMake = kFalse;
	//	bool16 isSameModel = kFalse;

	//	int32 makeIndex = -1;
	//	int32 modelIndex = -1;

	//	int32 makeRepeatCnt = -1;
	//	int32 modelRepeatCnt = -1;

	//	int32 currentMakeModelCnt = -1;
	//	int32 currentModelItemCnt = -1;
	//	int32 currentModelItemIndex = -1;

	//	int32 colIndexPerRow = -1;

	//	vector<int32> itemIds;

	//	Vec_CMakeModel::const_iterator itr;
	//	Vec_CModelModel::const_iterator itr1;

	//	CMakeModel *makeModelPtr = NULL;
	//	CModelModel *modelModelPtr = NULL;

	//	multimap<double, int32>sortItems;
	//	multimap<double, int32>::iterator mapItr;
	//	int32 ITEMID = -1;
	//	int32 itemIDIndex = -1;


	//	for(int tableIndex = 0; tableIndex < totalNumberOfTablesInsideTextFrame; tableIndex++)
	//	{//for..tableIndex

	//		bool16 isMakeIDAttachedToTableTag = kFalse;
	//		int32 getTableModelAtIndex = tableIndex;
	//		if(tableIndex == 0)
	//		{
	//			getTableModelAtIndex = 0;
	//		}
	//		else
	//		{
	//			getTableModelAtIndex = totalNumberOfTablesInsideTextFrame - tableIndex;
	//			
	//		}

	//		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(getTableModelAtIndex));
	//		if(tableModel == NULL)
	//		{
	//			//CA("continue");
	//			continue;//continues the for..tableIndex
	//		}
	//
	//		//bool16 HeaderRowPresent = kFalse;

	//		RowRange rowRange(0,0);
	//		rowRange = tableModel->GetHeaderRows();
	//		int32 headerStart = rowRange.start;
	//		int32 headerCount = rowRange.count;
	//		
	//		PMString HeaderStart = "";
	//		HeaderStart.AppendNumber(headerStart);
	//		PMString HeaderCount = "";
	//		HeaderCount.AppendNumber(headerCount);
	//		//CA("HeaderStart = " + HeaderStart + ", HeaderCount =  " + HeaderCount);

	//		if(headerCount != 0)
	//		{
	//			//CA("HeaderPresent");
	//			HeaderRowPresent = kTrue;
	//		}
	//		
	//		
	//		UIDRef tableRef(::GetUIDRef(tableModel)); 
	//		//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
	//		//IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;

	//		languageId = tagInfo.languageID; 

	//		//XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();			
	//		//UIDRef ContentRef = contentRef.GetUIDRef();
	//		//if( ContentRef != tableRef)
	//		//{
	//		//	CA("ContentRef != tableRef");
	//		//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
	//		//	continue; //continues the for..tableIndex
	//		//}

	//		int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
	//				
	//		int32 totalNumberOfRowsBeforeRowInsertion = 0;
	//		if(HeaderRowPresent)
	//			totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
	//		else
	//			totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;

	//		PMString r3("totalNumberOfBodyRowsBeforeRefresh ===03: ");
	//		r3.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//		//CA(r3);

	//		PMString numberOfRows = "";
	//		numberOfRows.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//		//CA("totalNumberOfRowsBeforeRowInsertion  = " + numberOfRows);
 //           		
	//		//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
	//		GridArea gridAreaForEntireTable,headerArea;;
	//		if(HeaderRowPresent)
	//		{	//CA("fill gridAreaForEntireTable");			 
	//			gridAreaForEntireTable.topRow = headerCount;
	//			gridAreaForEntireTable.leftCol = 0;
	//			gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
 //               gridAreaForEntireTable.rightCol = totalNumberOfColumns;
	//			//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
	//		
	//			headerArea.topRow = 0;
	//			headerArea.leftCol = 0;
	//			headerArea.bottomRow = headerCount;
	//			headerArea.rightCol = totalNumberOfColumns;			
	//		}
	//		else
	//		{
	//			gridAreaForEntireTable.topRow = 0;
	//			gridAreaForEntireTable.leftCol = 0;
	//			gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
 //               gridAreaForEntireTable.rightCol = totalNumberOfColumns;
	//			//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
	//		}

	//		GridArea gridAreaForLastRow;
	//		if(HeaderRowPresent)
	//		{
	//			gridAreaForLastRow.topRow = totalNumberOfRowsBeforeRowInsertion;
	//			gridAreaForLastRow.leftCol = 0;
	//			gridAreaForLastRow.bottomRow = totalNumberOfRowsBeforeRowInsertion +1;
 //               gridAreaForLastRow.rightCol = totalNumberOfColumns;
	//		}
	//		else
	//		{
	//			gridAreaForLastRow.topRow = totalNumberOfRowsBeforeRowInsertion-1;
	//			gridAreaForLastRow.leftCol = 0;
	//			gridAreaForLastRow.bottomRow = totalNumberOfRowsBeforeRowInsertion;
 //               gridAreaForLastRow.rightCol = totalNumberOfColumns;
	//		}


	//		InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
	//		if(tableGeometry == NULL)
	//		{
	//			//CA("tableGeometry == NULL");
	//			break;//breaks the for..tableIndex
	//		}
	//		PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 			
	//		

	//		//bool16 tableHeaderPresent = kFalse;
	//	


	//		//int32 tableTypeId = -1;		
	//		//int32 sort_by_attributeId = -1;
	//		//for(int32 tagIndex = 0;tagIndex < slugInfo.tagPtr->GetChildCount();tagIndex++)
	//		//{//start for tagIndex = 0
	//		//	XMLReference cellXMLElementRef =   slugInfo.tagPtr->GetNthChild(tagIndex);
	//		//	//This is a tag attached to the cell.
	//		//	//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//		//	InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//		//	//Get the text tag attached to the text inside the cell.
	//		//	//We are providing only one texttag inside cell.
	//		//	if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//		//	{
	//		//		continue;
	//		//	}
	//		//	for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//		//	{
	//		//		XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//		//		//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
	//		//		InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
	//		//		//The cell may be blank. i.e doesn't have any text tag inside it.
	//		//		if(cellTextTagPtr == NULL)
	//		//		{
	//		//			continue;
	//		//		}
	//		//		
	//		//		if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
	//		//			 && HeaderRowPresent == kFalse)
	//		//		{
	//		//			//CA("header present");
	//		//			tableHeaderPresent = kTrue;
	//		//		}

	//		//		PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
	//		//		tableTypeId = fld_1.GetAsNumber();
	//		//		
	//		//		if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
	//		//		{		
	//		//			if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
	//		//			{							
	//		//				PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
	//		//				sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
	//		//			}
	//		//		}
	//		//							
	//		//		
	//		//		//parentID
	//		//		PMString attributeVal("");
	//		//		attributeVal.AppendNumber(pNode.getPubId());
	//		//		cellTextTagPtr->SetAttributeValue(WideString("parentID"),WideString(attributeVal));
	//		//		attributeVal.Clear();

	//		//		PMString id = cellTextTagPtr->GetAttributeValue(WideString("ID"));
	//		//		int32 ID = id.GetAsNumber();
	//		//								
	//		//		//parentTypeID
	//		//		if(ID != -701 && ID != -702 && ID != -703 && ID != -704)
	//		//		{
	//		//			attributeVal.AppendNumber(pNode.getTypeId());
	//		//			cellTextTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeVal));
	//		//			attributeVal.Clear();
	//		//		}

	//		//		//sectionID
	//		//		attributeVal.AppendNumber(sectionid);
	//		//		cellTextTagPtr->SetAttributeValue(WideString("sectionID"),WideString(attributeVal));
	//		//		
	//		//		attributeVal.Clear();
	//		//		attributeVal.AppendNumber(slugInfo.tableType);
	//		//		cellTextTagPtr->SetAttributeValue(WideString("tableType"),WideString(attributeVal));
	//		//		
	//		//		attributeVal.Clear();
	//		//		attributeVal.AppendNumber(pNode.getPBObjectID());
	//		//		cellTextTagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attributeVal));					
	//		//	}
	//		//}//end for tagIndex = 0 


	//		
	//		//VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPBObjectID());
	//		//if(!tableInfo)
	//		//{
	//		//	//CA("tableinfo is NULL");
	//		//	makeTagInsideCellImpotent(slugInfo.tagPtr); 
	//		//	break;
	//		//}
	//		//
	//		//if(tableInfo->size()==0)
	//		//{ //CA("tableinfo size==0");
	//		//	makeTagInsideCellImpotent(slugInfo.tagPtr); 
	//		//	break;
	//		//}
	//		//CItemTableValue oTableValue;
	//		//VectorScreenTableInfoValue::iterator it;

	//		//bool16 typeidFound=kFalse;
	//		//vector<int32> vec_items;
	//		//vector<int32> FinalItemIds;
	//		//FinalItemIds.clear();

	//		//for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		//{				
	//		//	oTableValue = *it;				
	//		//	vec_items = oTableValue.getItemIds();
	//		//
	//		//	if(tableTypeId == -1)	//--------
	//		//	{
	//		//		if(FinalItemIds.size() == 0)
	//		//		{
	//		//			FinalItemIds = vec_items;
	//		//		}
	//		//		else
	//		//		{
	//		//			for(int32 i=0; i<vec_items.size(); i++)
	//		//			{	bool16 Flag = kFalse;
	//		//				for(int32 j=0; j<FinalItemIds.size(); j++)
	//		//				{
	//		//					if(vec_items[i] == FinalItemIds[j])
	//		//					{
	//		//						Flag = kTrue;
	//		//						break;
	//		//					}				
	//		//				}
	//		//				if(!Flag)
	//		//					FinalItemIds.push_back(vec_items[i]);
	//		//			}
	//		//		}
	//		//	}
	//		//	else
	//		//	{
	//		//		if(tableTypeId != oTableValue.getTableTypeID())
	//		//			continue;

	//		//		if(FinalItemIds.size() == 0)
	//		//		{
	//		//			FinalItemIds = vec_items;
	//		//		}
	//		//		else
	//		//		{
	//		//			for(int32 i=0; i<vec_items.size(); i++)
	//		//			{	bool16 Flag = kFalse;
	//		//				for(int32 j=0; j<FinalItemIds.size(); j++)
	//		//				{
	//		//					if(vec_items[i] == FinalItemIds[j])
	//		//					{
	//		//						Flag = kTrue;
	//		//						break;
	//		//					}				
	//		//				}
	//		//				if(!Flag)
	//		//					FinalItemIds.push_back(vec_items[i]);
	//		//			}
	//		//		}
	//		//	}
	//		//	
	//		//}

	//		///*PMString FinaiItemIdsStr("FinalItemIds size = ");
	//		//FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
	//		//CA(FinaiItemIdsStr);*/
	//		//
	//		//if(tableInfo)
	//		//	delete tableInfo;

	//		//CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);
	//		
	//		//int32 TotalNoOfItems = getMMYTableRows(MMYTablePtr,tableIndex);
	//		//if(TotalNoOfItems <= 0)
	//		//{
	//		//	//CA("TotalNoOfItem <= 0");
	//		//	makeTagInsideCellImpotent(slugInfo.tagPtr);
	//		//	break;
	//		//}

	//		GridArea bodyArea = tableModel->GetBodyArea();
	//		int32 bodyCellCount = 0;
	//		ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
	//		ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
	//		while (iterTable1 != endTable1)
	//		{
	//			bodyCellCount++;
	//			++iterTable1;
	//		}
	//		
	//		int32 tableRows = tableModel->GetBodyRows().count;
	//		int32 CellCountPerRow = bodyCellCount/tableRows;

	//		/*bool16 isSameMake = kFalse;
	//		bool16 isSameModel = kFalse;

	//		int32 makeIndex = -1;
	//		int32 modelIndex = -1;

	//		int32 makeRepeatCnt = -1;
	//		int32 modelRepeatCnt = -1;

	//		int32 currentMakeModelCnt = -1;
	//		int32 currentModelItemCnt = -1;
	//		int32 currentModelItemIndex = -1;

	//		int32 colIndexPerRow = -1;

	//		vector<int32> itemIds;

	//		Vec_CMakeModel::const_iterator itr;
	//		Vec_CModelModel::const_iterator itr1;

	//		CMakeModel *makeModelPtr = NULL;
	//		CModelModel *modelModelPtr = NULL;

	//		multimap<PMString, int32>sortItems;
	//		multimap<PMString, int32>::iterator mapItr;
	//		int32 ITEMID = -1;
	//		int32 itemIDIndex = -1;*/


	//		//int32 tableSprayedCount = 0;
	//		//for(int32 selectedTableIndex = 0 ; selectedTableIndex < MMYTablePtr->vec_CMakeModelPtr->size() ; selectedTableIndex++)
	//		{
	//				
	//			
	//			int32 rowPatternRepeatCount = TotalNoOfItemsPerMake(MMYTablePtr, tableIndex);
	//			if(totalNumberOfRowsBeforeRowInsertion < rowPatternRepeatCount)
	//			{
	//				//CA("need to add rows");
	//				int32 rowsToBeStillAdded ;
	//									
	//				rowsToBeStillAdded =   rowPatternRepeatCount - totalNumberOfRowsBeforeRowInsertion;
	//					
	//				PMString rowsToBeStillAddedStr = "";
	//				rowsToBeStillAddedStr.AppendNumber(rowsToBeStillAdded);					
	//				//CA("rowsToBeStillAdded = " + rowsToBeStillAddedStr);

	//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//				if(tableCommands==NULL)
	//				{
	//					break;//continues the for..tableIndex
	//				}

	//				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
	//				{
	//					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
	//				}
	//				
	//				
	//				if(rowsToBeStillAdded > 0)
	//				{
	//					bool16 canCopy = tableModel->CanCopy(gridAreaForLastRow);
	//					if(canCopy == kFalse)
	//					{
	//						//CA("canCopy == kFalse");
	//						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
	//						break;
	//					}
	//					//CA("canCopy");
	//					
	//					TableMemento * tempPtr = tableModel->Copy(gridAreaForLastRow);
	//					if(tempPtr == NULL)
	//					{
	//						//CA("tempPtr == NULL");
	//						ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
	//						break;
	//					}
	//					//CA_NUM("rowPatternRepeatCount = ", rowPatternRepeatCount);
	//					
	//					for(int32 pasteIndex = 0;pasteIndex < rowsToBeStillAdded;pasteIndex++)
	//					{
	//						//CA_NUM("pasteIndex",pasteIndex);
	//						tableModel->Paste(GridAddress(pasteIndex + totalNumberOfRowsBeforeRowInsertion + headerCount,0),ITableModel::eAll,tempPtr);
	//						tempPtr = tableModel->Copy(gridAreaForLastRow);
	//					}
	//				}
	//				

	//			}
	//			else if(totalNumberOfRowsBeforeRowInsertion > rowPatternRepeatCount)
	//			{
	//				//CA("need to delete rows");
	//				int32 rowsToBeDeleted = totalNumberOfRowsBeforeRowInsertion - rowPatternRepeatCount;
	//				
	//				PMString rowsToBeDeletedStr = "";
	//				rowsToBeDeletedStr.AppendNumber(rowsToBeDeleted);					
	//				//CA("rowsToBeDeletedStr = " + rowsToBeDeletedStr);

	//				
	//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//				if(tableCommands==NULL)
	//				{
	//					break;//continues the for..tableIndex
	//				}

	//				
	//				tableCommands->DeleteRows(RowRange(totalNumberOfRowsBeforeRowInsertion + headerCount - rowsToBeDeleted,rowsToBeDeleted));
	//				
	//				
	//			}

	//			

	//									//int32 totalNumberOfRowsBeforeRowInsertion_new;
	//									////if(selectedTableIndex == 0)
	//									//	totalNumberOfRowsBeforeRowInsertion_new  = totalNumberOfRowsBeforeRowInsertion;
	//									///*else
	//									//{
	//									//	if(HeaderRowPresent)
	//									//		totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetBodyRows().count;
	//									//	else
	//									//		totalNumberOfRowsBeforeRowInsertion_new =  tableModel->GetTotalRows().count;
	//									//}*/
	//									//PMString d("totalNumberOfRowsBeforeRowInsertion : ");
	//									//d.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//									//d.Append("\n totalNumberOfRowsBeforeRowInsertion_new : ");
	//									//d.AppendNumber(totalNumberOfRowsBeforeRowInsertion_new);
	//									//d.Append("\n headerCount : ");
	//									//d.AppendNumber(headerCount);
	//									////CA(d);


	//									////int32 rowPatternRepeatCount = TotalNoOfItems;			
	//									//int32 rowPatternRepeatCount = TotalNoOfItemsPerMake(MMYTablePtr, tableIndex/*selectedTableIndex*/);
	//									////CA_NUM("rowPatternRepeatCount = ", rowPatternRepeatCount);
	//									////int32 rowsToBeStillAdded = TotalNoOfItems - totalNumberOfRowsBeforeRowInsertion;
	//									////we have to deal with 2 cases
	//									////case 1: when tableHeaders are absent
	//									////then the total number of rows i.e rowCount = FinalItemIds.size()-1
	//									////case 2 : when tableHeaders are present
	//									////then the total number of rows i.e rowCount = FinalItemsIds.size()
	//									//if(tableHeaderPresent /*&& selectedTableIndex == 0*/)
	//									//{  	
	//									//	//CA("tableHeaderPresent");
	//									//	//rowsToBeStillAdded = rowsToBeStillAdded + 1;
	//									//	rowPatternRepeatCount = rowPatternRepeatCount + 1;
	//									//}
	//									//
	//									////int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
	//									//int32 rowsToBeStillAdded ;
	//									////if(selectedTableIndex == 0)	
	//									//{
	//									//	//rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
	//									//	rowsToBeStillAdded =   rowPatternRepeatCount - totalNumberOfRowsBeforeRowInsertion;
	//									//	//rowPatternRepeatCount--;
	//									//}
	//									////else
	//									////	rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion;
	//									//

	//									//PMString rowsToBeStillAddedStr = "";
	//									//rowsToBeStillAddedStr.AppendNumber(rowsToBeStillAdded);					
	//									////CA("rowsToBeStillAdded = " + rowsToBeStillAddedStr);

	//									//InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//									//if(tableCommands==NULL)
	//									//{
	//									//	break;//continues the for..tableIndex
	//									//}
	//									//
	//									//GridArea bodyAreaBeforeAddingRow  = tableModel->GetBodyArea();
	//									//GridArea gridAreaIncludingHeader;
	//									////if(HeaderRowPresent)
	//									//{
	//									//	//CA("HeaderRowPresent 1111");
	//									//	//if(selectedTableIndex != 0)
	//									//	//{
	//									//	//	for(int32 rowIndex = 0 ; rowIndex < headerCount ;rowIndex++)
	//									//	//	{
	//									//	//		tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
	//									//	//	}

	//									//	//	gridAreaIncludingHeader = tableModel->GetBodyArea();

	//									//	//	bool16 canCopy = tableModel->CanCopy(headerArea);
	//									//	//	if(canCopy == kFalse)
	//									//	//	{
	//									//	//		//CA("canCopy == kFalse");
	//									//	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
	//									//	//		break;
	//									//	//	}
	//									//	//	//CA("canCopy");
	//									//	//	TableMemento * tempPtr = tableModel->Copy(headerArea);
	//									//	//	if(tempPtr == NULL)
	//									//	//	{
	//									//	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
	//									//	//		break;
	//									//	//	}
	//									//	//	//for(int32 pasteIndex = 1;pasteIndex <= headerCount;pasteIndex++)
	//									//	//	//{
	//									//	//		tableModel->Paste(GridAddress((/*pasteIndex * */totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion)+headerCount,0),ITableModel::eAll,tempPtr);
	//									//	//		tempPtr = tableModel->Copy(gridAreaForEntireTable);									
	//									//	//	//}
	//									//	//	totalNumberOfRowsBeforeRowInsertion_new = totalNumberOfRowsBeforeRowInsertion_new + headerCount;
	//									//	//
	//									//	//	rowPatternRepeatCount++;
	//									//	//}

	//									//	for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
	//									//	{
	//									//		tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+(headerCount-1)+ rowIndex,1),Tables::eAfter,rowHeight);
	//									//	}
	//									//	
	//									//	
	//									//	if(rowsToBeStillAdded > 0)
	//									//	{
	//									//		bool16 canCopy = tableModel->CanCopy(gridAreaForLastRow);
	//									//		if(canCopy == kFalse)
	//									//		{
	//									//			//CA("canCopy == kFalse");
	//									//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
	//									//			break;
	//									//		}
	//									//		//CA("canCopy");
	//									//		
	//									//		TableMemento * tempPtr = tableModel->Copy(gridAreaForLastRow);
	//									//		if(tempPtr == NULL)
	//									//		{
	//									//			ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
	//									//			break;
	//									//		}
	//									//		//CA_NUM("rowPatternRepeatCount = ", rowPatternRepeatCount);
	//									//		
	//									//		for(int32 pasteIndex = 0;pasteIndex < rowsToBeStillAdded;pasteIndex++)
	//									//		{
	//									//			CA_NUM("pasteIndex",pasteIndex);
	//									//			tableModel->Paste(GridAddress(3/*pasteIndex + totalNumberOfRowsBeforeRowInsertion + headerCount*/,0),ITableModel::eAll,tempPtr);
	//									//			tempPtr = tableModel->Copy(gridAreaForLastRow);
	//									//		}
	//									//	}
	//									//	
	//									//}
	//									////else
	//									////{
	//									////	//CA("HeaderRowPresent==kFalse");
	//									////	for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
	//									////	{
	//									////		tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion_new+ rowIndex-1,1),Tables::eAfter,rowHeight);

	//									////	}
	//									////	
	//									////	bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
	//									////	if(canCopy == kFalse)
	//									////	{
	//									////		//CA("canCopy == kFalse");
	//									////		break;
	//									////	}
	//									////	//CA("canCopy");
	//									////	TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
	//									////	if(tempPtr == NULL)
	//									////	{
	//									////		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable) == NULL");
	//									////		break;
	//									////	}
	//									////
	//									////	for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
	//									////	{
	//									////		tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion + totalNumberOfRowsBeforeRowInsertion_new - totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
	//									////		tempPtr = tableModel->Copy(gridAreaForEntireTable);
	//									////	}
	//									////}

	//									////int32 nonHeaderRowPatternIndex = 0;
	//				
	//				if(HeaderRowPresent)
	//				{
	//					//CA("HeaderRowPresent 1");
	//				//	for(int32 indexOfRowInTheHeaderRowPattern = 0 ; indexOfRowInTheHeaderRowPattern < headerCount ; indexOfRowInTheHeaderRowPattern++)
	//				//	{//for..iterate through each row of the headerRowPattern
	//				//		for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
	//				//		{//for..iterate through each column
	//							
	//					//GridArea gridArea = tableModel->GetCellArea (GridAddress(indexOfRowInTheHeaderRowPattern,columnIndex));
	//					GridArea headerArea;
	//					//if(selectedTableIndex == 0)
	//						headerArea = tableModel->GetHeaderArea();
	//					//else
	//					//{
	//					//	//CA("selectedTableIndex != 0");
	//					//	headerArea.leftCol = gridAreaIncludingHeader.leftCol;
	//					//	headerArea.bottomRow = gridAreaIncludingHeader.bottomRow;
	//					//	headerArea.rightCol = gridAreaIncludingHeader.rightCol;
	//					//	headerArea.topRow = bodyAreaBeforeAddingRow.bottomRow;
	//					//}
	//					ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
	//					ITableModel::const_iterator endTable(tableModel->end(headerArea));
	//					while (iterTable != endTable)
	//					{
	//						//CA("Table iterator");
	//						GridAddress gridAddress = (*iterTable);         
	//						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
	//						if(cellContent == nil) 
	//						{
	//							//CA("cellContent == nil");
	//							break;
	//						}
	//						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
	//						if(!cellXMLReferenceData) {
	//							//CA("!cellXMLReferenceData");
	//							break;
	//						}
	//						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
	//
	//						++iterTable;
	//					//		int32 tagIndex = indexOfRowInTheHeaderRowPattern * totalNumberOfColumns + columnIndex;
	//							
	//					//		XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
	//							//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//							if(!isMakeIDAttachedToTableTag)
	//							{
	//								XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
	//								InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());

	//								PMString attrVal("");
	//								attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
	//								tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
	//								isMakeIDAttachedToTableTag = kTrue;
	//							}
	//							
	//							//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//							//Also note that, cell may be blank i.e doesn't contain any textTag.
	//							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//							{
	//								continue;
	//							}
	//							for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//							{
	//								XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//								//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//								InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//								//Check if the cell is blank.
	//								if(cellTextXMLElementPtr == NULL)
	//								{
	//								continue;
	//								}

	//								//Get all the elementID and languageID from the cellTextXMLElement
	//								//Note ITagReader plugin methods are not used.
	//								PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//								PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//								//check whether the tag specifies ProductCopyAttributes.
	//								PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//								PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
	//								PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));///11/05/07
	//								PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//								PMString strDataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));

	//		
	//								int32 elementID = strElementID.GetAsNumber();
	//								int32 languageID = strLanguageID.GetAsNumber();
	//														
	//								int32 tagStartPos = -1;
	//								int32 tagEndPos = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//								tagStartPos = tagStartPos + 1;
	//								tagEndPos = tagEndPos -1;

	//								PMString dispName("");
	//								////if normal tag present in header row...
	//								////then we are spraying the value related to the first item.
	//								int32 itemID = FinalItemIds[0];

	//								//if(strDataType == "4" && isCallFromTS)	//-------------
	//								//{
	//								//	//CA("Going to spray table Name");
	//								//	CItemTableValue oTableValue;
	//								//	VectorScreenTableInfoValue::iterator it;
	//								//	for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
	//								//	{				
	//								//		oTableValue = *it;		
	//								//		if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
	//								//		{
	//								//			dispName = oTableValue.getName();
	//								//			attributeVal.Clear();
	//								//			attributeVal.AppendNumber(oTableValue.getTableID());
	//								//			cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
	//								//			
	//								//			attributeVal.Clear();
	//								//			attributeVal.AppendNumber(oTableValue.getTableTypeID());
	//								//			cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attributeVal));

	//								//			//CA(dispName);
	//								//			break;
	//								//		}
	//								//	}

	//								//}
	//								//else 
	//								if(strTypeId == "-1")
	//								{
	//									//CA("strTypeId == -1");
	//									int32 parentTypeID = strParentTypeID.GetAsNumber();

	//									//CA("#################normal tag present in header row###############");
	//									if(tableFlag == "1")
	//									{
	//										//CA("tableFlag == 1");
	//										//added on 11July...serial blast day
	//										//The following code will spray the table preset inside the table cell.
	//										//for copy ID = -104
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")// || index == "4")
	//										{//Product ItemTable
	//											PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//											TagStruct tagInfo;												
	//											tagInfo.whichTab = index.GetAsNumber();
	//											/*if(index == "3")
	//												tagInfo.parentId = pNode.getPubId();*/
	//											/*else if(index == "4")
	//												tagInfo.parentId = pNode.getPBObjectID();*/

	//											tagInfo.isTablePresent = tableFlag.GetAsNumber();
	//											tagInfo.typeId = typeID.GetAsNumber();
	//											//tagInfo.sectionID = sectionid;
	//																						
	//											GetItemTableInTabbedTextForm(tagInfo,dispName);
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
	//											//CA(dispName);
	//										}
	//										//end added on 11July...serial blast day
	//										else if(index == "4")
	//										{//This is the Section level ItemTableTag and we have selected the product
	//										//for spraying
	//											makeTheTagImpotent(cellTextXMLElementPtr);										
	//										}
	//										
	//									}

	//										
	//									else if(imgFlag == "1")
	//									{							
	//										XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//										UIDRef ref = cntentREF.GetUIDRef();
	//										if(ref == UIDRef::gNull)
	//										{
	//											//CA("ref == UIDRef::gNull");
	//											continue;
	//										}

	//										InterfacePtr<ITagReader> itagReader
	//										((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//										if(!itagReader)
	//											continue ;
	//										
	//										TagList tList=itagReader->getTagsFromBox(ref);
	//										TagStruct tagInfoo;
	//										int numTags=static_cast<int>(tList.size());

	//										if(numTags<0)
	//										{
	//											continue;
	//										}

	//										tagInfoo=tList[0];

	//										//tagInfoo.parentId = pNode.getPubId();
	//										tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//										//tagInfoo.sectionID=CurrentSubSectionID;

	//										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//										if(!iDataSprayer)
	//										{	
	//											//CA("!iDtaSprayer");
	//											continue;
	//										}

	//										//iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//										
	//										PMString attrVal;
	//										attrVal.AppendNumber(itemID);
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//											
	//										for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//										{
	//											tList[tagIndex].tagPtr->Release();
	//										}
	//										continue;
	//									}
	//									else if(tableFlag == "0")
	//									{
	//										//CA("tableFlag == 0 AAA");
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index =="4")
	//										{//Item
	//											//CA("index == 4 ");
	//											//added on 22Sept..EventPrice addition.												
	//											/*if(strElementID == "-701")
	//											{
	//												PMString  attributeIDfromNotes = "-1";
	//												bool8 isNotesValid = getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
	//												cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//												elementID = attributeIDfromNotes.GetAsNumber();
	//											}*/
	//											if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) != WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) != WideString("-101"))
	//												itemID = -1;/*pNode.getPubId()*/;

	//											//if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
	//											//{
	//											//	TagStruct tagInfo;
	//											//	tagInfo.elementId = elementID;
	//											//	tagInfo.tagPtr = cellTextXMLElementPtr;
	//											//	tagInfo.typeId = itemID;
	//											//	tagInfo.parentTypeID = parentTypeID;//added by Tushar on 27/12/06
	//											//	handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
	//											//}
	//											////ended on 22Sept..EventPrice addition.
	//											//else if(elementID == -803)  // For 'Letter Key'
	//											//{
	//											//	dispName = "a" ;			// For First Item										
	//											//}
	//											//else if((isComponentAttributePresent == 1) && (elementID == -805) )  // For 'Quantity'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][0]);
	//											//}
	//											//else if((isComponentAttributePresent == 1) && (elementID == -806) )  // For 'Availability'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][1]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][0]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][1]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][2]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][3]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][4]);			
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -812) )  // For 'Quantity'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][0]);
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -813) )  // For 'Required'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][1]);
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -814) )  // For 'Comments'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][2]);
	//											//}
	//											//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[0][4]) == "N"))
	//											//{
	//											//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
	//											//}
	//											//else if(elementID == -827)  // For 'Number Key'
	//											//{
	//											//	dispName = "1" ;
	//											//}
	//											if(elementID == -401)
	//											{
	//												dispName = MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_Name();

	//												PMString attrVal("");
	//												attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_id());
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
	//											}
	//											else
	//											{
	//												//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);										
	//												dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);  // getting single item attributye data at a time.
	//											}
	//											
	//											PMString attrVal;
	//											attrVal.AppendNumber(itemID);
	//											if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) == WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) == WideString("-101"))
	//											{
	//												//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	// 												cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//											}
	//											else
	//											{
	//												/*PMString attrVal("");
	//												attrVal.AppendNumber(pNode.getPubId());
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));*/
	//											}
	//										}
	//										else if( index == "3")
	//										{//Product
	//											
	//											dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(/*pNode.getPubId()*/-1,elementID,languageID, tagInfo.sectionID, kTrue);
	//											/*PMString attrVal;
	//											attrVal.AppendNumber(pNode.getTypeId());													
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//											attrVal.Clear();

	//											attrVal.AppendNumber(pNode.getPubId());
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));*/
	//										}
	//									}								
	//								}
	//								//else if(imgFlag == "1")
	//								//{							
	//								//	XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//								//	UIDRef ref = cntentREF.GetUIDRef();
	//								//	if(ref == UIDRef::gNull)
	//								//	{
	//								//		//CA("ref == UIDRef::gNull");
	//								//		continue;
	//								//	}

	//								//	InterfacePtr<ITagReader> itagReader
	//								//	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//								//	if(!itagReader)
	//								//		continue ;
	//								//	
	//								//	TagList tList=itagReader->getTagsFromBox(ref);
	//								//	TagStruct tagInfoo;
	//								//	int numTags=static_cast<int>(tList.size());

	//								//	if(numTags<0)
	//								//	{
	//								//		continue;
	//								//	}

	//								//	tagInfoo=tList[0];

	//								//	tagInfoo.parentId = pNode.getPubId();
	//								//	tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//								//	tagInfoo.sectionID=CurrentSubSectionID;

	//								//	IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//								//	if(!iDataSprayer)
	//								//	{	
	//								//		CA("!iDtaSprayer");
	//								//		continue;
	//								//	}

	//								//	iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//								//	
	//								//	PMString attrVal;
	//								//	attrVal.AppendNumber(itemID);
	//								//	cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								//	
	//								//	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//								//	{
	//								//		tList[tagIndex].tagPtr->Release();
	//								//	}
	//								//	
	//								//	continue;
	//								//}
	//								else
	//								{							
	//									if(tableFlag == "1")
	//									{					
	//										//CA("inside cell table is present");
	//									//added on 11July...the serial blast day
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")
	//										{
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
	//											do
	//											{
	//												PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//			//CA(strTypeID);
	//												int32 typeId = strTypeID.GetAsNumber();
	//												VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
	//												if(typeValObj==NULL)
	//												{
	//													ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
	//													break;
	//												}

	//												VectorTypeInfoValue::iterator it1;

	//												bool16 typeIDFound = kFalse;
	//												for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
	//												{	
	//													if(typeId == it1->getTypeId())
	//														{
	//															typeIDFound = kTrue;
	//															break;
	//														}			
	//												}
	//												if(typeIDFound)
	//													dispName = it1->getName();

	//												if(typeValObj)
	//													delete typeValObj;
	//											}while(kFalse);
	//													
	//										}
	//										//This is a special case.The tag is of product but the Item is
	//										//selected.So make this tag unproductive for refresh.(Impotent)
	//										else if(index == "4")
	//										{
	//											makeTheTagImpotent(cellTextXMLElementPtr);
	//											
	//										}
	//												
	//												
	//									}
	//									else
	//									{
	//							
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")
	//										{//For product
	//											CElementModel  cElementModelObj;
	//											bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
	//											if(result)
	//												dispName = cElementModelObj.getDisplayName();
	//											PMString attrVal;
	//											attrVal.AppendNumber(/*pNode.getTypeId()*/-1);
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//																				
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("tableFlag"),WideString("-13"));
	//										}
	//										else if(index == "4")
	//										{
	//											//following code added by Tushar on 27/12/06
	//											if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
	//											{
	//												/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
	//												int32 parentTypeID = strParentTypeID.GetAsNumber();
	//												
	//												VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
	//												if(TypeInfoVectorPtr != NULL)
	//												{
	//													VectorTypeInfoValue::iterator it3;
	//													int32 Type_id = -1;
	//													PMString temp = "";
	//													PMString name = "";
	//													for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
	//													{
	//														Type_id = it3->getTypeId();
	//														if(parentTypeID == Type_id)
	//														{
	//															temp = it3->getName();
	//															if(elementID == -701)
	//															{
	//																dispName = temp;
	//															}
	//															else if(elementID == -702)
	//															{
	//																dispName = temp + " Suffix";
	//															}
	//														}														
	//													}													
	//												}
	//												if(TypeInfoVectorPtr)
	//													delete TypeInfoVectorPtr;*/
	//											}
	//											else if(elementID == -703)
	//											{
	//												dispName = "$Off";
	//											}
	//											else if(elementID == -704)
	//											{
	//												dispName = "%Off";
	//											}
	//											else if((elementID == -805) )  // For 'Quantity'
	//											{
	//												//CA("Quantity Header");
	//												dispName = "Quantity";
	//											}
	//											else if((elementID == -806) )  // For 'Availability'
	//											{
	//												dispName = "Availability";			
	//											}
	//											else if(elementID == -807)
	//											{									
	//												dispName = "Cross-reference Type";
	//											}
	//											else if(elementID == -808)
	//											{									
	//												dispName = "Rating";
	//											}
	//											else if(elementID == -809)
	//											{									
	//												dispName = "Alternate";
	//											}
	//											else if(elementID == -810)
	//											{									
	//												dispName = "Comments";
	//											}
	//											else if(elementID == -811)
	//											{									
	//												dispName = "";
	//											}
	//											else if((elementID == -812) )  // For 'Quantity'
	//											{
	//												//CA("Quantity Header");
	//												dispName = "Quantity";
	//											}
	//											else if((elementID == -813) )  // For 'Required'
	//											{
	//												//CA("Required Header");
	//												dispName = "Required";
	//											}
	//											else if((elementID == -814) )  // For 'Comments'
	//											{
	//												//CA("Comments Header");
	//												dispName = "Comments";
	//											}

	//											else
	//												dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

	//											//Modification of XMLTagAttributes.
	//											//modify the XMLTagAttribute "typeId" value to -2.
	//											//We have assumed that there is atleast one stencil with HeaderAttirbute
	//											//true .So assign for all the stencil in the header row , typeId == -2
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
	//											//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
	//										}									
	//									}
	//								}
	//								PMString textToInsert("");
	//								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//								if(!iConverter)
	//								{
	//									textToInsert=dispName;					
	//								}
	//								else
	//									textToInsert=iConverter->translateString(dispName);

	//								//CA("textToInsert = " + textToInsert);
	//								//Spray the Header data .We don't have to change the tag attributes
	//								//as we have done it while copy and paste.
	//								//WideString insertData(textToInsert);
	//								//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
	//								/*K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
	//								ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
	//								*/
	//								WideString insertText(textToInsert);

	//								if(iConverter)
	//									iConverter->ChangeQutationMarkONOFFState(kFalse);
	//								textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);

	//								if(iConverter)
	//									iConverter->ChangeQutationMarkONOFFState(kTrue);
	//							}
	//				//		}//for..iterate through each column
	//					}//for..iterate through each row of the headerRowPattern
	//				
	//					//nonHeaderRowPatternIndex = 1;
	//					//rowPatternRepeatCount++;

	//				}

	//				if(tableHeaderPresent)
	//				{//if 2 tableHeaderPresent 
	//					//CA("tableHeaderPresent");
	//				//First rowPattern  now is of header element.
	//				//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
	//				/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
	//				s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//				CA(s);*/
	//					for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
	//					{//for..iterate through each row of the repeatedRowPattern
	//						/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
	//						s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//						CA(s);*/
	//						for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
	//						{//for..iterate through each column
	//							int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
	//							XMLReference cellXMLElementRef = tagInfo.tagPtr->GetNthChild(tagIndex);
	//							//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//							//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//							//Also note that, cell may be blank i.e doesn't contain any textTag.
	//							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//							{
	//								continue;
	//							}
	//							for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//							{
	//								XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//								//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//								InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//								//Check if the cell is blank.
	//								if(cellTextXMLElementPtr == NULL)
	//								{
	//									continue;
	//								}

	//								//Get all the elementID and languageID from the cellTextXMLElement
	//								//Note ITagReader plugin methods are not used.
	//								PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//								PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//								//check whether the tag specifies ProductCopyAttributes.
	//								PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//								
	//								int32 elementID = strElementID.GetAsNumber();
	//								int32 languageID = strLanguageID.GetAsNumber();
	//								
	//								int32 tagStartPos = -1;
	//								int32 tagEndPos = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//								tagStartPos = tagStartPos + 1;
	//								tagEndPos = tagEndPos -1;

	//								PMString dispName(""); 
	//								PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//								if(index == "3")
	//								{
	//									makeTheTagImpotent(cellTextXMLElementPtr);
	//								}					
	//								else if(tableFlag == "1")
	//								{	
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
	//									do
	//									{
	//										PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//	//CA(strTypeID);
	//										int32 typeId = strTypeID.GetAsNumber();
	//										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
	//										if(typeValObj==NULL)
	//										{
	//											ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
	//											break;
	//										}

	//										VectorTypeInfoValue::iterator it1;

	//										bool16 typeIDFound = kFalse;
	//										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
	//										{	
	//											   if(typeId == it1->getTypeId())
	//												{
	//													typeIDFound = kTrue;
	//													break;
	//												}			
	//										}
	//										if(typeIDFound)
	//											dispName = it1->getName();

	//										if(typeValObj)
	//											delete typeValObj;
	//									}while(kFalse);
	//									//CA("You have to insert item item table here");
	//									
	//									//dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
	//									//Modification of XMLTagAttributes.
	//									//modify the XMLTagAttribute "typeId" value to -2.
	//									//We have assumed that there is atleast one stencil with HeaderAttirbute
	//									//true .So assign for all the stencil in the header row , typeId == -2
	//									//cellTextXMLElementPtr->SetAttributeValue("typeId","-2");
	//									
	//								}
	//								else
	//								{
	//									//CA("tableFlag	!= 1");
	//									//following code added by Tushar on 27/12/06
	//									if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
	//									{
	//										/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
	//										int32 parentTypeID = strParentTypeID.GetAsNumber();
	//										
	//										VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
	//										if(TypeInfoVectorPtr != NULL)
	//										{
	//											VectorTypeInfoValue::iterator it3;
	//											int32 Type_id = -1;
	//											PMString temp = "";
	//											PMString name = "";
	//											for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
	//											{
	//												Type_id = it3->getTypeId();
	//												if(parentTypeID == Type_id)
	//												{
	//													temp = it3->getName();
	//													if(elementID == -701)
	//													{
	//														dispName = temp;
	//													}
	//													else if(elementID == -702)
	//													{
	//														dispName = temp + " Suffix";
	//													}
	//												}
	//											}
	//										}

	//										if(TypeInfoVectorPtr)
	//											delete TypeInfoVectorPtr;*/
	//									}
	//									else if(elementID == -703)
	//									{
	//										dispName = "$Off";
	//									}
	//									else if(elementID == -704)
	//									{
	//										dispName = "%Off";
	//									}
	//									else if(elementID == -401)  
	//									{
	//										//CA("Make");
	//										dispName = "Make";
	//									}
	//									else if(elementID == -402)  
	//									{
	//										//CA("Model");
	//										dispName = "Model";
	//									}
	//									else if(elementID == -403)  
	//									{
	//										//CA("Year");
	//										dispName = "Year";
	//									}
	//									else if((elementID == -805) )  // For 'Quantity'
	//									{
	//										//CA("Quantity Header");
	//										dispName = "Quantity";
	//									}
	//									else if((elementID == -806) )  // For 'Availability'
	//									{
	//										dispName = "Availability";			
	//									}
	//									else if(elementID == -807)
	//									{									
	//										dispName = "Cross-reference Type";
	//									}
	//									else if(elementID == -808)
	//									{									
	//										dispName = "Rating";
	//									}
	//									else if(elementID == -809)
	//									{									
	//										dispName = "Alternate";
	//									}
	//									else if(elementID == -810)
	//									{									
	//										dispName = "Comments";
	//									}
	//									else if(elementID == -811)
	//									{									
	//										dispName = "";
	//									}
	//									else if((elementID == -812) )  // For 'Quantity'
	//									{
	//										//CA("Quantity Header");
	//										dispName = "Quantity";
	//									}
	//									else if((elementID == -813) )  // For 'Required'
	//									{
	//										//CA("Required Header");
	//										dispName = "Required";
	//									}
	//									else if((elementID == -814) )  // For 'Comments'
	//									{
	//										//CA("Comments Header");
	//										dispName = "Comments";
	//									}
	//									else
	//									{
	//										//CA("else");
	//										dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
	//										//CA("dispName	:	"+dispName);

	//									}									
	//									//CA("dispName :" + dispName);
	//									//Modification of XMLTagAttributes.
	//									//modify the XMLTagAttribute "typeId" value to -2.
	//									//We have assumed that there is atleast one stencil with HeaderAttirbute
	//									//true .So assign for all the stencil in the header row , typeId == -2
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
	//									//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
	//								}
	//								
	//								PMString textToInsert("");
	//								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//								if(!iConverter)
	//								{
	//									textToInsert=dispName;					
	//								}
	//								else
	//									textToInsert=iConverter->translateString(dispName);

	//								//CA("textToInsert = " + textToInsert);
	//								//Spray the Header data .We don't have to change the tag attributes
	//								//as we have done it while copy and paste.
	//								//WideString insertData(textToInsert);
	//								//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
	//								/*K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
	//								ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
	//							*/
	//								WideString insertText(textToInsert);

	//								if(iConverter)
	//									iConverter->ChangeQutationMarkONOFFState(kFalse);

	//								textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);

	//								if(iConverter)
	//									iConverter->ChangeQutationMarkONOFFState(kTrue);
	//							
	//							}
	//						}//end for..iterate through each column.
	//					}//end for..iterate through each row of repeatedRowPattern.
	//					//All the subsequent rows will now spray the ItemValue.
	////nonHeaderRowPatternIndex = 1;					
	//				}//end if 2 tableHeaderPresent

	//				
	//				GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);

	//				int32 ColIndex = -1;
	//				GridArea bodyArea_new = tableModel->GetBodyArea();
	//				
	//				ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
	//				ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));

	//			/*	bool16 isSameMake = kFalse;
	//				bool16 isSameModel = kFalse;

	//				int32 makeIndex = -1;
	//				int32 modelIndex = -1;

	//				int32 makeRepeatCnt = -1;
	//				int32 modelRepeatCnt = -1;

	//				int32 currentMakeModelCnt = -1;
	//				int32 currentModelItemCnt = -1;
	//				int32 currentModelItemIndex = -1;

	//				int32 colIndexPerRow = -1;

	//				vector<int32> itemIds;

	//				Vec_CMakeModel::const_iterator itr;
	//				Vec_CModelModel::const_iterator itr1;

	//				CMakeModel *makeModelPtr = NULL;
	//				CModelModel *modelModelPtr = NULL;

	//				multimap<PMString, int32>sortItems;
	//				multimap<PMString, int32>::iterator mapItr;
	//				int32 ITEMID = -1;
	//				int32 itemIDIndex = -1;*/

	//				PMString PrevYearString("");
	//				PMString PrevModelNameString("");
	//				while (iterTable != endTable)
	//				{
	//					//CA("Iterating table");
	//					GridAddress gridAddress = (*iterTable); 
	//					if(tableHeaderPresent)
	//						if(gridAddress <= headerRowGridAddress){
	//							iterTable++;							
	//							continue;
	//						}

	//					/*if(selectedTableIndex != 0)
	//					{
	//						if(bodyAreaBeforeAddingRow.Contains(gridAddress) || gridAreaIncludingHeader.Contains(gridAddress))
	//						{
	//							++iterTable;
	//							continue;
	//						}
	//					}*/

	//					if(isSameMake == kFalse)
	//					{
	//						//CA("isSameMake == kFalse");
	//						makeIndex++;

	//						makeModelPtr = MMYTablePtr->vec_CMakeModelPtr->at(makeIndex);

	//						currentMakeModelCnt = static_cast<int32>(makeModelPtr->vec_CModelModelPtr->size());

	//						isSameMake = kTrue;
	//					}

	//					if(isSameModel == kFalse)
	//					{
	//						//CA("isSameModel == kFalse");
	//						modelIndex++;
	//						modelModelPtr = makeModelPtr->vec_CModelModelPtr->at(modelIndex);
	//						
	//						currentModelItemCnt = static_cast<int32>(modelModelPtr->vec_ItemYearPtr->size());
	//						
	//						itemIds.clear();
	//						Vec_ItemYear::iterator itr2 = modelModelPtr->vec_ItemYearPtr->begin();
	//						
	//						for(;itr2 != modelModelPtr->vec_ItemYearPtr->end(); itr2++)
	//						{
	//							PMString itemIdStr("(*itr2)->item_Id = ");
	//							itemIdStr.AppendNumber((*itr2)->item_Id);
	//							//CA(itemIdStr);

	//							itemIds.push_back((*itr2)->item_Id);
	//						}

	//						if(sort_by_attributeId != -1)
	//						{
	//							sortItems.clear();
	//							vector<int32>::iterator itr3 = itemIds.begin();
	//							for(;itr3 != itemIds.end(); itr3++)
	//							{
	//								PMString itemIdStr("*itr3 = ");
	//								itemIdStr.AppendNumber(*itr3);
	//								//CA(itemIdStr);
	//								PMString sortAttributeValue = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(*itr3,sort_by_attributeId,languageId);
	//								//CA("sortAttributeValue = " + sortAttributeValue);
	//								double val = sortAttributeValue.GetAsDouble();
	//								sortItems.insert(multimap<double,int32>::value_type(val,*itr3));
	//							}
	//						}
	//						
	//						currentModelItemIndex++;

	//						isSameModel = kTrue;

	//						mapItr = sortItems.begin();
	//					}

	//					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
	//					if(cellContent == nil) 
	//					{
	//						break;
	//					}
	//					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
	//					if(!cellXMLReferenceData) {
	//						break;
	//					}

	//					


	//					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
	//					++iterTable;

	//					ColIndex++;
	//					
	//					int32 rowIndex = ColIndex/CellCountPerRow;
	//					//CA_NUM("ColIndex = ", ColIndex);
	//					//CA_NUM("CellCountPerRow = ", CellCountPerRow);
	//					//CA_NUM("rowIndex = ", rowIndex);

	//					colIndexPerRow++;	

	//					if(itemIDIndex != rowIndex /*&& mapItr != NULL*/)	//---conv.-VS 2005 to VS 2008-----
	//					{
	//						//CA("itemIDIndex != rowIndex && mapItr != NULL");
	//						ITEMID = mapItr->second;
	//						mapItr++;
	//						itemIDIndex = rowIndex;
	//					}

	//					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());										
	//					if(!isMakeIDAttachedToTableTag)
	//					{
	//						XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
	//						InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());

	//						PMString attrVal("");
	//						attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
	//						tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
	//						isMakeIDAttachedToTableTag = kTrue;
	//					}
	//					
	//					//Note that, cell may be blank i.e doesn't contain any textTag.
	//					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//					{
	//						//CA("continue");
	//						continue;
	//					}
	//		
	//					//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//					//so always take the 0th childTag of the Cell.
	//					int32 cellChildCount = cellXMLElementPtr->GetChildCount();
	//					for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount ;tagIndex1++)
	//					{
	//						XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//							
	//						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//						//Check if the cell is blank.
	//						if(cellTextXMLElementPtr == NULL)
	//						{	
	//							continue;
	//						}
	//						
	//						//Get all the elementID and languageID from the cellTextXMLElement
	//						//Note ITagReader plugin methods are not used.
	//						//CA(cellTextXMLElementPtr->GetTagString());

	//						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//						//check whether the tag specifies ProductCopyAttributes.
	//						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID")); //added by Tushar on 22/12/06
	//						PMString imageFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
	//						/*CA("strParentTypeID =" + strParentTypeID);
	//						CA("strElementID :" + strElementID);
	//						CA("tableFlag :" + tableFlag);*/
	//						
	//						int32 elementID = strElementID.GetAsNumber();
	//						int32 languageID = strLanguageID.GetAsNumber();
	//						int32 itemID = ITEMID;//FinalItemIds[itemIDIndex];
	//						int32 parentTypeID = strParentTypeID.GetAsNumber();	//added by Tushar on 22/12/06
	//																	 
	//						int32 tagStartPos = -1;
	//						int32 tagEndPos = -1;
	//						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//						tagStartPos = tagStartPos + 1;
	//						tagEndPos = tagEndPos -1;
	//					
	//						PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
	//						int32 isEventFieldVal = isEventFieldStr.GetAsNumber();

	//						PMString dispName("");
	//						PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//						if(index == "3")
	//						{
	//							makeTheTagImpotent(cellTextXMLElementPtr);
	//						}
	//						else if(tableFlag == "1")
	//						{
	//							//CA("tableFlag == 1");
	//							
	//							//The following code will spray the table preset inside the table cell.
	//							//for copy ID = -104
	//							
	//							if(index == "4")
	//							{//Item ItemTable
	//								//CA("index == 4");

	//								XMLContentReference xmlCntnRef = cellTextXMLElementPtr->GetContentReference();
	//								InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());

	//								bool16 isTablePresentInsideCell = xmlCntnRef.IsTable();
	//								/////following functionallity is not yet completed 
	//								//for section level item having items which contains their own item table. 
	//								if(isTablePresentInsideCell)
	//								{
	//									//CA("isTablePresentInsideCell = kTrue");
	//									//
	//									//InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
	//									//if(innerTableModel == NULL)
	//									//{
	//									//	CA("innerTableModel == NULL");
	//									//	continue;
	//									//}
	//									//UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
	//					
	//									//XMLTagAttributeValue xmlTagAttrVal;
	//									////collect all the attributes from the tag.
	//									////typeId,LanguageID are important
	//									//getAllXMLTagAttributeValues(cellTextXMLElementRef,xmlTagAttrVal);	

	//									////CA("xmlTagAttrVal.typeId : " + xmlTagAttrVal.typeId);

	//									///*if(xmlTagAttrVal.typeId.GetAsNumber() == -111)
	//									//{
	//									//	fillDataInCMedCustomTableInsideTable
	//									//	(
	//									//		cellTextXMLElementRef,
	//									//		innerTableModelUIDRef,
	//									//		pNode

	//									//	);
	//									//}
	//									//else
	//									//{*/
	//									//	FillDataTableForItemOrProduct
	//									//	(
	//									//		cellTextXMLElementRef,
	//									//		innerTableModelUIDRef,
	//									//		kFalse
	//									//	);
	//									///*}*/

	//									//UIDList itemList(slugInfo.curBoxUIDRef);
	//									//UIDList processedItems;
	//									//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
	//									//ErrorCode status =  ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);

	//									//tableflag++;
	//									//continue;
	//								}

	//								PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//								TagStruct tagInfo;												
	//								tagInfo.whichTab = index.GetAsNumber();
	//								tagInfo.parentId = -1; //pNode.getPBObjectID();

	//								tagInfo.isTablePresent = tableFlag.GetAsNumber();
	//								tagInfo.typeId = typeID.GetAsNumber();
	//								tagInfo.sectionID = -1;//sectionid;
	//								
	//									
	//								GetItemTableInTabbedTextForm(tagInfo,dispName);
	//								/*UIDRef tempBox = boxUIDRef;
	//								sprayTableInsideTableCell(tempBox,tagInfo);*/
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));

	//								PMString attrVal;
	//								attrVal.Clear();
	//								attrVal.AppendNumber(-1/*pNode.getPubId()*/);
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								attrVal.Clear();

	//								attrVal.AppendNumber(-1/*pNode.getPBObjectID()*/);
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));

	//								//CA(dispName);
	//							}
	//							//end added on 11July...serial blast day
	//							/*else if(index == "4")
	//							{//Item ItemTable

	//								CA("You have to add ItemItemTable here");													
	//								dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID, languageID, kFalse );
	//							
	//								//Modify the XMLTagAttribute "typeID" .
	//								//Set its value to itemID;
	//								PMString attrVal;
	//								attrVal.AppendNumber(itemID);
	//								cellTextXMLElementPtr->SetAttributeValue("typeId",attrVal);								
	//							}
	//							*/
	//						}
	//						else if(imageFlag == "1")
	//						{
	//							XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//							UIDRef ref = cntentREF.GetUIDRef();
	//							if(ref == UIDRef::gNull)
	//							{
	//								////CA("if(ref == UIDRef::gNull)");
	//								int32 StartPos1 = -1;
	//								int32 EndPos1 = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									

	//								InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
	//									textModel->QueryStrand(kOwnedItemStrandBoss, 
	//									IItemStrand::kDefaultIID)));
	//								if (itemStrand == nil) {
	//									//CA("invalid itemStrand");
	//									continue;
	//								}
	//								//CA("going for Owened items");
	//								OwnedItemDataList ownedList;
	//								itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
	//								int32 count = ownedList.size();
	//								
	//							/*	PMString ASD("count : ");
	//								ASD.AppendNumber(count);
	//								CA(ASD);*/

	//								if(count > 0)
	//								{
	//									//CA(" count > 0 ");
	//									UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
	//									if(newRef == UIDRef::gNull)
	//									{
	//										//CA("ref == UIDRef::gNull....2");
	//										continue;
	//									}
	//									ref = newRef;
	//								}else
	//									continue;
	//							}

	//							InterfacePtr<ITagReader> itagReader
	//							((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//							if(!itagReader)
	//								continue ;
	//							
	//							TagList tList=itagReader->getTagsFromBox(ref);
	//							TagStruct tagInfoo;
	//							int numTags=static_cast<int>(tList.size());

	//							if(numTags<=0)
	//							{
	//								continue;
	//							}

	//							tagInfoo=tList[0];

	//							tagInfoo.parentId = itemID;//pNode.getPubId();
	//							tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//							tagInfoo.sectionID=CurrentSubSectionID;

	//							IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//							if(!iDataSprayer)
	//							{	
	//								//CA("!iDtaSprayer");
	//								continue;
	//							}

	//							if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
	//							|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
	//							|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
	//							|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
	//							{
	//								iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
	//							}
	//							else if(tagInfoo.elementId > 0)
	//							{
	//								//isInlineImage = kTrue;
	//								//CA("Calling fillPVAndMPVImageInBox 1");
	//								ptrIAppFramework->clearAllStaticObjects();
	//								iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo,itemID,kTrue);
	//								iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
	//													
	//							}
	//							else
	//							{
	//								//iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//							}

	//							
	//						
	//							PMString attrVal;
	//							attrVal.AppendNumber(itemID);													
	//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//							attrVal.Clear();
	//							
	//							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//							{
	//								tList[tagIndex].tagPtr->Release();
	//							}
	//							continue;
	//						}
	//						else if(tableFlag == "0")
	//						{
	//							//CA("tableFlag == 0");
	//							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//							PMString colNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
	//							PMString ischildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
	//							PMString rowno = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
	//							if(index =="4" && ischildTag == "1")
	//							{
	//								//CA("index == 4 && ischildTag == 1");
	//								if(elementID == -803)  // For 'Letter Key'
	//								{
	//									//CA("here3");
	//									/*dispName.Clear();
	//									if(itemIDIndex < 26)
	//										dispName.Append(AlphabetArray[itemIDIndex]);
	//									else
	//										dispName = "a" ;*/	
	//									dispName.Clear();
	//									int wholeNo =  itemIDIndex / 26;
	//									int remainder = itemIDIndex % 26;								

	//									if(wholeNo == 0)
	//									{// will print 'a' or 'b'... 'z'  upto 26 item ids
	//										dispName.Append(AlphabetArray[remainder]);
	//									}
	//									else  if(wholeNo <= 26)
	//									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
	//										dispName.Append(AlphabetArray[wholeNo-1]);	
	//										dispName.Append(AlphabetArray[remainder]);	
	//									}
	//									else if(wholeNo <= 52)
	//									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
	//										dispName.Append(AlphabetArray[0]);
	//										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
	//										dispName.Append(AlphabetArray[remainder]);										
	//									}
	//								}
	//								//else if((isComponentAttributePresent == 1) && (elementID == -805))
	//								//{
	//								//	//CA("Here 1");
	//								//	dispName.Clear();			
	//								//	//CA(Kit_vec_tablerows[0][0]);
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);	
	//								//}
	//								//else if((isComponentAttributePresent == 1) && (elementID == -806))
	//								//{	//CA("Here 2");									
	//								//	dispName.Clear();	
	//								//	//CA(Kit_vec_tablerows[0][1]);
	//								//	dispName = ((Kit_vec_tablerows[itemIDIndex][1]));	
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][1]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][2]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][3]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][4]);			
	//								//	//CA(dispName);
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -812))
	//								//{	//CA("Here 3");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][0]));	
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -813))
	//								//{	//CA("Here 4");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][1]));	
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -814))
	//								//{	//CA("Here 5");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][2]));	
	//								//}
	//								//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[itemIDIndex][4]) == "N"))
	//								//{
	//								//	//CA(" non catalog items");
	//								//	dispName.Clear();	
	//								//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
	//								//}
	//								else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
	//								{
	//									/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(ITEMID,elementID,CurrentSubSectionID);
	//									if(vecPtr != NULL){
	//										if(vecPtr->size()> 0){
	//											dispName = vecPtr->at(0).getObjectValue();	
	//										}
	//									}*/
	//								}
	//								//else if(elementID == -401  || elementID == -402  ||  elementID == -403)		//----For Make, Mode, Year . 
	//								//{
	//								//	dispName = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemID,elementID, languageID);
	//								//}
	//								else if(elementID == -401)		//----For Make, Mode, Year . 
	//								{
	//									dispName = makeModelPtr->getMake_Name();
	//								}
	//								else if(elementID == -402)		//----For Make, Mode, Year . 
	//								{
	//									dispName = modelModelPtr->getModel_Name();										
	//									if(PrevModelNameString.IsEqual(dispName))
	//									{
	//										dispName = "";
	//										PMString attrVal;
	//										attrVal.AppendNumber(1);											
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));

	//									}
	//									else
	//										PrevModelNameString = dispName;
	//								}
	//								else if(elementID == -403)		//----For Make, Mode, Year . 
	//								{
	//									Vec_ItemYear::iterator itemYearItr = modelModelPtr->vec_ItemYearPtr->begin();
	//									for(; itemYearItr != modelModelPtr->vec_ItemYearPtr->end(); itemYearItr++)
	//									{
	//										if(itemID == (*itemYearItr)->item_Id)
	//										{
	//											dispName = (*itemYearItr)->year;
	//											if(PrevYearString.IsEqual(dispName))
	//											{
	//												dispName = "";
	//												PMString attrVal;
	//												attrVal.AppendNumber(1);											
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));
	//											}
	//											else
	//												PrevYearString = dispName;
	//										}
	//									}										
	//								}
	//								else if(elementID == -827)  //**** Number Keys
	//								{
	//									dispName.Clear();
	//									dispName.Append(itemIDIndex+1);
	//								}
	//								else 
	//								{
	//									//CA("catalog Items");
	//									dispName.Clear();	
	//									//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);
	//									dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);
	//								}
	//								PMString attrVal;
	//								attrVal.AppendNumber(itemID);
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//								
	//								/*attrVal.Clear();
	//								attrVal.AppendNumber(PARENTID);														
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								
	//								attrVal.Clear();
	//								attrVal.AppendNumber(pNode.getPBObjectID());
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));	
	//							*/
	//								if(rowno == "-904")
	//								{

	//									attrVal.Clear();
	//									attrVal.AppendNumber(makeModelPtr->getMake_id());														
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
	//								
	//									if(elementID != -401)		//----For Make, Mode, Year . 
	//									{
	//										attrVal.Clear();
	//										attrVal.AppendNumber(modelModelPtr->getModel_Id());														
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("field4"),WideString(attrVal));
	//									}
	//								}
	//							}
	//							else if( index == "4")
	//							{//SectionLevelItem
	//								//CA("index == 4");
	//							////added on 22Sept..EventPrice addition.
	//								//if(strElementID == "-701")
	//								//{
	//								//	PMString  attributeIDfromNotes = "-1";
	//								//	bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
	//								//	cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//								//	elementID = attributeIDfromNotes.GetAsNumber();
	//								//}
	//								////ended on 22Sept..EventPrice addition.	
	//								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
	//								{
	//									TagStruct tagInfo;
	//									tagInfo.elementId = elementID;
	//									tagInfo.tagPtr = cellTextXMLElementPtr;
	//									tagInfo.typeId = PARENTID;
	//									//handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);									
	//									//PMString  attributeIDfromNotes = "-1";
	//									//bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
	//									//cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//									//elementID = attributeIDfromNotes.GetAsNumber();
	//								}
	//								else if(elementID == -803)  // For 'Letter Key'
	//								{
	//									//CA("here4");
	//									/*dispName.Clear();
	//									if(itemIDIndex < 26)
	//										dispName.Append(AlphabetArray[itemIDIndex]);
	//									else
	//										dispName = "a" ;*/		
	//									dispName.Clear();
	//									int wholeNo =  itemIDIndex / 26;
	//									int remainder = itemIDIndex % 26;								

	//									if(wholeNo == 0)
	//									{// will print 'a' or 'b'... 'z'  upto 26 item ids
	//										dispName.Append(AlphabetArray[remainder]);
	//									}
	//									else  if(wholeNo <= 26)
	//									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
	//										dispName.Append(AlphabetArray[wholeNo-1]);	
	//										dispName.Append(AlphabetArray[remainder]);	
	//									}
	//									else if(wholeNo <= 52)
	//									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
	//										dispName.Append(AlphabetArray[0]);
	//										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
	//										dispName.Append(AlphabetArray[remainder]);										
	//									}
	//								}	
	//								else
	//									//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID,kTrue);
	//									dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(PARENTID,elementID,languageID);
	//								if(elementID == -827)  // For 'Number Key'
	//								{
	//									dispName.Clear();
	//									dispName.Append(itemIDIndex+1);
	//								}
	//								
	//								PMString attrVal;
	//								attrVal.AppendNumber(/*pNode.getPubId()*/PARENTID);	
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("childTag"),WideString("1"));

	//								cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//									
	//								
	//								/*attrVal.Clear();
	//								attrVal.AppendNumber(pNode.getPBObjectID());
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
	//					*/
	//							}

	//						}
	//						cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("-1"));
	//						/*if(isCallFromTS ){
	//							PMString temp("");
	//							temp.AppendNumber(tableSourceInfoValueObj->getVec_Table_ID().at(0));
	//							cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(temp));								
	//						}*/
	//						PMString textToInsert("");
	//						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//						if(!iConverter)
	//						{
	//							textToInsert=dispName;					
	//						}
	//						else
	//							textToInsert=iConverter->translateString(dispName);
	//						//CA("textToInsert = " + textToInsert);
	//						//Spray the Header data .We don't have to change the tag attributes
	//						//as we have done it while copy and paste.	
	//						//WideString insertData(textToInsert);
	//						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
	//						/*K2::shared_ptr<WideString> insertData(new WideString(textToInsert));
	//						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);*/
	//						
	//						WideString insertData(textToInsert);
	//						
	//						if(iConverter)
	//							iConverter->ChangeQutationMarkONOFFState(kFalse);

	//						textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);

	//						if(iConverter)
	//									iConverter->ChangeQutationMarkONOFFState(kTrue);
	//					}
	//					
	//					if(colIndexPerRow == CellCountPerRow - 1)
	//					{
	//						//CA("colIndexPerRow == CellCountPerRow - 1");
	//						colIndexPerRow = -1;
	//						currentModelItemIndex++;

	//						if(currentModelItemIndex == currentModelItemCnt)
	//						{
	//							//CA("currentModelItemIndex == currentModelItemCnt");
	//							if(modelIndex < currentMakeModelCnt - 1)
	//							{
	//								//CA("modelIndex < currentMakeModelCnt - 1");
	//								isSameModel = kFalse;
	//								currentModelItemCnt = -1;
	//								currentModelItemIndex = -1;

	//							}else
	//							{
	//								//CA("else");
	//								isSameMake = kFalse;
	//								isSameModel = kFalse;
	//								modelIndex = -1;
	//								currentMakeModelCnt = -1;
	//								currentModelItemCnt = -1;
	//								currentModelItemIndex = -1;
	//							}
	//						}
	//					}	
	//				}//end for..iterate through each column	
	//				
	//				//tableSprayedCount--;
	//			}
	//	}

	//	delete MMYTablePtr;
	//	result = kSuccess;
	//}while(kFalse);

	return result;
}

int32 findOutHowManyMMYTablesInTextFrame(TagStruct& tagInfo)
{
	int32 noOfMMYTables = 0;
	XMLReference printsourceXMLElementRef = tagInfo.tagPtr->GetParent();
	InterfacePtr<IIDXMLElement>printsourceXMLElementPtr(printsourceXMLElementRef.Instantiate());
	int32 printsourceChildCount = printsourceXMLElementPtr->GetChildCount();
	for(int32 tagIndex = 0;tagIndex < printsourceChildCount ;tagIndex++)
	{
		XMLReference tagXMLElementRef = printsourceXMLElementPtr->GetNthChild(tagIndex);
			
		//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
		InterfacePtr<IIDXMLElement>TagXMLElementPtr(tagXMLElementRef.Instantiate());
		
		PMString tagString = TagXMLElementPtr->GetTagString();
		if(tagString.IsEqual("PSTable0"))
			noOfMMYTables++;
	}

	return noOfMMYTables;

}

//int32 TotalNoOfItemsPerMake(const CMMYTable* MMYTablePtr, const int32 selectedTableIndex)
//{
//	int32 count = 0;
//	int32 index = 0;
//	Vec_CMakeModel::const_iterator itr = MMYTablePtr->vec_CMakeModelPtr->begin();
//	Vec_CModelModel::const_iterator itr1;
//	for(; itr != MMYTablePtr->vec_CMakeModelPtr->end(); itr++,index++)
//	{
//		if(index == selectedTableIndex)
//		{
//			itr1 = (*itr)->vec_CModelModelPtr->begin();
//			for(;itr1 != (*itr)->vec_CModelModelPtr->end(); itr1++)
//			{
//				int32 size = static_cast<int32>((*itr1)->vec_ItemYearPtr->size());
//				count = count + size;
//			}
//			return count;
//		}
//	}
//	return count;
//}

bool16 InspectChars(InterfacePtr<ITextModel>& textModel,TextIndex startIndex, TextIndex endIndex, TextIndex& tableStartIndex, TextIndex& tableEndIndex)
{
	bool16 status = kFalse;
	do {
		if (!textModel) {
			break;
		}

		bool16 tableFound = kFalse;
		TextIterator begin(textModel, startIndex);
		TextIterator end(textModel, endIndex);
		
		for (TextIterator iter = begin; iter != end; iter++) {
			const UTF32TextChar characterCode = *iter;
			PMString character = GetCharacter(characterCode);
			if((character.IsEqual("<Table>") && tableFound == kFalse) || character.IsEqual("<TableContinued>"))
			{
				if(character.IsEqual("<Table>"))
				{
					tableStartIndex = iter.Position();	
					tableFound = kTrue;	
					status = kTrue;
					tableEndIndex = 0;
				}
				else
				{
					tableEndIndex++;
					
				}
			}
			else 
			{
				if(tableFound == kTrue)
					break;
			}
		}	
	} while(false);

	return status;
	
}

PMString GetCharacter(UTF32TextChar character)
{
	PMString result;

	if (character == kTextChar_CR)
	{
		result.Append("<CR>");
	}
	else if (character == kTextChar_SoftCR)
	{
		result.Append("<SoftCR>");
	}
	else if (character == kTextChar_Table)
	{
		result.Append("<Table>");
	}
	else if (character == kTextChar_TableContinued)
	{
		result.Append("<TableContinued>");
	}
	else if (character == kTextChar_ObjectReplacementCharacter)
	{
		result.Append("<Object>");
	}
	else
	{
		result.AppendW(character);
	}
	return result;
}


bool16 deleteTableAndContents(TagStruct& tagInfo, int32 noOfTablesToBeDeleted)
{

	XMLReference printsourceXMLElementRef = tagInfo.tagPtr->GetParent();
	InterfacePtr<IIDXMLElement>printsourceXMLElementPtr(printsourceXMLElementRef.Instantiate());
	int32 printsourceChildCount = printsourceXMLElementPtr->GetChildCount();
	for(int32 tagIndex = 0;tagIndex < printsourceChildCount ;tagIndex++)
	{
		XMLReference tagXMLElementRef = printsourceXMLElementPtr->GetNthChild(printsourceChildCount - tagIndex - 1 );
			
		InterfacePtr<IIDXMLElement>TagXMLElementPtr(tagXMLElementRef.Instantiate());
		
		PMString tagString = TagXMLElementPtr->GetTagString();
		if(tagString.IsEqual("PSTable0") && noOfTablesToBeDeleted > 0)
		{
			XMLReference elementXMLref = TagXMLElementPtr->GetXMLReference();
			ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElementAndContent(elementXMLref, kFalse);
			noOfTablesToBeDeleted--;	
			if(noOfTablesToBeDeleted == 0)
				return kTrue;
		}

	}

	return kFalse;
	
}

bool16 addTableToFrame(InterfacePtr<ITextModel>& textModel, int32 noOfTablesToBeAdded)
{
  
	UIDRef txtModelUIDRef = ::GetUIDRef(textModel);

	int32 len = textModel->GetPrimaryStoryThreadSpan ();
	//CA_NUM("textModel->GetPrimaryStoryThreadSpan  = " , len);
	
	TextIndex threadStart;
	int32 threadSpan;
	TextIndex tableStartPosition = 0;
	TextIndex tableEndPosition = 0;
	TextIndex position = 0;


	InterfacePtr<ITextStoryThread> storyThread(textModel->QueryStoryThread(position, &threadStart, &threadSpan));
	if(storyThread == NULL)
	{
		//CA("storyThread == NULL");
		return kFalse; 
	}

	//CA_NUM("threadStart = " , threadStart);
	//CA_NUM("threadSpan = " , threadSpan);
	

	bool16 findTable = InspectChars(textModel,threadStart, threadSpan - 1,tableStartPosition,tableEndPosition);

	
	int32 startPos = tableStartPosition;
	int32 endPos = startPos+tableEndPosition+1;

	//CA_NUM("startPos = " , startPos);
	//CA_NUM("endPos = " , endPos);

	int32 length = endPos - startPos;
	int32 startPasteIndex = len - 1;
	TextIndex destEnd = startPasteIndex + 0; 

	int32 lineStartCharIndex = startPos;
	int32 lineEndCharIndex = endPos;


	//copyPaste MMY tables for no of Make Times
	for(int32 tableIndex=0;tableIndex<noOfTablesToBeAdded;tableIndex++)
	{
		do
		{
			// Create kCopyStoryRangeCmdBoss.
			InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
			if (copyStoryRangeCmd == nil) {CA("copyStoryRangeCmd == nil");
				break;
			}

			// Refer the command to the source story and range to be copied.
			InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
			if (sourceUIDData == nil) {CA("sourceUIDData == nil");
				break;
			}
			
			sourceUIDData->Set(txtModelUIDRef);
			InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
			if (sourceRangeData == nil) {CA("sourceRangeData == nil");
				break;
			}
			
			sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);

			// Refer the command to the destination story and the range to be replaced.
			UIDList itemList(txtModelUIDRef);
			copyStoryRangeCmd->SetItemList(itemList);
			
			InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

			destRangeData->Set(startPasteIndex, destEnd );

			// Process CopyStoryRangeCmd
			ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

		}while(kFalse);
	}
	return kTrue;

}


bool16 NeedToRefreshMMYCustomTable(const UIDRef& boxUIDRef,TagStruct& tagInfo)
{
	//CA("NeedToRefreshMMYCustomTable");
	bool16 anyChangeFoundInMMYTable = kFalse;
	//do
	//{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	UID textFrameUID = kInvalidUID;
	//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	//	if (graphicFrameDataOne) 
	//	{
	//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//	}

	//	if (textFrameUID == kInvalidUID)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}
	//	
	//	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
	//	if (graphicFrameHierarchy == nil) 
	//	{
	//		//CA(graphicFrameHierarchy);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!graphicFrameHierarchy");
	//		break;
	//	}
	//					
	//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//	if (!multiColumnItemHierarchy) {
	//		//CA(multiColumnItemHierarchy);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemHierarchy");
	//		break;
	//	}

	//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//	if (!multiColumnItemTextFrame) {
	//		//CA(!multiColumnItemTextFrame);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!multiColumnItemTextFrame");
	//		//CA("Its Not MultiColumn");
	//		break;
	//	}
	//	
	//	InterfacePtr<IHierarchy>
	//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	//	if (!frameItemHierarchy) {
	//		//CA(“!frameItemHierarchy”);
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!frameItemHierarchy");
	//		break;
	//	}

	//	InterfacePtr<ITextFrameColumn>
	//	textFrame(frameItemHierarchy, UseDefaultIID());
	//	if (!textFrame) {
	//		//CA("!!!ITextFrameColumn");
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::!textFrame");
	//		break;
	//	}
	//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	//	if (textModel == NULL)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}

	//	UIDRef txtModelUIDRef = ::GetUIDRef(textModel);



	////find out total MMY items 
	//	vector<int32> FinalItemIds;
	//	if(tagInfo.whichTab == 3)
	//	{
	//		VectorScreenTableInfoPtr tableInfo = NULL;
	//		
	//		// for publication
	//		tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.parentId,  tagInfo.languageID,  kTrue);
	//	
	//		if(!tableInfo)
	//		{
	//			break;
	//		}
	//		if(tableInfo->size()==0)
	//		{ 
	//			makeTagInsideCellImpotent(tagInfo.tagPtr);
	//			break;
	//		}
	//				
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;

	//		bool16 typeidFound=kFalse;
	//		vector<int32> vec_items;
	//		FinalItemIds.clear();

	//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		{				
	//			oTableValue = *it;				
	//			vec_items = oTableValue.getItemIds();
	//		
	//			if(tagInfo.field1 == -1)	//---------
	//			{
	//				if(FinalItemIds.size() == 0)
	//				{	//CA("FinalItemIds.size() == 0");
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
	//					{	
	//						bool16 Flag = kFalse;
	//						for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
	//						{
	//							if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
	//					}

	//				}
	//			}
	//			else
	//			{
	//				 
	//				if(tagInfo.field1  !=  oTableValue.getTableTypeID())
	//					continue;

	//				if(FinalItemIds.size() == 0)
	//				{	//CA("FinalItemIds.size() == 0");
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 oTableValueItemIndex=0; oTableValueItemIndex<vec_items.size(); oTableValueItemIndex++)
	//					{	
	//						bool16 Flag = kFalse;
	//						for(int32 finalItemIDItemIndex=0; finalItemIDItemIndex<FinalItemIds.size(); finalItemIDItemIndex++)
	//						{
	//							if(vec_items[oTableValueItemIndex] == FinalItemIds[finalItemIDItemIndex])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[oTableValueItemIndex]);
	//					}

	//				}

	//			}
	//		}

	//		if(tableInfo)
	//		{
	//			tableInfo->clear();
	//			delete tableInfo;
	//		}
	//	}
	//	else if(tagInfo.whichTab == 4)
	//	{
	//		VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tagInfo.parentId, tagInfo.sectionID  ,tagInfo.languageID );
	//		if(!tableInfo)
	//		{
	//			//CA("tableinfo is NULL");
	//			makeTagInsideCellImpotent(tagInfo.tagPtr); 
	//			break;
	//		}
	//		
	//		if(tableInfo->size()==0)
	//		{ //CA("tableinfo size==0");
	//			makeTagInsideCellImpotent(tagInfo.tagPtr); 
	//			break;
	//		}
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;

	//		bool16 typeidFound=kFalse;
	//		vector<int32> vec_items;
	//		
	//		FinalItemIds.clear();

	//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		{				
	//			oTableValue = *it;				
	//			vec_items = oTableValue.getItemIds();
	//		
	//			if(tagInfo.field1 == -1)	//--------
	//			{
	//				if(FinalItemIds.size() == 0)
	//				{
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 i=0; i<vec_items.size(); i++)
	//					{	bool16 Flag = kFalse;
	//						for(int32 j=0; j<FinalItemIds.size(); j++)
	//						{
	//							if(vec_items[i] == FinalItemIds[j])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[i]);
	//					}
	//				}
	//			}
	//			else
	//			{
	//				if(tagInfo.field1 != oTableValue.getTableTypeID())
	//					continue;

	//				if(FinalItemIds.size() == 0)
	//				{
	//					FinalItemIds = vec_items;
	//				}
	//				else
	//				{
	//					for(int32 i=0; i<vec_items.size(); i++)
	//					{	bool16 Flag = kFalse;
	//						for(int32 j=0; j<FinalItemIds.size(); j++)
	//						{
	//							if(vec_items[i] == FinalItemIds[j])
	//							{
	//								Flag = kTrue;
	//								break;
	//							}				
	//						}
	//						if(!Flag)
	//							FinalItemIds.push_back(vec_items[i]);
	//					}
	//				}
	//			}
	//			
	//		}		
	//	
	//		if(tableInfo)
	//			delete tableInfo;
	//	}

	//	PMString FinaiItemIdsStr("FinalItemIds size = ");
	//	FinaiItemIdsStr.AppendNumber(static_cast<int32>(FinalItemIds.size()));
	//	//CA(FinaiItemIdsStr);

	//	
	////getMMYTableByItemIds
	//	CMMYTable* MMYTablePtr = ptrIAppFramework->getMMYTableByItemIds(&FinalItemIds);

	//	PMString noOfMMYTables = "";
	//	noOfMMYTables.AppendNumber(static_cast<int32>(MMYTablePtr->vec_CMakeModelPtr->size()));
	//	//CA("MMYTablePtr->vec_CMakeModelPtr->size() = " + noOfMMYTables);
	//
	////check the no of table tags in frame and no of make tables in MMYTablePtr
	//	//if both are equal no need to add or delete tables
	//	//else if(no of table tags in frame < make tables in MMYTablePtr)
	//		//add tables to document
	//	//else if(no of table tags in frame > make tables in MMYTablePtr)
	//		//delete tables from document


	//	int32 noOfMMYTablesPresntInFrame = findOutHowManyMMYTablesInTextFrame(tagInfo);
	//	int32 noOfMakeFound = static_cast<int32>(MMYTablePtr->vec_CMakeModelPtr->size());

	//	PMString noOfMMYTablesPresntInFrameStr = "";
	//	noOfMMYTablesPresntInFrameStr.AppendNumber(noOfMMYTablesPresntInFrame);
	//	//CA("noOfMMYTablesPresntInFrameStr = " + noOfMMYTablesPresntInFrameStr);

	//	PMString noOfMakeFoundStr = "";
	//	noOfMakeFoundStr.AppendNumber(noOfMakeFound);
	//	//CA("noOfMakeFoundStr = " + noOfMakeFoundStr);

	//	if(noOfMMYTablesPresntInFrame < noOfMakeFound)
	//	{
	//		//CA("noOfMMYTablesPresntInFrame < noOfMakeFound");
	//		anyChangeFoundInMMYTable = kTrue;
	//		delete MMYTablePtr;
	//		return anyChangeFoundInMMYTable;			
	//	}
	//	else if(noOfMMYTablesPresntInFrame > noOfMakeFound)
	//	{
	//		//CA("noOfMMYTablesPresntInFrame > noOfMakeFound");
	//		anyChangeFoundInMMYTable = kTrue;
	//		delete MMYTablePtr;
	//		return anyChangeFoundInMMYTable;
	//	}
	//
	////for each table
	//	//find out current table tag's make_Id whether present in MMYTablePtr 
	//		//if it present checks the no of model in tag as well as in MMYTablePtr
	//			//there are 3 cases whether both are same...
	//			//so no need to add or delete rows from document table
	//			//Otherwise we need to add or delete rows
	//	


	//	int32 CurrentSubSectionID = tagInfo.sectionID;
	//	int32 PARENTID = tagInfo.parentId;
	//	
	//	int32 tableTypeId = -1;		
	//	int32 sort_by_attributeId = -1;
	//	bool16 HeaderRowPresent = kFalse;
	//	bool16 tableHeaderPresent = kFalse;
	//	int32 languageId = -1;

	//	bool16 sortByAttributeIdFound = kFalse;
	//	for(int32 tagIndex = 0;tagIndex < tagInfo.tagPtr->GetChildCount();tagIndex++)
	//	{//start for tagIndex = 0
	//		XMLReference cellXMLElementRef =   tagInfo.tagPtr->GetNthChild(tagIndex);
	//		//This is a tag attached to the cell.
	//		//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//		InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//		//Get the text tag attached to the text inside the cell.
	//		//We are providing only one texttag inside cell.
	//		if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//		{
	//			continue;
	//		}
	//		for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//		{
	//			XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//			//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
	//			InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
	//			//The cell may be blank. i.e doesn't have any text tag inside it.
	//			if(cellTextTagPtr == NULL)
	//			{
	//				continue;
	//			}
	//			
	//			if((cellTextTagPtr->GetAttributeValue(WideString("header")) == WideString("1"))
	//				 && HeaderRowPresent == kFalse)
	//			{
	//				//CA("header present");
	//				tableHeaderPresent = kTrue;
	//			}

	//			PMString fld_1 = cellTextTagPtr->GetAttributeValue(WideString("field1"));
	//			tableTypeId = fld_1.GetAsNumber();
	//			
	//			if(cellTextTagPtr->GetAttributeValue(WideString("childTag")) == WideString("1") && cellTextTagPtr->GetAttributeValue(WideString("tableFlag"))!= WideString("1")  && sort_by_attributeId == -1)
	//			{		
	//				if(cellTextTagPtr->GetAttributeValue(WideString("field2")) == WideString("1") )
	//				{							
	//					PMString Sort_By_AttributeId_Str = cellTextTagPtr->GetAttributeValue(WideString("ID"));
	//					sort_by_attributeId = Sort_By_AttributeId_Str.GetAsNumber();
	//					sortByAttributeIdFound = kTrue;
	//					break;
	//				}
	//			}			
	//		}

	//		if(sortByAttributeIdFound)
	//			break;
	//	}//end for tagIndex = 0 


	//	
	//	InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
	//	if(tableList==NULL)
	//	{
	//		break;//breaks the do{}while(kFalse)
	//	}
	//	int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

	//	if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable::totalNumberOfTablesInsideTextFrame < 0");																						
	//		break;//breaks the do{}while(kFalse)
	//	}

	//	PMString totalNumberOfTablesInsideTextFrameStr = "";
	//	totalNumberOfTablesInsideTextFrameStr.AppendNumber(totalNumberOfTablesInsideTextFrame);
	//	//CA("totalNumberOfTablesInsideTextFrame = " + totalNumberOfTablesInsideTextFrameStr);

	//	bool16 isSameMake = kFalse;
	//	bool16 isSameModel = kFalse;

	//	int32 makeIndex = -1;
	//	int32 modelIndex = -1;

	//	int32 makeRepeatCnt = -1;
	//	int32 modelRepeatCnt = -1;

	//	int32 currentMakeModelCnt = -1;
	//	int32 currentModelItemCnt = -1;
	//	int32 currentModelItemIndex = -1;

	//	int32 colIndexPerRow = -1;

	//	vector<int32> itemIds;

	//	Vec_CMakeModel::const_iterator itr;
	//	Vec_CModelModel::const_iterator itr1;

	//	CMakeModel *makeModelPtr = NULL;
	//	CModelModel *modelModelPtr = NULL;

	//	multimap<double, int32>sortItems;
	//	multimap<double, int32>::iterator mapItr;
	//	int32 ITEMID = -1;
	//	int32 itemIDIndex = -1;


	//	for(int tableIndex = 0; tableIndex < totalNumberOfTablesInsideTextFrame; tableIndex++)
	//	{//for..tableIndex

	//		bool16 isMakeIDAttachedToTableTag = kFalse;
	//		int32 getTableModelAtIndex = tableIndex;
	//		if(tableIndex == 0)
	//		{
	//			getTableModelAtIndex = 0;
	//		}
	//		else
	//		{
	//			getTableModelAtIndex = totalNumberOfTablesInsideTextFrame - tableIndex;
	//			
	//		}

	//		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(getTableModelAtIndex));
	//		if(tableModel == NULL)
	//		{
	//			//CA("continue");
	//			continue;//continues the for..tableIndex
	//		}
	//
	//		//bool16 HeaderRowPresent = kFalse;

	//		RowRange rowRange(0,0);
	//		rowRange = tableModel->GetHeaderRows();
	//		int32 headerStart = rowRange.start;
	//		int32 headerCount = rowRange.count;
	//		
	//		PMString HeaderStart = "";
	//		HeaderStart.AppendNumber(headerStart);
	//		PMString HeaderCount = "";
	//		HeaderCount.AppendNumber(headerCount);
	//		//CA("HeaderStart = " + HeaderStart + ", HeaderCount =  " + HeaderCount);

	//		if(headerCount != 0)
	//		{
	//			//CA("HeaderPresent");
	//			HeaderRowPresent = kTrue;
	//		}
	//		
	//		
	//		UIDRef tableRef(::GetUIDRef(tableModel)); 
	//		//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
	//		//IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;

	//		languageId = tagInfo.languageID; 

	//		//XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();			
	//		//UIDRef ContentRef = contentRef.GetUIDRef();
	//		//if( ContentRef != tableRef)
	//		//{
	//		//	CA("ContentRef != tableRef");
	//		//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
	//		//	continue; //continues the for..tableIndex
	//		//}

	//		int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
	//				
	//		int32 totalNumberOfRowsBeforeRowInsertion = 0;
	//		if(HeaderRowPresent)
	//			totalNumberOfRowsBeforeRowInsertion =  tableModel->GetBodyRows().count;
	//		else
	//			totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;

	//		PMString r4("totalNumberOfRowsBeforeRowInsertion 04: ");
	//		r4.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//		//CA(r4);


	//		PMString numberOfRows = "";
	//		numberOfRows.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//		//CA("totalNumberOfRowsBeforeRowInsertion  = " + numberOfRows);
 //           		
	//		//GridArea gridAreaForFirstRow(0,0,1,totalNumberOfColumns);
	//		GridArea gridAreaForEntireTable,headerArea;;
	//		if(HeaderRowPresent)
	//		{	//CA("fill gridAreaForEntireTable");			 
	//			gridAreaForEntireTable.topRow = headerCount;
	//			gridAreaForEntireTable.leftCol = 0;
	//			gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion+headerCount;
 //               gridAreaForEntireTable.rightCol = totalNumberOfColumns;
	//			//gridAreaForEntireTable(headerCount,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
	//		
	//			headerArea.topRow = 0;
	//			headerArea.leftCol = 0;
	//			headerArea.bottomRow = headerCount;
	//			headerArea.rightCol = totalNumberOfColumns;			
	//		}
	//		else
	//		{
	//			gridAreaForEntireTable.topRow = 0;
	//			gridAreaForEntireTable.leftCol = 0;
	//			gridAreaForEntireTable.bottomRow = totalNumberOfRowsBeforeRowInsertion;
 //               gridAreaForEntireTable.rightCol = totalNumberOfColumns;
	//			//gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
	//		}

	//		GridArea gridAreaForLastRow;
	//		if(HeaderRowPresent)
	//		{
	//			gridAreaForLastRow.topRow = totalNumberOfRowsBeforeRowInsertion;
	//			gridAreaForLastRow.leftCol = 0;
	//			gridAreaForLastRow.bottomRow = totalNumberOfRowsBeforeRowInsertion +1;
 //               gridAreaForLastRow.rightCol = totalNumberOfColumns;
	//		}
	//		else
	//		{
	//			gridAreaForLastRow.topRow = totalNumberOfRowsBeforeRowInsertion-1;
	//			gridAreaForLastRow.leftCol = 0;
	//			gridAreaForLastRow.bottomRow = totalNumberOfRowsBeforeRowInsertion;
 //               gridAreaForLastRow.rightCol = totalNumberOfColumns;
	//		}


	//		InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
	//		if(tableGeometry == NULL)
	//		{
	//			//CA("tableGeometry == NULL");
	//			break;//breaks the for..tableIndex
	//		}
	//		PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 			
	//		

	//	

	//		GridArea bodyArea = tableModel->GetBodyArea();
	//		int32 bodyCellCount = 0;
	//		ITableModel::const_iterator iterTable1(tableModel->begin(bodyArea));
	//		ITableModel::const_iterator endTable1(tableModel->end(bodyArea));
	//		while (iterTable1 != endTable1)
	//		{
	//			bodyCellCount++;
	//			++iterTable1;
	//		}
	//		
	//		int32 tableRows = tableModel->GetBodyRows().count;
	//		int32 CellCountPerRow = bodyCellCount/tableRows;

	//	
	//		{
	//				
	//			
	//			int32 rowPatternRepeatCount = TotalNoOfItemsPerMake(MMYTablePtr, tableIndex);

	//			PMString rowPatternRepeatCountStr = "";
	//			rowPatternRepeatCountStr.AppendNumber(rowPatternRepeatCount);
	//			//CA("rowPatternRepeatCountStr = " + rowPatternRepeatCountStr);

	//			PMString totalNumberOfRowsBeforeRowInsertionStr = "";
	//			totalNumberOfRowsBeforeRowInsertionStr.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//			//CA("totalNumberOfRowsBeforeRowInsertionStr = " + totalNumberOfRowsBeforeRowInsertionStr);

	//			if(totalNumberOfRowsBeforeRowInsertion < rowPatternRepeatCount)
	//			{
	//				//CA("totalNumberOfRowsBeforeRowInsertion < rowPatternRepeatCount");
	//				anyChangeFoundInMMYTable = kTrue;
	//				delete MMYTablePtr;
	//				return anyChangeFoundInMMYTable;
	//			}
	//			else if(totalNumberOfRowsBeforeRowInsertion > rowPatternRepeatCount)
	//			{
	//				//CA("totalNumberOfRowsBeforeRowInsertion > rowPatternRepeatCount");
	//				anyChangeFoundInMMYTable = kTrue;
	//				delete MMYTablePtr;
	//				return anyChangeFoundInMMYTable;
	//			}
	//								
	//				if(HeaderRowPresent)
	//				{
	//					//CA("HeaderRowPresent 1");
	//				//	for(int32 indexOfRowInTheHeaderRowPattern = 0 ; indexOfRowInTheHeaderRowPattern < headerCount ; indexOfRowInTheHeaderRowPattern++)
	//				//	{//for..iterate through each row of the headerRowPattern
	//				//		for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
	//				//		{//for..iterate through each column
	//							
	//					//GridArea gridArea = tableModel->GetCellArea (GridAddress(indexOfRowInTheHeaderRowPattern,columnIndex));
	//					GridArea headerArea;
	//					//if(selectedTableIndex == 0)
	//						headerArea = tableModel->GetHeaderArea();
	//					//else
	//					//{
	//					//	//CA("selectedTableIndex != 0");
	//					//	headerArea.leftCol = gridAreaIncludingHeader.leftCol;
	//					//	headerArea.bottomRow = gridAreaIncludingHeader.bottomRow;
	//					//	headerArea.rightCol = gridAreaIncludingHeader.rightCol;
	//					//	headerArea.topRow = bodyAreaBeforeAddingRow.bottomRow;
	//					//}
	//					ITableModel::const_iterator iterTable(tableModel->begin(headerArea));
	//					ITableModel::const_iterator endTable(tableModel->end(headerArea));
	//					while (iterTable != endTable)
	//					{
	//						//CA("Table iterator");
	//						GridAddress gridAddress = (*iterTable);         
	//						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
	//						if(cellContent == nil) 
	//						{
	//							//CA("cellContent == nil");
	//							break;
	//						}
	//						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
	//						if(!cellXMLReferenceData) {
	//							//CA("!cellXMLReferenceData");
	//							break;
	//						}
	//						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
	//
	//						++iterTable;
	//					//		int32 tagIndex = indexOfRowInTheHeaderRowPattern * totalNumberOfColumns + columnIndex;
	//							
	//					//		XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(tagIndex);
	//							//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//							if(!isMakeIDAttachedToTableTag)
	//							{
	//								XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
	//								InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());

	//								PMString attrVal("");
	//								attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
	//								tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
	//								isMakeIDAttachedToTableTag = kTrue;
	//							}
	//							
	//							//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//							//Also note that, cell may be blank i.e doesn't contain any textTag.
	//							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//							{
	//								continue;
	//							}
	//							for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//							{
	//								XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//								//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//								InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//								//Check if the cell is blank.
	//								if(cellTextXMLElementPtr == NULL)
	//								{
	//								continue;
	//								}

	//								//Get all the elementID and languageID from the cellTextXMLElement
	//								//Note ITagReader plugin methods are not used.
	//								PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//								PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//								//check whether the tag specifies ProductCopyAttributes.
	//								PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//								PMString imgFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
	//								PMString strParentTypeID =   cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));///11/05/07
	//								PMString strTypeId =   cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//								PMString strDataType =   cellTextXMLElementPtr->GetAttributeValue(WideString("dataType"));

	//		
	//								int32 elementID = strElementID.GetAsNumber();
	//								int32 languageID = strLanguageID.GetAsNumber();
	//														
	//								int32 tagStartPos = -1;
	//								int32 tagEndPos = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//								tagStartPos = tagStartPos + 1;
	//								tagEndPos = tagEndPos -1;

	//								PMString dispName("");
	//								////if normal tag present in header row...
	//								////then we are spraying the value related to the first item.
	//								int32 itemID = FinalItemIds[0];

	//								//if(strDataType == "4" && isCallFromTS)	//-------------
	//								//{
	//								//	//CA("Going to spray table Name");
	//								//	CItemTableValue oTableValue;
	//								//	VectorScreenTableInfoValue::iterator it;
	//								//	for(it = tableInfo->begin() ; it != tableInfo->end() ; it++)
	//								//	{				
	//								//		oTableValue = *it;		
	//								//		if(oTableValue.getTableID() ==  tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0))
	//								//		{
	//								//			dispName = oTableValue.getName();
	//								//			attributeVal.Clear();
	//								//			attributeVal.AppendNumber(oTableValue.getTableID());
	//								//			cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(attributeVal));
	//								//			
	//								//			attributeVal.Clear();
	//								//			attributeVal.AppendNumber(oTableValue.getTableTypeID());
	//								//			cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attributeVal));

	//								//			//CA(dispName);
	//								//			break;
	//								//		}
	//								//	}

	//								//}
	//								//else 
	//								if(strTypeId == "-1")
	//								{
	//									//CA("strTypeId == -1");
	//									int32 parentTypeID = strParentTypeID.GetAsNumber();

	//									//CA("#################normal tag present in header row###############");
	//									if(tableFlag == "1")
	//									{
	//										//CA("tableFlag == 1");
	//										//added on 11July...serial blast day
	//										//The following code will spray the table preset inside the table cell.
	//										//for copy ID = -104
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")// || index == "4")
	//										{//Product ItemTable
	//											PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//											TagStruct tagInfo;												
	//											tagInfo.whichTab = index.GetAsNumber();
	//											/*if(index == "3")
	//												tagInfo.parentId = pNode.getPubId();*/
	//											/*else if(index == "4")
	//												tagInfo.parentId = pNode.getPBObjectID();*/

	//											tagInfo.isTablePresent = tableFlag.GetAsNumber();
	//											tagInfo.typeId = typeID.GetAsNumber();
	//											//tagInfo.sectionID = sectionid;
	//																						
	//											GetItemTableInTabbedTextForm(tagInfo,dispName);
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));
	//											//CA(dispName);
	//										}
	//										//end added on 11July...serial blast day
	//										else if(index == "4")
	//										{//This is the Section level ItemTableTag and we have selected the product
	//										//for spraying
	//											makeTheTagImpotent(cellTextXMLElementPtr);										
	//										}
	//										
	//									}

	//										
	//									else if(imgFlag == "1")
	//									{							
	//										XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//										UIDRef ref = cntentREF.GetUIDRef();
	//										if(ref == UIDRef::gNull)
	//										{
	//											//CA("ref == UIDRef::gNull");
	//											continue;
	//										}

	//										InterfacePtr<ITagReader> itagReader
	//										((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//										if(!itagReader)
	//											continue ;
	//										
	//										TagList tList=itagReader->getTagsFromBox(ref);
	//										TagStruct tagInfoo;
	//										int numTags=static_cast<int>(tList.size());

	//										if(numTags<0)
	//										{
	//											continue;
	//										}

	//										tagInfoo=tList[0];

	//										//tagInfoo.parentId = pNode.getPubId();
	//										tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//										//tagInfoo.sectionID=CurrentSubSectionID;

	//										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//										if(!iDataSprayer)
	//										{	
	//											//CA("!iDtaSprayer");
	//											continue;
	//										}

	//										//iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//										
	//										PMString attrVal;
	//										attrVal.AppendNumber(itemID);
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//											
	//										for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//										{
	//											tList[tagIndex].tagPtr->Release();
	//										}
	//										continue;
	//									}
	//									else if(tableFlag == "0")
	//									{
	//										//CA("tableFlag == 0 AAA");
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index =="4")
	//										{//Item
	//											//CA("index == 4 ");
	//											//added on 22Sept..EventPrice addition.												
	//											/*if(strElementID == "-701")
	//											{
	//												PMString  attributeIDfromNotes = "-1";
	//												bool8 isNotesValid = getAttributeIDFromNotes(kTrue,sectionid,pNode.getPubId(),itemID,attributeIDfromNotes);
	//												cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//												elementID = attributeIDfromNotes.GetAsNumber();
	//											}*/
	//											if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) != WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) != WideString("-101"))
	//												itemID = -1;/*pNode.getPubId()*/;

	//											//if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
	//											//{
	//											//	TagStruct tagInfo;
	//											//	tagInfo.elementId = elementID;
	//											//	tagInfo.tagPtr = cellTextXMLElementPtr;
	//											//	tagInfo.typeId = itemID;
	//											//	tagInfo.parentTypeID = parentTypeID;//added by Tushar on 27/12/06
	//											//	handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
	//											//}
	//											////ended on 22Sept..EventPrice addition.
	//											//else if(elementID == -803)  // For 'Letter Key'
	//											//{
	//											//	dispName = "a" ;			// For First Item										
	//											//}
	//											//else if((isComponentAttributePresent == 1) && (elementID == -805) )  // For 'Quantity'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][0]);
	//											//}
	//											//else if((isComponentAttributePresent == 1) && (elementID == -806) )  // For 'Availability'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][1]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][0]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][1]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][2]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][3]);			
	//											//}
	//											//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
	//											//{
	//											//	dispName = (Kit_vec_tablerows[0][4]);			
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -812) )  // For 'Quantity'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][0]);
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -813) )  // For 'Required'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][1]);
	//											//}
	//											//else if((isComponentAttributePresent == 3) && (elementID == -814) )  // For 'Comments'
	//											//{
	//											//	dispName = (Accessory_vec_tablerows[0][2]);
	//											//}
	//											//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[0][4]) == "N"))
	//											//{
	//											//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
	//											//}
	//											//else if(elementID == -827)  // For 'Number Key'
	//											//{
	//											//	dispName = "1" ;
	//											//}
	//											if(elementID == -401)
	//											{
	//												dispName = MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_Name();

	//												PMString attrVal("");
	//												attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(/*selectedTableIndex*/tableIndex)->getMake_id());
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
	//											}
	//											else
	//											{
	//												//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);										
	//												dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);  // getting single item attributye data at a time.
	//											}
	//											
	//											PMString attrVal;
	//											attrVal.AppendNumber(itemID);
	//											if( cellTextXMLElementPtr->GetAttributeValue(WideString("childTag")) == WideString("1"))//if( cellTextXMLElementPtr->GetAttributeValue(WideString("rowno")) == WideString("-101"))
	//											{
	//												//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	// 												cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//											}
	//											else
	//											{
	//												/*PMString attrVal("");
	//												attrVal.AppendNumber(pNode.getPubId());
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));*/
	//											}
	//										}
	//										else if( index == "3")
	//										{//Product
	//											
	//											dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(/*pNode.getPubId()*/-1,elementID,languageID, tagInfo.sectionID, kTrue);
	//											/*PMString attrVal;
	//											attrVal.AppendNumber(pNode.getTypeId());													
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//											attrVal.Clear();

	//											attrVal.AppendNumber(pNode.getPubId());
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));*/
	//										}
	//									}								
	//								}
	//								//else if(imgFlag == "1")
	//								//{							
	//								//	XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//								//	UIDRef ref = cntentREF.GetUIDRef();
	//								//	if(ref == UIDRef::gNull)
	//								//	{
	//								//		//CA("ref == UIDRef::gNull");
	//								//		continue;
	//								//	}

	//								//	InterfacePtr<ITagReader> itagReader
	//								//	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//								//	if(!itagReader)
	//								//		continue ;
	//								//	
	//								//	TagList tList=itagReader->getTagsFromBox(ref);
	//								//	TagStruct tagInfoo;
	//								//	int numTags=static_cast<int>(tList.size());

	//								//	if(numTags<0)
	//								//	{
	//								//		continue;
	//								//	}

	//								//	tagInfoo=tList[0];

	//								//	tagInfoo.parentId = pNode.getPubId();
	//								//	tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//								//	tagInfoo.sectionID=CurrentSubSectionID;

	//								//	IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//								//	if(!iDataSprayer)
	//								//	{	
	//								//		CA("!iDtaSprayer");
	//								//		continue;
	//								//	}

	//								//	iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//								//	
	//								//	PMString attrVal;
	//								//	attrVal.AppendNumber(itemID);
	//								//	cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								//	
	//								//	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//								//	{
	//								//		tList[tagIndex].tagPtr->Release();
	//								//	}
	//								//	
	//								//	continue;
	//								//}
	//								else
	//								{							
	//									if(tableFlag == "1")
	//									{					
	//										//CA("inside cell table is present");
	//									//added on 11July...the serial blast day
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")
	//										{
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
	//											do
	//											{
	//												PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//			//CA(strTypeID);
	//												int32 typeId = strTypeID.GetAsNumber();
	//												VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
	//												if(typeValObj==NULL)
	//												{
	//													ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable::!typeValObj");																						
	//													break;
	//												}

	//												VectorTypeInfoValue::iterator it1;

	//												bool16 typeIDFound = kFalse;
	//												for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
	//												{	
	//													if(typeId == it1->getTypeId())
	//														{
	//															typeIDFound = kTrue;
	//															break;
	//														}			
	//												}
	//												if(typeIDFound)
	//													dispName = it1->getName();

	//												if(typeValObj)
	//													delete typeValObj;
	//											}while(kFalse);
	//													
	//										}
	//										//This is a special case.The tag is of product but the Item is
	//										//selected.So make this tag unproductive for refresh.(Impotent)
	//										else if(index == "4")
	//										{
	//											makeTheTagImpotent(cellTextXMLElementPtr);
	//											
	//										}
	//												
	//												
	//									}
	//									else
	//									{
	//							
	//										PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//										if(index== "3")
	//										{//For product
	//											CElementModel  cElementModelObj;
	//											bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
	//											if(result)
	//												dispName = cElementModelObj.getDisplayName();
	//											PMString attrVal;
	//											attrVal.AppendNumber(/*pNode.getTypeId()*/-1);
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//																				
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("tableFlag"),WideString("-13"));
	//										}
	//										else if(index == "4")
	//										{
	//											//following code added by Tushar on 27/12/06
	//											if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/ )
	//											{
	//												/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
	//												int32 parentTypeID = strParentTypeID.GetAsNumber();
	//												
	//												VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
	//												if(TypeInfoVectorPtr != NULL)
	//												{
	//													VectorTypeInfoValue::iterator it3;
	//													int32 Type_id = -1;
	//													PMString temp = "";
	//													PMString name = "";
	//													for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
	//													{
	//														Type_id = it3->getTypeId();
	//														if(parentTypeID == Type_id)
	//														{
	//															temp = it3->getName();
	//															if(elementID == -701)
	//															{
	//																dispName = temp;
	//															}
	//															else if(elementID == -702)
	//															{
	//																dispName = temp + " Suffix";
	//															}
	//														}														
	//													}													
	//												}
	//												if(TypeInfoVectorPtr)
	//													delete TypeInfoVectorPtr;*/
	//											}
	//											else if(elementID == -703)
	//											{
	//												dispName = "$Off";
	//											}
	//											else if(elementID == -704)
	//											{
	//												dispName = "%Off";
	//											}
	//											else if((elementID == -805) )  // For 'Quantity'
	//											{
	//												//CA("Quantity Header");
	//												dispName = "Quantity";
	//											}
	//											else if((elementID == -806) )  // For 'Availability'
	//											{
	//												dispName = "Availability";			
	//											}
	//											else if(elementID == -807)
	//											{									
	//												dispName = "Cross-reference Type";
	//											}
	//											else if(elementID == -808)
	//											{									
	//												dispName = "Rating";
	//											}
	//											else if(elementID == -809)
	//											{									
	//												dispName = "Alternate";
	//											}
	//											else if(elementID == -810)
	//											{									
	//												dispName = "Comments";
	//											}
	//											else if(elementID == -811)
	//											{									
	//												dispName = "";
	//											}
	//											else if((elementID == -812) )  // For 'Quantity'
	//											{
	//												//CA("Quantity Header");
	//												dispName = "Quantity";
	//											}
	//											else if((elementID == -813) )  // For 'Required'
	//											{
	//												//CA("Required Header");
	//												dispName = "Required";
	//											}
	//											else if((elementID == -814) )  // For 'Comments'
	//											{
	//												//CA("Comments Header");
	//												dispName = "Comments";
	//											}

	//											else
	//												dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );

	//											//Modification of XMLTagAttributes.
	//											//modify the XMLTagAttribute "typeId" value to -2.
	//											//We have assumed that there is atleast one stencil with HeaderAttirbute
	//											//true .So assign for all the stencil in the header row , typeId == -2
	//											cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
	//											//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
	//										}									
	//									}
	//								}
	//								PMString textToInsert("");
	//								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//								if(!iConverter)
	//								{
	//									textToInsert=dispName;					
	//								}
	//								else
	//									textToInsert=iConverter->translateString(dispName);

	//								int32 tagStartPos1 = -1;
	//								int32 tagEndPos1 = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);
	//								tagStartPos1 = tagStartPos1 + 1;
	//								tagEndPos1 = tagEndPos1 -1;

	//								
	//								PMString entireStory("");
	//								bool16 result1 = kFalse;

	//							
	//								result1=GetTextstoryFromBox(textModel, tagStartPos1, tagEndPos1, entireStory);

	//								if(!entireStory.IsEqual(textToInsert))
	//								{
	//									PMString test("entireStory = ");
	//									test.Append(entireStory);
	//									test.Append(" , textToInsert = ");
	//									test.Append(textToInsert);
	//									//CA(test);
	//									//CA("!entireStory.IsEqual(textToInsert)");
	//									return kTrue;
	//								}
	//				//		}//for..iterate through each column
	//					}//for..iterate through each row of the headerRowPattern
	//				
	//					//nonHeaderRowPatternIndex = 1;
	//					//rowPatternRepeatCount++;

	//				}

	//				
	//				}
	//				if(tableHeaderPresent)
	//				{//if 2 tableHeaderPresent 
	//					//CA("tableHeaderPresent");
	//				//First rowPattern  now is of header element.
	//				//i.e rowPattern ==0 so we have to take the first (toatlNumberOfColumns - 1) tag.
	//				/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
	//				s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//				CA(s);*/
	//					for(int32 indexOfRowInTheRepeatedPattern =0;indexOfRowInTheRepeatedPattern<totalNumberOfRowsBeforeRowInsertion;indexOfRowInTheRepeatedPattern++)
	//					{//for..iterate through each row of the repeatedRowPattern
	//						/*PMString s("totalNumberOfRowsBeforeRowInsertion : ");
	//						s.AppendNumber(totalNumberOfRowsBeforeRowInsertion);
	//						CA(s);*/
	//						for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
	//						{//for..iterate through each column
	//							int32 tagIndex = indexOfRowInTheRepeatedPattern * totalNumberOfColumns + columnIndex;
	//							XMLReference cellXMLElementRef = tagInfo.tagPtr->GetNthChild(tagIndex);
	//							//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
	//							//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//							//Also note that, cell may be blank i.e doesn't contain any textTag.
	//							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//							{
	//								continue;
	//							}
	//							for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
	//							{
	//								XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//								//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//								InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//								//Check if the cell is blank.
	//								if(cellTextXMLElementPtr == NULL)
	//								{
	//									continue;
	//								}

	//								//Get all the elementID and languageID from the cellTextXMLElement
	//								//Note ITagReader plugin methods are not used.
	//								PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//								PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//								//check whether the tag specifies ProductCopyAttributes.
	//								PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//								
	//								int32 elementID = strElementID.GetAsNumber();
	//								int32 languageID = strLanguageID.GetAsNumber();
	//								
	//								int32 tagStartPos = -1;
	//								int32 tagEndPos = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//								tagStartPos = tagStartPos + 1;
	//								tagEndPos = tagEndPos -1;

	//								PMString dispName(""); 
	//								PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//								if(index == "3")
	//								{
	//									makeTheTagImpotent(cellTextXMLElementPtr);
	//								}					
	//								else if(tableFlag == "1")
	//								{	
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-103"));										
	//									do
	//									{
	//										PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//	//CA(strTypeID);
	//										int32 typeId = strTypeID.GetAsNumber();
	//										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
	//										if(typeValObj==NULL)
	//										{
	//											ptrIAppFramework->LogDebug("AP7_DataSprayerModel::TableUtility::SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable: StructureCache_getListTableTypes's typeValObj is NULL");
	//											break;
	//										}

	//										VectorTypeInfoValue::iterator it1;

	//										bool16 typeIDFound = kFalse;
	//										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
	//										{	
	//											   if(typeId == it1->getTypeId())
	//												{
	//													typeIDFound = kTrue;
	//													break;
	//												}			
	//										}
	//										if(typeIDFound)
	//											dispName = it1->getName();

	//										if(typeValObj)
	//											delete typeValObj;
	//									}while(kFalse);
	//									//CA("You have to insert item item table here");
	//									
	//									//dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
	//									//Modification of XMLTagAttributes.
	//									//modify the XMLTagAttribute "typeId" value to -2.
	//									//We have assumed that there is atleast one stencil with HeaderAttirbute
	//									//true .So assign for all the stencil in the header row , typeId == -2
	//									//cellTextXMLElementPtr->SetAttributeValue("typeId","-2");
	//									
	//								}
	//								else
	//								{
	//									//CA("tableFlag	!= 1");
	//									//following code added by Tushar on 27/12/06
	//									if( elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704 */)
	//									{
	//										/*PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
	//										int32 parentTypeID = strParentTypeID.GetAsNumber();
	//										
	//										VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
	//										if(TypeInfoVectorPtr != NULL)
	//										{
	//											VectorTypeInfoValue::iterator it3;
	//											int32 Type_id = -1;
	//											PMString temp = "";
	//											PMString name = "";
	//											for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
	//											{
	//												Type_id = it3->getTypeId();
	//												if(parentTypeID == Type_id)
	//												{
	//													temp = it3->getName();
	//													if(elementID == -701)
	//													{
	//														dispName = temp;
	//													}
	//													else if(elementID == -702)
	//													{
	//														dispName = temp + " Suffix";
	//													}
	//												}
	//											}
	//										}

	//										if(TypeInfoVectorPtr)
	//											delete TypeInfoVectorPtr;*/
	//									}
	//									else if(elementID == -703)
	//									{
	//										dispName = "$Off";
	//									}
	//									else if(elementID == -704)
	//									{
	//										dispName = "%Off";
	//									}
	//									else if(elementID == -401)  
	//									{
	//										//CA("Make");
	//										dispName = "Make";
	//									}
	//									else if(elementID == -402)  
	//									{
	//										//CA("Model");
	//										dispName = "Model";
	//									}
	//									else if(elementID == -403)  
	//									{
	//										//CA("Year");
	//										dispName = "Year";
	//									}
	//									else if((elementID == -805) )  // For 'Quantity'
	//									{
	//										//CA("Quantity Header");
	//										dispName = "Quantity";
	//									}
	//									else if((elementID == -806) )  // For 'Availability'
	//									{
	//										dispName = "Availability";			
	//									}
	//									else if(elementID == -807)
	//									{									
	//										dispName = "Cross-reference Type";
	//									}
	//									else if(elementID == -808)
	//									{									
	//										dispName = "Rating";
	//									}
	//									else if(elementID == -809)
	//									{									
	//										dispName = "Alternate";
	//									}
	//									else if(elementID == -810)
	//									{									
	//										dispName = "Comments";
	//									}
	//									else if(elementID == -811)
	//									{									
	//										dispName = "";
	//									}
	//									else if((elementID == -812) )  // For 'Quantity'
	//									{
	//										//CA("Quantity Header");
	//										dispName = "Quantity";
	//									}
	//									else if((elementID == -813) )  // For 'Required'
	//									{
	//										//CA("Required Header");
	//										dispName = "Required";
	//									}
	//									else if((elementID == -814) )  // For 'Comments'
	//									{
	//										//CA("Comments Header");
	//										dispName = "Comments";
	//									}
	//									else
	//									{
	//										//CA("else");
	//										dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);
	//										//CA("dispName	:	"+dispName);

	//									}									
	//									//CA("dispName :" + dispName);
	//									//Modification of XMLTagAttributes.
	//									//modify the XMLTagAttribute "typeId" value to -2.
	//									//We have assumed that there is atleast one stencil with HeaderAttirbute
	//									//true .So assign for all the stencil in the header row , typeId == -2
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("1"));
	//									//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString("-2"));
	//								}
	//								
	//								PMString textToInsert("");
	//								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//								if(!iConverter)
	//								{
	//									textToInsert=dispName;					
	//								}
	//								else
	//									textToInsert=iConverter->translateString(dispName);

	//							
	//								int32 tagStartPos1 = -1;
	//								int32 tagEndPos1 = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);
	//								tagStartPos1 = tagStartPos1 + 1;
	//								tagEndPos1 = tagEndPos1 -1;

	//								
	//								PMString entireStory("");
	//								bool16 result1 = kFalse;

	//							
	//								result1=GetTextstoryFromBox(textModel, tagStartPos1, tagEndPos1, entireStory);

	//								if(!entireStory.IsEqual(textToInsert))
	//								{
	//									PMString test("entireStory 2 = ");
	//									test.Append(entireStory);
	//									test.Append(" , textToInsert 2 = ");
	//									test.Append(textToInsert);
	//									//CA(test);
	//									//CA("!entireStory.IsEqual(textToInsert)");
	//									return kTrue;
	//								}
	//								
	//							}
	//						}//end for..iterate through each column.
	//					}//end for..iterate through each row of repeatedRowPattern.
	//					//All the subsequent rows will now spray the ItemValue.
	////nonHeaderRowPatternIndex = 1;					
	//				}//end if 2 tableHeaderPresent

	//				
	//				GridAddress headerRowGridAddress(totalNumberOfRowsBeforeRowInsertion-1,totalNumberOfColumns-1);

	//				int32 ColIndex = -1;
	//				GridArea bodyArea_new = tableModel->GetBodyArea();
	//				
	//				ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
	//				ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));

	//			/*	bool16 isSameMake = kFalse;
	//				bool16 isSameModel = kFalse;

	//				int32 makeIndex = -1;
	//				int32 modelIndex = -1;

	//				int32 makeRepeatCnt = -1;
	//				int32 modelRepeatCnt = -1;

	//				int32 currentMakeModelCnt = -1;
	//				int32 currentModelItemCnt = -1;
	//				int32 currentModelItemIndex = -1;

	//				int32 colIndexPerRow = -1;

	//				vector<int32> itemIds;

	//				Vec_CMakeModel::const_iterator itr;
	//				Vec_CModelModel::const_iterator itr1;  ravik:8129/pwr72

	//				CMakeModel *makeModelPtr = NULL;
	//				CModelModel *modelModelPtr = NULL;

	//				multimap<PMString, int32>sortItems;
	//				multimap<PMString, int32>::iterator mapItr;
	//				int32 ITEMID = -1;
	//				int32 itemIDIndex = -1;*/

	//				PMString PrevYearString("");
	//				PMString PrevModelNameString("");
	//				while (iterTable != endTable)
	//				{
	//					//CA("Iterating table");
	//					GridAddress gridAddress = (*iterTable); 
	//					if(tableHeaderPresent)
	//						if(gridAddress <= headerRowGridAddress){
	//							iterTable++;							
	//							continue;
	//						}

	//					/*if(selectedTableIndex != 0)
	//					{
	//						if(bodyAreaBeforeAddingRow.Contains(gridAddress) || gridAreaIncludingHeader.Contains(gridAddress))
	//						{
	//							++iterTable;
	//							continue;
	//						}
	//					}*/

	//					if(isSameMake == kFalse)
	//					{
	//						//CA("isSameMake == kFalse");
	//						makeIndex++;

	//						makeModelPtr = MMYTablePtr->vec_CMakeModelPtr->at(makeIndex);

	//						currentMakeModelCnt = static_cast<int32>(makeModelPtr->vec_CModelModelPtr->size());

	//						isSameMake = kTrue;
	//					}

	//					if(isSameModel == kFalse)
	//					{
	//						//CA("isSameModel == kFalse");
	//						modelIndex++;
	//						modelModelPtr = makeModelPtr->vec_CModelModelPtr->at(modelIndex);
	//						
	//						currentModelItemCnt = static_cast<int32>(modelModelPtr->vec_ItemYearPtr->size());
	//						
	//						itemIds.clear();
	//						Vec_ItemYear::iterator itr2 = modelModelPtr->vec_ItemYearPtr->begin();
	//						
	//						for(;itr2 != modelModelPtr->vec_ItemYearPtr->end(); itr2++)
	//						{
	//							PMString itemIdStr("(*itr2)->item_Id = ");
	//							itemIdStr.AppendNumber((*itr2)->item_Id);
	//							//CA(itemIdStr);

	//							itemIds.push_back((*itr2)->item_Id);
	//						}

	//						if(sort_by_attributeId != -1)
	//						{
	//							sortItems.clear();
	//							vector<int32>::iterator itr3 = itemIds.begin();
	//							for(;itr3 != itemIds.end(); itr3++)
	//							{
	//								PMString itemIdStr("*itr3 = ");
	//								itemIdStr.AppendNumber(*itr3);
	//								//CA(itemIdStr);
	//								PMString sortAttributeValue = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(*itr3,sort_by_attributeId,languageId);
	//								//CA("sortAttributeValue = " + sortAttributeValue);
	//								double val = sortAttributeValue.GetAsDouble();
	//								sortItems.insert(multimap<double,int32>::value_type(val,*itr3));
	//							}
	//						}
	//						
	//						currentModelItemIndex++;

	//						isSameModel = kTrue;

	//						mapItr = sortItems.begin();
	//					}

	//					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
	//					if(cellContent == nil) 
	//					{
	//						break;
	//					}
	//					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
	//					if(!cellXMLReferenceData) {
	//						break;
	//					}

	//					


	//					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
	//					++iterTable;

	//					ColIndex++;
	//					
	//					int32 rowIndex = ColIndex/CellCountPerRow;
	//					//CA_NUM("ColIndex = ", ColIndex);
	//					//CA_NUM("CellCountPerRow = ", CellCountPerRow);
	//					//CA_NUM("rowIndex = ", rowIndex);

	//					colIndexPerRow++;	

	//					if(itemIDIndex != rowIndex /*&& mapItr != NULL*/)//---conv.-VS 2005 to VS 2008-----
	//					{
	//						//CA("itemIDIndex != rowIndex && mapItr != NULL");
	//						ITEMID = mapItr->second;
	//						mapItr++;
	//						itemIDIndex = rowIndex;
	//					}

	//					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
	//					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());										
	//					if(!isMakeIDAttachedToTableTag)
	//					{
	//						XMLReference tableXMLElementRef = cellXMLElementPtr->GetParent();
	//						InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());

	//						PMString attrVal("");
	//						attrVal.AppendNumber(MMYTablePtr->vec_CMakeModelPtr->at(tableIndex)->getMake_id());
	//						tableXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));			
	//						isMakeIDAttachedToTableTag = kTrue;
	//					}
	//					
	//					//Note that, cell may be blank i.e doesn't contain any textTag.
	//					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
	//					{
	//						//CA("continue");
	//						continue;
	//					}
	//		
	//					//We have considered that there is ONE and ONLY ONE text tag inside a cell.
	//					//so always take the 0th childTag of the Cell.
	//					int32 cellChildCount = cellXMLElementPtr->GetChildCount();
	//					for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount ;tagIndex1++)
	//					{
	//						XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
	//							
	//						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
	//						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
	//						//Check if the cell is blank.
	//						if(cellTextXMLElementPtr == NULL)
	//						{	
	//							continue;
	//						}
	//						
	//						//Get all the elementID and languageID from the cellTextXMLElement
	//						//Note ITagReader plugin methods are not used.
	//						//CA(cellTextXMLElementPtr->GetTagString());

	//						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
	//						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	//						//check whether the tag specifies ProductCopyAttributes.
	//						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	//						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID")); //added by Tushar on 22/12/06
	//						PMString imageFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
	//						/*CA("strParentTypeID =" + strParentTypeID);
	//						CA("strElementID :" + strElementID);
	//						CA("tableFlag :" + tableFlag);*/
	//						
	//						int32 elementID = strElementID.GetAsNumber();
	//						int32 languageID = strLanguageID.GetAsNumber();
	//						int32 itemID = ITEMID;//FinalItemIds[itemIDIndex];
	//						int32 parentTypeID = strParentTypeID.GetAsNumber();	//added by Tushar on 22/12/06
	//																	 
	//						int32 tagStartPos = -1;
	//						int32 tagEndPos = -1;
	//						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
	//						tagStartPos = tagStartPos + 1;
	//						tagEndPos = tagEndPos -1;
	//					
	//						PMString isEventFieldStr = cellTextXMLElementPtr->GetAttributeValue(WideString("isEventField"));
	//						int32 isEventFieldVal = isEventFieldStr.GetAsNumber();

	//						PMString dispName("");
	//						PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//						if(index == "3")
	//						{
	//							makeTheTagImpotent(cellTextXMLElementPtr);
	//						}
	//						else if(tableFlag == "1")
	//						{
	//							//CA("tableFlag == 1");
	//							
	//							//The following code will spray the table preset inside the table cell.
	//							//for copy ID = -104
	//							
	//							if(index == "4")
	//							{//Item ItemTable
	//								//CA("index == 4");

	//								XMLContentReference xmlCntnRef = cellTextXMLElementPtr->GetContentReference();
	//								InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());

	//								bool16 isTablePresentInsideCell = xmlCntnRef.IsTable();
	//								/////following functionallity is not yet completed 
	//								//for section level item having items which contains their own item table. 
	//								if(isTablePresentInsideCell)
	//								{
	//									//CA("isTablePresentInsideCell = kTrue");
	//									//
	//									//InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
	//									//if(innerTableModel == NULL)
	//									//{
	//									//	CA("innerTableModel == NULL");
	//									//	continue;
	//									//}
	//									//UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
	//					
	//									//XMLTagAttributeValue xmlTagAttrVal;
	//									////collect all the attributes from the tag.
	//									////typeId,LanguageID are important
	//									//getAllXMLTagAttributeValues(cellTextXMLElementRef,xmlTagAttrVal);	

	//									////CA("xmlTagAttrVal.typeId : " + xmlTagAttrVal.typeId);

	//									///*if(xmlTagAttrVal.typeId.GetAsNumber() == -111)
	//									//{
	//									//	fillDataInCMedCustomTableInsideTable
	//									//	(
	//									//		cellTextXMLElementRef,
	//									//		innerTableModelUIDRef,
	//									//		pNode

	//									//	);
	//									//}
	//									//else
	//									//{*/
	//									//	FillDataTableForItemOrProduct
	//									//	(
	//									//		cellTextXMLElementRef,
	//									//		innerTableModelUIDRef,
	//									//		kFalse
	//									//	);
	//									///*}*/

	//									//UIDList itemList(slugInfo.curBoxUIDRef);
	//									//UIDList processedItems;
	//									//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
	//									//ErrorCode status =  ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);

	//									//tableflag++;
	//									//continue;
	//								}

	//								PMString typeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
	//								TagStruct tagInfo;												
	//								tagInfo.whichTab = index.GetAsNumber();
	//								tagInfo.parentId = -1; //pNode.getPBObjectID();

	//								tagInfo.isTablePresent = tableFlag.GetAsNumber();
	//								tagInfo.typeId = typeID.GetAsNumber();
	//								tagInfo.sectionID = -1;//sectionid;
	//								
	//									
	//								GetItemTableInTabbedTextForm(tagInfo,dispName);
	//								/*UIDRef tempBox = boxUIDRef;
	//								sprayTableInsideTableCell(tempBox,tagInfo);*/
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("ID"),WideString("-104"));

	//								PMString attrVal;
	//								attrVal.Clear();
	//								attrVal.AppendNumber(-1/*pNode.getPubId()*/);
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								attrVal.Clear();

	//								attrVal.AppendNumber(-1/*pNode.getPBObjectID()*/);
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));

	//								//CA(dispName);
	//							}
	//							//end added on 11July...serial blast day
	//							/*else if(index == "4")
	//							{//Item ItemTable

	//								CA("You have to add ItemItemTable here");													
	//								dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID, languageID, kFalse );
	//							
	//								//Modify the XMLTagAttribute "typeID" .
	//								//Set its value to itemID;
	//								PMString attrVal;
	//								attrVal.AppendNumber(itemID);
	//								cellTextXMLElementPtr->SetAttributeValue("typeId",attrVal);								
	//							}
	//							*/
	//						}
	//						else if(imageFlag == "1")
	//						{
	//							XMLContentReference cntentREF = cellTextXMLElementPtr->GetContentReference();
	//							UIDRef ref = cntentREF.GetUIDRef();
	//							if(ref == UIDRef::gNull)
	//							{
	//								////CA("if(ref == UIDRef::gNull)");
	//								int32 StartPos1 = -1;
	//								int32 EndPos1 = -1;
	//								Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&StartPos1,&EndPos1);									

	//								InterfacePtr<IItemStrand> itemStrand (((IItemStrand*) 
	//									textModel->QueryStrand(kOwnedItemStrandBoss, 
	//									IItemStrand::kDefaultIID)));
	//								if (itemStrand == nil) {
	//									//CA("invalid itemStrand");
	//									continue;
	//								}
	//								//CA("going for Owened items");
	//								OwnedItemDataList ownedList;
	//								itemStrand->CollectOwnedItems(StartPos1 +1 , 1 , &ownedList);
	//								int32 count = ownedList.size();
	//								
	//							/*	PMString ASD("count : ");
	//								ASD.AppendNumber(count);
	//								CA(ASD);*/

	//								if(count > 0)
	//								{
	//									//CA(" count > 0 ");
	//									UIDRef newRef(boxUIDRef.GetDataBase() , ownedList[0].fUID);
	//									if(newRef == UIDRef::gNull)
	//									{
	//										//CA("ref == UIDRef::gNull....2");
	//										continue;
	//									}
	//									ref = newRef;
	//								}else
	//									continue;
	//							}

	//							InterfacePtr<ITagReader> itagReader
	//							((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//							if(!itagReader)
	//								continue ;
	//							
	//							TagList tList=itagReader->getTagsFromBox(ref);
	//							TagStruct tagInfoo;
	//							int numTags=static_cast<int>(tList.size());

	//							if(numTags<=0)
	//							{
	//								continue;
	//							}

	//							tagInfoo=tList[0];

	//							tagInfoo.parentId = itemID;//pNode.getPubId();
	//							tagInfoo.parentTypeID = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	//							tagInfoo.sectionID=CurrentSubSectionID;

	//							IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
	//							if(!iDataSprayer)
	//							{	
	//								//CA("!iDtaSprayer");
	//								continue;
	//							}

	//							if(tagInfoo.typeId == -207 || tagInfoo.typeId == -208 || tagInfoo.typeId == -209|| tagInfoo.typeId == -210 
	//							|| tagInfoo.typeId == -211 || tagInfoo.typeId == -212 || tagInfoo.typeId == -213 || tagInfoo.typeId == -214
	//							|| tagInfoo.typeId == -215 || tagInfoo.typeId == -216 || tagInfoo.typeId == -217 || tagInfoo.typeId == -218
	//							|| tagInfoo.typeId == -219 || tagInfoo.typeId == -220 || tagInfoo.typeId == -221 || tagInfoo.typeId == -222)
	//							{
	//								iDataSprayer->fillBMSImageInBox(ref ,tagInfoo, itemID);
	//							}
	//							else if(tagInfoo.elementId > 0)
	//							{
	//								//isInlineImage = kTrue;
	//								//CA("Calling fillPVAndMPVImageInBox 1");
	//								ptrIAppFramework->clearAllStaticObjects();
	//								iDataSprayer->fillPVAndMPVImageInBox(ref,tagInfoo,itemID,kTrue);
	//								iDataSprayer->fitInlineImageInBoxInsideTableCellNEW(boxUIDRef,textModel,cellXMLElementPtr);
	//													
	//							}
	//							else
	//							{
	//								//iDataSprayer->sprayItemImage(ref,tagInfoo,pNode,itemID);
	//							}

	//							
	//						
	//							PMString attrVal;
	//							attrVal.AppendNumber(itemID);													
	//							cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//							attrVal.Clear();
	//							
	//							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	//							{
	//								tList[tagIndex].tagPtr->Release();
	//							}
	//							continue;
	//						}
	//						else if(tableFlag == "0")
	//						{
	//							//CA("tableFlag == 0");
	//							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
	//							PMString colNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
	//							PMString ischildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));
	//							PMString rowno = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
	//							if(index =="4" && ischildTag == "1")
	//							{
	//								//CA("index == 4 && ischildTag == 1");
	//								if(elementID == -803)  // For 'Letter Key'
	//								{
	//									//CA("here5");
	//									/*dispName.Clear();
	//									if(itemIDIndex < 26)
	//										dispName.Append(AlphabetArray[itemIDIndex]);
	//									else
	//										dispName = "a" ;*/	
	//									dispName.Clear();
	//									int wholeNo =  itemIDIndex / 26;
	//									int remainder = itemIDIndex % 26;								

	//									if(wholeNo == 0)
	//									{// will print 'a' or 'b'... 'z'  upto 26 item ids
	//										dispName.Append(AlphabetArray[remainder]);
	//									}
	//									else  if(wholeNo <= 26)
	//									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
	//										dispName.Append(AlphabetArray[wholeNo-1]);	
	//										dispName.Append(AlphabetArray[remainder]);	
	//									}
	//									else if(wholeNo <= 52)
	//									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
	//										dispName.Append(AlphabetArray[0]);
	//										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
	//										dispName.Append(AlphabetArray[remainder]);										
	//									}
	//								}
	//								//else if((isComponentAttributePresent == 1) && (elementID == -805))
	//								//{
	//								//	//CA("Here 1");
	//								//	dispName.Clear();			
	//								//	//CA(Kit_vec_tablerows[0][0]);
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);	
	//								//}
	//								//else if((isComponentAttributePresent == 1) && (elementID == -806))
	//								//{	//CA("Here 2");									
	//								//	dispName.Clear();	
	//								//	//CA(Kit_vec_tablerows[0][1]);
	//								//	dispName = ((Kit_vec_tablerows[itemIDIndex][1]));	
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -807) )  // For 'Cross-reference Type'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][0]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -808) )  // For 'Rating'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][1]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -809) )  // For 'Alternate'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][2]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -810) )  // For 'Comments'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][3]);			
	//								//}
	//								//else if((isComponentAttributePresent == 2) && (elementID == -811) )  // For 'Catalog'
	//								//{
	//								//	dispName.Clear();	
	//								//	dispName = (Kit_vec_tablerows[itemIDIndex][4]);			
	//								//	//CA(dispName);
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -812))
	//								//{	//CA("Here 3");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][0]));	
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -813))
	//								//{	//CA("Here 4");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][1]));	
	//								//}
	//								//else if((isComponentAttributePresent == 3) && (elementID == -814))
	//								//{	//CA("Here 5");									
	//								//	dispName.Clear();	
	//								//	//CA(Accessory_vec_tablerows[0][1]);
	//								//	dispName = ((Accessory_vec_tablerows[itemIDIndex][2]));	
	//								//}
	//								//else if((isComponentAttributePresent == 2) && ( (Kit_vec_tablerows[itemIDIndex][4]) == "N"))
	//								//{
	//								//	//CA(" non catalog items");
	//								//	dispName.Clear();	
	//								//	dispName = ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(itemID,elementID,languageID); 
	//								//}
	//								else if(isEventFieldVal != -1  &&  isEventFieldVal != 0)
	//								{
	//									/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(ITEMID,elementID,CurrentSubSectionID);
	//									if(vecPtr != NULL){
	//										if(vecPtr->size()> 0){
	//											dispName = vecPtr->at(0).getObjectValue();	
	//										}
	//									}*/
	//								}
	//								//else if(elementID == -401  || elementID == -402  ||  elementID == -403)		//----For Make, Mode, Year . 
	//								//{
	//								//	dispName = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemID,elementID, languageID);
	//								//}
	//								else if(elementID == -401)		//----For Make, Mode, Year . 
	//								{
	//									dispName = makeModelPtr->getMake_Name();
	//								}
	//								else if(elementID == -402)		//----For Make, Mode, Year . 
	//								{
	//									dispName = modelModelPtr->getModel_Name();										
	//									if(PrevModelNameString.IsEqual(dispName))
	//									{
	//										dispName = "";
	//										PMString attrVal;
	//										attrVal.AppendNumber(1);											
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));

	//									}
	//									else
	//										PrevModelNameString = dispName;
	//								}
	//								else if(elementID == -403)		//----For Make, Mode, Year . 
	//								{
	//									Vec_ItemYear::iterator itemYearItr = modelModelPtr->vec_ItemYearPtr->begin();
	//									for(; itemYearItr != modelModelPtr->vec_ItemYearPtr->end(); itemYearItr++)
	//									{
	//										if(itemID == (*itemYearItr)->item_Id)
	//										{
	//											dispName = (*itemYearItr)->year;
	//											if(PrevYearString.IsEqual(dispName))
	//											{
	//												dispName = "";
	//												PMString attrVal;
	//												attrVal.AppendNumber(1);											
	//												cellTextXMLElementPtr->SetAttributeValue(WideString("field5"),WideString(attrVal));
	//											}
	//											else
	//												PrevYearString = dispName;
	//										}
	//									}										
	//								}
	//								else if(elementID == -827)  //**** Number Keys
	//								{
	//									dispName.Clear();
	//									dispName.Append(itemIDIndex+1);
	//								}
	//								else 
	//								{
	//									//CA("catalog Items");
	//									dispName.Clear();	
	//									//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemID,elementID,languageID,kTrue);
	//									dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(itemID,elementID,languageID);
	//								}
	//								PMString attrVal;
	//								attrVal.AppendNumber(itemID);
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//								
	//								/*attrVal.Clear();
	//								attrVal.AppendNumber(PARENTID);														
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								
	//								attrVal.Clear();
	//								attrVal.AppendNumber(pNode.getPBObjectID());
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));	
	//							*/
	//								if(rowno == "-904")
	//								{

	//									attrVal.Clear();
	//									attrVal.AppendNumber(makeModelPtr->getMake_id());														
	//									cellTextXMLElementPtr->SetAttributeValue(WideString("field3"),WideString(attrVal));
	//								
	//									if(elementID != -401)		//----For Make, Mode, Year . 
	//									{
	//										attrVal.Clear();
	//										attrVal.AppendNumber(modelModelPtr->getModel_Id());														
	//										cellTextXMLElementPtr->SetAttributeValue(WideString("field4"),WideString(attrVal));
	//									}
	//								}
	//							}
	//							else if( index == "4")
	//							{//SectionLevelItem
	//								//CA("index == 4");
	//							////added on 22Sept..EventPrice addition.
	//								//if(strElementID == "-701")
	//								//{
	//								//	PMString  attributeIDfromNotes = "-1";
	//								//	bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
	//								//	cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//								//	elementID = attributeIDfromNotes.GetAsNumber();
	//								//}
	//								////ended on 22Sept..EventPrice addition.	
	//								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
	//								{
	//									TagStruct tagInfo;
	//									tagInfo.elementId = elementID;
	//									tagInfo.tagPtr = cellTextXMLElementPtr;
	//									tagInfo.typeId = PARENTID;
	//									//handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);									
	//									//PMString  attributeIDfromNotes = "-1";
	//									//bool8 isNotesValid = getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
	//									//cellTextXMLElementPtr->SetAttributeValue("ID",attributeIDfromNotes);
	//									//elementID = attributeIDfromNotes.GetAsNumber();
	//								}
	//								else if(elementID == -803)  // For 'Letter Key'
	//								{
	//									//CA("here6");
	//									/*dispName.Clear();
	//									if(itemIDIndex < 26)
	//										dispName.Append(AlphabetArray[itemIDIndex]);
	//									else
	//										dispName = "a" ;*/		
	//									dispName.Clear();
	//									int wholeNo =  itemIDIndex / 26;
	//									int remainder = itemIDIndex % 26;								

	//									if(wholeNo == 0)
	//									{// will print 'a' or 'b'... 'z'  upto 26 item ids
	//										dispName.Append(AlphabetArray[remainder]);
	//									}
	//									else  if(wholeNo <= 26)
	//									{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
	//										dispName.Append(AlphabetArray[wholeNo-1]);	
	//										dispName.Append(AlphabetArray[remainder]);	
	//									}
	//									else if(wholeNo <= 52)
	//									{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
	//										dispName.Append(AlphabetArray[0]);
	//										dispName.Append(AlphabetArray[wholeNo -26 -1]);	
	//										dispName.Append(AlphabetArray[remainder]);										
	//									}
	//								}	
	//								else
	//									//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementID,languageID,kTrue);
	//									dispName = ptrIAppFramework->GETItem_GetIndividualItemAttributeDetailsByLanguageId(PARENTID,elementID,languageID);
	//								if(elementID == -827)  // For 'Number Key'
	//								{
	//									dispName.Clear();
	//									dispName.Append(itemIDIndex+1);
	//								}
	//								
	//								PMString attrVal;
	//								attrVal.AppendNumber(/*pNode.getPubId()*/PARENTID);	
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(attrVal));
	//								//cellTextXMLElementPtr->SetAttributeValue(WideString("childTag"),WideString("1"));

	//								cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(attrVal));
	//									
	//								
	//								/*attrVal.Clear();
	//								attrVal.AppendNumber(pNode.getPBObjectID());
	//								cellTextXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(attrVal));
	//					*/
	//							}

	//						}
	//						cellTextXMLElementPtr->SetAttributeValue(WideString("header"),WideString("-1"));
	//						/*if(isCallFromTS ){
	//							PMString temp("");
	//							temp.AppendNumber(tableSourceInfoValueObj->getVec_Table_ID().at(0));
	//							cellTextXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(temp));								
	//						}*/
	//						PMString textToInsert("");
	//						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//						if(!iConverter)
	//						{
	//							textToInsert=dispName;					
	//						}
	//						else
	//							textToInsert=iConverter->translateString(dispName);
	//					
	//						int32 tagStartPos1 = -1;
	//						int32 tagEndPos1 = -1;
	//						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos1,&tagEndPos1);
	//						tagStartPos1 = tagStartPos1 + 1;
	//						tagEndPos1 = tagEndPos1 -1;

	//						
	//						PMString entireStory("");
	//						bool16 result1 = kFalse;

	//					
	//						result1=GetTextstoryFromBox(textModel, tagStartPos1, tagEndPos1, entireStory);

	//						if(!entireStory.IsEqual(textToInsert))
	//						{
	//							PMString test("entireStory 3 = ");
	//							test.Append(entireStory);
	//							test.Append(" , textToInsert 3 = ");
	//							test.Append(textToInsert);
	//							//CA(test);
	//							//CA("!entireStory.IsEqual(textToInsert)");
	//							return kTrue;
	//						}

	//					}
	//					
	//					if(colIndexPerRow == CellCountPerRow - 1)
	//					{
	//						//CA("colIndexPerRow == CellCountPerRow - 1");
	//						colIndexPerRow = -1;
	//						currentModelItemIndex++;

	//						if(currentModelItemIndex == currentModelItemCnt)
	//						{
	//							//CA("currentModelItemIndex == currentModelItemCnt");
	//							if(modelIndex < currentMakeModelCnt - 1)
	//							{
	//								//CA("modelIndex < currentMakeModelCnt - 1");
	//								isSameModel = kFalse;
	//								currentModelItemCnt = -1;
	//								currentModelItemIndex = -1;

	//							}else
	//							{
	//								//CA("else");
	//								isSameMake = kFalse;
	//								isSameModel = kFalse;
	//								modelIndex = -1;
	//								currentMakeModelCnt = -1;
	//								currentModelItemCnt = -1;
	//								currentModelItemIndex = -1;
	//							}
	//						}
	//					}	
	//				}//end for..iterate through each column	
	//				
	//				//tableSprayedCount--;
	//			}
	//	}
	//	delete MMYTablePtr;
	//}while(kFalse);
	return kFalse;
}


bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	//CA(__FUNCTION__);
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		const textchar characterCode = (*iter).GetValue();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void handleSprayingOfEventPriceRelatedAdditions(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttributeValue)
{
	///added on 22Sept..EventPrice addition.
	//CA("handleSprayingOfEventPriceRelatedAdditions");
    //int32 eventTabletypeId = tagInfo.elementId;	//Table type id for Event table  ////commented on 21_12	
	double eventAttributeType = tagInfo.elementId;
	//int32 eventTabletypeId = -1;
	//int32 SalePriceTabletypeId = -1;
	//int32 RegularPriceTabletypeId = -1;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return ;

	//eventTabletypeId = tagInfo.parentTypeID;

	//CA_NUM("tagInfo.typeId : ",tagInfo.typeId);
	//int32 eventTabletypeId = 1172;
	
	/*if((tagInfo.elementId == -701 || tagInfo.elementId == -702 || tagInfo.elementId == -703 || tagInfo.elementId == -704) && globalParentTypeId != -1)
	{
		eventTabletypeId = globalParentTypeId;
		globalParentTypeId = -1;
	}*/
		
	//CA_NUM("tagInfo.parentTypeID() :",tagInfo.parentTypeID);
    //int32 eventAttributeType = tagInfo.typeId; //hardcoded price id to distinguish whether it is EventPrice,suffics, $ off and % off. //commented on 21_12	
	//		int32 eventAttributeType = tagInfo.elementId;
	//CA_NUM("elementId :",elementId);
	double itemId = -1;
	/*if(pNode.getIsProduct() == 0)
	{
		if(tagInfo.typeId == tagInfo.parentId && tagInfo.typeId != -1)
		{
			tagInfo.typeId = pNode.getPubId();
			itemId = pNode.getPubId();
		}
	}
	tagInfo.typeId = pNode.getPubId();*/

	if(tagInfo.childTag == 1)
		itemId = tagInfo.childId;
	else
		itemId = tagInfo.parentId;

	//int32 elementId = tagInfo.elementId; //old functionality commented by Tushar to remove hardcode functinality
	//CA_NUM("tagInfo.elementId() :",tagInfo.elementId);
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//			return ;
	double sectionid = tagInfo.sectionID;
	//int32 pbObjectID = -1;
	/*if(pNode.getIsProduct() == 1)
	{
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;
		
	}*/

//	PMString attributeIDfromNotes("-1");
//	PMString attributeIDfromNotesSales("-1");
//	PMString attributeIDfromNotesRegular("-1");

	//CA_NUM("sectionid :",sectionid);
	//CA_NUM("tagInfo.typeId :",tagInfo.typeId);

	double temp = tagInfo.elementId;

	//bool8 isValidNotes =  getAttributeIDFromNotesNew(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes);//old functionality commented by Tushar on 19/12/06
	//bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,tagInfo.elementId); //in this method 'tagInfo.elementId' is used as 'eventPriceTableTypeID'
	//if(eventAttributeType == -703 || eventAttributeType == -704)
	//{
	//	SalePriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemSalePriceTableType();
	//	RegularPriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemRegularPriceTableType();
	//	//CA_NUM("SalePriceTabletypeId :",SalePriceTabletypeId);
	//	//CA_NUM("RegularPriceTabletypeId :",RegularPriceTabletypeId);

	//	bool8 isValidNotes =  getAttributeIDFromNotes(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesSales,attributeIDfromNotesRegular,SalePriceTabletypeId,RegularPriceTabletypeId);
	//	//bool8 isValidNotes1 =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesRegular,RegularPriceTabletypeId);
	//}
	//else 
	//	bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,eventTabletypeId);
//     //CA_NUM("after tagInfo.typeId() :",tagInfo.typeId);
   //CA_NUM("after sectionid :",sectionid);
   //CA_NUM("after tagInfo.parentTypeID :",tagInfo.parentTypeID);

   //CA("after attributeIDfromNotes = " + attributeIDfromNotes);

	//added on 22Sept..EventPrice addition.
	PMString strPbObjectID;
	strPbObjectID.AppendNumber(PMReal(pNode.getPBObjectID()));
    //CA("strPbObjectID = " + strPbObjectID);
	//tagInfo.tagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(strPbObjectID));  //Cs4
	Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo.tagPtr->GetXMLReference(), WideString("pbObjectId"),WideString(strPbObjectID)); 

	//tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString("-1"/*strPbObjectID*/));  //Cs4
	Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo.tagPtr->GetXMLReference(), WideString("rowno"),WideString("-1"/*strPbObjectID*/));
	//added on 22Sept..EventPrice addition.
	//	CA_NUM("elementId :",elementId);

	//if(eventAttributeType == -701)
	//{
	//	//tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);//commented by Tushar on 28/12/06
	//	//tagInfo.elementId =attributeIDfromNotes.GetAsNumber();
	//	if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(attributeIDfromNotes.GetAsNumber()) == kTrue){
	//		VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(),tagInfo.sectionID);
	//		if(vecPtr != NULL)
	//			if(vecPtr->size()> 0)
	//				itemAttributeValue = vecPtr->at(0).getObjectValue();
	//	}
	//	else if(attributeIDfromNotes.GetAsNumber() == -401 || attributeIDfromNotes.GetAsNumber() == -402 || attributeIDfromNotes.GetAsNumber() == -403){
	//		itemAttributeValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID);
	//	}
	//	else{
	//		itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID, kFalse );
	//	}
	//}
	//else if(eventAttributeType == -702)
	//{
	//	itemAttributeValue = ptrIAppFramework->getPriceSuffix(attributeIDfromNotes.GetAsNumber());
	//	//CA(" inside itemAttributeValue" + itemAttributeValue);
	//}
	//else 
	if(eventAttributeType == -703 || eventAttributeType == -704)
	{
		/*if(attributeIDfromNotesSales == "" && attributeIDfromNotesRegular == "")
		{
			itemAttributeValue = "";
			return;
		}
		else if(attributeIDfromNotesSales == "" || attributeIDfromNotesSales == "")
		{
			itemAttributeValue = "";
			return;
		}*/


		double eventPrice = 0;
		double regularPrice = 0;


		double eventPriceId = -1;
		double regularPriceId = -1;	
		eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
		regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");				
		if(eventPriceId == -1 || regularPriceId == -1)
		{
			itemAttributeValue = "";
			return;
		}
		do
		{


			PMString eventPriceValue("");
			/*if(attributeIDfromNotesSales.GetAsNumber() == -401 || attributeIDfromNotesSales.GetAsNumber() == -402 || attributeIDfromNotesSales.GetAsNumber() == -403){
				eventPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesSales.GetAsNumber(), tagInfo.languageID);
			}*/
			//if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(eventPriceId) == kTrue){
			//	VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),eventPriceId,tagInfo.sectionID);
			//	if(vecPtr != NULL){
			//		if(vecPtr->size()> 0){
			//			eventPriceValue = vecPtr->at(0).getObjectValue();
			//			//CA("eventPriceValue 1111111111= " + eventPriceValue);
			//		}
			//	}
			//}
			//else
			{
				eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId, eventPriceId, tagInfo.languageID, tagInfo.sectionID, kFalse );

			}
			
			if(eventPriceValue == "")
			{
				itemAttributeValue = "";
				return;
			}
			
			PMString ::ConversionError * pErr = NULL;
			eventPrice = eventPriceValue.GetAsDouble(pErr,NULL);
			if(( pErr != NULL) &&(*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
			{//CA("11111111eventPriceValue == """);
				itemAttributeValue = "";
				return;
			}
		}while(kFalse);
		do
		{	

			PMString regularPriceValue("");
			/*if(attributeIDfromNotesRegular.GetAsNumber() == -401 || attributeIDfromNotesRegular.GetAsNumber() == -402 || attributeIDfromNotesRegular.GetAsNumber() == -403){
				regularPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesRegular.GetAsNumber(), tagInfo.languageID);
			}*/
			//if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(regularPriceId) == kTrue){
			//	VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),regularPriceId,tagInfo.sectionID);
			//	if(vecPtr != NULL){
			//		if(vecPtr->size()> 0){
			//			regularPriceValue = vecPtr->at(0).getObjectValue();
			//			//CA("regularPriceValue 1111111111= " + regularPriceValue);
			//		}
			//	}
			//}
			//else
			{
				regularPriceValue= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId ,regularPriceId , tagInfo.languageID, tagInfo.sectionID, kFalse );
				//CA("regularPriceValue = " + regularPriceValue);
			}
//					PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,regularPriceID, tagInfo.languageID, kTrue );
			if(regularPriceValue == "")
			{	
				itemAttributeValue = "";
				return;
			}

			PMString ::ConversionError * pErr = NULL;
			regularPrice = regularPriceValue.GetAsDouble(pErr,NULL);
			if(( pErr != NULL) && (*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
			{	
				itemAttributeValue = "";
				return;
			}
		}while(kFalse);

		if(eventAttributeType == -703)
		{
			//PMReal temp;
			//PMReal nearestDollar = ::Ceiling(realDifference);
			//itemAttributeValue.AppendNumber(nearestDollar);
			//itemAttributeValue.AppendNumber(intDifference);
			double realDifference = ptrIAppFramework->getDollerOff(regularPrice ,eventPrice);
			itemAttributeValue.AppendNumber(PMReal(realDifference));
		}
		else if(eventAttributeType == -704)
		{
			itemAttributeValue = "";
			if(regularPrice != 0)
			{
				//int32 actualPercentage = (int32)((realDifference/regularPrice) * 100);
				//int32 percentageDiff = ToInt32(actualPercentage);
				//itemAttributeValue.AppendNumber(percentageDiff);
				double percentageDiff = ptrIAppFramework->getPercentageOff(regularPrice ,eventPrice);
				itemAttributeValue.AppendNumber(PMReal(percentageDiff));
			}
		}
	
	}
	
	//ended on 22Sept..EventPrice addition.

//ended on 22Sept..EventPrice addition.
}
ErrorCode RefreshMultipleListsSprayedInOneTable(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
	//CA("......... RefreshMultipleListsSprayedInOneTable......");

	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
	vector<double>ItmID;
	vector<double>attriID;

	vector<double>rowvec;
	vector<double>colvec;
	vector<double>tableIDvec;
	vector<double>TypeIdvec;
	vector<int32>Indexvec;

	ItmID.clear();
	attriID.clear();
	rowvec.clear();
	colvec.clear();
	tableIDvec.clear();
	TypeIdvec.clear();
	Indexvec.clear();

	vector<double>ItemIDVecfromCM;
	ItemIDVecfromCM.clear();
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	bool16 AddHeaderToTable = kFalse;

	do
	{		

		




		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;//breaks the do{}while(kFalse)
		}		 	
						
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
			break;//breaks the do{}while(kFalse)
		}
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
			break;//breaks the do{}while(kFalse)
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;//breaks the do{}while(kFalse)
		}
		TableUtility tUtility;
		UIDRef tableUIDRef;
		if(tUtility.isTablePresent(boxUIDRef, tableUIDRef))
		{
		//	CA("Table is present");
		}
		else 
		{
			//CA("Table is not present");
		}

		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;//breaks the do{}while(kFalse)
		}

		InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
			//return ;
		}
		TagList tList; //,tList_checkForHybrid;
		tList =  itagReader->getTagsFromBox(boxUIDRef);
		for(int32 t=0;t<tList.size();t++)
		{
			int32 childTagCount = tList[t].tagPtr->GetChildCount();
			//if(tList[t].typeId==-3)
			{
				for(int32 childTagIndex=0;childTagIndex<childTagCount;childTagIndex++)
				{
					XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());//
					if(childTagXMLElementPtr == nil)
					continue;
					PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
					double childId22 = strchildId22.GetAsDouble();

					PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
					double ID22 = strID22.GetAsDouble();	

					PMString a("ID22 : ");
					a.AppendNumber(ID22);
					//CA(a);

					int32 elementCount=childTagXMLElementPtr->GetChildCount();
					for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
					{
						XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
						if(childElement == nil)
						continue;
						PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
						double childId11 = strchildId11.GetAsDouble();

						PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
						double ID11 = strID11.GetAsDouble();	

						PMString strTpye11 = childElement->GetAttributeValue(WideString("typeId"));//CS4
						double TypeId11 = strTpye11.GetAsDouble();	

						PMString strIndex11 = childElement->GetAttributeValue(WideString("index"));//CS4
						int32 Index11 = strIndex11.GetAsNumber();	

						PMString strrowno11 = childElement->GetAttributeValue(WideString("rowno"));//CS4
						double rowno11 = strrowno11.GetAsDouble();	

						PMString strcolno11 = childElement->GetAttributeValue(WideString("colno"));//CS4
						double colno11 = strcolno11.GetAsDouble();	

						PMString strtableflag11 = childElement->GetAttributeValue(WideString("tableFlag"));//CS4
						int32 TableFlag11 = strtableflag11.GetAsNumber();	




						//ItmID  attriID
						if(childId11 >0 || ID11>0 )
						{
							//CA("123");
							ItmID.push_back(childId11);
							attriID.push_back(ID11);
							rowvec.push_back(rowno11);
							colvec.push_back(colno11);
							tableIDvec.push_back(TableFlag11);
							TypeIdvec.push_back(TypeId11);
							Indexvec.push_back(Index11);
						
						}

						PMString a("childId11 : ");
						a.AppendNumber(childId11);
						a.Append("\n");
						a.Append("ID11 : ");
						a.AppendNumber(ID11);
						//CA(a);
			 
					}
			}
			}
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		
		listTableInfo listTableInfoObj;
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			//CA("tableIndex");
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == nil)
				continue;//continues the for..tableIndex

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
				continue;//continues the for..tableIndex
			}
					
			UIDRef tableRef(::GetUIDRef(tableModel)); 
			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
			IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}		

			//Get the SectionID,ProductID from the tableXMLElementTagPtr.
			PMString strSectionID =  tableXMLElementPtr->GetAttributeValue(WideString("sectionID"));
			PMString strProductID =  tableXMLElementPtr->GetAttributeValue(WideString("parentID"));
			PMString strLanguageID = tableXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
			//This is important addition.You will come to know from this value whether the table
			//contains Item's data or Product's data.

			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index"));
			PMString RowCountForRepetaionPatternString = tableXMLElementPtr->GetAttributeValue(WideString("rowno"));
			PMString strParentTypeId = tableXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
			PMString strChildId = tableXMLElementPtr->GetAttributeValue(WideString("childId"));
			
			double sectionID = strSectionID.GetAsDouble();
			double productID = strProductID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();			
			int32 RowCountForRepetaionPattern = RowCountForRepetaionPatternString.GetAsNumber();
			double parentTypeId = strParentTypeId.GetAsDouble();
			double childId = strChildId.GetAsDouble();

		

			

			 

			bool16 IsAutoResize = kFalse;
			if(slugInfo.isAutoResize)
				IsAutoResize= kTrue;		

			/*if(slugInfo.colno == -555)
				AddHeaderToTable = kFalse;
			else
				AddHeaderToTable = kTrue;*/

			bool16 HeaderRowPresent = kFalse;

			RowRange HeaderRowRange(0,0);
			HeaderRowRange = tableModel->GetHeaderRows();
			int32 headerStart = HeaderRowRange.start;
			int32 headerCount = HeaderRowRange.count;

			if(headerCount != 0)
			{	//CA("HeaderPresent");	
				HeaderRowPresent = kTrue;
			}
           
			bool16 refreshOriginalTablewithHeader = kFalse;
			int32 listTableNameTagCnt = 0;
			int32 childTagCnt = slugInfo.tagPtr->GetChildCount();
			for(int32 childIndex = 0 ; childIndex < childTagCnt; childIndex++)
			{
				XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(childIndex);
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					continue;
				}
				XMLReference cellChildXMLElementRef = cellXMLElementPtr->GetNthChild(0);
				InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLElementRef.Instantiate());
				if(cellChildXMLElementPtr==NULL )
				{
					continue;
				}
				PMString elementIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("ID"));
				if(elementIDValue == "-121"){
					refreshOriginalTablewithHeader = kTrue;
					listTableNameTagCnt++;
					PMString tableIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("tableId"));
					listTableInfoObj.tableID.push_back(tableIDValue.GetAsDouble());
				}
			}
			RowRange bodyRowRange(0,0);
			bodyRowRange = tableModel->GetBodyRows();
			int32 bodyRowStart = bodyRowRange.start;
			int32 bodyRowCount = bodyRowRange.count;
			
			PMString s("headerStart	:	");
			s.AppendNumber(headerStart);
			s.Append("\rheaderCount	:	");
			s.AppendNumber(headerCount);
			s.Append("\rbodyRowStart	:	");
			s.AppendNumber(bodyRowStart);
			s.Append("\rbodyRowCount	:	");
			s.AppendNumber(bodyRowCount);
			//CA(s);
			


			VectorScreenTableInfoPtr tableInfo= NULL;
			bool16 isSectionLevelItemItemPresent = kFalse;
			int32 isComponentAttributePresent =0;
			vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
												  //values inside it for spraying 'Quantity' & 'Availability' attributes.

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();

			vector<rowInfo> rowInfoVector;
			
			bool16 ListTableNameTagPresent = kFalse;
			bool16 isFirstTaggedCell = kTrue;
			
			if(refreshOriginalTablewithHeader)
			{

				//CA("refreshOriginalTablewithHeader   trueeeee");
				ITableModel::const_iterator iterTable(tableModel->begin());
				ITableModel::const_iterator end(tableModel->end());
				
				while(iterTable != end) 
				{
					
					//TextIndex	 threadStart;
					//int32		 threadLength;
					GridAddress gridAddress = *iterTable;

					const GridID gridID = tableModel->GetGridID(*iterTable);
				
					if(rowInfoVector.size() > 0)
					{
						bool16 isRowAlreadyPresent = kFalse;
						for(int32 j=0; j < rowInfoVector.size(); j++ )
						{
							if(rowInfoVector[j].rowno == gridAddress.row)
							{
								isRowAlreadyPresent = kTrue;
								rowInfoVector[j].numberOfColoumns = gridAddress.col;
								break;
							}
						}
						if(isRowAlreadyPresent)
						{	
							//CA("isRowAlreadyPresent");
							++iterTable;
							continue;
						}
					}

					rowInfo rowInfoObj;
					rowInfoObj.rowno = gridAddress.row;
					rowInfoObj.numberOfColoumns = gridAddress.col;
					if(HeaderRowPresent)
						rowInfoObj.isHeader = HeaderRowRange.Contains(gridAddress.row);

					bool16 doWhileBreak = kFalse;
					do
					{
						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
						if(cellContent == nil) 
						{
							//CA("cellContent == nil");
							break;
						}
						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
						if(!cellXMLReferenceData) {
							//CA("!cellXMLReferenceData");
							break;
						}
						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
						//++iterTable1;

						//ColIndex++;
						//int32 itemIndex = ColIndex/bodyCellsForItem;
					
			
						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							//CA("Blank Cell with no attributes ");
							
							
							
							rowInfoObj.Data = "";	
							rowInfoObj.tableId = -1; 
							rowInfoObj.itemId = -1;
							rowInfoObj.parentId = -1;
							rowInfoVector.push_back(rowInfoObj);
							++iterTable;
							doWhileBreak = kTrue;
							break;
						}
						PMString s("cellXMLElementPtr->GetChildCount() : ");
						s.AppendNumber(cellXMLElementPtr->GetChildCount());
						//CA(s);
						rowInfoObj.Data = "";
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
					
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								//CA("cellTextXMLElementPtr == NULL");
								continue;
							}
							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							PMString IDstr = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString TypeIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString ParentIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
							PMString tableIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
							PMString childIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));

							if(IDstr == "-121")
							{
								//CA("-121");
								rowInfoObj.Data.Append(IDstr);
								rowInfoObj.tableId = tableIdstr.GetAsDouble(); 
								rowInfoObj.itemId = -1;
								rowInfoObj.parentId = ParentIdstr.GetAsDouble();
								ListTableNameTagPresent = kTrue;
								

							}
							else
							{
								if(ListTableNameTagPresent == kFalse && isFirstTaggedCell == kTrue)
								{
									rowInfoObj.Data = "";
									rowInfoObj.tableId = tableIdstr.GetAsDouble(); 
									rowInfoObj.itemId = -1;
									rowInfoObj.parentId = ParentIdstr.GetAsDouble();
									rowInfoVector.push_back(rowInfoObj);
									isFirstTaggedCell = kFalse;
									
								}
								//CA(IDstr);
								rowInfoObj.Data.Append(IDstr);
								rowInfoObj.tableId = -1;
								rowInfoObj.itemId = childIdstr.GetAsDouble();
								rowInfoObj.parentId = ParentIdstr.GetAsDouble();
							}						
						}
						rowInfoVector.push_back(rowInfoObj);
					}while(0);
					if(doWhileBreak)
						continue;

					//CA("++iterTable");
					++iterTable;
				}

				PMString ASD("rowInfoVector size : ");
				ASD.AppendNumber((int32)rowInfoVector.size());
				//CA(ASD);

				vector<TableInfostruct> TableinfoVector;
				for(int32 i=0; i <rowInfoVector.size(); i++ )
				{
					//CA("TableInfostruct");

					//after inserting first row
					if(TableinfoVector.size() > 0)
					{
						if (i < TableinfoVector[(TableinfoVector.size() - 1)].bodyRowEnd +1 )
							continue;
					}

					TableInfostruct tableInfoObj;
					//if(i == 0)
					//{
						tableInfoObj.tableId = rowInfoVector[i].tableId;
						tableInfoObj.parentId = rowInfoVector[i].parentId;
						tableInfoObj.isHederPresentInTable = rowInfoVector[i].isHeader;
						tableInfoObj.headerRowStart = rowInfoVector[i].rowno;
						tableInfoObj.FirstRowData = rowInfoVector[i].Data;

						if(rowInfoVector[i].Data == "" || rowInfoVector[i].Data == "-121")
						{	// this means that first row of table is either blank or having List/TableName tag
							tableInfoObj.headerRowEnd = rowInfoVector[i].rowno;

							if(i < (rowInfoVector.size()-1))
							{
								tableInfoObj.bodyRowStart = -1;
								for(int32 j = i+1; j < rowInfoVector.size(); j++)
								{
									if(rowInfoVector[i].Data == rowInfoVector[j].Data)
									{
										tableInfoObj.bodyRowEnd = rowInfoVector[j-1].rowno;
										break;
									}
									else
									{
										if(tableInfoObj.bodyRowStart == -1)
										{
											tableInfoObj.bodyRowStart = rowInfoVector[j].rowno;
											tableInfoObj.parentId = rowInfoVector[j].parentId;
										}

										tableInfoObj.bodyRowEnd =  rowInfoVector[j].rowno;
										tableInfoObj.itemIds.push_back(rowInfoVector[j].itemId);
										
									}
								}
							}
						}
						else
						{	// this means first row is removed we are getting directly item row.
							tableInfoObj.headerRowStart = -1;
							tableInfoObj.headerRowEnd = -1;
							tableInfoObj.bodyRowStart = rowInfoVector[i].rowno;

							PMString ASD("tableInfoObj.bodyRowStart : ");
							ASD.AppendNumber(tableInfoObj.bodyRowStart);
							//CA(ASD);

							tableInfoObj.itemIds.push_back(rowInfoVector[i].itemId);
							if(i < (rowInfoVector.size()-1))
							{							
								for(int32 j = i+1; j < rowInfoVector.size(); j++)
								{
									if(rowInfoVector[i].Data != rowInfoVector[j].Data)
									{
										tableInfoObj.bodyRowEnd = rowInfoVector[j-1].rowno;
										break;
									}
									else
									{
										tableInfoObj.bodyRowEnd =  rowInfoVector[j].rowno;
										tableInfoObj.itemIds.push_back(rowInfoVector[j].itemId);
									}
								}
							}
						}

						
						TableinfoVector.push_back(tableInfoObj);

						continue;
					//}			
					
				}

				PMString ASD1("TableinfoVector size : ");
				ASD1.AppendNumber((int32)TableinfoVector.size());
				//CA(ASD1);

				for(int32 k=0; k< TableinfoVector.size(); k++)
				{
					PMString Table("Table Detils \n ");

					Table.Append("tableId : ");
					Table.AppendNumber(TableinfoVector[k].tableId);
					if(TableinfoVector[k].tableId == -1)
					{
						//CA("if(TableinfoVector[k].tableId == -1)");
						VectorLongIntPtr longIntPtrObj = NULL; //ptrIAppFramework->GetProjectProducts_getTableIDsByObjectIdItemIdStringAndSeqOrderstring(TableinfoVector[k].parentId, &(TableinfoVector[k].itemIds));
						if(longIntPtrObj != NULL)
						{
							if(longIntPtrObj->size() >0)
								//CA("Size >0");
								TableinfoVector[k].tableId = longIntPtrObj->at(0);
						}

						Table.Append("\n new tableId : ");
						Table.AppendNumber(TableinfoVector[k].tableId);
					}
					Table.Append("\n parentId : ");
					Table.AppendNumber(TableinfoVector[k].parentId);
					Table.Append("\n headerRowStart : ");
					Table.AppendNumber(TableinfoVector[k].headerRowStart);
					Table.Append("\n headerRowEnd : ");
					Table.AppendNumber(TableinfoVector[k].headerRowEnd);
					Table.Append("\n bodyRowStart : ");
					Table.AppendNumber(TableinfoVector[k].bodyRowStart);
					Table.Append("\n bodyRowEnd : ");
					Table.AppendNumber(TableinfoVector[k].bodyRowEnd);
					Table.Append("\n tableid : ");
					Table.AppendNumber(TableinfoVector[k].tableId);

					for(int32 p=0; p < TableinfoVector[k].itemIds.size();p++)
					{
						Table.Append("\n ItemID : ");
						Table.AppendNumber(TableinfoVector[k].itemIds[p]);
					}
					Table.Append("\n" + TableinfoVector[k].FirstRowData);

					//CA(Table);
				}

				if(slugInfo.whichTab != 3)
				{
					//CA("ALL list not for Product. Can not Refresh");
					return kFalse;
				}
				//CA("slugInfo.whichTab == 3");
				if(ptrIAppFramework->get_isONEsourceMode())
				{
					// For ONEsource mode
					//CA("123 For ONESOURCE");
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(productID);
				}else
				{
					//For publication
					//CA("234 FOR PUBLICATION");
					tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, productID, languageID);
				}

				if(tableInfo->size()==0)
				{ 
					//CA(" tableInfo->size()==0");						
					break;//breaks the for..tableIndex
				}

				// now we are iterating loop in reverse order to refresh lower table first.
				
				int32 tempTableinfoVectorSize = static_cast<int32>(TableinfoVector.size());
				int32 count=0;
				if(iscontentbuttonselected==kFalse)//added by sagar only working for structure not for content because we want to modify values not structure 
				{
					for(int32 k=tempTableinfoVectorSize; k>=  0; k--)
					{
						count++;
						for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						{		
							oTableValue = *it;	
							vec_items = oTableValue.getItemIds();
							//ItemIDVecfromCM = oTableValue.getItemIds();
							for(int32 j=0;j<vec_items.size();j++)
							{
								ItemIDVecfromCM.push_back(vec_items.at(j));
							}
							
							if(oTableValue.getTableID() != TableinfoVector[k].tableId)
							{
								oTableValue.getItemIds();
								//CA("Continue");
								continue;
							}

							

							int32 TotalNumberOfrowsBeforeRefresh = static_cast<int32>(TableinfoVector[k].itemIds.size());
							

							if(vec_items.size() == TableinfoVector[k].itemIds.size())
							{
								//CA("rowsize is same");
							}
							
							else if(vec_items.size() > TableinfoVector[k].itemIds.size())
							{
								//CA("need to add new items rows");

								int32 NoOfRowsToBeAdded = static_cast<int32>(vec_items.size()) - TotalNumberOfrowsBeforeRefresh;
								int32 rowPatternRepeatCount = (NoOfRowsToBeAdded / RowCountForRepetaionPattern);

								result = tableCommands->InsertRows(RowRange(TableinfoVector[k].bodyRowEnd, NoOfRowsToBeAdded), Tables::eAfter, 0);
								
								GridArea gridAreaForEntireTable;
								gridAreaForEntireTable.topRow = TableinfoVector[k].bodyRowStart;
								gridAreaForEntireTable.leftCol = 0;
								gridAreaForEntireTable.bottomRow = TableinfoVector[k].bodyRowStart + 1;
								gridAreaForEntireTable.rightCol = rowInfoVector[TableinfoVector[k].bodyRowStart].numberOfColoumns + 1;

								bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
								if(canCopy == kFalse)
								{
									//CA("canCopy == kFalse");
									ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
									break;
								}
								//CA("canCopy");					
								TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
								if(tempPtr == NULL)
								{
									ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
									break;
								}
								
								for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
								{
									/*PMString ASD("GridAddress : ");
									ASD.AppendNumber((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh));
									ASD.Append("  totalNumberOfBodyRowsBeforeRefresh : ");
									ASD.AppendNumber(totalNumberOfBodyRowsBeforeRefresh);
									ASD.Append("  RowCountForRepetaionPattern : ");
									ASD.AppendNumber(RowCountForRepetaionPattern);
									ASD.Append("  pasteIndex : ");
									ASD.AppendNumber(pasteIndex);
									CA(ASD);*/
									tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (TableinfoVector[k].bodyRowEnd+1),0),ITableModel::eAll,tempPtr);
									tempPtr = tableModel->Copy(gridAreaForEntireTable);
								}


							}
							else if(vec_items.size() < TableinfoVector[k].itemIds.size())
							{
								//CA("need to remove the items");
								int32 NoOfRowsToBeDeleted = static_cast<int32>(TableinfoVector[k].itemIds.size()) - static_cast<int32>(vec_items.size());
								result = tableCommands->DeleteRows(RowRange(TableinfoVector[k].bodyRowStart + static_cast<int32>(vec_items.size()) , NoOfRowsToBeDeleted));
							}
							
							int32 ColIndex = -1;
							//int32 totalBodyCell1 = 0;
							GridArea bodyArea_new1 = tableModel->GetBodyArea();
							ITableModel::const_iterator iterTable4(tableModel->begin(bodyArea_new1));
							ITableModel::const_iterator endTable4(tableModel->end(bodyArea_new1));

							int32 totalNoOfBodyRowsinNewTable = static_cast<int32>(vec_items.size());
							int32 startRowIndex = TableinfoVector[k].bodyRowStart;
							int32 endRowIndex = TableinfoVector[k].bodyRowStart -1 + totalNoOfBodyRowsinNewTable;
							int32 bodyCellsForItem = rowInfoVector[TableinfoVector[k].bodyRowStart].numberOfColoumns +1;

							while (iterTable4 != endTable4)
							{
								//CA("while (iterTable4 != endTable4)");
								GridAddress gridAddress = (*iterTable4);   
								
								if((gridAddress.row < startRowIndex) || (gridAddress.row > endRowIndex))
								{
									//CA("before continue");
									++iterTable4;
									continue;
								}

								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
								if(cellContent == nil) 
								{
									break;
								}
								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
								if(!cellXMLReferenceData) {
									break;
								}
								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
								++iterTable4;

								ColIndex++;
								int32 itemIndex = ColIndex/bodyCellsForItem;
							
					
								//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
								//Also note that, cell may be blank i.e doesn't contain any textTag.
								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
								{
									continue;
								}
								//PMString s("cellXMLElementPtr->GetChildCount() : ");
								//s.AppendNumber(cellXMLElementPtr->GetChildCount());
								//CA(s);
								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
								{
									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							
									//Check if the cell is blank.
									if(cellTextXMLElementPtr == NULL)
									{
										continue;
									}
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));

									if(index == "3")
									{
										
										continue;
									}
									else if(index == "4")
									{
										//CA("index == 4");
										PMString ItemID("");
										ItemID.AppendNumber(PMReal(vec_items[itemIndex]));//(p);

										PMString s("ItemID : ");
										s.AppendNumber(vec_items[itemIndex]);
										//s.Append(" ItemID :" + ItemID);
										//CA(s);
										//cellTextXMLElementPtr->SetAttributeValue(WideString("childId"), WideString(ItemID));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"), WideString(ItemID));
									}
								} 
							} 
						}
					//	break;//by amarjit patil     //og commented by sagar it needs to iterate for multiple table to add and delete new item rows
						
					}
				}
				
				
				//CA("aftreeeeeeeeeeeeeeee");
				
			//	for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
			//	{//start for tagIndex = 0
				int32 count11=0,z=-1;
				GridArea headerArea = tableModel->GetHeaderArea();
				GridArea tableArea_new = tableModel->GetTotalArea();
				ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
				ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));

				//CA("A");
				int32 count111=0;
				while (iterTable2 != endTable2)
				{
					
					//totalBodyCell++;
					
					GridAddress gridAddress = (*iterTable2);         
					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
					if(cellContent == nil) 
					{
						//CA("cellContent == nil");
						break;
					}
					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
					if(!cellXMLReferenceData) {
						//CA("!cellXMLReferenceData");
						break;
					}
					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
					++iterTable2;

					//XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);

					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}
					int32 NoofChilds = cellXMLElementPtr->GetChildCount();
					PMString String("");
					for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
					{ // iterate thruogh cell's tags
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());

						
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == nil)
						{
							continue;
						}
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
						PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
						PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
						PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));

						//added by Tushar on 26/12/06
						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 		double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();

						PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
						PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

						double ColNO = strColNo.GetAsDouble();
						double RowNo = strRowNo.GetAsDouble();
						double typeID = strTypeID.GetAsDouble();
						double sectionid = strSectionId.GetAsDouble();

						double parentId = strParentID.GetAsDouble();

						double childId = strChildID.GetAsDouble();
						int32 header = strHeader.GetAsNumber();
						double tableID = strTableID.GetAsDouble();
						int32 childTag = strChildTag.GetAsDouble();

						if(childId!=count11 && childId!=-1)
						{
							z++;	
						}

						count11=childId;
						bool16 isHeaderCell = headerArea.Contains(gridAddress);

						//if(sequenceNo != -2 && isHeaderCell == kFalse)  // for Header Elements
						//	typeID	= FinalItemIds[sequenceNo];
						//else if(isHeaderCell == kTrue)
						//	typeID = strProductID.GetAsNumber();
						
						/*PMString NewTypeId("");
						NewTypeId.AppendNumber(typeID);
						cellTextXMLElementPtr->SetAttributeValue("typeId", NewTypeId);*/

						double rowno	=	strRowNo.GetAsDouble();
						//added by Tushar on 26/12/06
						double parentTypeID	= strParentTypeID.GetAsDouble();
						

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						//We will come across three types of tags.
						//1. ProductCopyAttributes tableFlag == 0
						//for all other attributes tableFlag == 1
						//2. TableHeader..typeId == -2
						//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
						PMString dispName("");
						if(tableFlag == "-15")
						{//This is a impotent tag.Don't process this.	
							//CA("tableFlag == -15");
							continue;
						}
						else if(tableFlag == "0")
						{	
							//The cell contains only copy attributes
							//added on 25Sept..EventPrice addition.

							//ended on 25Sept..EventPrice addtion.
							if(strIndex == "3")
							{//Product copy attribute
								if(strElementID == "-121")
								{
									//CA("tagInfo.elementId == -121");							
															
									double objectId = strParentID.GetAsDouble(); 
									VectorScreenTableInfoPtr tableInfo = NULL;
									CItemTableValue oTableValue;	
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
									//CA("Not ONEsource");
									do
									{
										tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, languageID); //og

										if(!tableInfo)
										{
											//CA("!tableInfo");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
											break;
										}
										if(tableInfo->size()==0)
										{
											//CA(" tableInfo->size()==0");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
											break;
										}
										
										VectorScreenTableInfoValue::iterator it;
										bool16 typeidFound=kFalse;
										
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											//CA("for Loop");
											oTableValue = *it;
											if(oTableValue.getTableID() == tableID)
											{   
												//CA("typeidFound=kTrue;");
												typeidFound=kTrue;				
												break;
											}
										}
										
										if(typeidFound)
										{
											dispName = oTableValue.getName();
											
										}
										else
										{
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
											dispName = "";
										}
									}while(0);									
								}
								else
								{	
									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionid,  kFalse);
								}
							}
							else if(strIndex == "4" && header == 1)
							{
									//	CA("strIndex == 4 && typeID == -2");
								//Item copy attribue header
								//following code added by Tushar on 27/12/06
								if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
								{
									//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
									//if(TypeInfoVectorPtr != NULL)
									//{//CA("TypeInfoVectorPtr != NULL");
									//	VectorTypeInfoValue::iterator it3;
									//	int32 Type_id = -1;
									//	PMString temp = "";
									//	PMString name = "";
									//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									//	{
									//		Type_id = it3->getTypeId();
									//		if(parentTypeID == Type_id)
									//		{
									//			temp = it3->getName();
									//			if(elementID == -701)
									//			{
									//				dispName = temp;
									//			}
									//			else if(elementID == -702)
									//			{
									//				dispName = temp + " Suffix";
									//			}
									//		}
									//	}
									//}
									//if(TypeInfoVectorPtr)
									//	delete TypeInfoVectorPtr;
								}
								else if(elementID == -703)
								{
									//CA("dispName = $Off");
									dispName = "$Off";
								}
								else if(elementID == -704)
								{
									//CA("dispName = %Off");
									dispName = "%Off";
								}
								else if(elementID == -805)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -806)
								{									
									dispName = "Availablity";
                                }
								else if(elementID == -807)
								{									
									dispName = "Cross-reference Type";
                                }
								else if(elementID == -808)
								{									
									dispName = "Rating";
                                }
								else if(elementID == -809)
								{									
									dispName = "Alternate";
                                }
								else if(elementID == -810)
								{									
									dispName = "Comments";
                                }
								else if(elementID == -811)
								{									
									dispName = "";
                                }
								else if(elementID == -812)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -813)
								{									
									dispName = "Required";
                                }
								else if(elementID == -814)
								{									
									dispName = "Comments";
                                }
								else
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
							}
							else if(strIndex == "4")
							{
								//CA("TableID=0 ,Index =4");
                                
                                if(strElementID == "-121")
								{
									//CA("tagInfo.elementId == -121");
                                    
									double objectId = strParentID.GetAsDouble();
									VectorScreenTableInfoPtr tableInfo = NULL;
									CItemTableValue oTableValue;
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
									//CA("Not ONEsource");
									do
									{
                                        tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(objectId, sectionid, languageID);
                                        
										if(!tableInfo)
										{
											//CA("!tableInfo");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
											break;
										}
										if(tableInfo->size()==0)
										{
											//CA(" tableInfo->size()==0");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
											break;
										}
										
										VectorScreenTableInfoValue::iterator it;
										bool16 typeidFound=kFalse;
										
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											//CA("for Loop");
											oTableValue = *it;
											if(oTableValue.getTableID() == tableID)
											{
												//CA("typeidFound=kTrue;");
												typeidFound=kTrue;
												break;
											}
										}
										
										if(typeidFound)
										{
											dispName = oTableValue.getName();
											
										}
										else
										{
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
											dispName = "";
										}
									}while(0);									
								}
								//Item copy attribute 
								else if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
								{
								//	CA("common functions 1219");
									PublicationNode pNode;
									pNode.setPubId(productID);

									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
									pNode.setPBObjectID(strPBObjectID.GetAsDouble());
									if(indexValueAttachedToTable == "4")
									{
										
										pNode.setIsProduct(0);
									}
									else if(indexValueAttachedToTable == "3")
									{
										pNode.setIsProduct(1);
									}
									if(rowno != -1)
									{
										//CA("sETTING IsONEsource flag as false");
										pNode.setIsONEsource(kFalse);
									}
									TagStruct tagInfo;
									tagInfo.elementId = elementID;
									tagInfo.languageID = languageID;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = sectionID;
									tagInfo.tagPtr = cellTextXMLElementPtr;
									tagInfo.header = header;
									tagInfo.childId = childId;
									tagInfo.childTag = childTag;
									tagInfo.parentTypeID = parentTypeID;
									
									handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
								}
								
								else if(elementID == -803)  // For 'Letter Key'
								{	
									//CA("here7");
									dispName.Clear();
									//int wholeNo =  sequenceNo / 26;
									//int remainder = sequenceNo % 26;								

									//if(wholeNo == 0)
									//{// will print 'a' or 'b'... 'z'  upto 26 item ids
									//	dispName.Append(AlphabetArray[remainder]);
									//}
									//else  if(wholeNo <= 26)
									//{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
									//	dispName.Append(AlphabetArray[wholeNo-1]);	
									//	dispName.Append(AlphabetArray[remainder]);	
									//}
									//else if(wholeNo <= 52)
									//{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
									//	dispName.Append(AlphabetArray[0]);
									//	dispName.Append(AlphabetArray[wholeNo -26 -1]);	
									//	dispName.Append(AlphabetArray[remainder]);										
									//}
								}
								else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -807) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
                                }
								else if((elementID == -808) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
                                }
								else if((elementID == -809) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][2]);	
                                }
								else if((elementID == -810) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][3]);	
                                }
								else if((elementID == -811) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][4]);	
                                }
								else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][2]);	
								}
								else
								{
									SlugStruct stencileInfo;

									if(IsBookrefreshSelected==kTrue)
									{
										if(/*issrtucturebuttenselected==kTrue || */Booklevelstructurevariable == kTrue)
										{
											//CA("Book Structure");
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIDVecfromCM.at(z),elementID,languageID, sectionID, kTrue);
										}
										else if(/*iscontentbuttonselected==kTrue ||*/ Booklevelcontentvariable==kTrue)
										{
											//CA("Book Conent");
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID, sectionID, kTrue);
												PMString s("dispName : ");
												s.Append(dispName);
												s.Append("\n");
												s.Append("itemId : ");
												s.Append("\n");
												s.Append("ElementID : ");
												s.AppendNumber(elementID);
												s.AppendNumber(childId);
												//CA(s);
										}

									}
									else if(IsFrameRefreshSelected==kTrue)
									{

										ErrorCode err = kFailure;

									
										InterfacePtr<ITableUtility> iTableUtlObj
										((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
										if(!iTableUtlObj)
										{
											 //CA("!iTableUtlObj");
											ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
											//return ;
										}
											
										XMLReference txmlref;
										PMString TableTagNameNew = slugInfo.tagPtr->GetTagString();

										//temp_to_test  = oAttribVal.getDisplayName();

										if(issrtucturebuttenselected==kTrue /*|| Booklevelstructurevariable == kTrue*/)
										{
											//CA("Frame Structure");

											PMString tagName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,slugInfo.languageID );
											PMString TableColName("");
											//TableColName.Append("Model_Number");//Advance Table Cell Data
											//TableColName.Append("Model_Number");

											TableColName.Append("ATTRID_");
											TableColName.AppendNumber(PMReal(elementID));

											//TableColName = keepOnlyAlphaNumeric(TableColName);
											stencileInfo.TagName = TableColName;

											stencileInfo.typeId  = slugInfo.typeId;//-2;  // TypeId is hardcodded to -2 for Header Elements.
											stencileInfo.header  = -1;
											stencileInfo.parentId = slugInfo.parentId;  // -2 for Header Element
											stencileInfo.parentTypeId = slugInfo.parentTypeID;
											stencileInfo.whichTab = 4; // Item Attributes
											stencileInfo.imgFlag = slugInfo.sectionID; 
											stencileInfo.isAutoResize = slugInfo.isAutoResize;
											stencileInfo.LanguageID = slugInfo.languageID ;
											stencileInfo.pbObjectId = slugInfo.pbObjectId;
											stencileInfo.tableType = -1;
											stencileInfo.tableId = tableID;
											stencileInfo.row_no =RowNo;
											stencileInfo.col_no =ColNO;
											stencileInfo.elementId = elementID;
											//stencileInfo.tagStartPos=slugInfo.startIndex;
											//stencileInfo.tagEndPos = slugInfo.endIndex;
											stencileInfo.childId = ItemIDVecfromCM.at(z);
											stencileInfo.childTag=1;
											stencileInfo.elementName=tagName;
											

											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIDVecfromCM.at(z),elementID,languageID, sectionID, kTrue);

												PMString childValue("");
												childValue.AppendNumber(PMReal(ItemIDVecfromCM.at(z)));
												//cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(childValue));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("childId"),WideString(childValue));

												//iTableUtlObj->setTableRowColData(tableUIDRef, dispName, stencileInfo.row_no,stencileInfo.col_no);

												//iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,slugInfo.tagPtr->GetTagString(), stencileInfo.elementName, txmlref, stencileInfo,stencileInfo.row_no, stencileInfo.col_no,tableID);
											//count111++;*/
										}
										else if(iscontentbuttonselected==kTrue/* || Booklevelcontentvariable==kTrue*/)
										{
											//CA("Frame Content");

											PMString tagName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,slugInfo.languageID );
										PMString TableColName("");
										//TableColName.Append("Model_Number");//Advance Table Cell Data
										
										
										//TableColName.Append("Model_Number");

										TableColName.Append("ATTRID_");
										TableColName.AppendNumber(PMReal(elementID));

										//TableColName = keepOnlyAlphaNumeric(TableColName);
										stencileInfo.TagName = TableColName;

										stencileInfo.typeId  = TypeIdvec.at(count111);//-2;  // TypeId is hardcodded to -2 for Header Elements.
										stencileInfo.header  = -1;
										stencileInfo.parentId = slugInfo.parentId;  // -2 for Header Element
										stencileInfo.parentTypeId = slugInfo.parentTypeID;
										stencileInfo.whichTab = 4; // Item Attributes
										stencileInfo.imgFlag = slugInfo.sectionID; 
										stencileInfo.isAutoResize = slugInfo.isAutoResize;
										stencileInfo.LanguageID = slugInfo.languageID ;
										stencileInfo.pbObjectId = slugInfo.pbObjectId;
										stencileInfo.tableType = -1;
										stencileInfo.tableId = tableID;
										stencileInfo.row_no =RowNo;
										stencileInfo.col_no =ColNO;
										stencileInfo.elementId = elementID;
										//stencileInfo.tagStartPos=slugInfo.startIndex;
										//stencileInfo.tagEndPos = slugInfo.endIndex;
										stencileInfo.childId = childId;
										stencileInfo.childTag=1;
										stencileInfo.elementName=tagName;
										PMString a("tagName : ");
											a.Append(tagName);
											//CA(a);


											//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID,kTrue);
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItmID.at(count111),attriID.at(count111),languageID, sectionID, kTrue);
											PMString s("ItemId :  : ");
												s.AppendNumber(ItmID.at(count111));
												s.Append("\n");
												s.Append("attribute :  : ");
												s.AppendNumber(attriID.at(count111));
												/*s.Append("\n");
												s.Append("row : ");
												s.AppendNumber(stencileInfo.row_no);*/
												//CA(s);

											
											
											/*iTableUtlObj->setTableRowColData(tableUIDRef, dispName, stencileInfo.row_no,stencileInfo.col_no);
											iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,slugInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, stencileInfo.row_no,stencileInfo.col_no,tableID );*/
											//
												count111++;
										}
									
									}
									
								}
							}
							
						}
						else if(tableFlag == "1")
						{
							//CA("tableFlag==1");//the cell contains the table
							if(strIndex == "3")
							{
								if(strElementID == "-103")
								{
									//Print the table header..table type
									do
									{
										//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
	//CA(strTypeID);					//int32 typeId = strTypeID.GetAsNumber();
										
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}

										VectorTypeInfoValue::iterator it1;

										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
											if(typeID == it1->getTypeId())
											{
												typeIDFound = kTrue;
												break;
											}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse); 
								}
								else if(strElementID == "-104")
								{
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									tagInfo.parentId = strParentID.GetAsDouble();
									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);
								}
								else
								{
								  /*CA("Inside My code");*/
								  if(issrtucturebuttenselected== kTrue)
								  {
									VectorScreenTableInfoPtr tableInfo = NULL;
									CItemTableValue oTableValue;
									do
									{
										tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, parentId, languageID, kFalse);
										if(!tableInfo)
										{
											//CA("!tableInfo");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
											break;
										}
										if(tableInfo->size()==0)
										{
											//CA(" tableInfo->size()==0");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
											break;
										}
										VectorScreenTableInfoValue::iterator it;
										/*PMStringVec*/vector<PMString>::iterator strIt;
										bool16 typeidFound=kFalse;

										double tableId = 0;
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											//CA("for Loop");
											oTableValue = *it;
											double tttt=oTableValue.getTableID();
											

											if(oTableValue.getTableID() == tableID)
											{   
												//CA("typeidFound=kTrue;");
												typeidFound=kTrue;
												tableId = oTableValue.getTableID();
												/*PMString str1("tableID :");
												str1.AppendNumber(tableId);
												CA(str1);*/
												break;
											}
										}
										if(typeidFound)
										{
											//CA("typeidFound");
											//dispName = ptrIAppFramework->GetProduct_getItemByTableIDName(tableId);
											PMString e("dispName :");
											e.Append(dispName);
											//CA(e);
										}
										else
										{
											//CA("NOT FOUND");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
										}
									}while(0);
								  }
								  if(iscontentbuttonselected ==kTrue)
								  {
									  //CA("selected content");
									  //InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
									  //	if(!itagReader)
										//{ 
										//	ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::!itagReader");		
										//	return kFalse;
										//}
									  VectorScreenTableInfoPtr tableInfo = NULL;
									  //TagList tList;
									  ////tList = itagReader->getTagsFromBox(boxUIDRef);

									if(parentId != -1)
										{
										//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(parentId);
										}
									if(!tableInfo)
									{
										//CA("if(!tableInfo)");
										ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
										return kFalse;
									}
									//CA("outside");

									CItemTableValue oTableValue;
									VectorScreenTableInfoValue::iterator it12;
									for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
									{	
										
										oTableValue = *it12;
										if(tableID == oTableValue.getTableID())
										{	
											//CA("123333");
											dispName = oTableValue.getName();
											//CA("tempBuffer = " + tempBuffer);
											break;
										}
									}
									if(tableInfo)
										delete tableInfo;
								  
								  }
								}

							}
							else if(strIndex == "4")
							{
								if(strElementID == "-103")
								{
									do
									{
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}
										VectorTypeInfoValue::iterator it1;
										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse);
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									//This is just a temporary adjustment
									PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
									tagInfo.parentId = pbObjectId.GetAsDouble();

									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);

								}

							}
						}
						else if(tableFlag == "-13")
						{
							//the cell contains the product copy attribute header
							//CA("End.... ");
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
								if(result)
								dispName = cElementModelObj.getDisplayName();
						}
						
						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=dispName;					
						}
						else
							textToInsert=iConverter->translateString(dispName);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
						//WideString insertData(textToInsert);
						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);
					}
				}//end for tagIndex = 0 

			}// end of refreshOriginalTablewithHeader sagar
			else
			{
				//CA("for simple table");
				
				RefreshMultipleListsSprayedInOneTableNew(boxUIDRef, slugInfo);
				
			}
	   }//end for..tableIndex
	   errcode = kSuccess;
	}while(0);
	return errcode;     
}

//added by sagar if we have blank rows in header apart from listAndtableName
ErrorCode RefreshMultipleListsSprayedInOneTableNew(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
	//CA("......... RefreshMultipleListsSprayedInOneTable......");

	//CA("RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable");
	vector<double>ItmID;
	vector<double>attriID;

	vector<double>rowvec;
	vector<double>colvec;
	vector<double>tableIDvec;
	vector<double>TypeIdvec;
	vector<int32>Indexvec;

	ItmID.clear();
	attriID.clear();
	rowvec.clear();
	colvec.clear();
	tableIDvec.clear();
	TypeIdvec.clear();
	Indexvec.clear();

	vector<double>ItemIDVecfromCM;
	ItemIDVecfromCM.clear();
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	bool16 AddHeaderToTable = kFalse;

	do
	{		



		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;//breaks the do{}while(kFalse)
		}		 	
						
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
			break;//breaks the do{}while(kFalse)
		}
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
			break;//breaks the do{}while(kFalse)
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;//breaks the do{}while(kFalse)
		}
		TableUtility tUtility;
		UIDRef tableUIDRef;
		if(tUtility.isTablePresent(boxUIDRef, tableUIDRef))
		{
		//	CA("Table is present");
		}
		else 
		{
			//CA("Table is not present");
		}

		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;//breaks the do{}while(kFalse)
		}

		InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
			//return ;
		}
		TagList tList; //,tList_checkForHybrid;
		tList =  itagReader->getTagsFromBox(boxUIDRef);
		for(int32 t=0;t<tList.size();t++)
		{
			int32 childTagCount = tList[t].tagPtr->GetChildCount();
			//if(tList[t].typeId==-3)
			{
				for(int32 childTagIndex=0;childTagIndex<childTagCount;childTagIndex++)
				{
					XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());//
					if(childTagXMLElementPtr == nil)
					continue;
					PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
					double childId22 = strchildId22.GetAsDouble();

					PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
					double ID22 = strID22.GetAsDouble();	

					PMString a("ID22 : ");
					a.AppendNumber(ID22);
					//CA(a);

					int32 elementCount=childTagXMLElementPtr->GetChildCount();
					for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
					{
						XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
						if(childElement == nil)
						continue;
						PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
						double childId11 = strchildId11.GetAsDouble();

						PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
						double ID11 = strID11.GetAsDouble();	

						PMString strTpye11 = childElement->GetAttributeValue(WideString("typeId"));//CS4
						double TypeId11 = strTpye11.GetAsDouble();	

						PMString strIndex11 = childElement->GetAttributeValue(WideString("index"));//CS4
						int32 Index11 = strIndex11.GetAsNumber();	

						PMString strrowno11 = childElement->GetAttributeValue(WideString("rowno"));//CS4
						double rowno11 = strrowno11.GetAsDouble();	

						PMString strcolno11 = childElement->GetAttributeValue(WideString("colno"));//CS4
						double colno11 = strcolno11.GetAsDouble();	

						PMString strtableflag11 = childElement->GetAttributeValue(WideString("tableFlag"));//CS4
						int32 TableFlag11 = strtableflag11.GetAsNumber();	




						//ItmID  attriID
						if(childId11 >0 || ID11>0 )
						{
							//CA("123");
							ItmID.push_back(childId11);
							attriID.push_back(ID11);
							rowvec.push_back(rowno11);
							colvec.push_back(colno11);
							tableIDvec.push_back(TableFlag11);
							TypeIdvec.push_back(TypeId11);
							Indexvec.push_back(Index11);
						
						}

						PMString a("childId11 : ");
						a.AppendNumber(childId11);
						a.Append("\n");
						a.Append("ID11 : ");
						a.AppendNumber(ID11);
						//CA(a);
			 
					}
			}
			}
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		
		listTableInfo listTableInfoObj;
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			//CA("tableIndex");
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == nil)
				continue;//continues the for..tableIndex

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
				continue;//continues the for..tableIndex
			}
					
			UIDRef tableRef(::GetUIDRef(tableModel)); 
			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
			IIDXMLElement* & tableXMLElementPtr = slugInfo.tagPtr;
			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
				continue; //continues the for..tableIndex
			}		

			//Get the SectionID,ProductID from the tableXMLElementTagPtr.
			PMString strSectionID =  tableXMLElementPtr->GetAttributeValue(WideString("sectionID"));
			PMString strProductID =  tableXMLElementPtr->GetAttributeValue(WideString("parentID"));
			PMString strLanguageID = tableXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
			//This is important addition.You will come to know from this value whether the table
			//contains Item's data or Product's data.

			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index"));
			PMString RowCountForRepetaionPatternString = tableXMLElementPtr->GetAttributeValue(WideString("rowno"));
			PMString strParentTypeId = tableXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
			PMString strChildId = tableXMLElementPtr->GetAttributeValue(WideString("childId"));
			
			double sectionID = strSectionID.GetAsDouble();
			double productID = strProductID.GetAsDouble();
			double languageID = strLanguageID.GetAsDouble();			
			int32 RowCountForRepetaionPattern = RowCountForRepetaionPatternString.GetAsNumber();
			double parentTypeId = strParentTypeId.GetAsDouble();
			double childId = strChildId.GetAsDouble();

		

			

			 

			bool16 IsAutoResize = kFalse;
			if(slugInfo.isAutoResize)
				IsAutoResize= kTrue;		

			/*if(slugInfo.colno == -555)
				AddHeaderToTable = kFalse;
			else
				AddHeaderToTable = kTrue;*/

			bool16 HeaderRowPresent = kFalse;

			RowRange HeaderRowRange(0,0);
			HeaderRowRange = tableModel->GetHeaderRows();
			int32 headerStart = HeaderRowRange.start;
			int32 headerCount = HeaderRowRange.count;

			if(headerCount != 0)
			{	//CA("HeaderPresent");	
				HeaderRowPresent = kTrue;
			}
           
			//bool16 myFlag = kFalse;
			int32 listTableNameTagCnt = 0;
			int32 childTagCnt = slugInfo.tagPtr->GetChildCount();
			for(int32 childIndex = 0 ; childIndex < childTagCnt; childIndex++)
			{
				XMLReference cellXMLElementRef = slugInfo.tagPtr->GetNthChild(childIndex);
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				PMString elementIDValue("");
				elementIDValue.clear();
				if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
				{
					//continue;
					elementIDValue.Append("999");
					cellXMLElementRef = slugInfo.tagPtr->GetNthChild(childIndex+1);
					InterfacePtr<IIDXMLElement>cellXMLElementPtrnew(cellXMLElementRef.Instantiate());
					XMLReference cellChildXMLElementRef = cellXMLElementPtrnew->GetNthChild(0);
					InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLElementRef.Instantiate());
					if(cellChildXMLElementPtr==NULL )
					{
						continue;
					}
					//PMString elementIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("ID"));
					if(elementIDValue == "999"){
						//myFlag = kTrue;
						listTableNameTagCnt++;
						PMString tableIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("tableId"));
						listTableInfoObj.tableID.push_back(tableIDValue.GetAsDouble());
					}
				}
				//XMLReference cellChildXMLElementRef = cellXMLElementPtr->GetNthChild(0);
				//InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLElementRef.Instantiate());
				//if(cellChildXMLElementPtr==NULL )
				//{
				//	continue;
				//}
				////PMString elementIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("ID"));
				//if(elementIDValue == "999"){
				//	myFlag = kTrue;
				//	listTableNameTagCnt++;
				//	PMString tableIDValue = cellChildXMLElementPtr->GetAttributeValue(WideString("tableId"));
				//	listTableInfoObj.tableID.push_back(tableIDValue.GetAsNumber());
				//}
			}
			RowRange bodyRowRange(0,0);
			bodyRowRange = tableModel->GetBodyRows();
			int32 bodyRowStart = bodyRowRange.start;
			int32 bodyRowCount = bodyRowRange.count;
			
			PMString s("headerStart	:	");
			s.AppendNumber(headerStart);
			s.Append("\rheaderCount	:	");
			s.AppendNumber(headerCount);
			s.Append("\rbodyRowStart	:	");
			s.AppendNumber(bodyRowStart);
			s.Append("\rbodyRowCount	:	");
			s.AppendNumber(bodyRowCount);
			//CA(s);
			


			VectorScreenTableInfoPtr tableInfo= NULL;
			bool16 isSectionLevelItemItemPresent = kFalse;
			int32 isComponentAttributePresent =0;
			vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
												  //values inside it for spraying 'Quantity' & 'Availability' attributes.

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();

			vector<rowInfo> rowInfoVector;
			
			bool16 ListTableNameTagPresent = kFalse;
			bool16 isFirstTaggedCell = kTrue;
			
				//CA("for simple table");
				
				{

				
				ITableModel::const_iterator iterTable(tableModel->begin());
				ITableModel::const_iterator end(tableModel->end());
				
				while(iterTable != end) 
				{
					
					//TextIndex	 threadStart;
					//int32		 threadLength;
					GridAddress gridAddress = *iterTable;

					const GridID gridID = tableModel->GetGridID(*iterTable);
				
					if(rowInfoVector.size() > 0)
					{
						bool16 isRowAlreadyPresent = kFalse;
						for(int32 j=0; j < rowInfoVector.size(); j++ )
						{
							if(rowInfoVector[j].rowno == gridAddress.row)
							{
								isRowAlreadyPresent = kTrue;
								rowInfoVector[j].numberOfColoumns = gridAddress.col;
								break;
							}
						}
						if(isRowAlreadyPresent)
						{	
							//CA("isRowAlreadyPresent");
							++iterTable;
							continue;
						}
					}

					rowInfo rowInfoObj;
					rowInfoObj.rowno = gridAddress.row;
					rowInfoObj.numberOfColoumns = gridAddress.col;
					if(HeaderRowPresent)
						rowInfoObj.isHeader = HeaderRowRange.Contains(gridAddress.row);

					bool16 doWhileBreak = kFalse;
					do
					{
						InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
						if(cellContent == nil) 
						{
						//	CA("cellContent == nil");
							break;
						}
						InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
						if(!cellXMLReferenceData) {
							//CA("!cellXMLReferenceData");
							break;
						}
						XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
						//++iterTable1;

						//ColIndex++;
						//int32 itemIndex = ColIndex/bodyCellsForItem;
					
			
						//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						//We have considered that there is ONE and ONLY ONE text tag inside a cell.
						//Also note that, cell may be blank i.e doesn't contain any textTag.
						if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
						{
							//CA("Blank Cell with no attributes ");
							
							gridAddress = *(++iterTable);
							InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
							if(cellContent == nil) 
							{
								//CA("cellContent == nil");
								break;
							}
							InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
							if(!cellXMLReferenceData) {
								///CA("!cellXMLReferenceData");
								break;
							}
							XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
							//++iterTable1;

							//ColIndex++;
							//int32 itemIndex = ColIndex/bodyCellsForItem;
						
				
							//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
							InterfacePtr<IIDXMLElement>cellXMLElementPtrnew(cellXMLElementRef.Instantiate());
							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
							{
									//CA("Blank Cell with no attributes ");
							}
							
						
							XMLReference cellChildXMLElementRef = cellXMLElementPtrnew->GetNthChild(0);
							InterfacePtr<IIDXMLElement>cellChildXMLElementPtr(cellChildXMLElementRef.Instantiate());
							if(cellChildXMLElementPtr==NULL )
							{
								continue;
							}
							rowInfoObj.Data.Append("");
							PMString tableIdstr = cellChildXMLElementPtr->GetAttributeValue(WideString("tableId"));
							PMString ParentIdstr = cellChildXMLElementPtr->GetAttributeValue(WideString("parentID"));

							rowInfoObj.tableId =tableIdstr.GetAsDouble();
							rowInfoObj.itemId = -1;
							rowInfoObj.parentId = ParentIdstr.GetAsDouble();
							ListTableNameTagPresent = kTrue;
							rowInfoVector.push_back(rowInfoObj);
							//++iterTable;
							doWhileBreak = kTrue;
							break;
						}
						PMString s("cellXMLElementPtr->GetChildCount() : ");
						s.AppendNumber(cellXMLElementPtr->GetChildCount());
						//CA(s);
						rowInfoObj.Data = "";
						for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
						{
							XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
							//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
					
							//Check if the cell is blank.
							if(cellTextXMLElementPtr == NULL)
							{
								//CA("cellTextXMLElementPtr == NULL");
								continue;
							}
							PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
							PMString IDstr = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString TypeIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
							PMString ParentIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
							PMString tableIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
							PMString childIdstr = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));

							if(IDstr == "999")
							{
								//CA("-121");
								rowInfoObj.Data.Append(IDstr);
								rowInfoObj.tableId = tableIdstr.GetAsDouble(); 
								rowInfoObj.itemId = -1;
								rowInfoObj.parentId = ParentIdstr.GetAsDouble();
								ListTableNameTagPresent = kTrue;
								

							}
							else
							{
								if(ListTableNameTagPresent == kFalse && isFirstTaggedCell == kTrue)
								{
									rowInfoObj.Data = "";
									rowInfoObj.tableId = tableIdstr.GetAsDouble(); 
									rowInfoObj.itemId = -1;
									rowInfoObj.parentId = ParentIdstr.GetAsDouble();
									rowInfoVector.push_back(rowInfoObj);
									isFirstTaggedCell = kFalse;
									
								}
								//CA(IDstr);
								rowInfoObj.Data.Append(IDstr);
								rowInfoObj.tableId = -1;
								rowInfoObj.itemId = childIdstr.GetAsDouble();
								rowInfoObj.parentId = ParentIdstr.GetAsDouble();
							}						
						}
						rowInfoVector.push_back(rowInfoObj);
					}while(0);
					if(doWhileBreak)
						continue;

					//CA("++iterTable");
					++iterTable;
				}

				PMString ASD("rowInfoVector size : ");
				ASD.AppendNumber((int32)rowInfoVector.size());
				//CA(ASD);

				vector<TableInfostruct> TableinfoVector;
				for(int32 i=0; i <rowInfoVector.size(); i++ )
				{
					//CA("TableInfostruct");

					//after inserting first row
					if(TableinfoVector.size() > 0)
					{
						if (i < TableinfoVector[(TableinfoVector.size() - 1)].bodyRowEnd +1 )
							continue;
					}

					TableInfostruct tableInfoObj;
					//if(i == 0)
					//{
						tableInfoObj.tableId = rowInfoVector[i].tableId;
						tableInfoObj.parentId = rowInfoVector[i].parentId;
						tableInfoObj.isHederPresentInTable = rowInfoVector[i].isHeader;
						tableInfoObj.headerRowStart = rowInfoVector[i].rowno;
						tableInfoObj.FirstRowData = rowInfoVector[i].Data;

						if(rowInfoVector[i].Data == "" || rowInfoVector[i].Data == "999")
						{	// this means that first row of table is either blank or having List/TableName tag
							tableInfoObj.headerRowEnd = rowInfoVector[i].rowno;

							if(i < (rowInfoVector.size()-1))
							{
								tableInfoObj.bodyRowStart = -1;
								for(int32 j = i+1; j < rowInfoVector.size(); j++)
								{
									if(rowInfoVector[i].Data == rowInfoVector[j].Data)
									{
										tableInfoObj.bodyRowEnd = rowInfoVector[j-1].rowno;
										break;
									}
									else
									{
										if(tableInfoObj.bodyRowStart == -1)
										{
											tableInfoObj.bodyRowStart = rowInfoVector[j].rowno;
											tableInfoObj.parentId = rowInfoVector[j].parentId;
										}

										tableInfoObj.bodyRowEnd =  rowInfoVector[j].rowno;
										tableInfoObj.itemIds.push_back(rowInfoVector[j].itemId);
										
									}
								}
							}
						}
						else
						{	// this means first row is removed we are getting directly item row.
							tableInfoObj.headerRowStart = -1;
							tableInfoObj.headerRowEnd = -1;
							tableInfoObj.bodyRowStart = rowInfoVector[i].rowno;

							PMString ASD("tableInfoObj.bodyRowStart : ");
							ASD.AppendNumber(tableInfoObj.bodyRowStart);
							//CA(ASD);

							tableInfoObj.itemIds.push_back(rowInfoVector[i].itemId);
							if(i < (rowInfoVector.size()-1))
							{							
								for(int32 j = i+1; j < rowInfoVector.size(); j++)
								{
									if(rowInfoVector[i].Data != rowInfoVector[j].Data)
									{
										tableInfoObj.bodyRowEnd = rowInfoVector[j-1].rowno;
										break;
									}
									else
									{
										tableInfoObj.bodyRowEnd =  rowInfoVector[j].rowno;
										tableInfoObj.itemIds.push_back(rowInfoVector[j].itemId);
									}
								}
							}
						}

						
						TableinfoVector.push_back(tableInfoObj);

						continue;
					//}			
					
				}

				PMString ASD1("TableinfoVector size : ");
				ASD1.AppendNumber((int32)TableinfoVector.size());
				//CA(ASD1);

				for(int32 k=0; k< TableinfoVector.size(); k++)
				{
					PMString Table("Table Detils \n ");

					Table.Append("tableId : ");
					Table.AppendNumber(TableinfoVector[k].tableId);
					if(TableinfoVector[k].tableId == -1)
					{
						//CA("if(TableinfoVector[k].tableId == -1)");
						VectorLongIntPtr longIntPtrObj = NULL; // ptrIAppFramework->GetProjectProducts_getTableIDsByObjectIdItemIdStringAndSeqOrderstring(TableinfoVector[k].parentId, &(TableinfoVector[k].itemIds));
						if(longIntPtrObj != NULL)
						{
							if(longIntPtrObj->size() >0)
								//CA("Size >0");
								TableinfoVector[k].tableId = longIntPtrObj->at(0);
						}

						Table.Append("\n new tableId : ");
						Table.AppendNumber(TableinfoVector[k].tableId);
					}
					Table.Append("\n parentId : ");
					Table.AppendNumber(TableinfoVector[k].parentId);
					Table.Append("\n headerRowStart : ");
					Table.AppendNumber(TableinfoVector[k].headerRowStart);
					Table.Append("\n headerRowEnd : ");
					Table.AppendNumber(TableinfoVector[k].headerRowEnd);
					Table.Append("\n bodyRowStart : ");
					Table.AppendNumber(TableinfoVector[k].bodyRowStart);
					Table.Append("\n bodyRowEnd : ");
					Table.AppendNumber(TableinfoVector[k].bodyRowEnd);
					Table.Append("\n tableid : ");
					Table.AppendNumber(TableinfoVector[k].tableId);

					for(int32 p=0; p < TableinfoVector[k].itemIds.size();p++)
					{
						Table.Append("\n ItemID : ");
						Table.AppendNumber(TableinfoVector[k].itemIds[p]);
					}
					Table.Append("\n" + TableinfoVector[k].FirstRowData);

					//CA(Table);
				}

				if(slugInfo.whichTab != 3)
				{
					//CA("ALL list not for Product. Can not Refresh");
					return kFalse;
				}
				//CA("slugInfo.whichTab == 3");
				if(ptrIAppFramework->get_isONEsourceMode())
				{
					// For ONEsource mode
					//CA("123 For ONESOURCE");
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(productID);
				}else
				{
					//For publication
					//CA("234 FOR PUBLICATION");
					tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID, productID, languageID);
				}

				if(tableInfo->size()==0)
				{ 
					//CA(" tableInfo->size()==0");						
					break;//breaks the for..tableIndex
				}

				// now we are iterating loop in reverse order to refresh lower table first.
				
				int32 tempTableinfoVectorSize = static_cast<int32>(TableinfoVector.size());
				int32 count=0;
				if(iscontentbuttonselected==kFalse)//added by sagar only working for structure not for content because we want to modify values not structure 
				{
					for(int32 k=tempTableinfoVectorSize; k>=  0; k--)
					{
						count++;
						for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						{		
							oTableValue = *it;	
							vec_items = oTableValue.getItemIds();
							//ItemIDVecfromCM = oTableValue.getItemIds();
							for(int32 j=0;j<vec_items.size();j++)
							{
								ItemIDVecfromCM.push_back(vec_items.at(j));
							}
							
							if(oTableValue.getTableID() != TableinfoVector[k].tableId)
							{
								oTableValue.getItemIds();
								//CA("Continue");
								continue;
							}

							

							int32 TotalNumberOfrowsBeforeRefresh = static_cast<int32>(TableinfoVector[k].itemIds.size());
							

							if(vec_items.size() == TableinfoVector[k].itemIds.size())
							{
								//CA("rowsize is same");
							}
							
							else if(vec_items.size() > TableinfoVector[k].itemIds.size())
							{
								//CA("need to add new items rows");

								int32 NoOfRowsToBeAdded = static_cast<int32>(vec_items.size()) - TotalNumberOfrowsBeforeRefresh;
								int32 rowPatternRepeatCount = (NoOfRowsToBeAdded / RowCountForRepetaionPattern);

								result = tableCommands->InsertRows(RowRange(TableinfoVector[k].bodyRowEnd, NoOfRowsToBeAdded), Tables::eAfter, 0);
								
								GridArea gridAreaForEntireTable;
								gridAreaForEntireTable.topRow = TableinfoVector[k].bodyRowStart;
								gridAreaForEntireTable.leftCol = 0;
								gridAreaForEntireTable.bottomRow = TableinfoVector[k].bodyRowStart + 1;
								gridAreaForEntireTable.rightCol = rowInfoVector[TableinfoVector[k].bodyRowStart].numberOfColoumns + 1;

								bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
								if(canCopy == kFalse)
								{
									//CA("canCopy == kFalse");
									ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
									break;
								}
								//CA("canCopy");					
								TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
								if(tempPtr == NULL)
								{
									ptrIAppFramework->LogDebug("AP46_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
									break;
								}
								
								for(int32 pasteIndex = 0;pasteIndex < rowPatternRepeatCount;pasteIndex++)
								{
									/*PMString ASD("GridAddress : ");
									ASD.AppendNumber((pasteIndex * RowCountForRepetaionPattern) + (totalNumberOfBodyRowsBeforeRefresh));
									ASD.Append("  totalNumberOfBodyRowsBeforeRefresh : ");
									ASD.AppendNumber(totalNumberOfBodyRowsBeforeRefresh);
									ASD.Append("  RowCountForRepetaionPattern : ");
									ASD.AppendNumber(RowCountForRepetaionPattern);
									ASD.Append("  pasteIndex : ");
									ASD.AppendNumber(pasteIndex);
									CA(ASD);*/
									tableModel->Paste(GridAddress((pasteIndex * RowCountForRepetaionPattern) + (TableinfoVector[k].bodyRowEnd+1),0),ITableModel::eAll,tempPtr);
									tempPtr = tableModel->Copy(gridAreaForEntireTable);
								}


							}
							else if(vec_items.size() < TableinfoVector[k].itemIds.size())
							{
								//CA("need to remove the items");
								int32 NoOfRowsToBeDeleted = static_cast<int32>(TableinfoVector[k].itemIds.size()) - static_cast<int32>(vec_items.size());
								result = tableCommands->DeleteRows(RowRange(TableinfoVector[k].bodyRowStart + static_cast<int32>(vec_items.size()) , NoOfRowsToBeDeleted));
							}
							
							int32 ColIndex = -1;
							//int32 totalBodyCell1 = 0;
							GridArea bodyArea_new1 = tableModel->GetBodyArea();
							ITableModel::const_iterator iterTable4(tableModel->begin(bodyArea_new1));
							ITableModel::const_iterator endTable4(tableModel->end(bodyArea_new1));

							int32 totalNoOfBodyRowsinNewTable = static_cast<int32>(vec_items.size());
							int32 startRowIndex = TableinfoVector[k].bodyRowStart;
							int32 endRowIndex = TableinfoVector[k].bodyRowStart -1 + totalNoOfBodyRowsinNewTable;
							int32 bodyCellsForItem = rowInfoVector[TableinfoVector[k].bodyRowStart].numberOfColoumns +1;

							while (iterTable4 != endTable4)
							{
								//CA("while (iterTable4 != endTable4)");
								GridAddress gridAddress = (*iterTable4);   
								
								if((gridAddress.row < startRowIndex) || (gridAddress.row > endRowIndex))
								{
									//CA("before continue");
									++iterTable4;
									continue;
								}

								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
								if(cellContent == nil) 
								{
									break;
								}
								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
								if(!cellXMLReferenceData) {
									break;
								}
								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
								++iterTable4;

								ColIndex++;
								int32 itemIndex = ColIndex/bodyCellsForItem;
							
					
								//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
								//We have considered that there is ONE and ONLY ONE text tag inside a cell.
								//Also note that, cell may be blank i.e doesn't contain any textTag.
								if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
								{
									continue;
								}
								//PMString s("cellXMLElementPtr->GetChildCount() : ");
								//s.AppendNumber(cellXMLElementPtr->GetChildCount());
								//CA(s);
								for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
								{
									XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);										
									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLElementRef.Instantiate();
									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
							
									//Check if the cell is blank.
									if(cellTextXMLElementPtr == NULL)
									{
										continue;
									}
									PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));

									if(index == "3")
									{
										
										continue;
									}
									else if(index == "4")
									{
										//CA("index == 4");
										PMString ItemID("");
										ItemID.AppendNumber(PMReal(vec_items[itemIndex]));//(p);

										PMString s("ItemID : ");
										s.AppendNumber(vec_items[itemIndex]);
										//s.Append(" ItemID :" + ItemID);
										//CA(s);
										//cellTextXMLElementPtr->SetAttributeValue(WideString("childId"), WideString(ItemID));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("childId"), WideString(ItemID));
									}
								} 
							} 
						}
					//	break;//by amarjit patil     //og commented by sagar it needs to iterate for multiple table to add and delete new item rows
						
					}
				}
				
				
				//CA("aftreeeeeeeeeeeeeeee");
				
			//	for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
			//	{//start for tagIndex = 0
				int32 count11=0,z=-1;
				GridArea headerArea = tableModel->GetHeaderArea();
				GridArea tableArea_new = tableModel->GetTotalArea();
				ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
				ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));

				//CA("A");
				int32 count111=0;
				while (iterTable2 != endTable2)
				{
					
					//totalBodyCell++;
					
					GridAddress gridAddress = (*iterTable2);         
					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
					if(cellContent == nil) 
					{
						//CA("cellContent == nil");
						break;
					}
					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
					if(!cellXMLReferenceData) {
						//CA("!cellXMLReferenceData");
						break;
					}
					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
					++iterTable2;

					//XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);

					//This is a tag attached to the cell.
					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}
					int32 NoofChilds = cellXMLElementPtr->GetChildCount();
					PMString String("");
					for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
					{ // iterate thruogh cell's tags
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());

						
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == nil)
						{
							continue;
						}
						//Get all the elementID and languageID from the cellTextXMLElement
						//Note ITagReader plugin methods are not used.
						PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
						PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
						PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
						PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
						PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
						PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
						PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
						PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));

						//added by Tushar on 26/12/06
						PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
						//check whether the tag specifies ProductCopyAttributes.
						PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
                 		double elementID = strElementID.GetAsDouble();
						double languageID = strLanguageID.GetAsDouble();

						PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
						PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
						PMString strChildTag = cellTextXMLElementPtr->GetAttributeValue(WideString("childTag"));

						double ColNO = strColNo.GetAsDouble();
						double RowNo = strRowNo.GetAsDouble();
						double typeID = strTypeID.GetAsDouble();
						double sectionid = strSectionId.GetAsDouble();

						double parentId = strParentID.GetAsDouble();

						double childId = strChildID.GetAsDouble();
						int32 header = strHeader.GetAsNumber();
						double tableID = strTableID.GetAsDouble();
						int32 childTag = strChildTag.GetAsNumber();

						if(childId!=count11 && childId!=-1)
						{
							z++;	
						}

						count11=childId;
						bool16 isHeaderCell = headerArea.Contains(gridAddress);

						//if(sequenceNo != -2 && isHeaderCell == kFalse)  // for Header Elements
						//	typeID	= FinalItemIds[sequenceNo];
						//else if(isHeaderCell == kTrue)
						//	typeID = strProductID.GetAsNumber();
						
						/*PMString NewTypeId("");
						NewTypeId.AppendNumber(typeID);
						cellTextXMLElementPtr->SetAttributeValue("typeId", NewTypeId);*/

						double rowno	=	strRowNo.GetAsDouble();
						//added by Tushar on 26/12/06
						int32 parentTypeID	= strParentTypeID.GetAsDouble();
						

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						//We will come across three types of tags.
						//1. ProductCopyAttributes tableFlag == 0
						//for all other attributes tableFlag == 1
						//2. TableHeader..typeId == -2
						//3. ItemCopyAttributes..typeId == ItemID.i.e typeId != -2
						PMString dispName("");
						if(tableFlag == "-15")
						{//This is a impotent tag.Don't process this.	
							//CA("tableFlag == -15");
							continue;
						}
						else if(tableFlag == "0")
						{	
							//The cell contains only copy attributes
							//added on 25Sept..EventPrice addition.

							//ended on 25Sept..EventPrice addtion.
							if(strIndex == "3")
							{//Product copy attribute
								if(strElementID == "999")
								{
									//CA("tagInfo.elementId == -121");							
															
									double objectId = strParentID.GetAsDouble(); 
									VectorScreenTableInfoPtr tableInfo = NULL;
									CItemTableValue oTableValue;	
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
									//CA("Not ONEsource");
									do
									{
										tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, languageID); //og

										if(!tableInfo)
										{
											//CA("!tableInfo");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
											break;
										}
										if(tableInfo->size()==0)
										{
											//CA(" tableInfo->size()==0");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
											break;
										}
										
										VectorScreenTableInfoValue::iterator it;
										bool16 typeidFound=kFalse;
										
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											//CA("for Loop");
											oTableValue = *it;
											if(oTableValue.getTableID() == tableID)
											{   
												//CA("typeidFound=kTrue;");
												typeidFound=kTrue;				
												break;
											}
										}
										
										if(typeidFound)
										{
											dispName = oTableValue.getName();
											
										}
										else
										{
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
											dispName = "";
										}
									}while(0);									
								}
								else
								{	
									dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(productID,elementID,languageID, sectionid,  kFalse);
								}
							}
							else if(strIndex == "4" && header == 1)
							{
									//	CA("strIndex == 4 && typeID == -2");
								//Item copy attribue header
								//following code added by Tushar on 27/12/06
								if(elementID == -701 || elementID == -702 /*|| elementID == -703 || elementID == -704*/)
								{
									//VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
									//if(TypeInfoVectorPtr != NULL)
									//{//CA("TypeInfoVectorPtr != NULL");
									//	VectorTypeInfoValue::iterator it3;
									//	int32 Type_id = -1;
									//	PMString temp = "";
									//	PMString name = "";
									//	for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									//	{
									//		Type_id = it3->getTypeId();
									//		if(parentTypeID == Type_id)
									//		{
									//			temp = it3->getName();
									//			if(elementID == -701)
									//			{
									//				dispName = temp;
									//			}
									//			else if(elementID == -702)
									//			{
									//				dispName = temp + " Suffix";
									//			}
									//		}
									//	}
									//}
									//if(TypeInfoVectorPtr)
									//	delete TypeInfoVectorPtr;
								}
								else if(elementID == -703)
								{
									//CA("dispName = $Off");
									dispName = "$Off";
								}
								else if(elementID == -704)
								{
									//CA("dispName = %Off");
									dispName = "%Off";
								}
								else if(elementID == -805)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -806)
								{									
									dispName = "Availablity";
                                }
								else if(elementID == -807)
								{									
									dispName = "Cross-reference Type";
                                }
								else if(elementID == -808)
								{									
									dispName = "Rating";
                                }
								else if(elementID == -809)
								{									
									dispName = "Alternate";
                                }
								else if(elementID == -810)
								{									
									dispName = "Comments";
                                }
								else if(elementID == -811)
								{									
									dispName = "";
                                }
								else if(elementID == -812)
								{									
									dispName = "Quantity";
                                }
								else if(elementID == -813)
								{									
									dispName = "Required";
                                }
								else if(elementID == -814)
								{									
									dispName = "Comments";
                                }
								else
									dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID, languageID );
							}
							else if(strIndex == "4")
							{
								//CA("TableID=0 ,Index =4");
								//Item copy attribute 
								if(elementID == -701 || elementID == -702 || elementID == -703 || elementID == -704)
								{
								//	CA("common functions 1219");
									PublicationNode pNode;
									pNode.setPubId(productID);

									PMString strPBObjectID = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
									pNode.setPBObjectID(strPBObjectID.GetAsDouble());
									if(indexValueAttachedToTable == "4")
									{
										
										pNode.setIsProduct(0);
									}
									else if(indexValueAttachedToTable == "3")
									{
										pNode.setIsProduct(1);
									}
									if(rowno != -1)
									{
										//CA("sETTING IsONEsource flag as false");
										pNode.setIsONEsource(kFalse);
									}
									TagStruct tagInfo;
									tagInfo.elementId = elementID;
									tagInfo.languageID = languageID;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = sectionID;
									tagInfo.tagPtr = cellTextXMLElementPtr;
									tagInfo.header = header;
									tagInfo.childId = childId;
									tagInfo.childTag = childTag;
									tagInfo.parentTypeID = parentTypeID;
									
									handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,dispName);
								}
								
								else if(elementID == -803)  // For 'Letter Key'
								{	
									//CA("here7");
									dispName.Clear();
									//int wholeNo =  sequenceNo / 26;
									//int remainder = sequenceNo % 26;								

									//if(wholeNo == 0)
									//{// will print 'a' or 'b'... 'z'  upto 26 item ids
									//	dispName.Append(AlphabetArray[remainder]);
									//}
									//else  if(wholeNo <= 26)
									//{// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
									//	dispName.Append(AlphabetArray[wholeNo-1]);	
									//	dispName.Append(AlphabetArray[remainder]);	
									//}
									//else if(wholeNo <= 52)
									//{// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
									//	dispName.Append(AlphabetArray[0]);
									//	dispName.Append(AlphabetArray[wholeNo -26 -1]);	
									//	dispName.Append(AlphabetArray[remainder]);										
									//}
								}
								else if((elementID == -805) && (isComponentAttributePresent == 1)) // For Component Table 'Quantity'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -806) && (isComponentAttributePresent == 1)) // For Component Table 'Availability'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -807) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
                                }
								else if((elementID == -808) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
                                }
								else if((elementID == -809) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][2]);	
                                }
								else if((elementID == -810) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][3]);	
                                }
								else if((elementID == -811) && (isComponentAttributePresent == 2))
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][4]);	
                                }
								else if((elementID == -812) && (isComponentAttributePresent == 3)) // For Accessary Table 'Quantity'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][0]);	
								}
								else if((elementID == -813) && (isComponentAttributePresent == 3))// For Accessary Table 'Required'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][1]);	
								}
								else if((elementID == -814) && (isComponentAttributePresent == 3))// For Accessary Table 'Comments'
								{
									//dispName = (Kit_vec_tablerows[sequenceNo][2]);	
								}
								else
								{
									SlugStruct stencileInfo;

									if(IsBookrefreshSelected==kTrue)
									{
										if(/*issrtucturebuttenselected==kTrue || */Booklevelstructurevariable == kTrue)
										{
											//CA("Book Structure");
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIDVecfromCM.at(z),elementID,languageID, sectionID, kTrue);
										}
										else if(/*iscontentbuttonselected==kTrue ||*/ Booklevelcontentvariable==kTrue)
										{
											//CA("Book Conent");
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID, sectionID, kTrue);
												PMString s("dispName : ");
												s.Append(dispName);
												s.Append("\n");
												s.Append("itemId : ");
												s.Append("\n");
												s.Append("ElementID : ");
												s.AppendNumber(elementID);
												s.AppendNumber(childId);
												//CA(s);
										}

									}
									else if(IsFrameRefreshSelected==kTrue)
									{

										ErrorCode err = kFailure;

									
										InterfacePtr<ITableUtility> iTableUtlObj
										((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
										if(!iTableUtlObj)
										{
											 //CA("!iTableUtlObj");
											ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
											//return ;
										}
											
										XMLReference txmlref;
										PMString TableTagNameNew = slugInfo.tagPtr->GetTagString();

										//temp_to_test  = oAttribVal.getDisplayName();

										if(issrtucturebuttenselected==kTrue /*|| Booklevelstructurevariable == kTrue*/)
										{
											//CA("Frame Structure");

											PMString tagName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,slugInfo.languageID );
											PMString TableColName("");
											//TableColName.Append("Model_Number");//Advance Table Cell Data
											//TableColName.Append("Model_Number");

											TableColName.Append("ATTRID_");
											TableColName.AppendNumber(PMReal(elementID));

											//TableColName = keepOnlyAlphaNumeric(TableColName);
											stencileInfo.TagName = TableColName;

											stencileInfo.typeId  = slugInfo.typeId;//-2;  // TypeId is hardcodded to -2 for Header Elements.
											stencileInfo.header  = -1;
											stencileInfo.parentId = slugInfo.parentId;  // -2 for Header Element
											stencileInfo.parentTypeId = slugInfo.parentTypeID;
											stencileInfo.whichTab = 4; // Item Attributes
											stencileInfo.imgFlag = slugInfo.sectionID; 
											stencileInfo.isAutoResize = slugInfo.isAutoResize;
											stencileInfo.LanguageID = slugInfo.languageID ;
											stencileInfo.pbObjectId = slugInfo.pbObjectId;
											stencileInfo.tableType = -1;
											stencileInfo.tableId = tableID;
											stencileInfo.row_no =RowNo;
											stencileInfo.col_no =ColNO;
											stencileInfo.elementId = elementID;
											//stencileInfo.tagStartPos=slugInfo.startIndex;
											//stencileInfo.tagEndPos = slugInfo.endIndex;
											stencileInfo.childId = ItemIDVecfromCM.at(z);
											stencileInfo.childTag=1;
											stencileInfo.elementName=tagName;
											

											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIDVecfromCM.at(z),elementID,languageID, sectionID, kTrue);

												PMString childValue("");
												childValue.AppendNumber(PMReal(ItemIDVecfromCM.at(z)));
												//cellTextXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(childValue));
												Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("childId"),WideString(childValue));
												//iTableUtlObj->setTableRowColData(tableUIDRef, dispName, stencileInfo.row_no,stencileInfo.col_no);

												//iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,slugInfo.tagPtr->GetTagString(), stencileInfo.elementName, txmlref, stencileInfo,stencileInfo.row_no, stencileInfo.col_no,tableID);
											//count111++;*/
										}
										else if(iscontentbuttonselected==kTrue/* || Booklevelcontentvariable==kTrue*/)
										{
											//CA("Frame Content");

											PMString tagName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,slugInfo.languageID );
										PMString TableColName("");
										//TableColName.Append("Model_Number");//Advance Table Cell Data
										
										
										//TableColName.Append("Model_Number");

										TableColName.Append("ATTRID_");
										TableColName.AppendNumber(PMReal(elementID));

										//TableColName = keepOnlyAlphaNumeric(TableColName);
										stencileInfo.TagName = TableColName;

										stencileInfo.typeId  = TypeIdvec.at(count111);//-2;  // TypeId is hardcodded to -2 for Header Elements.
										stencileInfo.header  = -1;
										stencileInfo.parentId = slugInfo.parentId;  // -2 for Header Element
										stencileInfo.parentTypeId = slugInfo.parentTypeID;
										stencileInfo.whichTab = 4; // Item Attributes
										stencileInfo.imgFlag = slugInfo.sectionID; 
										stencileInfo.isAutoResize = slugInfo.isAutoResize;
										stencileInfo.LanguageID = slugInfo.languageID ;
										stencileInfo.pbObjectId = slugInfo.pbObjectId;
										stencileInfo.tableType = -1;
										stencileInfo.tableId = tableID;
										stencileInfo.row_no =RowNo;
										stencileInfo.col_no =ColNO;
										stencileInfo.elementId = elementID;
										//stencileInfo.tagStartPos=slugInfo.startIndex;
										//stencileInfo.tagEndPos = slugInfo.endIndex;
										stencileInfo.childId = childId;
										stencileInfo.childTag=1;
										stencileInfo.elementName=tagName;
										PMString a("tagName : ");
											a.Append(tagName);
											//CA(a);


											//dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID,kTrue);
											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItmID.at(count111),attriID.at(count111),languageID, sectionID, kTrue);
											PMString s("ItemId :  : ");
												s.AppendNumber(ItmID.at(count111));
												s.Append("\n");
												s.Append("attribute :  : ");
												s.AppendNumber(attriID.at(count111));
												/*s.Append("\n");
												s.Append("row : ");
												s.AppendNumber(stencileInfo.row_no);*/
												//CA(s);

											
											
											/*iTableUtlObj->setTableRowColData(tableUIDRef, dispName, stencileInfo.row_no,stencileInfo.col_no);
											iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,slugInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, stencileInfo.row_no,stencileInfo.col_no,tableID );*/
											//
												count111++;
										}
									
									}
									
								}
							}
							
						}
						else if(tableFlag == "1")
						{
							//CA("tableFlag==1");//the cell contains the table
							if(strIndex == "3")
							{
								if(strElementID == "-103")
								{
									//Print the table header..table type
									do
									{
										//PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue("typeId");
	//CA(strTypeID);					//int32 typeId = strTypeID.GetAsNumber();
										
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}

										VectorTypeInfoValue::iterator it1;

										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
											if(typeID == it1->getTypeId())
											{
												typeIDFound = kTrue;
												break;
											}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse); 
								}
								else if(strElementID == "-104")
								{
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									tagInfo.parentId = strParentID.GetAsDouble();
									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);
								}
								else
								{
								  /*CA("Inside My code");*/
								  if(issrtucturebuttenselected== kTrue)
								  {
									VectorScreenTableInfoPtr tableInfo = NULL;
									CItemTableValue oTableValue;
									do
									{
										tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, parentId, languageID, kFalse);
										if(!tableInfo)
										{
											//CA("!tableInfo");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
											break;
										}
										if(tableInfo->size()==0)
										{
											//CA(" tableInfo->size()==0");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
											break;
										}
										VectorScreenTableInfoValue::iterator it;
										/*PMStringVec*/vector<PMString>::iterator strIt;
										bool16 typeidFound=kFalse;

										double tableId = 0;
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											//CA("for Loop");
											oTableValue = *it;
											double tttt=oTableValue.getTableID();
											

											if(oTableValue.getTableID() == tableID)
											{   
												//CA("typeidFound=kTrue;");
												typeidFound=kTrue;
												tableId = oTableValue.getTableID();
												/*PMString str1("tableID :");
												str1.AppendNumber(tableId);
												CA(str1);*/
												break;
											}
										}
										if(typeidFound)
										{
											//CA("typeidFound");
											//dispName = ptrIAppFramework->GetProduct_getItemByTableIDName(tableId);
											PMString e("dispName :");
											e.Append(dispName);
											//CA(e);
										}
										else
										{
											//CA("NOT FOUND");
											ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
										}
									}while(0);
								  }
								  if(iscontentbuttonselected ==kTrue)
								  {
									  //CA("selected content");
									  //InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
										//if(!itagReader)
										//{ 
										//	ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::!itagReader");		
										//	return kFalse;
										//}
									  VectorScreenTableInfoPtr tableInfo = NULL;
									  //TagList tList;
									  ////tList = itagReader->getTagsFromBox(boxUIDRef);

									if(parentId != -1)
										{
										//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(parentId);
										}
									if(!tableInfo)
									{
										//CA("if(!tableInfo)");
										ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
										return kFalse;
									}
									//CA("outside");

									CItemTableValue oTableValue;
									VectorScreenTableInfoValue::iterator it12;
									for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
									{	
										
										oTableValue = *it12;
										if(tableID == oTableValue.getTableID())
										{	
											//CA("123333");
											dispName = oTableValue.getName();
											//CA("tempBuffer = " + tempBuffer);
											break;
										}
									}
									if(tableInfo)
										delete tableInfo;
								  
								  }
								}

							}
							else if(strIndex == "4")
							{
								if(strElementID == "-103")
								{
									do
									{
										VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
										if(typeValObj==nil)
										{
											ptrIAppFramework->LogError("AP46_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::StructureCache_getListTableTypes's typeValObj == nil");
											break;
										}
										VectorTypeInfoValue::iterator it1;
										bool16 typeIDFound = kFalse;
										for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
										{	
												if(typeID == it1->getTypeId())
												{
													typeIDFound = kTrue;
													break;
												}			
										}
										if(typeIDFound)
											dispName = it1->getName();
										if(typeValObj)
											delete typeValObj;
									}while(kFalse);
								}
								else if(strElementID == "-104")
								{//Print the entire table
									//PMString strIndex = cellTextXMLElementPtr->GetAttributeValue("index");
									//PMString strParentID = cellTextXMLElementPtr->GetAttributeValue("parentID");
									//PMString strSectionID = cellTextXMLElementPtr->GetAttributeValue("sectionID");
									TagStruct tagInfo;												
									tagInfo.whichTab = strIndex.GetAsNumber();
									//This is just a temporary adjustment
									PMString pbObjectId = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
									tagInfo.parentId = pbObjectId.GetAsDouble();

									tagInfo.isTablePresent = kTrue;
									tagInfo.typeId = typeID;
									tagInfo.sectionID = strSectionID.GetAsDouble();

									GetItemTableInTabbedTextForm(tagInfo,dispName);

								}

							}
						}
						else if(tableFlag == "-13")
						{
							//the cell contains the product copy attribute header
							//CA("End.... ");
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
								if(result)
								dispName = cElementModelObj.getDisplayName();
						}
						
						PMString textToInsert("");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							textToInsert=dispName;					
						}
						else
							textToInsert=iConverter->translateString(dispName);
						//Spray the Header data .We don't have to change the tag attributes
						//as we have done it while copy and paste.
						//WideString insertData(textToInsert);
						//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
						ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);
					}
				}//end for tagIndex = 0 

			}// end of myflag sagar
				
			
	   }//end for..tableIndex
	   errcode = kSuccess;
	}while(0);
	return errcode;     
}

//till here


ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text)
{
	//CA("CDataSprayer::ReplaceText");
	ErrorCode status = kFailure;
	do {
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			//CA("iConverter");
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}

		ASSERT(textModel);
		if (!textModel) {
			//CA("!textModel");
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			//CA("position invalid");
			break;
		}
		if (length < 0 || length >= textModel->TotalLength()) {
			//CA("length invalid");
			break;
		}
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			//CA("!textModelCmds");
			break;
		}

		if(length == 0)
		{
			InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(position, text));
			ASSERT(insertTextCmd);		
			if (!insertTextCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(insertTextCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}

		}
		else
		{
			InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
			ASSERT(replaceCmd);		
			if (!replaceCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(replaceCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}
		}
		//CA("After Replace");
		if(iConverter)
		{
			//CA("true iConverter");
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		
	} while(false);
	CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	return status;
}





ErrorCode refreshAdvancedTableByCell(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
	//CA("refreshAdvancedTableByCell");
   
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;
	bool16 AddHeaderToTable = kFalse;
	VectorScreenTableInfoPtr ItemtableInfo = NULL;
	VectorScreenTableInfoPtr tableInfo= NULL;
    
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::Pointer to IAppFramework is nil.");
			break;//breaks the do{}while(kFalse)
		}
		
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne)
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::textFrameUID == kInvalidUID");
			break;//breaks the do{}while(kFalse)
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil)
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
        
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}
        
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}
        
		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::textModel == nil");
			break;//breaks the do{}while(kFalse)
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));
        
		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;//breaks the do{}while(kFalse)
		}
		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
        
		if(totalNumberOfTablesInsideTextFrame != 1) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;//breaks the do{}while(kFalse)
		}
        
        double tabelId = slugInfo.tableId;
        double parentId = slugInfo.parentId;
        double sectionId = slugInfo.sectionID;
        double languageId = slugInfo.languageID;
        int32 index = slugInfo.whichTab;
        bool16 isProduct = kFalse;
        if(index == 3)
            isProduct = kTrue;
        else if(index == 4)
            isProduct = kFalse;
        
        
        InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::!ptrIClientOptions");
			return errcode;
		}
        
		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		if(imagePath!="")
		{
			const char *imageP=imagePath.GetPlatformString().c_str();
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
            #ifdef MACINTOSH
                imagePath+="/";
            #else
                imagePath+="\\";
            #endif
		}
		
		if(imagePath=="")
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::imagePath is blank");
			return errcode;
		}
        
        VectorAdvanceTableScreenValuePtr tableInfo = NULL;
        
        tableInfo =ptrIAppFramework->getHybridTableData(sectionId,parentId,languageId,isProduct, kFalse);
        if(!tableInfo )
		{
			
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::!tableInfo");
			return errcode;
		}
        if( tableInfo->size() ==0 )
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::tableInfo->size == 0");
			return errcode;
		}
        
        CAdvanceTableScreenValue oAdvanceTableScreenValue;
        VectorAdvanceTableScreenValue::iterator it;
        bool16 typeidFound = kFalse;
        
        for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
        {
            oAdvanceTableScreenValue = *it;
            
            if(oAdvanceTableScreenValue.getTableId() == tabelId)
            {
                typeidFound=kTrue;
                break;
            }
        }
        
        if(typeidFound != kTrue)
		{
            ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell::typeidFound != kTrue");
			return errcode;
		}
        
        vector<CAdvanceTableCellValue > vec_CAdvanceTableCellValue = oAdvanceTableScreenValue.getCells();
        vector<CAdvanceTableCellValue>::iterator it1;
        
        
		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		{//for..tableIndex
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
			if(tableModel == nil)
				continue;//continues the for..tableIndex
            
			int32 totalNumberColumns = tableModel->GetTotalCols().count;
			
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell:: Err: invalid interface pointer ITableCommands");
				continue;//continues the for..tableIndex
			}
			
			UIDRef tableRef(::GetUIDRef(tableModel));
            
			int32 tableRef12 =tableRef.GetUID().Get();
            
            IIDXMLElement* & frameXMLElementPtr = slugInfo.tagPtr;
            if(frameXMLElementPtr->GetChildCount() != 1)
            {
                ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell:: More than one child tags present.");
				continue;
                
            }
            
            XMLReference tableXMLElementRef =   frameXMLElementPtr->GetNthChild(0);
            InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLElementRef.Instantiate());
            
            if(tableXMLElementPtr==nil || tableXMLElementPtr->GetChildCount()<= 0)
            {
                ptrIAppFramework->LogError("if(tableXMLElementPtr==nil || tableXMLElementPtr->GetChildCount()<= 0)");
                continue;
            }
        
			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
			
			PMString strTableID = tableXMLElementPtr->GetAttributeValue(WideString("tableId"));
			double tableID = strTableID.GetAsDouble();
			if(tableID < -1){
				
				ptrIAppFramework->LogError("Required tableId of Advance table missing.");
				errcode = kFailure;
				return errcode;
			}
            
            for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
            {//start for tagIndex = 0
						
						XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
						//This is a tag attached to the cell.
						
						InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
						
						//Get the text tag attached to the text inside the cell.
						//We are providing only one texttag inside cell.
						if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
						{
							//CA("if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)");
							continue;
						}
                        
						for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
						{ // iterate thruogh cell's tags
							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
							
							InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
							//The cell may be blank. i.e doesn't have any text tag inside it.
							if(cellTextXMLElementPtr == nil)
							{
								//CA("if(cellTextXMLElementPtr == nil)");
								continue;
							}

                            PMString cellIdStr = cellTextXMLElementPtr->GetAttributeValue(WideString("field3"));
                            PMString imgFlagStr = cellTextXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
                        
                            //CA("CellId: " + cellIdStr);
                            //CA("imgFalg: " + imgFlagStr);
                 			
							int32 imgFlag = imgFlagStr.GetAsNumber();
                            double cellId1 = cellIdStr.GetAsDouble();
                    
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;
                            
                            CAdvanceTableCellValue cAdvCellObj;
                            bool16 cellIdFound = kFalse;
                            
                            for(it1 = vec_CAdvanceTableCellValue.begin(); it1 != vec_CAdvanceTableCellValue.end(); it1++)
                            {
                                cAdvCellObj = *it1;
                                if(cAdvCellObj.cellId == cellId1)
                                {
                                    cellIdFound = kTrue;
                                    break;
                                }
                                
                            }
                            
                            if(!cellIdFound)
                            {
                                ptrIAppFramework->LogError("AP7_RefreshContent::CommonFunctions::refreshAdvancedTableByCell:: Skipping Cell Refresh as cellId missing in table");
                                continue;
                            }
                            

							PMString dispName("");
                            
                            if(imgFlag == 1)
                            {
                                XMLContentReference cellContentRef = cellTextXMLElementPtr->GetContentReference();
                                UIDRef cellContentFrameRef = cellContentRef.GetUIDRef();
                                
                                PMString fileName = cAdvCellObj.getValue();
                                
                                if(fileName=="")
                                    continue;
                                
                                do
                                {
                                    SDKUtilities::Replace(fileName,"%20"," ");
                                }while(fileName.IndexOfString("%20") != -1);
                                
                                //CA(fileName);
                                PMString imagePathWithSubdir = imagePath;
                                
                                #ifdef WINDOWS
                                    SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
                                #endif
                                
                                
                                InterfacePtr<IDataSprayer> iDataSprayer((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
                                if(!iDataSprayer)
                                {
                                    //CA("Pointre to DataSprayerPtr not found");//
                                    return kFalse;
                                }
                                
                                if((iDataSprayer->fileExists(imagePathWithSubdir,fileName)))
                                {
                                    PMString total=imagePathWithSubdir+fileName;
                                    
                                    #ifdef MACINTOSH
                                    SDKUtilities::convertToMacPath(total);
                                    #endif
                                    
                                    if(iDataSprayer->ImportFileInFrame(cellContentFrameRef, total))
                                    {		
                                        iDataSprayer->fitImageInBox(cellContentFrameRef);
                                    }
                                }
                                
                                if(cAdvCellObj.getAssetTypeId() != -1)
                                {
                                    PMString NewId("");
                                    NewId.AppendNumber(PMReal(cAdvCellObj.getAssetTypeId()));
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("typeId"), WideString(NewId));
                                }
                                
                            }
                            else
                            {
                                dispName = cAdvCellObj.getValue();
							
							
                                PMString textToInsert("");
                                InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
                                if(!iConverter)
                                {
                                    textToInsert=dispName;
                                }
                                else{
                                    textToInsert=iConverter->translateString(dispName);
                                    iConverter->ChangeQutationMarkONOFFState(kFalse);
                                }
                                textToInsert.ParseForEmbeddedCharacters();
                                boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                                ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);
                            
                                if(iConverter)
                                {
                                    iConverter->ChangeQutationMarkONOFFState(kTrue);
                                }
                                
                                if(cAdvCellObj.getFieldId() != -1)
                                {
                                    PMString NewId("");
                                    NewId.AppendNumber(PMReal(cAdvCellObj.getFieldId()));
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("ID"), WideString(NewId));
                                }
                                
                                
                            }
                            if(cAdvCellObj.getItemId() != -1)
                            {
                                PMString NewId("");
                                NewId.AppendNumber(PMReal(cAdvCellObj.getItemId()));
                                Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("parentID"), WideString(NewId));
                                
                                Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLRef, WideString("childId"), WideString(NewId));
                            }

						}
					}//end for tagIndex = 0
            
        }//end for..tableIndex
        errcode = kSuccess;
	}while(0);
	return errcode;     
}

ErrorCode RefreshAttributeGroupInTabbedText(const UIDRef& boxID,TagStruct& tagInfo)
{
    //CA("inside RefreshAttributeGrroup");
    ErrorCode result = kFailure;
    do
    {
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            break;
        }
        
        vector<double> FinalAttributeIds;
        bool16 FinalAttrributeIdSizeIsZero = kFalse;
        
        IIDXMLElement *attributeGroupTextTagPtr = tagInfo.tagPtr;
        
        PMString strDataType =  attributeGroupTextTagPtr->GetAttributeValue(WideString("dataType"));
        PMString strParentID =  attributeGroupTextTagPtr->GetAttributeValue(WideString("parentID")); //Cs4
        PMString strLanguageID =  attributeGroupTextTagPtr->GetAttributeValue(WideString("LanguageID")); //Cs4
        PMString strSectionID = attributeGroupTextTagPtr->GetAttributeValue(WideString("sectionID"));//Cs4
        PMString strGrElementID = attributeGroupTextTagPtr->GetAttributeValue(WideString("ID"));
        PMString strGroupKey = attributeGroupTextTagPtr->GetAttributeValue(WideString("groupKey"));
        //-------------
        double field1 = -1;
        double elmentId1 = -1;
        double ParentId1 = -1;
        double ChildId1 = -1;
        //===================================Pivot List(We Dont want to refresh Pivot List for Structure option)=================================
        InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
        if(!itagReader)
        {
            ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshAttributeGroupInTabbedText::!itagReader");
            //return ;
        }
        
        bool16 IsPivotList=kFalse;
        TagList tList,tList_checkForHybrid;
        tList =  itagReader->getTagsFromBox(boxID);
        for(int32 t=0;t<tList.size();t++)
        {
            if(tList[t].dataType==6 && (tList[t].whichTab==4) && tList[t].elementId == tagInfo.elementId )
            {
                int32 childTagCount = tList[t].tagPtr->GetChildCount();
                PMString l("1st :");
                l.AppendNumber(childTagCount);
                //CA(l);
                
                
                VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr1 = NULL;
                VectorHtmlTrackerValue vectorObj1 ;
                vectorHtmlTrackerSPTBPtr1 = &vectorObj1;
                
                
                for(int32 tagIndex = 0;tagIndex < childTagCount;tagIndex++)
                {
                    XMLReference cellXMLElementRef = tList[t].tagPtr->GetNthChild(tagIndex);
                    //This is a tag attached to the cell.
                    //IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
                    InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
                    //Get the text tag attached to the text inside the cell.
                    //We are providing only one texttag inside cell.
                    if(cellXMLElementPtr==nil )
                    {
                        //CA("cellXMLElementPtr==nil ");
                        continue;
                    }
                    PMString strfield1 = cellXMLElementPtr->GetAttributeValue(WideString("field1"));
                    field1 =  strfield1.GetAsNumber();
                    
                    PMString strTypeID = cellXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
                    PMString strSectionID = cellXMLElementPtr->GetAttributeValue(WideString("sectionID"));
                    PMString strElementID = cellXMLElementPtr->GetAttributeValue(WideString("ID"));
                    PMString strLanguageID = cellXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
                    PMString strParentTypeID = cellXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
                    PMString strIndex = cellXMLElementPtr->GetAttributeValue(WideString("index"));
                    PMString strParentId = cellXMLElementPtr->GetAttributeValue(WideString("parentID"));
                    PMString strHeader = cellXMLElementPtr->GetAttributeValue(WideString("header"));
                    PMString strchildId = cellXMLElementPtr->GetAttributeValue(WideString("childId"));
                    PMString strchildTag = cellXMLElementPtr->GetAttributeValue(WideString("childTag"));
                    
                    int32 header = strHeader.GetAsNumber();
                    double childId = strchildId.GetAsDouble();
                    int32 childTag = strchildTag.GetAsNumber();
                    
                    double typeId = strTypeID.GetAsDouble();
                    double sectionID = strSectionID.GetAsDouble();
                    double elementID = strElementID.GetAsDouble();
                    double languageID = strLanguageID.GetAsDouble();
                    double parentTypeID = strParentTypeID.GetAsDouble();
                    int32 index = strIndex.GetAsNumber();
                    double parrentId = strParentId.GetAsDouble();
                    
                    PMString itemAttributeValue("");
                    if(typeId==-57 || iscontentbuttonselected == kTrue) // Refreshing table by Content or Pivote List.
                    {
                        IsPivotList=kTrue;
                        
                        if(header == 1)
                        {
                                itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
                        }
                        else
                        {
                            
                            itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parrentId,elementID, languageID, sectionID, kFalse );
                            
                        }
                        
                        int32 tagStartPos = -1;
                        int32 tagEndPos = -1;
                        Utils<IXMLUtils>()->GetElementIndices(cellXMLElementPtr,&tagStartPos,&tagEndPos);
                        
                        InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElementPtr->QueryContentTextStoryThread());
                        if(textStoryThread == NULL)
                        {
                            //CA("textStoryThread == NULL");
                            break;
                        }
                        InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
                        if(textModelptr == NULL)
                        {
                            //CA("textModel == NULL");
                            break;
                        }
                        
                        tagStartPos = tagStartPos + 1;
                        tagEndPos = tagEndPos -1;
                        
                        PMString textToInsert("");
                        InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
                        if(!iConverter)
                        {
                            //CA("24");
                            textToInsert=itemAttributeValue;
                        }
                        else
                        {
                            //CA("25");
                            //textToInsert=iConverter->translateString(itemAttributeValue);
                            vectorHtmlTrackerSPTBPtr1->clear();
                            textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr1);
                            iConverter->ChangeQutationMarkONOFFState(kFalse);
                        }
                        //WideString insertText(textToInsert);
                        //textModelptr->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
                        //Apsiva 9 comment
                        textToInsert.ParseForEmbeddedCharacters();
                        boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                        ReplaceText(textModelptr,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);
                        if(iConverter)
                        {
                            //CA("26");
                            iConverter->ChangeQutationMarkONOFFState(kTrue);
                        }
                    }
                    
                }
            }
        }
        
        
        //==============================End===============================================================
        if(IsPivotList==kFalse)  //  now refreshing tabbed text table by Structure option
        {
            //CA("A");
            int32 ChildCount=attributeGroupTextTagPtr->GetChildCount();
            
            for(int32 tagIndex = 0;tagIndex < attributeGroupTextTagPtr->GetChildCount();tagIndex++)
            {
                XMLReference cellXMLElementRef =   attributeGroupTextTagPtr->GetNthChild(tagIndex);
                //This is a tag attached to the cell.
                //IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
                InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
                //Get the text tag attached to the text inside the cell.
                //We are providing only one texttag inside cell.
                if(cellXMLElementPtr==nil )
                {
                    //CA("cellXMLElementPtr==nil ");
                    continue;
                }
                
                PMString strId1 = cellXMLElementPtr->GetAttributeValue(WideString("ID"));
                elmentId1 =  strId1.GetAsDouble();
                
                PMString strParentId1 = cellXMLElementPtr->GetAttributeValue(WideString("parentID"));
                ParentId1 =  strParentId1.GetAsDouble();
                
            }
            
            //CA("B");
            double itemID_or_productID = strParentID.GetAsDouble();
            double languageId = strLanguageID.GetAsDouble();
            double sectionID = strSectionID.GetAsDouble();
            
            bool16 isSectionLevelItemItemPresent = kFalse;
            int32 isComponentAttributePresent = 0;
            vector<vector<PMString> > Kit_vec_tablerows;  //To store Component's 'tableData' so that we can use
            
            ////get all itemIDs of section level item or product.............
            
            do{
                if(attributeGroupTextTagPtr->GetAttributeValue(WideString("index")) == WideString("4") )//Cs4
                {
                    bool16 isRefreshCall = kFalse; //kTrue;
                //    if(!(tagInfo.groupKey).empty())
                //    {
                //        ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupKey");
                //        FinalAttributeIds= ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(tagInfo.groupKey, tagInfo.parentId);
                //    }
                //    else
                    {
                        ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupId");
                        FinalAttributeIds= ptrIAppFramework->getItemAttributeListforAttributeGroupIdAndItemId(sectionID, tagInfo.parentId, tagInfo.elementId, languageId);
                    }
                    
                }
                

            }while(false);
            
            
            if(FinalAttributeIds.size()<=0)
            {
                FinalAttrributeIdSizeIsZero = kTrue;
                ptrIAppFramework->LogInfo("AP7_RefreshContent::CommonFunctions::RefreshAttributeGroupInTabbedText::FinalAttributeIds.size()<=0");
                break;
            }
            
            int32 finalItemAttrIDsize =static_cast<int32> (FinalAttributeIds.size());
            
            
            ////count number of newline characters present in current box
            int32 newLineCharacters = 0;
            
            UID textFrameUID = kInvalidUID;
            InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
            if (graphicFrameDataOne)
            {
                textFrameUID = graphicFrameDataOne->GetTextContentUID();
            }
            if (textFrameUID == kInvalidUID)
            {
                //CA("textFrameUID == kInvalidUID");
                break;
            }
            
            InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
            if (graphicFrameHierarchy == nil)
            {
                //CA("graphicFrameHierarchy is NULL");
                break;
            }
            
            InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
            if (!multiColumnItemHierarchy) {
                //CA("multiColumnItemHierarchy is NULL");
                break;
            }
            
            InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
            if (!multiColumnItemTextFrame) {
                //CA("multiColumnItemTextFrame is NULL");
                break;
            }
            InterfacePtr<IHierarchy>
            frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
            if (!frameItemHierarchy) {
                //CA("frameItemHierarchy is NULL");
                break;
            }
            
            InterfacePtr<ITextFrameColumn>
            frameItemTFC(frameItemHierarchy, UseDefaultIID());
            if (!frameItemTFC) {
                //CA("!!!ITextFrameColumn");
                break;
            }
            InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
            if (textModel == NULL)
            {
                //CA("textModel == NULL");
                break;
            }
            
            TextIndex startPos=0, endPos=0;
            Utils<IXMLUtils>()->GetElementIndices(attributeGroupTextTagPtr, &startPos, &endPos);
            
            
            TextIterator begin(textModel , startPos + 1);
            TextIterator end(textModel , endPos - 1);
            
            for(TextIterator iter = begin ;iter <= end ; iter++)
            {
                if(*iter == kTextChar_CR)
                {
                    newLineCharacters++;
                }
            }
            
            bool16 onlyOneLinePresent = kFalse; ///if only one line is present in text frame...
            if(newLineCharacters == 0)
            {
                onlyOneLinePresent = kTrue;
            }
            
            /////////////////////
            
            /////check whether number of item attributes currently present in box is same as FinalAttributeIds size
            ///if it is equal then there is no need to add or delete rows from box in case of headerPresent in box...
            ///otherwise there is need to add or delete rows from the box.....
            
            bool16 isNeedToDeleteRows = kFalse;
            bool16 isNeedToAddRows = kFalse;
            int32 noOfLinesToBeAddedOrDeleted = 0;
            
            
            if(finalItemAttrIDsize != newLineCharacters + 1)
            {
                if(finalItemAttrIDsize <= newLineCharacters)
                {
                    isNeedToDeleteRows = kTrue;
                    if(FinalAttrributeIdSizeIsZero == kTrue)
                        noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemAttrIDsize;
                    else
                        noOfLinesToBeAddedOrDeleted = newLineCharacters - finalItemAttrIDsize + 1;
                }
                else
                {
                    isNeedToAddRows = kTrue;
                    noOfLinesToBeAddedOrDeleted = finalItemAttrIDsize - newLineCharacters - 1;
                }
            }
            
            
            ////add no of lines
            if(!onlyOneLinePresent && isNeedToAddRows)////if more than one lines are already present in text Frame
                //and we want to add more in that
            {
                //CA("!onlyOneLinePresent && isNeedToAddRows");
                int32 startIndexForCopy = startPos + 1/*0*/;
                
                int32 j = 0;
                for(TextIterator iter = begin ;iter <= end ; iter++)
                {
                    if(*iter == kTextChar_CR)
                    {
                        j++;
                        if(j == newLineCharacters)
                        {
                            startIndexForCopy = iter.Position();
                            break;
                        }
                    }
                }
                
                int32 length = endPos - startIndexForCopy;
                //int32 length = endPos - startIndexForCopy;
                /*K2*/boost::shared_ptr< PasteData > pasteData;	//----CS5---
                
                for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
                {
                    //PMString StartIndexStr("startIndexForCopy");
                    //StartIndexStr.AppendNumber(startIndexForCopy);
                    //StartIndexStr.Append(" , length = ");
                    //StartIndexStr.AppendNumber(length);
                    //StartIndexStr.Append(" , endPos = ");
                    //StartIndexStr.AppendNumber(endPos);
                    //CA(StartIndexStr);
                    textModel->CopyRange(startIndexForCopy,length,pasteData);
                    textModel->Paste(/*kFalse,*/endPos /*-1*/,pasteData);
                    endPos = endPos + length;
                }
            }
            
            if(onlyOneLinePresent && isNeedToAddRows)
            {
                //CA("onlyOneLinePresent && isNeedToAddRows");
                WideString* newLineCharacter = new WideString("\r");
                
                //int32 startIndexForCopy = 0;
                
                int32 startIndexForCopy = startPos + 1/*1*/;
                //int32 length = endPos -1 -startIndexForCopy;
                int32 length = endPos - startIndexForCopy;
                
                
                /*K2*/boost::shared_ptr< PasteData > pasteData;	//---CS5--
                InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
                if(iConverter)
                {
                    iConverter->ChangeQutationMarkONOFFState(kFalse);
                }
                PMString a("noOfLinesToBeAddedOrDeleted : ");
                a.AppendNumber(noOfLinesToBeAddedOrDeleted);
                //CA(a);
                for(int i = 0; i < noOfLinesToBeAddedOrDeleted ; i++)
                {
                    textModel->CopyRange(startIndexForCopy,length,pasteData);
                    textModel->Insert(endPos ,newLineCharacter);
                    textModel->Paste(endPos+1,pasteData);
                    endPos = endPos+ 1 + length;
                }
                if(iConverter)
                {
                    iConverter->ChangeQutationMarkONOFFState(kTrue);
                }
                if(newLineCharacter)
                    delete newLineCharacter;
            }
            
            /////Delete no of lines
            if(!onlyOneLinePresent && isNeedToDeleteRows)
            {
                //CA("!onlyOneLinePresent && isNeedToDeleteRows");
            
                int32 startIndexForDelete = 0;
                
                int32 j = 0;
                for(TextIterator iter = begin ;iter <= end ; iter++)
                {
                    if(*iter == kTextChar_CR)
                    {
                        if(FinalAttrributeIdSizeIsZero)
                        {
                            startIndexForDelete = iter.Position();
                            break;
                        }
                        
                        j++;
                        if(j == finalItemAttrIDsize)
                        {
                            startIndexForDelete = iter.Position();
                            break;
                        }
                    }
                }
                
                
                //startIndexForDelete -= 1;
                int32 length = endPos - startIndexForDelete;
                
                PMString StartIndexStr("startIndexForDelete");
                StartIndexStr.AppendNumber(startIndexForDelete);
                StartIndexStr.Append(" , length = ");
                StartIndexStr.AppendNumber(length);
                
                //CA(StartIndexStr);
                
                textModel->Delete(startIndexForDelete,length);
                
            }
            
            //CA("Successfully added or deleted line in text frame");
            
            ////////if FinalItemIdSizeIsZero then make all child tags of customTabbedText Impotent
            if(FinalAttrributeIdSizeIsZero)
            {
                IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
                int32 newChildCount = customTabbedTextXMLElement->GetChildCount();
                
                for(int32 i = 0; i < newChildCount; i++)
                {
                    //ptrIAppFramework->clearAllStaticObjects();
                    XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(i);
                    //IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
                    InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
                    
                    int32 tagStart = -1;
                    int32 tagEnd = -1;
                    Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStart,&tagEnd);
                    tagStart = tagStart +1;
                    tagEnd = tagEnd - 1;
                    
                    InterfacePtr<ITextStoryThread> textStoryThread(childXMLElement->QueryContentTextStoryThread());
                    if(textStoryThread == NULL)
                    {
                        //CA("textStoryThread == NULL");
                        break;
                    }
                    InterfacePtr<ITextModel> textModelptr(textStoryThread->QueryTextModel());
                    if(textModelptr == NULL)
                    {
                        //CA("textModel == NULL");
                        break;
                    }
                    
                    PMString str("");
                    //WideString data(str);
                    //textModelptr->Replace(tagStart,tagEnd-tagStart + 1,&data);
                    boost::shared_ptr<WideString> insertText(new WideString(str));
                    ReplaceText(textModelptr,tagStart,tagEnd-tagStart + 1 ,insertText);
                }
                break;
            }

            
            ////////change attributeIds of all tags as per the FinalItemIds
            IIDXMLElement* customTabbedTextXMLElement = tagInfo.tagPtr;
            int32 newChildCount = customTabbedTextXMLElement->GetChildCount();
            
            int32 noOfTagsPresentPerRow = 0;
            
            if(isNeedToDeleteRows)
                noOfTagsPresentPerRow = newChildCount / (newLineCharacters - noOfLinesToBeAddedOrDeleted + 1);
            else
                noOfTagsPresentPerRow = newChildCount / (newLineCharacters + noOfLinesToBeAddedOrDeleted + 1);
            
            
            int32 index = 0;
            
            vector<double> ::iterator itr;
            itr = FinalAttributeIds.begin();
            
            
            for(int i = 0; i < FinalAttributeIds.size(); i++ ,itr++)
            {
                PMString attrributeId("");
                attrributeId.AppendNumber(PMReal(*itr));
                for(int j = 0 ; j < noOfTagsPresentPerRow ; j++)
                {
                    
                    XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(index);
                    InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString("ID"),WideString(attrributeId));
                    index++;
                }		
            }
            
            ///////////////////////
            
            ////////now we can refresh each tags 
            VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
            VectorHtmlTrackerValue vectorObj ;
            vectorHtmlTrackerSPTBPtr = &vectorObj;
            
            int32 j = 0;//temp variable
            int32 sequenceNo = 0; //index of itemID present in FinalItemIds.
            PMString ss("newChildCount-- : ");
            ss.AppendNumber(newChildCount);
            //CA(ss);
            
            for(int32 i = 0; i < newChildCount; i++)
            {
                //ptrIAppFramework->clearAllStaticObjects();
                XMLReference childXMLReference = customTabbedTextXMLElement->GetNthChild(i);
                //IIDXMLElement* childXMLElement = childXMLReference.Instantiate();
                InterfacePtr<IIDXMLElement>childXMLElement(childXMLReference.Instantiate());
                
                PMString strTypeID = childXMLElement->GetAttributeValue(WideString("typeId")); //Cs4
                PMString strSectionID = childXMLElement->GetAttributeValue(WideString("sectionID"));
                PMString strElementID = childXMLElement->GetAttributeValue(WideString("ID"));
                PMString strLanguageID = childXMLElement->GetAttributeValue(WideString("LanguageID"));
                PMString strParentTypeID = childXMLElement->GetAttributeValue(WideString("parentTypeID"));
                //PMString strIndex = childXMLElement->GetAttributeValue("index");
                PMString strIndex = customTabbedTextXMLElement->GetAttributeValue(WideString("index"));
                
                PMString strHeader = childXMLElement->GetAttributeValue(WideString("header"));
                PMString strchildId = childXMLElement->GetAttributeValue(WideString("childId"));
                PMString strchildTag = childXMLElement->GetAttributeValue(WideString("childTag"));//parentID
                PMString strParentTag = childXMLElement->GetAttributeValue(WideString("parentID"));
                
                int32 header = strHeader.GetAsNumber();
                double childId = strchildId.GetAsDouble();
                int32 childTag = strchildTag.GetAsNumber();
                double parentId = strParentTag.GetAsDouble();
                
                double typeId = strTypeID.GetAsDouble();
                double sectionID = strSectionID.GetAsDouble();
                double elementID = strElementID.GetAsDouble();
                double languageID = strLanguageID.GetAsDouble();
                double parentTypeID = strParentTypeID.GetAsDouble();
                int32 index = strIndex.GetAsNumber();

                
                PMString itemAttributeValue("");
                
                if(header == 1)
                {
                    itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID );
                }
                else
                {
                    if(j < noOfTagsPresentPerRow)///itemID for no of tags present in a single row is same. 
                    {
                        j++;
                    }
                    else
                    {
                        sequenceNo++;
                        j = 1;
                    }
                    
                    itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parentId,elementID, languageID, sectionID, kFalse );
                    
                }
                int32 tagStartPos = -1;
                int32 tagEndPos = -1;
                Utils<IXMLUtils>()->GetElementIndices(childXMLElement,&tagStartPos,&tagEndPos);
                tagStartPos = tagStartPos + 1;
                tagEndPos = tagEndPos -1;
                
                PMString textToInsert("");
                InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
                if(!iConverter)
                {
                    textToInsert=itemAttributeValue;					
                }
                else						
                {
                    //textToInsert=iConverter->translateString(itemAttributeValue);
                    vectorHtmlTrackerSPTBPtr->clear();
                    textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
                    iConverter->ChangeQutationMarkONOFFState(kFalse);
                }	
                //WideString insertText(textToInsert);
                //textModel->Replace(tagStartPos,tagEndPos-tagStartPos +1 ,&insertText);
                textToInsert.ParseForEmbeddedCharacters();
                boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);
                
                if(iConverter)
                {
                    iConverter->ChangeQutationMarkONOFFState(kTrue);			
                }
            }
        }
        
        //leaks found here
        for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
        {
            tList[tagIndex].tagPtr->Release();
        }
        result = kSuccess;
    }while(kFalse);
    return result;
}
