#ifndef __GLOBALDATA_H__
#define __GLOBALDATA_H__

#include "PublicationNode.h"
#include "RefreshData.h"
#include "FilterData.h"
#include "FrameRecord.h" 


PublicationNodeList pNodeDataList; 
FilterDataList samitvflist;
double CurrentSelectedSection;
bool8 FilterFlag=kFalse;
int32 NewFlag=0;


RefreshDataList rDataList; 
RefreshDataList OriginalrDataList;
RefreshDataList DocProductrDataList;
RefreshDataList rBookDataList;
RefreshDataList OriginalrBookDataList;

bool16 ISRfreshBookDlgOpen = kFalse;

vecFrameRecord frameRecordList;
vecUniqueItemOnDoc uniqueItemOnDocList;
vecNewItemIDS newItemIDList;

//end

#endif