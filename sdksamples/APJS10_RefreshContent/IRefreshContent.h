#ifndef __IRefreshContent__
#define __IRefreshContent__

#include "VCPluginHeaders.h"
#include "RfhID.h"

class IRefreshContent: public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IREFRESHCONTENT };
	
	virtual bool16 refreshThisBox(UIDRef&, PMString&)=0;
	virtual void generateRefreshReport(int32 doRefreshCasetype)=0; 
};

#endif

// Way get the interface pointer for IRefreshContent 	
// add two header file 1. RfhID.h & 2. IRefreshContent.h
// InterfacePtr<IRefreshContent> RefreshContentPtr((IRefreshContent*)::CreateObject(kRefreshContentBoss, IID_IREFRESHCONTENT));