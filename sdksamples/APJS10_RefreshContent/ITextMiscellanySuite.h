#ifndef _ITextMiscellanySuite_
#define _ITextMiscellanySuite_

// Interface includes:
#include "IPMUnknown.h"
#include "SPID.h"
//#include "ISpecifier.h"


class ITextMiscellanySuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ITEXTMISCELLANYSUITEE };
public:
//	virtual bool16 GetCurrentSpecifier(ISpecifier * & )=0;
	virtual bool16 GetUidList(UIDList &)=0;
	virtual bool16 GetFrameUIDRef(UIDRef &)=0;
	virtual bool16 GetCaretPosition(TextIndex &pos)=0;
};
#endif // _ITextMiscellanySuite_
