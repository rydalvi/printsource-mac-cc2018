#include "VCPlugInHeaders.h"

#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)

#include "CEventHandler.h"
#include "IEvent.h"
//#include "LPTLibraryService.h"
#include "IApplication.h"
#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "ILibraryPanelData.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "FileUtils.h"
#include "RfhID.h"
#include "IControlView.h"
#include "ILibrary.h"
#include "LibraryPanelID.h"
#include "ILibraryPanelData.h"
#include "ILibrary.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"
//For List box of Library Panel///////////////
#include "ILibraryItemButtonData.h"
#include "ILibraryAssetProxy.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
//////////////////////////////////////////////
//#include "LPTLibraryService.h"
//#include "LibraryListBoxObserver.h"
//#include "TableStyleUtils.h"

#include "AcquireModalCursor.h"
#include "IDragDropSource.h"
#include "IDragDropController.h"
#include "EventUtilities.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "IPalettePanelUtils.h"
#include "MediatorClass.h"

#include "ILayoutUtils.h"
#include "IDocument.h"

//extern SysFile systemFile;
//extern PMString fileName;

class MyEventHandler : public CEventHandler
{
	public:
		bool16 isOurEvent;
		InterfacePtr<IListBoxController>listBoxController;
		MyEventHandler(IPMUnknown *boss):CEventHandler(boss){}
		virtual ~MyEventHandler(){}
		// ...
		/*virtual bool16 ButtonDblClk(IEvent *e);
		virtual bool16 LButtonDn(IEvent *e);
		virtual bool16 LButtonUp(IEvent *e);
		virtual bool16 MouseMove(IEvent *e);*/
		virtual bool16 Activate (IEvent *e);

		//PMString getStyleName();
		
		private:
		//virtual bool16 IsOurEvent(IEvent *e, SysFile myLibraryFile);
		
		// ...

		
};
CREATE_PMINTERFACE(MyEventHandler, kMyEventHandlerImpl)
 bool16 MyEventHandler::Activate (IEvent *e)
{
	CA("Window is Deactivated");
	
	InterfacePtr<IDocument>fDoc((const IPMUnknown *)Utils<ILayoutUtils>()->GetFrontDocument(),UseDefaultIID());
	if(!fDoc)
		return kFalse;
	else
	{
		fDoc->AddRef();
		InterfacePtr<IDropDownListController> iDropDownListController(Mediator::dropdownCtrlView, UseDefaultIID());
		if (iDropDownListController == nil)
		{
			//CA("DropDownList controller is null");
			return kFalse;
		}
		int32 selectedIndex=iDropDownListController->GetSelected();
		iDropDownListController->Select(0);
		iDropDownListController->Select(selectedIndex);
		iDropDownListController->Select(selectedIndex);
		
	}
	
	return kFalse;
}
//bool16 MyEventHandler::MouseMove(IEvent *e)
//{
//	
//	return kTrue;
//}
//bool16 MyEventHandler::LButtonUp(IEvent *e)
//{
//	CA("Left Button Up");
//	return kTrue;
//}
//PMString MyEventHandler::getStyleName()
//{
//	PMString styleName("");
//	//CA("We got TRUE for my Event");
//	InterfacePtr<IPanelControlData>pnlData(LPTLibraryService::gLibControlView,UseDefaultIID());
//	if(!pnlData)
//	{
//		CA("Panel Control Data for Library is null. Can't proceed further.");
//		return styleName;
//	}
//	
//	InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemListBoxWidgetId),UseDefaultIID());
//	if(!icView)
//	{
//		CA("Some error while getting ControlView for ListBox. Can't proceed further.");
//	}
//	else
//	{
//		//CA("We got widget control view");
//		icView->AddRef();
//	}
//	InterfacePtr<IListBoxController>lstBoxController;
//	if(icView->IsVisible())
//	{
//		InterfacePtr<IListBoxController>templstBoxController(icView,UseDefaultIID());
//		lstBoxController=templstBoxController;
//	}
//	else
//	{
//		InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemGridWidgetId),UseDefaultIID());
//		if(!icView)
//		{
//			CA("Some error while getting ControlView of ListGrid. Can't proceed further");
//			return PMString(""); 
//		}
//		InterfacePtr<IListBoxController>templstBoxController(icView,UseDefaultIID());
//		lstBoxController=templstBoxController;
//	}
//	//InterfacePtr<IListBoxController>lstBoxController(icView,UseDefaultIID());
//	if(!lstBoxController)
//	{
//		CA("Some error in getting ListBoxController. Can't proceed further.");
//		return PMString("");
//	}
//
//	//CA("We got Library List Box Controller");
//	PMString selIndex("Selected Index: ");
//	listBoxController=lstBoxController;
//	int32 index=lstBoxController->GetSelected();
//	InterfacePtr<IPanelControlData>pnlCtrlData(icView,UseDefaultIID());
//	if(!pnlCtrlData)
//	{
//		CA("Error in getting Panel Control Data. Can't proceed further.");
//		return PMString("");
//
//	}
//	else
//	{
//		//CA("Got panel ctrl data for library list box");
//		InterfacePtr<IControlView>piSelectedItemView(pnlCtrlData->GetWidget(index), UseDefaultIID());
//		if(!piSelectedItemView)
//		{
//			CA("Selected item view is null");
//		}
//		else
//		{
//			//CA("We get selected item view");
//			InterfacePtr<ILibraryItemButtonData>piLibItemButtonData(piSelectedItemView, IID_ILIBRARYITEMBUTTONDATA);
//			if(!piLibItemButtonData)
//			{
//				CA("LibItemButton data is null");
//				return kFalse;
//			}
//			ILibraryAssetProxy *piLibAssetProxy =piLibItemButtonData->GetLibraryAssetProxy();
//			if(!piLibAssetProxy)
//			{
//				CA("Lib Asset proxy is null");
//				return kFalse;
//			}
//			styleName.Append(piLibAssetProxy->GetName());
//			PMString msg("Selected name: ");
//			msg.Append(styleName);
//			//CA(msg);
//		}
//
//	}
//	return styleName;
//}
//bool16 MyEventHandler::LButtonDn (IEvent *e)
//{
//	SysFile myLibraryFile=systemFile;
//	InterfacePtr<IDragDropSource> piDragSource(this, UseDefaultIID());
//
//		if(piDragSource && piDragSource->WillDrag(e))
//		{
//			bool16 bPatientUser;
//			if(::IsUserStartingDrag(e->GetSysEvent(), bPatientUser))
//			{
//			// Yes, positively there's a drag
//				InterfacePtr<IDragDropController>piDDController(gSession, UseDefaultIID());
//				if(!piDDController)
//				{
//					CA("IDragDropController is null in LButtonDN");
//					return kFalse;
//				}
//				
//				if(piDDController->StartDrag(piDragSource, e,bPatientUser))
//				{	
//					//CA("In if of Start Drag");
//					e->SetSystemHandledState(IEvent::kDontCall);
//					//CA("Returning True");
//					return kTrue;
//				}
//				//CA("Returning true as user start dragging");
//			}
//			
//		}
//		else
//			CA("No Drag");
//	bool16 myEvent=this->IsOurEvent(e,myLibraryFile);
//	if(myEvent)
//	{
//		//CA("It is our event");
//		
//	}
//	else
//		CA("it is NOT our event");
//	//CA("LButtonDn is over");
//	return kFalse;
//}
//bool16 MyEventHandler::ButtonDblClk (IEvent *e)
//{
//	 /*Check if the event occurred in our lib panel's window
//	  (see method below); myLibraryFile is a SysFile*/
//	SysFile myLibraryFile=systemFile;
//	bool16 myEvent=this->IsOurEvent(e, myLibraryFile);
//	if(myEvent == kFalse)
//	{
//		//CA("we have not handled our event");
//		isOurEvent=kFalse;	
//		return kFalse;
//	}
//	else
//	{
//
//		AcquireWaitCursor acqWaitCursor;
//		acqWaitCursor.Animate();
//		PMString styleName=getStyleName();
//		TableStyleUtils tblStyleUtils;
//		tblStyleUtils.ApplyStyleToSelectedFrame(styleName);
//	}
//	/*else
//	{
//		//CA("We got TRUE for my Event");
//		//InterfacePtr<IPanelControlData>pnlData(LPTLibraryService::gLibControlView,UseDefaultIID());
//		//if(!pnlData)
//		//{
//		//	CA("Library Panel Controll Data is null");
//		//}
//		//else
//		//	CA("We got Panel Contoll Data");
//		//
//		////InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemGridWidgetId),UseDefaultIID());
//		//InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemListBoxWidgetId),UseDefaultIID());
//		//if(!icView)
//		//{
//		//	CA("Controllview by finding widget for ListView is null");
//		//}
//		//else
//		//{
//		//	CA("We got widget control view");
//		//	icView->AddRef();
//		//}
//		//InterfacePtr<IListBoxController>lstBoxController;
//		//if(icView->IsVisible())
//		//{
//		//	InterfacePtr<IListBoxController>templstBoxController(icView,UseDefaultIID());
//		//	lstBoxController=templstBoxController;
//		//}
//		//else
//		//{
//		//	InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemGridWidgetId),UseDefaultIID());
//		//	if(!icView)
//		//	{
//		//		CA("GriedView is null");
//		//	}
//		//	InterfacePtr<IListBoxController>templstBoxController(icView,UseDefaultIID());
//		//	lstBoxController=templstBoxController;
//		//}
//		////InterfacePtr<IListBoxController>lstBoxController(icView,UseDefaultIID());
//		//if(!lstBoxController)
//		//{
//		//	CA("ListBoxController is null");
//		//}
//
//		//CA("We got Library List Box Controller");
//		//PMString selIndex("Selected Index: ");
//		//listBoxController=lstBoxController;
//		//int32 index=lstBoxController->GetSelected();
//		//InterfacePtr<IPanelControlData>pnlCtrlData(icView,UseDefaultIID());
//		//if(!pnlCtrlData)
//		//{
//		//	CA("panel control data for library list box is null");
//
//		//}
//		//else
//		//{
//		//	CA("Got panel ctrl data for library list box");
//		//	InterfacePtr<IControlView>piSelectedItemView(pnlCtrlData->GetWidget(index), UseDefaultIID());
//		//	if(!piSelectedItemView)
//		//	{
//		//		CA("Selected item view is null");
//		//	}
//		//	else
//		//	{
//		//		CA("We get selected item view");
//		//		InterfacePtr<ILibraryItemButtonData>piLibItemButtonData(piSelectedItemView, IID_ILIBRARYITEMBUTTONDATA);
//		//		if(!piLibItemButtonData)
//		//		{
//		//			CA("LibItemButton data is null");
//		//			return kFalse;
//		//		}
//		//		ILibraryAssetProxy *piLibAssetProxy =piLibItemButtonData->GetLibraryAssetProxy();
//		//		if(!piLibAssetProxy)
//		//		{
//		//			CA("Lib Asset proxy is null");
//		//			return kFalse;
//		//		}
//		//		PMString name = piLibAssetProxy->GetName();
//		//		PMString msg("Selected name: ");
//		//		msg.Append(name);
//		//		CA(msg);
//		//	}
//
//		//}
//	
//	}
//*/
//	// Yes, the event occurred in our panel's palette
//	//  Now, proceed to handle the event in our own way
//
//	// ...
//	
//	//CA("Finaly we have handled event");
//	
//	//isOurEvent=kTrue;
//	return kTrue; // Since we have handled the event
//}
//
//
//bool16 MyEventHandler::IsOurEvent(IEvent *e, SysFile myLibraryFile)
//{
//	// myLibraryFile is the library file which we have already opened
//
//	// Traverse to the window of the library panel
//	InterfacePtr<IApplication> piApp(gSession->QueryApplication());
//	if(!piApp)
//	{
//		CA("IApplication is null");
//		return kFalse;
//	}
//	InterfacePtr<IPaletteMgr>piPaletteMgr(piApp->QueryPaletteManager());
//	if(!piPaletteMgr)
//	{
//		CA("Some error in getting PaletteManager. Can't proceed further");
//		return kFalse;
//	}
//	InterfacePtr<IPanelMgr> piPanelMgr(piPaletteMgr, UseDefaultIID());
//	if(!piPanelMgr)
//	{
//		CA("Some error in getting Panel Manager.Can't proceed further");
//		return kFalse;
//	}
//	IControlView* piLibPanelCtrlView = nil;
//	WidgetID first = kFirstLibraryPanelWidgetId;
//	WidgetID last = kLibraryPanelPrefix + 255;
//	// Now we have got to our panel, get to its palette's window
//	for (; first <= last; first++)
//	{
//		piLibPanelCtrlView = piPanelMgr->GetVisiblePanel(first);
//		if(piLibPanelCtrlView == nil)
//		{	
//			//CA("IControl View is null");
//			continue;
//		}
//		//CA("We got library panel control view");
//		//InterfacePtr<ILibraryPanelData> piData(view,UseDefaultIID());
//		InterfacePtr<ILibraryPanelData> piData(piLibPanelCtrlView,UseDefaultIID());
//		if (piData == nil)
//		continue;
//		//CA("we get Library Panel Data");
//		ILibrary *piLibrary = piData->GetLibrary();
//		
//		if(piLibrary == nil)
//			continue;
//		
//		if(fileName==piLibrary->GetFileName())
//		{
//			//CA("Breadking loop");
//			break;
//		}
//		//else
//			//CA("Continuing Loop");
//		//CA(piLibrary->GetLibraryFileSpec().GetTrueName());
//		//if(FileUtils::IsEqual(myLibraryFile,myLibraryFile /*piLibrary->GetLibraryFileSpec()*/))
//		/*if(FileUtils::IsEqual(systemFile,piLibrary->GetLibraryFileSpec()))
//		{
//			CA("Breaking the loop");	
//			break;
//		}*/
//	}
//
//	if(!piLibPanelCtrlView )
//	{	
//		CA("Can't get Control View. Can't proceed further");
//		return kFalse;
//	}
//	
//	/*else
//	{
//		CA("ControlView is not null");
//		InterfacePtr<IControlView>ctrlView(piLibPanelCtrlView);
//		if(!ctrlView)
//		{
//			CA("For Library Palette view is null");
//
//		}
//		else
//		{
//			
//			ctrlView->AddRef();
//			//LPTLibraryService::gLibControlView=ctrlView;
//			//LPTLibraryService::gLibControlView->AddRef();
//			CA("Control view for list box is not null");
//			InterfacePtr<IPanelControlData>pnlData(ctrlView,UseDefaultIID());
//			if(!pnlData)
//			{
//				CA("Library Panel Controll Data is null");
//			}
//			else
//				CA("We got Panel Contoll Data");
//			
//			//InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemGridWidgetId),UseDefaultIID());
//			InterfacePtr<IControlView>icView(pnlData->FindWidget(kLibraryItemListBoxWidgetId),UseDefaultIID());
//			if(!icView)
//			{
//				CA("Controllview by finding widget is null");
//			}
//			else
//			{
//				CA("We got widget control view");
//				icView->AddRef();
//			}
//			InterfacePtr<IListBoxController>lstBoxController(icView,UseDefaultIID());
//			if(!lstBoxController)
//			{
//				CA("ListBoxController is null");
//			}
//			else
//			{
//				CA("We got Library List Box Controller");
//				PMString selIndex("Selected Index: ");
//				listBoxController=lstBoxController;
//				int32 index=lstBoxController->GetSelected();
//				InterfacePtr<IPanelControlData>pnlCtrlData(icView,UseDefaultIID());
//				if(!pnlCtrlData)
//				{
//					CA("panel control data for library list box is null");
//
//				}
//				else
//				{
//					CA("Got panel ctrl data for library list box");
//					InterfacePtr<IControlView>piSelectedItemView(pnlCtrlData->GetWidget(index), UseDefaultIID());
//					if(!piSelectedItemView)
//					{
//						CA("Selected item view is null");
//					}
//					else
//					{
//						CA("We get selected item view");
//						InterfacePtr<ILibraryItemButtonData>piLibItemButtonData(piSelectedItemView, IID_ILIBRARYITEMBUTTONDATA);
//						if(!piLibItemButtonData)
//						{
//							CA("LibItemButton data is null");
//							return kFalse;
//						}
//						ILibraryAssetProxy *piLibAssetProxy =piLibItemButtonData->GetLibraryAssetProxy();
//						if(!piLibAssetProxy)
//						{
//							CA("Lib Asset proxy is null");
//							return kFalse;
//						}
//						PMString name = piLibAssetProxy->GetName();
//						PMString msg("Selected name: ");
//						msg.Append(name);
//						CA(msg);
//					}
//
//				}
//				selIndex.AppendNumber(index);
//				selIndex.Append("   ");
//				selIndex.Append(lstBoxController->GetNthItemString(index));
//
//				CA(selIndex);
//			}
//		}
//	}*/
//	InterfacePtr<IWidgetParent> piLibPanelParent(piLibPanelCtrlView,UseDefaultIID());
//	if(!piLibPanelParent)
//	{
//		CA("Some error while getting Widget Parent. Can't proceed further.");
//		return kFalse;
//	}
//	
//	InterfacePtr<IWindow> piPaletteWindow((IWindow *)piLibPanelParent->QueryParentFor(IID_IWINDOW));
//	//InterfacePtr<IWindow> piPaletteWindow((IWindow *)piLibPanelParent->QueryParent());
//	if(!piPaletteWindow)
//	{
//		CA("Some error in getting LibraryPalette window. Can't proceed further.");
//		return kFalse;
//	}
//	// See if its the same window in which this event occurred
//	/*if(!LPTLibraryService::lbObserver)
//		CA("Invalid Observer Pointer");
//	else
//		CA("Valid Observer Pointer");
//	*/
//	if(piPaletteWindow->GetSysWindow() == e->GetSysWindow())
//	{
//		//CA("Returning true bcoz we get Window");
//		InterfacePtr<IControlView>ctrlView(piLibPanelCtrlView);
//		if(!ctrlView)
//		{
//			CA("Some error in getting LibraryPalette view. Can't proceed further.");
//			return kFalse;
//		}
//		else
//		{
//			
//			ctrlView->AddRef();
//			LPTLibraryService::gLibControlView=ctrlView;
//			LPTLibraryService::gLibControlView->AddRef();
//		}
//		return kTrue;
//	}
//	else
//	{
//		//CA("We didnt get window and returning false");
//		return kFalse;
//	}
//}
//
//
