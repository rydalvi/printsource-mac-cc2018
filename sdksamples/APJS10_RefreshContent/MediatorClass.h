#ifndef __MEDIATOR_CLASS_H__
#define __MEDIATOR_CLASS_H__

#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"

class Mediator
{
private:
	~Mediator();//Cannot instantiate this class
public:
	static int16 selectedRadioButton;
	static int32 refreshPageNumber;
	static IDialog * dialogPtr;
	static IControlView* listControlView;
	static IControlView* editBoxView;
	static IControlView* ChkHighlightView;
	static bool16 isGroupByObj;

	static IPanelControlData* iPanelCntrlDataPtr;
	static bool16	loadData;
	static IControlView* dropdownCtrlView;
	static IPanelControlData* iPanelCntrlDataPtrTemp;
	static IControlView* RfhRefreshButtonView;
	static IControlView* RfhLocateButtonView;
	static ITextControlData * EditBoxTextControlData;
	static IControlView* BooklistControlView;
	static IControlView* RfhDlgRefreshButtonView;
	static IControlView* RfhDlgLocateButtonView;
	static IControlView* RfhDlgHiliteChkBoxView;
	static IControlView* RfhDlgDeletChkBoxView;

	//Added on 30May by Yogesh Joshi
	static IControlView* selectAllCheckBoxView;
	//end addition

	//Added by vijay on 5 OCT 2006
	static IControlView* RfhReloadButtonView;
	//***Added By Sachin Sharma :9/07/08
	static IPanelControlData* catalogRefrshPanelControlData;

	static bool16 isSilentModeSelected;
	static bool16 isFrameRefresh; 


	static void initMembers()
	{
		selectedRadioButton=-1;
		refreshPageNumber=-1;
		listControlView=nil;
		editBoxView=nil;
		ChkHighlightView=nil;
		isGroupByObj=kTrue;
		iPanelCntrlDataPtr=nil;
		dropdownCtrlView=nil;
		iPanelCntrlDataPtrTemp=nil;
		RfhRefreshButtonView=nil;
		RfhLocateButtonView=nil;
		BooklistControlView=nil;
		RfhDlgRefreshButtonView=nil;
		RfhDlgLocateButtonView= nil;
		RfhDlgDeletChkBoxView= nil;
		RfhDlgHiliteChkBoxView= nil;
		catalogRefrshPanelControlData=nil;
		isSilentModeSelected= kTrue;
		isFrameRefresh = kFalse;
		
		
	}
};

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);

#endif