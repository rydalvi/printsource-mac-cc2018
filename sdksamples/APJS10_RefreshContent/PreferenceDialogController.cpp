//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/PreferenceDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"

#include "MediatorClass.h"

// Project includes:
#include "RfhID.h"

#include "CAlert.h"
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("BscDlgController.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
//extern IDialog* dialog1;

/** Implements IDialogController based on the partial implementation CDialogController; 
	its methods allow for the initialization, validation, and application of dialog widget values.
  
	The methods take an additional parameter for 3.0, of type IActiveContext.
	See the design document for an explanation of the rationale CMMCustomIconWidgetEH this
	new parameter and the renaming of the methods that CDialogController supports.
	
	
	@ingroup basicdialog	

*/


bool16 IsDeleteFlagSelected = kFalse;
extern bool16 refreshTableByAttribute ;

class PreferenceDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PreferenceDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/**
			Initializes each widget in the dialog with its default value.
			Called when the dialog is opened.
			@param Context
		*/
		virtual void InitializeDialogFields( IActiveContext* dlgContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			this method to be called. When all widgets are valid, 
			ApplyFields will be called.		
			@param myContext
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
		*/
		virtual WidgetID ValidateDialogFields( IActiveContext* myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param myContext
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(PreferenceDialogController, kPreferenceDialogControllerImpl)

/* ApplyFields
*/
void PreferenceDialogController::InitializeDialogFields( IActiveContext* dlgContext) 
{
// Put code to initialize widget values here.
	//CA("in InitializeDialogFields");
	
	do
	{
		
		CDialogController::InitializeDialogFields(dlgContext);
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
		{
			CA("dlgController == nil");
			break;
		}
		if(IsDeleteFlagSelected)
			dlgController->SetTriStateControlData(kUpdateDocWithDeleteWidgetID, kTrue, nil, kTrue, kFalse);
		else
			dlgController->SetTriStateControlData(kUpdateDocWithDeleteWidgetID, kFalse, nil, kTrue, kFalse);

		if(refreshTableByAttribute)
			dlgController->SetTriStateControlData(kRefreshCellRadioButtonWidgetID, kTrue, nil, kTrue, kFalse);
		else
			dlgController->SetTriStateControlData(kRefreshFullTableRadioButtonWidgetID, kTrue, nil, kTrue, kFalse);
		
	
	}while(kFalse);
	//CA("dialog controller 2");

	// Put code to initialize widget values here.
}

WidgetID PreferenceDialogController::ValidateDialogFields( IActiveContext* myContext) 
{
	WidgetID result = kNoInvalidWidgets;


	// Put code to validate widget values here.


	return result;
}
void PreferenceDialogController::ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId) 
{
	//CA("in ApplyDialogFields");
	do{
		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			CA("dialogController == nil");
			break;
		}
		
		ITriStateControlData::TriState UpdateDocWithDeleteState = dialogController->GetTriStateControlData(kUpdateDocWithDeleteWidgetID);
		if(UpdateDocWithDeleteState == ITriStateControlData::kSelected)
			IsDeleteFlagSelected = kTrue;
		else
			IsDeleteFlagSelected = kFalse;

		ITriStateControlData::TriState RefreshCellsState = dialogController->GetTriStateControlData(kRefreshCellRadioButtonWidgetID);
		if(RefreshCellsState == ITriStateControlData::kSelected)
			refreshTableByAttribute = kTrue;
		else
			refreshTableByAttribute = kFalse;


	}while(kFalse);
}
