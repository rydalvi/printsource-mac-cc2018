//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/RFHTreeNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"



// General includes:
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "RFHID.h"

#include "ITreeViewController.h"
#include "IntNodeID.h"
#include "RfhDataNode.h"
#include "RFHTreeDataCache.h"

#include "CAlert.h"
//#include "IMessageServer.h"
//---------CS3 Addition-----------------//
#include "ITriStateControlData.h"
#include "MediatorClass.h"
//#include "K2Vector.h"
#include "K2Vector.tpp" 
#include "k2Vector.h"
#include <vector>
#include "IAppFramework.h"
#include "ITreeViewMgr.h"

#define FILENAME			PMString("RFHTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class RFHTreeNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	RFHTreeNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~RFHTreeNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e){ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonDblClk(e);  return retval; }
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( RFHTreeNodeEH, kRFHTrvNodeEHImpl)

	
RFHTreeNodeEH::RFHTreeNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}



bool16 RFHTreeNodeEH::LButtonDn(IEvent* e)
{
	//CA("LButton Down");
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_IRFHTRVSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("APJS9_RefreshContent::RFHTreeNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}



	

	
		InterfacePtr<ITreeViewController> treeViewCtrl(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPanelTreeViewWidgetID), UseDefaultIID());
		if(treeViewCtrl==nil)
		{
			//CA("treeViewMgr is NULL");
			return retval;
		}

		NodeIDList selectedItem;
		treeViewCtrl->GetSelectedItems(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			return retval;
		}
		
		NodeID nid=selectedItem[0];

		InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
		if(!treeViewMgr)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
			return retval;
		}
		//QueryWidgetFromNode 

		InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			return retval;
		}

		IControlView* checkBoxCntrlView=panelControlData->FindWidget(kRfhCheckBoxWidgetID);
	if(checkBoxCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("APJS9_RefreshContent::RFHTreeNodeEH::LButtonDn::checkBoxCntrlView == nil");
		return retval;
	}
	
		
	InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
	if(itristatecontroldata==nil)
	{
		ptrIAppFramework->LogDebug("APJS9_RefreshContent::RFHTreeNodeEH::LButtonDn::itristatecontroldata is nil");		
		return retval;
	}
	PMString ASD("");
	//itristatecontroldata5->Deselect();
	if(!itristatecontroldata->IsSelected())
		{
			ASD.Append("Not Selected ");
		}
	else
		{
			ASD.Append("Selected ");
		}
	//		itristatecontroldata->Select();
			

		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

		int32 uid= uidNodeIDTemp->Get();
		int32 TextFrameuid = uidNodeIDTemp->Get();
		RfhDataNode rNode;
		RFHTreeDataCache dc1;

		dc1.isExist(uid, rNode);
		
		PMString pfName = rNode.getFieldName();
		ASD.Append( pfName );

		CA(ASD);
	
	return retval;

}
//	end, File:	RFHTreeNodeEH.cpp
