#include "VCPluginHeaders.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "BoxReader.h"
#include "ITableCommands.h"
#include "IControlView.h"
#include "IPanelControlData.h"

#include "ILayoutUtils.h"//Cs4
//#include "ISelection.h"
#include "CAlert.h"


//#include "ISelection.h"
#include "ILayoutUIUtils.h" //Cs4
#include "UIDList.h"
#include "CAlert.h"
#include "IClientOptions.h"
#include "IPageList.h"
#include "IDocument.h"
#include "ISelectUtils.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "ITextAttrUtils.h"
#include "ISwatchList.h"
#include "IWorkspace.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextFocusManager.h"
#include "ITextFocus.h"
#include "IFrameList.h"
#include "IFrameUtils.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "MediatorClass.h"
#include "IHierarchy.h"
#include "ITextMiscellanySuite.h"
//#include "IImportFileCmdData.h"
#include "IOpenFileDialog.h"
#include "IImportProvider.h"
#include "IGraphicFrameData.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IReplaceCmdData.h"
#include "SDKUtilities.h"
#include "TransformUtils.h"
#include "TableUtility.h"
#include "ITextTarget.h"
#include "ISelectionUtils.h"
#include "TextEditorID.h"
#include "TextIterator.h"
#include "PMString.h"
#include "ITriStateControlData.h"
#include "RfhSelectionObserver.h"
#include "PageData.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
//#include "ITextFrame.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ITextWalker.h"
#include "ITextWalkerSelectionUtils.h"
#include "IScrapItem.h"
#include "IDataSprayer.h"
#include <string.h>
#include "IFrameUtils.h"
#include "IXMLElementCommands.h"
#include "IFrameContentUtils.h"
#include "ITableModelList.h"
#include "ITableUtility.h"
#include "ITableCommands.h"
#include "CTUnicodeTranslator.h"
#include "TableStyleUtils.h"
#include "ITagReader.h"
#include "ITextAttributeSuite.h"
#include "FileUtils.h"
#include "ITextFrameColumn.h"

//21Jun for ItemTableInTabbedText format
#include "ITextStoryThread.h"
#include "CommonFunctions.h"
//21Jun

//7Aug..ItemTable Handling
#include "ITableTextContent.h"
#include "ICellContent.h"
#include "IPageItemTypeUtils.h"

#include "Report.h"
#include "IURIUtils.h"
#include "IImportResourceCmdData.h"
#include "URI.h"
#include "SDKLayoutHelper.h"
#include "BookReportData.h"
#include "IBookManager.h"
#include "IBookUtils.h"
#include "ITableUtils.h"
#include "IBook.h"
#include "ThreadTextFrame.h"
#include "IGeometry.h"
#include "ITableSelectionSuite.h"
#include "IFontMgr.h"
#include "IPMFont.h"
#include "ILinkUtils.h"
#include "IUIDData.h"
#include "ILinkObjectReference.h"
#include "ILinkState.h"
#include "IDataLinkReference.h"
#include "IDataLink.h"
#include <IImportImageCmdData.h>
#include <IPMStream.h>
#include <StreamUtil.h>
#include "ITextTarget.h"
#include  "pageitemscrapid.h"
#include <IFrameType.h>
#include "IXMLUtils.h"
#include "IXMLReferenceData.h"
#include "ItemModel.h"
#include "string.h"
#include <set>
//#include "SectionDataCache.h"
//#include "GetSectionData.h"

//sagar
#include "ProgressBar.h"

bool16 	isInlineImage = kFalse;
/*
#define CA(z) CAlert::InformationAlert(z)

#define CAI(y,x)	{\
						PMString tempString(y);\
						tempString.Append(" : ");\
						tempString.AppendNumber(x);\
						CAlert::InformationAlert(tempString);\
					}
*/
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("Refresh.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
PMString  numToPMString(int32 num);

//#define CA(X) CAlert::InformationAlert \
//	( \
//	PMString("Refresh.cpp") + PMString("\n") + \
//  PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + PMString("\n") + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


//7Jun..Yogesh ItemTable Handling
#include "ITableGeometry.h"
	
	
					
extern RefreshDataList rDataList;
extern bool16 HiliteFlag;
extern RefreshDataList OriginalrDataList;

extern RefreshDataList rBookDataList;
extern RefreshDataList OriginalrBookDataList;
extern bool16 ISRfreshBookDlgOpen;


extern bool16 IsHiliteFlagSelected;
extern bool16 IsDeleteFlagSelected;

extern bool16 issrtucturebuttenselected ;//= kFalse;
extern bool16 iscontentbuttonselected ;//= kFalse;

extern int GroupFlag;

/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
//By Amarjit
vector<double>itemids;
vector<double>itemgroupids;
extern vector<double>item_attribute_ids;
extern vector<double>itemgroup_attributeids;
extern map<int32,int32>mapforpagerange;
extern map<double,double>MapForItemGroup;
extern vector<double>ItemidsforpageRangevec;
extern vector<double>ItemGroupIDsForPageRange;
extern bool16 isRefreshTabByAttributeCheckBoxSelected;
extern bool16 Isframedelete;
double Languageid;
double typeID11;

double TypeidAdTable;
int32 HeaderAdTable;
int32 isAutoResizeAdTable;
int32 indexAdTable;
double pbObjectIdAdTable;
double sectionIDAdTable;
double parentTypeIDAdTable;
int32 tableFlagAdTable;
double tableTypeAdTable;
double tableIdAdTable;
int32 ChildtagAdTable;
double ParentIdAdTable;
vector<double>VecRownoForAdTable;
vector<double>VecColnoForAdTable;


///////////

RefreshDataList ColorDataList;

TextVectorList OriginalTextList;
TextVectorList NewTextList;
TextVectorList ChangedTextList;

int16 pgno,cursel,seltext,FlagParentID;

vector<double> itemsUpdated;
vector<double> itemsDeleted;
vector<double> productsUpdated;
vector<double> productsDeleted;

vector<double> itemImagesReLinked;
vector<double> productImagesReLinked;
vector<double> itemImagesDeleted;
vector<double> productImagesDeleted;

//vector<int32>upDatedItemParentID;

extern itemList tempItemList;
extern productList tempProductList;

bool16 frameDeleted = kFalse;
extern bool16 refreshTableByAttribute;

//vector<BookReportData> vecReportRows;
bool16 isNewItem = kFalse;
bool16 isnewSection = kFalse;
PMString newSectionName ="";
extern bool16 isDummyCall;

extern bool16 shouldRefreshFlag;
extern bool16 shouldPushInFreameVect;
extern int32 refreshModeSelected;


double CurrentSelectedDocumentSectionID = -1;
double CurrentSelectedDocumentPublicationID = -1;
double CurrentSelectedDocumentLanguageID = -1;

extern K2Vector<int32> selectedDocIndexList;
extern vector<IDFile> BookFileList;

extern PMString imageDownLoadPath;
multimap<double,double>itemid_attributeid;//by Amarjit patil
multimap<double,PMString>itemid_attributevalue;//by Amarjit patil
Attribute_valueptr attributeid_value_Map = NULL;//by Amarjit patil
MapInMapptr MapInMapptr_var =NULL;
MapInMapptr MapInMapptr_var_ItemGroup =NULL;
MapInMapptr MapInMapptr_CustumTable =NULL;


returnintvecptr return_vector =NULL;//by Amarjit patil
vector<PMString>AttributeValue;//by Amarjit patil
vector<PMString>valuebyitemId;//by Amarjit patil
vector<PMString>valuebyattributeId;//by Amarjit patil
extern vector<double> Itemattribute_Itemgroupattribute;
//vector<int32>ItemId_ItemgroupId;//by Amarjit patil
map<double,double>iTEM;//itemid,elementid
//multimap<int32,int32>AdavanceTablesItemIDsandElementIDsMap;
vector<double>ItemIdsForIndesignTable;
vector<double>AttibuteIdsForIndesignTable;
vector<double>ItemIDsForAdvancedTable;
vector<double>AttributeIDSforAdvanceTable;
map<double,double>iTEMGROUP;//itemgroupid,element this map is only used to get unique itemgroupids and element id from this map we are not using,we just fill in 
extern vector<double>textboxvaluevec;//by Amarjit patil
extern vector<double>AttributeIdsForFrameRefresh;//by amarjit patil
extern vector<double>ItemgroupAttributeIdsforframerefresh;

//Friday Neight
vector<double>Emptyvec_ItemGroup;
vector<double>Emptyvec_ItemGroupAttribute;

vector<double>Emptyvec_Item;
vector<double>Emptyvec_IemAttribute;


vector<double>NItemID;
vector<double>NAttributeID;
vector<PMString>NValue;

vector<double>NItemGroupID;
vector<double>NAttributeId_ItemGroup;
vector<PMString>NString;
multimap<vector<double>, vector<PMString> > *Result;
multimap<vector<double>, vector<PMString> > *Result_ItemGroup;
multimap<vector<double>, vector<PMString> > *Result_CustomTable;
vector<double>NItemGroupID_custumtable;

vector<PMString>Vec_CustumTable_String;
vector<double>NAttributeID333;

vector<double>vec_ItemIDsforcustumtable;//
vector<double>vec_AttributeIdforcustumtable;
vector<double>vec_colno;
vector<double>vec_customTablerows;
int32 totalRowsofcustumtable=0;
int32 totalsColsofcustumtable=0;
int32 totalheadersofcustumtable=0;
vector<double>vec_totalheadersofcustumtable;

double vector_iterater=0;
int32 SelectedUidListlength;


vec_GetMultipleSectionDataPtr vec_ptr = NULL;
extern IsMultipleSectionsDataAvailable isMultipleSectionsAvailable;
extern map<double,double>UniqueAttributes;

UniqueIds sectionIds;
//UniqueIds itemIds;
//UniqueIds productIds;

multiSectionMap MapForItemsPerSection;
multiSectionMap MapForProductsPerSection;

bool16 gIsInterFaceCall = kFalse;
bool16 isEmptySectionFound = kFalse;

using namespace std;


void Refresh::appendToGlobalList(const PageData& pData)
{
	//CA(__FUNCTION__);
	//DebugTestAlert
	/*PMString printData;
	printData.Append("The total number of Objects inside the pData is : ");
	printData.AppendNumber(pData.objectDataList.size());
	printData.Append("\n---------------\n");
	for(int32 pDataIndex = 0;pDataIndex < pData.objectDataList.size();pDataIndex++)
	{
		printData.Append(pData.objectDataList[pDataIndex].objectName);
		printData.Append(" : ");
		printData.AppendNumber(pData.objectDataList[pDataIndex].objectID);
		printData.Append("\nElements are : \n");
		for(int32 elementIndex = 0;elementIndex < pData.objectDataList[pDataIndex].elementList.size();elementIndex++)
		{
			printData.Append(pData.objectDataList[pDataIndex].elementList[elementIndex].elementName);
			printData.Append(" : ");
			printData.AppendNumber(pData.objectDataList[pDataIndex].elementList[elementIndex].elementID);
			printData.Append("\n=====\n");

		}
	}
	CA(printData);*/
//end DebugTestAlert

	RefreshData rData;
	//CA_NUM("pData.objectDataList.size() : ",static_cast<int32>(pData.objectDataList.size()));
 	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		//CA_NUM("pData.objectDataList[i].elementList.size() : ",static_cast<int32>(pData.objectDataList[i].elementList.size()));
		if(pData.objectDataList[i].elementList.size()==0 && pData.objectDataList[i].isTableFlag!=kTrue)
		{
			//CA("continue");
			continue;
		}
		
		pgno=1;
		cursel=1;
		rData.StartIndex= 0;
		rData.EndIndex= 0;
		rData.BoxUIDRef=pData.BoxUIDRef;
		rData.boxID=pData.boxID;
		rData.elementID=-1;
		rData.isObject=kTrue;
		rData.isSelected=kFalse;
		rData.name=pData.objectDataList[i].objectName;
		rData.objectID=pData.objectDataList[i].objectID;
		rData.publicationID=pData.objectDataList[i].publicationID;
		rData.isTableFlag=pData.objectDataList[i].isTableFlag;
		//Awasthi
		rData.isImageFlag=pData.objectDataList[i].isImageFlag;
		rData.whichTab = pData.objectDataList[i].whichTab;
        
        if(GroupFlag ==2)
        {
            UID uidOfBox = pData.BoxUIDRef.GetUID();
            int32 PageNumber = -1;
            if(framePageMap.size() > 0)
            {
                PageNumber = (framePageMap.find(uidOfBox))->second;
            }
            rData.pageNo = PageNumber;
            rData.name = "";
            rData.name.Append(" - PageNo: ");
            rData.name.AppendNumber(PageNumber);
            
        }
        
		if(ISRfreshBookDlgOpen == kFalse)
		{
			rDataList.push_back(rData);
			OriginalrDataList.push_back(rData);
		}
		else
		{
			rData.sysFile = pData.sysFile;
			rData.DocumentUIDRef = pData.DocumentUIDRef;
			rBookDataList.push_back(rData);
			OriginalrBookDataList.push_back(rData);
		}

		for(int j=0; j<pData.objectDataList[i].elementList.size(); j++)
		{			
			rData.StartIndex= pData.objectDataList[i].elementList[j].StartIndex;
			rData.EndIndex= pData.objectDataList[i].elementList[j].EndIndex;
			rData.BoxUIDRef=pData.BoxUIDRef;
			rData.boxID=pData.boxID;
			rData.elementID=pData.objectDataList[i].elementList[j].elementID;
			rData.isTaggedFrame=pData.objectDataList[i].elementList[j].isTaggedFrame;
			rData.isObject=kFalse;
			rData.isSelected=kFalse;
			rData.name=pData.objectDataList[i].elementList[j].elementName;
			rData.objectID=pData.objectDataList[i].objectID;
			rData.publicationID=pData.objectDataList[i].publicationID;
			rData.TypeID=pData.objectDataList[i].elementList[j].typeID;
			rData.whichTab =pData.objectDataList[i].elementList[j].whichTab;
			rData.LanguageID = pData.objectDataList[i].elementList[j].languageid;

			rData.parentTypeId =  pData.objectDataList[i].elementList[j].parentTypeId;
			rData.childTag =  pData.objectDataList[i].elementList[j].childTag;
			rData.catLevel =  pData.objectDataList[i].elementList[j].catLevel;

			rData.isAutoResize = pData.objectDataList[i].elementList[j].isAutoResize;
			rData.isSprayItemPerFrame = pData.objectDataList[i].elementList[j].isSprayItemPerFrame;
            rData.value = pData.objectDataList[i].elementList[j].value;
            rData.tStruct = pData.objectDataList[i].elementList[j].tStruct;
            
            if(GroupFlag ==2)
            {
                UID uidOfBox = pData.BoxUIDRef.GetUID();
                int32 PageNumber = -1;
                if(framePageMap.size() > 0)
                {
                    PageNumber = (framePageMap.find(uidOfBox))->second;
                }
                rData.pageNo = PageNumber;
                //rData.name.Append(" PageNo: ");
                //rData.name.AppendNumber(PageNumber);

            }

			if(ISRfreshBookDlgOpen == kFalse)
			{
				rDataList.push_back(rData);
				OriginalrDataList.push_back(rData);
			}
			else
			{
				rData.sysFile = pData.sysFile;
				rData.DocumentUIDRef = pData.DocumentUIDRef;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);
			}
		}
		
	}
	
//CA_NUM("rDataList.size() = ",rDataList.size());
}

bool16 Refresh::getAllPageItemsFromPage(int32 pageNumber)
{
	//CA(__FUNCTION__);
	/*	UID pageUID;
	TagReader tReader;
	if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
		IDocument* document = ::GetFrontDocument(); 
	IDataBase* database= ::GetDataBase(document); 
	//IDocument* document = layoutData->GetDocument();
	if (document == nil)
		return kFalse;
	IDataBase* database = ::GetDataBase(document);
	if(!database)
		return kFalse;*/
	
/*	InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
	int32 spreadNo = -1; 

	for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
	{ 
		UIDRef spreadRef(database, spreadList->GetNthSpreadUID(i)); 
		InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
		if(spread->GetPageIndex(pageUID)!= -1) 
		{			
			spreadNo = i; 
			break; 
		}
	}

	if(spreadNo==-1)
		return kFalse;

	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
	if (layoutData == nil)
		return kFalse;
	layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
	
	InterfacePtr<ILayoutControlData> layoutData1(::QueryFrontLayoutData());
	if (layoutData1 == nil)
		return kFalse;

	IGeometry* spreadItem = layoutData1->GetSpread();
	if (spreadItem == nil)
		return kFalse;
	InterfacePtr<ISpread> spread((IPMUnknown *)spreadItem, UseDefaultIID());
	if(spread == nil)
		return kFalse;
	UIDList tempList(database);

	spread->GetItemsOnPage(pageNumber, &tempList, kFalse);

	selectUIDList=tempList;

	for(int i=0; i<selectUIDList.Length(); i++)
	{	
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
			continue;

		UID kidUID;
		int32 numKids=iHier->GetChildCount();
		for(int j=0;j<numKids;j++)
		{
			IIDXMLElement* ptr;
			kidUID=iHier->GetChildUID(j);
			UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
			itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
			if(!doesExist(ptr))
				selectUIDList.Append(kidUID);
		}
	}
	return kTrue;
*/
		UID pageUID;
		//TagReader tReader;
		InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getAllPageItemsFromPage::!itagReader");
			return kFalse;
		}
		if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		IDataBase *db = ::GetDataBase(document); 
		ASSERT(db); 

		InterfacePtr<ISpreadList> spreadList(document, UseDefaultIID()); 
		ASSERT(spreadList); 

		int32 spreadNum = 0; 
		int32 spreadCount = spreadList->GetSpreadCount(); 
		int32 currentPageNumber = 0; 
		
		
		while(spreadNum < spreadCount) 
		{ 
			bool16 FirstWhilebreak= kFalse;
			InterfacePtr<ISpread> spread(db, spreadList->GetNthSpreadUID(spreadNum), IID_ISPREAD); 
			ASSERT(spread); 

			int32 pagesOnSpread = spread->GetNumPages(); 
			
			int32 PageNO=0;
			while(pagesOnSpread--) 
			{	
				bool16 SecondWhileBreak= kFalse;				
				if(currentPageNumber == pageNumber) 
				{ 					
					UIDList itemsList(db); 
					spread->GetItemsOnPage(/*pageNumber*/PageNO, &itemsList, kFalse); 
					selectUIDList = itemsList;					
					SecondWhileBreak= kTrue;
					FirstWhilebreak= kTrue;
					break;//return itemsList; 
				} 
				PageNO++;
				currentPageNumber++; 
				if(SecondWhileBreak)
					break;				
			} 
			++spreadNum; 
			if(FirstWhilebreak)
				break;
			
		} 

		for( int p=0; p<selectUIDList.Length(); p++)
		{	
			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
			if(!iHier)
				continue;
			UID kidUID;
			int32 numKids=iHier->GetChildCount();
			bool16 isGroupFrame = kFalse ;
			isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));

			for(int j=0;j<numKids;j++)
			{
				//CA("1");
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				IIDXMLElement* ptr;
				TagList tList,tList_checkForHybrid;
				
				if(isGroupFrame == kTrue) 
				{
					tList_checkForHybrid =itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
					if(tList_checkForHybrid.size() == 0)
					{
						//CA("tList_checkForHybrid.size() == 0");
						continue;
					}
					if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
						tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
					else
						tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
				}
				else 
				{
					tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
					if(tList_checkForHybrid.size() == 0)
					{
						//CA("tList_checkForHybrid.size() == 0");
						continue;
					}
					if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
						tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
					else
						tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
				}

				if(!doesExist(tList))//if(!doesExist(ptr))
				{
					selectUIDList.Append(kidUID);
				}
				//------lalit--------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
				{
					tList_checkForHybrid[tagIndex].tagPtr->Release();
				}
			}
		}
	return kTrue;

}

bool16 Refresh::isValidPageNumber(int32 pageNumber, UID& myPageUID)
{	
	//CA(__FUNCTION__);
	IDocument * myDocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());
	
	if(myDoc!=nil)
	{	
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::isValidPageNumber::myPageList==nil");
			return kFalse;
		}
		
		int32 PageCount = myPageList->GetPageCount();
		PageCount--;
		if(pageNumber> PageCount)
		{	
			myPageUID= kInvalidUID;
			return kFalse;
		}
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
		return kTrue;
	}
	
	return kFalse;
}

bool16 Refresh::isValidPageNumber(int32 pageNumber)
{
	//CA(__FUNCTION__);
	UID myPageUID;
	IDocument * myDocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());

	if(myDoc!=nil)
	{
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::isValidPageNumber::myPageList==nil");
			return kFalse;
		}
		
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
			return kTrue;
	}
	return kFalse;
}


bool16 Refresh::GetPageDataInfo(int whichSelection)
{	

//CA(__FUNCTION__);
	/*
	Important Note :
	whichSelection = 1 for current page option//itemsUpdated
				   = 2 for specific page number
				   = 3 for current selection.
	Note this order is exactly reverse to the corresponding options which 
	appear in the dropdown.
	To hell with the programmer who has written this code.
	*/
//CA("");
//CA_NUM("",whichSelection);
	BoxReader bReader;
	UID pageUID=kInvalidUID;
	int i,k=0;
	
	rDataList.clear();
	OriginalrDataList.clear();

	pgno=0;
	cursel=0;
	seltext=0;
	FlagParentID = 0;
	this->selectUIDList.Clear();

	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::GetPageDataInfo::itagReader==nil");
		return kFalse;
	}
	
	switch(whichSelection)
	{
	case 1://current page
		{ 
            //CA("1");
			itemsUpdated.clear();
			itemsDeleted.clear();
			productsUpdated.clear();
			productsDeleted.clear();
			//for current page option
     		UID pageUID;
			//TagReader tReader;
			int32 pageNumber= 0;
			if(!this->isValidPageNumber(pageNumber, pageUID))
			{
				return kFalse;
			}
			
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
			IDataBase* database= ::GetDataBase(document); 
			/*IDocument* document = layoutData->GetDocument();
			if (document == nil)
				return kFalse;
			IDataBase* database = ::GetDataBase(document);
			if(!database)
				return kFalse;*/
			
			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
			int32 spreadNo = -1; 
			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
			{ 
				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
				if(spread->GetPageIndex(pageUID)!= -1) 
				{			
					spreadNo = p; 
					break; 
				}
			}
			if(spreadNo==-1)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::GetPageDataInfo::spreadNo==-1");
				return kFalse;
			}

			InterfacePtr<ILayoutControlData>layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::GetPageDataInfo::layoutData == nil");			
				return kFalse;
			}
			pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::GetPageDataInfo::pageUID == kInvalidUID");			
				 return kFalse;
			}
		
			UIDRef SpreadRef=layoutData->GetSpreadRef(); 		
//			InterfacePtr<ISpread> iSpread((IPMUnknown *)spreadItem, UseDefaultIID());
			InterfacePtr<ISpread> iSpread(SpreadRef, UseDefaultIID());
			if (iSpread == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::GetPageDataInfo::iSpread == nil");			
				return kFalse;
			}

			UIDList tempList(database);
			pageNumber=iSpread->GetPageIndex(pageUID);
			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);
			selectUIDList=tempList;
            //CA("before first for");

			for( int p=0; p<selectUIDList.Length(); p++)
			{	
				InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
				if(!iHier)
					continue;
				UID kidUID;
				int32 numKids=iHier->GetChildCount();
				bool16 isGroupFrame = kFalse ;
				isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));

				for(int j=0;j<numKids;j++)
				{
					//CA("1");
					kidUID=iHier->GetChildUID(j);
					UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
					IIDXMLElement* ptr;
					TagList tList,tList_checkForHybrid;
					
					if(isGroupFrame == kTrue) 
					{
						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
						if(tList_checkForHybrid.size() == 0)
						{
						//CA("tList_checkForHybrid.size() == 0");
							continue;
						}
						if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
						else
							tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
					}
					else 
					{
						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
						if(tList_checkForHybrid.size() == 0)
						{
						//CA("tList_checkForHybrid.size() == 0");
							continue;
						}
						if(refreshTableByAttribute  /*&& tList_checkForHybrid[0].tableType != 3*/)
							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
						else
							tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
					}

					if(!doesExist(tList))//if(!doesExist(ptr))
					{
						selectUIDList.Append(kidUID);
					}
					//-------lalit---------
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
					{
						tList_checkForHybrid[tagIndex].tagPtr->Release();
					}
				}
			}
            //CA("after first for");
			for(int p=0; p<selectUIDList.Length(); p++)
			{	
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(p), pData);
				appendToGlobalList(pData);

				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(p);
				TagList tList,tList_checkForHybrid;
				tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList_checkForHybrid.size() == 0)
				{
					//CA("tList_checkForHybrid.size() == 0");
					continue;
				}
				//TagReader tReader;
				if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
					tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
				else
					tList=itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList.size()==0)
				{	
					tList=itagReader->getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}

				for(int j=0; j<tList.size(); j++)
				{		
					if(tList[j].parentId==-1)
						continue;				
					k=1;
				}
				if(k==0)
					FlagParentID = 1;			

				//--------lalit------------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
				{
					tList_checkForHybrid[tagIndex].tagPtr->Release();
				}

			}		
//CA("after second for");
		break;
		}

	case 2://Page number
		{	
//CA("2");
		itemsUpdated.clear();
		itemsDeleted.clear();
		productsUpdated.clear();
		productsDeleted.clear();

		if(!this->getAllPageItemsFromPage(Mediator::refreshPageNumber))
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::GetPageDataInfo::!getAllPageItemsFromPage");			
			return kFalse;
		}
		for(i=0; i<selectUIDList.Length(); i++)
		{	
			PageData pData;
			bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
			appendToGlobalList(pData);
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList,tList_checkForHybrid;
			tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
			if(tList_checkForHybrid.size() == 0)
			{
				//CA("tList_checkForHybrid.size() == 0");
				continue;
			}
			//TagReader tReader;
			if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
				tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			else
				tList=itagReader->getTagsFromBox_ForRefresh(boxID);
			if(tList.size()==0)
			{	
				tList=itagReader->getFrameTags(boxID);
				if(tList.size()==0)
				{
					//FlagParentID = 1;
					continue;
				}
			}
			for(int j=0; j<tList.size(); j++)
			{				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
				FlagParentID = 1;			

			//--------lalit------------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
			{
				tList_checkForHybrid[tagIndex].tagPtr->Release();
			}
		}
		break;
		}
	case 3://selected frame
		{

			//CA("case 3: selected frame");
		//itemsUpdated.clear();
		itemsDeleted.clear();
		productsUpdated.clear();
		productsDeleted.clear();

		if(!this->getAllBoxIds())	
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::GetPageDataInfo::!getAllBoxIds");
			return kFalse;
		}
		
		for(i=0; i<selectUIDList.Length(); i++)
		{
			PageData pData;
            //CA("Before getBoxInformation");
			bool16 result11 = bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
            //CA("After getBoxInformation");
			appendToGlobalList(pData);
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList,tList_checkForHybrid ;
			tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
			if(tList_checkForHybrid.size() == 0)
			{
				//CA("tList_checkForHybrid.size() == 0");
				continue;
			}
			//TagReader tReader;
			if(refreshTableByAttribute && tList_checkForHybrid[0].tableType != 3)
				tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			else
				tList=itagReader->getTagsFromBox_ForRefresh(boxID);
			
			if(tList.size()==0)
			{
				tList=itagReader->getFrameTags(boxID);
				if(tList.size()==0)
				{
					//CA("tList.size()==0");
					//FlagParentID = 1;
					continue;
				}
			}

			for(int j=0; j<tList.size(); j++)
			{				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
			{
				FlagParentID = 1;
			}
			//--------------------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
			{
				tList_checkForHybrid[tagIndex].tagPtr->Release();
			}

		}

		break;
		}
	case 4://entire document
		do
		{
//CA("4");
			itemsUpdated.clear();
			itemsDeleted.clear();
			productsUpdated.clear();
			productsDeleted.clear();

			///////////// Code to get Elementlist from Document/////////////////
			if(!getDocumentSelectedBoxIds())
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::GetPageDataInfo::!getDocumentSelectedBoxIds");
				return kFalse;
			}
            
            //CA("After getDocumentSelectedBoxIds");
            PMString a("selectUIDList.Length()	:	");
            a.AppendNumber(selectUIDList.Length());
            //CA(a);
            
            ptrIAppFramework->EventCache_clearCurrentSectionData();
            ptrIAppFramework->clearAllStaticObjects();
            
            bool16 isSectiondataPopulated = this->getSectionDataforBoxList(selectUIDList);  // Added to grab JSON data
            if(!isSectiondataPopulated )
            {
                ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isSectiondataPopulated");
                return kFalse;
            }
            
            //CA("After getSectionDataforBoxList");
            
            
			for(i=0; i<selectUIDList.Length(); i++)
			{
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(i), pData);

                //CA("before appendToGlobalList");
				appendToGlobalList(pData);
                //CA("after appendToGlobalList");
				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(i);
				TagList tList,tList_checkForHybrid;
				tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList_checkForHybrid.size() == 0)
				{
					//CA("tList_checkForHybrid.size() == 0 + continue");
					continue;
				}
				//TagReader tReader;
				if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
					tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
				else
					tList=itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList.size()==0)
				{
					tList=itagReader->getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}
				for(int j=0; j<tList.size(); j++)
				{				
					if(tList[j].parentId==-1)
						continue;
					k=1;
				}
				if(k==0)
					FlagParentID = 1;
				
                //-----------lalit---------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
				{
					tList_checkForHybrid[tagIndex].tagPtr->Release();
				}
				
			}
		}while(kFalse);
		break;
	}
	
	return kTrue;
}

//by amarjit
//bool16 Refresh::refreshThisBoxNewoption(UIDRef& boxID, PMString& imagePath, bool16 isInterFaceCall)
//{
//	//CA(" Refresh::refreshThisBoxNewoption");
//  //CA("Refresh::refreshThisBox");//shouldRefresh
//	//CA(__FUNCTION__);
//
//	//CA("inside Refresh::refreshThisBox");
//	//*******
//		//******Checking For Is InlineImage. 	
//	/*  **************************************	
//		*	Checking Here For IsImageInline means, is There any
//		*	Image Inside the TxtFrame, if True Then Set isInlineImage flag == kTrue;		
//		****************************************	
//	*/
//	//bool16 checkcondition = kFalse;
//	ItemIDsForAdvancedTable.clear();
//	AttributeIDSforAdvanceTable.clear();
//
//	ItemIdsForIndesignTable.clear();
//	AttibuteIdsForIndesignTable.clear();
//
//	VecRownoForAdTable.clear();
//	VecColnoForAdTable.clear();
//	UID textFrameUID = kInvalidUID;
//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());	
////	do
////	{		
////		if (graphicFrameDataOne) 
////			textFrameUID = graphicFrameDataOne->GetTextContentUID();
////		if (textFrameUID == kInvalidUID)			
////			break;
////		
////		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
////		if (graphicFrameHierarchy == nil) 
////		{
////			//CA(graphicFrameHierarchy);
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!graphicFrameHierarchy");
////			break;
////		}
////		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
////		if (!multiColumnItemHierarchy) {		
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!multiColumnItemHierarchy");
////			break;
////		}
////	
////		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
////		if (!multiColumnItemTextFrame) {				
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!multiColumnItemTextFrame");
////			//CA("Its Not MultiColumn");
////			break;
////		}
////
////		InterfacePtr<IHierarchy>
////		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
////		if (!frameItemHierarchy) {	
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!frameItemHierarchy");
////			break;
////		}
////
////		InterfacePtr<ITextFrameColumn>
////		textFrame(frameItemHierarchy, UseDefaultIID());
////		if (!textFrame) 
////		{		
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!textFrame");
////			break;
////		}
////	
////		InterfacePtr<ITextModel> textModel1(textFrame->QueryTextModel());
////		if (textModel1 == NULL)
////		{
////			ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!textModel");	
////			break;
////		}
////////===========================================Start ===================================================
//////		InterfacePtr<ITableModelList>tableList(textModel1, UseDefaultIID());
//////		if(tableList==nil)
//////		{
//////			//CA("tableList==nil");
//////			continue;
//////		}
//////		int32	tableIndex = tableList->GetModelCount() ;
//////		bool16 customItemTableFound = kFalse;
//////		for(int32 index = 0;index < tableIndex ; index++)
//////		{
//////			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(index));
//////			if(tableModel == nil) 
//////			{
//////				continue;
//////			}
//////
//////			InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
//////			if(tableTextContent == nil)
//////			{
//////				//CA("tableTextContent == nil");
//////				continue;
//////			}
//////			InterfacePtr<ITextModel> tableTextModel(tableTextContent->QueryTextModel());
//////			if(tableTextModel == nil)
//////			{
//////				//CA("tableTextModel == nil");
//////				continue;
//////			}
//////			//end 17July
//////
//////			UIDRef tableUIDRef(::GetUIDRef(tableModel));
//////			
//////			ColRange col = tableModel->GetTotalCols();
//////			RowRange row = tableModel->GetTotalRows();
//////
//////			InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
//////			/*if(ptrIAppFramework == nil)
//////				return;*/
//////
//////			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//////			if(tableCommands==nil)
//////			{
//////				//CA("Err: invalid interface pointer ITableCommands");
//////				break;
//////			}
//////
//////			InterfacePtr<ITagReader> itagReader
//////			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//////			/*if(!itagReader)
//////				return ;*/
//////
//////			InterfacePtr<ITableUtility> iTableUtlObj
//////			((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
//////			//if(!iTableUtlObj){// CA("!iTableUtlObj");
//////			//	return ;
//////			//}
//////
//////			TagList tList=itagReader->getTagsFromBox(boxID);	
//////			IIDXMLElement* XMLElementPtr =  NULL; 
//////			TagStruct tStruct;
//////			/*if(tList.size()==0)
//////				return;*/
//////
//////			for(int32 r=0; r<tList.size(); r++)
//////			{
//////				PMString tabletype("tabletype :");
//////				tabletype.AppendNumber(tList[r].tableType);
//////				
//////				tabletype.Append("\n");
//////				tabletype.Append("IsTablepresent :");
//////				tabletype.AppendNumber(tList[r].isTablePresent);
//////				//CA(tabletype);
//////				if(tList[r].imgFlag==0)
//////				{
//////					if(tList[r].isTablePresent == 1 && tList[r].tableType == 2/*tList[j].typeId == -3*/)
//////					{
//////						//checkcondition = kTrue;
//////						//CA("Before calling method--4");
//////						RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(boxID, tList[r]);
//////						//CA("After calling method--4");
//////					}
//////				}
//////				
//////			}
//////			//if(checkcondition)
//////			return kTrue;
//////		}
////////=======================================End ========================================================
////		//CA("outside my code");
////		IDataBase* db = ::GetDataBase(textModel1);
////		OwnedItemDataList ownedList;
////		Utils<ITextUtils>()->CollectOwnedItems(textModel1,
////											0,
////											textModel1->GetPrimaryStoryThreadSpan(), 
////											&ownedList,
////											kTrue);		
////		isInlineImage = kFalse;
////		for (int32 i = 0; i < ownedList.Length(); i++) 
////		{
////			if ((ownedList[i].fClassID == kInlineBoss))
////			{	
////				isInlineImage = kTrue;					
////				break;
////			}
////		}	
////	}while(kFalse);
//	//******
//	//*******
//	TagList tList,tList_checkForHybrid;
//	//TagReader tReader;
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return kFalse;
//	}
//
//	ptrIAppFramework->clearAllStaticObjects();
//	
//
//	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//	if(!itagReader)
//	{ 
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshThisBox::tagreader nil ");
//		return kFalse;
//	}
//	
//	bool16 isONEsource = kFalse;
//	isONEsource = ptrIAppFramework->get_isONEsourceMode();
//	//********Added For checkbox Selection of RelinkinImage
//	bool16 isupdateImageSelected = kTrue;
//	
//	if(!isDummyCall){
//		//CA("if(!isDummyCall){");
//		if(Mediator::catalogRefrshPanelControlData != nil )
//		{
//			
//			IControlView* RfhDlgUpdateImagesChkBxView = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhUpdateImagesCheckBoxWidgetID);
//			if(!RfhDlgUpdateImagesChkBxView)
//			{		
//				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::RfhDlgUpdateImagesChkBxView nil ");
//				return kFalse;
//			}
//
//			InterfacePtr<ITriStateControlData>updateImagesCheckBoxTriState(RfhDlgUpdateImagesChkBxView,UseDefaultIID());
//			if(updateImagesCheckBoxTriState == nil)
//			{
//				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::updateImagesCheckBoxTriState nil ");
//				return kFalse;
//			}
//
//			isupdateImageSelected = updateImagesCheckBoxTriState->IsSelected();	
//		}
//	}
//	//********upto Here
//	
//	if(!isInterFaceCall)
//	{
//		//CA("!isInterFaceCall");
//		//CA("1a");
//		tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
//		if(tList_checkForHybrid.size() == 0)
//		{
//			return kTrue;
//		}
//		if(refreshTableByAttribute && tList_checkForHybrid[0].tableType != 3){
//			//tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);//original
//			tList=itagReader->getTagsFromBox(boxID);// Table is refreshing well  but Group title field is not working
//			//tList=itagReader->getTagsFromBox_ForRefresh(boxID);
//		}
//		else{
//			tList=itagReader->getTagsFromBox_ForRefresh(boxID);
//		}
//	}
//	else
//	{
//		tList=itagReader->getTagsFromBox_ForRefresh(boxID);
//	}
//
//	if(tList.size()==0)
//	{	
//		tList=itagReader->getFrameTags(boxID);
//		if(tList.size()==0)
//		{
//			ptrIAppFramework->LogInfo("AP7_RefreshContent::Refresh::refreshThisBox::tList.size()==0");		
//			return kTrue;
//		}
//		refreshTaggedBox(boxID, imagePath, isInterFaceCall);
//		return kTrue;
//	}
//
//	BookReportData::langaugeID = tList[0].languageID; //For Report
//
//	
//	if(tList[0].isTablePresent == 1 && tList[0].tableType != 2/*tList[0].typeId != -3*/)
//	{	
//		typeID11 = tList[0].typeId;
//
//		Vec_CustumTable_String.clear();
//		vec_ItemIDsforcustumtable.clear();
//		vec_AttributeIdforcustumtable.clear();
//		vec_colno.clear();
//		vector<int32>IGIDs;
//		vector<int32>IGAttributeIDs;
//
//		int32 header1 = tList[0].header;
//		vec_totalheadersofcustumtable.clear();
//		NAttributeID333.clear();
//
//		
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//			//break;//breaks the do{}while(kFalse)
//		}		 	
//		
//		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/		
//		UID textFrameUID = kInvalidUID;
//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
//		if (graphicFrameDataOne) 
//		{
//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
//		}
//		if (textFrameUID == kInvalidUID)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrameUID == kInvalidUID");
//			//break;//breaks the do{}while(kFalse)
//		}
//		//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		//if (textFrame == nil)
//		//{
//		//	ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrame == nil");
//		//	break;//breaks the do{}while(kFalse)
//		//}
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			//break;
//		}
//
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			//break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			//break;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			//break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!!ITextFrameColumn");
//			//break;
//		}
//		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//		if (textModel == nil)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textModel == nil");
//			//break;//breaks the do{}while(kFalse)
//		}
//		UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//		if(tableList==nil)
//		{
//			//break;//breaks the do{}while(kFalse)
//		}
//		int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//		
//		if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//		{
//			//break;//breaks the do{}while(kFalse)
//		}
//				
//		for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//		{//for..tableIndex
//			
//			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//			if(tableModel == nil)
//				continue;//continues the for..tableIndex
//
//			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//			if(tableCommands==NULL)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
//				continue;//continues the for..tableIndex
//			}
//			
//			 totalRowsofcustumtable = tableModel->GetTotalRows().count-1;
//			 totalsColsofcustumtable = tableModel->GetTotalCols().count;
//			 totalheadersofcustumtable = tableModel->GetHeaderRows().count;//GetHeaderRows 
//
//			 if(header1==1 && totalheadersofcustumtable==0)
//			 {
//				totalheadersofcustumtable=1;
//				totalRowsofcustumtable=totalRowsofcustumtable-1;
//			 }
//
//			 if(totalheadersofcustumtable==0)
//			 {
//				totalRowsofcustumtable = tableModel->GetTotalRows().count;
//			 }
//			 else
//			 {
//				totalRowsofcustumtable = tableModel->GetTotalRows().count-1;
//			 }
//
//			 PMString e("totalRowsofcustumtable : ");
//			 e.AppendNumber(totalRowsofcustumtable);
//			 e.Append("\n");
//			 e.Append("totalsColsofcustumtable :");
//			 e.AppendNumber(totalsColsofcustumtable);
//			 e.Append("\n");
//			 e.Append("totalheadersofcustumtable : ");
//			 e.AppendNumber(totalheadersofcustumtable);
//			 //CA(e);
//
//
//			 vec_totalheadersofcustumtable.push_back(totalheadersofcustumtable);
//
//			UIDRef tableRef(::GetUIDRef(tableModel)); 
//
//			//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//			IIDXMLElement* & tableXMLElementPtr = tList[0].tagPtr;
//			XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
//			UIDRef ContentRef = contentRef.GetUIDRef();
//
//			PMString indexValueAttachedToTable = tableXMLElementPtr->GetAttributeValue(WideString("index"));
//			int32 Index = indexValueAttachedToTable.GetAsNumber();
//			
//
//		for(int32 t=0;t<tList.size();t++)
//		{
//			if(tList[t].imgFlag!=1 && (tList[t].whichTab==4 || tList[t].whichTab==3))
//			{
//				int32 childTagCount = tList[t].tagPtr->GetChildCount();
//				
//				for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
//				{
//					XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//					//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
//					if(childTagXMLElementPtr == nil)
//					continue;
//			        //PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4  parentID
//					PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));
//					int32 childId22 = strchildId22.GetAsNumber();
//					int32 elementCount=childTagXMLElementPtr->GetChildCount();
//					  
//					for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
//						{
//						     XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
//						     InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
//							 if(childElement == nil)
//								continue;
//								PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
//								int32 childId11 = strchildId11.GetAsNumber();
//
//								PMString strattibute11 = childElement->GetAttributeValue(WideString("ID"));//Cs4
//								int32 attribute11 = strattibute11.GetAsNumber();
//
//								//PMString strTypeID = childElement->GetAttributeValue(WideString("typeId")); //Cs4
//								// typeID11 = strTypeID.GetAsNumber();	
//
//								int32 sizeofvec1 =static_cast<int32>(ItemIdsForIndesignTable.size());
//								int32 sizeofvec2 =static_cast<int32>(AttibuteIdsForIndesignTable.size());
//
//								if(childId11!=-1)
//								{
//									if(sizeofvec1>0)
//									{
//										bool16 flag1 = kFalse;
//										for(int32 i=0;i<sizeofvec1;i++)
//										{
//											if(ItemIdsForIndesignTable.at(i)==childId11)
//												flag1=kTrue;
//										}
//										if(flag1==kFalse)
//											ItemIdsForIndesignTable.push_back(childId11);
//									}
//									if(sizeofvec2>0)
//									{
//										bool16 flag2 = kFalse;
//										for(int32 i=0;i<sizeofvec2;i++)
//										{
//											if(AttibuteIdsForIndesignTable.at(i)==attribute11)
//												flag2=kTrue;
//										}
//										if(flag2==kFalse)
//											AttibuteIdsForIndesignTable.push_back(attribute11);
//									}
//									if(sizeofvec1==0)
//										ItemIdsForIndesignTable.push_back(childId11);
//									if(sizeofvec2==0)
//										AttibuteIdsForIndesignTable.push_back(attribute11);
//
//								}
//								int32 elementcount1=childElement->GetChildCount();
//								for(int32 count1=0;count1<elementcount1;count1++)
//								{
//									TagStruct tagInfo;
//									XMLReference elementXMLref123 = childElement->GetNthChild(count1);
//									InterfacePtr<IIDXMLElement>childElement123(elementXMLref123.Instantiate());
//									if(childElement123 == nil)
//									  continue;
//									PMString strchildId = childElement123->GetAttributeValue(WideString("childId"));//Cs4 Languageid
//									int32 childId = strchildId.GetAsNumber();
//
//									PMString strAttribute = childElement123->GetAttributeValue(WideString("ID"));//Cs4
//									int32 Id = strAttribute.GetAsNumber();
//
//									PMString strLang = childElement123->GetAttributeValue(WideString("LanguageID"));//Cs4
//									Languageid=strLang.GetAsNumber();
//
//									PMString strTypeID = childElement123->GetAttributeValue(WideString("typeId"));
//									TypeidAdTable=strTypeID.GetAsNumber();
//
//									PMString strIndex = childElement123->GetAttributeValue(WideString("index"));
//									indexAdTable=strIndex.GetAsNumber();
//
//									PMString strParentID = childElement123->GetAttributeValue(WideString("parentID"));
//									ParentIdAdTable=strParentID.GetAsNumber();
//
//									PMString strRowNo = childElement123->GetAttributeValue(WideString("rowno"));
//									int32 rownoAdTable=strRowNo.GetAsNumber();
//									VecRownoForAdTable.push_back(rownoAdTable);
//
//									PMString strColNo = childElement123->GetAttributeValue(WideString("colno"));
//									int32 colnoAdTable=strColNo.GetAsNumber();
//									VecColnoForAdTable.push_back(colnoAdTable);
//
//									PMString strParentTypeID = childElement123->GetAttributeValue(WideString("parentTypeID"));
//									parentTypeIDAdTable=strParentTypeID.GetAsNumber();
//
//									PMString strChildtag = childElement123->GetAttributeValue(WideString("childTag"));
//									ChildtagAdTable=strChildtag.GetAsNumber();
//
//									PMString strtableType = childElement123->GetAttributeValue(WideString("tableType"));
//									tableTypeAdTable=strtableType.GetAsNumber();
//
//									PMString strtableId = childElement123->GetAttributeValue(WideString("tableId"));
//									tableIdAdTable=strtableId.GetAsNumber();
//
//									PMString strtableFlag = childElement123->GetAttributeValue(WideString("tableFlag"));
//									tableFlagAdTable=strtableFlag.GetAsNumber();
//
//									PMString strsectionID = childElement123->GetAttributeValue(WideString("sectionID"));
//									sectionIDAdTable=strsectionID.GetAsNumber();
//
//									PMString strpbObjectId = childElement123->GetAttributeValue(WideString("pbObjectId"));
//									pbObjectIdAdTable=strpbObjectId.GetAsNumber();
//
//									PMString strisAutoResize = childElement123->GetAttributeValue(WideString("isAutoResize"));
//									isAutoResizeAdTable=strisAutoResize.GetAsNumber();
//									
//									ItemIDsForAdvancedTable.push_back(childId);
//									AttributeIDSforAdvanceTable.push_back(Id);
//								}
//					}
//				
//				}
//			}
//		}
//
//			int32 ContentRef12=ContentRef.GetUID().Get();
//			if( ContentRef != tableRef)
//			{
//				//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//				continue; //continues the for..tableIndex
//			}	
//			int32 tableRef12 =tableRef.GetUID().Get();
//			if(Index==3)
//			{
//				for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
//				{//start for tagIndex = 0
//					XMLReference cellXMLElementRef =   tableXMLElementPtr->GetNthChild(tagIndex);
//					//This is a tag attached to the cell.
//					//IIDXMLElement * cellXMLElementPtr = cellXMLElementRef.Instantiate();
//					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//					//Get the text tag attached to the text inside the cell.
//					//We are providing only one texttag inside cell.
//					if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
//					{
//						continue;
//					}
//					for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
//					{
//						bool16 flag1=kFalse;
//						int32 sizz=static_cast<int32>(vec_AttributeIdforcustumtable.size());
//						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
//						//IIDXMLElement * cellTextTagPtr = cellTextXMLRef.Instantiate();
//						InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
//						//The cell may be blank. i.e doesn't have any text tag inside it.
//						if(cellTextTagPtr == NULL)
//						{
//							continue;
//						}
//
//						PMString  Attibute = cellTextTagPtr->GetAttributeValue(WideString("ID")); //-----
//						int32 attributeID = Attibute.GetAsNumber();
//
//						PMString strchildId = cellTextTagPtr->GetAttributeValue(WideString("childId"));//Cs4
//						int32 childId = strchildId.GetAsNumber();
//
//						PMString strcolNo = cellTextTagPtr->GetAttributeValue(WideString("colno"));//Cs4
//						int32 colno = strcolNo.GetAsNumber();
//
//						bool16 flag2=kFalse;
//						int32 size1 = static_cast<int32>(vec_ItemIDsforcustumtable.size());
//						if(size1>0)
//						{
//							for(int32 i=0;i<size1;i++)
//							{
//								if(vec_ItemIDsforcustumtable.at(i)==childId)
//									flag2=kTrue;
//							}
//						}
//						if(flag2==kFalse && childId!=-1)
//							vec_ItemIDsforcustumtable.push_back(childId);
//						if(sizz>0)
//						{
//							for(int32 i=0;i<sizz;i++)
//							{
//								if(vec_AttributeIdforcustumtable.at(i)==attributeID)
//									flag1=kTrue;
//							}
//						}
//						if(flag1==kFalse && childId!=-1)
//						{
//							vec_AttributeIdforcustumtable.push_back(attributeID);
//							PMString ii("columnNo : ");
//							ii.AppendNumber(colno);
//							ii.Append("\n");
//							ii.Append("Attribute : ");
//							ii.AppendNumber(attributeID);
//							//CA(ii);
//						}
//					}
//				}
//			}
//		}
//		int32 ff=static_cast<int32>(vec_AttributeIdforcustumtable.size());
//		if(IsDeleteFlagSelected == kTrue && tList[0].parentId != -1 && (isInterFaceCall == kFalse))
//		{
//			bool16 ISProdDeleted = kFalse;
//			if(isONEsource == kFalse)
//			{
//				ISProdDeleted  = kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
//			}
//			if(ISProdDeleted)
//			{
//				bool16 alreadyExist = kFalse;
//				if(tList[0].whichTab == 3)
//				{
//					int32 productID = tList[0].parentId;
//					for(int index = 0; index<productsDeleted.size();index++ )
//					{
//						if(productsDeleted[index] == productID)
//						alreadyExist = kTrue;
//					}
//					productsDeleted.push_back(productID);
//					if(isDummyCall){ //*** For Refresh Report
//						BookReportData::deletedItemTagList.push_back(tList);	
//					}
//				}
//
//				if(tList[0].whichTab == 4)
//				{
//					int32 itemID = tList[0].parentId;
//					for(int index = 0; index<itemsDeleted.size();index++ )
//					{
//						if(itemsDeleted[index] == itemID)
//						alreadyExist = kTrue;
//					}
//						itemsDeleted.push_back(itemID);
//					
//					if(isDummyCall){ //*** For Refresh Report
//						BookReportData::deletedItemTagList.push_back(tList);	
//					}
//				}
//				
//				if(!isDummyCall)
//					this->deleteThisBox(boxID);
//				
//				return kFalse;
//			}
//		}
//		
//		//CA("For custom Table")
//		if(vec_ItemIDsforcustumtable.size()>0 && vec_AttributeIdforcustumtable.size()>0)
//		{
//			MapInMapptr_CustumTable  = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIdsNewforCustomTable(vec_ItemIDsforcustumtable,vec_AttributeIdforcustumtable,Emptyvec_ItemGroup,Emptyvec_ItemGroupAttribute,tList[0].languageID);
//			if(MapInMapptr_CustumTable == NULL)
//			{
//				//CA("NULL");
//				//break;
//				//return;
//			}
//			int32 ww = static_cast<int32>(MapInMapptr_CustumTable->size());
//			
//			MapInMap::iterator iteer;
//			for(iteer=MapInMapptr_CustumTable->begin();iteer!=MapInMapptr_CustumTable->end();iteer++)
//			{
//				NItemGroupID_custumtable=iteer->first;
//				Result_CustomTable =iteer->second;
//			}
//			multimap<vector<int32>,vector<PMString>>::iterator iter2;
//			iter2=Result_CustomTable->begin();
//			if(iter2!=Result_CustomTable->end())
//			{
//				 NAttributeID333 = iter2->first;
//				 for(int32 j=0;j<NAttributeID333.size();j++)
//				 {
//					 Vec_CustumTable_String = iter2->second;
//					 PMString aq("str : ");
//						aq.Append(Vec_CustumTable_String.at(j));
//						//CA(aq);
//				 }
//				 MapInMapptr_CustumTable->clear();
//				 Result_CustomTable->clear();
//				 NItemGroupID_custumtable.clear();
//			}
//			int32 sa=static_cast<int32>(Vec_CustumTable_String.size());
//		}
//		
//		refreshTaggedBox(boxID, imagePath, isInterFaceCall);
//		
//		//leaks found here
//		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//		{
//			tList[tagIndex].tagPtr->Release();
//		}
//
//		if(MapInMapptr_CustumTable)
//		{
//			delete MapInMapptr_CustumTable;
//			MapInMapptr_CustumTable = NULL;
//		}
//		return kTrue;
//	}
//	if((tList[0].imgFlag == 0) || ((tList[0].imgFlag == 1) && ((tList[0].whichTab == 3)||(tList[0].whichTab == 4))))
//	{
//		//CA("26");
//		if(IsDeleteFlagSelected == kTrue && tList[0].parentId != -1 && tList[0].whichTab != 5 && (isInterFaceCall == kFalse))
//		{
//			bool16 ISProdDeleted = kFalse;
//			/*bool16 isONEsource = kFalse;
//			isONEsource = ptrIAppFramework->get_isONEsourceMode();*/
//			if(isONEsource == kFalse)
//			{
//				if(tList[0].imgFlag == 1 && tList[0].isAutoResize != 0 && tList[0].isAutoResize != 1 && tList[0].isAutoResize != 2)
//				{
//					ISProdDeleted  = kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].isAutoResize);
//				}
//				else
//				{
//					ISProdDeleted  = kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
//				}
//			}
//			if(ISProdDeleted)
//			{
//				bool16 alreadyExist = kFalse;
//				if(tList[0].whichTab == 3)
//				{
//					//CA("27");
//					int32 productID = tList[0].parentId;
//					for(int index = 0; index<productsDeleted.size();index++ )
//					{
//						if(productsDeleted[index] == productID)
//						alreadyExist = kTrue;
//					}
//
//					if(tList[0].imgFlag == 0)
//					{
//						productsDeleted.push_back(productID);
//					}
//					if(tList[0].imgFlag == 1)
//					{
//						productImagesDeleted.push_back(productID);
//					}
//					if(isDummyCall){ //*** For Refresh Report
//						BookReportData::deletedItemTagList.push_back(tList);	
//					}
//				}
//				if(tList[0].whichTab == 4)
//				{
//				//	CA("28");
//					int32 itemID = tList[0].parentId;
//					if(tList[0].imgFlag == 0)
//					{	
//						for(int index = 0; index<itemsDeleted.size();index++ )
//						{
//							if(itemsDeleted[index] == itemID)
//							alreadyExist = kTrue;
//						}
//						itemsDeleted.push_back(itemID);
//					}
//
//					if(tList[0].imgFlag == 1)
//						itemImagesDeleted.push_back(itemID);
//					if(isDummyCall){//*** For Refresh Report
//						BookReportData::deletedItemTagList.push_back(tList);	
//					}
//				}				
//				if(!isDummyCall) //*** For Refresh Report
//
//					this->deleteThisBox(boxID);								
//				
//				return kFalse;
//			}
//		}
//	}
//
//	int32 si3= (int32)tList.size();
//	bool16 IsMMYTableRefreshed = kFalse;
//	
//	
//	for(int j=0; j < tList.size(); j++)
//	{
//		
//		//======================================22-05-2012=================
//
//		int32 childTagCount = tList[j].tagPtr->GetChildCount();
//				for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
//				{
//					XMLReference childTagRef = tList[j].tagPtr->GetNthChild(childTagIndex);
//					//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
//					if(childTagXMLElementPtr == nil)
//					continue;
//			        //PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4  parentID
//					PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));
//					int32 childId22 = strchildId22.GetAsNumber();
//
//					int32 elementCount=childTagXMLElementPtr->GetChildCount();
//					for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
//						{
//						     XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
//						     InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
//							 if(childElement == nil)
//								continue;
//								PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
//								int32 childId11 = strchildId11.GetAsNumber();
//
//								PMString strattibute11 = childElement->GetAttributeValue(WideString("ID"));//Cs4
//								int32 attribute11 = strattibute11.GetAsNumber();
//
//								PMString strIndex = childElement->GetAttributeValue(WideString("index")); //Cs4
//							int32 index = strIndex.GetAsNumber();
//
//							
//						PMString q("childId: ");
//						q.AppendNumber(childId11);
//						q.Append("\n");
//						q.Append("Index :");
//						q.AppendNumber(index);
//						//CA(q);
//						
//
//								int32 sizeofvec1 =static_cast<int32>(ItemIdsForIndesignTable.size());
//								int32 sizeofvec2 =static_cast<int32>(AttibuteIdsForIndesignTable.size());
//								bool16 flag1 = kFalse;
//								if(index==4)
//								{
//								if(childId11!=-1)
//								{
//									if(sizeofvec1>0)
//									{
//										/*bool16 flag1 = kFalse;*/
//										for(int32 i=0;i<sizeofvec1;i++)
//										{
//											if(ItemIdsForIndesignTable.at(i)==childId11)
//												flag1=kTrue;
//										}
//										if(flag1==kFalse)
//											ItemIdsForIndesignTable.push_back(childId11);
//									}
//									if(sizeofvec2>0)
//									{
//										bool16 flag2 = kFalse;
//										for(int32 i=0;i<sizeofvec2;i++)
//										{
//											if(AttibuteIdsForIndesignTable.at(i)==attribute11)
//												flag2=kTrue;
//											
//										}
//										if(flag2==kFalse)
//											AttibuteIdsForIndesignTable.push_back(attribute11);
//									}
//									if(sizeofvec1==0)
//										ItemIdsForIndesignTable.push_back(childId11);
//									if(sizeofvec2==0)
//										AttibuteIdsForIndesignTable.push_back(attribute11);
//								}
//								}
//					}
//				
//				}
////===========================END=======================================
//		if(tList[j].parentId==-1)
//		{
//			continue;
//		}
//		bool16 alreadyExist = kFalse;
//		if(isInterFaceCall == kFalse)
//		{
//			if(tList[j].whichTab == 3 && tList[j].header != 1)
//			{
//				int32 productID = tList[j].parentId;
//				for(int index = 0; index<productsUpdated.size();index++ )
//				{
//					if(productsUpdated[index] == productID)
//						alreadyExist = kTrue;
//				}
//				//if(!alreadyExist && tList[j].imgFlag ==0) //******* 15-MARCH
//				//{
//				//	//CA("1");
//				//	//productsUpdated.push_back(productID);
//				//}
//
//				if(tList[j].imgFlag==0)
//				{
//					Report objReport;
//					objReport.attributeId = tList[j].elementId;
//					objReport.id = tList[j].parentId;
//					tempProductList.push_back(objReport);
//				}
//				if(tList[j].imgFlag==1)
//					productImagesReLinked.push_back(productID);
//			}
//			if(tList[j].whichTab == 3 && tList[j].header == 1 && tList[j].typeId == -5)
//			{
//				int32 productID = tList[j].parentId;
//				for(int index = 0; index<productsUpdated.size();index++ )
//				{
//					if(productsUpdated[index] == productID)
//					alreadyExist = kTrue;
//				}
//				if(!alreadyExist && tList[j].imgFlag ==0){	
//					productsUpdated.push_back(productID);				
//				}
//			}
//
//			if(tList[j].whichTab == 4 && tList[j].header != 1/*tList[j].typeId != -2*/ && tList[j].tableType != 2/*tList[j].typeId != -3*/ && tList[j].typeId != -5)
//			{
//				int32 itemID = -1;
//				if(tList[j].childTag == 1)
//					itemID = tList[j].childId;
//				else
//					itemID = tList[j].parentId;
//					
//				int32 itemParentID =tList[j].parentId;
//					
//				for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
//				{
//					if(itemsUpdated[index] == itemID)
//						alreadyExist = kTrue;
//					/*if(upDatedItemParentID[index] == itemParentID)
//						alreadyExist = kTrue;*/
//				}
//
//				//if(!alreadyExist && tList[j].typeId!= -99 && tList[j].imgFlag ==0)
//				//{	
//				//	//CA("A8");
//				//	
//				//	//itemsUpdated.push_back(itemID);
//				//	//upDatedItemParentID.push_back(itemParentID);
//				//}
//				
//				if(tList[j].imgFlag==0)
//				{
//					Report objReport;
//					objReport.attributeId = tList[j].elementId;
//					objReport.id = tList[j].typeId;
//
//					tempItemList.push_back(objReport);
//				}
//				if(tList[j].imgFlag==1 && isupdateImageSelected)
//				{
//					itemImagesReLinked.push_back(itemID);
//				}
//			}
//
//			
//			if(tList[j].whichTab == 4 && tList[j].header == 1 && tList[j].tableType == 1 && tList[j].elementId == -101)
//			{
//				//CA("A11");
//				int32 itemID = tList[j].parentId;
//													
//				for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
//				{
//					if(itemsUpdated[index] == itemID)
//						alreadyExist = kTrue;				
//				}
//
//				if(!alreadyExist && tList[j].typeId!= -99 && tList[j].imgFlag ==0)
//				{	
//					itemsUpdated.push_back(itemID);					
//				}			
//			}
//			//if(tList[j].whichTab == 4 && (/*tList[j].typeId == -3*/tList[j].tableType == 2 || tList[j].typeId == -5))/*typeId == -5 For CustomTabbedText*/
//			//{
//			//
//			//}
//
//		}
//		if(tList[j].imgFlag==1)
//		{
//			for(int32 index = 0 ; index < BookReportData::vecReportRows.size() ; index++)
//			{
//				if(BookReportData::vecReportRows[index].oneRawData.itemID == tList[j].parentId)
//				{
//					isNewItem = kFalse;
//					break;
//				}
//				else{
//
//					isNewItem = kTrue;
//				}
//			}
//			if(isInlineImage)
//			{
//				XMLContentReference contentRef = tList[j].tagPtr->GetContentReference();																									
//				UIDRef ContentRef = contentRef.GetUIDRef();				
//				fillImageInBox(ContentRef, tList[j], tList[j].parentId, imagePath, isInterFaceCall);
//				if(shouldPushInFreameVect == kTrue && shouldRefreshFlag == kFalse){
//					break;
//				}
//			}
//			else{
//				fillImageInBox(boxID, tList[j], tList[j].parentId, imagePath, isInterFaceCall);
//			}
//		}
//		else if(tList[j].imgFlag==0)
//		{	
//			
//			//DebugTestAlert : end Print the object id
//			if(tList[j].isTablePresent == 1 && tList[j].tableType == 2 && tList[j].rowno == -904)
//			{
//				if(!IsMMYTableRefreshed)
//				{
//					RefreshMMYCustomTable(boxID, tList[j]);
//					IsMMYTableRefreshed = kTrue;
//				}				
//			}
//			else if(tList[j].isTablePresent == 1 && tList[j].tableType == 2/*tList[j].typeId == -3*/)
//			{
//				RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(boxID, tList[j]);
//			}
//			else if(tList[j].typeId == -5 || tList[j].elementId==-101)/*typeId == -5 For CustomTabbedText*/
//			{
//				RefreshCustomTabbedTextNewoption(boxID, tList[j]);//RefreshCustomTabbedTextNewoption
//			}
//			else if(tList[j].tableType == 7/*tList[j].typeId == -116*/)
//			{
//				RefreshAllStandardTables(boxID, tList[j]);
//			}
//			else
//			{	
//				customTableRefreshByCellNewoption(boxID, tList[j], tList[j].parentId);
//			}
//		}		
//		
//        vector_iterater++;
//	}
//
//	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//	{	
//		tList[tagIndex].tagPtr->Release();
//	}
//	for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
//	{
//		tList_checkForHybrid[tagIndex].tagPtr->Release();
//	}
//	return kTrue;
//}


bool16 Refresh::refreshThisBox(UIDRef& boxID, PMString& imagePath, bool16 isInterFaceCall)
{
	//CA("refreshThisBox");

	//CA("inside Refresh::refreshThisBox");
	//*******
		//******Checking For Is InlineImage. 	
	/*  **************************************	
		*	Checking Here For IsImageInline means, is There any
		*	Image Inside the TxtFrame, if True Then Set isInlineImage flag == kTrue;		
		****************************************	
	*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());	
	do
	{		
		if (graphicFrameDataOne) 
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		if (textFrameUID == kInvalidUID)			
			break;
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA(graphicFrameHierarchy);
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!graphicFrameHierarchy");
			break;
		}
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!multiColumnItemHierarchy");
			break;
		}
	
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {				
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			break;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!frameItemHierarchy");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) 
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!textFrame");
			break;
		}
	
		InterfacePtr<ITextModel> textModel1(textFrame->QueryTextModel());
		if (textModel1 == NULL)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::CDataSprayer::sprayForThisBox::!textModel");	
			break;
		}

		IDataBase* db = ::GetDataBase(textModel1);
		OwnedItemDataList ownedList;
		Utils<ITextUtils>()->CollectOwnedItems(textModel1,
											0,
											textModel1->GetPrimaryStoryThreadSpan(), 
											&ownedList,
											kTrue);		
		isInlineImage = kFalse;
		for (int32 i = 0; i < ownedList.size(); i++)
		{
			if ((ownedList[i].fClassID == kInlineBoss))
			{	
				//CA("isInlineImage = kTrue");
				isInlineImage = kTrue;					
				break;
			}
		}	
	}while(kFalse);
	//******
	//*******
	TagList tList,tList_checkForHybrid, hybridTagList;
	//TagReader tReader;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return kFalse;
	}

	//ptrIAppFramework->clearAllStaticObjects();
	

	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshThisBox::tagreader nil ");
		return kFalse;
	}
	
	bool16 isONEsource = kFalse;
	isONEsource = ptrIAppFramework->get_isONEsourceMode();
	//********Added For checkbox Selection of RelinkinImage
	bool16 isupdateImageSelected = kTrue;
	if(!isDummyCall){
		if(Mediator::catalogRefrshPanelControlData != nil )
		{
			IControlView* RfhDlgUpdateImagesChkBxView = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhUpdateImagesCheckBoxWidgetID);
			if(!RfhDlgUpdateImagesChkBxView)
			{		
				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::RfhDlgUpdateImagesChkBxView nil ");
				return kFalse;
			}

			InterfacePtr<ITriStateControlData>updateImagesCheckBoxTriState(RfhDlgUpdateImagesChkBxView,UseDefaultIID());
			if(updateImagesCheckBoxTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::updateImagesCheckBoxTriState nil ");
				return kFalse;
			}

			isupdateImageSelected = updateImagesCheckBoxTriState->IsSelected();	

			//by Amarjit
			/*IControlView* newUpdateImagesCheckBoxView = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhnewUpdateImagesCheckBoxWidgetID);
			if(!newUpdateImagesCheckBoxView)
			{		
				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::RfhDlgUpdateImagesChkBxView nil ");
				return kFalse;
			}

			InterfacePtr<ITriStateControlData>updateitemgroupImagesCheckBoxTriState(newUpdateImagesCheckBoxView,UseDefaultIID());
			if(updateitemgroupImagesCheckBoxTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::refreshThisBox::updateImagesCheckBoxTriState nil ");
				return kFalse;
			}

			isupdateitemgroupSelected = updateitemgroupImagesCheckBoxTriState->IsSelected();	*/
		}
	}
	//********upto Here
	if(!isInterFaceCall)
	{
		//CA("!isInterFaceCall");
		tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
		if(tList_checkForHybrid.size() == 0)
		{
			/*	CA("tList_checkForHybrid");*/
			return kTrue;
		}
		if(refreshTableByAttribute && tList_checkForHybrid[0].tableType != 3 && tList_checkForHybrid[0].dataType !=6){
			//CA("True");//getTagsFromBox
			tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);//original
			
            //if(tList_checkForHybrid[0].tableType == 3)
            //{
            //    hybridTagList = itagReader->getTagsFromBox_ForRefresh(boxID);
            //    for(int32 l=0 ; l < hybridTagList.size(); l++){
            //        tList.push_back(hybridTagList[l]);
            //    }
                
            //}
		}
		else{
			//CA("False");
			tList=itagReader->getTagsFromBox_ForRefresh(boxID);
		}

		for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
		{
			tList_checkForHybrid[tagIndex].tagPtr->Release();
		}
	}
	else
		tList=itagReader->getTagsFromBox_ForRefresh(boxID);

	if(tList.size()==0)
	{	
		//CA(" Refresh::refreshThisBox  tList.size()==0 ");
		tList=itagReader->getFrameTags(boxID);
		if(tList.size()==0)
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::Refresh::refreshThisBox::tList.size()==0");		
			return kTrue;
		}
		//CA("11.5");
		refreshTaggedBox(boxID, imagePath, isInterFaceCall);
		return kTrue;
	}

	BookReportData::langaugeID = tList[0].languageID; //For Report


	if(tList[0].isTablePresent == 1 && tList[0].tableType != 2 && !(refreshTableByAttribute && tList[0].tableType == 3))
	{	
		//CA(" Refresh::refreshThisBox - Going fro refresh Tagged box ");
		typeID11 = tList[0].typeId;
		if(IsDeleteFlagSelected == kTrue && tList[0].parentId != -1 && (isInterFaceCall == kFalse))
		{
			//CA("13.1");
			bool16 ISProdDeleted = kFalse;
			if(isONEsource == kFalse)
			{
				//CA("if(isONEsource == kFalse)");
				ISProdDeleted  = kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
			}
			if(ISProdDeleted)
			{
				//CA(" product deleted Refresh::refreshThisBox 745 return ");
				bool16 alreadyExist = kFalse;
				if(tList[0].whichTab == 3)
				{
					//CA("tList[0].whichTab == 3");
					double productID = tList[0].parentId;
							
					for(int index = 0; index<productsDeleted.size();index++ )
					{
						if(productsDeleted[index] == productID)
							alreadyExist = kTrue;
					}
					//if(!alreadyExist)
						productsDeleted.push_back(productID);
					if(isDummyCall){ //*** For Refresh Report
						BookReportData::deletedItemTagList.push_back(tList);	
					}
				}

				if(tList[0].whichTab == 4)
				{
					double itemID = tList[0].parentId;
							
					for(int index = 0; index<itemsDeleted.size();index++ )
					{
						if(itemsDeleted[index] == itemID)
							alreadyExist = kTrue;
					}

					//if(!alreadyExist)
						itemsDeleted.push_back(itemID);
					
					if(isDummyCall){ //*** For Refresh Report
						BookReportData::deletedItemTagList.push_back(tList);	
					}
				}			
				/*if(!isDummyCall)
					this->deleteThisBox(boxID);*/

				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
		refreshTaggedBox(boxID, imagePath, isInterFaceCall);
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		return kTrue;
	}
	if((tList[0].imgFlag == 0) || ((tList[0].imgFlag == 1) && ((tList[0].whichTab == 3)||(tList[0].whichTab == 4))))
	{
		if(IsDeleteFlagSelected == kTrue && tList[0].parentId != -1 && tList[0].whichTab != 5 && (isInterFaceCall == kFalse))
		{
			//CA("13.2");
			bool16 ISProdDeleted = kFalse;
			/*bool16 isONEsource = kFalse;
			isONEsource = ptrIAppFramework->get_isONEsourceMode();*/
			if(isONEsource == kFalse)
			{
				if(tList[0].imgFlag == 1 && tList[0].isAutoResize != 0 && tList[0].isAutoResize != 1 && tList[0].isAutoResize != 2)
				{
					ISProdDeleted  =  kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].isAutoResize);
				}
				else
				{
					ISProdDeleted  =  kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
				}
			
			}
			if(ISProdDeleted)
			{
				//CA(" product deleted Refresh::refreshThisBox 913 return ");
				bool16 alreadyExist = kFalse;
				if(tList[0].whichTab == 3)
				{
					double productID = tList[0].parentId;
							
					for(int index = 0; index<productsDeleted.size();index++ )
					{
						if(productsDeleted[index] == productID)
							alreadyExist = kTrue;
					}

					//if(!alreadyExist)
						//productsDeleted.push_back(productID);

					if(tList[0].imgFlag == 0)
					{
						productsDeleted.push_back(productID);
					}
					if(tList[0].imgFlag == 1)
					{
						productImagesDeleted.push_back(productID);
					}
					if(isDummyCall){ //*** For Refresh Report
						BookReportData::deletedItemTagList.push_back(tList);	
					}
				}

				if(tList[0].whichTab == 4)
				{
					//CA("11111  tList[0].whichTab == 4");
					
					double itemID = tList[0].parentId;

					if(tList[0].imgFlag == 0)
					{	
						for(int index = 0; index<itemsDeleted.size();index++ )
						{
							if(itemsDeleted[index] == itemID)
								alreadyExist = kTrue;
						}

						//if(!alreadyExist)
						itemsDeleted.push_back(itemID);
					}

					if(tList[0].imgFlag == 1)
					{
						itemImagesDeleted.push_back(itemID);
					}
					if(isDummyCall){//*** For Refresh Report
						BookReportData::deletedItemTagList.push_back(tList);	
					}
				}
								
				//if(!isDummyCall) //*** For Refresh Report
				//	this->deleteThisBox(boxID);		
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
	}
	bool16 IsMMYTableRefreshed = kFalse;

	int32 si3= (int32)tList.size();
	for(int j=0; j < tList.size(); j++)
	{
		if(tList[j].parentId==-1)
		{
			continue;
		}

		if(!shouldRefresh(tList[j], boxID.GetUID(), kFalse))
			{//CA("return shouldREefresh");
				continue;
			}

		bool16 alreadyExist = kFalse;
		
		if(isInterFaceCall == kFalse)
		{
			if(tList[j].whichTab == 3 && tList[j].header != 1)
			{
				double productID = tList[j].parentId;
				
				for(int index = 0; index<productsUpdated.size();index++ )
				{
					if(productsUpdated[index] == productID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist && tList[j].imgFlag ==0) //******* 15-MARCH
				{
					//CA("3");
					//productsUpdated.push_back(productID);
				}

				if(tList[j].imgFlag==0)
				{
					Report objReport;
					objReport.attributeId = tList[j].elementId;
					objReport.id = tList[j].parentId;
					tempProductList.push_back(objReport);
				}

				if(tList[j].imgFlag==1)
				{
					//CA("A5");
					productImagesReLinked.push_back(productID);
				}
			}

			if(tList[j].whichTab == 3 && tList[j].header == 1 && tList[j].typeId == -5)
			{
				int32 productID = tList[j].parentId;
				
				for(int index = 0; index<productsUpdated.size();index++ )
				{
					if(productsUpdated[index] == productID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist && tList[j].imgFlag ==0){		//2/MAR/2010				
					productsUpdated.push_back(productID);				
				}
			}

			if(tList[j].whichTab == 4 && tList[j].header != 1/*tList[j].typeId != -2*/ && tList[j].tableType != 2/*tList[j].typeId != -3*/ && tList[j].typeId != -5)
			{
				//CA("A7");
				double itemID = -1;
				if(tList[j].childTag == 1)
					itemID = tList[j].childId;
				else
					itemID = tList[j].parentId;
					
				double itemParentID =tList[j].parentId;

				
				for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
				{
					if(itemsUpdated[index] == itemID)
						alreadyExist = kTrue;
					/*if(upDatedItemParentID[index] == itemParentID)
						alreadyExist = kTrue;*/
				}
				if(!alreadyExist && tList[j].typeId!= -99 && tList[j].imgFlag ==0)
				{					
					//CA("D");
					//itemsUpdated.push_back(itemID);
					//upDatedItemParentID.push_back(itemParentID);
				}
				
				PMString asd;
				asd.Append("ItemId =  ");
				asd.AppendNumber(itemID);
				asd.Append("\n itemsUpdated = ");
				asd.AppendNumber(static_cast<int32>(itemsUpdated.size()));
				//CA(asd); 
				if(tList[j].imgFlag==0)
				{
					Report objReport;
					objReport.attributeId = tList[j].elementId;
					objReport.id = tList[j].typeId;

					tempItemList.push_back(objReport);
				}
				if(tList[j].imgFlag==1 && isupdateImageSelected)
				{
					itemImagesReLinked.push_back(itemID);
				}
				
			}
			if(tList[j].whichTab == 4 && tList[j].header == 1 && tList[j].tableType == 1 && tList[j].elementId == -101)
			{
				double itemID = tList[j].parentId;
													
				for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
				{
					if(itemsUpdated[index] == itemID)
						alreadyExist = kTrue;				
				}

				if(!alreadyExist && tList[j].typeId!= -99 && tList[j].imgFlag ==0)
				{			
					itemsUpdated.push_back(itemID);					
				}			
			}
			//if(tList[j].whichTab == 4 && (/*tList[j].typeId == -3*/tList[j].tableType == 2 || tList[j].typeId == -5))/*typeId == -5 For CustomTabbedText*/
			//{
			//	
			//	
			//}
		}
	
		if(tList[j].imgFlag==1)
		{
			//CA("A12");
			for(int32 index = 0 ; index < BookReportData::vecReportRows.size() ; index++)
			{
				if(BookReportData::vecReportRows[index].oneRawData.itemID == tList[j].parentId)
				{
					isNewItem = kFalse;
					break;
				}
				else{
					isNewItem = kTrue;
				}
			}
			if(isInlineImage)
			{
				//CA("A13");
				XMLContentReference contentRef = tList[j].tagPtr->GetContentReference();																									
				UIDRef ContentRef = contentRef.GetUIDRef();				
				fillImageInBox(ContentRef, tList[j], tList[j].parentId, imagePath, isInterFaceCall);
				if(shouldPushInFreameVect == kTrue && shouldRefreshFlag == kFalse){
					//CA("Break");
					break;
				}
			}
			else{
				fillImageInBox(boxID, tList[j], tList[j].parentId, imagePath, isInterFaceCall);
			}
		}
		else if(tList[j].imgFlag==0 && tList[j].elementId!=-101)
		{	
			//CA("if(tList[j].imgFlag==0");
			//DebugTestAlert : end Print the object id
			if(tList[j].isTablePresent == 1 && tList[j].tableType == 2 && tList[j].rowno == -904)
			{
				//CA("RefreshMMYCustomTable");
				if(!IsMMYTableRefreshed)
				{
					//CA("goig to RefreshMMYCustomTable");
					RefreshMMYCustomTable(boxID, tList[j]);
					IsMMYTableRefreshed = kTrue;
					//if(tList[j].isAutoResize == 2)
					//{
					//	//CA("Inside overflow");
					//	ThreadTextFrame ThreadObj;
					//	ThreadObj.DoCreateAndThreadTextFrame(boxID);
					//}
				}				
			}
			else if(tList[j].isTablePresent == 1 && tList[j].tableType == 2/*tList[j].typeId == -3*/)
			{
				//CA("calling RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion");
				//RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable(boxID, tList[j]);
				RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(boxID, tList[j]);
			}
			else if(tList[j].typeId == -5 /*|| tList[j].elementId==-101*/)/*typeId == -5 For CustomTabbedText*/
			{///refresh item copy attributes for sectionlevel item or product
				RefreshCustomTabbedText(boxID, tList[j]);
			}
			else if(tList[j].tableType == 7/*tList[j].typeId == -116*/)
			{
				//CA("Refresh All Standrd Tables");
				RefreshAllStandardTables(boxID, tList[j]);
			}
            else if(tList[j].isTablePresent == 1 && tList[j].tableType == 3)
            {
                refreshAdvancedTableByCell(boxID, tList[j]);
                
            }
            else if(tList[j].dataType == 6 )
            {///refresh item copy attributeGroup for sectionlevel item 
                RefreshAttributeGroupInTabbedText(boxID, tList[j]);
            }
			else
			{	
				//CA("customTableRefreshByCell");
            	//fillDataInBox(boxID, tList[j], tList[j].parentId);	
				customTableRefreshByCell(boxID, tList[j], tList[j].parentId);
			}

			if( tList[j].isAutoResize == 1)
			{
				//CA("3k");
				/*UIDList itemList(boxID.GetDataBase(),boxID.GetUID());
				UIDList processedItems;
				K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
				ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);*/
				/*InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
				ASSERT(fitFrameToContentCmd != nil);
				if (fitFrameToContentCmd == nil) {
				return kFalse;
				}
				fitFrameToContentCmd->SetItemList(UIDList(boxID));
				if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
				ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
				return kFalse;
				}*/
			}		
		}	
		else if(tList[j].imgFlag==0)
		{	
			
			//DebugTestAlert : end Print the object id
			if(tList[j].isTablePresent == 1 && tList[j].tableType == 2 && tList[j].rowno == -904)
			{
				if(!IsMMYTableRefreshed)
				{
					RefreshMMYCustomTable(boxID, tList[j]);
					IsMMYTableRefreshed = kTrue;
				}				
			}
			else if(tList[j].isTablePresent == 1 && tList[j].tableType == 2/*tList[j].typeId == -3*/)
			{
				RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(boxID, tList[j]);
			}
			else if(tList[j].typeId == -5 && tList[j].elementId==-101)/*typeId == -5 For CustomTabbedText*/
			{
				RefreshCustomTabbedText(boxID, tList[j]);//RefreshCustomTabbedTextNewoption
			}
			else if(tList[j].typeId == -5 && tList[j].elementId==-101)/*typeId == -5 For CustomTabbedText*/
			{
				RefreshCustomTabbedText(boxID, tList[j]);//RefreshCustomTabbedTextNewoption
			}
			else if(tList[j].tableType == 7/*tList[j].typeId == -116*/)
			{
				RefreshAllStandardTables(boxID, tList[j]);
			}
			else
			{	
				customTableRefreshByCell(boxID, tList[j], tList[j].parentId);
			}
		}	
				
		//if(shouldPushInFreameVect == kTrue && shouldRefreshFlag == kFalse){
		//	//CA("Got the problem");
		//	break;
		//}
        vector_iterater++;
	}

//PMString updated = "";
//updated.Append("productsUpdated = ");
//updated.AppendNumber(static_cast<int32>(productsUpdated.size()));
//updated.Append("\nitemsUpdated = ");
//updated.AppendNumber(static_cast<int32>(itemsUpdated.size()));
//CA(updated);
	//------lalit------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	/*for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
	{
		tList_checkForHybrid[tagIndex].tagPtr->Release();
	}*/
	return kTrue;
}

bool16 Refresh::refreshTaggedBox(UIDRef& boxID, PMString& imagePath, bool16 isInterFaceCall)
{
	//CA(__FUNCTION__);
	//CA("Refresh::refreshTaggedBox");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return kFalse;
	}

	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::!itagReader");		
		return kFalse;
	}
//CA("1");
	TagList tList;
	TableUtility tUtility;
	tList=itagReader->getFrameTags(boxID);
	UIDRef tableUIDRef;
	if(tList.size()==0 /*|| tList.size()>1*/)
	{
		//CA("1613 return kFalse");
		return kFalse;
	}
//CA("2");
	TagList NewTAGList;
	if(tList[0].parentId==-1 && tList[0].typeId == -1)
	{   
		//CA("tList[0].parentId==-1 && tList[0].typeId == -1");
		NewTAGList = itagReader->getTagsFromBox(boxID);
		if(NewTAGList.size()==0 /*|| NewTAGList.size()>1*/)
		{
			//CA("NewTAGList.size()==0");
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
			{
				tList[tagIndex].tagPtr->Release();
			}			
			return kFalse;
		}
		
		//CA(NewTAGList[0].tagPtr->GetTagString());	
		if(!isInterFaceCall)
		{
			if(!shouldRefresh(NewTAGList[0], boxID.GetUID(), kTrue))
			{//CA("return shouldREefresh

				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
				{
					NewTAGList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
        
        if(NewTAGList[0].tableType == 9 && NewTAGList[0].dataType == 6)
        {
            //CA("ewTAGList[0].tableType == 9 && NewTAGList[0].dataType == 6");
            ErrorCode err= kFailure;
            
            err = tUtility.SprayItemAttributeGroupInsideTable(boxID,NewTAGList[0]);

            for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
            {
                tList[tagIndex].tagPtr->Release();
            }
            for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
            {
                NewTAGList[tagIndex].tagPtr->Release();
            }
            
            if(err)
                return kTrue;
            else
                return kFalse;
        }

		if(NewTAGList[0].tableType == 2/*NewTAGList[0].typeId== -3*/)
		{	
			//CA("NewTAGList[0].tableType== 2");
			ErrorCode err= kFailure;
			/*for(int32 p=0; p<NewTAGList.size(); p++)
			{*/
				//CA("we are here");
				//err = this->RefreshUserTableAsPerCellTagsForOneItem(boxID, NewTAGList[0]);
				//err = RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable(boxID,NewTAGList[0]);
				err = RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(boxID,NewTAGList[0]);
	
				
			/*}*/
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
				{
					NewTAGList[tagIndex].tagPtr->Release();
				}

			if(err)
					return kTrue;
				else
					return kFalse;
		}


		if(NewTAGList[0].dataType == 4)
		{
			PMString tempBuffer("");
		if(issrtucturebuttenselected==kTrue)
		{
			//CA("In Structure Option");
			if(NewTAGList[0].whichTab==4)
			{
				//CA("@@");
				VectorScreenTableInfoPtr tableInfo = NULL;

				//PMString temp("NewTAGList[0].sectionID =");
				//temp.AppendNumber(NewTAGList[0].sectionID);
				//temp.Append("NewTAGList[0].parentId");
				//temp.AppendNumber(NewTAGList[0].parentId);
				//CA(temp);

				if(NewTAGList[0].parentId != -1)

					tableInfo =ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(NewTAGList[0].parentId , NewTAGList[0].sectionID  ,NewTAGList[0].languageID);
				//else
				//	tableInfo =ptrIAppFramework->GETONEsourceObjects_getItemTablesByItenId(NewTAGList[0].parentId);

				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
					{
						tList[tagIndex].tagPtr->Release();
					}
					for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
					{
						NewTAGList[tagIndex].tagPtr->Release();
					}
					return kFalse;
				}

				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it12;
				for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
				{	
					oTableValue = *it12;
					if(NewTAGList[0].typeId == oTableValue.getTableTypeID())
					{					
						tempBuffer = oTableValue.getName();
						//CA("tempBuffer = " + tempBuffer);
						break;
					}
				}
				if(tableInfo)
					delete tableInfo;
			}
			
			
			if(NewTAGList[0].whichTab==3)
			{
				VectorScreenTableInfoPtr tableInfo = NULL;
				CItemTableValue oTableValue;
				//PMStringVecPtr otherLanguageValue;
				do
				{
				//tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.typeId);
					double sectionid=NewTAGList[0].sectionID;
					double eleID=NewTAGList[0].parentId;
					
				tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(NewTAGList[0].sectionID, NewTAGList[0].parentId, NewTAGList[0].languageID, kFalse);
				if(!tableInfo)
				{
					//CA("!tableInfo");
					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
					break;
				}
				if(tableInfo->size()==0)
				{
					//CA(" tableInfo->size()==0");
					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
					break;
				}
				VectorScreenTableInfoValue::iterator it;
				PMStringVec::iterator strIt;
				bool16 typeidFound=kFalse;
				
				double tableId = 0;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{
					//CA("for Loop");
					oTableValue = *it;
					double tttt=oTableValue.getTableID();
					PMString w1("getTableID() :");
					w1.AppendNumber(tttt);
					//CA(w1);
					PMString w2("tableId :");
					w2.AppendNumber( NewTAGList[0].tableId);
					//CA(w2);

					if(oTableValue.getTableID() ==  NewTAGList[0].tableId)
					{   
						//CA("typeidFound=kTrue;");
						typeidFound=kTrue;
						tableId = oTableValue.getTableID();
						tempBuffer = oTableValue.getName();
						break;
					}
				}
				if(typeidFound)
				{
					//CA("typeidFound");
					//tempBuffer = ptrIAppFramework->GetProduct_getItemByTableIDName(tableId);
				}
				else
				{
					//CA("NOT FOUND");
					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
				}
			}while(0);
			if(tableInfo)
			{
				//CA("DELETIN TABLE INFO");
				delete tableInfo;
				tableInfo = NULL;
			}
		}
		}
		
			if(iscontentbuttonselected==kTrue)
			{
				//CA("In Content Option");
				
				VectorScreenTableInfoPtr tableInfo = NULL;

					if(NewTAGList[0].parentId != -1)
						{
						//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(NewTAGList[0].parentId);
						}
					//else
					//	tableInfo =ptrIAppFramework->GETONEsourceObjects_getItemTablesByItenId(NewTAGList[0].parentId);

					if(!tableInfo)
					{
						//CA("if(!tableInfo)");
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
						return kFalse;
					}
					//CA("outside");

					CItemTableValue oTableValue;
					VectorScreenTableInfoValue::iterator it12;
					for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
					{	
						
						oTableValue = *it12;
						if(NewTAGList[0].tableId == oTableValue.getTableID())
						{	
							//CA("123333");
							tempBuffer = oTableValue.getName();
							//CA("tempBuffer = " + tempBuffer);
							break;
						}
					}
					if(tableInfo)
						delete tableInfo;
				
			}
		
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textFrameUID == kInvalidUID");
				return kFalse;
			}
			

			InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
			if (graphicFrameHierarchy == nil) 
			{
				//CA("graphicFrameHierarchy is NULL");
				return kFalse;
			}
							
			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
			if (!multiColumnItemHierarchy) {
				//CA("multiColumnItemHierarchy is NULL");
				return kFalse;
			}

			InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
			if (!multiColumnItemTextFrame) {
				//CA("multiColumnItemTextFrame is NULL");
				return kFalse;
			}
			InterfacePtr<IHierarchy>
			frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
			if (!frameItemHierarchy) {
				//CA("frameItemHierarchy is NULL");
				return kFalse;
			}

			InterfacePtr<ITextFrameColumn>
			frameItemTFC(frameItemHierarchy, UseDefaultIID());
			if (!frameItemTFC) {
				//CA("!!!ITextFrameColumn");
				return kFalse;
			}
			InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
			if (textModel == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CommonFunctions::RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable::textModel == nil");
				return kFalse;
			}

			int32 tagStartPos = -1;
			int32 tagEndPos = -1;
			Utils<IXMLUtils>()->GetElementIndices(NewTAGList[0].tagPtr,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			PMString pos("tagStartPos = ");
			pos.AppendNumber(tagStartPos);
			pos.Append(" , tagEndPos = ");
			pos.AppendNumber(tagEndPos);
			//CA(pos);

			PMString textToInsert("");
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;					
			}
			else
			{
				textToInsert=iConverter->translateString(tempBuffer);
				/*PMString txt("textToInsert :");
				txt.Append(textToInsert);
				CA(txt);*/
			}
			
			//WideString insertData(textToInsert);		
			//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData);
            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
			{
				NewTAGList[tagIndex].tagPtr->Release();
			}
			
			return kTrue;		
		}

		for(int32 tagIndex = 0 ; tagIndex < NewTAGList.size() ; tagIndex++)
		{
			NewTAGList[tagIndex].tagPtr->Release();
		}
	}

	//if(!isInterFaceCall)
	//{
	//	//CA("!isInterFaceCall");
	//	if(!shouldRefresh(tList[0], boxID.GetUID(), kTrue))
	//	{
	//		//CA("1785 return kFalse");	
	//		return kFalse;
	//	}
	//}
	
	//7Aug..Yogesh ItemLevelTable handling..
	//Only the outer if condition block is added as the itemtableframe was
	//getting deleted.
	if(tList[0].whichTab != 4 && tList[0].isTablePresent != kTrue && (isInterFaceCall == kFalse) )
	{
		//CA("tList[0].whichTab != 4 && tList[0].isTablePresent != kTrue");
		if(IsDeleteFlagSelected == kTrue && tList[0].parentId != -1)
		{	
			bool16 isONEsource = kFalse;
			bool16 ISProdDeleted = kFalse;
			isONEsource = ptrIAppFramework->get_isONEsourceMode();
			if(isONEsource == kFalse)
				 ISProdDeleted= kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
			if(ISProdDeleted)
			{	
				bool16 alreadyExist = kFalse;
				if(tList[0].whichTab == 3)
				{
					double productID = tList[0].parentId;
							
					for(int index = 0; index<productsDeleted.size();index++ )
					{
						if(productsDeleted[index] == productID)
							alreadyExist = kTrue;
					}

					//if(!alreadyExist)
						productsDeleted.push_back(productID);
				}

				if(tList[0].whichTab == 4)
				{
				//	CA("2222 tList[0].whichTab == 4");
					double itemID = tList[0].parentId;
							
					for(int index = 0; index<itemsDeleted.size();index++ )
					{
						if(itemsDeleted[index] == itemID)
							alreadyExist = kTrue;
					}

					//if(!alreadyExist)
						itemsDeleted.push_back(itemID);
				}

			//	PMString updated = "";
			//	updated.Append("productsDeleted = ");
			//	updated.AppendNumber(productsDeleted.size());
			//	updated.Append(" , itemsDeleted = ");
			//	updated.AppendNumber(itemsDeleted.size());

				//CA(updated);

				/*this->deleteThisBox(boxID);*/
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
	}
	//end //7Aug..Yogesh ItemLevelTable handling..
	//7Aug..Yogesh ItemLevelTable handling
	if(tList[0].whichTab == 4 && tList[0].isTablePresent == kTrue)
	{
		//CA("tList[0].whichTab == 4 && tList[0].isTablePresent == kTrue ");
		if((tList[0].typeId == -112) || (tList[0].typeId == -113) || (tList[0].typeId == -114))
		{
			//CA("Refreshing Component Table.");	
			
			UIDRef TableUIDRef ;
			if(tUtility.isTablePresent(boxID, TableUIDRef))
			{
				//CA("Got TableUIDRef");
				double tableId =0;
				bool16 isKitTable = kFalse;
				
				if(tList[0].typeId == -112)
					tUtility.fillDataInKitComponentTable( TableUIDRef, tList[0].parentId, tList[0], tableId, boxID, isKitTable);
				else if(tList[0].typeId == -113)
					tUtility.fillDataInXRefTable( TableUIDRef, tList[0].parentId, tList[0], tableId, boxID);
				else if(tList[0].typeId == -114)
					tUtility.fillDataInAccessoryTable( TableUIDRef, tList[0].parentId, tList[0], tableId, boxID);

			}
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
			{
				tList[tagIndex].tagPtr->Release();
			}
			return kFalse;
		}
		else if(tList[0].tableType == 3 && tList[0].whichTab == 4)///for hybrid Table
		{
			//frameDeleted = kFalse;
			UIDRef TableUIDRef ;
			if(tUtility.isTablePresent(boxID, TableUIDRef))
			{
				//CA("Add code here for item hybrid table refresh");
					
				//CA("3333 tList[0].whichTab == 4");
				double itemID = tList[0].parentId;
				bool16 alreadyExist = kFalse;
				for(int index = 0; index<itemsUpdated.size();index++ )
				{
					if(itemsUpdated[index] == itemID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist)
					itemsUpdated.push_back(itemID);
				
				IIDXMLElement *HybridTableTagPtr = tList[0].tagPtr;
				int32 childCount = HybridTableTagPtr->GetChildCount();
				double tableId = -1;
				if(childCount == 0)
				{
					//CA("childCount == 0");
					tableId = tList[0].typeId;
				}
				else
				{

					XMLReference tableXMLRef = HybridTableTagPtr->GetNthChild(0);
					//IIDXMLElement *PSHybridTableTagPtr = tableXMLRef.Instantiate();
					InterfacePtr<IIDXMLElement>PSHybridTableTagPtr(tableXMLRef.Instantiate());
						
					PMString tableIdStr = PSHybridTableTagPtr->GetAttributeValue(WideString("typeId")); //Cs4
					tableId= tableIdStr.GetAsDouble();
				}
				
                int answer=1;
                PMString message("Performing a STRUCTURE REFRESH will re-spray the table from the database and any existing styles applied to your table will be lost.  Proceed?");
                message.SetTranslatable(kFalse);
                
                answer=CAlert::ModalAlert(message, kOKString, kCancelString, kNullString, 1, CAlert::eQuestionIcon);
                if(answer==2)
                    return kFalse;

                
				//CA("before calling fillDataInHybridTable method========");
				tUtility.fillDataInHybridTable
				(TableUIDRef, tList[0].parentId, tList[0].typeId, tList[0].tableId, tList[0].imgFlag, tList[0], boxID);
				//CA("After calling fillDataInHybridTable method");
				
				
			}

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
			{
				tList[tagIndex].tagPtr->Release();
			}
			return kTrue;
			
		}
		
		TagStruct & tStruct = tList[0];
		IIDXMLElement * boxXMLPtr = tList[0].tagPtr;
		XMLReference xmlRef = boxXMLPtr->GetXMLReference();

		XMLTagAttributeValue xmlTagAttrVal;
		getAllXMLTagAttributeValues(xmlRef,xmlTagAttrVal);
		//start temporary adjustment in code
		//the typeId and the parentId should always be attached to the table and not
		//to its parent tag.but there is some logical error in the datasprayer plugin
		//while attaching tag to the table.The correct typeId and the parentId are getting
		//attached to the frametag instead of table tag.But it should be rectified 
		//immediately.
		PMString attrVal;
		attrVal.Append(xmlTagAttrVal.typeId);

		double typeId = attrVal.GetAsDouble();
		attrVal.Clear();

		//Note rowno stored the PBObjectID.
		attrVal.Append(xmlTagAttrVal.parentID/*pbObjectId*//*xmlTagAttrVal.rowno*/);
		double objectId = attrVal.GetAsDouble();
		attrVal.Clear();

		PMString colnoForTableFlag = xmlTagAttrVal.header; //xmlTagAttrVal.colno;
		//end temporary adjustment in code
		//Get the tableXMLPtr
		int32 childCount  = boxXMLPtr->GetChildCount();

		for(int32 tableIndex = 0;tableIndex < childCount;tableIndex++)
		{//start for 1
			XMLReference tableXMLElementRef = boxXMLPtr->GetNthChild(tableIndex);
			//IIDXMLElement * tableXMLPtr = tableXMLElementRef.Instantiate();
			InterfacePtr<IIDXMLElement>tableXMLPtr(tableXMLElementRef.Instantiate());
			
			if(tableXMLPtr == nil)
			{	
				continue;
			}

			//CA(tableXMLPtr->GetTagString());
			if(tableXMLPtr == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableXMLPtr == nil");					
				return kFalse;
			}
			//Get all the attribute Values
			PMString tempStr;
			tempStr.Clear();
			//tempStr.Append(tableXMLPtr->GetAttributeValue("parentID"));
			//int32 objectId = tempStr.GetAsNumber();
			//tempStr.Clear();
				
			//tempStr.Append(tableXMLPtr->GetAttributeValue("typeId"));
			//int32 typeId = tempStr.GetAsNumber();
			//tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.parentTypeID);
			double parentTypeId =tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.LanguageID);
			double languageId = tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.sectionID);
			double sectionId = tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.isAutoResize);
			int32 autoResize = tempStr.GetAsNumber();
			tempStr.Clear();

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return kFalse;
			}
			//ptrIAppFramework->clearAllStaticObjects();
			VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tStruct.parentId, tStruct.sectionID, tStruct.languageID); //objectId
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::refreshThisBox::tableInfo == nil");
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
			int32/*int64*/ numRows=0;
			int32/*int64*/ numCols=0;

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == tStruct.typeId && oTableValue.getTableID() == tStruct.tableId)
				{
					typeidFound=kTrue;				
					break;
				}
			}
			if(!typeidFound)
			{
				//CA("typeidFound == kFalse");
				if(!isInterFaceCall)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::typeidFound == nil");
					bool16 alreadyExist = kFalse;
					if(tList[0].whichTab == 3)
					{
						double productID = tList[0].parentId;
								
						for(int index = 0; index<productsDeleted.size();index++ )
						{
							if(productsDeleted[index] == productID)
								alreadyExist = kTrue;
						}
						//if(!alreadyExist)
							productsDeleted.push_back(productID);
					}

					if(tList[0].whichTab == 4)
					{
						//CA("3333 tList[0].whichTab == 4");
						double itemID = tList[0].parentId;								
						for(int index = 0; index<itemsDeleted.size();index++ )
						{
							if(itemsDeleted[index] == itemID)
								alreadyExist = kTrue;
						}
						//if(!alreadyExist)
							itemsDeleted.push_back(itemID);
					}

					//PMString updated = "";
					//updated.Append("productsDeleted = ");
					//updated.AppendNumber(productsDeleted.size());
					//updated.Append(" , itemsDeleted = ");
					//updated.AppendNumber(itemsDeleted.size());

					//CA(updated);

					//this->deleteThisBox(boxID);
				}
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}

			numRows = static_cast<int32>(oTableValue.getTableData().size());
			if(numRows<=0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
			numCols =static_cast<int32> (oTableValue.getTableHeader().size());
			if(numCols<=0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
				{
					tList[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}

			bool8 isTranspose = oTableValue.getTranspose();
			//Note that don't take the literal meaning of row and column
			//it has to be taken w.r.t the isTranspose flag.
			
			if(isTranspose == kFalse)
			{
				//rows are sprayed in rows of the table on document
				if(colnoForTableFlag == "1")
					numRows = numRows + 1;//add for header row
                else if(colnoForTableFlag == "2")
                    numRows = numRows + 2; // added for Header row + List Name row
                else if(colnoForTableFlag == "3")
                    numRows = numRows + 1; // added for List Name row only
			}
			else
			{
				//rows are sprayed in columns of the table on document
				//colums are sprayed in rows of the table on document
				int32 temp=0;
				temp = numRows;
				numRows = numCols;

				if(colnoForTableFlag == "1")
					temp = temp + 1;
                else if(colnoForTableFlag == "2")
                    temp = temp + 2; // added for Header row + List Name row
                else if(colnoForTableFlag == "3")
                    temp = temp + 1; // added for List Name row only
				
				
				numCols = temp;
			}

			XMLContentReference xmlContentRef = tableXMLPtr->GetContentReference();
			InterfacePtr<IXMLReferenceData> tableXMLRef (xmlContentRef.Instantiate());
			InterfacePtr<ITableModel> tableModel(tableXMLRef,UseDefaultIID());
			if(tableModel == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableModel == nil");									
				return kFalse;
			}
			//check the number of rows and colums of the current document table
			RowRange rowR = tableModel->GetTotalRows();
			ColRange colR = tableModel->GetTotalCols();
			int32 numberOfDocTableRows = rowR.count;
			int32 numberOfDocTableCols = colR.count;
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableCommands==nil");									
				return kFalse;
			}

			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
			if(tableGeometry == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableGeometry == nil");									
				return kFalse;
			}
			PMReal rowHt = tableGeometry->GetRowHeights(numberOfDocTableRows-1);
			PMReal colWidth = tableGeometry->GetColWidths(numberOfDocTableCols-1);
			if(numberOfDocTableRows != numRows)
			{			
				if(numberOfDocTableRows < numRows)
				{		
					//we have to add the additional rows at the end
					int32 difference = numRows-numberOfDocTableRows;
					RowRange rr(numberOfDocTableRows-1,difference);
					ErrorCode result = tableCommands->InsertRows(rr, Tables::eAfter, rowHt);
				}
				else
				{
					//we have to delete the surplus rows from bottom
					int32 difference = numberOfDocTableRows - numRows;
					RowRange rr(numberOfDocTableRows-difference,difference);
					ErrorCode result = tableCommands->DeleteRows(rr);
				}
			}
			if(numberOfDocTableCols != numCols)
			{
				if(numberOfDocTableCols < numCols)
				{
					//we have to add the additional cols at the end
					int32 difference = numCols-numberOfDocTableCols ;
					ColRange cr(numberOfDocTableCols-1,difference);
					ErrorCode result = tableCommands->InsertColumns(cr, Tables::eAfter,colWidth);
				}
				else
				{
					//we have to delete the surplus rows from bottom
					int32 difference = numberOfDocTableCols - numCols ;
					ColRange cr(numberOfDocTableCols-difference-1,difference);
					ErrorCode result = tableCommands->DeleteColumns(cr);
				}
			}

			//When the control comes to this part..we have ensured that both the document
			//table and the database table have same number of rows and columns.
			//So now delete each tag from the cell and add new tag.

			//CA("both the document table and the database table have same number of rows and columns.");

			bool16 alreadyExist = kFalse;
			
			if(tStruct.whichTab == 4 && (!isInterFaceCall))
			{
				//CA("tStruct.whichTab == 4");
				double itemID = tStruct.parentId;
				PMString temp = "";
				temp.AppendNumber(itemID);
				//CA("temp = " + temp);
					
				for(int index = 0; index<itemsUpdated.size();index++ )
				{
					if(itemsUpdated[index] == itemID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist)
					itemsUpdated.push_back(itemID);

				Report objReport;
				objReport.attributeId = 0;
				objReport.id = tStruct.parentId;

				tempItemList.push_back(objReport);
			}

			vector<double>  vec_tableheaders = oTableValue.getTableHeader();
			vector<vector<PMString> >  vec_tablerows = oTableValue.getTableData();
			vector<double>  vec_items = oTableValue.getItemIds();
			//vector<PMString>::iterator colIterator;
			
			//Get the TextModel
			InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
			if(tableTextContent == nil)
			{
				//CA("tableTextContent == nil");
				continue;
			}
			UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();

			InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel()/*,UseDefaultIID()*/);
			if(textModel == nil)
			{
				//CA("textModel == nil");
				continue;
			}

			int32 headerEntity1 = 0;
			int32 headerEntity2 = 0;
			
			int32 headerEntity = 0;//headerEntity can be row or column depending on the transpose
			vector<double> :: iterator colIterator;
		
			if(colnoForTableFlag == "1" || colnoForTableFlag == "2")
			{
				for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity++)
				{//for loop of Header
					//Table Header tag have typeId = -2..ItemId is not required.
					//ID is taken from the vec_tableheaders elements.
					//Iterate through the first row of the document table.
					GridAddress cellAddr;
					if(isTranspose == kFalse)
					{
						cellAddr.Set(0,headerEntity);
					}
					else
						cellAddr.Set(headerEntity,0);

					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName((*colIterator),languageId);
					PMString textToInsert("");
					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						textToInsert=dispname;					
					}
					else
						textToInsert=iConverter->translateString(dispname);
					
					//Update the Tag and its attribute values.
					PMString tagName("ATTRID_");
					tagName.AppendNumber(PMReal((*colIterator)));

					///start
					PMString attrVal;
					//XMLTagAttributeValue xmlTagAttrVal;
					//tagName
					xmlTagAttrVal.tagName = tagName;
					//ID..it is tableHeaderID			
					attrVal.Clear();
					attrVal.AppendNumber(PMReal((*colIterator)));
					xmlTagAttrVal.ID =attrVal;
					//typeId..it is -2 for tableHeader row
					attrVal.Clear();
					attrVal.AppendNumber(1);//attrVal.AppendNumber(-2);
					xmlTagAttrVal.header=attrVal;//xmlTagAttrVal.typeId=attrVal;
					AddTagToCellText
						(
							cellAddr,
							tableModel,
							textToInsert,
							textModel,
							xmlTagAttrVal
						);						
						
				}//end for loop of Header		
			
				headerEntity1 = 1;
			}
            if(colnoForTableFlag == "2" || colnoForTableFlag == "3") //refreshing List Name
            {
                
                GridAddress cellAddr;
                if(isTranspose == kFalse)
                {
                    cellAddr.Set(headerEntity1,0);
                }
                else
                    cellAddr.Set(0, headerEntity1);
                
                PMString dispname = oTableValue.getName();
                PMString textToInsert("");
                InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
                if(!iConverter)
                {
                    textToInsert=dispname;
                }
                else
                    textToInsert=iConverter->translateString(dispname);
                
                //Update the Tag and its attribute values.
                PMString tagName("List_Table_Name");
                XMLTagAttributeValue xmlTagAttrValforListName;
                xmlTagAttrValforListName = xmlTagAttrVal;
                ///start
                PMString attrVal;
                //XMLTagAttributeValue xmlTagAttrVal;
                //tagName
                xmlTagAttrValforListName.tagName = tagName;
                
                attrVal.Clear();
                attrVal.AppendNumber(tStruct.whichTab);
                xmlTagAttrValforListName.index = attrVal;
                
                xmlTagAttrValforListName.ID ="-121";
                xmlTagAttrValforListName.header="-1";
                xmlTagAttrValforListName.dataType = "4";
                
                
                AddTagToCellText
                (
                 cellAddr,
                 tableModel,
                 textToInsert,
                 textModel,
                 xmlTagAttrValforListName
                 );
                
                headerEntity1 = headerEntity1 + 1;
            }
			
			if(iscontentbuttonselected==kTrue)
			{
				//CA("Content is selected");
				for(int32 r=0;r<totalRowsofcustumtable;r++)
				{
					for(int32 c=0;c<totalsColsofcustumtable;c++)
					{
						PMString dataToBeSprayed("");
						PMString strdataToBeSprayed("");

						 strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIdsForIndesignTable.at(r),AttibuteIdsForIndesignTable.at(c),languageId, sectionId, /*kTrue*/kFalse);//23OCT09//Amit
						PMString p("strdataToBeSprayed : ");
						p.Append(strdataToBeSprayed);
						//CA(p);

						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							dataToBeSprayed=(strdataToBeSprayed);					
						}
						else
							dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);	
						
					GridAddress cellAddr;
					if(isTranspose == kFalse)
					{
						//CA("isTranspose == kFalse");
						cellAddr.Set(r,c);		
					}
					else
					{
						//CA("isTranspose == kTrue");
						cellAddr.Set(c,r);
					}
					PMString q("headerEntity1 : ");
					q.AppendNumber(headerEntity1);
					q.Append("\n");
					q.Append("headerEntity2 : ");
					q.AppendNumber(headerEntity2);
					//CA(q);

						PMString tagName("ATTRID_");
						tagName.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
						//CA(tagName);

						///start
					PMString attrVal;
					//XMLTagAttributeValue xmlTagAttrVal;
					//tagName
					xmlTagAttrVal.tagName = tagName;
					//ID..it is attributeID			
					attrVal.Clear();
					attrVal.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
					xmlTagAttrVal.ID =attrVal;

					attrVal.Clear();
					attrVal.AppendNumber(PMReal(ItemIdsForIndesignTable.at(r)));
					xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;

					attrVal.Clear();
					attrVal.AppendNumber(-1);
					xmlTagAttrVal.header = attrVal;
					
					attrVal.Clear();
					attrVal.AppendNumber(1);
					xmlTagAttrVal.childTag = attrVal;
					

					PMString str("string : ");
					str.Append(dataToBeSprayed);
					//CA(str);
						AddTagToCellText
						(
							cellAddr,
							tableModel,
							dataToBeSprayed,
							textModel,
							xmlTagAttrVal

						);
					}
				}
			}
			else if(issrtucturebuttenselected==kTrue)
			{
				//CA("Structure is selcted");
			vector<double> :: iterator rowIterator;// = vec_items.begin();
			//for(rowIterator=vec_tablerows.begin(); rowIterator!=vec_tablerows.end(); rowIterator++,rowNo++,itemIterator++)
			for(rowIterator=vec_items.begin(); rowIterator!=vec_items.end(); rowIterator++,headerEntity1++)
			{//start for 2
				//For each column the ID is different but the ItemID is same.
				vector<double>::iterator colIterator;
				headerEntity2 = 0;
				for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity2++)
				{				
					//row data feeding
					//ID is taken from the vec_tableheaders elements.
					//Iterate through the first row of the document table.	
					PMString dataToBeSprayed("");
					PMString strdataToBeSprayed("");
					/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific((*colIterator)) == kTrue){
						VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId((*rowIterator),(*colIterator),sectionId);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								strdataToBeSprayed = vecPtr->at(0).getObjectValue();
							delete vecPtr;
						}
					}
					else*/
					{
						strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId((*rowIterator),(*colIterator),languageId, sectionId, /*kTrue*/kFalse);//23OCT09//Amit
					}
					PMString p("strdataToBeSprayed : ");
					p.Append(strdataToBeSprayed);
					//CA(p);
					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						dataToBeSprayed=(strdataToBeSprayed);					
					}
					else
						dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);				
				///CA(dataToBeSprayed);
					GridAddress cellAddr;
					if(isTranspose == kFalse)
					{
						//CA("if(isTranspose == kFalse)");
						cellAddr.Set(headerEntity1,headerEntity2);
					}
					else
					{
						//CA("if(isTranspose == kTrue)");
						cellAddr.Set(headerEntity2,headerEntity1);
					}

					PMString q("headerEntity1 : ");
					q.AppendNumber(headerEntity1);
					q.Append("\n");
					q.Append("headerEntity2 : ");
					q.AppendNumber(headerEntity2);
					//CA(q);
					//Update the Tag and its attribute values.
					PMString tagName("ATTRID_");
					tagName.AppendNumber(PMReal((*colIterator)));
					//CA(tagName);

					///start
					PMString attrVal;
					//XMLTagAttributeValue xmlTagAttrVal;
					//tagName
					xmlTagAttrVal.tagName = tagName;
					//ID..it is attributeID			
					attrVal.Clear();
					attrVal.AppendNumber(PMReal((*colIterator)));
					xmlTagAttrVal.ID =attrVal;
					//typeId..it is 
					attrVal.Clear();
					attrVal.AppendNumber(PMReal((*rowIterator)));
					xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;

					attrVal.Clear();
					attrVal.AppendNumber(-1);
					xmlTagAttrVal.header = attrVal;
					
					attrVal.Clear();
					attrVal.AppendNumber(1);
					xmlTagAttrVal.childTag = attrVal;
					

					AddTagToCellText
					(
						cellAddr,
						tableModel,
						dataToBeSprayed,
						textModel,
						xmlTagAttrVal

					);
				
				}			
			}//end for 2
			}
		}//end for 1	
	
	}
	//end 7Aug..Yogesh ItemLevelTable handling

	PMString theData;
	if(tList[0].whichTab!=4)//For copy items
	{
	//	CA("tList[0].whichTab!=4");
		
		/*** Following is Ritesh's code for Table refresh ***
		getDataFromDB(theData, tList[0], tList[0].parentId); // parentid contains objectid
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			vector< vector<PMString> > tableText;

			int numCols=parseTheText(theData, tableText);

			tUtility.resizeTable(tableUIDRef, tableText.size(), numCols);

			for(int j=0; j<tableText.size(); j++)
			{
				for(int k=0; k<numCols; k++)
				{
					if(tableText[j].size()<=k)
						tUtility.setTableRowColData(tableUIDRef, PMString(" "), j, k);
					else
						tUtility.setTableRowColData(tableUIDRef, tableText[j][k], j, k);
				}
			}
		}
		else
			setTextInBox(boxID, theData, textAction::OverWrite);//Later changes here
		
		*********** My Changes (Rahul) for Table refresh start from here ****************/
		/* Check if table exists */
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			//CA("tUtility.isTablePresent");
			double tableId=0;
			// resize table to original nos of columns and rows
			
			InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
			if (!layoutSelectionSuite) {
				return kFalse;;
			}
			selectionManager->DeselectAll(nil); // deselect every active CSB
			
			UIDList ItemList(boxID.GetDataBase(), boxID.GetUID());
			//layoutSelectionSuite->Select(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	///	CS3 Change
			layoutSelectionSuite->SelectPageItems(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
			
		//	TableStyleUtils TabStyleObj;			
		//	TabStyleObj.SetTableModel(kTrue);	
			
		//	TabStyleObj.GetTableStyle();	
			
		//	TabStyleObj.getOverallTableStyle();
			
			// Work in Progress by Yogesh on 29 July 06
			//CA("Before tUtility.fillDataInTable");

			if(tList[0].typeId == -111 /*&& tList[0].parentId == -1*/ && tList[0].whichTab == 3)
			{
				//CA("Before tUtility.fillDataInCMedCustomTable");
				tUtility.fillDataInCMedCustomTable
				(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag, tList[0], boxID);
				//setTagParent(tStruct, idList[tStruct.whichTab-1], kTrue);//Re-Tag the box
				//setTagParent(tList, idList[tList.whichTab-1], kTrue);//Re-Tag the box
				//CA("After tUtility.fillDataInCMedCustomTable");
			}
			else if(tList[0].tableType == 3 && (tList[0].whichTab == 3 ||  tList[0].whichTab == 5))///for hybrid Table
			{

				frameDeleted = kFalse;
				IIDXMLElement *HybridTableTagPtr = tList[0].tagPtr;
				int32 childCount = HybridTableTagPtr->GetChildCount();
				if(childCount == 0)
				{
					//CA("childCount == 0");
					tableId = tList[0].typeId;
				}
				else
				{
					XMLReference tableXMLRef = HybridTableTagPtr->GetXMLReference();
					//IIDXMLElement *PSHybridTableTagPtr = tableXMLRef.Instantiate();
					InterfacePtr<IIDXMLElement>PSHybridTableTagPtr(tableXMLRef.Instantiate());
					PMString tableIdStr = PSHybridTableTagPtr->GetAttributeValue(WideString("tableId")); //Cs4
					tableId = tableIdStr.GetAsDouble();
				}
				
				double productID = tList[0].parentId;
				bool16 alreadyExist = kFalse;
				for(int index = 0; index<productsUpdated.size();index++ )
				{
					if(productsUpdated[index] == productID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist)
					productsUpdated.push_back(productID);

                int answer=1;
                PMString message("Performing a STRUCTURE REFRESH will re-spray the table from the database and any existing styles applied to your table will be lost.  Proceed?");
                message.SetTranslatable(kFalse);
                
                answer=CAlert::ModalAlert(message, kOKString, kCancelString, kNullString, 1, CAlert::eQuestionIcon);
                if(answer==2)
                    return kFalse;


				//CA("before calling fillDataInHybridTable method");
				tUtility.fillDataInHybridTable
				(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag, tList[0], boxID);
			}
			else
			{
				//CA("Before call to tUtility.fillDataInTable");
				frameDeleted = kFalse;
				/*if(typeID11 == 6210)*/
				{
					//CA("if(typeID11 == 6210)");
					TagStruct & tStruct = tList[0];
					IIDXMLElement * boxXMLPtr = tList[0].tagPtr;
					XMLReference xmlRef = boxXMLPtr->GetXMLReference();

					XMLTagAttributeValue xmlTagAttrVal;
					getAllXMLTagAttributeValues(xmlRef,xmlTagAttrVal);
					//start temporary adjustment in code
					//the typeId and the parentId should always be attached to the table and not
					//to its parent tag.but there is some logical error in the datasprayer plugin
					//while attaching tag to the table.The correct typeId and the parentId are getting
					//attached to the frametag instead of table tag.But it should be rectified 
					//immediately.
					PMString attrVal;
					attrVal.Append(xmlTagAttrVal.typeId);

					double typeId = attrVal.GetAsDouble();
					attrVal.Clear();

					//Note rowno stored the PBObjectID.
					attrVal.Append(xmlTagAttrVal.parentID/*xmlTagAttrVal.rowno*/);
					double objectId = attrVal.GetAsDouble();
					attrVal.Clear();

					PMString colnoForTableFlag = xmlTagAttrVal.header; //xmlTagAttrVal.colno;
					//end temporary adjustment in code
					//Get the tableXMLPtr
					int32 childCount  = boxXMLPtr->GetChildCount();

					for(int32 tableIndex = 0;tableIndex < childCount;tableIndex++)
					{//start for 1
						XMLReference tableXMLElementRef = boxXMLPtr->GetNthChild(tableIndex);
						//IIDXMLElement * tableXMLPtr = tableXMLElementRef.Instantiate();
						InterfacePtr<IIDXMLElement>tableXMLPtr(tableXMLElementRef.Instantiate());
			
						if(tableXMLPtr == nil)
						{	
							continue;
						}

						//CA(tableXMLPtr->GetTagString());
						if(tableXMLPtr == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableXMLPtr == nil");					
							return kFalse;
						}
						//Get all the attribute Values
						PMString tempStr;
						tempStr.Clear();
						//tempStr.Append(tableXMLPtr->GetAttributeValue("parentID"));
						//int32 objectId = tempStr.GetAsNumber();
						//tempStr.Clear();
				
						//tempStr.Append(tableXMLPtr->GetAttributeValue("typeId"));
						//int32 typeId = tempStr.GetAsNumber();
						//tempStr.Clear();

						tempStr.Append(xmlTagAttrVal.parentTypeID);
						double parentTypeId =tempStr.GetAsDouble();
						tempStr.Clear();

						tempStr.Append(xmlTagAttrVal.LanguageID);
						double languageId = tempStr.GetAsDouble();
						tempStr.Clear();

						tempStr.Append(xmlTagAttrVal.sectionID);
						double sectionId = tempStr.GetAsDouble();
						tempStr.Clear();

						tempStr.Append(xmlTagAttrVal.isAutoResize);
						int32 autoResize = tempStr.GetAsNumber();
						tempStr.Clear();

						InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
						if(ptrIAppFramework == nil)
						{
							CAlert::InformationAlert("Pointer to IAppFramework is nil.");
							return kFalse;
						}
						//ptrIAppFramework->clearAllStaticObjects();
						VectorScreenTableInfoPtr tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tStruct.sectionID, objectId, tStruct.languageID);
								//ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(objectId);
						if(!tableInfo)
						{
							//CA("!tableinfo");
							ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::refreshThisBox::tableInfo == nil");	
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
							{
								tList[tagIndex].tagPtr->Release();
							}
							return kFalse;
						}
			
						int32/*int64*/ numRows=0;
						int32/*int64*/ numCols=0;

						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it;

						bool16 typeidFound=kFalse;
						for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						{
							oTableValue = *it;
							if(oTableValue.getTableTypeID() == tStruct.typeId && oTableValue.getTableID() == tStruct.tableId)
							{
								typeidFound=kTrue;				
								break;
							}
						}
						if(!typeidFound)
						{
							//CA("typeidFound == kFalse");
							if(!isInterFaceCall)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::typeidFound == nil");
								bool16 alreadyExist = kFalse;
								if(tList[0].whichTab == 3)
								{
									double productID = tList[0].parentId;
								
									for(int index = 0; index<productsDeleted.size();index++ )
									{
										if(productsDeleted[index] == productID)
											alreadyExist = kTrue;
									}
									//if(!alreadyExist)
										productsDeleted.push_back(productID);
								}

								if(tList[0].whichTab == 4)
								{
									//CA("3333 tList[0].whichTab == 4");
									double itemID = tList[0].parentId;								
									for(int index = 0; index<itemsDeleted.size();index++ )
									{
										if(itemsDeleted[index] == itemID)
											alreadyExist = kTrue;
									}
									//if(!alreadyExist)
										itemsDeleted.push_back(itemID);
								}

								//PMString updated = "";
								//updated.Append("productsDeleted = ");
								//updated.AppendNumber(productsDeleted.size());
								//updated.Append(" , itemsDeleted = ");
								//updated.AppendNumber(itemsDeleted.size());

								//CA(updated);

								//this->deleteThisBox(boxID);
							}
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
							{
								tList[tagIndex].tagPtr->Release();
							}
							return kFalse;
						}

						numRows = static_cast<int32>(oTableValue.getTableData().size());
						if(numRows<=0)
						{
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
							{
								tList[tagIndex].tagPtr->Release();
							}
							return kFalse;
						}
						numCols =static_cast<int32> (oTableValue.getTableHeader().size());
						if(numCols<=0)
						{
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
							{
								tList[tagIndex].tagPtr->Release();
							}
							return kFalse;
						}

						bool8 isTranspose = oTableValue.getTranspose();
						//Note that don't take the literal meaning of row and column
						//it has to be taken w.r.t the isTranspose flag.
			
						if(isTranspose == kFalse)
						{
							//rows are sprayed in rows of the table on document
							if(colnoForTableFlag == "1")
								numRows = numRows + 1;//add for header row
                            else if(colnoForTableFlag == "2")
                                numRows = numRows + 2; // added for Header row + List Name row
                            else if(colnoForTableFlag == "3")
                                numRows = numRows + 1; // added for List Name row only
						}
						else
						{
							//rows are sprayed in columns of the table on document
							//colums are sprayed in rows of the table on document
							int32 temp=0;
							temp = numRows;
							numRows = numCols;

							if(colnoForTableFlag == "1")
								temp = temp + 1;
                            else if(colnoForTableFlag == "2")
                                temp = temp + 2; // added for Header row + List Name row
                            else if(colnoForTableFlag == "3")
                                 temp = temp + 1; // added for List Name row only
				
							numCols = temp;
						}

						XMLContentReference xmlContentRef = tableXMLPtr->GetContentReference();
						InterfacePtr<IXMLReferenceData> tableXMLRef( xmlContentRef.Instantiate());
						InterfacePtr<ITableModel> tableModel(tableXMLRef,UseDefaultIID());
						if(tableModel == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableModel == nil");									
							return kFalse;
						}
						//check the number of rows and colums of the current document table
						RowRange rowR = tableModel->GetTotalRows();
						ColRange colR = tableModel->GetTotalCols();
						int32 numberOfDocTableRows = rowR.count;
						int32 numberOfDocTableCols = colR.count;
						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
						if(tableCommands==nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableCommands==nil");									
							return kFalse;
						}

						InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
						if(tableGeometry == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableGeometry == nil");									
							return kFalse;
						}
						PMReal rowHt = tableGeometry->GetRowHeights(numberOfDocTableRows-1);
						PMReal colWidth = tableGeometry->GetColWidths(numberOfDocTableCols-1);
						if(numberOfDocTableRows != numRows)
						{			
							if(numberOfDocTableRows < numRows)
							{		
								//we have to add the additional rows at the end
								int32 difference = numRows-numberOfDocTableRows;
								RowRange rr(numberOfDocTableRows-1,difference);
								ErrorCode result = tableCommands->InsertRows(rr, Tables::eAfter, rowHt);
							}
							else
							{
								//we have to delete the surplus rows from bottom
								int32 difference = numberOfDocTableRows - numRows;
								RowRange rr(numberOfDocTableRows-difference,difference);
								ErrorCode result = tableCommands->DeleteRows(rr);
							}
						}
						if(numberOfDocTableCols != numCols)
						{
							if(numberOfDocTableCols < numCols)
							{
								//we have to add the additional cols at the end
								int32 difference = numCols-numberOfDocTableCols ;
								ColRange cr(numberOfDocTableCols-1,difference);
								ErrorCode result = tableCommands->InsertColumns(cr, Tables::eAfter,colWidth);
							}
							else
							{
								//we have to delete the surplus rows from bottom
								int32 difference = numberOfDocTableCols - numCols ;
								ColRange cr(numberOfDocTableCols-difference-1,difference);
								ErrorCode result = tableCommands->DeleteColumns(cr);
							}
						}

						//When the control comes to this part..we have ensured that both the document
						//table and the database table have same number of rows and columns.
						//So now delete each tag from the cell and add new tag.

						//CA("both the document table and the database table have same number of rows and columns.");

						bool16 alreadyExist = kFalse;
			
						if(tStruct.whichTab == 4 && (!isInterFaceCall))
						{
							//CA("tStruct.whichTab == 4");
							double itemID = tStruct.parentId;
							PMString temp = "";
							temp.AppendNumber(itemID);
							//CA("temp = " + temp);
					
							for(int index = 0; index<itemsUpdated.size();index++ )
							{
								if(itemsUpdated[index] == itemID)
									alreadyExist = kTrue;
							}

							if(!alreadyExist)
								itemsUpdated.push_back(itemID);

							Report objReport;
							objReport.attributeId = 0;
							objReport.id = tStruct.parentId;

							tempItemList.push_back(objReport);
						}

						vector<double>  vec_tableheaders = oTableValue.getTableHeader();
						vector<vector<PMString> >  vec_tablerows = oTableValue.getTableData();
						vector<double>  vec_items = oTableValue.getItemIds();
						//vector<PMString>::iterator colIterator;
			
						//Get the TextModel
						InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
						if(tableTextContent == nil)
						{
							//CA("tableTextContent == nil");
							continue;
						}
						UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();

						InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel()/*,UseDefaultIID()*/);
						if(textModel == nil)
						{
							//CA("textModel == nil");
							continue;
						}

						int32 headerEntity1 = 0;
						int32 headerEntity2 = 0;
			
						int32 headerEntity = 0;//headerEntity can be row or column depending on the transpose
						vector<double> :: iterator colIterator;
		
						if(colnoForTableFlag == "1" || colnoForTableFlag == "2") //refreshing Header
						{
							for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity++)
							{//for loop of Header
								//Table Header tag have typeId = -2..ItemId is not required.
								//ID is taken from the vec_tableheaders elements.
								//Iterate through the first row of the document table.
								GridAddress cellAddr;
								if(isTranspose == kFalse)
								{
									cellAddr.Set(0,headerEntity);
								}
								else
									cellAddr.Set(headerEntity,0);

								PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName((*colIterator),languageId);
								PMString textToInsert("");
								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
								if(!iConverter)
								{
									textToInsert=dispname;					
								}
								else
									textToInsert=iConverter->translateString(dispname);
					
								//Update the Tag and its attribute values.
								PMString tagName("ATTRID_");
								tagName.AppendNumber(PMReal((*colIterator)));

								///start
								PMString attrVal;
								//XMLTagAttributeValue xmlTagAttrVal;
								//tagName
								xmlTagAttrVal.tagName = tagName;

								attrVal.Clear();
								attrVal.AppendNumber(4);					
								xmlTagAttrVal.index =attrVal;

								//ID..it is tableHeaderID			
								attrVal.Clear();
								attrVal.AppendNumber(PMReal((*colIterator)));
								xmlTagAttrVal.ID =attrVal;
								//typeId..it is -2 for tableHeader row
								attrVal.Clear();
								attrVal.AppendNumber(1);//attrVal.AppendNumber(-2);
								xmlTagAttrVal.header=attrVal;//xmlTagAttrVal.typeId=attrVal;
								AddTagToCellText
									(
										cellAddr,
										tableModel,
										textToInsert,
										textModel,
										xmlTagAttrVal
									);						
						
							}//end for loop of Header		
			
							headerEntity1 = 1;
						}
                        if(colnoForTableFlag == "2" || colnoForTableFlag == "3") //refreshing List Name
						{
							
								GridAddress cellAddr;
								if(isTranspose == kFalse)
								{
									cellAddr.Set(headerEntity1,0);
								}
								else
									cellAddr.Set(0, headerEntity1);
                                
								PMString dispname = oTableValue.getName();
								PMString textToInsert("");
								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
								if(!iConverter)
								{
									textToInsert=dispname;
								}
								else
									textToInsert=iConverter->translateString(dispname);
                                
								//Update the Tag and its attribute values.
                                PMString tagName("List_Table_Name");
                                XMLTagAttributeValue xmlTagAttrValforListName;
                                xmlTagAttrValforListName = xmlTagAttrVal;
								///start
								PMString attrVal;
								//XMLTagAttributeValue xmlTagAttrVal;
								//tagName
								xmlTagAttrValforListName.tagName = tagName;
                            
                                attrVal.Clear();
                                attrVal.AppendNumber(tStruct.whichTab);
								xmlTagAttrValforListName.index = attrVal;
                            
								xmlTagAttrValforListName.ID ="-121";
								xmlTagAttrValforListName.header="-1";
                                xmlTagAttrValforListName.dataType = "4";

                            
								AddTagToCellText
                                (
                                 cellAddr,
                                 tableModel,
                                 textToInsert,
                                 textModel,
                                 xmlTagAttrValforListName
                                 );
                            
							headerEntity1 = headerEntity1 + 1;
						}

						if(iscontentbuttonselected==kTrue)
						{
							//CA("Content is selected");
							for(int32 r=0;r<totalRowsofcustumtable;r++)
							{
								for(int32 c=0;c<totalsColsofcustumtable;c++)
								{
									PMString dataToBeSprayed("");
									PMString strdataToBeSprayed("");

									 strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIdsForIndesignTable.at(r),AttibuteIdsForIndesignTable.at(c),languageId, sectionId, /*kTrue*/kFalse);//23OCT09//Amit
									PMString p("strdataToBeSprayed : ");
									p.Append(strdataToBeSprayed);
									//CA(p);

									InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
									if(!iConverter)
									{
										dataToBeSprayed=(strdataToBeSprayed);					
									}
									else
										dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);	
						
								GridAddress cellAddr;
								if(isTranspose == kFalse)
								{
									//CA("isTranspose == kFalse");
									cellAddr.Set(r,c);		
								}
								else
								{
									//CA("isTranspose == kTrue");
									cellAddr.Set(c,r);
								}
								PMString q("headerEntity1 : ");
								q.AppendNumber(headerEntity1);
								q.Append("\n");
								q.Append("headerEntity2 : ");
								q.AppendNumber(headerEntity2);
								//CA(q);

									PMString tagName("ATTRID_");
									tagName.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
									//CA(tagName);

									///start
								PMString attrVal;
								//XMLTagAttributeValue xmlTagAttrVal;
								//tagName
								xmlTagAttrVal.tagName = tagName;
								//ID..it is attributeID			
								attrVal.Clear();
								attrVal.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
								xmlTagAttrVal.ID =attrVal;

								attrVal.Clear();
								attrVal.AppendNumber(PMReal(ItemIdsForIndesignTable.at(r)));
								xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;
                                    
                                attrVal.Clear();
                                attrVal.AppendNumber(4);
                                xmlTagAttrVal.index =attrVal;

								attrVal.Clear();
								attrVal.AppendNumber(-1);
								xmlTagAttrVal.header = attrVal;
					
								attrVal.Clear();
								attrVal.AppendNumber(1);
								xmlTagAttrVal.childTag = attrVal;

								attrVal.Clear();
								attrVal.AppendNumber(4);
								xmlTagAttrVal.index = attrVal;
					

								PMString str("string : ");
								str.Append(dataToBeSprayed);
								//CA(str);
									AddTagToCellText
									(
										cellAddr,
										tableModel,
										dataToBeSprayed,
										textModel,
										xmlTagAttrVal

									);
								}
							}
						}
						else if(issrtucturebuttenselected==kTrue)
						{
							//CA("Structure is selcted");
						vector<double> :: iterator rowIterator;// = vec_items.begin();
						//for(rowIterator=vec_tablerows.begin(); rowIterator!=vec_tablerows.end(); rowIterator++,rowNo++,itemIterator++)
						for(rowIterator=vec_items.begin(); rowIterator!=vec_items.end(); rowIterator++,headerEntity1++)
						{//start for 2
							//For each column the ID is different but the ItemID is same.
							vector<double>::iterator colIterator;
							headerEntity2 = 0;
							for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity2++)
							{				
								//row data feeding
								//ID is taken from the vec_tableheaders elements.
								//Iterate through the first row of the document table.	
								PMString dataToBeSprayed("");
								PMString strdataToBeSprayed("");
								/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific((*colIterator)) == kTrue){
									VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId((*rowIterator),(*colIterator),sectionId);
									if(vecPtr != NULL)
									{
										if(vecPtr->size()> 0)
											strdataToBeSprayed = vecPtr->at(0).getObjectValue();
										delete vecPtr;
									}
								}
								else*/
								{
									strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId((*rowIterator),(*colIterator),languageId, sectionId, /*kTrue*/kFalse);//23OCT09//Amit
								}
								PMString p("strdataToBeSprayed : ");
								p.Append(strdataToBeSprayed);
								//CA(p);
								InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
								if(!iConverter)
								{
									dataToBeSprayed=(strdataToBeSprayed);					
								}
								else
									dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);				
							///CA(dataToBeSprayed);
								GridAddress cellAddr;
								if(isTranspose == kFalse)
								{
									//CA("if(isTranspose == kFalse)");
									cellAddr.Set(headerEntity1,headerEntity2);
								}
								else
								{
									//CA("if(isTranspose == kTrue)");
									cellAddr.Set(headerEntity2,headerEntity1);
								}

								PMString q("headerEntity1 : ");
								q.AppendNumber(headerEntity1);
								q.Append("\n");
								q.Append("headerEntity2 : ");
								q.AppendNumber(headerEntity2);
								//CA(q);
								//Update the Tag and its attribute values.
								PMString tagName("ATTRID_");
								tagName.AppendNumber(PMReal((*colIterator)));
								//CA(tagName);

								///start
								PMString attrVal;
								//XMLTagAttributeValue xmlTagAttrVal;
								//tagName
								xmlTagAttrVal.tagName = tagName;
								//ID..it is attributeID			
								attrVal.Clear();
								attrVal.AppendNumber(PMReal((*colIterator)));
								xmlTagAttrVal.ID =attrVal;
								//typeId..it is 
								attrVal.Clear();
								attrVal.AppendNumber(PMReal((*rowIterator)));
								xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;

								attrVal.Clear();
								attrVal.AppendNumber(-1);
								xmlTagAttrVal.header = attrVal;
					
								attrVal.Clear();
								attrVal.AppendNumber(1);
								xmlTagAttrVal.childTag = attrVal;
					

								AddTagToCellText
								(
									cellAddr,
									tableModel,
									dataToBeSprayed,
									textModel,
									xmlTagAttrVal

								);
				
							}			
						}//end for 2
						}
					}//end for 1	
	
				}
	//end 7Aug..Yogesh ItemLevelTable handling
				
				

				//tUtility.fillDataInTable(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag, tList[0], boxID);

//add sagar
				
//till here
			}
			//CA("After tUtility.fillDataInTable");
			//selectionManager->DeselectAll(nil); // deselect every active CSB
			if(frameDeleted == kFalse)
			{
				//layoutSelectionSuite->Select(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//CS3 Change
				layoutSelectionSuite->SelectPageItems(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);		
				//TabStyleObj.SetTableModel(kFalse);			
			//	TabStyleObj.ApplyTableStyle();			
				//TabStyleObj.setTableStyle();	
				if( tList[0].isAutoResize == 1)
				{
					/*UIDList itemList(boxID.GetDataBase(),boxID.GetUID());
					UIDList processedItems;
					K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
					ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);*/

					/*InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
					ASSERT(fitFrameToContentCmd != nil);
					if (fitFrameToContentCmd == nil) {
					return kFalse;
					}
					fitFrameToContentCmd->SetItemList(UIDList(boxID));
					if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
					ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
					return kFalse;
					}*/

				}
			}
		}
		else
		{  
			//This condition is separately handled in the function 
			/*
			sprayItemItemTableScreenPrintInTabbedTextForm
				and
			sprayItemTableInTabbedTextForm from CommonFunctions.cpp file
			*/
			/*
			///Tabbed Text Refresh
			TagList tabbedList;			
			tabbedList=itagReader->getTagsFromBox_ForRefresh(boxID);

			XMLContentReference contentRef = tList[0].tagPtr->GetContentReference();
			IXMLReferenceData* xmlRefData = contentRef.Instantiate();
			XMLReference xmlRef=xmlRefData->GetReference();
			IIDXMLElement *xmlElement=xmlRef.Instantiate();
			int elementCount=xmlElement->GetChildCount();
			
			PMString RealTagString = xmlElement->GetTagString();
			//CA(RealTagString);

			//PMString ASD("elementCount : ");
			//ASD.AppendNumber(elementCount);
			//CA(ASD);

			InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
			if(!unknown)
				return kFalse;
			UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);;
			if(textFrameUID==kInvalidUID)
			{
				//CA("textFrameUID==kInvalidUID");
				//this->convertBoxToTextBox(curBox);
				//InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
				//if(!unknown)
					//break;
				//textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				//if(textFrameUID == kInvalidUID)
				//{
					//break;
				//}		
			}
			InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
			if(textFrame==nil)
			{
				CA("Err: invalid interface pointer ITextFrame");	
				return kFalse;;
			}
			InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
			if (textModel == nil){
				CA("Err: invalid interface pointer ITextModel");
				return kFalse;;
			}
			
			WideString* myText=new WideString("");

			for(int32 p=elementCount-1; p>= 0 ; p--)
			{
				int32 tStart=0;	
				int32 tEnd=0;
				//CA("22");
				XMLReference elementXMLref=xmlElement->GetNthChild(p);
				IIDXMLElement * childElement=elementXMLref.Instantiate();
				if(childElement==nil)
					continue;
		
				PMString ChildtagName=childElement->GetTagString();
				//CA(ChildtagName);

				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);

				tStart = sIndex +1;
				tEnd = eIndex - tStart;

				
				ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);					
				ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElement(elementXMLref, kFalse);
			}

			TextIndex startIndex = textFrame->TextStart();
			TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;
			ErrorCode Err = textModel->Delete(kTrue,startIndex, finishIndex);


			// for Tabbed Text Functionality
			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{
				//CA("Pointre to DataSprayerPtr not found");//
				return kFalse;
			}

			PublicationNode refpNode;
			PMString PubName("PubName");
			refpNode.setAll(PubName, tList[0].parentId,tList[0].parentId, tList[0].sectionID,3,1,1,1,tList[0].typeId,0,kFalse);
			
			DataSprayerPtr->FillPnodeStruct(refpNode, tList[0].sectionID, 1);
			DataSprayerPtr->fillTaggedTableInBoxCS2(boxID, tList[0]);
		*/
		}
		
		/******************************** My Changes end here **********************************/
	}
	/*
	else
	{
		vector<int32> itemIDList;
		int numRows=getAllItemIDs(tList[0].parentId, tList[0].imgFlag, itemIDList);
		if(numRows==0)
		{
			setTextInBox(boxID, PMString(""), textAction::OverWrite);
			return kTrue;
		}

		vector<PMString> rowString;
		for(int i=0; i<numRows; i++)
		{
			PMString tempStr;
			if(!getDataFromDB(tempStr, tList[0], itemIDList[i]))
				return kFalse;
			rowString.push_back(tempStr);
		}
			
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			CA("Else...table present");
			PMString tempStr;
			tUtility.resizeTable(tableUIDRef, 1, 1);
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				tempStr.Append(rowString[j]);
			}
			tUtility.setTableRowColData(tableUIDRef, tempStr, 0, 0);
		}
		else
		{
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				if(!j)
					setTextInBox(boxID, rowString[j], textAction::OverWrite);
				else
					setTextInBox(boxID, rowString[j], textAction::AtEnd);
			}
		}
	}*/

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)//----lalit----
	{
		tList[tagIndex].tagPtr->Release();
	}
	
	return kTrue;
}


bool16 Refresh::setTextInBox(UIDRef& boxID, PMString& textToInsert, enum textAction action)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return kFalse;
	}
	/*InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::textFrameUID == kInvalidUID");	
		return kFalse;
	}
/*	/////	CS3 Change
	InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::textFrame == nil");		
		return kFalse;
	}
*/	
//////////////	Added By Amit
	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::graphicFrameHierarchy == nil");	
		return false;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::multiColumnItemHierarchy == nil");	
		return false;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::multiColumnItemTextFrame == nil");
		//CA("Its Not MultiColumn");
		return false;
	}

	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::frameItemHierarchy == nil");
		return false;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::textFrame == nil");
		return false;
	}

//////////////	End
	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::textModel == nil");		
		return kFalse;
	}

	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;


	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::textFocusManager == nil");		
		return kFalse;
	}

	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::setTextInBox::frameTextFocus == nil");			
		return kFalse;
	}
	
	WideString* myText=new WideString(textToInsert);

	if(action==Refresh::OverWrite)
	{
		//CS3 Change		
		//ErrorCode Err = textModel->Replace(kTrue,startIndex, finishIndex, myText);
		//ErrorCode Err = textModel->Replace(startIndex, finishIndex, myText);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
		ReplaceText(textModel,startIndex,finishIndex ,insertText);
		/*
		InterfacePtr<ICommand> pInsertTextCommand(textModel->ReplaceCmd(startIndex, finishIndex, myText, kFalse, nil));
		if (pInsertTextCommand ==nil )
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;
		*/
	}
	else if(action==Refresh::AtEnd)
	{
		//	CS3 Change		
		//textModel->Insert(kTrue,finishIndex, myText);
		textModel->Insert(finishIndex, myText);
/*		InterfacePtr<ICommand> pInsertTextCommand(textModel->InsertCmd(finishIndex, myText, kFalse, nil));
		if (pInsertTextCommand ==nil )	
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;*/
	}
	else
		return kFalse;//Will implement later if required
	if(myText)
		delete myText;
	return kTrue;
}

////////////////////////////////[ From old refresh ]//////////////////////////////////////


bool16 fileExists(PMString& path, PMString& name)
{
	//CA(__FUNCTION__);
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

    //CAlert::InformationAlert("fileExists:: file to be opened  " + theName);
    
    #ifdef MACINTOSH
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            return kFalse;
        }
        
        ptrIAppFramework->getUnixPath(theName);
    }
    #endif
    //CAlert::InformationAlert("fileExists:: file to be opened  " + theName);
	std::string tempString(theName.GetPlatformString());	
	const char *file= const_cast<char *> (tempString.c_str());
	//const char *file=(theName./*GrabCString()*/GetPlatformString().c_str());//Cs4
	
	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		//CA(theName + " opened  ");
		std::fclose(fp);
		return kTrue;
	}
	/*else
		CA(theName + " could not be opened  ");*/

	return kFalse;
}











bool16 Refresh::doesExist(IIDXMLElement * ptr)
{
	//CA(__FUNCTION__);
	//CA("Refresh::doesExist");
	//TagReader tReader;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doesExist::!itagReader");	
		return kFalse;
	}
	
	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		IIDXMLElement *xmlPtr;
		TagList tList;
		if(refreshTableByAttribute)
		{
			tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(i), &xmlPtr);
		}
		else
			tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(i), &xmlPtr);
		if(ptr==xmlPtr)
		{
			//CA("return kTrue");
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			return kTrue;
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	//CA("return kFalse");
	return kFalse;
}


bool16 Refresh::getAllBoxIds(void)
{
	//CA(__FUNCTION__);
	//CA("getAllBoxIds");
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getAllBoxIds::!itagReader");		
		return kFalse;
	}
/*	InterfacePtr<ISelection> selection(::QuerySelection());
	if(selection == nil)
		return kFalse;

	scoped_ptr<UIDList> _selectUIDList(selection->CreateUIDList());
	if(_selectUIDList==nil)
		return kFalse;

	const int32 listLength=_selectUIDList->Length();
*/	
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

	if(!iSelectionManager)
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getAllBoxIds::!iSelectionManager");	
		return 0;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		//CA("if(!txtMisSuite)");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getAllBoxIds::!txtMisSuite");
		return 0; 
	}
//	UIDList	selectUIDList;
	
	txtMisSuite->GetUidList(selectUIDList);

	const int32 listLength=selectUIDList.Length();
	SelectedUidListlength = selectUIDList.Length();
	
//PMString len;
//len.Append("Length=");
//len.AppendNumber(listLength);
//CA(len);
	if(listLength==0)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getAllBoxIds::listLength == 0");       
		return kFalse;
	}

	for( int p=0; p<selectUIDList.Length(); p++)
	{	
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
		if(!iHier)
			continue;
		UID kidUID;
		int32 numKids=iHier->GetChildCount();
		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));

		for(int j=0;j<numKids;j++)
		{
			//CA("1");
			kidUID=iHier->GetChildUID(j);
			UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
			IIDXMLElement* ptr;
			TagList tList,tList_checkForHybrid;
			
			if(isGroupFrame == kTrue) 
			{
				tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
				if(tList_checkForHybrid.size() == 0)
					continue;
				if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
					tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
				else
					tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
			}
			else 
			{
				tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
				if(tList_checkForHybrid.size() == 0)
					continue;
				if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
					tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
				else
					tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
			}

			if(!doesExist(tList))//if(!doesExist(ptr))
			{
				selectUIDList.Append(kidUID);
			}

			//------lalit------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
			{
				tList_checkForHybrid[tagIndex].tagPtr->Release();
			}

		}
	}
	PMString s1("selectUIDList12 : ");
		s1.AppendNumber(listLength);
		//CA(s1);
	return kTrue;
}

void Refresh::showTagInfo(UIDRef boxRef)
{
	//CA(__FUNCTION__);
	TagList tList;
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return ;
	}
	if(refreshTableByAttribute)
		tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef);
	else
		tList=itagReader->getTagsFromBox_ForRefresh(boxRef);


	//PMString allInfo;
	//for(int numTags=0; numTags<tList.size(); numTags++)
	//{
	//	/*allInfo.Clear();
	//	allInfo.AppendNumber(tList[numTags].elementId);
	//	allInfo+="\n";
	//	allInfo.AppendNumber(tList[numTags].parentId);
	//	allInfo+="\n";
	//	allInfo.AppendNumber(tList[numTags].typeId);
	//	allInfo+="\n";
	//	allInfo.AppendNumber(tList[numTags].imgFlag);
	//	allInfo+="\n";
	//	allInfo+="Start and end index :";
	//	allInfo.AppendNumber(tList[numTags].startIndex);
	//	allInfo+=" / ";
	//	allInfo.AppendNumber(tList[numTags].endIndex);*/
	//	//CAlert::InformationAlert(allInfo);
	//}
	//------lalit------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}

}

bool16 Refresh::shouldRefresh(/*const */TagStruct& tInfo, const UID& boxID, bool16 isTaggedFrame)
{ 
	//CA(__FUNCTION__);
	//rDataList = rBookDataList;
	if(isDummyCall)
		return kTrue;

	if(ISRfreshBookDlgOpen == kFalse)
	{	
		//CA("ISRfreshBookDlgOpen == kFalse");
		/*PMString as("rDataList.size()	:	");
		as.AppendNumber(static_cast<int32>(rDataList.size()));
		CA(as);*/
		//for(int i=0; i<rDataList.size(); i++)
		//{ 
		//	PMString ASD("rDataList[i].elementID : ");
		//	ASD.AppendNumber(rDataList[i].elementID);
		//	ASD.Append(" \nrDataList[i].objectID : ");
		//	ASD.AppendNumber(rDataList[i].objectID);
		//	ASD.Append(" \n rDataList[i].name	:	" +rDataList[i].name);
		//	ASD.Append("\n rDataList[i].isSelected	:	");
		//	if(rDataList[i].isSelected)
		//		ASD.Append("kTrue");
		//	else
		//		ASD.Append("kFalse");
		//	CA(ASD);
		//}

		for(int i=0; i<rDataList.size(); i++)
		{  			
			
			if(/*boxID==rDataList[i].boxID &&*/ rDataList[i].isSelected && ((!rDataList[i].isObject || GroupFlag == 3) || rDataList[i].isTableFlag==kTrue))
			{   
				PMString ASD("rDataList[i].elementID : ");
				ASD.AppendNumber(rDataList[i].elementID);
				ASD.Append(" \nrDataList[i].objectID : ");
				ASD.AppendNumber(rDataList[i].objectID);
				ASD.Append(" \nrDataList[i].dataType : ");
				ASD.AppendNumber(rDataList[i].dataType);
					

				if(!isTaggedFrame)
				{	
					if((rDataList[i].elementID==tInfo.elementId && rDataList[i].objectID==tInfo.parentId ) || (tInfo.whichTab == 5 && (rDataList[i].objectID == tInfo.sectionID)) )// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
					{	
						//CA(rDataList[i].name+ "Got .....");
						//CA("return kTrue");
						//CA("rDataList[i].name");
						return kTrue;
					}
				}
				else{ 
				
				/*PMString ASD("rDataList[i].TypeID : ");
				ASD.AppendNumber(rDataList[i].TypeID);
				ASD.Append("tInfo.typeId : ");
				ASD.AppendNumber(tInfo.typeId);
				ASD.Append("rDataList[i].objectID :");
				ASD.AppendNumber(rDataList[i].objectID);
				ASD.Append("tInfo.parentId : ");
				ASD.AppendNumber(tInfo.parentId);
				CA(ASD);*/

					if(rDataList[i].objectID==tInfo.parentId && rDataList[i].TypeID ==tInfo.typeId )// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
					{  
						//CA("kTrue");
						return kTrue;
					}
				}
			}
		}	
	}
	else if(ISRfreshBookDlgOpen == kTrue)
	{	
		//CA("ISRfreshBookDlgOpen == kTrue");
		//PMString a("rBookDataList.size()	:	");	
		//a.AppendNumber(static_cast<int32>(rBookDataList.size()));
		//CA(a);
		for(int i=0; i<rBookDataList.size(); i++)
		{  
			if(/*boxID==rDataList[i].boxID &&*/ rBookDataList[i].isSelected && (!rBookDataList[i].isObject || rBookDataList[i].isTableFlag==kTrue))
			{  
				//CA("rBookDataList[i].isSelected");
				if(!isTaggedFrame)
				{  
					//CA("Not !isTaggedFrame");
					
					if(rBookDataList[i].elementID==tInfo.elementId && rBookDataList[i].whichTab==tInfo.whichTab)// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
					{ 
						//CA("return kTrue in Not !isTaggedFrame");
						if(rBookDataList[i].whichTab == 3)
						{
							//CA("Inside rBookDataList[i].whichTab == 3");
							if(rBookDataList[i].TypeID==tInfo.typeId)
							{
								//CA("true");
								return kTrue;
							}
						}
						else //To Handle Item For Refresh on 1/11/06 
						{
							//CA("rBookDataList[i].whichTab == 4  ");
							if(tInfo.tableType == 3)
							{
								if(rBookDataList[i].TypeID==tInfo.typeId)							
									return kTrue;
							}
							else
							{
								//CA("tInfo.tableType != 3");
								return kTrue;
							}
						}
					 }

					if(tInfo.elementId == -101 || tInfo.elementId == -102)
					{
						//CA("tInfo.elementId == -101 || tInfo.elementId == -102");
						if(rBookDataList[i].TypeID ==tInfo.typeId ) //For Tabbed Text Standard Tables..
						{	
							//CA("rBookDataList[i].TypeID ==tInfo.typeId");
							return kTrue;
					    }	

					}
			    }
				else
				{
					//CA("isTaggedFrame");
					//PMString a("rBookDataList[i].TypeID	:	");
					//a.AppendNumber(rBookDataList[i].TypeID);
					//a.Append("\ntInfo.typeId	:	");
					//a.AppendNumber(tInfo.typeId);
					//CA(a);
					if(/*rBookDataList[i].objectID==tInfo.parentId &&*/ rBookDataList[i].TypeID == tInfo.typeId )// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
					{	//CA("return kTrue in isTaggedFrame");
						return kTrue;
				    }	
				}
			}
		}	
	}
	//CA("return kFalse");
	return kFalse;
}

//by Amarjit patil 
//void Refresh::doRefreshNewoption(int option, PMString bookName,PMString documentName)
//{
//	//CA("Refresh::doRefreshNewoption");
//	iTEM.clear();
//	vector<int32>ItemId_ItemgroupId;
//    int32 jk=0;
//	itemid_attributevalue.clear();
//	itemids.clear();
//	itemgroupids.clear();
//	ItemId_ItemgroupId.clear();
//	vector_iterater=0;
//	
//	//CA("inside Refresh::doRefresh 1321 ");
//	ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh.");
//	PMString imagePath;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return;
//	}
//
//	if(imageDownLoadPath == "")
//	{
//		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
//		if(ptrIClientOptions==nil)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
//			return;
//		}
//		imagePath=ptrIClientOptions->getImageDownloadPath();
//		imageDownLoadPath = imagePath;
//	}
//	if(imagePath == "")
//		imagePath = imageDownLoadPath;
//	//CA("imagePath 1	:	"+imagePath);	
//	//Commented by Yogesh Joshi on 1 March 06
//	if(imagePath!="")
//	{
//		const char *imageP=(imagePath.GrabCString()); //Cs4
//		 
//		#ifdef MACINTOSH
//			if(imageP[std::strlen(imageP)-1]!=':')
//				imagePath+=":";
//		#else
//			if(imageP[std::strlen(imageP)-1]!='\\')
//				imagePath+="\\";
//		#endif
//	}
//	//CA("imagePath 2 :	"+imagePath);
//
//	selectUIDList.Clear();
////	ChangedTextList.clear();
//	int i;
//	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//	if(!itagReader)
//	{ 
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
//		return ;
//	}
//
//	switch(option)
//	{
//	case 1:
//		  {
//			 ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   case 1: .");
//			//for current page option
//			//CA("Refresh::doRefresh 1");
//			UID pageUID;
//			//TagReader tReader;
//			int32 pageNumber= 0;
//			if(!this->isValidPageNumber(pageNumber, pageUID))
//			{
//				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isValidPageNumber");			
//				break; 
//			}
//			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
//			IDataBase* database= ::GetDataBase(document); 
//						
//			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
//			int32 spreadNo = -1; 
//
//			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
//			{ 
//				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
//				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
//				if(spread->GetPageIndex(pageUID)!= -1) 
//				{			
//					spreadNo = p; 
//					break; 
//				}
//			}
//
//			if(spreadNo==-1)
//			{
//				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::spreadNo == -1");			
//				break; 
//			}
//
//			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->/*::*/QueryFrontLayoutData()); //Cs4
//			if (layoutData == nil)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::layoutData == nil");			
//				break; 
//			}
//			 pageUID = layoutData->GetPage();
//			if(pageUID == kInvalidUID)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::pageUID == kInvalidUID");			
//				break; 
//			}
//			
//			UIDRef SpreadRef=layoutData->GetSpreadRef();
//			InterfacePtr<ISpread> iSpread(SpreadRef, UseDefaultIID());	
//			if (iSpread == nil)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::iSpread == nil");			
//				break; 
//			}
//
//            //CA("Refresh::doRefresh 1.1");
//			UIDList tempList(database);
//			pageNumber=iSpread->GetPageIndex(pageUID);
//			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);
//			selectUIDList=tempList;
//			
//			for( int p=0; p<selectUIDList.Length(); p++)
//			{	
//				InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
//				if(!iHier)
//					continue;
//				UID kidUID;
//				int32 numKids=iHier->GetChildCount();
//				bool16 isGroupFrame = kFalse ;
//				isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));
//
//				for(int j=0;j<numKids;j++)
//				{
//					//CA("1");
//					kidUID=iHier->GetChildUID(j);
//					UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
//					IIDXMLElement* ptr;
//					TagList tList,tList_checkForHybrid;
//
//					if(isGroupFrame == kTrue) 
//					{
//						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
//						if(tList_checkForHybrid.size() == 0)
//						{
//							//CA("tList_Checkforhybrid.size() == 0	+	return kFalse");
//							continue;
//						}
//						if(refreshTableByAttribute && tList_checkForHybrid[0].tableType != 3)
//							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
//						else
//							tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
//					}
//					else 
//					{
//						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
//						if(tList_checkForHybrid.size() == 0)
//						{
//							//CA("tList_Checkforhybrid.size() == 0	+	return kFalse");
//							continue;
//						}
//						if(refreshTableByAttribute && tList_checkForHybrid[0].tableType != 3)
//							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
//						else
//							tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
//					}
//
//					if(!doesExist(tList))//if(!doesExist(ptr))
//					{
//						selectUIDList.Append(kidUID);
//					}
//
//					//-------lalit--------
//					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//					{
//						tList[tagIndex].tagPtr->Release();
//					}
//					for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
//					{
//						tList_checkForHybrid[tagIndex].tagPtr->Release();
//					}
//
//				}
//			}
//
//            //CA("Refresh::doRefresh 1.2");
//			for(i=0; i<selectUIDList.Length(); i++)
//			{	
//				
//				UIDRef boxID=selectUIDList.GetRef(i);
//				this->refreshThisBox(boxID, imagePath);
//			}
//
//			if(isMultipleSectionsAvailable)
//			{
//				//ptrIAppFramework->multipleSectionDataClearInstance();
//			}
//			//CA("j");
//			//ptrIAppFramework->GetSectionData_clearSectionDataCache();		
//			break;
//		  }
//
//	case 2:
//		{
//			//CA("Refresh::doRefresh 2");//textboxvaluevec	
//			ElementIDptr resultvec = NULL;
//			vector<int32>itemmid;
//			vector<int32>ChildIDvec;
//			itemids.clear();
//			itemgroupids.clear();
//			
//			int32 siz=static_cast<int32>(textboxvaluevec.size());
//			ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   case 2: .");
//			if(!getAllPageItemsFromPage(Mediator::refreshPageNumber))
//			{
//				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!getAllPageItemsFromPage");		
//				return;
//			}
//			for(i=0; i<selectUIDList.Length(); i++)
//			{	
//				TagList tList;
//			    UIDRef boxID=selectUIDList.GetRef(i);
//				tList =  itagReader->getTagsFromBox(boxID);
//				PMString uidlist("selectUIDList First:");
//				uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//				//CA(uidlist);
//				if(tList.size() == 0)
//				{
//					continue;
//				}
//				
//				for(int32 t=0;t<tList.size();t++)
//				{
//					itemmid.clear();
//					ChildIDvec.clear();
//					if(tList[t].imgFlag!=1 && tList[t].whichTab==3 )
//					{
//						//CA("k");
//						if(tList[t].typeId==-5)
//						{
//							int32 childTagCount = tList[t].tagPtr->GetChildCount();
//							for(int32 childTagIndex=0;childTagIndex<childTagCount;childTagIndex++)
//							{
//								XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//								//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//								InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());//
//								if(childTagXMLElementPtr == nil)
//								continue;
//								PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
//								int32 childId22 = strchildId22.GetAsNumber();
//
//								PMString strParentID22 = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//								int32 parentID22 = strParentID22.GetAsNumber();	
//
//								PMString strChildtag22 = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));
//								int32 childTag22 = strChildtag22.GetAsNumber();
//								
//									if(parentID22!=-1)
//									ItemGroupIDsForPageRange.push_back(parentID22);
//
//								PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
//								int32 ID22 = strID22.GetAsNumber();	
//								
//									if(parentID22!=-1)
//										MapForItemGroup.insert(map<int32,int32>::value_type(parentID22,ID22));
//								
//
//								 int32 elementCount=childTagXMLElementPtr->GetChildCount();
//								 for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
//								 {
//									XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
//									InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
//									if(childElement == nil)
//									continue;
//									PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
//									int32 childId11 = strchildId11.GetAsNumber();
//
//									PMString strParentID11 = childElement->GetAttributeValue(WideString("parentID"));
//									int32 ParentID11 = strParentID11.GetAsNumber();
//
//									PMString strChildTag11 = childElement->GetAttributeValue(WideString("childTag"));
//									int32 ChildTag11 = strChildTag11.GetAsNumber();
//									
//									
//										if(ParentID11!=-1)
//										ItemGroupIDsForPageRange.push_back(ParentID11);
//									
//									PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
//									int32 ID11 = strID11.GetAsNumber();	
//									
//										if(ParentID11!=-1)
//											MapForItemGroup.insert(map<int32,int32>::value_type(ParentID11,ID11));
//
//									int32 elementcount1=childElement->GetChildCount();
//									for(int32 count1=0;count1<elementcount1;count1++)
//									{
//										TagStruct tagInfo;
//										XMLReference elementXMLref123 = childElement->GetNthChild(count1);
//										InterfacePtr<IIDXMLElement>childElement123(elementXMLref123.Instantiate());
//										 if(childElement123 == nil)
//										 continue;
//										 PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
//										 int32 childTag = strchildTag.GetAsNumber();
//
//										 PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//										 int32 parentID = strParentID.GetAsNumber();	
//	                                    
//										PMString strchildId = childElement123->GetAttributeValue(WideString("childId"));//Cs4
//										int32 childId = strchildId.GetAsNumber();
//										
//										 if(parentID!=-1)
//											 ItemGroupIDsForPageRange.push_back(parentID);
//
//										PMString strID = childElement123->GetAttributeValue(WideString("ID"));//CS4
//										int32 ID = strID.GetAsNumber();	
//										
//											if(parentID!=-1)
//												MapForItemGroup.insert(map<int32,int32>::value_type(parentID,ID));
//
//										int32 elementcount2= childElement123->GetChildCount();
//										for(int32 count2=0;count2<elementcount2;count2++)
//										{
//											TagStruct tagInfo;
//											XMLReference elementXMLref321 = childElement123->GetNthChild(count2);
//											InterfacePtr<IIDXMLElement>childElement321(elementXMLref321.Instantiate());
//											if(childElement123 == nil)
//											  continue;
//											PMString strchildId44 = childElement321->GetAttributeValue(WideString("childId"));//Cs4
//											int32 childId44 = strchildId44.GetAsNumber();
//
//											PMString strChildTag44 = childElement321->GetAttributeValue(WideString("childTag"));
//											int32 ChildTag44 = strChildTag44.GetAsNumber();
//
//											PMString strParentID44 = childElement321->GetAttributeValue(WideString("parentID"));
//											int32 ParentID44 = strParentID44.GetAsNumber();
//
//											
//												if(ParentID44!=-1)
//												ItemGroupIDsForPageRange.push_back(ParentID44);
//											
//												PMString strID44 = childElement321->GetAttributeValue(WideString("ID"));//CS4
//												int32 ID44 = strID44.GetAsNumber();	
//											
//												if(ParentID44!=-1)
//												MapForItemGroup.insert(map<int32,int32>::value_type(ParentID44,ID44));
//										}
//									}
//								}
//							}
//						}//
//						
//						  int32 itemIdvar= tList[t].parentId;
//						  int32 attributeid= tList[t].elementId;
//						  
//						  if(itemIdvar!=-1)
//						  MapForItemGroup.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//					    
//
//					}
//					//for geting ItemIds from Document
//					if(tList[t].imgFlag!=1 && tList[t].whichTab==4 )
//					{
//						if(tList[t].typeId==-5)
//						{
//							int32 childTagCount = tList[t].tagPtr->GetChildCount();
//							for(int32 childTagIndex=0;childTagIndex<childTagCount;childTagIndex++)
//							{
//								XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//								//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//								InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());//
//								if(childTagXMLElementPtr == nil)
//								continue;
//								PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
//								int32 childId22 = strchildId22.GetAsNumber();
//
//								PMString strParentID22 = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//								int32 parentID22 = strParentID22.GetAsNumber();	
//
//								PMString strChildtag22 = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));
//								int32 childTag22 = strChildtag22.GetAsNumber();
//								
//								if(childTag22==1)
//								{
//									if(childId22!=-1)
//									ItemidsforpageRangevec.push_back(childId22);
//								}
//								else
//								{
//									if(parentID22!=-1)
//									ItemidsforpageRangevec.push_back(parentID22);
//								}
//								PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
//								int32 ID22 = strID22.GetAsNumber();	
//								if(childTag22==1)
//								{
//								if(childId22!=-1)
//								mapforpagerange.insert(map<int32,int32>::value_type(childId22,ID22));
//								}
//								else
//								{
//									if(parentID22!=-1)
//										mapforpagerange.insert(map<int32,int32>::value_type(parentID22,ID22));
//								}
//
//								 int32 elementCount=childTagXMLElementPtr->GetChildCount();
//								 for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
//								 {
//									XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
//									InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
//									if(childElement == nil)
//									continue;
//									PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
//									int32 childId11 = strchildId11.GetAsNumber();
//
//									PMString strParentID11 = childElement->GetAttributeValue(WideString("parentID"));
//									int32 ParentID11 = strParentID11.GetAsNumber();
//
//									PMString strChildTag11 = childElement->GetAttributeValue(WideString("childTag"));
//									int32 ChildTag11 = strChildTag11.GetAsNumber();
//									
//									if(ChildTag11==1)
//									{
//										if(childId11!=-1)
//										ItemidsforpageRangevec.push_back(childId11);
//									}
//									else
//									{
//										if(ParentID11!=-1)
//										ItemidsforpageRangevec.push_back(ParentID11);
//									}
//									PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
//									int32 ID11 = strID11.GetAsNumber();	
//									if(ChildTag11==1)
//									{
//									if(childId11!=-1)
//									mapforpagerange.insert(map<int32,int32>::value_type(childId11,ID11));
//									}
//									else
//									{
//										if(ParentID11!=-1)
//											mapforpagerange.insert(map<int32,int32>::value_type(ParentID11,ID11));
//									}
//
//									int32 elementcount1=childElement->GetChildCount();
//									for(int32 count1=0;count1<elementcount1;count1++)
//									{
//										TagStruct tagInfo;
//										XMLReference elementXMLref123 = childElement->GetNthChild(count1);
//										InterfacePtr<IIDXMLElement>childElement123(elementXMLref123.Instantiate());
//										 if(childElement123 == nil)
//										 continue;
//										 PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
//										 int32 childTag = strchildTag.GetAsNumber();
//
//										 PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//										 int32 parentID = strParentID.GetAsNumber();	
//	                                    
//										PMString strchildId = childElement123->GetAttributeValue(WideString("childId"));//Cs4
//										int32 childId = strchildId.GetAsNumber();
//										if(childTag==1)
//										{
//											if(childId!=-1)
//												ItemidsforpageRangevec.push_back(childId);
//										}
//										else
//										{
//										 if(parentID!=-1)
//											 ItemidsforpageRangevec.push_back(parentID);
//										}
//										PMString strID = childElement123->GetAttributeValue(WideString("ID"));//CS4
//										int32 ID = strID.GetAsNumber();	
//										if(childTag==1)
//										{
//											if(childId!=-1)
//												mapforpagerange.insert(map<int32,int32>::value_type(childId,ID));
//										}
//										else
//										{
//											if(parentID!=-1)
//												mapforpagerange.insert(map<int32,int32>::value_type(parentID,ID));
//										}
//
//										int32 elementcount2= childElement123->GetChildCount();
//										for(int32 count2=0;count2<elementcount2;count2++)
//										{
//											TagStruct tagInfo;
//											XMLReference elementXMLref321 = childElement123->GetNthChild(count2);
//											InterfacePtr<IIDXMLElement>childElement321(elementXMLref321.Instantiate());
//											if(childElement123 == nil)
//											  continue;
//											PMString strchildId44 = childElement321->GetAttributeValue(WideString("childId"));//Cs4
//											int32 childId44 = strchildId44.GetAsNumber();
//
//											PMString strChildTag44 = childElement321->GetAttributeValue(WideString("childTag"));
//											int32 ChildTag44 = strChildTag44.GetAsNumber();
//
//											PMString strParentID44 = childElement321->GetAttributeValue(WideString("parentID"));
//											int32 ParentID44 = strParentID44.GetAsNumber();
//
//											if(ChildTag44==1)
//											{
//												if(childId44!=-1)
//												ItemidsforpageRangevec.push_back(childId44);
//											}
//											else
//											{
//												if(ParentID44!=-1)
//												ItemidsforpageRangevec.push_back(ParentID44);
//											}
//											PMString strID44 = childElement321->GetAttributeValue(WideString("ID"));//CS4
//											int32 ID44 = strID44.GetAsNumber();	
//											if(ChildTag44==1)
//											{
//												if(childId44!=-1)
//												mapforpagerange.insert(map<int32,int32>::value_type(childId44,ID44));
//											}
//											else
//											{
//												if(ParentID44!=-1)
//												mapforpagerange.insert(map<int32,int32>::value_type(ParentID44,ID44));
//											}
//										}
//									}
//								}
//							}
//						}//
//						else if(tList[t].childTag==-1)
//						{
//						  int32 itemIdvar= tList[t].parentId;
//						  int32 attributeid= tList[t].elementId;
//						  
//						  if(itemIdvar!=-1)
//						  mapforpagerange.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//					    }
//					    else if(tList[t].childTag==1)
//					     {
//							//CA("bbb");
//						  int32 itemIdvar=tList[t].childId;
//						  int32 attributeid= tList[t].elementId;
//						  if(itemIdvar!=-1)
//						  mapforpagerange.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//					     }
//					}
//					//following is for deleting Frames from Document ,if its Item/ItemGroup is not
//					//in DB or not in section etc.
//
//					int32 Parentid=tList[t].parentId;
//					int32 Parenttpyeid=tList[t].parentTypeID;
//					int32 ImageTypeid=tList[t].typeId;
//					int32 childId = tList[t].childId;
//					int32 lanuageid = tList[t].languageID;
//					int32 sectionId = tList[t].sectionID;
//
//					itemmid.push_back(Parentid);
//					ChildIDvec.push_back(childId);
//
//// Gor Item Group
//					//Images are deleting from document while refreshing when they are not in DB or item is deleted from DB
//					if(tList[t].whichTab==3)
//					{
//						//CA("j");
//						if(tList[t].imgFlag==1)
//						{
//							//CA("imageflag :");
//							//if image is not in DB then delete that image frame
//							bool16 AssetValuePtrObj = kFalse; //ptrIAppFramework->checkimageinDB_getAssetByParentAndType(Parentid,Parenttpyeid,ImageTypeid);
//							if(AssetValuePtrObj == kFalse)
//							{
//								//CA("if image is not in DB then delete image");							
//								DeleteImageFrame(boxID);
//							}
//							else
//							{
//								//if item is not in DB then delete that image frame
//								resultvec = kFalse; //ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//									if(resultvec!=NULL)
//									{
//										//CA("if Item is not in DB then delete image");
//										DeleteImageFrame(boxID);
//										//CA("not null-1");
//										delete resultvec;
//										resultvec=NULL;
//									}
//							}
//						}
//						//Normal spray case
//						if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							//CA("Normal Spray");
//							//if Item is not in DB then delete that text frame
//							resultvec = kFalse; //ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//								if(resultvec!=NULL)
//								{
//									//CA("if Item is not in DB then delete image");
//									DeleteImageFrame(boxID);
//									delete resultvec;
//									resultvec=NULL;
//								}
//						}
//						//This is for ItemList
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							int32 sectionIIID=tList[t].sectionID;
//							//CA("ItemList Spray");
//							//case 1: If the Item is not in DB then delete that text frame
//							resultvec = kFalse;// ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								DeleteTextFrame(boxID);
//								//CA("not null-2");
//								delete resultvec;
//								resultvec=NULL;
//								
//							}
//							else //case 2:If the parent or leading item is in DB but not in section then delete the entire list from the Section
//							{
//								resultvec = kFalse;// ptrIAppFramework->FindParentItemGroupInDB_getAllItemGroupItemsWithDeletedParents(ChildIDvec);
//								if(resultvec!=NULL)
//								{
//								 DeleteTextFrame(boxID);
//								 //CA("not null-3");
//								 delete resultvec;
//								 resultvec=NULL;
//								}
//							}
//							
//							//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//							//if the Leading item is in DB but not in Section then also delete that text frame from document.
//						}
//
//						//This is for  isSprayItemPerFrame
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//						{
//							//CA("isSprayItemPerFrame");
//							resultvec = kFalse;// ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								//CA("if Item is not in DB then delete image");
//								DeleteImageFrame(boxID);
//								//CA("not null-4");
//								delete resultvec;
//								resultvec=NULL;
//								
//							}
//						}
//					}
////=======================================================================================================================================
//// For Item 
//
//					//for image refresh for item and item group
//					if(tList[t].whichTab==4)
//					{
//						
//						if(tList[t].imgFlag==1)
//						{
//							//CA("imageflag :");
//							 bool16 AssetValuePtrObj = kFalse;// ptrIAppFramework->checkimageinDB_getAssetByParentAndType(Parentid,Parenttpyeid,ImageTypeid);
//							if(AssetValuePtrObj == kFalse)
//							{
//								//CA("if image is not in DB then delete image");							
//								DeleteImageFrame(boxID);
//							}
//							else
//							{
//								bool16 result = kFalse;// ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//								if(result==kFalse)
//									DeleteImageFrame(boxID);
//							}
//						}
//					//case 1: //This is for Normal Spray
//					if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						//CA("Normal Spray");
//						bool16 result = kFalse;// ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//						if(result==kFalse)
//							DeleteTextFrame(boxID);
//						else//if item is not in section then delete that Image frame 
//						{
//							bool16 result1 = kFalse;// ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,Parentid);
//							if(result1==kFalse)
//							 DeleteTextFrame(boxID);
//						}
//						//if item is not in DB but not in Section then delete that text frame
//					}
//					//case 2: //This is for ItemList
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						int32 sectionIIID=tList[t].sectionID;
//						//CA("ItemList Spray");
//						//case 1: If the Item is not in DB then delete that text frame
//						bool16 result = kFalse;// ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//						if(result==kFalse)
//						{
//						 DeleteTextFrame(boxID);
//						}
//						else //case 2:if items parent is not in DB then delete that items text frame from document
//						{
//							resultvec = kFalse;// ptrIAppFramework->FindParentItemInDB_findAllItemsWithDeletedParents(itemmid);
//							if(resultvec!=NULL)
//							{
//							 DeleteTextFrame(boxID);
//							// CA("not null-5");
//							 delete resultvec;
//							 resultvec=NULL;
//							}
//						}
//						//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//						//if the Leading item is in DB but not in Section then also delete that text frame from document.
//					}
//					//case 3: //This is for  isSprayItemPerFrame
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//					{
//						bool16 result =kFalse;//  ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//						if(result==kFalse)
//							DeleteTextFrame(boxID);
//					}
//				  }
//				}
//				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//				{
//					//CA("h");
//					tList[tagIndex].tagPtr->Release();
//				}
//				//till here
//			}
//
//			int32 sizz12=static_cast<int32>(MapForItemGroup.size());
//			PMString t("MapForItemGroup :");
//			t.AppendNumber(sizz12);
//			//CA(t);
//			map<int32,int32>::iterator itrr12;
//			for(itrr12=MapForItemGroup.begin();itrr12!=MapForItemGroup.end();itrr12++)
//			{
//			  int32 ItemGroupID=itrr12->first;
//			  itemgroupids.push_back(ItemGroupID);
//			}
//			if(itemgroupids.size()>0 && ItemgroupAttributeIdsforframerefresh.size()>0)
//			{
//				////CA("we don't have code for itemgroup for Content option so that it will not refresh.");
//				//break;
//			
//				//CA("++@++");
//				int32 finalItemIDsize =static_cast<int32> (ItemgroupAttributeIdsforframerefresh.size());
//				for(int32 k=0;k<finalItemIDsize;k++)
//				{
//					PMString a("ItemGroupAttributeIDS :");
//					a.AppendNumber(ItemgroupAttributeIdsforframerefresh.at(k));
//					//CA(a);
//				}
//////=====================For ItemGroup====================================================================
//			MapInMapptr_var_ItemGroup = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIdsNew(Emptyvec_Item,Emptyvec_IemAttribute,itemgroupids,ItemgroupAttributeIdsforframerefresh);
//			PMString OriginalStrIGroup;
//            MapInMap::iterator itrr1;
//			 int32 siz3=-1;
//            for(itrr1=MapInMapptr_var_ItemGroup->begin();itrr1!=MapInMapptr_var_ItemGroup->end();itrr1++)
//            {
//              NItemGroupID=itrr1->first;
//              Result_ItemGroup =itrr1->second;
//            }
//			
//            multimap<vector<int32>,vector<PMString>>::iterator itrr22;
//            itrr22=Result_ItemGroup->begin();
//            if(itrr22!=Result_ItemGroup->end())
//            {
//             NAttributeId_ItemGroup = itrr22->first;
//             for(int32 j=0;j<NAttributeId_ItemGroup.size();j++)
//	         NString = itrr22->second;
//            }
//		}
////=================================For Item===============================
//		//CA("before calling method");
//			int32 sizz=static_cast<int32>(mapforpagerange.size());
//				map<int32,int32>::iterator itrr;
//			for(itrr=mapforpagerange.begin();itrr!=mapforpagerange.end();itrr++)
//			{
//			  int32 ItemID=itrr->first;
//			  itemids.push_back(ItemID);
//			}
//			if(itemids.size()>0 && AttributeIdsForFrameRefresh.size()>0)
//			{
//				//CA("we don't have code for itemgroup for Content option so that it will not refresh.");
//				//break;
//			
//			int32 s=static_cast<int32>(AttributeIdsForFrameRefresh.size());
//			for(int32 t=0;t<s;t++)
//			{
//			PMString t3("AttributeID: ");
//			t3.AppendNumber(s);
//			//CA(t3);
//			}
//			MapInMapptr_var  = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIds(itemids,AttributeIdsForFrameRefresh,/*itemgroupids*/Emptyvec_ItemGroup,/*ItemgroupAttributeIdsforframerefresh*/Emptyvec_ItemGroupAttribute);
//			
//			PMString OriginalStr;
//            MapInMap::iterator itr;
//			 int32 siz2=-1;
//            for(itr=MapInMapptr_var->begin();itr!=MapInMapptr_var->end();itr++)
//            {
//              NItemID=itr->first;
//              Result =itr->second;
//            }
//			
//            multimap<vector<int32>,vector<PMString>>::iterator itr2;
//            itr2=Result->begin();
//            if(itr2!=Result->end())
//            {
//             NAttributeID = itr2->first;
//             for(int32 j=0;j<NAttributeID.size();j++)
//	         NValue = itr2->second;
//            }
//			
//		 }
//			
//			for(i=0; i<selectUIDList.Length(); i++)
//		    {	
//				
//			  UIDRef boxID=selectUIDList.GetRef(i);
//			  this->refreshThisBoxNewoption(boxID, imagePath);
//		    }
//			
//			if(MapInMapptr_var)
//			{
//				//CA("NOT NULL22");
//				delete MapInMapptr_var;
//				MapInMapptr_var = NULL;
//			}
//			if(MapInMapptr_var_ItemGroup)
//			{
//				//CA("NOT NULL22");
//				delete MapInMapptr_var_ItemGroup;
//				MapInMapptr_var_ItemGroup = NULL;
//			}
//			
//		break;
//		}
//	case 3:
//		{
//			//CA("Refresh::doRefresh 3");
//			itemids.clear();
//			itemgroupids.clear();
//			
//		ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   case 3: .");
//		if(!getAllBoxIds())
//		{
//			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!getAllBoxIds");		
//			return;
//		}
//		int32 siz11=static_cast<int32>(selectUIDList.Length());
//	
//		for(i=0; i<SelectedUidListlength/*selectUIDList.Length()*/; i++)
//		{
//			//CA("123");
//			ElementIDptr resultvec = NULL;
//			vector<int32>itemmid;
//			vector<int32>ChildIDvec;
//			int32 countofframes=0;
//			static int32 countp=0;
//
//			TagList tList,tList_checkForHybrid;
//			//IIDXMLElement* ptr;
//			int32 itemIdvar;
//			int32 attributeid;
//			int32 itemgroupIdvar=0;
//			UIDRef boxID = selectUIDList.GetRef(i);
//
//			tList =  itagReader->getTagsFromBox(selectUIDList.GetRef(i));
//			if(tList.size() == 0)
//			{
//				continue;
//			}
//			for(int32 t=0;t<tList.size();t++)
//			{
//				//CA("2222");
//				itemmid.clear();
//				ChildIDvec.clear();
//				if(tList[t].imgFlag!=1 && tList[t].whichTab==4 )
//				{
//					//CA("33333");
//					if(tList[t].typeId==-5)
//					{
//						//CA("666");
//						int32 childTagCount = tList[t].tagPtr->GetChildCount();
//						for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
//						{
//							XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//							//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//							InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
//							if(childTagXMLElementPtr == nil)
//								continue;						
//							TagStruct tagInfo;
//							PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
//							int32 childId = strchildId.GetAsNumber();
//
//							PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
//							int32 ID = strID.GetAsNumber();	
//							if(childId!=-1)
//							iTEM.insert(map<int32,int32>::value_type(childId,ID));
//						}
//					}
//					else if(tList[t].childTag==-1)
//					{
//					  itemIdvar= tList[t].parentId;
//					  attributeid= tList[t].elementId;
//					  if(itemIdvar!=-1)
//					  iTEM.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//				    }
//				    else if(tList[t].childTag==1)
//				     {
//						//CA("bbb");
//					  itemIdvar=tList[t].childId;
//					  attributeid= tList[t].elementId;
//					  if(itemIdvar!=-1)
//					  iTEM.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//				     }
//			     }
//				if(tList[t].imgFlag!=1 && tList[t].whichTab==3 )
//					{
//						//CA("Inside ItemGroup");
//						if(tList[t].typeId==-5)
//						{
//							//CA("5555");
//							int32 childTagCount = tList[t].tagPtr->GetChildCount();
//							for(int32 childTagIndex=0;childTagIndex<childTagCount;childTagIndex++)
//							{
//								XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//								//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//								InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());//
//								if(childTagXMLElementPtr == nil)
//								continue;
//								PMString strchildId22 = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
//								int32 childId22 = strchildId22.GetAsNumber();
//
//								PMString a("childId22 : ");
//								a.AppendNumber(childId22);
//								//CA(a);
//
//								PMString strParentID22 = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//								int32 parentID22 = strParentID22.GetAsNumber();	
//
//								PMString strChildtag22 = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));
//								int32 childTag22 = strChildtag22.GetAsNumber();
//
//								
//								if(childTag22==-1)
//								{
//									if(parentID22!=-1)
//									ItemGroupIDsForPageRange.push_back(parentID22);
//								}
//								else if(childTag22==1)
//								{
//									ItemGroupIDsForPageRange.push_back(childId22);
//								}
//
//								PMString strID22 = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
//								int32 ID22 = strID22.GetAsNumber();	
//
//								PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
//							    int32 index = strIndex.GetAsNumber();	
//
//									if(parentID22!=-1)
//										MapForItemGroup.insert(map<int32,int32>::value_type(parentID22,ID22));
//								
//								if(index==4)
//								{
//									iTEM.insert(map<int32,int32>::value_type(childId22,ID22));
//								}
//								
//
//								 int32 elementCount=childTagXMLElementPtr->GetChildCount();
//								 for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
//								 {
//									XMLReference elementXMLref1 = childTagXMLElementPtr->GetNthChild(elementIndex);
//									InterfacePtr<IIDXMLElement>childElement(elementXMLref1.Instantiate());
//									if(childElement == nil)
//									continue;
//									PMString strchildId11 = childElement->GetAttributeValue(WideString("childId"));//Cs4
//									int32 childId11 = strchildId11.GetAsNumber();
//
//									PMString a("childId11 : ");
//										a.AppendNumber(childId11);
//										//CA(a);
//
//									PMString strParentID11 = childElement->GetAttributeValue(WideString("parentID"));
//									int32 ParentID11 = strParentID11.GetAsNumber();
//
//									PMString strChildTag11 = childElement->GetAttributeValue(WideString("childTag"));
//									int32 ChildTag11 = strChildTag11.GetAsNumber();
//									
//									
//										if(ParentID11!=-1)
//										ItemGroupIDsForPageRange.push_back(ParentID11);
//									
//									PMString strID11 = childElement->GetAttributeValue(WideString("ID"));//CS4
//									int32 ID11 = strID11.GetAsNumber();	
//									
//										if(ParentID11!=-1)
//											MapForItemGroup.insert(map<int32,int32>::value_type(ParentID11,ID11));
//									
//
//									int32 elementcount1=childElement->GetChildCount();
//									for(int32 count1=0;count1<elementcount1;count1++)
//									{
//										TagStruct tagInfo;
//										XMLReference elementXMLref123 = childElement->GetNthChild(count1);
//										InterfacePtr<IIDXMLElement>childElement123(elementXMLref123.Instantiate());
//										 if(childElement123 == nil)
//										 continue;
//										 PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
//										 int32 childTag = strchildTag.GetAsNumber();
//
//										 PMString a("childTag : ");
//										a.AppendNumber(childTag);
//										//CA(a);
//
//
//										 PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
//										 int32 parentID = strParentID.GetAsNumber();	
//	                                    
//										PMString strchildId = childElement123->GetAttributeValue(WideString("childId"));//Cs4
//										int32 childId = strchildId.GetAsNumber();
//										
//										
//										 if(parentID!=-1)
//											 ItemGroupIDsForPageRange.push_back(parentID);
//										
//
//										PMString strID = childElement123->GetAttributeValue(WideString("ID"));//CS4
//										int32 ID = strID.GetAsNumber();	
//
//										
//											if(parentID!=-1)
//												MapForItemGroup.insert(map<int32,int32>::value_type(parentID,ID));
//										
//
//
//										int32 elementcount2= childElement123->GetChildCount();
//										for(int32 count2=0;count2<elementcount2;count2++)
//										{
//											TagStruct tagInfo;
//											XMLReference elementXMLref321 = childElement123->GetNthChild(count2);
//											InterfacePtr<IIDXMLElement>childElement321(elementXMLref321.Instantiate());
//											if(childElement123 == nil)
//											  continue;
//											PMString strchildId44 = childElement321->GetAttributeValue(WideString("childId"));//Cs4
//											int32 childId44 = strchildId44.GetAsNumber();
//
//											PMString strChildTag44 = childElement321->GetAttributeValue(WideString("childTag"));
//											int32 ChildTag44 = strChildTag44.GetAsNumber();
//
//											PMString strParentID44 = childElement321->GetAttributeValue(WideString("parentID"));
//											int32 ParentID44 = strParentID44.GetAsNumber();
//
//											
//												if(ParentID44!=-1)
//												ItemGroupIDsForPageRange.push_back(ParentID44);
//											
//											
//												PMString strID44 = childElement321->GetAttributeValue(WideString("ID"));//CS4
//												int32 ID44 = strID44.GetAsNumber();	
//											
//												if(ParentID44!=-1)
//												MapForItemGroup.insert(map<int32,int32>::value_type(ParentID44,ID44));
//											
//										}
//									}
//								}
//							}
//						}//
//						
//						  int32 itemIdvar= tList[t].parentId;
//						  int32 attributeid= tList[t].elementId;
//						  
//						  if(itemIdvar!=-1)
//						  MapForItemGroup.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//					    
//
//					}
//			}
//
//		}
//			int32 sizz12=static_cast<int32>(MapForItemGroup.size());
//			
//			map<int32,int32>::iterator itrr12;
//			for(itrr12=MapForItemGroup.begin();itrr12!=MapForItemGroup.end();itrr12++)
//			{
//			  int32 ItemGroupID=itrr12->first;
//			  itemgroupids.push_back(ItemGroupID);
//			}
//			if(itemgroupids.size()>0 && ItemgroupAttributeIdsforframerefresh.size()>0)
//			{
//				
//				////CA("we don't have code for itemgroup for Content option so that it will not refresh.");
//				//break;
//			
//				//CA("++@++");
//				int32 finalItemIDsize =static_cast<int32> (ItemgroupAttributeIdsforframerefresh.size());
//				
//////=====================For ItemGroup====================================================================
//			MapInMapptr_var_ItemGroup = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIdsNew(Emptyvec_Item,Emptyvec_IemAttribute,itemgroupids,ItemgroupAttributeIdsforframerefresh);
//
//			
//
//			PMString OriginalStrIGroup;
//            MapInMap::iterator itrr1;
//			 int32 siz3=-1;
//            for(itrr1=MapInMapptr_var_ItemGroup->begin();itrr1!=MapInMapptr_var_ItemGroup->end();itrr1++)
//            {
//              NItemGroupID=itrr1->first;
//              Result_ItemGroup =itrr1->second;
//            }
//			
//            multimap<vector<int32>,vector<PMString>>::iterator itrr22;
//            itrr22=Result_ItemGroup->begin();
//            if(itrr22!=Result_ItemGroup->end())
//            {
//             NAttributeId_ItemGroup = itrr22->first;
//             for(int32 j=0;j<NAttributeId_ItemGroup.size();j++)
//	         NString = itrr22->second;
//            }
//		}
//		int32 mapsize=static_cast<int32>(iTEM.size());
//
//		map<int32,int32>::iterator itr7;
//		for(itr7=iTEM.begin();itr7!=iTEM.end();itr7++)
//		{
//			int32 ID=itr7->first;
//			itemids.push_back(ID);
//		}
//		map<int32,int32>::iterator itr11;
//		for(itr11=UniqueAttributes.begin();itr11!=UniqueAttributes.end();itr11++)
//		{
//			int32 ID=itr11->first;
//			AttributeIdsForFrameRefresh.push_back(ID);
//		}
//
//		//AttributeIdsForFrameRefresh.erase(std::unique(AttributeIdsForFrameRefresh.begin(),AttributeIdsForFrameRefresh.end())/*,AttributeIdsForFrameRefresh.end()*/);
//		
//		if(itemids.size()>0 && AttributeIdsForFrameRefresh.size()>0 )
//		{
//			MapInMapptr_var  = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIds(itemids,AttributeIdsForFrameRefresh,itemgroupids,itemgroup_attributeids);
//			//CA("after java method gets call");
//
//		   PMString OriginalStr;
//           MapInMap::iterator itr;
//		   for(itr=MapInMapptr_var->begin();itr!=MapInMapptr_var->end();itr++)
//		   {
//			  NItemID=itr->first;
//			  Result =itr->second;
//		   }
//		   multimap<vector<int32>,vector<PMString>>::iterator itr2;
//		   itr2=Result->begin();
//		   if(itr2!=Result->end())
//		   {
//			 NAttributeID = itr2->first;
//			 for(int32 j=0;j<NAttributeID.size();j++)
//			 NValue = itr2->second;
//		  }
//		}
//	   if(MapInMapptr_var)
//	   {
//			delete MapInMapptr_var;
//			MapInMapptr_var = NULL;
//
//	   }
//	   PMString a("SelectedUidListlength : ");
//	   a.AppendNumber(SelectedUidListlength);
//	   a.Append("\n");
//	   a.Append("SelectedUidListlength : ");
//	   a.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//	   //CA(a);
//		for(i=0; i<SelectedUidListlength/*selectUIDList.Length()*/; i++)
//		{
//			UIDRef boxID=selectUIDList.GetRef(i);
//			TagList tagList=itagReader->getTagsFromBox(boxID);
//			if(tagList.size()<=0)
//			{
//				//CA("if(tagList.size()<=0)");
//				continue;
//			}
//			this->refreshThisBoxNewoption(boxID, imagePath);
//			//break;
//		}
//
//		if(isMultipleSectionsAvailable)
//		{
//			//ptrIAppFramework->multipleSectionDataClearInstance();
//		}
//		//ptrIAppFramework->GetSectionData_clearSectionDataCache();		
//
//		break;
//}
//	case 4:
//
////-----------------------------------------------------------------------------------------------------------------------
//
//		ElementIDptr resultvec = NULL;
//		vector<int32>itemmid;
//		vector<int32>ChildIDvec;
//		int32 countofframes=0;
//		static int32 countp=0;
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
//				if (layoutData == nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
//					break;
//				}
//
//				UIDRef newSpreadRef = layoutData->GetSpreadRef();
//				
//				UID pageUID1 = layoutData->GetPage();
//				if(pageUID1 == kInvalidUID)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
//					break;
//				}
//				IDocument* doc1=Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
//				if(doc1==nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No doc");
//					return;
//				}
//				IDataBase* database = ::GetDataBase(doc1);
//				if(database==nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No database");
//					return;
//				}
//
//
//				 InterfacePtr<IPageList> iPageList(doc1,UseDefaultIID());
//				 if (iPageList == nil){
//					// CA("iPageList == nil");
//					 break;
//				 }
//		 
//				 int32 totalNumOfPages = iPageList->GetPageCount();
//				 InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)doc1,UseDefaultIID());
//				if (iSpreadList1==nil)
//				{
//					ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
//					return ;
//				}
//				int32 noofspread= iSpreadList1->GetSpreadCount();
//				PMString spreadsize("spreadsize :");
//				spreadsize.AppendNumber(noofspread);
//				//CA(spreadsize);
//
//
//				for(int numSp_1=0; numSp_1< noofspread; numSp_1++)
//				{
//					//CA("numSp_1");
//					UIDRef temp_spreadUIDRef1(database, iSpreadList1->GetNthSpreadUID(numSp_1));
//					//CA("newSpreadRef == temp_spreadUIDRef1");
//					InterfacePtr<ISpread> spread(temp_spreadUIDRef1, UseDefaultIID());
//					if(!spread)
//					{
//						ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
//						return ;
//					}
//					int numPages=spread->GetNumPages();
//
//					PMString noofpages("noofpages :");
//					noofpages.AppendNumber(numPages);
//					//CA(noofpages);
//
//					for(int32 i=0;i<numPages;i++)
//					{
//						//CA("Pages");
//						UIDList itemsOnPageUIDList(database) ;
//						spread->GetItemsOnPage(i, &itemsOnPageUIDList,kFalse);
//						selectUIDList=itemsOnPageUIDList;
//
//						PMString uidlist("selectUIDList First:");
//						uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//					    //CA(uidlist);
//
//						for( int p=0; p<selectUIDList.Length(); p++)
//						{
//							/*PMString str;
//							str.AppendNumber(p);
//							CA("lllllllllll");
//							CA(str);*/
//							countofframes++;
//						}
//					}
//				}
//
//
//		PMString uidlist("selectUIDList First:");
//		uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//		//CA(uidlist);
//		
//		int32 prevSectionID = -1;
//
//			PMString name("Refreshing Book: ");
//			name.Append(bookName);
//			name.Append("...");
//		RangeProgressBar *progressBar = new RangeProgressBar(name, 0,countofframes+countofframes, kTrue); 
//		//RangeProgressBar progressBar(name, 0,countofframes+countofframes, kTrue);
//
//		/*PMString docname("Refreshing Documents:");
//		docname.Append(documentName);
//		docname.Append("...");*/
//		progressBar->SetTaskText(documentName);
//
//
//
//
//			for(int numSp_1=0; numSp_1< noofspread; numSp_1++)
//				{
//					//CA("numSp_1");
//					UIDRef temp_spreadUIDRef1(database, iSpreadList1->GetNthSpreadUID(numSp_1));
//					//CA("newSpreadRef == temp_spreadUIDRef1");
//					InterfacePtr<ISpread> spread(temp_spreadUIDRef1, UseDefaultIID());
//					if(!spread)
//					{
//						ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
//						return ;
//					}
//					int numPages=spread->GetNumPages();
//
//					PMString noofpages("noofpages :");
//					noofpages.AppendNumber(numPages);
//					//CA(noofpages);
//
//					for(int32 i=0;i<numPages;i++)
//					{
//						UIDList itemsOnPageUIDList(database) ;
//						spread->GetItemsOnPage(i, &itemsOnPageUIDList,kFalse);
//						selectUIDList=itemsOnPageUIDList;
//
//						
//
//						for( int p=0; p<selectUIDList.Length(); p++)
//						{	
//
//			//				countp++;
//			//				PMString tempString("Refreshing Document : ");
//			////				tempString += bookContentNames[p];
//			//				tempString += "...";
//			//				progressBar.SetTaskText(tempString);
//			//				/*for(int32 i = countofframes - 1 ; i >=0 ; --i)
//			//				{*/
//			//					
//			//				/*progressBar.SetPosition(index1);
//			//				index1++;*/
//			//					//Sleep(100);
//			//					progressBar.SetPosition(countp-(countp/2));
//			//				//}
//							
//							TagList tList,tList_checkForHybrid;
//							//IIDXMLElement* ptr;
//							int32 itemIdvar;
//							int32 attributeid;
//							int32 itemgroupIdvar;
//							UIDRef boxID = selectUIDList.GetRef(p);
//
//							tList =  itagReader->getTagsFromBox(selectUIDList.GetRef(p));
//							if(tList.size() == 0)
//							{
//								continue;
//							}
//							for(int32 t=0;t<tList.size();t++)
//							{
//
//
//								itemmid.clear();
//								ChildIDvec.clear();
//								if(tList[t].imgFlag!=1 && tList[t].whichTab==4 )
//								{
//									if(tList[t].typeId==-5)
//									{
//										int32 childTagCount = tList[t].tagPtr->GetChildCount();
//										for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
//										{
//											XMLReference childTagRef = tList[t].tagPtr->GetNthChild(childTagIndex);
//											//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
//											InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
//											if(childTagXMLElementPtr == nil)
//												continue;						
//											TagStruct tagInfo;
//											PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
//											int32 childId = strchildId.GetAsNumber();
//
//											PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
//											int32 ID = strID.GetAsNumber();	
//											if(childId!=-1)
//											iTEM.insert(map<int32,int32>::value_type(childId,ID));
//										}
//									}
//									else if(tList[t].childTag==-1)
//									{
//									  itemIdvar= tList[t].parentId;
//									  attributeid= tList[t].elementId;
//									  if(itemIdvar!=-1)
//									  iTEM.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//								    }
//								    else if(tList[t].childTag==1)
//								     {
//										//CA("bbb");
//									  itemIdvar=tList[t].childId;
//									  attributeid= tList[t].elementId;
//									  if(itemIdvar!=-1)
//									  iTEM.insert(map<int32,int32>::value_type(itemIdvar,attributeid));
//								     }
//							     }
//								else if(tList[t].whichTab==3 && tList[t].imgFlag!=1)
//								{
//									int32 attribute=tList[t].elementId;
//										if(tList[t].childTag==-1 && tList[t].parentId!=-1)
//										  itemgroupIdvar=tList[t].parentId;
//										else if(tList[t].childTag==1)
//										  itemgroupIdvar=tList[t].childId;
//
//										 MapForItemGroup.insert(map<int32,int32>::value_type(itemgroupIdvar,attribute));
//								}
//								int32 Parentid=tList[t].parentId;
//								int32 Parenttpyeid=tList[t].parentTypeID;
//								int32 ImageTypeid=tList[t].typeId;
//								int32 childId = tList[t].childId;
//								int32 lanuageid = tList[t].languageID;
//								int32 sectionId = tList[t].sectionID;
//
//								itemmid.push_back(Parentid);
//								ChildIDvec.push_back(childId);
//// For Item Group
////Images are deleting from document while refreshing when they are not in DB or item is deleted from DB
//					if(Isframedelete==kTrue)
//					{	
//						if(tList[t].whichTab==3)
//						{
//							if(tList[t].imgFlag==1)
//							{
//								//CA("imageflag :");
//								//if image is not in DB then delete that image frame
//								bool16 AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(Parentid,Parenttpyeid,ImageTypeid);
//								if(AssetValuePtrObj == kFalse)
//								{
//									//CA("if image is not in DB then delete image");							
//									DeleteImageFrame(boxID);
//								}
//								else
//								{
//									//if item is not in DB then delete that image frame
//									resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//										if(resultvec!=NULL)
//										{
//											//CA("if Item is not in DB then delete image");
//											DeleteImageFrame(boxID);
//											//CA("not null-1");
//											delete resultvec;
//											resultvec=NULL;
//										}
//								}
//							}
//							//Normal spray case
//							if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//							{
//								//CA("Normal Spray");
//								//if Item is not in DB then delete that text frame
//								resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//									if(resultvec!=NULL)
//									{
//										//CA("if Item is not in DB then delete image");
//										DeleteImageFrame(boxID);
//										delete resultvec;
//										resultvec=NULL;
//									}
//							}
//							//This is for ItemList
//							if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//							{
//								int32 sectionIIID=tList[t].sectionID;
//								//CA("ItemList Spray");
//								//case 1: If the Item is not in DB then delete that text frame
//								resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//								if(resultvec!=NULL)
//								{
//									DeleteTextFrame(boxID);
//									//CA("not null-2");
//									delete resultvec;
//									resultvec=NULL;
//									
//								}
//								else //case 2:If the parent or leading item is in DB but not in section then delete the entire list from the Section
//								{
//									resultvec = ptrIAppFramework->FindParentItemGroupInDB_getAllItemGroupItemsWithDeletedParents(ChildIDvec);
//									if(resultvec!=NULL)
//									{
//									 DeleteTextFrame(boxID);
//									// CA("not null-3");
//									 delete resultvec;
//									 resultvec=NULL;
//									}
//								}
//								
//								//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//								//if the Leading item is in DB but not in Section then also delete that text frame from document.
//							}
//
//							//This is for  isSprayItemPerFrame
//							if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//							{
//								//CA("isSprayItemPerFrame");
//								resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//								if(resultvec!=NULL)
//								{
//									//CA("if Item is not in DB then delete image");
//									DeleteImageFrame(boxID);
//									//CA("not null-4");
//									delete resultvec;
//									resultvec=NULL;
//									
//								}
//							}
//						}//end of if
////=======================================================================================================================================
//// For Item 
//					//for image refresh for item and item group
//						if(tList[t].whichTab==4)
//						{
//							if(tList[t].imgFlag==1)
//							{
//								//CA("imageflag :");
//								 bool16 AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(Parentid,Parenttpyeid,ImageTypeid);
//								if(AssetValuePtrObj == kFalse)
//								{
//									//CA("if image is not in DB then delete image");							
//									DeleteImageFrame(boxID);
//								}
//								else
//								{
//									bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//									if(result==kFalse)
//										DeleteImageFrame(boxID);
//								}
//							}
//
//						//case 1: //This is for Normal Spray
//						if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							//CA("Normal Spray");
//							bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//							if(result==kFalse)
//								DeleteTextFrame(boxID);
//							else//if item is not in section then delete that Image frame 
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,Parentid);
//								if(result1==kFalse)
//								 DeleteTextFrame(boxID);
//							}
//							//if item is not in DB but not in Section then delete that text frame
//						}
//
//						//case 2: //This is for ItemList
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							int32 sectionIIID=tList[t].sectionID;
//							//CA("ItemList Spray");
//							//case 1: If the Item is not in DB then delete that text frame
//							bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//							if(result==kFalse)
//							{
//							 DeleteTextFrame(boxID);
//							}
//							else //case 2:if items parent is not in DB then delete that items text frame from document
//							{
//								resultvec = ptrIAppFramework->FindParentItemInDB_findAllItemsWithDeletedParents(itemmid);
//								if(resultvec!=NULL)
//								{
//								 DeleteTextFrame(boxID);
//								 //CA("not null-5");
//								 delete resultvec;
//								 resultvec=NULL;
//								}
//							}
//							//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//							//if the Leading item is in DB but not in Section then also delete that text frame from document.
//						}
//
//						//case 3: //This is for  isSprayItemPerFrame
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//						{
//
//							bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(Parentid,lanuageid);
//							if(result==kFalse)
//								DeleteTextFrame(boxID);
//						}
//					  }
//				   }//end of if
//				}
//
//							//added by avinash
//							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//							{
//								tList[tagIndex].tagPtr->Release();
//							}
//							//for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//							//{
//								//tList_checkForHybrid[tagIndex].tagPtr->Release();
//							//}
//							//till here
//			  }
//			}
//		  }
//			int32 mapsize=static_cast<int32>(iTEM.size());
//			PMString siz("mapsize :");
//			siz.AppendNumber(mapsize);
//			//CA(siz);
//
//			map<int32,int32>::iterator itr;
//			for(itr=iTEM.begin();itr!=iTEM.end();itr++)
//			{
//				int32 ID=itr->first;
//				itemids.push_back(ID);
//				PMString ak("ItemID :");
//				ak.AppendNumber(ID);
//				//CA(ak);
//			}
////---------------------End----------------------------------------------------------------------------
//
////--------Start to collect ItemGroupIDs--------------------------------------------------------------
//			int32 sizz12=static_cast<int32>(MapForItemGroup.size());
//			PMString t("MapForItemGroup :");
//			t.AppendNumber(sizz12);
//			//CA(t);
//			map<int32,int32>::iterator itrr12;
//			for(itrr12=MapForItemGroup.begin();itrr12!=MapForItemGroup.end();itrr12++)
//			{
//			  int32 ItemGroupID=itrr12->first;
//			  if(ItemGroupID!=-1)
//			  itemgroupids.push_back(ItemGroupID);
//			}
////--------------------End ------------------------------------------------------------------------
//
//		ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   before getDocumentSelectedBoxIds.");
//		if(!getDocumentSelectedBoxIds(progressBar))
//		{					
//			return;
//		}
//		ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   after getDocumentSelectedBoxIds..");
//
//		if(Mediator::isFrameRefresh == kFalse)
//		{
//			CSprayStencilInfo objCSprayStencilInfo;
//
//			PMString selectUIDListLength("selectUIDList.size = ");
//			selectUIDListLength.AppendNumber(selectUIDList.Length());
//			ptrIAppFramework->LogDebug(selectUIDListLength);
//
//			PMString uidlist("selectUIDList second :");
//			uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//			//CA(uidlist);
//			
//			ptrIAppFramework->LogDebug("*** Document Scanning process Started ***");
//			bool16 result = getStencilInfoNewOption(selectUIDList,objCSprayStencilInfo);
//			ptrIAppFramework->LogDebug("*** Document Scanning process finished ***");
//			
//			if(result == kTrue)
//			{	
//				GetSectionData getSectionData;	
//				
//				if(objCSprayStencilInfo.isCopy)			
//					getSectionData.addCopyFlag = kTrue;			
//				if(objCSprayStencilInfo.isProductCopy)
//					getSectionData.addProductCopyFlag = kTrue;
//				if(objCSprayStencilInfo.isSectionCopy)
//				{
//					getSectionData.addSectionLevelCopyAttrFlag = kTrue;
//					getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
//					getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;
//				}
//
//				if(objCSprayStencilInfo.isAsset)
//					getSectionData.addImageFlag = kTrue;
//				if(objCSprayStencilInfo.isProductAsset)
//					getSectionData.addProductImageFlag = kTrue;
//				
//				if(objCSprayStencilInfo.isBMSAssets){
//					getSectionData.addImageFlag = kTrue;
//					getSectionData.addItemBMSAssetsFlag = kTrue;
//				}
//				if(objCSprayStencilInfo.isProductBMSAssets)
//				{
//					getSectionData.addImageFlag = kTrue;
//					getSectionData.addProductBMSAssetsFlag = kTrue;
//				}
//				if(objCSprayStencilInfo.isSectionLevelBMSAssets)
//					getSectionData.addPubLogoAssetFlag = kTrue;
//
//
//				if(objCSprayStencilInfo.isDBTable)
//				{				
//					getSectionData.addDBTableFlag = kTrue;
//					//getSectionData.addProductDBTableFlag = kTrue; //for Item Group Lists spray
//
//					getSectionData.addChildCopyAndImageFlag = kTrue;		
//				}
//				if(objCSprayStencilInfo.isProductDBTable)
//				{
//					getSectionData.addProductDBTableFlag = kTrue;
//					getSectionData.addChildCopyAndImageFlag = kTrue;		
//				}
//				if(objCSprayStencilInfo.isCustomTablePresent)
//					getSectionData.addCustomTablePresentFlag = kTrue;
//				
//				
//				if(objCSprayStencilInfo.isItemPVMPVAssets)
//				{
//					getSectionData.addItemPVMPVAssetFlag = kTrue;
//					if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
//							getSectionData.itemPVAssetIdList.push_back(objCSprayStencilInfo.itemPVAssetIdList[i]);				
//				}
//				if(objCSprayStencilInfo.isProductPVMPVAssets)
//				{
//					getSectionData.addProductPVMPVAssetFlag = kTrue;
//					if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
//							getSectionData.productPVAssetIdList.push_back(objCSprayStencilInfo.productPVAssetIdList[i]);	
//				}
//				if(objCSprayStencilInfo.isSectionPVMPVAssets)
//				{
//					getSectionData.addSectionPVMPVAssetFlag = kTrue;
//					if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
//							getSectionData.sectionPVAssetIdList.push_back(objCSprayStencilInfo.sectionPVAssetIdList[i]);	
//				}
//				if(objCSprayStencilInfo.isPublicationPVMPVAssets)
//				{
//					getSectionData.addPublicationPVMPVAssetFlag = kTrue;
//					if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
//							getSectionData.publicationPVAssetIdList.push_back(objCSprayStencilInfo.publicationPVAssetIdList[i]);	
//				}
//				if(objCSprayStencilInfo.isCatagoryPVMPVAssets)
//				{
//					getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
//					if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
//							getSectionData.catagoryPVAssetIdList.push_back(objCSprayStencilInfo.catagoryPVAssetIdList[i]);	
//				}
//				
//				if(objCSprayStencilInfo.isCategoryImages)
//				{
//					getSectionData.addCategoryImages = kTrue;
//					if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
//							getSectionData.categoryAssetIdList.push_back(objCSprayStencilInfo.categoryAssetIdList[i]);	
//				}
//
//				if(objCSprayStencilInfo.isEventSectionImages)
//				{
//					getSectionData.addEventSectionImages = kTrue;
//					if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
//						for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
//							getSectionData.eventSectionAssetIdList.push_back(objCSprayStencilInfo.eventSectionAssetIdList[i]);	
//				}
//						
//				if(objCSprayStencilInfo.isHyTable)
//					getSectionData.addHyTableFlag = kTrue;
//				if(objCSprayStencilInfo.isProductHyTable)
//					getSectionData.addHyTableFlag = kTrue;
//				if(objCSprayStencilInfo.isSectionLevelHyTable)
//					getSectionData.addHyTableFlag = kTrue;
//
//				if(objCSprayStencilInfo.isChildTag)			
//					getSectionData.addChildCopyAndImageFlag = kTrue;			
//
//				if(objCSprayStencilInfo.isProductChildTag)
//					getSectionData.addProductChildCopyAndImageFlag = kTrue;
//
//				if(objCSprayStencilInfo.isEventField)
//					getSectionData.isEventField = kTrue;
//
//				if(getSectionData.addChildCopyAndImageFlag)
//				{
//					getSectionData.addDBTableFlag = kTrue;
//					getSectionData.addProductDBTableFlag = kTrue;
//				}
//
//				if(getSectionData.addProductChildCopyAndImageFlag)
//					getSectionData.addCustomTablePresentFlag = kTrue;
//				
//				getSectionData.CatagoryId = -1;		
//				getSectionData.isOneSource = kFalse;
//				
//				getSectionData.addComponentTableFlag = kFalse;
//				getSectionData.addAccessoryTableFlag = kFalse;
//				getSectionData.addXRefTableFlag = kFalse;
//				
//						
//				getSectionData.isGetWholePublicationOrCatagoryDataFlag = kFalse;	
//				if(itemids.size()>0 && item_attribute_ids.size()>0)
//				{
//
//					MapInMapptr_var  = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIds(itemids,item_attribute_ids,itemgroupids,itemgroup_attributeids);
//
//					PMString OriginalStr;
//					MapInMap::iterator itr;
//					/*if(itemids.size()>0)
//					{*/
//						for(itr=MapInMapptr_var->begin();itr!=MapInMapptr_var->end();itr++)
//						{
//						  NItemID=itr->first;
//						  Result =itr->second;
//						}
//					   multimap<vector<int32>,vector<PMString>>::iterator itr2;
//					   itr2=Result->begin();
//					   if(itr2!=Result->end())
//					   {
//						 NAttributeID = itr2->first;
//						 for(int32 j=0;j<NAttributeID.size();j++)
//						 NValue = itr2->second;
//					  }
//				}
//				int32 tt=static_cast<int32>(itemgroupids.size());
//				for(int32 i=0;i<tt;i++)
//				{
//					PMString t("ItemGroupID :");
//					t.AppendNumber(itemgroupids.at(i));
//					//CA(t);
//				}
//				int32 rr=static_cast<int32>(itemgroup_attributeids.size());
//				for(int32 i=0;i<rr;i++)
//				{
//					PMString ww("IGAttributes :");
//					ww.AppendNumber(itemgroup_attributeids.at(i));
//					//CA(ww);
//
//				}
//				
//				
//				if(itemgroupids.size()>0 && itemgroup_attributeids.size()>0)
//				{
//					
//					MapInMapptr_var_ItemGroup = ptrIAppFramework->GetItem_getItemAndItemGroupByIdListAndFieldIdsNew(Emptyvec_Item,Emptyvec_IemAttribute,itemgroupids,itemgroup_attributeids);
//						
//						MapInMap::iterator itrr1;
//						for(itrr1=MapInMapptr_var_ItemGroup->begin();itrr1!=MapInMapptr_var_ItemGroup->end();itrr1++)
//						{
//						  NItemGroupID=itrr1->first;
//						  Result =itrr1->second;
//						}
//						
//						multimap<vector<int32>,vector<PMString>>::iterator itrr22;
//						itrr22=Result->begin();
//						if(itrr22!=Result->end())
//						{
//						 NAttributeId_ItemGroup = itrr22->first;
//						 for(int32 j=0;j<NAttributeId_ItemGroup.size();j++)
//						 NString = itrr22->second;
//						}
//
//
//					/*for(itr=MapInMapptr_var->begin();itr!=MapInMapptr_var->end();itr++)
//					{
//					  NItemGroupID=itr->first;
//					  Result =itr->second;
//					}
//				   multimap<vector<int32>,vector<PMString>>::iterator itr2;
//				   itr2=Result->begin();
//				   if(itr2!=Result->end())
//				   {
//					 NAttributeId_ItemGroup = itr2->first;
//					 for(int32 j=0;j<NAttributeId_ItemGroup.size();j++)
//					 NString = itr2->second;
//				  }*/
//				}
//			   if(MapInMapptr_var)
//			   {
//					//CA("NOT NULL");
//					delete MapInMapptr_var;
//					MapInMapptr_var = NULL;
//
//			   }
//			}			
//		}	
//		
//
//
//		for(i=0; i<selectUIDList.Length(); i++)
//		{	
//			//CA("aaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//			UIDRef boxID=selectUIDList.GetRef(i);					
//		
//				if(isMultipleSectionsAvailable)
//				{
//					TagList tList=itagReader->getTagsFromBox(boxID);
//					int numTags=static_cast<int>(tList.size());
//					if(numTags<=0)//This can be a Tagged Frame
//					{	
//						tList=itagReader->getFrameTags(boxID);							
//					}	
//
//					int numTags1=static_cast<int>(tList.size());
//					if(numTags1<=0)
//					{
//						continue;
//					}
//					//added by avinash
//					//CA("%%%%%%%%%%%");
//					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//					{
//						tList[tagIndex].tagPtr->Release();
//					}
//					//till here
//				}
//
//				
//
////				PMString tempString("Refreshing Document : ");
//////				tempString += bookContentNames[p];
////				tempString += "...";
////				progressBar.SetTaskText(tempString);
//				/*for(int32 i = countofframes - 1 ; i >=0 ; --i)
//				{*/
//					
//				/*progressBar.SetPosition(index1);
//				index1++;*/
//					//Sleep(100);
//					progressBar->SetPosition((countofframes+countofframes) - (countofframes-i) + 1);
//				//}
//	
//			if(isRefreshTabByAttributeCheckBoxSelected==kTrue)
//			{
//				ptrIAppFramework->LogDebug("**** before Refresh  ****");
//				this->refreshThisBoxNewoption(boxID, imagePath);	
//				ptrIAppFramework->LogDebug("**** after Refresh  ****");
//			}
//
//		}
//		
//		progressBar->Abort();
//		if(progressBar!=NULL)
//			delete progressBar;
//		progressBar=NULL;
//
//		
//		/*PMString uidlist11("selectUIDList First:");
//						uidlist11.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//					    CA(uidlist11);*/
//
//		if(isMultipleSectionsAvailable)
//		{
//			//CA("r");
//			//ptrIAppFramework->multipleSectionDataClearInstance();
//		}
//		//CA("s");
//		//ptrIAppFramework->GetSectionData_clearSectionDataCache();			
//		
//		
//
//		UIDRef newDocRef ;
//		if(BookReportData::vecReportRows.size() > 0 && isDummyCall){			
//			newDocRef = UIDRef::gNull;
//			BookReportData::entireReport.push_back(BookReportData::vecReportRows);
//
//		}
//		if(isDummyCall){
////			CA("isDummyCall == kTrue");	
//			for(i=0; i<selectUIDList.Length(); i++)
//			{	
//				UIDRef boxID=selectUIDList.GetRef(i);						
//				BookReportData::fillDocumentTagList(boxID,imagePath);
//			}
//			BookReportData::entireBookTagList.push_back(BookReportData::documentTagList);
//			
//			if(BookReportData::entireBookTagList.size() > 0)
//			{
//				//PMString asd("BEfor BookReportData::entireBookTagList \nDocument Taglist size = ");
//				//asd.AppendNumber(static_cast<int32>(BookReportData::entireBookTagList.size()));
//				//CA(asd);
//
//				BookReportData::fillVecNewReportData();
//				BookReportData::documentTagList.clear();
//				
//				//PMString asd1("entireNewItemList size = ");
//				//asd1.AppendNumber(static_cast<int32>(BookReportData::entireNewItemList.size()));
//				//CA(asd1);
//			}
//		}
//		break;
//	}
//}



void Refresh::doRefresh(int option,PMString bookName,PMString documentName)
{	
	//CA("inside Refresh::doRefresh 1321 ");
	PMString imagePath;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	if(imageDownLoadPath == "")
	{
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
			return;
		}
		imagePath=ptrIClientOptions->getImageDownloadPath();
		imageDownLoadPath = imagePath;
	}
	if(imagePath == "")
		imagePath = imageDownLoadPath;
	//CA("imagePath 1	:	"+imagePath);	
	//Commented by Yogesh Joshi on 1 March 06
	if(imagePath!="")
	{
		const char *imageP=(imagePath.GetPlatformString().c_str()/*GrabCString()*/); //Cs4
		 
		#ifdef MACINTOSH
			//if(imageP[std::strlen(imageP)-1]!=':')
			//	imagePath+=":";
		#else
			if(imageP[std::strlen(imageP)-1]!='\\')
				imagePath+="\\";
		#endif
	}
	//CA("imagePath 2 :	"+imagePath);

	selectUIDList.Clear();
	if(vec_ptr != NULL)
		delete vec_ptr;

	vec_ptr = new vec_GetMultipleSectionData; 
	if(vec_ptr == NULL)
	{			
		return;
	}

//	ChangedTextList.clear();
	int i;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::!itagReader");
		return ;
	}

	switch(option)
	{
	case 1:
		  {
			//for current page option
			//CA("Refresh::doRefresh 1");
			UID pageUID;
			//TagReader tReader;
			int32 pageNumber= 0;
			if(!this->isValidPageNumber(pageNumber, pageUID))
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isValidPageNumber");			
				break; 
			}
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
			IDataBase* database= ::GetDataBase(document); 
						
			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
			int32 spreadNo = -1; 

			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
			{ 
				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
				if(spread->GetPageIndex(pageUID)!= -1) 
				{			
					spreadNo = p; 
					break; 
				}
			}

			if(spreadNo==-1)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::spreadNo == -1");			
				break; 
			}

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->/*::*/QueryFrontLayoutData()); //Cs4
			if (layoutData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::layoutData == nil");			
				break; 
			}
			 pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::pageUID == kInvalidUID");			
				break; 
			}

			/*IGeometry* spreadItem = layoutData->GetSpread();
			if (spreadItem == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::spreadItem == nil");			
				break; 
			}*/
			UIDRef SpreadRef=layoutData->GetSpreadRef();
			InterfacePtr<ISpread> iSpread(SpreadRef, UseDefaultIID());	
			if (iSpread == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doRefresh::iSpread == nil");			
				break; 
			}

            //CA("Refresh::doRefresh 1.1");
			UIDList tempList(database);
			pageNumber=iSpread->GetPageIndex(pageUID);
			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);
			selectUIDList=tempList;


			
			for( int p=0; p<selectUIDList.Length(); p++)
			{	
				InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
				if(!iHier)
					continue;
				UID kidUID;
				int32 numKids=iHier->GetChildCount();
				bool16 isGroupFrame = kFalse ;
				isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));

				for(int j=0;j<numKids;j++)
				{
					//CA("1");
					kidUID=iHier->GetChildUID(j);
					UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
					IIDXMLElement* ptr;
					TagList tList,tList_checkForHybrid;

					if(isGroupFrame == kTrue) 
					{
						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
						if(tList_checkForHybrid.size() == 0)
						{
							//CA("tList_Checkforhybrid.size() == 0	+	return kFalse");
							continue;
						}
						if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
						else
							tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
					}
					else 
					{
						tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
						if(tList_checkForHybrid.size() == 0)
						{
							//CA("tList_Checkforhybrid.size() == 0	+	return kFalse");
							continue;
						}
						if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
							tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
						else
							tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
					}

					if(!doesExist(tList))//if(!doesExist(ptr))
					{
						selectUIDList.Append(kidUID);
					}

					//-------lalit--------
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
					{
						tList_checkForHybrid[tagIndex].tagPtr->Release();
					}

				}
			}

			bool16 isSectiondataPopulated = this->getSectionDataforBoxList(selectUIDList);  // Added to grab JSON data
			if(!isSectiondataPopulated )
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isSectiondataPopulated");	
				return;
			}

            //CA("Refresh::doRefresh 1.2");
			for(i=0; i<selectUIDList.Length(); i++)
			{	
				
				UIDRef boxID=selectUIDList.GetRef(i);
				this->refreshThisBox(boxID, imagePath);
			}

			if(isMultipleSectionsAvailable)
			{
				//ptrIAppFramework->multipleSectionDataClearInstance();
			}
			//ptrIAppFramework->GetSectionData_clearSectionDataCache();		

			ptrIAppFramework->EventCache_clearCurrentSectionData();	
			ptrIAppFramework->clearAllStaticObjects();

			break;
		  }

	case 2:
	{//for frame refresh 


//CA("Refresh::doRefresh 2");//textboxvaluevec	
		//if(siz==1)
		//{
		ElementIDptr resultvec = NULL;
		vector<double>itemmid;
		vector<double>ChildIDvec;
		
			ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   case 2: .");
			if(!getAllPageItemsFromPage(Mediator::refreshPageNumber))
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!getAllPageItemsFromPage");		
				return;
			}

			bool16 isSectiondataPopulated = this->getSectionDataforBoxList(selectUIDList);  // Added to grab JSON data
			if(!isSectiondataPopulated )
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isSectiondataPopulated");	
				return;
			}
				
			for(i=0; i<selectUIDList.Length(); i++)
			{	
				TagList tList;
				UIDRef boxID=selectUIDList.GetRef(i);
				tList =  itagReader->getTagsFromBox(boxID);
				if(tList.size() == 0)
				{
					continue;
				}
//				for(int32 t=0;t<tList.size();t++)
//				{
//					itemmid.clear();
//					ChildIDvec.clear();
//					vector<int32>Vec_sectionIdlist;
//					int32 PublicationID = tList[t].pbObjectId;
//					int32 parentid = tList[t].parentId;
//					int32 childId = tList[t].childId;
//					int32 languageId = tList[t].languageID;
//					int32 Parenttpyeid=tList[t].parentTypeID;
//					int32 ImageTypeid=tList[t].typeId;
//
//					Vec_sectionIdlist.clear();
//					int32 sectionId = tList[t].sectionID;
//					itemmid.push_back(parentid);
//					ChildIDvec.push_back(childId);
//
//					//Vec_sectionIdlist.push_back(sectionId);
// //For Item Group 
//					//Images are deleting from document while refreshing when they are not in DB or item is deleted from DB
//					if(tList[t].whichTab==3)
//					{
//						if(tList[t].imgFlag==1)
//						{
//							//CA("imageflag :");
//							//if image is not in DB then delete that image frame
//							bool16 AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(parentid,Parenttpyeid,ImageTypeid);
//							if(AssetValuePtrObj == kFalse)
//							{
//								//CA("if image is not in DB then delete image");							
//								DeleteImageFrame(boxID);
//							}
//							else
//							{
//								//if item is not in DB then delete that image frame
//								resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//								if(resultvec!=NULL)
//								{
//									//CA("if Item Group is not in DB then delete image");
//									DeleteImageFrame(boxID);
//									//CA("not null-7");
//									delete resultvec;
//									resultvec=NULL;
//								}
//								else//if item Group is not in section then delete that Image frame 
//								{
//									bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//									if(result1==kFalse)
//									{
//										//CA("if image is not in section then delete image");
//										DeleteImageFrame(boxID);
//									}
//								}
//							}
//						}
//						//Normal Spray
//						if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							//CA("Normal Spray");
//							//if Item Group is not in DB then delete that text frame
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//							if(resultvec != NULL)
//							{
//								//CA("if Item is not in DB then delete that text frame");
//								DeleteTextFrame(boxID);
//								//CA("not null-8");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else//if item Group is not in section then delete that text frame 
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//								if(result1==kFalse)
//								{
//									//CA("if item is not in section then delete that text frame ");
//									 DeleteTextFrame(boxID);
//								}
//							}
//						}
//						//This is for ItemList
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							int32 sectionIIID=tList[t].sectionID;
//							//CA("ItemList Spray");
//							//case 1: If the Item is not in DB then delete that text frame
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								DeleteTextFrame(boxID);
//								//CA("not null-9");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else //case 2:If the parent or leading item is in DB but not in section then delete the entire list from the Section
//							{
//								resultvec = ptrIAppFramework->FindParentItemGroupInDB_getAllItemGroupItemsWithDeletedParents(ChildIDvec);
//								if(resultvec!=NULL)
//								{
//								 DeleteTextFrame(boxID);
//								 //CA("not null-10");
//								 delete resultvec;
//								 resultvec=NULL;
//								}
//								else
//								{
//									resultvec = ptrIAppFramework->FindParentItemGroupInSection_getItemGroupPubItemsWithDeletedParents(childId,sectionId);
//									if(resultvec!=NULL)
//									{
//										 DeleteTextFrame(boxID);
//										 //CA("not null-11");
//										 delete resultvec;
//										 resultvec=NULL;
//									}
//								}
//							}
//							//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//							//if the Leading item is in DB but not in Section then also delete that text frame from document.
//						}
//
//						//This is for  isSprayItemPerFrame
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//						{
//							//CA("isSprayItemPerFrame");
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								DeleteTextFrame(boxID);
//								//CA("not null-11");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,childId);
//								if(result1==kFalse)
//								{
//									 DeleteTextFrame(boxID);
//									 //CA("not null-12");
//									 delete resultvec;
//									 resultvec=NULL;
//								}
//							}
//						}
//					}
////=====================================================================================================================================
// //for item only
//					if(tList[t].whichTab==4)
//					{
//					if(tList[t].imgFlag==1)
//					{
//						//CA("imageflag :");
//						//if image is not in DB then delete that image frame
//						//CA("b4 before");
//						bool16  AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(parentid,Parenttpyeid,ImageTypeid);
//						//CA("b4 after");
//						if(AssetValuePtrObj == kFalse)
//						{
//							//CA("if image is not in DB then delete image");							
//							DeleteImageFrame(boxID);
//						}
//						else
//						{
//							//if item is not in DB then delete that image frame
//							bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(parentid,languageId);
//							if(result==kFalse)
//							{
//								//CA("if Item is not in DB then delete image");
//								DeleteImageFrame(boxID);
//							}
//							else//if item is not in section then delete that Image frame 
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//								if(result1==kFalse)
//								{
//									//CA("if image is not in section then delete image");
//									DeleteImageFrame(boxID);
//								}
//							}
//						}
//					}
//					
//					//This is for Normal Spray
//					if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						//CA("Normal Spray");
//						//if Item is not in DB then delete that text frame
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(parentid,languageId);
//						if(result==kFalse)
//						{
//							//CA("if Item is not in DB then delete that text frame");
//						  DeleteTextFrame(boxID);
//						}
//						else//if item is not in section then delete that text frame 
//						{
//							bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//							if(result1==kFalse)
//							{
//								//CA("if item is not in section then delete that text frame ");
//							 DeleteTextFrame(boxID);
//							}
//						}
//					}
//					//This is for ItemList
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						int32 sectionIIID=tList[t].sectionID;
//						//CA("ItemList Spray");
//						//case 1: If the Item is not in DB then delete that text frame
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(childId,languageId);
//						if(result==kFalse)
//						{
//						 DeleteTextFrame(boxID);
//						}
//						else //case 2:if items parent is not in DB then delete that items text frame from document
//						{
//							resultvec = ptrIAppFramework->FindParentItemInDB_findAllItemsWithDeletedParents(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//							 DeleteTextFrame(boxID);
//							// CA("not null-14");
//							 delete resultvec;
//							 resultvec=NULL;
//							}
//							else//case 3:if items parent is in DB but not in section then delete that text frame 
//							{
//								resultvec = ptrIAppFramework->FindParentItemInSection_getPubItemWithDeletedParents(childId,sectionId);
//								if(resultvec!=NULL)
//								{
//									DeleteTextFrame(boxID);
//									//CA("not null-15");
//									delete resultvec;
//									resultvec=NULL;
//								}
//						
//							}
//						}
//						//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//						//if the Leading item is in DB but not in Section then also delete that text frame from document.
//					}
//					//This is for  isSprayItemPerFrame
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//					{
//						//CA("isSprayItemPerFrame");
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(childId,languageId);
//						if(result==kFalse)
//						{
//						 DeleteTextFrame(boxID);
//						}
//						else
//						{
//							bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,childId);
//							if(result1==kFalse)
//							{
//							 DeleteTextFrame(boxID);
//							}
//						}
//					}
//				  }
//				 
//				}

				
				//till here
				this->refreshThisBox(boxID, imagePath);

				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					//CA("not null-16");
					tList[tagIndex].tagPtr->Release();
				}
			}
		
			ptrIAppFramework->EventCache_clearCurrentSectionData();		
			ptrIAppFramework->clearAllStaticObjects();


		break;
		}
	case 3:
//CA("Refresh::doRefresh 3");
		{
		ptrIAppFramework->LogDebug("Inside AP7_RefreshContent::Refresh::doRefresh   case 3: .");
		if(!getAllBoxIds())
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!getAllBoxIds");		
			return;
		}
		int32 siz=static_cast<int32>(selectUIDList.Length());
		//PMString s("selectUIDList : ");
		//s.AppendNumber(siz);
		//CA(s);
		bool16 isSectiondataPopulated = this->getSectionDataforBoxList(selectUIDList);  // Added to grab JSON data
		if(!isSectiondataPopulated )
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isSectiondataPopulated");	
			return;
		}

		for(i=0; i<selectUIDList.Length(); i++)
		{
			//CA("123");
			UIDRef boxID=selectUIDList.GetRef(i);
			this->refreshThisBox(boxID, imagePath);
		}

		if(isMultipleSectionsAvailable)
		{
			//ptrIAppFramework->multipleSectionDataClearInstance();
		}
		ptrIAppFramework->EventCache_clearCurrentSectionData();		
		ptrIAppFramework->clearAllStaticObjects();

		break;
		}
	case 4:
// 4-Book level refresh

//CA("case 4");
	/*		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

			if(!iSelectionManager)
			{	
			//	CA("null selection");
				return;
			}
			// Get text selection 
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 

			InterfacePtr<ITextFocus> iFocus(pTextTarget->QueryTextFocus()); 

//		ITextFocus * iFocus=Utils<ISelectUtils>()->QueryTextFocus();
*/
/*		if(!iFocus)
			return;

		InterfacePtr<ITextModel> textModel(iFocus->QueryModel());
		if (textModel==nil)
			return;
		
		IFrameList* frameList=textModel->QueryFrameList();
		if(!frameList)
			return;
		UID frameUID=frameList->GetNthFrameUID(0);
				
		IDocument *docPtr=::GetFrontDocument();
		IDataBase* db =::GetDataBase(docPtr);
		if(db==nil)
			return;

		UIDRef boxUIDRef(db, frameUID);

		this->refreshThisBox(boxUIDRef, imagePath);
*/
//CA("Refresh::doRefresh 4");	
		ElementIDptr resultvec = NULL;
		vector<double>itemmid;
		vector<double>ChildIDvec;



		/*PMString uidlist("selectUIDList First:");
		uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
		CA(uidlist);*/
		
		InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!itagReader");
			return ;
		}
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::fntDoc==nil");	
			return;
		}
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::layout == nil");		
			return;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::database==nil");			
			return;
		}
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::iSpreadList==nil");				
			return;
		}
		bool16 collisionFlag=kFalse;
		UIDList allPageItems(database);

		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!spread");						
				return ;
			}
			int numPages=spread->GetNumPages();

			for(int i=0; i<numPages; i++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(i, &tempList, kFalse);
				allPageItems.Append(tempList);
			}
		}
		selectUIDList= allPageItems;
		

		int32 countUidis=0;
		for( int p=0; p<selectUIDList.Length(); p++)
		{
			countUidis++;
		}

		PMString uidlist("selectUIDList before for Loop :");
		uidlist.AppendNumber(static_cast<int32>(countUidis));
		//CA(uidlist);

			PMString name("Refreshing Book: ");
			name.Append(bookName);
			name.Append("...");
			name.SetTranslatable(kFalse);
		RangeProgressBar *progressBar = new RangeProgressBar(name,0,countUidis+countUidis/*, kTrue*/); 
		
		//PMString docname(/*"Refreshing Documents:"*/);
		//docname.Append(documentName);
		//docname.Append("...");
		progressBar->SetTaskText(documentName);
//till here

			/*CA("load 1");*/
		if(!getDocumentSelectedBoxIds(progressBar))
		{					
			return;
		}
			/*CA("load 2");*/
		if(Mediator::isFrameRefresh == kFalse)
		{
			//CA("in isFrameRefresh");
			CSprayStencilInfo objCSprayStencilInfo;

			/*itemIds.clear();
			productIds.clear();*/

			if(vec_ptr != NULL)
				delete vec_ptr;

			vec_ptr = new vec_GetMultipleSectionData; 
			if(vec_ptr == NULL)
			{
				
				return;
			}
			PMString selectUIDListLength("selectUIDList.size = ");
			selectUIDListLength.AppendNumber(selectUIDList.Length());
			ptrIAppFramework->LogDebug(selectUIDListLength);
			
			
			ptrIAppFramework->LogDebug("*** Document Scanning process Started ***");
			bool16 result = getStencilInfo(selectUIDList,objCSprayStencilInfo);
			ptrIAppFramework->LogDebug("*** Document Scanning process finished ***");
		
			if(result == kTrue)
			{	
				GetSectionData getSectionData;	
				
				if(objCSprayStencilInfo.isCopy)			
					getSectionData.addCopyFlag = kTrue;			
				if(objCSprayStencilInfo.isProductCopy)
					getSectionData.addProductCopyFlag = kTrue;
				if(objCSprayStencilInfo.isSectionCopy)
				{
					getSectionData.addSectionLevelCopyAttrFlag = kTrue;
					getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
					getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;
				}

				if(objCSprayStencilInfo.isAsset)
					getSectionData.addImageFlag = kTrue;
				if(objCSprayStencilInfo.isProductAsset)
					getSectionData.addProductImageFlag = kTrue;
				
				if(objCSprayStencilInfo.isBMSAssets){
					getSectionData.addImageFlag = kTrue;
					getSectionData.addItemBMSAssetsFlag = kTrue;
				}
				if(objCSprayStencilInfo.isProductBMSAssets)
				{
					getSectionData.addImageFlag = kTrue;
					getSectionData.addProductBMSAssetsFlag = kTrue;
				}
				if(objCSprayStencilInfo.isSectionLevelBMSAssets)
					getSectionData.addPubLogoAssetFlag = kTrue;


				if(objCSprayStencilInfo.isDBTable)
				{				
					getSectionData.addDBTableFlag = kTrue;
					getSectionData.addProductDBTableFlag = kTrue; //for Item Group Lists spray

					getSectionData.addChildCopyAndImageFlag = kTrue;		
				}
				if(objCSprayStencilInfo.isProductDBTable)
				{
					getSectionData.addProductDBTableFlag = kTrue;
					getSectionData.addChildCopyAndImageFlag = kTrue;		
				}
				if(objCSprayStencilInfo.isCustomTablePresent)
					getSectionData.addCustomTablePresentFlag = kTrue;
				
				
				if(objCSprayStencilInfo.isItemPVMPVAssets)
				{
					getSectionData.addItemPVMPVAssetFlag = kTrue;
					if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
							getSectionData.itemPVAssetIdList.push_back(objCSprayStencilInfo.itemPVAssetIdList[i]);				
				}
				if(objCSprayStencilInfo.isProductPVMPVAssets)
				{
					getSectionData.addProductPVMPVAssetFlag = kTrue;
					if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
							getSectionData.productPVAssetIdList.push_back(objCSprayStencilInfo.productPVAssetIdList[i]);	
				}
				if(objCSprayStencilInfo.isSectionPVMPVAssets)
				{
					getSectionData.addSectionPVMPVAssetFlag = kTrue;
					if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
							getSectionData.sectionPVAssetIdList.push_back(objCSprayStencilInfo.sectionPVAssetIdList[i]);	
				}
				if(objCSprayStencilInfo.isPublicationPVMPVAssets)
				{
					getSectionData.addPublicationPVMPVAssetFlag = kTrue;
					if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
							getSectionData.publicationPVAssetIdList.push_back(objCSprayStencilInfo.publicationPVAssetIdList[i]);	
				}
				if(objCSprayStencilInfo.isCatagoryPVMPVAssets)
				{
					getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
					if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
							getSectionData.catagoryPVAssetIdList.push_back(objCSprayStencilInfo.catagoryPVAssetIdList[i]);	
				}
				
				if(objCSprayStencilInfo.isCategoryImages)
				{
					getSectionData.addCategoryImages = kTrue;
					if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
							getSectionData.categoryAssetIdList.push_back(objCSprayStencilInfo.categoryAssetIdList[i]);	
				}

				if(objCSprayStencilInfo.isEventSectionImages)
				{
					getSectionData.addEventSectionImages = kTrue;
					if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
						for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
							getSectionData.eventSectionAssetIdList.push_back(objCSprayStencilInfo.eventSectionAssetIdList[i]);	
				}
						
				if(objCSprayStencilInfo.isHyTable)
					getSectionData.addHyTableFlag = kTrue;
				if(objCSprayStencilInfo.isProductHyTable)
					getSectionData.addHyTableFlag = kTrue;
				if(objCSprayStencilInfo.isSectionLevelHyTable)
					getSectionData.addHyTableFlag = kTrue;

				if(objCSprayStencilInfo.isChildTag)			
					getSectionData.addChildCopyAndImageFlag = kTrue;			

				if(objCSprayStencilInfo.isProductChildTag)
					getSectionData.addProductChildCopyAndImageFlag = kTrue;

				if(objCSprayStencilInfo.isEventField)
					getSectionData.isEventField = kTrue;

				if(getSectionData.addChildCopyAndImageFlag)
				{
					getSectionData.addDBTableFlag = kTrue;
					getSectionData.addProductDBTableFlag = kTrue;
				}

				if(getSectionData.addProductChildCopyAndImageFlag)
					getSectionData.addCustomTablePresentFlag = kTrue;
				
				getSectionData.CatagoryId = -1;		
				getSectionData.isOneSource = kFalse;
				
				getSectionData.addComponentTableFlag = kFalse;
				getSectionData.addAccessoryTableFlag = kFalse;
				getSectionData.addXRefTableFlag = kFalse;
					
				getSectionData.isGetWholePublicationOrCatagoryDataFlag = kFalse;	


				PMString aa("vec_ptr->size()");
				aa.AppendNumber((int32)vec_ptr->size());
				//CA(aa);
			//	if(vec_ptr->size() == 1)
			//	//for(int32 vectptr=0;vectptr<=vec_ptr->size();vectptr++)
			//	{	
			//		//CA("if(vec_ptr->size() == 1)");
			//		isMultipleSectionsAvailable = kFalse;
			//		getSectionData.SectionId = vec_ptr->at(0).sectionId;
			//		getSectionData.PublicationId = vec_ptr->at(0).publicationId;
			//		getSectionData.languageId = vec_ptr->at(0).languageId;	

			//		PMString MapForItemsPerSectionLength("MapForItemsPerSection.Length = ");
			//		MapForItemsPerSectionLength.AppendNumber(static_cast<int32>(MapForItemsPerSection.size()));
			//		ptrIAppFramework->LogDebug(MapForItemsPerSectionLength);
			//		
			//		PMString ItemIdsStrForDebug("ItemIds = ");

			//		multiSectionMap::iterator itr;
			//		itr = MapForItemsPerSection.find(getSectionData.SectionId);
			//		if(itr != MapForItemsPerSection.end())
			//		{
			//			UniqueIds itemIds = itr->second;
			//			UniqueIds::iterator itemIdsItr;
			//			for(itemIdsItr = itemIds.begin(); itemIdsItr != itemIds.end(); ++itemIdsItr)
			//			{
			//				ItemIdsStrForDebug.AppendNumber(*itemIdsItr);
			//				getSectionData.itemIdList.push_back(*itemIdsItr);
			//			}
			//		
			//			itr = MapForProductsPerSection.find(getSectionData.SectionId);
			//			if(itr != MapForProductsPerSection.end())
			//			{
			//				UniqueIds::iterator productIdItr;
			//				UniqueIds productIds = itr->second;
			//				for(productIdItr = productIds.begin(); productIdItr != productIds.end(); ++productIdItr)
			//					getSectionData.productIdList.push_back(*productIdItr);
			//			}
			//			
			//			ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
			//		}

			//	
			//}	
			//else if(vec_ptr->size() > 1)
			//	{
			//		//CA("else if(vec_ptr->size() > 1)");
			//		isMultipleSectionsAvailable = kTrue;
			//		
			//		multiSectionMap::iterator itr;
			//		for(int32 i = 0; i < vec_ptr->size(); ++i)
			//		{						
			//			itr = MapForItemsPerSection.find(vec_ptr->at(i).sectionId);
			//			if(itr != MapForItemsPerSection.end())
			//			{
			//				vec_ptr->at(i).itemIds = itr->second;
			//			}
			//			itr = MapForProductsPerSection.find(vec_ptr->at(i).sectionId);
			//			if(itr != MapForProductsPerSection.end())
			//			{
			//				vec_ptr->at(i).productIds = itr->second;
			//			}
			//		}
			//		ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);					
			//	}
				
		
		double prevSectionID = -1;

		int32 selectedUidlistsize=selectUIDList.size();
		//PMString sz1("size : ");
		//sz1.AppendNumber(selectedUidlistsize);
		bool16 isSectiondataPopulated = this->getSectionDataforBoxList(selectUIDList);  // Added to grab JSON data
		if(!isSectiondataPopulated )
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::doRefresh::!isSectiondataPopulated");	
			return;
		}

		//CA(sz1);

		//SectionDataCache* sectionDataCachePtr = SectionDataCache::getInstance();
		for(i=0; i<selectUIDList.Length(); i++)
		{	

			progressBar->SetPosition((selectUIDList.Length()+selectUIDList.Length()) - (selectUIDList.Length()-i) + 1);

			TagList tList;
			UIDRef boxID=selectUIDList.GetRef(i);
//==========================Code Starts for Frame Deletion================================
			tList =  itagReader->getTagsFromBox(boxID);
			if(tList.size() == 0)
			{
				continue;
			}
//			for(int32 t=0;t<tList.size();t++)
//			{
//				itemmid.clear();
//				ChildIDvec.clear();
//				vector<int32>Vec_sectionIdlist;
//				int32 PublicationID = tList[t].pbObjectId;
//				int32 parentid = tList[t].parentId;
//				int32 childId = tList[t].childId;
//				int32 languageId = tList[t].languageID;
//				int32 Parenttpyeid=tList[t].parentTypeID;
//				int32 ImageTypeid=tList[t].typeId;
//
//				Vec_sectionIdlist.clear();
//				int32 sectionId = tList[t].sectionID;
//				itemmid.push_back(parentid);
//				ChildIDvec.push_back(childId);
//				if(Isframedelete==kTrue)
//				{
//					//CA("if(Isframedelete==kTrue)");
////For Item Group 
//					//Images are deleting from document while refreshing when they are not in DB or item is deleted from DB
//				if(tList[t].whichTab==3)
//					{
//						if(tList[t].imgFlag==1)
//						{
//							//CA("imageflag :");
//							//if image is not in DB then delete that image frame
//							bool16 AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(parentid,Parenttpyeid,ImageTypeid);
//							if(AssetValuePtrObj == kFalse)
//							{
//								//CA("if image is not in DB then delete image");							
//								DeleteImageFrame(boxID);
//							}
//							else
//							{
//								//if item is not in DB then delete that image frame
//								resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//								if(resultvec!=NULL)
//								{
//									//CA("if Item Group is not in DB then delete image");
//									DeleteImageFrame(boxID);
//									//CA("not null-7");
//									delete resultvec;
//									resultvec=NULL;
//								}
//								else//if item Group is not in section then delete that Image frame 
//								{
//									bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//									if(result1==kFalse)
//									{
//										//CA("if image is not in section then delete image");
//										DeleteImageFrame(boxID);
//									}
//								}
//							}
//						}
//						//Normal Spray
//						if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							//CA("Normal Spray");
//							//if Item Group is not in DB then delete that text frame
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(itemmid);
//							if(resultvec != NULL)
//							{
//								//CA("if Item is not in DB then delete that text frame");
//								DeleteTextFrame(boxID);
//								//CA("not null-8");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else//if item Group is not in section then delete that text frame 
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//								if(result1==kFalse)
//								{
//									//CA("if item is not in section then delete that text frame ");
//									 DeleteTextFrame(boxID);
//								}
//							}
//						}
//						//This is for ItemList
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//						{
//							int32 sectionIIID=tList[t].sectionID;
//							//CA("ItemList Spray");
//							//case 1: If the Item is not in DB then delete that text frame
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								DeleteTextFrame(boxID);
//								//CA("not null-9");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else //case 2:If the parent or leading item is in DB but not in section then delete the entire list from the Section
//							{
//								resultvec = ptrIAppFramework->FindParentItemGroupInDB_getAllItemGroupItemsWithDeletedParents(ChildIDvec);
//								if(resultvec!=NULL)
//								{
//								 DeleteTextFrame(boxID);
//								// CA("not null-10");
//								 delete resultvec;
//								 resultvec=NULL;
//								}
//								else
//								{
//									resultvec = ptrIAppFramework->FindParentItemGroupInSection_getItemGroupPubItemsWithDeletedParents(childId,sectionId);
//									if(resultvec!=NULL)
//									{
//										 DeleteTextFrame(boxID);
//										// CA("not null-11");
//										 delete resultvec;
//										 resultvec=NULL;
//									}
//								}
//							}
//							//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//							//if the Leading item is in DB but not in Section then also delete that text frame from document.
//						}
//
//						//This is for  isSprayItemPerFrame
//						if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//						{
//							//CA("isSprayItemPerFrame");
//							resultvec = ptrIAppFramework->FindItemGroupInDB_getAllDeletedItemGroupByIds(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//								DeleteTextFrame(boxID);
//								//CA("not null-11");
//								delete resultvec;
//								resultvec=NULL;
//							}
//							else
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,childId);
//								if(result1==kFalse)
//								{
//									 DeleteTextFrame(boxID);
//									// CA("not null-12");
//									 delete resultvec;
//									 resultvec=NULL;
//								}
//							}
//						}
//					}
////=====================================================================================================================================
// //for item only
//					if(tList[t].whichTab==4)
//					{
//					if(tList[t].imgFlag==1)
//					{
//						//CA("imageflag :");
//						//if image is not in DB then delete that image frame
//						//CA("b4 before");
//						bool16  AssetValuePtrObj = ptrIAppFramework->checkimageinDB_getAssetByParentAndType(parentid,Parenttpyeid,ImageTypeid);
//						//CA("b4 after");
//						if(AssetValuePtrObj == kFalse)
//						{
//							//CA("if image is not in DB then delete image");							
//							DeleteImageFrame(boxID);
//						}
//						else
//						{
//							//if item is not in DB then delete that image frame
//							bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(parentid,languageId);
//							if(result==kFalse)
//							{
//								//CA("if Item is not in DB then delete image");
//								DeleteImageFrame(boxID);
//							}
//							else//if item is not in section then delete that Image frame 
//							{
//								bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//								if(result1==kFalse)
//								{
//									//CA("if image is not in section then delete image");
//									DeleteImageFrame(boxID);
//								}
//							}
//						}
//					}
//					
//					//This is for Normal Spray
//					if(tList[t].childTag==-1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						//CA("Normal Spray");
//						//if Item is not in DB then delete that text frame
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(parentid,languageId);
//						if(result==kFalse)
//						{
//							//CA("if Item is not in DB then delete that text frame");
//						  DeleteTextFrame(boxID);
//						}
//						else//if item is not in section then delete that text frame 
//						{
//							bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,parentid);
//							if(result1==kFalse)
//							{
//								//CA("if item is not in section then delete that text frame ");
//							 DeleteTextFrame(boxID);
//							}
//						}
//					}
//					//This is for ItemList
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==-1)
//					{
//						int32 sectionIIID=tList[t].sectionID;
//						//CA("ItemList Spray");
//						//case 1: If the Item is not in DB then delete that text frame
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(childId,languageId);
//						if(result==kFalse)
//						{
//						 DeleteTextFrame(boxID);
//						}
//						else //case 2:if items parent is not in DB then delete that items text frame from document
//						{
//							resultvec = ptrIAppFramework->FindParentItemInDB_findAllItemsWithDeletedParents(ChildIDvec);
//							if(resultvec!=NULL)
//							{
//							 DeleteTextFrame(boxID);
//							// CA("not null-14");
//							 delete resultvec;
//							 resultvec=NULL;
//							}
//							else//case 3:if items parent is in DB but not in section then delete that text frame 
//							{
//								resultvec = ptrIAppFramework->FindParentItemInSection_getPubItemWithDeletedParents(childId,sectionId);
//								if(resultvec!=NULL)
//								{
//									DeleteTextFrame(boxID);
//									//CA("not null-15");
//									delete resultvec;
//									resultvec=NULL;
//								}
//						
//							}
//						}
//						//here first check item is Leading item or not.if it is a leading item and it is deleted from DB,then delete that text frame from Document.
//						//if the Leading item is in DB but not in Section then also delete that text frame from document.
//					}
//					//This is for  isSprayItemPerFrame
//					if(tList[t].childTag==1 && tList[t].isSprayItemPerFrame==2)
//					{
//						//CA("isSprayItemPerFrame");
//						bool16 result = ptrIAppFramework->CheckItemInDB_getItemDetails(childId,languageId);
//						if(result==kFalse)
//						{
//						 DeleteTextFrame(boxID);
//						}
//						else
//						{
//							bool16 result1 = ptrIAppFramework->CheckItemInSection_getAllitemIdsBySectionIds(sectionId,childId);
//							if(result1==kFalse)
//							{
//							 DeleteTextFrame(boxID);
//							}
//						}
//					}
//				  }
//
//				  }
//			}
//======================Code Ends here ============================================================================
		
			//if(isMultipleSectionsAvailable)
			//{
			//	//CA("if(isMultipleSectionsAvailable)");
			//	TagList tList=itagReader->getTagsFromBox(boxID);
			//	
			//	int numTags=static_cast<int>(tList.size());
			//	if(numTags<=0)//This can be a Tagged Frame
			//	{	
			//		tList=itagReader->getFrameTags(boxID);							
			//	}	

			//	int numTags1=static_cast<int>(tList.size());
			//	if(numTags1<=0)
			//	{
			//		continue;
			//	}

			//	if(tList[0].sectionID == -1)
			//		continue;

			//	if(prevSectionID != tList[0].sectionID)
			//	{
			//		PMString str("");
			//		str.AppendNumber(tList[0].sectionID);
			//		ptrIAppFramework->LogDebug("**** going to switch Section Data **** " + str);
			//		ptrIAppFramework->switchSectionData(tList[0].sectionID);
			//		
			//		ptrIAppFramework->LogDebug("**** returned from switch Section Data **** " + str);
			//		prevSectionID = tList[0].sectionID;
			//	}
			//	//added by avinash
			//	//CA("%%%%%%%%%%%");
			//	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			//	{
			//		tList[tagIndex].tagPtr->Release();
			//	}
			//	//till here
			//}
			double sectionID = tList[0].sectionID;
			//if(prevSectionID != sectionID)
			//{
			//	for(int32 itrrr = 0;itrrr<vec_ptr->size();itrrr++)
			//	{
			//		if(sectionID == vec_ptr->at(itrrr).sectionId)
			//		{
			//			//CA("11111111111111");
			//			//sectionDataCachePtr->clearInstance(); 
			//			//ptrIAppFramework->GetSectionData_clearSectionDataCache();
			//			isMultipleSectionsAvailable = kFalse;
			//			getSectionData.SectionId = vec_ptr->at(itrrr).sectionId;
			//			getSectionData.PublicationId = vec_ptr->at(itrrr).publicationId;
			//			getSectionData.languageId = vec_ptr->at(itrrr).languageId;	

			//			PMString MapForItemsPerSectionLength("MapForItemsPerSection.Length = ");
			//			MapForItemsPerSectionLength.AppendNumber(static_cast<int32>(MapForItemsPerSection.size()));
			//			ptrIAppFramework->LogDebug(MapForItemsPerSectionLength);
			//			
			//			PMString ItemIdsStrForDebug("ItemIds = ");

			//			multiSectionMap::iterator itr;
			//			itr = MapForItemsPerSection.find(getSectionData.SectionId);
			//			if(itr != MapForItemsPerSection.end())
			//			{
			//				UniqueIds itemIds = itr->second;
			//				UniqueIds::iterator itemIdsItr;
			//				for(itemIdsItr = itemIds.begin(); itemIdsItr != itemIds.end(); ++itemIdsItr)
			//				{
			//					ItemIdsStrForDebug.AppendNumber(*itemIdsItr);
			//					getSectionData.itemIdList.push_back(*itemIdsItr);
			//				}
			//			
			//				itr = MapForProductsPerSection.find(getSectionData.SectionId);
			//				if(itr != MapForProductsPerSection.end())
			//				{
			//					UniqueIds::iterator productIdItr;
			//					UniqueIds productIds = itr->second;
			//					for(productIdItr = productIds.begin(); productIdItr != productIds.end(); ++productIdItr)
			//						getSectionData.productIdList.push_back(*productIdItr);
			//				}


			//				ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
			//			}
			//		}
			//	}
			//	prevSectionID=sectionID;
			//}
			if(isRefreshTabByAttributeCheckBoxSelected == kTrue || (GroupFlag == 2))
			{
				//CA("222222222222222222222222222222");
				//ptrIAppFramework->LogDebug("**** before Refresh  ****");
				this->refreshThisBox(boxID, imagePath);	
				//ptrIAppFramework->LogDebug("**** after Refresh  ****");
			}
			//added by avinash
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//till here

		}


		if(vec_ptr)
					delete vec_ptr;
				vec_ptr = NULL;
		}		

			
		progressBar->Abort();
		if(progressBar!= NULL)
			delete progressBar;
		progressBar=NULL;


		if(isMultipleSectionsAvailable)
		{
			//ptrIAppFramework->multipleSectionDataClearInstance();
		}
		//ptrIAppFramework->GetSectionData_clearSectionDataCache();			
		
		ptrIAppFramework->EventCache_clearCurrentSectionData();	
		ptrIAppFramework->clearAllStaticObjects();

		UIDRef newDocRef ;
		if(BookReportData::vecReportRows.size() > 0 && isDummyCall){			
			newDocRef = UIDRef::gNull;
			BookReportData::entireReport.push_back(BookReportData::vecReportRows);
			

		}
		if(isDummyCall){
//			CA("isDummyCall == kTrue");	
			for(i=0; i<selectUIDList.Length(); i++)
			{	
				UIDRef boxID=selectUIDList.GetRef(i);						
				BookReportData::fillDocumentTagList(boxID,imagePath);
			}
			BookReportData::entireBookTagList.push_back(BookReportData::documentTagList);
			
			if(BookReportData::entireBookTagList.size() > 0)
			{
				//PMString asd("BEfor BookReportData::entireBookTagList \nDocument Taglist size = ");
				//asd.AppendNumber(static_cast<int32>(BookReportData::entireBookTagList.size()));
				//CA(asd);

				BookReportData::fillVecNewReportData();
				BookReportData::documentTagList.clear();
				
				//PMString asd1("entireNewItemList size = ");
				//asd1.AppendNumber(static_cast<int32>(BookReportData::entireNewItemList.size()));
				//CA(asd1);
			}
		}
		break;
	}
}
}


void Refresh::fillDataInBox(const UIDRef& curBox, TagStruct& slugInfo, double objectId)
{
//	static int32 dummy1=0;
	//CA(__FUNCTION__);
	//int32 count[50];

//PMString a("slugInfo.tagPtr->GetTagString() = ");
//a.Append(slugInfo.tagPtr->GetTagString());
//a.Append("\nslugInfo.elementId = ");
//a.AppendNumber(slugInfo.elementId);
//a.Append("\nslugInfo.field3 = ");
//a.AppendNumber(slugInfo.field3);
//CA(a);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}
	/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrameUID is kInvalidUID");
		return;
	}
/*		/////CS3 Change
	InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrame == nil");	
		return;
	}
*/
////////////	Added by Amit
	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::graphicFrameHierarchy==nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemHierarchy==nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemTextFrame==nil");
		//CA("Its Not MultiColumn");	
		return;
	}

	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::frameItemHierarchy==nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrame==nil");
		//CA("!!!ITextFrameColumn");
		return;
	}

///////////		End
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");	
		return;
	}
	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFocusManager == nil");	
		return;
	}
	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::frameTextFocus == nil");	
		return;
	}
	int32 tStart1=0;
	int32 tEnd1=0;
	//TagReader tReader1;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::itagReader == nil");		
		return ;
	}

	TagList tList1;		
	if(refreshTableByAttribute)
        tList1=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
	else
		tList1=itagReader->getTagsFromBox_ForRefresh(curBox);
	if(!tList1.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList1=itagReader->getFrameTags(curBox);
		if(tList1.size()<=0)
		{
			//It really is not our box
			return ;			
		}
	}
		


	for(int j=0; j<tList1.size(); j++)
	{	//CA("333");
		if((slugInfo.elementId == tList1[j].elementId) && (slugInfo.whichTab==tList1[j].whichTab))	
		{	//CA("element id matches");
			if(slugInfo.childTag == 1 && tList1[j].childTag == 1){
				//CA("have childTag");
				if(slugInfo.childId == tList1[j].childId && slugInfo.field3 == tList1[j].field3 && slugInfo.field4 == tList1[j].field4){
//PMString a("slugInfo.childId = ");
//a.AppendNumber(slugInfo.childId);
//a.Append("\nslugInfo.field3 = ");
//a.AppendNumber(slugInfo.field3);
//a.Append("\nslugInfo.field4 = ");
//a.AppendNumber(slugInfo.field4);
//a.Append("\nslugInfo.elementId = ");
//a.AppendNumber(slugInfo.elementId );
//CA(a);
					//CA("Match childID");
					tStart1 = tList1[j].startIndex+1;
					tEnd1 = tList1[j].endIndex-1;
					break;
				}
			}
			else if(slugInfo.header == 1){
				//CA("haveing header");
				if(slugInfo.header == tList1[j].header){
					tStart1 = tList1[j].startIndex+1;
					tEnd1 = tList1[j].endIndex-1;
					break;
				}
			}
			else{
				//CA("Don't have childID");
				if(slugInfo.parentId == tList1[j].parentId && slugInfo.header == tList1[j].header && slugInfo.childTag == tList1[j].childTag){
					//CA("Match parent");
					tStart1 = tList1[j].startIndex+1;
					tEnd1 = tList1[j].endIndex-1;
					break;
				}
			}
			continue;
		}
	}
	//int32 tStart1=slugInfo.startIndex;
	//int32 tEnd1=slugInfo.endIndex-1;
	/*PMString ZXC("tStart1 : ");
	ZXC.AppendNumber(tStart1);
	ZXC.Append("  tEnd1 : ");
	ZXC.AppendNumber(tEnd1);*/
	//CA(ZXC);
	
	char *originalString="";
	char *changedString="";
	PMString entireStory("");
	bool16 result1 = kFalse;
	
	result1=this->GetTextstoryFromBox(textModel, tStart1, tEnd1, entireStory);
	//CA("entireStory = " + entireStory);
	changedString	= const_cast<char*>(entireStory.GetPlatformString().c_str()); //Cs4
	int32 tStart, tEnd;

	//TagReader tReader;
//CA("555");	
	if(!itagReader->GetUpdatedTag(slugInfo))
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::!itagReader->GetUpdatedTag(slugInfo)");
		return;
	}
//CA("666");
	tStart=slugInfo.startIndex+1;
	tEnd=slugInfo.endIndex-tStart;
	
	PMString textToInsert("");
	VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
	VectorHtmlTrackerValue vectorObj ;
	vectorHtmlTrackerSPTBPtr = &vectorObj;

	//elementId = 101 for Items tag when the Item Table is sprayed 
	//in tabbed text format.We can't use elementId = -1 as it is used 
	//for product number.
	//Added slugInfo.elementId != -1 For Med Custom Table By Dattatray on 17/11
	if(slugInfo.elementId != -101 && slugInfo.elementId != -103 && slugInfo.tableType != 3)//4 Sept ItemTableHeader
	{		
		//CA("calling getDataFromDB");
		isNewItem = kFalse;
		for(int32 index = 0 ; index < BookReportData::vecReportRows.size() ; index++)
		{
			if(slugInfo.childTag == 1 ){
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.childId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
			else{
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.parentId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
		}

		getDataFromDB(textToInsert, slugInfo, objectId);		
		//******
		//CA("getDataFromDB " + textToInsert);
	}
	else if(slugInfo.whichTab == 5)
	{
		getDataFromDB(textToInsert, slugInfo, objectId);
	}
	else
	{
		InterfacePtr<ITextStoryThread> textStoryThread(slugInfo.tagPtr->QueryContentTextStoryThread()/*,UseDefaultIID()*/);
		if(textStoryThread == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textStoryThread == nil");
			return;
		}
		InterfacePtr<ITextModel>textModel(textStoryThread->QueryTextModel()/*,UseDefaultIID()*/);
		if(textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");
			return;
		}
		//This code is for refresh
		UIDRef txtModelUIDRef = ::GetUIDRef(textModel);

		int32 startReplaceFrom = -1;
		int32 endReplace = -1;
		Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startReplaceFrom,&endReplace);
		startReplaceFrom = startReplaceFrom +1;
		endReplace = endReplace -1;

		//This code is for refresh
		//start 4 Sept ItemTableHeader
		PMString dispName("");

		if(slugInfo.elementId == -103)//4 Sept ItemTableHeader
		{
			//CA("3");
				do
				{
					double typeId = slugInfo.typeId;
					VectorTypeInfoPtr typeValObj = nil;
					if(slugInfo.whichTab == 3)
						typeValObj= ptrIAppFramework->StructureCache_getListTableTypes();
					else if(slugInfo.whichTab == 4)
						typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();

					if(typeValObj==nil)
					{
						ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::typeValObj == nil");					
						break;
					}

					VectorTypeInfoValue::iterator it1;

					bool16 typeIDFound = kFalse;
					for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
					{	
							if(typeId == it1->getTypeId())
							{
								typeIDFound = kTrue;
								break;
							}			
					}
					if(typeIDFound)
						dispName = it1->getName();
					if(typeValObj)
						delete typeValObj;
				}while(kFalse);

				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(iConverter)
				{
					iConverter->ChangeQutationMarkONOFFState(kFalse);			
				}

			//	WideString insertText(dispName);
				//I fooled the compiler.Instead of deleting each tag,I deleted all the
				//text range containing the tags.It will in turn delete all the tags.
//	CS3 Change				textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
				//textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
            dispName.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText1(new WideString(dispName));
				ReplaceText(textModel, startReplaceFrom, endReplace-startReplaceFrom + 1  ,insertText1);

				if(iConverter)
				{
					iConverter->ChangeQutationMarkONOFFState(kTrue);			
				}

				for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
				{
					tList1[tagIndex].tagPtr->Release();
				}

				return;
		}
		//end 4 Sept ItemTableHeader
		
		
		//WideString insertText(dispName);		
		//I fooled the compiler.Instead of deleting each tag,I deleted all the
		//text range containing the tags.It will in turn delete all the tags.
//CS3 Change		textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}
		//textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
        dispName.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(dispName));
		ReplaceText(textModel, startReplaceFrom, endReplace-startReplaceFrom + 1  ,insertText);

		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		if(slugInfo.whichTab == 3)
		{
			//CA("4");
			if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
			else
			{
				if(slugInfo.typeId != -111)
					sprayItemTableInTabbedTextForm(slugInfo,textModel);
			
				if(slugInfo.typeId == -111 ) //on 17/11 By Dattatray this typeId is HardCoded For Med Custom Table so For Med Custom Table in TabTextForm below Function get called..
					sprayCMedCustomTableScreenPrintInTabbedTextForm(slugInfo,textModel);
			}
		}
		else if(slugInfo.whichTab == 4)
		{	//CA("5");

			if((slugInfo.tableType == 4) || (slugInfo.tableType == 5) || (slugInfo.tableType == 6)/*(slugInfo.typeId == -112) || (slugInfo.typeId == -113) || (slugInfo.typeId == -114)*/)
			{
				//CA("Refreshing Kit Table in Tabbed text Fromat");
				bool16 isKitTable = kFalse;
				if(slugInfo.tableType == 4)
					sprayProductKitComponentTableScreenPrintInTabbedTextForm(slugInfo, curBox, isKitTable);
				else if(slugInfo.tableType == 6)
					sprayXrefTableInTabbedTextForm(slugInfo, curBox);
				else if(slugInfo.tableType == 5)
					sprayAccessoryTableScreenPrintInTabbedTextForm(slugInfo, curBox);

			}
			else if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
			else
				sprayItemItemTableScreenPrintInTabbedTextForm( slugInfo,textModel);
		}
		else if(slugInfo.whichTab == 5)
		{
			if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
		}

		for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
		{
			tList1[tagIndex].tagPtr->Release();
		}
		return;
		//sprayItemTableInTabbedTextForm(tStruct,textModel);		
		//end
	}
	//CA("6");

	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(!iConverter)
	{
		textToInsert=textToInsert;					
	}
	else
	{
		//textToInsert=iConverter->translateString(dispName);
		vectorHtmlTrackerSPTBPtr->clear();
		textToInsert=iConverter->translateStringNew(textToInsert, vectorHtmlTrackerSPTBPtr);
	}

	
	if(entireStory.Compare(kTrue,textToInsert) != 0)
	{
		//CA(" entireStory.Compare(kTrue,textToInsert) != 0");
//PMString a("entireStory	:	");
//a.Append(entireStory);
//a.Append("\rtextToInsert	:	");
//a.Append(textToInsert);
//CA(a);
		shouldPushInFreameVect = kTrue;
		double ItemID = -1;
		if(slugInfo.childTag == 1)
			ItemID = slugInfo.childId;
		else
			ItemID = objectId;
		//**** if Document Data and DB DAta is Different then add in to Report	
		BookReportData rowdata;
		PMString dummy(slugInfo.tagPtr->GetTagString());
		BookReportData::removeDashFromString(dummy);

		rowdata.oneRawData.fieldName      = dummy;
		rowdata.oneRawData.documentValue  = entireStory;
		rowdata.oneRawData.oneSourceValue = textToInsert;
		rowdata.oneRawData.isImage        = kFalse;
		rowdata.oneRawData.itemID = ItemID;
		
		if(isNewItem == kTrue){
			//CA("isNewItem == kTrue");
			rowdata.addEmptyRow = kTrue;	
			//isNewItem = kFalse;//18MAY09
		}
	/*PMString asd("BRFOR              Setting ISNewSection- BookReportData::currentSection    - ");
			asd.Append(BookReportData::currentSection);
			asd.Append("\n newSectionName = ");
			asd.Append(newSectionName);
			CA(asd);	*/
		if((newSectionName.Compare(kTrue,BookReportData::currentSection) !=0) )
		{		
			
			/*PMString asd("Setting ISNewSection- BookReportData::currentSection    - ");
			asd.Append(BookReportData::currentSection);
			asd.Append("\n newSectionName = ");
			asd.Append(newSectionName);
			CA(asd);*/
				if(newSectionName.Compare(kTrue,"") ==0 ){
					//CA("newSectionName.Compare(kTrue,"") ==0 ");
					rowdata.isNewSection = kFalse;//18MAY09	
					newSectionName = BookReportData::currentSection;
				}
				else{
					newSectionName = BookReportData::currentSection;
					rowdata.isNewSection = kTrue;//18MAY09
					::isnewSection = kFalse;
				}
		}
		if(isNewItem == kFalse)
		{
			//CA("isNewItem == kFalse");
			if(BookReportData::vecReportRows.size() > 0){
				BookReportData BookReportDataobj = BookReportData::vecReportRows.at(BookReportData::vecReportRows.size()-1);
				if(BookReportDataobj.oneRawData.itemID != ItemID)
				{
					//CA("going to currect insertion");
					vector<BookReportData>::iterator it;
					
					bool16 found = kFalse;
					
					for(it = BookReportData::vecReportRows.end()-1 ; it != BookReportData::vecReportRows.begin()-1 ; it-- )
					{
						BookReportData BookReportObj = (*it);
						if(BookReportObj.oneRawData.itemID == ItemID)
						{
							found = kTrue;
							break;
						}
					}
					if(found == kTrue)
					{
						//CA("found == kTrue");
						it++;
						BookReportData::vecReportRows.insert(it,rowdata);					
					}
				}
				else
					BookReportData::vecReportRows.push_back(rowdata);
			}
			else
				BookReportData::vecReportRows.push_back(rowdata);
		}
		else
			BookReportData::vecReportRows.push_back(rowdata);
//		dummy1++;
	}
	
	
	
	//********
	originalString	= const_cast<char*>(textToInsert.GetPlatformString().c_str()/*GrabCString()*/); //Cs4
//****
//PMString asd(originalString);
//CA("This is DB String..originalString = "+asd);
//****
	int32 flag=0;
	
	///////////////////////////rahul
	

	WideString* myText=new WideString(textToInsert);	
	if(!iConverter)
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::iConverter == nil");					
		return;
	}	
	iConverter->ChangeQutationMarkONOFFState(kFalse);

	WideString insertBlankString(" ");

	if(shouldRefreshFlag == kTrue && (!isDummyCall) && slugInfo.field5 != 1)//shouldRefreshFlag
	{
		//CA("BookReportData::isReportWithRefreshFlag");
		//textModel->Replace(tStart, tEnd ,&insertBlankString);
		//ErrorCode Err = textModel->Replace(tStart,1 /*tEnd*/, myText);

		boost::shared_ptr<WideString> insertText(new WideString(insertBlankString));
		ReplaceText(textModel,tStart,tEnd ,insertText);
	}
		
	iConverter->ChangeQutationMarkONOFFState(kTrue);
	this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tStart );

	int32 result2 =0;
	result2= textToInsert.Compare(kTrue, entireStory );
	

	TagList tList2;
	if(refreshTableByAttribute)
		tList2=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
	else
		tList2=itagReader->getTagsFromBox_ForRefresh(curBox);
	if(!tList2.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList2=itagReader->getFrameTags(curBox);
		if(tList2.size()<=0)//It really is not our box
		{
			return ;			
		}
	}
	for(int i=0; i<tList2.size(); i++)
	{	
		//CA("7");
		if((slugInfo.elementId == tList2[i].elementId) && (slugInfo.whichTab==tList2[i].whichTab))//slugInfo.elementId == tList[i].elementId)
		{
			tStart = tList2[i].startIndex+1;
			tEnd = tList2[i].endIndex;

			/*PMString debugStr("slugInfo.elementId = ");
			debugStr.AppendNumber(slugInfo.elementId);
			debugStr.Append(" , i = ");
			debugStr.AppendNumber(i);
			debugStr.Append(" , entireStory = ");
			debugStr.Append(entireStory);
			debugStr.Append(" , textToInsert = ");
			debugStr.Append(textToInsert);
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox *********   " + debugStr);*/

			break;
		}
	}
	PMString GreenColour ("C=75 M=5 Y=100 K=0");
	char * OriginalString= NULL;
	char * NewString= NULL;
	OriginalString = const_cast<char*>(entireStory.GetPlatformString().c_str());//Cs4
	NewString= const_cast<char*>(textToInsert.GetPlatformString().c_str()); //Cs4
	int32 k1=0;
	PMString ABC("");
	NewTextList.clear();
	int32 Q=tStart;
	int32 Flagg=0;
	for(int32 p=tStart; p<=tEnd; p++)
	{ 			
		TextVector Textvector1;
		if(NewString[k1]== ' '|| p==tEnd)
		{ 
			if(Flagg==0)
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=Textvector1.StartIndex + k1;
				Q=Textvector1.EndIndex+1;
				NewTextList.push_back(Textvector1);
				ABC.Clear();
				Flagg=1;
			}
			else
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=tStart+k1;
				Q=Textvector1.EndIndex+1;
				NewTextList.push_back(Textvector1);
				ABC.Clear();
			}
		}
		else
		{
			ABC.Append(NewString[k1]);
		}
		k1++;
	}
	Flagg=0;
	ABC.Clear();
	k1=0;
	OriginalTextList.clear();
	Q=tStart1;
	for( int32 p=tStart1; p<=tEnd1+1; p++)
	{ 
		TextVector Textvector1;
		if(OriginalString[k1]== ' ' || p==tEnd1+1)
		{ 
			if(Flagg==0)
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=Textvector1.StartIndex + k1;
				Q=Textvector1.EndIndex+1;
				OriginalTextList.push_back(Textvector1);
				ABC.Clear();
				Flagg=1;
			}
			else
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=tStart+k1;
				Q=Textvector1.EndIndex+1;
				OriginalTextList.push_back(Textvector1);
				ABC.Clear();
			}
		}
		else
		{
			ABC.Append(OriginalString[k1]);
		}
		k1++;
	}

		//-------lalit-----
	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
	{
		tList2[tagIndex].tagPtr->Release();
	}
	
	if(result2!=0 /*&& HiliteFlag==kTrue*/)
	{	
		RefreshData ColorData;
		ColorData.BoxUIDRef= curBox;
		//TagReader tReader;
		TagList tList;
		if(refreshTableByAttribute)
			tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
		else
			tList=itagReader->getTagsFromBox_ForRefresh(curBox);
		if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
		{
			tList=itagReader->getFrameTags(curBox);
			if(tList.size()<=0)
			{
				//It really is not our box
				return ;			
			}
		}
		for(int i=0; i<tList.size(); i++)
		{	
			if((slugInfo.elementId == tList1[i].elementId) && (slugInfo.whichTab==tList1[i].whichTab))//slugInfo.elementId == tList[i].elementId)
			{
				tStart = tList[i].startIndex+1;
				tEnd = tList[i].endIndex;
				break;
			}
		}
		//---------
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		
///////////////////////////////rahul
	
/*	
			
	int32 srt,en,flag2=0,k=0,a,b=0,c=0,tot1,tot2;//srt1,en1,
	char Odoubl[100][500],Cdoubl[100][500];
	srt = tStart1;
	en = tEnd1;
	b=0;
	
	for(a=0;originalString[a];a++)
	{
		if(originalString[a] == ' ' && originalString[a+1] == ' ')
			continue;
		if(originalString[a] == ' ')
		{
			Odoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Odoubl[b][c] = originalString[a];
			c++;
		}
	}
	Odoubl[b][c] = '\0';
	tot1=b;
	b=0;
	c=0;
	for(a=0;changedString[a];a++)
	{
		if(changedString[a] == ' ' && changedString[a+1] == ' ')
			continue;
		if(changedString[a] == ' ')
		{
			Cdoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Cdoubl[b][c] = changedString[a];
			c++;
		}
	}
	Cdoubl[b][c] = '\0';
	tot2=b;
	/////////////////////////////
	for(a=0;a<=tot2;a++)
	{
		PMString ab("");
		ab.Append(Cdoubl[a]);
		//CA(ab);
	}
	int32 m=0,n=0,ind=0;
	int32 Counter=0; 
	if(tot1==tot2)
	{
		for(a=0;a<=tot2;a++)
		{
			if(strcmp(Odoubl[a],Cdoubl[a])!=0)
				{
					PMString abc("");
					abc.Append(Odoubl[a]);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					ChangedTextList.push_back(Textvector1);
                    CA(abc);
				}
				
		}
	}
	else
	{
		for(a=0;a<=tot2;a++)
		{
			b=m;
			Counter = 0;
			for(;b<=tot1;b++)
			{
				
				if(strcmp(Odoubl[b],Cdoubl[a])==0)
					{
						m=b+1;
						break;
					}
				else
					{
						flag2 = 0;
						for(c=0;c<=tot2;c++)
						{
							if(strcmp(Odoubl[b],Cdoubl[c])==0)
							{
								flag2 = 1;
							}
						}
						if(flag2 == 0)
						{
							PMString abc("");
							abc.Append(Odoubl[b]);
							TextVector Textvector1;
							Textvector1.Word.Clear();
							Textvector1.Word.Append(abc);
							ChangedTextList.push_back(Textvector1);
							CA(abc);
						}
											
					}
					Counter++;

			}
			
			
		}
	}	

*/
	// Awasthi Added
		int32 tot1=static_cast<int32> (NewTextList.size());
		int32 tot2= static_cast<int32>(OriginalTextList.size());
		
		int32 m=0, flag2=0;
		if(tot1==tot2)
		{	
			for(int32 a=0; a<tot2; a++)
			{	
				if((NewTextList[a].Word.Compare(kTrue, OriginalTextList[a].Word))!=0)
					{							
						PMString abc("");
						abc.Append(NewTextList[a].Word);
						TextVector Textvector1;
						Textvector1.Word.Clear();
						Textvector1.Word.Append(abc);
						Textvector1.BoxUIDRef= curBox;
						Textvector1.StartIndex= NewTextList[a].StartIndex;
						Textvector1.EndIndex= NewTextList[a].EndIndex;
						ChangedTextList.push_back(Textvector1);		
					}					
			}
		}
		else
		{
			for(int32 a=0;a<tot2;a++)
			{
				int32 b=m;
				
				for(;b<tot1;b++)
				{					
					if((NewTextList[b].Word.Compare(kTrue, OriginalTextList[a].Word)==0))//(strcmp(Odoubl[b],Cdoubl[a])==0)
						{
							m=b+1;
							break;
						}
					else
						{
							flag2 = 0;
							for(int32 c=0;c<tot2;c++)
							{
								if(((NewTextList[b].Word.Compare(kTrue, OriginalTextList[c].Word))==0))//(strcmp(Odoubl[b],Cdoubl[c])==0)
								{
									flag2 = 1;
								}
							}
							if(flag2 == 0)
							{									
								PMString abc("");
								abc.Append(NewTextList[b].Word);
								TextVector Textvector1;
								Textvector1.Word.Clear();
								Textvector1.Word.Append(abc);
								Textvector1.BoxUIDRef= curBox;
								Textvector1.StartIndex= NewTextList[b].StartIndex;
								Textvector1.EndIndex= NewTextList[b].EndIndex;
								ChangedTextList.push_back(Textvector1);
							}												
						}
					}				
				}
			}

			if(tot1> tot2)
			{
				for (int C = tot2; C< tot1; C++)
				{
					PMString abc("");
					abc.Append(NewTextList[C].Word);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					Textvector1.BoxUIDRef= curBox;
					Textvector1.StartIndex= NewTextList[C].StartIndex;
					Textvector1.EndIndex= NewTextList[C].EndIndex;
					ChangedTextList.push_back(Textvector1);
				}
			}
			// Commented By Rahul ... no more needed now
				//for(int32 s=0; s<ChangedTextList.size();s++)
				//{	
				//	ColorData.elementID=slugInfo.elementId;
				//	ColorData.StartIndex=ChangedTextList[s].StartIndex;
				//	ColorData.EndIndex=ChangedTextList[s].EndIndex; 
				//	//this->ChangeColorOfText(textModel, ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex, GreenColour);
				//	this->SetFocusForText(curBox,ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex); 
				//	ColorDataList.push_back(ColorData);
				//}
			if(myText)
				delete myText;
	}
	
}
//By Amarjit patil
//multimap<int32 , PMString> *Result;
bool16 Refresh::getDataFromDBNewoption(PMString& textToInsert, TagStruct& slugInfo, double objectId,double elementIdd)
{
	int32 ItemSize = static_cast<int32>(NItemID.size());
	int32 ItemGSize = static_cast<int32>(NItemGroupID.size());
	//CA("getDataFromDBNewoption");
    PMString string;

	
	//=============For Iem =================
	for(int32 k=0;k<NItemID.size();k++)
    {
		//CA("NItemID");
	   if((NItemID.at(k)==objectId)&&(NAttributeID.at(k)==slugInfo.elementId))
	   {
		  string=NValue.at(k);
		  InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		  if(!iConverter)
		  {
			textToInsert=string;
			break;
		  }
			textToInsert=iConverter->translateString(string);
			PMString a("textToInsert : ");
			a.Append(textToInsert);
			//CA(a);
			break;
	   }
    }
	//CA("After Item");
	//===================Item Group =====================
	for(int32 k=0;k<NItemGroupID.size();k++)
    {
		//CA("NItemGroupID");
	   if((NItemGroupID.at(k)==objectId)&&(NAttributeId_ItemGroup.at(k)==slugInfo.elementId))
	   {
		   //CA("Inner for loop");
//-----------------------------------------For New Added fields in template builder--------------------------------------------------------
		   if((slugInfo.dataType == 4 || slugInfo.dataType == 5 ) && (slugInfo.elementId==-980 || slugInfo.elementId==-979 || slugInfo .elementId==-978 || slugInfo.elementId==-977 || slugInfo.elementId==-976 || slugInfo.elementId==-983 || slugInfo.elementId==-982 || slugInfo.elementId==-121))
		   {	
					VectorScreenTableInfoPtr tableInfo = NULL;
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
					if(!tableInfo)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
						break;
					}

					if(tableInfo->size() == 0)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo->size() == 0");	
						break;
					}

					CItemTableValue oTableValue;
					VectorScreenTableInfoValue::iterator it12;
					for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
					{	
						oTableValue = *it12;

						PMString e("getTableID :");
						e.AppendNumber(oTableValue.getTableID());
						e.Append("\n");
						e.Append("slugInfo.tableId :");
						e.AppendNumber(slugInfo.tableId);
						//CA(e);
						if(slugInfo.typeId == oTableValue.getTableTypeID())
						{					
							string = oTableValue.getName();
							PMString e("getName :");
							e.Append(string);
							//CA(e);
							break;
						}
						else if(slugInfo.typeId == -3 && slugInfo.elementId == -121 && oTableValue.getTableID()== slugInfo.tableId)
						{
							string = oTableValue.getName();
							break;
						}
						else if(slugInfo.typeId == -55) 
						{
							if(slugInfo.elementId == -983 && oTableValue.getTableID()== slugInfo.tableId )//---List Description
							{
								string = oTableValue.getDescription();	
							}
							else if(slugInfo.elementId == -982 && oTableValue.getTableID()== slugInfo.tableId)//---Stencil Name
							{
								string = oTableValue.getstencil_name();
							}
						}
						else if(slugInfo.typeId == -56 )//---"Note 1 to 5"
						{
							if(slugInfo.elementId == -980 && oTableValue.getTableID()== slugInfo.tableId )//---for Note_1 
							{
								//CA("for Note_1 ");
								string = oTableValue.getNotesList().at(0);	
							}
							else if(slugInfo.elementId == -979 && oTableValue.getTableID()== slugInfo.tableId)//---for Note_2 
							{
								string = oTableValue.getNotesList().at(1);		
							}
							else if(slugInfo.elementId == -978 && oTableValue.getTableID()== slugInfo.tableId)//---for Note_3
							{
								string = oTableValue.getNotesList().at(2);			
							}
							else if(slugInfo.elementId == -977 && oTableValue.getTableID()== slugInfo.tableId)//---for Note_4
							{
								string = oTableValue.getNotesList().at(3);	;		
							}
							else if(slugInfo.elementId == -976 && oTableValue.getTableID()== slugInfo.tableId)//---for Note_5 
							{
								string = oTableValue.getNotesList().at(4);	
							}
						}
					}
					if(tableInfo)
						delete tableInfo;
			}
		   else if((NAttributeId_ItemGroup.at(k)==-1) && (slugInfo.elementId==-1))//For ITEM_GROUP_FIELD coz this field has ElementID ==-1
		   {
			 string=ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(objectId ,slugInfo.elementId, slugInfo.languageID, slugInfo.sectionID,  kTrue);
		   }
		   else
		   {
		    string=NString.at(k);
		   }
	      
		  InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		  if(!iConverter)
		  {
			textToInsert=string;
			break;
		  }
			textToInsert=iConverter->translateString(string);
			break;
	   }
    }
  
	return kTrue;

}


bool16 Refresh::getDataFromDB(PMString& textToInsert, TagStruct& slugInfo, double objectId)
{
	//CA(__FUNCTION__);
	//CA("Refresh::getDataFromDB");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	char* tempText=NULL;

	do
	{
		PMString w("ElementID :  : ");
		w.AppendNumber(slugInfo.elementId);
		//CA(w);
		CObjectValue oVal;
		//if(slugInfo.elementId==-1 && slugInfo.typeId==-1)//NAME eg pfname/pgname/prname etc.
		//{
		//	PMString myTemp;
		//	//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		//	if(oVal.getRef_id()==-1)
		//		break;
		//	myTemp=oVal.getName();
		//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//	if(!iConverter)
		//	{
		//		textToInsert=myTemp;
		//		break;
		//	}
		//	textToInsert=iConverter->translateString(myTemp);
		//	break;
		//}

		//if(slugInfo.elementId==-2 && slugInfo.typeId==-1)//PR Number
		//{
		//	PMString myTemp;
		//	//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		//	if(oVal.getRef_id()==-1)
		//		break;
		//	myTemp=oVal.get_number();
		//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//	if(!iConverter)
		//	{
		//		textToInsert=myTemp;
		//		break;
		//	}
		//	textToInsert=iConverter->translateString(myTemp);
		//	break;
		//}
		
		if(slugInfo.whichTab==1 || slugInfo.whichTab==2 || slugInfo.whichTab==3)
		{
			//CA("slugInfo.whichTab==1 || slugInfo.whichTab==2 || slugInfo.whichTab==3");
			PMString tempBuffer("");
			if(slugInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-13") || slugInfo.header == 1)
			{
				CElementModel cElementModelObj;
				bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(slugInfo.elementId,cElementModelObj);
				if(result)
					tempBuffer = cElementModelObj.getDisplayName();
			}
			else if(slugInfo.colno == -201 || slugInfo.colno == -202 || slugInfo.colno == -203)
			{//start else if xxx
				tempBuffer = "";
					do
					{
						if(slugInfo.rowno == -1)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::slugInfo.rowno == -1");						
							break;					
						}
						else
						{//else 111 Apsiva Comment
							//VectorPossibeValueModelPtr vectorPV = ptrIAppFramework->GETProduct_getMPVAttributeValueByLanguageID(slugInfo.parentId,slugInfo.elementId,slugInfo.languageID,kTrue);
							//if(vectorPV == nil)
							//{
							//	ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::GETProduct_getMPVAttributeValueByLanguageID's vectorPV is nil");													
							//	break;
							//}
							//else
							//{//else 222
							//	vector<CPossibleValueModel> ::iterator it;
							//	bool16 found = kFalse;
							//	
							//	for(it = vectorPV->begin();it!= vectorPV->end();it++)
							//	{						
							//		if(slugInfo.rowno == it->getPv_id())
							//		{
							//			found = kTrue;
							//			break;
							//		}
							//	}
							//	if(found == kFalse)
							//	{
							//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::found == kFalse");														
							//		break;
							//	}

							//	CPossibleValueModel pvmdl = *it;
							//	if(slugInfo.colno == -201)
							//		tempBuffer = pvmdl.getName();
							//	else if(slugInfo.colno == -202)
							//		tempBuffer = pvmdl.getAbbrevation();
							//	else if(slugInfo.colno == -203)
							//		tempBuffer = pvmdl.getDescription();
							//		
							//}//end else 222
							//if(vectorPV)
							//	delete vectorPV;
						}//end else 111
						
					}while(kFalse);
			}//end else if xxx

			else if(slugInfo.dataType == 4 || slugInfo.dataType == 5 /*slugInfo.elementId == -121*/){	
				//CA("D");

                if(slugInfo.elementId == -975 || slugInfo.elementId == -974 || slugInfo.elementId == -973 || slugInfo.elementId == -972 || slugInfo.elementId == -971)
                {
                    VectorAdvanceTableScreenValuePtr advTableInfo = NULL;
                    advTableInfo =ptrIAppFramework->getHybridTableData(slugInfo.sectionID,slugInfo.parentId,slugInfo.languageID,kTrue, kFalse);
                    if(!advTableInfo )
                    {
                        
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::!advTableInfo");
                        break;;
                    }
                    if( advTableInfo->size() ==0 )
                    {
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::advTableInfo->size == 0");
                        break;;
                    }
                    
                    CAdvanceTableScreenValue oAdvanceTableScreenValue;
                    VectorAdvanceTableScreenValue::iterator it;
                    bool16 typeidFound = kFalse;
                    
                    for(it = advTableInfo->begin(); it!=advTableInfo->end(); it++)
                    {
                        oAdvanceTableScreenValue = *it;
                        
                        if(oAdvanceTableScreenValue.getTableId() == slugInfo.tableId)
                        {
                            typeidFound=kTrue;
                            break;
                        }
                    }
                    
                    if(typeidFound != kTrue)
                    {
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::typeidFound != kTrue");
                        break;
                    }
                    
                    else if(slugInfo.elementId == -975)//---for Note_1
                    {
                        //CA("for Note_1 ");
                        tempBuffer = oAdvanceTableScreenValue.getNote1();
                    }
                    else if(slugInfo.elementId == -974 )//---for Note_2
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote2();
                    }
                    else if(slugInfo.elementId == -973 )//---for Note_3
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote3();
                    }
                    else if(slugInfo.elementId == -972 )//---for Note_4
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote4();
                    }
                    else if(slugInfo.elementId == -971 )//---for Note_5
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote5();
                    }
                    if(advTableInfo)
                        delete advTableInfo;
                }
                else
                {
					VectorScreenTableInfoPtr tableInfo = NULL;
						
					tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(slugInfo.sectionID, slugInfo.parentId, slugInfo.languageID,  kTrue);
					if(!tableInfo)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");	
						break;
					}

					if(tableInfo->size() == 0)
					{
						//CA("tableInfo->size() == 0");
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo->size() == 0");	
						break;
					}
					CItemTableValue oTableValue;
					VectorScreenTableInfoValue::iterator it12;
					for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
					{	
						oTableValue = *it12;
						if((slugInfo.dataType == 4 || slugInfo.elementId == -121 ) && slugInfo.tableId== oTableValue.getTableID())
						{
							tempBuffer = oTableValue.getName();
							//CA("tempBuffer01 = " + tempBuffer);
							break;
						}
						else if(slugInfo.dataType == 5) 
						{
							if(slugInfo.elementId == -983 && slugInfo.tableId== oTableValue.getTableID())//---List Description
							{
								//CA("tagInfo.elementId == -983");
								tempBuffer = oTableValue.getDescription();	
							}
							else if(slugInfo.elementId == -982 && slugInfo.tableId== oTableValue.getTableID())//---Stencil Name
							{
								tempBuffer = oTableValue.getstencil_name();
							}
							else if(slugInfo.elementId == -980 && slugInfo.tableId== oTableValue.getTableID())//---for Note_1 
							{
								//CA("for Note_1 ");
								tempBuffer = oTableValue.getNotesList().at(0);	
							}
							else if(slugInfo.elementId == -979 && slugInfo.tableId== oTableValue.getTableID())//---for Note_2 
							{
								tempBuffer = oTableValue.getNotesList().at(1);		
							}
							else if(slugInfo.elementId == -978 && slugInfo.tableId== oTableValue.getTableID())//---for Note_3
							{
								tempBuffer = oTableValue.getNotesList().at(2);		
							}
							else if(slugInfo.elementId == -977 && slugInfo.tableId== oTableValue.getTableID())//---for Note_4
							{
								tempBuffer = oTableValue.getNotesList().at(3);		
							}
							else if(slugInfo.elementId == -976 && slugInfo.tableId== oTableValue.getTableID())//---for Note_5 
							{
								tempBuffer = oTableValue.getNotesList().at(4);	
							}
						}

						

					}
					if(tableInfo)
						delete tableInfo;
                }

			//	}
			}
			else if(slugInfo.isEventField)
			{
				//CA("E");
				/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(objectId,slugInfo.elementId,slugInfo.sectionID);
				if(vecPtr != NULL)
				{
					if(vecPtr->size()> 0)
						tempBuffer = vecPtr->at(0).getObjectValue();	
					delete vecPtr;
				}*/
				//CA("tempBuffer11 = " + tempBuffer);
			}
			/*else if(slugInfo.elementId == -401 || slugInfo.elementId == -401 || slugInfo.elementId == -401 || slugInfo.rowno == -904){
				CA("For MMY");
			}*/
			else{
				tempBuffer=ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(objectId ,slugInfo.elementId, slugInfo.languageID, slugInfo.sectionID,  kTrue);
			}
			/*if(tempBuffer == "")
				tempBuffer.Append("N/A");*/
			//CA(tempBuffer);
			// Commented here for Bold Letters issue.
			/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;
				break;
			}
			textToInsert=iConverter->translateString(tempBuffer);*/
			
			textToInsert=tempBuffer; // 
			//CA(textToInsert);
			break;
		}
		//21Jun.New addition for handling refreshing of the individual item_attribute tags.
		if(slugInfo.whichTab == 4 && slugInfo.imgFlag == 0)
		{
			PMString tempBuffer("");
			if(slugInfo.header == 1)
			{//CA("slugInfo.typeId == -2");
				//following code added by Tushar on 27/12/06
				if(slugInfo.elementId == -701 || slugInfo.elementId == -702 /*|| slugInfo.elementId == -703 || slugInfo.elementId == -704*/)
				{
					/*int32 parentTypeID = slugInfo.parentTypeID;
					
					VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
					if(TypeInfoVectorPtr != NULL)
					{
						VectorTypeInfoValue::iterator it3;
						int32 Type_id = -1;
						PMString temp = "";
						PMString name = "";
						for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
						{
							Type_id = it3->getType_id();
							if(parentTypeID == Type_id)
							{
								temp = it3->getName();
								if(slugInfo.elementId == -701)
								{
									tempBuffer = temp;
								}
								else if(slugInfo.elementId == -702)
								{
									tempBuffer = temp + " Suffix";
								}
							}
						}
					}
					if(TypeInfoVectorPtr)
						delete TypeInfoVectorPtr;*/
				}
				else if(slugInfo.elementId == -703)
				{
					tempBuffer = "$Off";
				}
				else if(slugInfo.elementId == -704)
				{
					tempBuffer = "%Off";
				}	
				else if((slugInfo.elementId == -805) )  // For 'Quantity'
				{
					//CA("Quantity Header");
					tempBuffer = "Quantity";
				}
				else if((slugInfo.elementId == -806) )  // For 'Availability'
				{
					tempBuffer = "Availability";			
				}
				else if(slugInfo.elementId == -807)
				{									
					tempBuffer = "Cross-reference Type";
				}
				else if(slugInfo.elementId == -808)
				{									
					tempBuffer = "Rating";
				}
				else if(slugInfo.elementId == -809)
				{									
					tempBuffer = "Alternate";
				}
				else if(slugInfo.elementId == -810)
				{									
					tempBuffer = "Comments";
				}
				else if(slugInfo.elementId == -811)
				{									
					tempBuffer = "";
				}
				else if((slugInfo.elementId == -812) )  // For 'Quantity'
				{
					//CA("Quantity Header");
					tempBuffer = "Quantity";
				}
				else if((slugInfo.elementId == -813) )  // For 'Required'
				{
					//CA("Required Header");
					tempBuffer = "Required";
				}
				else if((slugInfo.elementId == -814) )  // For 'Comments'
				{
					//CA("Comments Header");
					tempBuffer = "Comments";
				}
				else if(slugInfo.elementId == -401)
					tempBuffer = "Make";
				else if(slugInfo.elementId == -402)
					tempBuffer = "Model";
				else if(slugInfo.elementId == -403)
					tempBuffer = "Year";
				else
					tempBuffer=ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(slugInfo.elementId,slugInfo.languageID );
			}
            else if(slugInfo.dataType == 4 || slugInfo.dataType == 5 /*slugInfo.elementId == -121*/){
				//CA("D");
                
                if(slugInfo.elementId == -975 || slugInfo.elementId == -974 || slugInfo.elementId == -973 || slugInfo.elementId == -972 || slugInfo.elementId == -971)
                {
                    VectorAdvanceTableScreenValuePtr advTableInfo = NULL;
                    advTableInfo =ptrIAppFramework->getHybridTableData(slugInfo.sectionID,slugInfo.parentId,slugInfo.languageID,kFalse, kFalse);
                    if(!advTableInfo )
                    {
                        
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::!advTableInfo");
                        break;;
                    }
                    if( advTableInfo->size() ==0 )
                    {
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::advTableInfo->size == 0");
                        break;;
                    }
                    
                    CAdvanceTableScreenValue oAdvanceTableScreenValue;
                    VectorAdvanceTableScreenValue::iterator it;
                    bool16 typeidFound = kFalse;
                    
                    for(it = advTableInfo->begin(); it!=advTableInfo->end(); it++)
                    {
                        oAdvanceTableScreenValue = *it;
                        
                        if(oAdvanceTableScreenValue.getTableId() == slugInfo.tableId)
                        {
                            typeidFound=kTrue;
                            break;
                        }
                    }
                    
                    if(typeidFound != kTrue)
                    {
                        ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::getDataFromDB::typeidFound != kTrue");
                        break;
                    }
                    
                    else if(slugInfo.elementId == -975)//---for Note_1
                    {
                        //CA("for Note_1 ");
                        tempBuffer = oAdvanceTableScreenValue.getNote1();
                    }
                    else if(slugInfo.elementId == -974 )//---for Note_2
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote2();
                    }
                    else if(slugInfo.elementId == -973 )//---for Note_3
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote3();
                    }
                    else if(slugInfo.elementId == -972 )//---for Note_4
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote4();
                    }
                    else if(slugInfo.elementId == -971 )//---for Note_5
                    {
                        tempBuffer = oAdvanceTableScreenValue.getNote5();
                    }
                    
                    if(advTableInfo)
                        delete advTableInfo;
                }
                else
                {
                    VectorScreenTableInfoPtr tableInfo = NULL;
				
                    tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(slugInfo.parentId, slugInfo.sectionID, slugInfo.languageID);
                    if(!tableInfo)
                    {
                        ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo is NULL");
                        break;
                    }
                
                    if(tableInfo->size() == 0)
                    {
                        //CA("tableInfo->size() == 0");
                        ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDataFromDB::tableInfo->size() == 0");
                        break;
                    }
                    CItemTableValue oTableValue;
                    VectorScreenTableInfoValue::iterator it12;
                    for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
                    {
                        oTableValue = *it12;
                        if((slugInfo.dataType == 4 || slugInfo.elementId == -121) && slugInfo.tableId== oTableValue.getTableID())
                        {
                            tempBuffer = oTableValue.getName();
                            //CA("tempBuffer01 = " + tempBuffer);
                            break;
                        }
                        else if(slugInfo.dataType == 5)
                        {
                            if(slugInfo.elementId == -983 && slugInfo.tableId== oTableValue.getTableID())//---List Description
                            {
                                //CA("tagInfo.elementId == -983");
                                tempBuffer = oTableValue.getDescription();
                            }
                            else if(slugInfo.elementId == -982 && slugInfo.tableId== oTableValue.getTableID())//---Stencil Name
                            {
                                tempBuffer = oTableValue.getstencil_name();
                            }
                            else if(slugInfo.elementId == -980 && slugInfo.tableId== oTableValue.getTableID())//---for Note_1
                            {
                                //CA("for Note_1 ");
                                tempBuffer = oTableValue.getNotesList().at(0);
                            }
                            else if(slugInfo.elementId == -979 && slugInfo.tableId== oTableValue.getTableID())//---for Note_2
                            {
                                tempBuffer = oTableValue.getNotesList().at(1);
                            }
                            else if(slugInfo.elementId == -978 && slugInfo.tableId== oTableValue.getTableID())//---for Note_3
                            {
                                tempBuffer = oTableValue.getNotesList().at(2);
                            }
                            else if(slugInfo.elementId == -977 && slugInfo.tableId== oTableValue.getTableID())//---for Note_4
                            {
                                tempBuffer = oTableValue.getNotesList().at(3);
                            }
                            else if(slugInfo.elementId == -976 && slugInfo.tableId== oTableValue.getTableID())//---for Note_5
                            {
                                tempBuffer = oTableValue.getNotesList().at(4);
                            }
                        }
                        
                        
                        
                    }
                    if(tableInfo)
                    delete tableInfo;
                }
                
			}
			else
			{//CA("slugInfo.typeId != -2");
				double elementId = slugInfo.elementId;
				
				if(elementId == -701 || elementId == -702 || elementId == -703 || elementId == -704)
				{
					PublicationNode pNode;
					pNode.setPubId(slugInfo.parentId);
					pNode.setPBObjectID(slugInfo.rowno);

					if(slugInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-1001"))
					{
						//CA("item");
						pNode.setIsProduct(0);
					}
					else if(slugInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-12"))
					{
						//CA("product");
						pNode.setIsProduct(1);
					}
					if(slugInfo.rowno != -1)
					{
						//CA("sETTING IsONEsource flag as false");
						pNode.setIsONEsource(kFalse);
					}
					handleSprayingOfEventPriceRelatedAdditions(pNode,slugInfo,tempBuffer);
				}
				else if(slugInfo.tagPtr->GetAttributeValue(WideString("rowno")) == WideString("-NCitem")) // Non Catalog Items
				{
					//tempBuffer=ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(slugInfo.typeId,slugInfo.elementId, slugInfo.languageID);
				}
				else if(slugInfo.elementId == -401 || slugInfo.elementId == -402 || slugInfo.elementId == -403){
					//CA("For MMY");
					if(slugInfo.childTag && slugInfo.field5 != 1){
						//Apsiva comment
						/*if(slugInfo.elementId == -401)
							tempBuffer = ptrIAppFramework->MakeModelCache_getMakeValueByMakeID(slugInfo.field3);					
						else if(slugInfo.elementId == -402)
							tempBuffer = ptrIAppFramework->MakeModelCache_getModelValueByModelID(slugInfo.field4);	
						else if(slugInfo.elementId == -403)
							tempBuffer = ptrIAppFramework->GetMakeModel_getYearByItemIdModelId(slugInfo.childId,slugInfo.field4);*/
					}				
					//CA("tempBuffer = "+tempBuffer);
				}
				else if(slugInfo.childTag == 1)
				{
					if(slugInfo.isEventField == 1)
					{
						//CA("tagInfo.isEventField11");
						/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(slugInfo.childId,slugInfo.elementId,slugInfo.sectionID);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								tempBuffer = vecPtr->at(0).getObjectValue();
							delete vecPtr;
						}*/

						//CA("itemAttributeValue11 = " + itemAttributeValue);
					}	
					else if(slugInfo.elementId == -803 || slugInfo.elementId == -827)
					{
						tempBuffer = (PMString)slugInfo.tagPtr->GetAttributeValue(WideString("rowno"));
					}
					else{	
						tempBuffer=ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(slugInfo.childId,slugInfo.elementId, slugInfo.languageID, slugInfo.sectionID, /*kTrue*/kFalse );//23OCT09//Amit
					}
				}
				else{
					if(slugInfo.isEventField == 1)
					{
						//CA("tagInfo.isEventField22");
						/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(slugInfo.parentId,slugInfo.elementId,slugInfo.sectionID);
						if(vecPtr != NULL)
						{
							if(vecPtr->size()> 0)
								tempBuffer = vecPtr->at(0).getObjectValue();	
							delete vecPtr;
						}*/
						//CA("itemAttributeValue22 = " + itemAttributeValue);
					}
					else{
						tempBuffer=ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(slugInfo.parentId,slugInfo.elementId, slugInfo.languageID, slugInfo.sectionID, /* kTrue*/kFalse );//23OCT09//Amit
					}
				}
			}
            
			/*if(tempBuffer == "")
				tempBuffer.Append("N/A");*/
			//CA(tempBuffer);
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;
				break;
			}
			textToInsert=iConverter->translateString(tempBuffer);
			//CA(textToInsert);
			break;
		}

		
		if(slugInfo.whichTab == 5 && slugInfo.imgFlag == 0)
		{ 
			//CA("slugInfo.whichTab == 5");
			PMString tempBuffer("");

			//int32 pubID = slugInfo.parentId;
			double attributeID = slugInfo.elementId;
			double languageID = slugInfo.languageID;
			double TagtypeId = slugInfo.typeId;
			//started on 10Oct..
			
//				if(pNode.getIsONEsource())					//commented on 11/12/06 by Tushar
//				{											
//					CA("pNode.getIsONEsource");
					/*PMString data;
					data.Append("pNode.getPubId() :");
					data.AppendNumber(pNode.getPubId());
					data.Append("\n");
					data.Append("slugInfo.colno :");
					data.AppendNumber(slugInfo.colno);
					data.Append("\n");
					data.Append("attributeID :");
					data.AppendNumber(attributeID);
					data.Append("\n");
					data.Append("languageID :");
					data.AppendNumber(languageID);
					data.Append("\n");
					data.Append("pNode.getIsProduct() :");
					data.AppendNumber(pNode.getIsProduct() );
					CA(data);*/

					//This method is added on 10/10  to get className depend upon (selected level) stencils when Category is selected in stencilBuilder
					//Example:For Category Name-Level1 :: Master
					//For Category Name-Level2 :: COLORADO2 SPINAL SYSTEM... etc
					
					if(slugInfo.catLevel > 0)//if(slugInfo.catLevel != -1)
					{
						//Apsiva Comment
						//double ProductTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
						//int32 ItemTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("ITEM_LEVEL");
										
						//if(slugInfo.parentTypeID == ProductTypeId)
						//{
      //       				tempBuffer = ptrIAppFramework->getCategoryCopyAttributeValue(slugInfo.parentId,slugInfo.catLevel/*slugInfo.colno*/, attributeID,languageID, 1 );
						//}
						//else
						//{
						//	//CA("!! slugInfo.typeId == ProductTypeId");
						//	tempBuffer = ptrIAppFramework->getCategoryCopyAttributeValue(slugInfo.parentId, slugInfo.catLevel/*slugInfo.colno*/, attributeID,languageID, 0 );
						//}
					}
					else
					{
						//CA("Else Part slugInfo.colno != -1 ");
						if(attributeID < 0) // for static attribute Spray
						{
							//CA("attributeID < 0");
							//int32 result = ptrIAppFramework->StaticAttributeIDs_CheckPublicationID(attributeID); // for getting whether it is PR,SEC,SUBSEC
							double InputID = -1;
							//
							//if(result == 1)
							//	InputID = CurrentPublicationID;
							//else if(result == 2)
							//	InputID = CurrentSectionID;
							//else if(result == 3
							//	InputID = CurrentSubSectionID;

							InputID =	 ptrIAppFramework->getSectionIdByLevelAndEventId(slugInfo.catLevel ,slugInfo.sectionID, slugInfo.languageID);

			//				if(slugInfo.catLevel == -1)
			//				{
			//					CPubModel CPubObjectModel = ptrIAppFramework->getpubModelByPubID(slugInfo.sectionID,slugInfo.languageID);
			//					InputID = CPubObjectModel.getRootID();
			//				}							
			//				else
			//				{			
			//				//Apsiva Comment
			//					/*VectorPubModelPtr pVectorPubModel = ptrIAppFramework->ProjectCache_getParentsToRootPubIdListForIndesign(slugInfo.sectionID);
			//					if(pVectorPubModel == NULL)
			//					{
			//						break;
			//					}

			//					if(pVectorPubModel->size() <= 0)
			//						break;

			//					int32 sectionLevel = slugInfo.catLevel - (slugInfo.catLevel * 2);
			//					
			//					if(sectionLevel < pVectorPubModel->size())
			//						InputID = pVectorPubModel->at(sectionLevel).getPublicationID();
			//					else if(sectionLevel == pVectorPubModel->size())
			//						InputID = slugInfo.sectionID;
			//					else
			//						InputID = -1;
			//
			//					delete pVectorPubModel;		*/	

			//					InputID =  slugInfo.sectionID;
			//				}
							
							if(InputID == -1)
								break;
								
							tempBuffer = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(InputID,attributeID,languageID,kTrue);
						
						/*if(tempBuffer == "")
							tempBuffer.Append("N/A");*/
						}
						else  // for spraying Publication, Section & Subsection level copy attributes.
						{
							//Apsiva Comment
							//int32 PubtypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_PUBLICATION_TYPE");
							//int32 SecTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_SECTION_TYPE");
							//int32 SubSecTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_SUBSECTION_TYPE");
							
							double InputID = -1;
							//

							/*if(slugInfo.catLevel == -1)
							{
								CPubModel CPubObjectModel = ptrIAppFramework->getpubModelByPubID(slugInfo.sectionID,slugInfo.languageID);
								InputID = CPubObjectModel.getRootID();
							}
							else
								InputID = slugInfo.sectionID;*/
							//else if(TagtypeId == SecTypeId)
							//	InputID = slugInfo.sectionID;
							//else if(TagtypeId == SubSecTypeId)
							//	InputID = slugInfo.sectionID;
							////CA_NUM(" InputID : " , InputID);

							InputID =	 ptrIAppFramework->getSectionIdByLevelAndEventId(slugInfo.catLevel ,slugInfo.sectionID, slugInfo.languageID);

							tempBuffer = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(InputID,attributeID,languageID,kTrue);
							//CA(tempBuffer);
						}
					}


			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;
				break;
			}

			textToInsert=iConverter->translateString(tempBuffer);

			
	
			
			break;
		} // End Project

		//end 21Jun.
			
		//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);		
		//if(oVal.getRef_id()==-1)// This object may have been deleted
		//	break;	
		//tempText;//temp_to_test =ptrIAppFramework->PRODMngr_getProductColData(oVal.getRef_id(), slugInfo.elementId, slugInfo.sectionID);
		//textToInsert.Append(tempText);

		/*if(tempText)
			delete []tempText;*/
	}while(0);   
	return kTrue;
}

void Refresh::fillImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId, PMString imagePath, bool16 isInterfacecall)
{
	///CA(__FUNCTION__);
	//CA("inside Refresh::fillImageInBox");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	
	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- fill image in box 2 no image path  ----");
		return;
	}

	PMString imagePathWithSubdir = imagePath;
	PMString fileName("");
	double assetID;
	VectorAssetValuePtr AssetValuePtrObj = nil;

	//*********
	PMString oldImagePath  = slugInfo.tagPtr->GetAttributeValue(WideString("href"));
	oldImagePath = FileUtils::FileURLToPMString(oldImagePath);
//	PMString oldImagePath = "C:\\Documents and Settings\\Amit\\My Documents\\My Pictures\\AP_BRAND\\180.jpg";
//CA("oldImagePath	: "+oldImagePath);
	//*********



	if(slugInfo.whichTab!=4)
	{
		//temp_to_test 	fileName=ptrIAppFramework->getObjectAssetName(objectId, slugInfo.parentTypeID, slugInfo.typeId);
		
		//Modified on 22 Aug. By Yogesh..we have to take into account the MPVID also
		if(slugInfo.rowno == 0 || slugInfo.rowno == -1)
		{
			//CA("slugInfo.rowno == 0 || slugInfo.rowno == -1");
			//PMString typd("typd : ");
			//typd.AppendNumber(slugInfo.typeId);
			//CA(typd);//typeid is -98
			//typd.Clear();
			//typd.AppendNumber(objectId);
			//CA("objectId : "+typd);//object id means parent id=100211
			//typd.Clear();
			//typd.AppendNumber(slugInfo.parentTypeID);
			//CA("slugInfo.parentTypeID" +typd);//parent type id 73
			////upto here-------------------added by nitin
			int32 isProduct = 0;	
			if(slugInfo.whichTab == 3)
				isProduct =1;
			else if(slugInfo.whichTab == 4)
				isProduct = 0;
			else if (slugInfo.whichTab == 5)
				isProduct = 5;
				
			if(slugInfo.typeId == -98)
			{
				AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,slugInfo.sectionID, isProduct, -1);
			}
			else
			{

				if(slugInfo.typeId == -207 || slugInfo.typeId == -208 || slugInfo.typeId == -209|| slugInfo.typeId == -210 
				|| slugInfo.typeId == -211 || slugInfo.typeId == -212 || slugInfo.typeId == -213 || slugInfo.typeId == -214
				|| slugInfo.typeId == -215 || slugInfo.typeId == -216 || slugInfo.typeId == -217 || slugInfo.typeId == -218
				|| slugInfo.typeId == -219 || slugInfo.typeId == -220 || slugInfo.typeId == -221 || slugInfo.typeId == -222 || slugInfo.typeId == -223)
				{
					
					//CA("Yes In My Coundition ....");
					this->fillBMSImageInBox(boxUIDRef,slugInfo,objectId);
					return;
				}
				else
				{
					AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,slugInfo.sectionID, isProduct,slugInfo.typeId);
				}
			}
		}
		else
		{
			//CA("else");
		//Apsiva Comment
			if(slugInfo.whichTab == 3)
				AssetValuePtrObj =	ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(objectId,slugInfo.elementId,slugInfo.sectionID, slugInfo.languageID,1, slugInfo.typeId, slugInfo.imageIndex);
			//else if(slugInfo.whichTab == 5 && slugInfo.catLevel > 0 )
			//	AssetValuePtrObj =	ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(objectId,slugInfo.elementId,slugInfo.sectionI ,slugInfo.languageID,2, slugInfo.typeId, slugInfo.imageIndex);
			else if(slugInfo.whichTab == 5 )
				AssetValuePtrObj =	ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(objectId,slugInfo.elementId, slugInfo.sectionID, slugInfo.languageID,3, slugInfo.typeId, slugInfo.imageIndex);
		}
		//end modification done on 22 Aug by Yogesh.
		if(AssetValuePtrObj == NULL)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- GETAssets_GetAssetByParentAndType -- returned null vector--");
			return ;
		}

		if(AssetValuePtrObj->size() ==0)
		{
			return ;
		}

	}
	else
	{
		//CA("slugInfo.whichTab == 4");
		//Modified on 22 Aug. By Yogesh..we have to take into account the MPVID also
		if(slugInfo.elementId == -1 &&  slugInfo.typeId != -1 )
		{
			//CA("lugInfo.rowno == 0 || slugInfo.rowno == -1");

			int32 isProduct = 0;	
			if(slugInfo.whichTab == 3)
				isProduct =1;
			else if(slugInfo.whichTab == 4)
				isProduct = 0;
			else if(slugInfo.whichTab == 5)
				isProduct = 5;

			if( slugInfo.typeId == -99 )
			{
				AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,slugInfo.sectionID, isProduct, -1);
			}
			else{
				if(slugInfo.typeId == -207 || slugInfo.typeId == -208 || slugInfo.typeId == -209|| slugInfo.typeId == -210 
				|| slugInfo.typeId == -211 || slugInfo.typeId == -212 || slugInfo.typeId == -213 || slugInfo.typeId == -214
				|| slugInfo.typeId == -215 || slugInfo.typeId == -216 || slugInfo.typeId == -217 || slugInfo.typeId == -218
				|| slugInfo.typeId == -219 || slugInfo.typeId == -220 || slugInfo.typeId == -221 || slugInfo.typeId == -222 || slugInfo.typeId == -223){
					
					//CA("Yes In My Coundition ....");
					this->fillBMSImageInBox(boxUIDRef,slugInfo,objectId);
					return;
				}
				else{
                    
					AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,slugInfo.sectionID, isProduct,slugInfo.typeId);
				}
			}
		}
		else
		{
			if(slugInfo.typeId != -1)
            {
                AssetValuePtrObj =	ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(objectId,slugInfo.elementId, slugInfo.sectionID, slugInfo.languageID,0, slugInfo.typeId, slugInfo.imageIndex);
            }
            else if((slugInfo.typeId == -1 && slugInfo.elementId != -1)) //Item Asset Attribute
            {
                PMString assetIdStr("");
                assetIdStr = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(slugInfo.parentId,slugInfo.elementId , slugInfo.languageID, slugInfo.sectionID, kFalse );
                
                if(assetIdStr == "")
                {
                    ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- assetIdStr is null");
                    return ;
                }
                
                double assetId = assetIdStr.GetAsDouble();
                int32 isProduct =0;// for Item images
                AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndAssetId(slugInfo.parentId, slugInfo.sectionID, isProduct, assetId);
            }
		}
		//end modification done on 22 Aug by Yogesh.
		if(AssetValuePtrObj == NULL)
		{
			//CA("AssetValuePtrObj == NULL");
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- GETAssets_GetAssetByParentAndType -- returned null vector--");
			return ;
		}
		if(AssetValuePtrObj->size() ==0)
		{
			//CA("AssetValuePtrObj->size() ==0");
			delete AssetValuePtrObj;
			ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- GETAssets_GetAssetByParentAndType -- AssetValuePtrObj->size() ==0");
			return;
		}

	}
//PMString d("AssetValuePtrObj->size() = ");
//d.AppendNumber(static_cast<int32>(AssetValuePtrObj->size()));
//CA(d);
	VectorAssetValue::iterator it; // iterator of Asset value	
	bool16 SuccessfullSprayFlag = kFalse;
	for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
	{
		//CA("iterator");
		imagePathWithSubdir = imagePath;

		CAssetValue objCAssetvalue = *it;
		fileName = objCAssetvalue.geturl();
        //CAlert::InformationAlert(fileName);
		assetID = objCAssetvalue.getAsset_id();	

		if((slugInfo.elementId != -1) && (slugInfo.rowno > 0))
		{
			//CA("iterating");
			//Apsiva Comment
			double mpv_value_id = objCAssetvalue.getMpv_value_id();
			if(mpv_value_id != slugInfo.rowno)
				continue;
		}
		else{
			if(slugInfo.typeId != -99)
				if(slugInfo.typeId != objCAssetvalue.getType_id()){
					/*PMString a("slugInfo.typeId = ");
					a.AppendNumber(slugInfo.typeId);
					a.Append("\nobjCAssetvalue.getType_id() = ");
					a.AppendNumber(objCAssetvalue.getType_id());
					CA(a);*/
					
					//continue;
				}
		}
		do
		{
			if(fileName=="")
			{
				//CA("fileName is blank");
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::-- file name blank , hence returning --");
				break;
			}
			do
			{
				SDKUtilities::Replace(fileName,"%20"," ");
			}while(fileName.IndexOfString("%20") != -1);
            
            //CAlert::InformationAlert("fileName = "+fileName);

			if(!fileExists(imagePathWithSubdir,fileName))
			{
				//CA("File not exist");
				if(refreshModeSelected == 1){
					//CA("refreshModeSelected == 1");
					if(shouldRefreshFlag == kTrue){
						//CA("shouldRefreshFlag == kTrue");
						//this->deleteThisBox(boxUIDRef);
						break;
					}
					//CA("refreshModeSelected == 1 && fileExists");
					shouldPushInFreameVect = kTrue;
				}
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillImageInBox::!fileExists " + imagePathWithSubdir + fileName);	
				//break; //------
			}
//CA("fillImageInBox 7");			
			shouldPushInFreameVect = kTrue;
			PMString total=imagePathWithSubdir+fileName;
            
            #ifdef MACINTOSH
                SDKUtilities::convertToMacPath(total);
            #endif
//PMString a("oldImagePath	=	");
//a.Append(oldImagePath);
//a.Append("\ntotal	=	");
//a.Append(total);
//CA(a);
			if(refreshModeSelected == 1 && shouldRefreshFlag == kFalse)
			{
				//CA("Break;");
				shouldPushInFreameVect = kFalse;
				break;
			}
			if(oldImagePath.Compare(kFalse,total) != 0) // if Image Item is Changed
			{	
				//CA("oldImagePath != total");
				
				if(!(oldImagePath.Contains(fileName) && total.Contains(fileName)))//-----
				{
					productImagesReLinked.push_back(objectId);
				}
	
				
				if(refreshModeSelected != 1)
					shouldPushInFreameVect = kTrue;  //flag is for InteractiveRefresh

				PMString dummy(slugInfo.tagPtr->GetTagString());
				BookReportData::removeDashFromString(dummy);
				BookReportData rowdata;
				rowdata.oneRawData.fieldName      =  dummy/*slugInfo.tagPtr->GetTagString()*/;
				rowdata.oneRawData.documentValue  = oldImagePath;
				rowdata.oneRawData.oneSourceValue = total;
				rowdata.oneRawData.isImage        = kTrue;
				rowdata.oneRawData.itemID = objectId;
				if(isNewItem == kTrue){
					//CA("isNewItem == kTrue ... for Image");
					rowdata.addEmptyRow = kTrue;	
					isNewItem = kFalse;
				}				
				if(isNewItem == kFalse)
				{
					//CA("isNewItem == kFalse ... for Image");
					if(BookReportData::vecReportRows.size() > 0){
						BookReportData BookReportDataobj = BookReportData::vecReportRows.at(BookReportData::vecReportRows.size()-1);
						if(BookReportDataobj.oneRawData.itemID != objectId)
						{
							//CA("going to currect insertion");
							vector<BookReportData>::iterator it;
							bool16 found = kFalse;
							
							for(it = BookReportData::vecReportRows.end()-1 ; it != BookReportData::vecReportRows.begin()-1 ; it-- )
							{
								BookReportData BookReportObj = (*it);
								if(BookReportObj.oneRawData.itemID == objectId)
								{
									found = kTrue;
									break;
								}
							}
							if(found == kTrue)
							{
								//CA("found == kTrue");
								it++;
								BookReportData::vecReportRows.insert(it,rowdata);					
							}
						}
						else{
							//CA("inserted image");
							BookReportData::vecReportRows.push_back(rowdata);
						}

					}
					else
					{
						BookReportData::vecReportRows.push_back(rowdata);
						//CA("insetted for image in 1st row");
					}
				}
				else
					BookReportData::vecReportRows.push_back(rowdata);

				bool isImageChanged = Utils<ILinkUtils>()->IsLinkMissingOrOutOfDate(boxUIDRef);
				if(isImageChanged){
					//CA("IsLinkMissingOrOutOfDate == kTrue");
					shouldPushInFreameVect = kTrue;
				}
				else{
					//CA("IsLinkMissingOrOutOfDate == kFalse");
					shouldPushInFreameVect = kFalse;
				}
			}
			else{
				//CA("oldImagePath.Compare(kFalse,total) =====SAME= 0");
				if(Mediator::isSilentModeSelected == kFalse){
					bool isImageChanged = Utils<ILinkUtils>()->IsLinkMissingOrOutOfDate(boxUIDRef);
					if(isImageChanged){
						//CA("IsLinkMissingOrOutOfDate == kTrue");
						shouldPushInFreameVect = kTrue;
					}
					else{						
						shouldPushInFreameVect = kFalse;
						InterfacePtr<ILinkState> linkState(boxUIDRef, IID_ILINKSTATE);										
						bool16 isWanted =linkState->IsLinkWanted();
						if(isWanted){
							//CA("isWanted..T");
							shouldPushInFreameVect = kTrue;
						}
						else{
							//CA("False..isWanted");
							shouldPushInFreameVect = kFalse;
						}
						if(linkState->HasChanged())
							shouldPushInFreameVect = kTrue;
						else
							shouldPushInFreameVect = kFalse;
					}					
				}
				/////////
				if(shouldRefreshFlag == kFalse  || isDummyCall){
					//CA("ttt");
					//shouldPushInFreameVect = kFalse;
					SuccessfullSprayFlag = kTrue;
					break;
				}
			}
			//*************			
			if(shouldRefreshFlag == kTrue  && !isDummyCall)
			{
				//CA("Import path = "+total);
				CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
				if(!ImportFileInFrame(boxUIDRef,total)){
					//CAlert::InformationAlert("Unable to place picture in the box");
				}
				else
				{
					//CA(" Image Imported");
					SuccessfullSprayFlag = kTrue;
					//fitImageInBox(boxUIDRef, isInterfacecall);
				}
			}
			//else
				//CA("shouldRefreshFlag == kFalse  && isDummyCall == kFalse");
		}while(0);
		if(SuccessfullSprayFlag == kTrue)
			break;
	}

	if(AssetValuePtrObj)
		delete AssetValuePtrObj;
}



bool16 Refresh::ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath)
{
	//CA(__FUNCTION__);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::ImportFileInFrame::!fileExists");	
		return kFalse;
	}
	
	IDocument *docPtr=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db = ::GetDataBase(docPtr);
	if (db == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileFrame::db is nil");	
		return kFalse;
	}

	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));
	
	if(  FileUtils::GetFileSize (sysFile) <= 0 )
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::Got a Zero size file , aborting Import");		
		return kFalse;
	}

	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::!importCmd");	
		return kFalse;
	}
		
	//*****Depricated cs4
	//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
	//if(!importFileCmdData)
	//{
	//	ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::!importFileCmdData");	
	//	return kFalse;
	//}
	//
	//importFileCmdData->Set(db, sysFile, kMinimalUI);

	//*****New Added For Cs4
	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil) {
		//CA(" importFileCmdData == nil ");
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);

	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::err != kSuccess");	
		return kFalse;
	}

	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame:::!placeGun");
		return kFalse;
	}
	
//	UIDRef placedItem(db, placeGun->GetItemUID());	//`	CS3	Change
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::replaceCmd is nil");	
		return kFalse;
	}

	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::iRepData is nil");	
		return kFalse;
	}
	
	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);

	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ImportFileInFrame::status==kFailure");		
		return kFalse;
	}
	return kTrue;
}



int32 Refresh::getAllItemIDs(double parentID, double pubID, vector<double>& itemIDList)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	//temp_to_test if(!ptrIAppFramework->PUBCntrller_getChildrensByObjectId(parentID, pubID, itemIDList))
		return 0;
	return (int32)itemIDList.size();
}


int32 Refresh::parseTheText(PMString& rowString, vector< vector<PMString> >& dataPtr)
{
	//CA(__FUNCTION__);
	int maxCount=0;
	int tempCount=0;

	dataPtr.clear();
	vector<PMString> stringList;

	stringList.clear();
	tempCount=0;
	PMString hayStack=rowString;

	PMString tempString;
	for(int j=0; j<hayStack.NumUTF16TextChars(); j++)
	{
		if(hayStack[j]=='\t')
		{
			stringList.push_back(tempString);
			tempString.Clear();
			tempCount++;
		}
		else if(hayStack[j]=='\r')
		{
			if(tempString.NumUTF16TextChars())
			{
				stringList.push_back(tempString);
				tempString.Clear();
				tempCount++;
			}
			dataPtr.push_back(stringList);
			if(tempCount>maxCount)
				maxCount=tempCount;
			tempString.Clear();
			stringList.clear();
			tempCount=0;
		}
		else
			tempString.Append(hayStack[j]);
	}

	if(tempString.NumUTF16TextChars())
	{
		stringList.push_back(tempString);
		tempString.Clear();
		tempCount++;
	}

	if(tempCount>maxCount)
		maxCount=tempCount;
	dataPtr.push_back(stringList);

	if(maxCount==0)
		maxCount=1;

	return maxCount;
}


void Refresh::fitImageInBox(const UIDRef& boxUIDRef, bool16 isInterFaceCall)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do 
	{
		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
		{
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::Refresh::fitImageInBox::!iHier ");
			return;
		}

		if(iHier->GetChildCount()==0)
		{
			//There may not be any image at all????
			return;
		}

		UID childUID=iHier->GetChildUID(0);
		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
		if(isInterFaceCall == kFalse)
		{
			InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
			if(!iAlignCmd)
			{
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::Refresh::fitImageInBox::!iAlignCmd ");
				return;
			}
			
		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
		CmdUtils::ProcessCommand(iAlignCmd);
		}
		else
		{
			InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentToFrameCmdBoss));
			if(!iAlignCmd)
			{
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::Refresh::fitImageInBox::!iAlignCmd ");
				return;
			}

			iAlignCmd->SetItemList(UIDList(newChildUIDRef));
			CmdUtils::ProcessCommand(iAlignCmd);
		}

		//CA(" in Refresh::fitImageInBox -- at last table ");
	} while (false);
}

bool16 Refresh::GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	//CA(__FUNCTION__);
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		const textchar characterCode = (*iter).GetValue();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void Refresh::ChangeColorOfText(InterfacePtr<ITextModel> textModel1 ,int32 StartText1, int32 EndText1, PMString  altColorSwatch)
{
	//CA(__FUNCTION__);
	do
	{
		/*UIDRef	BoxRefUID1 = rDataList[c].BoxUIDRef; 
		int32 LObjectID1 = rDataList[c].objectID;
		int32 LElementID1 = rDataList[c].elementID;
		int32 StartText1 = rDataList[c].StartIndex;
		int32 EndText1 = rDataList[c].EndIndex;
		
		InterfacePtr<IPMUnknown> unknown(BoxRefUID1, IID_IUNKNOWN);
		UID textFrameUID1 = FrameUtils::GetTextFrameUID(unknown);
		if (textFrameUID1 == kInvalidUID)
			break;

		InterfacePtr<ITextFrame> textFrame1(BoxRefUID1.GetDataBase(), textFrameUID1, ITextFrame::kDefaultIID);
		if (textFrame1 == nil)
			break;


		TextIndex startIndex2 = textFrame1->TextStart();
		
		InterfacePtr<ITextModel> textModel1(textFrame1->QueryTextModel());
		if (textModel1 == nil)
			break;
		*/
		
		InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		InterfacePtr<ISwatchList> swatchList(workSpace,UseDefaultIID());
		if (swatchList == nil)
		{	//CA("swatchList == nil");
			ASSERT_FAIL("Unable to get swatch list from workspace!");
			break;
		}
		
		InterfacePtr<ITextAttrUID>
		textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
		UID colorUID;
		UID altColorUID = kInvalidUID;
		
		UIDRef altColorRef = swatchList->FindSwatch (altColorSwatch);
		if (altColorRef.GetUID() == kInvalidUID)
		{
			//ASSERT_FAIL("Unable to get alternate swatch in DeleteColor!");
			break;
		}
		else
		{
			altColorUID = altColorRef.GetUID();
		}
				
		textAttrUID->Set(altColorUID);
		int32 charCount = EndText1 -StartText1;
		
		InterfacePtr<ICommand> applyCmd(
						Utils<ITextAttrUtils>()->
						BuildApplyTextAttrCmd(textModel1,
						StartText1,
						charCount,
						textAttrUID,
						kCharAttrStrandBoss) );
		ErrorCode err = CmdUtils::ProcessCommand(applyCmd);
		
	}while(0);
}

bool16 Refresh::doesExist(IIDXMLElement * ptr, UIDRef BoxRef)
{
	//CA(__FUNCTION__);
	//TagReader tReader;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doesExist::!itagReader");
		return kFalse;
	}
	IIDXMLElement *xmlPtr;
	TagList tList1;
	if(refreshTableByAttribute)
		tList1=itagReader->getTagsFromBox_ForRefresh_ByAttribute(BoxRef, &xmlPtr);
	else
		tList1=itagReader->getTagsFromBox_ForRefresh(BoxRef, &xmlPtr);
	
		if(ptr==xmlPtr)
		{ 
			for(int32 j=0; j<tList1.size(); j++)
			{
				//CA("Inside check for Parent id");
				if(tList1[j].parentId==-1)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::doesExist::tList1[j].parentId==-1");	
					for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
					{
						tList1[tagIndex].tagPtr->Release();
					}
					return kFalse;
				}
			}

			for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
			{
				tList1[tagIndex].tagPtr->Release();
			}
			return kTrue;
		}
		//----lalit-----------
		for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
		{
			tList1[tagIndex].tagPtr->Release();
		}
	return kFalse;
}

/// Function to get elements from Whole Document
bool16 Refresh::getDocumentSelectedBoxIds(RangeProgressBar *progressBar)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	do
    {
	    //TagReader tReader;
		InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!itagReader");
			return kFalse;
		}
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::fntDoc==nil");	
			return kFalse;
		}
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::layout == nil");		
			return kFalse;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::database==nil");			
			return kFalse;
		}
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::iSpreadList==nil");				
			return kFalse;
		}
		bool16 collisionFlag=kFalse;
		UIDList allPageItems(database);
        int32 pageNumber = 0;
        framePageMap.clear();

		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!spread");						
				return kFalse;
			}
			int numPages=spread->GetNumPages();

			for(int i=0; i<numPages; i++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(i, &tempList, kFalse);
                
                for(int k= tempList.Length() -1 ; k >=0 ; k--)
                {
                    allPageItems.Append(tempList.At(k));
                }
                
                for(int32 j=0; j < tempList.Length(); j ++)
                {
                    UIDRef frameRef= tempList.GetRef(j);
                    UID franeUID = frameRef.GetUID();
                    framePageMap.insert(map<UID ,int32 > ::value_type(franeUID, pageNumber));
                }
                pageNumber++;
			}
		}
        
		selectUIDList= allPageItems;
		PMString uidlist("selectUIDList before for Loop :");
		uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
		//CA(uidlist);

		int32 count=0;
		for( int p=0; p<selectUIDList.Length(); p++)
		{
			
			if(progressBar!=NULL)
				progressBar->SetPosition(selectUIDList.Length()-(selectUIDList.Length()-p)+1);

			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
			if(!iHier)
				continue;
            
			UID kidUID;
			int32 numKids=iHier->GetChildCount();
			bool16 isGroupFrame = kFalse ;
			isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(p));

			for(int j=0;j<numKids;j++)
			{
				//CA("1");
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				IIDXMLElement* ptr;
				TagList tList,tList_checkForHybrid;
				
				if(isGroupFrame == kTrue) 
				{
					tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
					if(tList_checkForHybrid.size() == 0)
					{
						//CA("tList_checkForHybrid.size() == 0");
						continue;
					}
					if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
						tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
					else
						tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
				}
				else 
				{
					tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
				
					if(tList_checkForHybrid.size() == 0)
					{
						continue;
					}

					if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
						tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(p), &ptr);
					else
						tList = itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
				}

				if(!doesExist(tList))//if(!doesExist(ptr))
				{
					selectUIDList.Append(kidUID);
				}
				
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
				{
					tList_checkForHybrid[tagIndex].tagPtr->Release();
				}
			}
		}

    }while(0);
	return kTrue;
  }


void Refresh::SetFocusForText(const UIDRef &boxUIDRef ,int32 StartText1, int32 EndText1)
{
	//CA(__FUNCTION__);
//	RangeData rData( StartText1, EndText1);	
//	
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
//	InterfacePtr<ILayoutSelectionSuite>layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{	CA("!layoutSelectionSuite");
//				return;
//			}
//
//	IDataBase * idatabase = boxUIDRef.GetDataBase();
//		UID uid = boxUIDRef.GetUID();
//		UIDList itemList(idatabase,uid);	
//
//		
//	layoutSelectionSuite->Select(itemList,Selection::kReplace,Selection::kDontScrollSelection);
//		
//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//	if(!txtMisSuite)
//	{
//		//CA("retun");
//		
//		return; 
//	}
//
//	UIDList	newUIDList;
//	txtMisSuite->GetUidList(newUIDList);
//
//	InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager),UseDefaultIID());
//	if(!txtSelSuite)
//	{ 
//		//CA("Text Selectin suite is null"); 
//		return;
//	}
//
//	InterfacePtr<IConcreteSelection>pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
//	// deprecated but universal (CS/2.0.2)
//
//	InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
//
//InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
//		UID textFrameUID = FrameUtils::GetTextFrameUID(unknown);
//		if (textFrameUID == kInvalidUID)
//			return;
//
////	InterfacePtr<ITextFrame>textFrame(/*boxUIDRef*/newUIDList.GetRef(0),UseDefaultIID());
//		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//	if(!textFrame)
//	{	CA("!textFrame");
//		return;
//	}
//		InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
//		if(!textModel)
//		{   CA("!textModel");
//			return;
//		}	
//	
//	txtSelSuite->SetTextSelection(::GetUIDRef(textModel),rData,Selection::kScrollIntoView,nil);
//
//	pTextTarget->SetTextFocus(pTextTarget->GetTextModel(),rData);
//	CA("1");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::textFrameUID is kInvalidUID");		
			return;
		}

//	InterfacePtr<ITextFrame>textFrame(/*boxUIDRef*/newUIDList.GetRef(0),UseDefaultIID());
/*	CS3 Change	
	InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if(!textFrame)
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::!textFrame");
		return;
	}
*/
/////////	CS3 Change
		
	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::graphicFrameHierarchy==nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::multiColumnItemHierarchy==nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::multiColumnItemTextFrame==nil");
		//CA("Its Not MultiColumn");
		return;
	}

	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::frameItemHierarchy==nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::textFrame==nil");
		return;
	}

/////////	End
	InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
	if(!textModel)
	{  
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::!textModel");
		return;
	}	
//	
//	txtSelSuite->SetTextSelection(::GetUIDRef(textModel),rData,Selection::kScrollIntoView,nil);
//
//	pTextTarget->SetTextFocus(pTextTarget->GetTextModel(),rData);

	InterfacePtr<IK2ServiceRegistry> pServiceRegistry(/*gSession*/GetExecutionContextSession(), UseDefaultIID());//Cs4
	if(!pServiceRegistry)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::IK2ServiceRegistry is nil");
		return;
	}
	InterfacePtr<IK2ServiceProvider> pServiceProvider(pServiceRegistry->QueryServiceProviderByClassID(kTextWalkerService, kTextWalkerServiceProviderBoss));
	if(!pServiceProvider)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::IK2ServiceProvider is nil");
		return;
	}
			
	InterfacePtr<ITextWalker> pWalker(pServiceProvider, UseDefaultIID());
	if(!pWalker)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::ITextWalker is nil");
		return;
	}
	InterfacePtr<ITextWalkerSelectionUtils>pUtils(pWalker,UseDefaultIID());
			
	const TextWalkerSelections_CriticalSection criticalSection(pUtils);			
	if(!pUtils)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::ITextWalkerSelectionUtils is nil");
		return;
	}

	InterfacePtr<ITextWalkerSelectionUtils> pTextWalkerSelectUtils(pWalker, UseDefaultIID());

	if(!pTextWalkerSelectUtils)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::SetFocusForText::ITextWalkerSelectionUtils is nil");
		return;
	}

    UIDRef onlyForDamnXCode = ::GetUIDRef(textModel);
	pTextWalkerSelectUtils->SelectText(onlyForDamnXCode, StartText1, EndText1 - StartText1);


}

int Refresh::deleteThisBox(UIDRef boxUIDRef)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
	if(scrap==nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::scrap is nil");	
		return 0;
	}
	InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
	if(!command)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!command");	
		return 0;
	}
	command->SetItemList(UIDList(boxUIDRef));
	if(CmdUtils::ProcessCommand(command)!=kSuccess)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!kSuccess");	
		return 0;
	}
	return 1;
}

ErrorCode Refresh::ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)
{
	//CA(__FUNCTION__);
	ErrorCode status = kFailure;
	do
	{
		if (commandClass == kInvalidClass)
		{
			ASSERT_FAIL("ProcessSimpleCommand: commandClass is invalid"); break;
		}
		InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(commandClass));
		if (cmd == nil)
		{
			ASSERT(cmd); break;
		}

		cmd->SetItemList(itemsIn);
		status = CmdUtils::ProcessCommand(cmd);

		if (status == kSuccess)
		{
			const UIDList& local_itemsOut = cmd->GetItemListReference();
			itemsOut = local_itemsOut;
		}
		else
		{
			ASSERT_FAIL("ProcessSimpleCommand: The command failed");
		}
	} while (false);
	return status;
}


ErrorCode Refresh::RefreshUserTableAsPerCellTagsForOneItem(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{	//CA("Inside RefreshUserTableAsPerCellTagsForOneItem");
	/* Dont use this function..
	Use RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable from CommonFunctions.cpp
	*/
	//CA(__FUNCTION__);
	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	bool16 isTableSizeZero = kFalse;
	vector<double> FinalItemIds;
	TagList TableTagList;
	TableTagList.clear();
	bool16 DeleteItemTable = kFalse;

	/*
	do
	{			
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		return errcode;

		if(!itagReader->GetUpdatedTag(slugInfo))
			return errcode;
		 	
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);		
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		
		if (textFrameUID == kInvalidUID)
		{
			break;
		}
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
			break;
		}
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			break;
		}
		UIDRef StoryUIDRef(::GetUIDRef(textModel));

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			break;
		}
		int32	tableIndex = tableList->GetModelCount()// - 1;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}

		//PMString ASD("TableIndex : ");
		//ASD.AppendNumber(tableIndex);
		//CA(ASD);
		
		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){// CA("!iTableUtlObj");
			break ;
		}
			do
			{
				VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(slugInfo.sectionID,slugInfo.parentId );
				if(!tableInfo)
				{
					isTableSizeZero = kTrue;//CA("tableinfo is nil");
					break;
				}

				if(tableInfo->size()==0){ //CA(" tableInfo->size()==0");
					isTableSizeZero = kTrue;
					break;
				}
						
				CObjectTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;

				bool16 typeidFound=kFalse;
				vector<int32> vec_items;
				FinalItemIds.clear();

				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{				
					oTableValue = *it;				
					vec_items = oTableValue.getItemIds();
				
					if(FinalItemIds.size() == 0)
					{
						FinalItemIds = vec_items;
					}
					else
					{
						for(int32 i=0; i<vec_items.size(); i++)
						{	bool16 Flag = kFalse;
							for(int32 j=0; j<FinalItemIds.size(); j++)
							{
								if(vec_items[i] == FinalItemIds[j])
								{
									Flag = kTrue;
									break;
								}				
							}
							if(!Flag)
								FinalItemIds.push_back(vec_items[i]);
						}
					}
				}

				//PMString ASD("FinalItemIds Size : ");
				//ASD.AppendNumber(FinalItemIds.size());
				//CA(ASD);

				TableTagList = itagReader->getTagsFromBox_ForRefresh(boxUIDRef);

				//PMString ASD1("TableTagList Size : ");
				//ASD1.AppendNumber(TableTagList.size());
				//CA(ASD1);

				if(FinalItemIds.size()>=1)
				{
					if(TableTagList.size()> FinalItemIds.size())
						DeleteItemTable = kTrue;

					else if(FinalItemIds.size()> TableTagList.size() )
					{
						//Handle it Differently;
						if(TableTagList.size()>1)
						{
							for(int32 j=TableTagList.size()-1; j>0; j--)
							{								
								XMLReference elementXMLref = TableTagList[j].tagPtr->GetXMLReference();
								ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElementAndContent(elementXMLref, kFalse);
							}
                         }
						
						InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
						if(!DataSprayerPtr)
						{
							//CA("Pointre to DataSprayerPtr no0 found");//
							return errcode;
						}
						PublicationNode refpNode;
						PMString PubName("PubName");
						refpNode.setAll(PubName, TableTagList[0].parentId, TableTagList[0].sectionID,3,1,1,1,TableTagList[0].parentTypeID,0,kFalse);
					
						DataSprayerPtr->FillPnodeStruct(refpNode, TableTagList[0].sectionID, 1, TableTagList[0].sectionID);
	
						ErrorCode result1 = iTableUtlObj->SprayUserTableAsPerCellTagsForMultipleItem(boxUIDRef, TableTagList[0], refpNode);
						if(result1 == kSuccess)
						{
							TagList NewTableTagList = itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
							for(int32 i=0; i<NewTableTagList.size(); i++)
							{
								PMString  attribVal("");
								attribVal.AppendNumber(TableTagList[0].parentId);
								NewTableTagList[i].tagPtr->SetAttributeValue("parentID", attribVal);//ObjectID		
								
								attribVal.Clear();							
								attribVal.AppendNumber(TableTagList[0].sectionID);
								NewTableTagList[i].tagPtr->SetAttributeValue("sectionID", attribVal);//Publication ID
								attribVal.Clear();

								attribVal.AppendNumber(TableTagList[0].parentTypeID);			// Need to check
								NewTableTagList[i].tagPtr->SetAttributeValue("parentTypeID", attribVal);//Parent Type ID
							}
						}

						return errcode;
					}
				}
				
			}while(0);

			if(isTableSizeZero)
			{	
				TableTagList = itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
				if(TableTagList.size()>1)
				{   
					for(int32 j=TableTagList.size()-1; j>0; j--)
					{								
						XMLReference elementXMLref = TableTagList[j].tagPtr->GetXMLReference();
						ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElementAndContent(elementXMLref, kFalse);
					}
				}
			}

		for(int i = 0; i<tableIndex; i++)
		{
			InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(i));
			if(tableModel == nil) {
				break;
			}
			UIDRef tableRef(::GetUIDRef(tableModel));

			IIDXMLElement* XMLElementPtr =  TableTagList[i].tagPtr; 		
			
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==nil)
			{
				CA("Err: invalid interface pointer ITableCommands");
				break;
			}

			XMLContentReference contentRef = XMLElementPtr->GetContentReference();
			if(contentRef.IsTable()) {
				//CA("Table Content");
			} 
			else if(contentRef.IsTableCell()) {
				//CA("Table Cell");
			} 

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableRef)
			{
				//CA("ContentRef != tableRef");	
				continue;
			}
			InterfacePtr<IXMLReferenceData> xmlRefData (contentRef.Instantiate());
			XMLReference xmlRef=xmlRefData->GetReference();
			IIDXMLElement *xmlElement=xmlRef.Instantiate();
			int elementCount=xmlElement->GetChildCount();
			
			PMString ORgTagString = xmlElement->GetTagString();
			//CA(ORgTagString);		
			bool16 refreshItemFlag = kTrue;
			for(int i1=0; i1<elementCount; i1++)
			{
				//CA("21");		
				XMLReference elementXMLref=xmlElement->GetNthChild(i1);
				IIDXMLElement * childElement=elementXMLref.Instantiate();
				if(childElement==nil)
					continue;
				//CA("22");
				PMString ChildtagName=childElement->GetTagString();
				//CA(ChildtagName);
				//if(tagName == ChildtagName){  //CA("tagName == ChildtagName");
				//	existingTagUID = childElement->GetTagUID();
				//	break;
				//}
				int elementCount1=childElement->GetChildCount();
				//PMString ASD("elementCount1 for Cell : ");
				//ASD.AppendNumber(elementCount1);
				//CA(ASD);
				for(int i1=0; i1<elementCount1; i1++)
				{
					XMLReference elementXMLref1=childElement->GetNthChild(i1);
					IIDXMLElement * childElement1=elementXMLref1.Instantiate();
					if(childElement1==nil)
						continue;
					PMString ChileEleName = childElement1->GetTagString();
					//CA("23");
					//CA(ChileEleName);		
					
					TagStruct tInfo;
					int32 attribCount=childElement1->GetAttributeCount();
					TextIndex sIndex=0, eIndex=0;
					Utils<IXMLUtils>()->GetElementIndices(childElement1, &sIndex, &eIndex);
					
					tInfo.startIndex=sIndex;
					tInfo.endIndex=eIndex;
					tInfo.tagPtr=childElement;
					
					for(int j=0; j<attribCount; j++)
					{
						PMString attribName=childElement1->GetAttributeNameAt(j);
						PMString attribVal=childElement1->GetAttributeValue(attribName);
						itagReader->getCorrespondingTagAttributes(attribName, attribVal, tInfo);
					}
					tInfo.curBoxUIDRef=boxUIDRef;					

					if(i1 ==0)
					{						
						if(IsDeleteFlagSelected == kTrue && tInfo.parentId != -1)
						{	
							bool16 ISProdDeleted = ptrIAppFramework->IsProductDeleted(tInfo.sectionID, tInfo.parentId);
							if(ISProdDeleted)
							{	//CA("Delete Table");
								this->deleteThisBox(boxUIDRef);
								return errcode;
							}
						}
					}					
					
					if(tInfo.parentId == -1)
					{
						return errcode;
					}

					bool16 ItemPresent = kFalse;
					
					if(isTableSizeZero == kFalse)
					{
						for(int32 j=0; j<FinalItemIds.size(); j++)
						{	
							if(tInfo.typeId == FinalItemIds[j])
							{ 
								ItemPresent = kTrue;
								break;
							}						
						}						
					}
				
					if(!ItemPresent && DeleteItemTable)
					{
						
						XMLReference elementXMLref = TableTagList[i].tagPtr->GetXMLReference();
						ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElementAndContent(elementXMLref, kFalse);
						
						break;
					}
					if(tInfo.typeId == -1)
					{
						InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
						if(!DataSprayerPtr)
						{
							//CA("Pointre to DataSprayerPtr not found");//
							return errcode;
						}
						PublicationNode refpNode;
						PMString PubName("PubName");
						refpNode.setAll(PubName, slugInfo.parentId, slugInfo.sectionID,3,1,1,1,slugInfo.parentTypeID,0,kFalse);
					
						DataSprayerPtr->FillPnodeStruct(refpNode, tInfo.sectionID, 1, tInfo.sectionID);
	
						ErrorCode result1 = iTableUtlObj->SprayUserTableAsPerCellTagsForMultipleItem(boxUIDRef, slugInfo, refpNode);
						if(result1 == kSuccess)
						{
							PMString  attribVal("");
							attribVal.AppendNumber(tInfo.parentId);
							slugInfo.tagPtr->SetAttributeValue("parentID", attribVal);//ObjectID		
							
							attribVal.Clear();							
							attribVal.AppendNumber(tInfo.sectionID);
							slugInfo.tagPtr->SetAttributeValue("sectionID", attribVal);//Publication ID
							attribVal.Clear();

							attribVal.AppendNumber(tInfo.parentTypeID);			// Need to check
							slugInfo.tagPtr->SetAttributeValue("parentTypeID", attribVal);//Parent Type ID
						}

						return errcode;
					}
									
					PMString resultString("");
					if(ItemPresent )
						resultString= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tInfo.typeId,tInfo.elementId,tInfo.languageID, refreshItemFlag);
					else
						resultString = "";
					refreshItemFlag = kFalse;
					//CA(result);
					//if(result == ""){ //CA("result is Empty");
					//	result.Append("N/A");
					//}

					WideString* wStr=new WideString(resultString);

					tableCommands->SetCellText(*wStr, GridAddress(tInfo.rowno,tInfo.colno));
					SlugStruct stencileInfo;
					stencileInfo.elementId =tInfo.elementId;
					//temp_to_test CAttributeValue oAttribVal;
					//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(tInfo.elementId);
					PMString dispname;//temp_to_test  = oAttribVal.getDisplayName();
					//CA(dispname);
					stencileInfo.elementName = dispname;
					PMString TableColName("ATTRID_") ;
					TableColName.AppendNumber(tInfo.elementId);
					stencileInfo.TagName = TableColName;
					if(isTableSizeZero || !ItemPresent)
						stencileInfo.typeId = -1;
					else
					stencileInfo.typeId  = tInfo.typeId;  // TypeId is hardcodded to -2 for Header Elements.
					stencileInfo.parentId = tInfo.parentId;  // -2 for Header Element
					stencileInfo.parentTypeId = tInfo.parentTypeID;
					stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.imgFlag = tInfo.sectionID; 
					stencileInfo.sectionID = tInfo.sectionID;
					stencileInfo.LanguageID = tInfo.languageID;
					stencileInfo.isAutoResize = tInfo.isAutoResize;
					XMLReference txmlref;
				
					iTableUtlObj->TagTable(tableRef,boxUIDRef,xmlElement->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, tInfo.rowno, tInfo.colno, 0 );
	

				}
			}
			if(isTableSizeZero)
				break;
		}

	}while(0);
	*/

	return errcode = 1;
}


void Refresh::ToggleBold_or_Italic(InterfacePtr<ITextModel> textModel, VectorHtmlTrackerPtr vectHtmlValuePtr, int32 StartTextIndex)
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	if(vectHtmlValuePtr->size() == 0)
	{
		return;
	}

	/*PMString QWE("vectHtmlValuePtr->size()");
	QWE.AppendNumber(vectHtmlValuePtr->size());
	CA(QWE);*/

	InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{
		//CA("NULL");
	}

	InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
	if (!layoutSelectionSuite) 
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ToggleBold_or_Italic::!layoutSelectionSuite");	
		return;
	}
//	layoutSelectionSuite->DeselectAll();	///	CS3 Change
	layoutSelectionSuite->DeselectAllPageItems();

	VectorHtmlTrackerValue::iterator it1;
	for(it1 = vectHtmlValuePtr->begin(); it1!=vectHtmlValuePtr->end(); it1++)
	{
		//CA(" QWWW ");
		/*PMString ASD(" StartIndex:  ");
		ASD.AppendNumber(StartTextIndex);
		ASD.Append("  Start : " );
		ASD.AppendNumber(StartTextIndex + it1->startIndex);
		ASD.Append("  End : ");
		ASD.AppendNumber(StartTextIndex + it1->EndIndex);
		CA(ASD);*/

		RangeData rangeData(StartTextIndex + it1->startIndex ,StartTextIndex + it1->EndIndex);

		UIDRef storyUIDRef(::GetUIDRef(textModel)); 
		InterfacePtr<ITextSelectionSuite>  txtSelectionSuite ( Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager));
		txtSelectionSuite->SetTextSelection(storyUIDRef,rangeData,Selection::kDontScrollSelection,nil);
			
		InterfacePtr<ITextMiscellanySuite> txtMisSuite2(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite2)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ToggleBold_or_Italic::!txtMisSuite2");	
			break;
		}						
		
		InterfacePtr<ITextAttributeSuite> textAttr(txtMisSuite2,UseDefaultIID());
		if(textAttr == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::ToggleBold_or_Italic::!textAttr");	
			break;
		}	
		if(it1->style ==  1)
			textAttr->ToggleBold();
		else if(it1->style ==  2)
			textAttr->ToggleItalic();	

	}
}

void Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm(TagStruct & tagInfo,const UIDRef& curBox,  bool16 isKitTable)
{
	//CA("inside sprayProductKitComponentTableScreenPrintInTabbedTextForm function");
	int32 CopyFlag=0;

	
	PMString elementName,colName;

	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	
	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	//First collect all the tabbedTextTag in a vector tabbedTagStructList.
			
		//check for the tabbedTextTag
	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	do{
		//CA("Before do while");
		if(tagInfo.whichTab == 4)
		{//start if1
			//CA("Inside Do while");
			/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
			if(unknown == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!unknown");
				break;
			}
			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrameUID");
				break;
			}
/*		////////	CS3 Change				
			InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
			if (textFrame == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrame");
				break;
			}
*/			
////////////////	Added by Amit
		InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!graphicFrameHierarchy");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!multiColumnItemHierarchy");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			return;
		}

		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!frameItemHierarchy");
			return;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrame");
			return;
		}

////////////////	End
			InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
			if (temptextModel == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!temptextModel");
				break;
			}
			textModel = temptextModel;

			txtModelUIDRef = ::GetUIDRef(textModel);
			
		/*	int32 sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;*/
			
			//PMString attrVal;
			//attrVal.AppendNumber(pNode.getPubId());	
			//tagInfo.tagPtr->SetAttributeValue("parentID",attrVal);
			//attrVal.Clear();
			//
			//attrVal.AppendNumber(CurrentObjectTypeID);
			//tagInfo.tagPtr->SetAttributeValue("parentTypeID",attrVal);
			//attrVal.Clear();
			//
			//attrVal.AppendNumber(sectionid);
			//tagInfo.tagPtr->SetAttributeValue("sectionID",attrVal);
			//attrVal.Clear();
			////tableFlag =-11 to identify that this is the "item" table tag in the 
			////tabbed text format.
			//tagInfo.tagPtr->SetAttributeValue("tableFlag","-11");
			////change the ID from -1 to -101, as -1 is elementId for productNumber.
			//tagInfo.tagPtr->SetAttributeValue("ID","-101");

			//end update the attributes of the parent tag
			IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
			int32 tagStartPos = 0;
			int32 tagEndPos =0; 			
			Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			tabbedTextData.Clear();
			replaceItemWithTabbedTextFrom = tagStartPos;
			replaceItemWithTabbedTextTo	= tagEndPos;

						
			bool16 isTableDataPresent = kTrue;
			double itemId = tagInfo.parentId; //vec_item_new[0]/*30017240*/;
				
			/*PMString ASD("itemid: ");
			ASD.AppendNumber(itemId);
			CA(ASD);*/

			VectorScreenTableInfoPtr KittableInfo = NULL;

			//KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemId, tagInfo.languageID, isKitTable); 
			if(!KittableInfo)
			{
				ptrIAppFramework->LogError("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!KittableInfo");
				return;
			}

			if(KittableInfo->size()==0){
				//CA(" KittableInfo->size()==0");
				isTableDataPresent = kFalse;

				if(isTableDataPresent == kFalse)
				{
					PMString textToInsert("");
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						textToInsert=tabbedTextData;					
					}
					else{
						textToInsert=iConverter->translateString(tabbedTextData);				
						iConverter->ChangeQutationMarkONOFFState(kFalse);
					}
					//WideString insertText(textToInsert);	
// CS3 Change		textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
					//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

					if(iConverter)
						iConverter->ChangeQutationMarkONOFFState(kTrue);
					break;
				}
				break;
			}
				
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			it = KittableInfo->begin();
			oTableValue = *it;
			isTableDataPresent = kTrue;
			
			vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
			vector<double> vec_items = oTableValue.getItemIds();
			bool8 isTranspose = kFalse;
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;

			int32/*int64*/ noOfColumns =static_cast<int32> (vectorOfTableHeader.size());
			int32/*int64*/ noOfRows = static_cast<int32>(vec_items.size());			

			//Check for the tableHeaderFlag at colno
			double colnoAsHeaderFlag = tagInfo.colno;
			//update the attributes of the parent tag
			
			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				//Note typeId == -2 for TableHeaders.
				int32 rowCount =0, colCount=0;
				
				if(colnoAsHeaderFlag != -555)//Spray table with the headers.
				{
					int32 headerIndex = 0;
					
					{ // For Quantity First Attribute in Header
						double elementId = -805;

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						newTagToBeAdded.elementName = "Quantity";
						newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");						
						}
						else
						{
							insertPMStringText.Append("\r");
						}

						tabbedTextData.Append(insertPMStringText);					

						//CA("tabbedTextData 2: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						tagStartPos = tagEndPos+ 1 + 2;	
					}

					for(headerIndex = 1;headerIndex < noOfColumns;headerIndex++)
					{
						//CA("Insdie For looop");
						double elementId = vectorOfTableHeader[headerIndex];

						 SlugStruct newTagToBeAdded;
						 newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						 newTagToBeAdded.parentId = tagInfo.parentId;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
						 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						 newTagToBeAdded.whichTab =4 ;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.sectionID = tagInfo.sectionID ;
						 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						 newTagToBeAdded.tableFlag = -12;
						 newTagToBeAdded.row_no=-1;
						 newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						 //listOfNewTagsAdded.push_back(newTagToBeAdded);
						
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");						
						}
						else
						{
							insertPMStringText.Append("\r");
						}

						tabbedTextData.Append(insertPMStringText);
						//CA("tabbedTextData 1: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						/*WideString insertText(insertPMStringText);
						if(headerIndex == 0)
						{
							//CA("headerIndex == 0");
							textModel->Replace(kTrue,tagStartPos,tagEndPos-tagStartPos + 1 ,&insertText);
							//textModel->Replace(kTrue,1 ,7,&insertText);
							//return;
						}
						else
						{
							textModel->Insert(kTrue,tagStartPos,&insertText);
						}*/				
						 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						 newTagToBeAdded.tagStartPos=tagStartPos;
						 newTagToBeAdded.tagEndPos=tagEndPos;
						 listOfNewTagsAdded.push_back(newTagToBeAdded);
						 // XMLReference createdElement;
						// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						 tagStartPos = tagEndPos+ 1 + 2;
			
					}					

				//	{ // For Availablity
				//		headerIndex++;
				//		int32 elementId = --806;

				//		SlugStruct newTagToBeAdded;
				//		newTagToBeAdded.elementId = elementId;
				//		newTagToBeAdded.typeId = -2;
				//		newTagToBeAdded.parentId = pNode.getPubId();
				//		newTagToBeAdded.imgflag = 0;
				//		newTagToBeAdded.parentTypeId = pNode.getTypeId();
				//		newTagToBeAdded.elementName = "Availablity";
				//		newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
				//		newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
				//		newTagToBeAdded.whichTab =3 ;
				//		newTagToBeAdded.imgFlag = 0;
				//		newTagToBeAdded.sectionID = sectionid ;
				//		newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
				//		newTagToBeAdded.tableFlag = -12;
				//		newTagToBeAdded.row_no=-1;
				//		newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

				//		PMString insertPMStringText;
				//		insertPMStringText.Append(newTagToBeAdded.elementName);
				//		if(headerIndex != noOfColumns - 1)
				//		{
				//			insertPMStringText.Append("\t");						
				//		}
				//		else
				//		{
				//			insertPMStringText.Append("\r");
				//		}

				//		tabbedTextData.Append(insertPMStringText);	

				//		//CA("tabbedTextData 3: "+ tabbedTextData);

				//		int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
				//		tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
				//		
				//		newTagToBeAdded.tagStartPos=tagStartPos;
				//		newTagToBeAdded.tagEndPos=tagEndPos;
				//		listOfNewTagsAdded.push_back(newTagToBeAdded);
				//		tagStartPos = tagEndPos+ 1 + 2;					
				//	}
				//	

					rowCount++;
				}
				//This for loop will spray the Item_copy_attributes value]
				vector<vector<PMString> >::iterator it1;
				for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
				{
					
					vector<PMString>::iterator it2;
					double itemId =-1;
					if(colnoAsHeaderFlag != -555)
						itemId = vec_items[rowCount-1];
					else
						itemId = vec_items[rowCount];
					
					for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
					{
						double elementId = vectorOfTableHeader[colCount];
						
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

						 SlugStruct newTagToBeAdded;
						 newTagToBeAdded.elementId = elementId;
						 newTagToBeAdded.typeId = itemId;
						 newTagToBeAdded.parentId = tagInfo.parentId;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						 PMString TableColName("");  
						 TableColName.Append("ATTRID_");
						 TableColName.AppendNumber(PMReal(elementId));

						 newTagToBeAdded.elementName = TableColName;
						 newTagToBeAdded.TagName = prepareTagName(TableColName);
						 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						 newTagToBeAdded.whichTab =4 ;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.sectionID = tagInfo.sectionID;
						 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						 newTagToBeAdded.tableFlag = -12;
						 newTagToBeAdded.row_no=-1;
						 newTagToBeAdded.col_no=colnoAsHeaderFlag;

						 //start
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						PMString insertPMStringText;
						insertPMStringText.Append((*it2));
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(colCount != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");							
						}
						else if(rowCount != noOfRows-1)
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);
						//WideString insertText(insertPMStringText);						
						//textModel->Insert(kTrue,tagStartPos,&insertText);
										
						 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						 newTagToBeAdded.tagStartPos=tagStartPos;
						 newTagToBeAdded.tagEndPos=tagEndPos;
						 listOfNewTagsAdded.push_back(newTagToBeAdded);
						 //XMLReference createdElement;
						 //Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						 tagStartPos = tagEndPos+ 1 + 2;
						 //end
						 colCount ++;

						 if(it2 == (*it1).begin())
							 it2++;
						//listOfNewTagsAdded.push_back(newTagToBeAdded);		
					}
					colCount =0;
					rowCount++;
				}				
			}//end of if IsTranspose == kFalse
			
				//CA(tabbedTextData);
				PMString textToInsert("");
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(!iConverter)
				{
					textToInsert=tabbedTextData;					
				}
				else{
					textToInsert=iConverter->translateString(tabbedTextData);
					iConverter->ChangeQutationMarkONOFFState(kFalse);
				}
				//WideString insertText(textToInsert);	
//	CS3 Change	textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
				//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
                textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
				ReplaceText(textModel,replaceItemWithTabbedTextFrom, replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

				if(iConverter)
					iConverter->ChangeQutationMarkONOFFState(kTrue);

				/*PMString ASD1("listOfNewTagsAdded.size() : ");
				ASD1.AppendNumber(listOfNewTagsAdded.size());
				CA(ASD1);*/

				for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
				{
					SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
					//CA(newTagToBeAdded.TagName);
				
					XMLReference createdElement;
					Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
					addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
				}
		
			}//end if1
	}while(kFalse);	
//			}//end for1
}//end of function


void Refresh::sprayXrefTableInTabbedTextForm(TagStruct & tagInfo,const UIDRef& curBox)
{
	//CA("inside sprayXrefTableInTabbedTextForm function");
	int32 CopyFlag=0;
	
	PMString elementName,colName;

	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	
	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	//First collect all the tabbedTextTag in a vector tabbedTagStructList.
			
	//check for the tabbedTextTag
	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	do{
		//CA("Before do while");
		if(tagInfo.whichTab == 4)
		{//start if1
			//CA("Inside Do while");
			/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
			if(unknown == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayXrefTableInTabbedTextForm::!unknown");
				break;
			}
			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayXrefTableInTabbedTextForm::!textFrameUID");
				break;
			}
				
			/*InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
			if (textFrame == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayXrefTableInTabbedTextForm::!textFrame");
				break;
			}
			*/

		InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}

			InterfacePtr<ITextModel> temptextModel(frameItemTFC->QueryTextModel());
			if (temptextModel == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayXrefTableInTabbedTextForm::!temptextModel");
				break;
			}
			textModel = temptextModel;

			txtModelUIDRef = ::GetUIDRef(textModel);
			
			IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
			int32 tagStartPos = 0;
			int32 tagEndPos =0; 			
			Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			tabbedTextData.Clear();
			replaceItemWithTabbedTextFrom = tagStartPos;
			replaceItemWithTabbedTextTo	= tagEndPos;
						
			bool16 isTableDataPresent = kTrue;
			double itemId = tagInfo.parentId; //vec_item_new[0]/*30017240*/;
				
			/*PMString ASD("itemid: ");
			ASD.AppendNumber(itemId);
			CA(ASD);*/

			VectorScreenTableInfoPtr XreftableInfo = NULL;

			//XreftableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemId, tagInfo.languageID ); 
			if(!XreftableInfo)
			{
				ptrIAppFramework->LogError("Refresh::Refresh::sprayXrefTableInTabbedTextForm::!XreftableInfo");
				return;
			}

			if(XreftableInfo->size()==0)
			{
				//CA(" KittableInfo->size()==0");
				isTableDataPresent = kFalse;

				if(isTableDataPresent == kFalse)
				{
					PMString textToInsert("");
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						textToInsert=tabbedTextData;					
					}
					else{
						textToInsert=iConverter->translateString(tabbedTextData);				
						iConverter->ChangeQutationMarkONOFFState(kFalse);
					}
					//WideString insertText(textToInsert);	
					//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel,replaceItemWithTabbedTextFrom, replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

					if(iConverter)
						iConverter->ChangeQutationMarkONOFFState(kTrue);
					break;
				}
				break;
			}
				
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			it = XreftableInfo->begin();
			oTableValue = *it;
			isTableDataPresent = kTrue;
			
			vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
			vector<double> vec_items = oTableValue.getItemIds();
			bool8 isTranspose = /*oTableValue.getTranspose()*/kFalse;
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;

			int32/*int64*/ noOfColumns =static_cast<int32> (vectorOfTableHeader.size());
			int32/*int64*/ noOfRows = static_cast<int32>(vec_items.size());			

			/*PMString numRow("numRows::");
			numRow.AppendNumber(noOfRows);
			CA(numRow);
			PMString numCol("numcols::");
			numCol.AppendNumber(noOfColumns);
			CA(numCol);*/

			//Check for the tableHeaderFlag at colno
			int32 colnoAsHeaderFlag = tagInfo.colno;
			//update the attributes of the parent tag
			
			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				//Note typeId == -2 for TableHeaders.
				int32 rowCount =0, colCount=0;
				
				if(colnoAsHeaderFlag != -555)//Spray table with the headers.
				{
					int32 headerIndex = 5;
					
					for(;headerIndex < noOfColumns;headerIndex++)
					{
						//CA("Insdie For looop");					

						double elementId = vectorOfTableHeader[headerIndex];
						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						newTagToBeAdded.parentId = itemId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentId;
						newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						
						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID =  tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						 //listOfNewTagsAdded.push_back(newTagToBeAdded);						
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						
						insertPMStringText.Append("\t");						
						
						tabbedTextData.Append(insertPMStringText);
						//CA("tabbedTextData 1: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
									
						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						 // XMLReference createdElement;
						// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						tagStartPos = tagEndPos+ 1 + 2;			
					}	

					for(int32 p=0; p<5; p++ )
					{ 
						SlugStruct newTagToBeAdded;
						double elementId1 = -1;
						PMString ElementName1 = "";
						switch (p)
						{
							case 0:							
									elementId1 = -807;
									ElementName1 = "Cross-reference Type";
									break;							
							case 1:							
									elementId1 = -808;
									ElementName1 = "Rating";
									break;							
							case 2:							
									elementId1 = -809;
									ElementName1 = "Alternate";
									break;
							case 3:							
									elementId1 = -810;
									ElementName1 = "Comments";
									break;							
							case 4:							
									elementId1 = -811;
									ElementName1 = "";
									break;
							default:
									elementId1 = -1;
									ElementName1 = "";
									break;
						}
						
						newTagToBeAdded.elementId = elementId1;
						newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						newTagToBeAdded.parentId = itemId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentId;
						newTagToBeAdded.elementName = ElementName1;

						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId1));

						//newTagToBeAdded.elementName = TableColName;

						newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						if(p < 4)
						{
							insertPMStringText.Append("\t");						
						}
						else if(p == 4 )
						{
							insertPMStringText.Append("\r");
						}

						tabbedTextData.Append(insertPMStringText);					

						//CA("tabbedTextData 2: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						tagStartPos = tagEndPos+ 1 + 2;	
					}	

					rowCount++;
				}
				//This for loop will spray the Item_copy_attributes value]
				vector<vector<PMString> >::iterator it1;
				for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
				{
					
					vector<PMString>::iterator it2;
					double itemId =-1;
					if(colnoAsHeaderFlag != -555)
						itemId = vec_items[rowCount-1];
					else
						itemId = vec_items[rowCount];
					
					colCount=5;
					for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
					{
						if(it2==(*it1).begin())
						{
							it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
							it2++;
							it2++;
							it2++;
							it2++;					
						}

						double elementId = vectorOfTableHeader[colCount];
						
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = itemId;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						newTagToBeAdded.elementName = TableColName;
						newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;

						//start
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						PMString insertPMStringText;
						insertPMStringText.Append((*it2));
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						/*if(colCount != noOfColumns - 1)
						{*/
							insertPMStringText.Append("\t");							
						/*}*/
						/*else if(rowCount != noOfRows-1)
						{
							insertPMStringText.Append("\r");
						}*/
						//CA("insertPMStringText : " + insertPMStringText);

						tabbedTextData.Append(insertPMStringText);
						//WideString insertText(insertPMStringText);						
						//textModel->Insert(kTrue,tagStartPos,&insertText);
										
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						//XMLReference createdElement;
						//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						tagStartPos = tagEndPos+ 1 + 2;
						//end
						colCount ++;						
							
					}
					
					//CA(" Spraying Hardcoded part now");
					colCount =0;
					for(it2=(*it1).begin();it2!=(*it1).end();it2++)
					{
						if(colCount >=5)
							break;
		
						double elementId = vectorOfTableHeader[colCount];						
						
						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = itemId;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;

						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						newTagToBeAdded.elementName = TableColName;
						newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;

						//start
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						PMString insertPMStringText;
						insertPMStringText.Append((*it2));
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(colCount < 4)
						{
							//CA(" colCount < 4 ");
							insertPMStringText.Append("\t");							
						}
						else if(colCount == 4)
						{
							//CA(" colCount == 4 ");
							insertPMStringText.Append("\r");
						}

						tabbedTextData.Append(insertPMStringText);
						//WideString insertText(insertPMStringText);						
						//textModel->Insert(kTrue,tagStartPos,&insertText);
										
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);						
						tagStartPos = tagEndPos+ 1 + 2;						
						colCount ++;	
					}
					colCount =0;
					rowCount++;
				}	


			}//end of if IsTranspose == kFalse
			
				//CA(tabbedTextData);
				PMString textToInsert("");
				if(!iConverter)
				{
					textToInsert=tabbedTextData;					
				}
				else{
					textToInsert=iConverter->translateString(tabbedTextData);
					iConverter->ChangeQutationMarkONOFFState(kFalse);
				}
				//WideString insertText(textToInsert);	
				//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
                textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
				ReplaceText(textModel,replaceItemWithTabbedTextFrom, replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

				if(iConverter)
						iConverter->ChangeQutationMarkONOFFState(kTrue);
				/*PMString ASD1("listOfNewTagsAdded.size() : ");
				ASD1.AppendNumber(listOfNewTagsAdded.size());
				CA(ASD1);*/

				for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
				{
					SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
					//CA(newTagToBeAdded.TagName);
				
					XMLReference createdElement;
					Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
					addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
				}
		
			}//end if1
	}while(kFalse);	
//			}//end for1
}//end of function


void Refresh::sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo,const UIDRef& curBox)
{
	//CA("inside sprayProductKitComponentTableScreenPrintInTabbedTextForm function");
	int32 CopyFlag=0;

	
	PMString elementName,colName;

	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	
	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	//First collect all the tabbedTextTag in a vector tabbedTagStructList.
			
		//check for the tabbedTextTag
	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	do{
		//CA("Before do while");
		if(tagInfo.whichTab == 4)
		{//start if1
			//CA("Inside Do while");
			/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
			if(unknown == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!unknown");
				break;
			}
			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrameUID");
				break;
			}
				
			/*InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
			if (textFrame == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrame");
				break;
			}*/

			InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!!ITextFrameColumn");
			break;
		}
			
			InterfacePtr<ITextModel> temptextModel(frameItemTFC->QueryTextModel());
			if (temptextModel == NULL)
			{
				ptrIAppFramework->LogDebug("Refresh::Refresh::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!temptextModel");
				break;
			}
			textModel = temptextModel;

			txtModelUIDRef = ::GetUIDRef(textModel);
			
		/*	int32 sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;*/
			
			//PMString attrVal;
			//attrVal.AppendNumber(pNode.getPubId());	
			//tagInfo.tagPtr->SetAttributeValue("parentID",attrVal);
			//attrVal.Clear();
			//
			//attrVal.AppendNumber(CurrentObjectTypeID);
			//tagInfo.tagPtr->SetAttributeValue("parentTypeID",attrVal);
			//attrVal.Clear();
			//
			//attrVal.AppendNumber(sectionid);
			//tagInfo.tagPtr->SetAttributeValue("sectionID",attrVal);
			//attrVal.Clear();
			////tableFlag =-11 to identify that this is the "item" table tag in the 
			////tabbed text format.
			//tagInfo.tagPtr->SetAttributeValue("tableFlag","-11");
			////change the ID from -1 to -101, as -1 is elementId for productNumber.
			//tagInfo.tagPtr->SetAttributeValue("ID","-101");

			//end update the attributes of the parent tag
			IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
			int32 tagStartPos = 0;
			int32 tagEndPos =0; 			
			Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			tabbedTextData.Clear();
			replaceItemWithTabbedTextFrom = tagStartPos;
			replaceItemWithTabbedTextTo	= tagEndPos;

						
			bool16 isTableDataPresent = kTrue;
			double itemId = tagInfo.parentId; //vec_item_new[0]/*30017240*/;
				
			/*PMString ASD("itemid: ");
			ASD.AppendNumber(itemId);
			CA(ASD);*/

			VectorScreenTableInfoPtr AccessorytableInfo = NULL;

			//AccessorytableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemId, tagInfo.languageID); 
			if(!AccessorytableInfo)
			{
				ptrIAppFramework->LogError("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!AccessorytableInfo");
				return;
			}

			if(AccessorytableInfo->size()==0)
			{
				//CA(" XreftableInfo->size()==0");
				isTableDataPresent = kFalse;

				if(isTableDataPresent == kFalse)
				{
					PMString textToInsert("");					
					if(!iConverter)
					{
						textToInsert=tabbedTextData;					
					}
					else{
						textToInsert=iConverter->translateString(tabbedTextData);				
						iConverter->ChangeQutationMarkONOFFState(kFalse);
					}
					//WideString insertText(textToInsert);	
					//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel,replaceItemWithTabbedTextFrom, replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
					if(iConverter)
					{
						iConverter->ChangeQutationMarkONOFFState(kFalse);			
					}
					break;
				}
				break;
			}
				
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			it = AccessorytableInfo->begin();
			oTableValue = *it;
			isTableDataPresent = kTrue;
			
			vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
			vector<double> vec_items = oTableValue.getItemIds();
			bool8 isTranspose = /*oTableValue.getTranspose()*/kFalse;
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;

			int32/*int64*/ noOfColumns = static_cast<int32>(vectorOfTableHeader.size());
			int32/*int64*/ noOfRows =static_cast<int32> (vec_items.size());			

		/*	PMString numRow("numRows::");
			numRow.AppendNumber(noOfRows);
			CA(numRow);
			PMString numCol("numcols::");
			numCol.AppendNumber(noOfColumns);
			CA(numCol);*/

			//Check for the tableHeaderFlag at colno
			int32 colnoAsHeaderFlag = tagInfo.colno;
			//update the attributes of the parent tag
			
			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				//Note typeId == -2 for TableHeaders.
				int32 rowCount =0, colCount=0;
				
				if(colnoAsHeaderFlag != -555)//Spray table with the headers.
				{
					for(int32 p=0; p<3; p++ )
					{ 
						SlugStruct newTagToBeAdded;
						double elementId = -1;
						PMString ElementName = "";
						switch (p)
						{
							case 0:							
									elementId = -812;
									ElementName = "Quantity";
									break;							
							case 1:							
									elementId = -813;
									ElementName = "Required";
									break;							
							case 2:							
									elementId = -814;
									ElementName = "Comments";
									break;
							default:
									elementId = -1;
									ElementName = "";
									break;
						}
						
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						newTagToBeAdded.elementName = ElementName;

						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						//newTagToBeAdded.elementName = TableColName;
						/*newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);*/
						newTagToBeAdded.TagName = prepareTagName(TableColName);;
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						
						insertPMStringText.Append("\t");						
						
						
						tabbedTextData.Append(insertPMStringText);					

						//CA("tabbedTextData 2: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						tagStartPos = tagEndPos+ 1 + 2;	
					}

					int32 headerIndex = 3;

					for(;headerIndex < noOfColumns;headerIndex++)
					{
						//CA("Insdie For looop");					

						double elementId = vectorOfTableHeader[headerIndex];
						 SlugStruct newTagToBeAdded;
						 newTagToBeAdded.elementId = elementId;
						 newTagToBeAdded.typeId = tagInfo.typeId;
						newTagToBeAdded.header =1;
						 newTagToBeAdded.parentId =tagInfo.parentId;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);
						 //newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
						 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						 newTagToBeAdded.whichTab =4 ;
						 newTagToBeAdded.imgFlag = 0;
						 newTagToBeAdded.sectionID = tagInfo.sectionID ;
						 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						 newTagToBeAdded.tableFlag = -12;
						 newTagToBeAdded.row_no=-1;
						 newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

						 //listOfNewTagsAdded.push_back(newTagToBeAdded);						
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);
						
						if(headerIndex < noOfColumns -1)
							insertPMStringText.Append("\t");
						else
							insertPMStringText.Append("\r");
						
						tabbedTextData.Append(insertPMStringText);
						//CA("tabbedTextData 1: "+ tabbedTextData);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
									
						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						 // XMLReference createdElement;
						// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						tagStartPos = tagEndPos+ 1 + 2;			
					}	
									
					rowCount++;
				}
				//This for loop will spray the Item_copy_attributes value]

				//tableData =[1, false, , CGO60610P, Wall Poster, 24" x 36", 37.88, 31.99],	
				vector<vector<PMString> >::iterator it1;
					
				for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
				{					
					vector<PMString>::iterator it2;
					double itemId =-1;
					if(colnoAsHeaderFlag != -555)
						itemId = vec_items[rowCount-1];
					else
						itemId = vec_items[rowCount];


					//CA(" Spraying Hardcoded part first");
					colCount =0;
					for(it2=(*it1).begin();it2!=(*it1).end();it2++)
					{
						if(colCount == 3)
							break;
						double elementId = vectorOfTableHeader[colCount];						
						
						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = itemId;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;

						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						newTagToBeAdded.elementName = TableColName;
						newTagToBeAdded.TagName = prepareTagName(TableColName);
						//newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;

						//start
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						PMString insertPMStringText;
						insertPMStringText.Append((*it2));
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						
						insertPMStringText.Append("\t");							
						
						tabbedTextData.Append(insertPMStringText);
						//WideString insertText(insertPMStringText);						
						//textModel->Insert(kTrue,tagStartPos,&insertText);
										
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						//XMLReference createdElement;
						//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						tagStartPos = tagEndPos+ 1 + 2;
						//end
						colCount ++;
				
						//listOfNewTagsAdded.push_back(newTagToBeAdded);		
					}	

					//CA(" Spraying attributes Now");
					colCount=3;
					for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
					{

						if(it2==(*it1).begin())
						{
							it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
							it2++;
							it2++;
						}

						double elementId = vectorOfTableHeader[colCount];
						
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = itemId;
						newTagToBeAdded.parentId = tagInfo.parentId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = tagInfo.parentTypeID;
						PMString TableColName("");  
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(elementId));

						newTagToBeAdded.elementName = TableColName;
						newTagToBeAdded.TagName = prepareTagName(TableColName);
						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = tagInfo.sectionID ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=colnoAsHeaderFlag;

						//start
						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
						PMString insertPMStringText;
						insertPMStringText.Append((*it2));
						if(!iConverter)
						{
							insertPMStringText=insertPMStringText;					
						}
						else
							insertPMStringText=iConverter->translateString(insertPMStringText);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						/*if(colCount != noOfColumns - 1)
						{*/
						if(colCount < noOfColumns-1)
							insertPMStringText.Append("\t");
						else
							insertPMStringText.Append("\r");
						/*}*/
						/*else if(rowCount != noOfRows-1)
						{
							insertPMStringText.Append("\r");
						}*/
						//CA("insertPMStringText : " + insertPMStringText);

						tabbedTextData.Append(insertPMStringText);
						//WideString insertText(insertPMStringText);						
						//textModel->Insert(kTrue,tagStartPos,&insertText);
										
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						//XMLReference createdElement;
						//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
						tagStartPos = tagEndPos+ 1 + 2;
						//end
						colCount ++;						
							
					}
							

					colCount =0;
					rowCount++;	
				}				
			}
			
			//CA(tabbedTextData);
			PMString textToInsert("");
			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tabbedTextData;					
			}
			else{
				textToInsert=iConverter->translateString(tabbedTextData);
				iConverter->ChangeQutationMarkONOFFState(kFalse);
			}
			//WideString insertText(textToInsert);	
			//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,replaceItemWithTabbedTextFrom, replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
			if(iConverter)
			{
				iConverter->ChangeQutationMarkONOFFState(kTrue);					
			}
			/*PMString ASD1("listOfNewTagsAdded.size() : ");
			ASD1.AppendNumber(listOfNewTagsAdded.size());
			CA(ASD1);*/

			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
			{
				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
				//CA(newTagToBeAdded.TagName);
			
				XMLReference createdElement;
				Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
			}
	
		}//end if1
	}while(kFalse);	
//			}//end for1
}//end of function


//void Refresh::sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo ,const UIDRef& curBox)
//{
//	//CA("Refresh::sprayAccessoryTableScreenPrintInTabbedTextForm");
//	int32 CopyFlag=0;
//
//	PMString elementName,colName;
//
//	vector<int32> FinalItemIds;
//	bool16 firstTabbedTextTag = kTrue;
//	tabbedTagStructList.clear();
//
//	UIDRef txtModelUIDRef = UIDRef::gNull;
//	InterfacePtr<ITextModel> textModel;
//	
//	TextIndex startPos=-1;
//	TextIndex endPos = -1;
//	//First collect all the tabbedTextTag in a vector tabbedTagStructList.
//			
//		//check for the tabbedTextTag
//	int32 replaceItemWithTabbedTextFrom = -1;
//	int32 replaceItemWithTabbedTextTo = -1;
//	PMString tabbedTextData;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//	{
//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//		return;
//	}
//
//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//
//	do
//	{
//		if(tagInfo.whichTab == 4 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
//		{//start if1
//			//CA("Inside Do while");
//			InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
//			if(unknown == NULL)
//			{
//				ptrIAppFramework->LogDebug("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!unknown");
//				break;
//			}
//			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//			if (textFrameUID == kInvalidUID)
//			{
//				ptrIAppFramework->LogDebug("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrameUID");
//				break;
//			}
//				
//			InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//			if (textFrame == NULL)
//			{
//				ptrIAppFramework->LogDebug("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrame");
//				break;
//			}
//				
//			InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
//			if (temptextModel == NULL)
//			{
//				ptrIAppFramework->LogDebug("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!temptextModel");
//				break;
//			}
//			textModel = temptextModel;
//
//			txtModelUIDRef = ::GetUIDRef(textModel);
//			
//			int32 sectionid = -1;
//			if(global_project_level == 3)
//				sectionid = CurrentSubSectionID;
//			if(global_project_level == 2)
//				sectionid = CurrentSectionID;
//			
//			PMString attrVal;
//			attrVal.AppendNumber(pNode.getPubId());	
//			tagInfo.tagPtr->SetAttributeValue("parentID",attrVal);
//			attrVal.Clear();
//			
//			attrVal.AppendNumber(CurrentObjectTypeID);
//			tagInfo.tagPtr->SetAttributeValue("parentTypeID",attrVal);
//			attrVal.Clear();
//			
//			attrVal.AppendNumber(sectionid);
//			tagInfo.tagPtr->SetAttributeValue("sectionID",attrVal);
//			attrVal.Clear();
//			//tableFlag =-11 to identify that this is the "item" table tag in the 
//			//tabbed text format.
//			tagInfo.tagPtr->SetAttributeValue("tableFlag","-11");
//			//change the ID from -1 to -101, as -1 is elementId for productNumber.
//			tagInfo.tagPtr->SetAttributeValue("ID","-101");
//
//			//end update the attributes of the parent tag
//			IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
//			int32 tagStartPos = 0;
//			int32 tagEndPos =0; 			
//			Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
//			tagStartPos = tagStartPos + 1;
//			tagEndPos = tagEndPos -1;
//
//			tabbedTextData.Clear();
//			replaceItemWithTabbedTextFrom = tagStartPos;
//			replaceItemWithTabbedTextTo	= tagEndPos;
//						
//			bool16 isTableDataPresent = kTrue;
//			
//			int32 itemId = pNode.getPubId(); //vec_item_new[0]/*30017240*/;
//			
//			/*PMString ASD("itemid: ");
//			ASD.AppendNumber(itemId);
//			CA(ASD);*/
//
//			VectorScreenTableInfoPtr AccessorytableInfo = NULL;
//
//			AccessorytableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemId, tagInfo.languageID); 
//			if(!AccessorytableInfo)
//			{
//				ptrIAppFramework->LogError("Refresh::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!AccessorytableInfo");
//				return;
//			}
//
//			if(AccessorytableInfo->size()==0)
//			{
//				//CA(" XreftableInfo->size()==0");
//				isTableDataPresent = kFalse;
//
//				if(isTableDataPresent == kFalse)
//				{
//					PMString textToInsert("");					
//					if(!iConverter)
//					{
//						textToInsert=tabbedTextData;					
//					}
//					else
//						textToInsert=iConverter->translateString(tabbedTextData);				
//					WideString insertText(textToInsert);	
//					textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
//					break;
//				}
//				break;
//			}
//				
//			CObjectTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//			it = AccessorytableInfo->begin();
//			oTableValue = *it;
//			isTableDataPresent = kTrue;
//			
//			vector<int32> vectorOfTableHeader = oTableValue.getTableHeader();
//			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
//			vector<int32> vec_items = oTableValue.getItemIds();
//			bool8 isTranspose = kFalse;
//			vector<vector<SlugStruct> > RowList;
//			vector<SlugStruct> listOfNewTagsAdded;
//
//			int32 noOfColumns = vectorOfTableHeader.size();
//			int32 noOfRows = vec_items.size();			
//
//		/*	PMString numRow("numRows::");
//			numRow.AppendNumber(noOfRows);
//			CA(numRow);
//			PMString numCol("numcols::");
//			numCol.AppendNumber(noOfColumns);
//			CA(numCol);*/
//
//			//Check for the tableHeaderFlag at colno
//			int32 colnoAsHeaderFlag = tagInfo.colno;
//			//update the attributes of the parent tag
//
//			if(!isTranspose)
//			{
//				/*  
//				In case of Transpose == kFalse..the table gets sprayed like 
//					Header11 Header12  Header13 ...
//					Data11	 Data12	   Data13
//					Data21	 Data22	   Data23
//					....
//					....
//					So first row is Header Row and subsequent rows are DataRows.
//				*/
//				//The for loop below will spray the Table Header in left to right fashion.
//				//Note typeId == -2 for TableHeaders.
//				int32 rowCount =0, colCount=0;
//				
//				//tableHeader = [-812, -813, -814,  11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
//				if(colnoAsHeaderFlag != -555)//Spray table with the headers.
//				{
//					for(int32 p=0; p<3; p++ )
//					{ 
//						SlugStruct newTagToBeAdded;
//						int32 elementId = -1;
//						PMString ElementName = "";
//						switch (p)
//						{
//							case 0:							
//									elementId = -812;
//									ElementName = "Quantity";
//									break;							
//							case 1:							
//									elementId = -813;
//									ElementName = "Required";
//									break;							
//							case 2:							
//									elementId = -814;
//									ElementName = "Comments";
//									break;
//							default:
//									elementId = -1;
//									ElementName = "";
//									break;
//						}
//						
//						newTagToBeAdded.elementId = elementId;
//						newTagToBeAdded.typeId = -2;
//						newTagToBeAdded.parentId = pNode.getPubId();
//						newTagToBeAdded.imgflag = 0;
//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
//						newTagToBeAdded.elementName = ElementName;
//
//						PMString TableColName("");  
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(elementId);
//
//						//newTagToBeAdded.elementName = TableColName;
//
//						newTagToBeAdded.TagName = prepareTagName(TableColName);
//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);
//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						newTagToBeAdded.whichTab =4 ;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.sectionID = sectionid ;
//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						newTagToBeAdded.tableFlag = -12;
//						newTagToBeAdded.row_no=-1;
//						newTagToBeAdded.col_no=colnoAsHeaderFlag;					 
//
//						PMString insertPMStringText;
//						insertPMStringText.Append(newTagToBeAdded.elementName);
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//						
//						insertPMStringText.Append("\t");						
//						
//						
//						tabbedTextData.Append(insertPMStringText);					
//
//						//CA("tabbedTextData 2: "+ tabbedTextData);
//
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
//						
//						newTagToBeAdded.tagStartPos=tagStartPos;
//						newTagToBeAdded.tagEndPos=tagEndPos;
//						listOfNewTagsAdded.push_back(newTagToBeAdded);
//						tagStartPos = tagEndPos+ 1 + 2;	
//					}
//
//					int32 headerIndex = 3;
//
//					for(;headerIndex < noOfColumns;headerIndex++)
//					{
//						//CA("Insdie For looop");					
//
//						int32 elementId = vectorOfTableHeader[headerIndex];
//						 SlugStruct newTagToBeAdded;
//						 newTagToBeAdded.elementId = elementId;
//						 newTagToBeAdded.typeId = -2;
//						 newTagToBeAdded.parentId = pNode.getPubId();
//						 newTagToBeAdded.imgflag = 0;
//						 newTagToBeAdded.parentTypeId = pNode.getTypeId();
//						 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
//						 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);
//						 newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
//						 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						 newTagToBeAdded.whichTab =4 ;
//						 newTagToBeAdded.imgFlag = 0;
//						 newTagToBeAdded.sectionID = sectionid ;
//						 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						 newTagToBeAdded.tableFlag = -12;
//						 newTagToBeAdded.row_no=-1;
//						 newTagToBeAdded.col_no=colnoAsHeaderFlag;					 
//
//						 //listOfNewTagsAdded.push_back(newTagToBeAdded);						
//						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
//						
//						PMString insertPMStringText;
//						insertPMStringText.Append(newTagToBeAdded.elementName);
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//						
//						if(headerIndex < noOfColumns -1)
//							insertPMStringText.Append("\t");
//						else
//							insertPMStringText.Append("\r");
//						
//						tabbedTextData.Append(insertPMStringText);
//						//CA("tabbedTextData 1: "+ tabbedTextData);
//
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//									
//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
//						
//						newTagToBeAdded.tagStartPos=tagStartPos;
//						newTagToBeAdded.tagEndPos=tagEndPos;
//						listOfNewTagsAdded.push_back(newTagToBeAdded);
//						 // XMLReference createdElement;
//						// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
//						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
//						tagStartPos = tagEndPos+ 1 + 2;			
//					}	
//									
//					rowCount++;
//				}
//				//This for loop will spray the Item_copy_attributes value]
//
//				//tableData =[1, false, , CGO60610P, Wall Poster, 24" x 36", 37.88, 31.99],	
//				vector<vector<PMString> >::iterator it1;
//					
//				for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
//				{					
//					vector<PMString>::iterator it2;
//					int32 itemId =-1;
//					if(colnoAsHeaderFlag != -555)
//						itemId = vec_items[rowCount-1];
//					else
//						itemId = vec_items[rowCount];
//
//
//					//CA(" Spraying Hardcoded part first");
//					colCount =0;
//					for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//					{
//						if(colCount == 3)
//							break;
//						int32 elementId = vectorOfTableHeader[colCount];						
//						
//						SlugStruct newTagToBeAdded;
//						newTagToBeAdded.elementId = elementId;
//						newTagToBeAdded.typeId = itemId;
//						newTagToBeAdded.parentId = pNode.getPubId();
//						newTagToBeAdded.imgflag = 0;
//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
//
//						PMString TableColName("");  
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(elementId);
//
//						newTagToBeAdded.elementName = TableColName;
//						newTagToBeAdded.TagName = prepareTagName(TableColName);
//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						newTagToBeAdded.whichTab =4 ;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.sectionID = sectionid;
//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						newTagToBeAdded.tableFlag = -12;
//						newTagToBeAdded.row_no=-1;
//						newTagToBeAdded.col_no=colnoAsHeaderFlag;
//
//						//start
//						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
//						PMString insertPMStringText;
//						insertPMStringText.Append((*it2));
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//						
//						insertPMStringText.Append("\t");							
//						
//						tabbedTextData.Append(insertPMStringText);
//						//WideString insertText(insertPMStringText);						
//						//textModel->Insert(kTrue,tagStartPos,&insertText);
//										
//						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
//						newTagToBeAdded.tagStartPos=tagStartPos;
//						newTagToBeAdded.tagEndPos=tagEndPos;
//						listOfNewTagsAdded.push_back(newTagToBeAdded);
//						//XMLReference createdElement;
//						//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
//						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
//						tagStartPos = tagEndPos+ 1 + 2;
//						//end
//						colCount ++;
//				
//						//listOfNewTagsAdded.push_back(newTagToBeAdded);		
//					}	
//
//					//CA(" Spraying attributes Now");
//					colCount=3;
//					for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
//					{
//
//						if(it2==(*it1).begin())
//						{
//							it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
//							it2++;
//							it2++;
//						}
//
//						int32 elementId = vectorOfTableHeader[colCount];
//						
//						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
//
//						SlugStruct newTagToBeAdded;
//						newTagToBeAdded.elementId = elementId;
//						newTagToBeAdded.typeId = itemId;
//						newTagToBeAdded.parentId = pNode.getPubId();
//						newTagToBeAdded.imgflag = 0;
//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
//						PMString TableColName("");  
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(elementId);
//
//						newTagToBeAdded.elementName = TableColName;
//						newTagToBeAdded.TagName = prepareTagName(TableColName);
//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
//						newTagToBeAdded.whichTab =4 ;
//						newTagToBeAdded.imgFlag = 0;
//						newTagToBeAdded.sectionID = sectionid;
//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
//						newTagToBeAdded.tableFlag = -12;
//						newTagToBeAdded.row_no=-1;
//						newTagToBeAdded.col_no=colnoAsHeaderFlag;
//
//						//start
//						//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
//						PMString insertPMStringText;
//						insertPMStringText.Append((*it2));
//						if(!iConverter)
//						{
//							insertPMStringText=insertPMStringText;					
//						}
//						else
//							insertPMStringText=iConverter->translateString(insertPMStringText);
//
//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
//						/*if(colCount != noOfColumns - 1)
//						{*/
//						if(colCount < noOfColumns-1)
//							insertPMStringText.Append("\t");
//						else
//							insertPMStringText.Append("\r");
//						/*}*/
//						/*else if(rowCount != noOfRows-1)
//						{
//							insertPMStringText.Append("\r");
//						}*/
//						//CA("insertPMStringText : " + insertPMStringText);
//
//						tabbedTextData.Append(insertPMStringText);
//						//WideString insertText(insertPMStringText);						
//						//textModel->Insert(kTrue,tagStartPos,&insertText);
//										
//						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
//						newTagToBeAdded.tagStartPos=tagStartPos;
//						newTagToBeAdded.tagEndPos=tagEndPos;
//						listOfNewTagsAdded.push_back(newTagToBeAdded);
//						//XMLReference createdElement;
//						//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
//						// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
//						tagStartPos = tagEndPos+ 1 + 2;
//						//end
//						colCount ++;						
//							
//					}
//							
//
//					colCount =0;
//					rowCount++;	
//				}				
//			}//en
//
//			
//			//CA(tabbedTextData);
//			PMString textToInsert("");
//			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//			if(!iConverter)
//			{
//				textToInsert=tabbedTextData;					
//			}
//			else
//				textToInsert=iConverter->translateString(tabbedTextData);
//			
//			WideString insertText(textToInsert);	
//			textModel->Replace(kTrue,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
//			
//			/*	PMString ASD1("listOfNewTagsAdded.size() : ");
//				ASD1.AppendNumber(listOfNewTagsAdded.size());
//				CA(ASD1);*/
//
//			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
//			{
//				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
//				//CA(newTagToBeAdded.TagName);
//			
//				XMLReference createdElement;
//				Utils<IXMLElementCommands>()->CreateElement(newTagToBeAdded.TagName , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
//				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
//
//			}
//
//		}
//
//	}while( kFalse );
//}

void Refresh::sprayHybridTableScreenPrintInTabbedTextForm(TagStruct & tagInfo ,const UIDRef& curBox)
{
	//CA("Refresh::sprayHybridTableScreenPrintInTabbedTextForm");
	//int32 CopyFlag = 0;
	//	
	//PMString elementName,colName;

	//bool16 firstTabbedTextTag = kTrue;
	//
	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//TextIndex startPos=-1;
	//TextIndex endPos = -1;
	//
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData;
	//
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == NULL)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//	return;
	//}
	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//do{
	//	InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
	//	if(unknown == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!unknown");	
	//		break;
	//	}
	//	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	//	if (textFrameUID == kInvalidUID)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!textFrameUID");	
	//		break;
	//	}
	//	
	//	/*InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//	if (textFrame == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!textFrame");	
	//		break;
	//	}
	//	
	//	InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
	//	if (temptextModel == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!temptextModel");	
	//		break;
	//	}*/

	//	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	//	if (graphicFrameHierarchy == nil){
	//		//CA("graphicFrameHierarchy is NULL");
	//		break;
	//	}
	//					
	//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//	if (!multiColumnItemHierarchy){
	//		//CA("multiColumnItemHierarchy is NULL");
	//		break;
	//	}

	//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//	if (!multiColumnItemTextFrame){
	//		//CA("multiColumnItemTextFrame is NULL");
	//		break;
	//	}
	//	InterfacePtr<IHierarchy>
	//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	//	if (!frameItemHierarchy){
	//		//CA("frameItemHierarchy is NULL");
	//		break;
	//	}

	//	InterfacePtr<ITextFrameColumn>
	//	frameItemTFC(frameItemHierarchy, UseDefaultIID());
	//	if (!frameItemTFC) {
	//		//CA("!!!ITextFrameColumn");
	//		break;
	//	}
	//	InterfacePtr<ITextModel> temptextModel(frameItemTFC->QueryTextModel());
	//	if(!temptextModel)
	//	{
	//		//CA("!temptextModel" );
	//		break;
	//	}

	//	textModel = temptextModel;

	//	txtModelUIDRef = ::GetUIDRef(textModel);
	//		
	//	int32 objectId = tagInfo.parentId;
	//	int32 sectionId = tagInfo.sectionID;
	//	int32 parentTypeId = tagInfo.parentTypeID;
	//			
	//	IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//	int32 tagStartPos = 0;
	//	int32 tagEndPos =0; 			
	//	Utils<IXMLUtils>()->GetElementIndices(itemTagPtr,&tagStartPos,&tagEndPos);
	//	tagStartPos = tagStartPos + 1;
	//	tagEndPos = tagEndPos -1;

	//	tabbedTextData.Clear();
	//	replaceItemWithTabbedTextFrom = tagStartPos;
	//	replaceItemWithTabbedTextTo	= tagEndPos;
	//	
	//	bool16 isTableDataPresent = kTrue;
	//	bool16 typeidFound = kFalse;

	//	CAdvanceTableScreenValue oTableValue;
	//	do
	//	{
	//		VectorAdvanceTableScreenValuePtr tableInfo = NULL;
	//		if(tagInfo.whichTab == 5)
	//		{
	//			tableInfo =ptrIAppFramework->getHybridTableData(sectionId,objectId,tagInfo.languageID,kTrue);
	//		}
	//		else
	//			{
	//			tableInfo=ptrIAppFramework->getHybridTableData(sectionId, objectId,tagInfo.languageID,kFalse);
	//			}
	//		
	//		if(!tableInfo)
	//		{
	//			isTableDataPresent = kFalse;
	//			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!tableInfo");	
	//			break;
	//		}

	//		if(tableInfo->size()==0)
	//		{ 
	//			//CA(" tableInfo->size()==0");
	//			isTableDataPresent = kFalse;
	//			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::tableInfo->size()==0");	
	//			break;
	//		}
	//		
	//		VectorAdvanceTableScreenValue::iterator it;
	//		it=tableInfo->begin();

	//		//oTableValue = *it;
	//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//		{
	//			oTableValue = *it;
	//			if(oTableValue.getTableTypeId() == tagInfo.typeId)
	//			{   				
	//				typeidFound=kTrue;				
	//				break;
	//			}
	//		}
	//		if(tableInfo)
	//			delete tableInfo;

	//	}while(kFalse);
	//		
	//	if(isTableDataPresent == kFalse)
	//	{	
	//		if(typeidFound == kFalse)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::typeidFound == kFalse");	
	//		}			
	//		PMString textToInsert("");
	//		
	//		if(!iConverter)
	//		{
	//			textToInsert=tabbedTextData;				
	//		}
	//		else{
	//			textToInsert=iConverter->translateString(tabbedTextData);				
	//			iConverter->ChangeQutationMarkONOFFState(kFalse);
	//		}
	//		WideString insertText(textToInsert);	
	//		textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//		if(iConverter)
	//		{
	//			iConverter->ChangeQutationMarkONOFFState(kTrue);			
	//		}
	//		break;
	//	}

	//	if(typeidFound == kFalse)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::typeidFound == kFalse");	
	//		break;
	//	}
	//		
	//	bool8 isTranspose = oTableValue.getTranspose();
	//	
	//	int32 bodyRows= oTableValue.getBodyRows();
	//	int32 bodyColumns= oTableValue.getBodyColumns();
	//	int32 headerRows= oTableValue.getHeaderRows();
	//	//int32 headerColumns= oTableValue.getHeaderColumns();

	//	PMString Str("bodyRows = ");
	//	Str.AppendNumber(bodyRows);
	//	Str.Append("  , bodyColumns = ");
	//	Str.AppendNumber(bodyColumns);
	//	Str.Append("  , headerRows = ");
	//	Str.AppendNumber(headerRows);
	//	/*Str.Append("  , headerColumns = ");
	//	Str.AppendNumber(headerColumns);
	//	CA(Str);*/			
	//	
	//	vector<SlugStruct> listOfNewTagsAdded;

	//	////Check for the tableHeaderFlag at colno
	//	int32 headerFlag = tagInfo.header;
	//	
	//	//isTranspose=kTrue;
	//	if(!isTranspose)
	//	{				
	//	//	CA("!isTranspose");
	//		//	In case of Transpose == kFalse..the table gets sprayed like 
	//		//	Header11 Header12  Header13 ...
	//		//	Data11	 Data12	   Data13
	//		//	Data21	 Data22	   Data23
	//		//	....
	//		//	....
	//		//	So first row is Header Row and subsequent rows are DataRows.
	//		
	//		//The for loop below will spray the Table Header in left to right fashion.
	//		//Note typeId == -2 for TableHeaders.
	//		if(headerFlag == 1)//Spray table with the headers.
	//		{
	//			vector<vector<PMString> >vec_tableheaders=oTableValue.getTableHeader();
	//			vector<vector<PMString> >::iterator it1;
	//			
	//			for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
	//			{
	//				
	//				vector<PMString>vec_tableHeaderRow;
	//				vec_tableHeaderRow = (*it1);
	//				vector<PMString>::iterator it2;
	//				for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
	//				{			
	//					PMString dispname = (*it2);
	//					WideString* wStr=new WideString(*it2);
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId =-1;
	//					newTagToBeAdded.typeId = -2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = objectId;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = parentTypeId;
	//					newTagToBeAdded.elementName = dispname ;
	//					newTagToBeAdded.TagName = prepareTagName("Header");
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionId ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = 0;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;					 
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);	

	//					if(it2 != vec_tableHeaderRow.end() - 1)
	//					{
	//						
	//						insertPMStringText.Append("\t");						
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);
	//					
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					/*		
	//					PMString temp;
	//					temp.AppendNumber(newTagToBeAdded.tagStartPos);
	//					temp.Append("	,	");
	//					temp.AppendNumber(newTagToBeAdded.tagEndPos);
	//					CA("insertPMStringText	"+insertPMStringText);
	//					CA("tagStartPos &	tagendpos	 "+temp);
	//					*/   
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;

	//					if(wStr)
	//						delete wStr;
	//		
	//				}
	//			}
	//		}
	//		//CA(tabbedTextData);
	//		//This for loop will spray the Item_copy_attributes value
	//		vector<TableRow> vec_tableBodyData=oTableValue.getTableBodyData();
	//		vector<TableRow>::iterator it3;
	//		int j=0;
	//			
	//		for(it3 = vec_tableBodyData.begin(); it3 != vec_tableBodyData.end(); it3++)
	//		{
	//	//		CA("vec_tableBodyData");
	//			vector<CellData> vec_CellData=(*it3).tableRowContent;
	//			PMString textToInsert("");
	//			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//			if(!iConverter)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::sprayHybridTableScreenPrintInTabbedTextForm::!iConverter");	
	//				break;					
	//			}
	//			vector<CellData>::iterator it4;
	//			for(it4 = vec_CellData.begin(); it4 != vec_CellData.end(); it4++)
	//			{
	//			//	CA("Cell Data");
	//				CellData tempCellData=(*it4);
	//				if(j>=bodyColumns)
	//					break;

	//				TagList tagList;
	//				textToInsert.Clear();					
	//				textToInsert.Clear();					
	//				tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);

	//				int32/*int64*/ tagListSize = static_cast<int32>(tagList.size());
	//				
	//				PMString insertPMStringText;
	//				insertPMStringText.Append(textToInsert);//stencileInfo.elementName);
	//				
	//				//int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//				
	//			
	//				if(it4 == vec_CellData.end()-1)
	//				{
	//					insertPMStringText.Append("\r");
	//				}
	//				else
	//				{
	//					insertPMStringText.Append("\t");
	//				}
	//				
	//				tabbedTextData.Append(insertPMStringText);
	//				int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//				if(tagList.size() == 0)
	//				{
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = -1;
	//					newTagToBeAdded.typeId = -1;
	//					newTagToBeAdded.header = -1;
	//					newTagToBeAdded.parentId = objectId;//objectId;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = parentTypeId;
	//					//newTagToBeAdded.elementName =;
	//					PMString tagName("Static_Text");
	//					newTagToBeAdded.TagName = prepareTagName(tagName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =tagInfo.whichTab;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionId;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = 0;
	//					newTagToBeAdded.row_no = -1;
	//					newTagToBeAdded.col_no=-1;
	//						
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;			
	//		
	//					j++;
	//					continue;
	//				}

	//				for(int32 k = 0; k < tagListSize; k++)
	//				{
	//					
	//					TagStruct tInfo = tagList[k];
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.whichTab   = tInfo.whichTab;						
	//					newTagToBeAdded.typeId = tInfo.typeId;
	//					newTagToBeAdded.header = -1;
	//					newTagToBeAdded.parentId = objectId;						
	//					//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//					//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//					newTagToBeAdded.elementId  = tInfo.elementId;						
	//					PMString TagName("");
	//					TagName.Append("ATTRID_");
	//					TagName.AppendNumber(newTagToBeAdded.elementId);	
	//					newTagToBeAdded.TagName = prepareTagName(TagName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID =tagInfo.languageID;
	//					newTagToBeAdded.parentTypeId = parentTypeId;
	//					newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//					newTagToBeAdded.sectionID = sectionId; 
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.col_no = tagInfo.colno;
	//					
	//					/*PMString temp("tagStartPos = ");
	//					temp.AppendNumber(tagStartPos);
	//					temp.Append(" ,	tInfo.startIndex = ");
	//					temp.AppendNumber(tInfo.startIndex);
	//					temp.Append(" ,	tInfo.endIndex = ");
	//					temp.AppendNumber(tInfo.endIndex);
	//					temp.Append(" ,	lengthOfTheString = ");
	//					temp.AppendNumber(lengthOfTheString);
	//					CA(temp);*/

	//					if(tagListSize == 1)///case : only one tag is present inside each cell 
	//					{
	//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;
	//					}
	//					else //case : multiple tags are present inside each cell
	//					{
	//	
	//						/*int32 temp = tagStartPos;
	//						tagStartPos = tagStartPos + tInfo.startIndex;
	//						tagEndPos = temp + tInfo.endIndex;
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;*/

	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						CA(temp);*/

	//						/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						
	//						tagStartPos = temp + 2;*/
	//					}
	//											
	//				}
	//				//tagStartPos = tagStartPos + lengthOfTheString;

	//							
	//			}
	//			j=0;
	//		}
	//	}	//end of if IsTranspose == kFalse	
	//	else
	//	{
	//		//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
	//			
	//		//In case of Transpose == kTrue..the table gets sprayed like 

	//		//	Header11 Data11 Data12 Data13..
	//		//	Header22 Data21 Data22 Data23..
	//		//	Header33 Data31 Data32 Data33..
	//		//	....
	//		//	....
	//		//So First Column is HeaderColumn and subsequent columns are Data columns.
	//		vector<TableRow> vec_tableBodyData=oTableValue.getTableBodyData();
	//		if(headerFlag == 1)
	//		{
	//			vector<vector<PMString> >vec_tableheaders=oTableValue.getTableHeader();
	//			
	//			int32 Rows = bodyRows + headerRows;
	//			for(int32 rowIndex = 0; rowIndex < bodyColumns ; rowIndex++)
	//			{
	//				for(int32 colIndex = 0 ; colIndex < Rows ; colIndex++)
	//				{
	//					if(colIndex < headerRows)
	//					{
 //                           ///do it for header
	//						vector<PMString> headerRowObj = vec_tableheaders[colIndex];
	//						PMString TextToInsert = headerRowObj[rowIndex];
	//						
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId =-1;
	//						newTagToBeAdded.typeId = -1;
	//						newTagToBeAdded.header = 1;
	//						newTagToBeAdded.parentId = objectId;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = parentTypeId;
	//						newTagToBeAdded.elementName = TextToInsert ;
	//						newTagToBeAdded.TagName = prepareTagName("Header");
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//						newTagToBeAdded.whichTab =4 ;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionId ;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;					 
	//						
	//						PMString insertPMStringText;
	//						insertPMStringText.Append(newTagToBeAdded.elementName);
	//						if(!iConverter)
	//						{
	//							insertPMStringText=insertPMStringText;					
	//						}
	//						else
	//							insertPMStringText=iConverter->translateString(insertPMStringText);	

	//						insertPMStringText.Append("\t");						
	//						
	//						tabbedTextData.Append(insertPMStringText);
	//						
	//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						
	//						/*PMString temp;
	//						temp.AppendNumber(newTagToBeAdded.tagStartPos);
	//						temp.Append("	,	");
	//						temp.AppendNumber(newTagToBeAdded.tagEndPos);
	//						CA("insertPMStringText	"+insertPMStringText);
	//						CA("tagStartPos &	tagendpos	 "+temp);
	//						*/   
	//						
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;
	//						continue;
	//					}	
	//					PMString textToInsert("");
	//					CellData cellDataObj =  vec_tableBodyData[colIndex - headerRows].tableRowContent[rowIndex];
	//					TagList tagList;
	//					tagList = iConverter-> translateStringForAdvanceTableCell(cellDataObj,textToInsert);
	//					int32/*int64*/ tagListSize = static_cast<int32>(tagList.size());
	//				
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(textToInsert);//stencileInfo.elementName);
	//				
	//					if(colIndex	== Rows-1)
	//					{
	//						if(rowIndex != bodyColumns - 1)
	//							insertPMStringText.Append("\r");
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\t");
	//					}
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//					if(tagList.size() == 0)
	//					{
	//						
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId = -1;
	//						newTagToBeAdded.typeId = -1;
	//						newTagToBeAdded.header = -1;
	//						newTagToBeAdded.parentId = objectId;//objectId;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = parentTypeId;
	//						//newTagToBeAdded.elementName =;
	//						PMString tagName("Static_Text");
	//						newTagToBeAdded.TagName = prepareTagName(tagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//						newTagToBeAdded.whichTab =tagInfo.whichTab;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionId;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;
	//						
	//						if(colIndex	== Rows-1)
	//						{
	//							if(rowIndex == bodyColumns - 1)
	//								tagEndPos = tagStartPos + lengthOfTheString;
	//							else
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						else
	//						{
	//							tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;			
	//			
	//						continue;
	//					}

	//					for(int32 k = 0; k < tagListSize; k++)
	//					{
	//						
	//						TagStruct tInfo = tagList[k];
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.whichTab   = tInfo.whichTab;						
	//						newTagToBeAdded.typeId = tInfo.typeId;
	//						newTagToBeAdded.header = -1;
	//						newTagToBeAdded.parentId = objectId;					
	//						//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//						//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//						newTagToBeAdded.elementId  = tInfo.elementId;						
	//						PMString TagName("");
	//						TagName.Append("ATTRID_");
	//						TagName.AppendNumber(newTagToBeAdded.elementId);	
	//						newTagToBeAdded.TagName = prepareTagName(TagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID =tagInfo.languageID;
	//						newTagToBeAdded.parentTypeId = parentTypeId;
	//						newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//						newTagToBeAdded.sectionID = sectionId; 
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.col_no = tagInfo.colno;
	//						
	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						temp.Append(" ,	lengthOfTheString = ");
	//						temp.AppendNumber(lengthOfTheString);
	//						CA(temp);*/

	//						if(tagListSize == 1)///case : only one tag is present inside each cell 
	//						{
	//							
	//							if(colIndex	== Rows-1)
	//							{
	//								if(rowIndex == bodyColumns - 1)
	//									tagEndPos = tagStartPos + lengthOfTheString;
	//								else
	//									tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							else
	//							{
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//				
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;
	//							listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							tagStartPos = tagEndPos+ 1 + 2;
	//						}
	//						else //case : multiple tags are present inside each cell
	//						{
	//		
	//							/*int32 temp = tagStartPos;
	//							tagStartPos = tagStartPos + tInfo.startIndex;
	//							tagEndPos = temp + tInfo.endIndex;
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;*/

	//							/*PMString temp("tagStartPos = ");
	//							temp.AppendNumber(tagStartPos);
	//							temp.Append(" ,	tInfo.startIndex = ");
	//							temp.AppendNumber(tInfo.startIndex);
	//							temp.Append(" ,	tInfo.endIndex = ");
	//							temp.AppendNumber(tInfo.endIndex);
	//							CA(temp);*/

	//							/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							
	//							tagStartPos = temp + 2;*/
	//						}
	//												
	//					}

	//				}
	//			}
	//		}
	//		else
	//		{
	//			for(int32 rowIndex = 0; rowIndex < bodyColumns ; rowIndex++)
	//			{
	//				for(int32 colIndex = 0 ; colIndex < bodyRows ; colIndex++)
	//				{
	//					CellData cellDataObj =  vec_tableBodyData[colIndex].tableRowContent[rowIndex];
	//					TagList tagList;
	//					PMString textToInsert("");
	//					tagList = iConverter-> translateStringForAdvanceTableCell(cellDataObj,textToInsert);
	//					int32/*int64*/ tagListSize =static_cast<int32> (tagList.size());
	//				
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(textToInsert);//stencileInfo.elementName);
	//				
	//					if(colIndex	== bodyRows - 1)
	//					{
	//						if(rowIndex != bodyColumns - 1)
	//							insertPMStringText.Append("\r");
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\t");
	//					}
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//					if(tagList.size() == 0)
	//					{
	//						
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId = -1;
	//						newTagToBeAdded.typeId = -1;
	//						newTagToBeAdded.header = -1;
	//						newTagToBeAdded.parentId = objectId;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = parentTypeId;
	//						//newTagToBeAdded.elementName =;
	//						PMString tagName("Static_Text");
	//						newTagToBeAdded.TagName = prepareTagName(tagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//						newTagToBeAdded.whichTab =tagInfo.whichTab;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionId;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;
	//						
	//						if(colIndex	== bodyRows-1)
	//						{
	//							if(rowIndex == bodyColumns - 1)
	//								tagEndPos = tagStartPos + lengthOfTheString;
	//							else
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						else
	//						{
	//							tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;			
	//			
	//						continue;
	//					}

	//					for(int32 k = 0; k < tagListSize; k++)
	//					{
	//						
	//						TagStruct tInfo = tagList[k];
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.whichTab   = tInfo.whichTab;						
	//						newTagToBeAdded.typeId = tInfo.typeId;
	//						newTagToBeAdded.header = -1;
	//						newTagToBeAdded.parentId = objectId;					
	//						//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//						//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//						newTagToBeAdded.elementId  = tInfo.elementId;						
	//						PMString TagName("");
	//						TagName.Append("ATTRID_");
	//						TagName.AppendNumber(newTagToBeAdded.elementId);	
	//						newTagToBeAdded.TagName = prepareTagName(TagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID =tagInfo.languageID;
	//						newTagToBeAdded.parentTypeId = parentTypeId;
	//						newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//						newTagToBeAdded.sectionID = sectionId; 
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.col_no = tagInfo.colno;
	//						
	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						temp.Append(" ,	lengthOfTheString = ");
	//						temp.AppendNumber(lengthOfTheString);
	//						CA(temp);*/

	//						if(tagListSize == 1)///case : only one tag is present inside each cell 
	//						{
	//							if(colIndex	== bodyRows-1)
	//							{
	//								if(rowIndex == bodyColumns - 1)
	//									tagEndPos = tagStartPos + lengthOfTheString;
	//								else
	//									tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							else
	//							{
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;
	//							listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							tagStartPos = tagEndPos+ 1 + 2;
	//						}
	//						else //case : multiple tags are present inside each cell
	//						{
	//		
	//							/*int32 temp = tagStartPos;
	//							tagStartPos = tagStartPos + tInfo.startIndex;
	//							tagEndPos = temp + tInfo.endIndex;
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;*/

	//							/*PMString temp("tagStartPos = ");
	//							temp.AppendNumber(tagStartPos);
	//							temp.Append(" ,	tInfo.startIndex = ");
	//							temp.AppendNumber(tInfo.startIndex);
	//							temp.Append(" ,	tInfo.endIndex = ");
	//							temp.AppendNumber(tInfo.endIndex);
	//							CA(temp);*/

	//							/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							
	//							tagStartPos = temp + 2;*/
	//						}
	//												
	//					}
	//		
	//				}
	//			}

	//		}
	//	}
	//					
	//	WideString insertText(tabbedTextData);	
	//	if(iConverter)
	//		iConverter->ChangeQutationMarkONOFFState(kFalse);
	//	textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//	if(iConverter)
	//		iConverter->ChangeQutationMarkONOFFState(kTrue);
	//	for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//	{					
	//		SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//		XMLReference createdElement;
	//		Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//		addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//	}			
	//}while(kFalse);
}
bool16 Refresh::doesExist(TagList &tagList)
{
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		if(refreshTableByAttribute)
			tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(selectUIDList.GetRef(i));
		else
			tList =	itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(i));

		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue++++++++++++++");
			//added by avinash
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			return kTrue;
			// till here
		}
			//added by avinash
		//CA("here");
		for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		// till here
	}
	//CA("return kFalse");
	return kFalse;
}


bool16 Refresh::fillBMSImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId)
{
	//CA(__FUNCTION__);	
	static PMString SectionSpryImagePath("");
	PMString imagePath("");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	if(imageDownLoadPath == "")
	{
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillBMSImageInBox::Interface for IClientOptions not found.");
			return kFalse;
		}
		//PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		imageDownLoadPath=ptrIClientOptions->getImageDownloadPath();
		
	}
	imagePath = imageDownLoadPath;
	
	if(imagePath!="")
	{
		const char *imageP=imagePath.GetPlatformString().c_str();
		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
				imagePath+="/";
			#else
				imagePath+="\\";
			#endif
	}
	//CA("fillBMSImageInBox 1 ");
	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP7_RefreshContenet::Refresh::fillBMSImageInBox::imagePath is blank");
		return 1;
	}
	//CA("imagePath"+imagePath);
	
	PMString fileName("");

	double mpv_value_id = -1;
	CObjectValue oVal;
	
	slugInfo.parentId = objectId;


//	if(global_project_level == 3)
//		slugInfo.sectionID = CurrentSubSectionID;
//	if(global_project_level == 2)
//		slugInfo.sectionID = CurrentSectionID;

	InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
	if(!iHier)
	{
		ptrIAppFramework->LogError("AP7_RefreshContenet::Refresh::fillBMSImageInBox::!iHier");		
		return kFalse;
	}
	
	UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
	InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
	if(!iGeo)
	{
		ptrIAppFramework->LogError("AP7_RefreshContenet::Refresh::fillBMSImageInBox::!iGeo");		
		return kFalse;
	}

	PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
	PMRect originalPageBounds;
	PMRect FirstFrameBounds = pageBounds;

	//Apsiva Comment
	/*if(slugInfo.whichTab == 3)
		fileName = ptrIAppFramework->GetAssets_getProductBMSAssetFileNamebyParentIdTypeId(objectId, slugInfo.typeId);
	if(slugInfo.whichTab == 4)
		fileName = ptrIAppFramework->GetAssets_getItemBMSAssetFileNamebyParentIdTypeId(objectId, slugInfo.typeId);*/
//	if(slugInfo.whichTab == 5)
//		fileName = ptrIAppFramework->GetAssets_getPubLogoAssetFileNamebyParentIdTypeId(CurrentPublicationID, slugInfo.typeId);
	CAssetValue partnerAssetValueObj = ptrIAppFramework->getPartnerAssetValueByPartnerIdImageTypeId(objectId, slugInfo.sectionID, slugInfo.languageID,  slugInfo.typeId, slugInfo.whichTab);
	fileName = partnerAssetValueObj.geturl();
		
	if(fileName == "")
	{
		//CA("fileName == Null......");
		ptrIAppFramework->LogError("AP7_RefreshContenet::Refresh::fillBMSImageInBox::fileName == NULL");
		return kFalse;
	}

	UIDRef newItem;	
	do
	{
		SDKUtilities::Replace(fileName,"%20"," ");
	}while(fileName.IndexOfString("%20") != -1);

	//CA(fileName);
	PMString imagePathWithSubdir = imagePath;
	PMString typeCodeString;

	//Apsiva Comment
	//if(slugInfo.whichTab == 3 || slugInfo.whichTab == 4)
	//	typeCodeString=ptrIAppFramework->GetAssets_getAssetFolderForBMSandPubLogo(objectId, slugInfo.typeId);

//	if(slugInfo.whichTab == 5)
//		typeCodeString=ptrIAppFramework->GetAssets_getAssetFolderForBMSandPubLogo(CurrentPublicationID, slugInfo.typeId);
//CA("typeCodeString = " + typeCodeString);

	if(typeCodeString.NumUTF16TextChars()!=0)
	{ 
	
		PMString TempString = typeCodeString;		
		CharCounter ABC = 0;
		bool16 Flag = kTrue;
		bool16 isFirst = kTrue;
		do
		{					
			if(TempString.Contains("/", ABC))
			{
			 	ABC = TempString.IndexOfString("/"); 				
				
		 		PMString * FirstString = TempString.Substring(0, ABC);		 		
		 		PMString newsubString = *FirstString;		 	
					 			
		 		if(!isFirst)
		 		imagePathWithSubdir.Append("\\");
				
		 		imagePathWithSubdir.Append(newsubString);	
				//if(ptrIAppFramework->getAssetServerOption() == 0)
				// 		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
		 		isFirst = kFalse;					 		
		 		PMString * SecondString = TempString.Substring(ABC+1);
		 		PMString SSSTring =  *SecondString;		 		
		 		TempString = SSSTring;	
				if(FirstString)
					delete FirstString;

				if(SecondString)
					delete SecondString;

			}
			else
			{				
				if(!isFirst)
		 		imagePathWithSubdir.Append("\\");
				
		 		imagePathWithSubdir.Append(TempString);	
				//if(ptrIAppFramework->getAssetServerOption() == 0)
			 	//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
		 		isFirst = kFalse;				
				Flag= kFalse;
				
				//CA(" 2 : "+ imagePathWithSubdir);
			}				
				
		}while(Flag);

	}
	
	//if(ptrIAppFramework->getAssetServerOption() == 0)
	//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
    #ifdef Windows
        SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
    #endif
		//CA("imagePathWithSubdir = " + imagePathWithSubdir);

	//if(ptrIAppFramework->getAssetServerOption() == 0)
	//{
	//	if(slugInfo.whichTab == 3)
	//	{
	//		if(!ptrIAppFramework->GETAsset_downloadProductBMSAsset(objectId, slugInfo.typeId,imagePathWithSubdir))
	//		{
	//			ptrIAppFramework->LogDebug("AP7_RefreshContenet::Refresh::fillBMSImageInBox::!GETAsset_downloadProductBMSAsset");				
	//			return 1;
	//		}
	//	}
	//	if(slugInfo.whichTab == 4)
	//	{
	//		if(!ptrIAppFramework->GETAsset_downloadItemBMSAsset(objectId, slugInfo.typeId,imagePathWithSubdir))
	//		{
	//			ptrIAppFramework->LogDebug("AP7_RefreshContenet::Refresh::fillBMSImageInBox::!GETAsset_downloadItemBMSAsset");				
	//			return 1;
	//		}
	//	}
	///*	if(slugInfo.whichTab == 5)
	//	{
	//		if(!ptrIAppFramework->GETAsset_downloadPubLogoAsset(CurrentPublicationID, slugInfo.typeId,imagePathWithSubdir))
	//		{
	//			ptrIAppFramework->LogDebug("AP7_RefreshContent::CDataSprayer::fillBMSImageInBox::!GETAsset_downloadPubLogoAsset");				
	//			return 1;
	//		}
	//	}*/
	//	
	//}

	/*if(!fileExists(imagePathWithSubdir,fileName))	
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContenet::Refresh:: fillBMSImageInBox: Image Not Found on Local Drive");
		return 1; 
	}*/
	PMString total=imagePathWithSubdir+fileName;	
	//CA(total);
    #ifdef MACINTOSH
        SDKUtilities::convertToMacPath(total);
    #endif
		
	originalPageBounds = pageBounds;
	newItem = boxUIDRef;

	if(ImportFileInFrame(newItem, total))
	{									
		//fitImageInBox(newItem);						
	}
	
			
	return 1;
}





















UIDRef Refresh::createNewTableInsideTextFrame(UIDRef &textframeUIDRef,UIDRef& outtableUIDref,int32 reportType)
{
	//CA("Refresh::createNewTableInsideTextFrame");
	static int32 isFirstTime =1;
	InterfacePtr<ITableModel> gTableModel;
	UIDRef resReportDocRef = UIDRef::gNull;
	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		SDKLayoutHelper sdklhelp;
		SDKUtilities sdkUtilityObj;

		PMString FileName("");
	
		UIDRef reportDocRef = sdklhelp.CreateDocument();

	
		ErrorCode result1 = kFailure;
		result1 = sdklhelp.OpenLayoutWindow(reportDocRef);
		resReportDocRef  =reportDocRef ;
		PMReal left = 0.0;
		PMReal right = 551.0;
		PMReal top = 0.0;
		PMReal bottom = 712.0;

		InterfacePtr<ILayoutControlData>layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); 
		if(!layoutControlData)
		{
			//CA("!layoutControlData");
			return resReportDocRef;
		}
		UIDRef boxuidRef = UIDRef::gNull;
		BookReportData bd;
		boxuidRef = bd.CreateTextFrame(layoutControlData,PMRect(left,top,right,bottom));
		textframeUIDRef =  boxuidRef;
		bd.selectFrame(boxuidRef);
		
		//************
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxuidRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");			
			return resReportDocRef;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");			
			return resReportDocRef;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");			
			return resReportDocRef;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");			
			return resReportDocRef;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");			
			return resReportDocRef;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			//CA("!textModel" );			
			return resReportDocRef;
		}
		TextIndex startIndex = frameItemTFC->TextStart();
		TextIndex finishIndex = frameItemTFC->TextSpan()-1;

		RangeData range(finishIndex,finishIndex,nil); 
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(iSelectionManager) 
		{ 
			InterfacePtr<ITextSelectionSuite> textSelectionSuite(iSelectionManager, UseDefaultIID()); 
			if (textSelectionSuite) 
			{ 
				textSelectionSuite->SetTextSelection( 
				::GetUIDRef(textModel), 
				range, 
				Selection::kScrollIntoView, nil);
			} 
		}
		
		//*****Creating the new Table
		//CA("Creating First Table..");
		UIDRef tableUIDRef;
		if(reportType == 0){
			//CA("reportType == 0");
			 tableUIDRef = bd.CreateTable(boxuidRef,startIndex,3,3,20.0,70,kTextContentType,"" ,"",0); //for Upadet
		}
		else if(reportType == 1){
			//CA("reportType == 1");
			tableUIDRef = bd.CreateTable(boxuidRef,startIndex,3,3,20.0,70,kTextContentType,"" ,"",1); //For New
		}
		else {
			//CA("reportType == 2");
			tableUIDRef = bd.CreateTable(boxuidRef,startIndex,3,3,20.0,70,kTextContentType,"" ,"",2); //For Delets			
		}
		outtableUIDref = tableUIDRef;
		Utils<ITableUtils> tableUtils;
		if (!tableUtils) {
			//CA("tableUtils == nil");
			return resReportDocRef;
		}

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			//CA("Table Model List is Null");
			break;
		}
		int32	tableIndex = tableList->GetModelCount() - 1;

//PMString asf("111TAble Index = ");
//asf.AppendNumber(tableIndex);
//CA(asf);

		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			//CA("No Table in Frame");
			break;
		}
		/*else
			CA("Table Index is > 0");*/

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex)/*tableUIDRef, UseDefaultIID()*/);
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			return resReportDocRef;
		}
		gTableModel =	tableModel;
		//tableModel->AddRef();
	
		if(tableUIDRef != UIDRef::gNull && reportType == 0) //** For Update report
		{	
			//CA("tableUIDRef != UIDRef::gNull"); //rowData.oneRowData.addEmptyRow
			vector<vector<BookReportData> >::iterator itrEntireReport;
			for(itrEntireReport = BookReportData::entireReport.begin();itrEntireReport !=BookReportData::entireReport.end();itrEntireReport++ )
			{
				BookReportData::vecReportRows = *itrEntireReport;
				vector<BookReportData>::iterator	itrRowData;
				for(itrRowData =BookReportData::vecReportRows.begin();itrRowData!=BookReportData::vecReportRows.end() ;itrRowData++)
				{
				
					BookReportData rows = (*itrRowData);
					ReportRowData  oneRow = rows.oneRawData;
					
					//*****				
					//if(0 && rows.isNewSection)  //**** Dummy Zero Check to Avoid Creating New Table On Nwe Section
					//{						
					//	TextIndex tableStartIndex = frameItemTFC->TextSpan();
					//	
					//	int32 noofChar = textModel->GetPrimaryStoryThreadSpan();

					//	RangeData rd =textModel->GetStoryThreadRange(tableStartIndex);

					//	WideString* wStrcarriageReturn=new WideString("\r");
					//	textModel->Insert(tableStartIndex,wStrcarriageReturn);									
					//	delete wStrcarriageReturn;
					//	
					//	tableStartIndex = frameItemTFC->TextSpan();

					//	//CA("Second Table..");				
					//	UIDRef newtableUIDRef = bd.CreateTable(boxuidRef,tableStartIndex-1);	
					//	
					//	TextIndex endIndex = textModel->GetStoryThreadRange(tableStartIndex-1).End();

					//	InterfacePtr<ITableModelList> tableList1(textModel, UseDefaultIID());
					//	if(tableList1==NULL)
					//	{
					//		//CA("11Table Model List is Null");
					//		break;
					//	}
					//	int32	tableIndex1 = tableList->GetModelCount() - 1;

					//	/*PMString asf("22222TAble Index = ");
					//	asf.AppendNumber(tableIndex1);
					//	CA(asf);	*/				
					//	if(tableIndex1<0) //This check is very important...  this ascertains if the table is in box or not.
					//	{
					//		CA("11No Table in Frame");
					//		break;
					//	}
					//	/*else
					//		CA("111Table Index is > 0");*/
					//	
					//	
					//	InterfacePtr<ITableModel> tableModel1(tableList1->QueryNthModel(tableIndex1)/*newtableUIDRef,UseDefaultIID()*/);
					//	if (tableModel1 == NULL)
					//	{
					//		CA("Err:1 invalid interface pointer ITableFrame");
					//		return resReportDocRef;
					//	}
					//	tableModel1->AddRef();
					//	gTableModel =	tableModel1;
					//	
					//	InterfacePtr<ITableCommands> tableCommands1(gTableModel/*tableModel1*/, UseDefaultIID());
					//	if(tableCommands1==NULL)
					//	{
					//		CA("Err:1 invalid interface pointer ITableCommands");
					//		return resReportDocRef;
					//	}	
					//	
					//	tableUIDRef.ResetUID(newtableUIDRef.GetUID());							
					//		break;											
					//}
										
					
					bool16 status = 	BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
												
					if(!oneRow.isImage)
					{	
						//CA("  !oneRow.isImage   ");
						InterfacePtr<ITableCommands> tableCommands(gTableModel, UseDefaultIID());
						if(tableCommands==NULL)
						{
							//CA("Err:1323 invalid interface pointer ITableCommands");
							return resReportDocRef;
						}	
						if(rows.addEmptyRow == kTrue || BookReportData::currntRowIndex == 3){
						//	CA("going to addaddEmptyRow row");
							if(BookReportData::currntRowIndex != 3)
							{
								BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
								BookReportData::currntRowIndex++;
							}
							BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);	

							tableCommands->SetCellText(WideString("Item No "), GridAddress(BookReportData::currntRowIndex,0 ));

							//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(oneRow.itemID,BookReportData::langaugeID);

							CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(BookReportData::currentSection.GetAsNumber(), oneRow.itemID, 0, BookReportData::langaugeID);
							CItemModel itemModelObj =  pbObjValue.getItemModel();

							tableCommands->SetCellText(WideString(itemModelObj.getItemNo()), GridAddress(BookReportData::currntRowIndex,1 ));
							BookReportData::currntRowIndex++;
						}
						for(int32 colIndex = 0;colIndex < 3;colIndex++)
						{							
							if(colIndex == 0){  //**FieldName
								WideString* wStrcolData=new WideString(oneRow.fieldName);
								//CA("oneRow.fieldName = "+oneRow.fieldName);
								tableCommands->SetCellText(*wStrcolData, GridAddress(BookReportData::currntRowIndex,colIndex ));	
								//delete wStrcolData;//Amit 18MAY09
							}
							else if(colIndex == 1){ //**Document Value
								WideString* wStrcolData=new WideString(oneRow.documentValue);
								//CA("oneRow.documentValue = "+oneRow.documentValue);
								tableCommands->SetCellText(*wStrcolData, GridAddress(BookReportData::currntRowIndex,colIndex ));	
								//delete wStrcolData;//Amit 18MAY09
							}
							else if(colIndex == 2){  //**ONEsource Value
								WideString* wStrcolData=new WideString(oneRow.oneSourceValue);
								//CA("oneRow.oneSourceValue = "+oneRow.oneSourceValue);
								tableCommands->SetCellText(*wStrcolData, GridAddress(BookReportData::currntRowIndex,colIndex ));	
								//delete wStrcolData;//Amit 18MAY09
							}						
						}	
					}
					else
					{
						//CA("oneRow.isImage == kTrue");
						InterfacePtr<ITableCommands> tableCommands(gTableModel, UseDefaultIID());
						if(tableCommands==NULL)
						{
							//CA("Err:1323 invalid interface pointer ITableCommands");
							ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::createNewTableInsideTextFrame::tableCommands == NULL");
							return resReportDocRef;
						}	
						//CA("Image Found ");
						if(rows.addEmptyRow == kTrue || BookReportData::currntRowIndex == 3){
							//CA("rows.addEmptyRow == kTrue || BookReportData::currntRowIndex == 3");
							if(BookReportData::currntRowIndex != 3)
							{
								//CA("BookReportData::currntRowIndex != 3");
								BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
								BookReportData::currntRowIndex++;
							}
							BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);									
							tableCommands->SetCellText(WideString("Item No"), GridAddress(BookReportData::currntRowIndex,0 ));

							//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(oneRow.itemID,BookReportData::langaugeID);
							CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(BookReportData::currentSection.GetAsDouble(), oneRow.itemID, 0, BookReportData::langaugeID);
							CItemModel itemModelObj =  pbObjValue.getItemModel();

							tableCommands->SetCellText(WideString(itemModelObj.getItemNo()), GridAddress(BookReportData::currntRowIndex,1 ));
							BookReportData::currntRowIndex++;

						}
						for(int32 colIndex = 0;colIndex < 3;colIndex++){					
							if(colIndex == 0)
							{
								WideString* wStrcolData=new WideString(oneRow.fieldName);
								//CA("oneRow.fieldName = "+oneRow.fieldName);
								tableCommands->SetCellText(*wStrcolData, GridAddress(BookReportData::currntRowIndex,colIndex ));	
								delete wStrcolData;
							}
							else if(colIndex == 1)
							{			
								BookReportData::MoveCursorAtPosition(tableUIDRef,BookReportData::currntRowIndex,colIndex);
								PMString imagePath(oneRow.documentValue);
								do
								{
									SDKUtilities::Replace(imagePath,"%20"," ");
								}while(imagePath.IndexOfString("%20") != -1);
								BookReportData::CreatePictureBoxInCurrentCell(BookReportData::currntRowIndex,1,imagePath);
							}
							else if(colIndex == 2)
							{
								BookReportData::MoveCursorAtPosition(tableUIDRef,BookReportData::currntRowIndex,colIndex);							
								PMString imagePath(oneRow.oneSourceValue);
								do
								{
									SDKUtilities::Replace(imagePath,"%20"," ");
								}while(imagePath.IndexOfString("%20") != -1);
								BookReportData::CreatePictureBoxInCurrentCell(BookReportData::currntRowIndex,2,imagePath);
							}						
						}					
					}

					BookReportData::currntRowIndex++;	
					isFirstTime++;
				}	 //*** End of Inner Loop
			}//****End of Outer Loop...
//CA("After Spraying.");
			/*ThreadTextFrame th;
			if(th.IsTextFrameOverset(frameItemTFC))
			{				
				th.DoCreateAndThreadTextFrame(boxuidRef);						
			}*/			
			BookReportData::selectTableBodyColumns(tableModel);

			InterfacePtr<ITextAttributeSuite> textAttributeSuite((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));	
			if (textAttributeSuite == nil)
			{
				//CA("textAttributeSuite == nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::createNewTableInsideTextFrame::textAttributeSuite == NULL");
				break;
			}
			textAttributeSuite->ToggleBold();
			//CA("Break");
			//eak;
			//CA("After Spraying. ToggleBold");
			
			//CA("Start Font Stting");
			BookReportData::selectTableBodyColumns(tableModel,0,3);
			BookReportData::ChangeFontSizeOfSelectedTextInFrame(10);
//			BookReportData::setFontForSelectedTable(tableUIDRef,tableModel,"MyriadPro-Cond");
			//CA("End Of Font");

		}	
	}while(kFalse);
	return resReportDocRef;
}


 bool16 Refresh::getStencilInfo(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo)
{
	//CA("Refresh::getStencilInfo");
	bool16 result = kFalse;

	sectionIds.clear();
	MapForItemsPerSection.clear();
	MapForProductsPerSection.clear();
	bool16 isSprayItemPerFrameFlag = kFalse;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return result;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinderPalete::SubSectionSprayer::getStencilInfo:Pointer to DataSprayerPtr not found");//
		return result;
	}

	do
	{

		double prevSectionId = -1;
		sectionIds.clear();
		for(int32 index = 0; index < SelUIDList.Length(); index++)
		{
			UIDRef boxUIDRef = SelUIDList.GetRef(index);
			
			TagList tList=itagReader->getTagsFromBox_ForRefresh(boxUIDRef); //getTagsFromBox
			
			int numTags=static_cast<int>(tList.size());
			if(numTags<0)
			{
				//CA("numTags<0");
				//return result;
			}

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			/*PMString numOfTags("numTags = ");
			numOfTags.AppendNumber(numTags);
			CA(numOfTags);*/

			if(numTags<=0)//This can be a Tagged Frame
			{	
				tList=itagReader->getFrameTags(boxUIDRef);						
			}	

			int numTags1=static_cast<int>(tList.size());
			if(numTags1<=0)
			{
				//CA("numTags1<0");
				continue;
			}

			bool16 isCustomTablePresent = kFalse;
			TagList tableTagList1;
			

			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];
				

				if(tagInfo.sectionID == -1 || tagInfo.parentId == -1){
					continue;
				}
				
				objCSprayStencilInfo.langId = tagInfo.languageID;

				if(prevSectionId != tagInfo.sectionID)
				{
					bool16 sectionIdAlreadyExist = kFalse;

					UniqueIds::iterator itr;
					itr = sectionIds.find(tagInfo.sectionID);
					if(itr == sectionIds.end())
					{
						if(tagInfo.sectionID > 0)
						sectionIds.insert(tagInfo.sectionID);
					}
					else
					{
						sectionIdAlreadyExist = kTrue;
					}
					
					if(!sectionIdAlreadyExist)
					{
						GetMultipleSectionData ObjGetMultipleSectionData;
						ObjGetMultipleSectionData.sectionId = tagInfo.sectionID;
						ObjGetMultipleSectionData.languageId = tagInfo.languageID;

						CPubModel  cPubModelObj = ptrIAppFramework->getpubModelByPubID(tagInfo.sectionID,tagInfo.languageID);
						ObjGetMultipleSectionData.publicationId = cPubModelObj.getRootID();	
						
						vec_ptr->push_back(ObjGetMultipleSectionData);

					}

					prevSectionId = tagInfo.sectionID;
				}	

				/*if(CurrentSelectedDocumentSectionID == -1 && tagInfo.sectionID != -1)
							CurrentSelectedDocumentSectionID = tagInfo.sectionID;

				if(CurrentSelectedDocumentLanguageID == -1 && tagInfo.languageID != -1)
							CurrentSelectedDocumentLanguageID = tagInfo.languageID;*/

				if(tagInfo.whichTab == 4)
				{					
					multiSectionMap::iterator itr;
					UniqueIds::iterator itemIdsItr;
					itr = MapForItemsPerSection.find(tagInfo.sectionID);
					if(itr != MapForItemsPerSection.end())
					{
						if(tagInfo.imgFlag == 1 && tagInfo.isSprayItemPerFrame != -1 && (tagInfo.parentId != tagInfo.isAutoResize))
						{
							// this image frame is of child item in SprayItemPerFrame.In this case we have parentItemId in isAutoResize.
							itemIdsItr = itr->second.find(tagInfo.isAutoResize);
							if(itemIdsItr == itr->second.end())
							{
								itr->second.insert(tagInfo.isAutoResize);
							}
							isSprayItemPerFrameFlag = kTrue;
						}						
						else
						{
							itemIdsItr = itr->second.find(tagInfo.parentId);
							if(itemIdsItr == itr->second.end())
							{
								itr->second.insert(tagInfo.parentId);
							}
						}
					}
					else
					{
						/*if(tagInfo.imgFlag == 1 && tagInfo.isSprayItemPerFrame != -1 && (tagInfo.parentId != tagInfo.isAutoResize))
						{
							itemIdsItr = itr->second.find(tagInfo.isAutoResize);
							if(itemIdsItr == itr->second.end())
							{
								itr->second.insert(tagInfo.isAutoResize);
							}
						}						
						else
						{*/
						
							UniqueIds itemIds;
							itemIds.insert(tagInfo.parentId);
							

							MapForItemsPerSection.insert(multiSectionMap::value_type(tagInfo.sectionID, itemIds));
						//}
					}

					/*bool16 itemAlreadyPresent = kFalse;
					for(int32 itemIndex = 0; itemIndex < itemIds.size(); itemIndex++)
					{
						if(itemIds[itemIndex] == tagInfo.parentId)
						{
							itemAlreadyPresent = kTrue;
							break;
						}
					}

					if(!itemAlreadyPresent)
						itemIds.push_back(tagInfo.parentId);*/
				}

				if(tagInfo.whichTab == 3)
				{
					multiSectionMap::iterator itr;
					UniqueIds::iterator productIdsItr;

					itr = MapForProductsPerSection.find(tagInfo.sectionID);
					if(itr != MapForProductsPerSection.end())
					{
						productIdsItr = itr->second.find(tagInfo.parentId);
						if(productIdsItr == itr->second.end())
							itr->second.insert(tagInfo.parentId);
					}
					else
					{
						UniqueIds productIds;
						productIds.insert(tagInfo.parentId);

						MapForProductsPerSection.insert(multiSectionMap::value_type(tagInfo.sectionID, productIds));
					}

					/*bool16 productAlreadyPresent = kFalse;
					for(int32 pIndex = 0; pIndex < productIds.size(); pIndex++)
					{
						if(productIds[pIndex] == tagInfo.parentId)
						{
							productAlreadyPresent = kTrue;
							break;
						}
					}

					if(!productAlreadyPresent)
						productIds.push_back(tagInfo.parentId);*/
				}

				

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
					objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
					isSprayItemPerFrameFlag = kTrue;
				}


				if(tagInfo.isTablePresent || tagInfo.typeId == -5 || (tagInfo.tableType == 1 || tagInfo.tableType == 2 || tagInfo.tableType == 3 || tagInfo.tableType == 9))
				{
					
					if(tagInfo.whichTab == 3 && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId >0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
						

						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}
						for(int32 tagIndex = 0 ; tagIndex < tableTagList.size() ; tagIndex++)
						{
							tableTagList[tagIndex].tagPtr->Release();
						}
						//PMString numOfTags("numTags 33333333333= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 tableTagListIndex = 0; tableTagListIndex < numTags; tableTagListIndex++)
						{
							TagStruct & tagInfoo = tableTagList[tableTagListIndex];
							tableTagList1.push_back(tagInfoo);
						}	
						
					}
					else if(tagInfo.whichTab == 4  && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;

						objCSprayStencilInfo.isDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}
						
						for(int32 tagIndex = 0 ; tagIndex < tableTagList.size() ; tagIndex++)
						{
							tableTagList[tagIndex].tagPtr->Release();
						}
						//PMString numOfTags("numTags 2222222222= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 tableTagListIndex = 0; tableTagListIndex < numTags; tableTagListIndex++)
						{
							TagStruct & tagInfoo = tableTagList[tableTagListIndex];
							tableTagList1.push_back(tagInfoo);
						}
						

					}
                    else if(tagInfo.whichTab == 4  && (tagInfo.tableType == 9 || tagInfo.dataType == 6))
                    {
                        if( (tagInfo.elementId > 0) && (tagInfo.dataType == 6) ) // Attribute Group
                        {
                            /*
                            vector<double> attrributeList;
                            bool16 isRefreshCall = kFalse; //kTrue;
                            if(!(tagInfo.groupKey).empty())
                            {
                                ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupKey");
                                attrributeList= ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKey(tagInfo.groupKey, isRefreshCall);
                            }
                            else
                            {
                                ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupId");
                                attrributeList= ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupId(tagInfo.elementId, isRefreshCall);
                            }
                            for(int32 ct=0; ct < attrributeList.size(); ct++)
                                objCSprayStencilInfo.itemAttributeIds.push_back(attrributeList.at(ct));
                            */
                            objCSprayStencilInfo.itemAttributeGroupIds.push_back(tagInfo.elementId );
                        }
                    }
                    
					else
					{
                        if (tagInfo.tableType == 3) //advance table.
                        {
                            objCSprayStencilInfo.isHyTable = kTrue;
                        }
                        
						if(refreshTableByAttribute)
						{
							TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
							int numTags=static_cast<int>(tableTagList.size());
							if(numTags<0)
							{
								//CA("numTags<0");
								continue;
							}

							for(int32 tagIndex = 0 ; tagIndex < tableTagList.size() ; tagIndex++)
							{
								tableTagList[tagIndex].tagPtr->Release();
							}
							//PMString numOfTags("numTags 33333333333= ");
							//numOfTags.AppendNumber(numTags);
							//CA(numOfTags);

							for(int32 tableTagListIndex = 0; tableTagListIndex < numTags; tableTagListIndex++)
							{
								TagStruct & tagInfoo = tableTagList[tableTagListIndex];
								tableTagList1.push_back(tagInfoo);
							}
							// ??
							

						}

					}
					
					continue;
				}
			}

			
			if(isCustomTablePresent || refreshTableByAttribute)
			{

				int numTags=static_cast<int32>(tableTagList1.size());
				if(numTags > 0)
				{
					//CA("numTags<0");
					PMString numOfTags("numTags = ");
					numOfTags.AppendNumber(numTags);
					//CA(numOfTags);

					for(int32 tableTagList1Index = 0; tableTagList1Index < numTags; tableTagList1Index++)
					{
						TagStruct & tagInfoo = tableTagList1[tableTagList1Index];
						tList.push_back(tagInfoo);
					}	
									
					int numTagsnew=static_cast<int>(tList.size());
					if(numTagsnew < 0)
					{
						//CA("numTags<0");
						
					}

					PMString numOfTagss("numTags 11111111111= ");
					numOfTagss.AppendNumber(numTagsnew);
					//CA(numOfTagss);
				}
			}


			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];

				if(tagInfo.sectionID == -1 || tagInfo.parentId == -1){
					continue;
				}
				objCSprayStencilInfo.langId = tagInfo.languageID;
				/*if(CurrentSelectedDocumentSectionID == -1 && tagInfo.sectionID != -1)
							CurrentSelectedDocumentSectionID = tagInfo.sectionID;

				if(CurrentSelectedDocumentLanguageID == -1 && tagInfo.languageID != -1)
							CurrentSelectedDocumentLanguageID = tagInfo.languageID;*/

				if(isSprayItemPerFrameFlag == kFalse)
				{
					if(tagInfo.isSprayItemPerFrame > 0)
					{
						objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
						isSprayItemPerFrameFlag = kTrue;
					}					
				}
                
                if (tagInfo.elementId == -975 || tagInfo.elementId == -974 || tagInfo.elementId == -973 || tagInfo.elementId == -972 || tagInfo.elementId == -971 || tagInfo.tableType == 3) //advance table.
                {
                    objCSprayStencilInfo.isHyTable = kTrue;
                }

				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.elementId == -115 && tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 5)
					{
						objCSprayStencilInfo.isSectionLevelHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);						
					}
					else if(tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
                        
                        if(tagInfo.childTag != 1)
                        {
                            multiSectionMap::iterator itr;
                            UniqueIds::iterator itemIdsItr;
                            itr = MapForItemsPerSection.find(tagInfo.sectionID);
                            if(itr != MapForItemsPerSection.end())
                            {
                                itemIdsItr = itr->second.find(tagInfo.parentId);
                                if(itemIdsItr == itr->second.end())
                                {
                                    itr->second.insert(tagInfo.parentId);
                                }
                                
                            }
                            else
                            {				
                                UniqueIds itemIds;
                                itemIds.insert(tagInfo.parentId);
                                MapForItemsPerSection.insert(multiSectionMap::value_type(tagInfo.sectionID, itemIds));
                                
                            }
                        }
					}

					if(tagInfo.whichTab == 3)
						objCSprayStencilInfo.isProductCopy = kTrue;
						
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
					
					if(tagInfo.childTag == 1)
						objCSprayStencilInfo.isChildTag = kTrue;

					if(tagInfo.isEventField == 1)
						objCSprayStencilInfo.isEventField = kTrue;


					continue;
				}

				if(tagInfo.imgFlag == 1)
				{
					//CA("image Present");
					//objCSprayStencilInfo.isAsset = kTrue;
					
					if(tagInfo.typeId > 0)
						objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
					
					if(tagInfo.whichTab == 3)
					{
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.ProductAssetIds.push_back(tagInfo.typeId);
						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isProductBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isProductPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.productPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isProductAsset = kTrue;

					}
					else if(tagInfo.whichTab == 4)
					{
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.itemAssetIds.push_back(tagInfo.typeId);
						objCSprayStencilInfo.isProductChildTag = kTrue;

						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isItemPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.itemPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									if(tagInfo.typeId > 0)
										objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								if(tagInfo.typeId > 0)
								 objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isAsset = kTrue;

					}
					else if(tagInfo.whichTab == 5)
					{
						if(tagInfo.typeId == -222 || tagInfo.typeId == -223)
							objCSprayStencilInfo.isSectionLevelBMSAssets = kTrue;
						else if(tagInfo.colno == -28)
						{
							objCSprayStencilInfo.isSectionPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.sectionPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno == -27)
						{
							objCSprayStencilInfo.isPublicationPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.publicationPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno > 0)
						{
							objCSprayStencilInfo.isCatagoryPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.catagoryPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
						}

					}	

					continue;
				}
				
				//CA("copy Attribute");
				if(tagInfo.elementId > 0)
				objCSprayStencilInfo.AttributeIds.push_back(tagInfo.elementId);

				if(tagInfo.whichTab == 3)
				{
					objCSprayStencilInfo.isProductCopy = kTrue;
					if(tagInfo.elementId >0)
					objCSprayStencilInfo.ProductAttributeIds.push_back(tagInfo.elementId);
				}
				if(tagInfo.whichTab == 4)
				{
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
                    
                    if( (tagInfo.elementId > 0) && (tagInfo.dataType == 6) ) // Attribute Group
                    {
                        /*
                        vector<double> attrributeList;
                        bool16 isRefreshCall = kFalse; //kTrue;
                        if(!(tagInfo.groupKey).empty())
                        {
                            ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupKey");
                            attrributeList= ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKey(tagInfo.groupKey, isRefreshCall);
                        }
                        else
                        {
                            ptrIAppFramework->LogDebug("Getting getItemAttributeListforAttributeGroupId");
                            attrributeList= ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupId(tagInfo.elementId, isRefreshCall);
                        }
                        for(int32 ct=0; ct < attrributeList.size(); ct++)
                            objCSprayStencilInfo.itemAttributeIds.push_back(attrributeList.at(ct));
                         */
                        objCSprayStencilInfo.itemAttributeGroupIds.push_back(tagInfo.elementId);
                    }
                    else if(tagInfo.elementId >0)
						objCSprayStencilInfo.itemAttributeIds.push_back(tagInfo.elementId);
					else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
					{
						double eventPriceId = -1;
						double regularPriceId = -1;	
						eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
						regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");	
						objCSprayStencilInfo.itemAttributeIds.push_back(eventPriceId);
						objCSprayStencilInfo.itemAttributeIds.push_back(regularPriceId);
					}
                    
                    multiSectionMap::iterator itr;
                    UniqueIds::iterator itemIdsItr;
                    itr = MapForItemsPerSection.find(tagInfo.sectionID);
                    if(itr != MapForItemsPerSection.end())
                    {
                        itemIdsItr = itr->second.find(tagInfo.parentId);
                        if(itemIdsItr == itr->second.end())
                        {
                            itr->second.insert(tagInfo.parentId);
                        }
                    }
                    else
                    {
                        UniqueIds itemIds;
                        itemIds.insert(tagInfo.parentId);
                        MapForItemsPerSection.insert(multiSectionMap::value_type(tagInfo.sectionID, itemIds));
                        
                    }
                    
                    
				}
				if(tagInfo.whichTab == 5)
					objCSprayStencilInfo.isSectionCopy = kTrue;
				

				/*if(!isSpreadBasedLetterKeys)
				{
					if(tagInfo.elementId == -803 && tagInfo.typeId == 1)
					{
						isSpreadBasedLetterKeys = kTrue;
					}
					else 
					{
						isSpreadBasedLetterKeys = kFalse;
					}
				}*/

				if(tagInfo.childTag == 1)
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCopy = kTrue;
					if(tagInfo.elementId >0)
						objCSprayStencilInfo.childItemAttributeIds.push_back(tagInfo.elementId);
					else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
					{
						double eventPriceId = -1;
						double regularPriceId = -1;	
						eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
						regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");	
						objCSprayStencilInfo.childItemAttributeIds.push_back(eventPriceId);
						objCSprayStencilInfo.childItemAttributeIds.push_back(regularPriceId);
					}

					if(refreshTableByAttribute)
					{
						multiSectionMap::iterator itr;
						UniqueIds::iterator itemIdsItr;
						itr = MapForItemsPerSection.find(tagInfo.sectionID);
						if(itr != MapForItemsPerSection.end())
						{
								itemIdsItr = itr->second.find(tagInfo.childId);
								if(itemIdsItr == itr->second.end())
								{
									itr->second.insert(tagInfo.childId);
								}
							
						}
						else
						{				
							UniqueIds itemIds;
							itemIds.insert(tagInfo.childId);
							MapForItemsPerSection.insert(multiSectionMap::value_type(tagInfo.sectionID, itemIds));
							
						}

					}
				}

				if(tagInfo.isEventField == 1)
					objCSprayStencilInfo.isEventField = kTrue;

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
					objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
				}
				

			}

			/*for(int32 tagIndex = 0 ; tagIndex < tableTagList1.size() ; tagIndex++)
			{
				tableTagList1[tagIndex].tagPtr->Release();
			}*/

			//------------
			/*for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}*/
		}
	}while(kFalse);
	result = kTrue;
	return result;
}
bool16 Refresh::getStencilInfoNewOption(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo)
{
	//CA("SubSectionSprayer::getStencilInfo");
	bool16 result = kFalse;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return result;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinderPalete::SubSectionSprayer::getStencilInfo:Pointer to DataSprayerPtr not found");//
		return result;
	}


	do
	{
		for(int32 index = 0; index < SelUIDList.Length(); index++)
		{
			UIDRef boxUIDRef = SelUIDList.GetRef(index);
			
			TagList tList=itagReader->getTagsFromBox(boxUIDRef);
			int numTags=static_cast<int>(tList.size());
			if(numTags<0)
			{
				//CA("numTags<0");
			}

			/*PMString numOfTags("numTags = ");
			numOfTags.AppendNumber(numTags);
			CA(numOfTags);*/

			if(numTags<=0)//This can be a Tagged Frame
			{	
				tList=itagReader->getFrameTags(boxUIDRef);				
						
			}	

			int numTags1=static_cast<int>(tList.size());
			if(numTags1<0)
			{
				//CA("numTags1<0");
				continue;
			}

			bool16 isCustomTablePresent = kFalse;
			TagList tableTagList1;
			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
				}
				
				
				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.whichTab == 3 && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId != -1)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
						
						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int32 numTags=static_cast<int>(tableTagList.size());
						if(numTags < 0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 33333333333= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 index1 = 0; index1 < numTags; index1++)
						{
							TagStruct & tagInfoo = tableTagList[index1];
							tableTagList1.push_back(tagInfoo);
						}

						for(int32 tagIndex = 0 ; tagIndex < tableTagList.size() ; tagIndex++)
						{
							tableTagList[tagIndex].tagPtr->Release();
						}
					}
					else if(tagInfo.whichTab == 4  && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;

						objCSprayStencilInfo.isDBTable = kTrue;
						objCSprayStencilInfo.isCustomTablePresent = kTrue;
						if(tagInfo.typeId != -1)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 2222222222= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 index1 = 0; index1 < numTags; index1++)
						{
							TagStruct & tagInfoo = tableTagList[index1];
							tableTagList1.push_back(tagInfoo);
						}	
						for(int32 tagIndex = 0 ; tagIndex < tableTagList.size() ; tagIndex++)
						{
							tableTagList[tagIndex].tagPtr->Release();
						}
					}

					
					continue;
				}
			}
			if(isCustomTablePresent)
			{
				int numTags=static_cast<int32>(tableTagList1.size());
				if(numTags > 0)
				{
					//CA("numTags<0");
					PMString numOfTags("numTags = ");
					numOfTags.AppendNumber(numTags);
					//CA(numOfTags);

					for(int32 index2 = 0; index2 < numTags; index2++)
					{
						TagStruct & tagInfoo = tableTagList1[index2];
						tList.push_back(tagInfoo);
					}	
									
					int numTagsnew=static_cast<int>(tList.size());
					if(numTagsnew < 0)
					{
						//CA("numTags<0");
						
					}

					PMString numOfTagss("numTags 11111111111= ");
					numOfTagss.AppendNumber(numTagsnew);
					//CA(numOfTagss);
				}
			}

			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];
				
				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.elementId == -115 && tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 5)
					{
						objCSprayStencilInfo.isSectionLevelHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId != -1)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);						
					}
					else if(tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isDBTable = kTrue;
						if(tagInfo.typeId != -1)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
					}

					if(tagInfo.whichTab == 3)
						objCSprayStencilInfo.isProductCopy = kTrue;
						
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
					
					if(tagInfo.childTag == 1)
						objCSprayStencilInfo.isChildTag = kTrue;
					continue;
				}

				if(tagInfo.imgFlag == 1)
				{
					//CA("image Present");
					//objCSprayStencilInfo.isAsset = kTrue;
					objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
					
					if(tagInfo.whichTab == 3)
					{
						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isProductBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isProductPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.productPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isProductAsset = kTrue;

					}
					else if(tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isProductChildTag = kTrue;

						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isItemPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.itemPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isAsset = kTrue;

					}
					else if(tagInfo.whichTab == 5)
					{
						if(tagInfo.typeId == -222 || tagInfo.typeId == -223)
							objCSprayStencilInfo.isSectionLevelBMSAssets = kTrue;
						else if(tagInfo.colno == -28)
						{
							objCSprayStencilInfo.isSectionPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.sectionPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno == -27)
						{
							objCSprayStencilInfo.isPublicationPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.publicationPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno > 0)
						{
							objCSprayStencilInfo.isCatagoryPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.catagoryPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.catLevel < 0 )
						{
							objCSprayStencilInfo.isEventSectionImages = kTrue;
							if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.eventSectionAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
							}
							else
								objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
						}
						else if(tagInfo.catLevel > 0)
						{
							objCSprayStencilInfo.isCategoryImages = kTrue;
							if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.categoryAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
							}
							else
								objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
						}

					}	

					continue;
				}
				
				//CA("copy Attribute");
				objCSprayStencilInfo.AttributeIds.push_back(tagInfo.elementId);

				if(tagInfo.whichTab == 3)
					objCSprayStencilInfo.isProductCopy = kTrue;
				if(tagInfo.whichTab == 4)
				{
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
				}
				if(tagInfo.whichTab == 5)
					objCSprayStencilInfo.isSectionCopy = kTrue;
				

				/*if(!isSpreadBasedLetterKeys)
				{
					if(tagInfo.elementId == -803 && tagInfo.typeId == 1)
					{
						isSpreadBasedLetterKeys = kTrue;
					}
					else 
					{
						isSpreadBasedLetterKeys = kFalse;
					}
				}*/

				if(tagInfo.childTag == 1)
					objCSprayStencilInfo.isChildTag = kTrue;

				if(tagInfo.isEventField == 1)
					objCSprayStencilInfo.isEventField = kTrue;

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
				}
				
				

			}
			//------------
			
			for(int32 tagIndex = 0 ; tagIndex < tableTagList1.size() ; tagIndex++)
			{
				tableTagList1[tagIndex].tagPtr->Release();
			}

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
	}while(kFalse);
	result = kTrue;
	return result;
}


bool16 Refresh::getMultipleSectionData()
{
	CurrentSelectedDocumentSectionID = -1;
	vec_GetMultipleSectionDataPtr vec_ptr = new vec_GetMultipleSectionData; 
	if(vec_ptr == NULL)
	{
		//CA("vec_ptr == NULL");
		return kFalse;
	}
	
	if(selectedDocIndexList.size() > 0)
	{
		//for(int32 docIndex = selectedDocIndexList.size()-1; docIndex >= 0 ; docIndex--)
		for(int32 docIndex = 0; docIndex < selectedDocIndexList.size(); docIndex++)
		{
			//if(docIndex < selectedDocIndexList.size()-1)
			if(docIndex > 0)
				CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority);
			
			int32 docIndexInBookFileList = selectedDocIndexList[docIndex];

			SDKLayoutHelper sdklhelp;
			UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndexInBookFileList]);
			if(CurrDocRef == UIDRef ::gNull)
			{
				//CA("CurrDocRef is invalid");
				continue;
			}
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
			if(err == kFailure)
			{
				//CA("Error occured while opening the layoutwindow of the template");
				continue;
			}
			
			IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(fntDoc==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::fntDoc==nil");	
				return kFalse;
			}
			InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
			if (layout == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::layout == nil");		
				return kFalse;
			}
			IDataBase* database = ::GetDataBase(fntDoc);
			if(database==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::database==nil");			
				return kFalse;
			}
			InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
			if (iSpreadList==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::iSpreadList==nil");				
				return kFalse;
			}
			
			UIDList allPageItems(database);

			for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
			{
				UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

				InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
				if(!spread)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!spread");						
					return -1;
				}
				int numPages=spread->GetNumPages();

				for(int i=0; i<numPages; i++)
				{
					UIDList tempList(database);
					spread->GetItemsOnPage(i, &tempList, kFalse);
					allPageItems.Append(tempList);
				}
			}			

			for(int32 index = 0; index < allPageItems.Length(); index++)
			{
				UIDRef boxUIDRef = allPageItems.GetRef(index);
				
				TagList tList=itagReader->getTagsFromBox(boxUIDRef);
				int numTags=static_cast<int>(tList.size());
				if(numTags<0)
				{
					//CA("numTags<0");
					return kFalse;
				}

				/*PMString numOfTags("numTags = ");
				numOfTags.AppendNumber(numTags);
				CA(numOfTags);*/

				if(numTags<=0)//This can be a Tagged Frame
				{	
					tList=itagReader->getFrameTags(boxUIDRef);				
							
				}	

				int numTags1=static_cast<int>(tList.size());
				if(numTags1<0)
				{
					//CA("numTags1<0");
					return kFalse;
				}

				bool16 isCustomTablePresent = kFalse;
				TagList tableTagList1;
				for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
				{
					TagStruct & tagInfo = tList[tagIndex];
					if(tagInfo.sectionID == -1)
						continue;

					PMString temp("tagInfo.sectionID");
					temp.AppendNumber(tagInfo.sectionID);
					temp.Append(", CurrentSelectedDocumentSectionID");
					temp.AppendNumber(CurrentSelectedDocumentSectionID);
					//CA(temp);


					if(CurrentSelectedDocumentSectionID != tagInfo.sectionID || CurrentSelectedDocumentLanguageID != tagInfo.languageID)
					{
						bool16 alreadyPresent = kFalse;
						if(vec_ptr->size() > 0)
						{
							for(int32 j = 0; j < vec_ptr->size(); j++)
							{
								GetMultipleSectionData ObjGetMultipleSectionData = vec_ptr->at(j);
								if(ObjGetMultipleSectionData.sectionId == tagInfo.sectionID && ObjGetMultipleSectionData.languageId == tagInfo.languageID)
								{
									alreadyPresent = kTrue;
									break;
								}
							}
						}

						if(!alreadyPresent)
						{
							GetMultipleSectionData ObjGetMultipleSectionData;
							ObjGetMultipleSectionData.sectionId = tagInfo.sectionID;
							ObjGetMultipleSectionData.languageId = tagInfo.languageID;
							
							CPubModel  cPubModelObj = ptrIAppFramework->getpubModelByPubID(tagInfo.sectionID,tagInfo.languageID);
							ObjGetMultipleSectionData.publicationId = cPubModelObj.getRootID();	

							vec_ptr->push_back(ObjGetMultipleSectionData);							
						}

						CurrentSelectedDocumentSectionID = tagInfo.sectionID;
						CurrentSelectedDocumentLanguageID = tagInfo.languageID;
					}
				}		
				
				
				
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
		
			sdklhelp.CloseDocument(CurrDocRef, kTrue);										
		}					
	}
	else
		return kFalse;

	//ptrIAppFramework->GetSectionData_clearSectionDataCache();

	GetSectionData getSectionData;
	getSectionData.setAllkTrue();
	
	//ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);
	
	delete vec_ptr;
	return kTrue;
}


void Refresh::customTableRefreshByCell(const UIDRef& curBox, TagStruct& slugInfo, double objectId)
{
	//CA("customTableRefreshByCell");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrameUID is kInvalidUID");
		return;
	}

	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::graphicFrameHierarchy==nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemHierarchy==nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemTextFrame==nil");
		//CA("Its Not MultiColumn");	
		return;
	}

	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::frameItemHierarchy==nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrame==nil");
		//CA("!!!ITextFrameColumn");
		return;
	}

///////////		End
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");	
		return;
	}
//	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
//	if (textFocusManager == nil)
//	{
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFocusManager == nil");	
//		return;
//	}
//	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
//	if (frameTextFocus == nil)
//	{
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::frameTextFocus == nil");	
//		return;
//	}
	
//	//TagReader tReader1;
//	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//	if(!itagReader)
//	{ 
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::itagReader == nil");		
//		return ;
//	}
//
//	TagList tList1;		
//	if(refreshTableByAttribute)
//        tList1=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
//	else
//		tList1=itagReader->getTagsFromBox_ForRefresh(curBox);
//	if(!tList1.size())//The box contains no tags..But it can be the tagged frame!!!!!
//	{
//		tList1=itagReader->getFrameTags(curBox);
//		if(tList1.size()<=0)
//		{
//			//It really is not our box
//			return ;			
//		}
//	}
		
	if(!itagReader->GetUpdatedTag(slugInfo))
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::!itagReader->GetUpdatedTag(slugInfo)");
		return;
	}

	int32 tStart1 = slugInfo.startIndex+1;
	int32 tEnd1 = slugInfo.endIndex-1;

	//for(int j=0; j<tList1.size(); j++)
	//{	//CA("333");
		//if((slugInfo.elementId == tList1[j].elementId) && (slugInfo.whichTab==tList1[j].whichTab))	
		//{	//CA("element id matches");
			//if(slugInfo.childTag == 1 /*&& tList1[j].childTag == 1*/)
			//{
				//CA("have childTag");
				//if(slugInfo.childId == tList1[j].childId && slugInfo.field3 == tList1[j].field3 && slugInfo.field4 == tList1[j].field4)
				//{
//PMString a("slugInfo.childId = ");
//a.AppendNumber(slugInfo.childId);
//a.Append("\nslugInfo.field3 = ");
//a.AppendNumber(slugInfo.field3);
//a.Append("\nslugInfo.field4 = ");
//a.AppendNumber(slugInfo.field4);
//a.Append("\nslugInfo.elementId = ");
//a.AppendNumber(slugInfo.elementId );
//CA(a);
					//CA("Match childID");
					//tStart1 = slugInfo.startIndex+1;
					//tEnd1 = slugInfo.endIndex-1;
					//break;
				//}
			//}
	//		else if(slugInfo.header == 1){
	//			//CA("haveing header");
	//			if(slugInfo.header == slugInfo.header){
	//				tStart1 = slugInfo.startIndex+1;
	//				tEnd1 = slugInfo.endIndex-1;
	//				//break;
	//			}
	//		}
	//		else{
	//			//CA("Don't have childID");
	//			//if(slugInfo.parentId == tList1[j].parentId && slugInfo.header == tList1[j].header && slugInfo.childTag == tList1[j].childTag)
	//			{
	//				//CA("Match parent");
	//				tStart1 = slugInfo.startIndex+1;
	//				tEnd1 = slugInfo.endIndex-1;
	//				//break;
	//			}
	//		}
	//		//continue;
	//	}
	//}
	//int32 tStart1=slugInfo.startIndex;
	//int32 tEnd1=slugInfo.endIndex-1;
	/*PMString ZXC("tStart1 : ");
	ZXC.AppendNumber(tStart1);
	ZXC.Append("  tEnd1 : ");
	ZXC.AppendNumber(tEnd1);*/
	//CA(ZXC);
	
	char *originalString="";
	char *changedString="";
	PMString entireStory("");
	bool16 result1 = kFalse;
	
	result1=this->GetTextstoryFromBox(textModel, tStart1, tEnd1, entireStory);
	//CA("entireStory = " + entireStory);
	changedString	= const_cast<char*>(entireStory.GetPlatformString().c_str()); //Cs4
	int32 tStart, tEnd;

	//TagReader tReader;
//CA("555");	
	
//CA("666");
	tStart=slugInfo.startIndex+1;
	tEnd=slugInfo.endIndex-tStart;
	
	PMString textToInsert("");
	VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
	VectorHtmlTrackerValue vectorObj ;
	vectorHtmlTrackerSPTBPtr = &vectorObj;

	//elementId = 101 for Items tag when the Item Table is sprayed 
	//in tabbed text format.We can't use elementId = -1 as it is used 
	//for product number.
	//Added slugInfo.elementId != -1 For Med Custom Table By Dattatray on 17/11
	if(slugInfo.elementId != -101 && slugInfo.elementId != -103 && slugInfo.tableType != 3  && slugInfo.whichTab != 5)//4 Sept ItemTableHeader
	{		
		//CA("calling getDataFromDB");
		/*isNewItem = kFalse;
		for(int32 index = 0 ; index < BookReportData::vecReportRows.size() ; index++)
		{
			if(slugInfo.childTag == 1 ){
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.childId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
			else{
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.parentId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
		}*/

		double ItemID = -1;
		if(slugInfo.childTag == 1 && slugInfo.whichTab == 4)
			ItemID = slugInfo.childId;
		else
			ItemID = objectId;


        
		getDataFromDB(textToInsert, slugInfo, ItemID);	//getDataFromDBNewoption	
		
		//******
		//CA("getDataFromDB " + textToInsert);
	}
	else if(slugInfo.whichTab == 5)
	{
		
		getDataFromDB(textToInsert, slugInfo, objectId);
	}
	else 
	{
		InterfacePtr<ITextStoryThread> textStoryThread(slugInfo.tagPtr->QueryContentTextStoryThread()/*,UseDefaultIID()*/);
		if(textStoryThread == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textStoryThread == nil");
			return;
		}
		InterfacePtr<ITextModel>textModel(textStoryThread->QueryTextModel()/*,UseDefaultIID()*/);
		if(textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");
			return;
		}
		//This code is for refresh
		UIDRef txtModelUIDRef = ::GetUIDRef(textModel);

		int32 startReplaceFrom = -1;
		int32 endReplace = -1;
		Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startReplaceFrom,&endReplace);
		startReplaceFrom = startReplaceFrom +1;
		endReplace = endReplace -1;

		//This code is for refresh
		//start 4 Sept ItemTableHeader
		PMString dispName("");

		if(slugInfo.elementId == -103)//4 Sept ItemTableHeader
		{
			//CA("3");
				do
				{
					double typeId = slugInfo.typeId;
					VectorTypeInfoPtr typeValObj = nil;
					if(slugInfo.whichTab == 3)
						typeValObj= ptrIAppFramework->StructureCache_getListTableTypes();
					else if(slugInfo.whichTab == 4)
						typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();

					if(typeValObj==nil)
					{
						ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::typeValObj == nil");					
						break;
					}

					VectorTypeInfoValue::iterator it1;

					bool16 typeIDFound = kFalse;
					for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
					{	
							if(typeId == it1->getTypeId())
							{
								typeIDFound = kTrue;
								break;
							}			
					}
					if(typeIDFound)
						dispName = it1->getName();
					if(typeValObj)
						delete typeValObj;
				}while(kFalse);

				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				if(iConverter)
				{
					iConverter->ChangeQutationMarkONOFFState(kFalse);			
				}

				//WideString insertText(dispName);
				//I fooled the compiler.Instead of deleting each tag,I deleted all the
				//text range containing the tags.It will in turn delete all the tags.
//	CS3 Change				textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
				//textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
				//boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                dispName.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(dispName));
				ReplaceText(textModel, startReplaceFrom, endReplace-startReplaceFrom + 1 , insertText);
				if(iConverter)
				{
					iConverter->ChangeQutationMarkONOFFState(kTrue);			
				}
				return;
		}
		//end 4 Sept ItemTableHeader
		
		
		//WideString insertText(dispName);		
		//I fooled the compiler.Instead of deleting each tag,I deleted all the
		//text range containing the tags.It will in turn delete all the tags.
//CS3 Change		textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}
		//textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
        dispName.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(dispName));
		ReplaceText(textModel,startReplaceFrom,endReplace-startReplaceFrom + 1 ,insertText);
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		if(slugInfo.whichTab == 3)
		{
			//CA("4");
			if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
			else
			{
				if(slugInfo.typeId != -111)
					sprayItemTableInTabbedTextForm(slugInfo,textModel);
			
				if(slugInfo.typeId == -111 ) //on 17/11 By Dattatray this typeId is HardCoded For Med Custom Table so For Med Custom Table in TabTextForm below Function get called..
					sprayCMedCustomTableScreenPrintInTabbedTextForm(slugInfo,textModel);
			}
		}
		else if(slugInfo.whichTab == 4)
		{	//CA("5");

			if((slugInfo.tableType == 4) || (slugInfo.tableType == 5) || (slugInfo.tableType == 6)/*(slugInfo.typeId == -112) || (slugInfo.typeId == -113) || (slugInfo.typeId == -114)*/)
			{
				//CA("Refreshing Kit Table in Tabbed text Fromat");
				bool16 isKitTable = kFalse;
				if(slugInfo.tableType == 4)
					sprayProductKitComponentTableScreenPrintInTabbedTextForm(slugInfo, curBox, isKitTable);
				else if(slugInfo.tableType == 6)
					sprayXrefTableInTabbedTextForm(slugInfo, curBox);
				else if(slugInfo.tableType == 5)
					sprayAccessoryTableScreenPrintInTabbedTextForm(slugInfo, curBox);

			}
			else if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
			else
				sprayItemItemTableScreenPrintInTabbedTextForm( slugInfo,textModel);
		}
		else if(slugInfo.whichTab == 5)
		{
			if(slugInfo.tableType == 3)//for hybrid table
				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
		}
		return;
		//sprayItemTableInTabbedTextForm(tStruct,textModel);		
		//end
	}
	//CA("6");

	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(!iConverter)
	{
		textToInsert=textToInsert;					
	}
	else
	{
		//textToInsert=iConverter->translateString(dispName);
		vectorHtmlTrackerSPTBPtr->clear();
		textToInsert=iConverter->translateStringNew(textToInsert, vectorHtmlTrackerSPTBPtr);
	}
	shouldPushInFreameVect = kFalse;
	
	if(entireStory.Compare(kTrue,textToInsert) != 0)
	{
	
		double itemID = slugInfo.parentId;
		if(slugInfo.whichTab == 3 && slugInfo.imgFlag == 0 )
		{
			productsUpdated.push_back(objectId);
		}
		//itemsUpdated.push_back(itemID);

		//CA(" entireStory.Compare(kTrue,textToInsert) != 0");
//PMString a("entireStory	:	");
//a.Append(entireStory);
//a.Append("\rtextToInsert	:	");
//a.Append(textToInsert);
//CA(a);
		//shouldPushInFreameVect = kTrue;
		/*int32 ItemID = -1;
		if(slugInfo.childTag == 1)
			ItemID = slugInfo.childId;
		else
			ItemID = objectId;*/
		//**** if Document Data and DB DAta is Different then add in to Report	
		//BookReportData rowdata;
		//PMString dummy(slugInfo.tagPtr->GetTagString());
		//BookReportData::removeDashFromString(dummy);

		//rowdata.oneRawData.fieldName      = dummy;
		//rowdata.oneRawData.documentValue  = entireStory;
		//rowdata.oneRawData.oneSourceValue = textToInsert;
		//rowdata.oneRawData.isImage        = kFalse;
		//rowdata.oneRawData.itemID = ItemID;
		//
		//if(isNewItem == kTrue){
		//	//CA("isNewItem == kTrue");
		//	rowdata.addEmptyRow = kTrue;	
		//	//isNewItem = kFalse;//18MAY09
		//}
	/*PMString asd("BRFOR              Setting ISNewSection- BookReportData::currentSection    - ");
			asd.Append(BookReportData::currentSection);
			asd.Append("\n newSectionName = ");
			asd.Append(newSectionName);
			CA(asd);	*/
		//if((newSectionName.Compare(kTrue,BookReportData::currentSection) !=0) )
		//{		
		//	
		//	/*PMString asd("Setting ISNewSection- BookReportData::currentSection    - ");
		//	asd.Append(BookReportData::currentSection);
		//	asd.Append("\n newSectionName = ");
		//	asd.Append(newSectionName);
		//	CA(asd);*/
		//		if(newSectionName.Compare(kTrue,"") ==0 ){
		//			//CA("newSectionName.Compare(kTrue,"") ==0 ");
		//			rowdata.isNewSection = kFalse;//18MAY09	
		//			newSectionName = BookReportData::currentSection;
		//		}
		//		else{
		//			newSectionName = BookReportData::currentSection;
		//			rowdata.isNewSection = kTrue;//18MAY09
		//			::isnewSection = kFalse;
		//		}
		//}
//		if(isNewItem == kFalse)
//		{
//			//CA("isNewItem == kFalse");
//			if(BookReportData::vecReportRows.size() > 0){
//				BookReportData BookReportDataobj = BookReportData::vecReportRows.at(BookReportData::vecReportRows.size()-1);
//				if(BookReportDataobj.oneRawData.itemID != ItemID)
//				{
//					//CA("going to currect insertion");
//					vector<BookReportData>::iterator it;
//					
//					bool16 found = kFalse;
//					
//					for(it = BookReportData::vecReportRows.end()-1 ; it != BookReportData::vecReportRows.begin()-1 ; it-- )
//					{
//						BookReportData BookReportObj = (*it);
//						if(BookReportObj.oneRawData.itemID == ItemID)
//						{
//							found = kTrue;
//							break;
//						}
//					}
//					if(found == kTrue)
//					{
//						//CA("found == kTrue");
//						it++;
//						BookReportData::vecReportRows.insert(it,rowdata);					
//					}
//				}
//				else
//					BookReportData::vecReportRows.push_back(rowdata);
//			}
//			else
//				BookReportData::vecReportRows.push_back(rowdata);
//		}
//		else
//			BookReportData::vecReportRows.push_back(rowdata);
////		dummy1++;
	}
	else
	{
		return;
	}

	
		
	
	
	//********			
//	char* dbValue =	const_cast<char*>(textToInsert.GrabCString());
//	const char* att =strattName.GrabCString();
//	fputs(att,fp);
//	fprintf(fp,"\t\t");
//	fputs(changedString,fp);
//	fprintf(fp,"\t\t");
//	fputs(dbValue,fp);
//	fprintf(fp,"\n");
	

	//********
	originalString	= const_cast<char*>(textToInsert.GetPlatformString().c_str()); //Cs4
//****
//PMString asd(originalString);
//CA("This is DB String..originalString = "+asd);
//****
	int32 flag=0;
	
	///////////////////////////rahul
	

	WideString* myText=new WideString(textToInsert);	
	if(!iConverter)
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::iConverter == nil");					
		return;
	}	
	iConverter->ChangeQutationMarkONOFFState(kFalse);
	bool16 alreadyExist =kFalse;
	WideString insertBlankString(" ");

	//if(/*shouldRefreshFlag == kTrue && (!isDummyCall) && slugInfo.field5 != 1*/)//shouldRefreshFlag
	{
		if(slugInfo.whichTab==4)
		{
			for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
			{
				//CA("For loop");
				if(itemsUpdated[index] == objectId)
				{
					alreadyExist = kTrue;
				}
			}
			//CA("BookReportData::isReportWithRefreshFlag");
			if(!(alreadyExist == kTrue))
			itemsUpdated.push_back(objectId);
		}
		if(slugInfo.whichTab==3)
		{
			for(int index = 0; index<productsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
			{
				//CA("For loop");
				if(productsUpdated[index] == objectId)
				{
					alreadyExist = kTrue;
				}
			}
			//CA("BookReportData::isReportWithRefreshFlag");
			if(!(alreadyExist == kTrue))
			productsUpdated.push_back(objectId);
		}
		//textModel->Replace(tStart, tEnd ,&insertBlankString);
		boost::shared_ptr<WideString> insertText(new WideString(insertBlankString));
		ReplaceText(textModel,tStart, tEnd ,insertText);
		//ErrorCode Err = textModel->Replace(tStart,1 /*tEnd*/, myText);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText1(new WideString(textToInsert));
		ReplaceText(textModel,tStart, 1 ,insertText1);

	}
		
	iConverter->ChangeQutationMarkONOFFState(kTrue);
	this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tStart );

	//int32 result2 =0;
	//result2= textToInsert.Compare(kTrue, entireStory );
	

	//TagList tList2;
	//if(refreshTableByAttribute)
	//	tList2=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
	//else
	//	tList2=itagReader->getTagsFromBox_ForRefresh(curBox);
	//if(!tList2.size())//The box contains no tags..But it can be the tagged frame!!!!!
	//{
	//	tList2=itagReader->getFrameTags(curBox);
	//	if(tList2.size()<=0)//It really is not our box
	//	{
	//		return ;			
	//	}
	//}
	//for(int i=0; i<tList2.size(); i++)
	//{	
	//	//CA("7");
	//	if((slugInfo.elementId == tList2[i].elementId) && (slugInfo.whichTab==tList2[i].whichTab))//slugInfo.elementId == tList[i].elementId)
	//	{
	//		tStart = tList2[i].startIndex+1;
	//		tEnd = tList2[i].endIndex;

	//		/*PMString debugStr("slugInfo.elementId = ");
	//		debugStr.AppendNumber(slugInfo.elementId);
	//		debugStr.Append(" , i = ");
	//		debugStr.AppendNumber(i);
	//		debugStr.Append(" , entireStory = ");
	//		debugStr.Append(entireStory);
	//		debugStr.Append(" , textToInsert = ");
	//		debugStr.Append(textToInsert);
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox *********   " + debugStr);*/

	//		break;
	//	}
	//}
	//PMString GreenColour ("C=75 M=5 Y=100 K=0");
	//char * OriginalString= NULL;
	//char * NewString= NULL;
	//OriginalString = const_cast<char*>(entireStory.GrabCString()/*GetPlatformString().c_str()*/);//Cs4
	//NewString= const_cast<char*>(textToInsert.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
	//int32 k1=0;
	//PMString ABC("");
	//NewTextList.clear();
	//int32 Q=tStart;
	//int32 Flagg=0;
	//for(int32 p=tStart; p<=tEnd; p++)
	//{ 			
	//	TextVector Textvector1;
	//	if(NewString[k1]== ' '|| p==tEnd)
	//	{ 
	//		if(Flagg==0)
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=Textvector1.StartIndex + k1;
	//			Q=Textvector1.EndIndex+1;
	//			NewTextList.push_back(Textvector1);
	//			ABC.Clear();
	//			Flagg=1;
	//		}
	//		else
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=tStart+k1;
	//			Q=Textvector1.EndIndex+1;
	//			NewTextList.push_back(Textvector1);
	//			ABC.Clear();
	//		}
	//	}
	//	else
	//	{
	//		ABC.Append(NewString[k1]);
	//	}
	//	k1++;
	//}
	//Flagg=0;
	//ABC.Clear();
	//k1=0;
	//OriginalTextList.clear();
	//Q=tStart1;
	//for( int32 p=tStart1; p<=tEnd1+1; p++)
	//{ 
	//	TextVector Textvector1;
	//	if(OriginalString[k1]== ' ' || p==tEnd1+1)
	//	{ 
	//		if(Flagg==0)
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=Textvector1.StartIndex + k1;
	//			Q=Textvector1.EndIndex+1;
	//			OriginalTextList.push_back(Textvector1);
	//			ABC.Clear();
	//			Flagg=1;
	//		}
	//		else
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=tStart+k1;
	//			Q=Textvector1.EndIndex+1;
	//			OriginalTextList.push_back(Textvector1);
	//			ABC.Clear();
	//		}
	//	}
	//	else
	//	{
	//		ABC.Append(OriginalString[k1]);
	//	}
	//	k1++;
	//}

		//-------lalit-----
	/*for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
	{
		tList2[tagIndex].tagPtr->Release();
	}
	*/
//	if(result2!=0 /*&& HiliteFlag==kTrue*/)
//	{	
//		RefreshData ColorData;
//		ColorData.BoxUIDRef= curBox;
//		//TagReader tReader;
//		TagList tList;
//		if(refreshTableByAttribute)
//			tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
//		else
//			tList=itagReader->getTagsFromBox_ForRefresh(curBox);
//		if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
//		{
//			tList=itagReader->getFrameTags(curBox);
//			if(tList.size()<=0)
//			{
//				//It really is not our box
//				return ;			
//			}
//		}
//		for(int i=0; i<tList.size(); i++)
//		{	
//			if((slugInfo.elementId == tList1[i].elementId) && (slugInfo.whichTab==tList1[i].whichTab))//slugInfo.elementId == tList[i].elementId)
//			{
//				tStart = tList[i].startIndex+1;
//				tEnd = tList[i].endIndex;
//				break;
//			}
//		}
//		//---------
//		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//		{
//			tList[tagIndex].tagPtr->Release();
//		}
//		
/////////////////////////////////rahul
//	
///*	
//			
//	int32 srt,en,flag2=0,k=0,a,b=0,c=0,tot1,tot2;//srt1,en1,
//	char Odoubl[100][500],Cdoubl[100][500];
//	srt = tStart1;
//	en = tEnd1;
//	b=0;
//	
//	for(a=0;originalString[a];a++)
//	{
//		if(originalString[a] == ' ' && originalString[a+1] == ' ')
//			continue;
//		if(originalString[a] == ' ')
//		{
//			Odoubl[b][c] = '\0';
//			b++;
//			c=0;
//		}
//		else
//		{
//			Odoubl[b][c] = originalString[a];
//			c++;
//		}
//	}
//	Odoubl[b][c] = '\0';
//	tot1=b;
//	b=0;
//	c=0;
//	for(a=0;changedString[a];a++)
//	{
//		if(changedString[a] == ' ' && changedString[a+1] == ' ')
//			continue;
//		if(changedString[a] == ' ')
//		{
//			Cdoubl[b][c] = '\0';
//			b++;
//			c=0;
//		}
//		else
//		{
//			Cdoubl[b][c] = changedString[a];
//			c++;
//		}
//	}
//	Cdoubl[b][c] = '\0';
//	tot2=b;
//	/////////////////////////////
//	for(a=0;a<=tot2;a++)
//	{
//		PMString ab("");
//		ab.Append(Cdoubl[a]);
//		//CA(ab);
//	}
//	int32 m=0,n=0,ind=0;
//	int32 Counter=0; 
//	if(tot1==tot2)
//	{
//		for(a=0;a<=tot2;a++)
//		{
//			if(strcmp(Odoubl[a],Cdoubl[a])!=0)
//				{
//					PMString abc("");
//					abc.Append(Odoubl[a]);
//					TextVector Textvector1;
//					Textvector1.Word.Clear();
//					Textvector1.Word.Append(abc);
//					ChangedTextList.push_back(Textvector1);
//                    CA(abc);
//				}
//				
//		}
//	}
//	else
//	{
//		for(a=0;a<=tot2;a++)
//		{
//			b=m;
//			Counter = 0;
//			for(;b<=tot1;b++)
//			{
//				
//				if(strcmp(Odoubl[b],Cdoubl[a])==0)
//					{
//						m=b+1;
//						break;
//					}
//				else
//					{
//						flag2 = 0;
//						for(c=0;c<=tot2;c++)
//						{
//							if(strcmp(Odoubl[b],Cdoubl[c])==0)
//							{
//								flag2 = 1;
//							}
//						}
//						if(flag2 == 0)
//						{
//							PMString abc("");
//							abc.Append(Odoubl[b]);
//							TextVector Textvector1;
//							Textvector1.Word.Clear();
//							Textvector1.Word.Append(abc);
//							ChangedTextList.push_back(Textvector1);
//							CA(abc);
//						}
//											
//					}
//					Counter++;
//
//			}
//			
//			
//		}
//	}	
//
//*/
//	// Awasthi Added
//		int32 tot1=static_cast<int32> (NewTextList.size());
//		int32 tot2= static_cast<int32>(OriginalTextList.size());
//		
//		int32 m=0, flag2=0;
//		if(tot1==tot2)
//		{	
//			for(int32 a=0; a<tot2; a++)
//			{	
//				if((NewTextList[a].Word.Compare(kTrue, OriginalTextList[a].Word))!=0)
//					{							
//						PMString abc("");
//						abc.Append(NewTextList[a].Word);
//						TextVector Textvector1;
//						Textvector1.Word.Clear();
//						Textvector1.Word.Append(abc);
//						Textvector1.BoxUIDRef= curBox;
//						Textvector1.StartIndex= NewTextList[a].StartIndex;
//						Textvector1.EndIndex= NewTextList[a].EndIndex;
//						ChangedTextList.push_back(Textvector1);		
//					}					
//			}
//		}
//		else
//		{
//			for(int32 a=0;a<tot2;a++)
//			{
//				int32 b=m;
//				
//				for(;b<tot1;b++)
//				{					
//					if((NewTextList[b].Word.Compare(kTrue, OriginalTextList[a].Word)==0))//(strcmp(Odoubl[b],Cdoubl[a])==0)
//						{
//							m=b+1;
//							break;
//						}
//					else
//						{
//							flag2 = 0;
//							for(int32 c=0;c<tot2;c++)
//							{
//								if(((NewTextList[b].Word.Compare(kTrue, OriginalTextList[c].Word))==0))//(strcmp(Odoubl[b],Cdoubl[c])==0)
//								{
//									flag2 = 1;
//								}
//							}
//							if(flag2 == 0)
//							{									
//								PMString abc("");
//								abc.Append(NewTextList[b].Word);
//								TextVector Textvector1;
//								Textvector1.Word.Clear();
//								Textvector1.Word.Append(abc);
//								Textvector1.BoxUIDRef= curBox;
//								Textvector1.StartIndex= NewTextList[b].StartIndex;
//								Textvector1.EndIndex= NewTextList[b].EndIndex;
//								ChangedTextList.push_back(Textvector1);
//							}												
//						}
//					}				
//				}
//			}
//
//			if(tot1> tot2)
//			{
//				for (int C = tot2; C< tot1; C++)
//				{
//					PMString abc("");
//					abc.Append(NewTextList[C].Word);
//					TextVector Textvector1;
//					Textvector1.Word.Clear();
//					Textvector1.Word.Append(abc);
//					Textvector1.BoxUIDRef= curBox;
//					Textvector1.StartIndex= NewTextList[C].StartIndex;
//					Textvector1.EndIndex= NewTextList[C].EndIndex;
//					ChangedTextList.push_back(Textvector1);
//				}
//			}
//			// Commented By Rahul ... no more needed now
//				//for(int32 s=0; s<ChangedTextList.size();s++)
//				//{	
//				//	ColorData.elementID=slugInfo.elementId;
//				//	ColorData.StartIndex=ChangedTextList[s].StartIndex;
//				//	ColorData.EndIndex=ChangedTextList[s].EndIndex; 
//				//	//this->ChangeColorOfText(textModel, ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex, GreenColour);
//				//	this->SetFocusForText(curBox,ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex); 
//				//	ColorDataList.push_back(ColorData);
//				//}
//			if(myText)
//				delete myText;
//	}
//added by avinash for testing
	if(myText)
		delete myText;
	// till here
	
}
void Refresh::customTableRefreshByCellNewoption(const UIDRef& curBox, TagStruct& slugInfo, double objectId)
{
	//CA(" Refresh::customTableRefreshByCellNewoption");
	//CA("Refresh::customTableRefreshByCell");//shouldRefresh
	//CA(__FUNCTION__);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrameUID is kInvalidUID");
		return;
	}

	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::graphicFrameHierarchy==nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemHierarchy==nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::multiColumnItemTextFrame==nil");
		//CA("Its Not MultiColumn");	
		return;
	}

	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::frameItemHierarchy==nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textFrame==nil");
		//CA("!!!ITextFrameColumn");
		return;
	}

///////////		End
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");	
		return;
	}

	if(!itagReader->GetUpdatedTag(slugInfo))
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::!itagReader->GetUpdatedTag(slugInfo)");
		return;
	}

	int32 tStart1 = slugInfo.startIndex+1;
	int32 tEnd1 = slugInfo.endIndex-1;

	char *originalString="";
	char *changedString="";
	PMString entireStory("");
	bool16 result1 = kFalse;
	
	result1=this->GetTextstoryFromBox(textModel, tStart1, tEnd1, entireStory);
	//CA("entireStory = " + entireStory);
	changedString	= const_cast<char*>(entireStory.GetPlatformString().c_str()); //Cs4
	int32 tStart, tEnd;

	
	tStart=slugInfo.startIndex+1;
	tEnd=slugInfo.endIndex-tStart;
	
	PMString textToInsert("");
	VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
	VectorHtmlTrackerValue vectorObj ;
	vectorHtmlTrackerSPTBPtr = &vectorObj;

	//elementId = 101 for Items tag when the Item Table is sprayed 
	//in tabbed text format.We can't use elementId = -1 as it is used 
	//for product number.
	//Added slugInfo.elementId != -1 For Med Custom Table By Dattatray on 17/11
	if(slugInfo.elementId != -101 && slugInfo.elementId != -103 && slugInfo.tableType != 3  && slugInfo.whichTab != 5)//4 Sept ItemTableHeader
	{		
		//CA("calling getDataFromDB");
		/*isNewItem = kFalse;
		for(int32 index = 0 ; index < BookReportData::vecReportRows.size() ; index++)
		{
			if(slugInfo.childTag == 1 ){
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.childId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
			else{
				if(BookReportData::vecReportRows[index].oneRawData.itemID == slugInfo.parentId)
				{
					isNewItem = kFalse;
					break;
				}
				else
					isNewItem = kTrue;
			}
		}*/

		double ItemID = -1;
		if(slugInfo.childTag == 1 && slugInfo.whichTab == 4)
			ItemID = slugInfo.childId;
		else
			ItemID = objectId;


        getDataFromDBNewoption(textToInsert, slugInfo, ItemID,vector_iterater);
		//getDataFromDB(textToInsert, slugInfo, ItemID);	//getDataFromDBNewoption	
		//******
		//CA("getDataFromDB " + textToInsert);
	}
	else if(slugInfo.whichTab == 5)
	{
		getDataFromDB(textToInsert, slugInfo, objectId);
	}
//	else
//	{
////		InterfacePtr<ITextStoryThread> textStoryThread(slugInfo.tagPtr->QueryContentTextStoryThread()/*,UseDefaultIID()*/);
////		if(textStoryThread == nil)
////		{
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textStoryThread == nil");
////			return;
////		}
////		InterfacePtr<ITextModel>textModel(textStoryThread->QueryTextModel()/*,UseDefaultIID()*/);
////		if(textModel == nil)
////		{
////			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::textModel == nil");
////			return;
////		}
////		//This code is for refresh
////		UIDRef txtModelUIDRef = ::GetUIDRef(textModel);
////
////		int32 startReplaceFrom = -1;
////		int32 endReplace = -1;
////		Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startReplaceFrom,&endReplace);
////		startReplaceFrom = startReplaceFrom +1;
////		endReplace = endReplace -1;
////
////		//This code is for refresh
////		//start 4 Sept ItemTableHeader
////		PMString dispName("");
////
////		if(slugInfo.elementId == -103)//4 Sept ItemTableHeader
////		{
////			//CA("3");
////				do
////				{
////					int32 typeId = slugInfo.typeId;
////					VectorTypeInfoPtr typeValObj = nil;
////					if(slugInfo.whichTab == 3)
////						typeValObj= ptrIAppFramework->StructureCache_getListTableTypes();
////					else if(slugInfo.whichTab == 4)
////						typeValObj = ptrIAppFramework->AttributeCache_getItemTableTypes();
////
////					if(typeValObj==nil)
////					{
////						ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::fillDataInBox::typeValObj == nil");					
////						break;
////					}
////
////					VectorTypeInfoValue::iterator it1;
////
////					bool16 typeIDFound = kFalse;
////					for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
////					{	
////							if(typeId == it1->getType_id())
////							{
////								typeIDFound = kTrue;
////								break;
////							}			
////					}
////					if(typeIDFound)
////						dispName = it1->getName();
////					if(typeValObj)
////						delete typeValObj;
////				}while(kFalse);
////
////				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
////				if(iConverter)
////				{
////					iConverter->ChangeQutationMarkONOFFState(kFalse);			
////				}
////
////				WideString insertText(dispName);
////				//I fooled the compiler.Instead of deleting each tag,I deleted all the
////				//text range containing the tags.It will in turn delete all the tags.
//////	CS3 Change				textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
////				textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
////				if(iConverter)
////				{
////					iConverter->ChangeQutationMarkONOFFState(kTrue);			
////				}
////				return;
////		}
////		//end 4 Sept ItemTableHeader
////		
////		
////		WideString insertText(dispName);		
////		//I fooled the compiler.Instead of deleting each tag,I deleted all the
////		//text range containing the tags.It will in turn delete all the tags.
//////CS3 Change		textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
////		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
////		if(iConverter)
////		{
////			iConverter->ChangeQutationMarkONOFFState(kFalse);			
////		}
////		textModel->Replace(startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
////		if(iConverter)
////		{
////			iConverter->ChangeQutationMarkONOFFState(kTrue);			
////		}
////		if(slugInfo.whichTab == 3)
////		{
////			//CA("4");
////			if(slugInfo.tableType == 3)//for hybrid table
////				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
////			else
////			{
////				if(slugInfo.typeId != -111)
////					sprayItemTableInTabbedTextForm(slugInfo,textModel);
////			
////				if(slugInfo.typeId == -111 ) //on 17/11 By Dattatray this typeId is HardCoded For Med Custom Table so For Med Custom Table in TabTextForm below Function get called..
////					sprayCMedCustomTableScreenPrintInTabbedTextForm(slugInfo,textModel);
////			}
////		}
////		else if(slugInfo.whichTab == 4)
////		{	//CA("5");
////
////			if((slugInfo.tableType == 4) || (slugInfo.tableType == 5) || (slugInfo.tableType == 6)/*(slugInfo.typeId == -112) || (slugInfo.typeId == -113) || (slugInfo.typeId == -114)*/)
////			{
////				//CA("Refreshing Kit Table in Tabbed text Fromat");
////				bool16 isKitTable = kFalse;
////				if(slugInfo.tableType == 4)
////					sprayProductKitComponentTableScreenPrintInTabbedTextForm(slugInfo, curBox, isKitTable);
////				else if(slugInfo.tableType == 6)
////					sprayXrefTableInTabbedTextForm(slugInfo, curBox);
////				else if(slugInfo.tableType == 5)
////					sprayAccessoryTableScreenPrintInTabbedTextForm(slugInfo, curBox);
////
////			}
////			else if(slugInfo.tableType == 3)//for hybrid table
////				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
////			else
////				sprayItemItemTableScreenPrintInTabbedTextForm( slugInfo,textModel);
////		}
////		else if(slugInfo.whichTab == 5)
////		{
////			if(slugInfo.tableType == 3)//for hybrid table
////				sprayHybridTableScreenPrintInTabbedTextForm(slugInfo,curBox);
////		}
////		return;
////		//sprayItemTableInTabbedTextForm(tStruct,textModel);		
////		//end
//	}
	//CA("6");

	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(!iConverter)
	{
		textToInsert=textToInsert;					
	}
	else
	{
		//textToInsert=iConverter->translateString(dispName);
		vectorHtmlTrackerSPTBPtr->clear();
		textToInsert=iConverter->translateStringNew(textToInsert, vectorHtmlTrackerSPTBPtr);
	}
	shouldPushInFreameVect = kFalse;
	if(entireStory.Compare(kTrue,textToInsert) != 0)
	{
		double itemID = slugInfo.parentId;
		//CA("My Test");
		if(slugInfo.whichTab == 3 && slugInfo.imgFlag == 0) //----
		{
			productsUpdated.push_back(objectId);
		}
//		itemsUpdated.push_back(itemID);

		//CA(" entireStory.Compare(kTrue,textToInsert) != 0");
//PMString a("entireStory	:	");
//a.Append(entireStory);
//a.Append("\rtextToInsert	:	");
//a.Append(textToInsert);
//CA(a);
		//shouldPushInFreameVect = kTrue;
		/*int32 ItemID = -1;
		if(slugInfo.childTag == 1)
			ItemID = slugInfo.childId;
		else
			ItemID = objectId;*/
		//**** if Document Data and DB DAta is Different then add in to Report	
		//BookReportData rowdata;
		//PMString dummy(slugInfo.tagPtr->GetTagString());
		//BookReportData::removeDashFromString(dummy);

		//rowdata.oneRawData.fieldName      = dummy;
		//rowdata.oneRawData.documentValue  = entireStory;
		//rowdata.oneRawData.oneSourceValue = textToInsert;
		//rowdata.oneRawData.isImage        = kFalse;
		//rowdata.oneRawData.itemID = ItemID;
		//
		//if(isNewItem == kTrue){
		//	//CA("isNewItem == kTrue");
		//	rowdata.addEmptyRow = kTrue;	
		//	//isNewItem = kFalse;//18MAY09
		//}
	/*PMString asd("BRFOR              Setting ISNewSection- BookReportData::currentSection    - ");
			asd.Append(BookReportData::currentSection);
			asd.Append("\n newSectionName = ");
			asd.Append(newSectionName);
			CA(asd);	*/
		//if((newSectionName.Compare(kTrue,BookReportData::currentSection) !=0) )
		//{		
		//	
		//	/*PMString asd("Setting ISNewSection- BookReportData::currentSection    - ");
		//	asd.Append(BookReportData::currentSection);
		//	asd.Append("\n newSectionName = ");
		//	asd.Append(newSectionName);
		//	CA(asd);*/
		//		if(newSectionName.Compare(kTrue,"") ==0 ){
		//			//CA("newSectionName.Compare(kTrue,"") ==0 ");
		//			rowdata.isNewSection = kFalse;//18MAY09	
		//			newSectionName = BookReportData::currentSection;
		//		}
		//		else{
		//			newSectionName = BookReportData::currentSection;
		//			rowdata.isNewSection = kTrue;//18MAY09
		//			::isnewSection = kFalse;
		//		}
		//}
//		if(isNewItem == kFalse)
//		{
//			//CA("isNewItem == kFalse");
//			if(BookReportData::vecReportRows.size() > 0){
//				BookReportData BookReportDataobj = BookReportData::vecReportRows.at(BookReportData::vecReportRows.size()-1);
//				if(BookReportDataobj.oneRawData.itemID != ItemID)
//				{
//					//CA("going to currect insertion");
//					vector<BookReportData>::iterator it;
//					
//					bool16 found = kFalse;
//					
//					for(it = BookReportData::vecReportRows.end()-1 ; it != BookReportData::vecReportRows.begin()-1 ; it-- )
//					{
//						BookReportData BookReportObj = (*it);
//						if(BookReportObj.oneRawData.itemID == ItemID)
//						{
//							found = kTrue;
//							break;
//						}
//					}
//					if(found == kTrue)
//					{
//						//CA("found == kTrue");
//						it++;
//						BookReportData::vecReportRows.insert(it,rowdata);					
//					}
//				}
//				else
//					BookReportData::vecReportRows.push_back(rowdata);
//			}
//			else
//				BookReportData::vecReportRows.push_back(rowdata);
//		}
//		else
//			BookReportData::vecReportRows.push_back(rowdata);
////		dummy1++;
	}
	else
	{
		return;
	}

	originalString	= const_cast<char*>(textToInsert.GetPlatformString().c_str()); //Cs4

	int32 flag=0;
	
	WideString* myText=new WideString(textToInsert);	
	if(!iConverter)
	{	
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox::iConverter == nil");					
		return;
	}	
	iConverter->ChangeQutationMarkONOFFState(kFalse);
	bool16 alreadyExist =kFalse;
	WideString insertBlankString(" ");

	//if(/*shouldRefreshFlag == kTrue && (!isDummyCall) && slugInfo.field5 != 1*/)//shouldRefreshFlag
	{
		if(slugInfo.whichTab==4)
		{
			for(int index = 0; index<itemsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
			{
				//CA("For loop");
				if(itemsUpdated[index] == objectId)
				{
					alreadyExist = kTrue;
				}
			}
			if(!(alreadyExist == kTrue))
			itemsUpdated.push_back(objectId);
		}
		if(slugInfo.whichTab==3)
		{
			for(int index = 0; index<productsUpdated.size() /*&& (index < upDatedItemParentID.size())*/;index++ )
			{
				//CA("For loop");
				if(productsUpdated[index] == objectId)
				{
					alreadyExist = kTrue;
				}
			}
			//CA("BookReportData::isReportWithRefreshFlag");
			if(!(alreadyExist == kTrue))
			productsUpdated.push_back(objectId);
		}
		//textModel->Replace(tStart, tEnd ,&insertBlankString);
		boost::shared_ptr<WideString> insertText(new WideString(insertBlankString));
		ReplaceText(textModel,tStart,tEnd ,insertText);
		//ErrorCode Err = textModel->Replace(tStart,1 /*tEnd*/, myText);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText1(new WideString(textToInsert));
		ReplaceText(textModel,tStart,1 ,insertText1);
	}
		
	iConverter->ChangeQutationMarkONOFFState(kTrue);
	this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tStart );

	//added by avinash
	if(myText)
		delete myText;

	//till here

	//int32 result2 =0;
	//result2= textToInsert.Compare(kTrue, entireStory );
	

	//TagList tList2;
	//if(refreshTableByAttribute)
	//	tList2=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
	//else
	//	tList2=itagReader->getTagsFromBox_ForRefresh(curBox);
	//if(!tList2.size())//The box contains no tags..But it can be the tagged frame!!!!!
	//{
	//	tList2=itagReader->getFrameTags(curBox);
	//	if(tList2.size()<=0)//It really is not our box
	//	{
	//		return ;			
	//	}
	//}
	//for(int i=0; i<tList2.size(); i++)
	//{	
	//	//CA("7");
	//	if((slugInfo.elementId == tList2[i].elementId) && (slugInfo.whichTab==tList2[i].whichTab))//slugInfo.elementId == tList[i].elementId)
	//	{
	//		tStart = tList2[i].startIndex+1;
	//		tEnd = tList2[i].endIndex;

	//		/*PMString debugStr("slugInfo.elementId = ");
	//		debugStr.AppendNumber(slugInfo.elementId);
	//		debugStr.Append(" , i = ");
	//		debugStr.AppendNumber(i);
	//		debugStr.Append(" , entireStory = ");
	//		debugStr.Append(entireStory);
	//		debugStr.Append(" , textToInsert = ");
	//		debugStr.Append(textToInsert);
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillDataInBox *********   " + debugStr);*/

	//		break;
	//	}
	//}
	//PMString GreenColour ("C=75 M=5 Y=100 K=0");
	//char * OriginalString= NULL;
	//char * NewString= NULL;
	//OriginalString = const_cast<char*>(entireStory.GrabCString()/*GetPlatformString().c_str()*/);//Cs4
	//NewString= const_cast<char*>(textToInsert.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
	//int32 k1=0;
	//PMString ABC("");
	//NewTextList.clear();
	//int32 Q=tStart;
	//int32 Flagg=0;
	//for(int32 p=tStart; p<=tEnd; p++)
	//{ 			
	//	TextVector Textvector1;
	//	if(NewString[k1]== ' '|| p==tEnd)
	//	{ 
	//		if(Flagg==0)
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=Textvector1.StartIndex + k1;
	//			Q=Textvector1.EndIndex+1;
	//			NewTextList.push_back(Textvector1);
	//			ABC.Clear();
	//			Flagg=1;
	//		}
	//		else
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=tStart+k1;
	//			Q=Textvector1.EndIndex+1;
	//			NewTextList.push_back(Textvector1);
	//			ABC.Clear();
	//		}
	//	}
	//	else
	//	{
	//		ABC.Append(NewString[k1]);
	//	}
	//	k1++;
	//}
	//Flagg=0;
	//ABC.Clear();
	//k1=0;
	//OriginalTextList.clear();
	//Q=tStart1;
	//for( int32 p=tStart1; p<=tEnd1+1; p++)
	//{ 
	//	TextVector Textvector1;
	//	if(OriginalString[k1]== ' ' || p==tEnd1+1)
	//	{ 
	//		if(Flagg==0)
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=Textvector1.StartIndex + k1;
	//			Q=Textvector1.EndIndex+1;
	//			OriginalTextList.push_back(Textvector1);
	//			ABC.Clear();
	//			Flagg=1;
	//		}
	//		else
	//		{
	//			Textvector1.Word.Clear();
	//			Textvector1.Word.Append(ABC);
	//			Textvector1.StartIndex=Q;
	//			Textvector1.EndIndex=tStart+k1;
	//			Q=Textvector1.EndIndex+1;
	//			OriginalTextList.push_back(Textvector1);
	//			ABC.Clear();
	//		}
	//	}
	//	else
	//	{
	//		ABC.Append(OriginalString[k1]);
	//	}
	//	k1++;
	//}

		//-------lalit-----
	/*for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
	{
		tList2[tagIndex].tagPtr->Release();
	}
	*/
//	if(result2!=0 /*&& HiliteFlag==kTrue*/)
//	{	
//		RefreshData ColorData;
//		ColorData.BoxUIDRef= curBox;
//		//TagReader tReader;
//		TagList tList;
//		if(refreshTableByAttribute)
//			tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(curBox);
//		else
//			tList=itagReader->getTagsFromBox_ForRefresh(curBox);
//		if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
//		{
//			tList=itagReader->getFrameTags(curBox);
//			if(tList.size()<=0)
//			{
//				//It really is not our box
//				return ;			
//			}
//		}
//		for(int i=0; i<tList.size(); i++)
//		{	
//			if((slugInfo.elementId == tList1[i].elementId) && (slugInfo.whichTab==tList1[i].whichTab))//slugInfo.elementId == tList[i].elementId)
//			{
//				tStart = tList[i].startIndex+1;
//				tEnd = tList[i].endIndex;
//				break;
//			}
//		}
//		//---------
//		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//		{
//			tList[tagIndex].tagPtr->Release();
//		}
//		
/////////////////////////////////rahul
//	
///*	
//			
//	int32 srt,en,flag2=0,k=0,a,b=0,c=0,tot1,tot2;//srt1,en1,
//	char Odoubl[100][500],Cdoubl[100][500];
//	srt = tStart1;
//	en = tEnd1;
//	b=0;
//	
//	for(a=0;originalString[a];a++)
//	{
//		if(originalString[a] == ' ' && originalString[a+1] == ' ')
//			continue;
//		if(originalString[a] == ' ')
//		{
//			Odoubl[b][c] = '\0';
//			b++;
//			c=0;
//		}
//		else
//		{
//			Odoubl[b][c] = originalString[a];
//			c++;
//		}
//	}
//	Odoubl[b][c] = '\0';
//	tot1=b;
//	b=0;
//	c=0;
//	for(a=0;changedString[a];a++)
//	{
//		if(changedString[a] == ' ' && changedString[a+1] == ' ')
//			continue;
//		if(changedString[a] == ' ')
//		{
//			Cdoubl[b][c] = '\0';
//			b++;
//			c=0;
//		}
//		else
//		{
//			Cdoubl[b][c] = changedString[a];
//			c++;
//		}
//	}
//	Cdoubl[b][c] = '\0';
//	tot2=b;
//	/////////////////////////////
//	for(a=0;a<=tot2;a++)
//	{
//		PMString ab("");
//		ab.Append(Cdoubl[a]);
//		//CA(ab);
//	}
//	int32 m=0,n=0,ind=0;
//	int32 Counter=0; 
//	if(tot1==tot2)
//	{
//		for(a=0;a<=tot2;a++)
//		{
//			if(strcmp(Odoubl[a],Cdoubl[a])!=0)
//				{
//					PMString abc("");
//					abc.Append(Odoubl[a]);
//					TextVector Textvector1;
//					Textvector1.Word.Clear();
//					Textvector1.Word.Append(abc);
//					ChangedTextList.push_back(Textvector1);
//                    CA(abc);
//				}
//				
//		}
//	}
//	else
//	{
//		for(a=0;a<=tot2;a++)
//		{
//			b=m;
//			Counter = 0;
//			for(;b<=tot1;b++)
//			{
//				
//				if(strcmp(Odoubl[b],Cdoubl[a])==0)
//					{
//						m=b+1;
//						break;
//					}
//				else
//					{
//						flag2 = 0;
//						for(c=0;c<=tot2;c++)
//						{
//							if(strcmp(Odoubl[b],Cdoubl[c])==0)
//							{
//								flag2 = 1;
//							}
//						}
//						if(flag2 == 0)
//						{
//							PMString abc("");
//							abc.Append(Odoubl[b]);
//							TextVector Textvector1;
//							Textvector1.Word.Clear();
//							Textvector1.Word.Append(abc);
//							ChangedTextList.push_back(Textvector1);
//							CA(abc);
//						}
//											
//					}
//					Counter++;
//
//			}
//			
//			
//		}
//	}	
//
//*/
//	// Awasthi Added
//		int32 tot1=static_cast<int32> (NewTextList.size());
//		int32 tot2= static_cast<int32>(OriginalTextList.size());
//		
//		int32 m=0, flag2=0;
//		if(tot1==tot2)
//		{	
//			for(int32 a=0; a<tot2; a++)
//			{	
//				if((NewTextList[a].Word.Compare(kTrue, OriginalTextList[a].Word))!=0)
//					{							
//						PMString abc("");
//						abc.Append(NewTextList[a].Word);
//						TextVector Textvector1;
//						Textvector1.Word.Clear();
//						Textvector1.Word.Append(abc);
//						Textvector1.BoxUIDRef= curBox;
//						Textvector1.StartIndex= NewTextList[a].StartIndex;
//						Textvector1.EndIndex= NewTextList[a].EndIndex;
//						ChangedTextList.push_back(Textvector1);		
//					}					
//			}
//		}
//		else
//		{
//			for(int32 a=0;a<tot2;a++)
//			{
//				int32 b=m;
//				
//				for(;b<tot1;b++)
//				{					
//					if((NewTextList[b].Word.Compare(kTrue, OriginalTextList[a].Word)==0))//(strcmp(Odoubl[b],Cdoubl[a])==0)
//						{
//							m=b+1;
//							break;
//						}
//					else
//						{
//							flag2 = 0;
//							for(int32 c=0;c<tot2;c++)
//							{
//								if(((NewTextList[b].Word.Compare(kTrue, OriginalTextList[c].Word))==0))//(strcmp(Odoubl[b],Cdoubl[c])==0)
//								{
//									flag2 = 1;
//								}
//							}
//							if(flag2 == 0)
//							{									
//								PMString abc("");
//								abc.Append(NewTextList[b].Word);
//								TextVector Textvector1;
//								Textvector1.Word.Clear();
//								Textvector1.Word.Append(abc);
//								Textvector1.BoxUIDRef= curBox;
//								Textvector1.StartIndex= NewTextList[b].StartIndex;
//								Textvector1.EndIndex= NewTextList[b].EndIndex;
//								ChangedTextList.push_back(Textvector1);
//							}												
//						}
//					}				
//				}
//			}
//
//			if(tot1> tot2)
//			{
//				for (int C = tot2; C< tot1; C++)
//				{
//					PMString abc("");
//					abc.Append(NewTextList[C].Word);
//					TextVector Textvector1;
//					Textvector1.Word.Clear();
//					Textvector1.Word.Append(abc);
//					Textvector1.BoxUIDRef= curBox;
//					Textvector1.StartIndex= NewTextList[C].StartIndex;
//					Textvector1.EndIndex= NewTextList[C].EndIndex;
//					ChangedTextList.push_back(Textvector1);
//				}
//			}
//			// Commented By Rahul ... no more needed now
//				//for(int32 s=0; s<ChangedTextList.size();s++)
//				//{	
//				//	ColorData.elementID=slugInfo.elementId;
//				//	ColorData.StartIndex=ChangedTextList[s].StartIndex;
//				//	ColorData.EndIndex=ChangedTextList[s].EndIndex; 
//				//	//this->ChangeColorOfText(textModel, ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex, GreenColour);
//				//	this->SetFocusForText(curBox,ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex); 
//				//	ColorDataList.push_back(ColorData);
//				//}
//			if(myText)
//				delete myText;
//	}

	
}
ErrorCode Refresh::DeleteImageFrame(const UIDRef &  graphicFrameUIDRef)
{
	 ErrorCode status = kFailure;
     do {
         /*if (this->IsTextFrame(graphicFrameUIDRef) == kFalse) {
             break;
         }*/

		 InterfacePtr<IPMUnknown> unknown(graphicFrameUIDRef,IID_IUNKNOWN);
		 int16 isTextFrame=Utils<IFrameUtils>()->IsTextFrame(unknown);
		 
		 
		 if(isTextFrame==0)
		 {	
			 InterfacePtr<ICommand> deleteCmd(CmdUtils::CreateCommand(kDeleteCmdBoss));
			 ASSERT(deleteCmd);
			 if (!deleteCmd) {
				 break;
			 }
			 deleteCmd->SetItemList(UIDList(graphicFrameUIDRef));
			 status = CmdUtils::ProcessCommand(deleteCmd);
			 ASSERT_MSG(status == kSuccess, "kDeleteCmdBoss failed");
			//break;
		 }
     } while(false);
     return status;

}
ErrorCode Refresh::DeleteTextFrame(const UIDRef &  graphicFrameUIDRef)
{
	
	 ErrorCode status = kFailure;
     do {
         /*if (this->IsTextFrame(graphicFrameUIDRef) == kFalse) {
             break;
         }*/

		 InterfacePtr<IPMUnknown> unknown(graphicFrameUIDRef,IID_IUNKNOWN);
		 int16 isTextFrame=Utils<IFrameUtils>()->IsTextFrame(unknown);
		if(isTextFrame==0)
		{					
			break;
		}
        InterfacePtr<ICommand> deleteCmd(CmdUtils::CreateCommand(kDeleteCmdBoss));
         ASSERT(deleteCmd);
         if (!deleteCmd) {
             break;
         }
         deleteCmd->SetItemList(UIDList(graphicFrameUIDRef));
         status = CmdUtils::ProcessCommand(deleteCmd);
         ASSERT_MSG(status == kSuccess, "kDeleteCmdBoss failed");
     } while(false);
     return status;

}

//added by sagar for custom table refresh like printlist
bool16 Refresh::refreshCutomTableInBoxNew(const UIDRef& tableUIDRef, int32 isInterFaceCall, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef)
{
	
	
		TagStruct & tStruct = tagStruct;
		IIDXMLElement * boxXMLPtr = tagStruct.tagPtr;
		XMLReference xmlRef = boxXMLPtr->GetXMLReference();

		XMLTagAttributeValue xmlTagAttrVal;
		getAllXMLTagAttributeValues(xmlRef,xmlTagAttrVal);
		//start temporary adjustment in code
		//the typeId and the parentId should always be attached to the table and not
		//to its parent tag.but there is some logical error in the datasprayer plugin
		//while attaching tag to the table.The correct typeId and the parentId are getting
		//attached to the frametag instead of table tag.But it should be rectified 
		//immediately.
		PMString attrVal;
		attrVal.Append(xmlTagAttrVal.typeId);

		double typeId = attrVal.GetAsDouble();
		attrVal.Clear();

		//Note rowno stored the PBObjectID.
		attrVal.Append(xmlTagAttrVal.parentID/*xmlTagAttrVal.rowno*/);
		double objectId = attrVal.GetAsDouble();
		attrVal.Clear();

		PMString colnoForTableFlag = xmlTagAttrVal.header; //xmlTagAttrVal.colno;
		//end temporary adjustment in code
		//Get the tableXMLPtr
		int32 childCount  = boxXMLPtr->GetChildCount();


		
			
		vector<double> vector_items_doc;
		map<double,double> map_headers;
		vector_items_doc.clear();

		{
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
				return 0;
				//break;//breaks the do{}while(kFalse)
			}
			
			InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
			if (graphicFrameHierarchy == nil) 
			{
				//CA("graphicFrameHierarchy is NULL");
				return 0;
				//break;
			}
							
			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
			if (!multiColumnItemHierarchy) {
				//CA("multiColumnItemHierarchy is NULL");
				//break;
			}

			InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
			if (!multiColumnItemTextFrame) {
				//CA("multiColumnItemTextFrame is NULL");
				return 0;
				//break;
			}
			InterfacePtr<IHierarchy>
			frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
			if (!frameItemHierarchy) {
				//CA("frameItemHierarchy is NULL");
				return 0;
				//break;
			}

			InterfacePtr<ITextFrameColumn>
			frameItemTFC(frameItemHierarchy, UseDefaultIID());
			if (!frameItemTFC) {
				//CA("!!!ITextFrameColumn");
				return 0;
				//break;
			}
			InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
			if (textModel == nil)
			{
				ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
				return 0;
				//break;//breaks the do{}while(kFalse)
			}
			UIDRef StoryUIDRef(::GetUIDRef(textModel));

			InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
			if(tableList==nil)
			{
				return 0;
				//break;//breaks the do{}while(kFalse)
			}

			int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

			if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
			{
				return 0;
				//break;//breaks the do{}while(kFalse)
			}

			for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
			{//for..tableIndex
				//CA("tableIndex");
				InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
				if(tableModel == nil)
					continue;//continues the for..tableIndex

				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
				if(tableCommands==NULL)
				{
					ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
					continue;//continues the for..tableIndex
				}
						
				UIDRef tableRef(::GetUIDRef(tableModel)); 
				//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
				IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
				XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
				UIDRef ContentRef = contentRef.GetUIDRef();
			

				GridArea headerArea = tableModel->GetHeaderArea();
				GridArea tableArea_new = tableModel->GetTotalArea();
				ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
				ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));


				while (iterTable2 != endTable2)
				{
					GridAddress gridAddress = (*iterTable2);         
					InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
					if(cellContent == nil) 
					{
						//CA("cellContent == nil");
						break;
					}
					InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
					if(!cellXMLReferenceData) {
						//CA("!cellXMLReferenceData");
						break;
					}
					XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
					++iterTable2;

					
					InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
					
					//Get the text tag attached to the text inside the cell.
					//We are providing only one texttag inside cell.
					if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
					{
						continue;
					}
					int32 NoofChilds = cellXMLElementPtr->GetChildCount();

					PMString a("childTagCnt :  ");
					a.AppendNumber(NoofChilds);
					//CA(a);
					for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
					{ // iterate thruogh cell's tags
						XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
						//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
						InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());

						
						//The cell may be blank. i.e doesn't have any text tag inside it.
						if(cellTextXMLElementPtr == nil)
						{
							continue;
						}
						
						
						PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
						
						double childId = strChildID.GetAsDouble();
						if(childId > -1 )
						{
							PMString itemid1("");
							itemid1.Append("itemidssss	:: "+strChildID);
							vector_items_doc.push_back(childId);
							vector_items_doc.erase(std::unique(vector_items_doc.begin(), vector_items_doc.end()), vector_items_doc.end());
							
							itemid1.Append("	::vector_items_doc	::");
							itemid1.AppendNumber((int32)vector_items_doc.size());
							//CA(itemid1);
						}
						
						PMString strHeaderPresent = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));

						map_headers.insert(map<double,double>::value_type(vector_items_doc.at(gridAddress.row),gridAddress.row));
						

					}//for
				}//while
			}//end of for
		}//end of block



		///ttt
		map<double,double>::iterator itr7;
		for(itr7=map_headers.begin();itr7!=map_headers.end();itr7++)
		{
			double ID=itr7->first;
			//itemids.push_back(ID);
		}
		///tttt
		for(int32 tableIndex = 0;tableIndex < childCount;tableIndex++)
		{//start for 1
			XMLReference tableXMLElementRef = boxXMLPtr->GetNthChild(tableIndex);
			//IIDXMLElement * tableXMLPtr = tableXMLElementRef.Instantiate();
			InterfacePtr<IIDXMLElement>tableXMLPtr(tableXMLElementRef.Instantiate());
			
			if(tableXMLPtr == nil)
			{	
				continue;
			}

			//CA(tableXMLPtr->GetTagString());
			if(tableXMLPtr == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableXMLPtr == nil");					
				return kFalse;
			}
			//Get all the attribute Values
			PMString tempStr;
			tempStr.Clear();
			//tempStr.Append(tableXMLPtr->GetAttributeValue("parentID"));
			//int32 objectId = tempStr.GetAsNumber();
			//tempStr.Clear();
				
			//tempStr.Append(tableXMLPtr->GetAttributeValue("typeId"));
			//int32 typeId = tempStr.GetAsNumber();
			//tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.parentTypeID);
			double parentTypeId =tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.LanguageID);
			double languageId = tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.sectionID);
			double sectionId = tempStr.GetAsDouble();
			tempStr.Clear();

			tempStr.Append(xmlTagAttrVal.isAutoResize);
			int32 autoResize = tempStr.GetAsNumber();
			tempStr.Clear();

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return kFalse;
			}
			//ptrIAppFramework->clearAllStaticObjects();
			//VectorScreenTableInfoPtr tableInfo=
			//	ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tStruct.sectionID,objectId);
			//if(!tableInfo)
			//{
			//	ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::refreshThisBox::tableInfo == nil");					
			//	return kFalse;
			//}
			//int32/*int64*/ numRows=0;
			//int32/*int64*/ numCols=0;

			//CItemTableValue oTableValue;
			//VectorScreenTableInfoValue::iterator it;

			//bool16 typeidFound=kFalse;
			//for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			//{
			//	CA("for(it = tableInfo->begin(); it!=tableInfo->end(); it++)");
			//	oTableValue = *it;
			//	if(oTableValue.getTableTypeID() == tStruct.typeId && oTableValue.getTableID() == tStruct.tableId)
			//	{
			//		CA("if(oTableValue.getTableTypeID() == tStruct.typeId && oTableValue.getTableID() == tStruct.tableId)");
			//		typeidFound=kTrue;				
			//		break;
			//	}
			//}


			//ptrIAppFramework->clearAllStaticObjects();
			VectorScreenTableInfoPtr tableInfo=
				ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tStruct.sectionID,objectId , tStruct.languageID);
			if(!tableInfo)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::Refresh::refreshThisBox::tableInfo == nil");					
				return kFalse;
			}
			int32/*int64*/ numRows=0;
			int32/*int64*/ numCols=0;

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;		
		
			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == tStruct.typeId && oTableValue.getTableID() == tStruct.tableId)
				{
					typeidFound=kTrue;
					break;
				}
			}

			if(!typeidFound)
			{
				//CA("typeidFound == kFalse");
				if(!isInterFaceCall)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::typeidFound == nil");
					bool16 alreadyExist = kFalse;
					if(tagStruct.whichTab == 3)
					{
						double productID =tagStruct.parentId;
								
						for(int index = 0; index<productsDeleted.size();index++ )
						{
							if(productsDeleted[index] == productID)
								alreadyExist = kTrue;
						}
						if(!alreadyExist)
							productsDeleted.push_back(productID);
					}

					if(tagStruct.whichTab == 4)
					{
						//CA("3333 tList[0].whichTab == 4");
						double itemID = tagStruct.parentId;								
						for(int index = 0; index<itemsDeleted.size();index++ )
						{
							if(itemsDeleted[index] == itemID)
								alreadyExist = kTrue;
						}
						//if(!alreadyExist)
							itemsDeleted.push_back(itemID);
					}

					//PMString updated = "";
					//updated.Append("productsDeleted = ");
					//updated.AppendNumber(productsDeleted.size());
					//updated.Append(" , itemsDeleted = ");
					//updated.AppendNumber(itemsDeleted.size());

					//CA(updated);

					//this->deleteThisBox(BoxUIDRef);
				}
				return kFalse;
			}

			numRows = static_cast<int32>(oTableValue.getTableData().size());
			if(numRows<=0)
			{
				return kFalse;
			}
			numCols =static_cast<int32> (oTableValue.getTableHeader().size());
			if(numCols<=0)
			{
				return kFalse;
			}

			bool8 isTranspose = oTableValue.getTranspose();
			//Note that don't take the literal meaning of row and column
			//it has to be taken w.r.t the isTranspose flag.
			
			if(isTranspose == kFalse)
			{
				//rows are sprayed in rows of the table on document
				if(colnoForTableFlag == "1")
					numRows = numRows + 1;//add for header row
			}
			else
			{
				//rows are sprayed in columns of the table on document
				//colums are sprayed in rows of the table on document
				int32 temp=0;
				temp = numRows;
				numRows = numCols;

				if(colnoForTableFlag == "1")
					temp = temp + 1;
				
				numCols = temp;
			}

			XMLContentReference xmlContentRef = tableXMLPtr->GetContentReference();
			InterfacePtr<IXMLReferenceData> tableXMLRef( xmlContentRef.Instantiate());
			InterfacePtr<ITableModel> tableModel(tableXMLRef,UseDefaultIID());
			if(tableModel == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableModel == nil");									
				return kFalse;
			}
			//check the number of rows and colums of the current document table
			RowRange rowR = tableModel->GetTotalRows();
			ColRange colR = tableModel->GetTotalCols();
			int32 numberOfDocTableRows = rowR.count;
			int32 numberOfDocTableCols = colR.count;
			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableCommands==nil");									
				return kFalse;
			}

			InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
			if(tableGeometry == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::refreshTaggedBox::tableGeometry == nil");									
				return kFalse;
			}
			PMReal rowHt = tableGeometry->GetRowHeights(numberOfDocTableRows-1);
			PMReal colWidth = tableGeometry->GetColWidths(numberOfDocTableCols-1);
			if(numberOfDocTableRows != numRows)
			{			
				if(numberOfDocTableRows < numRows)
				{		
					//we have to add the additional rows at the end
					int32 difference = numRows-numberOfDocTableRows;
					RowRange rr(numberOfDocTableRows-1,difference);
					ErrorCode result = tableCommands->InsertRows(rr, Tables::eAfter, rowHt);
				}
				else
				{
					//we have to delete the surplus rows from bottom
					int32 difference = numberOfDocTableRows - numRows;
					RowRange rr(numberOfDocTableRows-difference,difference);
					ErrorCode result = tableCommands->DeleteRows(rr);
				}
			}
			if(numberOfDocTableCols != numCols)
			{
				if(numberOfDocTableCols < numCols)
				{
					//we have to add the additional cols at the end
					int32 difference = numCols-numberOfDocTableCols ;
					ColRange cr(numberOfDocTableCols-1,difference);
					ErrorCode result = tableCommands->InsertColumns(cr, Tables::eAfter,colWidth);
				}
				else
				{
					//we have to delete the surplus rows from bottom
					int32 difference = numberOfDocTableCols - numCols ;
					ColRange cr(numberOfDocTableCols-difference-1,difference);
					ErrorCode result = tableCommands->DeleteColumns(cr);
				}
			}

			//When the control comes to this part..we have ensured that both the document
			//table and the database table have same number of rows and columns.
			//So now delete each tag from the cell and add new tag.

			//CA("both the document table and the database table have same number of rows and columns.");

			bool16 alreadyExist = kFalse;
			
			if(tStruct.whichTab == 4 && (!gIsInterFaceCall))
			{
				//CA("tStruct.whichTab == 4");
				double itemID = tStruct.parentId;
				PMString temp = "";
				temp.AppendNumber(itemID);
				//CA("temp = " + temp);
					
				for(int index = 0; index<itemsUpdated.size();index++ )
				{
					if(itemsUpdated[index] == itemID)
						alreadyExist = kTrue;
				}

				if(!alreadyExist)
					itemsUpdated.push_back(itemID);

				Report objReport;
				objReport.attributeId = 0;
				objReport.id = tStruct.parentId;

				tempItemList.push_back(objReport);
			}

			vector<double>  vec_tableheaders = oTableValue.getTableHeader();		
			vector<vector<PMString> >  vec_tablerows = oTableValue.getTableData();
			vector<double>  vec_items = oTableValue.getItemIds();
			//vector<PMString>::iterator colIterator;
			
			//Get the TextModel
			InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
			if(tableTextContent == nil)
			{
				//CA("tableTextContent == nil");
				continue;
			}
			UIDRef tableTextModelUIDRef = tableTextContent->GetTextModelRef();

			InterfacePtr<ITextModel> textModel(tableTextContent->QueryTextModel()/*,UseDefaultIID()*/);
			if(textModel == nil)
			{
				//CA("textModel == nil");
				continue;
			}

			int32 headerEntity1 = 0;
			int32 headerEntity2 = 0;
			
			int32 headerEntity = 0;//headerEntity can be row or column depending on the transpose
			vector<double> :: iterator colIterator;
		
			if(colnoForTableFlag == "1")
			{
				for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity++)
				{//for loop of Header
					//Table Header tag have typeId = -2..ItemId is not required.
					//ID is taken from the vec_tableheaders elements.
					//Iterate through the first row of the document table.
					GridAddress cellAddr;
					if(isTranspose == kFalse)
					{
						cellAddr.Set(0,headerEntity);
					}
					else
						cellAddr.Set(headerEntity,0);

					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName((*colIterator),languageId);
					PMString textToInsert("");
					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						textToInsert=dispname;					
					}
					else
						textToInsert=iConverter->translateString(dispname);
					
					//Update the Tag and its attribute values.
					PMString tagName("ATTRID_");
					tagName.AppendNumber(PMReal((*colIterator)));

					///start
					PMString attrVal;
					//XMLTagAttributeValue xmlTagAttrVal;
					//tagName
					xmlTagAttrVal.tagName = tagName;
					//ID..it is tableHeaderID			
					attrVal.Clear();
					attrVal.AppendNumber(PMReal((*colIterator)));
					xmlTagAttrVal.ID =attrVal;
					//typeId..it is -2 for tableHeader row

					attrVal.Clear();
					attrVal.AppendNumber(4);
					//CA(" index = "+attrVal);
					xmlTagAttrVal.index = attrVal;


					attrVal.Clear();
					attrVal.AppendNumber(1);//attrVal.AppendNumber(-2);
					xmlTagAttrVal.header=attrVal;//xmlTagAttrVal.typeId=attrVal;
					AddTagToCellText
						(
							cellAddr,
							tableModel,
							textToInsert,
							textModel,
							xmlTagAttrVal
						);						
						
				}//end for loop of Header		
			
				headerEntity1 = 1;
			}
			//For each row the ID element(attribute Id) is same but the typeId(ItemID) changes.
			//ID is taken from the vec_tableheaders
			//vector<vector<PMString> >::iterator rowIterator;
			
			//int32 headerEntity1 = 1;
			//int32 headerEntity2 = 0;
			//When isTranspose == kFalse
			//	headerEntity1 is row
			//	heaerEntity2 is column
			//When isTranspose == kTrue
			//	headerEntity1 is column
			//	headerEntity2 is row		
			//for(int32 r=0;r<totalRowsofcustumtable;r++)
			//{
			//	PMString a("Second ID :");
			//	a.AppendNumber(ItemIdsForIndesignTable.at(r));
			//	//CA(a);
			//}
			//for(int32 c=0;c<totalsColsofcustumtable;c++)
			//{
			//	PMString a("SecondAttribute :");
			//	a.AppendNumber(AttibuteIdsForIndesignTable.at(c));
			//	//CA(a);
			//}
			if(iscontentbuttonselected==kTrue)
			{
				//CA("Content is selected");
				//CAI(headerEntity1);
				//CAI(totalRowsofcustumtable);
				for(int32 r=headerEntity1;r<numberOfDocTableRows;r++)
				{
					for(int32 c=0;c<numberOfDocTableCols;c++)
					{
						PMString dataToBeSprayed("");
						PMString strdataToBeSprayed("");

						PMString set1("");
						set1.Append(" ItemIdsForIndesignTable.size() ::  ");
						set1.AppendNumber((int32)ItemIdsForIndesignTable.size());
						set1.Append(" :: numberOfDocTableRows ::  ");
						set1.AppendNumber(numberOfDocTableRows);
						set1.Append(" :: AttibuteIdsForIndesignTable.size() ::  ");
						set1.AppendNumber((int32)AttibuteIdsForIndesignTable.size());
						set1.Append(" :: AttibuteIdsForIndesignTable ::  ");
						set1.AppendNumber(numberOfDocTableCols);
						//CA(set1);	
						
						//if(ItemIdsForIndesignTable.size()>=numberOfDocTableRows && AttibuteIdsForIndesignTable.size()>=numberOfDocTableCols)
						{
							bool16 itemFound = kFalse;
							if(vector_items_doc.size()>=(r-headerEntity1))
							{
								//CA("if(vector_items_doc.size()>=r-headerEntity1)");
								for(int32 ii=0;ii<vector_items_doc.size();ii++)
								{
									if(vector_items_doc.at(ii) == ItemIdsForIndesignTable.at(r-headerEntity1))
									{
										itemFound = kTrue;
									}
								}
								
							}
							if(itemFound == kFalse)
							{
								//CA("not present");
								break;
							}
							strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIdsForIndesignTable.at(r-headerEntity1),AttibuteIdsForIndesignTable.at(c),languageId, CurrSectionid, /*kTrue*/kFalse);//23OCT09//Amit
							PMString p("strdataToBeSprayed : ");
							p.Append(strdataToBeSprayed);
							//CA(p);

							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								dataToBeSprayed=(strdataToBeSprayed);					
							}
							else
								dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);	
							
							GridAddress cellAddr;
							if(isTranspose == kFalse)
							{
								//CA("isTranspose == kFalse");
								cellAddr.Set(r,c);		
							}
							else
							{
								//CA("isTranspose == kTrue");
								cellAddr.Set(c,r);
							}
							PMString q("headerEntity1 : ");
							q.AppendNumber(headerEntity1);
							q.Append("\n");
							q.Append("headerEntity2 : ");
							q.AppendNumber(headerEntity2);
							//CA(q);

							PMString tagName("ATTRID_");
							tagName.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
							//CA(tagName);

							///start
							PMString attrVal;
							//XMLTagAttributeValue xmlTagAttrVal;
							//tagName
							xmlTagAttrVal.tagName = tagName;
							//ID..it is attributeID			
							attrVal.Clear();
							attrVal.AppendNumber(PMReal(AttibuteIdsForIndesignTable.at(c)));
							xmlTagAttrVal.ID =attrVal;

							attrVal.Clear();
							attrVal.AppendNumber(PMReal(ItemIdsForIndesignTable.at(r-headerEntity1)));
							xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;

							attrVal.Clear();
							attrVal.AppendNumber(-1);
							xmlTagAttrVal.header = attrVal;
							
							attrVal.Clear();
							attrVal.AppendNumber(1);
							xmlTagAttrVal.childTag = attrVal;
							
							attrVal.Clear();
							attrVal.AppendNumber(4);
							//CA(" index = "+attrVal);
							xmlTagAttrVal.index = attrVal;

							PMString str("string : ");
							str.Append(dataToBeSprayed);
							//CA(str);
							AddTagToCellText
							(
								cellAddr,
								tableModel,
								dataToBeSprayed,
								textModel,
								xmlTagAttrVal

							);
						}//end of if
					}
				}
			}
			else if(issrtucturebuttenselected==kTrue)
			{
				//CA("Structure is selcted");
			vector<double> :: iterator rowIterator;// = vec_items.begin();
			//for(rowIterator=vec_tablerows.begin(); rowIterator!=vec_tablerows.end(); rowIterator++,rowNo++,itemIterator++)
				for(rowIterator=vec_items.begin(); rowIterator!=vec_items.end(); rowIterator++,headerEntity1++)
				{//start for 2
					//For each column the ID is different but the ItemID is same.
					vector<double>::iterator colIterator;
					headerEntity2 = 0;
					for(colIterator=vec_tableheaders.begin();colIterator!=vec_tableheaders.end();colIterator++,headerEntity2++)
					{				
						//row data feeding
						//ID is taken from the vec_tableheaders elements.
						//Iterate through the first row of the document table.	
						PMString dataToBeSprayed("");
						PMString strdataToBeSprayed("");
						/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific((*colIterator)) == kTrue){
							VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId((*rowIterator),(*colIterator),sectionId);
							if(vecPtr != NULL)
							{
								if(vecPtr->size()> 0)
									strdataToBeSprayed = vecPtr->at(0).getObjectValue();
								delete vecPtr;
							}
						}
						else*/
						{
							strdataToBeSprayed = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId((*rowIterator),(*colIterator),languageId, CurrSectionid, /*kTrue*/kFalse);//23OCT09//Amit
						}
						PMString p("strdataToBeSprayed : ");
						p.Append(strdataToBeSprayed);
						//CA(p);
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(!iConverter)
						{
							dataToBeSprayed=(strdataToBeSprayed);					
						}
						else
							dataToBeSprayed=iConverter->translateString(strdataToBeSprayed);				
					///CA(dataToBeSprayed);
						GridAddress cellAddr;
						if(isTranspose == kFalse)
						{
							//CA("if(isTranspose == kFalse)");
							cellAddr.Set(headerEntity1,headerEntity2);
						}
						else
						{
							//CA("if(isTranspose == kTrue)");
							cellAddr.Set(headerEntity2,headerEntity1);
						}

						PMString q("headerEntity1 : ");
						q.AppendNumber(headerEntity1);
						q.Append("\n");
						q.Append("headerEntity2 : ");
						q.AppendNumber(headerEntity2);
						//CA(q);
						//Update the Tag and its attribute values.
						PMString tagName("ATTRID_");
						tagName.AppendNumber(PMReal((*colIterator)));
						//CA(tagName);

						///start
						PMString attrVal;
						//XMLTagAttributeValue xmlTagAttrVal;
						//tagName
						xmlTagAttrVal.tagName = tagName;
						//ID..it is attributeID			
						attrVal.Clear();
						attrVal.AppendNumber(PMReal((*colIterator)));
						xmlTagAttrVal.ID =attrVal;
						//typeId..it is 
						attrVal.Clear();
						attrVal.AppendNumber(PMReal((*rowIterator)));
						xmlTagAttrVal.childId=attrVal;//xmlTagAttrVal.typeId=attrVal;
						

						
						attrVal.Clear();
						attrVal.AppendNumber(4);
						//CA(" index = "+attrVal);
						xmlTagAttrVal.index = attrVal;

						attrVal.Clear();
						attrVal.AppendNumber(-1);
						xmlTagAttrVal.header = attrVal;
						
						attrVal.Clear();
						attrVal.AppendNumber(1);
						xmlTagAttrVal.childTag = attrVal;
						

						AddTagToCellText
						(
							cellAddr,
							tableModel,
							dataToBeSprayed,
							textModel,
							xmlTagAttrVal

						);
					
					}			
				}//end for 2
			}
		}//end for 1	
		return kTrue;	

}
//till here	sagar


// Added by Rahul to collect Section Data for avialable sections from JSON call.
bool16 Refresh::getSectionDataforBoxList(UIDList selectUIDList )
{
		CSprayStencilInfo objCSprayStencilInfo;
		//ptrIAppFramework->GetSectionData_clearSectionDataCache();	
		bool16 result = getStencilInfo(selectUIDList,objCSprayStencilInfo);
		PMString itemFieldIds("");
		PMString itemAssetTypeIds("");
		PMString itemGroupFieldIds("");
		PMString itemGroupAssetTypeIds("");
		PMString listTypeIds("");
		PMString listItemFieldIds("");
		PMString itemIds("");
		PMString itemGroupIDs("");
        PMString tableIds("");
        PMString itemAttributeGroupIds("");
		bool16 isSprayItemPerFrameFlag = kFalse;

		set<double> setOfItemFieldIds;
		set<double> setOfItemAssetTypeIds;
		set<double> setOfItemGroupFieldIds;
		set<double> setOfItemGroupAssetTypeIds;
		set<double> setOfListTypeIds;
		set<double> setOfListItemFieldIds;
		set<double> setOfProductPVAssetIds;
        set<double> setOfAdvanceTableIds;
		

		double langId = 91;
		// Added by Apsiva for New JSON section Call.
		// collecting listTypes for Item Group and iTem
		if(  objCSprayStencilInfo.dBTypeIds.size() > 0)
		{
			listTypeIds = converIntVectorToUniqueIdString(objCSprayStencilInfo.dBTypeIds);
			
			/*for(int ct =0; ct < objCSprayStencilInfo.dBTypeIds.size(); ct++ )
			{
				listTypeIds.AppendNumber(objCSprayStencilInfo.dBTypeIds.at(ct));
				if(ct != (objCSprayStencilInfo.dBTypeIds.size() -1) )
				{
					listTypeIds.Append(",");
				}
			}*/
		}
        if(  objCSprayStencilInfo.HyTypeIds.size() > 0)
        {
            tableIds = converIntVectorToUniqueIdString(objCSprayStencilInfo.HyTypeIds);
        }
		// collecting Copy Attributes for Item Group
		if(  objCSprayStencilInfo.ProductAttributeIds.size() > 0)
		{
			itemGroupFieldIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.ProductAttributeIds);
			/*for(int ct =0; ct <objCSprayStencilInfo.ProductAttributeIds.size(); ct++ )
			{
				itemGroupFieldIds.AppendNumber(objCSprayStencilInfo.ProductAttributeIds.at(ct));
				if(ct != (objCSprayStencilInfo.ProductAttributeIds.size() -1) )
				{
					itemGroupFieldIds.Append(",");
				}
			}*/
		}

		if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
		{
			if(itemGroupFieldIds.NumUTF16TextChars () > 0)
			{
				itemGroupFieldIds.Append(",");
			}
			
			itemGroupFieldIds.Append(converIntVectorToUniqueIdString(objCSprayStencilInfo.productPVAssetIdList));
			/*for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
			{
				itemGroupFieldIds.AppendNumber(objCSprayStencilInfo.productPVAssetIdList[i]);
				if( i < objCSprayStencilInfo.productPVAssetIdList.size() -1 )
					itemGroupFieldIds.Append(",");	
			}*/
		}


		// collecting Copy Attributes for Item
		if(  objCSprayStencilInfo.itemAttributeIds.size() > 0)
		{
			itemFieldIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.itemAttributeIds);
		}
    
        if(  objCSprayStencilInfo.itemAttributeGroupIds.size() > 0)
        {
            itemAttributeGroupIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.itemAttributeGroupIds);
        }

		if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
		{
			if(itemFieldIds.NumUTF16TextChars () > 0)
			{
				itemFieldIds.Append(",");
			}

			itemFieldIds.Append(converIntVectorToUniqueIdString(objCSprayStencilInfo.itemAttributeIds));
		}

		// collecting Asset type Ids for Item Group
		if(  objCSprayStencilInfo.ProductAssetIds.size() > 0)
		{
			itemGroupAssetTypeIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.ProductAssetIds);
			/*for(int ct =0; ct < objCSprayStencilInfo.ProductAssetIds.size(); ct++ )
			{
				itemGroupAssetTypeIds.AppendNumber(objCSprayStencilInfo.ProductAssetIds.at(ct));
				if(ct != (objCSprayStencilInfo.ProductAssetIds.size() -1) )
				{
					itemGroupAssetTypeIds.Append(",");
				}
			}*/
		}
		// collecting Asset type Ids for Item
		if(  objCSprayStencilInfo.itemAssetIds.size() > 0)
		{
			itemAssetTypeIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.itemAssetIds);
			/*for(int ct =0; ct < objCSprayStencilInfo.itemAssetIds.size(); ct++ )
			{
				itemAssetTypeIds.AppendNumber(objCSprayStencilInfo.itemAssetIds.at(ct));
				if(ct != (objCSprayStencilInfo.itemAssetIds.size() -1) )
				{
					itemAssetTypeIds.Append(",");
				}
			}*/
		}

		// collecting listItemFieldIds for Item
		if(  objCSprayStencilInfo.childItemAttributeIds.size() > 0)
		{
			listItemFieldIds =  converIntVectorToUniqueIdString(objCSprayStencilInfo.childItemAttributeIds);
			/*for(int ct =0; ct < objCSprayStencilInfo.childItemAttributeIds.size(); ct++ )
			{
				listItemFieldIds.AppendNumber(objCSprayStencilInfo.childItemAttributeIds.at(ct));
				if(ct != (objCSprayStencilInfo.childItemAttributeIds.size() -1) )
				{
					listItemFieldIds.Append(",");
				}
			}*/
		}
		langId = objCSprayStencilInfo.langId;

		isSprayItemPerFrameFlag = objCSprayStencilInfo.isSprayItemPerFrame;

		ptrIAppFramework->clearAllStaticObjects();

		isEmptySectionFound = kFalse;
    
        bool16 useRefreshbyAttribute = refreshTableByAttribute;
    
        if (objCSprayStencilInfo.isHyTable)
            useRefreshbyAttribute = kFalse; // overwite refreshTableByAttribute flag for Advance table cell refresh.

		if(sectionIds.size() > 0)
		{
			UniqueIds::iterator itr;
			for(itr = sectionIds.begin(); itr != sectionIds.end(); ++itr)
			{
				double sectionId = *itr;
				
				multiSectionMap::iterator itr1;
				itr1 = MapForItemsPerSection.find(sectionId);
				itemIds = "";
				if(itr1 != MapForItemsPerSection.end())
				{
					UniqueIds currentitemIds = itr1->second;
					UniqueIds::iterator itemIdsItr;
					for(itemIdsItr = currentitemIds.begin(); itemIdsItr != currentitemIds.end(); ++itemIdsItr)
					{						
						if( itemIdsItr == currentitemIds.begin() )
						{
							itemIds.AppendNumber(PMReal(*itemIdsItr));
						}
						else
						{
							itemIds.Append(",");
							itemIds.AppendNumber(PMReal(*itemIdsItr));
						}

					}
				}

				itemGroupIDs = "";
				itr1 = MapForProductsPerSection.find(sectionId);
				if(itr1 != MapForProductsPerSection.end())
				{
					UniqueIds::iterator productIdItr;
					UniqueIds productIds = itr1->second;
					for(productIdItr = productIds.begin(); productIdItr != productIds.end(); ++productIdItr)
					{						
						if( productIdItr == productIds.begin() )
						{
							itemGroupIDs.AppendNumber(PMReal(*productIdItr));
						}
						else
						{
							itemGroupIDs.Append(",");
							itemGroupIDs.AppendNumber(PMReal(*productIdItr));
						}
					}
				}

 
				bool16 resultFlag = ptrIAppFramework->EventCache_setCurrentSectionData( *itr, langId ,itemGroupIDs, itemIds,  itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, useRefreshbyAttribute, kFalse, itemAttributeGroupIds);
				if(resultFlag == kFalse)
				{
					if(!isEmptySectionFound)
						isEmptySectionFound = kTrue;
				}
			}
		}
		else
		{
			ptrIAppFramework->LogError("No sectionIds captured in getstencil info so no refresh possible.");
			return kFalse;
		}

		return kTrue;
}


PMString Refresh::converIntVectorToUniqueIdString(vector<double> idVector)
{
	set<double> setOfInt;
	PMString returnString("");
	for(int ct =0; ct < idVector.size(); ct++ )
	{
		setOfInt.insert(idVector.at(ct));
				
	}
	if (setOfInt.size() > 0) 
	{
		int32 ct =0;
		for (set<double>::iterator i = setOfInt.begin(); i != setOfInt.end(); i++)
		{
			double Id = *i;
			returnString.AppendNumber(PMReal(Id));
			if(ct < setOfInt.size()-1)
			{
				returnString.Append(",");
			}
			ct++;
		}
	}
	setOfInt.clear();
	return returnString;
}
