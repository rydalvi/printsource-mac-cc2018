#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "CAlert.h"
#include "PMString.h"
#include "Refresh.h"
#include "IRefreshContent.h" //****   
#include "RfhDlgDialogObserver.h"


class RefreshContent : public CPMUnknown<IRefreshContent>
{
public:
	RefreshContent(IPMUnknown *boss);
	~RefreshContent(){};

	bool16 refreshThisBox(UIDRef&, PMString&);
	void generateRefreshReport(int32 doRefreshCasetype);  //*****Added	
};

CREATE_PMINTERFACE (RefreshContent, kRefreshContentImpl)

RefreshContent::RefreshContent(IPMUnknown* boss):CPMUnknown<IRefreshContent>(boss)
{
	
}

bool16 RefreshContent::refreshThisBox(UIDRef& BoxRef, PMString& ImagePath)
{	
	Refresh refreshObj;
	bool16 isInterFaceCall = kTrue;
	return refreshObj.refreshThisBox(BoxRef, ImagePath, isInterFaceCall);
}

void RefreshContent::generateRefreshReport(int32 doRefreshCasetype)
{	
//CAlert::InformationAlert(__FUNCTION__);
	/*Refresh refreshObj;
	refreshObj.doRefresh(doRefreshCasetype);*/
	
	RfhDlgDialogObserver rfh(this);
	rfh.dummyRefrsehForRepoert(4);

	
}
