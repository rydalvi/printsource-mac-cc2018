#ifndef __REFRESHDATA_H__
#define __REFRESHDATA_H__

#include "VCPluginHeaders.h"
#include "IDFile.h"
#include "TagStruct.h"
#include <vector>

using namespace std;

class RefreshData
{
public:
	UIDRef BoxUIDRef;
	int32 StartIndex;
	int32 EndIndex;
	UID boxID;
	double objectID;
	double publicationID;
	double elementID;
	PMString name;
	bool16 isSelected;
	bool16 isObject;
	bool16 isTaggedFrame;
	bool16 isProcessed;
	bool16 isTableFlag;
	bool16 isImageFlag;
	double TypeID;
	int32 whichTab;
	int32 DesignerAction;
	double PBObjectID;
	IDFile sysFile;
	UIDRef DocumentUIDRef;
	double LanguageID;

	int32 header;		// if 1 indicates its a Header (Display Name ) field
	bool16 isEventField; //for Event level attribute if true its
	int32 dataType;		// Type of Attribute -1= Normal Attributes, 1 = Possible Value, 2= Image Description , 3 = Image Name ,4 = Table name , 5 = Table Comments, 6= AttributeGroup
	double childId;	// in case of Table Child Item's ID
	int32 catLevel;		// Category Level in case of Category Attributes otherwise -1
	int32 imageIndex;	// image index for PV, Brand, Manufacturer & Supplier images.
	int32 flowDir;		// 0 = Horizontal Flow, 1= Verticle Flow, -1 = No flow is define its in case of all Images or PV images
	int32 childTag;		// if 1 indicates Child item tag 
	int32 tableType;	// displays different types of tables 1 = DB Table, 2 = Custom Table, 3 = Advance Table, 4 = Component , 5 = Accessaries, 6 XRef, 7 = All Standard Table, 8 = Table Inside Table Cell
	double tableId;		// Table ID after spray

	double parentTypeId;

	int32 isAutoResize;
	int32 isSprayItemPerFrame;
	
	/*int32 field1;
	int32 field2;
	int32 field3;
	int32 field4;
	int32 field5;*/
    int32 pageNo;
    PMString value;
    TagStruct tStruct;

	RefreshData()
	{
		boxID=kInvalidUID;
		objectID=-1;
		publicationID=-1;
		elementID=-1;
		isSelected=kFalse;
		isObject=kFalse;
		isTaggedFrame=kFalse;
		isProcessed=kFalse;
		isTableFlag=kFalse;
		StartIndex=0;
		EndIndex=0;
		TypeID=-1;
		whichTab=-1;
		isImageFlag=kFalse;
		DesignerAction=-1;
		PBObjectID=-1;
		LanguageID = -1;

		header = -1;		
		isEventField = kFalse; 
		dataType = -1;		
		childId = -1;	
		catLevel = -1;	
		
		imageIndex = -1;	
		flowDir = -1;		
		
		childTag = -1;		
		
		tableType = -1;	
		tableId = -1;	

		parentTypeId = -1;

		isAutoResize = -1;
		isSprayItemPerFrame = -1;
			
		/*field1 = -1;
		field2 = -1;
		field3 = -1;
		field4 = -1;
		field5 = -1;*/
        pageNo = -1;
        value = "";
	}
};

typedef vector<RefreshData> RefreshDataList;

#endif
