//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "PaletteRefUtils.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "ILayoutUIUtils.h"
#include "ITagReader.h"
#include "ISpecialChar.h"
// Project includes:
#include "RfhID.h"
#include "RfhActionComponent.h"
#include "IWindow.h"
#include "MediatorClass.h"
#include "ITextControlData.h"
#include "RfhSelectionObserver.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
#include "RefreshData.h"
#include "IDialogMgr.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "IBookManager.h"
#include "IBook.h"
#include "RfhDlgDialogObserver.h"
#include "IBookContentMgr.h"
#include "IBookContent.h"


#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("RfhActionComponent.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs10_refreshcontent

*/

bool16 FlgToCheckMenuAction= kFalse;
int32 Flag1;
IPaletteMgr* RfhActionComponent::palettePanelPtr=0;
int GroupFlag= 1;
bool16 HiliteFlag= kFalse;
extern RefreshDataList rDataList;

/// Global Pointers
ITagReader* itagReader = NULL;
IAppFramework* ptrIAppFramework = NULL;
ISpecialChar*  iConverter = NULL;
///////////
extern K2Vector<PMString> bookContentNames;

bool16 IsDeleteFlagSelected = kFalse;  //--------
bool16 showSummaryDialog = kFalse;
IDialog* Summarydialog = NULL;

//bool16 refreshTableByAttribute = kTrue;
//class RfhActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		RfhActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//
//
//};
//
///* CREATE_PMINTERFACE
// Binds the C++ implementation class onto its
// ImplementationID making the C++ code callable by the
// application.
//*/
CREATE_PMINTERFACE(RfhActionComponent, kRfhActionComponentImpl)

/* RfhActionComponent Constructor
*/
RfhActionComponent::RfhActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void RfhActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	//CA("RfhActionComponent::DoAction");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoAction");	

	switch (actionID.Get())
	{	
		//case kRfhAboutActionID:
	//	{
		//	this->DoAbout();
	//		break;
	//	}
					
		
		case kRfhMenuItem1ActionID:
		{	
			this->DoMenuItem1(ac);
			break;
		}

		case kRfhMenuItem2ActionID:
		{	
			this->DoMenuItem2(ac);
			break;
		}

	/*	case kRfhMenuItem3ActionID:
		{	
			this->DoMenuItem3(ac);
			break;
		}*/

		case kRfhMenuItem4ActionID:
			{
				this->DoMenuItem4(ac);
				break;
			}

		case kRfhPanelPSMenuActionID:
		case kRfhPanelWidgetActionID:
		{	
			FlgToCheckMenuAction= kTrue;
			this->DoPalette();
			break;
		}

		case kRfhBookDlgWidgetActionID:
		{	
			//CA("kRfhBookDlgWidgetActionID");
			Mediator::isFrameRefresh = kFalse;
			//FlgToCheckMenuAction= kTrue;
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent:: case kRfhBookDlgWidgetActionID");
			this->DoDialog();
			//ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent:: after 	this->DoDialog();");
			break;
		}
//---------------
		case kRfhMenuDeleteItemFrameActionID:
		{	
			//FlgToCheckMenuAction= kTrue;
			IsDeleteFlagSelected = !IsDeleteFlagSelected;
			break;
		}
//////////////// chetan
//		case kRfhMenuRefreshTabByAttributeActionID:
//		{	
//			//FlgToCheckMenuAction= kTrue;
//			refreshTableByAttribute = !refreshTableByAttribute;
//			break;
//		}
		
//////////// chetan end

		default:
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent:: case default");
			break;
		}
	}
}

/* DoAbout
*/
void RfhActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kRfhAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

/* DoMenuItem1
*/
void RfhActionComponent::DoMenuItem1(IActiveContext* ac)
{
	RfhSelectionObserver qwe(this);	
	qwe.reSortTheListForObject();
	qwe.fillDataInListBox(2);
	GroupFlag= 2;	/// Group By Object
}

void RfhActionComponent::DoMenuItem2(IActiveContext* ac)
{
	GroupFlag= 3;  //Group by Element
	RfhSelectionObserver qwe(this);
	qwe.reSortTheListForElem();
	qwe.fillDataInListBox(3);
	//CAlert::InformationAlert("Group by Element");
}

void RfhActionComponent::DoMenuItem3(IActiveContext* ac)
{
	if(!HiliteFlag)
		HiliteFlag= kTrue;
	else
		HiliteFlag=kFalse;
	//CAlert::InformationAlert("Hilite changed content");
}

void RfhActionComponent::DoMenuItem4(IActiveContext* ac)
{
	GroupFlag= 1; //Unique Attributes in Doc
	RfhSelectionObserver qwe(this);
	qwe.reSortTheListForUniqueAttr();
	qwe.fillDataInListBox(1);
	//CAlert::InformationAlert("Unique Attributes in Doc");
}

void RfhActionComponent::DoPalette()
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::DoPalette::iApplication is nil");
			break;
		}
/*		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::DoPalette::iPaletteMgr is nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(iPanelMgr == nil)
		{			
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::DoPalette::iPanelMgr is nil");
			break;
		}	
*/
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{			
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::DoPalette::iPanelMgr is nil");
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByMenuID(kRfhPanelWidgetActionID);
		DocWchUtils::StartDocResponderMode();
		//Mangesh Code for resizing initially
		if(FlgToCheckMenuAction)
		{
			IControlView* icontrol = iPanelMgr->GetVisiblePanel(kRfhPanelWidgetID);
			if(!icontrol)
			{
				return;
			}
		//	icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));
		//	icontrol->Invalidate();
		}
		//end Mangesh Code for resizing initially
//		RfhActionComponent::palettePanelPtr=iPaletteMgr;		
//A//		IsDeleteFlagSelected = kTrue;
		
	}while(kFalse);
}

void RfhActionComponent::UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{	
	
	do
	{
		if(ptrIAppFramework == NULL)
		{  			
			IAppFramework* ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFrameworkOne == nil)
				break;
			ptrIAppFramework = ptrIAppFrameworkOne;
		}

		if(iConverter == NULL)
		{	
			ISpecialChar* iConverterOne(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverterOne)
			{	
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!iConverterOne");
				break;
			}
			iConverter = iConverterOne;
		}

		if(itagReader ==NULL)
		{	
			ITagReader* itagReaderOne
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReaderOne)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!itagReaderOne");			
				break;
			}
			itagReader = itagReaderOne;
		}		
		
	}while(0);
	
	//CA("Inside RfhActionComponent::UpdateActionStates");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->getLoginStatus();

	IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
	//if(result  && iDoc)
	//{
	//	//CA("After Login");
	//	iListPtr->SetNthActionState(0,kEnabledAction);
	//	iListPtr->SetNthActionState(1,kEnabledAction); //Hilite changed content
	//	iListPtr->SetNthActionState(2,kEnabledAction); // Group by Element
	//	
	//	if(GroupFlag == 1)
	//	{
	//		iListPtr->SetNthActionState(2,kSelectedAction); 
	//		iListPtr->SetNthActionState(1,kEnabledAction);
	//		iListPtr->SetNthActionState(0,kEnabledAction);
	//	}
	//	else if(GroupFlag == 2)
	//	{
	//		iListPtr->SetNthActionState(1,kSelectedAction);
	//		iListPtr->SetNthActionState(0,kEnabledAction);
	//		iListPtr->SetNthActionState(2,kEnabledAction);
	//	}
	//	else if(GroupFlag == 3)
	//	{
	//		iListPtr->SetNthActionState(0,kSelectedAction);
	//		iListPtr->SetNthActionState(1,kEnabledAction);
	//		iListPtr->SetNthActionState(2,kEnabledAction);
	//	}	
	//}
	//else
	//{
	//	iListPtr->SetNthActionState(0,kDisabled_Unselected);
	//	iListPtr->SetNthActionState(1,kDisabled_Unselected);
	//	iListPtr->SetNthActionState(2,kDisabled_Unselected);
	//}

	for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	{
		ActionID actionID = iListPtr->GetNthAction(iter);		
		if(actionID == kRfhBookDlgWidgetActionID)
		{ 	
			InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::bookManager == nil");
				return ; 
			}
			
			IBook * activeBook = bookManager->GetCurrentActiveBook();
			if(activeBook == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::RfhActionComponent::UpdateActionStates::activeBook == nil");
				return ;			
			}
			PMString bookName = activeBook->GetBookTitleName();
			InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				CA("This book doesn't have a book content manager!  Something is wrong.");
				return;
			}

			//RfhDlgDialogObserver rfhDlgDialogObserverObj(this); //og
			//RfhDlgDialogObserver rfhDlgDialogObserverObj(widget);

			//if(result && rfhDlgDialogObserverObj.GetBookContentNames(bookContentMgr).size() > 0)
			if(result && this->GetBookContentNames(bookContentMgr).size() > 0)
				iListPtr->SetNthActionState(iter,kEnabledAction);
			else
				iListPtr->SetNthActionState(iter,kDisabled_Unselected);
		}

		switch(actionID.Get())
		{
			case kRfhPanelPSMenuActionID:
			case kRfhPanelWidgetActionID:
			{ 		
					//PMString string("Setting ActionState", -1, PMString::kNoTranslate);
					//CAlert::InformationAlert(string);
				if(result && iDoc)
				{
					InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); ///Cs4
					ASSERT(app);
					if(!app)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!app");
						return;
					}
/*	/////////	CS3 Change
					InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!paletteMgr");
						return;
					}
					InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!panelMgr");
						return;
					}
*/
					InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::UpdateActionStates::!panelMgr");
						return;
					}
//	CS3 Change		if(panelMgr->IsPanelWithWidgetIDVisible(kRfhPanelWidgetID))
					if(panelMgr->IsPanelWithWidgetIDShown(kRfhPanelWidgetID)) 	
					{
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
						iListPtr->SetNthActionState(iter,kEnabledAction);
					}

				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;

			}
//----------------
			case kRfhMenuDeleteItemFrameActionID:
			{					
				if(result && iDoc)
				{					
					if(IsDeleteFlagSelected)					
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);					
					else					
						iListPtr->SetNthActionState(iter,kEnabledAction);					
				}
				else				
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);				
				break;
			}
//// chetan
//			case kRfhMenuRefreshTabByAttributeActionID:
//			{					
//				if(result && iDoc)
//				{					
//					if(refreshTableByAttribute){
//						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);}
//					else{
//						iListPtr->SetNthActionState(iter,kEnabledAction);}
//				}
//				else				
//					iListPtr->SetNthActionState(iter,kDisabled_Unselected);				
//				break;
//			}
////
			case kRfhMenuItem4ActionID:
			{					
				if(result && iDoc)
				{					
					if(GroupFlag == 1)					
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);					
					else					
						iListPtr->SetNthActionState(iter,kEnabledAction);					
				}
				else				
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);				
				break;
			}

			case kRfhMenuItem1ActionID:
			{					
				if(result && iDoc)
				{					
					if(GroupFlag == 2)					
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);					
					else					
						iListPtr->SetNthActionState(iter,kEnabledAction);					
				}
				else				
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);				
				break;
			}

			case kRfhMenuItem2ActionID:
			{					
				if(result && iDoc)
				{					
					if(GroupFlag == 3)					
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);					
					else					
						iListPtr->SetNthActionState(iter,kEnabledAction);					
				}
				else				
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);				
				break;
			}

			default:
			{
				break;
			}
		}
	}
}

void RfhActionComponent::CloseRefreshPalette(void)
{
	do
	{	
		//DoPalette();
	// Added By Awasthi
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::CloseRefreshPalette::iApplication is nil");		
			break;
		}
/*	//////	CS3 Change
		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::CloseRefreshPalette::iPaletteMgr is nil");		
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		//Mediator::iPanelCntrlDataPtr = iPanelMgr;
	// End Awasthi
*/
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr== nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::CloseRefreshPalette::iPanelMgr is nil");				
			break;
		}
/*		if(Mediator::iPanelCntrlDataPtr== nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::CloseRefreshPalette::iPanelCntrlDataPtr is nil");				
			break;
		}
*/		
		/* Clear all listboxes and disable all controls */
		
		// MAke comment Awasthi
		/*IControlView* dropdownCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLDropDownWidgetID);
		
		if(!dropdownCtrlView)
			break;
		
		IControlView* refreshBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLRefreshIconSuiteWidgetID);
		if(!refreshBtnCtrlView)
			break;
		
		IControlView* appendRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
		if(!appendRadCtrlView)
			break;
		
		IControlView* embedRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
		if(!embedRadCtrlView)
			break;
		
		IControlView* tagFrameChkboxCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
		if(!tagFrameChkboxCtrlView)
			break;
		*/

		// End making Commments Awasthi
	//	IControlView* classTreeCtrlView=
	//		Mediator::iPanelCntrlDataPtr->FindWidget(kTPLClassTreeIconSuiteWidgetID);
	//	if(!classTreeCtrlView)
	//		break;
		
		//Mediator::dropdownCtrlView->Disable();
		//Mediator::RfhRefreshButtonView->Disable();		
		
//		SDKListBoxHelper listHelper(this, kRfhPluginID);	
		
	//	Mediator::listControlView->HideView();		

			IControlView* panelView = iPanelMgr->GetPanelFromActionID(kRfhPanelWidgetActionID);
			if(panelView == NULL)
			{
				//CA("panelView is NULL");
				return;
			}
		
		////----------------New CS3 Changes--------------------//
			PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(panelView);
		////----------------New CS3 Changes--------------------//
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
		//================================================UpTo Here
		
			
			////////////End		
//		if(iPaletteMgr)//TPLActionComponent::palettePanelPtr)	
		if(retval)
		{	
/*			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil)	
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhActionComponent::CloseRefreshPalette::iPanelMgr is nil");							
				break;
			}
*/			
			if(iPanelMgr->IsPanelWithMenuIDMostlyVisible(kRfhPanelWidgetActionID))
			{				
				iPanelMgr->HidePanelByMenuID(kRfhPanelWidgetActionID);				
			}
		}
		
	}while(kFalse);
	
}

//void RfhActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
//{
//
//	if (ac == nil)
//	{
//		ASSERT(ac);
//		return;
//	}
//
//	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
//	{
//		ActionID actionID = listToUpdate->GetNthAction(iter);
//		if (actionID.Get() == kRfhContentPanelWidgetActionID) 
//		{	
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == nil)
//				return;
//
//			bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();
//
//			IDocument *iDoc=::GetFrontDocument();
//
//			if(result  && iDoc)
//			{		
//				listToUpdate->SetNthActionState(iter, kEnabledAction);				
//				
//			}
//			else
//			{				
//				listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
//			}
//		}
//	}
//}

/* DoDialog
*/
void RfhActionComponent::DoDialog()
{//CA("RfhActionComponent::DoDialog");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog");	
	do
	{	
		
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		ASSERT(application);
		if (application == nil) {
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: application == nil");	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {//CA("dialogMgr == nil");
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: dialogMgr == nil");	
			break;
		}

		bool16 isDialogOpen = dialogMgr->IsModelessDialogInFront ();
		if(isDialogOpen && Summarydialog != NULL)
		{
			Summarydialog->Close();
			Summarydialog =  NULL;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kRfhPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kRfhDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kModeless , IDialogMgr::kDontAllowMultipleCopies);
		//ASSERT(dialog);
		if (dialog == nil) {//CA("dialog == nil");
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: dialog == nil");	
			break;
		}

		Summarydialog = NULL;
		if(showSummaryDialog)
			Summarydialog = dialog;
		// Set appropriate window title based on mode
		InterfacePtr<IWindow> window(dialog, UseDefaultIID());
		ASSERT(window != nil);
		if (window == nil) {//CA("window == nil");
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: window == nil");	
			break;
		}

		PMString windowTitle;
		
		PMString FIRST("Refresh Catalog");
		windowTitle = FIRST;		
		windowTitle.SetTranslatable(kFalse);

		window->SetTitle(windowTitle);
	//CA("before open ");
		bool16 isOpen = dialog->IsOpen();
		if(isOpen)
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: isOpen = kTrue 11111111111111111111111");
			dialog->Close();			
		}
		ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: before dialog->Open()");	
		dialog->Open();
		ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: after dialog->Open()");
		
		isOpen = dialog->IsOpen();
		if(isOpen)
		{
			ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhActionComponent::DoDialog: isOpen = kTrue ");					
		}
		//CA("After open");
	
	} while (false);			
}

void DoRfhPreferenceDialog()
{
//CA("DoRfhPreferenceDialog 1");
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if (application == nil) 
		{	
			CA("application == nil");
			break;
		}
//CA("DoRfhPreferenceDialog 2");
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil) 
		{
			CA("dialogMgr == nil");
			break;
		}
//CA("DoRfhPreferenceDialog 3");
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,						// Locale index from PMLocaleIDs.h. 
			kRfhPluginID,					// Our Plug-in ID  
			kViewRsrcType,					// This is the kViewRsrcType.
			kPreferenceDialogResourceID,	// Resource ID for our dialog.
			kTrue							// Initially visible.
		);
//CA("DoRfhPreferenceDialog 4");
		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog * dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal,kTrue,kFalse);
//CA("DoRfhPreferenceDialog 4.1");		
		if (dialog == nil) 
		{
			CA("dialog == nil");
			break;
		}
//CA("DoRfhPreferenceDialog 5");
		// Open the dialog.
		dialog->Open();

		//***Added By Sachin Sharma 
		dialog->WaitForDialog(kFalse);
	
	} while (false);
//CA("DoRfhPreferenceDialog 6");
}


K2Vector<PMString> RfhActionComponent::GetBookContentNames(IBookContentMgr* bookContentMgr)
{

	bookContentNames.clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content  Depricated cs3
			//InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			//if (bookLink == nil) 
			//{
			//	ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
			//	break; // out of for loop
			//}

			//// get the book name and add it to the list
			//PMString* baseName = bookLink->GetBaseName();  //Cs3
			//ASSERT(baseName && baseName->empty() == kFalse);

			//*****Added for Cs4
			InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
			if (bookContent == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IBookContent for book #%d is missing", i));
				//CA("bookContent == nil");
				break; // out of for loop
			}
			PMString baseName = bookContent->GetShortName();
			//***********
			bookContentNames.push_back(baseName);


			//Get Full Path
			//PMString* pathName = bookLink->GetFullName();
			//bookContentPaths.push_back(*pathName);
			//CA("pathName = " + *pathName);
		}

	} while (false);
	return bookContentNames;
}