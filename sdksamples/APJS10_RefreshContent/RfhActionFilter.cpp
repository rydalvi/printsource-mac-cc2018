//========================================================================================
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or
//  distribution of it requires the prior written permission of Adobe.
//
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActionFilter.h"
//#include "IEnvironment.h"

// General includes:
#include "ActionDefs.h" // for kCustomEnabling
//#include "ActionFilterHelper.h"
#include "CPMUnknown.h"
#include "CAlert.h"
//#include "UpdateManagerID.h" // for actionIDs and bossclasses

// Project includes:
#include "RfhID.h"
//#include "PnlMnuTestActionFilter.h"

class RfhActionFilter : public CPMUnknown<IActionFilter>
{
public:
    /** Constructor.
        @param boss interface ptr from boss object on which this interface is aggregated.
    */
    RfhActionFilter(IPMUnknown* boss);

    /** Destructor.
    */
    virtual ~RfhActionFilter(void)
    {
    }

    /** FilterAction
        @see IActionFilter
    */
    virtual void FilterAction(ClassID* componentClass,
                              ActionID* actionID,
                              PMString* actionName,
                              PMString* actionArea,
                              int16* actionType,
                              uint32* enablingType,
                              bool16* userEditable);

};

/* CREATE_PMINTERFACE
Binds the C++ implementation class onto its
ImplementationID making the C++ code callable by the
application.
*/
CREATE_PMINTERFACE(RfhActionFilter, kRfhActionFilterImpl)


/* Constructor
*/
RfhActionFilter::RfhActionFilter(IPMUnknown* boss)
    : CPMUnknown<IActionFilter>(boss)
{
    // does nothing.
}


/* FilterAction
*/
void RfhActionFilter::FilterAction(ClassID* componentClass, ActionID* actionID,
                                       PMString* actionName, PMString* actionArea,
                                       int16* actionType, uint32* enablingType,
                                       bool16* userEditable)
{
	// The panel widget (by way of the PanelList resrouce declaration in the .fr file)
	// is added to the action manager with the enablingType
	// as kDisableIfLowMem | kCustomEnabling so we are using this action filter
	// to set the componentClass to our action component to allow us to enable
	// and disable the menu item for this panel on the Window menu.
	if(actionID->Get() == kRfhPanelWidgetActionID)
	{		
		*componentClass = kRfhActionComponentBoss;
		//PMString string("Set to customEnabling", -1, PMString::kNoTranslate);
		//CAlert::InformationAlert(string);
	}
    
}
