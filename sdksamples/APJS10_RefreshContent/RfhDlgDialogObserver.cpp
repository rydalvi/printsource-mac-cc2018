//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/RfhDlgDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ISelectionManager.h"

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ILayoutUtils.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "RfhSelectionObserver.h"
#include "IListBoxController.h"
#include "ILayoutUtils.h"
#include "MediatorClass.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "RfhID.h"
#include "IFrameUtils.h"
#include "ILayoutUIUtils.h"
#include "IWorkspace.h"
#include "ISwatchList.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "IGeometry.h"
#include "RfhActionComponent.h"
#include "ISpreadList.h"
//#include "ITextFrame.h"
#include "SystemUtils.h"
#include "ILayoutUIUtils.h" //Cs4
#include "ITextAttrUtils.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
#include "ITextValue.h"
#include "AcquireModalCursor.h" 
#include "FileUtils.h"
//#include "BoxReader.h"
#include "SDKLayoutHelper.h"
#include "IStrokeAttributeSuite.h"
#include "ISelectionUtils.h"
#include "ILayoutSelectionSuite.h"
#include "DocWchUtils.h"
#include "CommonFunctions.h"
//added on 30May by Yogesh
#include "RfhDlgDialogController.h"
#include "IDialogMgr.h"
#include "IWindow.h"

#include "Report.h"

//end 30May

// Project includes:
#include "RfhID.h"
#include "CAlert.h"
//#include "IMessageServer.h"
//--------
#include "IBookManager.h"
#include "IDataLink.h"
#include "ISession.h"
#include "IBookContentMgr.h"
#include "IBook.h"
#include "ProgressBar.h"
#include "IBookContent.h"
#include "ITagReader.h"
#include "IHierarchy.h"
#include "IBookUtils.h"
#include "FrameRecord.h"
#include "IPageItemTypeUtils.h"
#include "IClientOptions.h"
#include "TableUtility.h"


#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ITableModel.h"
#include "BookReportData.h"
#include "RfhDlgDialogController.h"
#include "RfhDlgDialogObserver.h"
#include "SDKUtilities.h"
#include "ITextFrameColumn.h"
#include "ITableModelList.h"
#include "ThreadTextFrame.h"
#include "SPID.h"
#include "IContentSprayer.h"
#include "ITextAttributeSuite.h"


#include "IPageList.h"
#include <time.h>
#include <stdio.h>
//#include<windows.h>

#include "ITreeViewMgr.h"
#include "RFHTreeModel.h"
#include "RFHTreeDataCache.h"
#include "IntNodeID.h"



#define FILENAME			PMString("RfhDialogObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

TextVectorList FinalBookTextVectorListperFrame;
//extern RefreshDataList ColorDataList;
//extern bool16 FlgToCheckMenuAction;
//extern TextVectorList ChangedTextList;
//extern bool16 RefreshFlag;
extern vector<IDFile> BookFileList;
extern bool16 ISRfreshBookDlgOpen;
extern PMString BookName;
extern bool16 IsHiliteFlagSelected;
extern bool16 IsDeleteFlagSelected;


//Added on 30May by Yogesh
//All the following variables are defined in the RfhDlgDialogController.cpp
extern RefreshDataList rBookDataList;
extern RefreshDataList UniqueBookDataList;
extern RefreshDataList OriginalrBookDataList;

extern int32 TreeDisplayOption;

extern vecFrameRecord frameRecordList;
extern vecUniqueItemOnDoc uniqueItemOnDocList;
extern vecNewItemIDS newItemIDList;

// Global Pointers
extern IAppFramework* ptrIAppFramework;
//Added By Dattatray on 30/10
extern bool16 IsEventInListBox;
extern bool16 isnewSection ;

extern set<NodeID> UniqueCRBookDocNodeIds;

extern set<NodeID> UniqueCRBookFieldNodeIds;

bool16 Booklevelstructurevariable=kFalse;
bool16 Booklevelcontentvariable=kFalse;

bool16 isDummyCall = kFalse;
bool16 IsBookrefreshSelected;

extern bool16 issrtucturebuttenselected ;
extern bool16 iscontentbuttonselected ;

///////////
//end 30May

/**	Implements IObserver based on the partial implementation CDialogObserver; 
	allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	
	 @ingroup basicdialog
	
*/

extern vector<double>  itemsUpdated;
extern vector<double>  itemsDeleted;
extern vector<double>  productsUpdated;
extern vector<double>  productsDeleted;

extern vector<double> itemImagesDeleted;
extern vector<double> productImagesDeleted;
extern vector<double> itemImagesReLinked;
extern vector<double> productImagesReLinked;
extern vector<int32> pagesPerDocumetList;

extern bool16 refreshTableByAttribute;

vector<double> item_attribute_ids;//by Amarjit patil
vector<double> itemgroup_attributeids;//by Amarjit patil
vector<double> Itemattribute_Itemgroupattribute;
//extern vector<double>ItemId_ItemgroupId;
itemList tempItemList;
productList tempProductList;

K2Vector<PMString> bookContentNames; //-------------
K2Vector<bool16>  bookIsSelected; 

int32 currentDocumentIndex = -1;
UIDRef currentDocumentUIDRef = UIDRef::gNull;
int32 currentSelectedFrameIndex = -1;
K2Vector<int32> selectedDocIndexList;
bool16 shouldRefreshFlag = kTrue;
bool16 shouldPushInFreameVect = kFalse;
int32 refreshModeSelected = 0/*1*/;//1 = Delete ,2 = Update,3 = New
bool16 isRefreshTabByAttributeCheckBoxSelected = kFalse;
K2Vector<double> sectionIDInDoc; 

extern double CurrentSelectedDocumentSectionID;
extern double CurrentSelectedDocumentLanguageID;

extern bool16 IsFrameRefreshSelected;

extern PMString imageDownLoadPath;
bool16 Isframedelete = kFalse;


double itemDescriptionAttributeID = -1;
double itemGroupDescriptionElementID = -1;

RefreshDataList BookDocumentDataList;

//////class RfhDlgDialogObserver : public CDialogObserver
//////{
//////	public:
//////		/**
//////			Constructor.
//////			@param boss interface ptr from boss object on which this interface is aggregated.
//////		*/
//////		RfhDlgDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}
//////
//////		/** Destructor. */
//////		virtual ~RfhDlgDialogObserver() {}
//////
//////		/** 
//////			Called by the application to allow the observer to attach to the subjects 
//////			to be observed, in this case the dialog's info button widget. If you want 
//////			to observe other widgets on the dialog you could add them here. 
//////		*/
//////		virtual void AutoAttach();
//////
//////		/** Called by the application to allow the observer to detach from the subjects being observed. */
//////		virtual void AutoDetach();
//////
//////		/**
//////			Called by the host when the observed object changes, in this case when
//////			the dialog's info button is clicked.
//////			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
//////			@param theSubject points to the ISubject interface for the subject that has changed.
//////			@param protocol specifies the ID of the changed interface on the subject boss.
//////			@param changedBy points to additional data about the change. 
//////				Often this pointer indicates the class ID of the command that has caused the change.
//////		*/
//////		virtual void Update
//////		(
//////			const ClassID& theChange, 
//////			ISubject* theSubject, 
//////			const PMIID& protocol, 
//////			void* changedBy
//////		);
//////
//////		virtual K2Vector<PMString> GetBookContentNames(IBookContentMgr* bookContentMgr);
//////		void RefreshBookWithInteractiveMode();
//////		void fillFrameRecordVector();
//////		void OpenInteractiveRefreshDialog();
//////		void fillRefreshModeListBox(InterfacePtr<IPanelControlData> panelControlData);
//////		void selectNextFrame();
//////};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(RfhDlgDialogObserver, kRfhDialogObserverImpl)

/* AutoAttach
*/
void RfhDlgDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			//ASSERT_FAIL("BscDlgDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		// Now attach to BasicDialog's info button widget.
		AttachToWidget(kRfhDlgRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRfhHighliteCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRfhDeletFrameCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		//Added on 30May by Yogesh
		AttachToWidget(kRfhSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRfhDlgNext1ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgCancel1ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgBackButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgCancel2ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhFinishButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgNextButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgCancelButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kRfhDlgBack1ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//end 30May by Yogesh
		AttachToWidget(kRfhRefreshTabByAttributeCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData); //chetan

		AttachToWidget(kRfhDlgBookListBackButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgBookListCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgBookListNextButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhBookListSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kReportWithRefreshWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRefreshReportWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kInteractiveMessageYesButtonWidgetID,IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kInteractiveMessageNoButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kInteractiveMessageOKButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kRfhRefreshModeDlgCancelButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kRfhRefreshModeDlgLocateButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kRfhRefreshModeDlgNextButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhRefreshModeDlgPrevButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kRfhRefreshModeDlgApplyButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
//		AttachToWidget(kRfhRefreshModeDlgNextDocButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kRfhProcessOptionsDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kProceedToUpdateYesButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kInteractiveOptionOKButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		AttachToWidget(kInteractiveOptionCancelButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
//		AttachToWidget(kMyReportWithRefreshWidgetID, IID_ITRISTATECONTROLDATA, panelControlData); //***
//		AttachToWidget(kMyRefreshReportWidgetID, IID_ITRISTATECONTROLDATA, panelControlData); //***
		AttachToWidget(kRfhAnalyzeButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);

		AttachToWidget(kfirstcancelWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kfirstnextWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kcontentstructureWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);

		AttachToWidget(kRfhlastcancelCheckBoxWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhlastnextCheckBoxWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhnewbackCheckBoxWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);//kRfhnew14Back1ButtonWidgetID
		AttachToWidget(kRfhnew14Back1ButtonWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgfinishButtonWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRfhDlgCancel123ButtonWidgetID ,IID_ITRISTATECONTROLDATA,panelControlData);
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void RfhDlgDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			//ASSERT_FAIL("BscDlgDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from BasicDialog's info button widget.
		DetachFromWidget(kRfhDlgRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhHighliteCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDeletFrameCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		//Added on 30May by Yogesh
		DetachFromWidget(kRfhSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgNext1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgBackButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgCancel1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgCancel2ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhFinishButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRfhDlgNextButtonWidgetID, IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/, panelControlData);
		DetachFromWidget(kRfhDlgCancelButtonWidgetID, IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/, panelControlData);
		DetachFromWidget(kRfhDlgBack1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		DetachFromWidget(kRfhRefreshTabByAttributeCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData); //chetan

		DetachFromWidget(kRfhDlgBookListBackButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgBookListCancelButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgBookListNextButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhBookListSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kReportWithRefreshWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRefreshReportWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kInteractiveMessageYesButtonWidgetID,IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kInteractiveMessageNoButtonWidgetID, IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/, panelControlData);
		DetachFromWidget(kInteractiveMessageOKButtonWidgetID, IID_ITRISTATECONTROLDATA/* IID_IBOOLEANCONTROLDATA*/, panelControlData);
		DetachFromWidget(kRfhRefreshModeDlgCancelButtonWidgetID, IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/, panelControlData);
		DetachFromWidget(kRfhRefreshModeDlgLocateButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		DetachFromWidget(kRfhRefreshModeDlgNextButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		DetachFromWidget(kRfhRefreshModeDlgPrevButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		DetachFromWidget(kRfhRefreshModeDlgApplyButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
//		DetachFromWidget(kRfhRefreshModeDlgNextDocButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRfhProcessOptionsDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kProceedToUpdateYesButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		DetachFromWidget(kInteractiveOptionOKButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
		DetachFromWidget(kInteractiveOptionCancelButtonWidgetID,IID_ITRISTATECONTROLDATA/*IID_IBOOLEANCONTROLDATA*/,panelControlData);
//		DetachFromWidget(kMyReportWithRefreshWidgetID, IID_ITRISTATECONTROLDATA, panelControlData); //***
//		DetachFromWidget(kMyRefreshReportWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhAnalyzeButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);

		DetachFromWidget(kfirstcancelWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kfirstnextWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kcontentstructureWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

		DetachFromWidget(kRfhlastcancelCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRfhlastnextCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRfhnewbackCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);//kRfhDlgfinishButtonWidgetID
		DetachFromWidget(kRfhnew14Back1ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRfhDlgfinishButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRfhDlgCancel123ButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void RfhDlgDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	RfhDlgDialogController objDialogCont(this);
	do
	{	//CA("Inside CDialogObserver::Update");
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update:: controlView invalid");
			break;
		}

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelcontrol == nil");
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		//-----------
 
		//for content only radio button
			IControlView* RfhcontentRadioBtnView = panelControlData->FindWidget(kcontentonlyWidgetID);
			if(!RfhcontentRadioBtnView)
			{		
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData>contentRadioTriState(RfhcontentRadioBtnView,UseDefaultIID());
			if(contentRadioTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::contentRadioTriState == nil");
				return;
			}
		//for content and structure radio button
			IControlView* RfhcontentStructerRadioBtnView = panelControlData->FindWidget(kcontentstructureWidgetID);
			if(!RfhcontentStructerRadioBtnView)
			{		
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData>contentstructerRadioTriState(RfhcontentStructerRadioBtnView,UseDefaultIID());
			if(contentstructerRadioTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByCellRadioTriState == nil");
				return;
			}
//till here
		
		IControlView* RfhDeleteCheckBoxctrlvw = panelControlData->FindWidget(kRfhDeleteCheckBoxWidgetID);
		if(!RfhDeleteCheckBoxctrlvw)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTabByAttributeCheckBoxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>RfhDeleteCheckBoxtristate(RfhDeleteCheckBoxctrlvw,UseDefaultIID());
		if(RfhDeleteCheckBoxtristate == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RfhRefreshTabByAttributeCheckBoxTriState == nil");
			return;
		}
		Isframedelete = RfhDeleteCheckBoxtristate->IsSelected();

		IControlView* RfhRefreshTabByAttributeCheckBoxView = panelControlData->FindWidget(kRfhRefreshTabByAttributeCheckBoxWidgetID);
		if(!RfhRefreshTabByAttributeCheckBoxView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTabByAttributeCheckBoxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>RfhRefreshTabByAttributeCheckBoxTriState(RfhRefreshTabByAttributeCheckBoxView,UseDefaultIID());
		if(RfhRefreshTabByAttributeCheckBoxTriState == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RfhRefreshTabByAttributeCheckBoxTriState == nil");
			return;
		}
		isRefreshTabByAttributeCheckBoxSelected =RfhRefreshTabByAttributeCheckBoxTriState->IsSelected();
		//till here

		IControlView * zerothGroupPanelControlView = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
		if(zerothGroupPanelControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::zerothGroupPanelControlView == nil");
			break;
		}

		IControlView * midBookListGroupPanelControlView = panelControlData->FindWidget(kRfhMidBookListGroupPanelWidgetID);
		if(midBookListGroupPanelControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::midBookListGroupPanelControlView == nil");
			break;
		}

		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRfhFirstGroupPanelWidgetID);
		if(firstGroupPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::firstGroupPanelControlData == nil");
			break;
		}

		IControlView * secondGroupPanelControlView = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
		if(secondGroupPanelControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::secondGroupPanelControlView == nil");
			break;
		}
		
		IControlView * thirdGroupPanelControlView = panelControlData->FindWidget(kRfhThirdGroupPanelWidgetID);
		if(thirdGroupPanelControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::thirdGroupPanelControlView == nil");
			break;
		}

		IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
		if(interactiveListNilGroupPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
			break;
		}

		if (( theSelectedWidget == kRfhDlgRefreshButtonWidgetID  || (theSelectedWidget== kOKButtonWidgetID && secondGroupPanelControlView->IsVisible())) &&  (theChange == kTrueStateMessage) && contentstructerRadioTriState->IsSelected()==kTrue)
		{ 
			//CA("Structure book level");//doRefresh
			Booklevelstructurevariable=kTrue;
			Booklevelcontentvariable=kFalse;

			IsBookrefreshSelected=kTrue;
			IsFrameRefreshSelected=kFalse;
			ptrIAppFramework->LogDebug("*** Refresh button clicked ***");
			//By amarjit patil
			item_attribute_ids.clear();
			itemgroup_attributeids.clear();

			itemsUpdated.clear();
			itemsDeleted.clear();
			productsUpdated.clear();
			productsDeleted.clear();
			productImagesReLinked.clear();

			itemImagesDeleted.clear();
			productImagesDeleted.clear();
			itemImagesReLinked.clear();

		
			tempItemList.clear();
			tempProductList.clear();

			ISRfreshBookDlgOpen = kTrue;
			DocWchUtils::StopDocResponderMode();
            refreshTableByAttribute= kFalse;
            iscontentbuttonselected = kFalse;
            issrtucturebuttenselected= kTrue;


			if(UniqueCRBookFieldNodeIds.size() > 0)
			{
			
				set<NodeID>::iterator it;
				for(it=UniqueCRBookFieldNodeIds.begin(); it!=UniqueCRBookFieldNodeIds.end(); it++ )
				{
					NodeID nid= (NodeID)*it;
					
					InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::BooklistControlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::!treeViewMgr");					
						continue ;
					}
					//QueryWidgetFromNode 

					InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
					//ASSERT(panelControlData);
					if(panelControlData==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::panelControlData is nil");		
						continue ;
					}

					IControlView* checkBoxCntrlView=panelControlData->FindWidget(kRfhCheckBoxWidgetID);
					if(checkBoxCntrlView==nil) 
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::checkBoxCntrlView == nil");
						continue ;
					}
		
					InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::itristatecontroldata is nil");		
						continue ;
					}

					TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

					int32 uid= uidNodeIDTemp->Get();
					int32 TextFrameuid = uidNodeIDTemp->Get();
					RfhDataNode rNode;
					RFHTreeDataCache dc1;

					dc1.isExist(uid, rNode);

					PMString fieldName = rNode.getFieldName();

					for(int32 k =0; k < UniqueBookDataList.size(); k ++)
					{
						if(UniqueBookDataList[k].name == fieldName)
						{
							if(itristatecontroldata->IsSelected())
								UniqueBookDataList[k].isSelected = kTrue;
							else
								UniqueBookDataList[k].isSelected = kFalse;

							for(int j=0; j<rBookDataList.size(); j++)
							{				
								if(UniqueBookDataList[k].elementID == rBookDataList[j].elementID /*&& UniqueDataList[k].TypeID ==  rDataList[j].TypeID*/ && UniqueBookDataList[k].whichTab == rBookDataList[j].whichTab)
								{	
									if((UniqueBookDataList[k].whichTab != 4 || UniqueBookDataList[k].isImageFlag == kTrue || UniqueBookDataList[k].isTableFlag == kTrue) && !(UniqueBookDataList[k].elementID == - 121 && UniqueBookDataList[k].isTableFlag == kFalse))
									{						
										if( UniqueBookDataList[k].TypeID !=  rBookDataList[j].TypeID)
											continue;							
									}
									if(UniqueBookDataList[k].whichTab == 4 && UniqueBookDataList[k].elementID == -101 ) // for Item Table in Tabbed text
									{						
										if( UniqueBookDataList[k].TypeID !=  rBookDataList[j].TypeID)
											continue;							
									}					
									rBookDataList[j].isSelected = UniqueBookDataList[k].isSelected;				
								}
							}
						}
					}
					
				}
			}


			AcquireWaitCursor awc;
			awc.Animate();
			
			K2Vector<bool16>::iterator  itr; 
			itr=bookIsSelected.begin();
			int32 checkedNumber=0;
//-------------- //by amarjit patil------------------------------------------------------
			for(int32 listIndex=0;listIndex<rBookDataList.size();listIndex++)
            {
  				if(rBookDataList[listIndex].isSelected)
				{
				  int32 index1 =  rBookDataList[listIndex].whichTab;
                  if(index1==3)
				  {
                   double eelementid = rBookDataList[listIndex].elementID;
			       itemgroup_attributeids.push_back(eelementid);

				   PMString jkk("itemgroupattribute =");
					jkk.AppendNumber(eelementid);
					//CA(jkk);
				  }
				  if(index1==4)
				  {
				   double eelementid = rBookDataList[listIndex].elementID;
				   item_attribute_ids.push_back(eelementid);
				   PMString kkk("itemattribute=");
				   kkk.AppendNumber(eelementid);
				 //  CA(kkk);
				  }
				}
			}
			int32 siz =static_cast<int32>(item_attribute_ids.size());
			/*PMString kkk("tem_attribute_ids size =");
			kkk.AppendNumber(siz);
			CA(kkk);*/

			int32 siz11 =static_cast<int32>(itemgroup_attributeids.size());
			/*PMString jkk("itemgroup_attributeids size =");
			jkk.AppendNumber(siz11);
			CA(jkk);*/
//==========================================END==================================================================================			

		/*ElementIDptr resultvec = NULL;
		vector<int32>itemmid;
		vector<int32>ChildIDvec;*/

			//UIDList selectUIDList;
			//InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
			//	if (layoutData == nil)
			//	{
			//		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
			//		break;
			//	}

			//	UIDRef newSpreadRef = layoutData->GetSpreadRef();
			//	
			//	UID pageUID1 = layoutData->GetPage();
			//	if(pageUID1 == kInvalidUID)
			//	{
			//		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
			//		break;
			//	}
			//	IDocument* doc1=Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
			//	if(doc1==nil)
			//	{
			//		ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No doc");
			//		return;
			//	}
			//	IDataBase* database = ::GetDataBase(doc1);
			//	if(database==nil)
			//	{
			//		ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No database");
			//		return;
			//	}


			//	 InterfacePtr<IPageList> iPageList(doc1,UseDefaultIID());
			//	 if (iPageList == nil){
			//		// CA("iPageList == nil");
			//		 break;
			//	 }
		 //
			//	 int32 totalNumOfPages = iPageList->GetPageCount();
			//	 InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)doc1,UseDefaultIID());
			//	if (iSpreadList1==nil)
			//	{
			//		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
			//		return ;
			//	}
			//	int32 noofspread= iSpreadList1->GetSpreadCount();
			//	PMString spreadsize("spreadsize :");
			//	spreadsize.AppendNumber(noofspread);
			//	//CA(spreadsize);

			//	for(int numSp_1=0; numSp_1< noofspread; numSp_1++)
			//	{
			//		//CA("numSp_1");
			//		UIDRef temp_spreadUIDRef1(database, iSpreadList1->GetNthSpreadUID(numSp_1));
			//		//CA("newSpreadRef == temp_spreadUIDRef1");
			//		InterfacePtr<ISpread> spread(temp_spreadUIDRef1, UseDefaultIID());
			//		if(!spread)
			//		{
			//			ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
			//			return ;
			//		}
			//		int numPages=spread->GetNumPages();

			//		PMString noofpages("noofpages :");
			//		noofpages.AppendNumber(numPages);
			//		//CA(noofpages);

			//		for(int32 i=0;i<numPages;i++)
			//		{
			//			UIDList itemsOnPageUIDList(database) ;
			//			spread->GetItemsOnPage(i, &itemsOnPageUIDList,kFalse);
			//			selectUIDList=itemsOnPageUIDList;

			//			PMString uidlist("selectUIDList First:");
			//			uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
			//		    //CA(uidlist);

			//			for( int p=0; p<selectUIDList.Length(); p++)
			//			{
			//				PMString str;
			//				str.AppendNumber(p);
			//				CA("lllllllllll");
			//				CA(str);
			//			}
			//		}
			//	}
//till here


			for(checkedNumber = 0 ; itr!=bookIsSelected.end(); itr++)
			{
				if(*itr==kTrue)
					checkedNumber++;
			}

			InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
			if(ptrIAppFramework == nil)
				return ;


			InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
				return ; 
			}
			
			IBook * activeBook = bookManager->GetCurrentActiveBook();
			if(activeBook == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
				return ;			
			}
			PMString bookName = activeBook->GetBookTitleName();
			

			PlatformChar pchar('.');
			CharCounter index= bookName.IndexOfCharacter(pchar);
			
			PMString bk = bookName;
			bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indb"
			
			BookReportData::setCurrentBookName(bk);
			

			/*PMString name("Refreshing Book: ");
			name.Append(bookName);
			name.Append("...");
			RangeProgressBar progressBar(name, 0,checkedNumber, kTrue);
			progressBar.SetTaskText("Refreshing Documents");*/
			
			
			

			//ChangedTextList.clear();
			//RefreshFlag = kTrue;
			
			//CA("kTPLRefreshIconSuiteWidgetID");
			//bool16 retVal=0;	
			
			//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
			//ptrIAppFramework->GETCommon_refreshClientCache();
			//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			
			//if(retVal==0)
			//{
			//	//CA("retVal==0");
   //          	break;
			//}
			//CA("kTPLRefreshIconSuiteWidgetID 2");
			//HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			
			//int PreselectedRadioBUtton = Mediator::selectedRadioButton;
	//		K2Vector<bool16>  bookIsSelected; 

			if(bookIsSelected.size() == 0)
			{
				//CA("bookIsSelected.size() == 0");
				return;
			}

			int32 index1=0;
			itr=bookIsSelected.begin();
			for(int32 p=0; p< BookFileList.size(); p++,itr++)
			{	
				if(*itr==kFalse)//------					
					continue;
				
				
				if(p > 0)
					CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

				/*PMString tempString("Refreshing Document : ");
				tempString += bookContentNames[p];
				tempString += "...";
				tempString.SetTranslatable(kFalse);
				progressBar.SetTaskText(tempString);
				progressBar.SetPosition(index1);
				index1++;*/
				//****	
				PlatformChar pchar('.');
				CharCounter index= bookContentNames[p].IndexOfCharacter(pchar);
				
				PMString bk = bookContentNames[p];
				bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indb"
				BookReportData::currentSection = bk;
				//CA("BookReportData::currentSection   "+BookReportData::currentSection );
				//*****

				SDKLayoutHelper sdklhelp;
				UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
				ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
				
				Refresh refresh;		
			
				//Mediator::selectedRadioButton = 4;  // Entire Doc Option
				
				BookReportData::vecReportRows.clear();
                ptrIAppFramework->EventCache_clearCurrentSectionData();
                ptrIAppFramework->clearAllStaticObjects();

				//CA("After ............................")
				/*if(RfhRefreshTabByAttributeCheckBoxTriState->IsSelected()==kTrue)
				{*/
					//CA("in isRefreshTabByAttributeCheckBoxSelected  ");
					PMString tempString("Refreshing Document : ");
					tempString += bookContentNames[p];
					tempString += "...";
					tempString.SetTranslatable(kFalse);
					refresh.doRefresh(4,bookName,tempString);
					//CA("After calling Dorefresh ");
				//}

//till here 					
				isnewSection = kTrue;

//PMString a("BookFileList.size()	:	");
//a.AppendNumber(static_cast<int32>(BookFileList.size()));
//a.Append("\rp	:	");
//a.AppendNumber(p);
//a.Append("\rBookReportData::vecReportRows.size()	:	");
//a.AppendNumber(static_cast<int32>(BookReportData::vecReportRows.size()));
//CA(a);
				sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
				sdklhelp.CloseDocument(CurrDocRef, kFalse);				
			}	

			ISRfreshBookDlgOpen = kFalse;
			DocWchUtils::StartDocResponderMode();
//5-june chetan
			//hide the Second group panel
			
			secondGroupPanelControlView->HideView();

			IControlView * firstInteractiveGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
			if(firstInteractiveGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::firstInteractiveGroupPanelControlData == nil");
				break;
			}
			firstInteractiveGroupPanelControlData->HideView();

			interactiveListNilGroupPanelControlData->HideView();

			//Show the ThirdGroupPanel
			
			thirdGroupPanelControlView->ShowView();

			IControlView* TextWidget1 = panelControlData->FindWidget(kItemUpdatedWidgetID);
			if(!TextWidget1)
			{
				//CA("TextWidget1 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text1;
				int32/*int64*/ TotalUpdatedItem = /*tempItemList.size()*/static_cast<int32>(itemsUpdated.size()); 
				text1.AppendNumber(PMReal(TotalUpdatedItem));
				text1.SetTranslatable(kFalse); //added by avinash
                text1.ParseForEmbeddedCharacters();
				textcontrol->SetString(text1);			
			}

			IControlView* TextWidget2 = panelControlData->FindWidget(kItemDeletedWidgetID);
			if(!TextWidget2)
			{
				//CA("TextWidget2 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text2;
				int32/*int64*/ TotalDeletedItem =static_cast<int32> (itemsDeleted.size()); 
				text2.AppendNumber(PMReal(TotalDeletedItem));
				text2.SetTranslatable(kFalse); //added by avinash
                text2.ParseForEmbeddedCharacters();
				textcontrol->SetString(text2);			
			}

			IControlView* TextWidget3 = panelControlData->FindWidget(kProdUpdatedWidgetID);
			if(!TextWidget3)
			{
				//CA("TextWidget3 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				PMString text3;
				int32/*int64*/ TotalUpdatedProd =static_cast<int32> /*tempProductList.size()*/(productsUpdated.size()); 
				text3.AppendNumber(PMReal(TotalUpdatedProd));
				text3.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text3);			
			}

			IControlView* TextWidget4 = panelControlData->FindWidget(kProdDeletedWidgetID);
			if(!TextWidget4)
			{
				//CA("TextWidget4 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text4;
				int32/*int64*/ TotalDeletedProd =static_cast<int32> (productsDeleted.size()); 
				text4.AppendNumber(PMReal(TotalDeletedProd));
				text4.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text4);			
			}

			IControlView* TextWidget5 = panelControlData->FindWidget(kItemImagesRelinkWidgetID);
			if(!TextWidget5)
			{
				//CA("TextWidget5 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget5,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text5;
				int32/*int64*/ TotalItemImagesRelink = static_cast<int32>(itemImagesReLinked.size()); 
				text5.AppendNumber(PMReal(TotalItemImagesRelink));
				text5.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text5);			
			}

			IControlView* TextWidget6 = panelControlData->FindWidget(kProdImagesRelinkWidgetID);
			if(!TextWidget6)
			{
				//CA("TextWidget6 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget6,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text6;
				int32/*int64*/ TotalProdImagesRelink =static_cast<int32> (productImagesReLinked.size());
				text6.AppendNumber(PMReal(TotalProdImagesRelink));
				text6.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text6);			
			}

			IControlView* TextWidget7 = panelControlData->FindWidget(kItemImagesDeletedWidgetID);
			if(!TextWidget7)
			{
				//CA("TextWidget7 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget7,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text7;
				int32/*int64*/ TotalItemImagesDeleted =static_cast<int32> (itemImagesDeleted.size()); 
				text7.AppendNumber(PMReal(TotalItemImagesDeleted));
				text7.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text7);			
			}

			IControlView* TextWidget8 = panelControlData->FindWidget(kProdImagesDeletedWidgetID);
			if(!TextWidget8)
			{
				//CA("TextWidget8 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget8,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text8;
				int32/*int64*/ TotalProdImagesDeleted =static_cast<int32> (productImagesDeleted.size()); 
				text8.AppendNumber(PMReal(TotalProdImagesDeleted));
				text8.SetTranslatable(kFalse); //added by avinash
				textcontrol->SetString(text8);			
			}
//****Added from Here Date:7/7/08 Sachin Sharma
			
			InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if (application == nil) 
			{	
				//CA("MYYYYapplication == nil"); 
				break;
			}	
			InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			if (dialogMgr == nil) 
			{
				//CA("MyyyyydialogMgr == nil");
				break;
			}
			IWindow* iWindow = dialogMgr->GetFrontmostDialogWindow();
			if(iWindow == nil )
			{
				//CA("MyyyyyiWindow == nil");
				break;
			}
			PMString title("Refresh Summary");
			title.SetTranslatable(kFalse); // added by avinash
			iWindow->SetTitle(title);

//*******Up TO Here

			
			//5-june
			/*PMString temp = "";
			temp.Append("tempItemList.size = ");
			temp.AppendNumber(tempItemList.size());
			temp.Append(" , itemImagesDeleted.size =");
			temp.AppendNumber(itemImagesDeleted.size());
			temp.Append(" , itemImagesReLinked.size =");
			temp.AppendNumber(itemImagesReLinked.size());
			temp.Append(" , itemsDeleted.size =");
			temp.AppendNumber(itemsDeleted.size());
			temp.Append(" ,tempProductList.size() =");
			temp.AppendNumber(tempProductList.size());
			temp.Append(" ,productImagesReLinked.size() =");
			temp.AppendNumber(productImagesReLinked.size());
			temp.Append(" ,productImagesDeleted.size() =");
			temp.AppendNumber(productImagesDeleted.size());
			temp.Append(" ,productsFrameDeleted.size() =");
			temp.AppendNumber(productsDeleted.size());*/
			
			
			//CA(temp);
		}
////////// chetan
		//By amarjit patil
		else if(( theSelectedWidget == kRfhDlgRefreshButtonWidgetID  || (theSelectedWidget== kOKButtonWidgetID && secondGroupPanelControlView->IsVisible())) &&  (theChange == kTrueStateMessage) && contentRadioTriState->IsSelected()==kTrue)
		{
			 //CA("Content book level");//doRefresh
			 Booklevelcontentvariable=kTrue;
			 Booklevelstructurevariable=kFalse;

			 IsBookrefreshSelected=kTrue;
			 IsFrameRefreshSelected=kFalse;
            
            iscontentbuttonselected = kTrue;
            issrtucturebuttenselected= kFalse;

			ptrIAppFramework->LogDebug("*** Refresh button clicked ***");
			//By amarjit patil
			item_attribute_ids.clear();
			itemgroup_attributeids.clear();
			Itemattribute_Itemgroupattribute.clear();

			itemsUpdated.clear();
			itemsDeleted.clear();
			productsUpdated.clear();
			productsDeleted.clear();
			productImagesReLinked.clear();
            
			itemImagesDeleted.clear();//
			productImagesDeleted.clear();
			itemImagesReLinked.clear();

		
			tempItemList.clear();
			tempProductList.clear();

			ISRfreshBookDlgOpen = kTrue;
			DocWchUtils::StopDocResponderMode();
            refreshTableByAttribute= kTrue;

			if(UniqueCRBookFieldNodeIds.size() > 0)
			{
			
				set<NodeID>::iterator it;
				for(it=UniqueCRBookFieldNodeIds.begin(); it!=UniqueCRBookFieldNodeIds.end(); it++ )
				{
					NodeID nid= (NodeID)*it;
					
					InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::BooklistControlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::!treeViewMgr");					
						continue ;
					}
					//QueryWidgetFromNode 

					InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
					ASSERT(panelControlData);
					if(panelControlData==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::panelControlData is nil");		
						continue ;
					}

					IControlView* checkBoxCntrlView=panelControlData->FindWidget(kRfhCheckBoxWidgetID);
					if(checkBoxCntrlView==nil) 
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::checkBoxCntrlView == nil");
						continue ;
					}
		
					InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::itristatecontroldata is nil");		
						continue ;
					}

					TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

					int32 uid= uidNodeIDTemp->Get();
					int32 TextFrameuid = uidNodeIDTemp->Get();
					RfhDataNode rNode;
					RFHTreeDataCache dc1;

					dc1.isExist(uid, rNode);

					PMString fieldName = rNode.getFieldName();

					for(int32 k =0; k < UniqueBookDataList.size(); k ++)
					{
						if(UniqueBookDataList[k].name == fieldName)
						{
							if(itristatecontroldata->IsSelected())
								UniqueBookDataList[k].isSelected = kTrue;
							else
								UniqueBookDataList[k].isSelected = kFalse;

							for(int j=0; j<rBookDataList.size(); j++)
							{				
								if(UniqueBookDataList[k].elementID == rBookDataList[j].elementID /*&& UniqueDataList[k].TypeID ==  rDataList[j].TypeID*/ && UniqueBookDataList[k].whichTab == rBookDataList[j].whichTab)
								{	
									if((UniqueBookDataList[k].whichTab != 4 || UniqueBookDataList[k].isImageFlag == kTrue || UniqueBookDataList[k].isTableFlag == kTrue) && !(UniqueBookDataList[k].elementID == - 121 && UniqueBookDataList[k].isTableFlag == kFalse))
									{						
										if( UniqueBookDataList[k].TypeID !=  rBookDataList[j].TypeID)
											continue;							
									}
									if(UniqueBookDataList[k].whichTab == 4 && UniqueBookDataList[k].elementID == -101 ) // for Item Table in Tabbed text
									{						
										if( UniqueBookDataList[k].TypeID !=  rBookDataList[j].TypeID)
											continue;							
									}					
									rBookDataList[j].isSelected = UniqueBookDataList[k].isSelected;				
								}
							}
						}
					}
					
				}
			}


			AcquireWaitCursor awc;
			awc.Animate();
			
			K2Vector<bool16>::iterator  itr; 
			itr=bookIsSelected.begin();
			int32 checkedNumber=0;
//-------------- //by amarjit patil------------------------------------------------------
			for(int32 listIndex=0;listIndex<rBookDataList.size();listIndex++)
            {
  				if(rBookDataList[listIndex].isSelected)
				{
				  int32 index1 =  rBookDataList[listIndex].whichTab;
                  if(index1==3)
				  {
                   double eelementid = rBookDataList[listIndex].elementID;
			       itemgroup_attributeids.push_back(eelementid);

				   PMString jkk("itemgroupattribute =");
					jkk.AppendNumber(eelementid);
					//CA(jkk);
				  }
				  if(index1==4)
				  {
				   double eelementid = rBookDataList[listIndex].elementID;
				   item_attribute_ids.push_back(eelementid);
				   PMString kkk("itemattribute=");
				   kkk.AppendNumber(eelementid);
				  // CA(kkk);
				  }
				}
			}
			for(int32 i=0;i<item_attribute_ids.size();i++)
			{
				Itemattribute_Itemgroupattribute.push_back(item_attribute_ids.at(i));
			}
			PMString jkk("itemgroupattribute =");
			jkk.AppendNumber(static_cast<int32>(Itemattribute_Itemgroupattribute.size()));
			//CA(jkk);

			for(int32 i=0;i<itemgroup_attributeids.size();i++)
			{
				Itemattribute_Itemgroupattribute.push_back(itemgroup_attributeids.at(i));
			}
			PMString jk("itemgroupattribute =");
			jk.AppendNumber(static_cast<int32>(Itemattribute_Itemgroupattribute.size()));
			//CA(jk);
			int32 siz =static_cast<int32>(item_attribute_ids.size());
			/*PMString kkk("tem_attribute_ids size =");
			kkk.AppendNumber(siz);
			CA(kkk);*/

			int32 siz11 =static_cast<int32>(itemgroup_attributeids.size());
			/*PMString jkk("itemgroup_attributeids size =");
			jkk.AppendNumber(siz11);
			CA(jkk);*/
//==========================================END==================================================================================			
			/*for(checkedNumber; itr!=bookIsSelected.end(); itr++)
			{
				if(*itr==kTrue)
					checkedNumber++;
			}*/

			InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
			if(ptrIAppFramework == nil)
				return ;


			InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
				return ; 
			}
			
			IBook * activeBook = bookManager->GetCurrentActiveBook();
			if(activeBook == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
				return ;			
			}
			PMString bookName = activeBook->GetBookTitleName();
			

			PlatformChar pchar('.');
			CharCounter index= bookName.IndexOfCharacter(pchar);
			
			PMString bk = bookName;
			bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indb"
			
			BookReportData::setCurrentBookName(bk);
			


			
			
			

			//ChangedTextList.clear();
			//RefreshFlag = kTrue;
			
			//CA("kTPLRefreshIconSuiteWidgetID");
			//bool16 retVal=0;	
			
			//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
			//ptrIAppFramework->GETCommon_refreshClientCache();
			
			//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			
			//if(retVal==0)
			//{
			//	//CA("retVal==0");
   //          	break;
			//}
			//CA("kTPLRefreshIconSuiteWidgetID 2");
			//HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			
			//int PreselectedRadioBUtton = Mediator::selectedRadioButton;
	//		K2Vector<bool16>  bookIsSelected; 

			if(bookIsSelected.size() == 0)
			{
				//CA("bookIsSelected.size() == 0");
				return;
			}

			int32 index1=0;
			itr=bookIsSelected.begin();
			for(int32 p=0; p< BookFileList.size(); p++,itr++)
			{	

				SDKLayoutHelper sdklhelp;
				UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
				ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
				Refresh refresh;	


//				UIDList selectUIDList;
//				InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
//				if (layoutData == nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
//					break;
//				}
//
//				UIDRef newSpreadRef = layoutData->GetSpreadRef();
//				
//				UID pageUID1 = layoutData->GetPage();
//				if(pageUID1 == kInvalidUID)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
//					break;
//				}
//				IDocument* doc1=Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
//				if(doc1==nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No doc");
//					return;
//				}
//				IDataBase* database = ::GetDataBase(doc1);
//				if(database==nil)
//				{
//					ptrIAppFramework->LogDebug("AP46_ProductFinder::ProductSpray::startSpraying::No database");
//					return;
//				}
//
//
//				 InterfacePtr<IPageList> iPageList(doc1,UseDefaultIID());
//				 if (iPageList == nil){
//					// CA("iPageList == nil");
//					 break;
//				 }
//		 
//				 int32 totalNumOfPages = iPageList->GetPageCount();
//				 InterfacePtr<ISpreadList> iSpreadList1((IPMUnknown*)doc1,UseDefaultIID());
//				if (iSpreadList1==nil)
//				{
//					ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::iSpreadList==nil");				
//					return ;
//				}
//				int32 noofspread= iSpreadList1->GetSpreadCount();
//				PMString spreadsize("spreadsize :");
//				spreadsize.AppendNumber(noofspread);
//				//CA(spreadsize);
//				int32 countofframes = 0;
//
//				for(int32 numSp_1=0; numSp_1< noofspread; numSp_1++)
//				{
//					//CA("numSp_1");
//					UIDRef temp_spreadUIDRef1(database, iSpreadList1->GetNthSpreadUID(numSp_1));
//					//CA("newSpreadRef == temp_spreadUIDRef1");
//					InterfacePtr<ISpread> spread(temp_spreadUIDRef1, UseDefaultIID());
//					if(!spread)
//					{
//						ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
//						return ;
//					}
//					int32 numPages=spread->GetNumPages();
//
//					PMString noofpages("noofpages :");
//					noofpages.AppendNumber(numPages);
//					//CA(noofpages);
//
//					for(int32 i=0;i<numPages;i++)
//					{
//						UIDList itemsOnPageUIDList(database) ;
//						spread->GetItemsOnPage(i, &itemsOnPageUIDList,kFalse);
//						selectUIDList=itemsOnPageUIDList;
//
//						PMString uidlist("selectUIDList First:");
//						uidlist.AppendNumber(static_cast<int32>(selectUIDList.Length()));
//					    //CA(uidlist);
//
//						for( int32 p=0; p<selectUIDList.Length(); p++)
//						{
//							/*PMString str;
//							str.AppendNumber(p);
//
//							CA("lllllllllll");
//							CA(str);*/
//							countofframes++;
//						}
//					}
//				}
//				PMString str;
//				str.AppendNumber(countofframes);
//				//CA("lllllllllll");
//				CA(str);

				

				//	PMString tempString("Refreshing Document : ");
				//	tempString += bookContentNames[p];
				//	tempString += "...";
				//	progressBar.SetTaskText(tempString);
				//for(int32 i = countofframes - 1 ; i >=0 ; --i)
				//{
				//	
				///*progressBar.SetPosition(index1);
				//index1++;*/
				//	Sleep(100);
				//	progressBar.SetPosition(countofframes - i + 1);
				//}
//till here

				ptrIAppFramework->LogDebug("*** Inside doc for ***");
				if(*itr==kFalse)//------					
					continue;
				
				if(p > 0)
					CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
				//****	
				PlatformChar pchar('.');
				CharCounter index= bookContentNames[p].IndexOfCharacter(pchar);
				
				PMString bk = bookContentNames[p];
				bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indb"
				BookReportData::currentSection = bk;
				//CA("BookReportData::currentSection   "+BookReportData::currentSection );
				//*****

					
			
				//Mediator::selectedRadioButton = 4;  // Entire Doc Option
				
				BookReportData::vecReportRows.clear();

				ptrIAppFramework->LogDebug("*** before doRefresh(4) ***");
				//ItemId_ItemgroupId.clear();//ItemId_ItemgroupId.clear();

//added by sagar
				PMString tempString("Refreshing Document : ");
				tempString += bookContentNames[p];
				tempString += "...";
				tempString.SetTranslatable(kFalse);
                
                ptrIAppFramework->EventCache_clearCurrentSectionData();
                ptrIAppFramework->clearAllStaticObjects();
//
				//refresh.doRefreshNewoption(4,bookName,tempString); //Apsiva Comment
				refresh.doRefresh(4,bookName,tempString);
				
				
				ptrIAppFramework->LogDebug("*** after doRefresh(4) ***");

				
				isnewSection = kTrue;

//PMString a("BookFileList.size()	:	");
//a.AppendNumber(static_cast<int32>(BookFileList.size()));
//a.Append("\rp	:	");
//a.AppendNumber(p);
//a.Append("\rBookReportData::vecReportRows.size()	:	");
//a.AppendNumber(static_cast<int32>(BookReportData::vecReportRows.size()));
//CA(a);
				sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
				sdklhelp.CloseDocument(CurrDocRef, kFalse);				
			}	

			ISRfreshBookDlgOpen = kFalse;
			DocWchUtils::StartDocResponderMode();
//5-june chetan
			//hide the Second group panel
	
			secondGroupPanelControlView->HideView();
	
			IControlView * firstInteractiveGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
			if(firstInteractiveGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::firstInteractiveGroupPanelControlData == nil");
				break;
			}
			firstInteractiveGroupPanelControlData->HideView();

			interactiveListNilGroupPanelControlData->HideView();

			//Show the ThirdGroupPanel
			
			thirdGroupPanelControlView->ShowView();
	
			IControlView* TextWidget1 = panelControlData->FindWidget(kItemUpdatedWidgetID);
			if(!TextWidget1)
			{
				//CA("TextWidget1 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text1;
				int32/*int64*/ TotalUpdatedItem = /*tempItemList.size()*/static_cast<int32>(itemsUpdated.size()); 
				text1.AppendNumber(PMReal(TotalUpdatedItem));
				textcontrol->SetString(text1);			
			}

			IControlView* TextWidget2 = panelControlData->FindWidget(kItemDeletedWidgetID);
			if(!TextWidget2)
			{
				//CA("TextWidget2 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text2;
				int32/*int64*/ TotalDeletedItem =static_cast<int32> (itemsDeleted.size()); 
				text2.AppendNumber(PMReal(TotalDeletedItem));
				textcontrol->SetString(text2);			
			}

			IControlView* TextWidget3 = panelControlData->FindWidget(kProdUpdatedWidgetID);
			if(!TextWidget3)
			{
				//CA("TextWidget3 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				PMString text3;
				int32/*int64*/ TotalUpdatedProd =static_cast<int32> /*tempProductList.size()*/(productsUpdated.size()); 
				text3.AppendNumber(PMReal(TotalUpdatedProd));
				textcontrol->SetString(text3);			
			}

			IControlView* TextWidget4 = panelControlData->FindWidget(kProdDeletedWidgetID);
			if(!TextWidget4)
			{
				//CA("TextWidget4 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text4;
				int32/*int64*/ TotalDeletedProd =static_cast<int32> (productsDeleted.size()); 
				text4.AppendNumber(PMReal(TotalDeletedProd));
				textcontrol->SetString(text4);			
			}

			IControlView* TextWidget5 = panelControlData->FindWidget(kItemImagesRelinkWidgetID);
			if(!TextWidget5)
			{
				//CA("TextWidget5 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget5,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text5;
				int32/*int64*/ TotalItemImagesRelink = static_cast<int32>(itemImagesReLinked.size()); 
				text5.AppendNumber(PMReal(TotalItemImagesRelink));
				textcontrol->SetString(text5);			
			}

			IControlView* TextWidget6 = panelControlData->FindWidget(kProdImagesRelinkWidgetID);
			if(!TextWidget6)
			{
				//CA("TextWidget6 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget6,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text6;
				int32/*int64*/ TotalProdImagesRelink =static_cast<int32> (productImagesReLinked.size());
				text6.AppendNumber(PMReal(TotalProdImagesRelink));
				textcontrol->SetString(text6);			
			}

			IControlView* TextWidget7 = panelControlData->FindWidget(kItemImagesDeletedWidgetID);
			if(!TextWidget7)
			{
				//CA("TextWidget7 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget7,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text7;
				int32/*int64*/ TotalItemImagesDeleted =static_cast<int32> (itemImagesDeleted.size()); 
				text7.AppendNumber(PMReal(TotalItemImagesDeleted));
				textcontrol->SetString(text7);			
			}

			IControlView* TextWidget8 = panelControlData->FindWidget(kProdImagesDeletedWidgetID);
			if(!TextWidget8)
			{
				//CA("TextWidget8 == nil");
			}
			else
			{	

				InterfacePtr<ITextControlData> textcontrol(TextWidget8,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text8;
				int32/*int64*/ TotalProdImagesDeleted =static_cast<int32> (productImagesDeleted.size()); 
				text8.AppendNumber(PMReal(TotalProdImagesDeleted));
				textcontrol->SetString(text8);			
			}
//****Added from Here Date:7/7/08 Sachin Sharma
			
			InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if (application == nil) 
			{	
				//CA("MYYYYapplication == nil"); 
				break;
			}	
			InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			if (dialogMgr == nil) 
			{
				//CA("MyyyyydialogMgr == nil");
				break;
			}
			IWindow* iWindow = dialogMgr->GetFrontmostDialogWindow();
			if(iWindow == nil )
			{
				//CA("MyyyyyiWindow == nil");
				break;
			}
			PMString title("Refresh Summary");
			title.SetTranslatable(kFalse);
			iWindow->SetTitle(title);
            
           
		}

			
		//content else finish
//		else if(theSelectedWidget==kRfhRefreshTabByAttributeCheckBoxWidgetID && theChange==kTrueStateMessage)
//		{		
//			CA("Checked IsHiliteFlagSelected");
////			refreshTableByAttribute =kTrue;
	
////**************************ADDED BY LALIT ********************
////			IControlView* clusterPanelview = panelControlData->FindWidget(kClusterRadiowidgetid);
////			if(clusterPanelview == nil)
////			{
////				//CA("clusterPanelview == nil");
////				break;
////			}				
////			clusterPanelview->Enable();	
//
//			isRefreshTabByAttributeCheckBoxSelected=kTrue;
////till here						
//		}
//		else if(theSelectedWidget==kRfhRefreshTabByAttributeCheckBoxWidgetID && theChange==kFalseStateMessage)
//		{		
//			CA("Unchecked IsHiliteFlagSelected");
//		//	refreshTableByAttribute =kFalse;	
////added by sagar			
//////**************************ADDED BY LALIT ********************
//
////			IControlView* clusterPanelview = panelControlData->FindWidget(kClusterRadiowidgetid);
////			if(clusterPanelview == nil)
////			{
////				//CA("clusterPanelview == nil");
////				break;
////			}				
////			clusterPanelview->Disable();
//			isRefreshTabByAttributeCheckBoxSelected=kFalse;
////till here
//						
//		}
////////// chetan end
		else if(theSelectedWidget==kRfhHighliteCheckBoxWidgetID && theChange==kTrueStateMessage)
		{		//CA("Checked IsHiliteFlagSelected");
				IsHiliteFlagSelected =kTrue;			
		}
		else if(theSelectedWidget==kRfhHighliteCheckBoxWidgetID && theChange==kFalseStateMessage)
		{		//CA("Unchecked IsHiliteFlagSelected");
				IsHiliteFlagSelected =kFalse;			
		}
		else if(theSelectedWidget==kRfhDeletFrameCheckBoxWidgetID && theChange==kTrueStateMessage)
		{		//CA("Checked IsDeleteFlagSelected");
				IsDeleteFlagSelected =kTrue;			
		}
		else if(theSelectedWidget==kRfhDeletFrameCheckBoxWidgetID && theChange==kFalseStateMessage)
		{		//CA("Unchecked IsDeleteFlagSelected");
				IsDeleteFlagSelected =kFalse;			
		}
		//Added on 30May by Yogesh
		else if(theSelectedWidget==kRfhSelectAllCheckBoxWidgetID )//&& theChange==kTrueStateMessage )
		{	
			//CA("theSelectedWidget==kRfhSelectAllCheckBoxWidgetID......");
			//Added By Dattatray on 30/10 to deselect all the invidiual checkboxes(icons) when selectAll checkbox is deselected.
			if(theChange==kFalseStateMessage && IsEventInListBox == kFalse)
			{
				//CA("Inside IsEventInListBox == kFalse");
				//SDKListBoxHelper listHelper(this, kRfhPluginID);
				IControlView * listBox = Mediator ::BooklistControlView;
				if(listBox == nil) 
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update:: listBox == nil");			
					break;
				}
				for(int32 listIndex=0;listIndex<rBookDataList.size();listIndex++)
				{
					if(rBookDataList[listIndex].isSelected)
					{
						rBookDataList[listIndex].isSelected = kFalse;
						UniqueBookDataList[listIndex].isSelected = kFalse;
						OriginalrBookDataList[listIndex].isSelected = kFalse;
						
						/*listHelper.CheckUncheckRow
						(
							listBox,
							listIndex,
							(rBookDataList[listIndex].isSelected)
							
						);*/
					}
				}
			}
		//upto here----------By Dattatray on 30/10
			else if(theChange==kTrueStateMessage)
			{
				//CA("Inside theChange==kTrueStateMessage");
				/*rBookDataList.clear();
				UniqueBookDataList.clear();
				OriginalrBookDataList.clear();

				RfhDlgDialogController dlgController(this);
				dlgController.StartBookRefresh();	*/

				for(int32 listIndex=0;listIndex<rBookDataList.size();listIndex++)
				{
					if(!rBookDataList[listIndex].isSelected)
					{
						rBookDataList[listIndex].isSelected = kTrue;
						UniqueBookDataList[listIndex].isSelected = kTrue;
						OriginalrBookDataList[listIndex].isSelected = kTrue;
						
						/*listHelper.CheckUncheckRow
						(
							listBox,
							listIndex,
							(rBookDataList[listIndex].isSelected)
							
						);*/
					}
				}

				IsEventInListBox = kFalse;//This is used to handle Deselect All the CheckBoxes(icon) when unchecked selectAllCheckBox ......
			}


			TreeDisplayOption = 3; // Book Refresh Field Tree.

			InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::BooklistControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			RFHTreeDataCache dc;
			dc.clearMap();

			RFHTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

		}
		else if( (theSelectedWidget==kRfhDlgNext1ButtonWidgetID || (theSelectedWidget== kOKButtonWidgetID && firstGroupPanelControlData->IsVisible())) && ( theChange==kTrueStateMessage))
		{//CA("Next");
			//Hide the firstGroupPanel

				IControlView * originalGroupPanelControlData = panelControlData->FindWidget(kRfhoriginalGroupPanelWidgetID);
				if(originalGroupPanelControlData == nil)
				{
					//CA("secondGroupPanelControlData == nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
					return;
				}
				originalGroupPanelControlData->ShowView();

				IControlView * newoptionGroupPanelControlData = panelControlData->FindWidget(kRfhNewoptionGroupPanelWidgetID);
				if(newoptionGroupPanelControlData == nil)
				{
					//CA("secondGroupPanelControlData == nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
					return;
				}
				newoptionGroupPanelControlData->HideView();
//////////////	Amit				
				IControlView* RfhDeleteChkBxView = panelControlData->FindWidget(kRfhDeleteCheckBoxWidgetID);
				if(!RfhDeleteChkBxView)
				{		
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhDeleteChkBxView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData>DeleteCheckBoxTriState(RfhDeleteChkBxView,UseDefaultIID());
				if(DeleteCheckBoxTriState == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::DeleteCheckBoxTriState == nil");
					return;
				}
				IsDeleteFlagSelected = DeleteCheckBoxTriState->IsSelected();
				
				
				/*IControlView* RfhRefreshTableByAttrRadioBtnView = panelControlData->FindWidget(kNew_Refresh_ByCell_RadioButtonWidgetID);
				if(!RfhRefreshTableByAttrRadioBtnView)
				{		
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByAttrRadioBtnView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData>RefreshTableByAttRadioTriState(RfhRefreshTableByAttrRadioBtnView,UseDefaultIID());
				if(RefreshTableByAttRadioTriState == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByAttRadioTriState == nil");
					return;
				}
				refreshTableByAttribute = RefreshTableByAttRadioTriState->IsSelected();*/
	/////////////////	Amit End
				rBookDataList.clear();
				UniqueBookDataList.clear();
				OriginalrBookDataList.clear();

				refreshModeSelected = 0; //**** Added on 3/2/2010
				objDialogCont.StartBookRefresh();
				
				firstGroupPanelControlData->HideView();
				//Show the secondGroupPanel
				secondGroupPanelControlView->ShowView();
		}
		else if((theSelectedWidget == kRfhlastcancelCheckBoxWidgetID) && (theChange == kTrueStateMessage))// || (theSelectedWidget== kCancelButton_WidgetID && firstGroupPanelControlData->IsVisible()) )&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRfhDlgCancel1ButtonWidgetID");
			//ptrIAppFramework->LogInfo("CloseDialog 1");
			CDialogObserver::CloseDialog();
			break;
		}//kRfhlastnextCheckBoxWidgetID

	//else if((theSelectedWidget == kRfhlastnextCheckBoxWidgetID) && (theChange == kTrueStateMessage))
	//{
	//	 IControlView * originalGroupPanelControlData = panelControlData->FindWidget(kRfhoriginalGroupPanelWidgetID);
	//	if(originalGroupPanelControlData == nil)
	//	{
	//		CA("secondGroupPanelControlData == nil");
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
	//		return;
	//	}
	//	originalGroupPanelControlData->HideView();

	//	IControlView *lastGroupPanelControlData = panelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
	//	if(lastGroupPanelControlData == nil)
	//	{
	//		CA("secondGroupPanelControlData == nil");
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
	//		return;
	//	}
	//	lastGroupPanelControlData->HideView();

	//	IControlView * newoptionGroupPanelControlData = panelControlData->FindWidget(kRfhNewoptionGroupPanelWidgetID);
	//	if(newoptionGroupPanelControlData == nil)
	//	{
	//		CA("secondGroupPanelControlData == nil");
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
	//		return;
	//	}
	//	newoptionGroupPanelControlData->ShowView();

	//	IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
	//	if(secondGroupPanelControlData == nil)
	//	{
	//		CA("secondGroupPanelControlData == nil");
	//		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
	//		return;
	//	}
	//	rBookDataList.clear();
	//		UniqueBookDataList.clear();
	//		OriginalrBookDataList.clear();

	//		refreshModeSelected = 0; //**** Added on 3/2/2010
	//		objDialogCont.StartBookRefresh();

	//	secondGroupPanelControlData->ShowView();

	//}

		else if((theSelectedWidget == kRfhnewbackCheckBoxWidgetID) && (theChange == kTrueStateMessage))// || (theSelectedWidget== kCancelButton_WidgetID && firstGroupPanelControlData->IsVisible()) )&& (theChange == kTrueStateMessage))
		{
			//firstGroupPanelControlData->ShowView();
			//Show the secondGroupPanel
			IControlView * lastaddedInteractiveGroupPanelControlView = panelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
			if(lastaddedInteractiveGroupPanelControlView == nil)
			{
				CA("firstInteractiveGroupPanelControlView == nil");
				return;
			}	
			lastaddedInteractiveGroupPanelControlView->HideView();
			IControlView * BookListGroupPanelControlView = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
			if(BookListGroupPanelControlView == nil)
			{
				CA("firstInteractiveGroupPanelControlView == nil");
				return;
			}	
			BookListGroupPanelControlView->ShowView();
			
		}

		else if((theSelectedWidget == kRfhDlgCancel1ButtonWidgetID  || (theSelectedWidget== kCancelButton_WidgetID && firstGroupPanelControlData->IsVisible()) )&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRfhDlgCancel1ButtonWidgetID");
			//ptrIAppFramework->LogInfo("CloseDialog 1");
			CDialogObserver::CloseDialog();
			break;
		}
		else if(theSelectedWidget == kRfhDlgCancel2ButtonWidgetID && theChange == kTrueStateMessage)
		{
             ptrIAppFramework->LogInfo("CloseDialog 2");
			 CDialogObserver::CloseDialog();
			 break;
		}
		else if(theSelectedWidget == kRfhnew14Back1ButtonWidgetID && theChange == kTrueStateMessage)
		{
         IControlView * lastControlView = panelControlData->FindWidget(kRfhMidBookListGroupPanelWidgetID);
			if(lastControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			lastControlView->ShowView();

			IControlView * secondControlView = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
			if(secondControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			secondControlView->HideView();
		}else if(theSelectedWidget ==kRfhDlgCancel123ButtonWidgetID)
		{
			//ptrIAppFramework->LogInfo("CloseDialog 2");
			CDialogObserver::CloseDialog();
			break;
		}
		else if(( theSelectedWidget == kRfhDlgCancel2ButtonWidgetID  || (theSelectedWidget== kCancelButton_WidgetID && secondGroupPanelControlView->IsVisible()) ) &&  (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRfhDlgCancel2ButtonWidgetID");
			//ptrIAppFramework->LogInfo("CloseDialog 2");
			CDialogObserver::CloseDialog();
			break;
		}
		else if(theSelectedWidget == kRfhDlgBack1ButtonWidgetID && theChange == kTrueStateMessage)
		{
			firstGroupPanelControlData->ShowView();
			//Show the secondGroupPanel
			secondGroupPanelControlView->HideView();
		}
		else if(( theSelectedWidget == kRfhFinishButtonWidgetID  || (theSelectedWidget== kOKButtonWidgetID && thirdGroupPanelControlView->IsVisible())) &&  (theChange == kTrueStateMessage))
		{
/////////// chetan
//			refreshTableByAttribute = kTrue;
//////////  chetan end
			//CA("theSelectedWidget == kRfhFinishButtonWidgetID");
			//ptrIAppFramework->LogInfo("CloseDialog 3");
			CDialogObserver::CloseDialog();
			break;
		}
		//Added by amarjit patil
		else if(theSelectedWidget==kRfhDlgNextButtonWidgetID)//Next button on First Dialog
		{
		  IControlView * newModeControlView = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
			if(newModeControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			IControlView * zerothModeControlView = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
			if(newModeControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			zerothModeControlView->HideView();
			newModeControlView->ShowView();

			IControlView * hidecancelbuttonCtrlVw = panelControlData->FindWidget(kfirstcancelWidgetID);
			if(hidecancelbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidecancelbuttonCtrlVw->ShowView();

			IControlView * hideNextbuttonCtrlVw = panelControlData->FindWidget(kfirstnextWidgetID);
			if(hideNextbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideNextbuttonCtrlVw->ShowView();
		}
		else if( theSelectedWidget == kfirstcancelWidgetID ) //Cancel button on second dialog
		{
			ptrIAppFramework->LogInfo("CloseDialog 4");
			CDialogObserver::CloseDialog();

			//ptrIAppFramework->GetSectionData_clearSectionDataCache();
			//ptrIAppFramework->multipleSectionDataClearInstance();
			break;
		}
		else if(theSelectedWidget==kfirstnextWidgetID )
		{
			//CA("Zeroth Next Button");
            IControlView* RfhcontentStructerRadioBtnView = panelControlData->FindWidget(kcontentstructureWidgetID);
			if(!RfhcontentStructerRadioBtnView)
			{		
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData>contentstructerRadioTriState(RfhcontentStructerRadioBtnView,UseDefaultIID());
			if(contentstructerRadioTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByCellRadioTriState == nil");
				return;
			}
			refreshTableByAttribute = !(contentstructerRadioTriState->IsSelected());
            /*if(refreshTableByAttribute)
			{*/
			IControlView * hidezerothgrouppanelCtrlVw = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
			if(hidezerothgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidezerothgrouppanelCtrlVw->HideView();

			IControlView * hideFirstgrouppanelCtrlVw = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
			if(hideFirstgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideFirstgrouppanelCtrlVw->HideView();

			IControlView * hidecancelbuttonCtrlVw = panelControlData->FindWidget(kfirstcancelWidgetID);
			if(hidecancelbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidecancelbuttonCtrlVw->HideView();

			IControlView * hideNextbuttonCtrlVw = panelControlData->FindWidget(kfirstnextWidgetID);
			if(hideNextbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideNextbuttonCtrlVw->HideView();

			IControlView * reportModeControlView = panelControlData->FindWidget(kReportWithRefreshWidgetID);
			if(reportModeControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>refreshModeTriState(reportModeControlView,UseDefaultIID());
			if(refreshModeTriState == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::DeleteCheckBoxTriState == nil");
				return;
			}
			Mediator::isSilentModeSelected = refreshModeTriState->IsSelected();

					
			//CA("Mediator::isSilentModeSelected = kTrue");
			InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
			if(ptrIAppFramework == nil)
				return ;

			InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
				return ; 
			}
			
			IBook * activeBook = bookManager->GetCurrentActiveBook();
			if(activeBook == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
				return ;			
			}

			InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				CA("This book doesn't have a book content manager!  Something is wrong.");
				return;
			}
			
			bookContentNames.clear(); 
			K2Vector<PMString> bookContentNames = GetBookContentNames(bookContentMgr);
			
			int bookContentsCount = bookContentNames.size();
			if (bookContentsCount < 0)
			{
				CA("This book doesn't have any content!");
				return;
			}
			//------------------- --------------------
			//Show the Mid BookList GroupPanel
			midBookListGroupPanelControlView->ShowView();
			//Hide the ZerothGroupPanel
			zerothGroupPanelControlView->HideView();

		//--------AddElement(Book) ListBox---------------
			IControlView *bookListBoxControlView=panelControlData->FindWidget(/*kRfhBookListBoxWidgetID*/kRfhCRBookDocTreeViewWidgetID);
			if(bookListBoxControlView==nil)
			{
				CA("bookListBoxControlView==nil");
				break;
			}										 
			//SDKListBoxHelper booklistBoxHelper(this, kRfhPluginID);
			K2Vector<PMString>::iterator  bookItrator; 
			//booklistBoxHelper.EmptyCurrentListBox(bookListBoxControlView);
			bookIsSelected.clear();
			BookDocumentDataList.clear();
			int32 index=0;
			for(bookItrator=bookContentNames.begin();bookItrator != bookContentNames.end();bookItrator++)
			{
				//CA("listbox2");
				//booklistBoxHelper.AddElement(bookListBoxControlView,*bookItrator,kRfhDlgTextWidgetID,index,kTrue);
				//booklistBoxHelper.CheckUncheckRow(bookListBoxControlView,index,kTrue);
				bookIsSelected.push_back(kTrue);
				index++;

				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=index;
				rData.isObject=kFalse;
				rData.isSelected=kTrue;
				rData.name=*bookItrator;
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kFalse;
				rData.LanguageID = 1;
				//rData.TypeID = it->getElement_type_id();		
				rData.isImageFlag=kFalse;
				rData.whichTab = 1;
				BookDocumentDataList.push_back(rData);
			}


			TreeDisplayOption = 2; // Book Refresh Document Tree,

			InterfacePtr<ITreeViewMgr> treeViewMgr(bookListBoxControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			RFHTreeDataCache dc;
			dc.clearMap();

			RFHTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

		//}
		/*else
		{
           IControlView * lastaddedInteractiveGroupPanelControlView = panelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
			if(lastaddedInteractiveGroupPanelControlView == nil)
			{
				CA("firstInteractiveGroupPanelControlView == nil");
				return;
			}	
			lastaddedInteractiveGroupPanelControlView->ShowView();

			IControlView * hidezerothgrouppanelCtrlVw = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
			if(hidezerothgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidezerothgrouppanelCtrlVw->HideView();

			IControlView * hideFirstgrouppanelCtrlVw = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
			if(hideFirstgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideFirstgrouppanelCtrlVw->HideView();

			IControlView * hidecancelbuttonCtrlVw = panelControlData->FindWidget(kfirstcancelWidgetID);
			if(hidecancelbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidecancelbuttonCtrlVw->HideView();

			IControlView * hideNextbuttonCtrlVw = panelControlData->FindWidget(kfirstnextWidgetID);
			if(hideNextbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideNextbuttonCtrlVw->HideView();
		}*/
		}
		else if(( theSelectedWidget == kRfhDlgCancelButtonWidgetID  || ( theSelectedWidget == kCancelButton_WidgetID  &&  zerothGroupPanelControlView->IsVisible() )) && (theChange == kTrueStateMessage ))
		{
			ptrIAppFramework->LogInfo("CloseDialog 4");
			CDialogObserver::CloseDialog();

			//ptrIAppFramework->GetSectionData_clearSectionDataCache();
			//ptrIAppFramework->multipleSectionDataClearInstance();
			break;
		}
		else if(theSelectedWidget == kRfhDlgBackButtonWidgetID && theChange == kTrueStateMessage)
		{
			firstGroupPanelControlData->HideView();
			//Show the secondGroupPanel
			midBookListGroupPanelControlView->ShowView();
		}	
		
		else if(( theSelectedWidget==kRfhDlgBookListNextButtonWidgetID || ( theSelectedWidget == kOKButtonWidgetID && midBookListGroupPanelControlView->IsVisible())) &&  (theChange==kTrueStateMessage))
		{
			//CA("Book List Next Button");
						//Hide the Mid GroupPanel

			// For Tree book list widget get the selection first from check box and update bookIsSelected.
			if(UniqueCRBookDocNodeIds.size() > 0)
			{
			
				set<NodeID>::iterator it;
				for(it=UniqueCRBookDocNodeIds.begin(); it!=UniqueCRBookDocNodeIds.end(); it++ )
				{
					NodeID nid= (NodeID)*it;

					IControlView *bookListBoxControlView=panelControlData->FindWidget(/*kRfhBookListBoxWidgetID*/kRfhCRBookDocTreeViewWidgetID);
					if(bookListBoxControlView==nil)
					{
						CA("bookListBoxControlView==nil");
						break;
					}	

					InterfacePtr<ITreeViewMgr> treeViewMgr(bookListBoxControlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::!treeViewMgr");					
						continue ;
					}
					//QueryWidgetFromNode 

					InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
					ASSERT(panelControlData);
					if(panelControlData==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::panelControlData is nil");		
						continue ;
					}

					IControlView* checkBoxCntrlView=panelControlData->FindWidget(kRfhCheckBoxWidgetID);
					if(checkBoxCntrlView==nil) 
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::checkBoxCntrlView == nil");
						continue ;
					}
		
					InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::itristatecontroldata is nil");		
						continue ;
					}

					TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

					int32 uid= uidNodeIDTemp->Get();
					int32 TextFrameuid = uidNodeIDTemp->Get();
					RfhDataNode rNode;
					RFHTreeDataCache dc1;

					dc1.isExist(uid, rNode);

					PMString fieldName = rNode.getFieldName();

					for(int32 k =0; k < BookDocumentDataList.size(); k ++)
					{
						if(BookDocumentDataList[k].name == fieldName)
						{
							if(itristatecontroldata->IsSelected())
							{
								BookDocumentDataList[k].isSelected = kTrue;
								bookIsSelected[k] = kTrue; 
							}
							else
							{
								BookDocumentDataList[k].isSelected = kFalse;
								bookIsSelected[k] =  kFalse ;
							}							
						}
					}
					
				}
			}	



			//refreshTableByAttribute = contentstructerRadioTriState->IsSelected();
            refreshTableByAttribute = (contentRadioTriState->IsSelected());
			if(!refreshTableByAttribute) // for Structure refresh
			{
		 		midBookListGroupPanelControlView->HideView();

				//Show the firstGroupPanel
				if(Mediator::isSilentModeSelected )
				{
					firstGroupPanelControlData->ShowView();
					shouldRefreshFlag = kTrue;
				}
				else
				{
					//ptrIAppFramework->clearAllStaticObjects();
					itemDescriptionAttributeID = -1;
					itemGroupDescriptionElementID = -1;
					//CA("Mediator::isSilentModeSelected == kFalse");

					InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
					if(ptrIClientOptions==nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillBMSImageInBox::Interface for IClientOptions not found.");
						break;
					}
					imageDownLoadPath=ptrIClientOptions->getImageDownloadPath();


					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					
					firstGroupPanelControlData->ShowView();
					interactiveListNilGroupPanelControlData->HideView();
					
					IControlView* dropDownControlView = panelControlData->FindWidget(kRfhProcessOptionsDropDownWidgetID);
					if (dropDownControlView == nil)
					{
						CA("dropDownControlView == nil");
						break;
					}
					
					AcquireWaitCursor awc;
					awc.Animate();

					InterfacePtr<IDropDownListController> iDropDownListController(dropDownControlView, UseDefaultIID());
					if (iDropDownListController == nil)
					{
						//CA("iDropDownListController == nil");
						break;
					}
					iDropDownListController->Select(0,kFalse,kFalse);
					
					objDialogCont.StartBookRefresh();				
					
					selectedDocIndexList.clear();
					for(int32 docIndex = 0 ; docIndex < bookIsSelected.size() ; docIndex++)
					{
						if(bookIsSelected[docIndex] == kTrue)
							selectedDocIndexList.push_back(docIndex);
						
					}
					
					//for(int32 docIndex = 0 ; docIndex < selectedDocIndexList.size() ; docIndex++)
					if(selectedDocIndexList.size() > 0)
					{
						Refresh refreshObj;
						if(!refreshObj.getMultipleSectionData())
						{
							//CA("refreshObj.getMultipleSectionData returning kFalse");
						}	

						//CA("before ptrIAppFramework->switchSectionData");

						//ptrIAppFramework->switchSectionData(CurrentSelectedDocumentSectionID);

						//CA("after ptrIAppFramework->switchSectionData");


						CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority);
						int32 docIndexInBookFileList = selectedDocIndexList[0];

						SDKLayoutHelper sdklhelp;
						UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndexInBookFileList]);
						if(CurrDocRef == UIDRef ::gNull)
						{
							CA("CurrDocRef is invalid");
							continue;
						}
						ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
						if(err == kFailure)
						{
							CA("Error occured while opening the layoutwindow of the template");
							continue;
						}
						
						
						refreshTableByAttribute = kFalse;
						refreshModeSelected = 1;
						
						fillFrameRecordVector(refreshModeSelected);
				
						currentDocumentUIDRef = CurrDocRef;
						currentDocumentIndex = 0;
						if(static_cast<int32>(frameRecordList.size()) > 0){						
							
							fillRefreshModeListBox(panelControlData);
							
							IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
							IDataBase* database= ::GetDataBase(document);
							InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
							if(!iSelectionManager)
							{
								CA("NULL");
								return;
							}
					
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) 
							{	
								CA("!layoutSelectionSuite");
								return;
							}
							layoutSelectionSuite->DeselectAllPageItems();
							
							UIDList selectUIDList(database,frameRecordList[0].frameUID);
							layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
							currentSelectedFrameIndex = 0;		
							
							SDKListBoxHelper listHelper(this, kRfhPluginID);
							IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
							listHelper.CheckUncheckRow(listBoxControlView,0,!frameRecordList[0].isSelected,0);
							frameRecordList[0].isSelected = kTrue;

							InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
							if(listCntl == nil) 
								break;
							listCntl->Select(0);

							PMString temp("Currenty Running : ");
							if(refreshModeSelected == 1)
								temp.Append("Delete");
							else if(refreshModeSelected == 2)
								temp.Append("Update");
							else if(refreshModeSelected == 3)
								temp.Append("New");
								
							IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
							if(TextWidget)
							{
								//CA("TextWidget4 == nil");
								InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
								if(!textcontrol)
								{
									//CA("textcontrol == nil");
									return;
								}
								textcontrol->SetString(temp);
							}
						}
						else{
							//CA("frameRecordList.size() == 0"); 
							firstGroupPanelControlData->HideView();
							interactiveListNilGroupPanelControlData->ShowView();
							IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
							if(TextWidget4)
							{
								//CA("TextWidget4 == nil");
								InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
								if(!textcontrol)
								{
									//CA("textcontrol == nil");
									return;
								}
								if(currentDocumentIndex < (selectedDocIndexList.size()-1)){
									
									this->goForNextDocument();
								}
								else{
									//PMString text("There are no deletes in this document.Do you want to proceed to  Updates?");
									PMString text("There are no deleted items in this document. Do you want to  proceed with item updates?");
									textcontrol->SetString(text);

									IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
									if(OkButtonControlData == nil)
									{
										ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
										break;
									}
									OkButtonControlData->HideView();
									
									IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
									if(yesButtonControlData == nil)
									{
										ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
										break;
									}
									yesButtonControlData->ShowView();
									
									IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
									if(noButtonControlData == nil)
									{
										ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
										break;
									}
									noButtonControlData->ShowView();

									IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
									if(document){
										AcquireWaitCursor awc;
										awc.Animate();

										SDKLayoutHelper sdklhelp;
										int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
										sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
										sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
									}							
								}
							}							
						}					
					}				
				
					//CA("1111111");
				}
				break;
			}
		///-------------------------another part-------------------------------------------------------
//for content only radio button
			
			
			if(refreshTableByAttribute)
			{
			
				IControlView * BookListGroupPanelControlView = panelControlData->FindWidget(kRfhMidBookListGroupPanelWidgetID);
				if(BookListGroupPanelControlView == nil)
				{
					//CA("firstInteractiveGroupPanelControlView == nil");
					return;
				}	
				BookListGroupPanelControlView->HideView();

				/*IControlView * lastaddedInteractiveGroupPanelControlView = panelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
				if(lastaddedInteractiveGroupPanelControlView == nil)
				{
					CA("firstInteractiveGroupPanelControlView == nil");
					return;
				}	
				lastaddedInteractiveGroupPanelControlView->ShowView();*/



				IControlView * newoptionGroupPanelControlData = panelControlData->FindWidget(kRfhNewoptionGroupPanelWidgetID);
				if(newoptionGroupPanelControlData == nil)
				{
					CA("secondGroupPanelControlData == nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
					return;
				}

			  newoptionGroupPanelControlData->HideView();



				IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRfhFirstGroupPanelWidgetID);
				if(firstGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::firstGroupPanelControlData == nil");
					break;
				}
				firstGroupPanelControlData->ShowView();
//till here

				IControlView * hidezerothgrouppanelCtrlVw = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
				if(hidezerothgrouppanelCtrlVw == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
					break;
				}
				hidezerothgrouppanelCtrlVw->HideView();

				IControlView * hideFirstgrouppanelCtrlVw = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
				if(hideFirstgrouppanelCtrlVw == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
					break;
				}
				hideFirstgrouppanelCtrlVw->HideView();

				IControlView * hidecancelbuttonCtrlVw = panelControlData->FindWidget(kfirstcancelWidgetID);
				if(hidecancelbuttonCtrlVw == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
					break;
				}
				hidecancelbuttonCtrlVw->HideView();

				IControlView * hideNextbuttonCtrlVw = panelControlData->FindWidget(kfirstnextWidgetID);
				if(hideNextbuttonCtrlVw == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
					break;
				}
				hideNextbuttonCtrlVw->HideView();

				IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
				if(secondGroupPanelControlData == nil)
				{
					//CA("secondGroupPanelControlData == nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
					return;
				}
				rBookDataList.clear();
					UniqueBookDataList.clear();
					OriginalrBookDataList.clear();

					refreshModeSelected = 0; //**** Added on 3/2/2010
					objDialogCont.StartBookRefresh();

				secondGroupPanelControlData->HideView();

			}
		}
		else if((theSelectedWidget == kRfhDlgBookListCancelButtonWidgetID || ( theSelectedWidget == kCancelButton_WidgetID && midBookListGroupPanelControlView->IsVisible())) &&  (theChange == kTrueStateMessage))
		{
			ptrIAppFramework->LogInfo("CloseDialog 5");
			CDialogObserver::CloseDialog();
			break;
		}
		else if(theSelectedWidget == kRfhDlgBookListBackButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("Book List Back Button");
			//Hide the MidGroupPanel
			IControlView * hideFirstgrouppanelCtrlVw = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
			if(hideFirstgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideFirstgrouppanelCtrlVw->ShowView();

			IControlView * hidecancelbuttonCtrlVw = panelControlData->FindWidget(kfirstcancelWidgetID);
			if(hidecancelbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hidecancelbuttonCtrlVw->ShowView();

			IControlView * hideNextbuttonCtrlVw = panelControlData->FindWidget(kfirstnextWidgetID);
			if(hideNextbuttonCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideNextbuttonCtrlVw->ShowView();

			IControlView * hideZerothgrouppanelCtrlVw = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
			if(hideZerothgrouppanelCtrlVw == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::reportModeControlView == nil");
				break;
			}
			hideZerothgrouppanelCtrlVw->HideView();

			
			midBookListGroupPanelControlView->HideView();
			//Show the ZerothGroupPanel
			
			//zerothGroupPanelControlView->ShowView();
		}
	//-----------SelectAll CheckBox For List------
		else if(theSelectedWidget==kRfhBookListSelectAllCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//CA("theSelectedWidget==kRfhBookListSelectAllCheckBoxWidgetID && theChange==kTrueStateMessage");
			//SDKListBoxHelper listHelper(this, kRfhPluginID);
			IControlView *listBoxControlView  = panelControlData->FindWidget(/*kRfhBookListBoxWidgetID*/kRfhCRBookDocTreeViewWidgetID);
			//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			//if(listCntl == nil) 
			//	return;

			K2Vector<PMString>::iterator itr;
			if(bookContentNames.size()==0)
			{
				//CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
				return;
			}
			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  bookIsSelected.begin();
			for(itr=bookContentNames.begin();itr!=bookContentNames.end() && itrIsSelected !=  bookIsSelected.end() ;itr++,itrIsSelected++)
			{
				//CA("Inside for loop");
				//listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
				BookDocumentDataList.at(index).isSelected = kTrue;
				(*itrIsSelected) = kTrue;
			}

			TreeDisplayOption = 2; // Book Refresh Document Tree,

			InterfacePtr<ITreeViewMgr> treeViewMgr(listBoxControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			RFHTreeDataCache dc;
			dc.clearMap();

			RFHTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();


		}
		else if(theSelectedWidget==kRfhBookListSelectAllCheckBoxWidgetID && theChange == kFalseStateMessage)
		{	//CA("theSelectedWidget==kRfhBookListSelectAllCheckBoxWidgetID && theChange == kFalseStateMessage");
			//else if(theSelectedWidget==kRfhBookListSelectAllCheckBoxWidgetID && theChange == kTrueStateMessage)
			//CA("Check Box Select");
			//SDKListBoxHelper listHelper(this, kRfhPluginID);
			IControlView *listBoxControlView  = panelControlData->FindWidget(/*kRfhBookListBoxWidgetID*/kRfhCRBookDocTreeViewWidgetID);
			//InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			//if(listCntl == nil) 
			//	return;

			K2Vector<PMString>::iterator itr;
			if(bookContentNames.size()==0)
			{
				CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
				return;
			}
			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  bookIsSelected.begin();
			for(itr=bookContentNames.begin();itr!=bookContentNames.end()&& itrIsSelected !=  bookIsSelected.end() ;itr++,itrIsSelected++)
			{
				//CA("Inside for loop");
				//listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
				BookDocumentDataList.at(index).isSelected = kFalse;
				(*itrIsSelected) = kFalse;
			}		

			TreeDisplayOption = 2; // Book Refresh Document Tree,

			InterfacePtr<ITreeViewMgr> treeViewMgr(listBoxControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			RFHTreeDataCache dc;
			dc.clearMap();

			RFHTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();
		}
///////////////////////////////////////////////////////////	Related to Interactive refresh
		
		else if(theSelectedWidget == kRfhRefreshModeDlgPrevButtonWidgetID && theChange == kTrueStateMessage){
			int32 selectedCount = 0;
			int32 selectedFrameIndex = 0;
			for(int32 index = 0 ; index < frameRecordList.size() ; index++){
				if(frameRecordList[index].isProcessed == kFalse && frameRecordList[index].isSelected == kTrue){
					selectedCount++;
					selectedFrameIndex = index;
				}
			}
			if(selectedCount > 1){
				//CA("Multiple Frames selected");
				break;
			}
			currentSelectedFrameIndex = selectedFrameIndex ;
			if(currentSelectedFrameIndex-1 <= 0 && currentDocumentIndex-1  >= 0){
				goForPreviousDocument();
			}
			else{
				selectPreviousFrame();
			}
		}
		else if(theSelectedWidget == kRfhRefreshModeDlgNextButtonWidgetID && theChange == kTrueStateMessage){
			int32 selectedCount = 0;
			int32 selectedFrameIndex = 0;
			for(int32 index = 0 ; index < frameRecordList.size() ; index++){
				if(frameRecordList[index].isProcessed == kFalse && frameRecordList[index].isSelected == kTrue){
					selectedCount++;
					selectedFrameIndex = index;
				}
			}
			if(selectedCount > 1){
				//CA("Multiple Frames selected");
				break;
			}
			else if(selectedCount == 0 && refreshModeSelected == 3){
				if(currentDocumentIndex+1  < selectedDocIndexList.size()){
					goForNextDocument();
					break;
				}
				else{
					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					
					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						
						PMString text("Interactive refresh is now complete. Your catalog pages are updated.  Do you want to close document?");
						textcontrol->SetString(text);						
						
		
						IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
						if(OkButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
							break;
						}
						OkButtonControlData->HideView();
						
						IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
						if(yesButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
							break;
						}
						yesButtonControlData->ShowView();					
						
						IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
						if(noButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
							break;
						}
						noButtonControlData->ShowView();
					}
				}
			}
			currentSelectedFrameIndex = selectedFrameIndex;
			if(currentSelectedFrameIndex+1 >= frameRecordList.size() && currentDocumentIndex+1  < selectedDocIndexList.size()){
				goForNextDocument();
			}
			else{
				selectNextFrame();
			}
		}
		else if(theSelectedWidget==kRfhRefreshModeDlgLocateButtonWidgetID && theChange==kTrueStateMessage){
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(!document)
				return;
			int32 noOfSelectedRow = 0;
			int32 selectedRowNo = -1;
			for(int32 index = 0 ; index < frameRecordList.size() ; index++){
				if(frameRecordList[index].isSelected == kTrue && frameRecordList[index].isProcessed == kFalse){
					selectedRowNo = index;
					noOfSelectedRow++;
				}
			}
			/*if(noOfSelectedRow > 1){
				CA("Selected Multiple Frames");
			}
			else*/ 
			if(noOfSelectedRow == 1){
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
				IDataBase* database= ::GetDataBase(document);

				InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
				if(!iSelectionManager)
				{
					//CA("NULL");
				}
		
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) 
				{	//CA("!layoutSelectionSuite");
					return;
				}
				layoutSelectionSuite->DeselectAllPageItems();
				
				UIDList selectUIDList(database,frameRecordList[selectedRowNo].frameUID);
				layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
				
				currentSelectedFrameIndex = selectedRowNo;
			}			
		}	
		else if(theSelectedWidget==kRfhRefreshModeDlgApplyButtonWidgetID && theChange==kTrueStateMessage){
			//CA("theSelectedWidget==kRfhRefreshModeDlgApplyButtonWidgetID");
			
			AcquireWaitCursor awc;
			awc.Animate();			
			
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(!document)
				return;
			IDataBase* database= ::GetDataBase(document);

			PMString imagePath;
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return;
			}

			if(imageDownLoadPath == "")
			{
				InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
				if(ptrIClientOptions==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillBMSImageInBox::Interface for IClientOptions not found.");
					break;
				}
				//PMString imagePath=ptrIClientOptions->getImageDownloadPath();
				imageDownLoadPath=ptrIClientOptions->getImageDownloadPath();
				
			}
			imagePath = imageDownLoadPath;

			//CA(imagePath);	
			if(imagePath!="")
			{
				const char *imageP=(imagePath.GetPlatformString().c_str()); //Cs4
				if(imageP[std::strlen(imageP)-1]!='\\' || imageP[std::strlen(imageP)-1]!=':')
					#ifdef MACINTOSH
						imagePath+=":";
					#else
						imagePath+="\\";
					#endif
			}
			
			//AcquireWaitCursor awc;
			//awc.Animate();

			int32 nonProcessedFrame = -1;
			Refresh refreshObj;
			for(int32 index = 0 ; index < frameRecordList.size() ; index++){
				if(frameRecordList[index].isSelected == kTrue && frameRecordList[index].isProcessed == kFalse){	
					//CA("Selected == kTrue && isProcessed == kFalse");					

					if(frameRecordList[index].sectionID != CurrentSelectedDocumentSectionID)
					{
						//ptrIAppFramework->switchSectionData(frameRecordList[index].sectionID);
						CurrentSelectedDocumentSectionID = frameRecordList[index].sectionID;
					}


					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
						return;
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	
						//CA("!layoutSelectionSuite");
						return;
					}
					layoutSelectionSuite->DeselectAllPageItems();
					
					UIDList selectUIDList(database,frameRecordList[index].frameUID);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
										
					if(refreshModeSelected == 2){
						IControlView* RfhRefreshTableByCellRadioBtnView = panelControlData->FindWidget(kRefreshByCellRadioButtonWidgetID);
						if(!RfhRefreshTableByCellRadioBtnView)
						{		
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
							return;
						}
						InterfacePtr<ITriStateControlData>RefreshTableByCellRadioTriState(RfhRefreshTableByCellRadioBtnView,UseDefaultIID());
						if(RefreshTableByCellRadioTriState == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByCellRadioTriState == nil");
							return;
						}
						refreshTableByAttribute = RefreshTableByCellRadioTriState->IsSelected();						
					}
					else
						refreshTableByAttribute = kFalse;

					Refresh refresh;
					Mediator::selectedRadioButton = 3;
					
					//ptrIAppFramework->multipleSectionDataClearInstance();  //*******

					refresh.GetPageDataInfo(Mediator::selectedRadioButton);
										
					RfhSelectionObserver selectionobserver(this);
					selectionobserver.reSortTheListForUniqueAttr();
					shouldRefreshFlag  = kTrue;
					IsDeleteFlagSelected = kTrue;
					refresh.doRefresh(Mediator::selectedRadioButton);
					
					frameRecordList[index].isProcessed = kTrue;
					
					SDKListBoxHelper listHelper(this, kRfhPluginID);
					IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
					listHelper.CheckUncheckRow(listBoxControlView,index,frameRecordList[index].isSelected,2);					
					
					currentSelectedFrameIndex = index;
				}				
			}
			bool16 found = kFalse;
			int32 tempIndex = currentSelectedFrameIndex;
			while(found == kFalse)
			{
				if(frameRecordList.size() > 0){
					if(tempIndex < (frameRecordList.size()-1)){
						tempIndex++;
					}
					else{
						tempIndex = 0;
					}
					if(frameRecordList[tempIndex].isSelected == kFalse && frameRecordList[tempIndex].isProcessed == kFalse )
					{
						currentSelectedFrameIndex = tempIndex;
						found = kTrue;
					}
					if(tempIndex == currentSelectedFrameIndex)
					{
						break;
					}
				}
				else
					break;
			}
			if(found == kTrue)
			{
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
				IDataBase* database= ::GetDataBase(document);
				InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
				if(!iSelectionManager)
				{
					//CA("NULL");
				}
		
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) 
				{	//CA("!layoutSelectionSuite");
					return;
				}
				layoutSelectionSuite->DeselectAllPageItems();
				
				UIDList selectUIDList(database,frameRecordList[currentSelectedFrameIndex].frameUID);
				layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
				

				SDKListBoxHelper listHelper(this, kRfhPluginID);
				IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
				listHelper.CheckUncheckRow(listBoxControlView,currentSelectedFrameIndex,!frameRecordList[currentSelectedFrameIndex].isSelected,0);
				frameRecordList[currentSelectedFrameIndex].isSelected = kTrue;

				InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
				if(listCntl == nil) 
					break;
				listCntl->Select(currentSelectedFrameIndex);
			}
			else{
				if(currentDocumentIndex+1  < selectedDocIndexList.size()){
					goForNextDocument();
				}
				else{
					if(refreshModeSelected != 3){
						//CA("let's check");
						//processUpdateNew();
						IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
						if(firstGroupPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
							break;
						}
						
						firstGroupPanelControlData->HideView();
						interactiveListNilGroupPanelControlData->ShowView();
						
						IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
						if(TextWidget4)
						{
							//CA("TextWidget4 == nil");
							InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
							if(!textcontrol)
							{
								//CA("textcontrol == nil");
								return;
							}
							if(refreshModeSelected == 1){
								PMString text("All deleted items have been processed in this document. Do you  want to proceed with item updates?");
								textcontrol->SetString(text);
							}
							else{
								PMString text("All updated items have been processed in this document. Do you  want to proceed with new items?");
								textcontrol->SetString(text);
							}
			
							IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
							if(OkButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
								break;
							}
							OkButtonControlData->HideView();
							
							IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
							if(yesButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
								break;
							}
							yesButtonControlData->ShowView();					
							
							IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
							if(noButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
								break;
							}
							noButtonControlData->ShowView();

							IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
							if(document){
								AcquireWaitCursor awc;
								awc.Animate();

								SDKLayoutHelper sdklhelp;
								int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
								sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
								sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
							}
						}
					}
					else{
						IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
						if(firstGroupPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
							break;
						}
						
						firstGroupPanelControlData->HideView();
						interactiveListNilGroupPanelControlData->ShowView();
						
						IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
						if(TextWidget4)
						{
							//CA("TextWidget4 == nil");
							InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
							if(!textcontrol)
							{
								//CA("textcontrol == nil");
								return;
							}
							
							PMString text("Interactive refresh is now complete. Your catalog pages are updated. Do you want to close document?");
							textcontrol->SetString(text);						
							
			
							IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
							if(OkButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
								break;
							}
							OkButtonControlData->HideView();
							
							IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
							if(yesButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
								break;
							}
							yesButtonControlData->ShowView();					
							
							IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
							if(noButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
								break;
							}
							noButtonControlData->ShowView();
						}
					}
				}
			}
		}
		else if((theSelectedWidget == kInteractiveMessageNoButtonWidgetID || theSelectedWidget == kInteractiveMessageOKButtonWidgetID || theSelectedWidget == kRfhRefreshModeDlgCancelButtonWidgetID) && theChange==kTrueStateMessage){
			//CA("No/OK/Cancel");
			if(theSelectedWidget == kInteractiveMessageNoButtonWidgetID && refreshModeSelected == 3){
				IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
				if(firstGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
					break;
				}
				
				firstGroupPanelControlData->ShowView();
				interactiveListNilGroupPanelControlData->HideView();

				//CA("Thank you for using Interactive Refresh.");
				//ptrIAppFramework->GetSectionData_clearSectionDataCache();
				//ptrIAppFramework->multipleSectionDataClearInstance();

				break;
			}
			else if(theSelectedWidget != kRfhRefreshModeDlgCancelButtonWidgetID){
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
				if(document){
					
					SDKLayoutHelper sdklhelp;
					int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
					sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
					sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);

					//CA("theSelectedWidget != kRfhRefreshModeDlgCancelButtonWidgetID");
					//ptrIAppFramework->GetSectionData_clearSectionDataCache();
					//ptrIAppFramework->multipleSectionDataClearInstance();

				}
				if(refreshModeSelected == 3){
					if(sectionIDInDoc.size() > 0 && uniqueItemOnDocList.size() > 0){
						closeContentSprayer(sectionIDInDoc[0],uniqueItemOnDocList[0].langaugeID);
					//	CA("Thank you for using Interactive Refresh.");

						//ptrIAppFramework->GetSectionData_clearSectionDataCache();
						//ptrIAppFramework->multipleSectionDataClearInstance();
					}
				}
				
			}
			//CA("ptrIAppFramework->multipleSectionDataClearInstance.....2222222");	
			//ptrIAppFramework->multipleSectionDataClearInstance(); //*******

			ptrIAppFramework->LogInfo("CloseDialog 6");
			CDialogObserver::CloseDialog();
		}
		else if(((theSelectedWidget == kProceedToUpdateYesButtonWidgetID)  || (theSelectedWidget == kOKButtonWidgetID && interactiveListNilGroupPanelControlData->IsVisible())) && (theChange==kTrueStateMessage))
		{
			//CA("theSelectedWidget == kProceedToUpdateYesButtonWidgetID");
			if(refreshModeSelected == 1){
				IControlView * interactiveoptionControlData = panelControlData->FindWidget(kInteractiveoptionGroupPanelWidgetID);
				if(interactiveoptionControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveoptionControlData == nil");
					break;
				}
				interactiveoptionControlData->ShowView();
				
				IControlView* RfhRefreshFullTblRadioBtnView = panelControlData->FindWidget(kRefreshFullTblRadioButtonWidgetID);
				if(!RfhRefreshFullTblRadioBtnView)
				{		
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshFullTblRadioBtnView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData>RefreshFullTblRadioTriState(RfhRefreshFullTblRadioBtnView,UseDefaultIID());
				if(RefreshFullTblRadioTriState == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshFullTblRadioTriState == nil");
					return;
				}
				RefreshFullTblRadioTriState->Select();

				interactiveListNilGroupPanelControlData->HideView();
			}
			else if(refreshModeSelected == 2){
				//CA("going for new");
				refreshModeSelected = 3;
				CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
				processUpdateNew();
			}
			else if(refreshModeSelected == 3){
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
				if(document){
					SDKLayoutHelper sdklhelp;
					int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
					sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
					sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
				}	
				if(sectionIDInDoc.size() > 0 && uniqueItemOnDocList.size() > 0){
					ptrIAppFramework->LogInfo("CloseDialog 7");
					CDialogObserver::CloseDialog();
					closeContentSprayer(sectionIDInDoc[0],uniqueItemOnDocList[0].langaugeID);
					CA("Thank you for using Interactive Refresh.");	

					//ptrIAppFramework->GetSectionData_clearSectionDataCache();
					//ptrIAppFramework->multipleSectionDataClearInstance();
				}
			}

		}
		else if((theSelectedWidget == kInteractiveOptionOKButtonWidgetID || theSelectedWidget == kInteractiveOptionCancelButtonWidgetID)&& theChange==kTrueStateMessage){
			
			if(theSelectedWidget == kInteractiveOptionOKButtonWidgetID){
				IControlView* RfhRefreshTableByCellRadioBtnView = panelControlData->FindWidget(kRefreshByCellRadioButtonWidgetID);
				if(!RfhRefreshTableByCellRadioBtnView)
				{		
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData>RefreshTableByCellRadioTriState(RfhRefreshTableByCellRadioBtnView,UseDefaultIID());
				if(RefreshTableByCellRadioTriState == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByCellRadioTriState == nil");
					return;
				}
				refreshTableByAttribute = RefreshTableByCellRadioTriState->IsSelected();
			}
			else{
				IControlView* RfhRefreshFullTblRadioBtnView = panelControlData->FindWidget(kRefreshFullTblRadioButtonWidgetID);
				if(!RfhRefreshFullTblRadioBtnView)
				{		
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshFullTblRadioBtnView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData>RefreshFullTblRadioTriState(RfhRefreshFullTblRadioBtnView,UseDefaultIID());
				if(RefreshFullTblRadioTriState == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshFullTblRadioTriState == nil");
					return;
				}
				RefreshFullTblRadioTriState->Select();
				refreshTableByAttribute = kFalse;
			}
			IControlView * interactiveoptionControlData = panelControlData->FindWidget(kInteractiveoptionGroupPanelWidgetID);
			if(interactiveoptionControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveoptionControlData == nil");
				break;
			}
			interactiveoptionControlData->HideView();

			refreshModeSelected = 2;			
			CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
			processUpdateNew();
		
		}
		else if(theSelectedWidget == kRfhProcessOptionsDropDownWidgetID && protocol == IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("theSelectedWidget==kRfhProcessOptionsDropDownWidgetID");
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
			if(!document)
				return;
			IDataBase* database= ::GetDataBase(document);
			IControlView* iControlView = panelControlData->FindWidget(kRfhProcessOptionsDropDownWidgetID);
			if (iControlView == nil)
			{
				break;
			}
			
			InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
			if (iDropDownListController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::iDropDownListController == nil");																	
				break;
			}

			int32 selectedRowIndex=0; 
			selectedRowIndex = iDropDownListController->GetSelected(); 
			
			refreshModeSelected = selectedRowIndex;
			
			SDKListBoxHelper listBox(this, kRfhPluginID);
			switch(selectedRowIndex)
			{
				case 1:	
				{					
					//CA("case Delete");
					PMString temp("Currenty Running : Delete");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}

					fillFrameRecordVector(refreshModeSelected);
					fillRefreshModeListBox(panelControlData);
					
					break;
				}
				case 2:
				{
					//CA("case Update");
					PMString temp("Currenty Running : Update");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}

					fillFrameRecordVector(refreshModeSelected);
					fillRefreshModeListBox(panelControlData);
					
					break;
				}
				case 3:
				{
					//CA("case New");
					PMString temp("Currenty Running : New");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}

					fillFrameRecordVector(refreshModeSelected);
					fillRefreshModeListBox(panelControlData);
					getNewItemIDs();
					if(newItemIDList.size() > 0 && newItemIDList.at(0).size() > 0)
						openContentSprayer();
					break;
				}				
			}
			if(frameRecordList.size() > 0){
				InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
				if(!iSelectionManager)
				{
					//CA("NULL");
				}
		
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) 
				{	//CA("!layoutSelectionSuite");
					return;
				}
				layoutSelectionSuite->DeselectAllPageItems();
				
				UIDList selectUIDList(database,frameRecordList[0].frameUID);
				layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
				currentSelectedFrameIndex = 0;

				SDKListBoxHelper listHelper(this, kRfhPluginID);
				IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
				listHelper.CheckUncheckRow(listBoxControlView,0,!frameRecordList[0].isSelected,0);
				frameRecordList[0].isSelected = kTrue;

				InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
				if(listCntl == nil) 
					break;
				listCntl->Select(0);
			}
		}
		else if(theSelectedWidget == kRfhAnalyzeButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kRfhAnalyzeButtonWidgetID && theChange == kTrueStateMessage");
			if(refreshModeSelected == 3)
				return;

			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(!document)
				return;
			IDataBase* database= ::GetDataBase(document);

			for(int32 index = 0 ; index < frameRecordList.size() ; index++){
				if(frameRecordList[index].isProcessed == kFalse){	
					//CA("frameRecordList[index].isProcessed == kFalse");
					
					if(frameRecordList[index].sectionID != CurrentSelectedDocumentSectionID)
					{
						//ptrIAppFramework->switchSectionData(frameRecordList[index].sectionID);
						CurrentSelectedDocumentSectionID = frameRecordList[index].sectionID;
					}

					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
						return;
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	
						//CA("!layoutSelectionSuite");
						return;
					}
					layoutSelectionSuite->DeselectAllPageItems();
					
					UIDList selectUIDList(database,frameRecordList[index].frameUID);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
					if(refreshModeSelected == 2){
						IControlView* RfhRefreshTableByCellRadioBtnView = panelControlData->FindWidget(kRefreshByCellRadioButtonWidgetID);
						if(!RfhRefreshTableByCellRadioBtnView)
						{		
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::Update::RfhRefreshTableByCellRadioBtnView == nil");
							return;
						}
						InterfacePtr<ITriStateControlData>RefreshTableByCellRadioTriState(RfhRefreshTableByCellRadioBtnView,UseDefaultIID());
						if(RefreshTableByCellRadioTriState == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::CMMDialogObserver::Update::RefreshTableByCellRadioTriState == nil");
							return;
						}
						refreshTableByAttribute = RefreshTableByCellRadioTriState->IsSelected();						
					}
					else
						refreshTableByAttribute = kFalse;

					Refresh refresh;
					Mediator::selectedRadioButton = 3;
					
					refresh.GetPageDataInfo(Mediator::selectedRadioButton);
										
					RfhSelectionObserver selectionobserver(this);
					selectionobserver.reSortTheListForUniqueAttr();
					
					IsDeleteFlagSelected = kTrue;
					refresh.doRefresh(Mediator::selectedRadioButton);
					
					frameRecordList[index].isProcessed = kTrue;
					frameRecordList[index].isSelected = kTrue;
					SDKListBoxHelper listHelper(this, kRfhPluginID);
					IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
					listHelper.CheckUncheckRow(listBoxControlView,index,frameRecordList[index].isSelected,2);

					currentSelectedFrameIndex = index;
				}
			}
			if(currentDocumentIndex+1  < selectedDocIndexList.size()){
				goForNextDocument();
			}
			else{
				//if(refreshModeSelected != 3){
					//CA("let's check");
					//processUpdateNew();
					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					
					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						if(refreshModeSelected == 1){
							PMString text("All deleted items have been processed in this document. Do you  want to proceed with item updates?");
							textcontrol->SetString(text);
						}
						else{
							PMString text("All updated items have been processed in this document. Do you  want to proceed with new items?");
							textcontrol->SetString(text);
						}
		
						IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
						if(OkButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
							break;
						}
						OkButtonControlData->HideView();
						
						IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
						if(yesButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
							break;
						}
						yesButtonControlData->ShowView();					
						
						IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
						if(noButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
							break;
						}
						noButtonControlData->ShowView();

						IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
						if(document){
							AcquireWaitCursor awc;
							awc.Animate();

							SDKLayoutHelper sdklhelp;
							int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
							sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
							sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
						}
					}
				//}
			}
		}
		else{
			//CA("11");
			CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
		}
/////////////////////////////////////	END interactive refreh
	} while (false);

}


K2Vector<PMString> RfhDlgDialogObserver::GetBookContentNames(IBookContentMgr* bookContentMgr)
{

	bookContentNames.clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content  Depricated cs3
			//InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			//if (bookLink == nil) 
			//{
			//	ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
			//	break; // out of for loop
			//}

			//// get the book name and add it to the list
			//PMString* baseName = bookLink->GetBaseName();  //Cs3
			//ASSERT(baseName && baseName->empty() == kFalse);

			//*****Added for Cs4
			InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
			if (bookContent == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IBookContent for book #%d is missing", i));
				//CA("bookContent == nil");
				break; // out of for loop
			}
			PMString baseName = bookContent->GetShortName();
			//***********
			bookContentNames.push_back(baseName);


			//Get Full Path
			//PMString* pathName = bookLink->GetFullName();
			//bookContentPaths.push_back(*pathName);
			//CA("pathName = " + *pathName);
		}

	} while (false);
	return bookContentNames;
}

void RfhDlgDialogObserver::fillFrameRecordVector(int32 refreshMode) // 1 = delete , 2 = Update ,  3 = New
{
	//CA("RfhDlgDialogObserver::fillFrameRecordVector");
	
	do{
		frameRecordList.clear();
		uniqueItemOnDocList.clear();
		sectionIDInDoc.clear();

		AcquireWaitCursor awc;
		awc.Animate();
		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return ;
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

		InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::!itagReader");
			return;
		}
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::fntDoc==nil");	
			return;
		}
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::layout == nil");		
			return;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::database==nil");			
			return;
		}
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::iSpreadList==nil");				
			return;
		}
		int32 docIndexInBook = selectedDocIndexList[currentDocumentIndex];
		int32 pageIndexInDoc = 0;
		if(docIndexInBook != 0){
			for(int32 x = 0 ; x < pagesPerDocumetList.size(); x++){
				if(docIndexInBook > x)
					pageIndexInDoc = pageIndexInDoc +  pagesPerDocumetList[x];				
			}
		}

		int32 pageNo = 0;
		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::!spread");						
				return;
			}
			int numPages=spread->GetNumPages();
			Refresh refreshObj;
			for(int i=0 ; i< numPages ; i++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(i, &tempList, kFalse);
				refreshObj.selectUIDList = tempList;
				
				for(int p = 0; p < tempList.size() ; p++)
				{	
					InterfacePtr<IHierarchy> iHier(tempList.GetRef(p), UseDefaultIID());
					if(!iHier)
						continue;

					UID kidUID;
					int32 numKids=iHier->GetChildCount();
					bool16 isGroupFrame = kFalse ;
					isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(tempList.GetRef(p));

					for(int j=0;j<numKids;j++)
					{
						kidUID=iHier->GetChildUID(j);
						UIDRef boxRef(tempList.GetDataBase(), kidUID);
						IIDXMLElement* ptr;
						TagList tList,tList_checkForHybrid;
						
						if(isGroupFrame == kTrue) 
						{
							tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
							if(tList_checkForHybrid.size() == 0)
							{
								//CA("tList_checkForHybrid.size() == 0");
								continue;
							}
							if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
								tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
							else
								tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
						}
						else 
						{
							tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(p), &ptr);
						
							if(tList_checkForHybrid.size() == 0)
							{
								continue;
							}
							if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
								tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(tempList.GetRef(p), &ptr);
							else
								tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(p), &ptr);
						}
						
						if(!refreshObj.doesExist(tList))//if(!doesExist(ptr))
						{
							tempList.Append(kidUID);
						}
						
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
						for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
						{
							tList_checkForHybrid[tagIndex].tagPtr->Release();
						}						
					}
				}
				pageNo++;
				
				if(itemGroupDescriptionElementID == -1){
					itemGroupDescriptionElementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
				}
				
				if(refreshMode == 1){  /// for delete
					//CA("for delete");
					for(int32 UIDIndex = tempList.size()-1; UIDIndex >= 0 ; UIDIndex--)
					{
						//CA("For loop");
						FrameRecord freamRecordObj;
						IIDXMLElement* ptr;
						TagList tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex), &ptr);
						if(tList.size() <= 0)
							continue;
						if(tList[0].sectionID == -1 && tList[0].parentId == -1)
							continue;

						if(tList[0].sectionID != CurrentSelectedDocumentSectionID)
						{
							//ptrIAppFramework->switchSectionData(tList[0].sectionID);
							CurrentSelectedDocumentSectionID = tList[0].sectionID;
						}
						if(itemDescriptionAttributeID == -1){
							//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
							//itemDescriptionAttributeID = attributeMode.getAttributeId();
							itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
						}
						bool16 ISProdDeleted = kFalse;
						if(tList[0].imgFlag == 1 && tList[0].isAutoResize > 1){
							//CA("tList[0].imgFlag == 1 && tList[0].isAutoResize > 1");
							ISProdDeleted = kFalse; //ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].isAutoResize);
							if(ISProdDeleted == kFalse){
								VectorScreenTableInfoPtr tableInfo = NULL;
								tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tList[0].sectionID,tList[0].isAutoResize, tList[0].languageID,  kTrue);
								if(!tableInfo)
								{
									//CA("tableinfo is NULL");
									continue;
								}
								
								if(tableInfo->size()==0)
								{ 
									//CA("tableinfo size==0");
									delete tableInfo;
									continue;
								}
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
								vector<double>FinalItemIDS;
								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									//CA("iterate through vector tableInfo ");
									vector<double>vec_items;		
									oTableValue = *it;
									vec_items = oTableValue.getItemIds();								
									for(int32 indx1 = 0 ; indx1 < vec_items.size() ; indx1++){
										FinalItemIDS.push_back(vec_items[indx1]);
									}
								}
								bool16 itemFound = kFalse;
								for(int32 indx1 = 0 ; indx1 < FinalItemIDS.size() ; indx1++){
									if(tList[0].parentId == FinalItemIDS[indx1]){
										itemFound = kTrue;
										break;
									}
								}
								if(itemFound == kFalse)
									ISProdDeleted = kTrue;
								if(tableInfo)
									delete tableInfo;
							}							
						}
						else{
							//CA("tList[0].imgFlag != 1 && tList[0].isAutoResize <= 1");
							if(tList[0].whichTab == 5)								
								continue;
							ISProdDeleted = kFalse; // ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
						}

						if(ISProdDeleted == kTrue){
							//CA("ISProdDeleted == kTrue");
							if(tList[0].imgFlag == 1){
								//CA("ISProdDeleted == kTrue && tList[0].imgFlag == 1");
								freamRecordObj.frameType = 2;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
								if(tList[0].whichTab == 3){
									/*PMString imageLink = tList[0].tagPtr->GetAttributeValue(WideString("href"));
									int32 indexOfLastSlash = imageLink.LastIndexOfCharacter('/');
									
									if(indexOfLastSlash < 0)
										continue;
									PMString *imageName = imageLink.Substring(indexOfLastSlash+1);
									freamRecordObj.Data = *imageName;*/
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID,  kTrue);
									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
								}
								else if(tList[0].whichTab == 4){							
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID , tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();

									freamRecordObj.parentID = itemModelObj.getItemNo();
									if(itemDescriptionAttributeID == -1){
										//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
										//freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
																	
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
																		//freamRecordObj.Data =*imageName;
								}	
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);

								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}
							else if(tList[0].isTablePresent == kTrue){
								if(tList[0].tableType != 3){
									// Apsiva9
									tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(tempList.GetRef(UIDIndex), &ptr);

									freamRecordObj.frameType = 1;
									freamRecordObj.frameUID = tempList[UIDIndex];
									freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
									if(tList[0].whichTab == 3){
										freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);						
										//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
										freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
									else if(tList[0].whichTab == 4){
										//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
										CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID , tList[0].parentId, 0, tList[0].languageID);
										CItemModel itemModelObj =  pbObjValue.getItemModel();

										freamRecordObj.parentID = itemModelObj.getItemNo();
										if(itemDescriptionAttributeID == -1){
											//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
											//freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
											itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
										}
										
										{
											freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
										}
									}
									if(!iConverter)
									{
										freamRecordObj.Data=freamRecordObj.Data;					
									}
									else
										freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);
									
									
									freamRecordObj.sectionID = tList[0].sectionID;
									freamRecordObj.langId = tList[0].languageID;
									frameRecordList.push_back(freamRecordObj);
								}
								else{
									tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex), &ptr);
									//CA("Need to write for Table");							
								}
							}
							else{
								freamRecordObj.frameType = 0;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;

								if(tList[0].whichTab == 3){
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);						
								}
								else{
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID , tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();

									freamRecordObj.parentID = itemModelObj.getItemNo();
								}						
								if(tList[0].whichTab == 3){
									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
								}
								else if(tList[0].whichTab == 4){
									if(itemDescriptionAttributeID == -1){
									//	//CA("itemDescriptionAttributeID == -1");
									//	CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
									//	freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
									
									{
										//CA("itemDescriptionAttributeID != -1");
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
//CA("freamRecordObj.Data = "+freamRecordObj.Data);
								}
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);

								
								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}
						}
						else if(tList[0].imgFlag == 1){
							//CA("ISProdDeleted == kFalse && tList[0].imgFlag == 1");
							InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
							if(!iSelectionManager)
							{
								//CA("NULL");
								return;
							}
					
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) 
							{	 
								//CA("!layoutSelectionSuite");
								return;
							}
							layoutSelectionSuite->DeselectAllPageItems();
							
							UIDList selectUIDList(database,tempList[UIDIndex]);
							layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);

							refreshTableByAttribute = kTrue;
							Refresh refresh;
							Mediator::selectedRadioButton = 3;
							
							refresh.GetPageDataInfo(Mediator::selectedRadioButton);
												
							RfhSelectionObserver selectionobserver(this);
							selectionobserver.reSortTheListForUniqueAttr();
							
							
							shouldRefreshFlag = kFalse;
							shouldPushInFreameVect = kFalse;
							IsDeleteFlagSelected = kFalse;
							refresh.doRefresh(Mediator::selectedRadioButton);
							if(shouldPushInFreameVect == kTrue){
								//CA("shouldPushInFreameVect == kTrue");
								freamRecordObj.frameType = 2;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
								if(tList[0].whichTab == 3){
									/*PMString imageLink = tList[0].tagPtr->GetAttributeValue(WideString("href"));
									int32 indexOfLastSlash = imageLink.LastIndexOfCharacter('/');
									
									if(indexOfLastSlash < 0)
										continue;
									PMString *imageName = imageLink.Substring(indexOfLastSlash+1);
									freamRecordObj.Data = *imageName;*/
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);
									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
								}
								else if(tList[0].whichTab == 4){							
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID , tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();

									freamRecordObj.parentID = itemModelObj.getItemNo();
									if(itemDescriptionAttributeID == -1)
									{
										//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
										//freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID ,kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
									
									{
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
								}
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);
								
								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}
							shouldPushInFreameVect = kFalse;
						}

						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
					}
					//CA("Delete complated");
				}
				else if(refreshMode == 2)
				{ 
					// for update
					//CA("For Update");
					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if(!document)
						return;
					IDataBase* database= ::GetDataBase(document);

					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == nil)
					{
						//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
						return;
					}
								
					Refresh refreshObj;
					for(int32 UIDIndex = tempList.size()-1; UIDIndex >= 0 ; UIDIndex--){	
						//CA("Frame loop");
						InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection());
						if(!iSelectionManager)
						{
							//CA("NULL");
							return;
						}
				
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
						if (!layoutSelectionSuite) 
						{	
							//CA("!layoutSelectionSuite");16146
							return;
						}
						layoutSelectionSuite->DeselectAllPageItems();
						
						UIDList selectUIDList(database,tempList[UIDIndex]);
						layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
						
						IIDXMLElement* ptr;
						TagList tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex), &ptr);
						if(tList.size() <= 0)
							continue;
						if(tList[0].sectionID == -1 && tList[0].parentId == -1)
							continue;

						if(tList[0].sectionID != CurrentSelectedDocumentSectionID)
						{
							//ptrIAppFramework->switchSectionData(tList[0].sectionID);
							CurrentSelectedDocumentSectionID = tList[0].sectionID;
						}

						refreshTableByAttribute = kTrue;
						Refresh refresh;
						Mediator::selectedRadioButton = 3;
						
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
											
						RfhSelectionObserver selectionobserver(this);
						selectionobserver.reSortTheListForUniqueAttr();
						
						
						InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
						if(!panelControlData) {
							//CA("panelControlData == nil");
							break;
						}
						IControlView* RfhRefreshTableByCellRadioBtnView = panelControlData->FindWidget(kRefreshByCellRadioButtonWidgetID);
						if(!RfhRefreshTableByCellRadioBtnView)
						{		
							ptrIAppFramework->LogDebug("AP7_RefreshContent::fhDlgDialogObserver::fillFrameRecordVector::RfhRefreshTableByCellRadioBtnView == nil");
							return;
						}
						InterfacePtr<ITriStateControlData>RefreshTableByCellRadioTriState(RfhRefreshTableByCellRadioBtnView,UseDefaultIID());
						if(RefreshTableByCellRadioTriState == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::fhDlgDialogObserver::fillFrameRecordVector::RefreshTableByCellRadioTriState == nil");
							return;
						}
						bool16 refreshbyCellRadio = RefreshTableByCellRadioTriState->IsSelected();


						shouldRefreshFlag = kFalse;
						shouldPushInFreameVect = kFalse;
						IsDeleteFlagSelected = kFalse;
						if(tList[0].rowno == -904 && refreshbyCellRadio == kFalse){
							//CA("tList[0].rowno == -904 && refreshbyCellRadio == kFalse");
							shouldPushInFreameVect = NeedToRefreshMMYCustomTable(tempList.GetRef(UIDIndex),tList[0]);							
						}
						////////else if(( ((tList[0].whichTab == 3 ) && tList[0].imgFlag ==0) && (!tList[0].isProcessed) ) && ((!tList[0].isTablePresent)  ) ) //*** For Item Group
						////////{													
						////////	shouldPushInFreameVect = kTrue;							
						////////}
						else if(tList[0].rowno != -904){
							//CA("tList[0].rowno != -904");
							refreshTableByAttribute = RefreshTableByCellRadioTriState->IsSelected();

							IIDXMLElement* ptr;
							TagList tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex), &ptr);
							if(tList.size() <= 0)
								continue;

							if(tList[0].sectionID == -1 && tList[0].parentId == -1)
								continue;
							
							int32 custTableTagIndex = 0;
							bool16 isCustomTablePresent = kFalse;
							if(tList[0].typeId != -5 || tList[0].elementId != -101){
								//CA("tList[0].typeId != -5 || tList[0].elementId != -101");
								for(int32 tagIndex = 1 ; tagIndex < tList.size() ; tagIndex++)
								{
									if(tList[tagIndex].typeId == -5 || tList[tagIndex].elementId == -101){
										isCustomTablePresent = kTrue;
										custTableTagIndex = tagIndex;
										break;
									}
								}
							}

							if((tList[custTableTagIndex].isTablePresent || tList[custTableTagIndex].typeId == -5 || tList[custTableTagIndex].elementId == -101 ) && refreshTableByAttribute == kFalse)
							{
								//CA("1");
								bool16 insertFrameRecord = kFalse;
								IIDXMLElement* ptr1;

								TagList tList1 = itagReader->getTagsFromBox_ForRefresh_ByAttribute(tempList.GetRef(UIDIndex), &ptr1);
								if(tList1.size() == 0)
								{
									//CA("tList1.size() == 0");
									continue;
								}
								
								int32 colCount = -1;
								if(tList[custTableTagIndex].isTablePresent){
									UIDRef TableUIDRef;
									TableUtility tUtility;
									tUtility.isTablePresent(tempList.GetRef(UIDIndex), TableUIDRef);
									InterfacePtr<ITableModel> tableModel(TableUIDRef, UseDefaultIID());
									if (tableModel == NULL)
									{
										//CA("Err: invalid interface pointer ITableFrame");
										break;
									}
									ColRange colRange = tableModel->GetTotalCols();
									colCount = colRange.count;
								}
													
								vector<double> itemIDList,tableIDList,UniqueAttributeID;
								for(int32 tagIndex = 0 ; tagIndex < tList1.size(); tagIndex++)
								{
									if(tList1[tagIndex].childTag == 1 && tList1[tagIndex].header != 1)
									{
										bool16 found = kFalse;
										bool16 tableIDFound = kFalse;
										for(int32 idindex = 0 ; idindex < itemIDList.size() ; idindex++)
										{
											if(itemIDList[idindex] == tList1[tagIndex].childId){
												found = kTrue;
												break;
											}
										}
										if(found == kFalse)
											itemIDList.push_back(tList1[tagIndex].childId);

										for(int32 idindex = 0 ; idindex < tableIDList.size() ; idindex++)
										{
											if(tableIDList[idindex] == tList1[tagIndex].tableId){
												found = kTrue;
												break;
											}
										}
										if(found == kFalse && tList1[tagIndex].tableId > 0){
											tableIDList.push_back(tList1[tagIndex].tableId);
										}
									}
									if(tList[custTableTagIndex].isTablePresent == kFalse && tList1[tagIndex].header != 1){
										if(UniqueAttributeID.size() == 0){
											UniqueAttributeID.push_back(tList1[tagIndex].elementId);
										}
										else{
											bool16 foundID = kFalse;
											for(int32 idindex = 0 ; idindex < UniqueAttributeID.size() ; idindex++)
											{
												if(UniqueAttributeID[idindex] == tList1[tagIndex].elementId){
													foundID = kTrue;
													break;
												}
											}
											if(foundID == kFalse){
												UniqueAttributeID.push_back(tList1[tagIndex].elementId);
											}
										}									
									}
								}
								if(tList[custTableTagIndex].isTablePresent == kFalse)
								{
									colCount = static_cast<int32>(UniqueAttributeID.size());									
								}
								if(tList[custTableTagIndex].tableType == 1 || tList[custTableTagIndex].tableType ==2){
									//ptrIAppFramework->clearAllStaticObjects();
									vector<double> vec_items;
									int32 noCols = 0;
									if(tList[custTableTagIndex].whichTab == 4 ){								
										VectorScreenTableInfoPtr ItemtableInfo =
											ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(tList[custTableTagIndex].parentId, tList[custTableTagIndex].sectionID , tList[custTableTagIndex].languageID );

										if(!ItemtableInfo)
										{
											//CA("tableinfo is NULL");
											continue;
										}
										
										if(ItemtableInfo->size()==0)
										{ 
											//CA("tableinfo size==0");
											delete ItemtableInfo;
											continue;
										}
										CItemTableValue oTableValue;
										VectorScreenTableInfoValue::iterator it;
										
										if(tList[custTableTagIndex].tableId > 0){
											//CA("tList[0].tableId > 0");
											for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
											{
												//CA("iterate through vector tableInfo ");
												//for tabelInfo start				
												oTableValue = *it;
												if(oTableValue.getTableID() == tList[custTableTagIndex].tableId){
													//CA("Table Found");
													vec_items = oTableValue.getItemIds();
													noCols = static_cast<int32>(oTableValue.getTableHeader().size());
													break;
												}
											}
										}
										else{
											vector<double> uniqueItemIDs,vec_items1;
											for(it = ItemtableInfo->begin(); it!=ItemtableInfo->end(); it++)
											{
												oTableValue = *it;
												if(tableIDList.size() > 0){
													for(int32 tableIDIndex = 0 ; tableIDIndex < tableIDList.size() ; tableIDIndex++){
														if(oTableValue.getTableID() != tableIDList[tableIDIndex])
															continue;
													}
												}
												else if(tList[custTableTagIndex].field1 > 0){
													if(oTableValue.getTableTypeID() != tList[custTableTagIndex].field1)
														continue;
												}
												vec_items1 = oTableValue.getItemIds();
												if(uniqueItemIDs.size() == 0){
													uniqueItemIDs = vec_items1;
												}
												else{
													for(int32 i = 0 ; i < vec_items1.size() ; i++){
														bool16 found = kFalse;
														for(int32 j = 0 ; j < uniqueItemIDs.size() ; j++){
															if(vec_items1[i] == uniqueItemIDs[j]){
																found = kTrue;
																break;
															}
														}
														if(found == kFalse){
															uniqueItemIDs.push_back(vec_items1[i]);
														}
													}
												}
											}
											vec_items = uniqueItemIDs;
										}
										if(ItemtableInfo)
											delete ItemtableInfo;
									}
									else if(tList[custTableTagIndex].whichTab == 3)
									{
										//CA("tList[custTableTagIndex].whichTab == 3");
										
										VectorScreenTableInfoPtr tableInfo = NULL;
										tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tList[custTableTagIndex].sectionID, tList[custTableTagIndex].parentId, tList[custTableTagIndex].languageID, kTrue);
										if(!tableInfo)
										{
											//CA("tableinfo is NULL");
											continue;
										}
										
										if(tableInfo->size()==0)
										{ 
											//CA("tableinfo size==0");
											delete tableInfo;
											continue;
										}
										CItemTableValue oTableValue;
										VectorScreenTableInfoValue::iterator it;
										if(tList[custTableTagIndex].tableId > 0){
											//CA("tList[0].tableId > 0");
											for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
											{
												//CA("iterate through vector tableInfo ");
												//for tabelInfo start				
												oTableValue = *it;
												if(oTableValue.getTableID() == tList[custTableTagIndex].tableId){
													//tableFound = kTrue;
													vec_items = oTableValue.getItemIds();
													noCols = static_cast<int32>(oTableValue.getTableHeader().size());
												}
											}
										}		
										else
										{
											//CA("tList[0].tableId <= 0");
											vector<double> uniqueItemIDs,vec_items1;
											
											for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
											{
												oTableValue = *it;
												if(tableIDList.size() > 0){
													for(int32 tableIDIndex = 0 ; tableIDIndex < tableIDList.size() ; tableIDIndex++){
														if(oTableValue.getTableID() != tableIDList[tableIDIndex])
															continue;
													}
												}
												else if(tList[custTableTagIndex].field1 > 0){
													if(oTableValue.getTableTypeID() != tList[custTableTagIndex].field1)
														continue;
												}
												vec_items1 = oTableValue.getItemIds();
												
												if(uniqueItemIDs.size() == 0){
													uniqueItemIDs = vec_items1;
												}
												else{
													for(int32 i = 0 ; i < vec_items1.size() ; i++){
														bool16 found = kFalse;
														for(int32 j = 0 ; j < uniqueItemIDs.size() ; j++){
															if(vec_items1[i] == uniqueItemIDs[j]){
																found = kTrue;
																break;
															}
														}
														if(found == kFalse){
															uniqueItemIDs.push_back(vec_items1[i]);
														}
													}
												}
											}
											vec_items = uniqueItemIDs;

										}
										if(tableInfo)
											delete tableInfo;
										
									}
									if(vec_items.size() > 0){
										bool16 itemFound = kFalse;
										if(noCols != colCount && tList[custTableTagIndex].tableType == 1){
											shouldPushInFreameVect = kTrue;
										}
										else if(itemIDList.size() == vec_items.size()){
											//CA("else if(itemIDList.size() == vec_items.size())");
											int32 foundCount = 0;
											for(int32 i = 0 ; i < itemIDList.size() ; i++ )
											{
												bool16 itemFound = kFalse;
												for(int32 j = 0 ; j < vec_items.size() ; j++){
													if(itemIDList[i] == vec_items[j]){
														//CA("foundCount");
														itemFound = kTrue;
														foundCount++;
														break;
													}
												}
											}
											if(foundCount == static_cast<int32>(itemIDList.size())){									
												for(int32 index = 0 ; index < itemIDList.size() ; index++){
													if(itemIDList[index] != vec_items[index]){
														//CA("itemIDList[index] != vec_items[index]");
														shouldPushInFreameVect = kTrue;
														break;
													}
												}
											}
										}
										else if(itemIDList.size() > vec_items.size()){
											shouldPushInFreameVect = kTrue;
										}
										else if(itemIDList.size() < vec_items.size()){
											shouldPushInFreameVect = kTrue;
										}
									}
									else{
										if(itemIDList.size() > 0){
											//CA("itemIDList.size() > 0");
											shouldPushInFreameVect = kTrue;		
										}
									}									
								}							
							}
							
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
						}

						if((shouldPushInFreameVect == kFalse && tList[0].rowno != -904) || (tList[0].rowno == -904 && refreshbyCellRadio == kTrue)){
							//CA("checking by cell");
							refreshTableByAttribute = kTrue;
							refresh.doRefresh(Mediator::selectedRadioButton);
						}
						if(shouldPushInFreameVect == kTrue){
							//CA("shouldPushInFreameVect == kTrue");
							FrameRecord freamRecordObj;	// 0 = text Frame , 1 =  Table Fame , 2 Graphic Frame
							IIDXMLElement* ptr;
							TagList tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex),&ptr);
							if(tList.size() <= 0)
								continue;
							if(tList[0].sectionID == -1 && tList[0].parentId == -1)
								continue;	
							if(tList[0].isTablePresent || tList[0].typeId == -5 || tList[0].elementId == -101){
								TagList tList1 = itagReader->getTagsFromBox_ForRefresh_ByAttribute(tempList.GetRef(UIDIndex), &ptr);
								if(tList1.size() == 0)
								{
									//CA("tList1.size() == 0");
									continue;
								}

								freamRecordObj.frameType = 1;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
								if(tList[0].whichTab == 3){
									//CA("tList[0].whichTab == 3");
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);						
									//freamRecordObj.Data = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tList1[0].elementId ,tList1[0].languageID);
									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID , tList[0].sectionID, kFalse);
											
								}
								else if(tList[0].whichTab == 4){
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID, tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();
									freamRecordObj.parentID = itemModelObj.getItemNo();
									if(itemDescriptionAttributeID == -1){
										//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
										//freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
									
									{
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
								}									
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);

								
								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}
							else if(tList[0].imgFlag == 1){
								//CA("tList[0].imgFlag == 1");
								freamRecordObj.frameType = 2;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
								
								if(tList[0].whichTab == 3){
									//CA("AP");
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);

									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
								}
								else if(tList[0].whichTab == 4){
									//CA("tList[0].whichTab == 4");
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID, tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();

									freamRecordObj.parentID = itemModelObj.getItemNo();
									if(itemDescriptionAttributeID == -1){
										//CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
										//freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
									
									{
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
								}
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);

								
								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}
							else{
								//CA("Simple Text Frame");
								freamRecordObj.frameType = 0;
								freamRecordObj.frameUID = tempList[UIDIndex];
								freamRecordObj.pageNumber = pageIndexInDoc + pageNo;
								if(tList[0].whichTab == 3){
									freamRecordObj.parentID = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,-1,tList[0].languageID, tList[0].sectionID, kTrue);						
								}
								else{
									//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,tList[0].languageID);
									CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tList[0].sectionID, tList[0].parentId, 0, tList[0].languageID);
									CItemModel itemModelObj =  pbObjValue.getItemModel();

									freamRecordObj.parentID = itemModelObj.getItemNo();
								}						
								if(tList[0].whichTab == 3){
									//int32 elementID = ptrIAppFramework->CONFIGCACHE_getElementIDForItemGroupDescription();
									freamRecordObj.Data = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tList[0].parentId,/*elementID*/itemGroupDescriptionElementID,tList[0].languageID, tList[0].sectionID, kFalse);
								}
								else if(tList[0].whichTab == 4){
									if(itemDescriptionAttributeID == -1){
									//	//CA("itemDescriptionAttributeID == -1");
									//	CAttributeModel attributeMode = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(6,tList[0].languageID);
									//	freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,attributeMode.getAttributeId(),tList[0].languageID, tList[0].sectionID, kFalse);
										itemDescriptionAttributeID= ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
									}
									
									{
										//CA("itemDescriptionAttributeID != -1");
										freamRecordObj.Data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tList[0].parentId,itemDescriptionAttributeID,tList[0].languageID, tList[0].sectionID, kFalse);
									}
								}
								if(!iConverter)
								{
									freamRecordObj.Data=freamRecordObj.Data;					
								}
								else
									freamRecordObj.Data=iConverter->translateString(freamRecordObj.Data);

								
								freamRecordObj.sectionID = tList[0].sectionID;
								freamRecordObj.langId = tList[0].languageID;
								frameRecordList.push_back(freamRecordObj);
							}

							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
						}
						refreshTableByAttribute = kFalse;
						IsDeleteFlagSelected = kTrue;
						shouldPushInFreameVect = kFalse;						
					}
					shouldRefreshFlag = kTrue;
				}				
				else if(refreshMode == 3){					
					//CA("refreshMode == 3");
					for(int32 UIDIndex = tempList.size()-1; UIDIndex >= 0 ; UIDIndex--)
					{
						//CA("Inside for");
						FrameRecord freamRecordObj;
						IIDXMLElement* ptr;
						TagList tList = itagReader->getTagsFromBox_ForRefresh(tempList.GetRef(UIDIndex), &ptr);
						if(tList.size() <= 0)
							continue;
						if(tList[0].sectionID == -1 && tList[0].parentId == -1)
							continue;
						if(uniqueItemOnDocList.size() == 0)
						{
							UniqueItemOnDoc uniqueItemOnDocObj;
							uniqueItemOnDocObj.itemID = tList[0].parentId;
							uniqueItemOnDocObj.langaugeID = tList[0].languageID;
							uniqueItemOnDocObj.sectionID = tList[0].sectionID;
							uniqueItemOnDocList.push_back(uniqueItemOnDocObj);

							sectionIDInDoc.push_back(tList[0].sectionID);
						}
						else{
							bool16 found = kFalse;
							bool16 sectionIDFound = kFalse;
							for(int32 x = 0; x < uniqueItemOnDocList.size(); x++){
								if(tList[0].parentId == uniqueItemOnDocList[x].itemID){
									found = kTrue;
									break;
								}
								else{
									found = kFalse;
								}							
							}
							if(found == kFalse){
								UniqueItemOnDoc uniqueItemOnDocObj;
								uniqueItemOnDocObj.itemID = tList[0].parentId;
								uniqueItemOnDocObj.langaugeID = tList[0].languageID;
								uniqueItemOnDocObj.sectionID = tList[0].sectionID;
								uniqueItemOnDocList.push_back(uniqueItemOnDocObj);
							}

							for(int32 y = 0; y < sectionIDInDoc.size(); y++){
								if(tList[0].sectionID == sectionIDInDoc[y]){
									sectionIDFound = kTrue;
									break;
								}
								else{
									sectionIDFound = kFalse;
								}
							}
							if(sectionIDFound == kFalse)
								sectionIDInDoc.push_back(tList[0].sectionID);
						}
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
					}
					//CA("End of refreshMode == 3");
				}				
			}
		}
	}while(kFalse);
	//CA("end of RfhDlgDialogObserver::fillFrameRecordVector");
}
void RfhDlgDialogObserver::fillRefreshModeListBox(InterfacePtr<IPanelControlData> panelControlData){
	//CA("RfhDlgDialogObserver::fillRefreshModeListBox");
	do{
		IControlView *bookListBoxControlView=panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
		if(bookListBoxControlView==nil)
		{
			//CA("bookListBoxControlView==nil");
			break;
		}
		AcquireWaitCursor awc;
		awc.Animate();

		SDKListBoxHelper bookRefershlistBoxHelper(this, kRfhPluginID);
		bookRefershlistBoxHelper.EmptyCurrentListBox(bookListBoxControlView);

		for(int32 index = 0 ; index < static_cast<int32>(frameRecordList.size()) ; index++)
		{
			//CA("Loop");
			/*PMString insertData("");
			insertData.AppendNumber(frameRecordList[index].pageNumber);
			if(frameRecordList[index].frameType == 0)
				insertData.Append("/Text/");
			else if(frameRecordList[index].frameType == 1)
				insertData.Append("/Chart/");
			else if(frameRecordList[index].frameType == 2)
				insertData.Append("/Image/");
			insertData.Append(frameRecordList[index].parentID);
			insertData.Append("/");
			insertData.Append(frameRecordList[index].Data);*/

			PMString insertData("");
			if(refreshModeSelected == 1){
				insertData.Append("Delete ");
				if(frameRecordList[index].frameType == 0)
					insertData.Append("Text ");
				else if(frameRecordList[index].frameType == 1)
					insertData.Append("Chart ");
				else if(frameRecordList[index].frameType == 2)
					insertData.Append("Image ");
				insertData.Append("Frame on page ");
				insertData.AppendNumber(frameRecordList[index].pageNumber);
				insertData.Append(" for ");
				insertData.Append(frameRecordList[index].parentID);
				insertData.Append("/");
				insertData.Append(frameRecordList[index].Data);
			}
			else if(refreshModeSelected == 2){
				//CA("refreshModeSelected == 2");
				insertData.Append("Update ");
				if(frameRecordList[index].frameType == 0)
					insertData.Append("Text ");
				else if(frameRecordList[index].frameType == 1)
					insertData.Append("Chart ");
				else if(frameRecordList[index].frameType == 2)
					insertData.Append("Image ");
				insertData.Append("Frame on page ");
				insertData.AppendNumber(frameRecordList[index].pageNumber);
				insertData.Append(" for ");
				insertData.Append(frameRecordList[index].parentID);
				insertData.Append("/");
				insertData.Append(frameRecordList[index].Data);
			}
			bookRefershlistBoxHelper.AddElement(bookListBoxControlView,insertData,kRfhDlgTextWidgetID,index,kTrue);
			bookRefershlistBoxHelper.CheckUncheckRow(bookListBoxControlView,index,kFalse,0);
			frameRecordList[index].isSelected = kFalse;			
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::selectNextFrame(){
	//CA("RfhDlgDialogObserver::selectNextFrame");
	do{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelcontrol == nil");
			break;
		}
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
		if(!document)
			return;
		IDataBase* database= ::GetDataBase(document);
		
		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			//CA("NULL");
		}	
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	//CA("!layoutSelectionSuite");
			return;
		}
		
		int32 prevIndex = currentSelectedFrameIndex;
//PMString a("prevIndex	:	");		
//a.AppendNumber(prevIndex);
//a.Append("\nframeRecordList.size()	:	");
//a.AppendNumber(static_cast<int32>(frameRecordList.size()));
//CA(a);
		AcquireWaitCursor awc;
		awc.Animate();

		bool16 isProcessed = kFalse;
		do{
			if(currentSelectedFrameIndex+1 >= frameRecordList.size()){
				if(currentDocumentIndex+1  < selectedDocIndexList.size())
					goForNextDocument();//currentSelectedFrameIndex = 0;
				return;
			}
			else{
				currentSelectedFrameIndex++;
			}	
			if(frameRecordList[currentSelectedFrameIndex].isProcessed == kTrue){
				isProcessed = kTrue;
				continue;					
			}
			else{
				//CA("Else");
				isProcessed = kFalse;
			}

		}while(isProcessed==kTrue);

		if(prevIndex != currentSelectedFrameIndex){
			//CA("prevIndex != currentSelectedFrameIndex");
			//layoutSelectionSuite->DeselectAllPageItems();
			iSelectionManager->DeselectAll(nil);
			UIDList selectUIDList(database,frameRecordList[currentSelectedFrameIndex].frameUID);
			
			SDKListBoxHelper listHelper(this, kRfhPluginID);
			IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
			if(listBoxControlView == nil) 
				break;
		
			listHelper.CheckUncheckRow(listBoxControlView,currentSelectedFrameIndex,!frameRecordList[currentSelectedFrameIndex].isSelected,0);
			frameRecordList[currentSelectedFrameIndex].isSelected = kTrue;	
		
			if(frameRecordList[prevIndex].isProcessed == kFalse){
				listHelper.CheckUncheckRow(listBoxControlView,prevIndex,!frameRecordList[prevIndex].isSelected,0);
				frameRecordList[prevIndex].isSelected = kFalse;
			}
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
			if(listCntl == nil) 
				break;
			listCntl->Select(currentSelectedFrameIndex,kFalse,kFalse);
			
			layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::selectPreviousFrame(){
	do{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelcontrol == nil");
			break;
		}

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
		if(!document)
			return;
		IDataBase* database= ::GetDataBase(document);

		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			//CA("NULL");
		}	
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	//CA("!layoutSelectionSuite");
			break;
		}
		AcquireWaitCursor awc;
		awc.Animate();

		bool16 isProcessed = kFalse;
		int32 prevIndex = currentSelectedFrameIndex;
		do{
			currentSelectedFrameIndex--;
			if(currentSelectedFrameIndex < 0 ){
				if(currentDocumentIndex-1  >= 0)
					goForPreviousDocument();
				return;
			}
			if(frameRecordList[currentSelectedFrameIndex].isProcessed == kTrue){
				//CA("isProcessed == kTrue");
			/*	if(prevIndex == currentSelectedFrameIndex)
					return;*/
				isProcessed = kTrue;
				continue;					
			}
			else{
				isProcessed = kFalse;
			}			
		}while(isProcessed==kTrue);			
		
		if(prevIndex != currentSelectedFrameIndex){
			layoutSelectionSuite->DeselectAllPageItems();			
			UIDList selectUIDList(database,frameRecordList[currentSelectedFrameIndex].frameUID);
			layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);

			SDKListBoxHelper listHelper(this, kRfhPluginID);
			IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
			if(listBoxControlView == nil) 
				break;

			listHelper.CheckUncheckRow(listBoxControlView,currentSelectedFrameIndex,!frameRecordList[currentSelectedFrameIndex].isSelected,0);
			frameRecordList[currentSelectedFrameIndex].isSelected = kTrue;	
			
			if(frameRecordList[prevIndex].isProcessed == kFalse){		
				listHelper.CheckUncheckRow(listBoxControlView,prevIndex,!frameRecordList[prevIndex].isSelected,0);
				frameRecordList[prevIndex].isSelected = kFalse;
			}
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
			if(listCntl == nil) 
				break;
			listCntl->Select(currentSelectedFrameIndex);
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::processUpdateNew(){
	//CA("RfhDlgDialogObserver::processUpdateNew");
	do{
		currentDocumentIndex = -1;

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelControlData == nil");
			break;
		}

		int32 docIndexInBookFileList = -1;

		if(currentDocumentIndex+1  < selectedDocIndexList.size()){
			//CA("currentDocumentIndex+1  < selectedDocIndexList.size()");
			docIndexInBookFileList = selectedDocIndexList[++currentDocumentIndex];
//a.Clear();
//a.Append("docIndexInBookFileList	:	");
//a.AppendNumber(docIndexInBookFileList);
//a.Append("\rcurrentDocumentIndex	:	");
//a.AppendNumber(currentDocumentIndex);
//CA(a);
			SDKLayoutHelper sdklhelp;
			UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndexInBookFileList]);
			if(CurrDocRef == UIDRef ::gNull)
			{
				CA("CurrDocRef is invalid");
				continue;
			}
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
			if(err == kFailure)
			{
				CA("Error occured while opening the layoutwindow of the template");
				continue;
			}

			IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
			if(interactiveListNilGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
				break;
			}
			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			interactiveListNilGroupPanelControlData->HideView();
			firstGroupPanelControlData->ShowView();
			
			PMString temp("");
			if(refreshModeSelected == 3)
				temp.Append("Analyzing Frame New(s) in  ");
			else if(refreshModeSelected == 2)
				temp.Append("Analyzing Frame Updates in  ");	
			temp.Append(BookFileList[docIndexInBookFileList].GetFileName());
			IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString(temp);
			}

			fillFrameRecordVector(refreshModeSelected);

//PMString a("static_cast<int32>(frameRecordList.size())	:	");
//a.AppendNumber(static_cast<int32>(frameRecordList.size()));
//CA(a);	
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString("");
			}

			currentDocumentUIDRef = CurrDocRef;
			if(refreshModeSelected != 3){
				//CA("refreshModeSelected != 3");
				if(static_cast<int32>(frameRecordList.size()) > 0){
					interactiveListNilGroupPanelControlData->HideView();
					firstGroupPanelControlData->ShowView();

					fillRefreshModeListBox(panelControlData);

					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
					IDataBase* database= ::GetDataBase(document);
					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	//CA("!layoutSelectionSuite");
						return;
					}
					layoutSelectionSuite->DeselectAllPageItems();
					
					UIDList selectUIDList(database,frameRecordList[0].frameUID);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
					currentSelectedFrameIndex = 0;

					SDKListBoxHelper listHelper(this, kRfhPluginID);
					IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
					listHelper.CheckUncheckRow(listBoxControlView,0,!frameRecordList[0].isSelected,0);
					frameRecordList[0].isSelected = kTrue;

					InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
					if(listCntl == nil) 
						break;
					listCntl->Select(0);

					PMString temp("Currenty Running : ");
					if(refreshModeSelected == 1)
						temp.Append("Delete");
					else if(refreshModeSelected == 2)
						temp.Append("Update");
										
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{					
					//CA("frameRecordList.size() == 0");
					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();

					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						if(currentDocumentIndex+1 < selectedDocIndexList.size()){
							this->goForNextDocument();
						}
						else{
							if(refreshModeSelected == 1){
								//PMString text("All Deletes in this document are processed.Do you want to proceed  to Updates?");
								PMString text("All deleted items have been processed in this document. Do you  want to proceed with item updates?");
								textcontrol->SetString(text);
								//CA("check_1");
								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();
								
								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}
							}
							else if(refreshModeSelected == 2){
								//PMString text("There are no Updates in this document.Do you want to proceed to  New(s)?");
								PMString text("There are no updated items in this document. Do you  want to proceed with new items?");
								textcontrol->SetString(text);
								//CA("check_2");
								
								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();
								
								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}					
							}							
						}
					}					
				}
			}
			else{
				//CA("refreshModeSelected == 3");
				getNewItemIDs();			
				IControlView *bookListBoxControlView=panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
				if(bookListBoxControlView==nil)
				{
					//CA("bookListBoxControlView==nil");
					break;
				}
				AcquireWaitCursor awc;
				awc.Animate();
//CA("a");
				SDKListBoxHelper bookRefershlistBoxHelper(this, kRfhPluginID);
				bookRefershlistBoxHelper.EmptyCurrentListBox(bookListBoxControlView);

				if(newItemIDList.size() > 0 && newItemIDList.at(0).size() > 0){
					interactiveListNilGroupPanelControlData->HideView();
					firstGroupPanelControlData->ShowView();
					openContentSprayer();

					PMString temp("Currenty Running : New");
										
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{
					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							break;
						}
						if(currentDocumentIndex+1 < selectedDocIndexList.size()){
							this->goForNextDocument();
						}
						else{							
							PMString text("Interactive refresh is now complete. Your catalog pages are updated. Do you want to close document?");
							textcontrol->SetString(text);						
							
			
							IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
							if(OkButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
								break;
							}
							OkButtonControlData->HideView();
							
							IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
							if(yesButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
								break;
							}
							yesButtonControlData->ShowView();					
							
							IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
							if(noButtonControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
								break;
							}
							noButtonControlData->ShowView();
							
							newItemIDList.clear();
							openContentSprayer();							
						}
					}
				}
			}
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::goForNextDocument(){
	//CA("RfhDlgDialogObserver::goForNextDocument");
	do{
		AcquireWaitCursor awc;
		awc.Animate();

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelControlData == nil");
			break;
		}

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(!document)
			return;

		int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
		SDKLayoutHelper sdklhelp;
		sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
		sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
		
		CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

		/*PMString a("docIndexInBookFileList	:	");
		a.AppendNumber(docIndexInBookFileList);
		a.Append("\rcurrentDocumentIndex	:	");
		a.AppendNumber(currentDocumentIndex);
		CA(a);*/


		if(currentDocumentIndex+1  < selectedDocIndexList.size()){
			docIndexInBookFileList = selectedDocIndexList[++currentDocumentIndex];
//a.Clear();
//a.Append("docIndexInBookFileList	:	");
//a.AppendNumber(docIndexInBookFileList);
//a.Append("\rcurrentDocumentIndex	:	");
//a.AppendNumber(currentDocumentIndex);
//CA(a);
			SDKLayoutHelper sdklhelp;
			UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndexInBookFileList]);
			if(CurrDocRef == UIDRef ::gNull)
			{
				CA("CurrDocRef is invalid");
				continue;
			}
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
			if(err == kFailure)
			{
				CA("Error occured while opening the layoutwindow of the template");
				continue;
			}
		
			refreshTableByAttribute = kFalse;
			
			PMString temp("");
			if(refreshModeSelected == 3)
				temp.Append("Analyzing Frame New(s) in  ");
			else if(refreshModeSelected == 2)
				temp.Append("Analyzing Frame Updates in  ");
			else if(refreshModeSelected == 1)
				temp.Append("Analyzing Frame Deletes in  ");	

			temp.Append(BookFileList[docIndexInBookFileList].GetFileName());
			IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString(temp);
			}

			fillFrameRecordVector(refreshModeSelected);
			
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString("");
			}

//PMString a("static_cast<int32>(frameRecordList.size())	:	");
//a.AppendNumber(static_cast<int32>(frameRecordList.size()));
//CA(a);			
			currentDocumentUIDRef = CurrDocRef;
			if(refreshModeSelected != 3){
				if(static_cast<int32>(frameRecordList.size()) > 0){
					IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
					if(interactiveListNilGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
						break;
					}
					interactiveListNilGroupPanelControlData->HideView();

					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					firstGroupPanelControlData->ShowView();

					fillRefreshModeListBox(panelControlData);

					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
					IDataBase* database= ::GetDataBase(document);
					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	//CA("!layoutSelectionSuite");
						return;
					}
					layoutSelectionSuite->DeselectAllPageItems();
					
					UIDList selectUIDList(database,frameRecordList[0].frameUID);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
					currentSelectedFrameIndex = 0;

					SDKListBoxHelper listHelper(this, kRfhPluginID);
					IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
					listHelper.CheckUncheckRow(listBoxControlView,0,!frameRecordList[0].isSelected,0);
					frameRecordList[0].isSelected = kTrue;

					InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
					if(listCntl == nil) 
						break;
					listCntl->Select(0);

					PMString temp("Currenty Running : ");
					if(refreshModeSelected == 1)
						temp.Append("Delete");
					else if(refreshModeSelected == 2)
						temp.Append("Update");
					else if(refreshModeSelected == 3)
						temp.Append("New");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{
						
					//CA("frameRecordList.size() == 0");
					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
					if(interactiveListNilGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
						break;
					}

					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						if(currentDocumentIndex+1 < selectedDocIndexList.size()){
							goForNextDocument();
						}
						else{
							if(refreshModeSelected == 1){
								//CA("refreshModeSelected == 1");
								//PMString text("There are no deletes in this document.Do you want to proceed to  Updates?");
								PMString text("There are no deleted items in this document. Do you want to  proceed with item updates?");
								textcontrol->SetString(text);

								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();

								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}

							}
							else if(refreshModeSelected == 2){
								//PMString text("There are no Updates in this document.Do you want to proceed to  New(s)?");
								PMString text("There are no updated items in this document. Do you want to  proceed with new items?");
								textcontrol->SetString(text);

								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();

								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}							
							}
							
						}
					}					
				}
			}
			else{
				//CA("refreshModeSelected == 3");
				IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
				if(firstGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
					break;
				}
				IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
				if(interactiveListNilGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
					break;
				}
				getNewItemIDs();			
				if(newItemIDList.size() > 0 && newItemIDList.at(0).size() > 0){
					firstGroupPanelControlData->ShowView();
					interactiveListNilGroupPanelControlData->HideView();
					openContentSprayer();

					PMString temp("Currenty Running : ");
					if(refreshModeSelected == 1)
						temp.Append("Delete");
					else if(refreshModeSelected == 2)
						temp.Append("Update");
					else if(refreshModeSelected == 3)
						temp.Append("New");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{
					if(currentDocumentIndex+1 < selectedDocIndexList.size()){
						goForNextDocument();
						break;
					}					

					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						
						PMString text("Interactive refresh is now complete. Your catalog pages are updated. Do you want to close document?");
						textcontrol->SetString(text);						
						
		
						IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
						if(OkButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
							break;
						}
						OkButtonControlData->HideView();
						
						IControlView * yesButtonControlData = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
						if(yesButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData == nil");
							break;
						}
						yesButtonControlData->ShowView();					
						
						IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
						if(noButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
							break;
						}
						noButtonControlData->ShowView();
						
						newItemIDList.clear();
						openContentSprayer();
					}
					
				}
			}
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::goForPreviousDocument(){
	//CA("RfhDlgDialogObserver::goForPreviousDocument");
	do{
		AcquireWaitCursor awc;
		awc.Animate();

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) {
			//CA("panelControlData == nil");
			break;
		}

		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(!document)
			return;

		int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
		SDKLayoutHelper sdklhelp;
		sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
		sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
		
		CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

		/*PMString a("docIndexInBookFileList	:	");
		a.AppendNumber(docIndexInBookFileList);
		a.Append("\rcurrentDocumentIndex	:	");
		a.AppendNumber(currentDocumentIndex);
		CA(a);*/


		if(currentDocumentIndex-1  >= 0){
			docIndexInBookFileList = selectedDocIndexList[--currentDocumentIndex];
//a.Clear();
//a.Append("docIndexInBookFileList	:	");
//a.AppendNumber(docIndexInBookFileList);
//a.Append("\rcurrentDocumentIndex	:	");
//a.AppendNumber(currentDocumentIndex);
//CA(a);
			SDKLayoutHelper sdklhelp;
			UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndexInBookFileList]);
			if(CurrDocRef == UIDRef ::gNull)
			{
				CA("CurrDocRef is invalid");
				continue;
			}
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
			if(err == kFailure)
			{
				CA("Error occured while opening the layoutwindow of the template");
				continue;
			}
		
			refreshTableByAttribute = kFalse;
		
			PMString temp("");
			if(refreshModeSelected == 3)
				temp.Append("Analyzing Frame New(s) in  ");
			else if(refreshModeSelected == 2)
				temp.Append("Analyzing Frame Updates in  ");
			else if(refreshModeSelected == 1)
				temp.Append("Analyzing Frame Deletes in  ");	

			temp.Append(BookFileList[docIndexInBookFileList].GetFileName());
			IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString(temp);
			}

			fillFrameRecordVector(refreshModeSelected);
			
			if(TextWidget)
			{
				//CA("TextWidget4 == nil");
				InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				textcontrol->SetString("");
			}

//PMString a("static_cast<int32>(frameRecordList.size())	:	");
//a.AppendNumber(static_cast<int32>(frameRecordList.size()));
//CA(a);			
			currentDocumentUIDRef = CurrDocRef;
			
			if(refreshModeSelected != 3){
				if(static_cast<int32>(frameRecordList.size()) > 0){
					IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
					if(interactiveListNilGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
						break;
					}
					interactiveListNilGroupPanelControlData->HideView();

					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					firstGroupPanelControlData->ShowView();

					fillRefreshModeListBox(panelControlData);

					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
					IDataBase* database= ::GetDataBase(document);
					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	//CA("!layoutSelectionSuite");
						return;
					}
					layoutSelectionSuite->DeselectAllPageItems();
					
					UIDList selectUIDList(database,frameRecordList[0].frameUID);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
					currentSelectedFrameIndex = 0;

					SDKListBoxHelper listHelper(this, kRfhPluginID);
					IControlView *listBoxControlView  = panelControlData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
					listHelper.CheckUncheckRow(listBoxControlView,0,!frameRecordList[0].isSelected,0);
					frameRecordList[0].isSelected = kTrue;

					InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);		
					if(listCntl == nil) 
						break;
					listCntl->Select(0);

					PMString temp("Currenty Running : ");
					if(refreshModeSelected == 1)
						temp.Append("Delete");
					else if(refreshModeSelected == 2)
						temp.Append("Update");
					else if(refreshModeSelected == 3)
						temp.Append("New");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{
						
					//CA("frameRecordList.size() == 0");
					IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
					if(firstGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
						break;
					}
					IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
					if(interactiveListNilGroupPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
						break;
					}

					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						if(currentDocumentIndex-1 >= 0){
							goForPreviousDocument();
						}
						else{
							if(refreshModeSelected == 1){
								//PMString text("There are no deletes in this document.Do you want to proceed to  Updates?");
								PMString text("There are no deleted items in this document. Do you want to proceed with item updates?");
								textcontrol->SetString(text);

								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();

								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}

							}
							else if(refreshModeSelected == 2){
								//PMString text("There are no Updates in this document.Do you want to proceed to  New(s)?");
								PMString text("There are no updated items in this document. Do you want to  proceed with new items?");
								textcontrol->SetString(text);

								IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
								if(OkButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
									break;
								}
								OkButtonControlData->HideView();
								
								IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
								if(yesButtonControlData1 == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
									break;
								}
								yesButtonControlData1->ShowView();
								
								IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
								if(noButtonControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
									break;
								}
								noButtonControlData->ShowView();

								IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
								if(document){
									AcquireWaitCursor awc;
									awc.Animate();

									SDKLayoutHelper sdklhelp;
									int32 docIndexInBookFileList = selectedDocIndexList[currentDocumentIndex];
									sdklhelp.SaveDocumentAs(currentDocumentUIDRef,BookFileList[docIndexInBookFileList]);
									sdklhelp.CloseDocument(currentDocumentUIDRef, kTrue);	
								}							
							}
							
						}
					}					
				}
			}
			else{
				IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
				if(firstGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::firstGroupPanelControlData == nil");
					break;
				}
				IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
				if(interactiveListNilGroupPanelControlData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
					break;
				}


				getNewItemIDs();			
				if(newItemIDList.size() > 0 && newItemIDList.at(0).size() > 0){
					firstGroupPanelControlData->ShowView();
					interactiveListNilGroupPanelControlData->HideView();
					openContentSprayer();

					PMString temp("Currenty Running : New");
						
					IControlView* TextWidget = panelControlData->FindWidget(kProcessWidgetID);
					if(TextWidget)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						textcontrol->SetString(temp);
					}
				}
				else{
					if(currentDocumentIndex-1 >= 0){
						goForPreviousDocument();
						break;
					}
					

					firstGroupPanelControlData->HideView();
					interactiveListNilGroupPanelControlData->ShowView();
					IControlView* TextWidget4 = panelControlData->FindWidget(kInteractiveMessageWidgetID);
					if(TextWidget4)
					{
						//CA("TextWidget4 == nil");
						InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
						if(!textcontrol)
						{
							//CA("textcontrol == nil");
							return;
						}
						//PMString text("There are no New(s) in this document.");
						PMString text("There are no new items in this document.");
						textcontrol->SetString(text);

						IControlView * OkButtonControlData = panelControlData->FindWidget(kInteractiveMessageOKButtonWidgetID);
						if(OkButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::OkButtonControlData == nil");
							break;
						}
						OkButtonControlData->ShowView();
						
						IControlView * yesButtonControlData1 = panelControlData->FindWidget(kProceedToUpdateYesButtonWidgetID);
						if(yesButtonControlData1 == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::yesButtonControlData1 == nil");
							break;
						}
						yesButtonControlData1->HideView();
						
						IControlView * noButtonControlData = panelControlData->FindWidget(kInteractiveMessageNoButtonWidgetID);
						if(noButtonControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::noButtonControlData == nil");
							break;
						}
						noButtonControlData->HideView();
						openContentSprayer();	
					}
				}
			}
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::getNewItemIDs(){
	do{
		AcquireWaitCursor awc;
		awc.Animate();

		newItemIDList.clear();
		if(uniqueItemOnDocList.size() > 0){						
			for(int32 index = 0 ; index < sectionIDInDoc.size() ; index++){
				vector<double>newItemID;
				VectorPubObjectValuePtr pubObjectValuePtr = ptrIAppFramework->getProductsAndItemsForSection(sectionIDInDoc[index], uniqueItemOnDocList[0].langaugeID);
				if(pubObjectValuePtr == NULL)
					continue;
				if(pubObjectValuePtr->size() == 0){
					delete pubObjectValuePtr;
					continue;
				}
				
				VectorPubObjectValue::iterator it2;
				for(it2 = pubObjectValuePtr->begin(); it2 != pubObjectValuePtr->end(); it2++)
				{
					double objectID = it2->getObject_id();
					bool16 itemIDFound = kFalse;
					bool8 isNewFlagON = it2->getNew_product();
					for(int32 x = 0 ; x < uniqueItemOnDocList.size() ; x++){
						if(objectID == uniqueItemOnDocList[x].itemID){
							itemIDFound = kTrue;
							break;
						}
						else
							uniqueItemOnDocList[x].isNewflagON = isNewFlagON;
					}
					if(itemIDFound == kFalse){
						newItemID.push_back(objectID);
					}
				}
				newItemIDList.push_back(newItemID);

				if(pubObjectValuePtr)
					delete pubObjectValuePtr;
			}
			
		}
	}while(kFalse);
}
void RfhDlgDialogObserver::openContentSprayer(){
	//CA("RfhDlgDialogObserver::openContentSprayer");
	do{
		AcquireWaitCursor awc;
		awc.Animate();

		for(int32 indx = 0 ; indx < sectionIDInDoc.size() ;indx++){
			if(newItemIDList.size() > 0){
				for(int32 i = 0 ; i < newItemIDList[indx].size() ; i++){
					//ptrIAppFramework->UpdateNewFlagforPbObject(sectionIDInDoc[indx], newItemIDList[indx].at(i), true);
				}
			}
		}
		for(int32 index = 0 ; index < uniqueItemOnDocList.size() ; index++){
			if(uniqueItemOnDocList[index].isNewflagON == kTrue){
				//CA("calling UpdateNewFlagforPbObject");
				//ptrIAppFramework->UpdateNewFlagforPbObject(uniqueItemOnDocList[index].sectionID, uniqueItemOnDocList[index].itemID, false);
			}
		}

		//InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		////CA("ActionComponent 29");
		//if(iApplication==nil)
		//{	
		//	//CA("iApplication");
		//	break;
		//}
	
		//InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		//if(iPanelMgr == nil)
		//{			
		//	break;
		//}

		//iPanelMgr->ShowPanelByMenuID(kSPPanelWidgetActionID);

		//IControlView* icontrol = iPanelMgr->GetVisiblePanel(kSPPanelWidgetID);		
		//if (icontrol == nil)
		//{
		//	CA("icontrol invalid");
		//	break;
		//}
		
		InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
		if(!ContentSprayerPtr)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::RfhDlgDialogObserver::openContentSprayer::Pointre to ContentSprayerPtr not found");
			break;
		}
		//if(newItemIDList.size() > 0 && newItemIDList[0].size() > 0)
		if(sectionIDInDoc.size() > 0 && uniqueItemOnDocList.size() > 0)
			ContentSprayerPtr->openContentSprayerPanelFromRefreshContent(sectionIDInDoc[0],uniqueItemOnDocList[0].langaugeID);
		

	}while(kFalse);
}
void RfhDlgDialogObserver::closeContentSprayer(double sectionID,double langID){
	do{
		InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
		if(!ContentSprayerPtr)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::RfhDlgDialogObserver::openContentSprayer::Pointre to ContentSprayerPtr not found");
			break;
		}
		ContentSprayerPtr->closeContentSprayerPanelFromRefreshContent(sectionID,langID);
	}while(kFalse);
}
void RfhDlgDialogObserver::dummyRefrsehForRepoert(int32 casNo)
{	
	//CA("In Function ..dummyRefrsehForRepoert");	

	isDummyCall = kTrue;
	IsDeleteFlagSelected = kTrue;
	UIDRef textFrameUIDRef = UIDRef::gNull;
	UIDRef  newDocRef =  UIDRef::gNull;	
	UIDRef gnewTableUIDRef = UIDRef::gNull;
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return ;


	do{
		itemsUpdated.clear();
		itemsDeleted.clear();
		productsUpdated.clear();
		productsDeleted.clear();
		productImagesReLinked.clear();

		itemImagesDeleted.clear();
		productImagesDeleted.clear();
		itemImagesReLinked.clear();

	
		tempItemList.clear();
		tempProductList.clear();

		ISRfreshBookDlgOpen = kTrue;
		DocWchUtils::StopDocResponderMode();

			AcquireWaitCursor awc;
			awc.Animate();
			//CA("1");						
			

			InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::bookManager == nil");
				return ; 
			}
			
			IBook * activeBook = bookManager->GetCurrentActiveBook();
			if(activeBook == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::activeBook == nil");
				return ;			
			}
			PMString bookName = activeBook->GetBookTitleName();
			InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				//CA("This book doesn't have a book content manager!  Something is wrong.");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::bookContentMgr == nil");
				return;
			}

			PlatformChar pchar('.');
			CharCounter index= bookName.IndexOfCharacter(pchar);
			
			PMString bk = bookName;
			bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indb"
			
			BookReportData::setCurrentBookName(bk);

			IDataBase* bookDB = ::GetDataBase(bookContentMgr);
			if (bookDB == nil) 
			{
				//ASSERT_FAIL("bookDB is nil - wrong database?"); 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::bookDB == nil");
				break;
			}
			
			int32 contentCount = bookContentMgr->GetContentCount();
			//PMString ash("content Count = ");
			//ash.AppendNumber(contentCount);
			//CA(ash);
			BookFileList.clear();  //****
			for (int32 i = 0 ; i < contentCount ; i++) 
			{
				BookContentDocInfo contentDocInfo;
				UID contentUID = bookContentMgr->GetNthContent(i);
				if (contentUID == kInvalidUID) 
				{
					continue; 
				}
				
				InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
				if (bookContent == nil) 
				{
					//CA("bookContent == nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::bookContent == nil");
					break; // out of for loop
				}
							
				IDFile CurrFile;
				bool16 IsMissingPluginFlag = kFalse;

				IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
					(
						bookDB,
						contentUID,
						CurrFile,
						IsMissingPluginFlag
					);

				bool16 Flag1 = kFalse;
				Flag1 =  FileUtils::DoesFileExist(CurrFile);
				if(Flag1 == kFalse)
				{
					//CA("File does not Exists");
					continue;
				}
				BookFileList.push_back(CurrFile);
			}
					
			//bool16 retVal=0;	
			//CA("2");			
			//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
			//ptrIAppFramework->GETCommon_refreshClientCache();
			
			//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			/*if(retVal==0)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::retVal==0");
             	break;
			}*/
			
			K2Vector<PMString> bookContentNames1;
			bookContentNames1 = BookReportData::GetBookContentNames(bookContentMgr);
			
			int32 index1=0;
			IsDeleteFlagSelected = kTrue;
			for(int32 p=0; p< BookFileList.size(); p++/*,itr++*/)
			{	
			
				
				if(p > 0)
					CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
					
				index1++;

				//****	
				PlatformChar pchar('.');
				
				CharCounter index= bookContentNames1[p].IndexOfCharacter(pchar);

				PMString bk = bookContentNames1[p];
				
				bk.Remove(index,5);  // To Remove '.indb' from currnt book full Name  "filename.indd"

				BookReportData::currentSection = bk;
				//CA(" BookReportData::currentSection   "+BookReportData::currentSection );
				//*****

				//***** Here Detect is Document is BookNAme_Refresh_Report.indd then Remove it 				
				SDKLayoutHelper sdklhelp;
				UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
				ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
				
				Refresh refresh;													
				BookReportData::vecReportRows.clear();
				//CA("Start Refrsehing");
				refresh.doRefresh(4);

				//CA("After  Refrsehing");		
				sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
				sdklhelp.CloseDocument(CurrDocRef, kFalse);		
			}
			IsDeleteFlagSelected = kFalse;
				
	}while(kFalse);
	Refresh rf;		

//	PMString asd("entireReport.size = ");
//	asd.AppendNumber(static_cast<int32>(BookReportData::entireReport.size()));
//	asd.Append("\nBookReportData::DeleteTagList.size()");
//	asd.AppendNumber(static_cast<int32>(BookReportData::deletedItemTagList.size()));
	//CA(asd);
	
	//***** Now Start To Generate Report..For Updates 
	if(BookReportData::entireReport.size() > 0)
	{	
		//CA("Start Update Report.");
		newDocRef = rf.createNewTableInsideTextFrame(textFrameUIDRef,gnewTableUIDRef,0);			
	}

			
	//********** Appending Deletes Report in the Cueent Text Frame with Inserting New Table.
	//************
	do{	
		if(BookReportData::deletedItemTagList.size() == 0 )
			break;

		//CA("Second Table..Appending");	
		UIDRef newtableUIDRef ;
		BookReportData bd;
		if(BookReportData::entireReport.size() == 0 && BookReportData::deletedItemTagList.size() > 0)
		{
			//CA("BookReportData::entireReport.size() == 0 && BookReportData::deletedItemTagList.size() > 0");
			newDocRef = rf.createNewTableInsideTextFrame(textFrameUIDRef,gnewTableUIDRef,2);	
			newtableUIDRef = gnewTableUIDRef;
		}
		else{
//			CA("else Part");
//			newtableUIDRef = bd.CreateTable(textFrameUIDRef,tableStartIndex-1,3,3,20.0,70,kTextContentType,"" ,"",2);					
		}
		
		BookReportData::currntRowIndex =3;
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(textFrameUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");			
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemHierarchy==0");
			break;
		}
		
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemTextFrame==0");
			break;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemHierarchy==0");
			break;
		}

		InterfacePtr<ITextFrameColumn>frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemTFC==0");
			break;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			//CA("!textModel" );	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::textModel==0");
			break;
		}

		TextIndex tableStartIndex = frameItemTFC->TextSpan();		
		int32 noofChar = textModel->GetPrimaryStoryThreadSpan();

		RangeData rd =textModel->GetStoryThreadRange(tableStartIndex);

		WideString* wStrcarriageReturn=new WideString("\r");
		textModel->Insert(tableStartIndex,wStrcarriageReturn);									
		delete wStrcarriageReturn;
		
		tableStartIndex = frameItemTFC->TextSpan();

		bd.selectFrame(textFrameUIDRef);

		if(newDocRef != UIDRef::gNull){
			//CA("Niche Waal Coundition");
			/*UIDRef*/ newtableUIDRef = bd.CreateTable(textFrameUIDRef,tableStartIndex-1,3,3,20.0,70,kTextContentType,"" ,"",2);
		}
		
		vector<double>::iterator itr1;		
		vector<vector<BookReportData> >::iterator itr2;		
		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			//CA("Table Model List is Null");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableList==NULL");
			break;
		}
		int32	tableIndex = tableList->GetModelCount() - 1;

		tableIndex = BookReportData::currentTableIndex -1;

		if(tableIndex<0)
		{
			//CA("No Table in Frame");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableIndex<0");
			break;
		}
		
		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableModel == NULL");
			break;
		}	
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableCommands == NULL");
			break;
		}	
					
		for(int32 j=0 ; j < BookReportData::deletedItemTagList.size(); j++)			
		{			
			TagList tListMy = BookReportData::deletedItemTagList[j];
			for(int32 k=0; k < tListMy.size() ; k++)
			{				
				BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
				for(int32 colIndex=0; colIndex < 3 ;colIndex++)
				{
					if(colIndex == 0){
						PMString dummyStr(tListMy[k].tagPtr->GetTagString());
						BookReportData::removeDashFromString(dummyStr);
						tableCommands->SetCellText(WideString(dummyStr),GridAddress(BookReportData::currntRowIndex,0 ));					
					}
					else if(colIndex == 1){
						if(tListMy[k].imgFlag == 0){							
							if(tListMy[0].whichTab == 3){
								PMString ad = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tListMy[k].parentId,-1,tListMy[k].languageID, tListMy[k].sectionID, kTrue);														
								//CA(ad);
								tableCommands->SetCellText(WideString(ad),GridAddress(BookReportData::currntRowIndex,1 ));	
							}
							else{																
								PMString data = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tListMy[k].parentId,tListMy[k].elementId,tListMy[k].languageID, tListMy[k].sectionID, kFalse);
								//CA(data);
								tableCommands->SetCellText(WideString(data),GridAddress(BookReportData::currntRowIndex,1 ));								
							}													
						}						
					}
					else{
						if(tListMy[k].imgFlag == 1)
						{
							int32 curIndex = BookReportData::currntRowIndex;
							static int32 dum =0;							
							BookReportData::MoveCursorAtPosition(newtableUIDRef,curIndex-dum/*BookReportData::currntRowIndex*/,2);
							PMString imagePath(tListMy[k].tagPtr->GetAttributeValue(WideString("href")));
							do
							{
								SDKUtilities::Replace(imagePath,"%20"," ");
							}while(imagePath.IndexOfString("%20") != -1);
							BookReportData::CreatePictureBoxInCurrentCell(curIndex-dum,1,imagePath);
							dum++;
						}						
					}
				}
				BookReportData::currntRowIndex++;
			}
			BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
			BookReportData::currntRowIndex++;
		}		
		//BookReportData::deSelectAllPAgeItems();
		BookReportData::selectTableBodyColumns(tableModel,0,3);
//		BookReportData::ChangeFontSizeOfSelectedTextInFrame(10);
		
		BookReportData::selectTableBodyColumns(tableModel);
		InterfacePtr<ITextAttributeSuite> textAttributeSuite((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));	
		if (textAttributeSuite == nil)
		{
			//CA("textAttributeSuite == nil");
			break;
		}
		textAttributeSuite->ToggleBold();

//		PMReal real(0.25);
//		BookReportData::SetCellStroke(Tables::eBottomSide,real,newtableUIDRef,tableModel);
	}while(kFalse);


	//***** Now Appending the New Item/ItemGroup In the Refresh Report
	do{
		//CA("Creating BookReport for New");
		BookReportData bd;
		if(BookReportData::entireNewItemList.size()== 0)
			break;
		//CA("Second Table..Appending");		

		UIDRef newtableUIDRef = UIDRef::gNull;
		if(BookReportData::entireReport.size() == 0 && BookReportData::deletedItemTagList.size() == 0 && BookReportData::entireNewItemList.size() >0)
		{
			//CA("BookReportData::entireReport.size() == 0 && BookReportData::deletedItemTagList.size() == 0 && BookReportData::entireNewItemList.size() >0");
			newDocRef = rf.createNewTableInsideTextFrame(textFrameUIDRef,gnewTableUIDRef,1);	
			newtableUIDRef = gnewTableUIDRef;
		}
		else{
//			CA("else Part");
//			newtableUIDRef = bd.CreateTable(textFrameUIDRef,tableStartIndex-1,3,3,20.0,70,kTextContentType,"" ,"",2);					
		}

		BookReportData::currntRowIndex =2;
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(textFrameUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::graphicFrameHierarchy == NULL");
			break;
		}		

		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemHierarchy == NULL");
			break;
		}		
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemTextFrame == NULL");
			break;
		}

		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
		//	CA("frameItemHierarchy is NULL");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemHierarchy == NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemTFC == NULL");
			break;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
		//	CA("!textModel" );	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::textModel == NULL");
			break;
		}

		TextIndex tableStartIndex = frameItemTFC->TextSpan();		
		int32 noofChar = textModel->GetPrimaryStoryThreadSpan();

		RangeData rd =textModel->GetStoryThreadRange(tableStartIndex);

		WideString* wStrcarriageReturn=new WideString("\r");
		textModel->Insert(tableStartIndex,wStrcarriageReturn);									
		delete wStrcarriageReturn;
		
		tableStartIndex = frameItemTFC->TextSpan();

		bd.selectFrame(textFrameUIDRef);	
		if(newtableUIDRef == UIDRef::gNull){						
			 newtableUIDRef = bd.CreateTable(textFrameUIDRef,tableStartIndex-1,3,3,20.0,70,kTextContentType,"" ,"",1);
		}
		
		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			//CA("Table Model List is Null");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableList == NULL");
			break;
		}
		int32	tableIndex = tableList->GetModelCount() - 1;

		tableIndex = BookReportData::currentTableIndex -1;
		if(tableIndex<0)
		{
			//CA("No Table in Frame");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableIndex<0");
			break;
		}
		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableModel == NULL");
			break;
		}	
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::tableCommands == NULL");
			break;
		}					
		vector<vector<double> >::iterator itrEntireBkNewImLs;
		vector<double>::iterator itritemID;
		vector<double> itemID;
		vector<double>::iterator itrelementID;
		//BookReportData::ielementID.
		double iItemID ;
		if(BookReportData::entireNewItemList.size() > 0){
			
			for(itrEntireBkNewImLs = BookReportData::entireNewItemList.begin();itrEntireBkNewImLs != BookReportData::entireNewItemList.end();itrEntireBkNewImLs++/*,itrelementID++*/){
				
				 BookReportData::newItemidList = *itrEntireBkNewImLs;
				 double sectionIDD = BookReportData::currentSection.GetAsNumber();
				for(itritemID= BookReportData::newItemidList.begin();itritemID !=BookReportData::newItemidList.end();itritemID++)
				{
					iItemID =*itritemID;						
					BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
					for(int32 col=0; col < 3; col++)
					{
						if(col == 0)	
						{
							PMString dummyStr("Item No");
							BookReportData::removeDashFromString(dummyStr);
							tableCommands->SetCellText(WideString(dummyStr),GridAddress(BookReportData::currntRowIndex,0));	
						}
						else if(col ==1){						
							//for(itritemID = itemID.begin() ;itritemID != itemID.end() ;itritemID++)
							{							
								PMString tempBuffer;//	Show Item No Here	]
								//CA("A3");
								//CItemModel itemModelObj = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(iItemID,BookReportData::langaugeID);	
								
								CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(sectionIDD , iItemID, 0, BookReportData::langaugeID);
								CItemModel itemModelObj =  pbObjValue.getItemModel();

								PMString dummyStr(itemModelObj.getItemNo());
								tableCommands->SetCellText(WideString(dummyStr),GridAddress(BookReportData::currntRowIndex,1));

								//**** Show Description Here	
								//CA("A4");
								PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(iItemID,11000271, BookReportData::langaugeID, sectionIDD,  kFalse );
								BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
								BookReportData::currntRowIndex++;
								tableCommands->SetCellText(WideString("Description "),GridAddress(BookReportData::currntRowIndex,0));
								tableCommands->SetCellText(WideString(itemAttributeValue),GridAddress(BookReportData::currntRowIndex,1));									

								//******
								/*for(itrelementID = BookReportData::ielementID.begin();itrelementID != BookReportData::ielementID.end();itrelementID++)
								{
									if(*itrelementID == 11000271){
										BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
										BookReportData::currntRowIndex++;
										tableCommands->SetCellText(WideString("Description "),GridAddress(BookReportData::currntRowIndex,0));
										PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(iItemID,*itrelementID, BookReportData::langaugeID, kTrue );
										tableCommands->SetCellText(WideString(itemAttributeValue),GridAddress(BookReportData::currntRowIndex,1));									
									}
								}*/
							}
						}
						else{
								//CA("For Image:");
								CObjectValue oVal=ptrIAppFramework->GETProduct_getObjectElementValue(*itritemID, sectionIDD);  // new added in appFramework
								double parentTypeID= -1; //oVal.getObject_type_id();
								if(imageDownLoadPath == "")
								{
									InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
									if(ptrIClientOptions==nil)
									{
										ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::fillBMSImageInBox::Interface for IClientOptions not found.");
										break;
									}
									//PMString imagePath=ptrIClientOptions->getImageDownloadPath();
									imageDownLoadPath=ptrIClientOptions->getImageDownloadPath();
									
								}
								PMString imagePath = imageDownLoadPath;

								
								if(imagePath=="")
								{
									ptrIAppFramework->LogDebug("AP7_RfhDlgDialogObserver::RfhDlgDialogObserver::dummyRefreshForReport::case 1:imagePath == blank");					
									break;
								}

								if(imagePath!="")
								{
									const char *imageP=imagePath.GetPlatformString().c_str();
									if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
									#ifdef MACINTOSH
										imagePath+="/";
									#else
										imagePath+="\\";
									#endif
								}									
								CAssetValue objCAssetvalue;
								PMString fileName("");
								double assetID;

								//PMString ff("*itritemID  = ");
								//ff.AppendNumber(*itritemID);
								//ff.Append("\n parentTypeID = ");
								//ff.AppendNumber(parentTypeID);
								//CA(ff);

								VectorAssetValue::iterator it2, it1;
								VectorAssetValuePtr AssetValuePtrObj =ptrIAppFramework->GETAssets_GetAssetByParentAndType(*itritemID, sectionIDD , parentTypeID);/*GETAssets_GetAssetByParentAndType(*itritemID,parentTypeID,-99)*/;
								if(AssetValuePtrObj== NULL){
									//CA("AssetValuePtrObj== NULL");
									continue;
								}
								if(AssetValuePtrObj->size() == 0){

									delete AssetValuePtrObj;
									//CA("AssetValuePtrObj->size() == 0");
									continue;
								}
								for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++){
									objCAssetvalue = *it1;
									assetID = objCAssetvalue.getAsset_id();
									fileName = objCAssetvalue.geturl();

									if(fileName==""){
										//CA("File Not Found on Local drive");
										continue;
									}
								
									UIDRef newItem;	
									do
									{
										SDKUtilities::Replace(fileName,"%20"," ");
									}while(fileName.IndexOfString("%20") != -1);
									PMString imagePathWithSubdir = imagePath;

								PMString typeCodeString("");
								//////////////////////////////////////////////////=========================from here
								//typeCodeString = objCAssetvalue.getDescription();
								/*if(typeCodeString.NumUTF16TextChars()==0)
								{
									typeCodeString=ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(assetID);
								}*/
								//CA("objCAssetvalue.getDescription  1111");
								if(typeCodeString.NumUTF16TextChars()!=0)
								{ 
									//CA("typeCodeString.NumUTF16TextChars()!=0");
									//PMString s("typeCodeString : ");
									//s.Append(typeCodeString);
									//CA(s);

									PMString TempString = typeCodeString;		
									CharCounter ABC = 0;
									bool16 Flag = kTrue;
									bool16 isFirst = kTrue;
									do
									{
										
										if(TempString.Contains("/", ABC))
										{
			 								ABC = TempString.IndexOfString("/"); 				
										
											PMString * FirstString = TempString.Substring(0, ABC);		 		
		 									PMString newsubString = *FirstString;		 	
										 				 	
		 									if(!isFirst)
		 									imagePathWithSubdir.Append("\\");
										 	
		 									imagePathWithSubdir.Append(newsubString);	

											//if(ptrIAppFramework->getAssetServerOption() == 0)
			 								//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);
										 	
		 									isFirst = kFalse;
										 	
		 									PMString * SecondString = TempString.Substring(ABC+1);
		 									PMString SSSTring =  *SecondString;		 		
		 									TempString = SSSTring;	
											
											if(FirstString)
												delete FirstString;

											if(SecondString)
												delete SecondString;
										}								
										else
										{				
											if(!isFirst)
		 									imagePathWithSubdir.Append("\\");
										 	
		 									imagePathWithSubdir.Append(TempString);		
											//if(ptrIAppFramework->getAssetServerOption() == 0)
			 								//	FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
		 									isFirst = kFalse;				
											Flag= kFalse;																				
											//CA(" 2 : "+ imagePathWithSubdir);
										}
										
									}while(Flag);							
								}							
								/*#ifdef WINDOWS
									imagePathWithSubdir.Append("\\");
								#endif	*/	

								if(!BookReportData::isFileExist(imagePathWithSubdir,fileName))
								{		
									//CA("File Not Exist : "+imagePathWithSubdir+fileName);
									continue;
								}
								PMString total=imagePathWithSubdir+fileName;
                                    
                                #ifdef MACINTOSH
                                    SDKUtilities::convertToMacPath(total);
                                #endif
										
								//**** Now Adding Image to the Column.
								BookReportData::CreatePictureBoxInCurrentCell(BookReportData::currntRowIndex,2,imagePath);

							}						

							if(AssetValuePtrObj)
								delete AssetValuePtrObj;
						}					
					}				
					BookReportData::currntRowIndex++;

					BookReportData::addRowInTable(newtableUIDRef,1,3,20.0,183.6);
					BookReportData::currntRowIndex++;				
				}
			}		
		}
		//BookReportData::deSelectAllPAgeItems();
		BookReportData::selectTableBodyColumns(tableModel,0,3);
//		BookReportData::ChangeFontSizeOfSelectedTextInFrame(10);	

		BookReportData::selectTableBodyColumns(tableModel);
		InterfacePtr<ITextAttributeSuite> textAttributeSuite((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));	
		if (textAttributeSuite == nil)
		{
			//CA("textAttributeSuite == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::textAttributeSuite == NULL");
			break;
		}
		textAttributeSuite->ToggleBold();

//		PMReal real(0.25);
//		BookReportData::SetCellStroke(Tables::eBottomSide,real,newtableUIDRef,tableModel);
	
	}while(kFalse);
		
	//***To Save and Close the Report Document in Book.
	if(newDocRef != UIDRef::gNull)
	{
		//*** Now Resizing the Text Frame
		InterfacePtr<IHierarchy> graphicFrameHierarchy(textFrameUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::graphicFrameHierarchy == NULL");
			return ;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemHierarchy == NULL");
			return ;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::multiColumnItemTextFrame == NULL");
			return ;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemHierarchy == NULL");
			return ;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");	
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::frameItemTFC == NULL");
			return ;
		}
		ThreadTextFrame th;
		if(th.IsTextFrameOverset(frameItemTFC))
		{				
			th.DoCreateAndThreadTextFrame(textFrameUIDRef);						
		}
		//*********
		//CA("UID is Not Null");
		SDKLayoutHelper sdklhelp;
		SDKUtilities sdkutils;

		//CA("1");
		InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (bookManager == nil) {				
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::bookManager == NULL");
			return ; 
		}
		//CA("2");		
		IBook * activeBook = bookManager->GetCurrentActiveBook();
		if(activeBook == nil)	{			
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::dummyRefrsehForRepoert::activeBook == NULL");
			return;			
		}

		IDFile file = activeBook->GetBookFileSpec();
		//CA("3");
		PMString inddfilepath;
        #if WINDOWS
            inddfilepath= file.GetString();
        #else
            inddfilepath = FileUtils::SysFileToPMString(file);
        #endif
		

		sdkutils.RemoveLastElement(inddfilepath);
		sdkutils.AppendPathSeparator(inddfilepath);	

		inddfilepath.Append(BookReportData::getCurrentBookName());

		inddfilepath.Append("_Refresh_Report.indd");
		IDFile docFile = sdkutils.PMStringToSysFile(&inddfilepath);
					
		ErrorCode status = sdklhelp.SaveDocumentAs(newDocRef,docFile);	
		if(status == kFailure)			
			CA("document can not be saved");							
		
		K2Vector<IDFile> contentFileList;
		contentFileList.push_back(docFile);

		BookReportData::addReportToBook(contentFileList);	
		sdklhelp.CloseDocument(newDocRef,kTrue);
	}		
	else
		CAlert::InformationAlert("Since Book is Latest, could not generate any type of Report.");
	
	//***** Now Resetting all the Static Objcets to it's defult Value
	BookReportData::setCurrentBookName("");
	BookReportData::currentSection ="";

	BookReportData::currentTableIndex =0;
	BookReportData::currntRowIndex  =0;

	BookReportData::entireReport.clear();
	BookReportData::deletedItemTagList.clear();	

	BookReportData::newItemidList.clear();
	BookReportData::documentTagList.clear();

	BookReportData::entireNewItemList.clear();
	BookReportData::entireBookTagList.clear();

	BookReportData::ielementID.clear();
	isDummyCall = kFalse;
	IsDeleteFlagSelected = kFalse;
}
// End, BscDlgDialogObserver.cpp.
