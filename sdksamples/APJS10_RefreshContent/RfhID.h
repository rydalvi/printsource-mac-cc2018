//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __RfhID_h__
#define __RfhID_h__

#include "SDKDef.h"

// Company:
#define kRfhCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kRfhCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kRfhPluginName	"APJS10_RefreshContent"			// Name of this plug-in.
#define kRfhPrefixNumber	0xABA1A30 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kRfhVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kRfhAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kRfhPrefixNumber above to modify the prefix.)
#define kRfhPrefix		RezLong(kRfhPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kRfhStringPrefix	SDK_DEF_STRINGIZE(kRfhPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kRfhMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kRfhMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kRfhPluginID, kRfhPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kRfhActionComponentBoss, kRfhPrefix + 0)
DECLARE_PMID(kClassIDSpace, kRfhPanelWidgetBoss, kRfhPrefix + 1)
DECLARE_PMID(kClassIDSpace, kRfhPanelListBoxWidgetBoss,	kRfhPrefix + 3)
DECLARE_PMID(kClassIDSpace, kRfhDocWchResponderServiceBoss,	kRfhPrefix + 4)
DECLARE_PMID(kClassIDSpace, kRfhLoginEventsHandler,		kRfhPrefix + 5)
DECLARE_PMID(kClassIDSpace, kRfhIconSuiteWidgetBoss,	kRfhPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kTextSuiteBoss, kRfhPrefix + 7)
DECLARE_PMID(kClassIDSpace, kBookRefreshDialogBoss,		kRfhPrefix + 8)
DECLARE_PMID(kClassIDSpace, kRfhDlgListBoxWidgetBoss,	kRfhPrefix + 9)
DECLARE_PMID(kClassIDSpace, kRfhPanelActionFilterBoss,	kRfhPrefix + 10)
DECLARE_PMID(kClassIDSpace, kRfhLstboxMultilineTxtWidgetBoss, kRfhPrefix + 11)
DECLARE_PMID(kClassIDSpace, kRefreshContentBoss,		kRfhPrefix + 12)
DECLARE_PMID(kClassIDSpace, kPreferenceDialogBoss,		kRfhPrefix + 13)
DECLARE_PMID(kClassIDSpace, kRfhBookDlgListBoxWidgetBoss, kRfhPrefix + 14)//-----------
//DECLARE_PMID(kClassIDSpace, kRefreshModeRefreshDialogBoss, kRfhPrefix + 15)
DECLARE_PMID(kClassIDSpace, kRfhRefreshModeDlgListBoxWidgetBoss, kRfhPrefix + 16)
DECLARE_PMID(kClassIDSpace, kTableRefreshModeRefreshDialogBoss, kRfhPrefix + 17)
DECLARE_PMID(kClassIDSpace, kRFHTreeViewWidgetBoss, kRfhPrefix + 18)
DECLARE_PMID(kClassIDSpace, kRFHTreeNodeWidgetBoss, kRfhPrefix + 19)
DECLARE_PMID(kClassIDSpace, kRFHCRBookDocListTreeViewWidgetBoss, kRfhPrefix + 20)
DECLARE_PMID(kClassIDSpace, kRFHCRBookDocTreeNodeWidgetBoss, kRfhPrefix + 21)
DECLARE_PMID(kClassIDSpace, kRFHCRBookFieldListTreeViewWidgetBoss, kRfhPrefix + 22)
DECLARE_PMID(kClassIDSpace, kRFHCRBookFieldTreeNodeWidgetBoss, kRfhPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kRfhBoss, kRfhPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kRfhBoss, kRfhPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhSUITE,						kRfhPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhContentSELECTIONOBSERVER,	kRfhPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhWIDGETOBSERVER,				kRfhPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT,				kRfhPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_IRFHTRVSHADOWEVENTHANDLER,		kRfhPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRFHINTERFACE, kRfhPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kRfhActionComponentImpl,			kRfhPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kRfhContentSelectionObserverImpl,	kRfhPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kRfhPanelListBoxObserverImpl,		kRfhPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kRfhSuiteTextCSBImpl,				kRfhPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kRfhSuiteASBImpl,					kRfhPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kRfhWidgetObserverImpl,			kRfhPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kRfhDocWchServiceProviderImpl,		kRfhPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kRfhDocWchResponderImpl,			kRfhPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kRfhLoginEventsHandlerImpl,		kRfhPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kRfhIconWidgetObserverImpl,		kRfhPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kEventHandlerImpl,					kRfhPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kRfhDialogControllerImpl,			kRfhPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kRfhDialogObserverImpl,			kRfhPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kRfhDlgListBoxObserverImpl ,		kRfhPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kRfhActionFilterImpl ,				kRfhPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kRfhRefreshControlViewImpl ,		kRfhPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kRefreshContentImpl,				kRfhPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kPreferenceDialogControllerImpl,	kRfhPrefix + 17)
DECLARE_PMID(kImplementationIDSpace, kPreferenceDialogObserverImpl,		kRfhPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kRfhBookListBoxObserverImpl,		kRfhPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kRfhRefreshModeDialogObserverImpl, kRfhPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kRfhRefreshModeDialogControllerImpl,kRfhPrefix + 21)
DECLARE_PMID(kImplementationIDSpace, kRfhRefreshModeDlgListBoxObserverImpl, kRfhPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kRfhTableRefreshModeDialogObserverImpl, kRfhPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kRfhTableRefreshModeDialogControllerImpl, kRfhPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kRfhImpl, kRfhPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kRFHTreeViewHierarchyAdapterImpl,	kRfhPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kRFHTreeViewWidgetMgrImpl,			kRfhPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kRFHTreeObserverImpl,				kRfhPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kRFHTrvNodeEHImpl,					kRfhPrefix + 29)
DECLARE_PMID(kImplementationIDSpace, kRFHCRBookDocTreeViewHierarchyAdapterImpl,		kRfhPrefix + 30)
DECLARE_PMID(kImplementationIDSpace, kRFHCRBookDocTreeViewWidgetMgrImpl,			kRfhPrefix + 31)
DECLARE_PMID(kImplementationIDSpace, kRFHCRBookFieldTreeViewHierarchyAdapterImpl,	kRfhPrefix + 32)
DECLARE_PMID(kImplementationIDSpace, kRFHCRBookFieldTreeViewWidgetMgrImpl,			kRfhPrefix + 33)



// ActionIDs:
//DECLARE_PMID(kActionIDSpace, kRfhAboutActionID,			kRfhPrefix + 1)
DECLARE_PMID(kActionIDSpace, kRfhPanelWidgetActionID,	kRfhPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kRfhSeparator1ActionID,	kRfhPrefix + 2)
//DECLARE_PMID(kActionIDSpace, kRfhPopupAboutThisActionID,kRfhPrefix + 3)
DECLARE_PMID(kActionIDSpace, kRfhMenuItem1ActionID,		kRfhPrefix + (2))
DECLARE_PMID(kActionIDSpace, kRfhMenuItem2ActionID,		kRfhPrefix + (3))
//DECLARE_PMID(kActionIDSpace, kRfhMenuItem3ActionID,		kRfhPrefix + (3))
DECLARE_PMID(kActionIDSpace, kRfhMenuItem4ActionID,		kRfhPrefix + (1))
DECLARE_PMID(kActionIDSpace, kRfhBookDlgWidgetActionID,	kRfhPrefix + (4))
DECLARE_PMID(kActionIDSpace, kRfhPanelPSMenuActionID, kRfhPrefix + 5)
DECLARE_PMID(kActionIDSpace, kRfhMenuDeleteItemFrameActionID, kRfhPrefix + 10)
DECLARE_PMID(kActionIDSpace, kRfhSeparator2ActionID, kRfhPrefix + 11)
DECLARE_PMID(kActionIDSpace, kRfhMenuRefreshTabByAttributeActionID, kRfhPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kRfhActionID, kRfhPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kRfhPanelWidgetID,				kRfhPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kRfhOptionsDropDownWidgetID,	kRfhPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kRfhPageNumWidgetID,			kRfhPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelLstboxWidgetID,		kRfhPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshButtonWidgetID,		kRfhPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelListParentWidgetId,	kRfhPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kRfhUnCheckIconWidgetID,		kRfhPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kRfhCheckIconWidgetID,			kRfhPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelTextWidgetID,			kRfhPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kRfhLocateButtonWidgetID,		kRfhPrefix + 9) 
DECLARE_PMID(kWidgetIDSpace, kRfhDlgListParentWidgetId,		kRfhPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgUnCheckIconWidgetID,	kRfhPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCheckIconWidgetID,		kRfhPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgTextWidgetID,			kRfhPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kRfhDialogWidgetID,			kRfhPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgLstboxWidgetID,			kRfhPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgOptionButtonWidgetID,	kRfhPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgRefreshButtonWidgetID,	kRfhPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kRfhHighliteCheckBoxWidgetID, kRfhPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kRfhDeletFrameCheckBoxWidgetID, kRfhPrefix + 20)
//Added on 30May by Yogesh
DECLARE_PMID(kWidgetIDSpace, kRfhSelectAllCheckBoxWidgetID, kRfhPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kRfhReloadIconWidgetID, kRfhPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCancel1ButtonWidgetID, kRfhPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgNext1ButtonWidgetID, kRfhPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kRfhUpdateCheckBoxWidgetID, kRfhPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kRfhDeleteCheckBoxWidgetID, kRfhPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kRfhFirstGroupPanelWidgetID, kRfhPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kRfhSecondGroupPanelWidgetID, kRfhPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgBackButtonWidgetID, kRfhPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCancel2ButtonWidgetID, kRfhPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kRfhFinishButtonWidgetID, kRfhPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kRfhThirdGroupPanelWidgetID, kRfhPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kItemUpdatedWidgetID, kRfhPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kItemDeletedWidgetID, kRfhPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kProdUpdatedWidgetID, kRfhPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kProdDeletedWidgetID, kRfhPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kRfhZerothGroupPanelWidgetID, kRfhPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgNextButtonWidgetID, kRfhPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCancelButtonWidgetID, kRfhPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kBookPathWidgetID, kRfhPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kRfhUpdateImagesCheckBoxWidgetID, kRfhPrefix + 41)
DECLARE_PMID(kWidgetIDSpace, kItemImagesRelinkWidgetID, kRfhPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kProdImagesRelinkWidgetID, kRfhPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kItemImagesDeletedWidgetID, kRfhPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kProdImagesDeletedWidgetID, kRfhPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgBack1ButtonWidgetID, kRfhPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kRfhSelectionBoxtWidgetID, kRfhPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshTabByAttributeCheckBoxWidgetID, kRfhPrefix + 48)
//DECLARE_PMID(kWidgetIDSpace, kRfhPreferenceIconWidgetID, kRfhPrefix + 49) //Commented By sachi Sharma 15/07/08
DECLARE_PMID(kWidgetIDSpace, kPreferenceDialogWidgetID, kRfhPrefix + 50)
DECLARE_PMID(kWidgetIDSpace, kUpdateDocWithDeleteWidgetID, kRfhPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kRefreshCellRadioButtonWidgetID, kRfhPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kRefreshFullTableRadioButtonWidgetID, kRfhPrefix + 53)
//New Added By Lalit 
DECLARE_PMID(kWidgetIDSpace, kNew_Refresh_FullTable_RadioButtonWidgetID, kRfhPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kNew_Refresh_ByCell_RadioButtonWidgetID, kRfhPrefix + 55)
DECLARE_PMID(kWidgetIDSpace, kClusterRadiowidgetid, kRfhPrefix + 56)
DECLARE_PMID(kWidgetIDSpace, kRfhMidBookListGroupPanelWidgetID, kRfhPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kRfhBookListSelectAllCheckBoxWidgetID, kRfhPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kRfhBookListBoxWidgetID, kRfhPrefix + 59)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgBookListBackButtonWidgetID, kRfhPrefix + 60)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgBookListCancelButtonWidgetID, kRfhPrefix + 61)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgBookListNextButtonWidgetID, kRfhPrefix + 62)
DECLARE_PMID(kWidgetIDSpace, kSPRadioButtonGroupPanelWidgetID, kRfhPrefix + 63)
DECLARE_PMID(kWidgetIDSpace, kRefreshModeOptionWidgetID, kRfhPrefix + 64)
DECLARE_PMID(kWidgetIDSpace, kReportWithRefreshWidgetID, kRfhPrefix + 65)
DECLARE_PMID(kWidgetIDSpace, kRefreshReportWidgetID, kRfhPrefix + 66)
DECLARE_PMID(kWidgetIDSpace, kRefreshModeDialogWidgetID, kRfhPrefix + 67)
DECLARE_PMID(kWidgetIDSpace, kRfhProcessOptionsDropDownWidgetID, kRfhPrefix + 68)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgLstboxWidgetID, kRfhPrefix + 69)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModelDlgListParentWidgetId, kRfhPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgPrevButtonWidgetID, kRfhPrefix + 71)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgApplyButtonWidgetID, kRfhPrefix + 72)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgLocateButtonWidgetID, kRfhPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgNextButtonWidgetID, kRfhPrefix + 74)
//DECLARE_PMID(kWidgetIDSpace, kTableRefreshModeDialogWidgetID, kRfhPrefix + 75)
//DECLARE_PMID(kWidgetIDSpace, kRfhTableRefreshModeDlgOKButtonWidgetID, kRfhPrefix + 76)
//DECLARE_PMID(kWidgetIDSpace, kTableRefreshModeDlgCancelButtonWidgetID, kRfhPrefix + 77)
DECLARE_PMID(kWidgetIDSpace, kRefreshByCellRadioButtonWidgetID, kRfhPrefix + 78)
DECLARE_PMID(kWidgetIDSpace, kRefreshFullTblRadioButtonWidgetID, kRfhPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kFirstInteractiveGroupPanelWidgetID, kRfhPrefix + 80)
//DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgNextDocButtonWidgetID, kRfhPrefix + 81)
DECLARE_PMID(kWidgetIDSpace, kInteractiveListNilGroupPanelWidgetID, kRfhPrefix + 82)
DECLARE_PMID(kWidgetIDSpace, kInteractiveMessageWidgetID, kRfhPrefix + 83)
//DECLARE_PMID(kWidgetIDSpace, kInteractiveMessageYesButtonWidgetID,kRfhPrefix + 84)
DECLARE_PMID(kWidgetIDSpace, kInteractiveMessageNoButtonWidgetID,kRfhPrefix + 85)
DECLARE_PMID(kWidgetIDSpace, kInteractiveMessageOKButtonWidgetID,kRfhPrefix + 86)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshModeDlgCancelButtonWidgetID,kRfhPrefix + 87)
DECLARE_PMID(kWidgetIDSpace, kRfhNotProcessChkIconWidgetID,kRfhPrefix + 88)
DECLARE_PMID(kWidgetIDSpace, kRfhProcessChkIconWidgetID,kRfhPrefix + 89)
DECLARE_PMID(kWidgetIDSpace, kRfhAnalyzeButtonWidgetID, kRfhPrefix + 90)
DECLARE_PMID(kWidgetIDSpace, kProcessWidgetID, kRfhPrefix + 91)
DECLARE_PMID(kWidgetIDSpace, kRefreshReportOptionWidgetID, kRfhPrefix + 92)
DECLARE_PMID(kWidgetIDSpace, kProceedToUpdateYesButtonWidgetID,kRfhPrefix + 93)
DECLARE_PMID(kWidgetIDSpace, kInteractiveoptionGroupPanelWidgetID,kRfhPrefix + 94)
DECLARE_PMID(kWidgetIDSpace, kInteractiveOptionOKButtonWidgetID,kRfhPrefix + 95)
DECLARE_PMID(kWidgetIDSpace, kInteractiveOptionCancelButtonWidgetID,kRfhPrefix + 96)
DECLARE_PMID(kWidgetIDSpace, kRfhnewGroupPanelWidgetID,kRfhPrefix + 97)  //my first group panel
DECLARE_PMID(kWidgetIDSpace, knewRefreshModeOptionWidgetID,kRfhPrefix + 98)
DECLARE_PMID(kWidgetIDSpace, kcontentstructureWidgetID,kRfhPrefix + 99)
DECLARE_PMID(kWidgetIDSpace, kcontentonlyWidgetID,kRfhPrefix + 100)//

DECLARE_PMID(kWidgetIDSpace, kRfhlastGroupPanelWidgetID,kRfhPrefix + 101)//my second group panel
DECLARE_PMID(kWidgetIDSpace, kRfhnewUpdateCheckBoxWidgetID,kRfhPrefix + 102)
DECLARE_PMID(kWidgetIDSpace, kRfhnewUpdateImagesCheckBoxWidgetID,kRfhPrefix + 103)
DECLARE_PMID(kWidgetIDSpace, kRfhlastnextCheckBoxWidgetID,kRfhPrefix + 104)
DECLARE_PMID(kWidgetIDSpace, kRfhlastcancelCheckBoxWidgetID,kRfhPrefix + 105)
DECLARE_PMID(kWidgetIDSpace, kRfhnewbackCheckBoxWidgetID,kRfhPrefix + 106)//
DECLARE_PMID(kWidgetIDSpace, kClusterRadiowidgetnewid,kRfhPrefix + 107)

DECLARE_PMID(kWidgetIDSpace, kRfhnewMidBookListGroupPanelWidgetID,kRfhPrefix + 108)
DECLARE_PMID(kWidgetIDSpace, kRfhnewBookListSelectAllCheckBoxWidgetID,kRfhPrefix + 109)
DECLARE_PMID(kWidgetIDSpace, kRfhnewBookListBoxWidgetID,kRfhPrefix + 110)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgnewBookListBackButtonWidgetID,kRfhPrefix + 111)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgnewBookListCancelButtonWidgetID,kRfhPrefix + 112)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgnewBookListNextButtonWidgetID,kRfhPrefix + 113)//kRfhNewoptionGroupPanelWidgetID
DECLARE_PMID(kWidgetIDSpace, ktohidebtnWidgetID,kRfhPrefix + 114)
DECLARE_PMID(kWidgetIDSpace, kRfhNewoptionGroupPanelWidgetID,kRfhPrefix + 115)//kRfhDlgCancel123ButtonWidgetID
DECLARE_PMID(kWidgetIDSpace, kRfhDlgfinishButtonWidgetID,kRfhPrefix + 116)
DECLARE_PMID(kWidgetIDSpace, kRfhoriginalGroupPanelWidgetID,kRfhPrefix + 117)
DECLARE_PMID(kWidgetIDSpace, kRfhnew14Back1ButtonWidgetID,kRfhPrefix + 118)
DECLARE_PMID(kWidgetIDSpace, kRfhDlgCancel123ButtonWidgetID,kRfhPrefix + 119)
DECLARE_PMID(kWidgetIDSpace, kfirstnextWidgetID,kRfhPrefix + 120)
DECLARE_PMID(kWidgetIDSpace, kfirstcancelWidgetID,kRfhPrefix + 121)

DECLARE_PMID(kWidgetIDSpace, kRfhPanelTreeViewWidgetID,			kRfhPrefix + 122)
DECLARE_PMID(kWidgetIDSpace, kRfhTreePanelNodeWidgetID,			kRfhPrefix + 123) 
DECLARE_PMID(kWidgetIDSpace, kRfhTreeNodeExpanderWidgetID,		kRfhPrefix + 124) 
DECLARE_PMID(kWidgetIDSpace, kRfhTreeNodeNameWidgetID,			kRfhPrefix + 125) 
DECLARE_PMID(kWidgetIDSpace, kRfhCheckBoxWidgetID,				kRfhPrefix + 126) 


DECLARE_PMID(kWidgetIDSpace, kRfhCRBookDocTreeViewWidgetID,		kRfhPrefix + 127)
DECLARE_PMID(kWidgetIDSpace, kRfhCRBookFieldsTreeViewWidgetID,	kRfhPrefix + 128)



// "About Plug-ins" sub-menu:
#define kRfhAboutMenuKey			kRfhStringPrefix "kRfhAboutMenuKey"
#define kRfhAboutMenuPath		kSDKDefStandardAboutMenuPath kRfhCompanyKey

// "Plug-ins" sub-menu:
#define kRfhPluginsMenuKey 		kRfhStringPrefix "kRfhPluginsMenuKey"
#define kRfhPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kRfhCompanyKey kSDKDefDelimitMenuPath kRfhPluginsMenuKey

// Menu item keys:
#define kRfhMenuItem1MenuItemKey		"kRfhMenuItem1MenuItemKey"
#define kRfhMenuItem2MenuItemKey		"kRfhMenuItem2MenuItemKey"
#define kRfhMenuItem3MenuItemKey		"kRfhMenuItem3MenuItemKey"
#define kRfhMenuItem4MenuItemKey		"kRfhMenuItem4MenuItemKey"

// Other StringKeys:
#define kRfhAboutBoxStringKey	kRfhStringPrefix "kRfhAboutBoxStringKey"
#define kRfhPanelTitleKey					kRfhStringPrefix	"kRfhPanelTitleKey"
#define kRfhStaticTextKey kRfhStringPrefix	"kRfhStaticTextKey"
#define kRfhInternalPopupMenuNameKey kRfhStringPrefix	"kRfhInternalPopupMenuNameKey"
#define kRfhTargetMenuPath kRfhInternalPopupMenuNameKey

#define kRfhFrameRefreshStringKey  	kRfhStringPrefix "kRfhFrameRefreshStringKey"
#define kRfhUniqueAttrStringKey  	kRfhStringPrefix "kRfhUniqueAttrStringKey"
#define kRfhUpdateDocWithDeletesStringKey  	kRfhStringPrefix "kRfhUpdateDocWithDeletesStringKey"
#define kRfhCatlogRefreshStringKey  	kRfhStringPrefix "kRfhCatlogRefreshStringKey"
#define kRfhFrameRefresh2StringKey  	kRfhStringPrefix "kRfhFrameRefresh2StringKey"
#define kRfhSelectStringKey  	kRfhStringPrefix "kRfhSelectStringKey"
#define kRfhSelect2StringKey  	kRfhStringPrefix "kRfhSelect2StringKey"
#define kRfhSelectedFramesStringKey  	kRfhStringPrefix "kRfhSelectedFramesStringKey"
#define kRfhRefreshStringKey  	kRfhStringPrefix "kRfhRefreshStringKey"
#define kRfhLinksRefreshStringKey  	kRfhStringPrefix "kRfhLinksRefreshStringKey"
#define kRfhFullRefreshStringKey  	kRfhStringPrefix "kRfhFullRefreshStringKey"
#define kRfhBlankStringKey  	kRfhStringPrefix "kRfhBlankStringKey"
#define kRfhCatalogFullRefreshStringKey  	kRfhStringPrefix "kRfhCatalogFullRefreshStringKey"
#define kRfhSelectFieldsStringKey  	kRfhStringPrefix "kRfhSelectFieldsStringKey"
#define kRfhSelectAllStringKey  	kRfhStringPrefix "kRfhSelectAllStringKey"
#define kRfhPreferenceStringKey  	kRfhStringPrefix "kRfhPreferenceStringKey"

#define kRfhUpdatedocumentwithdeletesStringKey  	kRfhStringPrefix "kRfhUpdatedocumentwithdeletesStringKey"
#define kRfhRefreshTableStringKey  	kRfhStringPrefix "kRfhRefreshTableStringKey"
#define kRfhByCellStringKey  	kRfhStringPrefix "kRfhByCellStringKey"
#define kRfhFullTableStringKey  	kRfhStringPrefix "kRfhFullTableStringKey"
#define kRfhBlankStringKey  	kRfhStringPrefix "kRfhBlankStringKey"
#define kRfhBackupStringKey  	kRfhStringPrefix "kRfhBackupStringKey"
#define kRfhDocumentDirectoryStringKey  	kRfhStringPrefix "kRfhDocumentDirectoryStringKey"
#define kRfhSelectModeStringKey  	kRfhStringPrefix "kRfhSelectModeStringKey"
#define kRfhInThisModeStringKey  	kRfhStringPrefix "kRfhInThisModeStringKey"
#define kRfhInteractiveModeStringKey  	kRfhStringPrefix "kRfhInteractiveModeStringKey"
#define kRfhOneFrameStringKey  	kRfhStringPrefix "kRfhOneFrameStringKey"
#define kRfhNextStringKey  	kRfhStringPrefix "kRfhNextStringKey"
#define kRfhSelectContentToRefreshStringKey  	kRfhStringPrefix "kRfhSelectContentToRefreshStringKey"
#define kRfhRefreshDocumentStringKey  	kRfhStringPrefix "kRfhRefreshDocumentStringKey"
#define kRfhUpdateProductsStringKey  	kRfhStringPrefix "kRfhUpdateProductsStringKey"
#define kRfhUpdateItemStringKey  	kRfhStringPrefix "kRfhUpdateItemStringKey"
#define kRfhImageRelinkStringKey  	kRfhStringPrefix "kRfhImageRelinkStringKey"
#define kRfhPleaseindicateStringKey  	kRfhStringPrefix "kRfhPleaseindicateStringKey"
#define kRfhPublishingStringKey  	kRfhStringPrefix "kRfhPublishingStringKey"
#define kRfhInOneSourceStringKey  	kRfhStringPrefix "kRfhInOneSourceStringKey"
#define kRfhRefreshTableStringKey  	kRfhStringPrefix "kRfhRefreshTableStringKey"
#define kRfhPleaseSelectStringKey  	kRfhStringPrefix "kRfhPleaseSelectStringKey"
#define kRfhSummaryOfUpdatesandDeletesStringKey  	kRfhStringPrefix "kRfhSummaryOfUpdatesandDeletesStringKey"
#define kRfhItemsFiledUpdatedStringKey  	kRfhStringPrefix "kRfhItemsFiledUpdatedStringKey"
#define kRfhZeroStringKey  	kRfhStringPrefix "kRfhZeroStringKey"
#define kRfhItemImagesReLinkedStringKey  	kRfhStringPrefix "kRfhItemImagesReLinkedStringKey"
#define kRfhItemGroupFieldUpdatedStringKey  	kRfhStringPrefix "kRfhItemGroupFieldUpdatedStringKey"
#define kRfhItemGroupImagesReLinkedStringKey  	kRfhStringPrefix "kRfhItemGroupImagesReLinkedStringKey"
#define kRfhItemFramesDeletedStringKey  	kRfhStringPrefix "kRfhItemFramesDeletedStringKey"
#define kRfhItemImagesDeletedStringKey  	kRfhStringPrefix "kRfhItemImagesDeletedStringKey"
#define kRfhItemGroupImagesDeletedStringKey  	kRfhStringPrefix "kRfhItemGroupImagesDeletedStringKey"
#define kRfhProcessStringKey  	kRfhStringPrefix "kRfhProcessStringKey"
#define kRfhProcessDeletesStringKey  	kRfhStringPrefix "kRfhProcessDeletesStringKey"
#define kRfhProcessUpdatesStringKey  	kRfhStringPrefix "kRfhProcessUpdatesStringKey"
#define kRfhProcessNewsStringKey  	kRfhStringPrefix "kRfhProcessNewsStringKey"
#define kRfhAnalyzeStringKey  	kRfhStringPrefix "kRfhAnalyzeStringKey"
#define kRfhOKStringKey  	kRfhStringPrefix "kRfhOKStringKey"
#define kRfhCancelStringKey  	kRfhStringPrefix "kRfhCancelStringKey"
#define kRfhSpaceStringKey  	kRfhStringPrefix "kRfhSpaceStringKey"
#define kRfhSelectRefreshModeStringKey  	kRfhStringPrefix "kRfhSelectRefreshModeStringKey"
#define kRfhRefreshCatalogStringKey  	kRfhStringPrefix "kRfhRefreshCatalogStringKey"
#define kRfhRefreshingDocumentStringKey  	kRfhStringPrefix "kRfhRefreshingDocumentStringKey"
#define kRfhItemGroupFramesDeletedStringKey  	kRfhStringPrefix "kRfhItemGroupFramesDeletedStringKey"

#define kRfhTwowaystorefreshStringKey  	kRfhStringPrefix "kRfhTwowaystorefreshStringKey"
#define kRfhselectmethodStringKey  	kRfhStringPrefix "kRfhselectmethodStringKey"
#define kRfhMethodStringKey  	kRfhStringPrefix "kRfhMethodStringKey"
#define kRfhSelectrefreshmodeStringKey  	kRfhStringPrefix "kRfhSelectrefreshmodeStringKey"
#define kRfhContentonlyStringKey  	kRfhStringPrefix "kRfhContentonlyStringKey"
#define kRfhUseofcontentonlyoption1StringKey  	kRfhStringPrefix "kRfhUseofcontentonlyoption1StringKey"
#define kRfhUseofcontentonlyoption12StringKey  	kRfhStringPrefix "kRfhUseofcontentonlyoption12StringKey"
#define kRfhUseofContentOnlyoption123StringKey  	kRfhStringPrefix "kRfhUseofContentOnlyoption123StringKey"
#define kRfhUseofContentonlyoption1234StringKey  	kRfhStringPrefix "kRfhUseofContentonlyoption1234StringKey"
#define kRfhContentandStructureStringKey  	kRfhStringPrefix "kRfhContentandStructureStringKey"
#define kRfhUseofStructureoptionStringKey  	kRfhStringPrefix "kRfhUseofStructureoptionStringKey"
#define kRfhUseOfstructure12StringKey  	kRfhStringPrefix "kRfhUseOfstructure12StringKey"
#define kRfhUseofStructureoption123StringKey  	kRfhStringPrefix "kRfhUseofStructureoption123StringKey"
#define kRfhPageRangeStringKey  	kRfhStringPrefix "kRfhPageRangeStringKey"
#define kRfhCurrentPageStringKey  	kRfhStringPrefix "kRfhCurrentPageStringKey"

#define SilentModeKey  	kRfhStringPrefix "SilentModeKey"
#define ContentandStructureKey  	kRfhStringPrefix "ContentandStructureKey"
#define RefreshDocumentKey  	kRfhStringPrefix "RefreshDocumentKey"
#define UpdateItemAndItemGKey  	kRfhStringPrefix "UpdateItemAndItemGKey"
#define UpdateItemAndIGImagesKey  	kRfhStringPrefix "UpdateItemAndIGImagesKey"
#define ImagesRelinkedKey  	kRfhStringPrefix "ImagesRelinkedKey"
#define kRfhReloadStringKey kRfhStringPrefix "kRfhReloadStringKey"
#define kRfhEntireDocumentStringkey kRfhStringPrefix  "kRfhEntireDocumentStringkey"

// Menu item positions:

#define	kRfhSeparator1MenuItemPosition		10.0
#define kRfhAboutThisMenuItemPosition		11.0

#define kRfhPanelMenuItemPosition 3

#define kRfhMenuItem1MenuItemPosition			21.0
#define kRfhMenuItem2MenuItemPosition			22.0
#define kRfhMenuItem3MenuItemPosition			24.0

#define kRfhPanelLstBoxRsrcID					10090
#define kRfhDlgLstBoxRsrcID						10100
#define kRfhDialogResourceID					10110
#define kRfhRefreshIconID						11000  
#define kRfhUnChkIconID							11001
#define kRfhChkIconID							11002
#define kRefreshIconID							11004
#define kPreferenceIconID						11005
#define kPreferenceDialogResourceID				11006
#define kRfhRefreshModeDialogResourceID			11007
#define kRfhRefreshModeDlgLstBoxRsrcID			11008
#define kRfhTableRefreshModeDialogResourceID	11009
#define kRfhNotProcessChkIconResourceID			11010
#define kRfhProcessChkIconResourceID			11011

#define kRFHTreePanelNodeRsrcID					11012


// Initial data format version numbers
#define kRfhFirstMajorFormatNumber  RezLong(1)
#define kRfhFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kRfhCurrentMajorFormatNumber kRfhFirstMajorFormatNumber
#define kRfhCurrentMinorFormatNumber kRfhFirstMinorFormatNumber

//PNGR  ID
#define kPNGYesIconRsrcID                        3300     
#define kPNGYesIconRollRsrcID                    3300
#define kPNGNoIconRsrcID                         3301
#define kPNGNoIconRollRsrcID                     3301
#define kPNGInteractiveOptOKIconRsrcID           3302
#define kPNGInteractiveOptOKIconRollRsrcID       3302
#define kPNGInteractiveOptCancelIconRsrcID       3303
#define kPNGInteractiveOptCancelIconRollRsrcID   3303
#define kPNGPrevIconRsrcID                       3304
#define kPNGPrevIconRollRsrcID                   3304
#define kPNGApplyIconRsrcID                      3305 
#define kPNGApplyIconRollRsrcID                  3305
#define kPNGCancelIconRsrcID                     3306
#define kPNGCancelIconRollRsrcID                 3306
#define kPNGLocateIconRsrcID                     3307
#define kPNGLocateIconRollRsrcID                 3307
#define kPNGNextIconRsrcID1                      3308
#define kPNGNextIconRollRsrcID1                  3308
#define kPNGCancel1IconRsrcID                    3309
#define kPNGCancel1IconRollRsrcID                3309
#define kPNGNext1IconRsrcID                      3310
#define kPNGNext1IconRollRsrcID                  3310
#define kPNGInteractiveMessageOKIconRsrcID       3311
#define kPNGInteractiveMessageOKIconRollRsrcID   3311
#define kPNGBookListBackIconRsrcID               3312
#define kPNGBookListBackIconRollRsrcID           3312
#define kPNGBookListCancelIconRsrcID             3313
#define kPNGBookListCancelIconRollRsrcID         3313
#define kPNGBookListNextIconRsrcID               3314
#define kPNGBookListNextIconRollRsrcID           3314
#define kPNGDlgBackIconRsrcID                    3315
#define kPNGDlgBackIconRollRsrcID                3315
#define kPNGDlgCancelIconRsrcID                  3316
#define kPNGDlgCancelIconRollRsrcID              3316
#define kPNGDlgNextIconRsrcID                    3317
#define kPNGDlgNextIconRollRsrcID                3317
#define kPNGDlgBack1IconRsrcID                   3318
#define kPNGDlgBack1IconRollRsrcID               3318
#define kPNGDlgCancel1IconRsrcID                 3319
#define kPNGDlgCancel1IconRollRsrcID             3319
#define kPNGDlgFinishIconRsrcID                  3320
#define kPNGDlgFinishIconRollRsrcID              3320
#define kPNGDlgCloseIconRsrcID                   3321
#define kPNGDlgCloseIconRollRsrcID               3321
#define kPNGRfReloadIconRsrcID                   3322
#define kPNGRfReloadIconRollRsrcID               3322
#define kPNGRfRefreshIconRsrcID                  3323
#define kPNGRfRefreshIconRollRsrcID              3323

#define kPNGDlgnewNext1IconRsrcID                3324 //use for first dialog
#define kPNGDlgnewNextIconRollRsrcID             3324

#define kPNGDlgnew12Next1IconRsrcID              3325 //use for second dialog
#define kPNGDlgnew12NextIconRollRsrcID           3325

#define kPNGDlgCancel1newIconRsrcID              3326 //use for first dialog
#define kPNGDlgCancel1newIconRollRsrcID          3326

#define kPNGDlgCancel1new12IconRsrcID            3327 //use for second dialog
#define kPNGDlgCancel1new12IconRollRsrcID        3327

#define kPNGDlgBack1newIconRsrcID                3328 //use for second dialog
#define kPNGDlgBacknewIconRollRsrcID             3328

#define kPNGDlgBack13IconRsrcID                  3329
#define kPNGDlgBack13IconRollRsrcID              3329

#define kPNGDlgnewnewNext13IconRsrcID            3330
#define kPNGDlgnewnewNext13IconRollRsrcID        3330

#define kPNGDlgCancel1newnew13IconRsrcID         3331
#define kPNGDlgCancel1newnew13IconRollRsrcID     3331

#define kPNGDlgfinishnewIconRsrcID         3332
#define kPNGDlgfinishnewIconRollRsrcID     3332

#define kPNGDlgcancel123IconRsrcID         3333
#define kPNGDlgcancel123IconRollRsrcID     3333

#endif // __RfhID_h__
