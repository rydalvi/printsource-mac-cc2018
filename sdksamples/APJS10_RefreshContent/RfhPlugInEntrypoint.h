#ifndef __RfhPlugInEntrypoint_h__
#define __RfhPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class RfhPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif