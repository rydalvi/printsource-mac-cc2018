#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CObserver.h"
#include "RfhID.h"
#include "SystemUtils.h"
#include "ListBoxHelper.h"
#include "IListControlData.h"
#include "IPanelControlData.h"
#include "ITriStateControlData.h"
#include "CDialogController.h"


#include "CAlert.h"
//#include "IMessageServer.h"

#include "RfhSelectionObserver.h"
#include "FrameRecord.h"



#define CA(x) CAlert::InformationAlert(x)

extern vecFrameRecord frameRecordList;


class RfhRefreshModeListBoxDlgObserver : public CObserver
{
public:
	RfhRefreshModeListBoxDlgObserver(IPMUnknown *boss);
	~RfhRefreshModeListBoxDlgObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(RfhRefreshModeListBoxDlgObserver, kRfhRefreshModeDlgListBoxObserverImpl)

RfhRefreshModeListBoxDlgObserver::RfhRefreshModeListBoxDlgObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

RfhRefreshModeListBoxDlgObserver::~RfhRefreshModeListBoxDlgObserver()
{
}

void RfhRefreshModeListBoxDlgObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhRefreshModeListBoxDlgObserver::AutoDetach()
{
 	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhRefreshModeListBoxDlgObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	//CA("RfhRefreshModeListBoxDlgObserver::Update");
	InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
	{	
		SDKListBoxHelper listHelper(this, kRfhPluginID);
		IControlView *listBoxControlView  = pPanelData->FindWidget(kRfhRefreshModeDlgLstboxWidgetID);
		
		InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
		double currentlySelectedIndex = listCntl->GetClickItem(); 
		
		if(frameRecordList.size()==0){
			//CA("frameRecordList.size()==0");			
			return;
		}
		int32 showProcessed = 0;
		if(frameRecordList[currentlySelectedIndex].isProcessed){
			//if(frameRecordList[currentlySelectedIndex].isSelected)
			//	showProcessed = 1;
			//else
				showProcessed = 2;
		}
		listHelper.CheckUncheckRow(listBoxControlView,currentlySelectedIndex,!frameRecordList[currentlySelectedIndex].isSelected,showProcessed);
		frameRecordList[currentlySelectedIndex].isSelected = !frameRecordList[currentlySelectedIndex].isSelected;			
	}
}
 
