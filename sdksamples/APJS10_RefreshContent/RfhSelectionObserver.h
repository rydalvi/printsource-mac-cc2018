#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IAppFramework.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
//#include "CObserver.h"

#include "RfhID.h"

class RfhSelectionObserver : public ActiveSelectionObserver
{
	public:
		RfhSelectionObserver(IPMUnknown *boss);
		virtual ~RfhSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		
		void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
		void loadPaletteData();	
		bool16 fillDataInListBox(InterfacePtr<IPanelControlData>, bool16 isGroupByElem=kFalse);
		bool16 setSpreadFromPage(const UID& pageUID);
		bool16 reSortTheListForElem(void);
		bool16 reSortTheListForObject(void);
		void ChangeColorOfAllText(int c , PMString  altColorSwatch);
		bool16 isObjectGroup;
		IPanelControlData*	QueryPanelControlData();
		bool16 fillDataInListBox(int GroupFlag = 1);
		void DisableAll();
		void EnableAll();
		bool16 reSortTheListForUniqueAttr(void);
		bool16 reSortTheListForUniqueAttrNew(void);

		//void HandleActiveSelectionChanged  ( );
		virtual void HandleSelectionChanged(const ISelectionMessage* message);

        bool16 reSortTheListForDeltaElementValues(void);
		


	protected:
		
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void UpdatePanel();
		//virtual void HandleSelectionChanged(const ISelectionMessage* message);
		void HandleDropDownListClick(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void HandleDropDownListClickNewoption(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void ShowRefreshSummary();


		
		
};
