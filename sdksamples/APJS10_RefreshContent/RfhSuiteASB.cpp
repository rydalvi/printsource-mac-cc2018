#include "VCPlugInHeaders.h"								
// Interface includes:						
#include "ISelectionManager.h"
// General includes:
#include "CmdUtils.h"		// this is necessary for the selectionasbtemplates.tpp
#include "SelectionASBTemplates.tpp"
#include "CPMUnknown.h"
#include "K2Vector.h"

// Project includes:
#include "RfhID.h"
#include "IRfhSuit.h"	

class RfhSuiteASB : public CPMUnknown<IRfhSuite>
{
public:
	RfhSuiteASB  (IPMUnknown *iBoss);
	virtual ~RfhSuiteASB (void);
	virtual bool16 CanGetTextInsets(void);
	virtual void GetTextInsets(TextInsets& textInsets);
	virtual bool16	CanApplyTextInset(void);
	virtual ErrorCode ApplyTextInset(const PMReal& insetValue);
	virtual ErrorCode GetTextFocus(ITextFocus * & Ifocus);
};

/* CREATE_PMINTERFACE
 	Binds the C++ implementation class onto its ImplementationID making the C++ code callable by the application.
*/
CREATE_PMINTERFACE (RfhSuiteASB , kRfhSuiteASBImpl)



/* RfhSuiteASB Constructor
*/
RfhSuiteASB::RfhSuiteASB(IPMUnknown* boss) :
	CPMUnknown<IRfhSuite>(boss)
{
}

/* RfhSuiteASB Destructor
*/
RfhSuiteASB::~RfhSuiteASB(void)
{
}

/* 
*/
bool16 RfhSuiteASB::CanGetTextInsets(void)
{
	return (AnyCSBSupports (make_functor(&IRfhSuite::CanGetTextInsets), this));
}

/* 
*/
void RfhSuiteASB::GetTextInsets(TextInsets& textInsets)
{
	CallEach(make_functor(&IRfhSuite::GetTextInsets, textInsets), this);
}

/* 
*/
bool16 RfhSuiteASB::CanApplyTextInset(void)
{
	return (AnyCSBSupports (make_functor(&IRfhSuite::CanApplyTextInset), this));
}

/* 
*/
ErrorCode RfhSuiteASB::ApplyTextInset(const PMReal& insetValue)
{
	return (Process (make_functor(&IRfhSuite::ApplyTextInset, insetValue), this, IRfhSuite::kDefaultIID));
}
ErrorCode RfhSuiteASB::GetTextFocus(ITextFocus * & Ifocus)
{
	return (Process (make_functor(&IRfhSuite::GetTextFocus, Ifocus), this, IRfhSuite::kDefaultIID));
}

// End, RfhSuiteASB.cpp.



