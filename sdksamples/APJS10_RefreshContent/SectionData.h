#ifndef __SectionData_h__
#define __SectionData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class PubData{
	private:
		int32 pubId;
		PMString pubName;
		int32 pubLvlNo;
	public:
		PubData(){
			this->pubId=-1;
			this->pubName.Clear();
			this->pubLvlNo=-1;
		}
		int32 getPubId(void){
			return this->pubId;
		}
		void setPubId(int32 id){
			this->pubId=id;
		}
		PMString getPubName(void){
			return this->pubName;
		}
		void setPubName(PMString name){
			this->pubName=name;
		}
		int32 getPubLvlNo(void){
			return this->pubLvlNo;
		}
		void setPubLvlNo(int32 lvl){
			this->pubLvlNo=lvl;
		}
};

class SectionData{
	private:
		static vector<PubData> sectionInfoVector;
		static vector<PubData> subSectionInfoVector;
	public:
		void setSectionVectorInfo(int32,PMString,int32);
		PubData getSectionVectorInfo(int32);
		void clearSectionVector();
		void setSubsectionVectorInfo(int32,PMString,int32);
		PubData getSubsectionVectorInfo(int32);
		void clearSubsectionVector();
};
#endif