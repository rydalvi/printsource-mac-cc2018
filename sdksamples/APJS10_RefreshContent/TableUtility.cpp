#include "VCPluginHeaders.h"
#include "IGeometry.h"
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ITableCommands.h"
#include "TableUtility.h"
#include "IFrameUtils.h"
#include "IAppFramework.h"
#include "IDataSprayer.h"
#include "ITableUtility.h"
#include "Refresh.h"
#include "CAlert.h"
#include "ITextFrameColumn.h"
#include "ITextMiscellanySuite.h"
#include "IFrameContentSuite.h"
#include "TableStyleUtils.h"
#include "IFrameContentUtils.h"
#include "CommonFunctions.h"
#include "ISpecialChar.h"	
#include "Report.h"
#include "ITableSuite.h" 
#include "ICellContent.h"
#include "MediatorClass.h"
#include "IXMLUtils.h"
#include "XMLReference.h"
#include "IXMLReferenceData.h"
#include "ITableGeometry.h"
#include "IXMLAttributeCommands.h"
/// Global Pointers
//extern IAppFramework* ptrIAppFramework;
///////////

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TableUtility.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


//7Aug..ItemTable
#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextTarget.h"
#include "ITextModelCmds.h"
#include "IRangeData.h"
#include "IUIDData.h"
#include "IConcreteSelection.h"
#include "SDKLayoutHelper.h"
#include "ITextSelectionSuite.h"
//#define CA(x) CAlert::InformationAlert(x)
bool16 AddHeaderToTable = kTrue;
bool16 IsDBTable = kFalse;

extern vector<double>  itemsUpdated;
extern vector<double>  itemsDeleted;
extern vector<double>  productsUpdated;
extern vector<double>  productsDeleted;
extern vector<PMString>Vec_CustumTable_String;

extern vector<double> vec_ItemIDsforcustumtable;
extern vector<double> vec_totalheadersofcustumtable;
extern vector<double>vec_AttributeIdforcustumtable;


extern bool16 iscontentbuttonselected;
extern bool16 issrtucturebuttenselected;
extern int32 totalRowsofcustumtable;
extern int32 totalheadersofcustumtable;
extern int32 totalsColsofcustumtable;
extern bool16 Booklevelstructurevariable;
extern bool16 Booklevelcontentvariable;
//extern multimap<int32,int32>AdavanceTablesItemIDsandElementIDsMap;
extern vector<double>ItemIDsForAdvancedTable;
extern vector<double>AttributeIDSforAdvanceTable;
extern double Languageid;
extern double typeID11;

extern double TypeidAdTable;
extern int32 HeaderAdTable;
extern int32 isAutoResizeAdTable;
extern int32 indexAdTable;
extern double pbObjectIdAdTable;
extern double sectionIDAdTable;
extern double parentTypeIDAdTable;
extern int32 tableFlagAdTable;
extern int32 tableTypeAdTable;
extern double tableIdAdTable;
extern int32 ChildtagAdTable;
extern double ParentIdAdTable;
extern vector<double>VecRownoForAdTable;
extern vector<double>VecColnoForAdTable;

extern bool16 frameDeleted;

extern bool16 IsFrameRefreshSelected;
extern bool16 IsBookrefreshSelected;

extern productList tempProductList;
UIDRef newFrameUIDRef;		//// to add graphics frame in hybrid table


bool16 TableUtility::isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef, int32 tableNumber)
{
	bool16 returnValue=kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return kFalse;
	do
	{
		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::textFrameUID == kInvalidUID");		
			break;
		}
/*		///////////		CS3	Change 
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::textFrame == nil");				
			break;
		}
*/		
////////////	CS3 Change
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::graphicFrameHierarchy == nil");
			return false;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::multiColumnItemHierarchy == nil");
			return false;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return false;
		}

		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::frameItemHierarchy == nil");
			return false;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::textFrame == nil");
			return false;
		}

////////////	End	
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::textModel == nil");						
			break;
		}

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::tableList == nil");								
			break;
		}

		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0)
		{
			break;
		}

		tableIndex=tableNumber-1;

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::tableModel == nil");										
			break;
		}
		
		UIDRef tableRef(::GetUIDRef(tableModel));
		
		tableUIDRef=tableRef;

		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}

/*void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose)
{
	do
	{
		ErrorCode result;
		//CA("Resize");
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalCols().count;
		int32 presentCols = tableModel->GetTotalRows().count;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
			break;

		if(isTranspose)
		{
			result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(100.0));
		
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
			}
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}
		}
		else
		{
			if(tableCommands==nil)
				break;
			else
			{
				result = tableCommands->InsertRows(RowRange(1, 2), Tables::eAfter, PMReal(0.0));
			}
			
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows-1), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows+1));
			}			
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}
		}
	}
	while(kFalse);
}*/

void TableUtility::resizeTable
(const UIDRef& tableUIDRef,  int32& numRows,  int32& numCols, bool16 isTranspose)
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do
	{
		ErrorCode result;

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::resizeTable::Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;
		
		/*PMString PresentRows("");
		PresentRows.AppendNumber(presentRows);
		CA("presentRows&" + PresentRows);*/
		
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::resizeTable::tableCommands is nil");
			break;
		}

		if(!isTranspose)
		{	
			if(AddHeaderToTable)
			{
				numRows = numRows+1 ;
			}

			if(presentRows<(numRows))
			{
				// Awasthi
				//CA("presentRows<(numRows");
				//result = tableCommands->InsertRows(RowRange(presentRows-1, (numRows+1)-presentRows), Tables::eAfter, PMReal(0.0));
				result = tableCommands->InsertRows(RowRange(presentRows-1, (numRows)-presentRows), Tables::eAfter, PMReal(0.0));

			}
			else if(presentRows>(numRows))
			{
				
				//CA("presentRows>(numRows");
				//result = tableCommands->DeleteRows(RowRange(0, presentRows-(numRows+1)));
				result = tableCommands->DeleteRows(RowRange((numRows+1)-1, presentRows-(numRows/*+1*/)));

			}
			
			if(presentCols<numCols)
			{
				//  Awasthi
				
				result = tableCommands->InsertColumns(ColRange(presentCols - 1, numCols-presentCols), Tables::eAfter, PMReal(0.0));
			}
			else if(presentCols>numCols)
			{
				//result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
				result = tableCommands->DeleteColumns(ColRange(numCols /*- 1*/, presentCols-numCols));
			}

			// Disabled by Awasthi ( Not required insertion)
			//for table header
			//if(isTranspose)
			//{
				
			//	result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(30.0));
			//}
			//else
			//{
				
			//	result = tableCommands->InsertRows(RowRange(0, 1), Tables::eBefore, PMReal(30.0));
			//}
		}
		else
		{
			if(presentRows<numRows)
			{
				// Awasthi
				
				//result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(0.0));
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(0.0));

			}
			else if(presentRows>numRows)
			{
				
				//result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
				//result = tableCommands->DeleteRows(RowRange(presentRows-numRows-1, presentRows-numRows));
				result = tableCommands->DeleteRows(RowRange(numRows-1, presentRows-numRows));


			}
			if(AddHeaderToTable)
			{
				numCols = numCols+1;
			}
			if(presentCols<(numCols))
			{
				//  Awasthi
				
				//result = tableCommands->InsertColumns(ColRange(presentCols-1, (numCols+1)-presentCols), Tables::eAfter, PMReal(0.0));
				result = tableCommands->InsertColumns(ColRange(presentCols-1,  numCols - presentCols), Tables::eAfter, PMReal(0.0));  //13/11
			}
			else if(presentCols>(numCols))
			{
				//result = tableCommands->DeleteColumns(ColRange(0, presentCols-(numCols+1)));
				//result = tableCommands->DeleteColumns(ColRange((numCols+1)-1, presentCols-(numCols+1)));
				result = tableCommands->DeleteColumns(ColRange((numCols+1)-1, presentCols-numCols));//13/11
			}

		}

	}
	while(kFalse);
	
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////4Aug Item Table Preset Start ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//void TableUtility::fillDataInTableForItem
//(const UIDRef& tableUIDRef, TagStruct& tStruct,int32& tableId,const UIDRef& BoxUIDRef)
//{
//CA("in fillDataInTableForItem");
//	do
//	{
//		//1IsDBTable = kTrue;
//		bool16 IsAutoResize = kFalse;
//		if(tStruct.isAutoResize)
//			IsAutoResize= kTrue;
//		//1AddHeaderToTable = kTrue;
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//			break;
//		}
//
//		int32 objectId = tStruct.parentId; //Product object		
//	//	VectorScreenTableInfoPtr tableInfo=
//	//		ptrIAppFramework->OBJTABLECntrller_getAllScreenTables(objectId);
//
//		int32 sectionid = tStruct.sectionID;
//		
//
//		/*PMString ASD("sectionid : ");
//		ASD.AppendNumber(sectionid);
//		ASD.Append(" objectId : ");
//		ASD.AppendNumber(objectId);
//		ASD.Append(" global_project_level : ");
//		ASD.AppendNumber(global_project_level);
//		CA(ASD);*/
//
//		VectorScreenTableInfoPtr tableInfo=
//				ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(/*objectId*/objectId);
//		if(!tableInfo)
//		{
//			//CA("tableinfo is nil");
//			return;
//		}
//
//		if(tableInfo->size()==0){//CA(" tableInfo->size()==0");
//			break;
//		}
//
//// Added by Rahul
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{	//CA("Slection nil");
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			return;
//		}
//
//		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		//selectionManager->DeselectAll(nil); // deselect every active CSB
//		layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//
//		TableStyleUtils TabStyleObj;
//		TabStyleObj.SetTableModel(kTrue);
//		TabStyleObj.GetTableStyle();				
//		TabStyleObj.getOverallTableStyle();
//
//		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		if(!txtMisSuite)
//		{
//			//CA("My Suit nil");
//			//break;
//		}
//	
//		PMRect theArea;
//		PMReal rel;
//		int32 ht;
//		int32 wt;
//
//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		if(iGeometry==nil)
//			break;
//
//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		rel = theArea.Height();
//		ht=::ToInt32(rel);
//		rel=theArea.Width();	
//		wt=::ToInt32(rel);
//
//		/*PMString ASD("Width = ");
//		ASD.AppendNumber(wt);
//		CA(ASD);*/
////////////////
//		int32 numRows=0;
//		int32 numCols=0;
//
//		CItemTableValue oTableValue;
//		VectorScreenTableInfoValue::iterator it;
//
//		bool16 typeidFound=kFalse;
//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//		{
//			oTableValue = *it;
//			if(oTableValue.getTableTypeID() == tStruct.typeId)
//			{   				
//				typeidFound=kTrue;				
//				break;
//			}
//		}
//
//		if(!typeidFound){
//			break;
//		}
//
//		numRows = oTableValue.getTableData().size();
//		if(numRows<=0)
//			break;
//		numCols = oTableValue.getTableHeader().size();
//		if(numCols<=0)
//			break;
//		bool8 isTranspose = oTableValue.getTranspose();
//
//	//	isTranspose = kTrue;
//		InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//		if(!tWrite)
//			break;
//
//		if(isTranspose)
//		{
//			int32 i=0, j=0;
//			resizeTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht);
//		
//			vector<int32> vec_tableheaders;
//			vector<int32> vec_items;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//	
//			if(AddHeaderToTable){
//			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//			 i=1; j=0;
//			}
//
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			vec_items = oTableValue.getItemIds();
//
//			//vector<int32>::iterator temp;
//			/*	for(int i1=0;i1<vec_items.size();i1++)
//				{
//					PMString AS("vec Item : ");
//					AS.AppendNumber(vec_items[i1]);
//					CA(AS);
//				}*/
//			
//			vector<vector<PMString> >::iterator it1;
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				vector<PMString>::iterator it2;
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{					
//					if(j==0)
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//					setTableRowColData(tableUIDRef, (*it2), j-1, i);	
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j-1];
//				//	CAttributeValue oAttribVal;
//				//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//				//	PMString dispname = oAttribVal.getDisplayName();
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//					PMString TableColName("");// = oAttribVal.getTable_col();
//					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
//				//	{
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(vec_tableheaders[j-1]);
//				//	}
//					//CA(TableColName);
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					if(AddHeaderToTable)
//						stencileInfo.typeId  = vec_items[i-1];
//					else
//						stencileInfo.typeId  = vec_items[i];
//					stencileInfo.parentId = tStruct.parentId;
//					stencileInfo.parentTypeId = tStruct.parentTypeID;
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.LanguageID = tStruct.languageID;
//					XMLReference txmlref;
//	//CA(tStruct.tagPtr->GetTagString());		
//
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, j-1, i,tableId );
//					j++;
//				}
//				i++;
//				j=0;
//			}
//			//tWrite->SetSlug(tableUIDRef,vec_tableheaders,vec_items,isTranspose);
//		}
//		else
//		{
//			int32 i=0, j=0;
//
//			resizeTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht);
//
//			vector<int32> vec_tableheaders;
//			vector<int32> vec_items;
//
//			vec_tableheaders = oTableValue.getTableHeader();
//			if(AddHeaderToTable){
//			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef);
//			i=1; j=0;
//			}
//
//			vec_items = oTableValue.getItemIds();
//
//			//vector<int32>::iterator temp;
//			/*for(int i1=0;i1<vec_items.size();i1++)
//			{
//				PMString AS;
//				AS.AppendNumber(vec_items[i1]);
//				CA(AS);
//			}*/
//			vector<vector<PMString> > vec_tablerows;
//			vec_tablerows = oTableValue.getTableData();
//			vector<vector<PMString> >::iterator it1;
//
//			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
//			{
//				vector<PMString>::iterator it2;
//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
//				{
//					if(j==0 )
//					{
//						j++;
//						continue;
//					}
//					if((j-1)>=numCols)
//						break;
//		
//					setTableRowColData(tableUIDRef, (*it2), i, j-1);
//	
//					SlugStruct stencileInfo;
//					stencileInfo.elementId = vec_tableheaders[j-1];
//					/*CAttributeValue oAttribVal;
//					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
//					PMString dispname = oAttribVal.getDisplayName();*/
//					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tStruct.languageID );// changes is required language ID req.
//					stencileInfo.elementName = dispname;
//					//CA(dispname);
//					PMString TableColName("");  // = oAttribVal.getTable_col();
//					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
//					//{
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(vec_tableheaders[j-1]);
//					/*}*/
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					if(AddHeaderToTable)
//					stencileInfo.typeId  = vec_items[i-1];
//					else
//						stencileInfo.typeId  = vec_items[i];
//					stencileInfo.parentId = tStruct.parentId;;
//					stencileInfo.parentTypeId = tStruct.parentTypeID;
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = CurrentSectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					XMLReference txmlref;
//					//CA("Before Tag Table");
//					this->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, i, j-1, tableId );
//					//CA("After Tag Table");
//				j++;
//				}
//				i++;
//				j=0;
//			}
//			i=0;j=0;
//			//tWrite->SetSlug(tableUIDRef,vec_tableheaders,vec_items,isTranspose);
//
//			//InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//			//if (tableModel == nil)
//			//{
//			//	//	CA("Err: invalid interface pointer ITableFrame");
//			//	break;
//
//			//}
//
//			//InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//			//if(tableCommands==nil)
//			//	break;
//
//			//int32 Size = HeadersizeVector.size();
//			//int32 TotalSum = 0;
//			//for(int32 j=0; j<Size; j++)
//			//{
//			//	TotalSum += HeadersizeVector[j];
//			//}
//			//vector<int32> FinalColWidthVector;
//			//FinalColWidthVector.clear();
//
//			//for(int32 j=0; j<Size; j++)
//			//{				
//			//	float FinalWidth = ((float)HeadersizeVector[j]/TotalSum) * (wt);
//			//	FinalColWidthVector.push_back(FinalWidth);
//			//}
//
//			//for(int32 j=0; j<FinalColWidthVector.size(); j++)
//			//{
//			//	ErrorCode result = tableCommands->ResizeCols(ColRange(j,1), FinalColWidthVector[j] );
//			//}
//			
//		}
//		
//
//		
//		/////////////////////////////////////////////////////////////////////////////////
//		//
//		//InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		//if(!iSelectionManager)
//		//{ 
//		//CA("Selection Manager is Null ");
//		//break;
//		//}
//		//InterfacePtr<ITextMiscellanySuite> txtMisSuite(iSelectionManager,UseDefaultIID()); 
//		//if(!txtMisSuite)
//		//{
//		//CA("Suite Pointer is null");
//		//break;
//		//}
//		//
//		//InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite,UseDefaultIID());
//		//if(!frmContentSuite)
//		//{
//		//CA("IFrame Content Suite is null");
//		//return;
//		//}
//		//frmContentSuite->FitFrameToContent();
//		//
//		//
//
//		if(IsAutoResize)
//		{
//			UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//
//			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{	//CA("!layoutSelectionSuite");
//				return;
//			}
//			layoutSelectionSuite->DeselectAll();
//			layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
//			InterfacePtr<ITextMiscellanySuite> txtMisSuite1(static_cast<ITextMiscellanySuite* >
//			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//			if(!txtMisSuite1)
//			{
//				//CA("My Suit nil");
//				return;
//			}
//			InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite1,UseDefaultIID());
//			if(!frmContentSuite)
//			{
//				//CA("IFrame Content Suite is null");
//				return;
//			}
//			//		frmContentSuite->FitFrameToContent();
//			//CA("2222222");
//			//	if(frmContentSuite->CanFitContentProp())
//			//	{ CA("2 in Spray for this box");
//			//		frmContentSuite->FitContentProp();
//			//	}
//
//			//Commeted By Rahul to avoid Text Frame resizing for Lazboy
//			UIDList processedItems;
//			K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//			ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//			return ;
//		}
//		
//		
//		TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
//		TabStyleObj.ApplyTableStyle();
//		TabStyleObj.setTableStyle();
//	
//	}while(kFalse);
//	IsDBTable = kFalse;
//
//}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////4Aug Item Table Preset End ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////



void TableUtility::setTableRowColData
(const UIDRef& tableUIDRef, const PMString& data, const int32& rowIndex, const int32& colIndex)
{
	do
	{		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;
		
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::setTableRowColData::tableModel is nil");		
			break;
		}

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::setTableRowColData::tableCommands is nil");				
			break;
		}

		PMString textToInsert("");
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			textToInsert=data;					
		}
		else
		{
			textToInsert=iConverter->translateString(data);
			PMString w("data : ");
			w.Append(data);
			//CA(w);
			//CA(textToInsert);
		}

		//WideString* wStr=new WideString(textToInsert);
		const WideString wStr(textToInsert);
		tableCommands->SetCellText(wStr, GridAddress(rowIndex, colIndex));

		//if(wStr)
		//	delete wStr;
	}
	while(kFalse);
}


void TableUtility::fillDataInTable
(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId ,double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::Pointre to DataSprayerPtr not found");
		return;
	}

	VectorScreenTableInfoPtr result=NULL;
	do
	{	
		
		PublicationNode refpNode;
		PMString PubName("PubName");

		if(tagStruct.whichTab == 3)
		refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kTrue,kFalse);
		else if(tagStruct.whichTab == 4)
		refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
	
		DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);
		
		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			// CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		iTableUtlObj->SetDBTableStatus(kTrue);
		// if we want to refresh a Blank Table in Refresh
		int32 elementCountTable=tagStruct.tagPtr->GetChildCount();
		double tableId=0;
		if(elementCountTable == 0)
		{   //CA("Going for DataSprayer fillDataInTable ");
			iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId, BoxUIDRef);
			return;
		}
	
		//InterfacePtr<ITableUtility> iTableUtlObj
		//((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		//if(!iTableUtlObj){ CA("!iTableUtlObj");
		//	return ;
		//}

		//IIDXMLElement* XMLElementPtr =  tagStruct.tagPtr; 		
		//PMString Maintag = XMLElementPtr->GetTagString();
		////CA(Maintag);
		//int MainelementCount=XMLElementPtr->GetChildCount();
		//if(MainelementCount > 0)
		//{
		//	XMLReference MainelementXMLref=XMLElementPtr->GetNthChild(0);
		//	IIDXMLElement * TableElement=MainelementXMLref.Instantiate();
		//	if(TableElement==nil)
		//		break;
		//	//CA("22");
		//	PMString TabletagName=TableElement->GetTagString();
		//	//CA(TabletagName);			
		//	ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElement(MainelementXMLref, kFalse);
		//}
		//
		//iTableUtlObj->SetDBTableStatus(kTrue);		
		//iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId,BoxUIDRef);
		//iTableUtlObj->SetDBTableStatus(kFalse);
		
		//result = ptrIAppFramework->OBJTABLECntrller_getAllScreenTables(objectId);
		//PublicationNode pNode;	
		if(/*refpNode.getIsONEsource()*/ptrIAppFramework->get_isONEsourceMode())
		{
			// For ONEsource mode
			//CA("123 For ONESOURCE");
			//result =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
		}else
		{
			//For publication
			//CA("234 FOR PUBLICATION");
			/*PMString d("sectionID : ");
				d.AppendNumber(tagStruct.sectionID);
			d.Append("\n");
			d.Append("objectID : ");
			d.AppendNumber(objectId);*/
			//CA(d);
			result = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagStruct.sectionID, objectId, tagStruct.languageID/*,kTrue*/);//"True" is commented by amarjit
		}
		if(!result)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!result");		
			return;
		}
		
		bool16 IsAutoResize = kFalse;
		if(tagStruct.isAutoResize)
			IsAutoResize= kTrue;		

		if(tagStruct.header == 1/*tagStruct.colno == -1*/)
			AddHeaderToTable = kTrue;
		else
			AddHeaderToTable = kFalse;

		int32/*int64*/ numRows=0;
		int32/*int64*/ numCols=0;



		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;		
	
		bool16 typeidFound=kFalse;
		for(it = result->begin(); it!=result->end(); it++)
		{
			oTableValue = *it;
			if(oTableValue.getTableTypeID() == tableTypeId && oTableValue.getTableID() == tagStruct.tableId)
			{
				typeidFound=kTrue;
				break;
			}
		}
		
		if(!typeidFound)
		{ 
			bool16 alreadyExist = kFalse;
			if(tagStruct.whichTab == 3)
			{
				double productID = objectId;
						
				for(int index = 0; index<productsDeleted.size();index++ )
				{
					if(productsDeleted[index] == productID)
						alreadyExist = kTrue;
				}

				//if(!alreadyExist)
					productsDeleted.push_back(productID);
			}

			/*PMString updated = "";
			updated.Append("productsDeleted = ");
			updated.AppendNumber(productsDeleted.size());
			updated.Append(" , itemsDeleted = ");
			updated.AppendNumber(itemsDeleted.size());

			CA(updated);*/

			//CA("!typeidFound at line 902 of tableUtility");
			//Refresh refreshObj;
			//refreshObj.deleteThisBox(BoxUIDRef);
			//frameDeleted = kTrue;
			return ;
		}

		bool16 alreadyExist = kFalse;
		if(tagStruct.whichTab == 3)
		{
			//CA("tagStruct.whichTab == 3");
			double productID = tagStruct.parentId;
					
            for(int index = 0; index<productsUpdated.size();index++ )
			{
				if(productsUpdated[index] == productID)
					alreadyExist = kTrue;
			}

			if(!alreadyExist)
				productsUpdated.push_back(productID);

			Report objReport;
			objReport.attributeId = tagStruct.elementId;
			objReport.id = tagStruct.parentId;

			tempProductList.push_back(objReport);
		}


		/*PMString updated = "";
		updated.Append("productsUpdated = ");
		updated.AppendNumber(productsUpdated.size());
		updated.Append(" , itemsUpdated = ");
		updated.AppendNumber(itemsUpdated.size());
		CA(updated);*/
		if(IsBookrefreshSelected==kTrue)
		{
		
			if(/*issrtucturebuttenselected==kTrue || */Booklevelstructurevariable == kTrue)
			{
				//CA("AA");
				numRows = static_cast<int32>(oTableValue.getTableData().size());
				if(numRows<=0)
				{
					break;
				}
				numCols = static_cast<int32>(oTableValue.getTableHeader().size());
				if(numCols<=0)
				{
					break;
				}
			}
			else if(/*iscontentbuttonselected==kTrue ||*/ Booklevelcontentvariable==kTrue)
			{
				//CA("BB");
				numRows = totalRowsofcustumtable;
				numCols = totalsColsofcustumtable;
				
			}
		}
		else if(IsFrameRefreshSelected==kTrue)
		{
		
			if(issrtucturebuttenselected==kTrue /*|| Booklevelstructurevariable == kTrue*/)
			{
				//CA("AA");
				numRows = static_cast<int32>(oTableValue.getTableData().size());
				if(numRows<=0)
				{
					break;
				}
				numCols = static_cast<int32>(oTableValue.getTableHeader().size());
				if(numCols<=0)
				{
					break;
				}
			}
			else if(iscontentbuttonselected==kTrue /*|| Booklevelcontentvariable==kTrue*/)
			{
				//CA("BB");
				numRows = totalRowsofcustumtable;
				numCols = totalsColsofcustumtable;
				
			}
		}

		


		PMString NEWNo("no of Rows: ");
		NEWNo.AppendNumber(numRows);
		NEWNo.Append("\n");
		NEWNo.Append("  no of Cols: ");
		NEWNo.AppendNumber(numCols);
		//CA(NEWNo);
		
		bool8 isTranspose = oTableValue.getTranspose();		
//isTranspose = kTrue;
			
		//vector<int32> vector_items_doc;
		//vector_items_doc.clear();

		//{
		//	UID textFrameUID = kInvalidUID;
		//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		//	if (graphicFrameDataOne) 
		//	{
		//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
		//	}
		//	if (textFrameUID == kInvalidUID)
		//	{
		//		ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
		//		break;//breaks the do{}while(kFalse)
		//	}
		//	
		//	InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		//	if (graphicFrameHierarchy == nil) 
		//	{
		//		//CA("graphicFrameHierarchy is NULL");
		//		break;
		//	}
		//					
		//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		//	if (!multiColumnItemHierarchy) {
		//		//CA("multiColumnItemHierarchy is NULL");
		//		break;
		//	}

		//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		//	if (!multiColumnItemTextFrame) {
		//		//CA("multiColumnItemTextFrame is NULL");
		//		break;
		//	}
		//	InterfacePtr<IHierarchy>
		//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		//	if (!frameItemHierarchy) {
		//		//CA("frameItemHierarchy is NULL");
		//		break;
		//	}

		//	InterfacePtr<ITextFrameColumn>
		//	frameItemTFC(frameItemHierarchy, UseDefaultIID());
		//	if (!frameItemTFC) {
		//		//CA("!!!ITextFrameColumn");
		//		break;
		//	}
		//	InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		//	if (textModel == nil)
		//	{
		//		ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
		//		break;//breaks the do{}while(kFalse)
		//	}
		//	UIDRef StoryUIDRef(::GetUIDRef(textModel));

		//	InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		//	if(tableList==nil)
		//	{
		//		break;//breaks the do{}while(kFalse)
		//	}

		//	int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;

		//	if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
		//	{
		//		break;//breaks the do{}while(kFalse)
		//	}

		//	for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
		//	{//for..tableIndex
		//		//CA("tableIndex");
		//		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		//		if(tableModel == nil)
		//			continue;//continues the for..tableIndex

		//		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		//		if(tableCommands==NULL)
		//		{
		//			ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
		//			continue;//continues the for..tableIndex
		//		}
		//				
		//		UIDRef tableRef(::GetUIDRef(tableModel)); 
		//		//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
		//		IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
		//		XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
		//		UIDRef ContentRef = contentRef.GetUIDRef();
		//	

		//		GridArea headerArea = tableModel->GetHeaderArea();
		//		GridArea tableArea_new = tableModel->GetTotalArea();
		//		ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
		//		ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));


		//		while (iterTable2 != endTable2)
		//		{
		//			GridAddress gridAddress = (*iterTable2);         
		//			InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
		//			if(cellContent == nil) 
		//			{
		//				//CA("cellContent == nil");
		//				break;
		//			}
		//			InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
		//			if(!cellXMLReferenceData) {
		//				//CA("!cellXMLReferenceData");
		//				break;
		//			}
		//			XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
		//			++iterTable2;

		//			
		//			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
		//			
		//			//Get the text tag attached to the text inside the cell.
		//			//We are providing only one texttag inside cell.
		//			if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
		//			{
		//				continue;
		//			}
		//			int32 NoofChilds = cellXMLElementPtr->GetChildCount();

		//			PMString a("childTagCnt :  ");
		//			a.AppendNumber(NoofChilds);
		//			//CA(a);
		//			for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
		//			{ // iterate thruogh cell's tags
		//				XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
		//				//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
		//				InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());

		//				
		//				//The cell may be blank. i.e doesn't have any text tag inside it.
		//				if(cellTextXMLElementPtr == nil)
		//				{
		//					continue;
		//				}
		//				
		//				
		//				PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
		//				
		//				int32 childId = strChildID.GetAsNumber();
		//				if(childId > -1 )
		//				{
		//					PMString itemid1("");
		//					itemid1.Append("itemidssss	:: "+strChildID);
		//					vector_items_doc.push_back(childId);
		//					vector_items_doc.erase(std::unique(vector_items_doc.begin(), vector_items_doc.end()), vector_items_doc.end());
		//					itemid1.Append("	::vector_items_doc	::");
		//					itemid1.AppendNumber(vector_items_doc.size());
		//					CA(itemid1);
		//				}
		//			}//for
		//		}//while
		//	}//end of for
		//}//end of block

		if(isTranspose)
		{
			//CA("if(isTranspose)");
			int32 i=0, j=0;
			//Awasthi
			resizeTable(tableUIDRef, numCols, numRows, isTranspose);
			//End		
			vector<double> vec_tableheaders;
			vector<double> vec_items;
			vec_items = oTableValue.getItemIds();
			
			vec_tableheaders = oTableValue.getTableHeader();

			if(AddHeaderToTable){
				putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose,tagStruct, BoxUIDRef );
				 i=1; j=0;
			}
			vector<vector<PMString> > vec_tablerows;

			vec_tablerows = oTableValue.getTableData();
			//PMString temp1("Size in sprayer: ");
			//temp1.AppendNumber(vec_tablerows.size());
			//CA(temp1);

			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				//CA("row iterator");
				vector<PMString>::iterator it2;
				//PMString temp2("Size of cols in sprayer: ");
				//temp2.AppendNumber((*it1).size());
				//CA(temp2);

				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					//CA("col iterator");
					/*if(j==0)
					{
						j++;
						continue;
					}*/
					if((j/*-1*/)>=numCols)
						break;

					setTableRowColData(tableUIDRef, (*it2), j/*-1*/, i);
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j/*-1*/];
					//temp_to_test CAttributeValue oAttribVal;
					//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j/*-1*/],tagStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j/*-1*/]));
					//}
					//CA(TableColName);
					TableColName = keepOnlyAlphaNumeric(TableColName);
					stencileInfo.TagName = TableColName;


					if(AddHeaderToTable){
						stencileInfo.childId  = vec_items[i-1];//stencileInfo.typeId  = vec_items[i-1];
					}
					else{
						stencileInfo.childId  = vec_items[i];
						stencileInfo.childTag =  1;
					}
					stencileInfo.parentId = tagStruct.parentId;
					stencileInfo.parentTypeId = tagStruct.parentTypeID;
					stencileInfo.whichTab = 4; // Item Attributes
					//stencileInfo.imgFlag = tagStruct.sectionID; 
					stencileInfo.sectionID = tagStruct.sectionID; 
					stencileInfo.isAutoResize = tagStruct.isAutoResize;
					stencileInfo.LanguageID = tagStruct.languageID ;
					stencileInfo.pbObjectId = tagStruct.pbObjectId;
					XMLReference txmlref;
					//CA(tStruct.tagPtr->GetTagString());

					PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
					//CA("TableTagNameNew : " + TableTagNameNew);
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";
						//CA("PSTable Not Found : "+ TableTagNameNew);
					}
					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j/*-1*/, i,tableId );
					j++;
				}
				i++;
				j=0;
				//break;
			}
		}
		else
		{
			//CA("is not transpose");

//=========================New code ==========
//				if(issrtucturebuttenselected==kTrue || Booklevelstructurevariable==kTrue)
//				{
//					if(typeID11==6210)
//					{
//						vector<int32> vec_itemsNew;
//						vec_itemsNew.clear();
//						vec_itemsNew = oTableValue.getItemIds();
//						int32 siiz = static_cast<int32>(vec_itemsNew.size());
//
//						PMString ss("SIze of vec : ");
//						ss.AppendNumber(siiz);
//						//CA(ss);
//						
//						UID textFrameUID = kInvalidUID;
//						InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameDataOne) 
//						{
//							textFrameUID = graphicFrameDataOne->GetTextContentUID();
//						}
//						if (textFrameUID == kInvalidUID)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
//							break;//breaks the do{}while(kFalse)
//						}
//						
//						InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameHierarchy == nil) 
//						{
//							//CA("graphicFrameHierarchy is NULL");
//							break;
//						}
//										
//						InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//						if (!multiColumnItemHierarchy) {
//							//CA("multiColumnItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//						if (!multiColumnItemTextFrame) {
//							//CA("multiColumnItemTextFrame is NULL");
//							break;
//						}
//						InterfacePtr<IHierarchy>
//						frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//						if (!frameItemHierarchy) {
//							//CA("frameItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<ITextFrameColumn>
//						frameItemTFC(frameItemHierarchy, UseDefaultIID());
//						if (!frameItemTFC) {
//							//CA("!!!ITextFrameColumn");
//							break;
//						}
//						InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//						if (textModel == nil)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
//							break;//breaks the do{}while(kFalse)
//						}
//						UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//						InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//						if(tableList==nil)
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//
//						if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//						{//for..tableIndex
//							//CA("tableIndex");
//							InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//							if(tableModel == nil)
//								continue;//continues the for..tableIndex
//
//							InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//							if(tableCommands==NULL)
//							{
//								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
//								continue;//continues the for..tableIndex
//							}
//									
//							UIDRef tableRef(::GetUIDRef(tableModel)); 
//							//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//							IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
//							XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
//							UIDRef ContentRef = contentRef.GetUIDRef();
//							//if( ContentRef != tableRef)
//							//{
//							//	CA("if( ContentRef != tableRef)");
//							//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//							//	continue; //continues the for..tableIndex
//							//}	
//							//CA("outside");
//
//							/*int32 childTagCnt = tagStruct.tagPtr->GetChildCount();
//							PMString a("childTagCnt :  ");
//							a.AppendNumber(childTagCnt);
//							CA(a);*/
//
//							GridArea headerArea = tableModel->GetHeaderArea();
//							GridArea tableArea_new = tableModel->GetTotalArea();
//							ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
//							ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
//
//							int32 count11=0,z=-1;
//
//							while (iterTable2 != endTable2)
//							{
//								GridAddress gridAddress = (*iterTable2);         
//								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//								if(cellContent == nil) 
//								{
//									//CA("cellContent == nil");
//									break;
//								}
//								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//								if(!cellXMLReferenceData) {
//									//CA("!cellXMLReferenceData");
//									break;
//								}
//								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//								++iterTable2;
//
//								
//								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//								
//								//Get the text tag attached to the text inside the cell.
//								//We are providing only one texttag inside cell.
//								if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//								{
//									continue;
//								}
//								int32 NoofChilds = cellXMLElementPtr->GetChildCount();
//
//								PMString a("childTagCnt :  ");
//								a.AppendNumber(NoofChilds);
//								//CA(a);
//								for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//								{ // iterate thruogh cell's tags
//									XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//
//									
//									//The cell may be blank. i.e doesn't have any text tag inside it.
//									if(cellTextXMLElementPtr == nil)
//									{
//										continue;
//									}
//									//Get all the elementID and languageID from the cellTextXMLElement
//									//Note ITagReader plugin methods are not used.
//									PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//									PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//									PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//									PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//									PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
//									PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//									PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
//									PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));
//
//									//added by Tushar on 26/12/06
//									PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//									//check whether the tag specifies ProductCopyAttributes.
//									PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//         							int32 elementID = strElementID.GetAsNumber();
//									int32 languageID = strLanguageID.GetAsNumber();
//
//									PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
//									PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
//									PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
//
//									int32 ColNO = strColNo.GetAsNumber();
//									int32 RowNo = strRowNo.GetAsNumber();
//									int32 typeID = strTypeID.GetAsNumber();
//									int32 sectionid = strSectionId.GetAsNumber();
//
//									int32 parentId = strParentID.GetAsNumber();
//
//									int32 childId = strChildID.GetAsNumber();
//									int32 header = strHeader.GetAsNumber();
//									int32 tableID = strTableID.GetAsNumber();	
//
//									
//
//									if(count11!=RowNo && header!=1)
//									{
//										PMString a("count11 : ");
//										a.AppendNumber(count11);
//										a.Append("\n");
//										a.Append("RowNo : ");
//										a.AppendNumber(RowNo);
//										a.Append("\n");
//										a.Append("header : ");
//										a.AppendNumber(header);
//										//CA(a);
//										/*CA("z++");*/
//										z++;	
//									}
//									
//									count11=RowNo;
//
//									int32 tagStartPos = -1;
//									int32 tagEndPos = -1;
//									Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//									tagStartPos = tagStartPos + 1;
//									tagEndPos = tagEndPos -1;
//
//									PMString dispName("");
//									
//										//CA("Structure gets selected ");
//										if(header==1)
//										{
//										 dispName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);//temp_to_test  = oAttribVal.getDisplayName();
//										}else
//											dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(vec_itemsNew.at(z),elementID,languageID,kTrue);
//									
//									
//									/*PMString a("dispName : ");
//									a.Append(dispName);
//									CA(a);*/
//										if(header==-1)
//										{
//									PMString a("childId :  ");
//									a.AppendNumber(vec_itemsNew.at(z));
//									a.Append("\n");
//									a.Append("elementID : ");
//									a.AppendNumber(elementID);
//									a.Append("\n");
//									a.Append("dispName : ");
//									a.Append(dispName);
//									//CA(a);
//										}
//
//
//									PMString textToInsert("");
//									InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//									if(!iConverter)
//									{
//										textToInsert=dispName;					
//									}
//									else
//										textToInsert=iConverter->translateString(dispName);
//									//Spray the Header data .We don't have to change the tag attributes
//									//as we have done it while copy and paste.
//									WideString insertData(textToInsert);
//									
//									//CA_NUM("tagStartPos ",tagStartPos);
//									//CA_NUM("tagEndPos ",tagEndPos);
//									textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
//
//								}
//							}//end of while
//						}
//					}//end of if(typeID11==6210)
////added by sagar for custom table refresh like printlist
//					else
//					{
//						Refresh refreshObjNew;
//						refreshObjNew.refreshCutomTableInBoxNew(tableUIDRef, vector_items_doc , tableTypeId, tableId ,CurrSectionid, tagStruct, BoxUIDRef);
//					}//end of else for cutom table
////till here	sagar
//					
//				}
//				else if(iscontentbuttonselected==kTrue || Booklevelcontentvariable==kTrue)
//				{
//					if(typeID11==6210)
//					{
//						
//						UID textFrameUID = kInvalidUID;
//						InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameDataOne) 
//						{
//							textFrameUID = graphicFrameDataOne->GetTextContentUID();
//						}
//						if (textFrameUID == kInvalidUID)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
//							break;//breaks the do{}while(kFalse)
//						}
//						
//						InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameHierarchy == nil) 
//						{
//							//CA("graphicFrameHierarchy is NULL");
//							break;
//						}
//										
//						InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//						if (!multiColumnItemHierarchy) {
//							//CA("multiColumnItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//						if (!multiColumnItemTextFrame) {
//							//CA("multiColumnItemTextFrame is NULL");
//							break;
//						}
//						InterfacePtr<IHierarchy>
//						frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//						if (!frameItemHierarchy) {
//							//CA("frameItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<ITextFrameColumn>
//						frameItemTFC(frameItemHierarchy, UseDefaultIID());
//						if (!frameItemTFC) {
//							//CA("!!!ITextFrameColumn");
//							break;
//						}
//						InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//						if (textModel == nil)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
//							break;//breaks the do{}while(kFalse)
//						}
//						UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//						InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//						if(tableList==nil)
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//
//						if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//						{//for..tableIndex
//							//CA("tableIndex");
//							InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//							if(tableModel == nil)
//								continue;//continues the for..tableIndex
//
//							InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//							if(tableCommands==NULL)
//							{
//								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
//								continue;//continues the for..tableIndex
//							}
//									
//							UIDRef tableRef(::GetUIDRef(tableModel)); 
//							//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//							IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
//							XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
//							UIDRef ContentRef = contentRef.GetUIDRef();
//							//if( ContentRef != tableRef)
//							//{
//							//	CA("if( ContentRef != tableRef)");
//							//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
//							//	continue; //continues the for..tableIndex
//							//}	
//							//CA("outside");
//
//							/*int32 childTagCnt = tagStruct.tagPtr->GetChildCount();
//							PMString a("childTagCnt :  ");
//							a.AppendNumber(childTagCnt);
//							CA(a);*/
//
//								GridArea headerArea = tableModel->GetHeaderArea();
//								GridArea tableArea_new = tableModel->GetTotalArea();
//								ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
//								ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
//
//
//								while (iterTable2 != endTable2)
//								{
//									GridAddress gridAddress = (*iterTable2);         
//									InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//									if(cellContent == nil) 
//									{
//										//CA("cellContent == nil");
//										break;
//									}
//									InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//									if(!cellXMLReferenceData) {
//										//CA("!cellXMLReferenceData");
//										break;
//									}
//									XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//									++iterTable2;
//
//									
//									InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//									
//									//Get the text tag attached to the text inside the cell.
//									//We are providing only one texttag inside cell.
//									if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//									{
//										continue;
//									}
//									int32 NoofChilds = cellXMLElementPtr->GetChildCount();
//
//									PMString a("childTagCnt :  ");
//									a.AppendNumber(NoofChilds);
//									//CA(a);
//									for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//									{ // iterate thruogh cell's tags
//										XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//										//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//										InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//
//										
//										//The cell may be blank. i.e doesn't have any text tag inside it.
//										if(cellTextXMLElementPtr == nil)
//										{
//											continue;
//										}
//										//Get all the elementID and languageID from the cellTextXMLElement
//										//Note ITagReader plugin methods are not used.
//										PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
//										PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
//										PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
//										PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
//										PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
//										PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//										PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
//										PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));
//
//										//added by Tushar on 26/12/06
//										PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
//										//check whether the tag specifies ProductCopyAttributes.
//										PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
//                 						int32 elementID = strElementID.GetAsNumber();
//										int32 languageID = strLanguageID.GetAsNumber();
//
//										PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
//										PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
//										PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
//
//										int32 ColNO = strColNo.GetAsNumber();
//										int32 RowNo = strRowNo.GetAsNumber();
//										int32 typeID = strTypeID.GetAsNumber();
//										int32 sectionid = strSectionId.GetAsNumber();
//
//										int32 parentId = strParentID.GetAsNumber();
//
//										int32 childId = strChildID.GetAsNumber();
//										int32 header = strHeader.GetAsNumber();
//										int32 tableID = strTableID.GetAsNumber();	
//
//										
//										int32 tagStartPos = -1;
//										int32 tagEndPos = -1;
//										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
//										tagStartPos = tagStartPos + 1;
//										tagEndPos = tagEndPos -1;
//
//										PMString dispName("");
//										
//											//CA("Content gets selected ");
//											if(header==1)
//											{
//											 dispName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);//temp_to_test  = oAttribVal.getDisplayName();
//											}else
//											 dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID,kTrue);
//										
//										PMString a("dispName : ");
//										a.Append(dispName);
//										//CA(a);
//										
//
//
//										PMString textToInsert("");
//										InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//										if(!iConverter)
//										{
//											textToInsert=dispName;					
//										}
//										else
//											textToInsert=iConverter->translateString(dispName);
//										//Spray the Header data .We don't have to change the tag attributes
//										//as we have done it while copy and paste.
//										WideString insertData(textToInsert);
//										
//										//CA_NUM("tagStartPos ",tagStartPos);
//										//CA_NUM("tagEndPos ",tagEndPos);
//										textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
//				
//									}
//								}
//							}
//
//					}//end of if(typeID11==6210)
////added by sagar for custom table refresh like printlist
//					else
//					{
//						Refresh refreshObjNew;
//						refreshObjNew.refreshCutomTableInBoxNew(tableUIDRef, vector_items_doc, tableTypeId, tableId ,CurrSectionid, tagStruct, BoxUIDRef);
//					}//end of else for cutom table
////till here	sagar		
//				}//end of content if
//				
/////================End of new code 
//				else
				{
					int32 i=0, j=0;
					// Awasthi

					if(issrtucturebuttenselected==kTrue)
						resizeTable(tableUIDRef, numRows, numCols, isTranspose);
					
					vector<double> vec_tableheaders;
					
					vector<double> vec_items;
					if(issrtucturebuttenselected==kTrue || Booklevelstructurevariable==kTrue)
					{
						//if(typeID11==6210)
						{
							vector<double> vec_itemsNew;
							vec_itemsNew.clear();
							vec_itemsNew = oTableValue.getItemIds();
							int32 siiz = static_cast<int32>(vec_itemsNew.size());
	
							PMString ss("SIze of vec : ");
							ss.AppendNumber(siiz);
							//CA(ss);
							
							UID textFrameUID = kInvalidUID;
							InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
							if (graphicFrameDataOne) 
							{
								textFrameUID = graphicFrameDataOne->GetTextContentUID();
							}
							if (textFrameUID == kInvalidUID)
							{
								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
								break;//breaks the do{}while(kFalse)
							}
							
							InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
							if (graphicFrameHierarchy == nil) 
							{
								//CA("graphicFrameHierarchy is NULL");
								break;
							}
											
							InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
							if (!multiColumnItemHierarchy) {
								//CA("multiColumnItemHierarchy is NULL");
								break;
							}
	
							InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
							if (!multiColumnItemTextFrame) {
								//CA("multiColumnItemTextFrame is NULL");
								break;
							}
							InterfacePtr<IHierarchy>
							frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
							if (!frameItemHierarchy) {
								//CA("frameItemHierarchy is NULL");
								break;
							}
	
							InterfacePtr<ITextFrameColumn>
							frameItemTFC(frameItemHierarchy, UseDefaultIID());
							if (!frameItemTFC) {
								//CA("!!!ITextFrameColumn");
								break;
							}
							InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
							if (textModel == nil)
							{
								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
								break;//breaks the do{}while(kFalse)
							}
							UIDRef StoryUIDRef(::GetUIDRef(textModel));
	
							InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
							if(tableList==nil)
							{
								break;//breaks the do{}while(kFalse)
							}
	
							int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
	
							if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
							{
								break;//breaks the do{}while(kFalse)
							}
	
							for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
							{//for..tableIndex
								//CA("tableIndex");
								InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
								if(tableModel == nil)
									continue;//continues the for..tableIndex
	
								InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
								if(tableCommands==NULL)
								{
									ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
									continue;//continues the for..tableIndex
								}
										
								UIDRef tableRef(::GetUIDRef(tableModel)); 
								//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
								IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
								XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
								UIDRef ContentRef = contentRef.GetUIDRef();
								//if( ContentRef != tableRef)
								//{
								//	CA("if( ContentRef != tableRef)");
								//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
								//	continue; //continues the for..tableIndex
								//}	
								//CA("outside");
	
								/*int32 childTagCnt = tagStruct.tagPtr->GetChildCount();
								PMString a("childTagCnt :  ");
								a.AppendNumber(childTagCnt);
								CA(a);*/
	
								GridArea headerArea = tableModel->GetHeaderArea();
								GridArea tableArea_new = tableModel->GetTotalArea();
								ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
								ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
	
								int32 count11=0,z=-1;
	
								while (iterTable2 != endTable2)
								{
									GridAddress gridAddress = (*iterTable2);         
									InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
									if(cellContent == nil) 
									{
										//CA("cellContent == nil");
										break;
									}
									InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
									if(!cellXMLReferenceData) {
										//CA("!cellXMLReferenceData");
										break;
									}
									XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
									++iterTable2;
	
									
									InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
									
									//Get the text tag attached to the text inside the cell.
									//We are providing only one texttag inside cell.
									if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
									{
										continue;
									}
									int32 NoofChilds = cellXMLElementPtr->GetChildCount();
	
									/*PMString a("childTagCnt :  ");
									a.AppendNumber(NoofChilds);
									CA(a);*/
									for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
									{ // iterate thruogh cell's tags
										XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
										//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
										InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
	
										
										//The cell may be blank. i.e doesn't have any text tag inside it.
										if(cellTextXMLElementPtr == nil)
										{
											continue;
										}
										//Get all the elementID and languageID from the cellTextXMLElement
										//Note ITagReader plugin methods are not used.
										PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
										PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
										PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
										PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
										PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
										PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
										PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
										PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));
	
										//added by Tushar on 26/12/06
										PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
										//check whether the tag specifies ProductCopyAttributes.
										PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	         							double elementID = strElementID.GetAsDouble();
										double languageID = strLanguageID.GetAsDouble();
	
										PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
										PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
										PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
	
										double ColNO = strColNo.GetAsDouble();
										double RowNo = strRowNo.GetAsDouble();
										double typeID = strTypeID.GetAsDouble();
										double sectionid = strSectionId.GetAsDouble();
	
										double parentId = strParentID.GetAsDouble();
	
										double childId = strChildID.GetAsDouble();
										int32 header = strHeader.GetAsNumber();
										double tableID = strTableID.GetAsDouble();	
	
										
	
										if(count11!=RowNo && header!=1)
										{
											PMString a("count11 : ");
											a.AppendNumber(count11);
											a.Append("\n");
											a.Append("RowNo : ");
											a.AppendNumber(RowNo);
											a.Append("\n");
											a.Append("header : ");
											a.AppendNumber(header);
											//CA(a);
											/*CA("z++");*/
											z++;	
										}
										
										count11=RowNo;
	
										int32 tagStartPos = -1;
										int32 tagEndPos = -1;
										Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
										tagStartPos = tagStartPos + 1;
										tagEndPos = tagEndPos -1;
	
										PMString dispName("");
										
											//CA("Structure gets selected ");
											if(header==1)
											{
											 dispName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);//temp_to_test  = oAttribVal.getDisplayName();
											}else
												dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(vec_itemsNew.at(z),elementID,languageID,sectionid, kTrue);
										
										
										/*PMString a("dispName : ");
										a.Append(dispName);
										CA(a);*/
											if(header==-1)
											{
												PMString a("childId :  ");
												a.AppendNumber(vec_itemsNew.at(z));
												a.Append("\n");
												a.Append("elementID : ");
												a.AppendNumber(elementID);
												a.Append("\n");
												a.Append("dispName : ");
												a.Append(dispName);
										//CA(a);
											}
	
	
										PMString textToInsert("");
										InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
										if(!iConverter)
										{
											textToInsert=dispName;					
										}
										else
											textToInsert=iConverter->translateString(dispName);
										//Spray the Header data .We don't have to change the tag attributes
										//as we have done it while copy and paste.
										//WideString insertData(textToInsert);
										//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
                                        textToInsert.ParseForEmbeddedCharacters();
										boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
										ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1 ,insertText);
	
									}
								}//end of while
							}
						}//end of if(typeID11==6210)
						//else
						//{
						//	vec_items = oTableValue.getItemIds();
						//	vec_tableheaders = oTableValue.getTableHeader();
						//	int32 zz = static_cast<int32>(vec_tableheaders.size());
						//
						//	if(AddHeaderToTable){
						//		//CA("Before putHeaderDataInTable ");
						//		putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose,tagStruct, BoxUIDRef);
						//		i=1; j=0;
						//	}
						//	vector<vector<PMString> > vec_tablerows;

						//	vec_tablerows = oTableValue.getTableData();

						//	vector<vector<PMString> >::iterator it1;
						//	for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
						//	{
						//		//CA("row iterator");
						//		vector<PMString>::iterator it2;

						//		for(it2=(*it1).begin();it2!=(*it1).end();it2++)
						//		{
						//			//CA("col iterator");
						//			/*if(j==0)
						//			{
						//				j++;
						//				continue;
						//			}*/
						//			if((j/*-1*/)>=numCols)
						//			{
						//				break;
						//			}

						//			//PMString temp2("*it2 : ");
						//			//temp2.Append(*it2);
						//			//temp2.Append("\n");
						//			//temp2.Append("Val of i =");
						//			//temp2.AppendNumber(i);
						//			//temp2.Append("\n");
						//			//temp2.Append("Val of j =");
						//			//temp2.AppendNumber(j);
						//			//CA(temp2);

						//			setTableRowColData(tableUIDRef, (*it2), i, j/*-1*/);//original
						//			
						//			SlugStruct stencileInfo;
						//			stencileInfo.elementId = vec_tableheaders[j/*-1*/];
						//			//temp_to_test CAttributeValue oAttribVal;
						//			//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
						//			PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j/*-1*/],tagStruct.languageID );//temp_to_test  = oAttribVal.getDisplayName();
						//			
						//			stencileInfo.elementName = dispname;
						//			PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
						//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
						//			//{
						//				TableColName.Append("ATTRID_");
						//				TableColName.AppendNumber(vec_tableheaders[j/*-1*/]);
						//			//}
						//			TableColName = keepOnlyAlphaNumeric(TableColName);
						//			stencileInfo.TagName = TableColName;
						//			if(AddHeaderToTable)
						//				stencileInfo.childId  = vec_items[i-1];
						//			else
						//				stencileInfo.childId  = vec_items[i];
						//			
						//			stencileInfo.childTag =  1;
						//			stencileInfo.parentId = tagStruct.parentId;
						//			stencileInfo.parentTypeId = tagStruct.parentTypeID;
						//			stencileInfo.whichTab = 4; // Item Attributes
						//			//stencileInfo.imgFlag = tagStruct.sectionID; 
						//			stencileInfo.sectionID = tagStruct.sectionID; 
						//			stencileInfo.isAutoResize = tagStruct.isAutoResize;
						//			stencileInfo.LanguageID = tagStruct.languageID ;
						//			stencileInfo.pbObjectId = tagStruct.pbObjectId;
						//			stencileInfo.tableType = tagStruct.tableType;
						//			stencileInfo.tableId = tagStruct.tableId;

						//			XMLReference txmlref;
						//			PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
						//			if(TableTagNameNew.Contains("PSTable"))
						//			{
						//				//CA("PSTable Found");
						//			}
						//			else
						//			{				
						//				TableTagNameNew =  "PSTable";
						//				//CA("PSTable Not Found : "+ TableTagNameNew);
						//			}
						//			iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j/*-1*/, tableId );
						//			//	CA("After Tag Table");
						//			j++;
						//		}
						//		i++;
						//		j=0;
						//			//break;
						//	}//end of for
						//}
					}
					else if(iscontentbuttonselected==kTrue || Booklevelcontentvariable==kTrue)
					{
						/*if(typeID11==6210)*/
						{
							
							UID textFrameUID = kInvalidUID;
							InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
							if (graphicFrameDataOne) 
							{
								textFrameUID = graphicFrameDataOne->GetTextContentUID();
							}
							if (textFrameUID == kInvalidUID)
							{
								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
								break;//breaks the do{}while(kFalse)
							}
							
							InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
							if (graphicFrameHierarchy == nil) 
							{
								//CA("graphicFrameHierarchy is NULL");
								break;
							}
											
							InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
							if (!multiColumnItemHierarchy) {
								//CA("multiColumnItemHierarchy is NULL");
								break;
							}
	
							InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
							if (!multiColumnItemTextFrame) {
								//CA("multiColumnItemTextFrame is NULL");
								break;
							}
							InterfacePtr<IHierarchy>
							frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
							if (!frameItemHierarchy) {
								//CA("frameItemHierarchy is NULL");
								break;
							}
	
							InterfacePtr<ITextFrameColumn>
							frameItemTFC(frameItemHierarchy, UseDefaultIID());
							if (!frameItemTFC) {
								//CA("!!!ITextFrameColumn");
								break;
							}
							InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
							if (textModel == nil)
							{
								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
								break;//breaks the do{}while(kFalse)
							}
							UIDRef StoryUIDRef(::GetUIDRef(textModel));
	
							InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
							if(tableList==nil)
							{
								break;//breaks the do{}while(kFalse)
							}
	
							int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
	
							if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
							{
								break;//breaks the do{}while(kFalse)
							}
	
							for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
							{//for..tableIndex
								//CA("tableIndex");
								InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
								if(tableModel == nil)
									continue;//continues the for..tableIndex
	
								InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
								if(tableCommands==NULL)
								{
									ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
									continue;//continues the for..tableIndex
								}
										
								UIDRef tableRef(::GetUIDRef(tableModel)); 
								//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
								IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
								XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
								UIDRef ContentRef = contentRef.GetUIDRef();
								//if( ContentRef != tableRef)
								//{
								//	CA("if( ContentRef != tableRef)");
								//	//if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
								//	continue; //continues the for..tableIndex
								//}	
								//CA("outside");
	
								/*int32 childTagCnt = tagStruct.tagPtr->GetChildCount();
								PMString a("childTagCnt :  ");
								a.AppendNumber(childTagCnt);
								CA(a);*/
	
									GridArea headerArea = tableModel->GetHeaderArea();
									GridArea tableArea_new = tableModel->GetTotalArea();
									ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
									ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
	
	
									while (iterTable2 != endTable2)
									{
										GridAddress gridAddress = (*iterTable2);         
										InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
										if(cellContent == nil) 
										{
											//CA("cellContent == nil");
											break;
										}
										InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
										if(!cellXMLReferenceData) {
											//CA("!cellXMLReferenceData");
											break;
										}
										XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
										++iterTable2;
	
										
										InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
										
										//Get the text tag attached to the text inside the cell.
										//We are providing only one texttag inside cell.
										if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
										{
											continue;
										}
										int32 NoofChilds = cellXMLElementPtr->GetChildCount();
	
										PMString a("childTagCnt :  ");
										a.AppendNumber(NoofChilds);
										//CA(a);
										for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
										{ // iterate thruogh cell's tags
											XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
											//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
											InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
	
											
											//The cell may be blank. i.e doesn't have any text tag inside it.
											if(cellTextXMLElementPtr == nil)
											{
												continue;
											}
											//Get all the elementID and languageID from the cellTextXMLElement
											//Note ITagReader plugin methods are not used.
											PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
											PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
											PMString strTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("typeId"));
											PMString strIndex = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
											PMString strParentID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentID"));
											PMString strRowNo = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
											PMString strColNo = cellTextXMLElementPtr->GetAttributeValue(WideString("colno"));
											PMString strSectionId = cellTextXMLElementPtr->GetAttributeValue(WideString("sectionID"));
	
											//added by Tushar on 26/12/06
											PMString strParentTypeID = cellTextXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
											//check whether the tag specifies ProductCopyAttributes.
											PMString tableFlag =   cellTextXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	                 						double elementID = strElementID.GetAsDouble();
											double languageID = strLanguageID.GetAsDouble();
	
											PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
											PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
											PMString strTableID = cellTextXMLElementPtr->GetAttributeValue(WideString("tableId"));
	
											double ColNO = strColNo.GetAsDouble();
											double RowNo = strRowNo.GetAsDouble();
											double typeID = strTypeID.GetAsDouble();
											double sectionid = strSectionId.GetAsDouble();
	
											double parentId = strParentID.GetAsDouble();
	
											double childId = strChildID.GetAsDouble();
											int32 header = strHeader.GetAsNumber();
											double tableID = strTableID.GetAsDouble();	
	
											
											int32 tagStartPos = -1;
											int32 tagEndPos = -1;
											Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
											tagStartPos = tagStartPos + 1;
											tagEndPos = tagEndPos -1;
	
											PMString dispName("");
											
												//CA("Content gets selected ");
												if(header==1)
												{
												 dispName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementID,languageID);//temp_to_test  = oAttribVal.getDisplayName();
												}else
												 dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementID,languageID, sectionid, kTrue);
											
											PMString a("dispName : ");
											a.Append(dispName);
											//CA(a);
											
	
	
											PMString textToInsert("");
											InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
											if(!iConverter)
											{
												textToInsert=dispName;					
											}
											else
												textToInsert=iConverter->translateString(dispName);
											//Spray the Header data .We don't have to change the tag attributes
											//as we have done it while copy and paste.
											//WideString insertData(textToInsert);											
											//textModel->Replace(tagStartPos,tagEndPos-tagStartPos + 1,&insertData); //og
                                            textToInsert.ParseForEmbeddedCharacters();
											boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
											ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1 ,insertText);
					
										}
									}
								}
	
						}//end of if(typeID11==6210)
						//else
						//{	
						//	vec_items = vec_ItemIDsforcustumtable;
						//	//vec_tableheaders = vec_totalheadersofcustumtable;//vec_AttributeIdforcustumtable
						//	vec_tableheaders = vec_AttributeIdforcustumtable;
						//	int32 zz = static_cast<int32>(vec_items.size());
						//
						//	//if(AddHeaderToTable)
						//	if(totalheadersofcustumtable>0)
						//	{
						//		//CA("Before putHeaderDataInTable ");
						//		putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose,tagStruct, BoxUIDRef);
						//		i=1; j=0;
						//	}
						//	if(totalheadersofcustumtable>0)
						//		numRows=numRows-1;

						//	vector<vector<PMString> > vec_tablerows;

						//	vec_tablerows = oTableValue.getTableData();
						//	vector<vector<PMString> >::iterator it1;
						//	int32 x=0;
						//	int32 RowCount=1;
						//	//for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
						//	for(int32 m=0;m<numRows;m++)
						//	{
						//		//CA("row iterator");

						//		/*PMString cnt("Rowcount : ");
						//		cnt.AppendNumber(RowCount);
						//		cnt.Append("\n");*/
						//		PMString cnt;
						//		cnt.Append("numRows : ");
						//		cnt.AppendNumber(numRows);
						//		//CA(cnt);
						//		vector<PMString>::iterator it2;

						//		//for(it2=(*it1).begin();it2!=(*it1).end();it2++)//numRows  numCols
						//		for(int32 n=0;n<=numCols;n++)
						//		{
						//			//CA("col iterator");
						//			//if(j==0)
						//			//{
						//			//	//CA("if(j==0)");
						//			//	j++;
						//			//	continue;
						//			//}
						//			if((j/*-1*/)>=numCols)
						//			{
						//				break;
						//			}

						//			PMString str = Vec_CustumTable_String.at(x);

						//			PMString aq("str : ");
						//			aq.Append(str);
						//			//CA(aq);
						//			x++;
						//			//PMString iii("i = ");
						//			//iii.AppendNumber(i);
						//			//CA(iii);
						//			//setTableRowColData(tableUIDRef, (*it2), i, j-1);//original
						//			setTableRowColData(tableUIDRef, str, i, j/*-1*/);
						//			SlugStruct stencileInfo;
						//			stencileInfo.elementId = vec_tableheaders[j/*-1*/];
						//			//temp_to_test CAttributeValue oAttribVal;
						//			//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
						//			PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j/*-1*/],tagStruct.languageID );//temp_to_test  = oAttribVal.getDisplayName();
						//			
						//			
						//			stencileInfo.elementName = dispname;
						//			PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
						//			//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
						//			//{
						//				TableColName.Append("ATTRID_");
						//				TableColName.AppendNumber(vec_tableheaders[j/*-1*/]);
						//			//}
						//			TableColName = keepOnlyAlphaNumeric(TableColName);
						//			stencileInfo.TagName = TableColName;
						//			if(AddHeaderToTable)
						//			{
						//				//CA("if(AddHeaderToTable)");
						//				stencileInfo.childId  = vec_items[x-1];
						//			}
						//			else
						//			{
						//				//CA("inside else ");
						//				stencileInfo.childId  = vec_items[x-1];
						//			}

						//			PMString q("stencileInfo.childId : ");
						//			q.AppendNumber(stencileInfo.childId);
						//			//CA(q);
						//			
						//			stencileInfo.childTag =  1;
						//			stencileInfo.parentId = tagStruct.parentId;
						//			stencileInfo.parentTypeId = tagStruct.parentTypeID;
						//			stencileInfo.whichTab = 4; // Item Attributes
						//			//stencileInfo.imgFlag = tagStruct.sectionID; 
						//			stencileInfo.sectionID = tagStruct.sectionID; 
						//			stencileInfo.isAutoResize = tagStruct.isAutoResize;
						//			stencileInfo.LanguageID = tagStruct.languageID ;
						//			stencileInfo.pbObjectId = tagStruct.pbObjectId;
						//			stencileInfo.tableType = tagStruct.tableType;
						//			stencileInfo.tableId = tagStruct.tableId;

						//			XMLReference txmlref;
						//			//CA("Before Tag Table : " + tagStruct.tagPtr->GetTagString()+ " TagName : " + stencileInfo.TagName);
						//			PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
						//			//CA("TableTagNameNew : " + TableTagNameNew);
						//			if(TableTagNameNew.Contains("PSTable"))
						//			{
						//				//CA("PSTable Found");
						//			}
						//			else
						//			{				
						//				TableTagNameNew =  "PSTable";
						//				//CA("PSTable Not Found : "+ TableTagNameNew);
						//			}
						//			iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j/*-1*/, tableId );
						//			//	CA("After Tag Table");
						//			j++;
						//		}
						//		i++;
						//		j=0;
						//		RowCount++;
						//			//break;
						//	}
						//	
						//}
					}
				}
			
		}
		
		iTableUtlObj->SetDBTableStatus(kFalse);
	}
	while(kFalse);
	if(result)
		delete result;
}


void TableUtility::putHeaderDataInTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
//temp_to_test 	CAttributeValue oAttribVal;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){ 
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::putHeaderDataInTable::!iTableUtlObj");											
			return ;
		}

		for(int i=0;i<vec_tableheaders.size();i++)
		{			
			//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
			PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );//temp_to_test  = oAttribVal.getDisplayName();
			//CA(dispname);
			InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
			if (tableModel == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::putHeaderDataInTable::!tableModel");											
				break;
			}

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::putHeaderDataInTable::!tableCommands");											
				break;
			}
			//CA(data.GrabCString());
			PMString textToInsert("");
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=dispname;					
			}
			else
				textToInsert=iConverter->translateString(dispname);
	
			//WideString* wStr=new WideString(textToInsert);
			const WideString wStr(textToInsert);

			if(isTranspose)
			{

				tableCommands->SetCellText(wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = vec_tableheaders[i];
				//temp_to_test CAttributeValue oAttribVal;
				//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname;//temp_to_test  = oAttribVal.getDisplayName();
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				stencileInfo.elementName = dispname;
				PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
				//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				//	CA(TableColName);
				TableColName = keepOnlyAlphaNumeric(TableColName);
				stencileInfo.TagName = TableColName;
				stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = tStruct.parentId;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.LanguageID = tStruct.languageID ;
				stencileInfo.pbObjectId = tStruct.pbObjectId;
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.tableId = tStruct.tableId;
				XMLReference txmlref;	
				
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
				//CA("TableTagNameNew : " + TableTagNameNew);
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

			}
			else
			{				
				tableCommands->SetCellText(wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId =vec_tableheaders[i];
				//temp_to_test CAttributeValue oAttribVal;
				//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID ); //temp_to_test  = oAttribVal.getDisplayName();
				stencileInfo.elementName = dispname;
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
				//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				stencileInfo.TagName = TableColName;
				TableColName = keepOnlyAlphaNumeric(TableColName);
				//CA(TableColName);
				stencileInfo.typeId  = -1; //vec_items[i];
				stencileInfo.header  = 1;
				stencileInfo.parentId = tStruct.parentId;   // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.LanguageID = tStruct.languageID ;
				stencileInfo.pbObjectId = tStruct.pbObjectId;
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.tableId = tStruct.tableId;
				
				stencileInfo.row_no= tStruct.rowno;
				XMLReference txmlref;

				//CA(tStruct.tagPtr->GetTagString());
				//CA(stencileInfo.TagName);
				//CA("Before Header Tag Table : " + tStruct.tagPtr->GetTagString()+ " TagName : " + stencileInfo.TagName);
				
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
					//CA("TableTagNameNew : " + TableTagNameNew);
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";
					//CA("PSTable Not Found : "+ TableTagNameNew);
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );


			}
			//if(wStr)
			//	delete wStr;
		}
	}
	while(kFalse);
}
//Added on 9/11 By Dattatray to give refresh functionality For MedCustomTable (condition is TypeId == -111 & whichTab ==3) for ONEsource  

void TableUtility::fillDataInCMedCustomTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tStruct, const UIDRef& BoxUIDRef)
{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return;
//	}
//	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
//	if(!DataSprayerPtr)
//	{
//		ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::Pointre to DataSprayerPtr not found");//
//		return;
//	 }
//	InterfacePtr<ITableUtility> iTableUtlObj((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
//	if(!iTableUtlObj)
//	{
//		ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!iTableUtlObj");
//		 return ;
//	}
//	
//	do
//	{
//		IsDBTable = kTrue;
//		bool16 IsAutoResize = kFalse;
//		if(tStruct.isAutoResize)
//			IsAutoResize= kTrue;
//		if(tStruct.colno == -1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//
//		CMedCustomTableScreenValue* customtableInfoptr = ptrIAppFramework->GetONEsourceObjects_getMed_CustomTableScreenValue(objectId);
//		if(customtableInfoptr == NULL)
//		{
//			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!customtableInfoptr");
//			break;
//		}
//		CMedCustomTableScreenValue customtableInfo = *customtableInfoptr;//Currently it is empty ,And should be filled from DB
//		
//		//CA("Return to DataSprayer");
//		if(customtableInfo.HeaderList.size()==0)
//		{//replace v
//			break;
//		}
//
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!iSelectionManager");		
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!layoutSelectionSuite");		
//			return;
//		}
//
//		//UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		UIDList ItemList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		//selectionManager->DeselectAll(nil); // deselect every active CSB
////		layoutSelectionSuite->Select(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//CS3 Change
//		layoutSelectionSuite->SelectPageItems(ItemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
//		TableStyleUtils TabStyleObj;
//		TabStyleObj.SetTableModel(kTrue);
//		TabStyleObj.GetTableStyle();
//		TabStyleObj.getOverallTableStyle();
//
//		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		if(!txtMisSuite)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!txtMisSuite");		
//			break;
//		}
//	
//		PMRect theArea;
//		PMReal rel;
//		int32 ht;
//		int32 wt;
//
//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		if(iGeometry==nil)
//			break;
//
//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		rel = theArea.Height();
//		ht=::ToInt32(rel);
//		rel=theArea.Width();	
//		wt=::ToInt32(rel);
//
//		int32/*int64*/ numRows=0;
//		int32/*int64*/ numCols=0;
//
//		numRows =static_cast<int32>( customtableInfo.Tabledata.size());  
//		if(numRows<=0)
//		{
//			break;		
//		}
//
//		numCols = static_cast<int32>(customtableInfo.HeaderList.size());	
//		if(numCols<=0)
//		{
//			break;
//		}
//
//		/*PMString ASD(" no Rows : ");
//		ASD.AppendNumber(numRows);
//		ASD.Append("   no Clos : ");
//		ASD.AppendNumber(numCols);
//		CA(ASD);*/
//
//		bool8 isTranspose = kFalse;//Hardcoded as false for Medtronics
//		/*InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//		if(!tWrite)
//		{
//			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!tWrite");								
//			break;
//		}*/
//
//		if(!isTranspose)
//		{
//			int32 i=0, j=0;
//			resizeTable(tableUIDRef,(int32) numRows,(int32) numCols, isTranspose);
//		
//			if(AddHeaderToTable)
//			{
//				/*putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef); following function is commented by vijay on 16-10-2006*/
//
//				do
//				{
//								
//					if(ptrIAppFramework == nil)
//					{
//						//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//						break;
//					}
//					for(int k=0; k<customtableInfo.HeaderList.size(); k++)
//					{					
//						
//						InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//						if (tableModel == nil)
//						{
//							ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!tableModel");								
//							break;
//						}
//
//						InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//						if(tableCommands==nil)
//						{
//							ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!tableCommands");								
//							break;
//						}
//						
//						PMString textToInsert("");
//						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//						if(!iConverter)
//						{
//							textToInsert=customtableInfo.HeaderList[k].getdisplayString();					
//						}
//						else
//							textToInsert=iConverter->translateString(customtableInfo.HeaderList[k].getdisplayString());
//
//						//CA("Cell String : " + customtableInfo.HeaderList[k].getdisplayString());
//						WideString* wStr=new WideString(textToInsert);
//										
//						SlugStruct stencileInfo;						
//											
//						tableCommands->SetCellText(*wStr, GridAddress(0, k));
//						
//						stencileInfo.elementId =customtableInfo.HeaderList[k].getvalueAttributeId();												
//						PMString TableColName("");
//						TableColName.Append("ATTRID_");
//						TableColName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());
//				
//						stencileInfo.elementName = TableColName;
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tStruct.languageID;
//				
//						stencileInfo.typeId  = -1; 
//						stencileInfo.header  = 1;
//						stencileInfo.parentId = objectId ;  // -2 for Header Element
//						stencileInfo.parentTypeId = tStruct.parentTypeID;					
//						stencileInfo.whichTab = tStruct.whichTab;
//						stencileInfo.imgFlag = tStruct.sectionID; 
//						stencileInfo.isAutoResize = tStruct.isAutoResize;
//						stencileInfo.col_no = tStruct.colno;
//						XMLReference txmlref;
//						//CA("DataSprayer 11.........");
//						iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, 0, k, 0 );
//						if(wStr)
//							delete wStr;
//						
//					}
//				}
//				while(kFalse);
//			 i=1; j=0;
//			}
//
//			
//			int32/*int64*/ BodyRowCount = static_cast<int32> (customtableInfo.Tabledata.size());
//			PMString ASD1(" BodyRowCount : ");
//			ASD1.AppendNumber(BodyRowCount);
//			//CA(ASD1);
//			for(int32 count2 =0 ; count2 < BodyRowCount ;count2++) //new added by v
//			{	
//
//				VectorCustomItemTableRowInfo rowVector = customtableInfo.Tabledata[count2];
//				int32/*int64*/ rowVectorSize =static_cast<int32> (rowVector.size());
//			PMString ASD1(" rowVectorSize : ");
//				ASD1.AppendNumber(rowVectorSize);
//				//CA(ASD1);
//				for(int32 count1 =0 ; count1 < rowVectorSize ;count1++)//new added by v
//				{		
//					PMString ASD("row : " );
//					ASD.AppendNumber(i);
//					ASD.Append("  col : ");
//					ASD.AppendNumber(j);
//					//CA(ASD + "\n" + "Body Cell String : " + rowVector[count1].getdisplayString());
//					setTableRowColData(tableUIDRef,rowVector[count1].getdisplayString() , i, j);	
//					SlugStruct stencileInfo;
//
//					stencileInfo.elementId = rowVector[count1].getvalueAttributeId();
//				
//					PMString TableColName("");
//					TableColName.Append("ATTRID_");
//					TableColName.AppendNumber(rowVector[count1].getvalueAttributeId()/*vec_tableheaders[j-1]*/); // replaced v
//
//					stencileInfo.elementName = TableColName;
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//										
//					stencileInfo.typeId  = rowVector[count1].getItemId();
//					stencileInfo.parentId = objectId;
//					stencileInfo.parentTypeId = tStruct.parentTypeID;
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.imgFlag = tStruct.sectionID; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					XMLReference txmlref;
//					//CA(tStruct.tagPtr->GetTagString());		
//
//					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, i, j,0 );
//					j++;
//				}
//				i++;
//				j=0;
//			}
//			
//		}
//	
//		if(IsAutoResize)
//		{
//			UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
//
//			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{	ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!layoutSelectionSuite");								
//				return;
//			}
////			layoutSelectionSuite->DeselectAll();	//CS3 Change	
//			layoutSelectionSuite->DeselectAllPageItems();
//			//layoutSelectionSuite->Select(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );	//cs3 Change
//			layoutSelectionSuite->SelectPageItems(itemList, Selection::kReplace,  Selection::kDontScrollLayoutSelection  );
//			InterfacePtr<ITextMiscellanySuite> txtMisSuite1(static_cast<ITextMiscellanySuite* >
//			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//			if(!txtMisSuite1)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!txtMisSuite1");								
//				return;
//			}
//			InterfacePtr<IFrameContentSuite>frmContentSuite(txtMisSuite1,UseDefaultIID());
//			if(!frmContentSuite)
//			{
//				ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInCMedCustomTable::!frmContentSuite");								
//				return;
//			}
//			//Commeted By Rahul to avoid Text Frame resizing for Lazboy
//			UIDList processedItems;
//			K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
//			ErrorCode status =  DataSprayerPtr->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
//			return ;
//		}		
//		
//		TabStyleObj.targetTblModel = TabStyleObj.sourceTblModel;
//		TabStyleObj.ApplyTableStyle();
//		TabStyleObj.setTableStyle();
//	
//	}while(kFalse);
//	IsDBTable = kFalse;
}


void TableUtility::fillDataInKitComponentTable
(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef, bool16 isKitTable)
{
	//CA("in fillDataInKitComponentTable");
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{
		IsDBTable = kTrue;		//This metod is getting called only for DBTable.
		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
		if(tStruct.isAutoResize==1)
			IsAutoResize= 1;
		
		if(tStruct.isAutoResize==2)
			IsAutoResize= 2;

		if(tStruct.colno == -1)
			AddHeaderToTable = kTrue;
		else
			AddHeaderToTable = kFalse;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::Pointre to DataSprayerPtr not found");
			return;
		}

		PublicationNode refpNode;
		PMString PubName("PubName");
	
		refpNode.setAll(PubName, tStruct.parentId, tStruct.sectionID,3,1,1,1,tStruct.typeId,0,kFalse,kFalse);
	
		DataSprayerPtr->FillPnodeStruct(refpNode, tStruct.sectionID, 1, tStruct.sectionID);
		
		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			//CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		double objectId = ItemID; //Product object It is an objectID used for calling ApplicationFrameworks method.		

		double sectionid = -1;
		sectionid = tStruct.sectionID;
	
		double itemId = ItemID;  //vec_item_new[0]/*30017240*/;

		/*PMString ASD("itemid: ");
		ASD.AppendNumber(itemId);
		CA(ASD);*/

		VectorScreenTableInfoPtr KittableInfo = NULL;
		
		//KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemId, tStruct.languageID, isKitTable); 
		if(!KittableInfo)
		{
			//CA("KittableInfo is NULL");
			return;
		}

		if(KittableInfo->size()==0){
			//CA(" KittableInfo->size()==0");
			break;
		}

		/* Output of above method is of following format from Application framework :
		tableId = null
		typeId = null
		tableName = Kits
		transpose = false
		printHeader = true
		tableHeader = [ -805, 11000005, 11000012, 11000033]
		tableData = [[30021736, 1, , CGO60667M, Chicago Lakefront, 161.54]]
		seqList = null
		rowIdList = null
		columnIdList = null
		notesList = []
		*/

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it  ;
		it = KittableInfo->begin();
		oTableValue = *it;
			
// Added by Rahul
		//It checks the selection present or not........
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("Slection NULL");
			break;
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			//CA("!layoutSelectionSuite");
			return;
		}
		//It gets refernce to persistant objects present in the (database) document as long as document is open.
		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
		//selectionManager->DeselectAll(NULL); // deselect every active CSB
//		layoutSelectionSuite->Select(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//CS3 Change
		layoutSelectionSuite->SelectPageItems(CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			//CA("My Suit NULL");
			//break;
		}
		//These values are taken from db
		PMRect theArea;
		PMReal rel;
		int32 ht;
		int32 wt;

		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
		if(iGeometry==NULL)
			break;

		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
		rel = theArea.Height();
		ht=::ToInt32(rel);
		rel=theArea.Width();	
		wt=::ToInt32(rel);
		//----Comment removed by dattatray onc14/10 for testing
		//PMString ASD1("Width = ");
		//ASD1.AppendNumber(wt);
		//CA(ASD1);
		//----it should be commented by dattatray after testing
//////////////
		int32/*int64*/ numRows=0;
		int32/*int64*/ numCols=0;		

		numRows =static_cast<int32> (oTableValue.getTableData().size());
		if(numRows<=0)
			break; 
		numCols =static_cast<int32>( oTableValue.getTableHeader().size() ); 
		if(numCols<=0)
			break;
		bool8 isTranspose = oTableValue.getTranspose();
		
	//	isTranspose = kTrue;
		/*InterfacePtr<ITagWriter> tWrite
		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
		if(!tWrite)
			break;*/

		if(isTranspose)
		{
			//CA("Inside isTranspose");
			int32 i=0, j=0;
			resizeTable(tableUIDRef, numCols, numRows, isTranspose);
		
			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
	
			if(AddHeaderToTable){
			putHeaderDataInKitComponentTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef, ItemID);
			 i=1; j=0;
			}

			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vec_items = oTableValue.getItemIds();

			//vector<int32>::iterator temp;
			/*	for(int i1=0;i1<vec_items.size();i1++)
				{
					PMString AS("vec Item : ");
					AS.AppendNumber(vec_items[i1]);
					CA(AS);
				}*/
			
			vector<vector<PMString> >::iterator it1;
			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{								
					setTableRowColData(tableUIDRef, (*it2), j, i);	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
				//	CAttributeValue oAttribVal;
				//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
				//	PMString dispname = oAttribVal.getDisplayName();
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					PMString TableColName("");// = oAttribVal.getTable_col();
					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//	{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
				//	}
					//CA(TableColName);
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					if(AddHeaderToTable)
						stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.typeId  = vec_items[i];
					stencileInfo.parentId = ItemID;
					stencileInfo.parentTypeId = tStruct.parentTypeID;
					//4Aug..ItemTable
					//stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = tStruct.sectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.LanguageID = tStruct.languageID;

					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
	//CA(tStruct.tagPtr->GetTagString());		

					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";						
					}

					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i,tableId );
					j++;
					
					if(it2==(*it1).begin())
					{
						it2++; // This is to avoid Availabilty data spray which is in second position og it2
						//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
					}

				}
				i++;
				j=0;
			}
			//tWrite->SetSlug(tableUIDRef,vec_tableheaders,vec_items,isTranspose);
		}
		else
		{
			//CA("Inside !isTranspose");
			int32 i=0, j=0;
			
			//resizeTable gets called if Auto Re-Size is selected..
			resizeTable(tableUIDRef, numRows, numCols, isTranspose);

			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
			if(AddHeaderToTable){
			putHeaderDataInKitComponentTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef, ItemID);
			i=1; j=0;
			}

			vec_items = oTableValue.getItemIds();

			//vector<int32>::iterator temp;
			/*for(int i1=0;i1<vec_items.size();i1++)
			{
				PMString AS;
				AS.AppendNumber(vec_items[i1]);
				CA(AS);
			}*/
			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
				
					setTableRowColData(tableUIDRef, (*it2), i, j);
					//CA((*it2));
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					/*CAttributeValue oAttribVal;
					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname = oAttribVal.getDisplayName();*/
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j/*-1*/],tStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					//CA(dispname);
					PMString TableColName("");  // = oAttribVal.getTable_col();
					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j/*-1*/]));
					/*}*/
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					if(AddHeaderToTable)
					stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.typeId  = vec_items[i];
					stencileInfo.parentId = ItemID;
					stencileInfo.parentTypeId = tStruct.parentTypeID;
					//4Aug..ItemTable
					//stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = tStruct.sectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
					//CA("Before Tag Table");
					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";						
					}
					//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j/*-1*/, tableId );
					//CA("After Tag Table ...2");
					
					if(it2==(*it1).begin())
					{
						it2++; // This is to avoid Availabilty data spray which is in second position og it2
						//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
					}

				j++;
				}
				i++;
				j=0;
			}
			i=0;j=0;			
		}
		
		if(IsAutoResize == 1)
		{
			//CA("Inside IsAutoResize");
			//UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());			
			//UIDList processedItems;
			//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));			
			//ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);

			InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			ASSERT(fitFrameToContentCmd != nil);
			if (fitFrameToContentCmd == nil) {
			return ;
			}
			fitFrameToContentCmd->SetItemList(UIDList(BoxUIDRef));
			if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
			ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
			return ;
			}
			
			return ;
		}

		//if(IsAutoResize == 2)
		//{
		//	//CA("Inside overflow");
		//	ThreadTextFrame ThreadObj;
		//	ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
		//}

	//CA("End of Kit Table Spray");
		
	}while(kFalse);
	IsDBTable = kFalse;

}


void TableUtility::putHeaderDataInKitComponentTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInKitComponentTable");
	do
	{
		//HeadersizeVector.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			// CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
		//	CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		int i=0;

		//Adding Quantity & Avaibiablity 
		
		if(isTranspose)
		{
			//WideString* wStr=new WideString("Quantity");
			const WideString wStr("Quantity");

			tableCommands->SetCellText( wStr, GridAddress(i, 0));
			SlugStruct stencileInfo;
			stencileInfo.elementId = -805;		
			stencileInfo.TagName = "Quantity";
			stencileInfo.LanguageID = tStruct.languageID;
			stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID;  // -2 for Header Element
			stencileInfo.parentTypeId = tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;		

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );
			//delete wStr;
		}
		else
		{			
			//WideString* wStr=new WideString("Quantity");
			const WideString wStr("Quantity");
			tableCommands->SetCellText(wStr, GridAddress(0, i));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -805;		
			stencileInfo.TagName = "Quantity";
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId = tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
			//delete wStr;
		}
		i++;
		// at first place we have -805 for Quantity so take the Attributeids from 2nd place onwards
		for(i=1;i<vec_tableheaders.size();i++)
		{
			//oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
			//PMString dispname = oAttribVal.getDisplayName();

			PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
			
			//WideString* wStr=new WideString(dispname);
			const WideString wStr(dispname);
			//CA(dispname);
			int32 StringLength = dispname.NumUTF16TextChars();
			//HeadersizeVector.push_back(StringLength);

			if(isTranspose)
			{
				tableCommands->SetCellText(wStr, GridAddress(i , 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = vec_tableheaders[i];
				/*CAttributeValue oAttribVal;
				oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname = oAttribVal.getDisplayName();*/
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				stencileInfo.elementName = dispname;

				PMString TableColName("");//oAttribVal.getTable_col();
				//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				//	CA(TableColName);
				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;				

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

			}
			else
			{
				/*PMString ASD("vec_tableheaders[i] : ");
				ASD.AppendNumber(vec_tableheaders[i]);
				CA(ASD);*/

				tableCommands->SetCellText(wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId =vec_tableheaders[i];
				/*CAttributeValue oAttribVal;
				oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname = oAttribVal.getDisplayName();*/
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
				stencileInfo.elementName = dispname;
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				PMString TableColName("");// oAttribVal.getTable_col();
				//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID =tStruct.languageID;
				//CA(TableColName);
				stencileInfo.typeId  = -1; //vec_items[i];
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID ;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;

				//CA(tStruct.tagPtr->GetTagString());
				//CA(stencileInfo.colName);
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
			//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
			//	CA("After Tag Table");
			}
			//delete wStr;
		}

		
		//if(isTranspose)
		//{
		//	WideString* wStr=new WideString("Availablity");
		//	tableCommands->SetCellText(*wStr, GridAddress(i, 0));
		//	SlugStruct stencileInfo;
		//	stencileInfo.elementId = -402;		
		//	stencileInfo.TagName = "Availablity";
		//	stencileInfo.LanguageID = tStruct.languageID;
		//	stencileInfo.typeId  = -2;  // TypeId is hardcodded to -2 for Header Elements.
		//	stencileInfo.parentId = pNode.getPubId();  // -2 for Header Element
		//	stencileInfo.parentTypeId = pNode.getTypeId();
		//	//4Aug..ItemTable
		//	//stencileInfo.whichTab = 4; // Item Attributes
		//	stencileInfo.whichTab = tStruct.whichTab;
		//	stencileInfo.imgFlag = CurrentSectionID; 
		//	stencileInfo.isAutoResize = tStruct.isAutoResize;
		//	stencileInfo.col_no = tStruct.colno;
		//	XMLReference txmlref;	
		//	
		//	PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
		//	if(TableTagNameNew.Contains("PSTable"))
		//	{
		//		//CA("PSTable Found");
		//	}
		//	else
		//	{				
		//		TableTagNameNew =  "PSTable";						
		//	}

		//	this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

		//}
		//else
		//{			
		//	WideString* wStr=new WideString("Quantity");
		//	tableCommands->SetCellText(*wStr, GridAddress(0, i));
		//	SlugStruct stencileInfo;	
		//	stencileInfo.elementId = -401;		
		//	stencileInfo.TagName = "Quantity";
		//	stencileInfo.LanguageID =tStruct.languageID;	
		//	stencileInfo.typeId  = -2; //vec_items[i];
		//	stencileInfo.parentId = pNode.getPubId() ;  // -2 for Header Element
		//	stencileInfo.parentTypeId = pNode.getTypeId();
		//	//4Aug..ItemTable
		//	//stencileInfo.whichTab = 4; // Item Attributes
		//	stencileInfo.whichTab = tStruct.whichTab;
		//	stencileInfo.imgFlag = CurrentSectionID; 
		//	stencileInfo.isAutoResize = tStruct.isAutoResize;
		//	stencileInfo.col_no = tStruct.colno;
		//	XMLReference txmlref;			

		//	PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
		//	if(TableTagNameNew.Contains("PSTable"))
		//	{
		//		//CA("PSTable Found");
		//	}
		//	else
		//	{				
		//		TableTagNameNew =  "PSTable";						
		//	}
		//	this->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
		//}

	}
	while(kFalse);
}


ErrorCode TableUtility::ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)
{
	ErrorCode status = kFailure;
	do
	{
		if (commandClass == kInvalidClass)
		{
			ASSERT_FAIL("ProcessSimpleCommand: commandClass is invalid"); break;
		}
		InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(commandClass));
		if (cmd == NULL)
		{
			ASSERT(cmd); break;
		}

		cmd->SetItemList(itemsIn);
		status = CmdUtils::ProcessCommand(cmd);

		if (status == kSuccess)
		{
			const UIDList& local_itemsOut = cmd->GetItemListReference();
			itemsOut = local_itemsOut;
		}
		else
		{
			ASSERT_FAIL("ProcessSimpleCommand: The command failed");
		}
	} while (false);
	return status;
}



void TableUtility::fillDataInXRefTable
(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	//CA("in fillDataInKitComponentTable");
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{
		IsDBTable = kTrue;		//This metod is getting called only for DBTable.
		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
		if(tStruct.isAutoResize==1)
			IsAutoResize= 1;
		
		if(tStruct.isAutoResize==2)
			IsAutoResize= 2;

		if(tStruct.colno == -1)
			AddHeaderToTable = kTrue;
		else
			AddHeaderToTable = kFalse;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::Pointre to DataSprayerPtr not found");
			return;
		}

		PublicationNode refpNode;
		PMString PubName("PubName");
	
		refpNode.setAll(PubName, tStruct.parentId, tStruct.sectionID,3,1,1,1,tStruct.typeId,0,kFalse,kFalse);
	
		DataSprayerPtr->FillPnodeStruct(refpNode, tStruct.sectionID, 1, tStruct.sectionID);
		
		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			//CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		double objectId = ItemID; //Product object It is an objectID used for calling ApplicationFrameworks method.		

		double sectionid = -1;
		sectionid = tStruct.sectionID;
	
		double itemId = ItemID;  //vec_item_new[0]/*30017240*/;

		/*PMString ASD("itemid: ");
		ASD.AppendNumber(itemId);
		CA(ASD);*/

		VectorScreenTableInfoPtr XReftableInfo = NULL;
		
		//XReftableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemId, tStruct.languageID); 
		if(!XReftableInfo)
		{
			//CA("XReftableInfo is NULL");
			return;
		}

		if(XReftableInfo->size()==0){
			//CA(" XReftableInfo->size()==0");
			break;
		}

		/* Output of above method is of following format from Application framework :
		// Output of above method is of following format from Application framework :
		com.apsiva.servletframework.model.ObjectTableScreenValue@fcfa52
		tableId = null
		typeId = null
		tableName = XRef
		transpose = false
		printHeader = true
		tableHeader = [Item Id, -807, -808, -809, -810, -811, 11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
		tableData =[[ Competitor Reference, 5, false, , false, Test, DSA, 12023, , , ], 
			[ Competitor Reference, 1, false, , false, New Manufacturer, 999, 90op, , , ], 
			[ Manufacturer Reference, 5, false, , true, New Manufacturer, 289, Parquetry  Super Set, , 26.00, 18.95], 
			[ Manufacturer Reference, 7, false, ed, false, New Manufacturer, 334, drt, , , ]]

		seqList = null
		rowIdList = null
		columnIdList = null
		notesList = []

		*/

		

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it  ;
		it = XReftableInfo->begin();
		oTableValue = *it;
			
// Added by Rahul
		//It checks the selection present or not........
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("Slection NULL");
			break;
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			//CA("!layoutSelectionSuite");
			return;
		}
		//It gets refernce to persistant objects present in the (database) document as long as document is open.
		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
		//selectionManager->DeselectAll(NULL); // deselect every active CSB
		layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			//CA("My Suit NULL");
			//break;
		}
		//These values are taken from db
		PMRect theArea;
		PMReal rel;
		int32 ht;
		int32 wt;

		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
		if(iGeometry==NULL)
			break;

		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
		rel = theArea.Height();
		ht=::ToInt32(rel);
		rel=theArea.Width();	
		wt=::ToInt32(rel);
		//----Comment removed by dattatray onc14/10 for testing
		//PMString ASD1("Width = ");
		//ASD1.AppendNumber(wt);
		//CA(ASD1);
		//----it should be commented by dattatray after testing
//////////////
		int32/*int64*/ numRows=0;
		int32/*int64*/ numCols=0;		

		numRows = static_cast<int32>(oTableValue.getTableData().size());
		if(numRows<=0)
			break; 
		numCols =static_cast<int32> (oTableValue.getTableHeader().size()) ; 
		if(numCols<=0)
			break;
		bool8 isTranspose = oTableValue.getTranspose();
		
	//	isTranspose = kTrue;
		/*InterfacePtr<ITagWriter> tWrite
		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
		if(!tWrite)
			break;*/

	//	if(isTranspose)
	//	{
	//		//CA("Inside isTranspose");
	//		int32 i=0, j=0;
	//		resizeTable(tableUIDRef, numCols, numRows, isTranspose);
	//	
	//		vector<int32> vec_tableheaders;
	//		vector<int32> vec_items;

	//		vec_tableheaders = oTableValue.getTableHeader();
	//
	//		if(AddHeaderToTable){
	//		putHeaderDataInKitComponentTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef, ItemID);
	//		 i=1; j=0;
	//		}

	//		vector<vector<PMString> > vec_tablerows;
	//		vec_tablerows = oTableValue.getTableData();
	//		vec_items = oTableValue.getItemIds();

	//		//vector<int32>::iterator temp;
	//		/*	for(int i1=0;i1<vec_items.size();i1++)
	//			{
	//				PMString AS("vec Item : ");
	//				AS.AppendNumber(vec_items[i1]);
	//				CA(AS);
	//			}*/
	//		
	//		vector<vector<PMString> >::iterator it1;
	//		for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
	//		{
	//			vector<PMString>::iterator it2;
	//			for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//			{								
	//				setTableRowColData(tableUIDRef, (*it2), j, i);	
	//				SlugStruct stencileInfo;
	//				stencileInfo.elementId = vec_tableheaders[j];
	//			//	CAttributeValue oAttribVal;
	//			//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
	//			//	PMString dispname = oAttribVal.getDisplayName();
	//				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
	//				stencileInfo.elementName = dispname;
	//				PMString TableColName("");// = oAttribVal.getTable_col();
	//				//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
	//			//	{
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(vec_tableheaders[j]);
	//			//	}
	//				//CA(TableColName);
	//				stencileInfo.TagName = TableColName;
	//				stencileInfo.LanguageID =tStruct.languageID;
	//				if(AddHeaderToTable)
	//					stencileInfo.typeId  = vec_items[i-1];
	//				else
	//					stencileInfo.typeId  = vec_items[i];
	//				stencileInfo.parentId = ItemID;
	//				stencileInfo.parentTypeId = tStruct.parentTypeID;
	//				//4Aug..ItemTable
	//				//stencileInfo.whichTab = 4; // Item Attributes
	//				stencileInfo.whichTab = tStruct.whichTab;
	//				stencileInfo.imgFlag = tStruct.sectionID; 
	//				stencileInfo.isAutoResize = tStruct.isAutoResize;
	//				stencileInfo.LanguageID = tStruct.languageID;

	//				stencileInfo.col_no = tStruct.colno;
	//				stencileInfo.tableTypeId = tStruct.typeId;
	//				XMLReference txmlref;
	////CA(tStruct.tagPtr->GetTagString());		

	//				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
	//				if(TableTagNameNew.Contains("PSTable"))
	//				{
	//					//CA("PSTable Found");
	//				}
	//				else
	//				{				
	//					TableTagNameNew =  "PSTable";						
	//				}

	//				iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i,tableId );
	//				j++;
	//				
	//				if(it2==(*it1).begin())
	//				{
	//					it2++; // This is to avoid Availabilty data spray which is in second position og it2
	//					//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
	//				}

	//			}
	//			i++;
	//			j=0;
	//		}
	//		//tWrite->SetSlug(tableUIDRef,vec_tableheaders,vec_items,isTranspose);
	//	}
	//	else
		{
			//CA("Inside !isTranspose");
			int32 i=0, j=0;
			
			//resizeTable gets called if Auto Re-Size is selected..
			resizeTable(tableUIDRef, numRows, numCols, isTranspose);

			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
			if(AddHeaderToTable){
			putHeaderDataInXRefTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef, ItemID);
			i=1; j=0;
			}

			vec_items = oTableValue.getItemIds();

			//vector<int32>::iterator temp;
			/*for(int i1=0;i1<vec_items.size();i1++)
			{
				PMString AS;
				AS.AppendNumber(vec_items[i1]);
				CA(AS);
			}*/
			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				j=5;
				vector<PMString>::iterator it2;
				
				for(it2=(*it1).begin(); it2!=(*it1).end(); it2++)
				{
					if(it2==(*it1).begin())
					{
						it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
						it2++;
						it2++;
						it2++;
						it2++;					
					}
					//CA((*it2));
					setTableRowColData(tableUIDRef, (*it2), i, (j-5));
					
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
					/*CAttributeValue oAttribVal;
					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname = oAttribVal.getDisplayName();*/
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					//CA(dispname);
					PMString TableColName("");  // = oAttribVal.getTable_col();
					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
					/*}*/
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					if(AddHeaderToTable)
					stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.typeId  = vec_items[i];

					stencileInfo.parentId = ItemID;
					stencileInfo.parentTypeId = tStruct.parentTypeID;
					//4Aug..ItemTable
					//stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = tStruct.sectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
					//CA("Before Tag Table");
					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";						
					}
					//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, (j-5), tableId );
					//CA("After Tag Table ...2");				

					j++;
				}

				int32 k=0;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					if(k >= 5)
						break;
					setTableRowColData(tableUIDRef, (*it2), i, (j-5));
					//CA((*it2));
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[k];
					/*CAttributeValue oAttribVal;
					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname = oAttribVal.getDisplayName();*/
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[k],tStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					//CA(dispname);
					PMString TableColName("");  // = oAttribVal.getTable_col();
					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[k]));
					/*}*/
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					if(AddHeaderToTable)
					stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.typeId  = vec_items[i];

					stencileInfo.parentId = ItemID;
					stencileInfo.parentTypeId = tStruct.parentTypeID;
					//4Aug..ItemTable
					//stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = tStruct.sectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
					//CA("Before Tag Table");
					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";						
					}
					//CA("Before tagTable  2 : " + tStruct.tagPtr->GetTagString() + "  TableTagNameNew : " + TableTagNameNew);
					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, (j-5), tableId );
					//CA("After Tag Table ...2");
					j++;
					k++;
					
				}	
				i++;
				j=0;
			}
			i=0;j=0;
		}
		
		if(IsAutoResize == 1)
		{
			//CA("Inside IsAutoResize");
			//UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
			//UIDList processedItems;
			//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));
			//ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);

			InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			ASSERT(fitFrameToContentCmd != nil);
			if (fitFrameToContentCmd == nil) {
			return ;
			}
			fitFrameToContentCmd->SetItemList(UIDList(BoxUIDRef));
			if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
			ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
			return ;
			}
			
			return ;
		}

		//if(IsAutoResize == 2)
		//{
		//	//CA("Inside overflow");
		//	ThreadTextFrame ThreadObj;
		//	ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
		//}

	//CA("End of Kit Table Spray");
		
	}while(kFalse);
	IsDBTable = kFalse;

}


void TableUtility::putHeaderDataInXRefTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInKitComponentTable");
	do
	{
		//HeadersizeVector.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			// CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
		//	CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		int i=0;

		for(i=5;i<vec_tableheaders.size();i++)
		{
			/*PMString ASD(" First Value of i : ");
			ASD.AppendNumber(i);
			CA(ASD);*/
			PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
			
			//WideString* wStr=new WideString(dispname);
			const WideString wStr(dispname);
			
			int32 StringLength = dispname.NumUTF16TextChars();
			//HeadersizeVector.push_back(StringLength);
		
			/*PMString ASD("vec_tableheaders[i] : ");
			ASD.AppendNumber(vec_tableheaders[i]);
			CA(ASD);*/

			tableCommands->SetCellText(wStr, GridAddress(0, (i- 5)));
			SlugStruct stencileInfo;
			stencileInfo.elementId =vec_tableheaders[i];			
			stencileInfo.elementName = dispname;
			//CA("Inside PutHeaderDataIN Table");	CA(dispname);
			PMString TableColName("");// oAttribVal.getTable_col();
			
			TableColName.Append("ATTRID_");
			TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
			
			stencileInfo.TagName = TableColName;
			stencileInfo.LanguageID =tStruct.languageID;
			//CA(TableColName);
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId = tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;

			//CA(tStruct.tagPtr->GetTagString());
			//CA(stencileInfo.colName);
			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5) , 0 );
			//	CA("After Tag Table");			
			//delete wStr;
		}
		
		{   // For Cross-reference Type Start
			/*PMString ASD(" Value of (i-5) : ");
			ASD.AppendNumber((i-5));
			CA(ASD);*/
			//WideString* wStr=new WideString("Cross-reference Type");
			const WideString wStr("Cross-reference Type");
			tableCommands->SetCellText(wStr, GridAddress(0, (i-5)));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -807;		
			stencileInfo.TagName = "Cross-reference Type";
			stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId = tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
			//delete wStr;			
			i++;
		}   // // For Cross Referance Type End
		
		{   // For Rating Start
			//WideString* wStr=new WideString("Rating");
			const WideString wStr("Rating");
			tableCommands->SetCellText(wStr, GridAddress(0, (i-5)));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -808;		
			stencileInfo.TagName = "Rating";
			stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId =  tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
			//delete wStr;			
			i++;
		}   // For Rating End

		{   // For Alternate Start
			//WideString* wStr=new WideString("Alternate");
			const WideString wStr("Alternate");
			tableCommands->SetCellText(wStr, GridAddress(0, (i-5)));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -809;		
			stencileInfo.TagName = "Alternate";
			stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId =  tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
			//delete wStr;			
			i++;
		}   // For Alternate End

		{   // For Comments Start
			//WideString* wStr=new WideString("Comments");
			const WideString wStr("Comments");
			tableCommands->SetCellText(wStr, GridAddress(0, (i-5)));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -810;		
			stencileInfo.TagName = "Comments";
			stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId =  tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0,(i-5), 0 );
			//delete wStr;			
			i++;
		}   // For Alternate End

		{   // For Catalog Start
			//WideString* wStr=new WideString("");   //// Catalog HEADER IS Blank.
			const WideString wStr("");
			tableCommands->SetCellText(wStr, GridAddress(0,(i-5)));
			SlugStruct stencileInfo;	
			stencileInfo.elementId = -810;		
			stencileInfo.TagName = "Catalog";
			stencileInfo.TagName = keepOnlyAlphaNumeric(stencileInfo.TagName );
			stencileInfo.LanguageID =tStruct.languageID;	
			stencileInfo.typeId  = -1; //vec_items[i];
			stencileInfo.header  = 1;
			stencileInfo.parentId = ItemID ;  // -2 for Header Element
			stencileInfo.parentTypeId =  tStruct.parentTypeID;
			//4Aug..ItemTable
			//stencileInfo.whichTab = 4; // Item Attributes
			stencileInfo.whichTab = tStruct.whichTab;
			stencileInfo.imgFlag = tStruct.sectionID; 
			stencileInfo.isAutoResize = tStruct.isAutoResize;
			stencileInfo.col_no = tStruct.colno;
			stencileInfo.tableTypeId = tStruct.typeId;
			XMLReference txmlref;			

			PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
			if(TableTagNameNew.Contains("PSTable"))
			{
				//CA("PSTable Found");
			}
			else
			{				
				TableTagNameNew =  "PSTable";						
			}
			iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, (i-5), 0 );
			//delete wStr;			
			i++;
		}   // For Catalog End
		
		

	}
	while(kFalse);
}

void TableUtility::fillDataInAccessoryTable(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef)
{
	//CA("Inside TableUtility::fillDataInAccessoryTable");
	UIDRef boxUIDRef = BoxUIDRef;
	do
	{

		IsDBTable = kTrue;		//This metod is getting called only for DBTable.
		int32 IsAutoResize = 0; // Selected table get expanded or resized at runtime depends upon content.
		if(tStruct.isAutoResize==1)
			IsAutoResize= 1;
		
		if(tStruct.isAutoResize==2)
			IsAutoResize= 2;

		if(tStruct.colno == -1)
			AddHeaderToTable = kTrue;
		else
			AddHeaderToTable = kFalse;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInAccessoryTable::Pointre to DataSprayerPtr not found");
			return;
		}

		PublicationNode refpNode;
		PMString PubName("PubName");
	
		refpNode.setAll(PubName, tStruct.parentId, tStruct.sectionID,3,1,1,1,tStruct.typeId,0,kFalse,kFalse);
	
		DataSprayerPtr->FillPnodeStruct(refpNode, tStruct.sectionID, 1, tStruct.sectionID);
		
		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			//CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInAccessoryTable::!iTableUtlObj");
			return ;
		}




		double objectId = ItemID; //Product object It is an objectID used for calling ApplicationFrameworks method.

		double sectionid = -1;
		sectionid = tStruct.sectionID;
	
		double itemId = ItemID; //vec_item_new[0]/*30017240*/;

		/*PMString ASD("itemid: ");
		ASD.AppendNumber(itemId);
		CA(ASD);*/

		VectorScreenTableInfoPtr AccessorytableInfo = NULL;
			
		//AccessorytableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemId, tStruct.languageID); 
		if(!AccessorytableInfo)
		{
			//CA("AccessorytableInfo is NULL");
			return;
		}

		if(AccessorytableInfo->size()==0){
			//CA(" AccessorytableInfo->size()==0");
			break;
		}

		/*PMString AccessorytableInfoSize = "";
		AccessorytableInfoSize.AppendNumber(AccessorytableInfo->size());
		CA("AccessorytableInfoSize = " + AccessorytableInfoSize);*/

		/* Output of above method is of following format from Application framework :
			com.apsiva.servletframework.model.ObjectTableScreenValue@fcfa52
			tableId = null
			typeId = null
			tableName = Accessory
			transpose = false
			printHeader = true
			tableHeader = [Item Id, Quantity, Required, Comments, 11000005, 11000012, 11000020, 11000033, 11000043]
			tableData =[[30021729, 1, False, , CGO60610P, Wall Poster, 24" x 36", 37.88, 31.99], 
						[30021730, 1, false, , CGO60610B, Desk Blotter, 18" x 24", 18.88, 12.99],
						[30021731, 1, false, , CGO60610A, Album Photo, 5" x 7", 4.88, 2.99], 
						[30021732, 1, false, , CGO60604M, Chicago from the North, 7' x 10', 161.81, 149.99], 
						[30021733, 1, false, , CGO60604P, Wall Poster, 24" x 36", 37.32, 31.99],
						[30021734, 1, false, , CGO60604B, Large Desk Blotter, 20" x 26", 18.45, 11.99], 
						[30021736, 1, false, , CGO60667M, Chicago Lakefront, 7' x 10', 161.54, 149.99], 
						[30021737, 1, false, , CGO60667S, Back-Lit Sign, 30" x 48", 137.37, 131.99], 
						[30021738, 1, false, , CGO60667P, Wall Poster, 24" x 36", 37.88, 32.99]]


			seqList = null
			rowIdList = null
			columnIdList = null
			notesList = []

			*/

			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;
			it = AccessorytableInfo->begin();
			oTableValue = *it;

			//It checks the selection present or not........
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
			if(!iSelectionManager)
			{	//CA("Slection NULL");
				break;
			}

			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
			if (!layoutSelectionSuite) {
				//CA("!layoutSelectionSuite");
				return;
			}

			//It gets refernce to persistant objects present in the (database) document as long as document is open.
			UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
			//selectionManager->DeselectAll(NULL); // deselect every active CSB
			layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
			
			InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
			if(!txtMisSuite)
			{
				CA("My Suit NULL");
				//break;
			}
			//These values are taken from db
			PMRect theArea;
			PMReal rel;
			int32 ht;
			int32 wt;

			InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
			if(iGeometry==NULL)
				break;

			theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
			rel = theArea.Height();
			ht=::ToInt32(rel);
			rel=theArea.Width();	
			wt=::ToInt32(rel);
					
			int32 numRows=0;
			int32 numCols=0;		

			numRows = (int32)oTableValue.getTableData().size();
			if(numRows<=0)
				break; 
			numCols = static_cast<int32>(oTableValue.getTableHeader().size()) ; 
			if(numCols<=0)
				break;
			bool8 isTranspose = oTableValue.getTranspose();
			//For testing by dattatray on 14/10
			/*if(isTranspose == kTrue)
				CA("isTranspose is kTrue");*/
			PMString numRow("numRows::");
			numRow.AppendNumber(PMReal(numRows));
			//CA(numRow);
			PMString numCol("numcols::");
			numCol.AppendNumber(numCols);
			//CA(numCol);
			
		//	isTranspose = kTrue;
			/*InterfacePtr<ITagWriter> tWrite
			((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
			if(!tWrite)
				break;*/

			//TableFlagForStandardProductTable = 0;  // Since We are spraying Item Table
			
			//CA("Inside !isTranspose");
			/*PMString AS(" tStruct.whichtab : ");
			AS.AppendNumber(tStruct.whichTab);
			CA(AS);*/
			int32 i=0, j=0;
			
			//resizeTable gets called if Auto Re-Size is selected..
			resizeTable(tableUIDRef, numRows, numCols, isTranspose);

			vector<double> vec_tableheaders;
			vector<double> vec_items;

			vec_tableheaders = oTableValue.getTableHeader();
			if(AddHeaderToTable)
			{
				putHeaderDataInAccessoryTable(tableUIDRef, vec_tableheaders, isTranspose, tStruct, BoxUIDRef , ItemID);
				i=1; j=0;
			}

			vec_items = oTableValue.getItemIds();
			//vector<int32>::iterator temp;
			/*for(int i1=0;i1<vec_items.size();i1++)
			{
				PMString AS;
				AS.AppendNumber(vec_items[i1]);
				CA(AS);
			}*/
			vector<vector<PMString> > vec_tablerows;
			vec_tablerows = oTableValue.getTableData();
			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				vector<PMString>::iterator it2;
				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{	
                    setTableRowColData(tableUIDRef, (*it2), i,j);	
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j];
				//	CAttributeValue oAttribVal;
				//	oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
				//	PMString dispname = oAttribVal.getDisplayName();
					PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					PMString TableColName("");// = oAttribVal.getTable_col();
					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//	{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
				//	}
					//CA(TableColName);
					stencileInfo.TagName = TableColName;
					stencileInfo.LanguageID =tStruct.languageID;
					if(AddHeaderToTable)
						stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.typeId  = vec_items[i];
					stencileInfo.parentId = ItemID;
					stencileInfo.parentTypeId = tStruct.parentTypeID;
					//4Aug..ItemTable
					//stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.whichTab = tStruct.whichTab;
					stencileInfo.imgFlag = tStruct.sectionID; 
					stencileInfo.isAutoResize = tStruct.isAutoResize;
					stencileInfo.col_no = tStruct.colno;
					stencileInfo.tableTypeId = tStruct.typeId;
					XMLReference txmlref;
	//CA(tStruct.tagPtr->GetTagString());		

					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
					if(TableTagNameNew.Contains("PSTable"))
					{
						//CA("PSTable Found");
					}
					else
					{				
						TableTagNameNew =  "PSTable";						
					}

					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
					j++;
					
					//if(it2==(*it1).begin())
					//{
					//	it2++; // This is to avoid Availabilty data spray which is in second position og it2
					//	//tableData = [[1, , CGO60667M, Chicago Lakefront, 161.54]]
					//}
				}
				i++;
				j=0;
			}

			if(IsAutoResize == 1)
		{
			//CA("Inside IsAutoResize");
			//UIDList itemList(BoxUIDRef.GetDataBase(),BoxUIDRef.GetUID());
			//UIDList processedItems;
			//K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(itemList));			
			//ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
			InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			ASSERT(fitFrameToContentCmd != nil);
			if (fitFrameToContentCmd == nil) {
			return ;
			}
			fitFrameToContentCmd->SetItemList(UIDList(BoxUIDRef));
			if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
			ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
			return ;
			}

			return ;
		}

	}while(kFalse);
	IsDBTable = kFalse;

}

void TableUtility::putHeaderDataInAccessoryTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID)
{
	//CAttributeValue oAttribVal;
	//CA("Inside putHeaderDataInAccessoryTable");
	do
	{
		//HeadersizeVector.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			// CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::putHeaderDataInAccessoryTable::!iTableUtlObj");
			return ;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
		//	CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		int i=0;
		
		//Adding Quantity , Required , Comments
		if(isTranspose)
		{
			//Quantity
			{
				//WideString* wStr=new WideString("Quantity");
				const WideString wStr("Quantity");
				tableCommands->SetCellText( wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -812;		
				stencileInfo.TagName = "Quantity";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0, 0 );

				//delete wStr;
			}
			i++;
			
			//Required
			{
				WideString* wStr=new WideString("Required");
				tableCommands->SetCellText( *wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -813;		
				stencileInfo.TagName = "Required";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0,0 );

				delete wStr;
			}

			i++;

			//Comments
			{
				WideString* wStr=new WideString("Comments");
				tableCommands->SetCellText( *wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -814;		
				stencileInfo.TagName = "Comments";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, i,0, 0 );

				delete wStr;
			}
		}
		else
		{
			//Quantity
			{
				WideString* wStr=new WideString("Quantity");
				tableCommands->SetCellText( *wStr, GridAddress(0,i));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -812;		
				stencileInfo.TagName = "Quantity";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, 0,i, 0 );

				delete wStr;
			}
			i++;
			
			//Required
			{
				WideString* wStr=new WideString("Required");
				tableCommands->SetCellText( *wStr, GridAddress(0,i));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -813;		
				stencileInfo.TagName = "Required";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, 0 ,i ,0 );

				delete wStr;
			}

			i++;

			//Comments
			{
				WideString* wStr=new WideString("Comments");
				tableCommands->SetCellText( *wStr, GridAddress(0,i));
				SlugStruct stencileInfo;
				stencileInfo.elementId = -814;		
				stencileInfo.TagName = "Comments";
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;		

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo,0, i, 0 );

				delete wStr;
			}

		}
		i++;
		// at first place we have -812 for Quantity 
		// at second place -813 for Required
		// and at third place -814 for Comments
		//so take the Attributeids from 3nd place onwards
		for(;i<vec_tableheaders.size();i++)
		{
			//oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
			//PMString dispname = oAttribVal.getDisplayName();

			PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
			
			//WideString* wStr=new WideString(dispname);
			const WideString wStr(dispname);
			//CA(dispname);
			int32 StringLength = dispname.NumUTF16TextChars();
			//HeadersizeVector.push_back(StringLength);

			if(isTranspose)
			{
				tableCommands->SetCellText(wStr, GridAddress(i , 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = vec_tableheaders[i];
				/*CAttributeValue oAttribVal;
				oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname = oAttribVal.getDisplayName();*/
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				stencileInfo.elementName = dispname;

				PMString TableColName("");//oAttribVal.getTable_col();
				//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				//	CA(TableColName);
				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID = tStruct.languageID;
				stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;				

				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, 0, 0 );

			}
			else
			{
				/*PMString ASD("vec_tableheaders[i] : ");
				ASD.AppendNumber(vec_tableheaders[i]);
				CA(ASD);*/

				tableCommands->SetCellText(wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId =vec_tableheaders[i];
				/*CAttributeValue oAttribVal;
				oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname = oAttribVal.getDisplayName();*/
				PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
				stencileInfo.elementName = dispname;
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				PMString TableColName("");// oAttribVal.getTable_col();
				//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				stencileInfo.TagName = TableColName;
				stencileInfo.LanguageID =tStruct.languageID;
				//CA(TableColName);
				stencileInfo.typeId  = -1; //vec_items[i];
				stencileInfo.header  = 1;
				stencileInfo.parentId = ItemID ;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				//4Aug..ItemTable
				//stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.whichTab = tStruct.whichTab;
				stencileInfo.imgFlag =  tStruct.sectionID;
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.col_no = tStruct.colno;
				stencileInfo.tableTypeId = tStruct.typeId;
				XMLReference txmlref;

				//CA(tStruct.tagPtr->GetTagString());
				//CA(stencileInfo.colName);
				PMString TableTagNameNew = tStruct.tagPtr->GetTagString();					
				if(TableTagNameNew.Contains("PSTable"))
				{
					//CA("PSTable Found");
				}
				else
				{				
					TableTagNameNew =  "PSTable";						
				}
			//	CA("Before TagTable call : " + TableTagNameNew + "   // " + tStruct.tagPtr->GetTagString());
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, 0, i, 0 );
			//	CA("After Tag Table");
			}
			//delete wStr;
		}
	}while(kFalse);
}

void TableUtility::fillDataInHybridTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef)
{
	//CA("inside TableUtility::fillDataInHybridTable");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		//CA("!DataSprayerPtr");
		ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInHybridTable::Pointre to DataSprayerPtr not found");
		return;
	}

	
	VectorAdvanceTableScreenValuePtr tableInfo = NULL;
	do
	{	
		
		PublicationNode refpNode;
		PMString PubName("PubName");

		if(tagStruct.whichTab == 3)
			refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kTrue,kFalse);
		else if(tagStruct.whichTab == 4)
			refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
		else if(tagStruct.whichTab == 5)
			refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
	
		DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);
		
		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj)
		{
			// CA("!iTableUtlObj");
			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInHybridTable::!iTableUtlObj");
			return ;
		}

		
		resizeTableForHybridTable(tableUIDRef);

		ICommandSequence *seq = CmdUtils::BeginCommandSequence();
		iTableUtlObj->fillDataInHybridTable(tableUIDRef, refpNode, tagStruct,tableId,BoxUIDRef);

		CmdUtils::EndCommandSequence(seq);

		//InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		//if (tableModel == NULL)
		//{
		//	//CA("Err: invalid interface pointer ITableFrame");
		//	break;
		//}

		//InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		//if(tableCommands == NULL)
		//{
		//	//CA("Err: invalid interface pointer ITableCommands");
		//	break;
		//}
//
//
//		UIDRef boxUIDRef = BoxUIDRef;
//
//		IsDBTable = kTrue;		
//		int32 IsAutoResize = 0;
//		if(tagStruct.isAutoResize == 1)
//			IsAutoResize= 1;
//		
//		if(tagStruct.isAutoResize == 2)
//			IsAutoResize= 2;
//				
//		if(tagStruct.header == 1)
//			AddHeaderToTable = kTrue;
//		else
//			AddHeaderToTable = kFalse;
//
//		int32 objectId = refpNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.
//		int32 sectionid = refpNode.getPBObjectID();
//
//		PMString a("sectionid : ");
//		a.AppendNumber(sectionid);
//		a.Append("\n");
//		a.Append("objectId : ");
//		a.AppendNumber(objectId);
//		a.Append("\n");
//		a.Append("LanguageID : ");
//		a.AppendNumber(tagStruct.languageID);
//		//CA(a);
//
//
//		
//		if(tagStruct.whichTab == 5)
//		{
//			//tableInfo =ptrIAppFramework->getHybridTableData(sectionid,objectId,tagStruct.languageID,kTrue);
//		}
//		/*else
//			tableInfo =ptrIAppFramework->getHybridTableData(sectionid,objectId,tagStruct.languageID,kFalse);*/
//
//		
//
//		if( !tableInfo )
//		{
//			//CA("!tableInfo");
//			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInHybridTable::!tableInfo");
//			return;
//		}
//
//		if( tableInfo->size() == 0 )
//		{
//			//CA(" tableInfo->size()==0");
//			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInHybridTable::tableInfo->size() ==0");
//			break;
//		}
//		//It checks the selection present or not........
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
//		if( !iSelectionManager )
//		{	
//			//CA("Slection NULL");
//			break;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) {
//			//CA("!layoutSelectionSuite");
//			return;
//		}
//
//		//It gets refernce to persistant objects present in the (database) document as long as document is open.
//		UIDList CUrrentBoxUIDList(BoxUIDRef.GetDataBase(), BoxUIDRef.GetUID());
//		
//		//selectionManager->DeselectAll(NULL); // deselect every active CSB
//		layoutSelectionSuite->SelectPageItems (CUrrentBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
//		//These values are taken from db
//		PMRect theArea;
//		PMReal rel;
//		int32 ht = 0;
//		int32 wt = 0;
//		
//		//IGeometry is a required interface of page items,get the page and page geometry
//		InterfacePtr<IGeometry> iGeometry(BoxUIDRef, UseDefaultIID());
//		if(iGeometry==NULL)
//			break;
//		//the matrix required to transform the object (a page item supporting ITransform) from inner coordinates to PasteBoard coordinates
//		theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
//		
//		rel = theArea.Height();
//		ht=::ToInt32(rel);
//		rel=theArea.Width();	
//		wt=::ToInt32(rel);
//	
//		CAdvanceTableScreenValue oAdvanceTableScreenValue;
//		VectorAdvanceTableScreenValue::iterator it;
//		//it=tableInfo->begin();
//		
//		bool16 typeidFound = kFalse;
//		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//		{
//			oAdvanceTableScreenValue = *it;
//			if(oAdvanceTableScreenValue.getTableTypeId() == tagStruct.typeId)
//			{   				
//				typeidFound=kTrue;				
//				break;
//			}
//		}
//			//****
//			if(tableInfo)
//				delete tableInfo;
//			//****
//		//oAdvanceTableScreenValue=*it;
//		if(typeidFound == kFalse)
//		{
//			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInHybridTable::typeidFound == kFalse");	
//			break;
//		}	
//		PMString tableName("tableName = ");
//		tableName.Append(oAdvanceTableScreenValue.getTableName());
//		//CA(tableName);
//		int32 bodyRows=0;
//		int32 headerRows=0;
//		int32 numRows=0;
//		int32 numCols=0;		
//		bodyRows=oAdvanceTableScreenValue.getBodyRows();
//		//if(bodyRows<=0)
//		//	break;
//		headerRows=oAdvanceTableScreenValue.getHeaderRows();
//		//if(headerRows<=0)
//		//	break;
//		numCols=oAdvanceTableScreenValue.getBodyColumns();
//		//if(numCols<=0)
//		//	break;
//
//		int32 tableTypeID = oAdvanceTableScreenValue.getTableId();
//		// Total no. of rows
//		
//		//if(AddHeaderToTable)
//       //     numRows=bodyRows+headerRows; //reduces by one because in fuction resizeTable if AddHeaderToTable ==kTrue then one row added there.
//		//else 
//			numRows=bodyRows;
//
//		
//		bool8 isTranspose = oAdvanceTableScreenValue.getTranspose();
//		
//		/*InterfacePtr<ITagWriter> tWrite
//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));		
//		if(!tWrite)
//		{			
//			break;
//		}*/
//	
//		if(tagStruct.whichTab == 4)
//		{
//			//TableFlagForStandardProductTable = 2; // Since We are spraying Item Table.....this variable is used in TagTable to attach tag to complete tableTag.
//		}
//		else 
//		{	//TableFlagForStandardProductTable = 1; // Since We are spraying Product Table
//		}
//		if(isTranspose)
//		{
//			//CA("isTranspose kTrue");
//			/*if(AddHeaderToTable)
//			{
//				int32 col = numCols-1;
//				resizeTable(tableUIDRef, col, numRows, isTranspose);
//			}
//			else
//				resizeTable(tableUIDRef, numCols, numRows, isTranspose);*/
//
//			if(AddHeaderToTable)			
//				resizeTableForHybridTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht ,headerRows ,tagStruct);
//			else
//				resizeTableForHybridTable(tableUIDRef, numCols, numRows, isTranspose, wt, ht ,headerRows ,tagStruct);
//
//			
//			int32 i=0,j=0;
//			
//			vector<vector<PMString> >  vec_tableheaders=oAdvanceTableScreenValue.getTableHeader();
//
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueHeader = oAdvanceTableScreenValue.getTableHeaderValues();
//			vector<CAdvanceTableCellValue > vec_CAdvanceTableCellValue;
//			
//			/*PMString vec_vecCAdvanceTableCellValueSize("vec_vecCAdvanceTableCellValueHeaderSize = ");
//			vec_vecCAdvanceTableCellValueSize.AppendNumber(vec_vecCAdvanceTableCellValueHeader.size());
//			CA(vec_vecCAdvanceTableCellValueSize);*/
//			
//			vector<vector<CAdvanceTableCellValue> >::iterator it6;
//			vector<CAdvanceTableCellValue>::iterator it5;
//			
//			/*for(it6 = vec_vecCAdvanceTableCellValueHeader.begin(); it6 != vec_vecCAdvanceTableCellValueHeader.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueHeader");
//				vec_CAdvanceTableCellValue = *it6;
//				for(it5 = vec_CAdvanceTableCellValue.begin(); it5 != vec_CAdvanceTableCellValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvanceTableCellValue");
//				}
//			}*/
//			
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueBody = oAdvanceTableScreenValue.getTableBodyDataValues();
//			vector<CAdvanceTableCellValue> vec_CAdvannceTableCellBobyValue;
//			
//			/*PMString vec_vecCAdvanceTableCellValueBodySize("vec_CAdvannceTableCellBobyValueSize = ");
//			vec_vecCAdvanceTableCellValueBodySize.AppendNumber(vec_vecCAdvanceTableCellValueBody.size());
//			CA(vec_vecCAdvanceTableCellValueBodySize);*/
//
//			/*for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueBody");
//				vec_CAdvannceTableCellBobyValue = *it6;
//				for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvannceTableCellValue");
//				}
//			}*/
//
//			if(AddHeaderToTable)
//			{
//				tagStruct.typeId = tableTypeID;
//				putHeaderDataInHybridTable(tableUIDRef,vec_tableheaders,isTranspose,tagStruct,boxUIDRef, vec_vecCAdvanceTableCellValueHeader);
//				i=headerRows;
//				j=0;
//			}
//	
//			vector<TableRow> vec_tableBodyData=oAdvanceTableScreenValue.getTableBodyData();
//			vector<TableRow>::iterator it3;
//
//			for(it3 = vec_tableBodyData.begin(); it3 != vec_tableBodyData.end(); it3++)
//			{				
//				vector<CellData> vec_CellData=(*it3).tableRowContent;
//				PMString textToInsert("");
//				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//				if(!iConverter)
//				{
//					//CA("!iConverter");
//					break;	
//				}
//
//				vector<CellData>::iterator it4;
//				for(it4 = vec_CellData.begin(); it4 != vec_CellData.end(); it4++)
//				{	
//					CellData tempCellData=(*it4);
//					if(j>=numCols)
//					{
//						break;
//					}
//
//					TagList tagList;
//					textToInsert.Clear();
//					tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);
//					setTableRowColData(tableUIDRef, textToInsert, j, i);
//					int32/*int64*/ tagListSize =static_cast<int32>( tagList.size());	
//					if( tagList.size()==0 )
//					{
//						SlugStruct stencileInfo;
//						stencileInfo.whichTab   = 4;						
//						stencileInfo.parentId	= objectId;					
//						stencileInfo.tagStartPos= -1;						
//						stencileInfo.tagEndPos  = -1;
//						stencileInfo.elementId  = -1;						
//						PMString CellTagName("");
//						CellTagName.Append("ATTRID_");
//						CellTagName.AppendNumber(-1);				
//						PMString TableColName("");
//						TableColName.Append("Static_Text");//Advance Table Cell Data
//						TableColName = keepOnlyAlphaNumeric(TableColName);
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tagStruct.languageID;
//						//stencileInfo.parentId = pNode.getPubId();
//						stencileInfo.parentTypeId = tagStruct.parentTypeID;
//						stencileInfo.imgFlag = tagStruct.imgFlag; 
//						stencileInfo.sectionID = sectionid; 
//						stencileInfo.isAutoResize = tagStruct.isAutoResize;
//						stencileInfo.col_no = tagStruct.colno;
//						//stencileInfo.tableTypeId = tStruct.typeId;
//						stencileInfo.tableTypeId = tableTypeID;
//
//						stencileInfo.tableId = tagStruct.tableId;
//						stencileInfo.tableType = tagStruct.tableType;
//						stencileInfo.pbObjectId = tagStruct.pbObjectId;
//
//
//						XMLReference txmlref;
//						//CA(tStruct.tagPtr->GetTagString());		
//						PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//						//CA("TableTagNameNew : " + TableTagNameNew);
//						if( TableTagNameNew.Contains("PSHybridTable") )
//						{
//							//CA("PSTable Found");
//						}
//						else
//						{				
//							TableTagNameNew =  "PSHybridTable";
//						}
//						//CA("inside static_text before this->TagTable");
//						iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i, tableId );
//						//CA("after  this->TagTable");
//						j++;
//						continue;
//					}
//					for(int32 k = 0; k < tagListSize; k++)
//					{
//						//CA("inside tagListSize loop");	
//						TagStruct tInfo = tagList[k];
//						SlugStruct stencileInfo;
//						stencileInfo.whichTab = tInfo.whichTab;						
//						stencileInfo.typeId = tInfo.typeId;
//						stencileInfo.parentId = objectId;				
//						stencileInfo.tagStartPos= tInfo.startIndex;						
//						stencileInfo.tagEndPos = tInfo.endIndex;					
//						stencileInfo.elementId = tInfo.elementId;					
//						PMString CellTagName("");
//						CellTagName.Append("ATTRID_");
//						CellTagName.AppendNumber(stencileInfo.elementId);				
//						PMString TableColName("");
//						TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//						TableColName = keepOnlyAlphaNumeric(TableColName);
//						stencileInfo.TagName = TableColName;
//						stencileInfo.LanguageID =tagStruct.languageID;
//						//stencileInfo.parentId = pNode.getPubId();					
//						stencileInfo.parentTypeId = refpNode.getTypeId();					
//						stencileInfo.imgFlag = tagStruct.imgFlag; 
//						stencileInfo.sectionID = sectionid;					
//						stencileInfo.isAutoResize = tagStruct.isAutoResize;
//						stencileInfo.col_no = tagStruct.colno;					
//						//stencileInfo.tableTypeId = tStruct.typeId;
//						stencileInfo.tableTypeId = tableTypeID;
//
//						stencileInfo.tableId = tagStruct.tableId;
//						stencileInfo.tableType = tagStruct.tableType;
//						stencileInfo.pbObjectId = tagStruct.pbObjectId;
//						stencileInfo.childId = tInfo.childId;
//						stencileInfo.childTag = tInfo.childTag;						
//
//						XMLReference txmlref;
//						//CA("tStruct.tagPtr->GetTagString()");					
//						PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();					
//						if( TableTagNameNew.Contains("PSHybridTable") )
//						{
//							//CA("PSTable Found");
//						}
//						else
//						{				
//							TableTagNameNew =  "PSHybridTable";
//						}
//						//CA("before this->TagTable");
//						iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j,i,tableId );
//						//CA("after this->TagTable");
//					}
//					j++;
//				}					
//				i++;
//				j=0;
//			}	
//		}
//		else
//		{
//			/*PMString numRows1("numRows:: ");
//			numRows1.AppendNumber(numRows);
//			CA(numRows1);*/
//			/*if(AddHeaderToTable)
//			{
//				int32 rows = numRows-1;
//				resizeTable(tableUIDRef, rows , numCols, isTranspose);
//			}
//			else
//				resizeTable(tableUIDRef, numRows, numCols, isTranspose);*/
//
//			if(AddHeaderToTable)
//				resizeTableForHybridTable(tableUIDRef, numRows , numCols, isTranspose, wt, ht,headerRows, tagStruct);
//			else
//			{
//				if(iscontentbuttonselected==kTrue)
//				resizeTableForHybridTable(tableUIDRef, totalRowsofcustumtable, totalsColsofcustumtable, isTranspose, wt, ht,headerRows, tagStruct);	
//				else if(issrtucturebuttenselected==kTrue)
//					resizeTableForHybridTable(tableUIDRef, numRows, numCols, isTranspose, wt, ht,headerRows, tagStruct);	
//			}
//		
//			//if(AddHeaderToTable)
//			//{
//			//	//for convert to header rows
//			//	const GridArea headerRowsGridArea(0,0,headerRows,numCols);
//			//	const RowRange headerRowRange(headerRowsGridArea);
//			//											
//			//	InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//			//	if (tableModel == NULL)
//			//	{
//			//		//CA("tableModel == NULL");
//			//		break;
//			//	}
//
//			//	InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//			//	if(tableCommands==NULL)
//			//	{
//			//		//CA("tableCommands==NULL");
//			//		break;
//			//	}
//			//	ErrorCode status = tableCommands->ConvertToHeaderRows( headerRowRange );
//			//	if(status==kSuccess)
//			//	{
//			//		//CA("done");
//			//	}
//			//	//upto here---------
//			//}
//			
//			int32 i=0,j=0;
//			vector<vector<PMString> >  vec_tableheaders=oAdvanceTableScreenValue.getTableHeader();	
//
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueHeader = oAdvanceTableScreenValue.getTableHeaderValues();
//			vector<CAdvanceTableCellValue > vec_CAdvanceTableCellValue;
//			
//			/*PMString vec_vecCAdvanceTableCellValueSize("vec_vecCAdvanceTableCellValueHeaderSize = ");
//			vec_vecCAdvanceTableCellValueSize.AppendNumber(vec_vecCAdvanceTableCellValueHeader.size());
//			CA(vec_vecCAdvanceTableCellValueSize);*/
//			
//			vector<vector<CAdvanceTableCellValue> >::iterator it6;
//			vector<CAdvanceTableCellValue>::iterator it5;
//			
//			/*for(it6 = vec_vecCAdvanceTableCellValueHeader.begin(); it6 != vec_vecCAdvanceTableCellValueHeader.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueHeader");
//				vec_CAdvanceTableCellValue = *it6;
//				for(it5 = vec_CAdvanceTableCellValue.begin(); it5 != vec_CAdvanceTableCellValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvanceTableCellValue");
//				}
//			}*/
//			
//			vector<vector<CAdvanceTableCellValue> >vec_vecCAdvanceTableCellValueBody = oAdvanceTableScreenValue.getTableBodyDataValues();
//			vector<CAdvanceTableCellValue> vec_CAdvannceTableCellBobyValue;
//			
//			/*PMString vec_vecCAdvanceTableCellValueBodySize("vec_CAdvannceTableCellBobyValueSize = ");
//			vec_vecCAdvanceTableCellValueBodySize.AppendNumber(vec_vecCAdvanceTableCellValueBody.size());
//			CA(vec_vecCAdvanceTableCellValueBodySize);*/
//
//			/*for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//			{
//				//CA("inside for vec_vec_CAdvanceTableCellValueBody");
//				vec_CAdvannceTableCellBobyValue = *it6;
//				for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//				{
//					//CA("inside for vec_CAdvannceTableCellValue");
//				}
//			}*/
//
//			//for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//			//{
//
//			//	//CA("inside for vec_vec_CAdvanceTableCellValueBody");
//			//	vec_CAdvannceTableCellBobyValue = *it6;
//			//	for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//			//	{
//			//		//CA("inside for vec_CAdvannceTableCellValue");
//			//		/////merge cell here
//			//		
//			//		CAdvanceTableCellValue objCAdvanceTableCellValue = *it5;
//			//		
//			//		/*PMString temp("objCAdvanceTableCellValue.rowStartId = ");
//			//		temp.AppendNumber(objCAdvanceTableCellValue.rowStartId);
//			//		temp.Append("\n");
//			//		temp.Append(" objCAdvanceTableCellValue.rowEndId = ");
//			//		temp.AppendNumber(objCAdvanceTableCellValue.rowEndId);
//			//		temp.Append("\n");
//			//		temp.Append("objCAdvanceTableCellValue.colStartId = ");
//			//		temp.AppendNumber(objCAdvanceTableCellValue.colStartId);
//			//		temp.Append("\n");
//			//		temp.Append("objCAdvanceTableCellValue.colEndId = ");
//			//		temp.AppendNumber(objCAdvanceTableCellValue.colEndId);
//			//		CA(temp);*/
//
//			//		bool16 isMerge = kFalse;
//
//			//		if(objCAdvanceTableCellValue.rowStartId == 0 && objCAdvanceTableCellValue.colStartId ==0)
//			//			continue;
//
//			//		RowRange rowRange(objCAdvanceTableCellValue.rowStartId, 1);
//			//		if(!AddHeaderToTable)
//			//			rowRange.start = objCAdvanceTableCellValue.rowStartId - headerRows;
//
//			//		ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//			//		if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//			//		{
//			//			isMerge = kTrue;
//			//			if(AddHeaderToTable){}
//			//				//rowRange.Set(objCAdvanceTableCellValue.rowStartId , objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//			//			else{}
//			//				//rowRange.Set(objCAdvanceTableCellValue.rowStartId - headerRows, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//			//		}
//			//		if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//			//		{
//   //                     isMerge = kTrue;
//			//			//colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId  - objCAdvanceTableCellValue.colStartId + 1);
//			//		}
//			//		if(isMerge)
//			//		{
//			//			//CA("Going for Merge Cell");
//
//			//			GridArea gridArea(rowRange, colRange);
//			//			//ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//			//		}
//			//		//CA("end of for 2");
//			//	}
//			//}
//
//			if( AddHeaderToTable )
//			{
//				tagStruct.typeId = tableTypeID;
//				putHeaderDataInHybridTable(tableUIDRef,vec_tableheaders,isTranspose,tagStruct,boxUIDRef ,vec_vecCAdvanceTableCellValueHeader);
//				i=headerRows;
//				j=0;
//			}
////====================New code added here==========
//			
//			//When we change Refernece of a cell of Adv.Table in CM then its Item ID  and AttributeIds are changing.
//			//but for "Content Only " we have ItemId and AttributeId from Document .So Content Only option will not upadate 
//			//the cell whose reference is changed.
//		if(iscontentbuttonselected==kTrue)
//		{
//			{
//
//					vector<int32> vector_items_doc;
//					vector_items_doc.clear();
//
//					{
//						UID textFrameUID = kInvalidUID;
//						InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameDataOne) 
//						{
//							textFrameUID = graphicFrameDataOne->GetTextContentUID();
//						}
//						if (textFrameUID == kInvalidUID)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textFrameUID == kInvalidUID");
//							break;//breaks the do{}while(kFalse)
//						}
//						
//						InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
//						if (graphicFrameHierarchy == nil) 
//						{
//							//CA("graphicFrameHierarchy is NULL");
//							break;
//						}
//										
//						InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//						if (!multiColumnItemHierarchy) {
//							//CA("multiColumnItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//						if (!multiColumnItemTextFrame) {
//							//CA("multiColumnItemTextFrame is NULL");
//							break;
//						}
//						InterfacePtr<IHierarchy>
//						frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//						if (!frameItemHierarchy) {
//							//CA("frameItemHierarchy is NULL");
//							break;
//						}
//
//						InterfacePtr<ITextFrameColumn>
//						frameItemTFC(frameItemHierarchy, UseDefaultIID());
//						if (!frameItemTFC) {
//							//CA("!!!ITextFrameColumn");
//							break;
//						}
//						InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//						if (textModel == nil)
//						{
//							ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable::textModel == nil");
//							break;//breaks the do{}while(kFalse)
//						}
//						UIDRef StoryUIDRef(::GetUIDRef(textModel));
//
//						InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
//						if(tableList==nil)
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount()/* - 1*/;
//
//						if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
//						{
//							break;//breaks the do{}while(kFalse)
//						}
//
//						for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
//						{//for..tableIndex
//							//CA("tableIndex");
//							InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
//							if(tableModel == nil)
//								continue;//continues the for..tableIndex
//
//							InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//							if(tableCommands==NULL)
//							{
//								ptrIAppFramework->LogDebug("AP46_RefreshContent::CommonFunctions::RefreshMultipleListsSprayedInOneTable: Err: invalid interface pointer ITableCommands");
//								continue;//continues the for..tableIndex
//							}
//									
//							UIDRef tableRef(::GetUIDRef(tableModel)); 
//							//slugInfo.tagPtr is IIDXMLElement ptr attached to the table.
//							IIDXMLElement* & tableXMLElementPtr = tagStruct.tagPtr;
//							XMLContentReference contentRef = tableXMLElementPtr->GetContentReference();
//							UIDRef ContentRef = contentRef.GetUIDRef();
//						
//
//							GridArea headerArea = tableModel->GetHeaderArea();
//							GridArea tableArea_new = tableModel->GetTotalArea();
//							ITableModel::const_iterator iterTable2(tableModel->begin(tableArea_new));
//							ITableModel::const_iterator endTable2(tableModel->end(tableArea_new));
//
//
//							while (iterTable2 != endTable2)
//							{
//								//CA("1111");
//								GridAddress gridAddress = (*iterTable2); 
//								//CAI(gridAddress.row);
//								InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
//								if(cellContent == nil) 
//								{
//									//CA("cellContent == nil");
//									break;
//								}
//								InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
//								if(!cellXMLReferenceData) {
//									//CA("!cellXMLReferenceData");
//									break;
//								}
//								XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
//								++iterTable2;
//
//								
//								InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
//								
//								//Get the text tag attached to the text inside the cell.
//								//We are providing only one texttag inside cell.
//								if(cellXMLElementPtr==nil || cellXMLElementPtr->GetChildCount()<= 0)
//								{
//									continue;
//								}
//								int32 NoofChilds = cellXMLElementPtr->GetChildCount();
//
//								PMString a("childTagCnt :  ");
//								a.AppendNumber(NoofChilds);
//								//CA(a);
//								for(int32 celltagIndex = 0;celltagIndex < cellXMLElementPtr->GetChildCount();celltagIndex++)
//								{ // iterate thruogh cell's tags
//									XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(celltagIndex);
//									//IIDXMLElement * cellTextXMLElementPtr = cellTextXMLRef.Instantiate();
//									InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLRef.Instantiate());
//
//									
//									//The cell may be blank. i.e doesn't have any text tag inside it.
//									if(cellTextXMLElementPtr == nil)
//									{
//										continue;
//									}
//									
//									
//									PMString strChildID = cellTextXMLElementPtr->GetAttributeValue(WideString("childId"));
//									PMString strrowno = cellTextXMLElementPtr->GetAttributeValue(WideString("rowno"));
//									//CA("row :: "+ strrowno + " :: "+strChildID);
//									
//									int32 childId = strChildID.GetAsNumber();
//									if(childId > -1 )
//									{
//										PMString itemid1("");
//										itemid1.Append("itemidssss	:: "+strChildID);
//										vector_items_doc.push_back(childId);
//										vector_items_doc.erase(std::unique(vector_items_doc.begin(), vector_items_doc.end()), vector_items_doc.end());
//										itemid1.Append("	::vector_items_doc	::");
//										itemid1.AppendNumber((int32)vector_items_doc.size());
//										//CA(itemid1);
//									}
//								}//for
//							}//while
//						}//end of for
//					}//end of block
//
//					//CA("content is selected");
//					vector<TableRow> vec_tableBodyData=oAdvanceTableScreenValue.getTableBodyData();
//					vector<TableRow>::iterator it3;
//
//
//						PMString asd("presentRows  ");
//						asd.AppendNumber(totalRowsofcustumtable);
//						asd.Append("\npresentCols  ");
//						asd.AppendNumber(totalsColsofcustumtable);
//						asd.Append("\nHeaders  ");
//						asd.AppendNumber(totalheadersofcustumtable);
//						//CA(asd);
//
//						int32 x, xx = totalRowsofcustumtable - (int32)vec_tableBodyData.size() ;
//					for(it3 = vec_tableBodyData.begin(),it6 = vec_vecCAdvanceTableCellValueBody.begin(), x=0; x<totalRowsofcustumtable/*it3 != vec_tableBodyData.end()*/; x++,it3++,it6++)
//					{
//						//CA("First for loop");
//						//CAI(vec_tableBodyData.at(x).tableRowContent);
//						if((x==xx && xx!=0) || it3 == vec_tableBodyData.end())
//						{
//							CA("break");
//							break;
//						}
//						vector<CellData> vec_CellData=(*it3).tableRowContent;
//						PMString textToInsert("");
//						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//						if(!iConverter)
//						{
//							break;					
//						}
//						vector<CellData>::iterator it4;
//						vec_CAdvannceTableCellBobyValue = *it6;	
//						for(it4 = vec_CellData.begin(),it5 = vec_CAdvannceTableCellBobyValue.begin(); it4 != vec_CellData.end(); it4++,it5++)
//						{
//							CellData tempCellData=(*it4);
//							if(j>=numCols)
//								break;
//
//							TagList tagList;
//							textToInsert.Clear();
//							
//							tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);
//							//CA("textToInsert = " + textToInsert);		
//							//PMString w1("textToInsert12 : ");
//							//w1.Append(textToInsert);
//							////CA(w1);
//
//							CAdvanceTableCellValue objCAdvanceTableCellValue = *it5;
//							j = objCAdvanceTableCellValue.colId;
//
//							setTableRowColData(tableUIDRef, textToInsert, i,j);
//					//CA("After setTableRowColData");
//							int32/*int64*/ tagListSize =static_cast<int32>( tagList.size());
//							PMString w1("tagListSize : ");
//							w1.AppendNumber(tagListSize);
//							//CA(w1);
//							//CA(w1);
//							
//							//if(tagList.size() == 0)
//							//{
//							//	
//							//	SlugStruct stencileInfo;
//							//	stencileInfo.whichTab   = 4;						
//							//	stencileInfo.parentId	= objectId;					
//							//	stencileInfo.tagStartPos= -1;						
//							//	stencileInfo.tagEndPos  = -1;
//							//	stencileInfo.elementId  = -1;						
//							//	PMString CellTagName("");
//							//	CellTagName.Append("ATTRID_");
//							//	CellTagName.AppendNumber(-1);				
//							//	PMString TableColName("");
//							//	TableColName.Append("Static_Text");//Advance Table Cell Data
//							//	TableColName = keepOnlyAlphaNumeric(TableColName);
//							//	stencileInfo.TagName = TableColName;
//							//	stencileInfo.LanguageID =tagStruct.languageID;
//							//	//stencileInfo.parentId = pNode.getPubId();
//							//	stencileInfo.parentTypeId = tagStruct.parentTypeID;
//							//	stencileInfo.imgFlag = tagStruct.imgFlag; 
//							//	stencileInfo.sectionID = sectionid; 
//							//	stencileInfo.isAutoResize = tagStruct.isAutoResize;
//							//	stencileInfo.col_no = tagStruct.colno;
//							//	//stencileInfo.tableTypeId = tStruct.typeId;
//							//	stencileInfo.tableTypeId = tableTypeID;
//							//	XMLReference txmlref;
//							//	//CA(tStruct.tagPtr->GetTagString());		
//							//	PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//							//	//CA("TableTagNameNew : " + TableTagNameNew);
//							//	if( TableTagNameNew.Contains("PSHybridTable") )
//							//	{
//							//		//CA("PSTable Found");
//							//	}
//							//	else
//							//	{				
//							//		TableTagNameNew =  "PSHybridTable";
//							//	}
//							//	//CA("before tagList.size() == 0 this->TagTable");
//							//	iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//							//	
//							//	//CA("after  this->TagTable");
//							//	j++;
//							//	continue;
//							//}
//
//							if(tagListSize==0)
//							{
//								SlugStruct stencileInfo;
//									stencileInfo.whichTab   = 4;
//									stencileInfo.typeId = -1;
//									stencileInfo.parentId = 0;///////////need to addd
//									stencileInfo.tagStartPos= 0;						
//									stencileInfo.tagEndPos  = 5;
//
//									//PMString d("startIndex : ");
//									//d.AppendNumber(tInfo.startIndex);
//									//d.Append("\n");
//									//d.Append("endIndex : ");
//									//d.AppendNumber( tInfo.endIndex);
//									////CA(d);
//
//									stencileInfo.elementId  = -1;						
//									PMString CellTagName("");
//									CellTagName.Append("ATTRID_");
//									CellTagName.AppendNumber(-1);				
//									PMString TableColName("");
//									TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//									TableColName = keepOnlyAlphaNumeric(TableColName);
//									stencileInfo.TagName = TableColName;
//									stencileInfo.LanguageID =tagStruct.languageID;
//									//stencileInfo.parentId = pNode.getPubId();
//									stencileInfo.parentTypeId = refpNode.getTypeId();
//									stencileInfo.imgFlag = tagStruct.imgFlag; 
//									stencileInfo.sectionID = sectionid; 
//									stencileInfo.isAutoResize = tagStruct.isAutoResize;
//									stencileInfo.col_no = tagStruct.colno;
//									//stencileInfo.tableTypeId = tStruct.typeId;
//									stencileInfo.tableTypeId = tableTypeID;
//
//									stencileInfo.tableId = tagStruct.tableId;
//									stencileInfo.tableType = tagStruct.tableType;
//									stencileInfo.pbObjectId = tagStruct.pbObjectId;
//									stencileInfo.childId = -1;
//									stencileInfo.childTag = -1;
//
//									XMLReference txmlref;
//									//CA(tStruct.tagPtr->GetTagString());		
//									PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//									//CA("TableTagNameNew : " + TableTagNameNew);
//									if( TableTagNameNew.Contains("PSHybridTable") )
//									{
//										//CA("PSTable Found");
//									}
//									else
//									{				
//										TableTagNameNew =  "PSHybridTable";
//									}
//											
//
//							//CA("Before TagTable");
//									iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//								
//							}
//
//							for(int32 k = 0; k < tagListSize; k++)
//							{	
//								//CA("inside Tag list");
//								TagStruct tInfo = tagList[k];
//		///////////////////		Added for graphic frame inside hybrid table cell
//								if(tInfo.imgFlag == 1)
//								{
//									InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//									( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//									if(!txtMisSuite)
//									{
//										//CA("!txtMisSuite");
//										break; 
//									}
//
//									UIDRef ref;
//									bool16 ISTextFrame = txtMisSuite->GetFrameUIDRef(ref);
//									if(ISTextFrame)
//									{
//										int32 start=0;
//
//										InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//										if (tableModel == NULL)
//										{
//											//CA("Err: invalid interface pointer ITableFrame");
//											break;
//										}
//											
//										InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//										if(tableCommands==NULL)
//										{
//											//CA("Err: invalid interface pointer ITableCommands");
//											break;
//										}
//						
//										RowRange rowRange(i , 1) ;
//										ErrorCode result = tableCommands->ResizeRows(rowRange,108);
//										//if(result ==  kFailure)
//											//CA(" unable to resize row");
//										ColRange colRange(j , 1);
//									
//										result = tableCommands->ResizeCols(colRange,108);
//										//if(result ==  kFailure)
//											//CA(" unable to resize Cols");
//						
//										
//										InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
//										if(!pTextSel)
//										{
//											//CA("pTextSel is null");
//											break;
//										}
//										InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
//										if(!tblTxtSel)
//										{
//											//CA("tblTxtSel is null");
//											break;
//										}
//
//										GridAddress  gridAddress(i,j);
//										tblTxtSel->SelectTextInCell( tableModel,gridAddress);
//										InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager)/*,UseDefaultIID()*/);
//										if(!txtSelSuite)
//										{ 
//											//CA("Text Selectin suite is null"); 
//											return;
//										}
//									
//										InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
//										if(!pTextTarget)
//										{ 
//											//CA("pTextTarget is null"); 
//											return;
//										}
//										
//										RangeData rangData=pTextTarget->GetRange();
//										start=rangData.End();
//						
//										start += tInfo.startIndex;
//										/*InterfacePtr<ITextFrame>txtFrame(ref,UseDefaultIID());
//										if(!txtFrame)
//										{
//											CA("Text Frame is null");				
//											break;
//										}*/
//
//										////////////	CS3 Change
//										InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
//										if (graphicFrameHierarchy == nil) 
//										{
//											ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInHybridTable::graphicFrameHierarchy == nil");
//											return ;
//										}
//														
//										InterfacePtr<ITextFrameColumn>txtFrame(graphicFrameHierarchy, UseDefaultIID());
//										if (!txtFrame) {
//											ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInHybridTable::textFrame == nil");
//											break ;
//										}
//
//								////////////	End	
//										InterfacePtr<ITextModel>txtModel(txtFrame->QueryTextModel());
//										if(!txtModel)
//										{
//											CA("Text Model is null");				
//											break;
//										}
//										
//										UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
//										this->InsertInline(textStoryUIDRef,start);
//								
//										IDataSprayer* iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//										if(!iDataSprayer)
//										{	
//											//CA("!iDtaSprayer");
//											break;
//										}
//										
//										SlugStruct stencileInfo;
//										stencileInfo.whichTab   = tInfo.whichTab;
//										stencileInfo.typeId = tInfo.typeId;
//										stencileInfo.parentId = tInfo.parentId ;
//										stencileInfo.tagStartPos= tInfo.startIndex;						
//										stencileInfo.tagEndPos  = tInfo.endIndex;
//										stencileInfo.elementId  = tInfo.elementId;						
//										PMString CellTagName("");
//										CellTagName.Append("ATTRID_");
//										CellTagName.AppendNumber(stencileInfo.elementId);				
//										PMString TableColName("");
//										TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//										TableColName = keepOnlyAlphaNumeric(TableColName);
//										stencileInfo.TagName = TableColName;
//										stencileInfo.LanguageID =tagStruct.languageID;
//										stencileInfo.parentTypeId = tInfo.parentTypeID;
//										stencileInfo.imgFlag = tInfo.imgFlag ; 
//										stencileInfo.sectionID = sectionid; 
//										stencileInfo.isAutoResize = tagStruct.isAutoResize;
//										stencileInfo.col_no = tagStruct.colno;
//										//stencileInfo.tableTypeId = tStruct.typeId;
//										stencileInfo.tableTypeId = tableTypeID;
//										
//										stencileInfo.tableId = tagStruct.tableId;
//										stencileInfo.tableType = tagStruct.tableType;
//										stencileInfo.pbObjectId = tagStruct.pbObjectId;
//										stencileInfo.childId = tInfo.childId;
//										stencileInfo.childTag = tInfo.childTag;
//
//										XMLReference txmlref;
//										//CA(tStruct.tagPtr->GetTagString());		
//										PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//										//CA("TableTagNameNew : " + TableTagNameNew);
//										if( TableTagNameNew.Contains("PSHybridTable") )
//										{
//											//CA("PSTable Found");
//										}
//										else
//										{				
//											TableTagNameNew =  "PSHybridTable";
//										}
//										
//										TagStruct tagInfo;
//										tagInfo.whichTab     = tInfo.whichTab;
//										tagInfo.typeId       = tInfo.typeId;
//										tagInfo.parentId     = tInfo.parentId;
//										tagInfo.startIndex   = tInfo.startIndex;
//										tagInfo.endIndex     = tInfo.endIndex;
//										tagInfo.elementId    = tInfo.elementId;
//										tagInfo.languageID   = tagStruct.languageID;
//										tagInfo.parentTypeID = tInfo.parentTypeID;
//										tagInfo.imgFlag		 = tInfo.imgFlag;
//										tagInfo.sectionID	 = sectionid;
//										tagInfo.isAutoResize = tagStruct.isAutoResize;
//										tagInfo.colno		 = tagStruct.colno;
//										tagInfo.tagPtr		 = tagStruct.tagPtr;	
//
//										tagInfo.tableId = tagStruct.tableId;
//										tagInfo.tableType = tagStruct.tableType;
//										tagInfo.pbObjectId = tagStruct.pbObjectId;
//										tagInfo.childId = tInfo.childId;
//										tagInfo.childTag = tInfo.childTag;
//										
//										XMLReference xmlref;
//										
//						
//										if(tInfo.whichTab == 4)
//										{
//											//CA("Spraying item image");
//											iDataSprayer->sprayItemImageInsideHybridTable(newFrameUIDRef, tagInfo, tagInfo.parentId );
//										}
//										if(tInfo.whichTab == 3)
//											iDataSprayer->fillImageInBox(newFrameUIDRef , tagInfo, tagInfo.parentId );
//									
//										iDataSprayer->fitInlineImageInBoxInsideTableCell(newFrameUIDRef);
//										iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//									}	
//		/////////////////////			End 
//								}
//								else
//								{	
//									//CA("Inside else ");
//									SlugStruct stencileInfo;
//									stencileInfo.whichTab   = tInfo.whichTab;
//									stencileInfo.typeId = tInfo.typeId;
//									stencileInfo.parentId = objectId;
//									stencileInfo.tagStartPos= tInfo.startIndex;						
//									stencileInfo.tagEndPos  = tInfo.endIndex;
//
//									PMString d("startIndex : ");
//									d.AppendNumber(tInfo.startIndex);
//									d.Append("\n");
//									d.Append("endIndex : ");
//									d.AppendNumber( tInfo.endIndex);
//									//CA(d);
//
//									stencileInfo.elementId  = tInfo.elementId;						
//									PMString CellTagName("");
//									CellTagName.Append("ATTRID_");
//									CellTagName.AppendNumber(stencileInfo.elementId);				
//									PMString TableColName("");
//									TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//									TableColName = keepOnlyAlphaNumeric(TableColName);
//									stencileInfo.TagName = TableColName;
//									stencileInfo.LanguageID =tagStruct.languageID;
//									//stencileInfo.parentId = pNode.getPubId();
//									stencileInfo.parentTypeId = refpNode.getTypeId();
//									stencileInfo.imgFlag = tagStruct.imgFlag; 
//									stencileInfo.sectionID = sectionid; 
//									stencileInfo.isAutoResize = tagStruct.isAutoResize;
//									stencileInfo.col_no = tagStruct.colno;
//									//stencileInfo.tableTypeId = tStruct.typeId;
//									stencileInfo.tableTypeId = tableTypeID;
//
//									stencileInfo.tableId = tagStruct.tableId;
//									stencileInfo.tableType = tagStruct.tableType;
//									stencileInfo.pbObjectId = tagStruct.pbObjectId;
//									stencileInfo.childId = tInfo.childId;
//									stencileInfo.childTag = tInfo.childTag;
//
//									XMLReference txmlref;
//									//CA(tStruct.tagPtr->GetTagString());		
//									PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//									//CA("TableTagNameNew : " + TableTagNameNew);
//									if( TableTagNameNew.Contains("PSHybridTable") )
//									{
//										//CA("PSTable Found");
//									}
//									else
//									{				
//										TableTagNameNew =  "PSHybridTable";
//									}
//											
//
//							//CA("Before TagTable");
//									iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//								}
//								//CA("After TagTable");
//							}
//							//j++;					
//						}					
//						i++;
//						j=0;				
//					}						
//					//******
//					for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//					{
//						vec_CAdvannceTableCellBobyValue = *it6;
//						for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//						{					
//							/////merge cell here
//
//							CAdvanceTableCellValue objCAdvanceTableCellValue = *it5;
//												
//							bool16 isMerge = kFalse;
//
//							if(objCAdvanceTableCellValue.rowStartId == 0 && objCAdvanceTableCellValue.colStartId ==0)
//								continue;
//
//							RowRange rowRange(objCAdvanceTableCellValue.rowStartId, 1);
//							if(!AddHeaderToTable)
//								rowRange.start = objCAdvanceTableCellValue.rowStartId - headerRows;	
//							ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//							if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//							{
//								isMerge = kTrue;
//								if(AddHeaderToTable)
//									rowRange.Set(objCAdvanceTableCellValue.rowStartId , objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//								else
//									rowRange.Set(objCAdvanceTableCellValue.rowStartId - headerRows, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//							}
//							if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//							{
//								isMerge = kTrue;
//								colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId  - objCAdvanceTableCellValue.colStartId + 1);
//							}		
//							if(isMerge)
//							{
//								//CA("Going for Merge Cell");
//								GridArea gridArea(rowRange, colRange);
//								ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//							}
//							//CA("end of for 2");
//						}
//					}
//				}
//
//		//	//CA("content is selected");
//
//
//		//	PMString asd("presentRows  ");
//		//	asd.AppendNumber(totalRowsofcustumtable);
//		//	asd.Append("\npresentCols  ");
//		//	asd.AppendNumber(totalsColsofcustumtable);
//		//	asd.Append("\nHeaders  ");
//		//	asd.AppendNumber(totalheadersofcustumtable);
//		//	//CA(asd);
//		//	int32 sizeofItemID = static_cast<int32>(ItemIDsForAdvancedTable.size());
//		//	int32 sizeofAttributeId = static_cast<int32>(AttributeIDSforAdvanceTable.size());
//
//		//	//*********************** Advance Table ***************************
//		//	int32 count=0;
//		//	for(int32 x=0;x<totalRowsofcustumtable;x++)
//		//	{
//		//		//CA("Row");
//		//		for(int32 y=0;y<totalsColsofcustumtable;y++)
//		//		{
//		//			PMString itemAttributeValue(""); 
//		//			if(ItemIDsForAdvancedTable.at(count) !=-1 || AttributeIDSforAdvanceTable.at(count)!=-1)
//		//			{
//		//			 itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(ItemIDsForAdvancedTable.at(count),AttributeIDSforAdvanceTable.at(count), Languageid, /*kTrue*/kFalse );//23OCT09//Amit
//		//			//=============================Start====================================================
//
//		//					SlugStruct stencileInfo;
//		//					stencileInfo.whichTab   = indexAdTable;
//		//					stencileInfo.typeId = -1;
//		//					stencileInfo.parentId = ParentIdAdTable;
//		//					stencileInfo.tagStartPos= 0;						
//		//					stencileInfo.tagEndPos  = 5;
//
//		//					stencileInfo.elementId  = AttributeIDSforAdvanceTable.at(count);							
//		//					PMString CellTagName("");
//		//					CellTagName.Append("ATTRID_");
//		//					CellTagName.AppendNumber(AttributeIDSforAdvanceTable.at(count));				
//		//					PMString TableColName("");
//		//					TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//		//					TableColName = keepOnlyAlphaNumeric(TableColName);
//		//					stencileInfo.TagName = TableColName;
//		//					stencileInfo.LanguageID =tagStruct.languageID;
//		//					//stencileInfo.parentId = pNode.getPubId();
//		//					stencileInfo.parentTypeId = refpNode.getTypeId();
//		//					stencileInfo.imgFlag = tagStruct.imgFlag; 
//		//					stencileInfo.sectionID = sectionIDAdTable; 
//		//					stencileInfo.isAutoResize = tagStruct.isAutoResize;
//		//					stencileInfo.col_no = tagStruct.colno;
//		//					//stencileInfo.tableTypeId = tStruct.typeId;
//		//					stencileInfo.tableTypeId = tableTypeID;
//
//		//					stencileInfo.tableId = tagStruct.tableId;
//		//					stencileInfo.tableType = tagStruct.tableType;
//		//					stencileInfo.pbObjectId = tagStruct.pbObjectId;
//		//					stencileInfo.childId = ItemIDsForAdvancedTable.at(count);
//		//					stencileInfo.childTag = 1;
//
//		//					XMLReference txmlref;
//		//					//CA(tStruct.tagPtr->GetTagString());		
//		//					PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//		//					//CA("TableTagNameNew : " + TableTagNameNew);
//		//					if( TableTagNameNew.Contains("PSHybridTable") )
//		//					{
//		//						//CA("PSTable Found");
//		//					}
//		//					else
//		//					{				
//		//						TableTagNameNew =  "PSHybridTable";
//		//					}
//		//					setTableRowColData(tableUIDRef, itemAttributeValue, x,y);
//		//					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, x,y,tableId );
//		//				count++;
//		//			}
//		//			else
//		//			{
//		//				SlugStruct stencileInfo;
//		//					stencileInfo.whichTab   = indexAdTable;
//		//					stencileInfo.typeId = -1;
//		//					stencileInfo.parentId = ParentIdAdTable;
//		//					stencileInfo.tagStartPos= 0;						
//		//					stencileInfo.tagEndPos  = 5;
//
//		//					stencileInfo.elementId  = AttributeIDSforAdvanceTable.at(count);							
//		//					PMString CellTagName("");
//		//					CellTagName.Append("ATTRID_");
//		//					CellTagName.AppendNumber(AttributeIDSforAdvanceTable.at(count));				
//		//					PMString TableColName("");
//		//					TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//		//					TableColName = keepOnlyAlphaNumeric(TableColName);
//		//					stencileInfo.TagName = TableColName;
//		//					stencileInfo.LanguageID =tagStruct.languageID;
//		//					//stencileInfo.parentId = pNode.getPubId();
//		//					stencileInfo.parentTypeId = refpNode.getTypeId();
//		//					stencileInfo.imgFlag = tagStruct.imgFlag; 
//		//					stencileInfo.sectionID = sectionIDAdTable; 
//		//					stencileInfo.isAutoResize = tagStruct.isAutoResize;
//		//					stencileInfo.col_no = tagStruct.colno;
//		//					//stencileInfo.tableTypeId = tStruct.typeId;
//		//					stencileInfo.tableTypeId = tableTypeID;
//
//		//					stencileInfo.tableId = tagStruct.tableId;
//		//					stencileInfo.tableType = tagStruct.tableType;
//		//					stencileInfo.pbObjectId = tagStruct.pbObjectId;
//		//					stencileInfo.childId = ItemIDsForAdvancedTable.at(count);
//		//					stencileInfo.childTag = 1;
//
//		//					XMLReference txmlref;
//		//					//CA(tStruct.tagPtr->GetTagString());		
//		//					PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//		//					//CA("TableTagNameNew : " + TableTagNameNew);
//		//					if( TableTagNameNew.Contains("PSHybridTable") )
//		//					{
//		//						//CA("PSTable Found");
//		//					}
//		//					else
//		//					{				
//		//						TableTagNameNew =  "PSHybridTable";
//		//					}
//		//					setTableRowColData(tableUIDRef, itemAttributeValue, x,y);
//		//					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, x,y,tableId );
//		//				count++;
//		//			}
//		//		}
//		//	}
//
//		}
//		else if(issrtucturebuttenselected==kTrue)
//		{
//			//CA("Structure is selected");
//			vector<TableRow> vec_tableBodyData=oAdvanceTableScreenValue.getTableBodyData();
//			vector<TableRow>::iterator it3;
//
//			for(it3 = vec_tableBodyData.begin(),it6 = vec_vecCAdvanceTableCellValueBody.begin(); it3 != vec_tableBodyData.end(); it3++,it6++)
//			{
//				//CA("First for loop");
//				vector<CellData> vec_CellData=(*it3).tableRowContent;
//				PMString textToInsert("");
//				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//				if(!iConverter)
//				{
//					break;					
//				}
//				vector<CellData>::iterator it4;
//				vec_CAdvannceTableCellBobyValue = *it6;	
//				for(it4 = vec_CellData.begin(),it5 = vec_CAdvannceTableCellBobyValue.begin(); it4 != vec_CellData.end(); it4++,it5++)
//				{
//					CellData tempCellData=(*it4);
//					if(j>=numCols)
//						break;
//
//					TagList tagList;
//					textToInsert.Clear();
//					
//					tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);
//					//CA("textToInsert = " + textToInsert);		
//					//PMString w1("textToInsert12 : ");
//					//w1.Append(textToInsert);
//					////CA(w1);
//
//					CAdvanceTableCellValue objCAdvanceTableCellValue = *it5;
//					j = objCAdvanceTableCellValue.colId;
//
//					setTableRowColData(tableUIDRef, textToInsert, i,j);
//			//CA("After setTableRowColData");
//					int32/*int64*/ tagListSize =static_cast<int32>( tagList.size());
//					PMString w1("tagListSize : ");
//					w1.AppendNumber(tagListSize);
//					//CA(w1);
//					//CA(w1);
//					
//					//if(tagList.size() == 0)
//					//{
//					//	
//					//	SlugStruct stencileInfo;
//					//	stencileInfo.whichTab   = 4;						
//					//	stencileInfo.parentId	= objectId;					
//					//	stencileInfo.tagStartPos= -1;						
//					//	stencileInfo.tagEndPos  = -1;
//					//	stencileInfo.elementId  = -1;						
//					//	PMString CellTagName("");
//					//	CellTagName.Append("ATTRID_");
//					//	CellTagName.AppendNumber(-1);				
//					//	PMString TableColName("");
//					//	TableColName.Append("Static_Text");//Advance Table Cell Data
//					//	TableColName = keepOnlyAlphaNumeric(TableColName);
//					//	stencileInfo.TagName = TableColName;
//					//	stencileInfo.LanguageID =tagStruct.languageID;
//					//	//stencileInfo.parentId = pNode.getPubId();
//					//	stencileInfo.parentTypeId = tagStruct.parentTypeID;
//					//	stencileInfo.imgFlag = tagStruct.imgFlag; 
//					//	stencileInfo.sectionID = sectionid; 
//					//	stencileInfo.isAutoResize = tagStruct.isAutoResize;
//					//	stencileInfo.col_no = tagStruct.colno;
//					//	//stencileInfo.tableTypeId = tStruct.typeId;
//					//	stencileInfo.tableTypeId = tableTypeID;
//					//	XMLReference txmlref;
//					//	//CA(tStruct.tagPtr->GetTagString());		
//					//	PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//					//	//CA("TableTagNameNew : " + TableTagNameNew);
//					//	if( TableTagNameNew.Contains("PSHybridTable") )
//					//	{
//					//		//CA("PSTable Found");
//					//	}
//					//	else
//					//	{				
//					//		TableTagNameNew =  "PSHybridTable";
//					//	}
//					//	//CA("before tagList.size() == 0 this->TagTable");
//					//	iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//					//	
//					//	//CA("after  this->TagTable");
//					//	j++;
//					//	continue;
//					//}
//
//					if(tagListSize==0)
//					{
//						SlugStruct stencileInfo;
//							stencileInfo.whichTab   = 4;
//							stencileInfo.typeId = -1;
//							stencileInfo.parentId = 0;///////////need to addd
//							stencileInfo.tagStartPos= 0;						
//							stencileInfo.tagEndPos  = 5;
//
//							//PMString d("startIndex : ");
//							//d.AppendNumber(tInfo.startIndex);
//							//d.Append("\n");
//							//d.Append("endIndex : ");
//							//d.AppendNumber( tInfo.endIndex);
//							////CA(d);
//
//							stencileInfo.elementId  = -1;						
//							PMString CellTagName("");
//							CellTagName.Append("ATTRID_");
//							CellTagName.AppendNumber(-1);				
//							PMString TableColName("");
//							TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//							TableColName = keepOnlyAlphaNumeric(TableColName);
//							stencileInfo.TagName = TableColName;
//							stencileInfo.LanguageID =tagStruct.languageID;
//							//stencileInfo.parentId = pNode.getPubId();
//							stencileInfo.parentTypeId = refpNode.getTypeId();
//							stencileInfo.imgFlag = tagStruct.imgFlag; 
//							stencileInfo.sectionID = sectionid; 
//							stencileInfo.isAutoResize = tagStruct.isAutoResize;
//							stencileInfo.col_no = tagStruct.colno;
//							//stencileInfo.tableTypeId = tStruct.typeId;
//							stencileInfo.tableTypeId = tableTypeID;
//
//							stencileInfo.tableId = tagStruct.tableId;
//							stencileInfo.tableType = tagStruct.tableType;
//							stencileInfo.pbObjectId = tagStruct.pbObjectId;
//							stencileInfo.childId = -1;
//							stencileInfo.childTag = -1;
//
//							XMLReference txmlref;
//							//CA(tStruct.tagPtr->GetTagString());		
//							PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//							//CA("TableTagNameNew : " + TableTagNameNew);
//							if( TableTagNameNew.Contains("PSHybridTable") )
//							{
//								//CA("PSTable Found");
//							}
//							else
//							{				
//								TableTagNameNew =  "PSHybridTable";
//							}
//									
//
//					//CA("Before TagTable");
//							iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//						
//					}
//
//					for(int32 k = 0; k < tagListSize; k++)
//					{	
//						//CA("inside Tag list");
//						TagStruct tInfo = tagList[k];
/////////////////////		Added for graphic frame inside hybrid table cell
//						if(tInfo.imgFlag == 1)
//						{
//							InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//							( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//							if(!txtMisSuite)
//							{
//								//CA("!txtMisSuite");
//								break; 
//							}
//
//                            UIDRef ref;
//							bool16 ISTextFrame = txtMisSuite->GetFrameUIDRef(ref);
//							if(ISTextFrame)
//							{
//								int32 start=0;
//
//								InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//								if (tableModel == NULL)
//								{
//									//CA("Err: invalid interface pointer ITableFrame");
//									break;
//								}
//									
//								InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//								if(tableCommands==NULL)
//								{
//									//CA("Err: invalid interface pointer ITableCommands");
//									break;
//								}
//				
//								RowRange rowRange(i , 1) ;
//								ErrorCode result = tableCommands->ResizeRows(rowRange,108);
//								//if(result ==  kFailure)
//									//CA(" unable to resize row");
//								ColRange colRange(j , 1);
//							
//								result = tableCommands->ResizeCols(colRange,108);
//								//if(result ==  kFailure)
//									//CA(" unable to resize Cols");
//				
//								
//								InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
//								if(!pTextSel)
//								{
//									//CA("pTextSel is null");
//									break;
//								}
//								InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
//								if(!tblTxtSel)
//								{
//									//CA("tblTxtSel is null");
//									break;
//								}
//
//								GridAddress  gridAddress(i,j);
//								tblTxtSel->SelectTextInCell( tableModel,gridAddress);
//								InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager)/*,UseDefaultIID()*/);
//								if(!txtSelSuite)
//								{ 
//									//CA("Text Selectin suite is null"); 
//									return;
//								}
//							
//								InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
//								if(!pTextTarget)
//								{ 
//									//CA("pTextTarget is null"); 
//									return;
//								}
//								
//								RangeData rangData=pTextTarget->GetRange();
//								start=rangData.End();
//				
//								start += tInfo.startIndex;
//								/*InterfacePtr<ITextFrame>txtFrame(ref,UseDefaultIID());
//								if(!txtFrame)
//								{
//									CA("Text Frame is null");				
//									break;
//								}*/
//
//								////////////	CS3 Change
//								InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
//								if (graphicFrameHierarchy == nil) 
//								{
//									ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInHybridTable::graphicFrameHierarchy == nil");
//									return ;
//								}
//												
//								InterfacePtr<ITextFrameColumn>txtFrame(graphicFrameHierarchy, UseDefaultIID());
//								if (!txtFrame) {
//									ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::fillDataInHybridTable::textFrame == nil");
//									break ;
//								}
//
//						////////////	End	
//								InterfacePtr<ITextModel>txtModel(txtFrame->QueryTextModel());
//								if(!txtModel)
//								{
//									CA("Text Model is null");				
//									break;
//								}
//								
//								UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
//								this->InsertInline(textStoryUIDRef,start);
//						
//								InterfacePtr<IDataSprayer> iDataSprayer(static_cast<IDataSprayer*> (CreateObject(kDataSprayerBoss,IID_IDataSprayer)));
//								if(!iDataSprayer)
//								{	
//									//CA("!iDtaSprayer");
//									break;
//								}
//								
//								SlugStruct stencileInfo;
//								stencileInfo.whichTab   = tInfo.whichTab;
//								stencileInfo.typeId = tInfo.typeId;
//								stencileInfo.parentId = tInfo.parentId ;
//								stencileInfo.tagStartPos= tInfo.startIndex;						
//								stencileInfo.tagEndPos  = tInfo.endIndex;
//								stencileInfo.elementId  = tInfo.elementId;						
//								PMString CellTagName("");
//								CellTagName.Append("ATTRID_");
//								CellTagName.AppendNumber(stencileInfo.elementId);				
//								PMString TableColName("");
//								TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//								TableColName = keepOnlyAlphaNumeric(TableColName);
//								stencileInfo.TagName = TableColName;
//								stencileInfo.LanguageID =tagStruct.languageID;
//								stencileInfo.parentTypeId = tInfo.parentTypeID;
//								stencileInfo.imgFlag = tInfo.imgFlag ; 
//								stencileInfo.sectionID = sectionid; 
//								stencileInfo.isAutoResize = tagStruct.isAutoResize;
//								stencileInfo.col_no = tagStruct.colno;
//								//stencileInfo.tableTypeId = tStruct.typeId;
//								stencileInfo.tableTypeId = tableTypeID;
//								
//								stencileInfo.tableId = tagStruct.tableId;
//								stencileInfo.tableType = tagStruct.tableType;
//								stencileInfo.pbObjectId = tagStruct.pbObjectId;
//								stencileInfo.childId = tInfo.childId;
//								stencileInfo.childTag = tInfo.childTag;
//
//								XMLReference txmlref;
//								//CA(tStruct.tagPtr->GetTagString());		
//								PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//								//CA("TableTagNameNew : " + TableTagNameNew);
//								if( TableTagNameNew.Contains("PSHybridTable") )
//								{
//									//CA("PSTable Found");
//								}
//								else
//								{				
//									TableTagNameNew =  "PSHybridTable";
//								}
//								
//								TagStruct tagInfo;
//								tagInfo.whichTab     = tInfo.whichTab;
//								tagInfo.typeId       = tInfo.typeId;
//								tagInfo.parentId     = tInfo.parentId;
//								tagInfo.startIndex   = tInfo.startIndex;
//								tagInfo.endIndex     = tInfo.endIndex;
//								tagInfo.elementId    = tInfo.elementId;
//								tagInfo.languageID   = tagStruct.languageID;
//								tagInfo.parentTypeID = tInfo.parentTypeID;
//								tagInfo.imgFlag		 = tInfo.imgFlag;
//								tagInfo.sectionID	 = sectionid;
//								tagInfo.isAutoResize = tagStruct.isAutoResize;
//								tagInfo.colno		 = tagStruct.colno;
//								tagInfo.tagPtr		 = tagStruct.tagPtr;	
//
//								tagInfo.tableId = tagStruct.tableId;
//								tagInfo.tableType = tagStruct.tableType;
//								tagInfo.pbObjectId = tagStruct.pbObjectId;
//								tagInfo.childId = tInfo.childId;
//								tagInfo.childTag = tInfo.childTag;
//								
//								XMLReference xmlref;
//								
//				
//								if(tInfo.whichTab == 4)
//								{
//									//CA("Spraying item image");
//									iDataSprayer->sprayItemImageInsideHybridTable(newFrameUIDRef, tagInfo, tagInfo.parentId );
//								}
//								if(tInfo.whichTab == 3)
//									iDataSprayer->fillImageInBox(newFrameUIDRef , tagInfo, tagInfo.parentId );
//							
//								iDataSprayer->fitInlineImageInBoxInsideTableCell(newFrameUIDRef);
//								iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//							}	
///////////////////////			End 
//						}
//						else
//						{	
//							//CA("Inside else ");
//							SlugStruct stencileInfo;
//							stencileInfo.whichTab   = tInfo.whichTab;
//							stencileInfo.typeId = tInfo.typeId;
//							stencileInfo.parentId = objectId;
//							stencileInfo.tagStartPos= tInfo.startIndex;						
//							stencileInfo.tagEndPos  = tInfo.endIndex;
//
//							PMString d("startIndex : ");
//							d.AppendNumber(tInfo.startIndex);
//							d.Append("\n");
//							d.Append("endIndex : ");
//							d.AppendNumber( tInfo.endIndex);
//							//CA(d);
//
//							stencileInfo.elementId  = tInfo.elementId;						
//							PMString CellTagName("");
//							CellTagName.Append("ATTRID_");
//							CellTagName.AppendNumber(stencileInfo.elementId);				
//							PMString TableColName("");
//							TableColName.Append("ATTRID_ATCD");//Advance Table Cell Data
//							TableColName = keepOnlyAlphaNumeric(TableColName);
//							stencileInfo.TagName = TableColName;
//							stencileInfo.LanguageID =tagStruct.languageID;
//							//stencileInfo.parentId = pNode.getPubId();
//							stencileInfo.parentTypeId = refpNode.getTypeId();
//							stencileInfo.imgFlag = tagStruct.imgFlag; 
//							stencileInfo.sectionID = sectionid; 
//							stencileInfo.isAutoResize = tagStruct.isAutoResize;
//							stencileInfo.col_no = tagStruct.colno;
//							//stencileInfo.tableTypeId = tStruct.typeId;
//							stencileInfo.tableTypeId = tableTypeID;
//
//							stencileInfo.tableId = tagStruct.tableId;
//							stencileInfo.tableType = tagStruct.tableType;
//							stencileInfo.pbObjectId = tagStruct.pbObjectId;
//							stencileInfo.childId = tInfo.childId;
//							stencileInfo.childTag = tInfo.childTag;
//
//							XMLReference txmlref;
//							//CA(tStruct.tagPtr->GetTagString());		
//							PMString TableTagNameNew = tagStruct.tagPtr->GetTagString();
//							//CA("TableTagNameNew : " + TableTagNameNew);
//							if( TableTagNameNew.Contains("PSHybridTable") )
//							{
//								//CA("PSTable Found");
//							}
//							else
//							{				
//								TableTagNameNew =  "PSHybridTable";
//							}
//									
//
//					//CA("Before TagTable");
//							iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i,j,tableId );
//						}
//						//CA("After TagTable");
//					}
//					//j++;					
//				}					
//				i++;
//				j=0;				
//			}						
//			//******
//			for(it6 = vec_vecCAdvanceTableCellValueBody.begin(); it6 != vec_vecCAdvanceTableCellValueBody.end(); it6++)
//			{
//				vec_CAdvannceTableCellBobyValue = *it6;
//				for(it5 = vec_CAdvannceTableCellBobyValue.begin(); it5 != vec_CAdvannceTableCellBobyValue.end(); it5++)
//				{					
//					/////merge cell here
//
//					CAdvanceTableCellValue objCAdvanceTableCellValue = *it5;
//										
//					bool16 isMerge = kFalse;
//
//					if(objCAdvanceTableCellValue.rowStartId == 0 && objCAdvanceTableCellValue.colStartId ==0)
//						continue;
//
//					RowRange rowRange(objCAdvanceTableCellValue.rowStartId, 1);
//					if(!AddHeaderToTable)
//						rowRange.start = objCAdvanceTableCellValue.rowStartId - headerRows;	
//					ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//					if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//					{
//						isMerge = kTrue;
//						if(AddHeaderToTable)
//							rowRange.Set(objCAdvanceTableCellValue.rowStartId , objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//						else
//							rowRange.Set(objCAdvanceTableCellValue.rowStartId - headerRows, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//					}
//					if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					{
//                        isMerge = kTrue;
//						colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId  - objCAdvanceTableCellValue.colStartId + 1);
//		}		
//					if(isMerge)
//					{
//						//CA("Going for Merge Cell");
//						GridArea gridArea(rowRange, colRange);
//						ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//					}
//					//CA("end of for 2");
//				}
//			}
//		}
//			//****
//		}
//		
	}while(kFalse);
}

void TableUtility::putHeaderDataInHybridTable(const UIDRef& tableUIDRef, vector<vector<PMString> > vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef , vector<vector<CAdvanceTableCellValue> > &vec_vecCAdvanceTableCellValueHeader )
{
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//
//		InterfacePtr<ITableUtility> iTableUtlObj
//		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
//		if(!iTableUtlObj)
//		{
//			// CA("!iTableUtlObj");
//			ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
//			return ;
//		}
//
//		int32 i = 0;
//		int32 j = 0;
//
//		int32 p =0;
//		int32 q =0;
//	
//		vector<vector<PMString> >::iterator it1;
//		for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
//		{			
//			vector < PMString > vec_tableHeaderRow;			
//			vec_tableHeaderRow = (*it1);
//			vector < PMString >::iterator it2;
//		
//			for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
//			{	
//				PMString dispname = (*it2);
//				//CA(dispname);
//				InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//				if (tableModel == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableFrame");
//					break;
//				}
//
//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//				if(tableCommands == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableCommands");
//					break;
//				}
//				//WideString* wStr=new WideString(dispname);
//				const WideString wStr(dispname); 
//
//				if(isTranspose )
//				{
//					tableCommands->SetCellText(wStr, GridAddress(i,j));
//					SlugStruct stencileInfo;
//					//stencileInfo.elementId = vec_tableheaders[i];
//					/*CAttributeValue oAttribVal;
//					oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
//					PMString dispname = oAttribVal.getDisplayName();*/
//					//PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );// changes is required language ID req.
//					//CA("Inside PutHeaderDataIN Table");	CA(dispname);
//					//stencileInfo.elementName = dispname;
//					PMString TableColName("");//oAttribVal.getTable_col();
//					TableColName.Append("ATTRID_ATCD");
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID = tStruct.languageID;
//					stencileInfo.typeId  = -1;  // TypeId is hardcodded to -2 for Header Elements.
//					stencileInfo.header  = 1;
//					stencileInfo.whichTab = tStruct.whichTab;
//					stencileInfo.parentId = tStruct.parentId;//pNode.getPubId();  // -2 for Header Element
//					stencileInfo.parentTypeId = tStruct.parentTypeID; //pNode.getTypeId();
//					//4Aug..ItemTable
//					//stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
//					stencileInfo.imgFlag = tStruct.imgFlag; 
//					stencileInfo.sectionID = tStruct.sectionID;
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					
//					stencileInfo.tableId = tStruct.tableId;
//					stencileInfo.tableType = tStruct.tableType;
//					stencileInfo.pbObjectId = tStruct.pbObjectId;
//
//					XMLReference txmlref;
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					if( TableTagNameNew.Contains("PSHybridTable") )
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSHybridTable";
//					}
//					iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j, 0 );
//					i++;
//				}
//				else
//				{	
//					CAdvanceTableCellValue objCAdvanceTableCellValue = vec_vecCAdvanceTableCellValueHeader[p][q];
//					
//					//CA("test1");
//					//bool16 isMerge = kFalse;
//					//RowRange rowRange(objCAdvanceTableCellValue.rowStartId , 1);
//					//ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//					//if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//					//{
//					//	isMerge = kTrue;
//					//	rowRange.Set(objCAdvanceTableCellValue.rowStartId, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//					//
//					//}
//					//if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					//{
//                    //   isMerge = kTrue;
//					//	colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId + 1);
//					//
//					//}
//
//					//if(isMerge)
//					//{
//					//	//CA("Going for Merge Cell");
//					//	GridArea gridArea(rowRange, colRange);
//					//	ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//					//
//					//}
//
//					tableCommands->SetCellText(wStr, GridAddress(objCAdvanceTableCellValue.rowId, objCAdvanceTableCellValue.colId));
//					SlugStruct stencileInfo;			
//					stencileInfo.elementName = prepareTagName(dispname);
//					PMString TableColName("");// oAttribVal.getTable_col();
//					TableColName.Append("ATTRID_ATCD");
//					stencileInfo.TagName = TableColName;
//					stencileInfo.LanguageID =tStruct.languageID;
//					stencileInfo.typeId  = -1; //vec_items[i];
//					stencileInfo.header  = 1;
//					stencileInfo.parentId = tStruct.parentId;//pNode.getPubId() ;  // -2 for Header Element
//					stencileInfo.parentTypeId = tStruct.parentTypeID; //pNode.getTypeId();
//					//4Aug..ItemTable
//					//	stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
//					stencileInfo.whichTab = tStruct.whichTab; 
//					stencileInfo.imgFlag = tStruct.imgFlag; 
//					stencileInfo.isAutoResize = tStruct.isAutoResize;
//					stencileInfo.col_no = tStruct.colno;
//					stencileInfo.tableTypeId = tStruct.typeId;
//					//stencileInfo.tableTypeId = tableTypeID;
//					stencileInfo.sectionID = tStruct.sectionID;
//
//					stencileInfo.tableId = tStruct.tableId;
//					stencileInfo.tableType = tStruct.tableType;
//					stencileInfo.pbObjectId = tStruct.pbObjectId;
//
//					XMLReference txmlref;
//					//CA(tStruct.tagPtr->GetTagString());
//					//CA(stencileInfo.colName);				
//					PMString TableTagNameNew = tStruct.tagPtr->GetTagString();
//					//CA("TableTagNameNew : " + TableTagNameNew);
//					if( TableTagNameNew.Contains("PSHybridTable") )
//					{
//						//CA("PSTable Found");
//					}
//					else
//					{				
//						TableTagNameNew =  "PSHybridTable";
//						//CA("PSTable Not Found : "+ TableTagNameNew);
//					}
//					iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(),TableTagNameNew, txmlref, stencileInfo, objCAdvanceTableCellValue.rowId, objCAdvanceTableCellValue.colId, 0 );
//					i++;
//
//					if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					{
//						for(int z= 0; z < (objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId); z++ )
//						{
//							//CA("q incremented for Merge");
//						//	q++;
//						//	it2++;
//						}
//					}					
//					q++;
//
//				}
//				//delete wStr;
//			}
//			i=0;
//			j++;
//			q =0;
//			p++;
//		}
//
//		//****
//		 i = 0;
//		 j = 0;
//
//		 p =0;
//		 q =0;
//	
////		vector<vector<PMString> >::iterator it1;
//		for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
//		{			
//			vector < PMString > vec_tableHeaderRow;			
//			vec_tableHeaderRow = (*it1);
//			vector < PMString >::iterator it2;
//		
//			for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
//			{	
//				InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
//				if (tableModel == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableFrame");
//					break;
//				}
//
//				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//				if(tableCommands == NULL)
//				{
//					//CA("Err: invalid interface pointer ITableCommands");
//					break;
//				}
//				if(isTranspose )
//				{					
//				}
//				else
//				{	
//					CAdvanceTableCellValue objCAdvanceTableCellValue = vec_vecCAdvanceTableCellValueHeader[p][q];
//					
//					//CA("test1");
//					bool16 isMerge = kFalse;
//					RowRange rowRange(objCAdvanceTableCellValue.rowStartId , 1);
//					ColRange colRange(objCAdvanceTableCellValue.colStartId, 1);
//					if(objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId > 0)
//					{
//						isMerge = kTrue;
//						rowRange.Set(objCAdvanceTableCellValue.rowStartId, objCAdvanceTableCellValue.rowEndId - objCAdvanceTableCellValue.rowStartId + 1);
//
//					}
//					if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					{
//                        isMerge = kTrue;
//						colRange.Set(objCAdvanceTableCellValue.colStartId, objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId + 1);
//					}
//
//					if(isMerge)
//					{
//						//CA("Going for Merge Cell");
//						GridArea gridArea(rowRange, colRange);
//						ErrorCode errorCode = tableCommands->MergeCells(gridArea);
//
//					}
//
//					i++;
//
//					if(objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId > 0)
//					{
//						for(int z= 0; z < (objCAdvanceTableCellValue.colEndId - objCAdvanceTableCellValue.colStartId); z++ )
//						{
//							//CA("q incremented for Merge");
//						//	q++;
//						//	it2++;
//						}
//					}					
//					q++;
//				}				
//			}
//			i=0;
//			j++;
//			q =0;
//			p++;
//		}	
//		//*****
//
//	}while(kFalse);
}

void TableUtility::resizeTableForHybridTable
(const UIDRef& tableUIDRef)
{
	do
	{
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		InterfacePtr<ITableCommands> iTableCommands(tableModel, UseDefaultIID());
		ASSERT(iTableCommands);
		if(iTableCommands == nil) 
		{
			break;
		}
		

		ITableModel::const_iterator iterTable(tableModel->begin());
		ITableModel::const_iterator end(tableModel->end());

		while(iterTable != end) 
		{
			GridAddress gridAddress = *iterTable;			
			
			GridArea gridArea = tableModel->GetCellArea(gridAddress);
			iTableCommands->ClearContent(gridArea);
			iTableCommands->UnmergeCell(gridAddress);
			// goto to the next grid address
			++iterTable;
		}

		int32 presentTotalRows =tableModel->GetTotalRows().count;
		int32 presentBodyRows =tableModel->GetBodyRows().count;
		int32 presentHeaderRows =tableModel->GetHeaderRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;

		if(presentBodyRows > 1)
				iTableCommands->DeleteRows(RowRange((presentHeaderRows+1), (presentTotalRows- (presentHeaderRows+1))));

		if(presentCols>1)
		{				
			iTableCommands->DeleteColumns(ColRange(1, presentCols-1));
		}
	
	}while(0);

	//	int32 ColWidth;
	//	int32 RowHt;
	//	
 //       PMReal ColumnWidth(ColWidth);
	//	PMReal RowHeight(RowHt);

	//	int32 presentTotalRows =tableModel->GetTotalRows().count;
	//	int32 presentBodyRows =tableModel->GetBodyRows().count;
	//	int32 presentHeaderRows =tableModel->GetHeaderRows().count;
	//	
	//	int32 presentCols = tableModel->GetTotalCols().count;

	//	if(tagStruct.header == -1 && presentHeaderRows == 0)
	//	{
	//		int32 HeaderRowCount = 0;
	//		//int32 count = tagStruct.tagPtr->GetChildCount();
	//		XMLReference XMLRef = tagStruct.tagPtr->GetNthChild(0);
	//		//IIDXMLElement* xmlElementOfTable = XMLRef.Instantiate(); //PSHybridTable
	//		InterfacePtr<IIDXMLElement>xmlElementOfTable(XMLRef.Instantiate());

	//		int32 cellTagCount = xmlElementOfTable->GetChildCount();

	//		for(int32 index = 0 ; index < cellTagCount; index++)
	//		{
	//			XMLReference cellXMLRef = xmlElementOfTable->GetNthChild(index);
	//			//IIDXMLElement* xmlElementOfCell = cellXMLRef.Instantiate();
	//			InterfacePtr<IIDXMLElement>xmlElementOfCell(cellXMLRef.Instantiate());
	//			
	//			int32 tagCount = xmlElementOfCell->GetChildCount();
	//			if(tagCount <= 0)
	//				continue;

	//			XMLReference childXMLRef = xmlElementOfCell->GetNthChild(0);//get first tag inside cell
	//			//IIDXMLElement* xmlElementOfChild = childXMLRef.Instantiate();
	//			InterfacePtr<IIDXMLElement>xmlElementOfChild(childXMLRef.Instantiate());

	//			PMString headerStr = xmlElementOfChild->GetAttributeValue(WideString("header"));
	//			//CA("headerStr = " + headerStr);
	//			int32 header = headerStr.GetAsNumber();

	//			if(header == 1)
	//			{
	//				PMString rowNoStr = xmlElementOfChild->GetAttributeValue(WideString("rowno"));
	//				//CA("rowNoStr = " + rowNoStr);
	//				int32 rowNo = rowNoStr.GetAsNumber();
	//				if(HeaderRowCount < rowNo)
	//				{
	//					HeaderRowCount = rowNo;
	//				}
	//			}
	//			else
	//				break;//header row completed
	//		}

	//		presentHeaderRows = HeaderRowCount + 1;
	//		presentBodyRows = presentBodyRows - presentHeaderRows;
	//	}
	//
	//	/*PMString Temp("presentHeaderRows = ");
	//	Temp.AppendNumber((presentHeaderRows));
	//	Temp.Append("\n");
	//	Temp.Append(" , HeaderRows = ");
	//	Temp.AppendNumber(HeaderRows);
	//	Temp.Append("\n");
	//	Temp.Append(" , presentBodyRows = ");
	//	Temp.AppendNumber(presentBodyRows);
	//	CA(Temp);*/



	//	

	//	InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
	//	if(tableCommands==NULL)
	//		break;

	//	if(isTranspose)
	//	{
	//		if(AddHeaderToTable)
	//		{
	//			if((numRows + HeaderRows) > presentCols)
	//				result = tableCommands->InsertColumns(ColRange(presentCols-1,numRows + HeaderRows - presentCols), Tables::eAfter, 0/*PMReal(100.0)*//*ColumnWidth*/);
	//			if((numRows + HeaderRows) < presentCols)
	//				result = tableCommands->DeleteColumns(ColRange(numRows + HeaderRows-1, presentCols - (numRows + HeaderRows)));
	//		}
	//		else
	//		{
	//			if(numRows > presentCols)
	//				result = tableCommands->InsertColumns(ColRange(presentCols-1,numRows - presentCols), Tables::eAfter, 0/*PMReal(100.0)*//*ColumnWidth*/);
	//			if(numRows < presentCols)
	//				result = tableCommands->DeleteColumns(ColRange(numRows-1, presentCols-numRows));
	//		}

	//		if(numCols > presentTotalRows)
	//		{
	//			result = tableCommands->InsertRows(RowRange(presentTotalRows-1, numCols-presentTotalRows), Tables::eAfter, 0/*PMReal(15.0)*//*RowHeight*/);
	//		}
	//		if(numCols < presentTotalRows)
	//		{
	//			result = tableCommands->DeleteRows(RowRange(numCols-1, presentTotalRows-numCols));
	//		}			
	//	}
	//	else
	//	{
	//		if(AddHeaderToTable)
	//		{
	//			if(presentHeaderRows > HeaderRows)
	//			{
	//				result = tableCommands->DeleteRows(RowRange(0, presentHeaderRows - HeaderRows));
	//			}
	//			if(presentHeaderRows < HeaderRows)
	//			{
	//				result = tableCommands->InsertRows(RowRange(presentHeaderRows -1, HeaderRows - presentHeaderRows), Tables::eAfter, 0);
	//			}
	//			if(presentBodyRows < numRows)
	//			{				
	//				result = tableCommands->InsertRows(RowRange(presentBodyRows + HeaderRows -1, numRows - presentBodyRows), Tables::eAfter, 0);
	//			}
	//			if(presentBodyRows > numRows)
	//			{				
	//				result = tableCommands->DeleteRows(RowRange(numRows + HeaderRows -1, presentBodyRows - numRows));
	//			}
	//		}
	//		else
	//		{
	//			/*if(presentHeaderRows > 0) 
	//			{
	//				result = tableCommands->DeleteRows(RowRange(0, presentHeaderRows));
	//			}
	//			if(presentBodyRows < numRows)
	//			{				
	//				result = tableCommands->InsertRows(RowRange(presentBodyRows -1 , numRows - presentBodyRows), Tables::eAfter, 0);
	//			}
	//			if(presentBodyRows > numRows)
	//			{				
	//				result = tableCommands->DeleteRows(RowRange(numRows -1, presentBodyRows - numRows));
	//			}*/

	//			if(presentTotalRows < numRows)
	//			{				
	//				result = tableCommands->InsertRows(RowRange(presentTotalRows -1 , numRows - presentTotalRows), Tables::eAfter, 0);
	//			}
	//			if(presentTotalRows > numRows)
	//			{				
	//				result = tableCommands->DeleteRows(RowRange(numRows -1, presentTotalRows - numRows));
	//			}
	//		}

	//		if(presentCols<numCols)
	//		{
	//			//CA("\n presentCols<numCols ");
	//			//*****Added By Sachin Sharma 17/06/08
	//			for(int rowIndex = 0 ; rowIndex < numRows  ; rowIndex++)
	//			{
	//				for(int colIndex = 0;colIndex < numCols;colIndex++)
	//				{													
	//					if(!AddHeaderToTable )
	//					{
	//						//CA("In My Coundition");
	//						tableModel->Clear(GridArea(RowRange(rowIndex,1),ColRange(colIndex,1)));	
	//					}
	//					
	//				}
	//			}
	//			//****Added up to here
	//			result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, 0/*PMReal(30.0)*//*ColumnWidth*/);
	//		}
	//		else if(presentCols>numCols)
	//		{
	//			//CA("\n presentCols>numCols ");
	//			result = tableCommands->DeleteColumns(ColRange(numCols-1, presentCols-numCols));
	//		}

	//		/*for(int rowIndex = 0 ; rowIndex < numRows + HeaderRows ; rowIndex++)
	//		{
	//			for(int colIndex = 0;colIndex < numCols;colIndex++)
	//			{
	//				GridCoord rowGridCoord = rowIndex;
	//				GridCoord colGridCoord = colIndex;
	//				GridAddress gridAddr(rowGridCoord,colGridCoord);					
	//				tableModel->UnmergeCell(gridAddr);					
	//			}
	//		}*/
	//	}			
	//}
	//while(kFalse);
} 

ErrorCode TableUtility::InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex)	
{
	ErrorCode status = kFailure;
	//CmdUtils::SequencePtr cmdSeq(status);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do 
	{
		newFrameUIDRef = UIDRef::gNull;
		status = this->CreateFrame(storyUIDRef.GetDataBase(), newFrameUIDRef);
		if (status != kSuccess) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::InsertInline::status != kSuccess");
			break;
		}
		status = this->ChangeToInline(storyUIDRef, whereTextIndex, newFrameUIDRef);
		if (status != kSuccess) 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::InsertInline::status != kSuccess");
		
		
	} while (false);
	//cmdSeq.SetState(status);
	return status;
}

ErrorCode TableUtility::CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef)
{
	// You can make any type of frame into an inline. Here we make a new graphic frame.	
	PMRect bounds(0, 0, 100, 100);
	SDKLayoutHelper layoutHelper;
	newFrameUIDRef = layoutHelper.CreateRectangleFrame(UIDRef(database, kInvalidUID), bounds);
	if (newFrameUIDRef)
		return kSuccess;
	else
		return kFailure;
}

ErrorCode TableUtility::ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do {
		// Validate parameters.
		InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
		if(!textModel) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::!textModel");		
			break;
		}
		InterfacePtr<IGeometry> pageItemGeometry(frameUIDRef, UseDefaultIID());
		if (pageItemGeometry == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::pageItemGeometry == nil");		
			break;
		}

		// Insert character in text flow to anchor the inline.
		boost::shared_ptr<WideString>	insertMe(new WideString);

		insertMe->Append(kTextChar_Inline); 
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
		InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(whereTextIndex, insertMe));
		if(!insertTextCmd) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::insertTextCmd== nil");		
			break;
		}
		status = CmdUtils::ProcessCommand(insertTextCmd);
		if(status != kSuccess) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::status != kSuccess");		
			break;
		}

		// Change the page item into an inline.
		InterfacePtr<ICommand> changeILGCmd(CmdUtils::CreateCommand(kChangeILGCmdBoss));
		if (changeILGCmd == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::changeILGCmd == nil");		
			break;
		}
		InterfacePtr<IRangeData> rangeData(changeILGCmd, UseDefaultIID());
		if (rangeData == nil)

		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::rangeData == nil");
			break;
		}
		rangeData->Set(whereTextIndex, whereTextIndex);
		InterfacePtr<IUIDData> ilgUIDData(changeILGCmd, UseDefaultIID());
		if (ilgUIDData == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::ChangeToInline::ilgUIDData == nil");		
			break;
		}
		ilgUIDData->Set(frameUIDRef);
		changeILGCmd->SetItemList(UIDList(textModel));
		status = CmdUtils::ProcessCommand(changeILGCmd);
		ASSERT(status == kSuccess);

	} while(kFalse);
	return status;
}

ErrorCode TableUtility::SprayItemAttributeGroupInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo)
{
    //CA("TableUtility::SprayItemAttributeGroupInsideTable");
    
    bool16 isDeleteIfEmpty = kFalse;
    ErrorCode errcode = kFailure;
    ErrorCode result = kFailure;
    vector<double> FinalItemAttributeIds;
    do
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            break;
        }
        
        UID textFrameUID = kInvalidUID;
        InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
        if (graphicFrameDataOne)
        {
            textFrameUID = graphicFrameDataOne->GetTextContentUID();
        }
        
        if (textFrameUID == kInvalidUID)
        {
            break;
        }
        
        InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
        if (graphicFrameHierarchy == nil)
        {
            //CA(graphicFrameHierarchy);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!graphicFrameHierarchy");
            break;
        }
        
        InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
        if (!multiColumnItemHierarchy) {
            //CA(multiColumnItemHierarchy);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!multiColumnItemHierarchy");
            break;
        }
        
        InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
        if (!multiColumnItemTextFrame) {
            //CA(!multiColumnItemTextFrame);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!multiColumnItemTextFrame");
            //CA("Its Not MultiColumn");
            break;
        }
        
        InterfacePtr<IHierarchy>
        frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
        if (!frameItemHierarchy) {
            //CA(ì!frameItemHierarchyî);
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!frameItemHierarchy");
            break;
        }
        
        InterfacePtr<ITextFrameColumn>
        textFrame(frameItemHierarchy, UseDefaultIID());
        if (!textFrame) {
            //CA("!!!ITextFrameColumn");
            ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayItemAttributeGroupInsideTable::!textFrame");
            break;
        }
        
        InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
        if (textModel == NULL)
        {
            break;
        }
        UIDRef StoryUIDRef(::GetUIDRef(textModel));
        
        InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
        if(tableList==NULL)
        {
            break;
        }
        int32	totalNumberOfTablesInsideTextFrame = tableList->GetModelCount();
        
        if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
        {
            break;
        }
        
        for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
        {//for..tableIndex
            InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
            if(tableModel == NULL)
                continue;//continues the for..tableIndex
            
            UIDRef tableRef(::GetUIDRef(tableModel));
            
            XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
            XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();
            
            UIDRef ContentRef = contentRef.GetUIDRef();
            if(ContentRef != tableRef)
            {
                //if the frame contains more than 1 table.Iterate till we get the table specified by slugInfo.
                continue; //continues the for..tableIndex
            }
            int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
            
            int32 totalNumberOfRowsBeforeRowInsertion = 0;
            totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;
            
            GridArea gridAreaForEntireTable;
            
            gridAreaForEntireTable.topRow = 0;
            gridAreaForEntireTable.leftCol = 0;
            gridAreaForEntireTable.bottomRow = 1; //totalNumberOfRowsBeforeRowInsertion;
            gridAreaForEntireTable.rightCol = totalNumberOfColumns;
            
            InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
            if(tableGeometry == NULL)
            {
                //CA("tableGeometry == NULL");
                break;
            }
            
            PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1);
            
            double sectionid = slugInfo.sectionID;
            double parentId = slugInfo.parentId;
            double parentTypeId = slugInfo.parentTypeID;
            double pbObjectId = slugInfo.pbObjectId;
            
            double attributeGroupId = slugInfo.elementId;
            PMString groupKey = slugInfo.groupKey;
            isDeleteIfEmpty = slugInfo.deleteIfEmpty;
            /*
             ////////////////////////////////////
             collect the CellTags from the first row.
             The structure of the TableXMLElementTag is as below
             TableXMLElementTag|(TableXMLElementTag has all printsource attributes.)
             |
             |
             --CellXMLElementTag|(CellXMLElementTag doesn't have any printsource attributes.)
             |
             |
             --TextXMLElementTag  (TextXMLElementTag has all the printsource attributes.)
             We are basically interested in the TextXMLElementTag.
             ////////////////////////////////////////
             */
            
            int32 field_1 = -1;
            PMString attributeVal("");
            
            //rowno of Table tag will now have the number of rows in repetating pattern excluding Header rows.
//            attributeVal.AppendNumber(PMReal(totalNumberOfRowsBeforeRowInsertion));
//            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("rowno"),WideString(attributeVal));
//            attributeVal.Clear();
            
            if(slugInfo.whichTab == 4)
            {
                double languageID = slugInfo.languageID;
                
                vector<double> itemAttrributeList;
                
                //if(groupKey != "")
                //    itemAttrributeList = ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(groupKey, pNode.getPubId());
                //else
                itemAttrributeList = ptrIAppFramework->getItemAttributeListforAttributeGroupIdAndItemId(sectionid, parentId, attributeGroupId, languageID);
                
                if(itemAttrributeList.size() == 0 )
                {
                    PMString astr("");
                    astr.AppendNumber((attributeGroupId));
                    ptrIAppFramework->LogError("No Attributes found for attributeGroupId: " + astr + " & groupKey=" + groupKey );
                    continue;
                }
                
                
                bool16 typeidFound=kFalse;
                
                FinalItemAttributeIds.clear();
                
                if(isDeleteIfEmpty == kTrue)
                {
                    for(int32 index=0; index < itemAttrributeList.size(); index++)
                    {
                        PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parentId,itemAttrributeList.at(index), languageID, sectionid, kFalse );
                        
                        if(itemAttributeValue == "")
                            continue;
                        else
                            FinalItemAttributeIds.push_back(itemAttrributeList.at(index));
                    }
                }
                else
                {
                    FinalItemAttributeIds = itemAttrributeList;
                }
            }
            
            int32 TotalNoOfAttributes =static_cast<int32>(FinalItemAttributeIds.size());
            if(TotalNoOfAttributes <= 0)
            {
                break;
            }
            
            GridArea bodyArea = tableModel->GetBodyArea();
            int32 bodyCellCount = totalNumberOfColumns; // considering 1 row and multiple columns
            
            int32 rowPatternRepeatCount =static_cast<int32>(FinalItemAttributeIds.size());
            
            
            int32 rowsToBeStillAdded = rowPatternRepeatCount - totalNumberOfRowsBeforeRowInsertion;
            
            InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
            if(tableCommands==NULL)
            {
                ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: Err: invalid interface pointer ITableCommands");
                break;
            }
            
            
            for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
            {
                tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);
            }
            
            if(rowPatternRepeatCount > 1)
            {
                
                bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
                if(canCopy == kFalse)
                {
                    ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: CanCopy == kFalse");
                    break;
                }
                TableMemento* tempPtr = tableModel->Copy(gridAreaForEntireTable);
                if(tempPtr == NULL)
                {
                    ptrIAppFramework->LogDebug("AP7_DataSprayer::TableUtility::SprayIndividualItemAndNonItemCopyAttributesInsideTable: tempPtr == NULL");
                    break;
                }
                
                for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
                {
                    tableModel->Paste(GridAddress(pasteIndex * 1,0),ITableModel::eAll,tempPtr); // og
                    tempPtr = tableModel->Copy(gridAreaForEntireTable);
                }
            }
            
            
            
            int32 ColIndex = -1;
            GridArea bodyArea_new = tableModel->GetBodyArea();
            
            ITableModel::const_iterator iterTable(tableModel->begin(bodyArea_new));
            ITableModel::const_iterator endTable(tableModel->end(bodyArea_new));
            
            while (iterTable != endTable)
            {
                GridAddress gridAddress = (*iterTable);
                
                InterfacePtr<ICellContent> cellContent(tableModel->QueryCellContentBoss(gridAddress));
                if(cellContent == nil)
                {
                    break;
                }
                InterfacePtr<IXMLReferenceData> cellXMLReferenceData(cellContent, UseDefaultIID());
                if(!cellXMLReferenceData) {
                    break;
                }
                XMLReference cellXMLElementRef = cellXMLReferenceData->GetReference();
                ++iterTable;
                
                ColIndex++;
                int32 itemIDIndex = ColIndex/bodyCellCount;
                InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
                
                //Note that, cell may be blank i.e doesn't contain any textTag.
                if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
                {
                    continue;
                }
                
                // Checking for number of tag element inside cell
                int32 cellChildCount = cellXMLElementPtr->GetChildCount();
                for(int32 tagIndex1 = 0;tagIndex1 < cellChildCount;tagIndex1++)
                {
                    XMLReference cellTextXMLElementRef = cellXMLElementPtr->GetNthChild(tagIndex1);
                    InterfacePtr<IIDXMLElement>cellTextXMLElementPtr(cellTextXMLElementRef.Instantiate());
                    //Check if the cell is blank.
                    if(cellTextXMLElementPtr == NULL)
                    {
                        continue;
                    }
                    //Get all the elementID and languageID from the cellTextXMLElement
                    //Note ITagReader plugin methods are not used.
                    
                    PMString strElementID = cellTextXMLElementPtr->GetAttributeValue(WideString("ID"));
                    PMString strLanguageID = cellTextXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
                    //check whether the tag specifies ProductCopyAttributes.
                    PMString strHeader = cellTextXMLElementPtr->GetAttributeValue(WideString("header"));
                    
                    int32 header = strHeader.GetAsNumber();
                    double languageID = strLanguageID.GetAsDouble();
                    double elementId = FinalItemAttributeIds[itemIDIndex];
                    
                    int32 tagStartPos = -1;
                    int32 tagEndPos = -1;
                    Utils<IXMLUtils>()->GetElementIndices(cellTextXMLElementPtr,&tagStartPos,&tagEndPos);
                    tagStartPos = tagStartPos + 1;
                    tagEndPos = tagEndPos -1;
                    
                    PMString dispName("");
                    
                    PMString index = cellTextXMLElementPtr->GetAttributeValue(WideString("index"));
                    if(index =="4")
                    {//Item
                        
                        if(header == 1)
                        {
                            dispName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );
                        }
                        else
                        {
                            dispName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(parentId,elementId, languageID, sectionid, kFalse );
                        }
                        
                        
                        PMString attrVal;
                        attrVal.AppendNumber(PMReal(elementId));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("ID"),WideString(attrVal));
                        attrVal.clear();
                        
                        attrVal.Append("-1");
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("dataType"),WideString(attrVal));
                        attrVal.clear();
                        
                        attrVal.Append("-1");
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("tableType"),WideString(attrVal));
                        attrVal.clear();
                        
                    }
                    
                    //added to attach the tag Values to child elements
                    PMString rowValue("");
                    rowValue.AppendNumber(PMReal(gridAddress.row));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("rowno"),WideString(rowValue));
                    
                    PMString colValue("");
                    colValue.AppendNumber(PMReal(gridAddress.col));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextXMLElementRef, WideString("colno"),WideString(colValue));
                    
                    PMString textToInsert("");
                    textToInsert=dispName;
                    
                    textToInsert.ParseForEmbeddedCharacters();
                    boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
                    
                    ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
                }//end for..iterate through each cell each text tag
                
            }//end for..iterate through each column
            
            
            
        }//end for..tableIndex
        errcode = kTrue;
    }while(0);
    
    return errcode;
}
