#ifndef	__TABLEUTILITY_H__
#define	__TABLEUTILITY_H__

#include "VCPluginHeaders.h"
#include "vector"
#include "TagStruct.h"
#include "AdvanceTableScreenValue.h" 

using namespace std;


class TableUtility
{
public:
	bool16 isTablePresent(const UIDRef&, UIDRef&, int32 tableNumber=1);
	void setTableRowColData(const UIDRef&, const PMString&, const int32&, const int32&);
	void fillDataInTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef);
	void putHeaderDataInTable(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef);
	void resizeTable(const UIDRef& tableUIDRef,  int32& numRows,  int32& numCols, bool16 isTranspose);
	//
	void fillDataInTableForItem
	(const UIDRef& tableUIDRef,TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);
	//Added on 9/11 By Dattatray to give refresh functionality For MedCustomTable (condition is TypeId == -111 & whichTab ==3) for ONEsource  
	void fillDataInCMedCustomTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef);

	void fillDataInKitComponentTable
		(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef, bool16 isKitTable);
	void putHeaderDataInKitComponentTable
		(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID);

	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);

	void fillDataInXRefTable(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);

	void putHeaderDataInXRefTable(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID);

	void fillDataInAccessoryTable(const UIDRef& tableUIDRef, double ItemID, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);

	void putHeaderDataInAccessoryTable
		(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, double ItemID);

	void fillDataInHybridTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef);
		
	void putHeaderDataInHybridTable(const UIDRef&, vector<vector<PMString> >, bool16, TagStruct& , UIDRef boxUIDRef, vector<vector<CAdvanceTableCellValue> > &);

	void resizeTableForHybridTable(const UIDRef& /*, const int32&, const int32&, bool16,const int32&,const int32&,const int32&,const TagStruct &*/);

	ErrorCode InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex);
	ErrorCode CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef);
	ErrorCode ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef);
    ErrorCode SprayItemAttributeGroupInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo);
};

#endif
