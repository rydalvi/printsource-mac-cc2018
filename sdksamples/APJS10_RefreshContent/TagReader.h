#ifndef __TAGREADER_H__
#define __TAGREADER_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"

class TagReader
{
public:
	TagList getTagsFromBox(UIDRef, IIDXMLElement ** xmlPtr=nil);
	bool16 GetUpdatedTag(TagStruct&);
	TagList getFrameTags(UIDRef);
private:
	void getTextFrameTags(void);
	void getGraphicFrameTags(void);
	bool16 getCorrespondingTagAttributes(const PMString&, const PMString&, TagStruct&);
	
	TagList tList;
	UIDRef boxUIDRef;
	UID textFrameUID;
	IIDXMLElement* xmlPtr;
};
#endif