#include "VCPluginHeaders.h"
#include "TagStructure.h"

#include "IHierarchy.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextFrame.h"
#include "ISpecifier.h"
#include "IWorkspace.h"
#include "IStyleInfo.h"
#include "IStyleNameTable.h"
#include "ITextAttributes.h"
#include "IBoolData.h"
#include "IIDXMLElement.h"
#include "IXMLreferenceData.h"
#include "IXMLUtils.h"
#include "IXMLTag.h"
#include "IXMLTagList.h"
#include "IXMLStyleToTagMap.h"
#include "IXMLTagToStyleMap.h"
#include "IXMLTagCommands.h"
#include "IXMLElementCommands.h"
#include "IXMLMappingCommands.h"
#include "IXMLAttributeCommands.h"

#include "FrameUtils.h"


class TagWriter
{
public:
	void addTagToGraphicFrame(UIDRef, TagStruct&);
	void addTagToTextBox(UIDRef, TagStruct&);
private:
	PMString RemoveWhileSpace(PMString);
	XMLReference TagFrameElement(const XMLReference&, UID, const PMString&);
	PMString prepareTagName(PMString name);
};


PMString TagWriter::prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.Length(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;
		if(name.GetChar(i) == ' ' )
			tagName.Append("_");
		else tagName.Append(name.GetChar(i));
	}
	return tagName;
}



PMString TagWriter::RemoveWhileSpace(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.Length();i++)
	{
		if(name.GetChar(i) == ' ')
			tagName.Append("_");
		else tagName.Append(name.GetChar(i));
	}
	return tagName;
}

XMLReference TagWriter::TagFrameElement(const XMLReference& newElementParent, UID frameUID, const PMString& frameTagName)
{
	XMLReference resultXMLRef;

	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(frameTagName, frameUID, newElementParent, 0, &resultXMLRef);
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
		{
			ASSERT_FAIL("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
			break;
		}
		if (resultXMLRef == kInvalidXMLReference)
		{
			ASSERT_FAIL("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
			break;
		}
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			ASSERT_FAIL("ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
		}

	}while (false);
	return resultXMLRef;
}

void TagWriter::addTagToGraphicFrame(UIDRef curBox,TagStruct& tStruct)
{
	do{
		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
		if(unknown == nil)
			break;
		
		InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
		if(doc == nil)
			break;
		
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
		if (rootElement == nil)
			break;
				
		XMLReference parent = rootElement->GetXMLReference();
		PMString storyTagName =	RemoveWhileSpace(tStruct.elementName);

		XMLReference storyXMLRef = TagFrameElement(parent, curBox.GetUID(),storyTagName);
		if (storyXMLRef == kInvalidXMLReference)
			break;
		
		PMString attribName("ID");
		PMString attribVal("");
		attribVal.AppendNumber(tStruct.elementId);		
		ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "name";
		attribVal  = tStruct.elementName;
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "typeId";
		attribVal.AppendNumber(tStruct.typeId);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "tableCol";
		attribVal  = tStruct.colName;
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "index";
		attribVal.AppendNumber(tStruct.whichTab);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "imgFlag";
		attribVal.AppendNumber(tStruct.reserved1);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "parentId";
		attribVal.AppendNumber(tStruct.parentId);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "reserved";
		attribVal.AppendNumber(tStruct.reserved2);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);
		
	}while(kFalse);
}


void TagWriter::addTagToTextBox(UIDRef curBox, TagStruct& tStruct)
{
	do{
		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);

		bool16 isTextFrame = FrameUtils::IsTextFrame(unknown);
		if(isTextFrame == 0)
		{
			addTagToGraphicFrame(curBox, tStruct);
			return;
		}
		
		UID textFrameUID = FrameUtils::GetTextFrameUID(unknown);
		if (textFrameUID == kInvalidUID)
			return;
		
		InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
			return;
		
		TextIndex startIndex = textFrame->TextStart();
		TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

		InterfacePtr<ITextModel> txtModel(textFrame->QueryTextModel());
		if (txtModel == nil)
			return;

		int selStart = finishIndex - tStruct.elementName.Length() - 2;
		
		if(selStart < 0)
		{
			selStart = 0;
			finishIndex = tStruct.elementName.Length() +2;
		}

		if(tStruct.elementName.GetChar(0) == '\n')
		{			
			selStart++;
		}

		int32 selEnd = finishIndex;
		// Look for a text selection. Bail if we can't find one.
		UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		// Get the document's root element
		InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
		if (rootElement == nil)
		{
			// This menu item should only appear when there is a document.
			// If the XML plug-in is loaded then all documents should have a root element.
			ASSERT_FAIL("ExpXMLActionComponent::DoTagSelectedText - Nil IIDXMLElement* for document root");
			break;
		}

		// Before tagging the text itself, we have to verify the story itself is tagged.
		InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
		if (storyXMLRefData == nil)
		{
			// XML plug-in should insure that the story has this interface
			ASSERT_FAIL("ExpXMLActionComponent::DoTagSelectedText - nil IXMLReferenceData* from kTextStoryBoss");
			break;
		}
		XMLReference storyXMLRef = storyXMLRefData->GetReference();
		if (storyXMLRef == kInvalidXMLReference)
		{
			XMLReference parent = rootElement->GetXMLReference();
			PMString storyTagName("PRINTsource");
			storyXMLRef = TagFrameElement(parent, txtMdlUIDRef.GetUID(), storyTagName);
			if (storyXMLRef == kInvalidXMLReference)
			{				
				ASSERT_FAIL("ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
				break;
			}
		}	

		PMString myTextElement = prepareTagName(tStruct.elementName);
		XMLReference* newTag = new XMLReference;		
		Utils<IXMLElementCommands>()->CreateElement(myTextElement, txtMdlUIDRef, selStart, selEnd, kInvalidXMLReference, newTag);
	
		PMString attribName("ID");
		PMString attribVal("");
		attribVal.AppendNumber(tStruct.elementId);		
		ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);
		
		attribName.Clear();
		attribVal.Clear();
		attribName = "name";
		attribVal  = tStruct.elementName;
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "typeId";
		attribVal.AppendNumber(tStruct.typeId);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "tableCol";
		attribVal  = tStruct.colName;
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "index";
		attribVal.AppendNumber(tStruct.whichTab);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "imgFlag";
		attribVal.AppendNumber(tStruct.reserved1);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "parentId";
		attribVal.AppendNumber(tStruct.parentId);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,attribName,attribVal);

		attribName.Clear();
		attribVal.Clear();
		attribName = "reserved";
		attribVal.AppendNumber(tStruct.reserved2);

	} while (false);
}
