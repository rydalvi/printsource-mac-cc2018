#include "VCPlugInHeaders.h"	

#include "ILayoutTarget.h"

#include "ITextMiscellanySuite.h"
#include "CmdUtils.h"
//#include "ISpecifier.h"			//It is removed from CS3 API
#include "IPageItemUtils.h" //cs4
#include "ITextTarget.h"
#include "ITextModel.h"
#include "IFrameList.h"
//#include "ITextFrame.h"			//It is removed from CS3 API
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "ITextFocus.h"////can be deleted
#include "CAlert.h"
#include "ITextFrameColumn.h"
#include "ITOPFrameData.h"
#include "IHierarchy.h"
#include "ITextUtils.h"
#include "RfhID.h"


//Alert Macros.////////////
inline PMString numToPMString1(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("TextMiscellanySuiteLayoutCSB.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString1(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////


class TextMiscellanySuiteLayoutCSB : public CPMUnknown<ITextMiscellanySuite>
{
public:
	TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss);

	virtual	~TextMiscellanySuiteLayoutCSB(void);

//	virtual bool16 GetCurrentSpecifier(ISpecifier* & );        //CS3 change

	virtual bool16 GetUidList(UIDList &);
	virtual bool16 GetFrameUIDRef(UIDRef & frameUIDRef);
	virtual bool16 GetCaretPosition(TextIndex &pos);
	virtual bool16 GetTextSelectionRange(TextIndex &start, TextIndex &end);


};
CREATE_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kRFHTextMiscellanySuiteLayoutCSBImpl)

TextMiscellanySuiteLayoutCSB::TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

/* Destructor
*/
TextMiscellanySuiteLayoutCSB::~TextMiscellanySuiteLayoutCSB(void)
{
}

//----------------CS3 Change-----------------------------//
//bool16 TextMiscellanySuiteLayoutCSB::GetCurrentSpecifier(ISpecifier * & Xspec)
//{
//	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
//	const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
//	ISpecifier * spec = PageItemUtils::QuerySpecifier(selectedItems);
//	if(spec )
//	{
//		Xspec = spec;
//		return(1);
//	}
//	return(0);
//}
bool16 TextMiscellanySuiteLayoutCSB::GetUidList(UIDList & TempUidList)
{
	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	const UIDList	selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	TempUidList = selectedItems;	
	return 1;
}

bool16 TextMiscellanySuiteLayoutCSB::GetFrameUIDRef(UIDRef & frameUIDRef)
{
	//CA(__FUNCTION__);
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found ");
		return kFalse;
	}
	else
	{
		//CA("Text Target in suite is not null");
		InterfacePtr<ITextModel>txtModel(txtTarget->QueryTextModel()/*,UseDefaultIID()*/);
		if(txtModel)
		{	//CA("txtModel 1");
				
			InterfacePtr<IFrameList>frameList(txtModel->QueryFrameList()/*,UseDefaultIID()*/);
			if(!frameList)
			{
				CA("!frameList");
				return kTrue;
			}
		/*	InterfacePtr<ITextFrame>textFrame(frameList->QueryNthFrame(0),UseDefaultIID());
			if(!textFrame)
				return kTrue;*/
	//CA("txtModel 1");		
			//InterfacePtr<IDocument>currDoc(Utils<ILayoutUIUtils>()->GetFrontDocument(),UseDefaultIID());
			//if(currDoc == NULL)
			//{
			//	CA("currDoc is NULL");
			//}
			////IDocument *currDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			//frameUIDRef=UIDRef(currDoc->GetDocWorkSpace().GetDataBase(),frameList->/*GetTextModelUID()*/GetNthFrameUID(0));
			//
			//if (frameUIDRef == UIDRef::gNull)
			//{
			//	CA("frameUIDRef is NULL");
			//}
			//	InterfacePtr<ITextFocus>textFocus(txtTarget->QueryTextFocus(),UseDefaultIID());
			//	if(!textFocus)
			//	{
			//		CA("Type Tool Selection not found");				
			//		return kTrue;
			//	}
			//	else
			//	{					
			//		/*PMString ep("End Position: ");
			//		ep.AppendNumber(textFocus->GetEnd());
			//		CA(ep);*/
			//		CA("Inside else of GetFrameUIDRef....");
			//	}

			InterfacePtr<ITextFrameColumn>TextFrameColumnPtr(frameList->QueryNthFrame(0)/*,UseDefaultIID()*/);
			if(!TextFrameColumnPtr)
			{
				//CA("!TextFrameColumnPtr");
				return kTrue;
			}

			// Check for a text frame for text on a path.
			InterfacePtr<ITOPFrameData> topFrameData(TextFrameColumnPtr, UseDefaultIID());
			if (topFrameData != nil) {

				// This is a text on a path text frame. Refer to the
				// spline that the text on a path is associated with.
				frameUIDRef = UIDRef(::GetDataBase(TextFrameColumnPtr), topFrameData->GetMainSplineItemUID());
				return kTrue;
			}

			// Check for a regular text frame
			InterfacePtr<IHierarchy> graphicFrameHierarchy(Utils<ITextUtils>()->QuerySplineFromTextFrame(TextFrameColumnPtr));
			if(graphicFrameHierarchy != nil)
				frameUIDRef = ::GetUIDRef(graphicFrameHierarchy);

           		 
		}

	}
	return kTrue;
}

bool16 TextMiscellanySuiteLayoutCSB::GetCaretPosition(TextIndex &pos)
{
	bool16 result=kFalse;
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found");
	}
	else
	{
		RangeData rangData=txtTarget->GetRange();
		pos=rangData.End();	
		result=kTrue;
	}
	return result;
}


bool16 TextMiscellanySuiteLayoutCSB::GetTextSelectionRange(TextIndex &start, TextIndex &end)
{
	bool16 result=kFalse;
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found");
	}
	else
	{
		RangeData rangData=txtTarget->GetRange();
		end=rangData.End();
		start = rangData.Start(0);		
		result=kTrue;
	}
	return result;
}
