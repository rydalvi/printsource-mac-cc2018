#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "CObserver.h"
#include "RfhID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class kRfhIconWidgetObserver : public CObserver
{
	public:
		kRfhIconWidgetObserver(IPMUnknown *boss);
		~kRfhIconWidgetObserver();
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

	private:
		void handleWidgetHit(InterfacePtr<IControlView>& controlView, const ClassID& theChange, WidgetID widgetID) ;
		void attachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		const PMIID fObserverIID;
};

CREATE_PMINTERFACE(kRfhIconWidgetObserver, kRfhIconWidgetObserverImpl)

kRfhIconWidgetObserver::kRfhIconWidgetObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_IOBSERVER)
{
}

kRfhIconWidgetObserver::~kRfhIconWidgetObserver()
{
}

void kRfhIconWidgetObserver::AutoAttach()
{
	do {
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
	//	AttachWidget(panelControlData, kRfhRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);

	} while(kFalse);
}

void kRfhIconWidgetObserver::AutoDetach()
{
	do {
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
	} while(kFalse);
}

void kRfhIconWidgetObserver::attachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);

	TRACE("TblAttWidgetObserver::AttachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if(!panelControlData) break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
		{
			break;
		}
		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

void kRfhIconWidgetObserver::detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);
	do
	{
		if(panelControlData == nil) break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
		{
			break;
		}
		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (false);
}


void kRfhIconWidgetObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	do 
	{	
		ASSERT(theSubject);
		if(!theSubject) 
		{
			break;
		}
		InterfacePtr<IControlView>  icontrolView(theSubject, UseDefaultIID());
		ASSERT(icontrolView);
		if(!icontrolView) 
		{
			break;
		}
		WidgetID thisID = icontrolView->GetWidgetID();
		if(thisID != kInvalidWidgetID) 
		{
			this->handleWidgetHit(icontrolView, theChange, thisID);
		}
	} while(kFalse);
}

void kRfhIconWidgetObserver::handleWidgetHit(InterfacePtr<IControlView> & controlView, 
										   const ClassID & theChange, 
										   WidgetID widgetID) 
{
	do {
			
		ASSERT(widgetID != kInvalidWidgetID);
		ASSERT(controlView);
		if(controlView==nil) {
			break;

		}
	
	} while(kFalse);
}


