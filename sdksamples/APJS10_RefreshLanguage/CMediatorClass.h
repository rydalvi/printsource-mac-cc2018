#ifndef __CMediatorClass_h__
#define __CMediatorClass_h__

#include "VCPluginHeaders.h"
#include <vector>

using namespace std;
class listBoxData
{
public :

	int32 index;
	double old_AttributeId;
	double replaceWith_AttributeId;
	PMString replaceWith_AttributeName;
};

typedef vector<listBoxData> vec_listBoxData, *vecPTR_ListBoxData;

class CMediatorClass
{
public:
	static double oldAttributeLanguageID;
	static double replaceWithAttributeLanguageID;

	static int32 noOfItemFieldsUpdated;
	static int32 noOfProductFieldsUpdated;
	
	static vecPTR_ListBoxData vecPtr_listBoxData;

	static PMString currentlySelectedAttributeReplaceName;


};




#endif