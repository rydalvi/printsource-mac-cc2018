#ifndef __CommanFunctions_h__
#define	__CommanFunctions_h__


#include "IBookContentMgr.h"
#include "IDocument.h"
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ProgressBar.h"
#include <vector>
#include <set>
#include "TagStruct.h"
#include "IndexReference.h"

using namespace std;

class BookContentDocInfo
{
public:
	PMString DocumentName;
	IDFile DocFile;
	int32 index;
	UID DocUID;	
};

typedef vector< BookContentDocInfo> BookContentDocInfoVector;


class CommonFunctions
{
public:
	void scanBook();
	void startUpdate();
	void StartUpdateJL();
	
private:
	BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
	void scanEachDocument();
	void UpdateDocument(InterfacePtr<IDocument> bgDoc,PMString FileName, RangeProgressBar* ptr=NULL);
	void UpdateTabbedDataWithXML(const UIDRef& BoxUIDRef,PMString FileName,int32 pageNo);
	void replaceOldTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo,double SelectedNewAttrID);
	bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story);
	void updateTheTagWithModifiedAttributeValues(IIDXMLElement * &XMLElementTagPtr,double newAttrID);
	void UpdateTabelDataWithXML(InterfacePtr<ITableModelList>& tableList,const UIDRef& TextUIDRef,PMString FileName,int32 pageNo);
	void replaceOldProductTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo,double SelectedNewAttrID);
	ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text);
	void AddKeysWithoutSectionIds(IIDXMLElement* xmlElement, set<PMString>& resultIds);
	void RefreshJL(IIDXMLElement* xmlElement,
									   vector<IndexReference> &indexReferences,
									   bool isTable, set<WideString> &childIds, bool isChild, WideString oldLanguageId, WideString newLanguageId,
									   set<WideString> itemFieldIdsSet, set<WideString> itemGroupFieldIdsSet, set<WideString> sectionFieldIdsSet);


};



#endif
