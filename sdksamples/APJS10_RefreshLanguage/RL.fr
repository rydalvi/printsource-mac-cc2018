//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss

#include "EveInfo.fh"	// Required when using EVE for dialog layout/widget placement

// Project includes:
#include "RLID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kRLPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kRLCurrentMajorFormatNumber, kRLCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kRLVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kRLCompanyValue,			// Company name
	kRLMissingPluginURLValue,	// URL 
	kRLMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs9_refreshlanguage
	 */
	Class
	{
		kRLActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kRLActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/*
	 * This boss class implements the dialog for this plug-in. All
	 * dialogs must implement IDialogController. Specialisation of
	 * IObserver is only necessary if you want to handle widget
	 * changes dynamically rather than just gathering their values
	 * and applying in the IDialogController implementation.
	 * In this implementation IObserver is specialised so that the
	 * plug-in's about box is popped when the info button widget
	 * is clicked.
     *
	 * 
	 * @ingroup apjs9_refreshlanguage
	 */
	Class
	{
		kRLDialogBoss,
			kDialogBoss,
		{
			/** Provides management and control over the dialog.
			*/
			IID_IDIALOGCONTROLLER, kRLDialogControllerImpl,
				/** Allows dynamic processing of dialog changes.
				*/
				IID_IOBSERVER, kRLDialogObserverImpl,
			}

	},
	
	Class
	{
		kRLSelectAttribDialogBoss,
		kDialogBoss,
		{
			/** Provides management and control over the dialog.
			*/
			IID_IDIALOGCONTROLLER, kSelectAttribDialogControllerImpl,
			/** Allows dynamic processing of dialog changes.
			*/
			IID_IOBSERVER, kSelectAttribDialogObserverImpl,
		}

	},
	
	Class
	{
		kRLSelectOtherLanguageAttribDialogBoss,
		kDialogBoss,
		{
			/** Provides management and control over the dialog.
			*/
			IID_IDIALOGCONTROLLER, kSelectOtherLanguageAttribDialogControllerImpl,
			/** Allows dynamic processing of dialog changes.
			*/
			IID_IOBSERVER, kSelectOtherLanguageAttribDialogObserverImpl,
		}

	},
	
	
	Class 
	{
		kRLListBoxWidgetBoss,
		kWidgetListBoxWidgetNewBoss,
		{
			//IID_IOBSERVER,	kRLListBoxObserverImpl,
		}
	},
	
	Class 
	{
		kRLRollOverIconButtonWidgetBoss,
		kRollOverIconButtonBoss,
		{
			/**
			Receive Update messages from the application core when end-user clicks on the eyeball iconic button.
			*/
			//IID_IOBSERVER,	kRLListBoxObserverImpl,//kSelectOtherLanguageAttributeListBoxObserverImpl,;
			//IID_IOBSERVER,	kSelectOtherLanguageAttributeListBoxObserverImpl,
		}
	},
	
	
	/*Class
	{
		kRLIconSuiteWidgetBoss,
		kIconSuiteWidgetBoss,
		{			
		}
	},*/
	
	
	/** 
	Custom picture widget boss class. kPictureWidgetBoss, plus an event handler to catch left-button down evnets.
	@ingroup pictureicon
	*/
	Class 
	{
		kRLCustomIconSuiteWidgetBoss,
		kIconSuiteWidgetBoss,
		{
			/** 
				We're overriding the left-button down method.
			*/
			IID_IEVENTHANDLER, kRLCustomIconSuitWidgetEHImpl,
			/** 
				Flag used to preserve the internal state (selected or otherwise)
			*/
			IID_IBOOLEANCONTROLDATA,kCBooleanControlDataImpl,
			/** 
				This interface has state data consisting of two integers, one for the default
				state and one for the 'selected' or hilight state. Although it seems to be about fonts, it
				isn't specific to fonts and 
				it is appropriate here since we have two int32 resource IDs that we wish to maintain on
				a data interface. This saves us having to write a custom data interface or use two int-data
				interfaces with different IIDs. 
			*/
			IID_IUIFONTSPEC, kUIFontSpecImpl
			// I know they're not fonts.. but we need two integers to be read in... 
			// so it's a hack... but an efficient one.
		}
	},
	
	Class 
	{
		kRLRollOverIconWidgetBoss,
		kIconSuiteWidgetBoss,
		{
			/** 
				We're overriding the left-button down method.
			*/
			IID_IEVENTHANDLER, kRLRollOverIconWidgetEHImpl,
			/** 
				Flag used to preserve the internal state (selected or otherwise)
			*/
			IID_IBOOLEANCONTROLDATA,kCBooleanControlDataImpl,
			/** 
				This interface has state data consisting of two integers, one for the default
				state and one for the 'selected' or hilight state. Although it seems to be about fonts, it
				isn't specific to fonts and 
				it is appropriate here since we have two int32 resource IDs that we wish to maintain on
				a data interface. This saves us having to write a custom data interface or use two int-data
				interfaces with different IIDs. 
			*/
			IID_IUIFONTSPEC, kUIFontSpecImpl
			// I know they're not fonts.. but we need two integers to be read in... 
			// so it's a hack... but an efficient one.
		}
	},

}}};

/*  
 * Implementation definition.
 */
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "RLFactoryList.h"
	}
};

/*  
 * Menu definition.
 */
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About Plug-ins sub-menu item for this plug-in.
		kRefreshByLaanguageActionID //kRLAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		"BookPanelPopup",//kRLAboutMenuPath,			// Menu Path.
		1312, //kSDKDefAlphabeticPosition,			// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag


		// The Plug-ins menu sub-menu items for this plug-in.
	//	kRLDialogActionID,
	//	kRLPluginsMenuPath,
	//	kRLDialogMenuItemPosition,
	//	kSDKDefIsNotDynamicMenuFlag,
		

	}
};

/* 
 * Action definition.
 */
resource ActionDef (kSDKDefActionResourceID)
{
	{
		//kRLActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		//kRLAboutActionID,	// ActionID.
		//kRLAboutMenuKey,	// Sub-menu string.
		//kOtherActionArea,				// Area name (see ActionDefs.h).
		//kNormalAction,					// Type of action (see ActionDefs.h).
		//kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		//kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		//kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.


		kRLActionComponentBoss,
		kRefreshByLaanguageActionID, //kRLDialogActionID,		
		kRLPluginStringKey, //kRLDialogMenuItemKey,		
		kOtherActionArea,			
		kNormalAction,				
		kCustomEnabling, //kDisableIfLowMem,	
		kInvalidInterfaceID,		
		kSDKDefInvisibleInKBSCEditorFlag ,//kSDKDefVisibleInKBSCEditorFlag,


	}
};


/*  
 * Locale Indicies.
 * The LocaleIndex should have indicies that point at your
 * localizations for each language system that you are localized for.
 */

/*  
 * String LocaleIndex.
 */
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

// Strings not being localized
resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
	}
};

/*  
 * Dialog LocaleIndex.
 */
resource LocaleIndex (kSDKDefDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS,	k_Wild, kSDKDefDialogResourceID + index_enUS
	}
};

resource LocaleIndex (kRLDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS,	k_Wild, kRLDialogResourceID + index_enUS
	}
};

resource LocaleIndex (kRLSelectAttributeDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS,	k_Wild, kRLSelectAttributeDialogResourceID + index_enUS
	}
};



resource LocaleIndex (kRLListElementRsrcID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, kRLListElementRsrcID + index_enUS
	}
};




/*  Type definition.
*/
type RLDialogWidget(kViewRsrcType) : DialogBoss(ClassID = kRLDialogBoss)
{
};

type RLSelectAttribDialogWidget(kViewRsrcType):DialogBoss(ClassID = kRLSelectAttribDialogBoss)
{
};

type RLSelectOtherLangAttribDialogWidget(kViewRsrcType):DialogBoss(ClassID = kRLSelectOtherLanguageAttribDialogBoss)
{
};


type RLListBox(kViewRsrcType):WidgetListBoxWidgetN(ClassID = kRLListBoxWidgetBoss)
{ 
};

type RLRollOverIconButtonWidget 	(kViewRsrcType) : RollOverIconButtonWidget (ClassID = kRLRollOverIconButtonWidgetBoss) {};


//type RLIconSuiteWidget(kViewRsrcType) :IconSuiteWidget(ClassID = kRLIconSuiteWidgetBoss){ };




type RLCustomIconSuiteWidget(kViewRsrcType) : IconSuiteWidget(ClassID = kRLCustomIconSuiteWidgetBoss) 
{
	UIFontSpec;	// because we want to read in two integers for the default and hilite states.
};

type RLRollOverIconWidget(kViewRsrcType) : IconSuiteWidget(ClassID = kRLRollOverIconWidgetBoss) 
{
	UIFontSpec;	// because we want to read in two integers for the default and hilite states.
};



/*  Dialog definition.
	This view is not localised: therefore, it can reside here.
	However, if you wish to localise it, it is recommended to locate it in one of
	the localised framework resource files (i.e. RL_enUS.fr etc.) and
	update your Dialog LocaleIndex accordingly.
*/
//**********************************************************
resource PNGA(kPNGRLNextIconRsrcID) "Next_Hover65x22.png" 
resource PNGR(kPNGRLNextIconRollRsrcID)  "Next_65x22.png"

resource PNGA(kPNGRLCancleIconRsrcID)   "Cancel_Hover65x22.png"
resource PNGR(kPNGRLCancleIconRollRsrcID) "Cancel_65x22.png"

resource PNGA(kPNGRLBack1IconRsrcID) "Back_hover65x22.png" 
resource PNGR(kPNGRLBack1IconRollRsrcID)  "Back_65x22.png"

resource PNGA(kPNGRLCancle1IconRsrcID)"Cancel_Hover65x22.png"  
resource PNGR(kPNGRLCancle1IconRollRsrcID)  "Cancel_65x22.png"

resource PNGA(kPNGRLNext1IconRsrcID) "Next_Hover65x22.png"
resource PNGR(kPNGRLNext1IconRollRsrcID)   "Next_65x22.png"

resource PNGA(kPNGRLFinish1IconRsrcID) "finish_hover65x22.png"  
resource PNGR(kPNGRLFinish1IconRollRsrcID) "finish_65x22.png"

resource PNGA(kPNGRLOKIconRsrcID)  "Ok_Hover65x22.png"
resource PNGR(kPNGRLOKIconRollRsrcID) "Ok_65x22.png"

resource PNGA(kPNGRLCancel2IconRsrcID) "Cancel_Hover65x22.png" 
resource PNGR(kPNGRLCancel2IconRollRsrcID)  "Cancel_65x22.png"


//**********************************************************
resource RLDialogWidget (kSDKDefDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kRLDialogWidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	0,0,300,120,				// Frame (l,t,r,b)
	kTrue, kTrue,				// Visible, Enabled
	kRLDialog1TitleKey,			// Dialog name
	{
	//<FREDDYWIDGETDEFLISTUS>
	
		GroupPanelWidget
		(
			// CControlView properties
			kRLGroupPanel1WidgetID, // widget ID
			kPMRsrcID_None, // PMRsrc ID
			kBindNone, // frame binding
			Frame(5.0,5.0,295.0,115.0)  //Frame(5.0,5.0,295.0,95.0) //  left, top, right, bottom
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0, // header widget ID
			{ // CPanelControlData Children
			
				InfoStaticTextWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(15.0,20.0,138.0,40.0) //Frame(15.0,25.0,125.0,45.0)					// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					kRLStaticText1Key,						// Text
					0,
					//0, 
					//1,
					kPaletteWindowSystemScriptFontId, 
					kPaletteWindowSystemScriptFontId,
				),
				DropDownListWidget
				(
					// CControlView properties
					kRLLocaleDropDownWidgetID, // widget ID
					kSysDropDownPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(147.0,20.0,280.0,40.0)  //Frame(130.0,25.0,280.0,45.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// DropDownListControlData properties
					{{				
					}}
				),
		
	//			ButtonWidget
	//			(
	//				// CControlView properties
	//				kRLNextButtonWidgetID, // widget ID
	//				kSysButtonPMRsrcId, // PMRsrc ID
	//				kBindNone, // frame binding
	//				155.0,80.0,220.0,105.0
	//				kTrue, // visible
	//				kTrue, // enabled
	//				// CTextControlData properties
	//				kRLButton1Key, // control label
	//			),
				
				
	//			CancelButtonWidget
	//			(
	//				kCancelButton_WidgetID,	// WidgetID
	//				kSysButtonPMRsrcId,		// RsrcID
	//				kBindNone,				// Binding
	//				80.0,80.0,145.0,105.0			// Frame (l,t,r,b)
	//				kTrue, kTrue,			// Visible, Enabled
	//				kSDKDefCancelButtonApplicationKey,	// Button name
	//				kFalse,					// Change to Reset on option-click.
	//			),
	
				
				RollOverIconButtonWidget
				(
					kRLNextDialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLNextIconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(155,80,220,102)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kRLCancleDialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLCancleIconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(70,80,135,102)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),				
				//-----------		
			}
		),
		
				
		GroupPanelWidget
		(
			// CControlView properties
			kRLGroupPanel2WidgetID, // widget ID
			kPMRsrcID_None, // PMRsrc ID
			kBindNone, // frame binding
			Frame(5.0,5.0,295.0,115.0)  //Frame(5.0,5.0,295.0,95.0) //  left, top, right, bottom
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0, // header widget ID
			{ // CPanelControlData Children
			
				InfoStaticTextWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(15.0,20.0,125.0,40.0) //Frame(15.0,25.0,125.0,45.0)					// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,	kTrue,						// Visible, Enabled
					kRLStaticText9Key,						// Text
					0,
					kPaletteWindowSystemScriptFontId, 
					kPaletteWindowSystemScriptFontId,
				),
				
				
				ClusterPanelWidget
				(
					kRLSecondGroupPanelClusterPanelWidgetID, // widget ID
					kPMRsrcID_None, // PMRsrc ID
					kBindNone, // frame binding
					Frame(130,18,294,42)//  left, top, right, bottom 
					kTrue, // visible
					kTrue, // enabled
					// CTextControlData properties
					"", // control label
					{ // CPanelControlData Children
						
						RadioButtonWidget
						(
							// CControlView properties
							kRLRadio1WidgetID, // widget ID
							kSysRadioButtonPMRsrcId, // PMRsrc ID
							kBindNone, // frame binding
							Frame(2,10,60,30) //  left, top, right, bottom 
							kTrue, // visible
							kTrue, // enabled
							// CTextControlData properties
							kRLRadioButton1Key, // control label
						),
				
						RadioButtonWidget
						(
							// CControlView properties
							kRLRadio2WidgetID, // widget ID
							kSysRadioButtonPMRsrcId, // PMRsrc ID
							kBindNone, // frame binding
							Frame(70,10,140,30) //  left, top, right, bottom 
							kTrue, // visible
							kTrue, // enabled
							// CTextControlData properties
							kRLRadioButton2Key, // control label
						),
					}
				)
				
				ButtonWidget
				(
					// CControlView properties
					kRLBackButtonWidgetID, // widget ID
					kSysButtonPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					150.0-37.5-55.0,85.0,150.0-37.5,105.0
					kTrue, // visible
					kTrue, // enabled
					// CTextControlData properties
					kRLButton2Key, // control label
				),
				
				ButtonWidget
				(
					// CControlView properties
					kRLCancelButton1WidgetID, // widget ID
					kSysButtonPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					150.0 - 27.5 ,85.0,150.0+27.5,105.0
					kTrue, // visible
					kTrue, // enabled
					// CTextControlData properties
					kRLButton3Key, // control label
				),
				
				
				ButtonWidget
				(
					// CControlView properties
					kRLNextButton1WidgetID, // widget ID
					kSysButtonPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					150.0+37.5,85.0,150.0+37.5+55.0,105.0
					kTrue, // visible
					kTrue, // enabled
					// CTextControlData properties
					kRLButton1Key, // control label
				),
			}
		),
		
		
		GroupPanelWidget
		(
			// CControlView properties
			kRLGroupPanel3WidgetID, // widget ID
			kPMRsrcID_None, // PMRsrc ID
			kBindNone, // frame binding
			Frame(5.0,5.0,295.0,115.0)  //Frame(5.0,5.0,295.0,95.0) //  left, top, right, bottom
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0, // header widget ID
			{ // CPanelControlData Children
			
				/*StaticTextWidget
				(
					// CControlView properties
					kInvalidWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(15.0,20,200.0,40.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize, // Ellipsize style
					// CTextControlData properties
					"No. of Item fields updated", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),*/
				
				InfoStaticTextWidget
				(
					kStaticText1WidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindNone,					// Frame binding
					Frame(10.0,20,224.0,40.0)						// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",										// Text
					0,
					0, 
					1,
				),
				

				StaticTextWidget
				(
					// CControlView properties
					kColonWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(225.0,20.0,234.0,40.0)//  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					kRLStaticText8key, //" : ", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
		
				

				
				
				StaticTextWidget
				(
					// CControlView properties
					kItemsRefreshedWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(248.0,20.0,289.0,40.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					"0", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				
				
				/*StaticTextWidget
				(
					// CControlView properties
					kInvalidWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(15.0,50,200.0,70.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize, // Ellipsize style
					// CTextControlData properties
					"No. of Product fields updated", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),*/
				
				InfoStaticTextWidget
				(
					kStaticText2WidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindNone,					// Frame binding
					Frame(10.0,50,224.0,70.0)						// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",										// Text
					0,
					0, 
					1,
				),
				
				StaticTextWidget
				(
					// CControlView properties
					kColonWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(225.0,50.0,234.0,70.0)//  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					kRLStaticText8key, //" : ", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				StaticTextWidget
				(
					// CControlView properties
					kProductsRefreshedWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(248.0,50.0,289.0,70.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					"0", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				
				
				
	//			ButtonWidget
	//			(
	//				// CControlView properties
	//				kRLFinishedButtonWidgetID, // widget ID
	//				kSysButtonPMRsrcId, // PMRsrc ID
	//				kBindNone, // frame binding
	//				117.5 ,80.0,182.5,105.0
	//				kTrue, // visible
	//				kTrue, // enabled
	//				// CTextControlData properties
	//				kRLButton4Key, // control label
	//			),
//******************************************************************
				RollOverIconButtonWidget
				(
					kRLFinish1DialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLFinish1IconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(112,82,178,104)	// Frame(120,82,185,104)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),				
//******************************************************************				
			}
		),
		DefaultButtonWidget
		(
			kOKButtonWidgetID,		// WidgetID 
			kSysButtonPMRsrcId,		// RsrcID
			kBindNone,				// Binding
			Frame(0,0,1,1)
			kTrue, kTrue,			// Visible, Enabled
			kRLButton5Key, //" &Ok ",	// Button text
			
		),		
		CancelButtonWidget
		(
			kCancelButton_WidgetID,	// WidgetID
			kSysButtonPMRsrcId,		// RsrcID
			kBindNone,				// Binding
			Frame(0,0,1,1)
			kTrue, kTrue,			// Visible, Enabled
			kRLButton3Key, //"&Cancel",	// Button name
			kFalse,					// Change to Reset on option-click.
		),
	//</FREDDYWIDGETDEFLISTUS>
	},
};


resource RLSelectAttribDialogWidget(kRLDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kRLDialog1WidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	0, 0, 500,300,				// Frame (l,t,r,b)
	kTrue, kTrue,				// Visible, Enabled	
	kRLDialog2TitleKey,	// Dialog name
	{
	
		GroupPanelWidget
       (
			kInvalidWidgetID ,					// widget ID
			kPMRsrcID_None ,					// PMRsrc ID
			kBindLeft | kBindRight ,			// frame binding
			Frame(1 , 2 , 499 , 298)				//-- testing only  //Frame(1,2,219,65)	// left, top, right, bottom//(1,2,247,75)
			kTrue ,								// visible
			kTrue ,								// enabled
			0 ,									// header widget ID
			{
			
				//*****For SelectAll CheckBox
				CheckBoxWidget
				(
					kSelectAllCheckBoxWidgetID,					// WidgetId
					kSysCheckBoxPMRsrcId,				// RsrcId
					kBindRight | kBindBottom,
					Frame(7,5,22,30)						// Frame30,121.0,260,141
					kTrue,								// Visible
					kTrue,								// Enabled
					kAlignLeft,							// Alignment
					""							// Initial text
				),				
				StaticTextWidget
				(
					// CControlView properties
					kInvalidWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(60,5,198,30)//Frame(5,5,395,35) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					kRLStaticText3Key, // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				
				SeparatorWidget
				(
					kSeparatorWidgetID, 
					kPMRsrcID_None,		// WidgetId, RsrcId
					kBindLeft | kBindRight,                     // Frame binding
					//Frame(85,29,87,46)
					Frame(244,5,246,30)
					kTrue,kTrue,          // Visible, Enabled
				),
				
				StaticTextWidget
				(
					// CControlView properties
					kInvalidWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(274,5,395 + 34,30)//Frame(5,5,395,35) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue, // Ellipsize style
					// CTextControlData properties
					kRLStaticText4Key, // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				
					
				
				GroupPanelWidget
				(
					kInvalidWidgetID ,					// widget ID
					kPMRsrcID_None ,					// PMRsrc ID
					kBindLeft | kBindRight ,			// frame binding
					Frame(2 , 37 , 496 , 264.5)				//-- testing only  //Frame(2 , 37 , 396 , 170)	// left, top, right, bottom//(1,2,247,75)
					kTrue ,								// visible
					kTrue ,								// enabled
					0 ,									// header widget ID
					{
				//		//****List Box
				//		RLListBox
				//		(
				//			kRLListBoxWidgetID, 
				//			kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
				//			kBindAll,						// Frame bindin							
				//			Frame(3,3,491,224)             //4,3,390,165
				//			kTrue, kTrue,									// Visible, Enabled
				//			1,
				//			0,												// List dimensions
				//			19,													// Cell height
				//			2,													// Border width
				//			kFalse,kTrue,									// Has scroll bar (h,v)
				//			kFalse,												// Multiselection
				//			kFalse,												// List items can be reordered
				//			kFalse,												// Draggable to new/delete buttons
				//			kFalse,												// Drag/Dropable to other windows
				//			kFalse,												// An item always has to be selected
				//			kFalse,												// Don't notify on reselect
				//			kRLListElementRsrcID								// Fill list box with widgets with this ID (default is 0)
				//			{
				//					CellPanelWidget
				//					(
				//						kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
				//						kBindAll,									// Frame binding										
				//						Frame(-1,1,490,224)                         //-1,1,390,130
				//						kTrue, kTrue								// Visible, Enabled
				//						{
				//						// ----- This is the CPanelControlData that holds the widgets
				//						//		 that are items in the list box. They are not persistent
				//						//		 and so are not specified as part of the resource. [amb]
				//						}
				//					)
				//			},
				//		),
				//		//***
					}
				),
				
				
				
		//		ButtonWidget
		//		(
		//			// CControlView properties
		//			kRLBackButton2WidgetID, // widget ID
		//			kSysButtonPMRsrcId, // PMRsrc ID
		//			kBindNone, // frame binding
		//			142.5,267,207.5,292
		//			kTrue, // visible
		//			kTrue, // enabled
		//			// CTextControlData properties
		//			kRLButton2Key, // control label
		//		),
		//		
		//		ButtonWidget
		//		(
		//			// CControlView properties
		//			kRLCancelButton2WidgetID, // widget ID
		//			kSysButtonPMRsrcId, // PMRsrc ID
		//			kBindNone, // frame binding
		//			217.5,272.0 - 5,282.5,292.0
		//			kTrue, // visible
		//			kTrue, // enabled
		//			// CTextControlData properties
		//			kRLButton3Key, // control label
		//		),
				
				
		//		ButtonWidget
		//		(
		//			// CControlView properties
		//			kRLNextButton2WidgetID, // widget ID
		//			kSysButtonPMRsrcId, // PMRsrc ID
		//			kBindNone, // frame binding
		//			292.5,272.0 -5,357.5,292.0
		//			kTrue, // visible
		//			kTrue, // enabled
		//			// CTextControlData properties
		//			kRLButton1Key, // control label
		//		),
//**************************************************************
				RollOverIconButtonWidget
				(
					kRLBack1DialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLBack1IconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(120,267,185,289)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kRLCancle1DialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLCancle1IconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(205,267,270,289)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
					
				RollOverIconButtonWidget
				(
					kRLNext1DialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLNext1IconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(290,267,355,289)                      //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
//***************************************************************				
				DefaultButtonWidget
				(
					kOKButtonWidgetID,		// WidgetID 
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					Frame(0,0,1,1)
					kTrue, kTrue,			// Visible, Enabled
					kRLButton5Key, //" &Ok ",	// Button text
					
				),		
				CancelButtonWidget
				(
					kCancelButton_WidgetID,	// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					Frame(0,0,1,1)
					kTrue, kTrue,			// Visible, Enabled
					kRLButton3Key, //"&Cancel",	// Button name
					kFalse,					// Change to Reset on option-click.
				),
	
			
			}			
		),
	
	},
};

resource PrimaryResourcePanelWidget (kRLListElementRsrcID + index_enUS)
{
	__FILE__, __LINE__,
	kRLListParentWidgetId,
	0,	// WidgetId, RsrcId
	kRLPluginID,	
	kBindLeft | kBindRight,			// Frame binding	
	Frame(0, 0,490, 19),
	kTrue, kTrue,					// Visible, Enabled
	"",								// Panel name
	{
			
	/*	RLIconSuiteWidget
		(
			kRLUnCheckIconWidgetID,
			kUncheckIconID,
			kRLPluginID,
			kBindLeft,
			Frame(2,0, 28, 19),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),
		RLIconSuiteWidget
		(
			kRLCheckIconWidgetID,
			kCheckIconID,
			kRLPluginID,
			kBindLeft,
			Frame(2,0, 28, 19),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),*/
		
		 RLCustomIconSuiteWidget
		 (
			kRLCheckIconWidgetID,
			kUncheckIconID,  
			kRLPluginID,
			kBindLeft,
			Frame(1,0, 19, 20),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType,
			// Custom fields
			kUncheckIconID,
			kCheckIconID,
			
		),

/*		IconSuiteWidget //index 1
		(
			kCheckIconWidgetID,
			kCheckIconID,
			kRLPluginID,
			kBindLeft,
			Frame(2,0, 21, 20),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),
*/
		
		// Just a info-static text widget with about-box text view to get white bg.
		InfoStaticTextWidget
		(
			kRLAttributeNameTextWidgetID, 
			kPMRsrcID_None,//WidgetId, RsrcId
			kBindLeft | kBindRight,								// Frame binding
			//Frame(21,0,80,19)
			Frame(30,0,230,19),//Frame(21,0,208,21)										// Frame
			kTrue, kTrue, kAlignLeft,kEllipsizeEnd,kTrue,				// Visible, Enabled, Ellipsize style
			"",													// Initial text
			0,													// Associated widget for focus
			kPaletteWindowSystemScriptFontId,					// default font 
			kPaletteWindowSystemScriptFontId, //kPaletteWindowSystemScriptHiliteFontId,				// for highlight state.
		),	
		
		SeparatorWidget
		(
			kSeparatorInListWidgetID, 
			kPMRsrcID_None,		// WidgetId, RsrcId
			kBindLeft | kBindRight,                     // Frame binding
			//Frame(85,29,87,46)
			Frame(240,5,265,30)
			kTrue,kTrue,          // Visible, Enabled
		),
			
		InfoStaticTextWidget
		(
			kRLReplaceWithAttributeNameTextWidgetID, 
			kPMRsrcID_None,//WidgetId, RsrcId
			kBindLeft | kBindRight,								// Frame binding
			//Frame(21,0,80,19)
			Frame(270,0,450,19),//Frame(21,0,208,21)										// Frame
			kTrue, kTrue, kAlignLeft,kEllipsizeEnd,kTrue,				// Visible, Enabled, Ellipsize style
			"",													// Initial text
			0,													// Associated widget for focus
			kPaletteWindowSystemScriptFontId,					// default font 
			kPaletteWindowSystemScriptFontId, //kPaletteWindowSystemScriptHiliteFontId,				// for highlight state.
		),	
		
		/*RLRollOverIconButtonWidget
		(
			//kListBoxSelectTemplateFileButtonWidgetID,
			kSelectOtherLanguageAttributeRollOverButtonWidgetID, 
			kLocateIconID, 
			kRLPluginID,										// WidgetId, RsrcId
			kBindLeft,	
			Frame(346,0,368,20)
			kTrue, kTrue,										// Visible, Enabled
			kADBEIconSuiteButtonType,
		),*/
		
		RLRollOverIconWidget
		 (
			kSelectOtherLanguageAttributeRollOverButtonWidgetID,
			kLocateIconID,  
			kRLPluginID,
			kBindLeft,
			Frame(446,0,468,20),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType,
			// Custom fields
			kLocateIconID,
			kCheckIconID,
			
		),
				
		
	}
};




resource RLSelectOtherLangAttribDialogWidget (kRLSelectAttributeDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kRLDialogWidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	0,0,300,120,				// Frame (l,t,r,b)
	kTrue, kTrue,				// Visible, Enabled
	kRLDialog3TitleKey,			// Dialog name
	{
	//<FREDDYWIDGETDEFLISTUS>
	
		GroupPanelWidget
		(
			// CControlView properties
			kRLSelectAttributeGroupPanelWidgetID, // widget ID
			kPMRsrcID_None, // PMRsrc ID
			kBindNone, // frame binding
			Frame(5.0,5.0,295.0,115.0)  //Frame(5.0,5.0,295.0,95.0) //  left, top, right, bottom
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0, // header widget ID
			{ // CPanelControlData Children
			
				InfoStaticTextWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(35.0,20.0,125.0,40.0) //Frame(15.0,25.0,125.0,45.0)					// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					kRLStaticText5Key,						// Text
					0,
					0, 
					1,
				),
				DropDownListWidget
				(
					// CControlView properties
					kRLAttributeDropDownWidgetID, // widget ID
					kSysDropDownPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(130.0,20.0,280.0,40.0)  //Frame(130.0,25.0,280.0,45.0) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// DropDownListControlData properties
					{{				
					}}
				),
		
	//			ButtonWidget
	//			(
	//				// CControlView properties
	//				kRLOKButtonWidgetID, // widget ID
	//				kSysButtonPMRsrcId, // PMRsrc ID
	//				kBindNone, // frame binding
	//				80,80.0,145,105.0
	//				kTrue, // visible
	//				kTrue, // enabled
	//				// CTextControlData properties
	//				kRLButton5Key, // control label
	//			),	
				
	//			ButtonWidget
	//			(
	//				// CControlView properties
	//				kRLCancelButtonWidgetID, // widget ID
	//				kSysButtonPMRsrcId, // PMRsrc ID
	//				kBindNone, // frame binding
	//				155,80.0,220,105.0
	//				kTrue, // visible
	//				kTrue, // enabled
	//				// CTextControlData properties
	//				kRLButton3Key, // control label
	//			),	
//******************************************************************************
				RollOverIconButtonWidget
				(
					kRLOKDialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLOKIconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(80 -20,80.0,145-20,105.0),	// Frame(105,80,126,98)     
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kRLCancel2DialogIconWidgetID, 
					//kBackIconID, 
					kPNGRLCancel2IconRsrcID
					kRLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(165,80.0,230,105.0),		//	Frame(136,80,175,98)                 
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				DefaultButtonWidget
				(
					kOKButtonWidgetID,		// WidgetID 
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					Frame(0,0,1,1)
					kTrue, kTrue,			// Visible, Enabled
					kRLButton5Key, //" &Ok ",	// Button text
					
				),		
				CancelButtonWidget
				(
					kCancelButton_WidgetID,	// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					Frame(0,0,1,1)
					kTrue, kTrue,			// Visible, Enabled
					kRLButton3Key, //"&Cancel",	// Button name
					kFalse,					// Change to Reset on option-click.
				),
//******************************************************************************				
				
			}
		),
		
				
		

	//</FREDDYWIDGETDEFLISTUS>

	},
};



#endif // __ODFRC__

#include "RL_enUS.fr"
#include "RL_jaJP.fr"

//  Code generated by DollyXs code generator

