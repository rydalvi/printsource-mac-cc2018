//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "RLID.h"

#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"


#include "IAppFramework.h"
#include "CMediatorClass.h"


#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X);

extern bool16 showSummaryDialog;
/** RLDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup apjs10_refreshlanguage
*/
class RLDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~RLDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(RLDialogController, kRLDialogControllerImpl)

/* ApplyFields
*/
void RLDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if(showSummaryDialog)
	{

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;


		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel1WidgetID); //og
		//InterfacePtr<IControlView> firstGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel1WidgetID)); //added by avinash
		if(firstGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::firstGroupPanelControlData == nil");
			return;
		}
		firstGroupPanelControlData->HideView();
		firstGroupPanelControlData->Disable();

		IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel2WidgetID); //og
		//InterfacePtr<IControlView> secondGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel2WidgetID)); //added by avinash
		if(secondGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
			return;
		}
		secondGroupPanelControlData->HideView();
		secondGroupPanelControlData->Disable();
		
		IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel3WidgetID); //og
		//InterfacePtr<IControlView> thirdGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel3WidgetID)); //added by avinash
		if(thirdGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
			return;
		}
		thirdGroupPanelControlData->ShowView();
		thirdGroupPanelControlData->Enable();

		
		IControlView* TextWidget1 = panelControlData->FindWidget(kStaticText1WidgetID); //og
		//InterfacePtr<IControlView> TextWidget1(panelControlData->FindWidget(kStaticText1WidgetID)); //added by avinash
		if(!TextWidget1)
		{
			CA("TextWidget1 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}

			PMString insertText(kRLStaticText6Key,PMString::kTranslateDuringCall);
			PMString temp("Item");
			//temp = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp),1);
			insertText.Append(temp);
			PMString temp2(kRLStaticText7Key,PMString::kTranslateDuringCall);
			insertText.Append(temp2);
			insertText.SetTranslatable(kFalse);
            insertText.ParseForEmbeddedCharacters();
			textcontrol->SetString(insertText);
		}

		


		IControlView* TextWidget2 = panelControlData->FindWidget(kItemsRefreshedWidgetID); //og
		//InterfacePtr<IControlView> TextWidget2(panelControlData->FindWidget(kItemsRefreshedWidgetID)); //added by avinash
		if(!TextWidget2)
		{
			CA("TextWidget2 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber(PMReal(CMediatorClass::noOfItemFieldsUpdated));
			textcontrol->SetString(text);
		}

		



		IControlView* TextWidget3 = panelControlData->FindWidget(kStaticText2WidgetID); //og
		//InterfacePtr<IControlView> TextWidget3(panelControlData->FindWidget(kStaticText2WidgetID)); //added by avinash
		if(!TextWidget3)
		{
			CA("TextWidget2 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
				
			PMString insertText(kRLStaticText6Key,PMString::kTranslateDuringCall);
			PMString temp("Item Group");
			//temp = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp),1);
			insertText.Append(temp);
			PMString temp2(kRLStaticText7Key,PMString::kTranslateDuringCall);
			insertText.Append(temp2);
			insertText.SetTranslatable(kFalse);
            insertText.ParseForEmbeddedCharacters();
			textcontrol->SetString(insertText);
		}



		IControlView* TextWidget4 = panelControlData->FindWidget(kProductsRefreshedWidgetID); //og
		//InterfacePtr<IControlView> TextWidget4(panelControlData->FindWidget(kProductsRefreshedWidgetID)); //added by avinash
		if(!TextWidget4)
		{
			CA("TextWidget4 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber(PMReal(CMediatorClass::noOfProductFieldsUpdated));
			textcontrol->SetString(text);
		}
	}
	else
	{
		IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel3WidgetID); //og
		//InterfacePtr<IControlView> thirdGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel3WidgetID)); //added by avinash
		if(thirdGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
			return;
		}
		thirdGroupPanelControlData->HideView();
		thirdGroupPanelControlData->Disable();

		IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel2WidgetID); //og
		//InterfacePtr<IControlView> secondGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel2WidgetID)); //added by avinash
		if(secondGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
			return;
		}
		secondGroupPanelControlData->HideView();
		secondGroupPanelControlData->Disable();

		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel1WidgetID); //og
		//InterfacePtr<IControlView> firstGroupPanelControlData(panelControlData->FindWidget(kRLGroupPanel1WidgetID)); //added by avinash
		if(firstGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::firstGroupPanelControlData == nil");
			return;
		}
		firstGroupPanelControlData->ShowView();
		firstGroupPanelControlData->Enable();


		InterfacePtr<IStringListControlData> LocaleDropListData(
			this->QueryListControlDataInterface(kRLLocaleDropDownWidgetID));
		if(LocaleDropListData == nil){
			CAlert::InformationAlert("LocaleDropListData nil");
			return;
		}

		LocaleDropListData->Clear(kFalse,kFalse);		
		InterfacePtr<IDropDownListController> LocaleDropListControllerx(LocaleDropListData,UseDefaultIID());
		if (LocaleDropListControllerx == nil){
			CAlert::InformationAlert("LocaleDropListControllerx nil");
			return;
		}

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return ;
		}
		
		/*bool16 clearFlag = */ptrIAppFramework->/*LANGUAGECACHE_clearInstance*/StructureCache_clearInstance();
		
		//********** Get all languages
		VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
		if(VectorLocValPtr == NULL)
		{
			//CAlert::InformationAlert("VectorLocValPtr == NULL");
		}
		
		VectorLanguageModel::iterator it;
		
		int j = 0;
		for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
		{		
			//CA(it->getLangugeName());
			if(j == 0)
				CMediatorClass::replaceWithAttributeLanguageID = it->getLanguageID();

			PMString localename(it->getLangugeName());
			localename.SetTranslatable(kFalse);
			LocaleDropListData->AddString(localename, j, kFalse, kFalse);
			j++;
		}

		//added by avinash
		//if(VectorLocValPtr != NULL)
		//{
			//CA("ZZZZZZZZZ");
			//VectorLocValPtr->clear();
			//delete VectorLocValPtr;
		//}
		//till here
		
		//if(VectorLocValPtr->size() == 1)
			LocaleDropListControllerx->Select(0);
		//else
			//LocaleDropListControllerx->Select(-1);

		//IControlView* controlView1 = panelControlData->FindWidget(kRLNextButtonWidgetID);
		//if (controlView1 == nil){
		//	CAlert::InformationAlert("Ok Button ControlView nil");
		//	return;
		//}
		//if(controlView1->GetEnableState()){
		//	controlView1->Disable(kTrue);
		//	//controlView1->Enable(kTrue);
		//}
	}

}

/* ValidateFields
*/
WidgetID RLDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void RLDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}

//  Code generated by DollyXs code generator
