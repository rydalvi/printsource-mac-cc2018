//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
// Project includes:
#include "RLID.h"


#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IAppFramework.h"
#include "IDialogController.h"

#include "IDialogMgr.h"
#include "LocaleSetting.h"
#include "IApplication.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SDKListBoxHelper.h"

#include "CommonFunctions.h"
#include "CMediatorClass.h"
#include "IWidgetParent.h"
#include "IDialog.h"
#include "RLActionComponent.h"
#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X);

IDialog* secondDialog = 0;
extern bool16 showSummaryDialog;

//extern VectorAttributeInfoPtr vec_attributesPtr;
//extern VectorElementInfoPtr vec_elementsPtr;



/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup apjs10_refreshlanguage
*/
class RLDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~RLDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/

		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);

		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);

private:
	PMString GetSelectedLocaleName(IControlView*);
	PMString localeName;

	void DoSelectAttributeDialog();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RLDialogObserver, kRLDialogObserverImpl)

/* AutoAttach
*/
void RLDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		AttachWidget(panelControlData, kRLLocaleDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		//AttachToWidget(kRLNextButtonWidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLBackButtonWidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLCancelButton1WidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLNextButton1WidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		//AttachToWidget(kRLFinishedButtonWidgetID, IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLNextDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRLCancleDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRLFinish1DialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

	} while (kFalse);
}

/* AutoDetach
*/
void RLDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.
		DetachWidget(panelControlData, kRLLocaleDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA);	
		//DetachFromWidget(kRLNextButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRLBackButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRLCancelButton1WidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRLNextButton1WidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		//DetachFromWidget(kRLFinishedButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRLNextDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRLCancleDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRLFinish1DialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

	
	
	} while (kFalse);
}

/* Update
*/
void RLDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		//CA("CDialogObserver::Update");
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this
		
		//----------
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel1WidgetID);
		if(firstGroupPanelControlData == nil)
		{
			CA("AP46_RefreshLanguage::RLDialogObserver::Update::firstGroupPanelControlData == nil");
			return;
		}
		
		IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel3WidgetID);
		if(thirdGroupPanelControlData == nil)
		{
			//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
			return;
		}

		if(theSelectedWidget ==kRLLocaleDropDownWidgetID)
		{
			//CA("theSelectedWidget ==kLocaleDropDownWidgetID");		
			
			localeName = GetSelectedLocaleName(controlView);
			//CA("localeName : " + localeName);
			//********** Get all languages
			VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
			if(VectorLocValPtr == NULL)
			{
				//CAlert::InformationAlert("VectorLocValPtr == NULL");
			}
			
			VectorLanguageModel::iterator it;
			
			for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
			{		
				if(localeName == it->getLangugeName())
				{
					CMediatorClass::replaceWithAttributeLanguageID = it->getLanguageID();
					break;
				}				
			}

			//added by avinash
			//if(VectorLocValPtr != NULL)
			//{
			//	CA("ZZZZZZZZZ");
			//	VectorLocValPtr->clear();
			//	delete VectorLocValPtr;
			//}
			//till here
		}
		else if((theSelectedWidget == kRLCancleDialogIconWidgetID ||  theSelectedWidget == kCancelButton_WidgetID )&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancleDialogIconWidgetID");
			if(thirdGroupPanelControlData->IsVisible())
				showSummaryDialog = kFalse;
			CDialogObserver::CloseDialog();
		}

		else if((theSelectedWidget == kRLNextDialogIconWidgetID  || (theSelectedWidget == kOKButtonWidgetID && firstGroupPanelControlData->IsVisible())/*kRLNextButtonWidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLNextButtonWidgetID");
		/*	InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				CA("dialogController == nil");
				break;
			}		

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel1WidgetID);
			if(firstGroupPanelControlData == nil)
			{
				CA("AP46_RefreshLanguage::RLDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->Hide();
			firstGroupPanelControlData->Disable();

			IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel3WidgetID);
			if(thirdGroupPanelControlData == nil)
			{
				//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
				return;
			}
			thirdGroupPanelControlData->Hide();
			thirdGroupPanelControlData->Disable();

			IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel2WidgetID);
			if(secondGroupPanelControlData == nil)
			{
				CA("AP46_RefreshLanguage::RLDialogObserver::Update::secondGroupPanelControlData == nil");
				break;
			}
			secondGroupPanelControlData->Show();
			secondGroupPanelControlData->Enable();

			dialogController->SetTriStateControlData(kRLRadio1WidgetID,kTrue);*/
			CMediatorClass::noOfItemFieldsUpdated = 0;
			CMediatorClass::noOfProductFieldsUpdated =0;
			//CA("AAAAAAAAAAAAAAAAAAAAA");
			CDialogObserver::CloseDialog();

			CommonFunctions objCommonFunctions;

			//CA("Commented SCAN BOOK");
			objCommonFunctions.scanBook();

			
			/*VectorAttributeInfoPtr vec_attributesPtrTemp = vec_attributesPtr;
			VectorElementInfoPtr vec_elementsPtrTemp = vec_elementsPtr;

			vec_attributesPtr = nil;
			vec_elementsPtr = nil;


			if(vec_attributesPtrTemp != nil)
			{
				vec_attributesPtrTemp->clear();
				delete vec_attributesPtrTemp;
			}
			if(vec_elementsPtrTemp != nil)
			{
				vec_elementsPtrTemp->clear();
				delete vec_elementsPtrTemp;
			}

			CMediatorClass::vecPtr_listBoxData = new vec_listBoxData;*/
			

			
			objCommonFunctions.StartUpdateJL();

			//this->DoSelectAttributeDialog();

			/*showSummaryDialog = kTrue;
			RLActionComponent RLac(this);
			RLac.DoSummaryDialog();*/
			PMString ReportString("");
			ReportString.Append("               Summary Report             ");
			ReportString.Append("\n");
			ReportString.Append("\n");
			ReportString.Append("\n");
			ReportString.Append("No. of Item Fields Updated  : ");
			ReportString.AppendNumber(PMReal(CMediatorClass::noOfItemFieldsUpdated));
			ReportString.Append("\n");
			ReportString.Append("\n");
			ReportString.Append("No. of Item Group Fields Updated  : ");
			ReportString.AppendNumber(PMReal(CMediatorClass::noOfProductFieldsUpdated));
			ReportString.Append("\n");
			ReportString.Append("\n");

			ReportString.SetTranslatable(kFalse);

			CAlert::InformationAlert(ReportString);
			


		}
		else if((theSelectedWidget == kRLBackButtonWidgetID)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLBackButtonWidgetID");
			InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				//CA("dialogController == nil");
				break;
			}

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());

			IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel2WidgetID);
			if(secondGroupPanelControlData == nil)
			{
				//CA("AP46_RefreshLanguage::RLDialogObserver::Update::secondGroupPanelControlData == nil");
				break;
			}
			secondGroupPanelControlData->HideView();
			secondGroupPanelControlData->Disable();

			IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel3WidgetID);
			if(thirdGroupPanelControlData == nil)
			{
				//CA("AP46_CatalogIndex::CTIDialogObserver::Update::secondGroupPanelControlData == nil");
				return;
			}
			thirdGroupPanelControlData->HideView();
			thirdGroupPanelControlData->Disable();

			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRLGroupPanel1WidgetID);
			if(firstGroupPanelControlData == nil)
			{
				//CA("AP46_RefreshLanguage::RLDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->ShowView();
			firstGroupPanelControlData->Enable();			
		}
		else if((theSelectedWidget == kRLCancelButton1WidgetID)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancelButton1WidgetID");
			CDialogObserver::CloseDialog();
		}
		else if((theSelectedWidget == kRLNextButton1WidgetID)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLNextButton1WidgetID");

			//CA("BBBBBBBBBBBBBBBBBBB");
			
			CommonFunctions objCommonFunctions;
			objCommonFunctions.scanBook();

			this->DoSelectAttributeDialog();
		}
		else if((theSelectedWidget == kRLFinish1DialogIconWidgetID || ( theSelectedWidget == kOKButtonWidgetID && thirdGroupPanelControlData->IsVisible()))&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLFinishedButtonWidgetID");
			showSummaryDialog = kFalse;
			CDialogObserver::CloseDialog();
		}

	} while (kFalse);
}

//  Code generated by DollyXs code generator

void RLDialogObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{	
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil){
			//ASSERT_FAIL("widget has no ISubject... Ouch!");
			break;
		}
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); // only do once
}

/*	::DetachWidget
*/
void RLDialogObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
		{
			//ASSERT_FAIL("iControlView invalid. Where the widget are you?");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
		{
			//ASSERT_FAIL("PstLst Panel widget has no ISubject... Ouch!");
			break;
		}
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

PMString RLDialogObserver::GetSelectedLocaleName(IControlView* LocaleControlView)
{	
	//CA("PubBrowseDlgObserver::GetSelectedLocaleName");
	PMString locName("");
	do{
		InterfacePtr<IStringListControlData> LocaleDropListData(LocaleControlView,UseDefaultIID());	
		if(LocaleDropListData == nil){
			//CAlert::InformationAlert("LocaleDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> LocaleDropListController(LocaleDropListData, UseDefaultIID());
		if(LocaleDropListController == nil){
			//CAlert::InformationAlert("LocaleDropListController nil");
			break;
		}
		int32 selectedRow = LocaleDropListController->GetSelected();
		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			break;
		}
		locName = LocaleDropListData->GetString(selectedRow);
		if(selectedRow>=0){
			InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
			if (panelControlData == nil){
				//CAlert::InformationAlert("panelControlData nil");
				break;
			}
			IControlView* controlView = panelControlData->FindWidget(kRLLocaleDropDownWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("Publication Browse Button ControlView nil");
				break;
			}
			IControlView* controlView1 = panelControlData->FindWidget(kRLNextDialogIconWidgetID/*kRLNextButtonWidgetID*/);
			if (controlView1 == nil){
				//CAlert::InformationAlert("Ok Button ControlView nil");
				break;
			}
			if(!controlView->GetEnableState()){
				controlView->Enable(kTrue);
				//controlView1->Enable(kTrue);
			}
		}
	}while(0);	
	return locName;
} 

//******Method To Open the DocSelectDialog
void RLDialogObserver::DoSelectAttributeDialog()
{
	InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
	ASSERT(application);
	if (application == nil) {	
		return;
	}
	InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
	ASSERT(dialogMgr);
	if (dialogMgr == nil) {
		return;
	}
	
	PMLocaleId nLocale = LocaleSetting::GetLocale();
	RsrcSpec dialogSpec
	(
		nLocale,					// Locale index from PMLocaleIDs.h. 
		kRLPluginID,				// Our Plug-in ID  
		kViewRsrcType,				// This is the kViewRsrcType.
		kRLDialogResourceID,		// Resource ID for our dialog.
		kTrue						// Initially visible.
	);

	// CreateNewDialog takes the dialogSpec created above, and also
	// the type of dialog being created (kMovableModal).
	IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
	if (dialog == nil) {
		return;
	}	

	secondDialog = dialog;


	//dialog->SetDialogFocusingAlgorithm(IDialog::kNoAutoFocus);
	
	dialog->Open(); 
	
	
	//WidgetID theSelectedWidget = dialog->SetDefaultButton(kCancelActionID, kRLCancelButton1WidgetID);
//	dialog->WaitForDialog();

}

