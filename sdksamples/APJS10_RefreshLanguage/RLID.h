//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __RLID_h__
#define __RLID_h__

#include "SDKDef.h"

// Company:
#define kRLCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kRLCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kRLPluginName	"APJS10_RefreshLanguage"			// Name of this plug-in.
#define kRLPrefixNumber	0xDA80A9 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kRLVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kRLAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kRLPrefixNumber above to modify the prefix.)
#define kRLPrefix		RezLong(kRLPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kRLStringPrefix	SDK_DEF_STRINGIZE(kRLPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kRLMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kRLMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kRLPluginID, kRLPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kRLActionComponentBoss, kRLPrefix + 0)
DECLARE_PMID(kClassIDSpace, kRLDialogBoss, kRLPrefix + 2)
DECLARE_PMID(kClassIDSpace, kRLIconSuiteWidgetBoss, kRLPrefix + 3)
DECLARE_PMID(kClassIDSpace, kRLListBoxWidgetBoss, kRLPrefix + 4)
DECLARE_PMID(kClassIDSpace, kRLSelectAttribDialogBoss, kRLPrefix + 5)
DECLARE_PMID(kClassIDSpace, kRLRollOverIconButtonWidgetBoss, kRLPrefix + 6)
DECLARE_PMID(kClassIDSpace, kRLCustomIconSuiteWidgetBoss, kRLPrefix + 7)
DECLARE_PMID(kClassIDSpace, kRLRollOverIconWidgetBoss, kRLPrefix + 8)
DECLARE_PMID(kClassIDSpace, kRLSelectOtherLanguageAttribDialogBoss, kRLPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kRLBoss, kRLPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IRLINTERFACE, kRLPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kRLActionComponentImpl, kRLPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kRLDialogControllerImpl, kRLPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kRLDialogObserverImpl, kRLPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kRLListBoxObserverImpl, kRLPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kSelectAttribDialogControllerImpl, kRLPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kSelectAttribDialogObserverImpl, kRLPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kSelectOtherLanguageAttributeListBoxObserverImpl, kRLPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kRLCustomIconSuitWidgetEHImpl, kRLPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kRLRollOverIconWidgetEHImpl, kRLPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kSelectOtherLanguageAttribDialogControllerImpl, kRLPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kSelectOtherLanguageAttribDialogObserverImpl, kRLPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kRLImpl, kRLPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kRLAboutActionID, kRLPrefix + 0)

DECLARE_PMID(kActionIDSpace, kRLDialogActionID, kRLPrefix + 4)
DECLARE_PMID(kActionIDSpace, kRefreshByLaanguageActionID, kRLPrefix + 5)
//DECLARE_PMID(kActionIDSpace, ktempActionID, kRLPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kRLActionID, kRLPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kRLDialogWidgetID, kRLPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kRLGroupPanel1WidgetID, kRLPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kRLLocaleDropDownWidgetID, kRLPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kRLGroupPanel2WidgetID, kRLPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kRLSecondGroupPanelClusterPanelWidgetID, kRLPrefix + 5)

DECLARE_PMID(kWidgetIDSpace, kRLRadio1WidgetID, kRLPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kRLRadio2WidgetID, kRLPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kRLNextButtonWidgetID, kRLPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kRLBackButtonWidgetID, kRLPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kRLCancelButton1WidgetID, kRLPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kRLNextButton1WidgetID, kRLPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kRLDialog1WidgetID, kRLPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kSelectAllCheckBoxWidgetID, kRLPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kRLListParentWidgetId, kRLPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kRLUnCheckIconWidgetID, kRLPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kRLCheckIconWidgetID, kRLPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kRLAttributeNameTextWidgetID, kRLPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kRLListBoxWidgetID, kRLPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kSeparatorWidgetID, kRLPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kSeparatorInListWidgetID, kRLPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kRLReplaceWithAttributeNameTextWidgetID, kRLPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kSelectOtherLanguageAttributeRollOverButtonWidgetID, kRLPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kRLBackButton2WidgetID, kRLPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kRLCancelButton2WidgetID, kRLPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kRLNextButton2WidgetID, kRLPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kRLGroupPanel3WidgetID, kRLPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kColonWidgetID, kRLPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kItemsRefreshedWidgetID, kRLPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kProductsRefreshedWidgetID, kRLPrefix + 29)
//DECLARE_PMID(kWidgetIDSpace, kRLFinishedButtonWidgetID, kRLPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kRLSelectAttributeGroupPanelWidgetID, kRLPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kRLAttributeDropDownWidgetID, kRLPrefix + 32)
//DECLARE_PMID(kWidgetIDSpace, kRLOKButtonWidgetID, kRLPrefix + 33)
//DECLARE_PMID(kWidgetIDSpace, kRLCancelButtonWidgetID, kRLPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kStaticText1WidgetID, kRLPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kStaticText2WidgetID, kRLPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kRLNextDialogIconWidgetID, kRLPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kRLCancleDialogIconWidgetID, kRLPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kRLBack1DialogIconWidgetID, kRLPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kRLCancle1DialogIconWidgetID, kRLPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kRLNext1DialogIconWidgetID, kRLPrefix + 41)
DECLARE_PMID(kWidgetIDSpace, kRLFinish1DialogIconWidgetID, kRLPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kRLOKDialogIconWidgetID, kRLPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kRLCancel2DialogIconWidgetID, kRLPrefix + 44)






// "About Plug-ins" sub-menu:
#define kRLAboutMenuKey			kRLStringPrefix "kRLAboutMenuKey"
#define kRLAboutMenuPath		kSDKDefStandardAboutMenuPath kRLCompanyKey

// "Plug-ins" sub-menu:
#define kRLPluginsMenuKey 		kRLStringPrefix "kRLPluginsMenuKey"
#define kRLPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kRLCompanyKey kSDKDefDelimitMenuPath kRLPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kRLAboutBoxStringKey	kRLStringPrefix "kRLAboutBoxStringKey"
#define kRLTargetMenuPath kRLPluginsMenuPath



#define kRLPluginStringKey	kRLStringPrefix "kRLPluginStringKey"

//Dialog Title Keys
#define kRLDialog1TitleKey	kRLStringPrefix "kRLDialog1TitleKey"
#define kRLDialog2TitleKey	kRLStringPrefix "kRLDialog2TitleKey"
#define kRLDialog3TitleKey	kRLStringPrefix "kRLDialog3TitleKey"
#define kRLDialog4TitleKey	kRLStringPrefix "kRLDialog4TitleKey"



//Static Text Keys
#define kRLStaticText1Key	kRLStringPrefix "kRLStaticText1Key"
#define kRLStaticText9Key	kRLStringPrefix "kRLStaticText9Key"
#define kRLStaticText3Key	kRLStringPrefix "kRLStaticText3Key"
#define kRLStaticText4Key	kRLStringPrefix "kRLStaticText4Key"
#define kRLStaticText5Key	kRLStringPrefix "kRLStaticText5Key"
#define kRLStaticText6Key	kRLStringPrefix "kRLStaticText6Key"
#define kRLStaticText7Key	kRLStringPrefix "kRLStaticText7Key"
#define kRLStaticText8key	kRLStringPrefix "kRLStaticText8key"





//Button Keys
#define kRLButton1Key	kRLStringPrefix "kRLButton1Key"
#define kRLButton2Key	kRLStringPrefix "kRLButton2Key"
#define kRLButton3Key	kRLStringPrefix "kRLButton3Key"
#define kRLButton4Key	kRLStringPrefix "kRLButton4Key"
#define kRLButton5Key	kRLStringPrefix "kRLButton5Key"




//Radio Button Keys
#define kRLRadioButton1Key	kRLStringPrefix "kRLRadioButton1Key"
#define kRLRadioButton2Key	kRLStringPrefix "kRLRadioButton2Key"






// Menu item positions:

#define kRLDialogTitleKey kRLStringPrefix "kRLDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kRLDialogMenuItemKey kRLStringPrefix "kRLDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kRLDialogMenuItemPosition	12.0

//Resource ID
#define kRLDialogResourceID					200
#define kRLListElementRsrcID				201
#define kRLSelectAttributeDialogResourceID					202
#define kCheckIconID						11000
#define kUncheckIconID						11001
#define kLocateIconID						11002




// Initial data format version numbers
#define kRLFirstMajorFormatNumber  RezLong(1)
#define kRLFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kRLCurrentMajorFormatNumber kRLFirstMajorFormatNumber
#define kRLCurrentMinorFormatNumber kRLFirstMinorFormatNumber

//PNG ID
#define kPNGRLNextIconRsrcID                  1200
#define kPNGRLNextIconRollRsrcID              1200
#define kPNGRLCancleIconRsrcID                1201
#define kPNGRLCancleIconRollRsrcID            1201
#define kPNGRLBack1IconRsrcID                 1202
#define kPNGRLBack1IconRollRsrcID             1202
#define kPNGRLCancle1IconRsrcID               1203
#define kPNGRLCancle1IconRollRsrcID           1203
#define kPNGRLNext1IconRsrcID                 1204
#define kPNGRLNext1IconRollRsrcID             1204
#define kPNGRLFinish1IconRsrcID               1205
#define kPNGRLFinish1IconRollRsrcID           1205
#define kPNGRLOKIconRsrcID                    1206
#define kPNGRLOKIconRollRsrcID                1206
#define kPNGRLCancel2IconRsrcID               1207
#define kPNGRLCancel2IconRollRsrcID           1207


#endif // __RLID_h__

//  Code generated by DollyXs code generator
