//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
#include "CDialogController.h"
// Project includes:
#include "RLID.h"

#include "SDKListBoxHelper.h"
#include "IListBoxController.h"

#include <vector>

#include "IDialog.h"


#include "RLActionComponent.h"
#include "CommonFunctions.h"
#include "CMediatorClass.h"


#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

using namespace std;

//extern K2Vector<PMString> bookContentNames;
extern vector<double> attributeIDsList;
extern vector<double> elementIDsList;
extern vector<double> eventAttributeIDsList;
extern K2Vector<bool16>  isSelected;

extern vector<IDFile> BookFileList;///IDFile of all documents present inside book.

extern IDialog* firstDialog;
extern IDialog* secondDialog;
bool16 showSummaryDialog = kFalse;

/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup writefishprice
*/
class RLSelectAttribDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLSelectAttribDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~RLSelectAttribDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RLSelectAttribDialogObserver, kSelectAttribDialogObserverImpl)


void RLSelectAttribDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Attach to other widgets you want to handle dynamically here.

		AttachToWidget(kSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kRLCancelButton2WidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		//AttachToWidget(kRLBackButton2WidgetID, IID_IBOOLEANCONTROLDATA ,panelControlData);
		//AttachToWidget(kRLNextButton2WidgetID, IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLBack1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRLCancle1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRLNext1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		
	} while (kFalse);
}

/* AutoDetach
*/
void RLSelectAttribDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.

		DetachFromWidget(kSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kRLCancelButton2WidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
	//	DetachFromWidget(kRLBackButton2WidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		//DetachFromWidget(kRLNextButton2WidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kRLBack1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRLCancle1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRLNext1DialogIconWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		

	} while (kFalse);
}

/* Update
*/
void RLSelectAttribDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		//CA("RLSelectAttribDialogObserver::Update ");
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this


		InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
		
		if(theSelectedWidget==kSelectAllCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Check Box Select");
			SDKListBoxHelper listHelper(this, kRLPluginID,0,0);
			IControlView *listBoxControlView  = pPanelData->FindWidget(kRLListBoxWidgetID);
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				return;

			vector<double>::iterator itr;

			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  isSelected.begin();

			if(attributeIDsList.size() != 0)
			{
				for(itr=attributeIDsList.begin();itr!=attributeIDsList.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					//CA("Inside for loop");
					listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
					(*itrIsSelected) = kTrue;
				}				
			}

			if(elementIDsList.size() != 0)
			{
				for(itr=elementIDsList.begin();itr!=elementIDsList.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
					(*itrIsSelected) = kTrue;
				}				
			}	
			
			if(eventAttributeIDsList.size() != 0)
			{
				for(itr=eventAttributeIDsList.begin();itr!=eventAttributeIDsList.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
					(*itrIsSelected) = kTrue;
				}	
			}
			//	break;
		}
		else if(theSelectedWidget==kSelectAllCheckBoxWidgetID && theChange==kFalseStateMessage)
		{
			//CA("Check Box DeSelect");
			SDKListBoxHelper listHelper(this, kRLPluginID,0,0);
			IControlView *listBoxControlView  = pPanelData->FindWidget(kRLListBoxWidgetID);
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				return;

			vector<double>::iterator itr;

			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  isSelected.begin();

			if(attributeIDsList.size() != 0)
			{
				for(itr=attributeIDsList.begin();itr!=attributeIDsList.end()&& itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					//CA("Inside for loop");
					listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
					(*itrIsSelected) = kFalse;
				}
			}

			if(elementIDsList.size() != 0)
			{
				for(itr=elementIDsList.begin();itr!=elementIDsList.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
					(*itrIsSelected) = kFalse;
				}				
			}
			if(eventAttributeIDsList.size() != 0)
			{
				for(itr=eventAttributeIDsList.begin();itr!=eventAttributeIDsList.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
				{
					listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
					(*itrIsSelected) = kFalse;
				}	
			}
			//	break;
		}
		else if((theSelectedWidget == kRLCancle1DialogIconWidgetID ||  theSelectedWidget == kCancelButton_WidgetID/*kRLCancelButton2WidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancelButton1WidgetID");
			CDialogObserver::CloseDialog();
		}
		else if((theSelectedWidget == kRLBack1DialogIconWidgetID /*kRLBackButton2WidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancelButton1WidgetID");
			CDialogObserver::CloseDialog();
			RLActionComponent RLac(this);
			RLac.DoDialog();
		}
		else if((theSelectedWidget == kRLNext1DialogIconWidgetID  || theSelectedWidget == kOKButtonWidgetID /*kRLNextButton2WidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLNextButton2WidgetID");
			if(BookFileList.size() > 0)
			{
				/*PMString temp("CMediatorClass::vecPtr_listBoxData->size = ");
				temp.AppendNumber(CMediatorClass::vecPtr_listBoxData->size());
				CA(temp);*/


				if(secondDialog)
				{
					if(secondDialog->IsOpen())
					{
						secondDialog->Close();
					}
				}
				if(firstDialog)
				{
					if(firstDialog->IsOpen())
					{
						firstDialog->Close();
					}
				}

				//CommonFunctions objCommonFunctions;
				//objCommonFunctions.startUpdate();

				if(CMediatorClass::vecPtr_listBoxData != nil)
				{
					CMediatorClass::vecPtr_listBoxData->clear();
					delete CMediatorClass::vecPtr_listBoxData;
					CMediatorClass::vecPtr_listBoxData = nil;
				}
				
				showSummaryDialog = kTrue;
				RLActionComponent RLac(this);
				RLac.DoSummaryDialog();
			}
		}		
	} while (kFalse);
}
