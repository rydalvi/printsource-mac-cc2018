//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "ITextEditSuite.h"  			// STEP 7.1
#include "ISelectionManager.h"			// STEP 7.1
#include "IPanelControlData.h" 			// STEP 9.1
#include "IDropDownListController.h" 	// STEP 9.1

// General includes:
#include "CDialogController.h"

// Project includes:
#include "RLID.h"
#include "SDKListBoxHelper.h"

#include "IAppFramework.h"
#include "CMediatorClass.h"
#include <vector>

#include "IStringListControlData.h"

#include "CMediatorClass.h"


#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x);

using namespace std;
//extern K2Vector<PMString> bookContentNames;  
extern vector<int32> attributeIDsList;
extern vector<int32> elementIDsList;
extern int32 classID;
extern int32 selectedAttributeIndex;
extern bool16 isParametric;;

VectorAttributeInfoPtr vec_attributesPtr;
VectorElementInfoPtr vec_elementsPtr;

/** WFPDialogController

	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup writefishprice
*/
class RLSelectOtherLanguageAttribDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLSelectOtherLanguageAttribDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~RLSelectOtherLanguageAttribDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	    virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(RLSelectOtherLanguageAttribDialogController, kSelectOtherLanguageAttribDialogControllerImpl)


void RLSelectOtherLanguageAttribDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA("CPDFSelectDocsDialogController::InitializeDialogFields_  Select Other Attribute Dlg-2 ");
	CDialogController::InitializeDialogFields(dlgContext);

	// STEP 9.1  - BEGIN
	do {
		// Get current panel control data.

		InterfacePtr<IStringListControlData> attributeDropListData(this->QueryListControlDataInterface(kRLAttributeDropDownWidgetID));
		if(attributeDropListData == nil){
			//CAlert::InformationAlert("attributeDropListData nil");
			return;
		}

		attributeDropListData->Clear(kFalse,kFalse);		
		InterfacePtr<IDropDownListController> attributeDropListControllerx(attributeDropListData,UseDefaultIID());
		if (attributeDropListControllerx == nil){
			//CAlert::InformationAlert("attributeDropListControllerx nil");
			return;
		}

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			///CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return ;
		}

		if(selectedAttributeIndex == 4)
		{
			int32 index = 0;			
				
			VectorAttributeInfoPtr vec_attributesPtr1 = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(classID,CMediatorClass::replaceWithAttributeLanguageID);
			if(vec_attributesPtr1 == nil)
			{
				//CAlert::InformationAlert("Pointer to vec_attributesPtr1 is nil.");
				attributeDropListControllerx->Select(0);
				return;
			}

			VectorAttributeInfoValue::iterator itr1 = vec_attributesPtr1->begin();
			PMString str3("--Select--");
			attributeDropListData->AddString(str3);
			index++;
			
			for(;itr1 != vec_attributesPtr1->end(); itr1++)
			{
				Attribute objAttributeModel = *itr1;		

				attributeDropListData->AddString(objAttributeModel.getDisplayName(), index, kFalse, kFalse);
				index++;
			}
			
			attributeDropListControllerx->Select(0);
		}
		else if(selectedAttributeIndex == 3)
		{
			if(vec_elementsPtr == nil)
				vec_elementsPtr = ptrIAppFramework->/*ElementCache_getCopyAttributesByIndex*/StructureCache_getItemGroupCopyAttributesByLanguageId(/*3,*/ CMediatorClass::replaceWithAttributeLanguageID);
			
			if(vec_elementsPtr==nil)
				return;

			int32 index = 0;
			PMString str1("--Select--");
			attributeDropListData->AddString(str1, index, kFalse, kFalse);
			index++;

			
			VectorElementInfoValue::iterator itr;

			for(itr = vec_elementsPtr->begin(); itr != vec_elementsPtr->end(); itr++)
			{
				attributeDropListData->AddString((*itr).getDisplayName(), index, kFalse, kFalse);
				index++;				
			}
			attributeDropListControllerx->Select(0);

		}
		
		else if(selectedAttributeIndex == 5)
		{
			//CA("selectedAttributeIndex == 5");
			int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();
			int32 index = 0;
			PMString str1("--Select--");
			attributeDropListData->AddString(str1, index, kFalse, kFalse);
			index++;

			do{
				VectorElementInfoPtr vec_elementsPtr = ptrIAppFramework->/*ElementCache_getProjectSecSubSecAttributesByIndex*/StructureCache_getSectionCopyAttributesByLanguageId(/*1,*/  CMediatorClass::replaceWithAttributeLanguageID);
				if(vec_elementsPtr==nil)
				{
					break;
				}
				
				VectorElementInfoValue::iterator itr;
	
				for(itr = vec_elementsPtr->begin(); itr != vec_elementsPtr->end(); itr++)
				{
					if(CMediatorClass::replaceWithAttributeLanguageID == (*itr).getLanguageId())
					{
						attributeDropListData->AddString((*itr).getDisplayName(), index, kFalse, kFalse);
						index++;				
					}
				}
				vec_elementsPtr = ptrIAppFramework->/*ElementCache_getProjectSecSubSecAttributesByIndex*/StructureCache_getSectionCopyAttributesByLanguageId(/*2,*/CMediatorClass::replaceWithAttributeLanguageID);
				if(vec_elementsPtr==nil)
				{
					break;
				}
				for(itr = vec_elementsPtr->begin(); itr != vec_elementsPtr->end(); itr++)
				{
					if(CMediatorClass::replaceWithAttributeLanguageID == (*itr).getLanguageId())
					{
						attributeDropListData->AddString((*itr).getDisplayName(), index, kFalse, kFalse);
						index++;				
					}
				}
				if(PM_Project_Level == 3)
				{
					//CA("PM_Project_Level == 3");
					vec_elementsPtr = ptrIAppFramework->/*ElementCache_getProjectSecSubSecAttributesByIndex*/StructureCache_getSectionCopyAttributesByLanguageId(/*3,*/CMediatorClass::replaceWithAttributeLanguageID);
					if(vec_elementsPtr==nil)
					{
						break;
					}
					for(itr = vec_elementsPtr->begin(); itr != vec_elementsPtr->end(); itr++)
					{
						if(CMediatorClass::replaceWithAttributeLanguageID == (*itr).getLanguageId())
						{
							attributeDropListData->AddString((*itr).getDisplayName(), index, kFalse, kFalse);
							index++;				
						}
					}
				}
				//vec_elementsPtr = ptrIAppFramework->getCategoryCopyAttributesForPrint(CMediatorClass::replaceWithAttributeLanguageID);
				//if(vec_elementsPtr==nil)
				//{
				//	break;
				//}
				//int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();

				//PMString temp1;
				//PMString temp2;
				//PMString Level;
				//PMString temp3("Category");
				////temp3 = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp3),CMediatorClass::replaceWithAttributeLanguageID);
				//for(int32 j=1 ; j<=levels ;j++ )
				//{
				//	temp1.Append(", Level ");
				//	Level.Clear();
				//	Level.AppendNumber(j);
				//	temp1.AppendNumber(j);

				//	for(itr=vec_elementsPtr->begin();itr!=vec_elementsPtr->end();itr++)
				//	{
				//		temp2 = temp3;
				//		temp2.Append(itr->getDisplayName());
				//		temp2.Append(temp1);

				//		attributeDropListData->AddString(temp2, index, kFalse, kFalse);
				//		index++;				
				//		temp2.Clear();	
				//	}
				//	temp1.Clear();
				//}

			}while(kFalse);
			attributeDropListControllerx->Select(0);

		}


		
	} while (kFalse);
	// STEP 9.1  - END
}

/* ValidateFields
*/
WidgetID RLSelectOtherLanguageAttribDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void RLSelectOtherLanguageAttribDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// STEP 5.1 - BEGIN
	// DropDownList result string.
	//CA("CPDFSelectDocsDialogController::ApplyDialogFields");
	PMString resultString;
	
	//Get Selected text of DropDownList.
	//resultString = this->GetTextControlData(kWFPDropDownListWidgetID);
	//resultString.Translate(); // Look up our string and replace.
	//// STEP 5.1 - END

	//// STEP 6.1 - BEGIN
	//// Get the editbox list widget string.
	//PMString editBoxString = this->GetTextControlData(kWFPTextEditBoxWidgetID);
	//// STEP 6.1 - END

	//// STEP 6.2 - BEGIN
	//PMString moneySign(kWFPStaticTextKey);
	//moneySign.Translate(); // Look up our string and replace.
	//
	//resultString.Append('\t'); // Append tab code.
	//resultString.Append(moneySign);
	//resultString.Append(editBoxString);
	//resultString.Append('\r'); // Append return code.
	//// STEP 6.2 - END

	//// STEP 7.1 - BEGIN
 //   if (myContext == nil)
	//{
	//	ASSERT(myContext);
	//	return;
	//}
	//// Insert resultString to TextFrame.
 //   InterfacePtr<ITextEditSuite> textEditSuite(myContext->GetContextSelection(), UseDefaultIID());

	//// STEP 7.2 - BEGIN
 //   if (textEditSuite && textEditSuite->CanEditText())
 //   {
	//	// STEP 7.3 - BEGIN
 //       ErrorCode status = textEditSuite->InsertText(WideString(resultString));
 //       ASSERT_MSG(status == kSuccess, "CPDDialogController::ApplyDialogFields: can't insert text"); 
	//	// STEP 7.3 - END
 //   }
	// STEP 7.2 - END
}

/* ApplyFields
*/