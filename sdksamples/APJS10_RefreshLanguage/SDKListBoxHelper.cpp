#include "VCPlugInHeaders.h"
#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "PersistUtils.h"
//#include "PalettePanelUtils.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "RLID.h"
//#include "RefreshData.h"
#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
//#include "ISpecialChar.h"
#include "ISubject.h"

#include "IBooleanControlData.h"
#include "IUIFontSpec.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("SDKListBoxHelper.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(x)	CAlert::InformationAlert(x)
//extern RefreshDataList rDataList;

////Global Pointers
//extern ISpecialChar* iConverter;

///////////////

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown* owner, const PluginID& pluginID, 
								   const WidgetID& listBoxID, const WidgetID& owningWidgetID) : fOwner(owner), 
													fOwningPluginID(pluginID), fListboxID(listBoxID),
													fOwningWidgetID(owningWidgetID)
{

}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwningPluginID=0;
}

IControlView* SDKListBoxHelper ::FindCurrentListBox(InterfacePtr<IPanelControlData> iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	//CA("SDKListBoxHelper ::FindCurrentListBox");
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView2 = nil;

	do {
		if(iPanelControlData ==nil)
		{
			break;
		}
		if(i==1)
		{
			listBoxControlView2 = iPanelControlData->FindWidget(kRLListBoxWidgetID);
		}
		
		if(listBoxControlView2 == nil) 
			break;

	} while(0);
	return listBoxControlView2;
}

void SDKListBoxHelper::AddElement(IControlView* lstboxControlView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{	
	if(!verifyState())
		return;
	do	
	{
		//CA("SDKListBoxHelper::AddElement ");
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil) 
		{
			CA("listAttr == nil");
			break;	
		}
	
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID==0)
		{
            CA("widgetRsrcID==0");
			break;
		}

		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwningPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		if(newElView==nil) 
		{
			CA("newElView==nil");
			break;
		}

		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x, isObject);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==nil) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/

void SDKListBoxHelper::addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{
	
	if(elView == nil || lstboxControlView == nil ) 
		return;
	do {
	
		//CA("addListElementWidget");
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if(newElPanelData == nil)
		{
			CA("newElPanelData == nil");
			break;
		}

		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if(nameTextView == nil)
		{
			CA("nameTextView == nil");
			break;
		}
//CA("addListElementWidget  2");
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if(newEltext == nil)
		{
			CA("newEltext == nil");		
			break;
		}
//CA("addListElementWidget  3");
		// Awasthi
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(displayName));*/
		// End Awasthi
		PMString stringToInsert("");
		stringToInsert.Append(displayName);
//CA("addListElementWidget  4" + stringToInsert);
        stringToInsert.ParseForEmbeddedCharacters();
		newEltext->SetString(stringToInsert, kTrue, kTrue);
		
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
			break;
//CA("addListElementWidget  5");
	
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
			break;

//CA("addListElementWidget  6");
		/*if(!isObject)
		{
			int indent=10;
			PMRect cellRect;
			elView->GetFrame();
			cellRect.Left(cellRect.Left()+indent);
			cellRect.Right(cellRect.Right()+indent);
			elView->SetFrame(cellRect);
		}*/
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
			break;

//CA("addListElementWidget  7");
		cellPanelData->AddWidget(elView);				
		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == nil)  
			break;
//CA("addListElementWidget  8");
		listData->Add(elView);	
	} while(0);
}

void SDKListBoxHelper::CheckUncheckRow(IControlView *listboxCntrlView, int32 index, bool checked)
{		
	do
	{
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);
		if(listCntl == nil)
			break;

		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil)
			break;

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		if(cellControlView == nil)
			break;

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());
		if(cellPanelData == nil)
			break;


		IControlView* primaryResourcePanelWidgetControlView = cellPanelData->GetWidget(index);
		if ( primaryResourcePanelWidgetControlView == nil ) 
			break;

		InterfacePtr<IPanelControlData> primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView, UseDefaultIID());		
		if(primaryResourcePanelWidgetPanelControlData == nil)
			break;

		
		int32 UnCheckIconWidgetIndex = 0;
		//int32 CheckIconWidgetIndex = 1;
		
		IControlView *UnCheckIconWidgetControlView=primaryResourcePanelWidgetPanelControlData->GetWidget(UnCheckIconWidgetIndex);
		if(UnCheckIconWidgetControlView==nil)
			break;

		InterfacePtr<IBooleanControlData> controlData(UnCheckIconWidgetControlView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No IBooleanControlData found on this boss.");
			break;
		}

		InterfacePtr<IUIFontSpec> fontSpec(UnCheckIconWidgetControlView, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) {
			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}

		int32 rsrcID = 0;
		//bool16 isPressedIn = controlData->IsSelected();
		
		if(!checked)  // Selectecd Star  Modified..Sachin !.....
		{//// Showing Selected Check Icon....
			
			//CA("SdklistBoxHelper.cpp..Line-438 else part..Show_Check_Icon_Widget");
			//UnCheckIconWidgetControlView->Show();
			controlData->Select();	
			rsrcID = fontSpec->GetFontID();

		}else  // Unselected Star
		{// Showing UnSelected Check Icon...
			
			//CA("in..Show_Check_Icon_Widget");
			IControlView *UnCheckIconWidgetControlView=primaryResourcePanelWidgetPanelControlData->GetWidget(UnCheckIconWidgetIndex);
			if(UnCheckIconWidgetControlView==nil)
			{
				//CA("UnCheckIconWidgetControlView==nil..Line..439");
				break;
			}
			//UnCheckIconWidgetControlView->Show();
			controlData->Deselect();
			rsrcID =fontSpec->GetHiliteFontID();
		}
		
		if(rsrcID != 0) {
			UnCheckIconWidgetControlView->SetRsrcID(rsrcID);
		}

	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox(IControlView *listboxCntrlView)
{
	
	do {
		//IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		//if(listBoxControlView == nil) 
		//	break;
		InterfacePtr<IListControlData> listData (listboxCntrlView, UseDefaultIID());
		if(listData == nil) 
			break;
		InterfacePtr<IPanelControlData> iPanelControlData(listboxCntrlView, UseDefaultIID());
		if(iPanelControlData == nil) 
			break;
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) 
			break;
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) 
			break;
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		//listBoxControlView->Invalidate();
		
	} while(0);
}

//void SDKListBoxHelper::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
//{
//	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
//	{
//		CA("ListBoxObserver::Update");
//		SDKListBoxHelper sList(this, kRfhPluginID);
//		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
//		if(listCntl == nil) 
//			return;
//
//		K2Vector<int32> curSelection ;
//		listCntl->GetSelected(curSelection ) ;
//		const int kSelectionLength =  curSelection.Length();
//		if(kSelectionLength<=0)
//			return;
//		rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
//		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
//	//	listCntl->DeselectAll();
//
//		bool16 isGroupByObj=Mediator::isGroupByObj;
//
//		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
//        CA("ListBoxObserver::Update 1");
//		if(isGroupByObj)//If its grouped by object
//		{
//			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
//			{
//				 CA("ListBoxObserver::Update 2");
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					CA("ListBoxObserver::Update 3");
//					if(rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//                    CA("ListBoxObserver::Update 4");
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				CA("ListBoxObserver::Update 5");
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(rDataList[j].isObject)
//					{
//						CA("ListBoxObserver::Update 6");
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						CA("ListBoxObserver::Update 7");
//						break;
//					}
//					
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//		}
//
//		///////////////////////////[ GROUPED BY ELEMENT ]//////////////////////////////
//
//		else//Its grouped by element
//		{
//			if((!rDataList[curSelection[0]].isObject) && curSelection[0]<rDataList.size())
//			{	
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(!rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{	
//				for(int j=curSelection[0]; j>=0; j--)
//				{	
//					if(!rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//					
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(!rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(!rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//
//		}		
//		
//	}
//}

void SDKListBoxHelper::AddElement(IControlView* lstboxControlView,  PMString & displayName1, WidgetID updateWidgetId1,
								   PMString & displayName2, WidgetID updateWidgetId2,
								  int atIndex)
{
	if(!verifyState())
		return;

	do			// false loop
	{
		//CA("SDKListBoxHelper::AddElement");
		/*IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}*/
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil) {
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
				return;
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwningPluginID/*fOwningPluginID*/, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {
			break;
		}
		this->AddListElementWidget(newElView, displayName1, updateWidgetId1, displayName2, updateWidgetId2, atIndex);
	
	}
	while (false);			// false loop
}


void SDKListBoxHelper::AddListElementWidget(InterfacePtr<IControlView> & elView,  PMString & displayName1,
											WidgetID updateWidgetId1,  PMString & displayName2,
											WidgetID updateWidgetId2, int atIndex)
{

	//CA("SDKListBoxHelper::AddListElementWidget");
	IControlView * listbox = this->FindCurrentListBox();
	if(elView == nil || listbox == nil ) {CA("elView == nil || listbox == nil");
		return;
	}

	do {
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) {CA("newElPanelData == nil");
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView1 = newElPanelData->FindWidget(updateWidgetId1);
		if ( (nameTextView1 == nil)  ) {CA("nameTextView1 == nil");
			break;
		}
//
		IControlView* nameTextView2 = newElPanelData->FindWidget(updateWidgetId2);
		if ( (nameTextView2 == nil)  ) {CA("nameTextView2 == nil");
			break;
		}

//
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext1 (nameTextView1,UseDefaultIID());
		if (newEltext1 == nil) {CA("newEltext1 == nil");
			break;
		}
        displayName1.ParseForEmbeddedCharacters();
		newEltext1->SetString(displayName1, kTrue, kTrue);
//
		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext2 (nameTextView2,UseDefaultIID());
		if (newEltext2 == nil) {CA("newEltext2 == nil");
			break;
		}
        displayName2.ParseForEmbeddedCharacters();
		newEltext2->SetString(displayName2, kTrue, kTrue);

		
		IControlView* nameTextView7 = newElPanelData->FindWidget(kRLCheckIconWidgetID);
		if ( (nameTextView7 == nil)  ) {CA("nameTextView7 == nil");
			break;
		}		
		
		InterfacePtr<IBooleanControlData> controlData(nameTextView7, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		    if(controlData==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No IBooleanControlData found on this boss.");
			break;
		}
		
		InterfacePtr<IUIFontSpec> fontSpec(nameTextView7, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) {
			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}

		int32 rsrcID = 0;

		//nameTextView7->Show();
		controlData->Select();
		rsrcID =fontSpec->GetHiliteFontID(); 
		
		if(rsrcID != 0) {			
			nameTextView7->SetRsrcID(rsrcID);		
		}

//
		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::AddListElementWidget() Cannot get panelData");
		if(panelData == nil) {
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::AddListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::AddListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) {
			break;
		}

		// Add the element widget to the list
		if(atIndex<0 || atIndex >= cellPanelData->Length()) {
			// Caution: an index of (-1) signifies add at the end of the panel controldata, but
			// and index of (-2) signifies add at the end of the list controldata.
			cellPanelData->AddWidget(elView);
		// add at the end (default)
		}
		else {
			cellPanelData->AddWidget(elView,atIndex);	
		}
		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());
		ASSERT_MSG(listData != nil, "SDKListBoxHelper::AddListElementWidget() listData nil");
		if(listData == nil) { 
			break;
		}
		listData->Add(elView, atIndex);
	} while(0);

}


IControlView * SDKListBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView = nil;
	do {
		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		ASSERT_MSG(iPanelControlData != nil, "SDKListBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) {
			break;
		}
		listBoxControlView = 	iPanelControlData->FindWidget(kRLListBoxWidgetID);
		ASSERT_MSG(listBoxControlView != nil, "SDKListBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) {
			break;
		}
	
	} while(0);

	return listBoxControlView;
}

void SDKListBoxHelper::UpdateStaticTextWidget(const IPanelControlData *  cellPanelWidgetPanelControlData,const WidgetID & widgetID, PMString & text,const int32 & index)
{
	do
	{
		
		
		
		
		
		IControlView* primaryResourcePanelWidgetControlView = cellPanelWidgetPanelControlData->GetWidget(index);
		if ( primaryResourcePanelWidgetControlView == nil )
		{
			CA("primaryResourcePanelWidgetControlView == nil");
			break;
		}

		InterfacePtr<IPanelControlData> primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView, UseDefaultIID());		
		if(primaryResourcePanelWidgetPanelControlData == nil)
		{
			CA("primaryResourcePanelWidgetPanelControlData == nil");
			break;
		}
		
		IControlView * textView = primaryResourcePanelWidgetPanelControlData->FindWidget(widgetID);
		if(textView == nil)
		{
			CA("textView == nil");
			break;
		}
		InterfacePtr<ITextControlData> iTextControlData(textView,UseDefaultIID());
		if(iTextControlData == nil)
		{
			
			CA("iTextControlData == nil");
			break;
		}
        text.ParseForEmbeddedCharacters();
		iTextControlData->SetString(text);
		

	}while(kFalse);
}