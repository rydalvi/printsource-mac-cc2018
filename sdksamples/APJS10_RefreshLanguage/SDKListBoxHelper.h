#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__
#include "ISubject.h"

class IPMUnknown;
class IControlView;
class IPanelControlData;

class SDKListBoxHelper
{
public:
	SDKListBoxHelper(IPMUnknown* fOwner, const PluginID& pluginID, const WidgetID& listBoxID, const WidgetID& owningWidgetID);
	virtual ~SDKListBoxHelper();
	void AddElement(IControlView* lstboxControlView, PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2, int x=1, bool16 isObject=kTrue);
	void RemoveElementAt(int indexRemove, int x);
	void RemoveLastElement( int x);
	IControlView * FindCurrentListBox(InterfacePtr<IPanelControlData> panel, int x);
	void EmptyCurrentListBox(IControlView *listboxCntrlView);

	int GetElementCount(int x);
	void CheckUncheckRow(IControlView* listboxCntrlView, int32, bool);
	void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);


	void AddElement(IControlView* lstboxControlView,  PMString & displayName1,WidgetID updateWidgetId1,
					  PMString & displayName2,WidgetID updateWidgetId2,
					 int atIndex = -2 /* kEnd */);

	void AddListElementWidget(InterfacePtr<IControlView> & elView, 
							   PMString & displayName1,WidgetID updateWidgetId1,
							   PMString & displayName2,WidgetID updateWidgetId2,
							  int atIndex);
	/**
		Query for the list-box on the panel or a dialog that is currently visible, assumes one visible at a time.
		@return reference to the current listbox, not add-ref'd.
	*/
	IControlView * FindCurrentListBox();

	void UpdateStaticTextWidget(const IPanelControlData * cellPanelWidgetPanelControlData,const WidgetID & widgetID, PMString & text,const int32 & index);


private:
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	void addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16);
	void removeCellWidget(IControlView * listBox, int removeIndex);
	
	IPMUnknown * fOwner;
	PluginID fOwningPluginID;
	WidgetID fListboxID;
	WidgetID fOwningWidgetID;
	
};

#endif 