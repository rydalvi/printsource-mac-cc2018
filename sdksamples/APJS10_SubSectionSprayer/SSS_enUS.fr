//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_enUS)
{
        k_enUS,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
        	 // ----- Menu strings
                kSSSCompanyKey,					kSSSCompanyValue,
                kSSSAboutMenuKey,					kSSSPluginName "[US]...",
                kSSSPluginsMenuKey,				kSSSPluginName "[US]",
				kSSSDialogMenuItemKey,			"Show dialog[US]",
	
                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
				kSSSDialogTitleKey,     kSSSPluginName "",


		// ----- Misc strings
                kSSSAboutBoxStringKey,			kSSSPluginName " [US], version " kSSSVersion " by " kSSSAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,
                kSSSHorizontalStringKey, "Horizontal Points:",
                kSSSVerticalStringKey, "Vertical Points:",
                kSSSWithoutPageBreakStringKey, "Without Page Break",
                kSSSEventStringKey, "Spray all Sections at current level",//"Event",
                kPageStringKey, " Page ",
                kAddSectionStencilStringKey, "Add Section Stencil",
                kAtStartOfSectionStringKey, "At The Start Of Section",
                kAtStartOfEachPageStringKey, "At The Start Of Each Page",
                kAtStartOfFirstPageOfSpreadStringKey, "At The Start Of First Page Of Spread",
                kHorizontalFlowForAllImageSpray, "Horizontal Flow For All Image Spray ", 
                kBkanStringKey, "",
				kSelectStencilStringKey, "Select Stencil",
				kRightStringKey,		"Right:",
				kLeftStringKey,			"Left:",
				kBottomStringKey,		"Bottom:",
				kTopStringKey,			"Top:",
				kMarginOffsetStringKey, "Margin Offsets",
		
        }

};

#endif // __ODFRC__
