#ifndef __TSDATANODE_H__
#define __TSDATANODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class TSDataNode
{
private:
	PMString strPublicationName;
	int32 pubId;
	int32 ParentId;
	int32 seqNumber;
	int32 childCount;
	int32 hitCount;
	
public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	TSDataNode():strPublicationName(""), pubId(0),  childCount(0), hitCount(0)	, seqNumber(0){}
	/*
		One time access to all the private members
	*/
	void setAll(PMString pubName, int32 pubId,int32 ParentId, int32 seqNumber, int32 childCount, int32 hitCount)
	{
		this->strPublicationName=pubName;
		this->pubId=pubId;
		this->ParentId = ParentId;	
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
	}
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->strPublicationName; }
	/*
		@returns the id of the publication
	*/
	int32 getPubId(void) { return this->pubId; }
		
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int32 getHitCount(void) { return this->hitCount; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	int32 getParentId(void) { return this->ParentId; }

	void setParentId(int32 ParentId) { this->ParentId = ParentId; }
	void setPubId(int32 pubId) { this->pubId=pubId; }	
	
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setPublicationName(PMString& pubName) { this->strPublicationName=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int32 hitCount) { this->hitCount=hitCount; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }
};

typedef vector<TSDataNode> TSDataNodeList;

#endif