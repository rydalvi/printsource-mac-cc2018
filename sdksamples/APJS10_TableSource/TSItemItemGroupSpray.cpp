#include "VCPluginHeaders.h"
#include "TSItemItemGroupSpray.h"
#ifdef MACINTOSH 
	#include "K2Vector.tpp"
#endif

#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "ISpread.h"
#include "ILayoutSelectionSuite.h"
#include "IPageList.h"
#include "IMargins.h"
#include "IClipboardController.h"
#include "IScrapSuite.h"
#include "IScrapItem.h"
#include "ISpreadList.h"
#include "ICommand.h"
#include "ICopyCmdData.h"
#include "IClipboardController.h"
#include "IDataExchangeHandler.h"
#include "IPageItemScrapData.h"



#define CA(X) CAlert::InformationAlert(X)


extern TSDataNodeList PRDataNodeList;
extern VectorScreenTableInfoPtr tableInfo;
extern double currentobjectID;
extern VectorAdvanceTableScreenValuePtr hybridTableInfo;
extern int32 SelectedRowNo;
extern TSDataNodeList ITEMDataNodeList;
extern VectorScreenTableInfoPtr typeTableValObj;
extern VectorAdvanceTableScreenValuePtr typeHybridTableValObj;
extern double currentSectionID;
extern double pubObjectID;
extern double itemId;
extern bool16 SprayAllButtonSelected;

bool16 isSprayItemPerFrameFlag1 = kFalse;
bool16 isItemHorizontalFlow1 = kFalse;
typedef vector<UID> PageUIDList;
PageUIDList pageUidList;
int32 PageCount;

char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";

bool16 ItemItemGroupSpray::getMaxLimitsOfBoxes(const UIDList& boxList, PMRect& maxBounds, vectorBoxBounds &boxboundlist)
{
	boxboundlist.clear();
	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;
	UIDRef boxUIDRef = boxList.GetRef(0);
	int index =0;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
	if(!geometryPtr)
	{
		//CA("!geometryPtr First time");
		if(boxList.Length() > 1)  // This is if first frame is deleted after spray // (Ref: New Indicator Spray Functionality)
		{
			boxUIDRef = boxList.GetRef(1);
			InterfacePtr<IGeometry> geometryPtr1(boxUIDRef, UseDefaultIID());
			geometryPtr = geometryPtr1;
			index = index+2;
		}
		else
		{	//CA("Return False from getMaxLimitsOfBoxes");
			return kFalse;
		}
	}
	else
	{
		index = index + 1;
	}

	if(!geometryPtr)
		return kFalse;

	PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
	
	PMReal top = boxBounds.Top();
	PMReal left = boxBounds.Left();
	PMReal bottom = boxBounds.Bottom();
	PMReal right = boxBounds.Right();

	BoxBounds orgBoxBound;
	orgBoxBound.Top = top;
	orgBoxBound.Left = left;
	orgBoxBound.Right = right;
	orgBoxBound.Bottom = bottom;
	orgBoxBound.BoxUIDRef = boxUIDRef;
	orgBoxBound.compressShift =0;
	orgBoxBound.enlargeShift =0;
	orgBoxBound.shiftUP = kTrue;
	boxboundlist.push_back(orgBoxBound);

	/*PMString ASD("orgBoxBound.Top() : ");
	ASD.AppendNumber((orgBoxBound.Top));
	ASD.Append("  orgBoxBound.Left : ");
	ASD.AppendNumber((orgBoxBound.Left));
	ASD.Append("  orgBoxBound.Bottom : ");
	ASD.AppendNumber((orgBoxBound.Bottom));
	ASD.Append("  orgBoxBound.Right : ");
	ASD.AppendNumber((orgBoxBound.Right));
	CA(ASD);*/

	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(index=1;index<boxList.Length();index++)
	{
		boxUIDRef = boxList.GetRef(index);

		InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
		if(!geometryPtr)
		{
			//CA("!geometryPtr");  // if any frame is missing then continue to other.
			continue;
		}

		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
				
		top = boxBounds.Top();
		left = boxBounds.Left();
		bottom = boxBounds.Bottom();
		right = boxBounds.Right();

		BoxBounds orgBoxBound1;
		orgBoxBound1.Top = top;
		orgBoxBound1.Left = left;
		orgBoxBound1.Right = right;
		orgBoxBound1.Bottom = bottom;
		orgBoxBound1.BoxUIDRef = boxUIDRef;
		orgBoxBound1.compressShift =0;
		orgBoxBound1.enlargeShift =0;
		orgBoxBound1.shiftUP = kTrue;
		boxboundlist.push_back(orgBoxBound1);

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	//PMString ASD("maxBounds.Top() : ");
	//ASD.AppendNumber((maxBounds.Top()));
	//ASD.Append("  maxBounds.Left() : ");
	//ASD.AppendNumber((maxBounds.Left()));
	//ASD.Append("  maxBounds.Bottom() : ");
	//ASD.AppendNumber((maxBounds.Bottom()));
	//ASD.Append("  maxBounds.Right() : ");
	//ASD.AppendNumber((maxBounds.Right()));
	//CA(ASD);
	return kTrue;

}
void ItemItemGroupSpray::moveAutoResizeBoxAfterSpray(const UIDList& copiedBoxUIDList, vectorBoxBounds vectorCurBoxBoundsBeforeSpray )
{
	do
	{
		//CA("inside moveAutoResizeBoxAfterSpray ");
		PMRect curMaxBoxBounds;
		vectorBoxBounds vectorCurBoxBounds;
		bool8 result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, curMaxBoxBounds, vectorCurBoxBounds);
		if(result == kFalse)
		{
			//CA("result == kFalse");
			break;
		}

		PMReal MaxTop =  curMaxBoxBounds.Top();
		InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
		return ;
		}	
///////////////
	/*	PMString ASD("Size of vectorCurBoxBounds : ");
		ASD.AppendNumber(vectorCurBoxBounds.size());
		CA(ASD);*/

		vectorBoxBounds TempVectorBoxBounds;
		vectorBoxBounds TempCurrVectorBoxBoundsBeforeSpray;
		TempCurrVectorBoxBoundsBeforeSpray.clear();
		TempVectorBoxBounds.clear();
		vectorBoxBounds vectorCurBoxBoundsBeforeSprayAfterDeletes;
		if(vectorCurBoxBoundsBeforeSpray.size() != vectorCurBoxBounds.size())
		{
			//CA("Size Diffears");
			for(int32 p=0; p < vectorCurBoxBoundsBeforeSpray.size(); p++)
			{				
				for(int32 q=0; q < vectorCurBoxBounds.size(); q++)
				{
					if(vectorCurBoxBoundsBeforeSpray[p].BoxUIDRef.GetUID() == vectorCurBoxBounds[q].BoxUIDRef.GetUID())
					{						
						vectorCurBoxBoundsBeforeSprayAfterDeletes.push_back(vectorCurBoxBoundsBeforeSpray[p]);
						break;
					}
				}
			}
			if(vectorCurBoxBoundsBeforeSprayAfterDeletes.size() != vectorCurBoxBounds.size())
			{	
			//	CA("Still Count Diffarers");
			}
			else
			{
				//CA("Now Count is same");
				vectorCurBoxBoundsBeforeSpray = vectorCurBoxBoundsBeforeSprayAfterDeletes;
			}
		}

		if(vectorCurBoxBoundsBeforeSpray.size() == vectorCurBoxBounds.size())
		{
			for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
			{	
				//if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
				//{  // When Sprayed Box Height is less than Original Box height
					//CA("Less Than 0--- 1");
					
					TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
					/*if(tList.size() == 0)
						continue;*/

					bool16 Flag = kFalse;	
					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{
						//CA("Less Than 0--- 2");
						if((vectorCurBoxBounds[j].Left == vectorCurBoxBounds[i].Left) && ( vectorCurBoxBounds[j].Right == vectorCurBoxBounds[i].Right)&& ( vectorCurBoxBounds[j].Top == vectorCurBoxBounds[i].Top) && ( vectorCurBoxBounds[j].Bottom == vectorCurBoxBounds[i].Bottom))
						{
							//CA("Less Than 0--- 3");
							continue;
						}

						if((vectorCurBoxBounds[j].Left <= vectorCurBoxBounds[i].Left) && ( vectorCurBoxBounds[j].Right >= vectorCurBoxBounds[i].Right)&& ( vectorCurBoxBounds[j].Top <= vectorCurBoxBounds[i].Top) && ( vectorCurBoxBounds[j].Bottom >= vectorCurBoxBounds[i].Bottom))
						{	
							/*PMString QWE(" vectorCurBoxBounds[j].Left : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Left);
							QWE.Append("  vectorCurBoxBounds[i].Left : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Left);
							QWE.Append(" vectorCurBoxBounds[j].Right : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Right);
							QWE.Append("  vectorCurBoxBounds[i].Right : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Right);
							QWE.Append(" vectorCurBoxBounds[j].Top : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Top);
							QWE.Append("  vectorCurBoxBounds[i].Top : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Top);
							QWE.Append(" vectorCurBoxBounds[j].Bottom : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Bottom);
							QWE.Append("  vectorCurBoxBounds[i].Bottom : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Bottom);
							CA(QWE);*/
							
							//CA("Less Than 0--- 4");
							if(tList.size() > 0)
							{
								if(tList[0].imgFlag == 1)
								{	
									//CA("Current Box is Inside Other Box");
									Flag = kTrue;	
								}
							}
						}
					}
					if(Flag== kFalse){		
						//CA("Less Than 0--- 5");
						vectorCurBoxBounds[i].isUnderImage = kFalse;
						TempVectorBoxBounds.push_back(vectorCurBoxBounds[i]);
						TempCurrVectorBoxBoundsBeforeSpray.push_back(vectorCurBoxBoundsBeforeSpray[i]);
					}
					//CA("Less Than 0--- 6");
					//------------
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					continue;
				//}	
			

			}
		//	CA("Less Than 0--- 7");
			vectorCurBoxBounds = TempVectorBoxBounds;
			vectorCurBoxBoundsBeforeSpray = TempCurrVectorBoxBoundsBeforeSpray;
		}
		else
		{ 
			//CA(" vectorCurBoxBoundsBeforeSpray.size() != vectorCurBoxBounds.size() Returning");
			return;
		}
		/*ASD.Clear();
		ASD.Append("Size of vectorCurBoxBounds : ");
		ASD.AppendNumber(vectorCurBoxBounds.size());
		CA(ASD);*/
//////////
		
//PMString s("vectorCurBoxBounds.size() : ");
//s.AppendNumber(vectorCurBoxBounds.size());
//CA(s);
		for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
		{
			if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) == 0)
			{	//CA("Equal");
				TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
				if(tList.size() == 0)
					continue;
				else if(tList[0].imgFlag == 1)
				{	
					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{
						if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
						{	//CA("shiftUP = kTrue");
							vectorCurBoxBounds[j].shiftUP = kTrue;
							vectorCurBoxBounds[j].isUnderImage = kFalse;
						}
						else
						{	//CA("shiftUP = kFalse");
							vectorCurBoxBounds[j].shiftUP = kFalse;
							vectorCurBoxBounds[j].isUnderImage = kTrue;
						}
					}
				}
				//------------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				continue;
				
			}
			else if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
			{  // When Sprayed Box Height is less than Original Box height
				///CA("Less Than 0");
				
				TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
				if(tList.size() == 0){// CA(" tList.size() == 0 continue");
					continue;
				}
				
				//CA("Before cheking img flag");
				if(tList[0].imgFlag == 1)
				{	
					//CA("Image Flag Found");
					PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
				
					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{	//CA("0000");
						if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
						{//	CA("0001");
							if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
							{	//CA("Before Continue");
								vectorCurBoxBounds[j].isUnderImage = kFalse;
								continue;
							}
							else
							{	
								PMReal top = (IncreaseByPoints);
								vectorCurBoxBounds[j].compressShift = top;	
								vectorCurBoxBounds[j].shiftUP = kTrue;
								vectorCurBoxBounds[j].isUnderImage = kTrue;	
								//CA("UnderImage true");
							}
						}
					}
				}
					//------------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}

			}
			
				
		}
		


		ErrorCode errorCode;
		for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
		{
			if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) == 0)
			{	//CA("Equal");					
				continue;				
			}
			else if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
			{  // When Sprayed Box Height is less than Original Box height
				//CA("Sprayed Box Height is less than Original Box height");
				
				PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
				
				TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
				if(tList.size() == 0)
					continue;					

				for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
				{	
					if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
					{	
						if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
						{	//CA("Before Continue");
							continue;
						}
						else
						{	
							if(vectorCurBoxBounds[j].shiftUP == kFalse)
							{
								//CA("vectorCurBoxBounds[j].shiftUP == kFalse");	
								continue;
							}
							if(vectorCurBoxBounds[j].shiftUP == kTrue && vectorCurBoxBounds[j].isUnderImage == kFalse )
							{ 
								//CA("vectorCurBoxBounds[j].shiftUP == kTrue AND isUnderImage == kFalse");
								UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
								
								PMReal top = (IncreaseByPoints);
								PMReal left = vectorCurBoxBounds[j].Left;

								const PBPMPoint moveByPoints(0, top);
									//Commented By Sachin Sharma on 2/07/07
								/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
								MovePageItemRelative(transform, moveByPoints);*/
								//===============================================Added
								UIDList moveUIDList(boxUIDRef);
								Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
								PBPMPoint referencePoint(PMPoint(0,0));
								errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(0, top));

								//===============================================


							}

							if((vectorCurBoxBounds[j].shiftUP == kTrue) && (vectorCurBoxBounds[j].isUnderImage == kTrue) && (tList[0].imgFlag == 1))
							{  //CA("vectorCurBoxBounds[j].shiftUP == kTrue  AND tList[0].imgFlag == 1");
								UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
								
								PMReal top = vectorCurBoxBounds[j].compressShift;
								PMReal left = vectorCurBoxBounds[j].Left;

								const PBPMPoint moveByPoints(0, top);
								//Commented By Sachin sharma on 2/07/07
								/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
								MovePageItemRelative(transform, moveByPoints);*/

								UIDList moveUIDList(boxUIDRef);
								Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
								PBPMPoint referencePoint(PMPoint(0,0));
								errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(0, top));


							}
						}
					}
				}					
					
				//------------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}

			}
			else if( (vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) > 0)
			{ 
				// When Sprayed Box Height is Greater Than Original Box Height
				//CA("Sprayed Box Height is Greater Than Original Box Height");
				PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);

				for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
				{	
					if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
					{	
						//CA("vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top");
						if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
						{	
							//CA("Before Continue");
							continue;
						}
						else
						{	
							//CA("Elseeeeeeeeeee");
							UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
							
							/*PMString ASD("Increase By Points : ");
							ASD.AppendNumber(IncreaseByPoints);
							CA(ASD);*/

							/*PMString ASD1("vectorCurBoxBoundsBeforeSpray[j].Top : ");
							ASD1.AppendNumber(vectorCurBoxBoundsBeforeSpray[j].Top);
							CA(ASD1);*/

							PMReal top = IncreaseByPoints;
							PMReal left = vectorCurBoxBounds[j].Left;

							const PBPMPoint moveByPoints(0, top);
							//Commented By Sachin sharma on 2/07/07
							/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
							MovePageItemRelative(transform, moveByPoints);*/
							//============================================
							UIDList moveUIDList(boxUIDRef);
							Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
							PBPMPoint referencePoint(PMPoint(0,0));
							errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(0, top));
							//=============================

						}
					}
				}
            	
			}
				
				
		}
	

	}
	while(kFalse);
}

void ItemItemGroupSpray::startSpraying()
{
	//CA("ItemItemGroupSpray::startSpraying()");
	do{
			
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		IControlView* ProductTreePanelCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
		if(ProductTreePanelCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::Update::ProductTreePanelCtrlView is nil");
			break;
		}
		TSMediatorClass::PRLstboxCntrlView = ProductTreePanelCtrlView;

		InterfacePtr<ITreeViewController> treeViewMgr(ProductTreePanelCtrlView, UseDefaultIID());
		if(treeViewMgr==nil)
		{
			break;
		}
		
		NodeIDList selectedItem;
		treeViewMgr->GetSelectedItems(selectedItem);

		if( (selectedItem.size()<1) && (SprayAllButtonSelected == kFalse)  )
		{
			break;
		}


		InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::No itagReader");			
			break;
		}
		
		UIDList selectUIDList;
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			//CA("ReturniSelectionManager");
			break;
		}

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			//CA("returntxtMisSuite");//
			break; 
		}
		
		txtMisSuite->GetUidList(selectUIDList);
		if(selectUIDList.size() < 1)
			break;

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::Pointre to DataSprayerPtr not found");
			break;
		}
				

		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
	    if(ptrTableSourceHelper == nil)
	    {
			//CA("TABLEsource not found ");
			break;
	    }
		TableSourceInfoValue TableSourceInfoValueObj;
		vector<double> selectedTableID;
		vector<double> selectedTableTypeID; 
		
		int32 hitCount=0;
		PMString sn("Selected Node : ");
		sn.AppendNumber(selectedItem.size());
		//CA(sn);
		for(int32 selectedNodeIndex = 0 ; selectedNodeIndex < selectedItem.size() ; selectedNodeIndex ++)
		{
			NodeID nid=selectedItem[selectedNodeIndex];

			TreeNodePtr<UIDNodeID>uidNodeIDTemp(nid);

			int32 uid= uidNodeIDTemp->GetUID().Get();

			TSDataNode tsNode;
			TSTreeDataCache dc;

			dc.isExist(uid, tsNode);
			
			PMString pfName = tsNode.getName();
			PMString a("pfName : ");
			a.Append(pfName);
			//CA(a);
							
			TSMediatorClass::curSelLstbox = SelectedRowNo;
			TSMediatorClass::curSelRowIndex = tsNode.getSequence();
			TSMediatorClass::curSelRowString = pfName;

			TSTreeData PRTreeData;
			int32 vectSize=PRTreeData.returnListVectorSize(SelectedRowNo);
			TSTreeInfo PRTreeInfo;
			// For getting data of the selected row of Tree 
			for(int y=0;y<vectSize;y++)
			{
				if(y==TSMediatorClass::curSelRowIndex)
				{
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,y);
					break;
				}
			}
			selectedTableID.push_back(PRTreeInfo.id);
			selectedTableTypeID.push_back(PRTreeInfo.typeId);
		
			if(SelectedRowNo == 3)
				hitCount = PRDataNodeList.at(uid-1).getHitCount();
			if(SelectedRowNo == 4)
				hitCount = ITEMDataNodeList.at(uid-1).getHitCount();
	
		}

		int32 listLength = selectUIDList.Length();
		/*PMString  l("listLength : ");
		l.AppendNumber(listLength);
		CA(l);*/
		
		if((selectedItem.size() > 1) || (SprayAllButtonSelected == kTrue) )
		{

			if(SprayAllButtonSelected == kTrue)
			{
				selectedTableID.clear();
				selectedTableTypeID.clear();

				TSTreeData PRTreeData;
				int32 vectSize=PRTreeData.returnListVectorSize(SelectedRowNo);
				
				// For getting data of the all row of Tree 
				for(int y=0;y<vectSize;y++)
				{
					TSTreeInfo PRTreeInfo;
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,y);
					selectedTableID.push_back(PRTreeInfo.id);
					selectedTableTypeID.push_back(PRTreeInfo.typeId);
				}
				if(vectSize > 0)
				{
					if(SelectedRowNo == 3)
					hitCount = PRDataNodeList.at(0).getHitCount();
					if(SelectedRowNo == 4)
					hitCount = ITEMDataNodeList.at(0).getHitCount();
				}

			}

			if(selectedTableID.size() == 0)
				return;
			
			if(selectedTableID.size() > 1)
			{
				bool16 resultFlag =  SprayTemplatePerTablefromTableSource(selectUIDList, selectedTableID, selectedTableTypeID, hitCount );
				return;
			}
		}


		TagList tagList;

		isSprayItemPerFrameFlag1 = checkIsSprayItemPerFrameTag(selectUIDList , isItemHorizontalFlow1);
		if(isSprayItemPerFrameFlag1)
		{
			if(isItemHorizontalFlow1)
				DataSprayerPtr->setFlow(kTrue);
			else
				DataSprayerPtr->setFlow(kFalse);
		}
		//CA("before start of big if");
		if(isSprayItemPerFrameFlag1/*iSSSprayer->getSprayItemPerFrameFlag()*/)
		{
			//CA("isSprayItemPerFrameFlag1");

			if(SelectedRowNo == 3)
			{
				//CA("SelectedRowNo == 3");
				TableSourceInfoValueObj.setAll(NULL,NULL,NULL,kTrue,kTrue,kTrue,selectedTableTypeID,selectedTableID);
				
			}
			if(SelectedRowNo == 4)
			{
				//CA("SelectedRowNo == 4");
				TableSourceInfoValueObj.setAll(NULL,NULL,NULL,kFalse,kTrue,kTrue,selectedTableTypeID,selectedTableID);				
			}
			ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);

			//CA("Inside isSprayItemPerFrameFlag case");
			vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
			PMRect CopiedItemMaxBoxBoundsBforeSpray;
			UIDList ItemFrameUIDList(selectUIDList.GetDataBase());
			//CA("1");
			bool16 result = kFalse;
			result = this->getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);

			
			for(int i=0; i<listLength; i++)
			{
				tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
				if(tagList.size()<=0)//This can be a Tagged Frame
				{	
					//CA(" tagList.size()<=0 ");
					if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
					{	
						//CA("if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))");
						tagList.clear();
						tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));
						if(tagList.size()==0)//Ordinary box
						{					
							continue ;
						}	
						
						for(int32 j=0; j <tagList.size(); j++)
						{
							if(tagList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
							{
								//CA("Item Tag Found");
								ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
								break; // break out from for loop
							}
						}
					}
					else
					{
						// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
						//CA(" else DataSprayerPtr->isFrameTagged");
						InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
						if(!iHier)
						{
							//CA(" !iHier >> Continue ");
							continue;
						}
						UID kidUID;				
						int32 numKids=iHier->GetChildCount();				
						IIDXMLElement* ptr = NULL;

						for(int j=0;j<numKids;j++)
						{
							//CA("Inside For Loop");
							bool16 Flag12 =  kFalse;
							kidUID=iHier->GetChildUID(j);
							UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);			
							TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
							if(NewList.size()<=0)//This can be a Tagged Frame
							{
								NewList.clear();
								NewList=itagReader->getFrameTags(selectUIDList.GetRef(i));
								if(NewList.size()==0)//Ordinary box
								{					
									continue ;
								}	
								
								for(int32 j=0; j <NewList.size(); j++)
								{
									if(NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
									{
										//CA("Item Tag Found");
										ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
										Flag12 = kTrue;
										break; // break out from for loop
									}
								}
							}
							else
							{
								for(int32 j=0; j <NewList.size(); j++)
								{
									if(NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
									{
										//CA("Item Tag Found");
										ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
										Flag12 = kTrue;
										break; // break out from for loop
									}
								}
							}
							//-------lalit-----
							for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
							{
								NewList[tagIndex].tagPtr->Release();
							}

							if(Flag12)
								break;
						}
					}
				}
				else
				{
					for(int32 j=0; j <tagList.size(); j++)
					{
						if(tagList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
						{
							//CA("Item Tag Found");
							ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());					
							break; // break out from for loop
						}
					}
				}

				for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
				{
					tagList[tagIndex].tagPtr->Release();
				}
			}
		
			int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
			if(ItemFrameUIDListSize == 0)
			{
				//CA("ItemFrameUIDListSize == 0");
				//SingleItemSprayReturnFlag = kTrue;
				ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::SingleItemSprayReturnFlag = kTrue;");
			}
			/*PMString ASD("Length of ItemFrameUIDList : ");
			ASD.AppendNumber(ItemFrameUIDListSize);
			CA(ASD);*/

			int AlphabetArrayCount =0;

			//if((pNodeDataList[sprayedProductIndex].getIsProduct() == 1)  || (pNodeDataList[sprayedProductIndex].getIsProduct() == 0))
			if(SelectedRowNo == 3 || SelectedRowNo == 4)
			{
				//CA("Only Product Spray");			
				VectorLongIntPtr ItemIDInfo = NULL;
				vector<double> FinalItemIds;
				do
				{
					//if(pNodeDataList[sprayedProductIndex].getIsONEsource())
					//{
					//	//If ONEsource mode  is selected and Table stencils for item is selected.Then to get all information of table.
					//	ItemIDInfo=ptrIAppFramework->GETProduct_getAllItemIDsFromTables(pNodeDataList[sprayedProductIndex].getPubId());
					//}
					//else
					{
						//If publication mode is selected.					
						/*PMString ASD("ObjectID : ");
						ASD.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());
						ASD.Append("  CurrentSectionID :  " );
						ASD.AppendNumber(CurrentSelectedSubSection);
						CA(ASD);*/
						//ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(pNodeDataList[sprayedProductIndex].getPubId(), CurrentSelectedSubSection);

						if(SelectedRowNo == 3/*pNodeDataList[sprayedProductIndex].getIsProduct() == 1*/)
						{
							//ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(currentobjectID, currentSectionID);
							VectorScreenTableInfoPtr tableInfo = NULL;
							//if(pNode.getIsONEsource())
							//{
							//	// For ONEsource mode
							//	tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
							//}else
							{
								//For publication mode 
//CA("XXXXXXXXXXXXXXXXXXXXXXXX");
								tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(currentSectionID, currentobjectID, TSMediatorClass::CurrLanguageID, kTrue);
							}

							if(!tableInfo)
							{
								ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
								//ItemAbsentinProductFlag = kTrue;
								break;
							}

							vector<double>::iterator itrID;		

							vector<double> vec_items;
							CItemTableValue oTableSourceValue;
							VectorScreenTableInfoValue::iterator itr;
							for(itr = tableInfo->begin(); itr!=tableInfo->end(); itr++)
							{
								oTableSourceValue = *itr;				
								vec_items = oTableSourceValue.getItemIds();
								double table_ID = oTableSourceValue.getTableID();
								double table_Type_ID = oTableSourceValue.getTableTypeID();
								int32 check=0;

						
								for(itrID=selectedTableID.begin(); itrID !=selectedTableID.end();itrID++,check++)
								{
									/*PMString t("selectedTblTypID : ");
										t.AppendNumber(selectedTblTypID.at(check));
										t.Append("\r table_Type_ID : ");
										t.AppendNumber(table_Type_ID);
										t.Append("\r selectedTblID  : ");
										t.AppendNumber(selectedTblID.at(check));
										t.Append("\r table_ID : ");
										t.AppendNumber(table_ID);
										CA(t);*/
									if(selectedTableTypeID.at(check) != table_Type_ID || selectedTableID.at(check) != table_ID)
										continue;
										
									/*PMString t1("Size : ");
									t1.AppendNumber(vec_items.size());
									CA(t1);*/

									if(FinalItemIds.size() == 0)
									{
										FinalItemIds = vec_items;
										/*for(int32 index = 0 ; index < FinalItemIds.size() ; index++)
										{
											vecTableID.push_back(table_ID);
											vecTableTypeID.push_back(table_Type_ID);
										}*/
									}
									else
									{
										for(int32 i=0; i<vec_items.size(); i++)
										{
											bool16 Flag = kFalse;
											for(int32 j=0; j<FinalItemIds.size(); j++)
											{
												if(vec_items[i] == FinalItemIds[j])
												{
													Flag = kTrue;
													break;
												}				
											}
											if(!Flag )
											{
												FinalItemIds.push_back(vec_items[i]);
												/*vecTableID.push_back(table_ID);
												vecTableTypeID.push_back(table_Type_ID);*/
											}
											
										}
									}
									
								}
								
							}


						
							if(tableInfo)
								delete tableInfo;

							ItemIDInfo = &FinalItemIds;
						
						}
						else if(SelectedRowNo == 4/*pNodeDataList[sprayedProductIndex].getIsProduct() == 0*/)
						{
							do
							{				
								VectorScreenTableInfoPtr tableInfo=
									ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(/*pubObjectID*/itemId, TSMediatorClass::sectionID, TSMediatorClass::CurrLanguageID);
								if(!tableInfo)
								{
									ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
									break;
								}
								if(tableInfo->size()==0)
								{ 
									ptrIAppFramework->LogInfo("AP7_TABLEsource::ItemItemGroupSpray::startSpraying: table size = 0");
									break;
								}
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;

								bool16 typeidFound=kFalse;
								vector<double> vec_items;
								vector<double>::iterator itrID;
								

								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{//for tabelInfo start				
									oTableValue = *it;

									double table_ID = oTableValue.getTableID();
									double table_Type_ID = oTableValue.getTableTypeID();
									int32 check=0;							

									vec_items = oTableValue.getItemIds();

									for(itrID=selectedTableID.begin(); itrID !=selectedTableID.end();itrID++,check++)
									{
										/*PMString t("selectedTblTypID : ");
											t.AppendNumber(selectedTblTypID.at(check));
											t.Append("\r table_Type_ID : ");
											t.AppendNumber(table_Type_ID);
											t.Append("\r selectedTblID  : ");
											t.AppendNumber(selectedTblID.at(check));
											t.Append("\r table_ID : ");
											t.AppendNumber(table_ID);
											CA(t);*/
										if(selectedTableTypeID.at(check) != table_Type_ID || selectedTableID.at(check) != table_ID)
											continue;
								
										if(FinalItemIds.size() == 0)
										{
											FinalItemIds = vec_items;
										}
										else
										{
											for(int32 i=0; i<vec_items.size(); i++)
											{	bool16 Flag = kFalse;
												for(int32 j=0; j<FinalItemIds.size(); j++)
												{
													if(vec_items[i] == FinalItemIds[j])
													{
														Flag = kTrue;
														break;
													}				
												}
												if(!Flag)
													FinalItemIds.push_back(vec_items[i]);
											}
										}
									}
								}//for tabelInfo end
								if(tableInfo)
									delete tableInfo;

							}while(kFalse);
							ItemIDInfo = &FinalItemIds;
						}
					}
					if(!ItemIDInfo)
					{	
						//SingleItemSprayReturnFlag = kTrue;
						//CA("AP46_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo is NULL");
						ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying:::ItemIDInfo is NULL");																										
						break;
					}
					
					if(ItemIDInfo->size()==0)
					{
						//SingleItemSprayReturnFlag = kTrue;
						//CA("ItemIDInfo->size()==0");
						ptrIAppFramework->LogInfo("AP7_TABLEsource::ItemItemGroupSpray::startSpraying:::ItemIDInfo->size()==0");
						break;
					}
							
					UIDRef originalPageUIDRef, originalSpreadUIDRef;
					result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
					if(result == kFalse){ 
						//SingleItemSprayReturnFlag = kTrue;
						//CA("AP46_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
						ptrIAppFramework->LogError("AP7_TABLEsource::ItemItemGroupSpray::startSpraying:::!getCurrentPage");
						break;
					}

					PMRect PagemarginBoxBounds;
					result = getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
					if(result == kFalse)
					{
						result = getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
						if(result == kFalse)
						{
//							SingleItemSprayReturnFlag = kTrue;
							///CA("result == kFalse");
							break;
						}
					}

					PMRect ItemFramesStencilMaxBounds = kZeroRect;
					vectorBoxBounds ItemFramesBoxBoundVector;

					result = getMaxLimitsOfBoxes(ItemFrameUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
					if(result == kFalse){
//						SingleItemSprayReturnFlag = kTrue;
						//CA("getMaxLimitsOfBoxes result == kFalse");
						break;
					}
					
					PMReal LeftMark = (ItemFramesStencilMaxBounds.Left());
					PMReal BottomMark = (ItemFramesStencilMaxBounds.Bottom());
					PMReal TopMark = (ItemFramesStencilMaxBounds.Top());
					PMReal RightMark = (ItemFramesStencilMaxBounds.Right());

					if(LeftMark > PagemarginBoxBounds.Right() )
					{
						InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
						if (layoutData == nil)
							break;

						IDocument* document = layoutData->GetDocument();
						if (document == NULL)
							break;

						IDataBase* database = ::GetDataBase(document);
						if(!database)
							break;
						//CA("Got wrong Page UID");
						/*IGeometry* spreadItem = layoutData->GetSpread();
						if(spreadItem == nil)
							break;*/
						UIDRef ref = layoutData->GetSpreadRef();

						InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
						if (iSpread == nil)
							break;

						int numPages=iSpread->GetNumPages();
						
						UID pageUID= iSpread->GetNthPageUID(numPages-1);

						UIDRef pageRef(database, pageUID);
						getPageBounds (pageRef, PagemarginBoxBounds);
					}

					InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) {
//						SingleItemSprayReturnFlag = kTrue;
						//CA("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");										
						ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::!layoutSelectionSuite");										
						break;
					}
					selectionManager->DeselectAll(nil); // deselect every active CSB

					int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
					/*PMString ASD("Length of ItemFrameUIDList : ");
			ASD.AppendNumber(ItemFrameUIDListSize);
			CA(ASD);*/

					//layoutSelectionSuite->Select(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	  //Commented By Sachin sharma on 2/07/07
					layoutSelectionSuite->SelectPageItems(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

					//copy the selected items
					CopySelectedItems();

					//now get the copied item list
					UIDList FirstcopiedBoxUIDList;
					result = getSelectedBoxIds(FirstcopiedBoxUIDList);
					if(result == kFalse)
						break;

					// For First Item of Product
					VectorLongIntValue::iterator it1;
					it1 = ItemIDInfo->begin();
					double FirstItemId = *it1;
					it1++;
					AlphabetArrayCount =0;

					for(int i=0; i<ItemFrameUIDListSize; i++)
					{
						tagList=itagReader->getTagsFromBox(ItemFrameUIDList.GetRef(i));
						if(tagList.size()<=0)//This can be a Tagged Frame
						{	
							//CA(" tagList.size()<=0 ");
							if(DataSprayerPtr->isFrameTagged(ItemFrameUIDList.GetRef(i)))
							{	
								tagList.clear();
								tagList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
								if(tagList.size()==0)//Ordinary box
								{					
									continue ;
								}	
								
								for(int32 j=0; j <tagList.size(); j++)
								{
									if(tagList[j].whichTab == 4)
									{
										////CA("Item Tag Found");
										//ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
										//break; // break out from for loop
										if(tagList[j].imgFlag == 1)
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(FirstItemId));
											tagList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
										
											attributeValue.clear();
											attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
											tagList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4
										}
										else
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(FirstItemId));
											tagList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
											tagList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

											PMString childTag("1");
											tagList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));

											if(tagList[j].elementId == -803)
											{
												attributeValue.Clear();
												if(AlphabetArrayCount < 26)
													attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
												else
													attributeValue.Append("a");
												tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
											}
											else if(tagList[j].elementId == -827)
											{
												attributeValue.Clear();
												int32 numberKey = AlphabetArrayCount + 1;
												
												attributeValue.AppendNumber(numberKey);

												tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
											}
										}
										PMString attributeValue;
										attributeValue.AppendNumber(-777);
										tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
									}
								}

								for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
								{
									tagList[tagIndex].tagPtr->Release();
								}
							}
							else
							{
								// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
								//CA(" else DataSprayerPtr->isFrameTagged");
								InterfacePtr<IHierarchy> iHier(ItemFrameUIDList.GetRef(i), UseDefaultIID());
								if(!iHier)
								{
									//CA(" !iHier >> Continue ");
									continue;
								}
								UID kidUID;				
								int32 numKids=iHier->GetChildCount();				
								IIDXMLElement* ptr = NULL;

								for(int j=0;j<numKids;j++)
								{
									//CA("Inside For Loop");
									bool16 Flag12 =  kFalse;
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(ItemFrameUIDList.GetDataBase(), kidUID);			
									TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
									if(NewList.size()<=0)//This can be a Tagged Frame
									{
										NewList.clear();
										NewList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
										if(NewList.size()==0)//Ordinary box
										{					
											continue ;
										}	
										
										for(int32 j=0; j <NewList.size(); j++)
										{
											if(NewList[j].whichTab == 4)
											{
												if(tagList[j].imgFlag == 1)
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(FirstItemId));
													tagList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
													
													attributeValue.clear();
													attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
													tagList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4
												}
												else
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(FirstItemId));
													tagList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
													tagList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

													PMString childTag("1");
													tagList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));

													if(tagList[j].elementId == -803)
													{
														attributeValue.Clear();
														if(AlphabetArrayCount < 26)
															attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
														else
															attributeValue.Append("a");
														tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
													}
													else if(tagList[j].elementId == -827)
													{
														attributeValue.Clear();
														int32 numberKey = AlphabetArrayCount + 1;
														
														attributeValue.AppendNumber(numberKey);

														tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
													}
												}
												PMString attributeValue;
												attributeValue.AppendNumber(-777);
												tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
											
											}
										}
									}
									else
									{
										for(int32 j=0; j <NewList.size(); j++)
										{
											if(NewList[j].whichTab == 4)
											{
												if(NewList[j].imgFlag == 1)
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(FirstItemId));
													NewList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
												
													attributeValue.clear();
													attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
													NewList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4
												
												}
												else
												{
													PMString attributeValue;
													attributeValue.AppendNumber(PMReal(FirstItemId));
													NewList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
													NewList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

													PMString childTag("1");
													NewList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
													if(NewList[j].elementId == -803)
													{
														attributeValue.Clear();
														if(AlphabetArrayCount < 26)
															attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
														else
															attributeValue.Append("a");
														NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
													}
													else if(tagList[j].elementId == -827)
													{
														attributeValue.Clear();
														int32 numberKey = AlphabetArrayCount + 1;
														
														attributeValue.AppendNumber(numberKey);

														NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
													}
												}
												PMString attributeValue;
												attributeValue.AppendNumber(-777);
												NewList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
											
											}
										}
									}
									//------------
									for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
									{
										NewList[tagIndex].tagPtr->Release();
									}

									if(Flag12)
										break;
								}
							}
						}
						else
						{
							for(int32 j=0; j <tagList.size(); j++)
							{
								if(tagList[j].whichTab == 4)
								{
									if(tagList[j].imgFlag == 1)
									{
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(FirstItemId));
										tagList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
									
										attributeValue.clear();
										attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
										tagList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4
									
									}
									else
									{
										PMString attributeValue;
										attributeValue.AppendNumber(PMReal(FirstItemId));
										tagList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
										tagList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

										PMString childTag("1");
										tagList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
										
										if(tagList[j].elementId == -803)
										{
											attributeValue.Clear();
											if(AlphabetArrayCount < 26)
												attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
											else
												attributeValue.Append("a");
											tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue));//Cs4
										}
										else if(tagList[j].elementId == -827)
										{
											attributeValue.Clear();
											int32 numberKey = AlphabetArrayCount + 1;
											
											attributeValue.AppendNumber(numberKey);

											tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
										}
									}
									PMString attributeValue;								
									attributeValue.AppendNumber(-777);
									tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
								
								}
							}
						}

						for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
						{
							tagList[tagIndex].tagPtr->Release();
						}

					}
					
                    PublicationNode pNode;
                    pNode.setPubId(currentobjectID);
                    pNode.setParentId(TSMediatorClass::sectionID);
                    
                    if(SelectedRowNo == 3)
                        pNode.setIsProduct(1);
                    if(SelectedRowNo == 4)
                        pNode.setIsProduct(0);
                    
                    pNode.setHitCount(hitCount);
                    pNode.setIsONEsource(kFalse);
                    pNode.setSectionID(TSMediatorClass::sectionID);
                    pNode.setPBObjectID(TSMediatorClass::pbObjectID);
                    pNode.setTypeId(TSMediatorClass::parentTypeID);
                    
                    if(hitCount == 2)
                    {
                        DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);
                        
                        if(SelectedRowNo == 3)
                            TableSourceInfoValueObj.setAll(NULL,NULL,hybridTableInfo,kTrue,kTrue,kTrue,selectedTableTypeID,selectedTableID);
                        if(SelectedRowNo == 4)
                            TableSourceInfoValueObj.setAll(NULL,NULL,typeHybridTableValObj,kFalse,kTrue,kTrue,selectedTableTypeID,selectedTableID);
                        
                        ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
                        
                    }
                    if(hitCount == 8)
                    {
                        DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);
                        
                        if(SelectedRowNo == 3)
                            TableSourceInfoValueObj.setAll(NULL,tableInfo,NULL,kTrue,kTrue,kTrue,selectedTableTypeID,selectedTableID);
                        if(SelectedRowNo == 4)
                            TableSourceInfoValueObj.setAll(typeTableValObj,NULL,NULL,kFalse,kTrue,kTrue,selectedTableTypeID,selectedTableID);
                        
                        ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
                        
                    }
                    
                    DataSprayerPtr->getAllIds(pNode.getPubId());

                    
					if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/)
						DataSprayerPtr->setFlow(kTrue);
					else
						DataSprayerPtr->setFlow(kFalse);
			//		DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());
			//		DataSprayerPtr->setFlow(isItemHorizontalFlow);

					DataSprayerPtr->ClearNewImageFrameList();

					//CA("Before Sprayting Original Frame");
					////// Spraying original frames first:
					ICommandSequence *seq=CmdUtils::BeginCommandSequence();
					for(int i=0; i<listLength; i++)
					{ 
						PMString allInfo;	
						//CA("before calling itagReader->getTagsFromBox()");
						tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
						//CA("after calling itagReader->getTagsFromBox()");
						if(tagList.size()<=0)//This can be a Tagged Frame
						{	
							//CA(" tagList.size()<=0 ");
							if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
							{	
								//CA("isFrameTagged");
								bool16 flaG = kFalse;		

								tagList.clear();
								tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));

								if(tagList.size()==0)//Ordinary box
								{					
									continue ;
								}

								//CA("Frame Tags Found");
								InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
								if (!layoutSelectionSuite) {
									break;
								}
						
								selectionManager->DeselectAll(nil); // deselect every active CSB
								//layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
								layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
							
								//CA("Before sprayForTaggedBox");
								DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));	

								for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
								{
									tagList[tagIndex].tagPtr->Release();
								}
							}
							else
							{
								// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
								//CA(" else DataSprayerPtr->isFrameTagged");
								InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
								if(!iHier)
								{
									//CA(" !iHier >> Continue ");
									continue;
								}
								UID kidUID;				
								int32 numKids=iHier->GetChildCount();				
								IIDXMLElement* ptr = NULL;

								for(int j=0;j<numKids;j++)
								{
									//CA("Inside For Loop");
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);			
									TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
									if(NewList.size()<=0)//This can be a Tagged Frame
									{
										if(DataSprayerPtr->isFrameTagged(boxRef))
										{	
											//CA("isFrameTagged(selectUIDList.GetRef(i))");
											DataSprayerPtr->sprayForTaggedBox(boxRef);				
										}
										continue;
									}
									DataSprayerPtr->sprayForThisBox(boxRef, NewList);
									//------------
									for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
									{
										NewList[tagIndex].tagPtr->Release();
									}
								}
							}
						//	CA("Before Continue");
							continue;
						}
						
						bool16 flaG = kFalse;			
					//	CA("Before sprayForThisBox");
						DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
					
						if(flaG)
						{			
							InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) {
								break;
							}
							selectionManager->DeselectAll(nil); // deselect every active CSB
							//layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
							layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added

							TagList NewTagList;
							NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

							if(NewTagList.size()==0)//Ordinary box
							{
								return ;
							}
							//------------
							for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
							{
								NewTagList[tagIndex].tagPtr->Release();
							}
						}

						for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
						{
							tagList[tagIndex].tagPtr->Release();
						}
					}
					//CA("12");
					moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);
					//CA("13");				
					CmdUtils::EndCommandSequence(seq);
					
					UIDList newTempUIDList(ItemFrameUIDList);
					VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

					if(newAddedFrameUIDListAfterSpray.size() > 0)
					{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
						for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
						{					
							newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
							selectUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
						}
					}

					result = getMaxLimitsOfBoxes(/*ItemFrameUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
					if(result == kFalse)
						break;

					// Right now only for Vertical Flow
					PMReal LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
					PMReal BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
					PMReal TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
					PMReal RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

					PMReal MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
					PMReal MaxRightMarkSprayHZ = RightMarkAfterSpray;
					
					for(; it1 != ItemIDInfo->end(); it1++)
					{
						//CA("Inside Loop...... ");
						PMReal NewLeft = 0.0;
						PMReal NewTop = 0.0;
					
						//	if(isItemHorizontalFlow == kFalse)
						if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
						{
							NewLeft = LeftMarkAfterSpray;
							NewTop =  BottomMarkAfterSpray + 5.0 ; // Vertical Spacing 5 
							
						}
						else
						{
							NewLeft = RightMarkAfterSpray + 5.0; // Horizontal Spacing 5 
							NewTop =  TopMarkAfterSpray ; 

							/*PMString ASD("NewLeft : ");
							ASD.AppendNumber(NewLeft);
							ASD.Append("  NewTop : ");
							ASD.AppendNumber(NewTop);
							CA(ASD);*/

						}
						AlphabetArrayCount++;
						
						selectionManager->DeselectAll(nil); // deselect every active CSB
						//layoutSelectionSuite->Select(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
						layoutSelectionSuite->SelectPageItems(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added	

						//copy the selected items
						CopySelectedItems();
						
						UIDList SecondcopiedBoxUIDList;
						result = getSelectedBoxIds(SecondcopiedBoxUIDList);
						if(result == kFalse){
							//CA("getSelectedBoxIds result == kFalse ");
							break;
						}
						
						int32 SecondcopiedBoxUIDListSize = SecondcopiedBoxUIDList.Length();
						if(SecondcopiedBoxUIDListSize == 0){
							//CA("SecondcopiedBoxUIDListSize == 0");					
							continue;
						}

						PBPMPoint moveToPoints(NewLeft, NewTop);	
						moveBoxes(SecondcopiedBoxUIDList, moveToPoints);


						vectorCopiedBoxBoundsBforeSpray.clear();
						CopiedItemMaxBoxBoundsBforeSpray = kZeroRect;
						result = kFalse;
						result = this->getMaxLimitsOfBoxes(SecondcopiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);



						double SecondItemId = *it1;

						for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
						{
							tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
							if(tagList.size()<=0)//This can be a Tagged Frame
							{	
								//CA(" tagList.size()<=0 ");
								if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
								{	
									tagList.clear();
									tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
									if(tagList.size()==0)//Ordinary box
									{					
										continue ;
									}	
									
									for(int32 j=0; j <tagList.size(); j++)
									{
										if(tagList[j].whichTab == 4)
										{
											////CA("Item Tag Found");
											//ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
											//break; // break out from for loop
											if(tagList[j].imgFlag == 1)
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(SecondItemId));
												tagList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
											
												attributeValue.clear();
												attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
												tagList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4

											}
											else
											{
												PMString attributeValue;
												attributeValue.AppendNumber(PMReal(SecondItemId));
												tagList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
												tagList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

												PMString childTag("1");
												tagList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
												
												if(tagList[j].elementId == -803)
												{
													attributeValue.Clear();
													if(AlphabetArrayCount < 26)
														attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
													else
														attributeValue.Append("a");
													tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
												}
												else if(tagList[j].elementId == -827)
												{
													attributeValue.Clear();
													int32 numberKey = AlphabetArrayCount + 1;
													
													attributeValue.AppendNumber(numberKey);

													tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
												}
											}
											PMString attributeValue;								
											attributeValue.AppendNumber(-777);
											tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
												
										}
									}

									for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
									{
										tagList[tagIndex].tagPtr->Release();
									}
								}
								else
								{
									// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
									//CA(" else DataSprayerPtr->isFrameTagged");
									InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
									if(!iHier)
									{
										//CA(" !iHier >> Continue ");
										continue;
									}
									UID kidUID;				
									int32 numKids=iHier->GetChildCount();				
									IIDXMLElement* ptr = NULL;

									for(int j=0;j<numKids;j++)
									{
										//CA("Inside For Loop");
										bool16 Flag12 =  kFalse;
										kidUID=iHier->GetChildUID(j);
										UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
										TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
										if(NewList.size()<=0)//This can be a Tagged Frame
										{
											NewList.clear();
											NewList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
											if(NewList.size()==0)//Ordinary box
											{					
												continue ;
											}	
											
											for(int32 j=0; j <NewList.size(); j++)
											{
												if(NewList[j].whichTab == 4)
												{
													if(NewList[j].imgFlag == 1)
													{
														PMString attributeValue;
														attributeValue.AppendNumber(PMReal(SecondItemId));
														NewList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
													
														attributeValue.clear();
														attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
														NewList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4

													}
													else
													{
														PMString attributeValue;
														attributeValue.AppendNumber(PMReal(SecondItemId));
														NewList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
														NewList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

														PMString childTag("1");
														NewList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
														if(NewList[j].elementId == -803)
														{
															attributeValue.Clear();
															if(AlphabetArrayCount < 26)
																attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
															else
																attributeValue.Append("a");
															NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
														}
														else if(tagList[j].elementId == -827)
														{
															attributeValue.Clear();
															int32 numberKey = AlphabetArrayCount + 1;
															
															attributeValue.AppendNumber(numberKey);

															NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
														}
													}
													PMString attributeValue;								
													attributeValue.AppendNumber(-777);
													tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
												
												}
											}
										}
										else
										{
											for(int32 j=0; j <NewList.size(); j++)
											{
												if(NewList[j].whichTab == 4)
												{
													if(NewList[j].imgFlag == 1)
													{
														PMString attributeValue;
														attributeValue.AppendNumber(PMReal(SecondItemId));
														NewList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
													
														attributeValue.clear();
														attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
														NewList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4

													
													}
													else
													{
														PMString attributeValue;
														attributeValue.AppendNumber(PMReal(SecondItemId));
														NewList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
														NewList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

														PMString childTag("1");
														NewList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
														
														if(NewList[j].elementId == -803)
														{
															attributeValue.Clear();
															if(AlphabetArrayCount < 26)
																attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
															else
																attributeValue.Append("a");
															NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue));//Cs4
														}
														else if(tagList[j].elementId == -827)
														{
															attributeValue.Clear();
															int32 numberKey = AlphabetArrayCount + 1;
															
															attributeValue.AppendNumber(numberKey);

															NewList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
														}

													}
													PMString attributeValue;								
													attributeValue.AppendNumber(-777);
													NewList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
												
												}
											}
										}
										//------------
										for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
										{
											NewList[tagIndex].tagPtr->Release();
										}

										if(Flag12)
											break;
									}
								}
							}
							else
							{
								for(int32 j=0; j <tagList.size(); j++)
								{
									if(tagList[j].whichTab == 4)
									{
										if(tagList[j].imgFlag == 1)
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(SecondItemId));
											tagList[j].tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attributeValue)); //Cs4
										
											attributeValue.clear();
											attributeValue.AppendNumber(PMReal(currentobjectID/*pNodeDataList[sprayedProductIndex].getPubId()*/));
											tagList[j].tagPtr->SetAttributeValue(WideString("isAutoResize"),WideString(attributeValue)); //Cs4

										}
										else
										{
											PMString attributeValue;
											attributeValue.AppendNumber(PMReal(SecondItemId));
											tagList[j].tagPtr->SetAttributeValue(WideString("typeId"),WideString(attributeValue)); //Cs4
											tagList[j].tagPtr->SetAttributeValue(WideString("childId"),WideString(attributeValue));

											PMString childTag("1");
											tagList[j].tagPtr->SetAttributeValue(WideString("childTag"),WideString(childTag));
														
											if(tagList[j].elementId == -803)
											{
												attributeValue.Clear();
												if(AlphabetArrayCount < 26)
													attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
												else
													attributeValue.Append("a");
												tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4										}
											}
											else if(tagList[j].elementId == -827)
											{
												attributeValue.Clear();
												int32 numberKey = AlphabetArrayCount + 1;
												
												attributeValue.AppendNumber(numberKey);

												tagList[j].tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue)); //Cs4
											}
										}
										PMString attributeValue;								
										attributeValue.AppendNumber(-777);
										tagList[j].tagPtr->SetAttributeValue(WideString("colno"),WideString(attributeValue)); //Cs4
									
									}
								}
							}

							for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
							{
								tagList[tagIndex].tagPtr->Release();
							}
						}

						//CA("Before Spraying Second frame onwards");
						////// Spraying Second Frame onwords.
					
						if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/)
						{
				//			CA("1");					
							DataSprayerPtr->setFlow(kTrue);
						}
						else
						{
							//CA("2");
							DataSprayerPtr->setFlow(kFalse);
						}
					//	DataSprayerPtr->setFlow(isItemHorizontalFlow);
					//	DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());

						DataSprayerPtr->ClearNewImageFrameList();

						ICommandSequence *seq=CmdUtils::BeginCommandSequence();
						for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
						{ 
							PMString allInfo;	
							//CA("before calling itagReader->getTagsFromBox()");
							tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
							//CA("after calling itagReader->getTagsFromBox()");
							if(tagList.size()<=0)//This can be a Tagged Frame
							{	
								//CA(" tagList.size()<=0 ");
								if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
								{	
									//CA("isFrameTagged");
									bool16 flaG = kFalse;		

									tagList.clear();
									tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));

									if(tagList.size()==0)//Ordinary box
									{					
										continue ;
									}

									//CA("Frame Tags Found");
									InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
									InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
									if (!layoutSelectionSuite) {
										break;
									}
							
									selectionManager->DeselectAll(nil); // deselect every active CSB
									//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
									layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
								
								//	CA("Before sprayForTaggedBox");
									DataSprayerPtr->sprayForTaggedBox(SecondcopiedBoxUIDList.GetRef(i));	

									for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
									{
										tagList[tagIndex].tagPtr->Release();
									}
								}
								else
								{
									// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
									//CA(" else DataSprayerPtr->isFrameTagged");
									InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
									if(!iHier)
									{
										//CA(" !iHier >> Continue ");
										continue;
									}
									UID kidUID;				
									int32 numKids=iHier->GetChildCount();				
									IIDXMLElement* ptr = NULL;

									for(int j=0;j<numKids;j++)
									{
										//CA("Inside For Loop");
										kidUID=iHier->GetChildUID(j);
										UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
										TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
										if(NewList.size()<=0)//This can be a Tagged Frame
										{
											if(DataSprayerPtr->isFrameTagged(boxRef))
											{	
												//CA("isFrameTagged(selectUIDList.GetRef(i))");
												DataSprayerPtr->sprayForTaggedBox(boxRef);				
											}
											continue;
										}
										DataSprayerPtr->sprayForThisBox(boxRef, NewList);
										//------------
										for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
										{
											NewList[tagIndex].tagPtr->Release();
										}
									}
								}
							//	CA("Before Continue");
								continue;
							}
							
							bool16 flaG = kFalse;			
						//	CA("Before sprayForThisBox");
							DataSprayerPtr->sprayForThisBox(SecondcopiedBoxUIDList.GetRef(i), tagList);
						
							if(flaG)
							{			
								InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
								if (!layoutSelectionSuite) {
									break;
								}
								selectionManager->DeselectAll(nil); // deselect every active CSB
								//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
								layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added

								TagList NewTagList;
								NewTagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));

								if(NewTagList.size()==0)//Ordinary box
								{
									return ;
								}
								//------------
								for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
								{
									NewTagList[tagIndex].tagPtr->Release();
								}

							}

							for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
							{
								tagList[tagIndex].tagPtr->Release();
							}
						}
						//CA("14");					
						moveAutoResizeBoxAfterSpray(SecondcopiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
						//CA("15");
						CmdUtils::EndCommandSequence(seq);
						
						UIDList newTempUIDList(SecondcopiedBoxUIDList);
						VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

						if(newAddedFrameUIDListAfterSpray.size() > 0)
						{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
							for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
							{					
								newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
							}
						}


						ItemFramesStencilMaxBounds = kZeroRect;
						result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
						if(result == kFalse)
							break;
						
				//		if(isItemHorizontalFlow == kFalse)
						if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
						{
							if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
							{
								// For Vertical Flow.........
								//CA("Going out of Bottom Margin ");
								LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + 5.0;
								TopMarkAfterSpray = TopMark;
								BottomMarkAfterSpray = TopMark - 5.0;

								/*PMString ASD("LeftMarkAfterSpray : " );
								ASD.AppendNumber(LeftMarkAfterSpray);
								ASD.Append("   TopMarkAfterSpray : ");
								ASD.AppendNumber(TopMarkAfterSpray);
								CA(ASD);*/

								selectionManager->DeselectAll(nil); // deselect every active CSB
								//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented BY sachin sharma on 2/07/07
								layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
								if(ItemFramesStencilMaxBounds.Top() != TopMark)
								{
									deleteThisBoxUIDList(newTempUIDList);
									--it1;
									continue;
								}

								//PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
								//moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
								//CA("After Moving Boxes ");
								ItemFramesStencilMaxBounds = kZeroRect;
								result = getMaxLimitsOfBoxes(newTempUIDList/*SecondcopiedBoxUIDList*/, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
								if(result == kFalse)
									break;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);

							}
							else
							{
								// Right now only for Vertical Flow
								//for Vertical Flow
								if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - TopMark))
								{  // if there is no space for next frame below 
									//CA(" No NEXT frame on Bottom side");
									LeftMarkAfterSpray = (MaxRightMarkSprayHZ) + 5.0;
									BottomMarkAfterSpray = TopMark - 5.0;
									TopMarkAfterSpray = TopMark;
									RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								}else
								{	//CA("Verticale Flow");
									LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
									BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
									TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
									RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

									/*PMString ASD(" LeftMarkAfterSpray : ");
									ASD.AppendNumber(LeftMarkAfterSpray);
									ASD.Append("  BottomMarkAfterSpray: ");
									ASD.AppendNumber(BottomMarkAfterSpray);
									ASD.Append("TopMarkAfterSpray : ");
									ASD.AppendNumber(TopMarkAfterSpray);
									ASD.Append("RightMarkAfterSpray : ");
									ASD.AppendNumber(RightMarkAfterSpray);
									CA(ASD);*/
								}
								
								if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
									MaxRightMarkSprayHZ = RightMarkAfterSpray;
		
								selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
							}
						}
						else
						{
							if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
							{
								//CA("Going out of Right Margin overflow ");
								LeftMarkAfterSpray = LeftMark;
								TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
								RightMarkAfterSpray = LeftMark - 5.0;

							/*	PMString ASD("LeftMarkAfterSpray : " );
								ASD.AppendNumber(LeftMarkAfterSpray);
								ASD.Append("   TopMarkAfterSpray : ");
								ASD.AppendNumber(TopMarkAfterSpray);
								CA(ASD);*/

								selectionManager->DeselectAll(nil); // deselect every active CSB
								//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
								layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
								if(ItemFramesStencilMaxBounds.Left() != LeftMark)
								{
									deleteThisBoxUIDList(newTempUIDList);
									--it1;
									continue;
								}

								///*PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
								//moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
								//CA("After Moving Boxes ");
								ItemFramesStencilMaxBounds = kZeroRect;
								result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
								if(result == kFalse)
									break;

								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

								selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);

							}
							else
							{
								if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (RightMark - LeftMark))
								{  // if there is no space for next frame on right side 
									//CA("No Next Frame on Right Side ");
									LeftMarkAfterSpray = LeftMark;
									BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
									if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
										MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

									TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
									RightMarkAfterSpray = LeftMark - 5.0;

								}else
								{
									//CA("Horizontal Flow ");
									LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
									BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
									TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
									RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());						

									if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
										MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
								}

								selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
							}
						}
					}
				  //CCCCCCCCCCCCC
					
					deleteThisBoxUIDList(FirstcopiedBoxUIDList);

				}while(0);
			}
		}

		//CA("outside of big if");
			


		if(isSprayItemPerFrameFlag1)
			return;


		//CA("isSprayItemPerFrameFlag1 == kFalse");
		PublicationNode pNode;
		pNode.setPubId(currentobjectID);
		pNode.setParentId(TSMediatorClass::sectionID);
		//CA(pfName);
		//pNode.setPublicationName(pfName);
		
		if(SelectedRowNo == 3)
			pNode.setIsProduct(1);
		if(SelectedRowNo == 4)
			pNode.setIsProduct(0);

		pNode.setHitCount(hitCount);
		pNode.setIsONEsource(kFalse);
		pNode.setSectionID(TSMediatorClass::sectionID);
		pNode.setPBObjectID(TSMediatorClass::pbObjectID);
		pNode.setTypeId(TSMediatorClass::parentTypeID);
		/*PMString s("pNode.getSectionID()	:	");
		s.AppendNumber(pNode.getSectionID());
		CA(s);*/
		//CA("Before SetDataFromTSToSpray");
		
		if(hitCount == 2)
		{
			DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);
			
			if(SelectedRowNo == 3)
				TableSourceInfoValueObj.setAll(NULL,NULL,hybridTableInfo,kTrue,kTrue,kTrue,selectedTableTypeID,selectedTableID);
			if(SelectedRowNo == 4)
				TableSourceInfoValueObj.setAll(NULL,NULL,typeHybridTableValObj,kFalse,kTrue,kTrue,selectedTableTypeID,selectedTableID);
			
			ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);

		}
		if(hitCount == 8)
		{
			DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);

			if(SelectedRowNo == 3)
				TableSourceInfoValueObj.setAll(NULL,tableInfo,NULL,kTrue,kTrue,kTrue,selectedTableTypeID,selectedTableID);
			if(SelectedRowNo == 4)
				TableSourceInfoValueObj.setAll(typeTableValObj,NULL,NULL,kFalse,kTrue,kTrue,selectedTableTypeID,selectedTableID);

			ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
			
		}

		DataSprayerPtr->getAllIds(pNode.getPubId());
		
		vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
		PMRect CopiedItemMaxBoxBoundsBforeSpray;
		bool16 result = kFalse;
		
		result = getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
		
		ICommandSequence *seq = CmdUtils::BeginCommandSequence();
		for(int32 boxIndex = 0 ; boxIndex < selectUIDList.size() ; boxIndex++)
		{
			TagList tList = itagReader->getTagsFromBox(selectUIDList.GetRef(boxIndex));
			if(tList.size()<=0)//This can be a Tagged Frame
			{
				//CA("tList.size()<=0");
				if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(boxIndex)))
				{	
					//CA("going to call sprayForTaggedBox");
					DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(boxIndex));
				}
			 	continue;
			}
			//CA("Going to call sprayForThisBox");
//CA("*****DS 5 call***");
			DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(boxIndex),tList);
			
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}

		moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);

		CmdUtils::EndCommandSequence(seq);
		
		PMRect CopiedItemMaxBoxBounds;
		vectorBoxBounds vectorCopiedBoxBounds;
		result = kFalse;
		result = this->getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);

		UIDRef originalPageUIDRef, originalSpreadUIDRef;
		result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
		if(result == kFalse){ 
			//SingleItemSprayReturnFlag = kTrue;
			//CA("AP46_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
			ptrIAppFramework->LogError("AP7_TABLEsource::ItemItemGroupSpray::startSpraying:::!getCurrentPage");
			break;
		}

		PMRect PagemarginBoxBounds;
		result = getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
		if(result == kFalse)
		{
			result = getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
			if(result == kFalse)
			{
//							SingleItemSprayReturnFlag = kTrue;
				///CA("result == kFalse");
				break;
			}
		}
		// This functionality is added to resize page overflowing list frames after spray to bottom page margins.
		if((PagemarginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0))
		{
			this->AdjustMaxLimitsOfBoxes(selectUIDList,PagemarginBoxBounds);
		}

		

		PublicationNode pNode1;
		selectedTableTypeID.clear();
		selectedTableID.clear();
		
		DataSprayerPtr->FillPnodeStruct(pNode1);
		TableSourceInfoValueObj.setAll(NULL,NULL,NULL,kFalse,kFalse,kFalse,selectedTableTypeID,selectedTableID);
		ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);

	}while(kFalse);
}

bool16 ItemItemGroupSpray::checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow)
{
//CA("ProductSpray::doesExist");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));
		
		if(tList.size()==0 )
		{
			//CA("continue");
			continue;
		}

		for(int32 j=0; j <tList.size(); j++)
		{
			if(tList[j].whichTab == 4)
			{
				if(tList[j].isSprayItemPerFrame == 1)
				{
					isItemHorizontalFlow = kTrue;
					return kTrue;
				}
				else if(tList[j].isSprayItemPerFrame == 2)
				{
					isItemHorizontalFlow = kFalse;
					return kTrue;
				}
			}
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		
	}
	//CA("return kFalse");
	return kFalse;
}

bool16 ItemItemGroupSpray::getCurrentPage(UIDRef& pageUIDRef, UIDRef& spreadUIDRef)
{
	bool16 result = kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
		if (layoutData == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
			break;
		}

		IDataBase* database = ::GetDataBase(document);
		if(!database)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No database");		
			break;
		}
		
		UID pageUID = layoutData->GetPage();
		if(pageUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
			break;
		}
		
		int32 CurrentPageIndex=static_cast<int32> (pageUidList.size());

		if(CurrentPageIndex== 0 )  //CurrentPageIndex== PageCount)
		{
			//CA("CurrentPageIndex== 0  ");
			UIDRef pageRef(database, pageUID);

			pageUIDRef = pageRef;

		//Commented By Sachin sharma on 2/07/07
			/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
			if(spreadGeomPtr == nil)
				break;*/

			/*InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr,IID_IHIERARCHY);
			if(hierarchyPtr == nil)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No hierarchyPtr");			
				break;
			}*/
//+=============++++++++++++++++++++============================Added
			UIDRef spreadRef=layoutData->GetSpreadRef();
//			InterfacePtr<ISpread>iSpread(spreadUIDRef,UseDefaultIID());
//			if (iSpread == NULL)
//			{
//				//CA("iSpread == NULL");
//				return kFalse;			
//			}
/////////////+==========++++++++++++++======++++++++++===========
//			//UID spreadUID = hierarchyPtr->GetSpreadUID();  //Commented By Sachin sharma on 2/07/07
//			UID spreadUID=	iSpread->GetNthPageUID(CurrentPageIndex);//added
//			UIDRef spreadRef(database, spreadUID);
			spreadUIDRef = spreadRef;
			result = kTrue;		
			break;
		}
		else if(CurrentPageIndex!=PageCount)
		{
			//CA("CurrentPageIndex!=PageCount  ");
			if(pageUidList[CurrentPageIndex-1]!= pageUID)
			{
				//CA(" pageUidList[CurrentPageIndex-1]!= pageUID  ");
				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;

				//Commented by Sachin SHarma 0n 2/07/07
				/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
				if(spreadGeomPtr == nil)
					break;

				InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				if(hierarchyPtr == nil)
					break;*/
//+=======================================================ADDEd
				UIDRef spreadRef=layoutData->GetSpreadRef();
				InterfacePtr<ISpread>iSpread(spreadRef, UseDefaultIID());
				if (iSpread == NULL)
				{
					//CA("iSpread22222 == NULL");
					return kFalse;
				}
//++==================================================
				//UID spreadUID = hierarchyPtr->GetSpreadUID();//Commented By Sachin Sharma 0n 2/07/07]
				//UID spreadUID = iSpread->GetNthPageUID((CurrentPageIndex-1));//Added
				//UIDRef spreadRef(database, spreadUID);
				spreadUIDRef = spreadRef;
				result = kTrue;				
				break;		
			}
			else
			{
				//CA(" pageUidList[CurrentPageIndex-1]!= pageUID  Else PArt");
				
				//Commented By Sachin sharma on 2/07/07
				/*IGeometry* spreadItem = layoutData->GetSpread();
				if(spreadItem == nil)
					return kFalse;

				InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
				if (iSpread == nil)
					return kFalse;*/
//==========+++++++++++++++++++++++++++++++++++++++Added
				UIDRef spreadRef=layoutData->GetSpreadRef();
				InterfacePtr<ISpread>iSpread(spreadRef, UseDefaultIID());
				if (iSpread == NULL)
				{
					//CA("iSpread 33333== NULL");
					return kFalse;	
				}

//==========+++++++++++++++++++++++++++++++++++++++
				int numPages=iSpread->GetNumPages();
				int OldPageIndex= iSpread->GetPageIndex(pageUID);
				if(numPages >= OldPageIndex+1 )
				 pageUID= iSpread->GetNthPageUID(OldPageIndex+1);

				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;
				
				//Commented By Sachin sharma on 2/07/07
				/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
				if(spreadGeomPtr == nil)
					break;

				InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				if(hierarchyPtr == nil)
					break;*/

				//UID spreadUID = hierarchyPtr->GetSpreadUID();  //Commented By Sachin Sharma on 2/07/07
				//UID spreadUID=	iSpread->GetNthPageUID(OldPageIndex);//Added
				//UIDRef spreadRef(database, spreadUID);
				spreadUIDRef = spreadRef;
				result = kTrue;				
				break;		
			}
		}
		else if(CurrentPageIndex == PageCount)
		{
			//Commented By Sachin Sharma on 2/07/07
			/*IGeometry* spreadItem = layoutData->GetSpread();
			if(spreadItem == nil)
				return kFalse;*/
			UIDRef spreadRef=layoutData->GetSpreadRef(); //Added
			//InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
			InterfacePtr<ISpread> iSpread(spreadRef, UseDefaultIID());//Added
			if (iSpread == nil)
			{
				//CA("iSpread == nil");
				return kFalse;
			}

			int numPages = iSpread->GetNumPages();

//			if(numPages >= CurrentPageIndex )
//				pageUID= iSpread->GetNthPageUID(CurrentPageIndex);

			pageUID = iSpread->GetNthPageUID(numPages-1);		//Amit

			UIDRef pageRef(database, pageUID);
			pageUIDRef = pageRef;

			//Commented By Sachin sharma on 2/07/07
			/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
			if(spreadGeomPtr == nil)
				break;

			InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
			if(hierarchyPtr == nil)
				break;*/

			//UID spreadUID = hierarchyPtr->GetSpreadUID();  //Comented By Sachin Sharma on 2/07/07
			//UID spreadUID =iSpread->GetNthPageUID(CurrentPageIndex);//Added
			//UIDRef spreadRef(database, spreadUID);
			spreadUIDRef = spreadRef;
			result = kTrue;				
			break;		
		}


	}
	while(kFalse);
	return result;

}


bool16 ItemItemGroupSpray::getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return result;
		}

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
			break;

		IDocument* doc = layoutData->GetDocument();
		if (doc == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
			break;
		}
		InterfacePtr<IPageList> pageList(doc, UseDefaultIID());
		if (pageList == nil)
		{
			ASSERT_FAIL("pageList is invalid");
			break;
		}
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		PMRect pageBounds = pageGeometry->GetStrokeBoundingBox();

		PMReal leftMargin =0.0,topMargin =0.0,rightMargin = 0.0,bottomMargin =0.0;
		// ... and the page's margins.
		/*InterfacePtr<IMargins> pageMargins(pageGeometry, IID_IMARGINS);
		if (pageMargins == nil)
		{
			ASSERT_FAIL("pageMargins is invalid");
			break;
		}*/
		
		/*pageMargins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);*/

		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}
		PMRect marginBBox;
		InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
		// Note it's OK if the page does not have margins.
		if (margins) {
			margins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);
		}

		PageType pageType = pageList->GetPageType(pageUIDRef.GetUID()) ;

		PMPoint leftTop;
		PMPoint rightBottom;

		if(pageType == kLeftPage)
		{
			leftTop.X(pageBounds.Left()+rightMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - leftMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
	
		}
		else if(pageType == kRightPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
		}
		else if(pageType == kUnisexPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);

		}

		// Place the item into a frame the size of the page margins
		// with origin at the top left margin. Note that the frame
		// is automatically resized to fit the content if the 
		// content is a graphic. Convert the points into the
		// pasteboard co-ordinate space.
		/*CAI("leftMargin", leftMargin);
		CAI("topMargin", topMargin );
		CAI("rightMargin", rightMargin );
		CAI("bottomMargin", bottomMargin );*/
		/*PMPoint leftTop(pageBounds.Left()+leftMargin, pageBounds.Top()+topMargin);
		PMPoint rightBottom(pageBounds.Right() - rightMargin, pageBounds.Bottom() - bottomMargin);*/

		/*CAI("PageleftMargin", pageBounds.Left()+leftMargin);
		CAI("PagetopMargin", pageBounds.Top()+topMargin );
		CAI("PagerightMargin", pageBounds.Right() - rightMargin );
		CAI("PagebottomMargin", pageBounds.Bottom() - bottomMargin );*/
		
		///****Commented By Sachin sharma
		/*::InnerToPasteboard(pageGeometry,&leftTop);
		::InnerToPasteboard(pageGeometry,&rightBottom);*/

		//****ADded
		::TransformInnerPointToPasteboard(pageGeometry,&leftTop);
		::TransformInnerPointToPasteboard(pageGeometry,&rightBottom);

		marginBoxBounds.Left() = leftTop.X();
		marginBoxBounds.Top() = leftTop.Y();
		marginBoxBounds.Right() = rightBottom.X();
		marginBoxBounds.Bottom() = rightBottom.Y();

		result = kTrue;
	}
	while(kFalse);

	return result;
}

bool16 ItemItemGroupSpray::getPageBounds(const UIDRef& pageUIDRef, PMRect& pageBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		
		pageBounds = pageGeometry->GetStrokeBoundingBox();
		result = kTrue;
	}
	while(kFalse);

	return result;
}

bool16 ItemItemGroupSpray::CopySelectedItems()
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	// We'll use a do-while(0) to break out on bad pointers:
	do
	{
		// Acquire the interfaces we need to use IScrapSuite:
		InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
		if (scrapSuite == nil)
		{
			//CA("CopySelectedItems: scrapSuite invalid");
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::CopySelectedItems:: scrapSuite invalid");
			break;
		}
		InterfacePtr<IClipboardController> clipController(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (clipController == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
			break;
		}

		InterfacePtr<IControlView> controlView(Utils<ILayoutUIUtils>()->QueryFrontView()/*::QueryFrontView()*/);//Cs4
		if (controlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::CopySelectedItems:: controlView invalid");
			break;
		}


		// Copy and paste the selection:
		if(scrapSuite->CanCopy(clipController) != kTrue)
			break;
		if(scrapSuite->Copy(clipController) != kSuccess)
			break;
		if(scrapSuite->CanPaste(clipController) != kTrue)
			break;
		if(scrapSuite->Paste(clipController, controlView) != kSuccess)
			break;

		status = kSuccess;

	} while (false); // Only do once.

	bool16 errored = kFalse;
	if (status != kSuccess)
		errored = kTrue;

	return errored;
}


bool16 ItemItemGroupSpray::getSelectedBoxIds(UIDList& selectUIDList)
{	
	//CA("SubSectionSprayer::getSelectedBoxIds");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	
		//CA("ReturniSelectionManager");
		return kFalse;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		//CA("returntxtMisSuite");
		return kFalse; 
	}
	
	txtMisSuite->GetUidList(selectUIDList);
	const int32 listLength=selectUIDList.Length();

	/*PMString selectUIDListLength = "";
	selectUIDListLength.AppendNumber(listLength);
	CA("selectUIDListLength = " + selectUIDListLength);*/

	
	if(listLength==0){ //CA("listLength==0");
		return kFalse;
	}

	///////////////////////for passing database pointer////////////////
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
	if (layoutData == nil){// CA("layoutData == nil");
		return kFalse;
	}

	IDocument* document = layoutData->GetDocument();
	if (document == nil){// CA("document == nil");
		return kFalse;
	}
	IDataBase* database = ::GetDataBase(document);
	if(!database){ //CA("!database");
		return kFalse;
	}
	///////////////////////for passing database pointer////////////////

	/*for(int32 i=0; i<listLength;i++)
	{
		UIDRef boxUIDRef = _selectUIDList->GetRef(i);
		if(this->isBoxParent(boxUIDRef, database)==kFalse)
		{
			CA("Please select only parent boxes and not embedded boxes while spraying.");
			return kFalse;
			
		}
	}*/
	
	//selectUIDList = getParentSelectionsOnly(selectUIDList, database); //required for copying embedded boxes also.

	UIDList TempUIDList(database);

	//if(selectUIDList.Length() <=0 )
	//	return kTrue;

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getSelectedBoxIds::No DataSprayerPtr");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getSelectedBoxIds::No iTagReader");			
		return kFalse;
	}


	//for(int i=0; i<selectUIDList.Length(); i++)
	//{
	//	CA("For Loop ");
	//	InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
	//	if(!iHier)
	//	{
	//		CA(" !iHier ");
	//		continue;
	//	}
	//	UID kidUID;
	//	
	//	int32 numKids=iHier->GetChildCount();
	//	IIDXMLElement* ptr = NULL;
	//	//for(int j=0;j<numKids;j++)
	//	//{
	//	//	kidUID=iHier->GetChildUID(j);
	//	//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
	//	//	
	//	//	TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
	//	//	if(!DataSprayerPtr->doesExist(ptr, boxRef))
	//	//	{
	//	//		CA("!DataSprayerPtr->doesExist(ptr, boxRef)");
	//	//		selectUIDList.Append(kidUID);					
	//	//	}
	//	//	/*if(!this->doesExist(ptr, boxRef, selectUIDList))
	//	//	{
	//	//		selectUIDList.Append(kidUID);					
	//	//	}*/
	//	//}
	//}

	for(int32 i= selectUIDList.Length()-1; i>=0; i--)
	{
		//CA("Appending to TempUIDList");
		TempUIDList.Append(selectUIDList.GetRef(i).GetUID());
	}

	selectUIDList = TempUIDList;
	return kTrue;
	
	//return kFalse;
}

void ItemItemGroupSpray::moveBoxes(const UIDList& copiedBoxUIDList, const PBPMPoint& moveToPoints)
{
	do
	{
		PMRect curMaxBoxBounds;
		vectorBoxBounds vectorCurBoxBounds;
		bool8 result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, curMaxBoxBounds, vectorCurBoxBounds);
		if(result == kFalse)
			break;

		//PMString ASD1("moveToPoints.Y() : ");
		//ASD1.AppendNumber(moveToPoints.Y());
		//ASD1.Append(" curMaxBoxBounds.Top() : ");
		//ASD1.AppendNumber(curMaxBoxBounds.Top());
		//CA(ASD1);

		PMReal left = moveToPoints.X() - curMaxBoxBounds.Left();
		PMReal top = moveToPoints.Y() - curMaxBoxBounds.Top();

		/*PMString ASD2("top : ");
		ASD2.AppendNumber(top);
		CA(ASD2);*/
		const PBPMPoint moveByPoints(left, top);
		ErrorCode errorCode;
		for(int32 i = copiedBoxUIDList.Length() - 1; i >= 0; --i)
		{
			UIDRef boxUIDRef = copiedBoxUIDList.GetRef(i);
			//Commented By Sachin sharma on 2/07/07
			/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
			if(!transform)
			{
				CA("ITransform nil");
				break;
			}*/
			//MovePageItemRelative(transform, moveByPoints);
			//=============
			UIDList moveUIDList(boxUIDRef);
			Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
			PBPMPoint referencePoint(PMPoint(0,0));
			errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y())/*(0, top)*/);
			//+=============
		}

	}
	while(kFalse);
}


int ItemItemGroupSpray::deleteThisBoxUIDList(UIDList boxUIDList)
{

	///	Commented By Amit on 1/2/2008 Because this code use to delete the frames only not tags of the frames
/*	for(int32 i=0; i< boxUIDList.Length(); i++)
	{
		InterfacePtr<IScrapItem> scrap(boxUIDList.GetRef(i), UseDefaultIID());
		if(scrap==nil)
			continue;
		InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
		if(!command)
			continue;
		command->SetItemList(UIDList(boxUIDList.GetRef(i)));
		if(CmdUtils::ProcessCommand(command)!=kSuccess)
			continue;
	}
*/	
///	Following code use to delete the frames as well as tags of the frames
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return 0;
	}
	InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::deleteThisBoxUIDList::!itagReader");
		return 0;
	}
 
	TagList tList;
	for(int i=0; i<boxUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(boxUIDList.GetRef(i));
		if(tList.size() > 0)
		{
			InterfacePtr<IPMUnknown> unknown(boxUIDList.GetRef(i), IID_IUNKNOWN);
			if(!unknown)
			{
				CA("No unknown prt available");
				break;
			}
			
			IIDXMLElement * xmlElementPtr = tList[0].tagPtr;
		
			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
			if (textFrameUID == kInvalidUID)
			{
				XMLReference xmlRef = xmlElementPtr->GetXMLReference();
				Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
			}
			else
			{
				XMLReference xmlRef = xmlElementPtr->GetParent();
				Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
			}
		}
		else
		{
			InterfacePtr<IScrapItem> scrap(boxUIDList.GetRef(i), UseDefaultIID());
			if(scrap==nil)
				continue;
			InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
			if(!command)
				continue;
			command->SetItemList(UIDList(boxUIDList.GetRef(i)));
			if(CmdUtils::ProcessCommand(command)!=kSuccess)
				continue;
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	//------------
	/*for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}*/
	return 1;
}


ErrorCode ItemItemGroupSpray::AdjustMaxLimitsOfBoxes(const UIDList& boxList, PMRect PagemaxBounds)
{
	ErrorCode result1 = kFailure;	
	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;	

	minTop = PagemaxBounds.Top();
	minLeft = PagemaxBounds.Left();
	maxBottom = PagemaxBounds.Bottom();
	maxRight = PagemaxBounds.Right();

	/*PMString ASD("PagemaxBounds.Top() : ");
	ASD.AppendNumber((PagemaxBounds.Top()));
	ASD.Append("  PagemaxBounds.Left() : ");
	ASD.AppendNumber((PagemaxBounds.Left()));
	ASD.Append("  PagemaxBounds.Bottom() : ");
	ASD.AppendNumber((PagemaxBounds.Bottom()));
	ASD.Append("  PagemaxBounds.Right() : ");
	ASD.AppendNumber((PagemaxBounds.Right()));
	CA(ASD);*/
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result1;
	}
	for(int i=0;i<boxList.Length();i++)
	{
		UIDRef boxUIDRef = boxList.GetRef(i);

		InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
		if(!geometryPtr)
		{
			continue;
		}

		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
				
		PMReal top = boxBounds.Top();
		PMReal left = boxBounds.Left();
		PMReal bottom = boxBounds.Bottom();
		PMReal right = boxBounds.Right();
		
		/*PMString ASD("boxBounds.Top() : ");
		ASD.AppendNumber((boxBounds.Top()));
		ASD.Append("  boxBounds.Left() : ");
		ASD.AppendNumber((boxBounds.Left()));
		ASD.Append("  boxBounds.Bottom() : ");
		ASD.AppendNumber((boxBounds.Bottom()));
		ASD.Append("  boxBounds.Right() : ");
		ASD.AppendNumber((boxBounds.Right()));
		CA(ASD);*/

		if(bottom > maxBottom){ 
			//CA("Box Bounds are greater Than Page Bottom Bound");

			PMReal newBottom  = bottom - maxBottom;
			PMRect NewBoxBounds;
			NewBoxBounds.Top(top);
			NewBoxBounds.Left(left);
			NewBoxBounds.Bottom(maxBottom );
			NewBoxBounds.Right(right);

			//PMString ASD("NewBoxBounds.Top() : ");
			//ASD.AppendNumber((NewBoxBounds.Top()));
			//ASD.Append("  NewBoxBounds.Left() : ");
			//ASD.AppendNumber((NewBoxBounds.Left()));
			//ASD.Append("  NewBoxBounds.Bottom() : ");
			//ASD.AppendNumber((NewBoxBounds.Bottom()));
			//ASD.Append("  NewBoxBounds.Right() : ");
			//ASD.AppendNumber((NewBoxBounds.Right()));
			//CA(ASD);

			//result1 = geometryPtr->SetStrokeBoundingBox( InnerToPasteboardMatrix(geometryPtr), NewBoxBounds, IGeometry::kResizeItemAndChildren);  //Commented By Sachin sharma on 2/07/07
			result1 = geometryPtr->SetStrokeBoundingBox( InnerToPasteboardMatrix(geometryPtr), NewBoxBounds);
			
		}
		
	}
		
	return result1;
}


bool16 ItemItemGroupSpray::SprayTemplatePerTablefromTableSource(UIDList selectUIDList, vector<double> selectedTableID, vector<double> selectedTableTypeID, int32 hitCount )
{
	bool16 resultFlag = kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return resultFlag;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::Pointre to DataSprayerPtr not found");
		return resultFlag;
	}
		
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::No itagReader");			
		return resultFlag;
	}	

	InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
	if(ptrTableSourceHelper == nil)
	{
		//CA("TABLEsource not found ");
		return resultFlag;
	}
	TableSourceInfoValue TableSourceInfoValueObj;
	int32 listLength = selectUIDList.Length();
	TagList tagList;
		
	int selTablCt=0;
	if( selectedTableID.size() > 1)
	{
		vector<double> currSelectedTableID;
		vector<double> currSelectedTableTypeID;

		currSelectedTableID.push_back(selectedTableID.at(selTablCt));
		currSelectedTableTypeID.push_back(selectedTableTypeID.at(selTablCt));

		PublicationNode pNode;
		pNode.setPubId(currentobjectID);
		pNode.setParentId(TSMediatorClass::sectionID);
			
		if(SelectedRowNo == 3)
			pNode.setIsProduct(1);
		if(SelectedRowNo == 4)
			pNode.setIsProduct(0);

		pNode.setHitCount(hitCount);
		pNode.setIsONEsource(kFalse);
		pNode.setSectionID(TSMediatorClass::sectionID);
		pNode.setPBObjectID(TSMediatorClass::pbObjectID);
		pNode.setTypeId(TSMediatorClass::parentTypeID);

		if(hitCount == 2)
		{
			DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);
			
			if(SelectedRowNo == 3)
				TableSourceInfoValueObj.setAll(NULL,NULL,hybridTableInfo,kTrue,kTrue,kTrue,currSelectedTableTypeID,currSelectedTableID);
			if(SelectedRowNo == 4)
				TableSourceInfoValueObj.setAll(NULL,NULL,typeHybridTableValObj,kFalse,kTrue,kTrue,currSelectedTableTypeID,currSelectedTableID);
			
			ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);

		}
		if(hitCount == 8)
		{
			DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);

			if(SelectedRowNo == 3)
				TableSourceInfoValueObj.setAll(NULL,tableInfo,NULL,kTrue,kTrue,kTrue,selectedTableTypeID,currSelectedTableID);
			if(SelectedRowNo == 4)
				TableSourceInfoValueObj.setAll(typeTableValObj,NULL,NULL,kFalse,kTrue,kTrue,selectedTableTypeID,currSelectedTableID);

			ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
			
		}

		DataSprayerPtr->getAllIds(pNode.getPubId());

		//CA("Inside isSprayItemPerFrameFlag case");
		vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
		PMRect CopiedItemMaxBoxBoundsBforeSpray;
		UIDList ItemFrameUIDList(selectUIDList.GetDataBase());
		//CA("1");
		bool16 result = kFalse;
		result = this->getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
				
		
		ItemFrameUIDList = selectUIDList;
		int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
		if(ItemFrameUIDListSize == 0)
		{
			//CA("ItemFrameUIDListSize == 0");
			//SingleItemSprayReturnFlag = kTrue;
			ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::SingleItemSprayReturnFlag = kTrue;");
		}
			
		int AlphabetArrayCount =0;

		if(SelectedRowNo == 3 || SelectedRowNo == 4)
		{
			do
			{
							
				UIDRef originalPageUIDRef, originalSpreadUIDRef;
				result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
				if(result == kFalse)
				{ 
					//SingleItemSprayReturnFlag = kTrue;
					//CA("AP46_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
					ptrIAppFramework->LogError("AP7_TABLEsource::ItemItemGroupSpray::startSpraying:::!getCurrentPage");
					return resultFlag;
				}

				PMRect PagemarginBoxBounds;
				result = getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
				if(result == kFalse)
				{
					result = getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
					if(result == kFalse)
					{
//							SingleItemSprayReturnFlag = kTrue;
						///CA("result == kFalse");
						return resultFlag;
					}
				}

				PMRect ItemFramesStencilMaxBounds = kZeroRect;
				vectorBoxBounds ItemFramesBoxBoundVector;

				result = getMaxLimitsOfBoxes(ItemFrameUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
				if(result == kFalse){
//						SingleItemSprayReturnFlag = kTrue;
					//CA("getMaxLimitsOfBoxes result == kFalse");
					return resultFlag;
				}
					
				PMReal LeftMark = (ItemFramesStencilMaxBounds.Left());
				PMReal BottomMark = (ItemFramesStencilMaxBounds.Bottom());
				PMReal TopMark = (ItemFramesStencilMaxBounds.Top());
				PMReal RightMark = (ItemFramesStencilMaxBounds.Right());

				if(LeftMark > PagemarginBoxBounds.Right() )
				{
					InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
					if (layoutData == nil)
						return resultFlag;

					IDocument* document = layoutData->GetDocument();
					if (document == NULL)
						return resultFlag;

					IDataBase* database = ::GetDataBase(document);
					if(!database)
						return resultFlag;
					//CA("Got wrong Page UID");
					/*IGeometry* spreadItem = layoutData->GetSpread();
					if(spreadItem == nil)
						break;*/
					UIDRef ref = layoutData->GetSpreadRef();

					InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
					if (iSpread == nil)
						return resultFlag;

					int numPages=iSpread->GetNumPages();
						
					UID pageUID= iSpread->GetNthPageUID(numPages-1);

					UIDRef pageRef(database, pageUID);
					getPageBounds (pageRef, PagemarginBoxBounds);
				}

				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
//						SingleItemSprayReturnFlag = kTrue;
					//CA("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");										
					ptrIAppFramework->LogDebug("AP7_TABLEsource::ItemItemGroupSpray::startSpraying::!layoutSelectionSuite");										
					return resultFlag;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB

				int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
				/*PMString ASD("Length of ItemFrameUIDList : ");
				ASD.AppendNumber(ItemFrameUIDListSize);
				CA(ASD);*/

				//layoutSelectionSuite->Select(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	  //Commented By Sachin sharma on 2/07/07
				layoutSelectionSuite->SelectPageItems(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

				//copy the selected items
				CopySelectedItems();

				//now get the copied item list
				UIDList FirstcopiedBoxUIDList;
				result = getSelectedBoxIds(FirstcopiedBoxUIDList);
				if(result == kFalse)
					return resultFlag;

				// For First Item of Product
				//VectorLongIntValue::iterator it1;
				//it1 = ItemIDInfo->begin();
				//int32 FirstItemId = *it1;
				//it1++;
				AlphabetArrayCount =0;
				if(isItemHorizontalFlow1)
					DataSprayerPtr->setFlow(kTrue);
				else
					DataSprayerPtr->setFlow(kFalse);
		
				DataSprayerPtr->ClearNewImageFrameList();

				//CA("Before Sprayting Original Frame");
				////// Spraying original frames first:
				ICommandSequence *seq=CmdUtils::BeginCommandSequence();
				for(int i=0; i<listLength; i++)
				{ 
					PMString allInfo;	
					//CA("before calling itagReader->getTagsFromBox()");
					tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
					//CA("after calling itagReader->getTagsFromBox()");
					if(tagList.size()<=0)//This can be a Tagged Frame
					{	
						//CA(" tagList.size()<=0 ");
						if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
						{	
							//CA("isFrameTagged");
							bool16 flaG = kFalse;		

							tagList.clear();
							tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));

							if(tagList.size()==0)//Ordinary box
							{					
								continue ;
							}

							//CA("Frame Tags Found");
							InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) {
								break;
							}
						
							selectionManager->DeselectAll(nil); // deselect every active CSB
							//layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
							layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
							
							//CA("Before sprayForTaggedBox");
							DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));	

							for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
							{
								tagList[tagIndex].tagPtr->Release();
							}
						}
						else
						{
							// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
							//CA(" else DataSprayerPtr->isFrameTagged");
							InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
							if(!iHier)
							{
								//CA(" !iHier >> Continue ");
								continue;
							}
							UID kidUID;				
							int32 numKids=iHier->GetChildCount();				
							IIDXMLElement* ptr = NULL;

							for(int j=0;j<numKids;j++)
							{
								//CA("Inside For Loop");
								kidUID=iHier->GetChildUID(j);
								UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);			
								TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
								if(NewList.size()<=0)//This can be a Tagged Frame
								{
									if(DataSprayerPtr->isFrameTagged(boxRef))
									{	
										//CA("isFrameTagged(selectUIDList.GetRef(i))");
										DataSprayerPtr->sprayForTaggedBox(boxRef);				
									}
									continue;
								}
								DataSprayerPtr->sprayForThisBox(boxRef, NewList);
								//------------
								for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
								{
									NewList[tagIndex].tagPtr->Release();
								}
							}
						}
					//	CA("Before Continue");
						continue;
					}
						
					bool16 flaG = kFalse;			
				//	CA("Before sprayForThisBox");
					DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
					
					if(flaG)
					{			
						InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
						if (!layoutSelectionSuite) {
							break;
						}
						selectionManager->DeselectAll(nil); // deselect every active CSB
						//layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
						layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added

						TagList NewTagList;
						NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));

						if(NewTagList.size()==0)//Ordinary box
						{
							return resultFlag;
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
						{
							NewTagList[tagIndex].tagPtr->Release();
						}
					}

					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						tagList[tagIndex].tagPtr->Release();
					}
				}
				//CA("12");
				moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);
				//CA("13");				
				CmdUtils::EndCommandSequence(seq);
					
				UIDList newTempUIDList(ItemFrameUIDList);
				VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

				if(newAddedFrameUIDListAfterSpray.size() > 0)
				{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
					for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
					{					
						newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
						selectUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
					}
				}

				result = getMaxLimitsOfBoxes(/*ItemFrameUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
				if(result == kFalse)
					return resultFlag;

				// Right now only for Vertical Flow
				PMReal LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
				PMReal BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
				PMReal TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
				PMReal RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

				PMReal MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
				PMReal MaxRightMarkSprayHZ = RightMarkAfterSpray;
				
				selTablCt++;
				for(; selTablCt < selectedTableID.size(); selTablCt++)
				{
					currSelectedTableID.clear();
					currSelectedTableTypeID.clear();
					currSelectedTableID.push_back(selectedTableID.at(selTablCt));
					currSelectedTableTypeID.push_back(selectedTableTypeID.at(selTablCt));
						
					if(hitCount == 2)
					{
						DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);
			
						if(SelectedRowNo == 3)
							TableSourceInfoValueObj.setAll(NULL,NULL,hybridTableInfo,kTrue,kTrue,kTrue,currSelectedTableTypeID,currSelectedTableID);
						if(SelectedRowNo == 4)
							TableSourceInfoValueObj.setAll(NULL,NULL,typeHybridTableValObj,kFalse,kTrue,kTrue,currSelectedTableTypeID,currSelectedTableID);
			
						ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);

					}
					if(hitCount == 8)
					{
						DataSprayerPtr->FillPnodeStruct(pNode,TSMediatorClass::sectionID);

						if(SelectedRowNo == 3)
							TableSourceInfoValueObj.setAll(NULL,tableInfo,NULL,kTrue,kTrue,kTrue,selectedTableTypeID,currSelectedTableID);
						if(SelectedRowNo == 4)
							TableSourceInfoValueObj.setAll(typeTableValObj,NULL,NULL,kFalse,kTrue,kTrue,selectedTableTypeID,currSelectedTableID);

						ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
			
					}

						
					//CA("Inside Loop...... ");
					PMReal NewLeft = 0.0;
					PMReal NewTop = 0.0;
					
					//	if(isItemHorizontalFlow == kFalse)
					if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
					{
						NewLeft = LeftMarkAfterSpray;
						NewTop =  BottomMarkAfterSpray + 5.0 ; // Vertical Spacing 5 
							
					}
					else
					{
						NewLeft = RightMarkAfterSpray + 5.0; // Horizontal Spacing 5 
						NewTop =  TopMarkAfterSpray ; 

					}
					AlphabetArrayCount++;
						
					selectionManager->DeselectAll(nil); // deselect every active CSB
					layoutSelectionSuite->SelectPageItems(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added	

					//copy the selected items
					CopySelectedItems();
						
					UIDList SecondcopiedBoxUIDList;
					result = getSelectedBoxIds(SecondcopiedBoxUIDList);
					if(result == kFalse)
					{
						//CA("getSelectedBoxIds result == kFalse ");
						break;
					}
						
					int32 SecondcopiedBoxUIDListSize = SecondcopiedBoxUIDList.Length();
					if(SecondcopiedBoxUIDListSize == 0){
						//CA("SecondcopiedBoxUIDListSize == 0");					
						continue;
					}

					PBPMPoint moveToPoints(NewLeft, NewTop);	
					moveBoxes(SecondcopiedBoxUIDList, moveToPoints);


					vectorCopiedBoxBoundsBforeSpray.clear();
					CopiedItemMaxBoxBoundsBforeSpray = kZeroRect;
					result = kFalse;
					result = this->getMaxLimitsOfBoxes(SecondcopiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);

					//CA("Before Spraying Second frame onwards");
					////// Spraying Second Frame onwords.
					
					if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/)
					{
			//			CA("1");					
						DataSprayerPtr->setFlow(kTrue);
					}
					else
					{
						//CA("2");
						DataSprayerPtr->setFlow(kFalse);
					}
				//	DataSprayerPtr->setFlow(isItemHorizontalFlow);
				//	DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());

					DataSprayerPtr->ClearNewImageFrameList();

					ICommandSequence *seq=CmdUtils::BeginCommandSequence();
					for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
					{ 
						PMString allInfo;	
						//CA("before calling itagReader->getTagsFromBox()");
						tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
						//CA("after calling itagReader->getTagsFromBox()");
						if(tagList.size()<=0)//This can be a Tagged Frame
						{	
							//CA(" tagList.size()<=0 ");
							if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
							{	
								//CA("isFrameTagged");
								bool16 flaG = kFalse;		

								tagList.clear();
								tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));

								if(tagList.size()==0)//Ordinary box
								{					
									continue ;
								}

								//CA("Frame Tags Found");
								InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
								InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
								if (!layoutSelectionSuite) {
									break;
								}
							
								selectionManager->DeselectAll(nil); // deselect every active CSB
								//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
								layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
								
							//	CA("Before sprayForTaggedBox");
								DataSprayerPtr->sprayForTaggedBox(SecondcopiedBoxUIDList.GetRef(i));	

								for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
								{
									tagList[tagIndex].tagPtr->Release();
								}
							}
							else
							{
								// This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
								//CA(" else DataSprayerPtr->isFrameTagged");
								InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
								if(!iHier)
								{
									//CA(" !iHier >> Continue ");
									continue;
								}
								UID kidUID;				
								int32 numKids=iHier->GetChildCount();				
								IIDXMLElement* ptr = NULL;

								for(int j=0;j<numKids;j++)
								{
									//CA("Inside For Loop");
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
									TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
									if(NewList.size()<=0)//This can be a Tagged Frame
									{
										if(DataSprayerPtr->isFrameTagged(boxRef))
										{	
											//CA("isFrameTagged(selectUIDList.GetRef(i))");
											DataSprayerPtr->sprayForTaggedBox(boxRef);				
										}
										continue;
									}
									DataSprayerPtr->sprayForThisBox(boxRef, NewList);
									//------------
									for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
									{
										NewList[tagIndex].tagPtr->Release();
									}
								}
							}
						//	CA("Before Continue");
							continue;
						}
							
						bool16 flaG = kFalse;			
					//	CA("Before sprayForThisBox");
						DataSprayerPtr->sprayForThisBox(SecondcopiedBoxUIDList.GetRef(i), tagList);
						
						if(flaG)
						{			
							InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
							InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
							if (!layoutSelectionSuite) {
								break;
							}
							selectionManager->DeselectAll(nil); // deselect every active CSB
							//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
							layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added

							TagList NewTagList;
							NewTagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));

							if(NewTagList.size()==0)//Ordinary box
							{
								return resultFlag;
							}
							//------------
							for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
							{
								NewTagList[tagIndex].tagPtr->Release();
							}

						}

						for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
						{
							tagList[tagIndex].tagPtr->Release();
						}
					}
					//CA("14");					
					moveAutoResizeBoxAfterSpray(SecondcopiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
					//CA("15");
					CmdUtils::EndCommandSequence(seq);
						
					UIDList newTempUIDList(SecondcopiedBoxUIDList);
					VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

					if(newAddedFrameUIDListAfterSpray.size() > 0)
					{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
						for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
						{					
							newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
						}
					}


					ItemFramesStencilMaxBounds = kZeroRect;
					result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
					if(result == kFalse)
						break;
						
			//		if(isItemHorizontalFlow == kFalse)
					if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
					{
						if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
						{
							// For Vertical Flow.........
							//CA("Going out of Bottom Margin ");
							LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + 5.0;
							TopMarkAfterSpray = TopMark;
							BottomMarkAfterSpray = TopMark - 5.0;

							/*PMString ASD("LeftMarkAfterSpray : " );
							ASD.AppendNumber(LeftMarkAfterSpray);
							ASD.Append("   TopMarkAfterSpray : ");
							ASD.AppendNumber(TopMarkAfterSpray);
							CA(ASD);*/

							selectionManager->DeselectAll(nil); // deselect every active CSB
							//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented BY sachin sharma on 2/07/07
							layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
							if(ItemFramesStencilMaxBounds.Top() != TopMark)
							{
								deleteThisBoxUIDList(newTempUIDList);
								--selTablCt;
								continue;
							}

							//PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
							//moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
							//CA("After Moving Boxes ");
							ItemFramesStencilMaxBounds = kZeroRect;
							result = getMaxLimitsOfBoxes(newTempUIDList/*SecondcopiedBoxUIDList*/, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
							if(result == kFalse)
								break;

							LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
							BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
							TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
							RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

							selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);

						}
						else
						{
							// Right now only for Vertical Flow
							//for Vertical Flow
							if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - TopMark))
							{  // if there is no space for next frame below 
								//CA(" No NEXT frame on Bottom side");
								LeftMarkAfterSpray = (MaxRightMarkSprayHZ) + 5.0;
								BottomMarkAfterSpray = TopMark - 5.0;
								TopMarkAfterSpray = TopMark;
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

							}else
							{	//CA("Verticale Flow");
								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

								/*PMString ASD(" LeftMarkAfterSpray : ");
								ASD.AppendNumber(LeftMarkAfterSpray);
								ASD.Append("  BottomMarkAfterSpray: ");
								ASD.AppendNumber(BottomMarkAfterSpray);
								ASD.Append("TopMarkAfterSpray : ");
								ASD.AppendNumber(TopMarkAfterSpray);
								ASD.Append("RightMarkAfterSpray : ");
								ASD.AppendNumber(RightMarkAfterSpray);
								CA(ASD);*/
							}
								
							if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
								MaxRightMarkSprayHZ = RightMarkAfterSpray;
		
							selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
						}
					}
					else
					{
						if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
						{
							//CA("Going out of Right Margin overflow ");
							LeftMarkAfterSpray = LeftMark;
							TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
							RightMarkAfterSpray = LeftMark - 5.0;

						/*	PMString ASD("LeftMarkAfterSpray : " );
							ASD.AppendNumber(LeftMarkAfterSpray);
							ASD.Append("   TopMarkAfterSpray : ");
							ASD.AppendNumber(TopMarkAfterSpray);
							CA(ASD);*/

							selectionManager->DeselectAll(nil); // deselect every active CSB
							//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
							layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
							if(ItemFramesStencilMaxBounds.Left() != LeftMark)
							{
								deleteThisBoxUIDList(newTempUIDList);
								--selTablCt;
								continue;
							}

							///*PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
							//moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
							//CA("After Moving Boxes ");
							ItemFramesStencilMaxBounds = kZeroRect;
							result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
							if(result == kFalse)
								break;

							LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
							BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
							TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
							RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());

							if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
								MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

							selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);

						}
						else
						{
							if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (RightMark - LeftMark))
							{  // if there is no space for next frame on right side 
								//CA("No Next Frame on Right Side ");
								LeftMarkAfterSpray = LeftMark;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;

								TopMarkAfterSpray = MaxBottomMarkSprayHZ + 5.0;
								RightMarkAfterSpray = LeftMark - 5.0;

							}else
							{
								//CA("Horizontal Flow ");
								LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
								TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
								RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());						

								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
							}

							selectUIDList.Append(newTempUIDList);
						}


					}						

				}
				  					
				deleteThisBoxUIDList(FirstcopiedBoxUIDList);

				PublicationNode pNode1;
				selectedTableTypeID.clear();
				selectedTableID.clear();
		
				DataSprayerPtr->FillPnodeStruct(pNode1);
				TableSourceInfoValueObj.setAll(NULL,NULL,NULL,kFalse,kFalse,kFalse,selectedTableTypeID,selectedTableID);
				ptrTableSourceHelper->setTableSourceInfoValueObj(TableSourceInfoValueObj);
				resultFlag = kTrue;

			}while(0);
		}
	}
	return resultFlag;
}