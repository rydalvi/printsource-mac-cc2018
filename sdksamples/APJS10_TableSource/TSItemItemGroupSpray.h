#ifndef __TSITEMITEMGROUPSPRAY_H__
#define __TSITEMITEMGROUPSPRAY_H__

#include "IAppFramework.h"
#include "ITagReader.h"
#include "IDataSprayer.h"

#include "vector"
#include "UIDList.h"
#include "TransformUtils.h"
#include "ITreeViewController.h"
#include "UIDNodeID.h"
#include "NodeID.h"
#include "IGeometry.h"
#include "ITransformFacade.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITableUtility.h"

#include "PublicationNode.h"
#include "TableSourceInfoValue.h"
#include "TSMediatorClass.h"
#include "TSID.h"
#include "TSTreeModel.h"
#include "TSTableSourceHelper.h"
#include "TSTreeData.h"
#include "TSDataNode.h"
#include "TSTreeDataCache.h"
//MessageServer
#include "CAlert.h"

using namespace std;

class BoxBounds
{
public:
	PMReal Top;
	PMReal Bottom;
	PMReal Left;
	PMReal Right;
	UIDRef BoxUIDRef;
	PMReal compressShift;
	PMReal enlargeShift;
	bool16 shiftUP;
	bool16 isUnderImage;
};

typedef vector<BoxBounds> vectorBoxBounds;

class ItemItemGroupSpray
{
public:
	void startSpraying();
	bool16 getMaxLimitsOfBoxes(const UIDList&, PMRect&, vectorBoxBounds &boxboundlist);
	void moveAutoResizeBoxAfterSpray(const UIDList& copiedBoxUIDList, vectorBoxBounds vectorCurBoxBoundsBeforeSpray );

	bool16 checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow);
	bool16 getCurrentPage(UIDRef&, UIDRef&);
	bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds);
	bool16 getPageBounds(const UIDRef& pageUIDRef, PMRect& pageBounds);
	bool16 CopySelectedItems();
	bool16 getSelectedBoxIds(UIDList& selectUIDList);
	void moveBoxes(const UIDList& copiedBoxUIDList, const PBPMPoint& moveToPoints);
	int deleteThisBoxUIDList(UIDList boxUIDList);
	ErrorCode AdjustMaxLimitsOfBoxes(const UIDList& boxList, PMRect PagemaxBounds);
	bool16 SprayTemplatePerTablefromTableSource(UIDList selectUIDList, vector<double> selectedTableID, vector<double> selectedTableTypeID, int32 hitCount );

};


#endif