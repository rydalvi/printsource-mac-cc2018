#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "TSID.h"
#include "ILoginEvent.h"
#include "TSLoginEventsHandler.h"
#include "TSSelectionObserver.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "TSActionComponent.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#include "LNGID.h"
#include "TSTableSourceHelper.h"

extern VectorScreenTableInfoPtr tableInfo ;
extern VectorAdvanceTableScreenValuePtr hybridTableInfo;
extern VectorScreenTableInfoPtr typeTableValObj;
extern VectorAdvanceTableScreenValuePtr typeHybridTableValObj;

#define CA(X) CAlert::InformationAlert(X)

CREATE_PMINTERFACE(TSLoginEventsHandler,kTSLoginEventsHandlerImpl)

TSLoginEventsHandler::TSLoginEventsHandler(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
}

TSLoginEventsHandler::~TSLoginEventsHandler()
{
}

bool16 TSLoginEventsHandler::userLoggedIn(void)
{
	//CA("TSLoginEventsHandler::userLoggedIn");
	do
	{
		TSSelectionObserver selObserver(this);
		selObserver.loadPaletteData();
	}while(kFalse);			
	return kTrue;	
}

bool16 TSLoginEventsHandler::userLoggedOut(void)
{	
	//CA("TSLoginEventsHandler::userLoggedOut");
    
	/*if(tableInfo)
	 {
		tableInfo->clear();
		delete tableInfo;
	 }
	 if(hybridTableInfo)
	 {
		hybridTableInfo->clear();
		delete hybridTableInfo;
	 }
	if(typeTableValObj)
	{
		typeTableValObj->clear();
		delete typeTableValObj;
	}
	if(typeHybridTableValObj)
	{
		typeHybridTableValObj->clear();
		delete typeHybridTableValObj;
	}*/

	TSActionComponent actionComponetObj(this);
	actionComponetObj.CloseTSPalette();
	return kTrue;	
}

bool16 TSLoginEventsHandler::resetPlugIn(void)
{
	//CA("TSLoginEventsHandler::resetPlugIn");
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IApplication> 	iApplication(GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){ 
			//CA("No iApplication");
			ptrIAppFramework->LogDebug("AP7_TABLESource::SPLoginEventsHandler::resetPlugIn::iApplication==nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil){ 
			ptrIAppFramework->LogDebug("AP7_TABLESource::SPLoginEventsHandler::resetPlugIn::iPanelMgr == nil");
			break;
		}

		bool16 isPanelOpen = iPanelMgr->IsPanelWithMenuIDMostlyVisible(kTSPanelWidgetActionID);
		if(!isPanelOpen){
			break;
		}
		
		/*TSActionComponent actionComponetObj(this);
		actionComponetObj.CloseTSPalette();*/
						
		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
		if(!isUserLoggedIn)
			break;


	}while(kFalse);
	return kTrue;
}