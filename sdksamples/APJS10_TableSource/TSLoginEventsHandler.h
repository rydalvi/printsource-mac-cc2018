#ifndef __TSLoginEventsHandler_h__
#define __TSLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "TSID.h"
#include "ILoginEvent.h"

class TSLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	TSLoginEventsHandler(IPMUnknown* );
	~TSLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif