
#include "VCPlugInHeaders.h"
#include "TSMediatorClass.h"
#include "IControlView.h"

//UIDRef		TSMediatorClass::curSelItemUIDRef=-1;
double       TSMediatorClass::sectionID=-1;
double       TSMediatorClass::objectID=-1;
double       TSMediatorClass::parentID=-1;
double       TSMediatorClass::parentTypeID=-1;
double       TSMediatorClass::pbObjectID=-1;
int32       TSMediatorClass::count5=-1;
int32		TSMediatorClass::curSelRowIndex=-1;
PMString	TSMediatorClass::curSelRowString("");
int32		TSMediatorClass::curSelRadBtn=1;	
int32		TSMediatorClass::curSelLstbox=1;	
bool16		TSMediatorClass::imageFlag=kFalse;
double		TSMediatorClass::lastSelClasId=0;
PMString	TSMediatorClass::lastSelClasName("");
PMString	TSMediatorClass::lastSelClasHierarchy("");
double		TSMediatorClass::lastSelClasIdForRefresh=0;
PMString	TSMediatorClass::lastSelClasNameForRefresh("");
PMString	TSMediatorClass::lastSelClasHierarchyForRefresh("");
IControlView* TSMediatorClass::PFLstboxCntrlView=nil;
IControlView* TSMediatorClass::PGLstboxCntrlView=nil;
IControlView* TSMediatorClass::PRLstboxCntrlView=nil;
IControlView* TSMediatorClass::ItemLstboxCntrlView=nil;
IControlView* TSMediatorClass::ProjectLstboxCntrlView=nil;

// Added by Awasthi
IControlView* TSMediatorClass::dropdownCtrlView1=nil;
IControlView* TSMediatorClass::refreshBtnCtrlView1=nil;
IControlView* TSMediatorClass::appendRadCtrlView1=nil;
IControlView* TSMediatorClass::embedRadCtrlView1=nil;
IControlView* TSMediatorClass::tagFrameChkboxCtrlView1=nil;
// End Awasthi

IPanelControlData* TSMediatorClass::iPanelCntrlDataPtr=nil;
IPanelControlData* TSMediatorClass::iPanelCntrlDataPtrTemp=nil;
int32 TSMediatorClass::tableFlag=0;
bool16 TSMediatorClass::loadData=kFalse;
//TSMediaterClass::TSTreeData

//LanguageModelCache* TSMediatorClass::cacheForLangModel=new LanguageModelCache();

CLanguageModel TSMediatorClass::CurrentLanguageModel;
int32 TSMediatorClass::LanguageModelCacheSize = -1;

ActionID TSMediatorClass::CurrentLangActionID = -1;
double TSMediatorClass::CurrLanguageID = -1;

bool16 TSMediatorClass::doReloadFlag = kFalse;

//added by vijay choudhari on 24-04-2006 . UID of the frame(Active frame which
//has cursor)is stored in following variable to make it persistent.
//int32	TSMediatorClass::initialSelItemUIDRef = 0;
//int32	TSMediatorClass::initialCaratPosition = 0;
//int32   TSMediatorClass::overLapingStatus = 0;
//int32   TSMediatorClass::addTagToTextDoubleClickVersion = 0;
//int32   TSMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = 0;
//int32   TSMediatorClass::appendTextIntoSelectedAndOverlappedBox = 0;
//int32	TSMediatorClass::isCaratInsideTable = 0;
//bool16  TSMediatorClass::isInsideTable = kFalse;
//bool16	TSMediatorClass::checkForOverlapWithTextFrame = kFalse; 
//bool16	TSMediatorClass::checkForOverlapWithMasterframe = kFalse;
//UIDList	TSMediatorClass::UIDListofMasterPageItems;
//added by vijay on 9-9-2006
bool16	TSMediatorClass::IsOneSourceMode = kFalse;
//added by vijay on 9-11-2006
//bool16	TSMediatorClass::IsTableInsideTableCell= kFalse;

int32 TSMediatorClass::isProduct =-1; //0= Item, 1= Item Group
