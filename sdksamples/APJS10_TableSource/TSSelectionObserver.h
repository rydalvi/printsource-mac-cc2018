#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
//#include "CObserver.h"
#include "TSID.h"
#include "IPanelControlData.h"
#include "IAppFramework.h"
#include "ITextModel.h"
#include "PMPathPoint.h"
#include "IPageItemScrapData.h"
#include "ITableModel.h"
#include "CSprayStencilInfo.h"
#include "TagStruct.h"


class TSSelectionObserver : public ActiveSelectionObserver
{
	public:
		TSSelectionObserver(IPMUnknown *boss );
		~TSSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void loadPaletteData();	
		bool16 getStencilInfo(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo);
		bool16 getSelectedBoxIds(UIDList& selectUIDList);
		bool16 doesExist(TagList &tagList,const UIDList &selectUIDList);

	protected:	
		IPanelControlData*	QueryPanelControlData();
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		
	
};


