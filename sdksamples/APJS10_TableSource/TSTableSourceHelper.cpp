#include "VCPlugInHeaders.h"

#include "TSTableSourceHelper.h"
#include "TSID.h"

#include "IAppFramework.h"
#include "ICoreFilename.h"
#include "SDKUtilities.h"
#include "ICoreFilenameUtils.h"
#include "CAlert.h"
#include "IPanelControlData.h"
#include "IClientOptions.h"
#include "K2Vector.h" 
#include "FileUtils.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "IApplication.h"
#include "IListBoxController.h"
#include "ILibrary.h"
#include "LibraryProviderID.h"
//#include "IDFile.h"
#include "LocaleSetting.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "PMString.h"
#include "ISysFileData.h"
//#include "SDKFileHelper.h"
#include "IListBoxController.h"
#include "StreamUtil.h"
//#include "IImportProvider.h"
#include "PaletteRef.h"

#include "TSDataNode.h"
#include "ITreeViewMgr.h"
#include "TSTreeModel.h"
#include "TSTreeData.h"
#include "TSMediatorClass.h"
#include "K2TypeTraits.h"
#include "MSysType.h"


#define CA(x) CAlert::InformationAlert(x)

using namespace std;

TSTreeData ItemData;	
TSTreeData PRData;
TSDataNodeList PRDataNodeList;
TSDataNodeList PFDataNodeList;
TSDataNodeList PGDataNodeList;
TSDataNodeList ITEMDataNodeList;
extern double globalLanguageID;
int32 SelectedRowNo=0;
double currentSectionID = -1;
double currentobjectID = -1;
VectorScreenTableInfoPtr tableInfo = NULL;
VectorAdvanceTableScreenValuePtr hybridTableInfo = NULL;

VectorScreenTableInfoPtr typeTableValObj = NULL;
VectorAdvanceTableScreenValuePtr typeHybridTableValObj = NULL;

TableSourceInfoValue TableSourceInfoValueObj;
double pubObjectID = -1;
double itemId = -1;

class TSTableSourceHelper : public CPMUnknown<ITSTableSourceHelper>
{
public :
	//TSTableSourceHelper();
	TSTableSourceHelper(IPMUnknown*  boss);
	~TSTableSourceHelper();
	virtual void showTableSourcePanel() ;
	virtual void positionTABLESourcePanel(int32 left , int32 righ , int32 top , int32 bottom) ;
	virtual void showTableList(double objectID , double langID , double parentID , double parentTypeID , double sectionID , int32 isProductOrItem,double pbObjectID);
	virtual void populateProductTableList(double,double);
	virtual bool16  isTABLESourcePanelOpen();
	virtual void populateItemTableList(double sectionID , double objectID);
	
	virtual void setTableSourceInfoValueObj(TableSourceInfoValue &obj);
	virtual TableSourceInfoValue * getTableSourceInfoValueObj();

	IPMUnknown* boss_;
};

CREATE_PMINTERFACE(TSTableSourceHelper , kTSTableSourceHelperImpl)

TSTableSourceHelper::TSTableSourceHelper(IPMUnknown* boss):CPMUnknown<ITSTableSourceHelper>(boss)
{
	 boss_ = boss ;
}

TSTableSourceHelper ::~TSTableSourceHelper()
{}

void TSTableSourceHelper::showTableSourcePanel()
{
	//CA("TSTableSourceHelper :: showTableSourcePanel()");
	do
	{
		InterfacePtr<IApplication> 	iApplication(GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication == nil)
		{
			//CA("No iApplication");		
			break ;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == NULL)
		{
			//CA("No iPanelMgr");
			break ;
		}

		const ActionID MyPalleteActionID = kTSPanelWidgetActionID;
		iPanelMgr->ShowPanelByMenuID(MyPalleteActionID);

	}while(kFalse);
}

void TSTableSourceHelper :: positionTABLESourcePanel(int32 TopBound, int32 LeftBound, int32 BottomBound, int32 RightBound)
{
	//CA(__FUNCTION__);
	do
	{
		//GSysRect PalleteBounds ;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			return ;
		}

		InterfacePtr<IApplication> 	iApplication(GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication == nil)
		{
			//CA("iApplication == nil");
			break;
		}
		
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());//QueryPaletteManager());Modified By Sachin Sharma on 23/06/07
		//if(iPaletteMgr==nil)
		//	break;
		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");
			break;
		}

		
		//UID paletteUID = kInvalidUID ;
		const ActionID MyPalleteActionID = kTSPanelWidgetActionID ;
		//paletteUID = iPanelMgr->GetPaletteUID(MyPalleteActionID) ;  //Commented By aschin Sharma on 23/06/07

		//Added By sachin Sharma on 23/06/07
		IControlView* iControlView = NULL;
		iControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(iControlView==NULL)
		{
			//CA("CTBActionComponent::DoPallete::iControlView == nil");		
			break;
		}
		
		PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(iControlView );
		if(!palRef.IsValid())
		{
			//CA("palRef is invalid");
			break;
		}
		
		PaletteRef parentRef = PaletteRefUtils::GetParentOfPalette( palRef ); 
		if(!parentRef.IsValid())
		{
			//CA("parentRef is invalid");
			break;
		}
		PaletteRef parentparentRef = PaletteRefUtils::GetParentOfPalette( parentRef ); 
		if(!parentparentRef.IsValid())
		{
			//CA("parentparentRef is invalid");
			break;
		}
		//PaletteRefUtils::ShowHidePalette(paletteRef,PaletteRefUtils::kShowPalette);  //Commented By Sachin sharma
		
		//=============================================
		//IWindow* palette = nil;
		//IDataBase* db = ::GetDataBase(gSession);
		//if (db == nil)
		//{
			//CA("db == nil");				
			//break;
		//}
		//palette = static_cast<IWindow*>(db->Instantiate(paletteUID, IWindow::kDefaultIID));
		//if (palette == nil)
		//{
			//CA("could not query palette based on given UID!kDCNPanelWidgetActionID ");
			//break;
		//}		
		//palette->AddRef();
		//PalleteBounds  = palette->GetFrameBBox();
		
		SysRect PalleteBound = PaletteRefUtils::GetPaletteBounds(parentparentRef);
		//Int32Rect NewRect = (Int32Rect)PalleteBound; //APSCC2017_Comment
		
		int32 TemplateTop = 0;
		int32 TemplateLeft = 0;
		int32 TemplateBottom = 0;
		int32 TemplateRight = 0;
        
        TemplateTop = SysRectTop(PalleteBound);
        TemplateLeft = SysRectLeft(PalleteBound);
        TemplateRight = SysRectRight(PalleteBound);
        TemplateBottom =SysRectBottom(PalleteBound);
		
		//PMString temp = "";
		//temp.Append("top =");
		//temp.AppendNumber(TemplateTop);
		//temp.Append(", left = ");
		//temp.AppendNumber(TemplateLeft);
		//temp.Append(" , bottom = ");
		//temp.AppendNumber(TemplateBottom);
		//temp.Append(" , right = ");
		//temp.AppendNumber(TemplateRight);
		//CA(temp);
		int32 NewLeftBound = 0 ;
		if(LeftBound - 207 < 0)
			NewLeftBound = RightBound -5 ;
		else
			NewLeftBound = LeftBound - (TemplateRight - TemplateLeft -5 );
		//palette->SetPosition(NewLeftBound , TopBound , kTrue);
		PaletteRefUtils::SetPalettePosition(parentparentRef,NewLeftBound,TopBound);
	
	}
	while(kFalse);
}
bool16 TSTableSourceHelper :: isTABLESourcePanelOpen()
{
	//CA(__FUNCTION__);
	bool16 result = kFalse ;

	do{
		InterfacePtr<IApplication> 	iApplication(GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication == nil)
		{
			//CA("isImagePanelOpen::iApplication is NULL");
			break;
		}
		
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());  //QueryPaletteManager()); Modified By Sachin sharma 0n 23/06/07
		//if(iPaletteMgr == nil)
		//	break;

		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("isImagePanelOpen::iPanelMgr is NULL");		
			break;
		}

		PMLocaleId nLocale = LocaleSetting::GetLocale();

		if(iPanelMgr->IsPanelWithWidgetIDShown(kTSPanelWidgetID)) 
			result = kTrue ;

	}
	while(kFalse);
	//CA(" return image panel  opening ");
	return result ;
}

void TSTableSourceHelper::showTableList(double objectID , double langID , double parentID , double parentTypeID , double sectionID ,int32 isProductOrItem,double pbObjectID)
{
	//CA("TSTableSourceHelper::showTableList");
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		
		//ptrIAppFramework->clearAllStaticObjects();
		TSMediatorClass::sectionID  = sectionID;
		TSMediatorClass::objectID  = objectID;
		TSMediatorClass::parentID  = parentID;
		TSMediatorClass::parentTypeID  = parentTypeID;
		TSMediatorClass::pbObjectID = pbObjectID;		
		TSMediatorClass::isProduct = isProductOrItem;


		PMString itemFieldIds("");
		PMString itemAssetTypeIds("");
		PMString itemGroupFieldIds("");
		PMString itemGroupAssetTypeIds("");
		PMString listTypeIds("");
		PMString listItemFieldIds("");
		double langId = ptrIAppFramework->getLocaleId();
		PMString currentSectionitemGroupIds("");
		PMString currentSectionitemIds("");
				
		if(isProductOrItem == 3)
		{
			//CA("isProductOrItem == 3");
			SelectedRowNo=isProductOrItem;
			int32 isProduct =1;
			currentSectionitemGroupIds.AppendNumber(PMReal(TSMediatorClass::objectID));
			
			//ptrIAppFramework->EventCache_setCurrentObjectData(sectionID, objectID, isProduct , langId, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds,  listTypeIds, listItemFieldIds);

			ptrIAppFramework->EventCache_setCurrentSectionData( sectionID, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, kFalse);

			populateProductTableList(sectionID,objectID);
		}
		else if(isProductOrItem == 4)
		{
			//CA("isProductOrItem == 4"); 2EFGB/'

			SelectedRowNo=isProductOrItem;
			int32 isProduct =0;
			currentSectionitemIds.AppendNumber(PMReal(TSMediatorClass::objectID));
			//ptrIAppFramework->EventCache_setCurrentObjectData(sectionID, objectID, isProduct , langId, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds,  listTypeIds, listItemFieldIds);
			ptrIAppFramework->EventCache_setCurrentSectionData( sectionID, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, kFalse);

			populateItemTableList(sectionID,objectID);
		}

		IControlView* TreeCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
		if(TreeCtrlView==nil)
		{
			//CA("TreeCtrlView==nil");
			break;
		}
		InterfacePtr<ITreeViewMgr> treeViewMgr(TreeCtrlView, UseDefaultIID());
		if(!treeViewMgr) { 
			//CA("!treeViewMgr");
			break;
		}

		TSTreeModel pModel;
		PMString root("Root");
					
		switch(SelectedRowNo) 
		{
			case 0:
				pModel.setRoot(-1, root, 1);
				break;
			case 1:
				pModel.setRoot(-1, root, 2);
				break;
			case 2:
				pModel.setRoot(-1, root, 3);
				break;
			case 3:
				pModel.setRoot(-1, root, 4);
				break;
			case 4:
				pModel.setRoot(-1, root, 5);
				break;
							
		}
		treeViewMgr->ClearTree(kTrue);
		pModel.GetRootUID();
		treeViewMgr->ChangeRoot();

		IControlView* listTableNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kListTableNameWidgetID);
		if (listTableNameCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::showTableList::listTableNameCtrlView is nil");
			break ;
		}
		InterfacePtr<ITextControlData> listTableNameTxtCntrlData(listTableNameCtrlView, UseDefaultIID());
		if(listTableNameTxtCntrlData==nil)
		{
			//CA("listTableNameTxtCntrlData is NULL");
			break ;
		}

		IControlView* stencilNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kStencilNameWidgetID);
		if (stencilNameCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::showTableList::stencilNameCtrlView is nil");
			break;
		}

		InterfacePtr<ITextControlData> stencilNameTxtCntrlData(stencilNameCtrlView, UseDefaultIID());
		if(stencilNameTxtCntrlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::showTableList::stencilNameTxtCntrlData is nil");
			break;
		}
		
		IControlView* updateDateCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kUpdateDateWidgetID);
		if (updateDateCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::showTableList::updateDateCtrlView is nil");
			break;
		}
		InterfacePtr<ITextControlData> updateDateTxtCntrlData(updateDateCtrlView, UseDefaultIID());
		if(updateDateTxtCntrlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::showTableList::updateDateTxtCntrlData is nil");
			break;
		}
		listTableNameTxtCntrlData->SetString("");
		stencilNameTxtCntrlData->SetString("");
		updateDateTxtCntrlData->SetString("");

		//================================MANE=================04/06/09 

	
		//CA("result inshowTableList");
		IControlView* RelodButtonCtrlView1= TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSRelodButtonWidgetID);
		if (RelodButtonCtrlView1== nil)
		{
			ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::RelodButtonCtrlView is nil");
			break;
		}
		RelodButtonCtrlView1->Enable();
	
		IControlView* UpButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSUpButtonWidgetID);
		if (UpButtonCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::UpButtonCtrlView is nil");
			break;
		}
		UpButtonCtrlView->Enable();

		IControlView* DownButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSDownButtonWidgetID);
		if (DownButtonCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::DownButtonCtrlView is nil");
			break;
		}
		DownButtonCtrlView->Enable();			

		IControlView* PreviewButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSPreviewButtonWidgetID);
		if (PreviewButtonCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::PreviewButtonCtrlView is nil");
			break;
		}
		PreviewButtonCtrlView->Enable();

		IControlView* SprayButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSSprayButtonWidgetID);
		if (SprayButtonCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::SprayButtonCtrlView is nil");
			break;
		}
		SprayButtonCtrlView->Enable();
	
		//===================================UP TO HERE==========04/06/09 
		ptrIAppFramework->EventCache_clearCurrentObjectData();
	}while(kFalse);

}

void TSTableSourceHelper::populateProductTableList(double sectionID , double objectID)
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}

		currentSectionID = sectionID;
		currentobjectID = objectID;

		PRDataNodeList.clear();
		TSDataNode PRListData;
		PRData.clearVector(3);
		int i=0;
		
		do{
			
			tableInfo = NULL;
			
			if(ptrIAppFramework->get_isONEsourceMode())
			{
				//CA("  get_isONEsourceMode  ");
				//tableInfo = ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectID);
			}
			else
			{
				//CA(" Product ");
				tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionID,objectID, TSMediatorClass::CurrLanguageID, kFalse);
			}
			
			if(!tableInfo)
			{
				//CA("!tableInfo");
				ptrIAppFramework->LogDebug("AP7_TABLESource::TSTableSourceHelper::populateProductTableList::tableInfo is NULL");	
				break;
			}
			if(tableInfo->size()==0)
			{
				//CA("tableInfo->size()==0");
				break;
			}
			VectorScreenTableInfoValue::iterator it;
			for(it=tableInfo->begin();it!=tableInfo->end();it++)
			{
				PRData.setData
				(
					3,
					i,
					it->getTableID(),
					it->getTableTypeID(),
					TSMediatorClass::CurrLanguageID, 
					it->getName(),
					kTrue,
					"",
					kFalse,
					1 // For Tables only
				);
				
				PMString temp(it->getName());
				//temp.Append(" : List");
				temp.Append(" : ");
				temp.Append(ptrIAppFramework->StructureCache_getListNameByTypeId(it->getTableTypeID()));
				PRListData.setAll(temp,it->getTableID(),1,1,0,8);
				PRListData.setHitCount(8);				
				PRDataNodeList.push_back(PRListData);
				i++;
			}
		
		}while(kFalse);
		//------------
		do{
			hybridTableInfo = ptrIAppFramework->getHybridTableData(sectionID,objectID,TSMediatorClass::CurrLanguageID, 1, kFalse);
			if(!hybridTableInfo)
			{
				//CA("!hybridTableInfo");
				ptrIAppFramework->LogDebug("AP7_TABLESource::TSTableSourceHelper::populateProductTableList::hybridTableInfo is NULL");
				break;
			}
			if(hybridTableInfo->size()==0)
			{
				break;
			}
			VectorAdvanceTableScreenValue::iterator it1;
			for(it1=hybridTableInfo->begin();it1!=hybridTableInfo->end();it1++)
			{
				PRData.setData
				(
					3,
					i,
					it1->getTableId(),//-115
					it1->getTableTypeId(),
					TSMediatorClass::CurrLanguageID,
					it1->getTableName(),
					kTrue,
					"",
					kFalse,
					1
				);
				
				PMString temp(it1->getTableName());
				//temp.Append(" (Table)");
				temp.Append(" : ");
				temp.Append(ptrIAppFramework->StructureCache_getListNameByTypeId(it1->getTableTypeId()));
				PRListData.setAll(temp,it1->getTableTypeId(),1,1,0,2);
				PRListData.setHitCount(2);
				PRDataNodeList.push_back(PRListData);
				i++;
			}
		}while(kFalse);
	}while(kFalse);
}

void TSTableSourceHelper::setTableSourceInfoValueObj(TableSourceInfoValue &obj)
{
	//CA("aaaa");
	TableSourceInfoValueObj = obj;
	//CA("bbbb");
}
TableSourceInfoValue * TSTableSourceHelper::getTableSourceInfoValueObj()
{
	return &TableSourceInfoValueObj;
}

void TSTableSourceHelper::populateItemTableList(double sectionID , double objectID)
{
	//CA("TSTableSourceHelper::populateItemTableList");
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}

		currentSectionID = sectionID;
		currentobjectID = objectID;

		ITEMDataNodeList.clear();
		TSDataNode ItemListData;
		ItemData.clearVector(4);
		int i=0;
		//int32 pubObjectID;
		
		do{
			if(ptrIAppFramework->get_isONEsourceMode())
			{
				//typeTableValObj = ptrIAppFramework->GETONEsourceObjects_getItemTablesByItenId(currentobjectID);
				
			}
			else
			{
				int32 isProduct =0;
				CPbObjectValue CPbObjectValueobj = ptrIAppFramework->getPbObjectValueBySectionIdObjectId(currentSectionID,currentobjectID, isProduct, TSMediatorClass::CurrLanguageID);
				pubObjectID = CPbObjectValueobj.getPub_object_id();
				itemId = CPbObjectValueobj.getObject_id();
		
				typeTableValObj = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(itemId, currentSectionID, TSMediatorClass::CurrLanguageID);
			}
			if(typeTableValObj==nil)
			{
				//CA("typeTableValObj==nil");
				ptrIAppFramework->LogError("AP7_TABLESource::TSTableSourceHelper::populateItemTableList::AttributeCache_getItemTableTypes's typeTableValObj is nil");	
				break;
			}
			if(typeTableValObj->size()==0)
			{
				//CA("typeTableValObj->size()==0");
				break;
			}

			VectorScreenTableInfoValue::iterator it1;

			for(it1=typeTableValObj->begin();it1!=typeTableValObj->end();it1++)
			{
				ItemData.setData
				(	4,
					i,
					it1->getTableID(),//-1,
					it1->getTableTypeID(),
					TSMediatorClass::CurrLanguageID, 
					it1->getName(),
					kTrue,
					"",
					kFalse,
					1 // For Tables only
				);

				//CA("List Table : "+it1->getName());
				PMString temp(it1->getName());
				//temp.Append(" (List)");
				temp.Append(" : ");
				temp.Append(ptrIAppFramework->StructureCache_getListNameByTypeId(it1->getTableTypeID()));
				ItemListData.setAll(temp,it1->getTableTypeID(),1,1,0,8);
				ItemListData.setHitCount(8);	
				ITEMDataNodeList.push_back(ItemListData);
				i++;
			}
		}while(kFalse);
		do{
			
			typeHybridTableValObj = ptrIAppFramework->getHybridTableData(sectionID,objectID,TSMediatorClass::CurrLanguageID, 0, kFalse);
			if(typeHybridTableValObj!=nil)
			{
				VectorAdvanceTableScreenValue::iterator it2;
				for(it2=typeHybridTableValObj->begin();it2!=typeHybridTableValObj->end();it2++)
				{
					//CA("Hybrid Table : "+it2->getTableName());
					ItemData.setData
					(	4,
						i,
						it2->getTableId(),//-115,
						it2->getTableTypeId(),
						TSMediatorClass::CurrLanguageID,
						it2->getTableName(),
						kTrue,
						"",//it2->getCode(),
						kFalse,
						1
					);
					
					PMString temp(it2->getTableName());
					//temp.Append(" (Table)");
					temp.Append(" : ");
					temp.Append(ptrIAppFramework->StructureCache_getListNameByTypeId(it2->getTableTypeId()));
					ItemListData.setAll(temp,it2->getTableTypeId(),1,1,0,2);
					ItemListData.setHitCount(2);
					ITEMDataNodeList.push_back(ItemListData);
					i++;
				}
			}
		}while(kFalse);

	}while(kFalse);
}
