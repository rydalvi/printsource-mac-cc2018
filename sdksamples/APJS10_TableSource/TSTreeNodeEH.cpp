//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "IEventHandler.h"
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "TSID.h"
#include "TSMediatorClass.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "ITextFrame.h"         //Removed from CS3 API
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITreeViewController.h"
#include "UIDNodeID.h"
#include "TSDataNode.h"
#include "TSTreeDataCache.h"
//#include "TSCommonFunctions.h"
//#include "TSManipulateInline.h" 
#include "ILayoutSelectionSuite.h"
#include "ITableModelList.h"
#include "ILayoutUIUtils.h"
#include "IXMLUtils.h"
#include "ITextStoryThreadDict.h"
//#include "ITblBscSuite.h"
#include <IFrameUtils.h>
#include "CAlert.h"
//#include "IMessageServer.h"
//---------CS3 Addition-----------------//
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "SDKLayoutHelper.h"
#include "IGraphicFrameData.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITriStateControlData.h"

#include "IDialogController.h"
//#include "ILoginHelper.h"

#include "IAppFramework.h"
#include "ITextControlData.h"

#include "IDataSprayer.h"
#include "AcquireModalCursor.h"
#include "TSTreeData.h"
#include "VirtualKey.h"
#include "TableSourceInfoValue.h"
#include "TSTableSourceHelper.h"
#include "TSItemItemGroupSpray.h"

#ifdef MACINTOSH
#include "K2Vector.tpp"
#endif


#define FILENAME			PMString("TSTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern VectorScreenTableInfoPtr tableInfo;
extern double currentobjectID;
extern VectorAdvanceTableScreenValuePtr hybridTableInfo;
extern TSDataNodeList PRDataNodeList;

extern int32 SelectedRowNo;
extern VectorScreenTableInfoPtr typeTableValObj;
extern VectorAdvanceTableScreenValuePtr typeHybridTableValObj;
extern TSDataNodeList ITEMDataNodeList;
//extern bool16 ISTabbedText;
//extern UIDRef newFrameUIDRef;    // Global variable in TSManipulateInline.cpp
//extern bool16 isComponentAttr;
//extern bool16 isXRefAttr;
//extern bool16 isAccessoryAttr;
//extern bool16 isAddAsDisplayName;
//extern bool16 isAddTableHeader;
//extern bool16 isAddImageDescription;

//#include "PnlTrvFileNodeID.h"
//#include "PnlTrvUtils.h"

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	PnlTrvNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~PnlTrvNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);//{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonDblClk(e);  return retval; }
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvNodeEH, kPnlTrvNodeEHImpl)

	
PnlTrvNodeEH::PnlTrvNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 PnlTrvNodeEH::LButtonDn(IEvent* e)
{
	//CA("LButton Down");
	bool16 retval = kFalse; 

//	InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  
//	retval =  delegate->LButtonDn(e);  

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}
			
	IControlView* ProductTreePanelCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
	if (ProductTreePanelCtrlView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::Update::ProductTreePanelCtrlView is nil");
		return retval;
	}
	TSMediatorClass::PRLstboxCntrlView = ProductTreePanelCtrlView;

	InterfacePtr<ITreeViewController> treeViewMgr(ProductTreePanelCtrlView, UseDefaultIID());
	if(treeViewMgr==nil)
	{
		return retval;
	}
	NodeIDList previousSelectedItem;
	treeViewMgr->GetSelectedItems(previousSelectedItem);
	

	//// Added on 20109
	if(!(e->LeftControlDown() || e->RightControlDown() ||  e->ShiftKeyDown()))
		treeViewMgr->DeselectAll();

	InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);
	retval =  delegate->LButtonDn(e);
	//// END

	NodeIDList selectedItem;
	treeViewMgr->GetSelectedItems(selectedItem);

	if(selectedItem.size()<=0)
	{
		return retval;
	}
	
	NodeID nid=selectedItem[selectedItem.size()-1];

	TreeNodePtr<UIDNodeID>uidNodeIDTemp(nid);

	int32 uid= uidNodeIDTemp->GetUID().Get();
	TSDataNode tsNode;
	TSTreeDataCache dc;

	if(uid == 0)
		return retval;

	bool16 isPresent = dc.isExist(uid, tsNode);
	if(isPresent == kFalse)
	{
		//CA("isPresent == kFalse");
		return retval;
	}

	IControlView* listTableNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kListTableNameWidgetID);
	if (listTableNameCtrlView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::listTableNameCtrlView is nil");
		return retval;
	}
	InterfacePtr<ITextControlData> listTableNameTxtCntrlData(listTableNameCtrlView, UseDefaultIID());
	if(listTableNameTxtCntrlData==nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::listTableNameTxtCntrlData is nil");
		return kFalse;
	}

	IControlView* stencilNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kStencilNameWidgetID);
	if (stencilNameCtrlView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::stencilNameCtrlView is nil");
		return retval;
	}
	InterfacePtr<ITextControlData> stencilNameTxtCntrlData(stencilNameCtrlView, UseDefaultIID());
	if(stencilNameTxtCntrlData==nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::stencilNameTxtCntrlData is nil");
		return kFalse;
	}

	IControlView* updateDateCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kUpdateDateWidgetID);
	if (updateDateCtrlView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::updateDateCtrlView is nil");
		return retval;
	}
	InterfacePtr<ITextControlData> updateDateTxtCntrlData(updateDateCtrlView, UseDefaultIID());
	if(updateDateTxtCntrlData==nil)
	{
		ptrIAppFramework->LogDebug("AP7_TABLEsource::PnlTrvNodeEH::LButtonDn::updateDateTxtCntrlData is nil");
		return kFalse;
	}

	if(SelectedRowNo == 3)	//-------Condtion For Product Table----
	{
		//CA("SelectedRowNo == 3");
		int32 hitCount=0;
		int32 size = 0;
		if(tableInfo)
			 size=static_cast<int32>(tableInfo->size());
		
		PMString tableName,updateDate,stencilName;
		hitCount=PRDataNodeList.at(uid-1).getHitCount();

		if(hitCount == 8)  //----Condtion For Table(List)------
		{
			//CA("hitCount == 8");
			tableName.clear();
			updateDate.clear();
			stencilName.clear();

			tableName.Append(tableInfo->at(uid-1).getName());

			//updateDate.Append(tableInfo->at(uid-1).getobjectTableValueValue().getformattedUpdateDate());
			stencilName.Append(tableInfo->at(uid-1).getstencil_name());
			tableName.SetTranslatable(kFalse);
			stencilName.SetTranslatable(kFalse);
            tableName.ParseForEmbeddedCharacters();
            stencilName.ParseForEmbeddedCharacters();
			listTableNameTxtCntrlData->SetString(tableName);
			stencilNameTxtCntrlData->SetString(stencilName);	
			//updateDateTxtCntrlData->SetString(updateDate);
		}
		if(hitCount == 2)	//-----Condition For Hybrid Table-----
		{
			//CA("hitCount == 2");
			if(e->LeftControlDown()|| e->RightControlDown()|| e->ShiftKeyDown())
			{
				if(previousSelectedItem.size()>0)
				{
					for(int32 i=0;i<previousSelectedItem.size();i++)
					{
						treeViewMgr->Deselect(previousSelectedItem[i],kTrue,kTrue);
					}
				}
			}
			tableName.clear();
			stencilName.clear();
			updateDate.clear();

			tableName.Append(hybridTableInfo->at(uid-size-1).getTableName());
			//updateDate.Append(hybridTableInfo->at(uid-size-1).getupdateDate());
			//stencilName.Append(hybridTableInfo->at(uid-size-1).getstencil_name
			tableName.ParseForEmbeddedCharacters();
			tableName.SetTranslatable(kFalse);

			listTableNameTxtCntrlData->SetString(tableName);
			stencilNameTxtCntrlData->SetString(stencilName);
			updateDateTxtCntrlData->SetString(updateDate);

		}
	}
	if(SelectedRowNo == 4)	//----Condtion For Item Table----
	{
		//CA("SelectedRowNo == 4");
		int32 hitCount=0;
		int32 size = 0;
		if(typeTableValObj)
			 size=static_cast<int32> (typeTableValObj->size());
		
		PMString tableName,updateDate,stencilName;
		hitCount=ITEMDataNodeList.at(uid-1).getHitCount();

		if(hitCount == 8)  //----Condtion For Table(List)------
		{
			//CA("hitCount == 8");
			tableName.clear();
			updateDate.clear();
			stencilName.clear();

			tableName.Append(typeTableValObj->at(uid-1).getName());
			//updateDate.Append(typeTableValObj->at(uid-1).getformattedUpdateDate());
			stencilName.Append(typeTableValObj->at(uid-1).getstencil_name());
			tableName.SetTranslatable(kFalse);
			stencilName.SetTranslatable(kFalse);
            tableName.ParseForEmbeddedCharacters();
            stencilName.ParseForEmbeddedCharacters();
			listTableNameTxtCntrlData->SetString(tableName);
			stencilNameTxtCntrlData->SetString(stencilName);	
			//updateDateTxtCntrlData->SetString(updateDate);
		}
		if(hitCount == 2)	//-----Condition For Hybrid Table-----
		{
			//CA("hitCount == 2");
			if(e->LeftControlDown()|| e->RightControlDown()|| e->ShiftKeyDown())
			{
				if(previousSelectedItem.size()>0)
				{
					for(int32 i=0;i<previousSelectedItem.size();i++)
					{
						treeViewMgr->Deselect(previousSelectedItem[i],kTrue,kTrue);
					}
				}
			}
			tableName.clear();
			stencilName.clear();
			updateDate.clear();

			tableName.Append(typeHybridTableValObj->at(uid-size-1).getTableName());
			//updateDate.Append(typeHybridTableValObj->at(uid-size-1).getupdateDate());
			//stencilName.Append(typeHybridTableValObj->at(uid-size-1).getstencil_name());
			tableName.ParseForEmbeddedCharacters();
			tableName.SetTranslatable(kFalse);

			listTableNameTxtCntrlData->SetString(tableName);
			stencilNameTxtCntrlData->SetString(stencilName);
			updateDateTxtCntrlData->SetString(updateDate);
		}

	}
	
	return kTrue;

}
bool16 PnlTrvNodeEH::ButtonDblClk(IEvent* e)
{
	//CA("ButtonDblClk");
	bool16 retval=kFalse;
	InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER); 
	retval =  delegate->ButtonDblClk(e);  
	
	AcquireWaitCursor awc;
	awc.Animate(); 
	ItemItemGroupSpray itemItemGroupSprayObj;
	itemItemGroupSprayObj.startSpraying();

	return kTrue;
}

//	end, File:	PnlTrvNodeEH.cpp
