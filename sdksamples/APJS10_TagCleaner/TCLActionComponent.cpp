//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "TCLID.h"
#include "IAppFramework.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "TCLActionComponent.h"
#include "LocaleSetting.h"
#include "IPanelControlData.h"
#include "IActionStateList.h"
#include "ITextControlData.h"

bool16 FunctionAccess_MMY = kFalse;

bool16 IsRefresh;
int32 flag_1;
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup ap7_tagcleaner

*/
//class TCLActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		TCLActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(TCLActionComponent, kTCLActionComponentImpl)

/* TCLActionComponent Constructor
*/
TCLActionComponent::TCLActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void TCLActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kTCLPopupAboutThisActionID:
		case kTCLPanelPSMenuActionID :
		case kTCLPanelWidgetActionID:
			{
				
			this->DoPalette();
			break;	
			}
		case kTCLAboutActionID:
		{
			this->DoAbout();
			break;
		}
					


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void TCLActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kTCLAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

void TCLActionComponent::DoPalette()
{
	//CA("DoPalette");
	
   PMString var="NotanalyzedNew";
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");		
			break;
		}
		/*InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
			break;*/
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");	
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		// Here we are opening our Template Palette
		// [[[[[[[[ IMP Function ]]]]]]]]
		//CA("DP1");
		iPanelMgr->ShowPanelByMenuID(kTCLPanelWidgetActionID);
		/*iPanelMgr->ShowPanelByWidgetID(kTPLPanelWidgetID);
		iPanelMgr->SetPanelResizabilityByWidgetID(kTPLPanelWidgetID,kTrue);*/
		
		if(!IsRefresh && flag_1 == 1)
		{		
			//CA("!IsRefresh && Flag1 ==1");
			//FunctionAccess_MMY = ptrIAppFramework->GetConfig_getFunctionAccessByFunctionCode("MMY");

			if(iPanelMgr->IsPanelWithWidgetIDShown(kTCLPanelWidgetID))
			{
				flag_1++;
				//CA("iPanelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID)");

				IControlView* icontrol = iPanelMgr->GetVisiblePanel(kTCLPanelWidgetID);
				if(!icontrol)
				{
					//CA("!icontrol");
					return;
				}
				//icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));//15-01-08		Amit
			}
			
			//icontrol->Resize();
			
		}

//		TPLActionComponent::palettePanelPtr=iPaletteMgr;
		

	}while(kFalse);
}

void TCLActionComponent::ClosePallete(void)
{
	//CA("CTBActionComponent::ClosePallete");
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication==nil)
		{
			//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iApplication == nil");										
			break;
		}
//Commented by nitin
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		//if(iPaletteMgr==nil)
		//{
		//	//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iPaletteMgr == nil");										
		//	break;
		//}

		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
//Following code added bye nitin
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 

		//if(ClassMediator::iPanelCntrlDataPtr == nil)
		//{
		//	//CA("ClassMediator::iPanelCntrlDataPtr == nil");
		//	//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iPanelCntrlDataPtr == nil");										
		//	break;
		//}

		//if(ClassMediator::ClassTreeCntrlView == nil)
		//{
		//	//CA("ClassMediator::ClassTreeCntrlView == nil");
		//	//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::ClassTreeCntrlView == nil");										
		//	break;
		//}
	
	//	ClassMediator::ClassTreeCntrlView->Hide();
//commented by nitin
		//if(iPaletteMgr)
//Following code commented and added bye nitin
		if(iApplication->QueryPanelManager())
		{ 		
//			InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iPanelMgr == nil");													
				break;
			}
		 
			//if(iPanelMgr->IsPanelWithMenuIDVisible(kCTBPanelWidgetActionID))
			if(iPanelMgr->IsPanelWithMenuIDShown(kTCLPanelWidgetActionID))
			{				
				//CA("IsPanelWithMenuIDShown return kTrue");
				iPanelMgr->HidePanelByMenuID(kTCLPanelWidgetActionID);				
			}
		}
      /*
		if(itemProductCategoryCountPtr)
		{
			delete itemProductCategoryCountPtr;
			itemProductCategoryCountPtr = NULL;
		}
		if(itemProductSectionCountPtr)
		{
			delete itemProductSectionCountPtr;
			itemProductSectionCountPtr = NULL;
		}
		*/

	}while(kFalse);	
}
void TCLActionComponent::UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{	
	//CA("TPLActionComponent::UpdateActionStates");
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	//CA("ptrIAppFramework == nil");
	//	return;
	//}
	//bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();

	//for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	//{
	//	ActionID actionID = iListPtr->GetNthAction(iter);
	//		
	//		if(result)
	//		{
	//			iListPtr->SetNthActionState(iter,kEnabledAction|kSelectedAction);
	//	//		iListPtr->SetNthActionState(iter,kSelectedAction);
	//		}		
	//		else
	//			iListPtr->SetNthActionState(iter,kEnabledAction);

			//switch(actionID.Get())
			//{ 	
			//case kTCLPanelWidgetActionID:
			//{
			//	//PMString string("Setting ActionState", -1, PMString::kNoTranslate);
			//	//CAlert::InformationAlert(string);
			//	if(result)
			//	{
			//		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			//		ASSERT(app);
			//		if(!app)
			//		{
			//			ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLActionComponent::UpdateActionStates::!app");
			//			return;
			//		}
			//		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
			//		ASSERT(paletteMgr);
			//		if(!paletteMgr)
			//		{
			//			ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLActionComponent::UpdateActionStates::!paletteMgr");
			//			return;
			//		}*/
			//		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager()/*, UseDefaultIID()*/);
			//		ASSERT(panelMgr);
			//		if(!panelMgr)
			//		{
			//			ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLActionComponent::UpdateActionStates::!panelMgr");
			//			return;
			//		}
			//		
			//		if(panelMgr->IsPanelWithWidgetIDShown(kTCLPanelWidgetID))
			//		{
			//			iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
			//		}
			//		else
			//		{
			//			iListPtr->SetNthActionState(iter,kEnabledAction);
			//		}

			//	}
			//	else
			//		{
			//			iListPtr->SetNthActionState(iter,kDisabled_Unselected);
			//		}	
			//	break;
			/*}
		}
	}*/

		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			return;
		}
	
		bool16 result=ptrIAppFramework->getLoginStatus();
		
		for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
		{
			ActionID actionID = iListPtr->GetNthAction(iter);		
			
			if(actionID == kTCLPanelPSMenuActionID)
			{
				if(result)
					iListPtr->SetNthActionState(iter,kEnabledAction);
				else
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
			}
		}
}




//  Code generated by DollyXs code generator
