#ifndef __TCLActionComponent_h__
#define __TCLActionComponent_h__

#include "CActionComponent.h"
//#include "IPaletteMgr.h"          //Removed From Indesign CS3 API

class TCLActionComponent:public CActionComponent
{
	public:
		TCLActionComponent(IPMUnknown* boss);
		//~TCLActionComponent();
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		//virtual void UpdateActionStates(IActionStateList* listToUpdate);
		virtual void UpdateActionStates(IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint , IPMUnknown* widget );

		void ClosePallete();
		void DoPalette();

	private:
		void DoAbout();
		void DoItemOne();
		
//		static IPaletteMgr* palettePanelPtr;         //CS3 change
};

#endif