//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __TCLID_h__
#define __TCLID_h__

#include "SDKDef.h"

// Company:
#define kTCLCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kTCLCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kTCLPluginName	"Document Cleaner"			// Name of this plug-in.APJS9_TagCleaner
#define kTCLPrefixNumber	0xDB3200 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kTCLVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kTCLAuthor		"Apsiva"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kTCLPrefixNumber above to modify the prefix.)
#define kTCLPrefix		RezLong(kTCLPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kTCLStringPrefix	SDK_DEF_STRINGIZE(kTCLPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kTCLMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kTCLMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kTCLPluginID, kTCLPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kTCLActionComponentBoss, kTCLPrefix + 0)
DECLARE_PMID(kClassIDSpace, kTCLPanelWidgetBoss, kTCLPrefix + 1)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kTCLBoss, kTCLPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLContentSELECTIONOBSERVER,	kTCLPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITCLINTERFACE, kTCLPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kTCLActionComponentImpl, kTCLPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kTCLContentSelectionObserverImpl,	kTCLPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 2)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kTCLImpl, kTCLPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kTCLAboutActionID, kTCLPrefix + 0)
DECLARE_PMID(kActionIDSpace, kTCLPanelWidgetActionID, kTCLPrefix + 1)
DECLARE_PMID(kActionIDSpace, kTCLSeparator1ActionID, kTCLPrefix + 2)
DECLARE_PMID(kActionIDSpace, kTCLPopupAboutThisActionID, kTCLPrefix + 3)//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 5)
DECLARE_PMID(kActionIDSpace, kTCLPanelPSMenuActionID,	kTCLPrefix + (10 + 2))
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kTCLActionID, kTCLPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kTCLPanelWidgetID, kTCLPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kStartCleanUpButtonWidgetID, kTCLPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kClose1ButtonWidgetID, kTCLPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kTLCSecondGroupPanelWidgetID, kTCLPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kLevel2TextWidgetID, kTCLPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kLevel3TextWidgetID, kTCLPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kLevel4TextWidgetID, kTCLPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kLevel5TextWidgetID, kTCLPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kLevel6TextWidgetID, kTCLPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kAnalyzeDocumentWidgetID, kTCLPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kLevel7TextWidgetID, kTCLPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kLevel20TextWidgetID, kTCLPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kLevel8TextWidgetID, kTCLPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kLevel9TextWidgetID, kTCLPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kBoldTextWidgetID, kTCLPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kBold1TextWidgetID, kTCLPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kWarningStaticTextWidgetID, kTCLPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, knewWarningStaticTextWidgetID, kTCLPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kfilenameTextWidgetID, kTCLPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kfilehasbeencleanedTextWidgetID, kTCLPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kconcatfilenameTextWidgetID, kTCLPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kTCLWidgetID, kTCLPrefix + 25)


// "About Plug-ins" sub-menu:
#define kTCLAboutMenuKey			kTCLStringPrefix "kTCLAboutMenuKey"
#define kTCLAboutMenuPath		kSDKDefStandardAboutMenuPath kTCLCompanyKey

// "Plug-ins" sub-menu:
#define kTCLPluginsMenuKey 		kTCLStringPrefix "kTCLPluginsMenuKey"
#define kTCLPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kTCLCompanyKey kSDKDefDelimitMenuPath kTCLPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kTCLAboutBoxStringKey	kTCLStringPrefix "kTCLAboutBoxStringKey"
#define kTCLPanelTitleKey					kTCLStringPrefix	"kTCLPanelTitleKey"
#define kTCLStaticTextKey kTCLStringPrefix	"kTCLStaticTextKey"
#define kTCLInternalPopupMenuNameKey kTCLStringPrefix	"kTCLInternalPopupMenuNameKey"
#define kTCLTargetMenuPath kTCLInternalPopupMenuNameKey
#define kTCLNotAnalyzedKey kTCLStringPrefix "kTCLNotAnalyzedKey"
#define kTCLClickAnalyzeButtonKey kTCLStringPrefix "kTCLClickAnalyzeButtonKey"
#define kTCLHowManyUnusedFramesKey kTCLStringPrefix "kTCLHowManyUnusedFramesKey"
#define kTCLAnyUnusedFramesKey kTCLStringPrefix "kTCLAnyUnusedFramesKey"
#define kTCLCleanUpDocumentKey kTCLStringPrefix "kTCLCleanUpDocumentKey"
#define kTCLTotalFramesInDocumentKey kTCLStringPrefix "kTCLTotalFramesInDocumentKey"
#define kTCLUnusedFramesInDocumentKey kTCLStringPrefix "kTCLUnusedFramesInDocumentKey"
#define kTCLPrintsourceKey kTCLStringPrefix "kTCLPrintsourceKey"
#define kTCLUnusedFrameRemovalKey kTCLStringPrefix "kTCLUnusedFrameRemovalKey"
#define kTCLUsusedFrameRemovalInProgressKey kTCLStringPrefix "kTCLUsusedFrameRemovalInProgressKey"
#define kTCLHasBeenCleanedKey kTCLStringPrefix "kTCLHasBeenCleanedKey"
#define kTCLUnusedFramesHaveBeenRemovedKey kTCLStringPrefix "kTCLUnusedFramesHaveBeenRemovedKey"


// Menu item positions:

#define	kTCLSeparator1MenuItemPosition		10.0
#define kTCLAboutThisMenuItemPosition		11.0

#define kPNGCleanUp_DocumentIconRsrcID     						1010
#define kPNGCleanUp_DocumentIconRollRsrcID 						1010

#define kTCLPNGCloseIconRsrcID                          1011
#define kTCLPNGCloseIconRollRsrcID                      1011

#define kTCLPNGAnalyze_DocumentIconRsrcID                1012
#define kTCLPNGAnalyze_DocumentIconRollRsrcID            1012

// Initial data format version numbers
#define kTCLFirstMajorFormatNumber  RezLong(1)
#define kTCLFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kTCLCurrentMajorFormatNumber kTCLFirstMajorFormatNumber
#define kTCLCurrentMinorFormatNumber kTCLFirstMinorFormatNumber

#endif // __TCLID_h__
