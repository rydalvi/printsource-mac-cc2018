#ifndef __ITAGREADER_H__
#define __ITAGREADER_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"
#include "TGRID.h"

class ITagReader : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_ITAGREADER };

		virtual TagList getTagsFromBox(UIDRef, IIDXMLElement ** xmlPtr=nil)= 0;
		virtual TagList getFrameTags(UIDRef frameUIDRef) = 0;
		virtual bool16	GetUpdatedTag(TagStruct&) = 0;
	//private:
		virtual void	getTextFrameTags(void) = 0;
		virtual void	getGraphicFrameTags(void) = 0;
		virtual bool16	getCorrespondingTagAttributes(const PMString&, const PMString&, TagStruct&) = 0;
		virtual void	getTextFrameTagsForRefresh(void)=0; // To be use only for Refresh Content
		virtual TagList getTagsFromBox_ForRefresh(UIDRef boxId, IIDXMLElement** xmlPtr=nil)=0; 

		virtual TagList getTextFrameTagsForIndex(UIDRef boxUIDRef)=0;
		virtual TagList getTagsFromBox_ForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr=nil)=0;
		virtual	void getTextFrameTagsForUpdate(void) = 0;
		virtual void getGraphicFrameTagsForUpdate(void) = 0;
		virtual	TagList getTagsFromBoxForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr =nil) =0;
		virtual bool16 getCorrespondingTagAttributesForUpdate(const PMString&, const PMString&, TagStruct& /*, int32&*/) = 0;

		virtual	TagList getTagsFromBox_ForRefresh_ByAttribute(UIDRef boxId, IIDXMLElement** xmlPtr= nil)=0;   // To be use only for Refresh Content Attribute Level Refresh
		virtual void getTextFrameTagsForRefresh_ByAttribute(void)=0; // To be use only for Refresh Content Attribute Level Refresh

		virtual TagList getAllChildTags(IIDXMLElement* xmlElement)=0;
		virtual void insertStencilNameInTag(UIDRef BoxUIDRef,PMString stencilName)=0;
		virtual PMString getStencilNameAndIndex(UIDRef BoxUIDRef,PMString& index)=0;
		
		TagList			tList;
		UIDRef			boxUIDRef;
		UID				textFrameUID;
		IIDXMLElement*	xmlPtr;
};
#endif