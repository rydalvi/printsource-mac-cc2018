//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/dynamicmenu/DynMnuDynamicMenu.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  For a dynamic menu example this class gets the UIDs of the spreads in the document and 
//  makes a menu entry for each one. Selecting that menu item causes action to be taken by 
//  the ActionComponent class.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IApplication.h"
#include "IDynamicMenu.h"
#include "IActionIDToUIDData.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IActionComponent.h"
#include "IDocument.h"
#include "ISpreadList.h"

// General includes:
#include "UIDList.h"
#include "ILayoutUIUtils.h" //cs4
#include "ActionDefs.h"
#include "CPMUnknown.h"
#include "TPLID.h"
#include "LanguageModelCache.h"
#include "IAppFramework.h"
#include "LanguageModel.h"
#include "TPLMediatorClass.h"

//Alert Macros.////////////
#include "CAlert.h"

inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("DynMnuDynamicMenu.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////

//#define CA(X) CAlert::InformationAlert(X)

/** DynMnuDynamicMenu
	implements the IDynamicMenu interface.  Builds the menu each time it is
	exposed.

	
*/

double localeId=-1;
double catalogId=-1;	

class DynMnuDynamicMenu : public CPMUnknown<IDynamicMenu>
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		DynMnuDynamicMenu(IPMUnknown *boss);

		/**
			Destructor
		*/
		~DynMnuDynamicMenu();

		/** 
			This method controls what menu items appear at any given time. InDesign calls this method
			when the menu is exposed and any menu entry in the MenuDef resource uses the kSDKDefIsDynamicMenuFlag.
			Control is exercised by rebuilding this action component's valid menus with the menu and action managers. 
			This example implementation builds the menus and action IDs based on the number of 
			spreads in the front document.
			@param dynamicActionID ID of menu hit just before this method is called.
			@param widget contains the widget that invoked this action. May be nil. e.g. Useful to use as a basis for Utils<IWidgetUtils>()->QueryRelatedWidget(...) to find widgets in a panel after its popup menu is hit.
		*/
		void RebuildMenu(ActionID dynamicActionID, IPMUnknown* widget);
		
		void addAttributes(InterfacePtr<IActionManager>&actionMgr,InterfacePtr<IMenuManager> &menuMgr,InterfacePtr<IActionComponent> bscMnuActComp,int32 &base,VectorLanguageModelPtr attrPtr, int32 MenuPos);
		


//	protected:

		/**
			Gets the UIDs for spreads 1..n in the front document. This method will always return
			a UIDList with length > 0, event though the [0] entry may be kInvalidUID.  It is the
			responsibility of the caller to insure that [0] is a valid UID and the Idatabase* is
			is not nil.
			@return List of front document spread UIDs
		*/
		//UIDList GetSpreadUIDs();
	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(DynMnuDynamicMenu, kDynMnuDynamicMenuImpl )


/* DynMnuDynamicMenu Constructor
*/
DynMnuDynamicMenu::DynMnuDynamicMenu(IPMUnknown *boss) : 
	CPMUnknown<IDynamicMenu>(boss) 
{
}


/* DynMnuDynamicMenu Destructor
*/
DynMnuDynamicMenu::~DynMnuDynamicMenu()
{
}

/* Rebuild Menu
*/
void DynMnuDynamicMenu::RebuildMenu(ActionID dynamicActionID, IPMUnknown* widget)
{
	//CA("Rebuild Menu");
  do{
		
		// Get the menu and action managers
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if (app == nil)
		{
			ASSERT_FAIL("Nil IApplication");
			break;
		}
		InterfacePtr<IActionManager> actionMgr(app->QueryActionManager());
		if (actionMgr == nil)
		{
			ASSERT_FAIL("Nil IActionManager");
			break;
		}
		InterfacePtr<IMenuManager> menuMgr(actionMgr,UseDefaultIID());
		if (menuMgr == nil)
		{
			ASSERT_FAIL("Nil IMenuManager");
			break;
		}
		InterfacePtr<IActionComponent> bscMnuActComp(this,UseDefaultIID());
		ASSERT(bscMnuActComp);
		if (!bscMnuActComp) {
			break;
		}

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			//CA("ptrIAppFramework is nil");		
			break;
		}

		bool16 result=ptrIAppFramework->getLoginStatus();
		if(!result){
			ptrIAppFramework->LogError("AP7_TemplateBuilder::DynMnuDynamicMenu::RebuildMenu::getLoginStatus returns kFalse");		
			break;
		}
		
		////////////////////////////remove whole menu/////////////////////////////////////////////
//	
		int32 totalAttributes=TPLMediatorClass::LanguageModelCacheSize;
		int32 OldNoofMenus = TPLMediatorClass::LanguageModelCacheSize;
		int32 ABC = TPLMediatorClass::cacheForLangModel->getTotalAttributes();

		if(totalAttributes>0)
		{
			for(int32 index=0;index<ABC;index++)
			{
				ActionID nextDynMenuID(kTPLDynMenuStartID+index);			
				CLanguageModel MenuLanguageModel;
				LanguageModelCache LangInCh;
				bool16 result = kFalse;
				result = TPLMediatorClass::cacheForLangModel->getLanguageModel(nextDynMenuID.Get(), MenuLanguageModel);
				if(result)
				{
					// Remove entry from action manager
					if (actionMgr->IsValidAction(nextDynMenuID)){
						
						// Remove the entry from the menu manager.  Menu path is same as used in menu def resource
						menuMgr->RemoveAllMenusForAction(nextDynMenuID);
						menuMgr->RemoveMenuItem(kTPLPluginInternalPopUP, nextDynMenuID);
						actionMgr->RemoveAction(nextDynMenuID);
						OldNoofMenus--;
					}
					else
					{	
						//CA("removing MenuPath");
						menuMgr->RemoveMenuItem(kTPLPluginInternalPopUP, nextDynMenuID);
						OldNoofMenus--;
					}
				}
				
			}
			//Mediator::cacheForStencil->clearStencilCache();
		}
		TPLMediatorClass::cacheForLangModel->clearLanguageModelCache();

		if(OldNoofMenus>0){
			//CA("Still Old Menus avalable");		
			break;
		}

		PMString LocaleName("");
		PMString CatalogName("");
		
        TPLMediatorClass::CurrLanguageID = ptrIAppFramework->getLocaleId();
		int32 base= 0;
		int32 totEntries=0;
		double MasterCatalogID = -1;		
		
		VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
		if(VectorLocValPtr == NULL)
		{
			ptrIAppFramework->LogError("AP7_TemplateBuilder::DynMnuDynamicMenu::RebuildMenu::StructureCache_getAllLanguages's VectorLocValPtr == NULL");
			break;
		}
			int SepMenupos = 15;
			SepMenupos = 15;

			if(VectorLocValPtr != NULL)
			{
				this->addAttributes(actionMgr,menuMgr,bscMnuActComp,base,VectorLocValPtr, SepMenupos);
				totEntries=static_cast<int32>(VectorLocValPtr->size());
				base +=totEntries;
				SepMenupos = SepMenupos+totEntries;

				if(TPLMediatorClass::CurrLanguageID == -1)
				{
					ptrIAppFramework->setLocaleId(VectorLocValPtr->at(0).getLanguageID());
					TPLMediatorClass::CurrLanguageID = VectorLocValPtr->at(0).getLanguageID();
				}
			}	
			TPLMediatorClass::LanguageModelCacheSize = static_cast<int32>(VectorLocValPtr->size());
			
	 }while(0);
	
}



void DynMnuDynamicMenu::addAttributes(InterfacePtr<IActionManager>&actionMgr,InterfacePtr<IMenuManager> &menuMgr,InterfacePtr<IActionComponent> bscMnuActComp,int32 &base,VectorLanguageModelPtr attrPtr, int32 MenuPos)
{
	int32 size = 0;
	if(attrPtr)
		size=static_cast<int32>(attrPtr->size());
	else{ 
		return;
	}

	VectorLanguageModel::iterator attrIterator;
	VectorLanguageModel::iterator startAttrIterator=attrPtr->begin();
	VectorLanguageModel::iterator endAttrIterator=attrPtr->end();
	int32 index=0; 
	for(attrIterator=startAttrIterator;attrIterator!=endAttrIterator;attrIterator++)
	{	
		CLanguageModel langModel;
		langModel.setLanguageID(attrIterator->getLanguageID());
		langModel.setLanguageName(attrIterator->getLangugeName());
		langModel.setAbbreviation(attrIterator->getAbbrevation());		
		
		ActionID newActionID(kTPLDynMenuStartID + base+index);
		actionMgr->AddAction(bscMnuActComp,			// ptr to IActionComponent to field menu hit
										newActionID,			// Action ID 
										langModel.getLangugeName(),				// Sub-menu string 
										kOtherActionArea,  //kMiscellaneousArea,		// Action area
										kNormalAction,			// Action type
										kCustomEnabling, //kCustomEnabling,		// Enabling type
										kInvalidInterfaceID,	// Selection IID	
										kFalse					// User editability
										);

		menuMgr->AddMenuItem(newActionID,					// Action ID
										kTPLPluginInternalPopUP,			// Menu path
										MenuPos,						// Menu position
										kSDKDefIsNotDynamicMenuFlag);	// Note we say it's not dynamic here.

		TPLMediatorClass::cacheForLangModel->addLanguageModel(newActionID.Get(),langModel);
		index++;
		MenuPos++;

		if(TPLMediatorClass::CurrLanguageID == attrIterator->getLanguageID())
		{
			//CA("Language ID Found");
			TPLMediatorClass::CurrentLangActionID = newActionID;
			TPLMediatorClass::CurrLanguageID = attrIterator->getLanguageID();
		}
	}
}



