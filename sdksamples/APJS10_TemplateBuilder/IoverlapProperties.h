#ifndef _IOVERLAPPROPERTIES__H_
#define _IOVERLAPPROPERTIES__H_
#include "TPLID.h"

#include "IPMUnknown.h"

class IoverlapProperties : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_IOVERLAPPROPERTIES };
		virtual bool16 IsOverlapable() = 0;
		virtual void setOverlapableFlag(bool16 flag)=0;
};

#endif	