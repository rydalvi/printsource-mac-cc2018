#include "VCPluginHeaders.h"
#include "IIDXMLElement.h"
#include "LanguageModelCache.h"

//Alert Macros.////////////
#include "CAlert.h"

inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("LanguageModelCache.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////

//#define NUM_TAGS_FIELDS 7

//using namespace std;

map<double,CLanguageModel>* LanguageModelCache::languageModelCache = NULL;

LanguageModelCache::LanguageModelCache()
{
	if(!languageModelCache)
		languageModelCache=new map<double,CLanguageModel>;
}

void LanguageModelCache::addLanguageModel(double id,CLanguageModel languageModel)
{
	if(!languageModelCache)
		languageModelCache=new map<double,CLanguageModel>;
	languageModelCache->insert(map<double, CLanguageModel>::value_type(id, languageModel));
}
bool16 LanguageModelCache::getLanguageModel(double id,CLanguageModel &languageModel)
{
	if(!languageModelCache)
	{
		languageModelCache=new map<double, CLanguageModel>;
		return kFalse;
	}

	map<double, CLanguageModel>::iterator mapIterator;

	mapIterator=languageModelCache->find(id);
	if(mapIterator==languageModelCache->end())
		return kFalse;

	languageModel=(*mapIterator).second;
	return kTrue;
}
void LanguageModelCache::clearLanguageModelCache()
{
	languageModelCache->clear();
	
}
int32 LanguageModelCache::getTotalAttributes()
{
	int32 totalAttributes=static_cast<int32>(languageModelCache->size());
	return totalAttributes;
}