#ifndef __LANGUAGEMODELCACHE_H__
#define __LANGUAGEMODELCACHE_H__

#include "VCPluginHeaders.h"
#include "IIDXMLElement.h"
#include "LanguageModel.h"
//#include "vector"
#include "map"

//#define NUM_TAGS_FIELDS 7

using namespace std;


class LanguageModelCache
{
private:
	static map<double,CLanguageModel> *languageModelCache;
	//StencilInfoCache *stencilInfoCache;
	
	
public:
	LanguageModelCache();
	void addLanguageModel(double id,CLanguageModel languageModel);
	bool16 getLanguageModel(double id,CLanguageModel &languageModel);
	static LanguageModelCache* getLanguageModelCache();
	void clearLanguageModelCache();
	int32 getTotalAttributes();
	
};
#endif