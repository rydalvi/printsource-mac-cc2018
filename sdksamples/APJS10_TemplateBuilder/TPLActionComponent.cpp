//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "TPLID.h"
#include "IActionStateList.h"
#include "IAppFramework.h"
#include "IApplication.h"
//#include "IPaletteMgr.h"             //CS3 Change it is removed from API
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "TPLMediatorClass.h"
#include "SDKListBoxHelper.h"
#include "ITextControlData.h"
#include "TPLSelectionObserver.h"
#include "TPLActionComponent.h"
//#include "UIDList.h"
//#include <UIDRef.h>
//#include "IMessageServer.h"
#include "TablesUIID.h"
//#include "ITextFrame.h"                     //It is removed from CS3 API
#include "ITextMiscellanySuite.h"
#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "TblBscSuiteTextCSB.h"
#include "ICategoryBrowser.h"
#include "ILoginHelper.h"
#include "ITblBscSuite.h"
#include "ITagReader.h"
//-------------CS3 Addition--------------------//
#include "PaletteRefUtils.h"
#include "SnapshotUtils.h"

//#include "IMenuManager.h"

//Alert Macros.////////////
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("TPLActionComponent.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}

#define CA_New(X) CAlert::InformationAlert(X) 
	
//end Alert Macros./////////////

int32 Flag1;
extern bool16 IsRefresh;
extern int32 SelectedRowNo;
bool16 ISTabbedText = kTrue;
bool16 isAddTableHeader = kFalse;
bool16 isAddAsDisplayName = kFalse;
bool16 isAddImageDescription = kFalse;
bool16 isAddListName = kFalse;
bool16 isOutputAsSwatch = kFalse;

bool16 isComponentAttr = kFalse;
bool16 isXRefAttr = kFalse;
bool16 isAccessoryAttr = kFalse;
bool16 isMMYAttr = kFalse;
bool16 isMMYSortAttr = kFalse;

bool16 isTagAttr = kFalse;

extern int32 flag_1;


bool16 FunctionAccess_MMY = kFalse;


 // Default for English
//CREATE_PMINTERFACE(TPLActionComponent, kTPLActionComponentImpl)

//IPaletteMgr* TPLActionComponent::palettePanelPtr=0;
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup ap7_templatebuilder

*/
//class TPLActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		TPLActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(TPLActionComponent, kTPLActionComponentImpl)

/* TPLActionComponent Constructor
*/
TPLActionComponent::TPLActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{

}

TPLActionComponent::~TPLActionComponent()
{
	//CAlert::InformationAlert(__FUNCTION__);
}

/* DoAction
*/
void TPLActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		//CA("No ptrIAppFramework");
		return;
	}
	
	switch (actionID.Get())
	{

		//case kTPLPopupAboutThisActionID:
		//case kTPLAboutActionID:
		//{
		//	this->DoAbout();
		//	break;
		////}
	//case kConvertTextActionID:
	case kConvertTableToTextActionID:
		{
			//CA("my dialog box");
			do
			{  //do 1
					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{	
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iSelectionManager == nil");
						break;
					}
					InterfacePtr<ITblBscSuite> iTblBscSuite(static_cast<ITblBscSuite* >(Utils<ISelectionUtils>()->QuerySuite(ITblBscSuite::kDefaultIID,iSelectionManager)));
					if(iTblBscSuite == nil)
						{
							ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iTblBscSuite == nil");
							break;
						}

					
					InterfacePtr<ITableModel> tblMdl;
					if(iTblBscSuite->queryTableModel(tblMdl) == kFalse)
						{
							ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::tblMdl == nil");
						break;
						}
					
						
					/*
					InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
					( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
					if(!txtMisSuite)
					{
						CA("txtMisSuite == nil");
						break; 
					}
					UIDRef frameUIDRef(UIDRef::gNull);
					txtMisSuite->GetFrameUIDRef(frameUIDRef);

					UID textFrameUID = frameUIDRef.GetUID();

					InterfacePtr<ITextFrame> textFrame(frameUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
					if (textFrame == nil)
					{
						CA("textFrame == nil");
						break;
					}			
					InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
					if (textModel == nil)
					{
						//CA("textModel == nil");
						break;
					}
				*/
					InterfacePtr<IXMLReferenceData> xmlRefData(tblMdl,UseDefaultIID());
					if(xmlRefData != nil)
					{
						do
						{//do 2 
							XMLReference xmlRef = xmlRefData->GetReference();
							//IIDXMLElement * xmlTagPtr = xmlRef.Instantiate();
							InterfacePtr<IIDXMLElement>xmlTagPtr(xmlRef.Instantiate());
							if(xmlTagPtr == nil)
							{
								ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::xmlTagPtr == nil");
								break;
							}
							XMLReference parentXMLRef = xmlTagPtr->GetParent();
							//IIDXMLElement * parentXMLElementPtr = parentXMLRef.Instantiate();
							InterfacePtr<IIDXMLElement>parentXMLElementPtr(parentXMLRef.Instantiate());
							if(parentXMLElementPtr == nil)
								{
									ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::parentXMLElementPtr == nil");
									break;
								}
							PMString printSourceTag = parentXMLElementPtr->GetTagString();
							//CA(printSourceTag);
							if((printSourceTag == "PRINTsource") || (printSourceTag == "Items"))
							{
								int16 userChoice = CAlert::ModalAlert
									(
										"This operation will break the existing tag structure.\n"
										"Do you want to continue?",								
										"Yes","No",kNullString,2,CAlert::eWarningIcon
									);
								if(userChoice == 1){
									ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::userChoice == 1");								
									break;
								}
								else if(userChoice == 2){
									ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::userChoice ==2");
									return;
								}

							}
						}while(kFalse);//end do 2
					}
			}while(kFalse);	//end do 1


			IActionComponent * fOrigActionComponent = CreateObject2<IActionComponent>(kTablesUIMenuActionBoss);
			fOrigActionComponent->AddRef();
			fOrigActionComponent->DoAction
				(
				ac, actionID, mousePoint, widget
				);
			fOrigActionComponent->Release();
			break;
		}
					
		case kTPLPanelPSMenuActionID:
		case kTPLPanelWidgetActionID:
			//CA("kTPLPanelWidgetActionID");
			Flag1 = 1;
			this->DoPalette();
			break;


		case kTPLAddAsCompAttWidgetID:
		{
			isComponentAttr = !isComponentAttr;
			break;
		}
		case kTPLAddAsXRefAttWidgetID:
		{
			isXRefAttr = !isXRefAttr;
			break;
		}
		case kTPLAddAsAccessoryAttWidgetID:
		{
			isAccessoryAttr = !isAccessoryAttr;
			break;
		}

		case kTPLAddAsMMYAttWidgetID:
		{
			isMMYAttr = !isMMYAttr;
			break;
		}

		
		case kTPLAddAsMMYSortAttWidgetID:
		{
			isMMYSortAttr = !isMMYSortAttr;
			break;
		}

//***added by shinde 

		case kTPLTagInfoWidgetID:
		{
			bool16 isFrameTaggedFlag=kFalse;
			UIDRef overlapBoxUIDRef;
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{
				//CA("ISelectionManager");
				break;
			}
			
			UIDRef ref;

			InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(/*ITextMiscellanySuite::*/IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
		
		
			if(!txtSelectionSuite)
			{
				CA_New("Please set text cursor in desired field tag");
			}
			else
			{			
				//TPLCommonFunctions tpl;	
//We are passing UIDRef ref as an out parameter to	GetFrameUIDRef() method.
//following method will return kTrue or kFalse depending upon whether that
//frame is TextFrame or not.
				
				bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);					
				if(	ISTextFrame)
				{	
//CA("ISTextFrame");					

//Following function i.e.GetCaretPosition(start) gives current cursor position
//inside the frame. We are passing start as an out parameter to this method.
//Before that we are initializing it to '0'.
					int32 start=0;
					bool16 carat=txtSelectionSuite->GetCaretPosition(start);
					if(carat)
					{
						//CA("return true");
						PMString ll="";
						ll.AppendNumber(start);
						//CA("CaretPosition is ::  ");
						//CA(ll);

						//TPLTagReader tagReader;
						InterfacePtr<ITagReader> iTagReaderPtr
								(static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,ITagReader::kDefaultIID)));
						if(!iTagReaderPtr)
							break;
						TagList taglis=iTagReaderPtr->getTagsFromBox(ref);
						if(taglis.size()<=0)
						{
							// the overlapped box is not our dragged box
							ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLCommonFunctions::checkIfFrameIsTagged::tagList's size is 0");
							break;

						}					
						for(int32 tagIndex = 0 ; tagIndex < taglis.size() ; tagIndex++)
						{
							if(taglis[tagIndex].startIndex <= start && taglis[tagIndex].endIndex >= start)
							{
								PMString temp;

								temp.Append("field Name = ");
								temp.Append(taglis[tagIndex].tagPtr->GetTagString());

								temp.Append("\n");
								temp.Append("\n Include Header/ Display Name = ");
								if(taglis[tagIndex].header == -1)
								{
									temp.Append("OFF");
								}
								else
								{
									temp.Append("ON");
								}
							
								temp.Append("\n EventField = ");
								if(!taglis[tagIndex].isEventField)
								{
									temp.Append("OFF");
								}
								else
								{
									temp.Append("ON");
								}
								
								temp.Append("\n Delete If Empty = ");
								if(taglis[tagIndex].deleteIfEmpty ==-1)
								{
									temp.Append("OFF");
								}
								else
								{
									temp.Append("ON");
								}
								
								if(taglis[tagIndex].isAutoResize ==-1)
								{
									temp.Append("\n Auto Resize = ");
									temp.Append("OFF");
									temp.Append("\n Overflow = ");
									temp.Append("OFF");
								}
								if(taglis[tagIndex].isAutoResize == 1)
								{
									temp.Append("\n Auto Resize = ");
									temp.Append("ON");
									temp.Append("\n Overflow = ");
									temp.Append("OFF");
								}
								else
								{
									temp.Append("\n Auto Resize = ");
									temp.Append("OFF");
									temp.Append("\n Overflow = ");
									temp.Append("ON");
								}
								
								temp.Append("\n SprayItemPerFrame = ");
								if(taglis[tagIndex].isSprayItemPerFrame == -1)
								{
									temp.Append("OFF");
								}
								else
								{
									temp.Append("ON");
								}
								
								temp.Append("\n Item List = ");
								if(taglis[tagIndex].childTag == 1)
								{
									temp.Append("ON");
								}
								else
								{
									temp.Append("OFF");
								}
								temp.SetTranslatable(kFalse);
								CA_New(temp);
								break;
							}
						}
					}
					else
					{
						PMString warningText("Please set text cursor in desired field tag");
						warningText.SetTranslatable(kFalse);
						CA_New(warningText);
					}
				}
				else
				{
					PMString warningText("Please set text cursor in desired field tag");
						warningText.SetTranslatable(kFalse);
						CA_New(warningText);
				}
			}
			break;
		}

/////	End
		case kTPLShowCategoryTreeID:
		{
			//-----------------------New CS3 Addition-------------------------------------------------//
			//CA("Inside Show Category.");
			
			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil){ 
				CA("No iApplication");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iApplication==nil");
				break;
			}
			//CS3 change
			/*InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
			if(iPaletteMgr==nil){ 
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iPaletteMgr==nil");
				break;
			}*/
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
			if(iPanelMgr == nil){ 
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iPanelMgr == nil");
				break;
			}

			IControlView* pnlControlView = iPanelMgr->GetPanelFromWidgetID(kTPLPanelWidgetID);
			if(pnlControlView == NULL)
			{
				CA("pnlControlView is NULL");
				break;
			}
			UID paletteUID = kInvalidUID;
			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;	
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;	
			const ActionID MyPalleteActionID = kTPLPanelWidgetActionID;
			
				//paletteUID = iPanelMgr->GetPaletteUID(MyPalleteActionID);
				//IWindow* palette = nil;
				//IDataBase* db = ::GetDataBase(gSession);
				//if (db == nil)
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::db == nil");				
				//	break;
				//}
				//palette = static_cast<IWindow*>(db->Instantiate(paletteUID, IWindow::kDefaultIID));
				//if (palette == nil)
				//{
				//	//CA("could not query palette based on given UID!");
				//	break;
				//}

//-------------0n 9/07/07-----------------------------------//
			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}
			InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			if(palette == NULL)
			{
				//CA("palette is NULL");
				break;
			}

		//---------------------end-----------------------------------//

		////IWindow* palette = nil;
		//IDataBase* db = ::GetDataBase(gSession);
		//if (db == nil)
		//{
		//	CA("db is NULL");
		//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::db == nil");				
		//	break;
		//}
		//palette = static_cast<IWindow*>(db->Instantiate(panelUID, IWindow::kDefaultIID));
		//if (palette == nil)
		//{
		//	CA("could not query palette based on given UID!");
		//	break;
		//}
//----------------------------------


		
			/*PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(pnlControlView);

			if(!palRef.IsValid())
			{
				CA("IsInValid ");			}
		
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);*/
	
		
			if(palette)
			{
					
				//CA("pallete found ");
				//palette->AddRef();
				//GSysRect PalleteBounds(/*(SysCoord)0, (SysCoord)0, (SysCoord)200, (SysCoord)150*/);
				//GSysRect PalleteBounds = palette->GetFrameBBox();
			
				/*TemplateTop		= PalleteBounds.top -30;
				TemplateLeft	= PalleteBounds.left;
				TemplateRight	= PalleteBounds.right;
				TemplateBottom	= PalleteBounds.bottom;*/

				TemplateTop	=0;
				TemplateLeft = 0;	
				TemplateRight =150;	
				TemplateBottom	=300;

				PMString dimension("Dimension::");
				dimension.AppendNumber(TemplateTop);
				dimension.Append("*****");
				dimension.AppendNumber(TemplateLeft);
				dimension.Append("*****");
				dimension.AppendNumber(TemplateRight);
				dimension.Append("*****");
				dimension.AppendNumber(TemplateBottom);
				dimension.Append("*****");
					//CA(dimension);

				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointre to TemplateBuilderPtr not found");
					return ;
				}

				CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight ,2 ,TPLMediatorClass::currTempletSelectedClassID);
			}
			

			break;
		}
	//end table header	

		default:
		{
			InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if (app == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Nil IApplication");
				break;
			}
			InterfacePtr<IActionManager> actionMgr(app->QueryActionManager());
			if (actionMgr == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Nil IActionManager");
				break;
			}			

			CLanguageModel LanguageModelInfo;
			bool16 result = kFalse;
			LanguageModelCache LangInCh;
			result = LangInCh.getLanguageModel(actionID.Get(), LanguageModelInfo);
			if(result == kFalse){
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::getLanguageModel returns kFalse");
				break;
			}

			TPLMediatorClass::CurrentLanguageModel = LanguageModelInfo;
			TPLMediatorClass::CurrLanguageID = LanguageModelInfo.getLanguageID();
			//CA(LanguageModelInfo.getLangugeName());
			TPLSelectionObserver SelObs(this);
			TPLMediatorClass::loadData=kTrue;
			//IsRefresh = kTrue;
			Flag1 =1;
			SelObs.loadPaletteData();
			//IsRefresh = kFalse;

			break;
		}
	}
}

/* DoAbout
*/
void TPLActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kTPLAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

void TPLActionComponent::DoItemOne()
{
	CAlert::InformationAlert(kTPLItemOneStringKey);
}


void TPLActionComponent::UpdateActionStates (IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{	
	//CA("TPLActionComponent::UpdateActionStates");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	

	bool16 result=ptrIAppFramework->getLoginStatus();

	InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::UpdateActionStates::Pointer to ptrLogInHelper is nil.");
		return  ;
	}	

	LoginInfoValue cserverInfoValue;
	bool16 resultValue = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
	if(resultValue)
	{
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
		if(clientInfoObj.getisItemTablesasTabbedText() == 0)
			ISTabbedText = kFalse;
		else
			ISTabbedText = kTrue;
		if(clientInfoObj.getisAddTableHeaders() == 0)
			isAddTableHeader = kFalse;
		else
			isAddTableHeader = kTrue;
	}


	for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	{
		ActionID actionID = iListPtr->GetNthAction(iter);
		if(actionID == TPLMediatorClass::CurrentLangActionID)
		{	
			if(result)
			{
				iListPtr->SetNthActionState(iter,kEnabledAction|kSelectedAction);
		//		iListPtr->SetNthActionState(iter,kSelectedAction);
			}			
		}
		else
			iListPtr->SetNthActionState(iter,kEnabledAction);

		
		switch(actionID.Get())
		{ 	

/////////////////////////////////////////////////////////
			//case kConvertTextActionID:
			case kConvertTableToTextActionID:
			{
				//CA("my dialog box");
				IActionComponent * fOrigActionComponent = CreateObject2<IActionComponent>(kTablesUIMenuActionBoss);
				fOrigActionComponent->AddRef();//CA("376 TPLActionComponent");
				fOrigActionComponent->UpdateActionStates
					(
					ac, iListPtr, mousePoint, widget
					);
				fOrigActionComponent->Release();
				break;
			}
/////////////////////////////////////////////////////////
			case kTPLPanelPSMenuActionID:
			case kTPLPanelWidgetActionID:
			{
				//PMString string("Setting ActionState", -1, PMString::kNoTranslate);
				//CAlert::InformationAlert(string);
				if(result)
				{
					InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
					ASSERT(app);
					if(!app)
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::UpdateActionStates::!app");
						return;
					}
					/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::UpdateActionStates::!paletteMgr");
						return;
					}*/
					InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager()/*, UseDefaultIID()*/);
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::UpdateActionStates::!panelMgr");
						return;
					}
					
					if(panelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID))
					{
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
						iListPtr->SetNthActionState(iter,kEnabledAction);
					}

				}
				else
					{
						iListPtr->SetNthActionState(iter,kDisabled_Unselected);
					}	
				break;
			}
			
//			case kTPLAddTabbedTextID:
//			{	
//				if(result )
//				{
//					if(ISTabbedText)
//						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
//					else
//						iListPtr->SetNthActionState(iter,kEnabledAction);
//				}
//				else
//				{
//					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
//				}
//				break;
//			}
//			//add table header
//			case kTPLAddTableHeaderID:
//			{	
//				if(result )
//				{
//					if(isAddTableHeader)
//						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
//					else
//						iListPtr->SetNthActionState(iter,kEnabledAction);
//				}
//				else
//				{
//					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
//				}
//				break;
//			}
//			//end table header
//////		Added by Amit on 12/9/07 
//			case kTPLAddAsDisplayNameID:
//			{	
//				if(result )
//				{
//					if(isAddAsDisplayName)
//						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
//					else
//						iListPtr->SetNthActionState(iter,kEnabledAction);
//				}
//				else
//				{
//					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
//				}
//				break;
//			}
//			case kTPLAddImageDescriptionID:
//			{	
//				if(result )
//				{
//					if(isAddImageDescription)
//						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
//					else
//						iListPtr->SetNthActionState(iter,kEnabledAction);
//				}
//				else
//				{
//					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
//				}
//				break;
//			}

			case kTPLAddAsCompAttWidgetID:
			{	


	/*			if(ptrIAppFramework->ConfigCache_getDisplayItemComponents())
				{
					iChkboxCntrlViewTabbedTextCMP->Show();
					InterfacePtr<ITriStateControlData> itristatecontroldataCMPAttr(iChkboxCntrlViewTabbedTextCMP, UseDefaultIID());
					if(itristatecontroldataCMPAttr==nil) 
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldataCMPAttr == nil");
						return;
					}
					if(isComponentAttr )
						itristatecontroldataCMPAttr->Select();
					else
						itristatecontroldataCMPAttr->Deselect();
				}
				else
					iChkboxCntrlViewTabbedTextCMP->Hide();

*/
				if(result /*&& ptrIAppFramework->ConfigCache_getDisplayItemComponents()*/ )
				{
					if(isComponentAttr)
							iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
						else
							iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}
			
			case kTPLAddAsXRefAttWidgetID:
			{	
				if(result /*&& ptrIAppFramework->ConfigCache_getDisplayItemXref()*/)
				{
					if(isXRefAttr)
						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}
			
			case kTPLAddAsAccessoryAttWidgetID:
			{	
				if(result /*&& ptrIAppFramework->ConfigCache_getDisplayItemAccessory()*/)
				{
					if(isAccessoryAttr)
						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}
////	End
			case kTPLShowCategoryTreeID:
			{	
				if(result )
				{
					if(SelectedRowNo == 4)
						iListPtr->SetNthActionState(iter,kEnabledAction);
					else
						iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}

			case kTPLAddAsMMYAttWidgetID:
			{
				//CA("kTPLAddAsMMYAttWidgetID");
				if(result && FunctionAccess_MMY)
				{
					if(isMMYAttr)
						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}
			
			case kTPLAddAsMMYSortAttWidgetID:
			{
				//CA("kTPLAddAsMMYSortAttWidgetID");
				if(result && FunctionAccess_MMY)
				{
					if(isMMYSortAttr)
						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}

			//Added by shinde
			case kTPLTagInfoWidgetID:
			{
				if(result)
				{
					if(isTagAttr)
						iListPtr->SetNthActionState(iter,kEnabledAction| kSelectedAction);
					else
						iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else
				{
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);

				}
				break;
			}
		}		
	}
	
}

void TPLActionComponent::DoPalette()
{
	//CA("DoPalette");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	

	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");		
			break;
		}
		/*InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
			break;*/
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");	
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		// Here we are opening our Template Palette
		// [[[[[[[[ IMP Function ]]]]]]]]
	//	CA("DP1");
		iPanelMgr->ShowPanelByMenuID(kTPLPanelWidgetActionID);
		/*iPanelMgr->ShowPanelByWidgetID(kTPLPanelWidgetID);
		iPanelMgr->SetPanelResizabilityByWidgetID(kTPLPanelWidgetID,kTrue);*/
			flag_1 = ptrIAppFramework->getLoginStatus(); /*AP*/
		
		if(!IsRefresh && flag_1 == 1)
		{		
			//CA("!IsRefresh && Flag1 ==1");
			//FunctionAccess_MMY = ptrIAppFramework->GetConfig_getFunctionAccessByFunctionCode("MMY");






			if(iPanelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID))
			{
				flag_1++;
				//CA("iPanelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID)");
				IControlView* icontrol = iPanelMgr->GetVisiblePanel(kTPLPanelWidgetID);
				if(!icontrol)
				{
					//CA("!icontrol");
					return;
				}
				//icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));//15-01-08		Amit
			}
			
			
			//icontrol->Resize();
			
		}

//		TPLActionComponent::palettePanelPtr=iPaletteMgr;
	}while(kFalse);
}

void TPLActionComponent::CloseTemplatePalette(void)
{
	//CA("TPLActionComponent::CloseTemplatePalette");
	do
	{	
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			return;
		}
		
		//DoPalette();
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::CloseTemplatePalette::iApplication is nil");		
			break;
		}

		/*InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil){
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::CloseTemplatte::iPaletteMge is null");		
			break;
		}*/

		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		//TPLMediatorClass::iPanelCntrlDataPtr = iPanelMgr;

		if(TPLMediatorClass::iPanelCntrlDataPtr== nil){
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::CloseTemplatePalette::iPanelControlData is null");
			break;
		}

		/* Clear all listboxes and disable all controls */
		
		// MAke comment Awasthi
		/*IControlView* dropdownCtrlView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLDropDownWidgetID);
		
		if(!dropdownCtrlView)
			break;
		
		IControlView* refreshBtnCtrlView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLRefreshIconSuiteWidgetID);
		if(!refreshBtnCtrlView)
			break;
		
		IControlView* appendRadCtrlView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
		if(!appendRadCtrlView)
			break;
		
		IControlView* embedRadCtrlView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
		if(!embedRadCtrlView)
			break;
		
		IControlView* tagFrameChkboxCtrlView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
		if(!tagFrameChkboxCtrlView)
			break;
		*/

		// End making Commments Awasthi
	//	IControlView* classTreeCtrlView=
	//		TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLClassTreeIconSuiteWidgetID);
	//	if(!classTreeCtrlView)
	//		break;

		if(TPLMediatorClass::dropdownCtrlView1 == nil){
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponents::CloseTemplatePalette::dropdownCtrlView1 is nil");
			break;
		}
		TPLMediatorClass::dropdownCtrlView1->Disable();
		TPLMediatorClass::refreshBtnCtrlView1->Disable();
		TPLMediatorClass::appendRadCtrlView1->Disable();
		TPLMediatorClass::embedRadCtrlView1->Disable();
		TPLMediatorClass::tagFrameChkboxCtrlView1->Disable();
	//	classTreeCtrlView->Disable();
		
		//SDKListBoxHelper listHelper(this, kTPLPluginID);
		
		/*listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 1);
		listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 2);
		listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 3);
		listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 4);
		listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 5);
		*/
		if(TPLMediatorClass::ProjectLstboxCntrlView== nil){
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::CloseTemplatePalette::ProjectLstboxCntrlView is nil");
			break;
		}

	/*TPLMediatorClass::PFLstboxCntrlView->Hide();
		TPLMediatorClass::PGLstboxCntrlView->Hide();
		TPLMediatorClass::PRLstboxCntrlView->Hide();
		TPLMediatorClass::ItemLstboxCntrlView->Hide();*/
		TPLMediatorClass::ProjectLstboxCntrlView->HideView();

		/* For class tree string text contrl */
/*		IControlView* textView=
			TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kClassHierarchyStaticTxtWidgetID);
		if(textView==nil)
			break;

		InterfacePtr<ITextControlData> iData(textView, UseDefaultIID());
		if(iData==nil)
			break;
		iData->SetString("");
*/
		// Awasthi

		//if(iPaletteMgr)//TPLActionComponent::palettePanelPtr)
		//{ 
		
			//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr/*TPLActionComponent::palettePanelPtr*/, UseDefaultIID()); 
			//if(iPanelMgr == nil)
			//{
			//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::CloseTemplatePalette::iPanelMgr is NULL");			
			//	break;
			//}
		
			if(iPanelMgr->IsPanelWithMenuIDShown (kTPLPanelWidgetActionID))
			{				
				iPanelMgr->HidePanelByMenuID(kTPLPanelWidgetActionID);				
			}

		//}
		
	}while(kFalse);
	
}
