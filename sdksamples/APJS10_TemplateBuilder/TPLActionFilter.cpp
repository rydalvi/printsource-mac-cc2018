//========================================================================================
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or
//  distribution of it requires the prior written permission of Adobe.
//
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActionFilter.h"

// General includes:
#include "ActionDefs.h" // for kCustomEnabling
#include "CPMUnknown.h"
#include "CAlert.h"

// Project includes:
#include "TPLID.h"
#include "TablesUIID.h"

//Alert Macros.////////////
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("TPLActionFilter.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////

class TPLActionFilter : public CPMUnknown<IActionFilter>
{
public:
    /** Constructor.
        @param boss interface ptr from boss object on which this interface is aggregated.
    */
    TPLActionFilter(IPMUnknown* boss);

    /** Destructor.
    */
    virtual ~TPLActionFilter(void)
    {
    }

    /** FilterAction
        @see IActionFilter
    */
    virtual void FilterAction(ClassID* componentClass,
                              ActionID* actionID,
                              PMString* actionName,
                              PMString* actionArea,
                              int16* actionType,
                              uint32* enablingType,
                              bool16* userEditable);

};

/* CREATE_PMINTERFACE
Binds the C++ implementation class onto its
ImplementationID making the C++ code callable by the
application.
*/
CREATE_PMINTERFACE(TPLActionFilter, kTPLActionFilterImpl)


/* Constructor
*/
TPLActionFilter::TPLActionFilter(IPMUnknown* boss)
    : CPMUnknown<IActionFilter>(boss)
{
    // does nothing.
}


/* FilterAction
*/
void TPLActionFilter::FilterAction(ClassID* componentClass, ActionID* actionID,
                                       PMString* actionName, PMString* actionArea,
                                       int16* actionType, uint32* enablingType,
                                       bool16* userEditable)
{
	// The panel widget (by way of the PanelList resrouce declaration in the .fr file)
	// is added to the action manager with the enablingType
	// as kDisableIfLowMem | kCustomEnabling so we are using this action filter
	// to set the componentClass to our action component to allow us to enable
	// and disable the menu item for this panel on the Window menu.

	if(actionID->Get() == kTPLPanelWidgetActionID || actionID->Get() == kConvertTableToTextActionID)//|| actionID->Get() == kConvertTextActionID 
	{		
		*componentClass = kTPLActionComponentBoss;
		//PMString string("Set to customEnabling", -1, PMString::kNoTranslate);
		//CAlert::InformationAlert(string);
	}
    
}
