#ifndef __TPLCOMMONFUNCTIONS_H__
#define __TPLCOMMONFUNCTIONS_H__

#include "VCPlugInHeaders.h"
#include "IIDXMLElement.h"
#include "IXMLreferenceData.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "TPLListboxData.h"
#include "ITableModel.h"

class TPLCommonFunctions
{
public:
	int16 deleteThisBox(UIDRef);
	int16 convertBoxToTextBox(UIDRef);
	XMLReference TagFrameElement(const XMLReference&,UID,const PMString&);
	PMString prepareTagName(PMString);
	PMString RemoveWhiteSpace(PMString);
	void attachAttributes(XMLReference*,int32,int32);
	void addTagToGraphicFrame(UIDRef,int32,int32);
	void addTagToText(UIDRef,int32,int32,PMString);
	int16 appendTextIntoBox(UIDRef,PMString,bool16);
	int16 embedBoxIntoBox(UIDRef,UIDRef );
	PBPMPoint GetActiveReferencePoint(void);
	int16 getSelectionFromLayout(UIDRef&);
	void insertOrAppend(int32,PMString,int32,int32);
	int16 getBoxDimensions(UIDRef,PMRect&);
	int16 checkForOverLaps(UIDRef,int32,UIDRef&,UIDList&);
	int16 refreshLstbox(UID);
	void updateIcon(IControlView*,int32,bool16);
	int16 createNewTable(void);
	int changeMode(int whichMode);
	ITool* queryTool(const ClassID& toolClass);	
	int16 appendNewTable(UIDRef);
	int16 embedOrAppendTable(int16,int32,int32);
	int16 getTableUIDOfSelBox(UIDRef &selBoxUIDRef);
	bool16 getFrameTagChkboxState();
	bool16 checkIfFrameIsTagged();
	bool16 getAutoResizeChkboxState();
	bool16 getOverflowChkboxState();

	UIDRef AcquireTag(const UIDRef& documentUIDRef,const PMString& tagName, bool16 &ISTagPresent);
	ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef& BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference, TPLListboxInfo&, int32 startTextIndex);
	//overloaded TagTable function
	ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef& BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference, TPLListboxInfo&);
	
	
	ErrorCode TagStory(const PMString& tagName,
									const UIDRef& textModelUIDRef);
	ErrorCode TagCurrentCell(const XMLReference& xmlRef);
	ErrorCode AddTableAndCellElements(const UIDRef& tableModelUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								int32 listBoxType          //////////  Added on 14/08/2006  ////////////
								);
	ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,
								XMLReference &parentXMLRef, GridID id,
								GridArea gridArea, XMLReference &CellTextxmlRef, TPLListboxInfo&, int32 startTextIndex);
	//Overloaded TagTableCellText function
	ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,
								XMLReference &parentXMLRef, GridID id,
								GridArea gridArea, XMLReference &CellTextxmlRef, TPLListboxInfo&);


	void attachAttributes(XMLReference* newTag,bool16 IsPrintSourceTag, TPLListboxInfo lstboxInfo,double rowno, double colno);

	PMString keepOnlyAlphaNumeric(PMString name);

	//added by vijay choudhari on 19-4-2006///////////////////////
	void insertOrAppendOnDoubleClick(int32,PMString,int32);
	
	
    //added by vijay choudhari on 24-4-2006//////////////////////
	void addTagToTextDoubleClickVersion
(UIDRef curBox,int32 selIndex,int32 selTab,PMString result);

	//added by vijay choudhari on 5-5-2006//////////////////////
	void appendTextIntoSelectedAndOverlappedBox(UIDRef,PMString);

	//added by vijay choudhari on 31-5-2006//////////////////////
	void checkForOverlapWithTextFrame(PMPoint);

	//added by vijay choudhari on 1-8-2006//////////////////////
	bool16 checkForOverlapWithMasterFrameFunction(UIDRef);

	void addTagToPVBoxDoubleClickVersion(UIDRef curBox,int32 selIndex,int32 selTab,PMString result);
	//added by vijay choudhari on 7-11-2006//////////////////////
	ErrorCode CreateTable(const UIDRef& storyRef, 
										const TextIndex at,
										  const int32 numRows,
										  const int32 numCols,
										  const PMReal rowHeight,
                                          const PMReal colWidth,
										  const CellType cellType = kTextContentType);
	//added by vijay choudhari on 10-11-2006//////////////////////
	bool16 InsertTableInsideTableCellAndApplyTag(TPLListboxInfo lstboxInfo,UIDRef ref);

	void appendTableNameAndTable(int32 selectedRowIndex, PMString theContent, int32 whichTab);

};




#endif