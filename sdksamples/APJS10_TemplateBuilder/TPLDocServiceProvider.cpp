#include "VCPlugInHeaders.h"
#include "CServiceProvider.h"
#include "K2Vector.h"
#include "DocumentID.h"
#include "TPLID.h"

class TPLDocServiceProvider : public CServiceProvider
{
	public:

		/**
			Constructor initializes a list of service IDs, one for each file action signal that DocWchResponder will handle.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		TPLDocServiceProvider(IPMUnknown* boss);
		
		/**
			Destructor.  
		*/
		virtual	~TPLDocServiceProvider();

		/**
			GetName initializes the name of the service.
			@param pName Ptr to PMString to receive the name.
		*/
		virtual void GetName(PMString* pName);

		/**
			GetServiceID returns a single service ID.  This is required, even though
			GetServiceIDs() will return the complete list initialized in the constructor.
			This method just returns the first service ID in the list.
		*/
		virtual ServiceID GetServiceID();

		/**
			IsDefaultServiceProvider tells the application this service is not the default service.
		*/
		virtual bool16 IsDefaultServiceProvider();
		
		/**
			GetInstantiationPolicy returns a InstancePerX value to indicate that only
			one instance per session is needed.
		*/
		virtual InstancePerX GetInstantiationPolicy();

		/**
			HasMultipleIDs returns kTrue in order to force a call to GetServiceIDs().
		*/
		virtual bool16 HasMultipleIDs() const;

		/**
			GetServiceIDs returns a list of services provided.
			@param serviceIDs List of IDs describing the services that TPLDocServiceProvider registers to handle.
		*/
		virtual void GetServiceIDs(K2Vector<ServiceID>& serviceIDs);

	private:

		K2Vector<ServiceID> fSupportedServiceIDs;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(TPLDocServiceProvider, kTPLDocServiceProviderImpl)


/* TPLDocServiceProvider Constructor
*/
TPLDocServiceProvider::TPLDocServiceProvider(IPMUnknown* boss)
	: CServiceProvider(boss)
{
	// Add the service IDs we want the associated responder to handle.
	//  (See DocumentID.h)
	fSupportedServiceIDs.clear();
	
	//	NewDoc
//	fSupportedServiceIDs.Append(kAfterNewDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterNewDocSignalResponderService);

	//	OpenDoc
	//fSupportedServiceIDs.Append(kDuringOpenDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringOpenDocSignalResponderService);

	//	CloseDoc
	//fSupportedServiceIDs.Append(kBeforeCloseDocSignalResponderService);
	fSupportedServiceIDs.push_back(kBeforeCloseDocSignalResponderService);

	if (fSupportedServiceIDs.size()<=0)
	{
		ASSERT_FAIL("TPLDocServiceProvider must support at least 1 service ID");
		//fSupportedServiceIDs.Append(kInvalidService);
		fSupportedServiceIDs.push_back(kInvalidService);
	}

}

/* TPLDocServiceProvider Dtor
*/
TPLDocServiceProvider::~TPLDocServiceProvider()
{
}

/* TPLDocServiceProvider::GetName
*/
void TPLDocServiceProvider::GetName(PMString* pName)
{
	pName->SetCString("PstLst Responder Service");
}

/* TPLDocServiceProvider::GetServiceID
*/
ServiceID TPLDocServiceProvider::GetServiceID() 
{
	// Should never be called given that HasMultipleIDs() returns kTrue.
	return fSupportedServiceIDs[0];
}

/* TPLDocServiceProvider::IsDefaultServiceProvider
*/
bool16 TPLDocServiceProvider::IsDefaultServiceProvider()
{
	return kFalse;
}

/* TPLDocServiceProvider::GetInstantiationPolicy
*/
IK2ServiceProvider::InstancePerX TPLDocServiceProvider::GetInstantiationPolicy()
{
	return IK2ServiceProvider::kInstancePerSession;
}

/* TPLDocServiceProvider::HasMultipleIDs
*/
bool16 TPLDocServiceProvider::HasMultipleIDs() const
{
	return kTrue;
}

/* TPLDocServiceProvider::GetServiceIDs
*/
void TPLDocServiceProvider::GetServiceIDs(K2Vector<ServiceID>& serviceIDs)
{
	// Append a service IDs for each service provided. 
	for (int32 i = 0; i<fSupportedServiceIDs.size(); i++)
		//serviceIDs.Append(fSupportedServiceIDs[i]);
		serviceIDs.push_back(fSupportedServiceIDs[i]);

}
