#ifndef __TPLListboxData_H__
#define __TPLListboxData_H__

#include "VCPlugInHeaders.h"
#include "vector"

using namespace std;

class TPLListboxInfo
{
	public:	
		int16 index;
		double id;
		double typeId;
		double LanguageID;
		PMString name;
		bool16 isSelected;
		UID boxUID;
		bool16 isDragged;
		PMString code;
		bool16 isImageFlag; // kFalse:Text box kTrue:Image box
		int32	tableFlag;	// for Tables // 0: No table 1:Table

		//added on 11July...serial blast day
		int32 listBoxType;


		bool16 isEventField; //for event level attribute
		int32 header;
		int32 childTag;
		int32 tableType;
		int32 flowDir;
		int32 isSprayItemPerFrame;
		int32 catLevel;
		int32 imageIndex;
		int32 deleteIfEmpty;
		int32 dataType;
		int32 field4 ;	//--ClientID
		int32 field5;	//--AttributeCodeID


		TPLListboxInfo()
		{
			index=0;
			id=0;
			typeId=-1;//0;
			isSelected=kFalse;
			boxUID=kInvalidUID;
			isDragged=kFalse;
			isImageFlag=kFalse;
			tableFlag=0;
			LanguageID=0;

			//added on 11July...serial blast day
			listBoxType = -1;

			name = "";
			code = "";

			isEventField = kFalse;
			header = -1;
			childTag = -1;
			tableType = -1;
			flowDir = -1;
			isSprayItemPerFrame = -1;
			catLevel = -1;
			imageIndex = -1;
			deleteIfEmpty = -1;
			dataType = -1;
			field4 = -1;
			field5 = -1;

		}
};

class TPLListboxData
{
	private :
		static vector<TPLListboxInfo> PFListboxData;
		static vector<TPLListboxInfo> PGListboxData;
		static vector<TPLListboxInfo> PRListboxData;
		static vector<TPLListboxInfo> ItemListboxData;
		static vector<TPLListboxInfo> ProjectListboxData;
//added by vijay on 10-10-2006
		static vector<TPLListboxInfo> CatagoryListboxData;
	public:
		void setData
		(int16 , int16 , double , double , double , PMString , bool16 , PMString, bool16, int32,
		bool16 isEventField = kFalse,
		int32 header = -1,
		int32 childTag = -1,
		int32 tableType = -1,
		int32 flowDir = -1,
		int32 isSprayItemPerFrame = -1,
		int32 catLevel = -1,
		int32 imageIndex = -1,
		int32 deleteIfEmpty = -1,
		int32 dataType = -1,
		int32 field4 = -1,
		int32 field5 = -1);

		TPLListboxInfo getData(int16 , int16);
		int32 returnListVectorSize(int16);
		void clearVector(int16);
		void setIsBoxDragged(int16 , int16 , bool16);
};


#endif