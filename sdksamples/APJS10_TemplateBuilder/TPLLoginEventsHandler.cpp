#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "TPLID.h"
#include "ILoginEvent.h"
#include "CAlert.h"
#include "TPLLoginEventsHandler.h"
#include "TPLSelectionObserver.h"
//#include "IPaletteMgr.h"              //--------------Removed from CS3 API
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "TPLActionComponent.h"
#include "TPLTreeDataCache.h"

#include "CAlert.h"
//#include "IMessageServer.h"



#define FILENAME			PMString("TPLLoginEventsHandler.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


extern double CurrentClassID;
extern int32 Flag1;
extern bool16 IsRefresh;
int32 flag_1;
CREATE_PMINTERFACE(TPLLoginEventsHandler,kTPLLoginEventsHandlerImpl)

TPLLoginEventsHandler::TPLLoginEventsHandler
(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
}

TPLLoginEventsHandler::~TPLLoginEventsHandler()
{}

bool16 TPLLoginEventsHandler::userLoggedIn(void)
{
	//CA("TPLLoginEventsHandler::userLoggedIn");
	do
	{
		/* Check if the Template palette is on screen */
/*		InterfacePtr<IApplication> iApplication(gSession->QueryApplication());
		if(!iApplication)
		{
			CA("1");
			return kFalse;
		}

		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(!iPaletteMgr)
		{
			CA("2");
			return kFalse;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(!iPanelMgr)
		{
			CA("3");
			return kFalse;
		}
		
		PMLocaleId nLocale = LocaleSetting::GetLocale();	

		//iPaletteMgr->Startup(nLocale);
		IWindow* wndPtr=iPaletteMgr->GetPalette(0);
		if(!wndPtr)
		{
			CA("4");
			return kFalse;
		}
	//	CA("No Probs at all");
		 Template Palette is present in the screen */
		//CA("Before Template UserLoggedIn");
		TPLSelectionObserver selObserver(this);
		TPLMediatorClass::doReloadFlag = kFalse;
		CurrentClassID = -1;
		flag_1 = 1;
		selObserver.loadPaletteData();
	}while(kFalse);			
	return kTrue;	
}

bool16 TPLLoginEventsHandler::userLoggedOut(void)
{	
	//CA("TPLLoginEventsHandler::userLoggedOut");
	// Awasthi
	flag_1 = 0;
	//CA("Insdie UserLogged out");
	CurrentClassID = -1;
	TPLMediatorClass::CurrLanguageID = -1;
    TPLActionComponent actionComponetObj(this);
	actionComponetObj.CloseTemplatePalette();
	TPLTreeDataCache dc;
	dc.clearMap();

	return kTrue;	
}

bool16 TPLLoginEventsHandler::resetPlugIn(void)
{
	//CA("TPLLoginEventsHandler::resetPlugIn");
	do
	{
		flag_1 = 1;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){ 
			//CA("No iApplication");
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLLoginEventsHandler::resetPlugIn::iApplication==nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil){ 
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLLoginEventsHandler::resetPlugIn::iPanelMgr == nil");
			break;
		}

		bool16 isPanelOpen = iPanelMgr->IsPanelWithMenuIDMostlyVisible(kTPLPanelWidgetActionID);
		if(!isPanelOpen){//CA("!isPanelOpen");
			break;
		}

		CurrentClassID = -1;
		TPLActionComponent actionComponetObj(this);
		actionComponetObj.CloseTemplatePalette();
	
		Flag1 = 1;		
		
		TPLMediatorClass::iPanelCntrlDataPtr = nil;
		TPLMediatorClass::iPanelCntrlDataPtrTemp = nil;
		TPLMediatorClass::PFLstboxCntrlView = nil;
		TPLMediatorClass::PGLstboxCntrlView = nil;
		TPLMediatorClass::PRLstboxCntrlView = nil;
		TPLMediatorClass::ItemLstboxCntrlView = nil;
		TPLMediatorClass::ProjectLstboxCntrlView = nil;
		TPLMediatorClass::dropdownCtrlView1 = nil;
		TPLMediatorClass::refreshBtnCtrlView1 = nil;
		TPLMediatorClass::appendRadCtrlView1 = nil;
		TPLMediatorClass::embedRadCtrlView1 = nil;
		TPLMediatorClass::tagFrameChkboxCtrlView1 = nil;

		
		PMLocaleId nLocale=LocaleSetting::GetLocale();
		// Here we are opening our Template Palette
		// [[[[[[[[ IMP Function ]]]]]]]]
	
		iPanelMgr->ShowPanelByMenuID(kTPLPanelWidgetActionID);
		/*iPanelMgr->ShowPanelByWidgetID(kTPLPanelWidgetID);
		iPanelMgr->SetPanelResizabilityByWidgetID(kTPLPanelWidgetID,kTrue);*/

		//if(!IsRefresh && Flag1 ==1)
		//{		
			//CA("!IsRefresh && Flag1 ==1");
			if(iPanelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID))
			{
				//CA("IsPanelWithWidgetIDShown is true");
				
				IControlView* icontrol = iPanelMgr->GetVisiblePanel(kTPLPanelWidgetID);
				if(!icontrol)
				{
					//CA("!icontrol template");
					return kFalse;
				}
				icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));
				icontrol->SetFrameBinding(icontrol->GetFrameBinding());
				
			}			
			//icontrol->Resize();			
		//}
//		TPLActionComponent::palettePanelPtr=iPaletteMgr;
	}while(kFalse);
	return kTrue;
}