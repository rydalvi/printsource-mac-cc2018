////////////////////////////////////////////////////////////////////////////////
////////  This file implements the functionality to obtain Graphic frame ///////
/////////////////////////////////////////////////////////////////////////////////

#include "VCPlugInHeaders.h"

// Interface includes:
#include "INewPageItemCmdData.h"
//#include "ICreateFrameData.h"      //----------------Removed From CS3 API
#include "ITextModel.h"
#include "IGeometry.h"
#include "IRangeData.h"
#include "IUIDData.h"
#include "ITextModelCmds.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "SDKLayoutHelper.h"

// General includes:
#include "Utils.h"
#include "SplineID.h"
#include "textchar.h"
#include "UIDList.h"
#include "IAppFramework.h"
#include "TPLManipulateInline.h"
#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)

UIDRef newFrameUIDRef;

/*
*/
ErrorCode TPLManipulateInline::InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex)	
{
	ErrorCode status = kFailure;
//	CmdUtils::SequencePtr cmdSeq(status);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do {

		//UIDRef newFrameUIDRef;
		status = this->CreateFrame(storyUIDRef.GetDataBase(), newFrameUIDRef);
		ASSERT(status == kSuccess);
		if (status != kSuccess) {
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::InsertInline::status != kSuccess");
			break;
		}
		status = this->ChangeToInline(storyUIDRef, whereTextIndex, newFrameUIDRef);
		ASSERT(status == kSuccess);
	} while (false);
	//cmdSeq.SetState(status);
	return status;
}

/* 
*/
ErrorCode TPLManipulateInline::CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef)
{
	// You can make any type of frame into an inline. Here we make a new graphic frame.	
	PMRect bounds(0, 0, 100, 100);
	SDKLayoutHelper layoutHelper;
	newFrameUIDRef = layoutHelper.CreateRectangleFrame(UIDRef(database, kInvalidUID), bounds);
	if (newFrameUIDRef)
		return kSuccess;
	else
		return kFailure;
}


/*
*/
ErrorCode TPLManipulateInline::ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do {
		// Validate parameters.
		InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
		ASSERT(textModel != nil);
		if(!textModel) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::!textModel");		
			break;
		}
		InterfacePtr<IGeometry> pageItemGeometry(frameUIDRef, UseDefaultIID());
		ASSERT(pageItemGeometry != nil);
		if (pageItemGeometry == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::pageItemGeometry == nil");		
			break;
		}

		// Insert character in text flow to anchor the inline.
		/*K2*/boost::shared_ptr<WideString>	insertMe(new WideString);	//---CS5--

		insertMe->Append(kTextChar_Inline); 
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
		InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(whereTextIndex, insertMe));
		ASSERT(insertTextCmd != nil);
		status = CmdUtils::ProcessCommand(insertTextCmd);
		ASSERT(status == kSuccess);
		if(status != kSuccess) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::status != kSuccess");		
			break;
		}

		// Change the page item into an inline.
		InterfacePtr<ICommand> changeILGCmd(CmdUtils::CreateCommand(kChangeILGCmdBoss));
		ASSERT(changeILGCmd != nil);
		if (changeILGCmd == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::changeILGCmd == nil");		
			break;
		}
		InterfacePtr<IRangeData> rangeData(changeILGCmd, UseDefaultIID());
		ASSERT(rangeData != nil);
		if (rangeData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::rangeData == nil");
			break;
		}
		rangeData->Set(whereTextIndex, whereTextIndex);
		InterfacePtr<IUIDData> ilgUIDData(changeILGCmd, UseDefaultIID());
		ASSERT(ilgUIDData != nil);
		if (ilgUIDData == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLManipulateInline::ChangeToInline::ilgUIDData == nil");		
			break;
		}
		ilgUIDData->Set(frameUIDRef);
		changeILGCmd->SetItemList(UIDList(textModel));
		status = CmdUtils::ProcessCommand(changeILGCmd);
		ASSERT(status == kSuccess);

	} while(kFalse);
	return status;
}