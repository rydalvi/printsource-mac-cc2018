#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TPLMediatorClass.h"
#include "TPLListboxData.h"
#include "IListControlData.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLPGListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

int32 PGRow = -1;
class TPLPGListBoxObserver : public CObserver
{
public:
	TPLPGListBoxObserver(IPMUnknown *boss);
	~TPLPGListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(TPLPGListBoxObserver, kTPLPGListBoxObserverImpl)

TPLPGListBoxObserver::TPLPGListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLPGListBoxObserver::~TPLPGListBoxObserver()
{
}

void TPLPGListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLPGListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this);
	}
}

void TPLPGListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			PGRow = -1;
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
				break;

			SDKListBoxHelper listHelper(this, kTPLPluginID);

			IControlView* PGLstboxCntrlView = listHelper.FindCurrentListBox(panel, 2);
			if(PGLstboxCntrlView == nil) 
				break;

			InterfacePtr<IListBoxController> listCntl(PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				break;

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if(kSelectionLength>0)
			{
				curSelRowIndex=curSelection[0];
				TPLListboxData PGLstboxData;
				int32 vectSize=PGLstboxData.returnListVectorSize(2);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TPLListboxInfo PGListboxInfo=PGLstboxData.getData(2,y);
						curSelRowStr=PGListboxInfo.name;
						TPLMediatorClass::imageFlag=PGListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TPLMediatorClass::curSelLstbox=2;
			TPLMediatorClass::curSelRowIndex=curSelRowIndex;
			PGRow = curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::PGLstboxCntrlView=PGLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;
		} while(0);
	}
}

