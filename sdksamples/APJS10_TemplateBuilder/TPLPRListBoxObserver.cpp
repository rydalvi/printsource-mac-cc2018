#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TPLMediatorClass.h"
#include "TPLListboxData.h"
#include "IListControlData.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLPRListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

int32 PRRow = -1;
class TPLPRListBoxObserver : public CObserver
{
public:
	TPLPRListBoxObserver(IPMUnknown *boss);
	~TPLPRListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(TPLPRListBoxObserver, kTPLPRListBoxObserverImpl)

TPLPRListBoxObserver::TPLPRListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLPRListBoxObserver::~TPLPRListBoxObserver()
{
}

void TPLPRListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLPRListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);      //CS3 Change
	}
}

void TPLPRListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CAlert::InformationAlert(__FUNCTION__);
	if (protocol == IID_ILISTCONTROLDATA && theChange == kListSelectionChangedByUserMessage ) 
	{
		do 
		{
			PRRow = -1;
			/*InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
				break;

			SDKListBoxHelper listHelper(this, kTPLPluginID);

			IControlView* PRLstboxCntrlView = listHelper.FindCurrentListBox(panel, 3);
			if(PRLstboxCntrlView == nil) 
				break;*/

			InterfacePtr<IListBoxController> listCntl(/*PRLstboxCntrlView*/TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				break;

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if(kSelectionLength>0)
			{
				curSelRowIndex=curSelection[0];
				TPLListboxData PRLstboxData;
				int32 vectSize=PRLstboxData.returnListVectorSize(3);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TPLListboxInfo PRListboxInfo=PRLstboxData.getData(3,y);
						curSelRowStr=PRListboxInfo.name;
						TPLMediatorClass::tableFlag=PRListboxInfo.tableFlag;
						TPLMediatorClass::imageFlag=PRListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TPLMediatorClass::curSelLstbox=3;
			TPLMediatorClass::curSelRowIndex=curSelRowIndex;
			PRRow = curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			//TPLMediatorClass::PRLstboxCntrlView=PRLstboxCntrlView;
		} while(0);
	}
}

