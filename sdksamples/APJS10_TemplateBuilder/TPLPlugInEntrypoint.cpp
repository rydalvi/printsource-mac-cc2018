#include "VCPlugInHeaders.h"
#include "TPLPlugInEntrypoint.h"
#include "CAlert.h"
#include "TPLID.h"
#include "TPLLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLPluginEntryPoint.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#ifdef WINDOWS
	ITypeLib* TPLPlugInEntrypoint::fSPTypeLib = nil;
#endif

bool16 TPLPlugInEntrypoint::Load(ISession* theSession)
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IRegisterLoginEvent> regEvt
		((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt)
		{
			//CA("Invalid regEvt");
			break;
		}
		regEvt->registerLoginEvent(kTPLLoginEventsHandler); 
	}while(kFalse);
	return kTrue;
}

/* Global
*/
static TPLPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}
