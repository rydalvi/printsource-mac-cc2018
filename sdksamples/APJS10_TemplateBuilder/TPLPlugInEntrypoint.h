#ifndef __TPLPlugInEntrypoint_h__
#define __TPLPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class TPLPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif