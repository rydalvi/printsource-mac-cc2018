#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TPLMediatorClass.h"
#include "TPLListboxData.h"
#include "IListControlData.h"
#include "IAppFramework.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLProjectListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


int32 ProjectRow = -1;

class TPLProjectListBoxObserver : public CObserver
{
public:
	TPLProjectListBoxObserver(IPMUnknown *boss);
	~TPLProjectListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

//CREATE_PMINTERFACE(TPLProjectListBoxObserver, kTPLProjectListBoxObserverImpl)

TPLProjectListBoxObserver::TPLProjectListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLProjectListBoxObserver::~TPLProjectListBoxObserver()
{
}

void TPLProjectListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLProjectListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		
		subject->DetachObserver(this,IID_ILISTCONTROLDATA); // CS3 Change
	}
	CAlert::InformationAlert("TPLProjectListBoxObserver Detach");
}

void TPLProjectListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{//CAlert::InformationAlert(__FUNCTION__);
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	if((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			ProjectRow = -1;
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLProjectListBoxObserver::Update::panel == nil");			
				break;
			}

			SDKListBoxHelper listHelper(this, kTPLPluginID);

			IControlView* ProjectLstboxCntrlView= listHelper.FindCurrentListBox(panel, 5);
			if(ProjectLstboxCntrlView== nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLProjectListBoxObserver::Update::ProjectLstboxCntrlView == nil");			
				break;
			}

			InterfacePtr<IListBoxController> listCntl(ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLProjectListBoxObserver::Update::listCntl == nil");			
				break;
			}

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/*Getting current selection here*/

			if (kSelectionLength> 0 )
			{
				curSelRowIndex=curSelection[0];

				TPLListboxData ProjectLstboxData;
				
				int32 vectSize=ProjectLstboxData.returnListVectorSize(5);

				/* For getting data of the selected row of listbox */	
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TPLListboxInfo ProjectListboxInfo=ProjectLstboxData.getData(5,y);
						curSelRowStr=ProjectListboxInfo.name;
						TPLMediatorClass::imageFlag=ProjectListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TPLMediatorClass::curSelLstbox=5;
			TPLMediatorClass::curSelRowIndex=curSelRowIndex;
			ProjectRow = curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ProjectLstboxCntrlView=ProjectLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;

		} while(0);
	}
}

/*
PMString row123("Row no:");
					InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
								if (iPanelControlData == nil)
									CA("break1");//break;

								IControlView* controlView=iPanelControlData->FindWidget(kTPLProjectPanelLstboxWidgetID);
								if(controlView==nil) 
									CA("break2");//break;
								CA("start");
								InterfacePtr<IListBoxController> ilistboxcontroller(controlView, UseDefaultIID());
								if(ilistboxcontroller==nil)
								{
									CA("break");
									//break;
								}
								CA("end");
								int32 rowinlistbox;
				rowinlistbox = ilistboxcontroller->GetSelected();
								
								row123.AppendNumber(rowinlistbox);
								CA(row123);
*/