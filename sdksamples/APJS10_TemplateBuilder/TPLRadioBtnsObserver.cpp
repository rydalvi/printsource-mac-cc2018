#include "VCPlugInHeaders.h"
#include "CAlert.h"
#include "TPLID.h"
#include "CObserver.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ISubject.h"
#include "ITristateControlData.h"
#include "TPLMediatorClass.h"
#include "IAppFramework.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLRadioBtnsObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

class TPLRadioBtnsObserver : public CObserver
{
	public:
		TPLRadioBtnsObserver(IPMUnknown* boss) : CObserver(boss) {}
		virtual ~TPLRadioBtnsObserver() {}
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
		void enableDisableChkbox(bool16 state);
};

CREATE_PMINTERFACE(TPLRadioBtnsObserver, kTPLRadioBtnsObserverImpl)

void TPLRadioBtnsObserver::AutoAttach()
{
	CObserver::AutoAttach();
	do
	{
		InterfacePtr<ISubject> subject( this, UseDefaultIID() ) ;
		if (subject == nil)
		{
//			CA("Err: invalid iSubject pointer");
			break;
		}
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA) ;
	} while (false);
}

void TPLRadioBtnsObserver::AutoDetach()
{
	CObserver::AutoDetach();
	do
	{
		InterfacePtr<ISubject> subject( this, UseDefaultIID() ) ;
		if (subject == nil)
		{
//			CA("Err: invalid iSubject pointer");
			break;
		}
		subject->DetachObserver(this, IID_ITRISTATECONTROLDATA) ;
	} while (false);
}

void TPLRadioBtnsObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
	if (controlView == nil)
	{
		//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLRadioBtnsObserver::Update::controlView == nil");	
		return;
	}

	WidgetID theSelectedWidget = controlView->GetWidgetID();
	if (theSelectedWidget == kAppendRadBtnWidgetID)
	{
		this->setTriState(kAppendRadBtnWidgetID,ITriStateControlData::kSelected);
		this->setTriState(kEmbedRadBtnWidgetID,ITriStateControlData::kUnselected);
		this->enableDisableChkbox(kFalse);
		TPLMediatorClass::curSelRadBtn=1;
	}
	if (theSelectedWidget == kEmbedRadBtnWidgetID)
	{
		this->setTriState(kAppendRadBtnWidgetID,ITriStateControlData::kUnselected);
		this->setTriState(kEmbedRadBtnWidgetID,ITriStateControlData::kSelected);		
		this->enableDisableChkbox(kTrue);
		TPLMediatorClass::curSelRadBtn=2;
	}
}

void TPLRadioBtnsObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do 
	{
		IControlView * iControlView  = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(widgetID);
		if(iControlView==nil) 
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLRadioBtnsObserver::setTriState::iControlView == nil");		
			break;
		}
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLRadioBtnsObserver::setTriState::itristatecontroldata == nil");		
			break;
		}

		itristatecontroldata->SetState(state,kTrue,kFalse);
	} while(kFalse);	
}

void TPLRadioBtnsObserver::enableDisableChkbox(bool16 state)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do{
		IControlView* controlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
		if(controlView==nil) 
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLRadioBtnsObserver::enableDisableChkbox::controlView == nil");		
			break;
		}

		if(state) 
			controlView->Enable(); 
		else 
			controlView->Disable();
	}while(kFalse);
}
