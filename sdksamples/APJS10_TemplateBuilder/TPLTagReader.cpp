#include "VCPluginHeaders.h"
#include "IPMUnknown.h"
#include "UIDRef.h"
#include "ITextFrame.h"
#include "IXMLUtils.h"
#include "IXMLTagList.h"

#include "IDocument.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextModel.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "IGraphicFrameData.h"
#include "UIDList.h"
#include "XMLReference.h"
#include "FrameUtils.h"
#include "LayoutUtils.h"
#include "k2smartptr.h"
#include "CAlert.h"
#include "IXMLUtils.h"

#include "TPLTagReader.h"

#define CA(z) CAlert::InformationAlert(z)

void TPLTagReader::getTextFrameTags(void)
{
	do
	{	
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil){
			//CA("textFrame is nil");
			break;
		}
				
		InterfacePtr<ITextModel> objTxtMdl(textFrame->QueryTextModel());
		if (objTxtMdl == nil){
			//CA("ITextModel is nil");
			break;
		}

		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		if (objXMLRefDat==nil){
			//CA("IXMLReferenceData* nil");
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		IIDXMLElement* xmlElement=xmlRef.Instantiate();
		if(xmlElement==nil){
			//CA("IIDXMLElement nil");
			break;
		}

		int elementCount=xmlElement->GetChildCount();
		
		for(int i=0; i<elementCount; i++)
		{
			XMLReference elementXMLref=xmlElement->GetNthChild(i);
			IIDXMLElement * childElement=elementXMLref.Instantiate();
			if(childElement==nil)
				continue;

			PMString tagName=childElement->GetTagString();

			TPLTagStruct tInfo;

			int32 attribCount=childElement->GetAttributeCount();
			TextIndex sIndex=0, eIndex=0;
			Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
			
			tInfo.startIndex=sIndex;
			tInfo.endIndex=eIndex;
			tInfo.tagPtr=childElement;
		
			for(int j=0; j<attribCount; j++)
			{
				PMString attribName=childElement->GetAttributeNameAt(j);
				PMString attribVal=childElement->GetAttributeValue(attribName);
				getCorrespondingTagAttributes(attribName, attribVal, tInfo);
			}
			tList.push_back(tInfo);
		}
	}while(0);
}

void TPLTagReader::getGraphicFrameTags(void)
{
	do{		
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);	
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
		if (objXMLRefDat == nil)
			break;

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		IIDXMLElement *xmlElement = xmlRef.Instantiate();
		if(xmlElement == nil)
			break;

		PMString tagName = xmlElement->GetTagString();
		
		TPLTagStruct tInfo;
		tInfo.tagPtr=xmlElement;

		int32 attribCount = xmlElement->GetAttributeCount();

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName = xmlElement->GetAttributeNameAt(j);
			PMString attribVal  = xmlElement->GetAttributeValue(attribName);
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		tInfo.endIndex=-1;
		tInfo.startIndex=-1;
		tList.push_back(tInfo);
	}while(0);
}

TPLTagList TPLTagReader::getTagsFromBox(UIDRef boxId)
{
	tList.clear();
	InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = FrameUtils::GetTextFrameUID(unknown);
	
	boxUIDRef=boxId;
	textFrameUID=frameUID;

	if(frameUID == kInvalidUID)
		getGraphicFrameTags();			
	else 
		getTextFrameTags();
	
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;

	return tList;
}


TPLTagList TPLTagReader::getFrameTags(UIDRef frameUIDRef)
{
	tList.clear();
	boxUIDRef=frameUIDRef;
	// Get the graphic frame tags as in case of graphic frames tag is attached with the frame itself
	getGraphicFrameTags();			

	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;

	return tList;
}

bool16 TPLTagReader::getCorrespondingTagAttributes(const PMString& attribName, const PMString& attribVal, TPLTagStruct& tStruct)
{
	if(attribName.IsEqual("ID", kFalse))
	{
		tStruct.elementId=attribVal.GetAsNumber();
		return kTrue;
	}
	
	if(attribName.IsEqual("imgFlag", kFalse))
	{
		tStruct.reserved1=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("index", kFalse))
	{
		tStruct.whichTab=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("typeId", kFalse))
	{
		tStruct.typeId=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("parentId", kFalse))
	{
		tStruct.parentId=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("reserved", kFalse))
	{
		tStruct.reserved2=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("parentTypeID", kFalse))
	{
		tStruct.parentTypeID=attribVal.GetAsNumber();
		return kTrue;
	}
	return kFalse;
}


bool16 TPLTagReader::GetUpdatedTag(TPLTagStruct& tInfo)
{
	if(!tInfo.tagPtr)
		return kFalse;
	
	TextIndex sIndex=0, eIndex=0;
	Utils<IXMLUtils>()->GetElementIndices(tInfo.tagPtr, &sIndex, &eIndex);
	
	tInfo.startIndex=sIndex;
	tInfo.endIndex=eIndex;
	
	return kTrue;
}

