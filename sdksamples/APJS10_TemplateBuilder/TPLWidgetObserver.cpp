#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "CObserver.h"
#include "TPLID.h"
#include "CAlert.h"
#include "ITriStateControlData.h"


//#include "IMessageServer.h"
#define FILENAME			PMString("TPLWidgetObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str(x);CA(str);}


class TPLWidgetObserver : public CObserver
{
public:
	TPLWidgetObserver(IPMUnknown *boss);
	~TPLWidgetObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
private:
	void handleWidgetHit(InterfacePtr<IControlView>& controlView, const ClassID& theChange, WidgetID widgetID) ;
	void attachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
	const PMIID fObserverIID;
};

CREATE_PMINTERFACE(TPLWidgetObserver, kTPLWidgetObserverImpl)
	
TPLWidgetObserver::TPLWidgetObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_ITPLWIDGETOBSERVER)
{
}

TPLWidgetObserver::~TPLWidgetObserver()
{
}

void TPLWidgetObserver::AutoAttach()
{
	do {
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData)
			break;
	
		this->attachWidget(panelControlData, kAppendRadBtnWidgetID, IID_ITRISTATECONTROLDATA);
		this->attachWidget(panelControlData, kEmbedRadBtnWidgetID, IID_ITRISTATECONTROLDATA);
			
	} while(kFalse);
}	

void TPLWidgetObserver::AutoDetach()
{
	do {
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData)
			break;

		this->detachWidget(panelControlData, kAppendRadBtnWidgetID, IID_ITRISTATECONTROLDATA);
		this->detachWidget(panelControlData, kEmbedRadBtnWidgetID, IID_ITRISTATECONTROLDATA);
		
	} while(kFalse);
}

void TPLWidgetObserver::attachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);

	//TRACE("TblAttWidgetObserver::AttachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if(!panelControlData) 
			break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
			break;

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
			break;

		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

void TPLWidgetObserver::detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData != nil);
	do
	{
		if(panelControlData == nil) 
			break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView != nil);
		if (controlView == nil)
			break;

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject != nil);
		if (subject == nil)
			break;

		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (false);
}

void TPLWidgetObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	do {
		ASSERT(theSubject);
		if(!theSubject) {
			break;
		}
		InterfacePtr<IControlView>  icontrolView(theSubject, UseDefaultIID());
		ASSERT(icontrolView);
		if(!icontrolView) {
			break;
		}
		WidgetID thisID = icontrolView->GetWidgetID();
		if(thisID != kInvalidWidgetID) {
			this->handleWidgetHit(icontrolView, theChange, thisID);
		}
	} while(kFalse);
}

void TPLWidgetObserver::handleWidgetHit
(InterfacePtr<IControlView> & controlView, 
const ClassID & theChange, 
WidgetID widgetID) 
{
	do 
	{
		ASSERT(widgetID != kInvalidWidgetID);
		ASSERT(controlView);
		if(controlView==nil) 
			break;
		
		WidgetID theSelectedWidget = widgetID.Get();//controlView->GetWidgetID();
		if (theChange == kTrueStateMessage)
			break;

		if (theSelectedWidget == kEmbedRadBtnWidgetID
				&& theChange == kTrueStateMessage)
			break;

	} while(kFalse);
}

void TPLWidgetObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	do 
	{
		InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
		ASSERT(iPanelControlData);
		if(iPanelControlData==nil) 
			break;
		
		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
			break;
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		ASSERT(itristatecontroldata);
		if(itristatecontroldata==nil) 
			break;
		
		itristatecontroldata->SetState(state);
	} while(kFalse);	
}

