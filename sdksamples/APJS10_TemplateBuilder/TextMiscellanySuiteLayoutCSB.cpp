#include "VCPlugInHeaders.h"	

#include "ILayoutTarget.h"

#include "ITextMiscellanySuite.h"
#include "CmdUtils.h"
#include "IPageItemUtils.h"
#include "ITextTarget.h"
#include "ITextModel.h"
#include "IFrameList.h"
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "CAlert.h"
#include "ITextFrameColumn.h"
#include "ITOPFrameData.h"
#include "IHierarchy.h"
#include "ITextUtils.h"


//Alert Macros.////////////
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("TextMiscellanySuiteLayoutCSB.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////


class TextMiscellanySuiteLayoutCSB : public CPMUnknown<ITextMiscellanySuite>
{
public:
	TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss);

	virtual	~TextMiscellanySuiteLayoutCSB(void);

	virtual bool16 GetUidList(UIDList &);
	virtual bool16 GetFrameUIDRef(UIDRef & frameUIDRef);
	virtual bool16 GetCaretPosition(TextIndex &pos);
	virtual bool16 GetTextSelectionRange(TextIndex &start, TextIndex &end);


};
CREATE_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kTPLTextMiscellanySuiteLayoutCSBImpl)

TextMiscellanySuiteLayoutCSB::TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

/* Destructor
*/
TextMiscellanySuiteLayoutCSB::~TextMiscellanySuiteLayoutCSB(void)
{
}

bool16 TextMiscellanySuiteLayoutCSB::GetUidList(UIDList & TempUidList)
{
	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	const UIDList	selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	TempUidList = selectedItems;	
	return 1;
}

bool16 TextMiscellanySuiteLayoutCSB::GetFrameUIDRef(UIDRef & frameUIDRef)
{
	//CA(__FUNCTION__);
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found ");
		return kFalse;
	}
	else
	{
		//CA("Text Target in suite is not null");
		InterfacePtr<ITextModel>txtModel(txtTarget->QueryTextModel());
		if(txtModel)
		{	//CA("txtModel 1");
				
			InterfacePtr<IFrameList>frameList(txtModel->QueryFrameList());
			if(!frameList)
			{
				CA("!frameList");
				return kTrue;
			}

			InterfacePtr<ITextFrameColumn>TextFrameColumnPtr(frameList->QueryNthFrame(0));
			if(!TextFrameColumnPtr)
			{
				//CA("!TextFrameColumnPtr");
				return kTrue;
			}

			// Check for a text frame for text on a path.
			InterfacePtr<ITOPFrameData> topFrameData(TextFrameColumnPtr, UseDefaultIID());
			if (topFrameData != nil) {

				// This is a text on a path text frame. Refer to the
				// spline that the text on a path is associated with.
				frameUIDRef = UIDRef(::GetDataBase(TextFrameColumnPtr), topFrameData->GetMainSplineItemUID());
				return kTrue;
			}

			// Check for a regular text frame
			InterfacePtr<IHierarchy> graphicFrameHierarchy(Utils<ITextUtils>()->QuerySplineFromTextFrame(TextFrameColumnPtr));
			if(graphicFrameHierarchy != nil)
				frameUIDRef = ::GetUIDRef(graphicFrameHierarchy);

           		 
		}

	}
	return kTrue;
}

bool16 TextMiscellanySuiteLayoutCSB::GetCaretPosition(TextIndex &pos)
{
	bool16 result=kFalse;
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found");
	}
	else
	{
		RangeData rangData=txtTarget->GetRange();
		pos=rangData.End();	
		result=kTrue;
	}
	return result;
}


bool16 TextMiscellanySuiteLayoutCSB::GetTextSelectionRange(TextIndex &start, TextIndex &end)
{
	bool16 result=kFalse;
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found");
	}
	else
	{
		RangeData rangData=txtTarget->GetRange();
		end=rangData.End();
		start = rangData.Start(0);		
		result=kTrue;
	}
	return result;
}
