#include "VCPluginHeaders.h"

#include "IPMUnknown.h"
#include "UIDRef.h"
//#include "ITextFrame.h"
#include "IXMLUtils.h"
#include "IXMLTagList.h"
//#include "ISelection.h"
#include "ISelectUtils.h"
#include "IHierarchy.H" 
#include "TextIterator.h"
#include "IDocument.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextModel.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "IGraphicFrameData.h"
#include "UIDList.h"
#include "XMLReference.h"
#include "IFrameUtils.h" //FrameUtils.h"
#include "ILayoutUtils.h" //Cs4
#include "k2smartptr.h"
#include "CAlert.h"
#include "IXMLUtils.h"
#include "CAlert.h"
#include "TagReader.h" 
#include "ILayoutUtils.h"//Cs4
#include "ISpreadList.h"
#include "ISpread.h"
#include "ILayoutControlData.h"
#include "CTUnicodeTranslator.h"
#include "BoxReader.h"
#include "GlobalData.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ISpread.h" 
#include "IGeometry.h"
//#include "LayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include "ITagReader.h"
#include "CheckTag.h"
#include "IAppFramework.h"
#define CA(x) CAlert::InformationAlert(x)
///Global Pointers
extern ITagReader* itagReader;
/////////

// selects the Elements from Selected Frame--- option1
bool16 CheckTag::getTextSelectedBoxIds(UIDList&  selectUIDList)
{
	//CA(__FUNCTION__);
	//	TagReader tReader;
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
/*	InterfacePtr<ISelection> selection(::QuerySelection());
	if(selection == NULL)
		return kFalse;

	scoped_ptr<UIDList> _selectUIDList(selection->CreateUIDList());
	if(_selectUIDList==NULL)
		return kFalse;

	const int32 listLength=_selectUIDList->Length();
	
	if(listLength==0)
		return kFalse;
*/
	UIDList selectUID;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	return kFalse;
	
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

	if(!iSelectionManager)
	{	
		return 0;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		//CA("retun");
		
		return 0; 
	}

//	UIDList	selectUIDList;
//	txtMisSuite->GetUidList(selectUIDList);
	txtMisSuite->GetUidList(selectUID);
	
	for(int i=0; i<selectUID.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(selectUID.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;
		//UID kidUID;
		//int32 numKids=iHier->GetChildCount();
		
		//for(int j=0;j<numKids;j++)
		//{
			IIDXMLElement* ptr;
		//	kidUID=iHier->GetChildUID(j);
					
		//	UIDRef boxRef(selectUID.GetDataBase(), kidUID);
			itagReader->getTagsFromBox(selectUID.GetRef(i), &ptr);
			
		//	if(this->doesExist(ptr,selectUID.GetRef(i)))
		//	{		
		//			selectUIDList.Append(kidUID);
		//			
		//		//	this->showTagInfo(boxRef);
		//	}
		//	if(!this->doesExist(ptr,selectUID.GetRef(i)))
		//	{
		//	//ignore the element if it does not contain XML tags
		//	//	CA("XML tags not found ");
		//	}
		//}
	}
	if(selectUIDList.Length()==0)
	{
		return kFalse;
	}
	
	return kTrue;

}

//Selects the elements from the Current Page--- option2
bool16 CheckTag::getPageSelectedBoxIds(UIDList&  selectUIDList)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	return kFalse;
	do{ 	
    
		// TagReader tReader;
		 InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){ 
			ptrIAppFramework->LogError("AP46_UpdateRepository::CheckTag::getPageSelectedBoxIds::!itagReader");	
			return kFalse;
		}

		 IDocument* doc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
		if(doc==NULL)
		{
			//CA("No Page Found");
			break;
		}

		IDataBase* database = ::GetDataBase(doc);
		if(database==NULL)
		{
			CA("Error database==NULL");
			break;
		}

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == NULL)
			return kFalse;

		UID pageUID = layoutData->GetPage();
		if(pageUID == kInvalidUID)
			return kFalse;

	/*		/////CS3 Change
		IGeometry* spreadItem = layoutData->GetSpread();
		if(spreadItem == NULL)
			return kFalse;

		InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
		if (iSpread == NULL)
			return kFalse;
	*/
	/////Added by Amit
		UIDRef spreadUIDRef=layoutData->GetSpreadRef();
		InterfacePtr<ISpread>iSpread(spreadUIDRef, UseDefaultIID());
		if (iSpread == NULL)
			return kFalse;
	//////	end
					
		int pageNum=iSpread->GetPageIndex(pageUID);

		UIDList allPageItems(database);
		UIDList tempList(database);
		iSpread->GetItemsOnPage(pageNum,&tempList, kTrue);
		allPageItems.Append(tempList);
		
		for(int i=0; i<allPageItems.Length(); i++)
		{
			//InterfacePtr<IHierarchy> iHier(allPageItems.GetRef(i), UseDefaultIID());
			//if(!iHier)
			//	continue;

			//UID kidUID;

			//int32 numKids=iHier->GetChildCount();
			//for(int j=0;j<numKids;j++)
			//{
			IIDXMLElement* ptr;

			//		kidUID=iHier->GetChildUID(j);
			//	UIDRef boxRef(allPageItems.GetDataBase(), kidUID);
			
			itagReader->getTagsFromBox(allPageItems.GetRef(i), &ptr);
			
			//	if(this->doesExist(ptr,boxRef))
			//	{
			//		selectUIDList.Append(kidUID);	
			//	//	this->showTagInfo(boxRef);
			//	}
			//	if(!this->doesExist(ptr,boxRef))
			//	{
			//		//	CA("XML tags not found ");				
			//	}
			//}
		}
	
	}while(0);
	
	if(selectUIDList.Length()==0)
	{
		return kFalse;
	}
 return kTrue;
}


//Selects the elements from the Current Spread --- option3
bool16 CheckTag::getSpreadSelectedBoxIds(UIDList&  selectUIDList)
{
	//CA(__FUNCTION__);
	do
	{
	 //TagReader tReader;
//		InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){ 
			return kFalse;
		}

	 IDocument* doc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	if(doc==NULL)
	{
		//CA("No Page Found");
		break;
	}

	IDataBase* database = ::GetDataBase(doc);
	if(database==NULL)
	{
		//CA("Error database==NULL");
		break;
	}

	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
	if (layoutData == NULL)
		return kFalse;
/*		//////CS3 Change
	IGeometry* spreadItem = layoutData->GetSpread();
	if(spreadItem == NULL)
		return kFalse;

	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	if (iSpread == NULL)
		return kFalse;
*/
	//////Added by Amit	
	UIDRef spreadUIDRef=layoutData->GetSpreadRef();
	InterfacePtr<ISpread>iSpread(spreadUIDRef, UseDefaultIID());
	if (iSpread == NULL)
		return kFalse;
	///////end	 
	int numPages=iSpread->GetNumPages();

	UIDList allPageItems(database);

	for(int k=0; k<numPages; k++)
	{
		UIDList tempList(database);
		iSpread->GetItemsOnPage(k, &tempList, kFalse);
		allPageItems.Append(tempList);
	}

	
	for(int i=0; i<allPageItems.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(allPageItems.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;

		//UID kidUID;

		//int32 numKids=iHier->GetChildCount();
		//for(int j=0;j<numKids;j++)
		//{
			IIDXMLElement* ptr;
		//
		//	kidUID=iHier->GetChildUID(j);
		//	UIDRef boxRef(allPageItems.GetDataBase(), kidUID);
			itagReader->getTagsFromBox(allPageItems.GetRef(i), &ptr);
		//	if(this->doesExist(ptr,allPageItems.GetRef(i)))
		//	{
		//		selectUIDList.Append(kidUID);	
		//	//	this->showTagInfo(boxRef);
		//	}
		//	if(!this->doesExist(ptr,allPageItems.GetRef(i)))
		//	{
		//	//	CA("XML tags not found ");
		//		
		//	}
		//}
	}
	
 }while(0);
	if(selectUIDList.Length()==0)
	{
		return kFalse;
	}
 return kTrue;
}

// Selects the element from the Current Document --- option 4
bool16 CheckTag::getDocumentSelectedBoxIds(UIDList&  selectUIDList)
{
  // CA(__FUNCTION__);
	do {
	//TagReader tReader;
//	   InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){ 
			return kFalse;
		}
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
		if(fntDoc==NULL)

			return -1;

		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
		if (layout == NULL)
			return -1;

		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==NULL)
			return -1;

		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==NULL)
			return -1;
		
		bool16 collisionFlag=kFalse;
		UIDList allPageItems(database);

		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
				return -1;

			int numPages=spread->GetNumPages();

			for(int i=0; i<numPages; i++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(i, &tempList, kFalse);
				allPageItems.Append(tempList);
			}
		}

		for(int i=0; i<allPageItems.Length(); i++)
		{
			//InterfacePtr<IHierarchy> iHier(allPageItems.GetRef(i), UseDefaultIID());
			//if(!iHier)
			//	continue;

			//UID kidUID;

			//int32 numKids=iHier->GetChildCount();
			//for(int j=0;j<numKids;j++)
			//{
				IIDXMLElement* ptr;

			//	kidUID=iHier->GetChildUID(j);
			//	UIDRef boxRef(allPageItems.GetDataBase(), kidUID);
				itagReader->getTagsFromBox(allPageItems.GetRef(i), &ptr);
			//	if(this->doesExist(ptr,allPageItems.GetRef(i)))
			//	{
			//		selectUIDList.Append(kidUID);	
			//	//	this->showTagInfo(boxRef);
			//	}
			//	if(!this->doesExist(ptr,allPageItems.GetRef(i)))
			//	{
			//		//	CA("XML tags not found ");				
			//	}
			//}
		}

	}while(0);
	if(selectUIDList.Length()==0)
	{
		return kFalse;
	}
	return kTrue;
   }

// Checks if XML Tags are exist
bool16 CheckTag::doesExist(IIDXMLElement * ptr, UIDRef BoxRef)
{
	//CA(__FUNCTION__);
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
	IIDXMLElement *xmlPtr;
	TagList tList1;
	tList1=itagReader->getTagsFromBox(BoxRef, &xmlPtr);
	if(ptr==xmlPtr)
	{ 
		for(int32 j=0; j<tList1.size(); j++)
		{//CA("Inside check for Parent id");
			if(tList1[j].parentId==-1)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
				{
					tList1[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
		for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
		{
		tList1[tagIndex].tagPtr->Release();
		}
		return kTrue;
	}
	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	return kFalse;
}



void CheckTag::showTagInfo(UIDRef boxRef)
{
	//CA(__FUNCTION__);
	TagList tList;
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){ 
			return ;
		}
	tList=itagReader->getTagsFromBox(boxRef);
	PMString allInfo;
	for(int numTags=0; numTags<tList.size(); numTags++)
	{
		allInfo.Clear();
		allInfo.AppendNumber(PMReal(tList[numTags].elementId));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].parentId));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].imgFlag));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].sectionID));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].parentTypeID));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].typeId));
		allInfo+="\n";
		allInfo+="Start and end index :";
		allInfo.AppendNumber(PMReal(tList[numTags].startIndex));
		allInfo+=" / ";
		allInfo.AppendNumber(tList[numTags].endIndex);
		CAlert::InformationAlert(allInfo);
	}
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
}


UIDList CheckTag::GetTagList()
{
	//CA(__FUNCTION__);
	return taglist;
}

void CheckTag::SetTagList(UIDList & tlist)
{
	//CA(__FUNCTION__);
	taglist = tlist;
}

// Get the Tag Information from the Given BoxUIDRef and Store it in the "pData" structure
bool16 CheckTag::GetPageDataInfo(UIDList selectUIDList)
{	
	//CA(__FUNCTION__);
	BoxReader bReader;
	int i;
	rDataList.clear();

	if(selectUIDList.Length()==0)
		return kFalse;
	
	for(i=0; i<selectUIDList.Length(); i++)
	{
		//PageDataList pDataList;
		PageData pData;			
		bReader.getBoxInformation(selectUIDList.GetRef(i), pData);			
		this->appendToGlobalList(pData);			
	}
	return kTrue;
}


bool16 CheckTag::GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{
	//CA(__FUNCTION__);
	/*TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	for (TextIterator iter = begin; iter <= end; iter++)
	{
		const textchar characterCode = (*iter).GetTextChar();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		UTF16TextChar utfChar[2];
		 int32 len=2;
		 (*iter).ToUTF16(utfChar,&len);
		 ::CTUnicodeTranslator::Instance()->TextCharToChar(utfChar, 1, &buf, 1); 
		story.Append(buf);
	}*/
	//return kTrue;
	
	PMString asd("StartIndex: ");
	asd.AppendNumber(startIndex);
	asd.Append("\n end Index: ");
	asd.AppendNumber(finishIndex);
	//CA(asd);


	//CA(startIndex);
	//CA(finishIndex);

	TextIterator begin(iModel, startIndex +1);
	TextIterator end(iModel, finishIndex - 1 );
	WideString myWString;
	for (TextIterator iter = begin; iter <= end; iter++)
	{		
		/*UTF16TextChar utfChar[2];
		 int32 len=2;
		 (*iter).ToUTF16(utfChar,&len);		
		story.AppendW(utfChar);*/
		UTF32TextChar tc = *iter;
		myWString.Append(tc);
		
	}
	story.Append(myWString);
	//CA("557 CheckTag story : " + story);
	return kTrue;
}

// Append the Data from pData structure to the Global RefreshData Stucture
void CheckTag::appendToGlobalList(const PageData pData )
{
	//CA(__FUNCTION__);
	RefreshData rData;
	
	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		
		if(pData.objectDataList[i].elementList.size()==0)
			continue;
		rData.StartIndex= 0;
		rData.EndIndex= 0;
		rData.SubSectionID = pData.SubSectionID;;
		rData.BoxUIDRef=pData.BoxUIDRef;
		rData.boxID=pData.boxID;
		rData.elementID=-1;
		rData.isObject=kTrue;
		rData.isSelected=kFalse;
		rData.name=pData.objectDataList[i].objectName;
		rData.objectID=pData.objectDataList[i].objectID;
		rData.publicationID=pData.objectDataList[i].publicationID;
		rDataList.push_back(rData);
	
	
		for(int j=0; j<pData.objectDataList[i].elementList.size(); j++)
		{	
			rData.StartIndex= pData.objectDataList[i].elementList[j].StartIndex;
			rData.EndIndex= pData.objectDataList[i].elementList[j].EndIndex;
			rData.SubSectionID = pData.SubSectionID;
			rData.BoxUIDRef=pData.BoxUIDRef;
			rData.boxID=pData.boxID;
			rData.elementID=pData.objectDataList[i].elementList[j].elementID;
			rData.isTaggedFrame=pData.objectDataList[i].elementList[j].isTaggedFrame;
			rData.isObject=kFalse;
			rData.isSelected=kFalse;
			rData.name=pData.objectDataList[i].elementList[j].elementName;
			rData.objectID=pData.objectDataList[i].objectID;
			rData.publicationID=pData.objectDataList[i].publicationID;
			rDataList.push_back(rData);
		}
	}	
}

bool16 CheckTag::reSortTheListForObject(void)//Only to be called when the list has been resorted as Element
{
	//CA(__FUNCTION__);
	RefreshDataList theDataList=rDataList;
	rDataList.clear();
	
	for(int i=0; i<theDataList.size(); i++)
	{
		if(!theDataList[i].isObject || theDataList[i].isProcessed)
			continue;

		theDataList[i].isProcessed=kTrue;
		RefreshData referenceNode=theDataList[i];
		rDataList.push_back(theDataList[i]);//Added the Object
		RefreshData nodeToAdd;

		for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
		{	
			if(!theDataList[j].isObject)
			{
				nodeToAdd=theDataList[j];
				continue;
			}
			if(theDataList[i].objectID==theDataList[j].objectID)
			{
				nodeToAdd.objectID=referenceNode.objectID;
				nodeToAdd.isSelected=theDataList[j].isSelected;
				rDataList.push_back(nodeToAdd);//Added the Element
				theDataList[j].isProcessed=kTrue;
			}
		}
	}
	return kTrue;
}


bool16 CheckTag::reSortTheListForElem(void)//We want to group by element
{
	//CA(__FUNCTION__);
	RefreshDataList theDataList=rDataList;
	rDataList.clear();
	
	for(int i=0; i<theDataList.size(); i++)
	{
		if(theDataList[i].isObject || theDataList[i].isProcessed)
			continue;

		theDataList[i].isProcessed=kTrue;
		rDataList.push_back(theDataList[i]);//Added the element
		RefreshData nodeToAdd;

		for(int j=0; j<theDataList.size(); j++)//Now adding the objects which have these elements
		{	
			if(theDataList[j].isObject)
			{
				nodeToAdd=theDataList[j];
				continue;
			}

			if(theDataList[i].elementID==theDataList[j].elementID && theDataList[j].objectID==nodeToAdd.objectID)
			{
				rDataList.push_back(nodeToAdd);//Added the Object
				theDataList[j].isProcessed=kTrue;
			}
		}
	}

	return kTrue;
}