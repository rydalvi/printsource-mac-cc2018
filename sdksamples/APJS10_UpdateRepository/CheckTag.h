#ifndef __CHECKTAG_H__
#define __CHECKTAG_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"
#include "PageData.h"

#include "UIDList.h"	
class CheckTag
{
public:
	bool16 getTextSelectedBoxIds(UIDList&  selectUIDList);

	UIDList getParentSelectionsOnly(UIDList& theUIDList, IDataBase* database);

	bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxRef);

	void showTagInfo(UIDRef boxRef);
	
	UIDList GetTagList();
	void SetTagList(UIDList & tlist);

	bool16 getPageSelectedBoxIds(UIDList&  selectUIDList);

	bool16 getSpreadSelectedBoxIds(UIDList&  selectUIDList);

	bool16 getDocumentSelectedBoxIds(UIDList&  selectUIDList);

	bool16 GetPageDataInfo(UIDList selectUIDList);

	bool16 GetTextstoryFromBox( InterfacePtr<ITextModel>, int32, int32, PMString&);
	
	void appendToGlobalList(const PageData pData);

	bool16 reSortTheListForObject(void);

	bool16 reSortTheListForElem(void);//We want to group by element


private:
	UIDList taglist;		

};



#endif