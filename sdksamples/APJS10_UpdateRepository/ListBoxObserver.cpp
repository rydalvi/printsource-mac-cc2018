#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "UPDID.h"
#include "SystemUtils.h"
#include "RefreshData.h"
#include "ListBoxHelper.h"
#include "MediatorClass.h"
#include "IListControlData.h"
#include "IAppFramework.h"
#include "ITriStateControlData.h"


#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)

extern RefreshDataList rDataList;
extern RefreshDataList UniqueDataList;
extern int32 GroupFlag;
bool16 CheckBoxFlage=kTrue;
bool16 SelectBoxFlage=kFalse;

int32 CurrentSelectedRowno;

class ListBoxObserver : public CObserver
{
public:
	ListBoxObserver(IPMUnknown *boss);
	~ListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	//void updateListBox(bool16 isAttaching);
};

CREATE_PMINTERFACE(ListBoxObserver, kUPDPanelListBoxObserverImpl)

ListBoxObserver::ListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{

}


ListBoxObserver::~ListBoxObserver()
{

}

void ListBoxObserver::AutoAttach()
{	
	InterfacePtr<ISubject> subject(this , UseDefaultIID());
	if (subject != NULL)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
	//updateListBox(kTrue);
}


void ListBoxObserver::AutoDetach()
{
	//CA("ListBoxObserver::AutoDetach");
 	//updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != NULL)
	{
		subject->DetachObserver(this,IID_LISTCONTROLDATA);
	}
}


void ListBoxObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	
	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
	{   
		//edit by nitin---------
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(ptrIAppFramework == nil);
			return;
		}
		IControlView * iControlView=Mediator::iPanelCntrlDataPtr->FindWidget(kUPDSelectionBoxtWidgetID);
		if(iControlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDListBoxObserver::Update::iControlView is nil");		
			return;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==nil)
		{			
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDListBoxObserver::Update::itristatecontroldata is nil");				
			return;
		}
		//upto here-------------------		
		SDKListBoxHelper sList(this, kUPDPluginID);
		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
		if(listCntl == NULL) 
			return;

		K2Vector<int32> curSelection ;
		listCntl->GetSelected(curSelection ) ;
		const int kSelectionLength =  curSelection.Length();
		if(kSelectionLength<=0)
			return;

		CurrentSelectedRowno = curSelection[0];
		//88888888888888888888888888888888888888888888888888888888888888//
		/*PMString item("Item clicked is : ");
		item.AppendNumber(listCntl->GetClickItem());
		CA( item );*/

		/*InterfacePtr<IPanelControlData>ipanel(listCntl,UseDefaultIID());
		if(!ipanel)
		{
					CAlert::InformationAlert("panel is NULL");
					return;
		}
		IControlView * cellpanelControlView = ipanel->FindWidget(kCellPanelWidgetID);
		if(cellpanelControlView == NULL)
		{
					CAlert::InformationAlert("cell panel control view is null");					
					return;
		}
		InterfacePtr<IPanelControlData> cellPanelData(cellpanelControlView, UseDefaultIID());		
		if(cellPanelData == NULL)
		{
					CAlert::InformationAlert("Cell panel data is NULL");
					return;
		}
		
						
		IControlView* nameTextView = cellPanelData->GetWidget(curSelection[0]);
		if ( nameTextView == NULL )
			{
						CAlert::InformationAlert("Icontrolview of cell is NULL");
						return;
			}

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == NULL)
		{
						CAlert::InformationAlert("Cell Panel Data child is NULL");					
						return;
		}
		
		bool16 isCheckBoxSelected = kFalse;
		IControlView *check1View=cellPanelDataChild->GetWidget(0);
		IControlView *check2View=cellPanelDataChild->GetWidget(1);
		if(check1View->IsHilited() || check2View->IsHilited())
		{
					CA("Check box 1 is Hilited");
					isCheckBoxSelected = kTrue;
		}

		*/


		//if(isCheckBoxSelected)
		//{
			

			//	CA("CheckBoxes are selected");

		
		//88888888888888888888888888888888888888888888888888888888888888//
		if(GroupFlag != 1)
		{
		rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
		

		bool16 isGroupByObj=Mediator::isGroupByObj;

		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
		if(/*isGroupByObj*/GroupFlag == 2)//If its grouped by object
		{
			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
			{
				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
				{
					if(rDataList[j].isObject)
						break;
					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
				}
			}
			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
			{
				for(int j=curSelection[0]; j>=0; j--)
				{
					if(rDataList[j].isObject)
					{
						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
						rDataList[j].isSelected=kFalse;
						break;
					}
				}
			}
			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
			{
				for(int i=curSelection[0]; i<rDataList.size(); i++)
				{
					if(rDataList[i].isObject)
						break;
					if(!rDataList[i].isSelected)
						return;
				}
				int32 k=0;
				for(k=curSelection[0]; k>=0; k--)
				{
					if(rDataList[k].isObject)
						break;
		
					if(!rDataList[k].isSelected)
						return;
				}
				rDataList[k].isSelected=kTrue;
				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
			}
			///////////////////////////nitin////////////////////////////////////////////
			for(int i=0;i<rDataList.size();i++)
			{					
				if(rDataList[i].isSelected )
					CheckBoxFlage=kTrue;						
				else
				{						
					CheckBoxFlage=kFalse;
					if(itristatecontroldata->IsSelected())
					{							
						SelectBoxFlage=kTrue;//true
						itristatecontroldata->Deselect();
					}										
					break;
				}
			}
			if(CheckBoxFlage==kTrue)
			{
				SelectBoxFlage=kTrue;
				itristatecontroldata->Select();
			}
			/////////////////////////upto here///////////////////////////////////////////
		}
		///////////////////////////[ GROUPED BY ELEMENT ]//////////////////////////////
		else if(GroupFlag == 3)//Its grouped by element
		{
			if(!(rDataList[curSelection[0]].isObject) && curSelection[0]<rDataList.size())
			{
				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
				{
					if(!rDataList[j].isObject)
						break;
					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
				}
			}
			else if(rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
			{
				for(int j=curSelection[0]; j>=0; j--)
				{
					if(!rDataList[j].isObject)
					{
						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
						rDataList[j].isSelected=kFalse;
						break;
					}
				}
			}
			else if(rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
			{
				for(int i=curSelection[0]; i<rDataList.size(); i++)
				{
					if(!rDataList[i].isObject)
						break;
					if(!rDataList[i].isSelected)
						return;
				}
				int32 k=0;
				for(k=curSelection[0]; k>=0; k--)
				{
					if(!rDataList[k].isObject)
						break;
					if(!rDataList[k].isSelected)
						return;
				}
				rDataList[k].isSelected=kTrue;
				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
			}
				///////////////////////nitin////////////////////////////////////////////////
				for(int i=0;i<rDataList.size();i++)
				{
					if(rDataList[i].isSelected )
						CheckBoxFlage=kTrue;						
					else
					{						
						CheckBoxFlage=kFalse;
						if(itristatecontroldata->IsSelected())
						{							
							SelectBoxFlage=kTrue;
							itristatecontroldata->Deselect();							
						}										
						break;
					}
				}
				if(CheckBoxFlage==kTrue)
				{					
					SelectBoxFlage=kTrue;
					itristatecontroldata->Select();								
				}
				///////////////upto here///////////////////
			}
		}		
		else if(GroupFlag == 1)
		{
			UniqueDataList[curSelection[0]].isSelected=(UniqueDataList[curSelection[0]].isSelected)? kFalse: kTrue;		
			sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], UniqueDataList[curSelection[0]].isSelected);		
			for(int j=0; j<rDataList.size(); j++)
			{				
				if(UniqueDataList[curSelection[0]].elementID == rDataList[j].elementID /*&& UniqueDataList[curSelection[0]].TypeID ==  rDataList[j].TypeID*/ && UniqueDataList[curSelection[0]].whichTab == rDataList[j].whichTab)
				{					
					if(UniqueDataList[curSelection[0]].whichTab != 4 || UniqueDataList[curSelection[0]].isImageFlag == kTrue || UniqueDataList[curSelection[0]].isTableFlag == kTrue)
					{						
						if(UniqueDataList[curSelection[0]].TypeID !=  rDataList[j].TypeID)
							continue;							
					}
					if(UniqueDataList[curSelection[0]].whichTab == 4 && UniqueDataList[curSelection[0]].elementID == -101 ) // for Item Table in Tabbed text
					{						
						if( UniqueDataList[curSelection[0]].TypeID !=  rDataList[j].TypeID)
							continue;							
					}					
					rDataList[j].isSelected = UniqueDataList[curSelection[0]].isSelected;				
				}
			}
			/////////////////////////////nitin////////////////////////////////////////////
			//for(int i=0;i<rDataList.size();i++)
			for(int i=0;i<UniqueDataList.size();i++)				
			{						
				if( rDataList[i].isSelected )
					CheckBoxFlage=kTrue;						
				else
				{						
					CheckBoxFlage=kFalse;
					if(itristatecontroldata->IsSelected())
					{	
						SelectBoxFlage=kTrue;//true
						itristatecontroldata->Deselect();				
					}											
					break;
				}
			}
			if(CheckBoxFlage==kTrue)
			{
				SelectBoxFlage=kTrue;
				itristatecontroldata->Select();									
			}
			///////////////////////upto here///////////////////////////////////

		}

	}
}

//void ListBoxObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
//{
//	//if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
//	//{
//		/*
//		SDKListBoxHelper sList(this, kUPDPluginID);
//		//InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
//		IControlView * listBox = sList.FindCurrentListBox();
//		if(listCntl == NULL) 
//			return;
//
//		K2Vector<int32> curSelection ;
//		listCntl->GetSelected(curSelection ) ;
//		const int kSelectionLength =  curSelection.Length();
//		if(kSelectionLength<=0)
//			return;
//		//rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
//		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
//		
//
//		bool16 isGroupByObj=Mediator::isGroupByObj;
//
//		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
//
//		if(isGroupByObj)//If its grouped by object
//		{
//			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
//			{
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//					
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//		}
//
//		///////////////////////////[ GROUPED BY ELEMENT ]//////////////////////////////
//
//		else//Its grouped by element
//		{
//			if(!(rDataList[curSelection[0]].isObject) && curSelection[0]<rDataList.size())
//			{
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(!rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(!rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(!rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(!rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//
//		}
//		*/
//	//}
//	
//	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
//	{
//	
//		SDKListBoxHelper sList(this, kUPDPluginID);
//		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
//		if(listCntl == NULL) 
//			return;
//
//		K2Vector<int32> curSelection ;
//		listCntl->GetSelected(curSelection ) ;
//		const int kSelectionLength =  curSelection.Length();
//		if(kSelectionLength<=0)
//			return;
//	
//		rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
//		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
//
//		int32 OBjectID=rDataList[curSelection[0]].objectID;
//		int32 PubId=rDataList[curSelection[0]].publicationID;
//		bool16 isGroupByObj=Mediator::isGroupByObj;
//
//		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
//
//		if(isGroupByObj)//If its grouped by object
//		{
//			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
//			{
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//					
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				int32 i=0;
//				for(i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				for(i=curSelection[0]; i>=0; i--)
//				{
//					if(rDataList[i].isObject)
//						break;
//		
//					if(!rDataList[i].isSelected)
//						return;
//				}
//					rDataList[i].isSelected=kTrue;
//					sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
//				}
//			}
//
//
//	}
//}
// 
//void ListBoxObserver::updateListBox(bool16 isAttaching)
//{
//	/*CA("Inside UpdateListBox");
//	
//	SDKListBoxHelper listHelper(this, kUPDPluginID);
//	InterfacePtr<IPanelControlData>panel(this,UseDefaultIID());
//	if(!panel)
//	{
//		CA("IPanelControlData is NULL");
//		return;
//	}
//	//Mediator::iPanelCntrlDataPtr = panel;
//	//listHelper.EmptyCurrentListBox(panel,1);
//	
//	if(isAttaching) {
//		
//		
//		do {
//				
//			CA("Inside isAttaching");
//
//			
//				InterfacePtr<IControlView>listBoxControl(this,UseDefaultIID());
//				if(!listBoxControl)
//				{
//					CA("IControlView of listbox is NULL");
//					break;
//				}
//				Mediator::listControlView = listBoxControl;
//				PMString displayname("temp");
//				
//				listHelper.AddElement(listBoxControl, displayname, kUPDUnCheckIconWidgetID);
//				listHelper.AddElement(listBoxControl, displayname, kUPDCheckIconWidgetID);
//				listHelper.AddElement(listBoxControl, displayname, kUPDPanelTextWidgetID);
//
//				
//
//					
//		
//				
//
//		   } while(0);
//	}*/
//	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
//	{
//	
//		SDKListBoxHelper sList(this, kUPDPluginID);
//		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
//		if(listCntl == NULL) 
//			return;
//
//		K2Vector<int32> curSelection ;
//		listCntl->GetSelected(curSelection ) ;
//		const int kSelectionLength =  curSelection.Length();
//		if(kSelectionLength<=0)
//			return;
//	
//		rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
//		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
//
//		int32 OBjectID=rDataList[curSelection[0]].objectID;
//		int32 PubId=rDataList[curSelection[0]].publicationID;
//		bool16 isGroupByObj=Mediator::isGroupByObj;
//
//		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
//
//		//if(isGroupByObj)//If its grouped by object
//		{
//			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
//			{
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//					
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				int32 i=0;
//				for(i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				for(i=curSelection[0]; i>=0; i--)
//				{
//					if(rDataList[i].isObject)
//						break;
//		
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				rDataList[i].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
//			}
//		}
//
//
//	}
//	
//}
