#ifndef __PAGEDATA_H__
#define __PAGEDATA_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"
#include "vector"

using namespace std;

class TagInfo
{
public:
	double elementID;
	double typeID;
	int32 header;		// if 1 indicates its a Header (Display Name ) field
	bool16 isEventField; //for Event level attribute if true its 
	int32 deleteIfEmpty;// -1 = not applied, 1= delete If Empty true (Delete empty tags after spray)
	int32 dataType;		// Type of Attribute -1= Normal Attributes, 1 = Possible Value, 2= Image Description , 3 = Image Name ,4 = Table name , 5 = Table Comments
	int32 languageid;
	int32 whichTab;
	double pbObjectId;  // Pb object id for publication items and product in case of ONEsource its -1
	double childId;	// in case of Table Child Item's ID
	int32 catLevel;		// Category Level in case of Category Attributes otherwise -1
	int32 imageFlag;
	int32 imageIndex;	// image index for PV, Brand, Manufacturer & Supplier images.
	int32 flowDir;		// 0 = Horizontal Flow, 1= Verticle Flow, -1 = No flow is define its in case of all Images or PV images
	int32 childTag;		// if 1 indicates Child item tag 
	int32 tableFlag;
	int32 tableType;	// displays different types of tables 1 = DB Table, 2 = Custom Table, 3 = Advance Table, 4 = Component , 5 = Accessaries, 6 XRef, 7 = All Standard Table
	double tableId;		// Table ID after spray

	PMString	elementName;
	int32 StartIndex;
	int32 EndIndex;
	bool16 isTaggedFrame;
	

	TagInfo()
	{
		elementID=-1;
		typeID=-1;
		header=-1;
		isEventField = kFalse;
		deleteIfEmpty= -1;
		dataType = -1;
		languageid = -1;
		whichTab=-1;
		pbObjectId = -1;
		childId = -1;
		catLevel = -1;
		imageFlag = 1;
		imageIndex = 1;
		flowDir = -1;
		childTag = -1;
		tableFlag = -1;
		tableType = -1;
		tableId = -1;
		
		isTaggedFrame=kFalse;
		
		
		StartIndex = -1;
		EndIndex = -1;

	}
};

typedef vector<TagInfo> ElementInfoList;

class ObjectData
{
public:
	double			objectID;
	double			objectTypeID;
	double			publicationID;
	PMString		objectName;
	bool16			isTableFlag;
	bool16			isImageFlag;
	int32			whichTab;
	ElementInfoList elementList;
	ObjectData()
	{
		objectID=-1;
		objectTypeID=-1;
		publicationID=-1;
		whichTab=-1;
	}
};



typedef vector<ObjectData> ObjectDataList;

class PageData
{
public:
	UIDRef BoxUIDRef;
	UID boxID;
	//Yogesh
	double SubSectionID;
	ObjectDataList objectDataList;
	PageData()
	{
		boxID=kInvalidUID;
	}
};
class TextVector
{
public:
PMString Word;
int32 StartIndex;
int32 EndIndex;
TextVector()
{
StartIndex=0;
EndIndex=0;
}
};
typedef vector<TextVector> TextVectorList;

#endif