#ifndef __REFRESHDATA_H__
#define __REFRESHDATA_H__

#include "VCPluginHeaders.h"
#include <vector>

using namespace std;

class RefreshData
{
public:
	UIDRef BoxUIDRef;
	int32 StartIndex;
	int32 EndIndex;
	UID boxID;
	double objectID;
	double publicationID;
	
	double SubSectionID;
	double elementID;
	PMString name;
	bool16 isSelected;
	bool16 isObject;
	bool16 isTaggedFrame;
	bool16 isProcessed;
	bool16 isTableFlag;
	bool16 isImageFlag;
	double TypeID;
	int32 whichTab;
	double LanguageID;
	bool16 isElementUpdated ; //Flag for storing status of Each element when we update to Database it changes the state updated successfully(kTrue) & when Not Updated successfully flag is (kFalse) 
	double childId;	// in case of Table Child Item's ID
	int32 childTag;		// if 1 indicates Child item tag
	int32 tableType;	// displays different types of tables 1 = DB Table, 2 = Custom Table, 3 = Advance Table, 4 = Component , 5 = Accessaries, 6 XRef, 7 = All Standard Table
	double tableId;		// Table ID after spray
	int32 iconDispayHint; // 0= deactivated, 2 = Failed, 1= Success

	RefreshData()
	{
		boxID=kInvalidUID;
		objectID=-1;
		publicationID=-1;
		//Yogesh
		SubSectionID = -1;
		elementID=-1;
		isSelected=kFalse;
		isObject=kFalse;
		isTaggedFrame=kFalse;
		isProcessed=kFalse;
		isTableFlag=kFalse;
		StartIndex=0;
		EndIndex=0;
		TypeID=-1;
		whichTab=-1;
		isImageFlag=kFalse;
		LanguageID = -1;
		isElementUpdated =kFalse; //Added
		childId = -1;
		childTag = -1;
		tableType = -1;
		tableId = -1;
		iconDispayHint =0;
	}
};

typedef vector<RefreshData> RefreshDataList;

#endif