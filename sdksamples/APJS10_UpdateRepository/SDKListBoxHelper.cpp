#include "VCPlugInHeaders.h"
#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "PersistUtils.h"
//#include "PalettePanelUtils.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "UPDID.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "ISpecialChar.h"
#include "MediatorClass.h"

/// Global Pointers
extern ISpecialChar* iConverter;
///////////

#define CA(x)	CAlert::InformationAlert(x)

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{
}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=NULL;
	fOwnerPluginID=0;
}

IControlView* SDKListBoxHelper ::FindCurrentListBox(InterfacePtr<IPanelControlData> iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	if(!verifyState())
		return NULL;

	IControlView * listBoxControlView2 = NULL;

	do {
		if(iPanelControlData ==NULL)
		{
			break;
		}
		if(i==1)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kUPDPanelLstboxWidgetID);
		}
		
		if(listBoxControlView2 == NULL) 
			break;

	} while(0);
	return listBoxControlView2;
}

void SDKListBoxHelper::AddElement(IControlView* lstboxControlView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{
	//CA("Inside AddElement");
	if(!verifyState())
		return;
	do	
	{
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == NULL) 
			break;	
		
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID==0)
			break;

		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		if(newElView==NULL) 
			break;

		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x, isObject);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == NULL) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != NULL, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==NULL) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == NULL) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != NULL, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==NULL) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == NULL) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != NULL, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==NULL) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==NULL) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != NULL, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == NULL) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != NULL, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == NULL) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != NULL,"SDKListBoxHelper::removeCellWidget() cellPanelData NULL"); 
		if(cellPanelData == NULL) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/

void SDKListBoxHelper::addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{
	if(elView == NULL || lstboxControlView == NULL ) 
		return;
	do {
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == NULL) 
			break;
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ((nameTextView == NULL)) 
			break;

		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == NULL) 
			break;

		// Awasthi
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(displayName));
		// End Awasthi

		newEltext->SetString(stringToInsert, kTrue, kTrue);
		
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != NULL, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == NULL) 
			break;

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != NULL, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == NULL) 
			break;

		if(!isObject)
		{
			int indent=10;
			PMRect cellRect;
			elView->GetFrame();
			cellRect.Left(cellRect.Left()+indent);
			cellRect.Right(cellRect.Right()+indent);
			elView->SetFrame(cellRect);
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != NULL, "SDKListBoxHelper::addListElementWidget()  cellPanelData NULL");
		if(cellPanelData == NULL) 
			break;
		cellPanelData->AddWidget(elView);				

		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == NULL)  
			break;
		listData->Add(elView);	
	} while(0);
}

void SDKListBoxHelper::CheckUncheckRow(IControlView *listboxCntrlView, int32 index, bool checked)
{
	do
	{
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == NULL) 
			break;
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == NULL) 
			break;
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == NULL)
			break;

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == NULL)
			break;
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == NULL ) 
			break;

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == NULL)
			break;

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}
		
		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==NULL)
			break;

		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==NULL)
			break;

		childHideView->Hide();
		childShowView->Show();	
	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox(InterfacePtr<IPanelControlData> panel, int x)
{
	do {
		//IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		//if(listBoxControlView == NULL) 
		//	break;
		InterfacePtr<IListControlData> listData (Mediator::listControlView, UseDefaultIID());
		if(listData == NULL) 
			break;
		InterfacePtr<IPanelControlData> iPanelControlData(Mediator::listControlView, UseDefaultIID());
		if(iPanelControlData == NULL) 
			break;
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == NULL) 
			break;
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == NULL) 
			break;
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		//listBoxControlView->Invalidate();
	} while(0);
}

//Added by Yogesh
//Updates the Signal Icon shows White Signal if ther is no Update action occur, Red if Update Fails & Green if Update Successful.
void SDKListBoxHelper::UpdateSignalRow(IControlView *listboxCntrlView, int32 index, int32 Check)
{
	do
	{	
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == NULL) 
			break;
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == NULL) 
			break;
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == NULL)
			break;
		
		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == NULL)
			break;
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == NULL ) 
			break;
		
		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == NULL)
			break;
		
		
		IControlView *childwhiteView=cellPanelDataChild->GetWidget(4);
		if(childwhiteView==NULL)
			break;
			
		IControlView *childredView=cellPanelDataChild->GetWidget(3);
		if(childredView==NULL)
			break;
		
		IControlView *childgreenView=cellPanelDataChild->GetWidget(2);
		if(childgreenView==NULL)
			break;
		
		if(Check == 0)				//normal
		{
			childredView->Hide();
			childgreenView->Hide();
			childwhiteView->Show();
		}
		else if(Check == 1)			//sucess
		{
			childredView->Hide();
			childwhiteView->Hide();
			childgreenView->Show();
		}
		else if(Check == 2)			//failuer
		{
			childwhiteView->Hide();
			childgreenView->Hide();	
			childredView->Show();
		}

	}while(kFalse);
}
//end by Yogesh
