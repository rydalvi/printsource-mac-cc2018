#include "VCPluginHeaders.h"
#include "TagWriter.h"

PMString TagWriter::prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;
		if(name.GetChar(i) == ' ' )
			tagName.Append("_");
		else tagName.Append(name.GetChar(i));
	}
	return tagName;
}


XMLReference TagWriter::TagFrameElement(const XMLReference& newElementParent, UID frameUID, const PMString& frameTagName)
{
	XMLReference resultXMLRef=kInvalidXMLReference;
	
	do {
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef);
		if (errCode != kSuccess)
			break;
		
		if (resultXMLRef == kInvalidXMLReference)
			break;
		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
	}while (false);
	return resultXMLRef;
}

void TagWriter::addTagToGraphicFrame(UIDRef curBox,TagStruct& tStruct, PMString storyTagName)
{
	do{
		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
		if(unknown == NULL)
			break;

		bool16 isTextFrame = Utils<IFrameUtils>()->IsTextFrame(unknown);
		if(isTextFrame)
			return;

		InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
		if(doc == NULL)
			break;
		
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
		if (rootElement==NULL)
			break;
				
		XMLReference parent = rootElement->GetXMLReference();

		XMLReference storyXMLRef = TagFrameElement(parent, curBox.GetUID(), storyTagName);
		if (storyXMLRef==kInvalidXMLReference)
			break;
		
		PMString attribName("ID");
		PMString attribVal("");
		attribVal.AppendNumber(PMReal(tStruct.elementId));
		ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal)); //cs4

		attribName.Clear();
		attribVal.Clear();
		attribName = "typeId";
		attribVal.AppendNumber(PMReal(tStruct.typeId));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "index";
		attribVal.AppendNumber(tStruct.whichTab);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "imgFlag";
		attribVal.AppendNumber(tStruct.imgFlag);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "parentId";
		attribVal.AppendNumber(PMReal(tStruct.parentId));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "reserved";
		attribVal.AppendNumber(PMReal(tStruct.sectionID));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "parentTypeID";
		attribVal.AppendNumber(PMReal(tStruct.parentTypeID));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(storyXMLRef,WideString(attribName),WideString(attribVal));
		
	}while(kFalse);
}