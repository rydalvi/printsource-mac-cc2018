#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
//#include "IPaletteMgr.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "ILayoutUtils.h" //Cs4

// Project includes:
#include "UPDID.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

*/
class UPDActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		UPDActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be NULL. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		void DoPalette();
		void UpdateActionStates(IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget);
		void CloseUpdateRepositoryPalette();
	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
	       /** Encapsulates functionality for the MenuItem1 menu item. */
		void DoMenuItem1(IActiveContext* ac);
		void DoMenuItem2(IActiveContext* ac);
		void DoMenuItem3(IActiveContext* ac);
		void DoMenuItem4(IActiveContext* ac);

		static IPaletteMgr* palettePanelPtr;


};