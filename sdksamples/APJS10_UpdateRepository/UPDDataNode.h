#ifndef __UPDDATANODE_H__
#define __UPDDATANODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
//#include "vector"

using namespace std;

class UPDDataNode
{
private:
	PMString fieldName;
	int32 fieldId;
	int32 ParentId;
	int32 seqNumber; // corresponding seq number of UniqueAttributeList
	int32 childCount;
	int32 isSelected; 
	int32 hitCount;
	int32 iconDispayHint; // 0= deactivated, 2 = Failed, 1= Success
	
public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	UPDDataNode():fieldName(""), fieldId(0),  childCount(0), isSelected(0)	, seqNumber(0) , hitCount(0) , iconDispayHint(0)	{}
	/*
		One time access to all the private members
	*/
	void setAll(PMString pubName, int32 fieldId,int32 ParentId, int32 seqNumber, int32 childCount, int32 isSelected , int32 hitCount, int32 iconDispayHint)
	{
		this->fieldName=pubName;
		this->fieldId=fieldId;
		this->ParentId = ParentId;	
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->isSelected=isSelected;
		this->hitCount=hitCount;
		this->iconDispayHint = iconDispayHint;
	}
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getFieldName(void) { return this->fieldName; }
	/*
		@returns the id of the publication
	*/
	int32 getFieldId(void) { return this->fieldId; }
		
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int32 getHitCount(void) { return this->hitCount; }

	int32 getIsSelected(void) { return this->isSelected; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	int32 getParentId(void) { return this->ParentId; }

	void setParentId(int32 ParentId) { this->ParentId = ParentId; }
	void setFieldId(int32 fieldId) { this->fieldId=fieldId; }	
	
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setFieldName(PMString& pubName) { this->fieldName=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int32 hitCount) { this->hitCount=hitCount; }
	void setIsSelected(int32 isSelected) { this->isSelected=isSelected; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	void setIconDispayHint(int32 hint) { this->iconDispayHint=hint; }
	int32 getIconDispayHint() { return this->iconDispayHint; }
};

typedef vector<UPDDataNode> RfhDataNodeList;

#endif