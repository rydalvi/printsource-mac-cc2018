#include "VCPlugInHeaders.h"
#include "UPDPlugInEntrypoint.h"
#include "CAlert.h"
#include "UPDID.h"
#include "UPDLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"

#define CA(z) CAlert::InformationAlert(z)

#ifdef WINDOWS
	ITypeLib* UPDPlugInEntrypoint::fSPTypeLib = NULL;
#endif

bool16 UPDPlugInEntrypoint::Load(ISession* theSession)
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IRegisterLoginEvent> regEvt
		((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt)
		{
			//CA("Invalid regEvt");
			break;
		}
		regEvt->registerLoginEvent(kUPDLoginEventsHandler); 
	}while(kFalse);
	return kTrue;
}

/* Global
*/
static UPDPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}
