
#include "VCPlugInHeaders.h"								
// Interface headers
#include "ITextTarget.h"
#include "ITextModel.h"
#include "ITextFocus.h"
#include "ITextParcelList.h"
#include "IParcelList.h"
//#include "ITextFrame.h"
#include "IFrameList.h"	
// General includes:
#include "CPMUnknown.h"
#include "UIDList.h"
// Project includes:
#include "UPDID.h"
//#include "TinMutHelper.h"
#include "IUPDSuit.h"

class UPDSuiteTextCSB : public CPMUnknown<IUPDSuite>
{
public:
	UPDSuiteTextCSB(IPMUnknown* boss);
	virtual ~UPDSuiteTextCSB(void);
	virtual bool16 CanGetTextInsets(void);
	virtual void GetTextInsets(TextInsets& textInsets);
	virtual bool16 CanApplyTextInset(void);
	virtual ErrorCode ApplyTextInset(const PMReal& insetValue);
	virtual ErrorCode GetTextFocus(ITextFocus * &);
private:
	
	//UIDList GetItemList(void);

	//UIDList GetItemListNotTableAware(void);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID making the C++ code callable by the application.
*/
CREATE_PMINTERFACE (UPDSuiteTextCSB, kUPDSuiteTextCSBImpl)

/*
*/
UPDSuiteTextCSB::UPDSuiteTextCSB(IPMUnknown* boss) :
	CPMUnknown<IUPDSuite>(boss)
{
}

/*
*/
UPDSuiteTextCSB::~UPDSuiteTextCSB(void)
{
}

/*
*/
bool16 UPDSuiteTextCSB::CanGetTextInsets(void)
{
	/*bool16 result = kFalse;
	do {
		UIDList itemList = this->GetItemList();
		if (itemList.Length() <= 0) {	
			break;
		}
		TinMutHelper tinMutHelper;
		result = tinMutHelper.CanGetTextInsets(itemList);
	} while (false);
	*/
	return 0;
}

/*
*/
void UPDSuiteTextCSB::GetTextInsets(TextInsets& textInsets)
{
/*	do {
		UIDList itemList = this->GetItemList();
		if (itemList.Length() <= 0) {	
			break;
		}
		TinMutHelper tinMutHelper;
		tinMutHelper.GetTextInsets(itemList, textInsets);
	} while (false);*/
}

/*
*/
bool16 UPDSuiteTextCSB::CanApplyTextInset(void)
{
	return this->CanGetTextInsets();
}

/*
*/
ErrorCode UPDSuiteTextCSB::ApplyTextInset(const PMReal& insetValue)
{
	ErrorCode status = kFailure;
/*	do {
		UIDList itemList = this->GetItemList();
		if (itemList.Length() <= 0) {	
			break;
		}
		TinMutHelper tinMutHelper;
		status = tinMutHelper.ProcessSetTextInsetCmd(itemList, insetValue);				
	} while(false);
*/	
	return status;
}

/*
*/
/*
UIDList RfhSuiteTextCSB::GetItemList(void)
{
	UIDList result;
	do {
#if 0
		// Gather the list of pageItems (splines) in the selection.
		InterfacePtr<ITextTarget> theTarget(this, UseDefaultIID());
		InterfacePtr<ITextModel> textModel(theTarget->QueryTextModel());

		UIDList splineList(::GetDataBase(textModel));
		// The following will result in crash as of DT207 when the text focus is in a table cell.
		// It works on regular text frame though, that's why I am leaving this here just in case
		Utils<ITextUtils>()->GetSelectedTextItemsFromTextTarget(theTarget, NULL, NULL, &splineList);
		result = splineList;
#endif
		// Find the text model and range of text selected from
		// the text target.
		InterfacePtr<ITextTarget> textTarget(this, UseDefaultIID());
		if (textTarget == NULL) {
			break;
		}
		result = UIDList(textTarget->GetTextModel().GetDataBase());
		InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel ());
		if (textModel == NULL)
			break;
		InterfacePtr<ITextFocus> textFocus(textTarget->QueryTextFocus());
		if (textFocus == NULL)
			break;

		// Find the index of the parcel that contains the first character
		// in the range of text selected.
		TextIndex startTextIndex = textFocus->GetStart(NULL);
		if (startTextIndex == kInvalidTextIndex)
			break;
		InterfacePtr<ITextParcelList> textParcelList(textModel->QueryTextParcelList(startTextIndex));
		if (textParcelList == NULL) {
			break;
		}
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		if (parcelList == NULL)
			break;
		ParcelKey startParcel = textParcelList->GetParcelContaining(startTextIndex);
		if (!startParcel.IsValid()) {
			break;
		}

		// Find the index of the parcel that contains the last character
		// in the range of text selected.
		TextIndex endTextIndex = textFocus->GetEnd();
		ParcelKey endParcel;
		if (endTextIndex != kInvalidTextIndex)
			endParcel = textParcelList->GetParcelContaining(endTextIndex);
		else
			endParcel = parcelList->GetLastParcelKey();
		if (!endParcel.IsValid()) {
			break;
		}
		
		// Find each frame(spline) touched by the text selection.
		TinMutHelper tinMutHelper;
		IParcelList::const_iterator iter = parcelList->begin(startParcel);
		IParcelList::const_iterator end = parcelList->end(endParcel);
		for (; iter != end; ++iter)
		{
			InterfacePtr<ITextFrame> textFrame(parcelList->QueryParcelFrame(*iter));
			UID graphicFrameUID = tinMutHelper.FindAssociatedGraphicFrame(textFrame);
			if (graphicFrameUID != kInvalidUID)
			result.Append(graphicFrameUID);
		}
		

	} while(false);
	return result;
}

/*
Interesting to compare this implementation with the one above. This one is
not aware of tables. If a text selection exists inside a table this implementation
can't detect the frames associated with it. The one above can.
*/
/*
UIDList RfhSuiteTextCSB::GetItemListNotTableAware(void)
{
	UIDList result;
	do {
		InterfacePtr<ITextTarget> textTarget(this, UseDefaultIID());
		if (textTarget == NULL) {
			break;
		}
		result = UIDList(textTarget->GetTextModel().GetDataBase());
		InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel ());
		if (textModel == NULL)
			break;
		InterfacePtr<IFrameList> frameList(textModel->QueryFrameList ());
		if (frameList == NULL)
			break;
		InterfacePtr<ITextFocus> textFocus(textTarget->QueryTextFocus());
		if (textFocus == NULL)
			break;

		//Get the frame index of the first selected character
		int32 firstFrameIndex = frameList->GetFrameIndexContaining(textFocus->GetStart(NULL), kFalse);
		//Get the frame index of the last selected character
		int32 lastFrameIndex = frameList->GetFrameIndexContaining(textFocus->GetEnd(), kFalse);

		// Find each graphic frame(spline) touched by the text selection.
		TinMutHelper tinMutHelper;
		for (int32 i = firstFrameIndex; i <= lastFrameIndex; i++) {
			 InterfacePtr<ITextFrame> textFrame(frameList->QueryNthFrame(i));
			 UID graphicFrameUID = tinMutHelper.FindAssociatedGraphicFrame(textFrame);
			 if (graphicFrameUID != kInvalidUID)
				result.Append(graphicFrameUID);
		}

	} while(false);
	return result;
}
*/

ErrorCode UPDSuiteTextCSB::GetTextFocus(ITextFocus* & tempFocus)
{
	InterfacePtr<ITextTarget> iTextTarget(this, UseDefaultIID());
	InterfacePtr<ITextFocus> iFocus(iTextTarget->QueryTextFocus());
	tempFocus = iFocus;
	return 1;

}
// End, RfhSuiteTextCSB.cpp.




