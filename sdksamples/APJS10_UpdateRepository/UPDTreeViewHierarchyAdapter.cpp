#include "VCPlugInHeaders.h"
#include "CPMUnknown.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IUIDData.h"
#include "UPDID.h"
#include "IntNodeID.h"
#include "UPDTreeModel.h"


class UPDTreeViewHierarchyAdapter : public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
	UPDTreeViewHierarchyAdapter(IPMUnknown* boss);
	virtual ~UPDTreeViewHierarchyAdapter();
	virtual NodeID_rv	GetRootNode() const;
	virtual NodeID_rv	GetParentNode( const NodeID& node ) const;
	virtual int32		GetNumChildren( const NodeID& node ) const;
	virtual NodeID_rv	GetNthChild( const NodeID& node, const int32& nth ) const;
	virtual int32		GetChildIndex( const NodeID& parent, const NodeID& child ) const;
	virtual NodeID_rv	GetGenericNodeID() const;
	virtual bool16  ShouldAddNthChild( const NodeID& node, const int32& nth ) const { return kTrue; }

private:
	UPDTreeModel	fBscTreeModel;
};	

CREATE_PMINTERFACE(UPDTreeViewHierarchyAdapter, kUPDTreeViewHierarchyAdapterImpl)

UPDTreeViewHierarchyAdapter::UPDTreeViewHierarchyAdapter(IPMUnknown* boss) : 
	CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
}

UPDTreeViewHierarchyAdapter::~UPDTreeViewHierarchyAdapter()
{
}

NodeID_rv	UPDTreeViewHierarchyAdapter::GetRootNode() const
{
	int32 rootUID = fBscTreeModel.GetRootUID();
	return IntNodeID::Create(rootUID);
}

NodeID_rv	UPDTreeViewHierarchyAdapter::GetParentNode( const NodeID& node ) const
{
	do 
	{
		TreeNodePtr<IntNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break; 

		int32 uid = uidNodeID->Get();
		if(uid == fBscTreeModel.GetRootUID()) 
			break;
		
		ASSERT(uid != kInvalidUID);
		if(uid == kInvalidUID)
			break;
		
		int32 uidParent = fBscTreeModel.GetParentUID(uid);
		if(uidParent != kInvalidUID) 
			return IntNodeID::Create(uidParent);

	}while(kFalse);
	return kInvalidNodeID;	
}

int32 UPDTreeViewHierarchyAdapter::GetNumChildren( const NodeID& node ) const
{
	int32 retval=0;
	do 
	{
		TreeNodePtr<IntNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break;
		
		int32 uid = uidNodeID->Get();
		if(uid == kInvalidUID) 
			break;
		
		if(uid == fBscTreeModel.GetRootUID()) 
			retval = fBscTreeModel.GetRootCount();
		else 
			retval = fBscTreeModel.GetChildCount(uid);
		
	} while(kFalse);
	return retval;
}

NodeID_rv	UPDTreeViewHierarchyAdapter::GetNthChild( const NodeID& node, const int32& nth ) const
{
	TreeNodePtr<IntNodeID>	uidNodeID(node);
	if( uidNodeID != nil)
	{
		int32 uidChild = 0 ; //kInvalidUID;
		if(uidNodeID->Get() == fBscTreeModel.GetRootUID()) 
		{
			uidChild = fBscTreeModel.GetNthRootChild(nth);
		}
		else 
		{
			uidChild = fBscTreeModel.GetNthChildUID(uidNodeID->Get(), nth);			
		}

		if(uidChild != kInvalidUID)
			return IntNodeID::Create(uidChild);
	}
	return kInvalidNodeID;	
}

int32 UPDTreeViewHierarchyAdapter::GetChildIndex
	(const NodeID& parent, const NodeID& child ) const
{
	do 
	{
		TreeNodePtr<IntNodeID>	parentUIDNodeID(parent);
		ASSERT(parentUIDNodeID);
		if(parentUIDNodeID==nil) 
			break;
		
		TreeNodePtr<IntNodeID>	childUIDNodeID(child);
		ASSERT(childUIDNodeID);
		if(childUIDNodeID==nil) 
			break;
		
		if(parentUIDNodeID->Get() == kInvalidUID) 
			break;
		
		if(childUIDNodeID->Get() == kInvalidUID) 
			break;

		if(parentUIDNodeID->Get() == fBscTreeModel.GetRootUID()) 
			return fBscTreeModel.GetIndexForRootChild(childUIDNodeID->Get());
		else 
			return fBscTreeModel.GetChildIndexFor(parentUIDNodeID->Get(), childUIDNodeID->Get());			
	} while(kFalse);
	return (-1);
}

NodeID_rv	UPDTreeViewHierarchyAdapter::GetGenericNodeID() const
{
	return IntNodeID::Create(0/*kInvalidUID*/);
}





