//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"
#include "PanelList.fh"
#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss

// Project includes:
#include "CSWFID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kCSWFPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kCSWFCurrentMajorFormatNumber, kCSWFCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kCSWFVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kCSWFCompanyValue,			// Company name
	kCSWFMissingPluginURLValue,	// URL 
	kCSWFMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs10_workflow
	 */
	Class
	{
		kCSWFActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kCSWFActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

    /*
	 * This boss class inherits from an API panel boss class, and
	 * adds an interface to control a pop-up menu on the panel.
	 * The implementation for this interface is provided by the API.
     *
	 * 
	 * @ingroup apjs10_workflow
	 */
	Class
	{
		kCSWFPanelWidgetBoss,
		kPalettePanelWidgetBoss,
		{
			/*
			 * The plug-in's implementation of ITextControlData with an exotic IID of IID_IPANELMENUDATA.
			 * Implementation provided by the API.
			 */
			IID_IPANELMENUDATA, kCPanelMenuDataImpl,
            IID_IOBSERVER, kCSWFSelectionObserverImpl,

		}
	},

    Class
    {
        kCSWFTreeViewWidgetBoss,
        kTreeViewWidgetBoss,
        {
            IID_ITREEVIEWHIERARCHYADAPTER, kCSWFTreeViewHierarchyAdapterImpl,
            IID_ITREEVIEWWIDGETMGR, kCSWFTreeViewWidgetMgrImpl,
            //	IID_IOBSERVER, kTPLTreeObserverImpl;	// Commeted so tht we can get the Drag Drop Event handler
        }
    },

    Class
    {
        kCSWFTreeNodeWidgetBoss,
        kTreeNodeWidgetBoss,
        {
            /** What the application framework thinks is the  control's event handler */
            //IID_IEVENTHANDLER, kPnlTrvNodeEHImpl,/*kDragDropSourceEHImpl*/
            /** The real event handler associated with this control, we delegate to this implementation */
            //IID_IPNLTRVSHADOWEVENTHANDLER,  kTreeNodeEventHandlerImpl,
			/** Observer for changes in node state such as expand/contract */
            //	IID_IOBSERVER,  kPnlTrvNodeObserverImpl,
			/** Provides the node with drag source capability for the SysFile
				associated with the widget.
				See PnlTrvDragDropSource.
			 */
            //IID_IDRAGDROPSOURCE, kTPLDragSourceImpl, /* kPnlTrvDragDropSourceImpl,*/
        }
    },

}}};

/*
 * Implementation definition.
 */
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "CSWFFactoryList.h"
	}
};

/*  
 * Menu definition.
 */
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About sub-menu item for this plug-in.
		kCSWFAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		kCSWFAboutMenuPath,			// Menu Path.
		kSDKDefAlphabeticPosition,											// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,										// kSDKDefIsNotDynamicMenuFlag or       kSDKDefIsDynamicMenuFlag

		// Separator for the popup menu on the panel
		kCSWFSeparator1ActionID,
		kCSWFInternalPopupMenuNameKey kSDKDefDelimiterAndSeparatorPath,	// :- to mark an item as a separator.
		kCSWFSeparator1MenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,

		// About this plug-in sub-menu for the popup menu on the panel
		kCSWFPopupAboutThisActionID,
		kCSWFTargetMenuPath
		kCSWFAboutThisMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,

	}
};

/* 
 * Action definition.
 */
resource ActionDef (kSDKDefActionResourceID)
{
	{
		kCSWFActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		kCSWFAboutActionID,	// ActionID.
		kCSWFAboutMenuKey,	// Sub-menu string.
		kOtherActionArea,				// Area name (see ActionDefs.h).
		kNormalAction,					// Type of action (see ActionDefs.h).
		kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.

		kCSWFActionComponentBoss,
		kCSWFPopupAboutThisActionID,
		kSDKDefAboutThisPlugInMenuKey,	// Key to the name of this action
		kOtherActionArea,
		kNormalAction,
		kDisableIfLowMem,
		kInvalidInterfaceID,
		kSDKDefInvisibleInKBSCEditorFlag,

	}
};


/*  
 * Locale Indicies.
 * The LocaleIndex should have indicies that point at your
 * localizations for each language system that you are localized for.
 */

/*  
 * String LocaleIndex.
 */
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

resource LocaleIndex (kCSWFTreePanelNodeRsrcID)
{
    kViewRsrcType,
    {
        kWildFS, k_Wild,    kCSWFTreePanelNodeRsrcID + index_enUS
    }
};


// Strings not being localized
resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
		kCSWFInternalPopupMenuNameKey,	kCSWFInternalPopupMenuNameKey,		// No need to translate, internal menu name.
	
	}
};

/*  
 * Panel LocaleIndex.
 */
resource LocaleIndex (kSDKDefPanelResourceID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, 	kSDKDefPanelResourceID + index_enUS
	}
};

/*  
 * Type definition.
 */
type CSWFPanelWidget(kViewRsrcType) : PalettePanelWidget(ClassID = kCSWFPanelWidgetBoss)
{
	CPanelMenuData;
};

///////////////////// Tree Type Definations ///////////////////
type CSWFTreeViewWidget(kViewRsrcType) : TreeViewWidget(ClassID = kCSWFTreeViewWidgetBoss){}
type CSWFTreePanelNodeWidget(kViewRsrcType) : PrimaryResourcePanelWidget(ClassID = kCSWFTreeNodeWidgetBoss){}
//type CSWFTreeViewTextWidget(kViewRsrcType) : InfoStaticTextWidget(ClassID=kCSWFTreeTextBoxWidgetBoss){ };


/*  
 * PanelList definition.
 */
resource PanelList (kSDKDefPanelResourceID)
{
	{
		// 1st panel in the list
		kSDKDefPanelResourceID,		// Resource ID for this panel (use SDK default rsrc ID)
		kCSWFPluginID,			// ID of plug-in that owns this panel
		kIsResizable,
		kCSWFPanelWidgetActionID,	// Action ID to show/hide the panel
		kCSWFPanelTitleKey,	// Shows up in the Window list.
		"",							// Alternate menu path of the form "Main:Foo" if you want your palette menu item in a second place
		0.0,						// Alternate Menu position Alternate Menu position for determining menu order
		0,0,						// Rsrc ID, Plugin ID for a PNG icon resource to use for this palette
		c_Panel
	}
};

/* 
 * PanelView definition.
 * Note: it is not necessary to use EVE for panels.
 * The view is not currently localised: therefore, it can reside here.
 * However, if you wish to localise it, it is recommended to locate it in one of
 * the localised framework resource files (i.e. CSWF_enUS.fr etc.).
 */
resource CSWFPanelWidget(kSDKDefPanelResourceID + index_enUS)
{
	__FILE__, __LINE__,					// Localization macro
	kCSWFPanelWidgetID,					// WidgetID
	kPMRsrcID_None,						// RsrcID
	kBindNone,							// Binding (0=none)
	Frame(0, 0, 207, 400),				// Frame: left, top, right, bottom.
	kTrue, kTrue,						// Visible, Enabled
	kFalse,								// Erase background
	kInterfacePaletteFill,				// Erase to color
	kFalse,								// Draw dropshadow
	kCSWFPanelTitleKey,					// Panel name
	{
		// Replace this text widget with your panel content.
		StaticTextWidget
		(
			0,								 // WidgetId (default=0)
			kSysStaticTextPMRsrcId,			 // RsrcId
			kBindNone,						 // Frame binding
			Frame(10, 10, 202, 27),			 // Frame: left, top, right, bottom.
			kTrue, kTrue,					 // Visible, Enabled
			kAlignLeft, kEllipsizeEnd, kTrue // Alignment, ellipsize style, convert ampersands
			kCSWFStaticTextKey,				// Initial text.
			0								// No associated widget
		),

        GroupPanelWidget
        (
            kCSWFProjectPanelID,						// widget ID
            kPMRsrcID_None,							// PMRsrc ID
            kBindRight | kBindBottom | kBindLeft | kBindTop , //kBindNone,								// frame binding
            Frame(2,30,205 ,298)	//Frame(2,30,205 ,266)//Frame(2,30,205 ,253)						// left, top, right, bottom
            kTrue,									// visible
            kTrue,									// enabled
            0,
            {

                //////////////////////// Tree Widget for Project list //////////////////

                CSWFTreeViewWidget
                (
                    kProjectTreeViewWidgetID,		// widget ID
                    kPMRsrcID_None,					// PMRsrc ID
                    kBindAll,						// frame binding
                    Frame(3,4,201 ,266)				// left, top, right, bottom	//Frame(1,2,201 ,266)//Frame(1,2,201,190)
                    kTrue, kTrue					// visible
                    kTrue,
                    kInterfacePaletteFill,			// enabled
                    "",								// control label
                    // TreeAttributes properties
                    kFalse,							// fShouldDisplayRootNode
                    kFalse,							// fShouldUseHScrollBar
                    kTrue,							// fShouldUseVScrollBar
                    18,								// fVScrollButtonIncrement
                    18,								// fVThumbScrollIncrement
                    18,								// fHScrollButtonIncrement
                    18,								// fHThumbScrollIncrement
                    // TreeViewController properties
                    1,								// fItemsSelectableMode
                    kFalse,							// fShouldAllowChildrenFromMultipleParentsSelected
                    kFalse,							// fShouldAllowDiscontiguousSelection
                    {								// CPanelControlData Children
                    }
                ),
            }
        ),

        GroupPanelWidget
        (
            kInvalidWidgetID,						// widget ID
            kPMRsrcID_None,							// PMRsrc ID
            kBindRight | kBindBottom | kBindLeft ,	//kBindNone,								// frame binding
            Frame(2, 296, 205 , 395)				//Frame(2, 264, 205 , 290)					// left, top, right, bottom//(2, 199, 190, 269)
            kTrue,									// visible
            kTrue,									// enabled
            0,
            {

            }
        ),

    }

	kCSWFInternalPopupMenuNameKey		// Popup menu name (internal)
};

///////////////// Resource for Project tree /////////////////////////

resource CSWFTreePanelNodeWidget (kCSWFTreePanelNodeRsrcID + index_enUS)
{
    __FILE__, __LINE__,
    kCSWFTreePanelNodeWidgetID, kPMRsrcID_None,				// WidgetId, RsrcId
    kBindLeft | kBindRight,
    Frame(0, 0, 250 - 13 , 18),									// Frame//(0, 0, 250, 20),
    kTrue, kTrue,											// Visible, Enabled
    "",														// Panel name
    {
        //Drop down arrow
        TreeExpanderWidget
        (
            kCSWFTreeNodeExpanderWidgetID,                    // WidgetId
            kTreeBranchCollapsedRsrcID,
            kApplicationRsrcPluginID,						// RsrcId for collapsed node icon
            kBindLeft,
            Frame(0,0,0,0)								// Frame//(0,2,16,18)
            kTrue, kTrue,									// Visible, Enabled
            kTreeBranchExpandedRsrcID,
            kApplicationRsrcPluginID,						// RsrcId for expanded node icon
            kTrue,											// Cmd-Click expands/collapses children,
            kTrue,											// Scroll to show children when expanded
            kIconNoCentering,
            kIconRaisedLook,		// Sets the icon look -- (kIconRaisedLook | kIconFlushLook)
            kFalse					// Bool integer sets whether the icon draws a well.

        ),


        InfoStaticTextWidget //CSWFTreeViewTextWidget
        (
            kCSWFTreeNodeNameWidgetID,
            kSysStaticTextPMRsrcId, 						// WidgetId, RsrcId
            // kBindLeft,
            kBindRight | kBindLeft | kBindBottom,			// Lee's bindings
            Frame(02,2,200,17),								// Frame//(45,2,300,18),
            kTrue, kTrue, kAlignLeft,						// Visible, Enabled, padding, Alignment
            kDontEllipsize,	kTrue,								// Ellipsize style
            "",
            0,
            kPaletteWindowSystemScriptFontId,
            kPaletteWindowSystemScriptFontId,
        ),


    }
};


#endif // __ODFRC__

#include "CSWF_enUS.fr"
#include "CSWF_jaJP.fr"
