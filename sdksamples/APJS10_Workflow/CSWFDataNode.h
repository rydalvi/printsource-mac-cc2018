#ifndef __CSWFDATANODE_H__
#define __CSWFDATANODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class CSWFDataNode
{
private:
	PMString ItemNumberNameStr;
	int32 TaskId;
	int32 ParentId;
	int32 seqNumber;
	int32 childCount;
	int32 hitCount;
    PMString objectType;
    PMString taskName;
    PMString taskStatus;
    double objectId;
    double currentStepId;
    double dueDate;
    
    
public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	CSWFDataNode():ItemNumberNameStr(""), TaskId(0),  childCount(0), hitCount(0)	, seqNumber(0){}
	/*
		One time access to all the private members
	*/
	void setAll(PMString ItemNumberNameStr, int32 TaskId,int32 ParentId, int32 seqNumber, int32 childCount, int32 hitCount,
                PMString objectType, PMString taskName, PMString taskStatus, double objectId, double currentStepId, double dueDate)
	{
		this->ItemNumberNameStr=ItemNumberNameStr;
		this->TaskId=TaskId;
		this->ParentId = ParentId;	
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
        
        this->objectType=objectType;
        this->taskName=taskName;
        this->taskStatus=taskStatus;
        this->objectId=objectId;
        this->currentStepId=currentStepId;
        this->dueDate = dueDate;
	}
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->ItemNumberNameStr; }
	/*
		@returns the id of the TaskId
	*/
	int32 getTaskId(void) { return this->TaskId; }
		
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int32 getHitCount(void) { return this->hitCount; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	int32 getParentId(void) { return this->ParentId; }

	void setParentId(int32 ParentId) { this->ParentId = ParentId; }
	void setTaskId(int32 TaskId) { this->TaskId=TaskId; }
	
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setItemNumberNameStr(PMString& pubName) { this->ItemNumberNameStr=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int32 hitCount) { this->hitCount=hitCount; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }
    double getCurrentStepId()
    {
        return currentStepId;
    }
    
    void setCurrentStepId(double id)
    {
        currentStepId = id;
    }
    
    double getObjectId()
    {
        return objectId;
    }
    
    void setObjectId(double id)
    {
        objectId = id;
    }
    
    PMString getTaskStatus()
    {
        return taskStatus;
    }
    
    void setTaskStatus(PMString stringObj)
    {
        taskStatus = stringObj;
    }
    
    PMString getObjectType()
    {
        return objectType;
    }
    
    void setObjectType(PMString stringObj)
    {
        objectType = stringObj;
    }
    
    PMString getTaskName()
    {
        return taskName;
    }
    
    void setTaskName(PMString stringObj)
    {
        taskName = stringObj;
    }
    
    double getDueDate()
    {
        return dueDate;
    }
    
    void setDueDate(double id)
    {
        dueDate = id;
    }
};

typedef vector<CSWFDataNode> CSWFDataNodeList;

#endif