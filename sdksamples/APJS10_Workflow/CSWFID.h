//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CSWFID_h__
#define __CSWFID_h__

#include "SDKDef.h"

// Company:
#define kCSWFCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCSWFCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCSWFPluginName	"APJS10_Workflow"			// Name of this plug-in.
#define kCSWFPrefixNumber	0xABA4900 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCSWFVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCSWFAuthor		"Catsy"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCSWFPrefixNumber above to modify the prefix.)
#define kCSWFPrefix		RezLong(kCSWFPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCSWFStringPrefix	SDK_DEF_STRINGIZE(kCSWFPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCSWFMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCSWFMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCSWFPluginID, kCSWFPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCSWFActionComponentBoss, kCSWFPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCSWFPanelWidgetBoss, kCSWFPrefix + 1)
DECLARE_PMID(kClassIDSpace, kCSWFTreeViewWidgetBoss, kCSWFPrefix + 3)
DECLARE_PMID(kClassIDSpace, kCSWFTreeNodeWidgetBoss, kCSWFPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCSWFBoss, kCSWFPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICSWFINTERFACE, kCSWFPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCSWFActionComponentImpl, kCSWFPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCSWFTreeViewHierarchyAdapterImpl, kCSWFPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kCSWFTreeViewWidgetMgrImpl, kCSWFPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kCSWFSelectionObserverImpl, kCSWFPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCSWFImpl, kCSWFPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCSWFAboutActionID, kCSWFPrefix + 0)
DECLARE_PMID(kActionIDSpace, kCSWFPanelWidgetActionID, kCSWFPrefix + 1)
DECLARE_PMID(kActionIDSpace, kCSWFSeparator1ActionID, kCSWFPrefix + 2)
DECLARE_PMID(kActionIDSpace, kCSWFPopupAboutThisActionID, kCSWFPrefix + 3)//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCSWFActionID, kCSWFPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCSWFPanelWidgetID, kCSWFPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kCSWFProjectPanelID, kCSWFPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kProjectTreeViewWidgetID, kCSWFPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kCSWFTreeNodeExpanderWidgetID, kCSWFPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kCSWFTreeNodeNameWidgetID, kCSWFPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kCSWFTreePanelNodeWidgetID, kCSWFPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kCSWFWidgetID, kCSWFPrefix + 25)


// "About Plug-ins" sub-menu:
#define kCSWFAboutMenuKey			kCSWFStringPrefix "kCSWFAboutMenuKey"
#define kCSWFAboutMenuPath		kSDKDefStandardAboutMenuPath kCSWFCompanyKey

// "Plug-ins" sub-menu:
#define kCSWFPluginsMenuKey 		kCSWFStringPrefix "kCSWFPluginsMenuKey"
#define kCSWFPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCSWFCompanyKey kSDKDefDelimitMenuPath kCSWFPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCSWFAboutBoxStringKey	kCSWFStringPrefix "kCSWFAboutBoxStringKey"
#define kCSWFPanelTitleKey					kCSWFStringPrefix	"kCSWFPanelTitleKey"
#define kCSWFStaticTextKey kCSWFStringPrefix	"kCSWFStaticTextKey"
#define kCSWFInternalPopupMenuNameKey kCSWFStringPrefix	"kCSWFInternalPopupMenuNameKey"
#define kCSWFTargetMenuPath kCSWFInternalPopupMenuNameKey

// Menu item positions:

#define	kCSWFSeparator1MenuItemPosition		10.0
#define kCSWFAboutThisMenuItemPosition		11.0

#define kCSWFTreePanelNodeRsrcID 3300


// Initial data format version numbers
#define kCSWFFirstMajorFormatNumber  RezLong(1)
#define kCSWFFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCSWFCurrentMajorFormatNumber kCSWFFirstMajorFormatNumber
#define kCSWFCurrentMinorFormatNumber kCSWFFirstMinorFormatNumber

#endif // __CSWFID_h__
