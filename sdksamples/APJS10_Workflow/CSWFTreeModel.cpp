#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "CSWFID.h"
#include "CSWFTreeModel.h"
#include "CAlert.h"
//#include "CSWFMediatorClass.h"


#include "CSWFDataNode.h"
//#include "IClientOptions.h"
//#include "IAppFrameWork.h"
#include "CSWFTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("CSWFTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

CSWFTreeDataCache dc;

int32 CSWFTreeModel::root=0;
PMString CSWFTreeModel::publicationName;
double CSWFTreeModel::classId=0;
extern CSWFDataNodeList WorkTaskDataNodeList;
//extern CSWFDataNodeList PFDataNodeList;
//extern CSWFDataNodeList PGDataNodeList;
//extern CSWFDataNodeList PRDataNodeList;
//extern CSWFDataNodeList ITEMDataNodeList;
//extern CSWFDataNodeList CatagoryDataNodeList;
int32 RowCountForTree = -1;
extern int32 SelectedRowNo;


CSWFTreeModel::CSWFTreeModel()
{
	//root=-1;
	//classId=11002;
}

CSWFTreeModel::~CSWFTreeModel() 
{
}


int32 CSWFTreeModel::GetRootUID() const
{	
	if(classId<=0)
		return 0;// kInvalidUID;
	
	CSWFDataNode pNode;
	/*if(dc.isExist(root, pNode))
	{
		return root;
	}*/
	
	dc.clearMap();
	
	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Fail to CreateObject of Application FrameWork.");
		return kInvalidUID;
	}
	
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				*/

	/*VectorPubObjectValuePPtr VectorFamilyInfoValuePtr = nil;
	VectorFamilyInfoValuePtr = ptrIAppFramework->PUBMngr_getPFTreesForSubSection(classId, kFalse, kFalse, kFalse);
	if(VectorFamilyInfoValuePtr == nil)
		return kInvalidUID;

	if(VectorFamilyInfoValuePtr->size()==0)
	{
		delete VectorFamilyInfoValuePtr;
		return kInvalidUID;
	}
	*/
	/*Adding root*/
	//CA("1");
	
	CSWFDataNodeList CurrentNodeList;
	CurrentNodeList.clear();

    CurrentNodeList = WorkTaskDataNodeList;


	RowCountForTree = 0;
	PMString name("Root");
	pNode.setTaskId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setItemNumberNameStr(name);
	pNode.setSequence(0);
	dc.add(pNode);

	/*Adding kids*/
	/*VectorPubObjectValuePointer::iterator it1;
	VectorPubObjectValue::iterator it2;*/
	
	int count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
			
				pNode.setParentId(root);
				pNode.setSequence(i);
				count++;
				pNode.setTaskId(CurrentNodeList[i].getTaskId()); //
                PMString ASD(CurrentNodeList[i].getName());
				pNode.setItemNumberNameStr(ASD);
				//CA(ASD);
				pNode.setChildCount(0);
                pNode.setHitCount(CurrentNodeList[i].getHitCount());
			dc.add(pNode);
		}
	return root;
}


int32 CSWFTreeModel::GetRootCount() const
{
	int32 retval=0;
	CSWFDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 CSWFTreeModel::GetChildCount(const int32& uid) const
{
	CSWFDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

int32 CSWFTreeModel::GetParentUID(const int32& uid) const
{
	CSWFDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		if(pNode.getParentId()==-2)
			return 0; //kInvalidUID;
		return pNode.getParentId();
	}
	return 0; //kInvalidUID;
}


int32 CSWFTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	CSWFDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, pNode))
		return pNode.getSequence();
	return -1;
}


int32 CSWFTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
	CSWFDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID, index, pNode))
		return pNode.getTaskId();
	return 0; // kInvalidUID;
}


int32 CSWFTreeModel::GetNthRootChild(const int32& index) const
{
	CSWFDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getTaskId();
	}
	return 0; //kInvalidUID;
}

int32 CSWFTreeModel::GetIndexForRootChild(const int32& uid) const
{
	CSWFDataNode pNode;
	if(dc.isExist(uid, pNode))
		return pNode.getSequence();
	return -1;
}

PMString CSWFTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	CSWFDataNode pNode;
	PMString name("");

	if(dc.isExist(uid, pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
        PMString Text("");
        Text.Append(pNode.getObjectType());
        Text.Append("\t");
        Text.Append(pNode.getName());
        Text.Append("\t");
        Text.Append(pNode.getTaskName());
        
        //CA(Text);
		return Text;
	}
	return name;
}


int32 CSWFTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	CSWFDataNode pNode;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode))
		return (1);
	return -1;
}
