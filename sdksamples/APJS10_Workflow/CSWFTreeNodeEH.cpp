//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "TPLID.h"
#include "TPLMediatorClass.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "ITextFrame.h"         //Removed from CS3 API
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITreeViewController.h"
#include "IntNodeID.h"
#include "TPLDataNode.h"
#include "TPLTreeDataCache.h"
#include "TPLCommonFunctions.h"
#include "TPLManipulateInline.h" 
#include "ILayoutSelectionSuite.h"
#include "ITableModelList.h"
#include "ILayoutUIUtils.h"
#include "IXMLUtils.h"
#include "ITextStoryThreadDict.h"
#include "ITblBscSuite.h"
#include <IFrameUtils.h>
#include "CAlert.h"
//#include "IMessageServer.h"
//---------CS3 Addition-----------------//
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "SDKLayoutHelper.h"
#include "IGraphicFrameData.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITriStateControlData.h"

#include "IDialogController.h"
#include "ILoginHelper.h"
#include "K2Vector.tpp"
#include "IAppFramework.h"

#define FILENAME			PMString("TPLTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern int32 SelectedRowNo;
extern TPLDataNodeList PRDataNodeList;
extern bool16 ISTabbedText;
extern UIDRef newFrameUIDRef;    // Global variable in TPLManipulateInline.cpp
extern bool16 isComponentAttr;
extern bool16 isXRefAttr;
extern bool16 isAccessoryAttr;
extern bool16 isMMYAttr;
extern bool16 isMMYSortAttr;
extern bool16 isAddAsDisplayName;
extern bool16 isAddTableHeader;
extern bool16 isAddImageDescription;
extern bool16 isAddListName;
extern bool16 isOutputAsSwatch;

extern bool16 isSpreadBasedLetterKeys;
//#include "PnlTrvFileNodeID.h"
//#include "PnlTrvUtils.h"

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	PnlTrvNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~PnlTrvNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( PnlTrvNodeEH, kPnlTrvNodeEHImpl)

	
PnlTrvNodeEH::PnlTrvNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 PnlTrvNodeEH::ButtonDblClk(IEvent* e) 
{
	//CA("PnlTrvNodeEH::ButtonDblClk");
//if(TPLMediatorClass::tableFlag == 0 || ISTabbedText == kTrue)
//Modified on 11July.To make the Items stencil insertable into the table cell.
	if(TPLMediatorClass::imageFlag == 0 || ISTabbedText == kTrue)
	{		
		if(/*TPLMediatorClass::curSelLstbox*/ (SelectedRowNo == 4 ||SelectedRowNo == 0 ||SelectedRowNo == 3) && TPLMediatorClass::imageFlag==kFalse)
	{	

//following code added by vijay on 9-11-2006 ======================from here
		InterfacePtr<IDocument> document(Utils<ILayoutUIUtils>()->GetFrontDocument(),UseDefaultIID());
		if(document == nil)
		{
			//CA("document nil"); 
			return kFalse; 
		}

		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(document));
		if (rootElement == nil)
		{
			//CA("rootElement is NULL");
			return kFalse;
		}
//==================================================================upto here				
		TPLCommonFunctions tpl;

		InterfacePtr<ITreeViewController> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(treeViewMgr==nil)
		{
			//CA("treeViewMgr is NULL");
			return kFalse;
		}

		NodeIDList selectedItem;
		treeViewMgr->GetSelectedItems(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			return kFalse;
		}
		
		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

		int32 uid= uidNodeIDTemp->Get();
		int32 TextFrameuid = uidNodeIDTemp->Get();
		TPLDataNode pNode;
		TPLTreeDataCache dc;

		dc.isExist(uid, pNode);
		
		PMString pfName = pNode.getName();
				
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;
		
		TPLMediatorClass::tableFlag=0;
		TPLListboxData ItemLstboxData;
		int32 vectSize=ItemLstboxData.returnListVectorSize(4);

		/* For getting data of the selected row of listbox */
		for(int y=0;y<vectSize;y++)
		{
			
			if(y==TPLMediatorClass::curSelRowIndex)
			{
								
//following code is added by vijay choudhari on 5-5-2006
//i.e. if conditions////////////////////////////////
////////////////////From here to
				if( SelectedRowNo == 4)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);

					PMString temp;
					temp.AppendNumber(ItemListboxInfo.listBoxType);
					// CA("Index "+temp );
					 temp.Clear();
	////////////////////////////////////////////////////////////
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				}
				if(SelectedRowNo == 3)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(3,y);
					//curSelRowStr=ItemListboxInfo.name;
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
					
				}
				
				if(SelectedRowNo == 2)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(2,y);
					//curSelRowStr=ItemListboxInfo.name;
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				
				}
				
				if(SelectedRowNo == 1)
				{//CA("1 selected");
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(1,y);
					//curSelRowStr=ItemListboxInfo.name;
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				
				}
				if(SelectedRowNo == 0)
				{//CA("0 selected");
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(5,y);
					//curSelRowStr=ItemListboxInfo.name;
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
					
				}
//////////////////////////////upto here
			}
		}
		if(TPLMediatorClass::curSelRowIndex==-1)
		{	
			//CA("curSelRowIndex ==-1");
			return kFalse;
		}

		if(TPLMediatorClass::imageFlag == kTrue)
		{
			//CA("TPLMediatorClass::imageFlag == kTrue");
			return kFalse;
		}
		
		TPLListboxData lstboxData;
		TPLListboxInfo lstboxInfo;

		
//following code is added by vijay choudhari on 4-5-2006
//i.e. if---else series	
/////////////////////////////from here to
		//ItemAttributes' row
		if( SelectedRowNo == 4)
		{
			lstboxInfo=lstboxData.getData(4, TPLMediatorClass::curSelRowIndex);
			
		}
		//ProductAttributes' row
		else if( SelectedRowNo == 3)
		{//CA("SelectedRowNo == 3");
			lstboxInfo=lstboxData.getData(3, TPLMediatorClass::curSelRowIndex);			
		}		
		else if( SelectedRowNo == 2)
		{
			lstboxInfo=lstboxData.getData(2, TPLMediatorClass::curSelRowIndex);			
		}
		else if( SelectedRowNo == 1)
		{
			lstboxInfo=lstboxData.getData(1, TPLMediatorClass::curSelRowIndex);			
		}		
		else if( SelectedRowNo == 0)
		{
			lstboxInfo=lstboxData.getData(5, TPLMediatorClass::curSelRowIndex);							
		}
/////////////////////////////////upto here

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("!iSelectionManager");
			return kFalse;
		}
		UIDRef ref;
		
		InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(/*ITextMiscellanySuite::*/IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
		if(!txtSelectionSuite){
			//CA("Please Select a Type Tool");
		}
		else
		{
			bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
			
			if(	ISTextFrame)
			{	
				//CA("ISTextFrame");	
				PMString result = lstboxInfo.name;									
				if(isComponentAttr && ( SelectedRowNo == 4) )
					result.Append("_Component");
				else if(isXRefAttr && (SelectedRowNo == 4))
					result.Append("_Xref") ; /// For XRef Table Attribute
				else if(isAccessoryAttr && (SelectedRowNo == 4))
					result.Append("_Accessory") ; /// For Accessory Table Attribute	
				else if(isMMYAttr && (SelectedRowNo == 4))
					result.Append("_MMY") ; /// For MMY Table Attribute	

				if(isAddAsDisplayName)
					result.Append("_DsplName");

				if(isAddImageDescription)
					result.Append("_Description");

				InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());		
				
				//result = tpl.prepareTagName(result);		
		
				if((lstboxInfo.id == -1) && (lstboxInfo.typeId == -1001) ) //Possible Vlaue Box Case;
				{
					//CA("lstboxInfo.id == -1 and lstboxInfo.typeId == -1001");
					result = tpl.prepareTagName(result);
					tpl.addTagToPVBoxDoubleClickVersion(ref, TPLMediatorClass::curSelRowIndex  , SelectedRowNo , result);
					return kFalse;
				}
				int32 start=0;
				txtSelectionSuite->GetCaretPosition(start);
				int32 end=start+result.NumUTF16TextChars();
			
				InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA("graphicFrameHierarchy is NULL");
					return kFalse;
				}
					
				int32 chldCount = graphicFrameHierarchy->GetChildCount();
				PMString Count("ChildCount::");
				Count.AppendNumber(chldCount);
				//CA(Count);
				/*if(chldCount==0)
					IHierarchy* multiColumnItemHierarchy = graphicFrameHierarchy->QueryParent();*/
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA("multiColumnItemHierarchy is NULL");
				}
				
				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					//CA("Its Not MultiColumn");
					return kFalse;
				}	

				InterfacePtr<ITextModel> txtModel(multiColumnItemTextFrame->QueryTextModel());
				if(!txtModel)
				{
				//	CA("!txtModel" );
						return kFalse;
				}
//---------------------CS3 TextFrame Change--------------------------------------------------------------------//
				UIDRef ref1=::GetUIDRef(multiColumnItemTextFrame);
				//UIDRef ref1 = ref;
				bool16 isInsideTable = kFalse;
				isInsideTable=Utils<ITableUtils>()->InsideTable(txtModel, start);
				if(isInsideTable)
				{
					//CA("Insdide table")
					TPLMediatorClass::tableFlag=lstboxInfo.tableFlag;
					if(TPLMediatorClass::tableFlag==1 && ISTabbedText == kFalse  && isAddAsDisplayName == kFalse)
					{
						if(ISTabbedText == kFalse)
						{
							TPLCommonFunctions tp1;
							TPLMediatorClass::tableFlag=lstboxInfo.tableFlag;
							tp1.InsertTableInsideTableCellAndApplyTag(lstboxInfo, ref);
						}
					}
                    //============================on 10-11-2006 upto here
					else
					{
						//following if condion is added by vijay on 9-11-2006
						if(TPLMediatorClass::IsTableInsideTableCell ==  kFalse)
						{
							TextIndex TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel,start);
							
							UIDRef tableRef1=( Utils<ITableUtils>()->GetTableModel(txtModel, TablStartIndex));
							
							if (textEditSuite && textEditSuite->CanEditText())
							{
                                result.ParseForEmbeddedCharacters();
								ErrorCode status = textEditSuite->InsertText(WideString(result));
							//	ASSERT_MSG(status == kSuccess, "WFPDialogController::ApplyDialogFields: can't insert text"); 
							}
													
							PMString CellTagName = tpl.prepareTagName(result/*lstboxInfo.name*/);
							XMLReference xmlRef;
							tpl.TagTable(tableRef1, ref1, "PRINTsourceTable", CellTagName, xmlRef, lstboxInfo , start);	
						}
						else
						{
							
							tpl.insertOrAppendOnDoubleClick( TPLMediatorClass::curSelRowIndex , result , SelectedRowNo );
						}
						return kFalse;
					}
				}
//---------------------------------------------------------------upto here
				else 
				{
					//Following is added by vijay choudhari
					//change in following function (3rd arg)is made by vijay choudhari on 4-5-2006
					//from '4'  to  SelectedRowNo
				
					tpl.insertOrAppendOnDoubleClick( TPLMediatorClass::curSelRowIndex , result , SelectedRowNo );
				}
			}
		  }
		}
     }	

//////////////////////////////////////////////////////////////////////////
//////////////// Following code is added by Chetan Dogra on 10 Nov 2006 //
/////////////// To attain Image Frame in a table's cell /////////////////
	if(TPLMediatorClass::imageFlag == 1 )
	{
		//CA("TPLMediatorClass::imageFlag == 1");
		TPLCommonFunctions tpl;
		InterfacePtr<ITreeViewController> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(treeViewMgr==nil)
		{
			//CA("treeViewMgr==nil");
			return kFalse;
		}

		NodeIDList selectedItem;
		treeViewMgr->GetSelectedItems(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			return kFalse;
		}
		
		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
		int32 uid= uidNodeIDTemp->Get();

		TPLDataNode pNode;
		TPLTreeDataCache dc;
		dc.isExist(uid, pNode);
		
		PMString pfName = pNode.getName();
				
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;

		TPLMediatorClass::tableFlag=0;
		TPLListboxData ItemLstboxData;
		int32 vectSize=ItemLstboxData.returnListVectorSize(4);

		for(int y=0;y<vectSize;y++)
		{			
			if(y==TPLMediatorClass::curSelRowIndex)
			{
				if( SelectedRowNo == 4)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);
					PMString temp;
					temp.AppendNumber(ItemListboxInfo.listBoxType);
					temp.Clear();
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				}
				if(SelectedRowNo == 3)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(3,y);
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				
				}
				if(SelectedRowNo == 2)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(2,y);
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				}
				if(SelectedRowNo == 1)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(1,y);
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;						
				}
				if(SelectedRowNo == 0)
				{
					TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(5,y);
					TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
					break;
				}
			}
		}
		if(TPLMediatorClass::curSelRowIndex==-1)
		{	
			return kFalse;
		}
		
		TPLListboxData lstboxData;
		TPLListboxInfo lstboxInfo;
		

		//ItemAttributes' row
		if( SelectedRowNo == 4)
		{
			lstboxInfo=lstboxData.getData(4, TPLMediatorClass::curSelRowIndex);
		}
		//ProductAttributes' row
		else if( SelectedRowNo == 3)
		{
			lstboxInfo=lstboxData.getData(3, TPLMediatorClass::curSelRowIndex);
		}
		else if( SelectedRowNo == 2)
		{
			lstboxInfo=lstboxData.getData(2, TPLMediatorClass::curSelRowIndex);
		}
		else if( SelectedRowNo == 1)
		{
			lstboxInfo=lstboxData.getData(1, TPLMediatorClass::curSelRowIndex);
		}		
		else if( SelectedRowNo == 0)
		{
			lstboxInfo=lstboxData.getData(5, TPLMediatorClass::curSelRowIndex);
		}
		
		UIDRef ref;
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("!iSelectionManager");
			return kFalse;
		}

		InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
		if(!txtSelectionSuite){
			//CA("Please Select a Type Tool");
		}
		else
		{		
			
			bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
			if(	ISTextFrame)
			{	
				
				PMString result = lstboxInfo.name;
				InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
				int32 start=0;
				txtSelectionSuite->GetCaretPosition(start);
				int32 end=start+result.NumUTF16TextChars();
//------------------------CS3 TextFrame Change---------------------------------------//
				/*InterfacePtr<ITextFrame>txtFrame(ref,UseDefaultIID());
				if(!txtFrame)
				{
					CA("Text Frame is null");				
					return kFalse;
				}
				InterfacePtr<ITextModel>txtModel(txtFrame->QueryTextModel(),UseDefaultIID());
				if(!txtModel)
				{
					CA("Text Model is null");				
					return kFalse;
				}*/
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA("graphicFrameHierarchy is NULL");
					return kFalse;
				}
				/*				
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					CA("multiColumnItemHierarchy is NULL");
					return kFalse;
				}

				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					CA("multiColumnItemTextFrame is NULL");
					return kFalse;
				}*/				
				
				InterfacePtr<IHierarchy>
				multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA("multiColumnItemHierarchy is NULL");
					return kFalse;
				}

				InterfacePtr<IMultiColumnTextFrame>
				frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
				if (!frameItemTFC) {
					//CA("!!!ITextFrameColumn");
					return kFalse;
				}

				InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
				if(!txtModel)
				{
				//	CA("!txtModel" );
					return kFalse;
				}

//-----------------------CS3 TextFrame Change-----------------------------------------//
				UIDRef ref1=::GetUIDRef(frameItemTFC);
				bool16 isInsideTable = kFalse;
				isInsideTable=Utils<ITableUtils>()->InsideTable(txtModel, start);
	
				if(isInsideTable)
				{					
					if(TPLMediatorClass::imageFlag == kTrue )
					{
						if(!frameItemTFC)
						{	
							return kFalse;
						}
						/*InterfacePtr<ITextModel>txtModel(frameItemTFC->QueryTextModel(),UseDefaultIID());3412183
						if(!txtModel)
						{
							return kFalse;
						}*/

						TextIndex TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel,start);
						UIDRef tableRef=( Utils<ITableUtils>()->GetTableModel(txtModel, TablStartIndex));

						UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
						UIDRef ref1=::GetUIDRef(frameItemTFC);   
						UID uidref = ref1.GetUID();
						
						TPLManipulateInline instance;
						instance.InsertInline (textStoryUIDRef,start);
					
						PMString CellTagName = tpl.prepareTagName(lstboxInfo.name);
						XMLReference xmlRef;

						tpl.TagTable(tableRef, /*newFrameUIDRef*/ ref1 , "PRINTsourceTable", CellTagName, xmlRef, lstboxInfo);
					}
				}
				else if(isAddAsDisplayName) //for image tag in text frame
				{
					//CA("isAddAsDisplayName");
					//PMString tempStr("[");
					//tempStr.Append(lstboxInfo.name);
					result.Append("_DsplName");
					result.Append("]");		
					
					PMString theContent("");
					theContent.Clear();
					theContent.Append("[");
					theContent.Append(result);
					//theContent.Append(tempStr);
			
					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
                    theContent.ParseForEmbeddedCharacters();
					WideString str(theContent);				
					textEditSuite->InsertText(str);	

					TPLCommonFunctions objCommonFunctions;
					objCommonFunctions.addTagToTextDoubleClickVersion(ref , TPLMediatorClass::curSelRowIndex,TPLMediatorClass::curSelLstbox/*whichTab*/,theContent );	
				}
				else if(isAddImageDescription)
				{
					//CA("Add image Description");
					//PMString tempStr("[");
					//tempStr.Append(lstboxInfo.name);
					result.Append("_Description");
					result.Append("]");	
					
					PMString theContent("");
					theContent.Clear();
					//theContent.Append(tempStr);
					theContent.Append("[");
					theContent.Append(result);
			
					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
                    theContent.ParseForEmbeddedCharacters();
					WideString str(theContent);				
					textEditSuite->InsertText(str);	

					TPLCommonFunctions objCommonFunctions;
					objCommonFunctions.addTagToTextDoubleClickVersion(ref , TPLMediatorClass::curSelRowIndex,TPLMediatorClass::curSelLstbox/*whichTab*/,theContent );	
				}
				else if((!isInsideTable) && (TPLMediatorClass::imageFlag ==kTrue))//*******
				{ 
					//CA("Here Commes in Mycoundition..");						
					if(TPLMediatorClass::imageFlag == kTrue)
					{	
						//bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
//						InterfacePtr<IHierarchy> gaphicFrameHierarchy(ref,UseDefaultIID());
//						if (gaphicFrameHierarchy == nil) 
//						{
//							//CA("graphicFrameHierarchy is NULL");
//							return kFalse;
//						}
//						int32 child = gaphicFrameHierarchy->GetChildCount();
//PMString asd("Child Count = ");
//asd.AppendNumber(child);
//CA(asd);

						//CA("TPLMediatorClass::imageFlag == kTrue....");
						if(!frameItemTFC)						
							return kFalse;
																		
						UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
						UIDRef ref1=::GetUIDRef(frameItemTFC);
												
						TPLManipulateInline instance;
						instance.InsertInline(textStoryUIDRef,start);

					
										
						PMString theContent("");
						theContent.Clear();
					
						theContent.Append("[");
						theContent.Append(result);
						theContent.Append("]");
PMString as("ImageRef = ");
as.AppendNumber(ref.GetUID().Get());
//CA(as);
														
						TPLCommonFunctions objCommonFunctions;						
						objCommonFunctions.addTagToTextDoubleClickVersion(ref,TPLMediatorClass::curSelRowIndex,
							    TPLMediatorClass::curSelLstbox,theContent);							
					}
				}
			}
		}

}		

	return kFalse;
} 

bool16 PnlTrvNodeEH::LButtonDn(IEvent* e)
{
	//CA("LButton Down");
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_IPNLTRVSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	IControlView* iImgGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
	if(iImgGrpCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iImgGrpCntrlView == nil");
		return retval;
	}
	IControlView* iAttrGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
	if(iAttrGrpCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iAttrGrpCntrlView == nil");
		return retval;
	}
	IControlView* iTblGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
	if(iTblGrpCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iTblGrpCntrlView == nil");
		return retval;
	}

    IControlView* iSprayItemPerFrameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kSprayItemPerFrameWidgetID);
    if(iSprayItemPerFrameCntrlView==nil)
    {
        ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iSprayItemPerFrameCntrlView is nil");
        return retval;
    }

    InterfacePtr<ITriStateControlData> itristatecontroldata5(iSprayItemPerFrameCntrlView, UseDefaultIID());
    if(itristatecontroldata5==nil)
    {
        ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata5 is nil");
        return retval;
    }
		
    //itristatecontroldata5->Deselect();
    iSprayItemPerFrameCntrlView->Enable();
			
//------------------------------------------- Prabhat till here

	
	if(TPLMediatorClass::tableFlag == 1)
	{
		//CA("TPLMediatorClass::tableFlag == 1");
		isAddImageDescription = kFalse;
        isOutputAsSwatch = kFalse;
		iImgGrpCntrlView->HideView();
		iImgGrpCntrlView->Disable();

		iAttrGrpCntrlView->HideView();
		iAttrGrpCntrlView->Disable();
		
		iTblGrpCntrlView->ShowView();
		iTblGrpCntrlView->Enable();
//------------------------------------------

		itristatecontroldata5->Deselect();
		iSprayItemPerFrameCntrlView->Disable();                            //it is disable();

//------------------------------------------



    
		IControlView* iRadioButtonCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTabbedTextWidgetID);
		if(iRadioButtonCntrlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iRadioButtonCntrlView is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata(iRadioButtonCntrlView, UseDefaultIID());
		if(itristatecontroldata==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata is nil");		
			return retval;
		}
		
		IControlView* iRadioButtonCntrlView1=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTableWidgetID);
		if(iRadioButtonCntrlView1==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iRadioButtonCntrlView1 is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata1(iRadioButtonCntrlView1, UseDefaultIID());
		if(itristatecontroldata1==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata1 is nil");		
			return retval;
		}

		IControlView* iTableNameCntrlView1=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputTableNameWidgetID);
		if(iTableNameCntrlView1==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iTableNameCntrlView1 is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata3(iTableNameCntrlView1, UseDefaultIID());
		if(itristatecontroldata3==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata3 is nil");		
			return retval;
		}

		IControlView* iIncludeTableHeaderCntrlView1=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeTableHeaderWidgetID);
		if(iIncludeTableHeaderCntrlView1==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iIncludeTableHeaderCntrlView1 is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata4(iIncludeTableHeaderCntrlView1, UseDefaultIID());
		if(itristatecontroldata4==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata4 is nil");		
			return retval;
		}

        
        IControlView* iIncludeListNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeListNameWidgetID);
		if(iIncludeListNameCntrlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iIncludeListNameCntrlView is nil");
			return retval;
		}
        InterfacePtr<ITriStateControlData> itristatecontroldataIncludeListName(iIncludeListNameCntrlView, UseDefaultIID());
		if(itristatecontroldataIncludeListName==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldataIncludeListName is nil");
			return retval;
		}


		if(ISTabbedText)
		{
			//CA("ISTabbedText == kTrue");
			itristatecontroldata1->Deselect();
			if(!itristatecontroldata->IsSelected())
				itristatecontroldata->Select();
            isAddListName= kFalse;
		}
		else
		{
			//CA("ISTabbedText == kFalse");
			itristatecontroldata->Deselect();
			if(!itristatecontroldata1->IsSelected())
				itristatecontroldata1->Select();
		}
		if(isAddAsDisplayName)
		{
			//CA("isAddAsDisplayName == kTrue");
			if(itristatecontroldata3->IsSelected() == kFalse)
				itristatecontroldata3->Select();
		}
		else
		{
			//CA("isAddAsDisplayName == kFalse");
			if(itristatecontroldata3->IsSelected())
				itristatecontroldata3->Deselect();
		}

		if(isAddTableHeader)
		{
			//CA("isAddTableHeader");
			itristatecontroldata4->Select();
		}
		else
			itristatecontroldata4->Deselect();
        
        if(isAddListName)
		{
			itristatecontroldataIncludeListName->Select();
		}
		else
			itristatecontroldataIncludeListName->Deselect();
        
        
        InterfacePtr<ITreeViewController> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(treeViewMgr==nil)
		{
			//CA("treeViewMgr is NULL");
			return kFalse;
		}
        
		NodeIDList selectedItem;
		treeViewMgr->GetSelectedItems(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			return kFalse;
		}
		
		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
        
		int32 uid= uidNodeIDTemp->Get();
		int32 TextFrameuid = uidNodeIDTemp->Get();
		TPLDataNode pNode;
		TPLTreeDataCache dc;
        
		dc.isExist(uid, pNode);
        
        //CA(pNode.getName());
        //CA_NUM("pNode.getHitCount()", pNode.getHitCount());
        
        if(pNode.getHitCount() == 2 +1) // Advance Table
        {
            iIncludeTableHeaderCntrlView1->HideView();
            iIncludeListNameCntrlView->HideView();
            isAddTableHeader = kFalse;
            isAddListName = kFalse;
        }else{
            iIncludeTableHeaderCntrlView1->ShowView();
            iIncludeListNameCntrlView->ShowView();
        }
        

	}
	else if(TPLMediatorClass::imageFlag == 1)
	{
		//----------------------added 30 march by mahesh
		//itristatecontroldata5->Deselect();
		iSprayItemPerFrameCntrlView->Enable();
		//-----------------------------------------------------
		//CA("TPLMediatorClass::imageFlag == 1    Mahesh  1160");

		//---------------------------------added 22 march mahesh
			/*PMString q("SelectedRowNo : ");
			q.AppendNumber(SelectedRowNo);
			CA(q);*/
		//----------------------------------
		iTblGrpCntrlView->HideView();
		iTblGrpCntrlView->Disable();
		
		iAttrGrpCntrlView->HideView();
		iAttrGrpCntrlView->Disable();

		iImgGrpCntrlView->ShowView();
		iImgGrpCntrlView->Enable();

		//----------------------------------------------added by mahesh
		
		if(SelectedRowNo==0)
		{

			//CAlert::InformationAlert("SelectedRowNo==0");	
			itristatecontroldata5->Deselect();
			iSprayItemPerFrameCntrlView->Disable();                       
		}

		if(SelectedRowNo==3)
		{
			//CAlert::InformationAlert("SelectedRowNo==3 in imageflag mahesh 1187 ");	
			itristatecontroldata5->Deselect();
			iSprayItemPerFrameCntrlView->Disable();                //// it is disable();
		}
				
		//---------------------------------------------------till here

	
	
	
		/*isAddAsDisplayName = kFalse;
		
		IControlView* iAttNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
		if(iAttNameCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iAttNameCntrlView == nil");
			return retval;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata(iAttNameCntrlView, UseDefaultIID());
		if(itristatecontroldata==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
			return retval;
		}
		if(itristatecontroldata->IsSelected())
		{
			itristatecontroldata->Deselect();
		}

		IControlView* iTableNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputTableNameWidgetID);
		if(iTableNameCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTableNameCntrlView == nil");
			return retval;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata1(iTableNameCntrlView, UseDefaultIID());
		if(itristatecontroldata1==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
			return retval;
		}
		if(itristatecontroldata1->IsSelected())
		{
			itristatecontroldata1->Deselect(); 
		}	
		*/
		IControlView* iImageDescriptionCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsImageDescriptionWidgetID);
		if(iImageDescriptionCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iImageDescriptionCntrlView == nil");
			return retval;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata2(iImageDescriptionCntrlView, UseDefaultIID());
		if(itristatecontroldata2==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata2 == nil");			
			return retval;
		}
/////
		IControlView* iImageNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsFieldNameCheckBoxWidgetID);
		if(iImageNameCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iImageNameCntrlView == nil");
			return retval;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata3(iImageNameCntrlView, UseDefaultIID());
		if(itristatecontroldata3==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata3 == nil");			
			return retval;
		}
        
        IControlView* iOutputAsSwatchCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsSwatchWidgetID);
        if(iOutputAsSwatchCntrlView==nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iOutputAsSwatchCntrlView == nil");
            return retval;
        }
        InterfacePtr<ITriStateControlData> itristatecontroldataOpAsSwatch(iOutputAsSwatchCntrlView, UseDefaultIID());
        if(itristatecontroldataOpAsSwatch==nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldataOpAsSwatch == nil");
            return retval;
        }
        
		if(isAddAsDisplayName)
		{
			//CA("isAddAsDisplayName");
			if(itristatecontroldata3->IsSelected() == kFalse)	
				itristatecontroldata3->Select();
		}
		else
        {
			if(itristatecontroldata3->IsSelected() == kTrue)
				itristatecontroldata3->Deselect();
        }

		if(isAddImageDescription)
		{
			itristatecontroldata2->Select();
		}
		else
		{
			if(itristatecontroldata2->IsSelected())
				itristatecontroldata2->Deselect();
		}
        
        if(isOutputAsSwatch)
        {
            if(itristatecontroldataOpAsSwatch->IsSelected() == kFalse)
				itristatecontroldataOpAsSwatch->Select();
        }
        else
        {
            if(itristatecontroldataOpAsSwatch->IsSelected() == kTrue)
                itristatecontroldataOpAsSwatch->Deselect();
        }
        
		
	}
	else 
	{
		//CA("ellse");
		iTblGrpCntrlView->HideView();
		iTblGrpCntrlView->Disable();
		
		iImgGrpCntrlView->HideView();
		iImgGrpCntrlView->Disable();

		iAttrGrpCntrlView->ShowView();
		iAttrGrpCntrlView->Enable();

		//------------------------------------------

		//itristatecontroldata5->Deselect();
		//iSprayItemPerFrameCntrlView->Disable();                    // it is disable();
		
		//------------------------------------------
    
		
		/*IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
		if(iItemListCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::attachAttributes::iItemListCntrlView == nil");
			return retval;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata6(iItemListCntrlView, UseDefaultIID());
		if(itristatecontroldata6==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::attachAttributes::itristatecontroldata6 == nil");			
			return retval;
		}

		itristatecontroldata6->Deselect();
		iItemListCntrlView->Disable();*/

		////-----------------------------------------

		isAddImageDescription= kFalse;
        isOutputAsSwatch = kFalse;

		InterfacePtr<ITreeViewController> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(treeViewMgr==nil)
		{
			//CA("treeViewMgr is NULL");
			return kFalse;
		}

		NodeIDList selectedItem;
		treeViewMgr->GetSelectedItems(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			return kFalse;
		}
		
		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

		int32 uid= uidNodeIDTemp->Get();
		int32 TextFrameuid = uidNodeIDTemp->Get();
		TPLDataNode pNode;
		TPLTreeDataCache dc;

		dc.isExist(uid, pNode);
		
		PMString pfName = pNode.getName();

		IControlView* iSpreadBasedLetterKeysCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kSpreadBasedLetterKeysWidgetID);
		if(iSpreadBasedLetterKeysCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iSpreadBasedLetterKeysCntrlView == nil");
			return kFalse;
		}
		InterfacePtr<ITriStateControlData> itristatecontroldata3(iSpreadBasedLetterKeysCntrlView, UseDefaultIID());
		if(itristatecontroldata3==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata3 == nil");			
			return kFalse;
		}

		IControlView* iItemListCntrlView =TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
		if(iItemListCntrlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iItemListCntrlView is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata(iItemListCntrlView, UseDefaultIID());
		if(itristatecontroldata==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata is nil");		
			return retval;
		}

		IControlView* iIncludeHeaderCntrlView =TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
		if(iIncludeHeaderCntrlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iIncludeHeaderCntrlView is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata1(iIncludeHeaderCntrlView, UseDefaultIID());
		if(itristatecontroldata1==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata1 is nil");		
			return retval;
		}
		
		IControlView* iAttributeNameCntrlView =TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
		if(iAttributeNameCntrlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iAttributeNameCntrlView is nil");	
			return retval;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata2(iAttributeNameCntrlView, UseDefaultIID());
		if(itristatecontroldata2==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata2 is nil");		
			return retval;
		}


		IControlView* iDelUnSprayedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
		if(iDelUnSprayedCntrlView==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iDelUnSprayedCntrlView == nil");
			return retval;
		}

		//----------------------------------------------added by mahesh
		IControlView * itemListTextControlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kItemListTextWidgetID);
		if(itemListTextControlView == nil)
		{
			//CA("itemListTextControlView == nil");
			//break;
		}
		//-------------------------------------------------------------------

		if(pfName == "Letter Keys" || pfName == "Number Keys") //**** Added 10/09/09
		{

			//itristatecontroldata3->Deselect();
			
			iSpreadBasedLetterKeysCntrlView->ShowView(); 
			iSpreadBasedLetterKeysCntrlView->Enable();

			iAttributeNameCntrlView->HideView();
			iDelUnSprayedCntrlView->HideView();
			iIncludeHeaderCntrlView->HideView();
		}
		else
		{
			itristatecontroldata3->Deselect();
			iSpreadBasedLetterKeysCntrlView->Disable();
			iSpreadBasedLetterKeysCntrlView->HideView(); 
			isSpreadBasedLetterKeys = kFalse;

			iAttributeNameCntrlView->ShowView();
			iDelUnSprayedCntrlView->ShowView();
			iIncludeHeaderCntrlView->ShowView();
		}
	
		if(ISTabbedText)
		{
			//CAlert::InformationAlert("ISTabbedText");
			if(SelectedRowNo == 4)	//------
			{
				iAttributeNameCntrlView->HideView();	
				iDelUnSprayedCntrlView->HideView();
			}
			else
			{
				iAttributeNameCntrlView->ShowView();	
				iDelUnSprayedCntrlView->ShowView();
			}

			itristatecontroldata->Select();
			if(isAddTableHeader)
				itristatecontroldata1->Select();
			else
				itristatecontroldata1->Deselect();
		}
		else
		{
			//CAlert::InformationAlert("ISTabbedText=false");
			iAttributeNameCntrlView->ShowView();	//---------
			iDelUnSprayedCntrlView->ShowView();
			
			if(itristatecontroldata->IsSelected())
			{
				//CAlert::InformationAlert("itristatecontroldata->IsSelected()");
				itristatecontroldata->Deselect();
			}
			
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return retval;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			
			if(result)
			{
				//CAlert::InformationAlert("result");
				//-------------------------------------------------
				if(SelectedRowNo==0)
				{
					//CAlert::InformationAlert("SelectedRowNo==0");
										
					if(TPLMediatorClass::imageFlag==kTrue)
					{
						//CAlert::InformationAlert("TPLMediatorClass::imageFlag");
						itristatecontroldata5->Deselect();
						iSprayItemPerFrameCntrlView->Disable();
					}
					/*else if(TPLMediatorClass::tableFlag)
					{
						CAlert::InformationAlert("else if that is tableFlag");
					}*/
					
					else if(TPLMediatorClass::tableFlag==0 && TPLMediatorClass::imageFlag==0 )
					{	
						//CAlert::InformationAlert("else if of textflag that is textflag");
						iItemListCntrlView->Disable();
						itemListTextControlView->Disable();
						
					}
					else
					{
						iItemListCntrlView->Enable();
						itemListTextControlView->Enable();
						
					}
				}
				//CAlert::InformationAlert("comes out of else if of text flag and enables itemlist again");
					
				
				if(SelectedRowNo==0) // for event
				{
					//CAlert::InformationAlert("SelectedRowNo==0 is selected");
					iItemListCntrlView->Disable();
					itemListTextControlView->Disable();
				}
				else if(SelectedRowNo == 4)
				{
					//CAlert::InformationAlert("SelectedRowNo==4");
					iItemListCntrlView->Enable();
					itemListTextControlView->Enable();

					iSprayItemPerFrameCntrlView->Enable();                        // added 23 april by mahesh
				}
				else
				{
					//CAlert::InformationAlert("else of SelectedRowNo==0 is selected");
					PMString q("SelectedRowNo : ");
					q.AppendNumber(SelectedRowNo);
					//CA(q);
					if(SelectedRowNo==3) // for item group
					{
						//CAlert::InformationAlert("SelectedRowNo==3");
						iItemListCntrlView->Disable();
						itemListTextControlView->Disable();
						//break;
					}
				}
			
				

				
				//---------------------------------------------------
				isAddTableHeader = kFalse;
                isAddListName = kFalse;

				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisAddTableHeaders(0) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);
			}
		}

				//-------------------------------------adding for pivot
				/*PMString q("pfName: ");
				q.Append(pfName);
				CA(q);*/
				if(pfName == "Pivot List")    
				{
					//-----------------------------added 30 march by mahesh-
					//iItemListCntrlView->Disable();
					//itemListTextControlView->Disable();

					//iIncludeHeaderCntrlView->Disable();   //kOutputAsAttributeNameWidgetID
					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Disable();

					itristatecontroldata1->Deselect();
					iDelUnSprayedCntrlView->Disable();


					

					//------------------------------------------------------
					/*iAttrGrpCntrlView->HideView();
					iAttrGrpCntrlView->Disable();*/
				}
				else
				{
					//itristatecontroldata2->Deselect();
					//iAttributeNameCntrlView->Enable();
					
					//itristatecontroldata1->Deselect();
					//iDelUnSprayedCntrlView->Enable();

				}


			
				//---------------------------------------


		if(isAddAsDisplayName)
		{
			//CA("Attribute isAddAsDisplayName");
			itristatecontroldata2->Select();
		}
		else
		{
			if(itristatecontroldata2->IsSelected())
				itristatecontroldata2->Deselect();
		
		}
		//----

		
	}
	return retval;

}
//	end, File:	PnlTrvNodeEH.cpp
