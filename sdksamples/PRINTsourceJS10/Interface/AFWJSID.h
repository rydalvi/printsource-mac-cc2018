//========================================================================================
//  
//  $File: $
//  
//  Owner: APSIVA
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __AFWJSID_h__
#define __AFWJSID_h__

#include "SDKDef.h"

// Company:
#define kAFWJSCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kAFWJSCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kAFWJSPluginName	"APJS9_AppFramework"			// Name of this plug-in.
#define kAFWJSPrefixNumber	0xDe9100 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kAFWJSVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kAFWJSAuthor		"APSIVA"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kAFWJSPrefixNumber above to modify the prefix.)
#define kAFWJSPrefix		RezLong(kAFWJSPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kAFWJSStringPrefix	SDK_DEF_STRINGIZE(kAFWJSPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kAFWJSMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kAFWJSMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kAFWJSPluginID, kAFWJSPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kAFWJSActionComponentBoss, kAFWJSPrefix + 0)
DECLARE_PMID(kClassIDSpace, kAFWJSDialogBoss, kAFWJSPrefix + 2)
DECLARE_PMID(kClassIDSpace, kAppFrameworkBoss, kAFWJSPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kClientOptionsReaderBoss, kAFWJSPrefix + 4)
DECLARE_PMID(kClassIDSpace, kSpecialCharBoss, kAFWJSPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kAFWJSBoss, kAFWJSPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IAPPFRAMEWORK, kAFWJSPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTOPTIONS, kAFWJSPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_ISPECIALCHAR, kAFWJSPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFWJSINTERFACE, kAFWJSPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kAppFrameworkImpl, kAFWJSPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kAFWJSActionComponentImpl, kAFWJSPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kAFWJSDialogControllerImpl, kAFWJSPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kAFWJSDialogObserverImpl, kAFWJSPrefix + 3 )
//DECLARE_PMID(kImplementationIDSpace, kClientOptionsImpl, kAFWJSPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kSpecialCharImpl, kAFWJSPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kAFWJSImpl, kAFWJSPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kAFWJSAboutActionID, kAFWJSPrefix + 0)

DECLARE_PMID(kActionIDSpace, kAFWJSDialogActionID, kAFWJSPrefix + 4)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kAFWJSActionID, kAFWJSPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kAFWJSDialogWidgetID, kAFWJSPrefix + 1)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kAFWJSWidgetID, kAFWJSPrefix + 25)


// "About Plug-ins" sub-menu:
#define kAFWJSAboutMenuKey			kAFWJSStringPrefix "kAFWJSAboutMenuKey"
#define kAFWJSAboutMenuPath		kSDKDefStandardAboutMenuPath kAFWJSCompanyKey

// "Plug-ins" sub-menu:
#define kAFWJSPluginsMenuKey 		kAFWJSStringPrefix "kAFWJSPluginsMenuKey"
#define kAFWJSPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kAFWJSCompanyKey kSDKDefDelimitMenuPath kAFWJSPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kAFWJSAboutBoxStringKey	kAFWJSStringPrefix "kAFWJSAboutBoxStringKey"
#define kAFWJSTargetMenuPath kAFWJSPluginsMenuPath

// Menu item positions:

#define kAFWJSDialogTitleKey kAFWJSStringPrefix "kAFWJSDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kAFWJSDialogMenuItemKey kAFWJSStringPrefix "kAFWJSDialogMenuItemKey"

#define kAFWJSPanelWidgetStringKey	kAFWJSStringPrefix "kAFWJSPanelWidgetStringKey"
#define kAFWJSPanelWidgetAboutPRINTsourceStringKey	kAFWJSStringPrefix "kAFWJSPanelWidgetAboutPRINTsourceStringKey"
#define kAFWJSPanelWidgetHelpStringKey	kAFWJSStringPrefix "kAFWJSPanelWidgetHelpStringKey"
#define kAFWJSMainKey kAFWJSStringPrefix "kAFWJSMainKey"
#define kAFWJSPRINTsourceKey kAFWJSStringPrefix "kAFWJSPRINTsourceKey"


#define kAFWMainMenuKey kAFWMainKey kSDKDefDelimitMenuPath kAFWPRINTsourceKey

// "Plug-ins" sub-menu item position for dialog:
#define kAFWJSDialogMenuItemPosition	1


// Initial data format version numbers
#define kAFWJSFirstMajorFormatNumber  RezLong(1)
#define kAFWJSFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kAFWJSCurrentMajorFormatNumber kAFWJSFirstMajorFormatNumber
#define kAFWJSCurrentMinorFormatNumber kAFWJSFirstMinorFormatNumber

#endif // __AFWJSID_h__
