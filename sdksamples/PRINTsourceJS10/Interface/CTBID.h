//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CTBID_h__
#define __CTBID_h__

#include "SDKDef.h"

// Company:
#define kCTBCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCTBCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCTBPluginName	"APJS9_CategoryBrowser"			// Name of this plug-in.
#define kCTBPrefixNumber	0xDe9110  		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCTBVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCTBAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCTBPrefixNumber above to modify the prefix.)
#define kCTBPrefix		RezLong(kCTBPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCTBStringPrefix	SDK_DEF_STRINGIZE(kCTBPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCTBMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCTBMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCTBPluginID, kCTBPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCTBActionComponentBoss, kCTBPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCTBPanelWidgetBoss, kCTBPrefix + 1)
DECLARE_PMID(kClassIDSpace, kCTBClassTreeViewWidgetBoss,	kCTBPrefix + 3)
DECLARE_PMID(kClassIDSpace, kCTBCategoryBrowserBoss,		kCTBPrefix + 4)
DECLARE_PMID(kClassIDSpace, kCTBHTILstboxMultilineTxtWidgetBoss, kCTBPrefix + 5)
DECLARE_PMID(kClassIDSpace, kCTBTreeTextBoxWidgetBoss, kCTBPrefix + 6)
DECLARE_PMID(kClassIDSpace, kCTBLoginEventsHandler, kCTBPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCTBBoss, kCTBPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ICATEGORYBROWSER, kCTBPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTBINTERFACE, kCTBPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCTBActionComponentImpl,				kCTBPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCTBClassTreeViewHierarchyAdapterImpl, kCTBPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kCTBClassTreeViewWidgetMgrImpl,		kCTBPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kCTBClassTreeObserverImpl,				kCTBPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kCTBSelectionObserverImpl,				kCTBPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kCTBCategoryBrowserImpl,				kCTBPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kSnipCTBRunControlViewImpl, 			kCTBPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kCTBEventHandlerImpl,					kCTBPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kCTBLoginEventsHandlerImpl,			kCTBPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCTBImpl, kCTBPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCTBAboutActionID,			kCTBPrefix + 0)
DECLARE_PMID(kActionIDSpace, kCTBPanelWidgetActionID,	kCTBPrefix + 1)
DECLARE_PMID(kActionIDSpace, kCTBSeparator1ActionID,	kCTBPrefix + 2)
DECLARE_PMID(kActionIDSpace, kCTBPopupAboutThisActionID,kCTBPrefix + 3)//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 5)
DECLARE_PMID(kActionIDSpace, kCTBMenuActionID,			kCTBPrefix + 4)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCTBActionID, kCTBPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCTBPanelWidgetID,					kCTBPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kCTBClassTreeViewWidgetID,			kCTBPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kCTBClassTreeNodeExpanderWidgetID, kCTBPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kCTBClassTreeNodeNameWidgetID,		kCTBPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kCTBClassTreePanelNodeWidgetID,	kCTBPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kCTBStaticTextWidgetID,			kCTBPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kCTBMultilineTextWidgetID, 		kCTBPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kCTBRefreshButtonWidgetID,			kCTBPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kCTBClassTreeNodePUBNameWidgetID,	kCTBPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kCTBSelectlanguageDropDownWidgetID,kCTBPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kCTBSaveButtonWidgetID,			kCTBPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kCTBCloseButtonWidgetID,			kCTBPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kCTBSettingsSavedStaticTextWidgetID, kCTBPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kCTBWidgetID, kCTBPrefix + 25)


// "About Plug-ins" sub-menu:
#define kCTBAboutMenuKey			kCTBStringPrefix "kCTBAboutMenuKey"
#define kCTBAboutMenuPath		kSDKDefStandardAboutMenuPath kCTBCompanyKey

// "Plug-ins" sub-menu:
#define kCTBPluginsMenuKey 		kCTBStringPrefix "kCTBPluginsMenuKey"
#define kCTBPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCTBCompanyKey kSDKDefDelimitMenuPath kCTBPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCTBAboutBoxStringKey					kCTBStringPrefix "kCTBAboutBoxStringKey"
#define kCTBPanelTitleKey						kCTBStringPrefix	"kCTBPanelTitleKey"
#define kCTBStaticTextKey						kCTBStringPrefix	"kCTBStaticTextKey"
#define kCTBInternalPopupMenuNameKey			kCTBStringPrefix	"kCTBInternalPopupMenuNameKey"
#define kCTBTargetMenuPath						kCTBInternalPopupMenuNameKey
//#define kCTBPanelTitleKey kCTBStringPrefix "kCTBPanelTitleKey"
#define kCTBSelectLangaugeandEventCategoryStringKey		kCTBStringPrefix "kCTBSelectLangaugeandEventCategoryStringKey"
#define kCTBSelectLanguageStringKey				kCTBStringPrefix	"kCTBSelectLanguageStringKey"
#define kCTBNoTextStringKey						kCTBStringPrefix	"kCTBNoTextStringKey"
#define kCTBBlankSpaceStringKey					kCTBStringPrefix	"kCTBBlankSpaceStringKey"

// Menu item positions:

#define	kCTBSeparator1MenuItemPosition		10.0
#define kCTBAboutThisMenuItemPosition		11.0
#define kCTBClassTreePanelNodeRsrcID		11012
#define kCTBRefreshIconID					11000  
#define kCTBSaveIconID						11001  


// Initial data format version numbers
#define kCTBFirstMajorFormatNumber  RezLong(1)
#define kCTBFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCTBCurrentMajorFormatNumber kCTBFirstMajorFormatNumber
#define kCTBCurrentMinorFormatNumber kCTBFirstMinorFormatNumber

// PNG specific ID
#define kCTPNGRefreshRsrcID						20000
#define kCTPNGRefreshRollRsrcID                 20000

#define kCTPNGSaveRsrcID                        20001   
#define kCTPNGSaveRollRsrcID					20001

#define kCTPNGCloseRsrcID						20002
#define kCTPNGCloseRollRsrcID					20002

#endif // __CTBID_h__
