#ifndef __ICMMInterface__
#define __ICMMInterface__

#include "IPMUnknown.h"
#include "CMMID.h"
#include "IAppFramework.h"

class ICMMInterface : public IPMUnknown
{
    public:
		enum { kDefaultIID = IID_ICMMINTERFACE };
    
		virtual void setEventInCMM(const double eventId, const double languageId ) = 0;
		
};

#endif