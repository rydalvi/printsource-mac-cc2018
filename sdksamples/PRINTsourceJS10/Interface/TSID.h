//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __TSID_h__
#define __TSID_h__

#include "SDKDef.h"

// Company:
#define kTSCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kTSCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kTSPluginName	"Lists & Tables" //"APJS9_TableSource"			// Name of this plug-in.
#define kTSPrefixNumber	0xe9800 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kTSVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kTSAuthor		"Apsiva Inc"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kTSPrefixNumber above to modify the prefix.)
#define kTSPrefix		RezLong(kTSPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kTSStringPrefix	SDK_DEF_STRINGIZE(kTSPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kTSMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kTSMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kTSPluginID, kTSPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kTSActionComponentBoss, kTSPrefix + 0)
DECLARE_PMID(kClassIDSpace, kTSPanelWidgetBoss, kTSPrefix + 1)
DECLARE_PMID(kClassIDSpace, kTSTableSourceHelperBoss, kTSPrefix + 3)
DECLARE_PMID(kClassIDSpace, kTSTreeViewWidgetBoss, kTSPrefix + 4)
DECLARE_PMID(kClassIDSpace, kTSTreeNodeWidgetBoss, kTSPrefix + 5)
DECLARE_PMID(kClassIDSpace, kTSSelectionObserverWidgetBoss, kTSPrefix + 6)
DECLARE_PMID(kClassIDSpace, kTSTreeTextBoxWidgetBoss, kTSPrefix + 7)
DECLARE_PMID(kClassIDSpace, kTSIconSuiteWidgetBoss, kTSPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kTSPRTreeWidgetBoss, kTSPrefix + 9)
DECLARE_PMID(kClassIDSpace, kTSLoginEventsHandlerBoss, kTSPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kTSBoss, kTSPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IPRODUCTTABLEIFACE, kTSPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IPNLTRVSHADOWEVENTHANDLER, kTSPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITSINTERFACE, kTSPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kTSActionComponentImpl, kTSPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kTSTableSourceHelperImpl, kTSPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kTSSnipRunControlViewImpl, kTSPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kTSTreeViewHierarchyAdapterImpl, kTSPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kTSTreeViewWidgetMgrImpl, kTSPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kTSSelectionObserverImpl, kTSPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kTSSprayerInterfaceImpl, kTSPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kTSLoginEventsHandlerImpl, kTSPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kPnlTrvNodeEHImpl, kTSPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kTSImpl, kTSPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kTSAboutActionID, kTSPrefix + 0)
DECLARE_PMID(kActionIDSpace, kTSPanelWidgetActionID, kTSPrefix + 1)
DECLARE_PMID(kActionIDSpace, kTSSeparator1ActionID, kTSPrefix + 2)
DECLARE_PMID(kActionIDSpace, kTSPopupAboutThisActionID, kTSPrefix + 3)//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kTSActionID, kTSPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kTSPanelWidgetID, kTSPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kTSTableSourceGroupPanelWidgetID, kTSPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kTSRelodButtonWidgetID, kTSPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kTSPreviewButtonWidgetID, kTSPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kTSEditStencilButtonWidgetID, kTSPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kTSSprayButtonWidgetID, kTSPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kTSTreeGroupPanelWidgetID, kTSPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kTSTreeViewWidgetID, kTSPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kTSTreePanelNodeWidgetID, kTSPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kTSTreeNodeExpanderWidgetID, kTSPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kTSTreeNodeNameWidgetID, kTSPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kTSTreeListIconSuiteWidgetID, kTSPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kTSTreeTableIconSuiteWidgetID, kTSPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kStaticTextGroupWidgetID, kTSPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kListTableNameWidgetID, kTSPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kStencilNameWidgetID, kTSPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kUpdateDateWidgetID, kTSPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kTSUpButtonWidgetID, kTSPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kTSDownButtonWidgetID, kTSPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kTSWidgetID, kTSPrefix + 25)


// "About Plug-ins" sub-menu:
#define kTSAboutMenuKey			kTSStringPrefix "kTSAboutMenuKey"
#define kTSAboutMenuPath		kSDKDefStandardAboutMenuPath kTSCompanyKey

// "Plug-ins" sub-menu:
#define kTSPluginsMenuKey 		kTSStringPrefix "kTSPluginsMenuKey"
#define kTSPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kTSCompanyKey kSDKDefDelimitMenuPath kTSPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kTSAboutBoxStringKey	kTSStringPrefix "kTSAboutBoxStringKey"
#define kTSPanelTitleKey					kTSStringPrefix	"kTSPanelTitleKey"
#define kTSStaticTextKey kTSStringPrefix	"kTSStaticTextKey"
#define kTSInternalPopupMenuNameKey kTSStringPrefix	"kTSInternalPopupMenuNameKey"
#define kTSTargetMenuPath kTSInternalPopupMenuNameKey

#define kTSListTableNameKey kTSStringPrefix "kTSListTableNameKey"
#define kTSStencilNaleKey kTSStringPrefix "kTSStencilNaleKey"
#define kTSUpdateDateKey kTSStringPrefix "kTSUpdateDateKey"

#define kTSReloadKey kTSStringPrefix "kTSReloadKey"
#define kTSSprayKey kTSStringPrefix  "kTSSprayKey"
#define kTSPreviewKey kTSStringPrefix "kTSPreviewKey"
#define kTSEditStencilkey kTSStringPrefix "kTSEditStencilkey"

// Menu item positions:

#define	kTSSeparator1MenuItemPosition		10.0
#define kTSAboutThisMenuItemPosition		11.0


// Initial data format version numbers
#define kTSFirstMajorFormatNumber  RezLong(1)
#define kTSFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kTSCurrentMajorFormatNumber kTSFirstMajorFormatNumber
#define kTSCurrentMinorFormatNumber kTSFirstMinorFormatNumber

#define	kTSPreviewIcon				80000
#define kTSRelodIcon				80002
#define kTSSprayIcon				80004
#define kTSEditStencilIcon			80006
#define	kTSTableIcon				80008
#define	kTSListIcon					80010
#define	kTSUpArrowIcon				80012
#define	kTSDownArrowIcon			80014

#define kTSTreePanelNodeRsrcID		8200

#endif // __TSID_h__
