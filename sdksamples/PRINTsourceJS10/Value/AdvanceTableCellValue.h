#ifndef __CAdvanceTableCellValue__h
#define __CAdvanceTableCellValue__h

#include "VCPluginHeaders.h"
#include "PMString.h"
using namespace std;

class CAdvanceTableCellValue
{
public:
	
	double rowId;
	double colId;
	int32 rowLength;
	int32 colLength;
	double fieldId;
	double itemId;
	double assetTypeId;
	double languageId;
	PMString value;
    double cellId;

	CAdvanceTableCellValue()
	{		
		rowId = -1;
		colId = -1;
		rowLength = -1;
		colLength = -1;
		fieldId = -1;
		itemId = -1;
		assetTypeId = -1;
		languageId = -1;
		value = "";
        cellId = -1;
	}

	double getRowId()
	{
		return rowId;
	}
	void setRowId(double id)
	{
		rowId = id;
	}

	double getColId()
	{
		return colId;
	}
	void setColId(double id)
	{
		colId = id;
	}

	int32 getRowLength()
	{
		return rowLength;
	}
	void setRowLength(int32 id)
	{
		rowLength = id;
	}

	int32 getColLength()
	{
		return colLength;
	}
	void setColLength(int32 id)
	{
		colLength = id;
	}

	double getFieldId()
	{
		return fieldId;
	}
	void setFieldId(double id)
	{
		fieldId = id;
	}

	double getItemId()
	{
		return itemId;
	}
	void setItemId(double id)
	{
		itemId = id;
	}

	double getAssetTypeId()
	{
		return assetTypeId;
	}
	void setAssetTypeId(double id)
	{
		assetTypeId = id;
	}

	double getLanguageId()
	{
		return languageId;
	}
	void setLanguageId(double id)
	{
		languageId = id;
	}

	PMString getValue()
	{
		return value;
	}
	void setValue(PMString temp)
	{
		value = temp;
	}
    
    double getCellId()
	{
		return cellId;
	}
	void setCellId(double id)
	{
		cellId = id;
	}

};
#endif