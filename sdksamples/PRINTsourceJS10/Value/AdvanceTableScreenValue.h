#ifndef __CAdvanceTableScreenValue_h__
#define __CAdvanceTableScreenValue_h__

#include "VCPluginHeaders.h"
#include "AdvanceTableCellValue.h"
#include "PMString.h"
#include "vector"
using namespace std;


class CAdvanceTableScreenValue
{
private:
	double tableId;
	int32 headerRowCount;
	int32 bodyRowCount;
	int32 columnCount;
	PMString tableName;
	double tableTypeId;
	double languageId;
	double sectionResultId;
    PMString note1;
    PMString note2;
    PMString note3;
    PMString note4;
    PMString note5;

	vector<CAdvanceTableCellValue> cells;
	
public:

	inline double getTableId()
	{
		return tableId;
	}

	inline void setTableId(double id)
	{
		tableId = id;
	}

	inline int32 getHeaderRowCount()
	{
		return headerRowCount;
	}

	inline void setHeaderRowCount(int32 id)
	{
		headerRowCount = id;
	}

	inline int32 getBodyRowCount()
	{
		return bodyRowCount;
	}

	inline void setBodyRowCount(int32 id)
	{
		bodyRowCount = id;
	}

	inline PMString getTableName()
	{
		return tableName;
	}

	inline void setTableName(PMString temp)
	{
		tableName = temp;
	}

	inline double getTableTypeId()
	{
		return tableTypeId;
	}

	inline void setTableTypeId(double id)
	{
		tableTypeId = id;
	}

	inline int32 getColumnCount()
	{
		return columnCount;
	}

	inline void setColumnCount(int32 id)
	{
		columnCount = id;
	}

	inline double getSectionResultId()
	{
		return sectionResultId;
	}

	inline void setSectionResultId(double id)
	{
		sectionResultId = id;
	}

	inline vector<CAdvanceTableCellValue> getCells()
	{
		return cells;
	}

	inline void setCells(vector<CAdvanceTableCellValue> cells)
	{
		this->cells = cells;
	}

	inline double getLanguageId()
	{
		return languageId;
	}

	inline void setLanguageId(double id)
	{
		languageId = id;
	}
	
    inline PMString getNote1()
	{
		return note1;
	}
    
	inline void setNote1(PMString temp)
	{
		note1 = temp;
	}
    
    inline PMString getNote2()
	{
		return note2;
	}
    
	inline void setNote2(PMString temp)
	{
		note2 = temp;
	}
    
    inline PMString getNote3()
	{
		return note3;
	}
    
	inline void setNote3(PMString temp)
	{
		note3 = temp;
	}
    
    inline PMString getNote4()
	{
		return note4;
	}
    
	inline void setNote4(PMString temp)
	{
		note4 = temp;
	}
    
    inline PMString getNote5()
	{
		return note5;
	}
    
	inline void setNote5(PMString temp)
	{
		note5 = temp;
	}
};

#endif 