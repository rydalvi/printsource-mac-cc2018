#ifndef __Attribute__
#define __Attribute__


#include "VCPlugInHeaders.h"
#include "PicklistField.h"
#include "PMString.h"
#include "Details.h"

using namespace std;

class Attribute : public PicklistField
{

private:

	PMString attributeNo;
	PMString tableCol; // sysName
	PMString displayName;
	PMString emptyValueDisplay;
	double attributeId;
	double catalogId;

	int32 uniqueness;
	int32 uniquenessCondensed;
	int32 uniquenessCaseSensitive;
	int32 minLength;
	int32 maxLength;
	int32 decMinLength;
	int32 decMaxLength;
	int32 required;
	int32 editLines;
	int32 allowNewEntry;
	double domainId;
	double unitId;
	double languageId;
	double parentAttributeId;
	double typeId;
	int32 pubItem;
	
	PMString dataTypeString;
	bool16	isParametric;
	double dataTypeId;
    bool16 isEventSpecific;

	vector<Details>	nameList;


public:

	Attribute()
	{
		attributeId = -1;
		attributeNo = "";
		//class_id = 0;	
		displayName = "";
		emptyValueDisplay ="";
		//seq_order= -1;	
		uniqueness= 0;	
		uniquenessCondensed = 0;
		uniquenessCaseSensitive = 0;
		//data_type=-1;		
		minLength=-1;
		maxLength= -1;
		decMinLength= -1;
		decMaxLength= -1;
		required= kFalse;			
		languageId= -1;		
		parentAttributeId= -1;
		typeId= -1;	
		isParametric= kFalse;
		pubItem = kFalse;
		catalogId = -1;
		editLines =0;
		allowNewEntry=0;
		domainId=-1;
		unitId =-1;
		pubItem=0;
		dataTypeString = "";
		isParametric = kFalse;
		dataTypeId = -1;
        isEventSpecific = kFalse;
	}

	PMString getDataType() {
		return dataTypeString;
	}

	void setDataType(PMString dataTypeString) {
		this->dataTypeString = dataTypeString;
	}

	void setName(PMString attributeName) {
		this->displayName = attributeName;
	}

	PMString getNumber() {
		return attributeNo;
	}

	void setNumber(PMString attributeNo) {
		this->attributeNo = attributeNo;
	}

	PMString getTableCol() {
		return tableCol;
	}

	void setTableCol(PMString tableCol) {
		this->tableCol = tableCol;
	}

	PMString getDisplayName() {
		return displayName;
	}

	void setDisplayName(PMString displayName) {
		this->displayName = displayName;
	}

	PMString getEmptyValueDisplay() {
		return emptyValueDisplay;
	}

	void setEmptyValueDisplay(PMString emptyValueDisplay) {
		this->emptyValueDisplay = emptyValueDisplay;
	}


	double getAttributeId() {
		return attributeId;
	}

	void setAttributeId(double attributeId) {
		this->attributeId = attributeId;
	}


	double getCatalogId() {
		return catalogId;
	}

	void setCatalogId(double catalogId) {
		this->catalogId = catalogId;
	}

	int32 getUniqueness() {
		return uniqueness == 0 ? 0 : uniqueness;
	}

	void setUniqueness(int32 uniqueness) {
		this->uniqueness = uniqueness;
	}

	int32 getUniquenessCondensed() {
		return uniquenessCondensed;
	}

	void setUniquenessCondensed(int32 uniquenessCondensed) {
		this->uniquenessCondensed = uniquenessCondensed;
	}

	int32 getUniquenessCaseSensitive() {
		return uniquenessCaseSensitive;
	}

	void setUniquenessCaseSensitive(int32 uniquenessCaseSensitive) {
		this->uniquenessCaseSensitive = uniquenessCaseSensitive;
	}

	int32 getMinLength() {
		return minLength;
	}

	void setMinLength(int32 minLength) {
		this->minLength = minLength;
	}

	int32 getMaxLength() {
		return maxLength;
	}

	void setMaxLength(int32 maxLength) {
		this->maxLength = maxLength;
	}

	int32 getDecMinLength() {
		return decMinLength;
	}

	void setDecMinLength(int32 decMinLength) {
		this->decMinLength = decMinLength;
	}

	int32 getDecMaxLength() {
		return decMaxLength;
	}

	void setDecMaxLength(int32 decMaxLength) {
		this->decMaxLength = decMaxLength;
	}

	int32 getRequired() {
		return required;
	}

	void setRequired(int32 required) {
		this->required = required;
	}

	int32 getEditLines() {
		return editLines;
	}

	void setEditLines(int32 editLines) {
		this->editLines = editLines;
	}

	int32 getAllowNewEntry() {
		return allowNewEntry;
	}

	void setAllowNewEntry(int32 allowNewEntry) {
		this->allowNewEntry = allowNewEntry;
	}

	double getDomainId() {
		return domainId;
	}

	void setDomainId(double domainId) {
		this->domainId = domainId;
	}

	double getUnitId() {
		return unitId;
	}

	void setUnitId(double unitId) {
		this->unitId = unitId;
	}

	double getLanguageId() {
		return languageId;
	}

	void setLanguageId(double languageId) {
		this->languageId = languageId;
	}

	double getParentAttributeId() {
		return parentAttributeId;
	}

	void setParentAttributeId(double parentAttributeId) {
		this->parentAttributeId = parentAttributeId;
	}

	double getTypeId() {
		return typeId;
	}

	void setTypeId(double typeId) {
		this->typeId = typeId;
	}

	int32 getPubItem() {
		return pubItem;
	}

	void setPubItem(int32 pubItem) {
		this->pubItem = pubItem;
	}

	bool16 getisParametric(){
		return isParametric;
	}

	void setisParametric(bool16 Flag){
		this->isParametric = Flag;
	}

	double getDataTypeId() {
		return dataTypeId;
	}

	void setDataTypeId(double Id) {
		this->dataTypeId = Id;
	}

	void setNameList(vector<Details> vectorObj)
	{
		nameList = vectorObj;
	}

	vector<Details> getNameList()
	{
		return nameList;
	}


	PMString getDisplayNameByLanguageId(double languageId) {

		PMString result("");
		for(int32 i =0; i < nameList.size(); i++)
		{
			Details detailObj = nameList.at(i);
			if(detailObj.getLanguageId() == languageId)
			{
				return detailObj.getValue();
			}
		}
		return displayName;
	}
    
    bool16 getIsEventSpecific(){
		return isEventSpecific;
	}
    
	void setIsEventSpecific(bool16 Flag){
		this->isEventSpecific = Flag;
	}
    
};

#endif
