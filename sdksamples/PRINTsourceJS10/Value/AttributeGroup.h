#ifndef __ATTRIBUTEGROUP__
#define __ATTRIBUTEGROUP__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class AttributeGroup
{
private:

    PMString name;
    double attributeGroupId;
    int32 seqOrder;
    PMString key;
    vector<double> attributeIds;
    
public:
	AttributeGroup()
	{
        name = "";
        attributeGroupId = -1;
        key="";
	}
	
	~AttributeGroup()
	{
	}
	
    PMString getName() {
        return name;
    }
    
    void setName(PMString name) {
        this->name = name;
    }
    
    double getAttributeGroupId() {
        return attributeGroupId;
    }
    
    void setAttributeGroupId(double attributeGroupId) {
        this->attributeGroupId = attributeGroupId;
    }
    
    double getSeqOrder() {
        return seqOrder;
    }
    
    void setSeqOrder(int32 Id) {
        this->seqOrder = Id;
    }
    
    void setAttributeIds(vector<double> vectorObj)
    {
        attributeIds = vectorObj;
    }
    
    vector<double> getAttributeIds()
    {
        return attributeIds;
    }
    
    PMString getKey() {
        return key;
    }
    
    void setKey(PMString key) {
        this->key = key;
    }

};

#endif
