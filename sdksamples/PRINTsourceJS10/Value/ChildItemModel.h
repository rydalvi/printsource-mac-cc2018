#ifndef __ChildItemModel_H__
#define __ChildItemModel_H__

#include "VCPluginHeaders.h"
#include "PMString.h"

class ChildItemModel
{
private:
	double ItemID;
	PMString ItemDescription;
	PMString ItemNumber;
	double  ClassID;
	double  sectionResultId;
	double  parentId;
	double	parent_type_id;


public:

	ChildItemModel()
	{
		ItemID = -1;
		ItemDescription = "";
		ItemNumber = "";
		ClassID = -1;
		sectionResultId =-1;
		parentId =-1;
		parent_type_id = -1;

	}

	double getItemID()
	{
		return ItemID;
	}

	PMString getItemDesc()
	{
		return ItemDescription;
	}

	PMString getItemNo()
	{
		return ItemNumber;
	}

	void setItemID(double id)
	{
		ItemID = id;
	}

	void setItemDesc(PMString itemdesc)
	{
		ItemDescription = itemdesc;
	}

	void setItemNo(PMString itemNo)
	{
		ItemNumber = itemNo;
	}
	
	double getClassID()
	{
		return ClassID;
	}
	void setClassID(double id)
	{
		ClassID = id;
	}

	double getSectionResultId() {
		return sectionResultId;
	}

	void setSectionResultId(double id) {
		sectionResultId = id;
	}

	double getParentId()
	{
		return parentId;
	}

	void setParentId(double id)
	{
		parentId = id;
	}

	double getParent_type_id()
	{
		return parent_type_id;
	}

	void setParent_type_id(double id)
	{
		parent_type_id = id;
	}
   
};


#endif
