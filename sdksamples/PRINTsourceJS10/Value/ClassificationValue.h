#ifndef __CClassificationValue_h__
#define __CClassificationValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CClassificationValue
{
private:
	double class_id;
	PMString classification_name;	
	int32 class_level;
	double parent_id;
	int32 child_count;

public:
	CClassificationValue()
	{
	}	
	
	double getClass_id()
	{
		return class_id;
	}
	
	long getClass_level()
	{
		return class_level;
	}	
	
	PMString getClassification_name()
	{
		return classification_name;
	}	
	
	double getParent_id()
	{
		return parent_id;
	}
	
	long getChildCount()
	{
		return child_count;
	}

	void setClass_id(double newClass_id)
	{
		class_id = newClass_id;
	}
	
	void setClass_level(long newClass_level)
	{
		class_level = newClass_level;
	}	
	
	void setClassification_name(PMString newClassification_name)
	{
		classification_name = newClassification_name;
	}	
	
	void setParent_id(double newParent_id)
	{
		parent_id = newParent_id;
	}	
	
	void setChildCount(long count)
	{
		child_count=count;
	}
};

#endif