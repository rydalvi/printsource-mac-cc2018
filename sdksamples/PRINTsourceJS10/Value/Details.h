#ifndef __DETAILS__
#define __DETAILS__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class Details
{
private:
	double languageId;
	PMString fieldName;
	PMString value;

public:
	Details()
	{
		languageId = -1;
		fieldName = "";
		value ="";
	}
	
	~Details()
	{
	}
	

	double getLanguageId() {
		return languageId;
	}

	void setLanguageId(double languageId) {
		this->languageId = languageId;
	}

	PMString getFieldName() {
		return fieldName;
	}

	void setFieldName(PMString fieldName) {
		this->fieldName = fieldName;
	}
	
	PMString getValue() {
		return value;
	}

	void setValue(PMString value) {
		this->value = value;
	}

};

#endif