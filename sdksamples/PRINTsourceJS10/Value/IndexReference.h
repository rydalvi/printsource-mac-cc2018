#ifndef __IndexReference_H__
#define __IndexReference_H__

#include "VCPluginHeaders.h"
#include "PMString.h"

class IndexReference
{
private:
	double SectionID;
	double ParentTypeID;
	double ParentID;
	double ChildID;
	double OldFieldID;
	double NewFieldID;
	double TableId;
	PMString NewValue;
	bool16 isItem;
	bool16 isItemGroup;
	bool16 isSection;

	vector<PMString>		indexTerms;
	vector<PMString>		indexTerms2;
	vector<PMString>		indexTerms3;

public:
	IndexReference()
	{
		SectionID = -1;
		ParentTypeID = -1;
		ParentID = -1;
		ChildID = -1;
		TableId = -1;
	}

	double getSectionID()
	{
		return SectionID;
	}

	double getParentTypeID()
	{
		return ParentTypeID;
	}

	double getParentID()
	{
		return ParentID;
	}

	double getChildID()
	{
		return ChildID;	
	}

	double getNewFieldID()
	{
		return NewFieldID;	
	}

	double getOldFieldID()
	{
		return OldFieldID;	
	}

	PMString getNewValue()
	{
		return NewValue;
	}

	bool16 getIsItem()
	{
		return isItem;
	}

	bool16 getIsItemGroup()
	{
		return isItemGroup;
	}

	bool16 getIsSection()
	{
		return isSection;
	}

	void setIsItem(bool16 flag)
	{
		isItem = flag;
	}

	void setIsItemGroup(bool16 flag)
	{
		isItemGroup = flag;
	}

	void setIsSection(bool16 flag)
	{
		isSection = flag;
	}

	void setSectionID(double id)
	{
		SectionID = id;
	}

	void setParentTypeID(double id)
	{
		ParentTypeID = id;
	}

	void setParentID(double id)
	{
		ParentID = id;
	}

	void setChildID(double id)
	{
		ChildID = id;
	}

	void setNewFieldID(double id)
	{
		NewFieldID = id;
	}

	void setOldFieldID(double id)
	{
		OldFieldID = id;
	}

	void setNewValue(PMString v)
	{
		NewValue = v;
	}

	void setIndexTerms(vector<PMString> vectorObj)
	{
		indexTerms = vectorObj;
	}

	vector<PMString> getIndexTerms()
	{
		return indexTerms;
	}

	void setIndexTerms2(vector<PMString> vectorObj2)
	{
		indexTerms2 = vectorObj2;
	}

	vector<PMString> getIndexTerms2()
	{
		return indexTerms2;
	}

	void setIndexTerms3(vector<PMString> vectorObj3)
	{
		indexTerms3 = vectorObj3;
	}

	vector<PMString> getIndexTerms3()
	{
		return indexTerms3;
	}

	
	double getTableId()
	{
		return TableId;	
	}

	void setTableId(double id)
	{
		TableId = id;
	}
};


#endif