#ifndef __ItemModel_H__
#define __ItemModel_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "ItemValue.h"
#include "ItemTableValue.h"
#include "AssetValue.h"

class CItemModel
{
private:
	double ItemID;
	double BrandID;
	double ManufacturerID;
	double SupplierID;
	PMString ItemDescription;
	PMString ItemNumber;
	double  ClassID;
	double  StageId;
	int32   childItemCount;
	double  sectionResultId;
	double  parentId;
	double	parent_type_id;
	int32	isStared;  // values 0 or 1
	int32	isNewProduct; // values 0 or 1

	vector<CAssetValue>		assetList;
	vector<ItemValue>		values;
	vector<CItemTableValue>	lists;
	vector<CAdvanceTableScreenValue> advanceTables;
    vector<AttributeGroup> attributeGroups;
    PMString attributeValueStr;

public:

	CItemModel()
	{
		ItemID = -1;
		BrandID = -1;
		ManufacturerID = -1;
		SupplierID = -1;
		ItemDescription = "";
		ItemNumber = "";
		ClassID = -1;
		StageId = -1;
		childItemCount = -1;
		sectionResultId =-1;
		parentId =-1;
		parent_type_id = -1;
		isStared =0;
		isNewProduct =0;
        attributeValueStr ="";
	}

	double getItemID()
	{
		return ItemID;
	}

	double getBrandID()
	{
		return BrandID;
	}

	double getManufacturerID()
	{
		return ManufacturerID;
	}

	double getSupplierID()
	{
		return SupplierID;	
	}

	PMString getItemDesc()
	{
		return ItemDescription;
	}

	PMString getItemNo()
	{
		return ItemNumber;
	}

	void setItemID(double id)
	{
		ItemID = id;
	}

	void setBrandID(double brndID)
	{
		BrandID = brndID;
	}

	void setManufacturerID(double manuID)
	{
		ManufacturerID = manuID;
	}

	void setSupplierID(double suppID)
	{
		SupplierID = suppID;
	}

	void setItemDesc(PMString itemdesc)
	{
		ItemDescription = itemdesc;
	}

	void setItemNo(PMString itemNo)
	{
		ItemNumber = itemNo;
	}
	
	double getClassID()
	{
		return ClassID;
	}
	void setClassID(double id)
	{
		ClassID = id;
	}

	double getStageId()
	{
		return StageId;
	}
	void setStageId(double id)
	{
		StageId = id;
	}

	int32 getChildItemCount()
	{
		return childItemCount;	
	}
	void setChildItemCount(int32 count)
	{
		childItemCount = count;
	}


	void setAssetList(vector<CAssetValue> vectorObj)
	{
		assetList = vectorObj;
	}

	vector<CAssetValue> getAssetList()
	{
		return assetList;
	}

	void setValues(vector<ItemValue> vectorObj)
	{
		values = vectorObj;
	}

	vector<ItemValue> getValues()
	{
		return values;
	}

	void setLists(vector<CItemTableValue> vectorObj)
	{
		lists = vectorObj;
	}

	vector<CItemTableValue> getLists()
	{
		return lists;
	}

	double getSectionResultId() {
		return sectionResultId;
	}

	void setSectionResultId(double id) {
		sectionResultId = id;
	}

	double getParentId()
	{
		return parentId;
	}

	void setParentId(double id)
	{
		parentId = id;
	}

	double getParent_type_id()
	{
		return parent_type_id;
	}

	void setParent_type_id(double id)
	{
		parent_type_id = id;
	}

	//isStared
	int32 getIsStared() {
		return isStared;
	}

	void setIsStared(int32 id) {
		isStared = id;
	}

	//isNewProduct
	int32 getIsNewProduct() {
		return isNewProduct;
	}

	void setIsNewProduct(int32 id) {
		isNewProduct = id;
	}

	void setAdvanceTables(vector<CAdvanceTableScreenValue> vectorObj)
	{
		advanceTables = vectorObj;
	}

	vector<CAdvanceTableScreenValue> getAdvanceTables()
	{
		return advanceTables;
	}
    
    void setAttributeGroups(vector<AttributeGroup> vectorObj)
    {
        attributeGroups = vectorObj;
    }
    
    vector<AttributeGroup> getAttributeGroups()
    {
        return attributeGroups;
    }
    
    vector<double> getAttributeIdsByAttributeGroupId(double attributeGroupId)
    {
        vector<double> result;
        if(attributeGroups.size() > 0)
        {
            for(int count=0; count < attributeGroups.size(); count++)
            {
                if(attributeGroupId == attributeGroups.at(count).getAttributeGroupId())
                {
                    result = attributeGroups.at(count).getAttributeIds();
                    break;
                }
            }
        }
        return result;
    }
    
    
    PMString getAttributeValueStr()
    {
        return attributeValueStr;
    }
    
    void setAttributeValueStr(PMString str)
    {
        attributeValueStr = str;
    }
   
};


struct Item
{
public : 
	double itemId;
	double itemType;
	int32 count;
};

#endif
