#ifndef __LOGININFOVALUE__
#define __LOGININFOVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "ClientInfoValue.h"

using namespace std;

class LoginInfoValue
{
private:
	PMString envName;	
	PMString cmurl;
	PMString username;
	PMString password;

	PMString dbVendorName;
	PMString dbVersion;
	PMString dbServerName;	
	int32    dbServerPort;  //isDefault		
	PMString dbSchema;
	PMString dbUserName;	//userName
	PMString dbPassword;	//Password
		
	ClientInfoValue currentClientInfo;
	vector<ClientInfoValue> clientList;	



public:
	LoginInfoValue()
	{
		envName = "";	
		cmurl = "";
		username = "";
		password = "";
				
		dbVendorName= "";	
		dbVersion= "";	
		dbServerName= "";		
		dbServerPort= 0;		
		dbSchema= "";	
		dbUserName= "";	
		dbPassword= "";	

	}
	
	~LoginInfoValue()
	{
	}

	PMString getCmurl()
	{
		return cmurl;
	}

	void setCmurl(PMString str)
	{
		cmurl = str;
	}

	PMString getUsername()
	{
		return username;
	}

	void setUsername(PMString str)
	{
		username = str;
	}

	PMString getPassword()
	{
		return password;
	}

	void setPassword(PMString str)
	{
		password = str;
	}
		
		
	
	PMString getEnvName(void)
	{
		return envName;
	}
	
	void setEnvName(PMString envName1)
	{
		envName=envName1;
	}	

	ClientInfoValue getClientInfoByClientNo(PMString clientNo)
	{
		for(int i= 0; i < clientList.size(); i++)
		{
			ClientInfoValue clientInfoObj = clientList.at(i);
			if(clientInfoObj.getClientno() == clientNo )
				{
					currentClientInfo = clientInfoObj;
					return currentClientInfo;
				}
		}
		return currentClientInfo;
	}

	void setClientInfoValue(ClientInfoValue clinetInfoObj)
	{
		bool flag = false;

		for(int i= 0; i < clientList.size(); i++)
		{			
			if(clientList.at(i).getClientno() == clinetInfoObj.getClientno() )
			{
				clientList.at(i) = clinetInfoObj;
				currentClientInfo = clinetInfoObj;
				flag = true;
				break;
			}
		}
		if(!flag)
		{
			clientList.push_back(clinetInfoObj);
			currentClientInfo = clinetInfoObj;
		}
	}

	void setClientList(vector<ClientInfoValue> vectClinetInfoObj)
	{
		clientList = vectClinetInfoObj;
	}

	vector<ClientInfoValue> getClientList()
	{
		return clientList;
	}

	PMString getVendorName(void)
	{
		return dbVendorName;
	}

	PMString getVersion(void)
	{
		return dbVersion;
	}

	PMString getServerName(void)
	{
		return dbServerName;
	}

	int32 getServerPort(void)
	{
		return dbServerPort;
	}

	PMString getSchema(void)
	{
		return dbSchema;
	}

	PMString getDbUserName(void)
	{
		return dbUserName;
	}

	PMString getDbPassword(void)
	{
		return dbPassword;
	}


	void setVendorName(PMString vendorName)
	{
		dbVendorName=vendorName;
	}

	void setVersion(PMString version)
	{
		dbVersion=version;
	}

	void setServerName(PMString serverName)
	{
		dbServerName=serverName;
	}

	void setServerPort(int32 serverPort)
	{
		dbServerPort=serverPort;
	}
	
	void setSchema(PMString schema)
	{
		dbSchema=schema;
	}
	
	void setDbUserName(PMString userName)
	{
		dbUserName=userName;
	}
	
	void setDbPassword(PMString password)
	{
		dbPassword=password;
	}
};

#endif