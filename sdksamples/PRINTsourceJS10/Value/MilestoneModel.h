#ifndef __CMILESTONEMODEL_H__
#define __CMILESTONEMODEL_H__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CMilestoneModel 
{

public :
	 PMString name ;
	 PMString number;
	 PMString code;
	 double parent_type_id;
	 int32 seq_order;
	 double id ;
	
	PMString getCode() 
	{
		return code;
	}
	
	void setCode(PMString code) 
	{
		this->code = code;
	}
	
	PMString getNumber() 
	{
		return number;
	}
	
	void setNumber(PMString number)
	{
		this->number = number;
	}
	
	double getParent_type_id()
	{
		return parent_type_id;
	}
	
	void setParent_type_id(double parent_type_id)
	{
		this->parent_type_id = parent_type_id;
	}
	
	int32 getSeq_order() 
	{
		return seq_order;
	}
	
	void setSeq_order(int32 seq_order) 
	{
		this->seq_order = seq_order;
	}

	double getId()
	{
		return id;
	}
	
	void setId(double id) 
	{
		this->id = id;
	}
	
	PMString getName() 
	{
		return name;
	}
	
	void setName(PMString name) 
	{
		this->name = name;
	}
};

#endif 