#ifndef __PARTNERCACHEVALUE__
#define __PARTNERCACHEVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "AssetValue.h"
#include "PartnerValue.h"
#include "vector"

using namespace std;

class PartnerCacheValue
{
private:


	vector<PartnerValue> brands;
	vector<PartnerValue> manufacturers;
	vector<PartnerValue> vendors;
	

public:
	
	PartnerCacheValue()
	{
		
	}
	
	~PartnerCacheValue()
	{
	}

	vector<PartnerValue> getBrands() {
		return brands;
	}
	void setBrands(vector<PartnerValue> brands) {
		this->brands = brands;
	}
	
	vector<PartnerValue> getManufacturers() {
		return manufacturers;
	}
	void setManufacturers(vector<PartnerValue> manufacturers) {
		this->manufacturers = manufacturers;
	}

	vector<PartnerValue> getVendors() {
		return vendors;
	}
	void setVendors(vector<PartnerValue> vendors) {
		this->vendors = vendors;
	}

	PartnerValue getBrandPartnerValueByBrandId(double brandId)
	{
		PartnerValue result;
		if(brands.size() > 0)
		{
			for(int32 i=0; i < brands.size(); i++)
			{
				if(brandId == brands.at(i).getId())
					{
						result = brands.at(i);
						break;
					}
			}

		}
		return result;
	}

	PartnerValue getManufacturerPartnerValueByMfgId(double mfgId)
	{
		PartnerValue result;
		if(manufacturers.size() > 0)
		{
			for(int32 i=0; i < manufacturers.size(); i++)
			{
				if(mfgId == manufacturers.at(i).getId())
					{
						result = manufacturers.at(i);
						break;
					}
			}

		}
		return result;
	}

	PartnerValue getVendorPartnerValueByBrandId(double vendorId)
	{
		PartnerValue result;
		if(vendors.size() > 0)
		{
			for(int32 i=0; i < vendors.size(); i++)
			{
				if(vendorId == vendors.at(i).getId())
					{
						result = vendors.at(i);
						break;
					}
			}

		}
		return result;
	}

	PMString getPartnerNameByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
	{
		PMString result("");
		PartnerValue partnerValueObj;

		switch (partnerType)
		{
		case 1:
			partnerValueObj = getBrandPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getName();
			break;

		case 2:
			partnerValueObj = getManufacturerPartnerValueByMfgId(partnerId);
			result = partnerValueObj.getName();
			break;

		case 3:
			partnerValueObj = getVendorPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getName();
			break;

		default:
			break;
		}

		return result;
	}


	CAssetValue getPartnerAssetValueByPartnerIdImageTypeId(double partnerId, int32 imageTypeId)
	{
		CAssetValue partnerAssetObj;
		if(partnerId > 0)
		{
			PartnerValue partnerValueObj;
			if( imageTypeId == -207 || imageTypeId == -208 || imageTypeId == -209 || imageTypeId == -210 || imageTypeId == -211 )
			{
				partnerValueObj = getBrandPartnerValueByBrandId(partnerId);
				if(imageTypeId == -207)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename1());
				else if(imageTypeId == -208)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename2());
				else if(imageTypeId == -209)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename3());
				else if(imageTypeId == -210)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename4());
				else if(imageTypeId == -211)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename5());
			}
			else if( imageTypeId == -212 || imageTypeId == -213 || imageTypeId == -214 || imageTypeId == -215 || imageTypeId == -216 )
			{
				partnerValueObj = getManufacturerPartnerValueByMfgId(partnerId);
				if(imageTypeId == -212)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename1());
				else if(imageTypeId == -213)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename2());
				else if(imageTypeId == -214)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename3());
				else if(imageTypeId == -215)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename4());
				else if(imageTypeId == -216)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename5());
			}
			else if( imageTypeId == -217 || imageTypeId == -218 || imageTypeId == -219 || imageTypeId == -220 || imageTypeId == -221 )
			{
				partnerValueObj = getVendorPartnerValueByBrandId(partnerId);
				if(imageTypeId == -217)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename1());
				else if(imageTypeId == -218)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename2());
				else if(imageTypeId == -219)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename3());
				else if(imageTypeId == -220)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename4());
				else if(imageTypeId == -221)
					partnerAssetObj.setUrl(partnerValueObj.getImageFilename5());
			}
		}
		return partnerAssetObj;
	}


	PMString getPartnerDescription1ByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
	{
		PMString result("");
		PartnerValue partnerValueObj;

		switch (partnerType)
		{
		case 1:
			partnerValueObj = getBrandPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getDescription1();
			break;

		case 2:
			partnerValueObj = getManufacturerPartnerValueByMfgId(partnerId);
			result = partnerValueObj.getDescription1();
			break;

		case 3:
			partnerValueObj = getVendorPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getDescription1();
			break;

		default:
			break;
		}

		return result;
	}

	PMString getPartnerDescription2ByPartnerIdPartnerType(double partnerId, int32 partnerType) // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
	{
		PMString result("");
		PartnerValue partnerValueObj;

		switch (partnerType)
		{
		case 1:
			partnerValueObj = getBrandPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getDescription2();
			break;

		case 2:
			partnerValueObj = getManufacturerPartnerValueByMfgId(partnerId);
			result = partnerValueObj.getDescription2();
			break;

		case 3:
			partnerValueObj = getVendorPartnerValueByBrandId(partnerId);
			result = partnerValueObj.getDescription2();
			break;

		default:
			break;
		}

		return result;
	}
	
};

#endif