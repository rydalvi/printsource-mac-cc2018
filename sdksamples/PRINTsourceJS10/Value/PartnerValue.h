#ifndef __PartnerValue_h__
#define __PartnerValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class PartnerValue 
{
public:
	 double 	    id;
	 PMString   name;
	 PMString 	number;
	 PMString 	code;
	 PMString 	description1;
	 PMString 	description2;	
	 int32 	    displayPartner;

	 PMString 	imageFilename1;
	 PMString 	imageFilename2;
	 PMString 	imageFilename3;
	 PMString 	imageFilename4;
	 PMString 	imageFilename5;
	
	double getId()
	{
		return id;
	}
	
	void setId(double id) 
	{
		this->id = id;
	}
	
	PMString getName() 
	{
		return name;
	}
	
	void setName(PMString name) 
	{
		this->name = name;
	}

	PMString getNumber() 
	{
		return number;
	}
	
	void setNumber(PMString no) 
	{
		this->number = no;
	}

	PMString getCode() 
	{
		return code;
	}
	
	void setCode(PMString code) 
	{
		this->code = code;
	}	

	int32 getDisplayPartner() 
	{
		return displayPartner;
	}
	
	void setDisplayPartner(int32 Id) 
	{
		this->displayPartner = Id;
	}


	PMString getImageFilename1() {
		return imageFilename1;
	}

	void setImageFilename1(PMString imageFilename1) {
		this->imageFilename1 = imageFilename1;
	}

	PMString getImageFilename2() {
		return imageFilename2;
	}

	void setImageFilename2(PMString imageFilename2) {
		this->imageFilename2 = imageFilename2;
	}

	PMString getImageFilename3() {
		return imageFilename3;
	}

	void setImageFilename3(PMString imageFilename3) {
		this->imageFilename3 = imageFilename3;
	}

	PMString getImageFilename4() {
		return imageFilename4;
	}

	void setImageFilename4(PMString imageFilename4) {
		this->imageFilename4 = imageFilename4;
	}

	PMString getImageFilename5() {
		return imageFilename5;
	}

	void setImageFilename5(PMString imageFilename5) {
		this->imageFilename5 = imageFilename5;
	}

	PMString getDescription1() {
		return description1;
	}

	void setDescription1(PMString description1) {
		this->description1 = description1;
	}

	PMString getDescription2() {
		return description2;
	}

	void setDescription2(PMString description2) {
		this->description2 = description2;
	}

};

#endif 
