#ifndef __PICKLISTGROUP__
#define __PICKLISTGROUP__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "PickListValue.h"
#include "vector"

using namespace std;

class PicklistGroup
{
private:
	PMString name;
	PMString code;
	PMString description;
	double typeId;
	vector<PicklistValue> picklistValues;

public:
	PicklistGroup()
	{
		name = "";
		code = "";
		description = "";
        typeId = -1;
	}
	
	~PicklistGroup()
	{
		//delete picklistValues;
	}
	

	double getTypeId() {
		return typeId;
	}
	void setTypeId(double typeId) {
		this->typeId = typeId;
	}
	PMString getName() {
		return name;
	}
	void setName(PMString name) {
		this->name = name;
	}
	PMString getCode() {
		return code;
	}
	void setCode(PMString code) {
		this->code = code;
	}
	PMString getDescription() {
		return description;
	}
	void setDescription(PMString description) {
		this->description = description;
	}
	vector<PicklistValue> getPicklistValues() {
		return picklistValues;
	}
	void setPicklistValues(vector<PicklistValue> picklistValues) {
		this->picklistValues = picklistValues;
	}
};

#endif