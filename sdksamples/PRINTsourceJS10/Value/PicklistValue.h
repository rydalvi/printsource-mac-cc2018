#ifndef __PICKLISTVALUE__
#define __PICKLISTVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class PicklistValue
{
private:
	double valueId;
	PMString abbreviation;
	PMString possibleValue;
	PMString description;	
	PMString imageFileName1;
	PMString imageFileName2;
	PMString imageFileName3;
	PMString imageFileName4;
	PMString imageFileName5;

public:
	PicklistValue()
	{
		valueId = -1;
		abbreviation = "";
		possibleValue = "";
		description = "";	
		imageFileName1 = "";
		imageFileName2 = "";
		imageFileName3 = "";
		imageFileName4 = "";
		imageFileName5 = "";

	}
	
	~PicklistValue()
	{
	}
	

	double getValueId() {
		return valueId;
	}

	void setValueId(double valueId) {
		this->valueId = valueId;
	}
	
	PMString getAbbreviation() {
		return abbreviation;
	}

	void setAbbreviation(PMString abbreviation) {
		this->abbreviation = abbreviation;
	}

	PMString getPossibleValue() {
		return possibleValue;
	}

	void setPossibleValue(PMString possibleValue) {
		this->possibleValue = possibleValue;
	}

	PMString getDescription() {
		return description;
	}

	void setDescription(PMString description) {
		this->description = description;
	}

	PMString getImageFileName1() {
		return imageFileName1;
	}

	void setImageFileName1(PMString imageFileName1) {
		this->imageFileName1 = imageFileName1;
	}

	PMString getImageFileName2() {
		return imageFileName2;
	}

	void setImageFileName2(PMString imageFileName2) {
		this->imageFileName2 = imageFileName2;
	}

	PMString getImageFileName3() {
		return imageFileName3;
	}

	void setImageFileName3(PMString imageFileName3) {
		this->imageFileName3 = imageFileName3;
	}

	PMString getImageFileName4() {
		return imageFileName4;
	}

	void setImageFileName4(PMString imageFileName4) {
		this->imageFileName4 = imageFileName4;
	}

	PMString getImageFileName5() {
		return imageFileName5;
	}

	void setImageFileName5(PMString imageFileName5) {
		this->imageFileName5 = imageFileName5;
	}
};

#endif