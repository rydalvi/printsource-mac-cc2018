#ifndef __CPOSSIBLEVALUEMODEL_h__
#define __CPOSSIBLEVALUEMODEL_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CPossibleValueModel
{
private:
	double pv_id;
	PMString name;
	double   typeId;
	PMString abbrevation;
	PMString description;	
	long   seq_order;
	
public:

	CPossibleValueModel()
	{
	}
	//getter methods
	double getPv_id()
	{
		return pv_id;
	}
	
	PMString getName()
	{
		return name;
	}
	
	double getTypeId()
	{
		return typeId;
	}
	
	PMString getAbbrevation()
	{
		return abbrevation;
	}
	
	PMString getDescription()
	{
		return description;
	}	
	long getSeq_order()
	{
		return  seq_order;
	}

	//setter methods
	void setPv_id(double num)
	{
		pv_id = num;
	}
	
	void setName(PMString str)
	{
		name = str;
	}
	
	void setTypeId(double num)
	{
		typeId = num;
	}
	
	void setAbbrevation(PMString str)
	{
		abbrevation = str;
	}
	
	void setDescription(PMString str)
	{
		description = str;
	}	
	void setSeq_order(long num)
	{
		seq_order = num;
	}
	
	

};

#endif