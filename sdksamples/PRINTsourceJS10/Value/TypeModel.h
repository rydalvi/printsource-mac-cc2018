#ifndef __CTypeModel_h__
#define __CTypeModel_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CTypeModel 
{
	   
private:	
	double 		typeId;
	double 		typeGroupId;
	PMString 	name;
	PMString 	code;
	//int32 		seq_order;
	PMString 	description;
	//PMString	info;		
	PMString	url;
	bool16		active;
	bool16		cdActive;
	bool16		printActive;
	bool16		webActive;
	int32		thumbnail;
	int32		flag1;
	int32		flag2;
	
	
public:
	 CTypeModel() 
	 {	
		typeId = 0;
		typeGroupId =0;
		name= "";
		code = "";
		description = "";
		//info = "";		
		url = "";
		active= kFalse;
		cdActive= kFalse;
		printActive= kTrue;
		webActive= kFalse;
		thumbnail=0;
		flag1=0;
		flag2=0;
	}
	/**
	 * @return Returns the active.
	 */
	bool16 getActive() {
		return active;
	}
	/**
	 * @param active The active to set.
	 */
	void setActive(bool16 active) {
		this->active = active;
	}
	/**
	 * @return Returns the code.
	 */
	PMString getCode() {
		return code;
	}
	/**
	 * @param code The code to set.
	 */
	void setCode(PMString code) {
		this->code = code;
	}
	/**
	 * @return Returns the description.
	 */
	PMString getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	void setDescription(PMString description) {
		this->description = description;
	}
	///**
	// * @return Returns the info.
	// */
	//PMString getInfo() {
	//	return info;
	//}
	///**
	// * @param info The info to set.
	// */
	//void setInfo(PMString info) {
	//	this->info = info;
	//}
	/**
	 * @return Returns the name.
	 */
	PMString getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	void setName(PMString name) {
		this->name = name;
	}
	/**
	 * @return Returns the seq_order.
	 */
//	int32 getSeq_order() {
//		return seq_order;
//	}
	/**
	 * @param seq_order The seq_order to set.
	 */
//	void setSeq_order(int32 seq_order) {
//		this->seq_order = seq_order;
//	}
	/**
	 * @return Returns the typeGroupId.
	 */
	double getTypeGroupId() {
		return typeGroupId;
	}
	/**
	 * @param typeGroupId The typeGroupId to set.
	 */
	void setTypeGroupId(double typeGroupId) {
		this->typeGroupId = typeGroupId;
	}
	/**
	 * @return Returns the typeId.
	 */
	double getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId The typeId to set.
	 */
	void setTypeId(double typeId) {
		this->typeId = typeId;
	}
	/**
	 * @return Returns the url.
	 */
	PMString getUrl() {
		return url;
	}
	/**
	 * @param url The url to set.
	 */
	void setUrl(PMString url) {
		this->url = url;
	}

	/**
	 * @return Returns the cdActive.
	 */
	bool16 getCdActive() {
		return cdActive;
	}
	/**
	 * @param cdActive The cdActive to set.
	 */
	void setCdActive(bool16 cdActive) {
		this->cdActive = cdActive;
	}
	
	/**
	 * @return Returns the printActive.
	 */
	bool16 getPrintActive() {
		return printActive;
	}
	/**
	 * @param printActive The printActive to set.
	 */
	void setPrintActive(bool16 printActive) {
		this->printActive = printActive;
	}

	/**
	 * @return Returns the webActive.
	 */
	bool16 getWebActive() {
		return webActive;
	}
	/**
	 * @param webActive The webActive to set.
	 */
	void setWebActive(bool16 webActive) {
		this->webActive = webActive;
	}

	/**
	 * @return Returns the thumbnail.
	 */
	int32 getThumbnail() {
		return thumbnail;
	}
	/**
	 * @param thumbnail The thumbnail to set.
	 */
	void setThumbnail(int32 thumbnail) {
		this->thumbnail = thumbnail;
	}

	/**
	 * @return Returns the flag1.
	 */
	int32 getFlag1() {
		return flag1;
	}
	/**
	 * @param flag1 The flag1 to set.
	 */
	void setFlag1(int32 flag1) {
		this->flag1 = flag1;
	}

	/**
	 * @return Returns the flag2.
	 */
	int32 getFlag2() {
		return flag2;
	}
	/**
	 * @param flag2 The flag2 to set.
	 */
	void setFlag2(int32 flag2) {
		this->flag2 = flag2;
	}
};

#endif
