#ifndef __GETSECTIONDATA_H__
#define __GETSECTIONDATA_H__

#include <vector>
#include <set>
#include <map>

#include "VCPluginHeaders.h"

using namespace std;

class GetSectionData
{
public:

	double SectionId;
	double PublicationId;
	double CatagoryId;
	double languageId;

	bool16 addCopyFlag;
	bool16 addProductCopyFlag;
	bool16 addImageFlag;
	bool16 addProductImageFlag;
	bool16 addDBTableFlag;
	bool16 addProductDBTableFlag;
	bool16 addCustomTablePresentFlag;
	bool16 addHyTableFlag;
	bool16 addComponentTableFlag;
	bool16 addAccessoryTableFlag;
	bool16 addXRefTableFlag;

	bool16 isEventField;

	bool16 addChildCopyAndImageFlag;
	bool16 addProductChildCopyAndImageFlag;


	bool16 addItemBMSAssetsFlag;
	bool16 addProductBMSAssetsFlag;
	bool16 addPubLogoAssetFlag;

	bool16 addItemPVMPVAssetFlag;
	bool16 addProductPVMPVAssetFlag;
	bool16 addSectionPVMPVAssetFlag;
	bool16 addPublicationPVMPVAssetFlag;
	bool16 addCatagoryPVMPVAssetFlag;

	bool16 isOneSource;
	
	bool16 isGetWholePublicationOrCatagoryDataFlag;
	
	bool16 addSectionLevelCopyAttrFlag;
	bool16 addPublicationLevelCopyAttrFlag;
	bool16 addCatagoryLevelCopyAttrFlag;

	bool16 addEventSectionImages;
	bool16 addCategoryImages;

	vector<double> itemIdList;
	vector<double> productIdList;
	vector<double> hybridIdList;

	vector<double > itemPVAssetIdList;
	vector<double > productPVAssetIdList;
	vector<double > sectionPVAssetIdList;
	vector<double > publicationPVAssetIdList;
	vector<double > catagoryPVAssetIdList;

	vector<double > categoryAssetIdList;
	vector<double > eventSectionAssetIdList;


	GetSectionData()
	{
		addCopyFlag = kFalse;
		addProductCopyFlag = kFalse;
		addImageFlag = kFalse;
		addProductImageFlag = kFalse;
		addDBTableFlag = kFalse;
		addProductDBTableFlag = kFalse;
		addCustomTablePresentFlag = kFalse;
		addHyTableFlag = kFalse;
		addComponentTableFlag = kFalse;
		addAccessoryTableFlag = kFalse;
		addXRefTableFlag = kFalse;

		isEventField = kFalse;

		addChildCopyAndImageFlag = kFalse;
		addProductChildCopyAndImageFlag = kFalse;

		addItemBMSAssetsFlag = kFalse;
		addProductBMSAssetsFlag = kFalse;
		addPubLogoAssetFlag = kFalse;

		addItemPVMPVAssetFlag = kFalse;
		addProductPVMPVAssetFlag = kFalse;
		addSectionPVMPVAssetFlag = kFalse;
		addPublicationPVMPVAssetFlag = kFalse;
		addCatagoryPVMPVAssetFlag = kFalse;

		isOneSource = kFalse;
		
		isGetWholePublicationOrCatagoryDataFlag = kFalse;
		
		addSectionLevelCopyAttrFlag = kFalse;
		addPublicationLevelCopyAttrFlag = kFalse;
		addCatagoryLevelCopyAttrFlag = kFalse;

		addEventSectionImages = kFalse;
		addCategoryImages = kFalse;


		
	}

	void setAllkTrue()
	{
		addCopyFlag = kTrue;
		addProductCopyFlag = kTrue;
		addImageFlag = kTrue;
		addProductImageFlag = kTrue;
		addDBTableFlag = kTrue;
		addProductDBTableFlag = kTrue;
		addCustomTablePresentFlag = kTrue;
		addHyTableFlag = kTrue;
		addComponentTableFlag = kFalse;
		addAccessoryTableFlag = kFalse;
		addXRefTableFlag = kFalse;

		isEventField = kTrue;

		addChildCopyAndImageFlag = kTrue;
		addProductChildCopyAndImageFlag = kTrue;

		addItemBMSAssetsFlag = kTrue;
		addProductBMSAssetsFlag = kTrue;
		addPubLogoAssetFlag = kTrue;

		addItemPVMPVAssetFlag = kTrue;
		addProductPVMPVAssetFlag = kTrue;
		addSectionPVMPVAssetFlag = kTrue;
		addPublicationPVMPVAssetFlag = kTrue;
		addCatagoryPVMPVAssetFlag = kTrue;

		isOneSource = kFalse;
		
		isGetWholePublicationOrCatagoryDataFlag = kTrue;
		
		addSectionLevelCopyAttrFlag = kTrue;
		addPublicationLevelCopyAttrFlag = kTrue;
		addCatagoryLevelCopyAttrFlag = kFalse;

		addEventSectionImages = kTrue;
		addCategoryImages = kFalse;

	}


};

typedef set<double> UniqueIds;

struct GetMultipleSectionData
{
	double sectionId;
	double publicationId;
	double languageId;

	UniqueIds itemIds;
	UniqueIds productIds;
};

typedef vector<GetMultipleSectionData> vec_GetMultipleSectionData;
typedef vector<GetMultipleSectionData>* vec_GetMultipleSectionDataPtr;

typedef bool16 IsMultipleSectionsDataAvailable;
typedef map<double, UniqueIds > multiSectionMap;

#endif